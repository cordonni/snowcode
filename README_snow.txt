Interactive Generation of Time-evolving, Snow-Covered Landscapes with Avalanches
================================================================================
Source code manual

* Setup
** Dependencies
   - Qt5 (tested with 5.6.1+)
   - OpenGL (4.3+)
   - boost (tested with 1.62.0)
   - CUDA (tested with 8.0+)
     
*** Installing Qt/Boost
    - On Windows
      Install and compile boost libs. Make sure that headers are in the "boost"
      directory, and that "boost_regex.lib" is in a "lib" directory, at the same
      level.
   
      Create a file named UserConfiguration.pro in the Build-QMake directory of
      expressive, with the following:
   
      ```
      windows {
        BOOST_HOME = C:\path_to_boost
      }
      ```
   
    - On Debian/Ubuntu
      ```
      > sudo apt-get install qt5 qtcreator libboost-all-dev
      ```

*** Installing CUDA
    Install the latest CUDA package from Nvidia's website, as well as the
    provided samples.
    
    Add the following lines to your UserConfiguration.pro file:

    ```
    CUDA_SAMPLES = "path_to_cuda_samples"       # Path to cuda SDK install
    CUDA_DIR = "path_to_cuda_toolkit"           # Path to cuda toolkit install
    ```

   Then open snowProject.pro with qtcreator.

** Compilation hints
*** Multi processor build
    Go to the "project" tab in the left panel of qtcreator, under "Compilation
    Steps", Make arguments, add '-j 12', replacing 12 by your number of CPU cores.

*** Qt version
    To make sure that you are using the right Qt version, go to "Project" >
    "Manage Kits". You can then add new Qt version by going to the "Qt Versions"
    tab, or select the version to use in the "Kits" tab.
    
*** CUDA C/C++ compiler
    CUDA needs a C/C++ compiler to work, but the latest versions are not always
    supported. In case of CUDA related compilation error, adding a binary or
    shortcut to a downgraded compiler in the CUDA toolkit bin directory often
    solves the issue. On Linux, gcc and g++ 4.8 and 5 are supported.

*** qmake
    qmake is not always automatic, especially on complex projects. When an
    unexpected error occurs, in the top menu, go to "compile">"run qmake", then
    build
    
* Project
** Structure
   This source code is part of the expressive project (https://gforge.inria.fr/scm/?group_id=5655).

   The main function is in the (qtcreator) subfolder snowBins, under Sources,
   landscape, snowmain.cpp (physical path src/landscape/snowmain.cpp)

   The main files specific to this project are located in the qtcreator folders
   "snow">"Sources">"Snow" and "snow">"Sources">"CUDA" (physical paths
   src/landscape/Grid).

   The important files are:
   - Snow/SnowEvent*.cu: GPU implementation of the different events
   - Snow/RegisterSnow.cpp: parameters of the events and interface setup
   - Snow/SnowData(.h, .cpp): simulation parameters declaration and initialization
   - Snow/SnowDataIO(.h, .cpp): import and export functions for the simulation
   - Snow/SnowRenderFunc.cu: rendering of the terrain
   - CUDA/StochasticSimulationCUDA(.h, .cpp): GPU event engine

** Adding an event
   If needed, you can add new variables to the grid in SnowData(.h, .cpp). These
   variables can also be added to the interface for interactive manipulation in
   the RegisterSnow.cpp file.

   Then, add the implementation in a new file Snow/SnowEventTest.cu and its
   declaration in SnowData.h. To allow compilation of your file, add it to the
   project with a new line in the snow/snow.pro project file. Finally, you need
   to register the event to the engine, by adding a RegisterEvent call in
   RegisterSnow.cpp.
   
** Refined ski tracks
   Since the refined ski tracks are only used for rendering purposes and do not
   interact with the rest of the simulation, they are disabled by default. To
   enable them, the macro DETAILED_TRACKS must be defined in SnowData.h.

** Data and results folders
   The input heightmaps are in "resources/heightfields".
   The location of the data and results is defined by the user when exporting.

** Interface
*** Timeline and player
   The timeline in the bottom of the window allows the user to replay the
   animation: the replay state is shown in blue and the simulation state in
   red.

   Below the timeline, from left to right, are buttons with the following
   functions:
   - Toggle recording of simulated frames in the timeline
   - Restart simulation
   - Go to first frame
   - Play / pause
   - Previous frame
   - Current frame number (replay)
   - Next frame
   - Last frame
   - Playback speed (ms per frame)
   - Last frame (simulation target)

*** Top menu
    - Load/Save: load or save the whole animation in a binary format
    - Import: import a file in a terragen .TER format and restart the simulation
    - Export: export all maps of a frame in a folder. The terrain heightmap is
      stored as "bedrock.ter" and the snow heightmap as "snow.ter". Textures for
      the illumination and ski tracks are saves as respectively "illum.png" and
      "ski_tracks.png"
      
*** Left menu
    - Parameters: view and change the simulation parameters
    - Layers: toggle the display of specific layers of the model on the
      terrain. Also includes a brush icon to select on which layer to paint
    - Paint: define the parameters of the user brush, allowing live edits of the
      simulation
    - Cam: store the current parameters of the camera in the clipboard, useful
      to render the scene from a specific viewpoint later on

*** OpenGlView
    The point of the scene under the mouse is the center of all view manipulations
    - Left click: paint on the selected layer with the currently defined brush
    - Middle click: pan the camera
    - Right ckick: rotate around the scene
    - Mouse wheel: zoom in/out
