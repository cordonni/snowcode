import QtQuick 2.0
import QtGraphicalEffects 1.0


Item {
    id: button
    width: 100
    height: 80

    property string icon : "../../gui/images/VIEW_SIDE.svg"
    objectName: "OrientationButton"
    signal clicked

    Image {
        id:button_icon
        source: icon
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
    }
   Glow {
           id:glow
           anchors.fill: button_icon
           radius: 0
           samples: 17
           color: "white"
           source: button_icon
           Behavior on radius {
               NumberAnimation { duration : 200 }
           }
       }

    MouseArea {
        id: mouseArea1
        width: 50
        hoverEnabled: true
        anchors.fill: parent
        onClicked: {
            parent.clicked();
        }
        onContainsMouseChanged: {
            parent.state= containsMouse ? "Hovering" : ""
        }
    }

    states: [
        State {
            name: ""
            PropertyChanges {
                target:glow
                radius:0
            }
        },
        State {
            name:"Hovering"
            when:mouseArea1.containsMouse
            PropertyChanges {
                target:glow
                radius:25
            }
        }
    ]

    transitions: [
        Transition {
            from: "*"; to: "Hovering"
            NumberAnimation {duration: 200 }
        },

        Transition {
            from: "Hovering"; to: "*"
            NumberAnimation {duration: 200 }
        }
    ]

}

