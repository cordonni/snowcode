import QtQuick 2.0
import QtGraphicalEffects 1.0

BaseButton {
    id: button
    rect.radius:1
    buttonOpacity:0.75
    shear : -0.5
    width:80

    invisible_x:0.0
    invisible_y:0.0
    fontSize : 12.0
}
