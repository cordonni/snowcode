import QtQuick 2.0
import QtGraphicalEffects 1.0

import "../"

import Expressive 1.0

Item {
    Camera {
        id: camera
        engine:expressive_engine
    }

    Text {
        text: "Views :"
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: circle.left
        anchors.rightMargin: 20
    }

    Circle {
        id: circle
        anchors.fill:parent
        color :"transparent"
        border.width: 5
        border.color: "#15728d"
        opacity:1
    }

    Image {
        id:image_front
        source: "../../../gui/images/VIEWS_FRONT.svg"
        height:parent.height / 2.0
        width: parent.height / 2.0
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
//        anchors.fill:parent
//        layer.enabled: false
//        layer.effect: Glow {
//            radius:2
//            color:"white"
//            samples:3
//            transparentBorder:true
//        }
        MouseArea {
            id:mouseAreaFront
            hoverEnabled: true
            anchors.fill:parent
            onClicked: {
                camera.set_position(Camera.E_DEFAULT)
            }
        }
        states : [
            State {
                name:"Hovering"
                when: mouseAreaFront.containsMouse
                PropertyChanges {
                    target:image_front
//                    layer.enabled: true
                    source:"../../../gui/images/VIEWS_FRONT_HOVERED.svg"
                }
            }
        ]
    }

    CardinalPoint {
       id: north
       anchors.verticalCenter: circle.top
       anchors.verticalCenterOffset: circle.border.width / 2.0
       anchors.horizontalCenter: circle.horizontalCenter
       onClicked: {
           camera.set_position(Camera.E_TOP)
       }
    }
    CardinalPoint {
       id: east
       anchors.verticalCenter: circle.verticalCenter
       anchors.horizontalCenterOffset: -circle.border.width / 2.0
       anchors.horizontalCenter: circle.right
       onClicked: {
           camera.set_position(Camera.E_RIGHT)
       }
    }
    CardinalPoint {
       id: south
       anchors.verticalCenter: circle.bottom
       anchors.verticalCenterOffset: -circle.border.width / 2.0
       anchors.horizontalCenter: circle.horizontalCenter
       onClicked: {
           camera.set_position(Camera.E_BOTTOM)
       }
    }
    CardinalPoint {
       id: west
       anchors.verticalCenter: circle.verticalCenter
       anchors.horizontalCenterOffset: circle.border.width / 2.0
       anchors.horizontalCenter: circle.left
       onClicked: {
           camera.set_position(Camera.E_LEFT)
       }
    }

    Image {
        id:image_side
        source: "../../../gui/images/VIEWS_SIDE.svg"
        height:parent.height / (3.0 / 2.0)
        width: parent.height / (3.0 / 2.0)
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset: parent.width + 20
//        anchors.fill:parent
//        layer.enabled: false
        property int last_used: Camera.E_LEFT
//        layer.effect: Glow {
//            radius:2
//            color:"white"
//            samples:3
//            transparentBorder:true
//        }
        MouseArea {
            id:mouseAreaSide
            hoverEnabled: true
            anchors.fill:parent
            onClicked: {
                image_side.last_used = (image_side.last_used === Camera.E_LEFT) ? Camera.E_RIGHT : Camera.E_LEFT
                camera.set_position(image_side.last_used)
            }
        }
        states : [
            State {
                name:"Hovering"
                when: mouseAreaSide.containsMouse
                PropertyChanges {
                    target:image_side
//                    layer.enabled: true
                    source:"../../../gui/images/VIEWS_SIDE_HOVERED.svg"
                }
            }
        ]
    }


    Rectangle {
        id:line
        width: parent.width
        height:3
        color:"#15728d"
        anchors.verticalCenter: image_side.verticalCenter
        anchors.horizontalCenter: image_side.horizontalCenter
    }
    CardinalPoint {
       id: front
       anchors.verticalCenter: line.verticalCenter
       anchors.horizontalCenter: line.right
       onClicked: {
           camera.set_position(Camera.E_FRONT)
       }
    }
    CardinalPoint {
       id: back
       anchors.verticalCenter: line.verticalCenter
       anchors.horizontalCenter: line.left
       onClicked: {
           camera.set_position(Camera.E_BACK)
       }
    }
}
