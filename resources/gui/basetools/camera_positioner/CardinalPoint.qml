import QtQuick 2.0
import "../"

Circle {
    id: root
    width: parent.width / 6.0
    border.color :"#15728d"
    color:"transparent"
    opacity:1
    signal clicked()
    
    Behavior on border.color {
        ColorAnimation { duration : 200 }
    }
    MouseArea {
        id: mouseArea1
        width: 50
        hoverEnabled: true
        anchors.fill: parent
        onClicked: {
            parent.clicked();
        }
        onContainsMouseChanged: {
            parent.state= containsMouse ? "Hovering" : ""
        }
    }

    states: [
        State {
            name: ""
            PropertyChanges {
                target:root
                border.color:"#15728d"
                border.width:1
            }
        },
        State {
            name:"Hovering"
            when:mouseArea1.containsMouse
            PropertyChanges {
                target:root
                border.color:"white"
                border.width:2
            }
        }
    ]
}
