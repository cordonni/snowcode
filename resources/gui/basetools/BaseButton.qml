import QtQuick 2.5
import QtGraphicalEffects 1.0

Item {

    id: button
    width: 50
    height: 30

    property string icon: ""
    property string caption : qsTr("")
    property real buttonOpacity : 1.0
    property real radius : 0.0
    property string buttonColor : "#a7d8ed"
    property string pressedColor : "#486069"
    property string activeColor : "#fdc300"
    property string hoveringColor : "#338926"
    property string shadowColor : "#486069"
    property string textColor : "#040404"
    property bool checkable: false
    property var targetTile
    property bool pressed: false
    x : 0
    y : 0
    opacity:buttonOpacity
    property int original_x : 0
    property int original_y : 0
    property real invisible_x : x
    property real invisible_y : y
    property bool interactible : true
    property real shear : 0.0

    property alias fontSize : buttonLabel.font.pointSize
    property alias color : _rect.color
    property alias border : _rect.border
    property alias rect : _rect
    property alias img : _img

    property bool activateOnHovering : false
    property bool horizontalDeployement : true

    objectName: "BaseButton"
    signal clicked(var clicked_button)

    Component.onCompleted: {
        original_x = x
        original_y = y
    }

    onClicked: {
        if (targetTile) {
            targetTile.visible = !targetTile.visible
        }
     }
//    onStateChanged: {
//        console.log(caption + "::" + state)
//    }

    function set_position(new_x, new_y) {
        original_x = new_x
        original_y = new_y
        x = new_x
        y = new_y
    }

    function show(do_show) {
        interactible=do_show
    }



    Rectangle {
        id: _rect
        color:buttonColor
        anchors.fill:parent
        radius:parent.radius

        transform: Matrix4x4 {
            matrix: Qt.matrix4x4(1, shear, 0, 0,
                                 0,     1, 0, 0,
                                 0,     0, 1, 0,
                                 0,     0, 0, 1)
        }
//        Rectangle {
//            id: _rectShadow
//            color:(icon!=="" && caption!=="")?_rect.color : "transparent"
//            anchors.top:_rect.bottom
//            anchors.topMargin: -1
//            width:_rect.width
//            height:buttonLabel.font.pointSize
//    //        x : (shear / Math.abs(shear)) * height

//        }
    }

    Image {
        id:_img
        source: icon
        width:parent.width
        height:parent.height
        x : 0.5 * shear * width / 2.0
        fillMode: Image.PreserveAspectFit
        //        sourceSize.height: 800
        smooth: true
    }

    Text {
        id: buttonLabel
        width: parent.width
//        height: parent.height
        text: caption
        color: textColor

//        text: "#MENU#"
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: _rect.horizontalCenter
        anchors.horizontalCenterOffset: icon==="" ? 0.5 * shear * width / 2.0 : 0.0

        anchors.verticalCenterOffset: 0
        anchors.verticalCenter:icon==="" ? parent.verticalCenter : parent.top
        transformOrigin: Item.Center
//        font.pixelSize: 12
    }

    MouseArea {
        id: mouseArea1
        hoverEnabled: true
        anchors.fill: parent
        z:4
        onClicked: {
            parent.pressed = !parent.pressed
            parent.clicked(button);
        }
        onContainsMouseChanged: {
            if (containsMouse  && activateOnHovering && !parent.pressed) {
                parent.pressed = !parent.pressed
                parent.clicked(button);
            }
        }
    }

    states: [
        State {
            name: ""
            PropertyChanges {
                target:button
                color: buttonColor
                opacity:buttonOpacity
//                visible:true
                x:original_x;
                y:original_y;
            }
        },
        State {
            name: "Pressed"
            when:mouseArea1.pressedButtons && interactible// || (activateOnHovering && mouseArea1.containsMouse)
            PropertyChanges {
                target:button
                color:pressedColor
//                x:original_x;
//                y:original_y;
            }
        },
        State {
            name:"Hovering"
            when:mouseArea1.containsMouse && interactible // && !activateOnHovering
            PropertyChanges {
                target:button
                color:hoveringColor
//                x:original_x;
//                y:original_y;
            }
        },
        State {
            name:"Activated"
            when: (targetTile && targetTile.visible) || (button.checkable && button.pressed)
            PropertyChanges {
                target:button
                color:activeColor
//                x:original_x;
//                y:original_y;
            }
        },
        State {
            name:"Invisible"
            when:!interactible
            PropertyChanges{
                target:button;
                opacity:0.0;
//                visible:false
                x:invisible_x;
                y:invisible_y;
            }
        }

    ]

    transitions: [
        Transition {
            from: "*"; to: "Hovering"
            ColorAnimation {duration: 200 }
        },
        Transition {
            from: "*"; to: "Pressed"
            ColorAnimation {duration: 10 }
        },
        Transition {
            from: "Invisible"; to: "*"
            SequentialAnimation {
                NumberAnimation {
                    target:button
                    properties : horizontalDeployement ? "opacity,x,visible" : "opacity,y,visible"
                    duration:350
                    easing.type:Easing.InOutQuad
                }
                NumberAnimation {
                    target:button
                    properties : horizontalDeployement ? "y" : "x"
                    duration:250
                    easing.type:Easing.InOutQuad
                }
            }
        },
        Transition {
            from: "*"; to: "Invisible"
            SequentialAnimation {
                NumberAnimation {
                    target:button
                    properties : horizontalDeployement ? "y" : "x"
                    duration:250
                    easing.type:Easing.InOutQuad
                }
                NumberAnimation {
                    target:button
                    properties : horizontalDeployement ? "opacity,x,visible" : "opacity,y,visible"
                    duration:450
                    easing.type:Easing.InOutQuad
                }
            }
        }
    ]
}

