import QtQuick 2.0
import QtQuick.Layouts 1.1

Component {
    id:root
    Rectangle {
        height:15
        width:200
        color:"orange"

//        Text {
//            anchors.fill:parent
//            text: name
//        }

        GridLayout {
            anchors.fill:parent
            columns: 4
//            Text {
//                height:15
//                width:15
//                text: model.name
//            }

            Item {
                Layout.columnSpan: 3
                height:15
                Loader {
                    id:itemLoader
//                    source:

//                    onLoaded: {
//                        console.log("loaded")
//                        item.caption = model.name + " : "
//                        item.value = model.value
//                    }
                    function updateEditorValue()
                    {
//                        console.log(this)
//                        console.log(model.name + "---> " +  model.value)
                        item.value = model.value
                    }

                    function updateModelValue()
                    {
//                        console.log("<--- " + item.value)
                        model.value = item.value
                    }

                    Component.onCompleted: {
                        var inputBoxType = model.type==="float" ? "InputNumberBox.qml" :
                                           model.type==="vector" ? "InputVectorBox.qml" :
                                           model.type==="bool" ? "InputCheckBox.qml" :
                                           "InputNumberBox.qml";

                        itemLoader.setSource(inputBoxType,
                                             {
                                                 "caption": model.name,
                                                 "value":model.value,
                                                 "constant": model.constant
                                             });
//                        console.log(model)
//                        console.log(model.valueChanged)
//                        console.log(model.modelData);
//                        console.log(model.modelData.valueChanged);
                        model.modelData.valueChanged.connect(updateEditorValue)
                        item.accepted.connect(updateModelValue)
                    }
                }
            }

//        Column {
//            visible:model.type==="float"
//            Layout.columnSpan: 3

//            InputNumberBox {
//                caption:"float value:"
//                value:model.type==="float"? model.value.toFixed(2) : 0.0
//                decimals:2
//                onAccepted: {
//                    model.value = value
//                }
//            }
//        }
//        Column {
//            visible:model.type==="vector"
//            Layout.columnSpan: 3
//            Row {
//                InputNumberBox {
//                    caption:"x :"
//                    value:value.x.toFixed(2)
//                    decimals:2
//                    onAccepted: {
//                        model.value.x = value
//                    }
//                }
//                InputNumberBox {
//                    caption:"y :"
//                    value:value.y.toFixed(2)
//                    decimals:2
//                    onAccepted: {
//                        model.value.y = value
//                    }
//                }
//                InputNumberBox {
//                    caption:"z:"
//                    value:value.z.toFixed(2)
//                    decimals:2
//                    onAccepted: {
//                        model.value.z = value
//                    }
//                }
//            }
//        }
        }
    }
}
