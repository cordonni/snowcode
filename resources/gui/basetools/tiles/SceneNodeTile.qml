import QtQuick 2.0
import QtQuick.Layouts 1.1

import "../"

Tile
{
    property var sceneManager;
    property var count:sceneManager ? sceneManager.selected_nodes.length : 0

    id:root
    title: "Node Properties"
    width:300
    height:200

    Component {
        id:selectedNodeDelegate

        Rectangle {
            border.width:1
            border.color: "grey"
            color:"transparent"
            width:root.width
            height:compoDelegateView.height + compoText.height

            Text {
                id:compoText
                height:15
                text:name
            }

            ListView {
                id:compoDelegateView
                height:30*parameters.length
                interactive:false
                model:parameters
                delegate:ValueEditor {}
            }
        }
    }

    GridLayout  {
        y:title_rect.height
        columns:4
        columnSpacing: 5
        rowSpacing:0
        width:parent.width
        Text {
            id:node_name
            Layout.columnSpan: 4
            anchors.horizontalCenter: parent.horizontalCenter
            text: count > 0 ? (count > 1 ? "Selection" : sceneManager.selected_nodes[0].name) : "No selection"
            font.pixelSize: 25
        }
        Text {
            id:node_type
            Layout.columnSpan: 4
            anchors.horizontalCenter: parent.horizontalCenter
            text: count > 0 ? (count > 1 ? "Various types" : sceneManager.selected_nodes[0].type) : ""
            font.pixelSize: 15
        }

        Rectangle {
            Layout.columnSpan: 4
            border.width: 1
            border.color: "blue"
            width:root.width
            height:delegates_view.height

            ListView {
                id:delegates_view
                interactive:false
                anchors.fill:parent
                height:200//30*sceneManager.selected_nodes.length
                model:sceneManager.selected_nodes
                delegate:selectedNodeDelegate
                onCountChanged: {
//                    console.log("selected count changed " + count + "::" + sceneManager.selected_nodes.length)
                }
            }
        }

////        Item {
////            Layout.columnSpan: 4
////            visible:count > 0
////            anchors.horizontalCenter: parent.horizontalCenter

//            Text { text:"position : " }

//            InputNumberBox {
//                id:posX
//                caption:"x: "
//                value: count === 1 ? sceneManager.selected_nodes[0].position.x.toFixed(2) : ""
//                decimals:2
//                onAccepted: {
//                    for (var i = 0; i < count; ++i) {
//                        sceneManager.selected_nodes[i].position.x = value
//                    }
//                }
//            }
//            InputNumberBox {
//                id:posY
//                caption:"y: "
//                value: count === 1 ? sceneManager.selected_nodes[0].position.y.toFixed(2) : ""
//                decimals:2
//                onAccepted: {
//                    for (var i = 0; i < count; ++i) {
//                        sceneManager.selected_nodes[i].position.y = value
//                    }
//                }
//            }
//            InputNumberBox {
//                id:posZ
//                caption:"z: "
//                value: count === 1 ? sceneManager.selected_nodes[0].position.z.toFixed(2) : ""
//                decimals:2
//                onAccepted: {
//                    for (var i = 0; i < count; ++i) {
//                        sceneManager.selected_nodes[i].position.z = value
//                    }
//                }
//            }
////        }
////        Item {
////            Layout.columnSpan: 4
////            visible:count > 0
////            anchors.horizontalCenter: parent.horizontalCenter

//            Text { text:"Target : "}

//            InputNumberBox {
//                caption:"x: "
//                value: count === 1 ? sceneManager.selected_nodes[0].orientation.x.toFixed(2) : ""
//                decimals:2
//                onAccepted: {
//                    for (var i = 0; i < count; ++i) {
//                        sceneManager.selected_nodes[i].orientation.x = value
//                    }
//                }
//            }
//            InputNumberBox {
//                caption:"y: "
//                value: count === 1 ? sceneManager.selected_nodes[0].orientation.y.toFixed(2) : ""
//                decimals:2
//                onAccepted: {
//                    for (var i = 0; i < count; ++i) {
//                        sceneManager.selected_nodes[i].orientation.y = value
//                    }
//                }
//            }
//            InputNumberBox {
//                caption:"z: "
//                value: count === 1 ? sceneManager.selected_nodes[0].orientation.z.toFixed(2) : ""
//                decimals:2
//                onAccepted: {
//                    for (var i = 0; i < count; ++i) {
//                        sceneManager.selected_nodes[i].orientation.z = value
//                    }
//                }
//            }
////        }
////        Item{
////            Layout.columnSpan: 4
////            visible:count > 0
////            anchors.horizontalCenter: parent.horizontalCenter

//            Text { text:"Scale : " }

//            InputNumberBox {
//                caption:"x: "
//                value: count === 1 ? sceneManager.selected_nodes[0].scale.x.toFixed(2) : ""
//                decimals:2
//                onAccepted: {
//                    for (var i = 0; i < count; ++i) {
//                        sceneManager.selected_nodes[i].scale.x = value
//                    }
//                }
//            }
//            InputNumberBox {
//                caption:"y: "
//                value: count === 1 ? sceneManager.selected_nodes[0].scale.y.toFixed(2) : ""
//                decimals:2
//                onAccepted: {
//                    for (var i = 0; i < count; ++i) {
//                        sceneManager.selected_nodes[i].scale.y = value
//                    }
//                }
//            }
//            InputNumberBox {
//                caption:"z: "
//                value: count === 1 ? sceneManager.selected_nodes[0].scale.z.toFixed(2) : ""
//                decimals:2
//                onAccepted: {
//                    for (var i = 0; i < count; ++i) {
//                        sceneManager.selected_nodes[i].scale.z = value
//                    }
//                }
//            }
//        }
    }
}
