import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.1

import "../"
import "scene/"
import Expressive 1.0

Tile {
    id : scene_tile
    width:parent.width
    title:"Scene"
    height:40
//    property var tile : this
    property var sceneManager
    objectName:"SceneNodeGraph"


    function getHeight () {
        var h = title_rect.height;
        for (var i = 0; i < view.count; ++i) {
            if (view.contentItem.children[i] && view.contentItem.children[i].objectName==="SceneNodeGraph") {
                h += view.contentItem.children[i].getHeight()
            }
        }
        return h;
    }

    ListView {
        id: view
        property var tile:scene_tile
        clip:true
        interactive:false
        model:sceneManager.nodes
        width:parent.width
        anchors.bottom:parent.bottom
        anchors.top:title_rect.bottom

        delegate : SceneNodeGraph { tile:scene_tile }
        onCountChanged: {
//            console.log("Total count changed " + count + "::" + sceneManager.nodes.length)
//            scene_tile.height += title_rect.height + view.count * (20 + 2)
        }
    }
    Behavior on height {
        NumberAnimation {
            duration:100
        }
    }
}
