import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.4

import "../../"
import Expressive 1.0

Rectangle {
    id:delegateItem

    property var tile : parent.parent.tile
    property bool clear_selection: true
    property bool collapsed:true
    property int rowSize : 18

    color:hovered ? "#A0FFA500" : "transparent"
    width:parent.parent.width
    height:getHeight()
    objectName:"SceneNodeGraph"

    onCollapsedChanged: {
        height = getHeight()
    }

    onHeightChanged: {
        var p = getParentNode()
        if (p) {
            p.height = p.getHeight()
        }
    }

    function getParentNode() {
        var p = parent
        while (p && p.objectName !== "SceneNodeGraph") {
            p = p.parent
        }
        return p;
    }

    function getHeight () {
        var h = rowSize;
        if (!collapsed) {
            for (var i = 0; i < children_view.count; ++i) {
                h += children_view.contentItem.children[i].getHeight()
            }
        }
        return h;
    }

    MouseArea {
        anchors.fill:parent
        hoverEnabled: true
        onContainsMouseChanged: model.hovered = containsMouse
        onClicked: {
            model.modelData.set_selected(!model.selected, true)
        }
    }

    GridLayout {
        id:myGrid
//        anchors.fill:parent
        columns: 3
        rowSpacing:0

        Rectangle {
            id:rect
            width:rowSize
            height:rowSize
            color:"transparent"
//            color:model.children.length > 0 ? "orange" : "transparent"
            Image {
                anchors.fill:parent
                anchors.margins: 3
                source: model.children.length > 0 ? (delegateItem.collapsed ? "../../../images/ICO_ARROW_RIGHT.svg" : "../../../images/ICO_ARROW_BOTTOM.svg") : ""
            }
            MouseArea {
                anchors.fill:parent
                onClicked:{
                    if (model.children.length > 0) {
//                        children_view.deployed = !children_view.deployed
                        collapsed = !collapsed
                        delegateItem.height = getHeight()
                    }
                }
            }
        }

        Rectangle {
            id:textBoundaries
            width: delegateItem.width - rect.width - boxes.width - myGrid.columnSpacing
            height:rowSize
            color:"transparent"
            Text {
                clip:true
                text: type +"(" +name+")"
                font.pixelSize:10
                anchors.verticalCenter: parent.verticalCenter
                width:parent.width
//                text: getHeight() + "::" + delegateItem.height + "::" +children_view.height + "::" + collapsed
//                text:width + "::"+ delegateItem.width +"::"+ rect.width +"::"+ boxes.width +"::"+ myGrid.columnSpacing
//                anchors.fill:parent
                elide:Text.ElideRight
            }
        }

        Column {
            Row {
                id:boxes
                height:rowSize
    //            Layout.preferredWidth: 60
                width:rowSize * 5
                CheckBox {
                    id:visibleCheckbox
                    onClicked: model.visible = checked
                    Component.onCompleted: {
                        checked = model.visible
                    }
                    Connections {
                        target: model
                        onVisibleChanged: visibleCheckbox.checked = model.visible
                    }
                    style: CheckBoxStyle {
                        indicator:
                            Image {
                                width:20
                                height:10
                                source:control.checked ? "../../../images/ICO_VISIBLE.svg" : "../../../images/ICO_INVISIBLE.svg"
                        }
                    }
                }
                CheckBox {
                    id:wireframeCheckbox
                    onClicked: model.wireframe = checked
                    Component.onCompleted: checked = model.wireframe
                    Connections {
                        target: model
                        onWireframeChanged: wireframeCheckbox.checked = model.wireframe
                    }
                    style: CheckBoxStyle {
                        indicator:
                            Image {
                                width:rowSize - 2
                                height:rowSize - 2
                                source:control.checked ? "../../../images/ICO_WIREFRAME.svg" : "../../../images/ICO_NO_WIREFRAME.svg"
                        }
                    }
                }
                CheckBox {
                    id:selectedCheckbox
                    Component.onCompleted: checked = model.selected
                    onClicked: model.modelData.set_selected(checked, false)

                    Connections {
                        target: model
                        onSelectedChanged: selectedCheckbox.checked = model.selected
                    }

                    style: CheckBoxStyle {
                        indicator: Rectangle {
                            implicitWidth: 16
                            implicitHeight: 16
                            radius: 3
                            border.color: control.activeFocus ? "darkblue" : "gray"
                            border.width: 1
                            Rectangle {
                                visible: control.checked
                                color: "#555"
                                border.color: "#333"
                                radius: 1
                                anchors.margins: 4
                                anchors.fill: parent
                            }
                        }
                    }
                }
            }
        }

        Rectangle {
            width:10
            height:rowSize
            color:"transparent"
//            border.color:"green"
//            border.width: 1
        }

        ListView {
            id:children_view
            property var tile: delegateItem.tile
            interactive:false
            visible:!delegateItem.collapsed
            Layout.columnSpan: 2
            width:boxes.width + textBoundaries.width//parent.width - 10
            height:children_view.count * rowSize//delegateItem.getHeight() - 20

            signal childrenHeightChanged();
            onCountChanged: {
                if (children_view.count != 0) {
                    delegateItem.height = delegateItem.getHeight()
                }
            }

            Rectangle {
//                border.width:1
                color:"transparent"
//                border.color:"red"
                anchors.fill:parent
            }
        }

        Component.onCompleted: {
            if (tile !== undefined) {
                children_view.model = model.children;
                children_view.delegate = Qt.createComponent("SceneNodeGraph.qml")
            }
        }
    }
}
