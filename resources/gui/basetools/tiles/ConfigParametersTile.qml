import QtQuick 2.0
import QtQuick.Layouts 1.1

import "../"
import Expressive 1.0

Tile {
    id:tile
    width:parent.width
    title:"Options"

    ConfigHandler
    {
        id:config
        engine:expressive_engine
    }

    ListView {
        id:view
        boundsBehavior: Flickable.StopAtBounds
        flickableDirection: Flickable.VerticalFlick
        clip: true
        interactive:true
        model: config.parameters
        width:parent.width
        anchors.bottom:parent.bottom
        anchors.top:title_rect.bottom
        delegate : Grid {
            columns : 2
            spacing : 2
            Text {
                text:name;
                width:200;
                height:15;
                color: (type === 0 || type === ConfigParam.E_FLOAT) ? "blue" : "red"
            }
            Text {
                text: (type === 0 || type === ConfigParam.E_FLOAT) ? float_value : string_value;
                width:300;
                height:15;
                color: (type === 0 || type === ConfigParam.E_FLOAT) ? "blue" : "red"
            }
        }
        onCountChanged: {
            tile.height = title_rect.height + view.count * (15 + 2)
        }
    }
}

