import QtQuick 2.2
import QtQuick.Controls 1.0

import "../../colorpicker"
import "../../colorpicker/content"

import ".."

import Expressive 1.0

Item {
    SketchingModeler
    {
        id: sketcher
        engine: expressive_engine
        color: "orange"
        brush_size:0.05
        symmetry_mode: SketchingModeler.E_NO_SYMMETRY
    }

    ColorPicker
    {
        id: colorPicker
        width:parent.width
        height:parent.height
        color:sketcher.color
        onColorPicked: {
            sketcher.color = color
        }
        deployed:false

    }

    Row {
        id : sketchingOptions
        anchors.right:colorPicker.left
        anchors.rightMargin: 100
        anchors.bottom:parent.bottom

        TileButton {
            id:skeleton
            caption:"Squelettes"
            height:60
            width:140
            rect.border.width:2.0
            buttonColor:"#73acc9"
            hoveringColor: buttonColor
            invisible_x: width * 4
            opacity:0.0
            Component.onCompleted: {
                show(sketcher.enabled)
            }
            ButtonEnum {
                id:skeletonEnum
                x:0
                y:-height
                width:parent.width
                Row {
                    width:parent.width
                    BaseButton {
                        caption: "Oui"
                        width:parent.width / parent.children.length
                        checkable: true
                        onClicked: sketcher.show_skeleton = true
                    }
                    BaseButton {
                        caption: "Non"
                        width:parent.width / parent.children.length
                        checkable: true
                        pressed:true
                        onClicked: sketcher.show_skeleton = false
                    }
                }
            }
        }
        TileButton {
            id:depth
            caption:"Projection"
            height:60
            width:140
            rect.border.width:2.0
            buttonColor:"#73acc9"
            hoveringColor: buttonColor
            invisible_x: width * 4
            opacity:0.0
            Component.onCompleted: {
                show(sketcher.enabled)
            }
            ButtonEnum {
                id:depthEnum
                x:0
                y:-height
                width:parent.width
                Row {
                    width:parent.width
                    BaseButton {
                        icon:"../../gui/images/PROJECTION_SURFACE.svg"
                        caption: "Oui"
                        width:parent.width / parent.children.length
                        checkable: true
                        pressed:true
                        onClicked: sketcher.depth_selection_mode = SketchingModeler.E_SURFACE
                    }
                    BaseButton {
                        icon:"../../gui/images/PROJECTION_PLANAR.svg"
                        caption: "Non"
                        width:parent.width / parent.children.length
                        checkable: true
                        onClicked: sketcher.depth_selection_mode = SketchingModeler.E_PLANE
                    }
                }
            }
            Rectangle {
                color:"black"
                width:3
                height:depthEnum.height
                y:-height
                anchors.horizontalCenter: parent.left
            }
        }
        TileButton {
            id:mode
            caption:"Mode"
            height:60
            width:140
            rect.border.width:2.0
            buttonColor:"#73acc9"
            hoveringColor: buttonColor
            invisible_x: width * 4
            opacity:0.0
            Component.onCompleted: {
                show(sketcher.enabled)
            }
            ButtonEnum {
                id:modeEnum
                x:0
                y:-height
                width:parent.width
                Row {
                    width:parent.width
                    BaseButton {
//                        icon:"../../gui/images/ICO_PLUS.svg"
                        width:parent.width / parent.children.length
                        caption: "Ajouter"
                        checkable: true
                        pressed:true
                        onClicked: {
                            sketcher.behavior_mode = SketchingModeler.E_ADD;
                        }
                    }
                    BaseButton {
//                        icon:"../../gui/images/ICO_MINUS.svg"
                        width:parent.width / parent.children.length
                        caption: "Creuser"
                        checkable: true
                        onClicked: {
                            sketcher.behavior_mode = SketchingModeler.E_REMOVE
                        }
                    }
//                    BaseButton {
//                        icon:"../../gui/images/ICO_CUT.svg"
//                        width:parent.width / parent.children.length
//                        caption: "Cut"
//                        checkable: true
//                        buttonColor: "grey"
//                        hoveringColor: "grey"
//                        onClicked: {
//                            sketcher.behavior_mode = SketchingModeler.E_CUT
//                        }
//                    }
                }
            }
            Rectangle {
                color:"black"
                width:3
                height:modeEnum.height
                y:-height
                anchors.horizontalCenter: parent.left
            }
        }
        TileButton {
            id:symmetry
            caption:"Symétrie"
            height:60
            width:140
            rect.border.width:2.0
            hoveringColor: buttonColor
            invisible_x: width * 4
            opacity:0.0
            Component.onCompleted: {
                show(sketcher.enabled)
            }
            ButtonEnum {
                id:symmetryEnum
                x:0
                y:-height
                width:parent.width
                Row {
                    width:parent.width

                    BaseButton {
                        width:parent.width / parent.children.length
                        caption: "Sans"
                        checkable: true
                        pressed:true
                        onClicked: sketcher.symmetry_mode = SketchingModeler.E_NO_SYMMETRY
                    }
                    BaseButton {
                        width:parent.width / parent.children.length
                        icon:"../../gui/images/SYMMETRY_VERTICAL.svg"
                        checkable: true
                        onClicked: sketcher.symmetry_mode = SketchingModeler.E_VERTICAL_SYMMETRY
                    }
                    BaseButton {
                        width:parent.width / parent.children.length
                        icon:"../../gui/images/SYMMETRY_HORIZONTAL.svg"
                        checkable: true
                        onClicked: sketcher.symmetry_mode = SketchingModeler.E_HORIZONTAL_SYMMETRY
                    }
                    BaseButton {
                        width:parent.width / parent.children.length
                        icon:"../../gui/images/SYMMETRY_BOTH.svg"
                        checkable: true
                        onClicked: sketcher.symmetry_mode = SketchingModeler.E_BOTH_SYMMETRY
                    }
                }
            }
            Rectangle {
                color:"black"
                width:3
                height:symmetryEnum.height
                y:-height
                anchors.horizontalCenter: parent.left
            }
        }
        TileButton {
            id:brush
            caption:"Taille Pinceau"
            height:60
            width:140
            hoveringColor: buttonColor
            rect.border.width:2.0
            invisible_x: width * 4
            opacity:0.0
            Component.onCompleted: {
                show(sketcher.enabled)
            }
            Row {
                id:brushRow
                width:parent.width
                y: -height
                BaseButton {
                    width:parent.width / parent.children.length
//                    caption: "Smaller"
                    icon:"../../gui/images/ICO_MINUS.svg"
                    onClicked: sketcher.brush_size = sketcher.brush_size / 1.5
                }
                BaseButton {
                    width:parent.width / parent.children.length
                    caption: "Normal"
                    onClicked: sketcher.brush_size = 0.05
                }
                BaseButton {
                    width:parent.width / parent.children.length
                    icon:"../../gui/images/ICO_PLUS.svg"
                    onClicked: sketcher.brush_size = sketcher.brush_size * 1.5
                }
            }
            Rectangle {
                color:"black"
                width:3
                height:brushRow.height
                y:-height
                anchors.horizontalCenter: parent.left
            }

        }

        Column {
            TileButton {
                id:draw
                caption:"Dessiner !"
                height:60
                width:140
                buttonColor:"#8fcdcf"
//                rect.border.width:2.0
                visible:!sketcher.enabled
                onClicked: {
                    sketcher.enabled = true
                }
                onVisibleChanged: {
                    if (!visible) {
                        symmetry.show(true)
                        depth.show(true)
                        mode.show(true)
                        brush.show(true)
                        skeleton.show(true)
                    } else {
                        symmetry.show(false)
                        depth.show(false)
                        mode.show(false)
                        brush.show(false)
                        skeleton.show(false)
                    }
                }
            }
            TileButton {
                id:cancel
                caption:"Annuler"
                height:60
                width:140
                buttonColor:"#8fcdcf"
                rect.border.width:2.0
                visible:sketcher.enabled
                onClicked: {
                    sketcher.enabled = false
                }
                Row {
                    x: -parent.width
                    y: -150.0
                    width:100.0
//                    spacing:-5
                    TileButton {
                        x:-100
                        width : 100.0
                        height:50.0
                        caption: "Valider"
                        icon:"../../gui/images/ICO_VALIDATE.svg"
                        onClicked: sketcher.validate()
                    }
                    TileButton {
                        x:-50
                        width : 100.0
                        height:50.0
                        caption: "Tout effacer"
                        onClicked: sketcher.clear()
                    }
                }
                Row {
                    x: parent.width / 2.0
                    y: -150.0
                    width:parent.width * 2.0
                    spacing : 5
                    ButtonEnum {
                        Column {
                            spacing:-5
                            TileButton {
                                id:paintButton
                                width : 100.0
                                height:50.0
//                                x:-8
                                caption:"paint"
                                pressed:true
                                checkable: true
                                onClicked: {
                                    sketcher.sketch_mode = SketchingModeler.E_PAINT
                                }
                            }
                            TileButton {
                                x:-30
                                width : 100.0
                                height:50.0
                                icon:"../../gui/images/ICO_ERASE.svg"
                                buttonColor:"#8fcdcf"
                                checkable: true
                                onClicked: {
                                    sketcher.sketch_mode = SketchingModeler.E_ERASE
                                }
                            }
                            TileButton {
                                x:-8
                                width : 100.0
                                height:50.0
                                caption:"fill"
                                checkable: true
                                onClicked: {
                                    sketcher.sketch_mode = SketchingModeler.E_FILL
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
