import QtQuick 2.3
import QtGraphicalEffects 1.0

TileButton {
    id: buttonContainer
    property bool horizontalAlignement : false
    property bool automaticChildPositioning : false
    property bool automaticClose : true

//    buttonColor :"transparent"
    border.width:1.5
    border.color:"#73acc9"
//    icon:"qrc://gui/images/Onglet_menu.png"

//    transform: Matrix4x4 {
//        property real shear : -1.0
//        matrix: Qt.matrix4x4(1, shear, 0, 0,
//                             0,     1, 0, 0,
//                             0,     0, 1, 0,
//                             0,     0, 0, 1)
//    }

    function show_tabs(show) {
        for (var i = 0; i < children.length; i++) {
            if (children[i].objectName !== "BaseButton") {
                continue;
            }
            children[i].show(show)
        }
        pressed = show
    }

    onClicked: {
//        console.log("button container clicked :"  + pressed)
        if (automaticClose) {
            parent.closeAllTabs(this)
        }

        show_tabs(pressed)
    }

    Component.onCompleted: {
        var cpt = 0
        var prevPos = 0
        show_tabs(pressed)

        if (automaticChildPositioning) {
            for (var i = 0; i < children.length; i++) {
                if (children[i].objectName !== "BaseButton") {
                    continue;
                }

                if (i%2) {
                    children[i].z = 1
                } else {
                    children[i].z = 2
                }
                if (horizontalAlignement) {
                    prevPos += children[i].width + (Math.random() * 40 - 20)
                    children[i].set_position(prevPos, (Math.random() * 40 - 20))
                } else {
                    prevPos += children[i].height + (Math.random() * 40 - 20);
                    children[i].set_position(width + (Math.random() * 40 - 20), prevPos - (height * (children.length / 2.0 - 1)))
                }
            }

            cpt = cpt + 1
        }
    }
}
