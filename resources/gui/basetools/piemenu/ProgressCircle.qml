// Progress circle implementation found on http://www.bytebau.com/progress-circle-with-qml-and-javascript/
// ByteBau (Jörn Buchholz) @bytebau.com

import QtQuick 2.0
import QtQml 2.2

Item {
    id: root

    width: size
    height: size

    property int size: 200                  // The size of the circle in pixel
    property real arcBegin: 0               // start arc angle in degree
    property real arcEnd: 270               // end arc angle in degree
    property real arcOffset: 0              // rotation
    property bool isPie: false              // paint a pie instead of an arc
    property bool showBackground: false     // a full circle as a background of the arc
    property bool showExternalBorder:false  // the border of the arc
    property bool showSideBorder: true      // the border of the arc

    property real fillPercentage:100        // fill percentage
    property real lineWidth: 20             // width of the line
    property real externalBorderWidth: 4    // width of the border
    property real sideBorderWidth: 2        // width of the border
    property string colorCircle: "#CC3333"
    property string unfilledColorCircle: "#cc3333f0"
    property string colorBackground: "#779933"
    property string colorBorder:"#303030"

    property alias beginAnimation: animationArcBegin.enabled
    property alias endAnimation: animationArcEnd.enabled

    property int animationDuration: 200

    onFillPercentageChanged: canvas.requestPaint()
    onShowExternalBorderChanged: canvas.requestPaint()
    onArcBeginChanged: {/* console.log("0 arc begin " + arcBegin + "arc end " + arcEnd); */canvas.requestPaint();}
    onArcEndChanged: {/*console.log("1 arc begin " + arcBegin + "arc end " + arcEnd);*/ canvas.requestPaint();}

    Behavior on arcBegin {
       id: animationArcBegin
       enabled: true
       NumberAnimation {
           duration: root.animationDuration
           easing.type: Easing.InOutCubic
       }
    }

    Behavior on arcEnd {
       id: animationArcEnd
       enabled: true
       NumberAnimation {
           duration: root.animationDuration
           easing.type: Easing.InOutCubic
       }
    }

    Canvas {
        id: canvas
        anchors.fill: parent
        rotation: -90 + parent.arcOffset

        onPaint: {
//            console.log("drawing canvas : " +root.x +":"+  root.y)
            var ctx = getContext("2d")
            var x = width / 2
            var y = height / 2
            var start = Math.PI * (parent.arcBegin / 180)
            var end = Math.PI * (parent.arcEnd / 180)

            var mid = start + (end - start) * fillPercentage / 100.0

            ctx.reset()

            if (root.isPie) {
                if (root.showBackground) {
                    ctx.beginPath()
                    ctx.fillStyle = root.colorBackground
                    ctx.moveTo(x, y)
                    ctx.arc(x, y, width / 2, 0, Math.PI * 2, false)
                    ctx.lineTo(x, y)
                    ctx.fill()
                }
                if (root.showExternalBorder) {
                    ctx.beginPath()
                    ctx.fillStyle = root.colorBorder
                    ctx.moveTo(x, y)
                    ctx.arc(x, y, width / 2, start, end, false)
                    ctx.lineTo(x, y)
                    ctx.fill()
                }

                if (root.showSideBorder) {
                    if (parent.arcBegin % 360 !== parent.arcEnd % 360) {
                        ctx.beginPath()
                        ctx.strokeStyle = root.colorBorder
                        ctx.lineWidth = root.sideBorderWidth
                        ctx.moveTo(x + (width / 2.0) * Math.cos(start), x + (width / 2.0) * Math.sin(start))
                        ctx.lineTo(x + (parent.lineWidth + width / 2.0) * Math.cos(start), x + (parent.lineWidth + width / 2.0) * Math.sin(start))
                        ctx.stroke()
                    }
                }

                if (mid - start > 0) {
                    ctx.beginPath()
                    ctx.fillStyle = root.colorCircle
                    ctx.moveTo(x, y)
                    ctx.arc(x, y, (width - (showExternalBorder ? root.externalBorderWidth : 0)) / 2, start, mid, false)
                    ctx.lineTo(x, y)
                    ctx.fill()
                }

                if (end - mid > 0) {
                    ctx.globalAlpha = 0.3
                    ctx.beginPath()
                    ctx.fillStyle = root.unfilledColorCircle
                    ctx.moveTo(x, y)
                    ctx.arc(x, y, (width - (showExternalBorder ? root.externalBorderWidth : 0)) / 2, mid, end, false)
                    ctx.lineTo(x, y)
                    ctx.fill()
                    ctx.globalAlpha = 1.0
                }

            } else {
                if (root.showBackground) {
                    ctx.beginPath();
                    ctx.arc(x, y, (width / 2) - parent.lineWidth / 2, 0, Math.PI * 2, false)
                    ctx.lineWidth = root.lineWidth
                    ctx.strokeStyle = root.colorBackground
                    ctx.stroke()
                }

                if (mid - start > 0) {
                    ctx.beginPath();
                    ctx.arc(x, y, (width / 2) - parent.lineWidth / 2, start, mid, false)
                    ctx.lineWidth = root.lineWidth
                    ctx.strokeStyle = root.colorCircle
                    ctx.stroke()
                }

                if (end - mid > 0) {
                    ctx.globalAlpha = 0.3
                    ctx.beginPath();
                    ctx.arc(x, y, (width / 2) - parent.lineWidth / 2, mid, end, false)
                    ctx.lineWidth = root.lineWidth
                    ctx.strokeStyle = root.colorCircle
                    ctx.stroke()

                    ctx.globalAlpha = 1.0
                }

                if (root.showSideBorder) {
                    if (parent.arcBegin % 360 !== parent.arcEnd % 360) {
                        ctx.beginPath()
                        ctx.strokeStyle = root.colorBorder
                        ctx.lineWidth = root.sideBorderWidth
                        ctx.moveTo(x + (width / 2.0) * Math.cos(start), x + (width / 2.0) * Math.sin(start))
                        ctx.lineTo(x + (-parent.lineWidth + width / 2.0) * Math.cos(start), x + (-parent.lineWidth + width / 2.0) * Math.sin(start))
                        ctx.stroke()
                    }
                }

                if (root.showExternalBorder) {
                    ctx.beginPath();
                    ctx.arc(x, y, (width / 2) - parent.externalBorderWidth / 2, start, end, false)
                    ctx.lineWidth = root.externalBorderWidth
                    ctx.strokeStyle = root.colorBorder
                    ctx.stroke()
                }
            }
        }
    }
}
