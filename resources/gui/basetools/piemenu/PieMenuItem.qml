import QtQuick 2.0               //import the main Qt QML module

Item {
    property bool isSlider : false;
    property real minValue:0
    property real maxValue:255.0
    property real value : 100.0
    property string text:""
    property color color:"#73acc9"
    property int level: parent.level+1
    property var callback : function placeholder() {console.log("clicked on " + text);}

    property list<Item> items;

    onValueChanged: console.log("value changed " + value)

}
