import QtQuick 2.0               //import the main Qt QML module

import Expressive 1.0

Item
{
    id:root

    property color color:"#8fcdcf"
    property color background_color:"#73acc9"
    property real cancel_radius:width / 4.0
    property real children_width:width / 8.0

    property real arcBegin:0
    property real arcEnd: 360
    property bool isPie:true

    property list<PieMenuItem> items;
    property real item_arc_length:(arcEnd - arcBegin) / items.length;
    property int level : 0

    property QtObject pieController: QtObject {
        property var visibleItems: []
        property point selectionPos : Qt.point(root.width / 2, root.height / 2);
        readonly property real selectionAngle: {
            var centerX = width / 2;
            var centerY = height / 2;
            var targetX = selectionPos.x
            var targetY = selectionPos.y;

            var xDistance = centerX - targetX;
            var yDistance = centerY - targetY;

            var angleToTarget = Math.atan2(xDistance, yDistance) * -1;
            angleToTarget;
        }

        property var hoveredItem : undefined
        property var selectedItem : undefined
//        property bool editingSelectedItem : false
        property point editionStartingPoint : Qt.point(0, 0)
        property real editionStartingValue : 0
    }

    width:200
    height:width
    visible:controller.expanded
    readonly property bool hovered:controller.expanded


    x:controller.mouse.x - width / 2.0
    y:controller.mouse.y - width / 2.0

    PieMenuController
    {
        id:controller
        engine:expressive_engine
    }

    ProgressCircle {
        colorCircle:root.color;
        colorBackground: root.background_color
        arcBegin:root.arcBegin
        arcEnd:root.arcEnd
        isPie:root.isPie
        size:root.cancel_radius
        x:root.width / 2.0 - width / 2.0
        y:x
    }

    MouseArea {
        anchors.fill:parent
        cursorShape:Qt.CrossCursor
        hoverEnabled: true
        onPositionChanged: {
            handleMouseMove(mouseX, mouseY);
        }
        onClicked: {
            handleClick()
        }
    }

    Component {
        id: itemDelegate
        ProgressCircle {
            id:itemCircle

            Rectangle{
                color:"#0000ff30"
                anchors.fill:parent
                border.color: "grey"
                border.width: 2
            }

            // Own properties
            property var model_data : model
//            property bool selected:hovered && mouseIsInRadius
            property bool hovered: childContainsMouse && mouseIsInRadius
            property real item_arc_length: (arcEnd - arcBegin) / items.length;

            // General properties
            width:root.cancel_radius + 2.0 * children_width * level
            height:width
            visible:parent.childContainsMouse

            x: level == 1 ? (root.width / 2.0 - width / 2.0) : -children_width
            y: x

            // ProgressCircle Properties
            size:/*parent.size + */children_width + root.cancel_radius
            lineWidth:root.children_width
            arcBegin: parent.arcBegin + parent.item_arc_length * index;
            arcEnd : parent.arcBegin + parent.item_arc_length * (index + 1);
            isPie:false
            colorCircle: model.color//Qt.rgba((index+1.0) * 1.0 / 5.0, 0.0, 0.0, 1.0);
            fillPercentage: model.isSlider ? 100.0 * (model.value - model.minValue) / (model.maxValue - model.minValue) : 100.0
            colorBorder:"steelblue"
            showExternalBorder : childContainsMouse

            function clicked () {
                callback()
            }

            onHoveredChanged: {
                if (hovered) {
                    pieController.hoveredItem = this
                } else if (pieController.hoveredItem === this && !pieController.selectedItem) {
                    pieController.hoveredItem = undefined
                }
            }

            Text {
                id:rect
                text:model.text + "::" + model.isSlider
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color:"steelblue"
                width: 2.0 * Math.PI * (parent.width / 2.0) * (arcEnd - arcBegin) / 360.0
                height:children_width / 2.0
                rotation: arcBegin + (arcEnd - arcBegin) / 2.0
                property var t : -90.0 + arcBegin + (arcEnd - arcBegin) / 2.0
                x:(itemCircle.width - width) / 2.0 + (itemCircle.width - children_width) * Math.cos(Math.PI * t / 180.0) / 2.0
                y:(itemCircle.width - height) / 2.0 + (itemCircle.width- children_width) * Math.sin(Math.PI * t / 180.0) / 2.0
            }

            property bool mouseIsInRadius: {

                var x1 = width / 2;
                var y1 = height / 2;
                var x2 = pieController.selectionPos.x - root.width / 2.0;// - (level * children_width);
                var y2 = pieController.selectionPos.y - root.height / 2.0;// - (level * children_width);
                var distanceFromCenter = x2 * x2 + y2 * y2//Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2);

                var lowerRadius = (root.cancel_radius / 2.0)+ (level - 1) * children_width
                var lowerRadiusSquared = Math.pow(lowerRadius, 2.0)
                var styleRadiusSquared = Math.pow(root.cancel_radius / 2.0 + level * children_width, 2);
                var isWithinOurRadius = distanceFromCenter >= lowerRadiusSquared
                    && distanceFromCenter < styleRadiusSquared;

                return isWithinOurRadius;
            }

            property bool childContainsMouse: {
                if (pieController.selectedItem) {
                    if (pieController.selectedItem === this) {
                        return true;
                    } else {
                        return false;
                    }
                }

                // Our mouse angle's origin is north naturally, but the section angles need to be
                // altered to have their origin north, so we need to remove the alteration here in order to compare properly.
                // For example, section 0 will start at -1.57, whereas we want it to start at 0.
                var sectionStart = (Math.PI * arcBegin / 180.0);// + Math.PI / 2;
                var sectionEnd = (Math.PI * arcEnd / 180.0);// + Math.PI / 2;


                var selAngle = pieController.selectionAngle;
                var isWithinOurAngle = false;

                // If the section crosses the -180 => 180 wrap-around point (from atan2),
                // temporarily rotate the section so it doesn't.
                if (sectionStart > Math.PI) {
                    var difference = sectionStart - Math.PI;
                    selAngle -= difference;
                    sectionStart -= difference;
                    sectionEnd -= difference;
                } else if (sectionStart < -Math.PI) {
                    difference = Math.abs(sectionStart - (-Math.PI));
                    selAngle += difference;
                    sectionStart += difference;
                    sectionEnd += difference;
                }

                if (sectionEnd > Math.PI) {
                    difference = sectionEnd - Math.PI;
                    selAngle -= difference;
                    sectionStart -= difference;
                    sectionEnd -= difference;
                } else if (sectionEnd < -Math.PI) {
                    difference = Math.abs(sectionEnd - (-Math.PI));
                    selAngle += difference;
                    sectionStart += difference;
                    sectionEnd += difference;
                }

                // If we moved the mouse past -180 or 180, we need to move it back within,
                // without changing its actual direction.
                if (selAngle > Math.PI) {
                    selAngle = selAngle - 2.0 * Math.PI;
                } else if (selAngle < -Math.PI) {
                    selAngle += 2.0 * Math.PI;
                }

                if (sectionStart > sectionEnd) {
                    isWithinOurAngle = selAngle >= sectionEnd && selAngle < sectionStart;
                } else {
                    isWithinOurAngle = selAngle >= sectionStart && selAngle < sectionEnd;
                }

                var x1 = width / 2;
                var y1 = height / 2;
                var x2 = pieController.selectionPos.x - root.width / 2.0;// - (level * children_width);
                var y2 = pieController.selectionPos.y - root.height / 2.0;// - (level * children_width);
                var distanceFromCenter = x2 * x2 + y2 * y2//Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2);

                var lowerRadius = (root.cancel_radius / 2.0)+ (level - 1) * children_width
                var lowerRadiusSquared = Math.pow(lowerRadius, 2.0)
                var styleRadiusSquared = Math.pow(root.cancel_radius / 2.0 + level * children_width, 2);
                var isWithinOurRadius = distanceFromCenter >= lowerRadiusSquared
//                    && distanceFromCenter < styleRadiusSquared;

                return isWithinOurAngle && isWithinOurRadius;
            }

            Repeater {
                model:items
                delegate: itemDelegate
            }

            states: [
                State {
                    when : visible;
                    PropertyChanges {
                        target:itemCircle
                        opacity:1.0
                    }

                },
                State {
                    when : !visible;
                    PropertyChanges {
                        target:itemCircle
                        opacity:0.0
                    }
                }
            ]

            transitions:Transition {
                NumberAnimation {
                    property: "opacity"
                    duration: 200
                }
            }
        }
    }

    Repeater {
        anchors.fill:parent
        model:items
        delegate: itemDelegate
    }

    function handleClick()
    {
        if (visible && pieController.hoveredItem) {
            if (pieController.hoveredItem.model_data.isSlider) {
                if (pieController.selectedItem) {
                    controller.expanded = false
                    pieController.selectedItem = undefined
                } else {
                    pieController.selectedItem = pieController.hoveredItem
                    pieController.editionStartingPoint = pieController.selectionPos
                    pieController.editionStartingValue = pieController.selectedItem.model_data.value
                }
            } else {
                pieController.hoveredItem.model_data.callback()//clicked()
                controller.expanded = false
            }
        } else {
            controller.expanded = true
        }
    }

    function handleMouseMove(mouseX, mouseY)
    {
        pieController.selectionPos  = Qt.point(mouseX, mouseY)
        if (pieController.selectedItem) {
            pieController.selectedItem.model_data.value = pieController.editionStartingValue - (pieController.selectionPos.y - pieController.editionStartingPoint.y)
        }
    }

    Connections {
        target:controller
        onClicked: {
            handleClick()
        }

    }

//    onVisibleChanged: {
//        if (!visible && selectedItem) {
//            selectedItem.clicked();
//        }
//    }
}
