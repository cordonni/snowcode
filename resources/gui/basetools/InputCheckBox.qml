//  Custom checkbox

import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.4

Item {
    id: root
    property string caption:"value"
    property bool value : true
    property bool constant:true

    signal accepted()

    Row {
        Text {
            text:caption
        }

        CheckBox {
            id:visibleCheckbox
            onClicked: {
                if (!constant) {
                    root.value = checked;
                    root.accepted();
                }
            }
            checked:root.value
            style: CheckBoxStyle {
                indicator: Rectangle {
                    implicitWidth: 16
                    implicitHeight: 16
                    radius: 3
                    border.color: control.activeFocus ? "darkblue" : "gray"
                    border.width: 1
                    Rectangle {
                        visible: control.checked
                        color: "#555"
                        border.color: "#333"
                        radius: 1
                        anchors.margins: 4
                        anchors.fill: parent
                    }
                }
            }
        }
    }
}


