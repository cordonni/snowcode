import QtQuick 2.3
import QtGraphicalEffects 1.0
import QtQuick 2.0
import QtGraphicalEffects 1.0

TileButton {
    id: buttonContainer
    property string deployDirection : "right"
//    property bool horizontalAlignement : true
    property bool automaticChildPositioning : true
    property bool automaticClose : true

    width:140
    img.anchors.right: buttonContainer.right
    fontSize: 10.0


    function show_tabs(show) {
        for (var i = 0; i < children.length; i++) {
            if (children[i].objectName !== "BaseButton") {
                continue;
            }
            children[i].show(show)
        }
        pressed = show
    }

    onClicked: {
        if (automaticClose) {
            parent.closeAllTabs(this)
        }

        show_tabs(pressed)
    }

    Component.onCompleted: {
        var cpt = 0
        var prevPos = 0
        if (deployDirection === "right") {
            prevPos = width
        }

        show_tabs(pressed)

        if (automaticChildPositioning) {
            for (var i = 0; i < children.length; i++) {
                if (children[i].objectName !== "BaseButton") {
                    continue;
                }
                if (deployDirection === "right") {
                    children[i].height = height
                    children[i].set_position(prevPos, (i%2 ? 1 : -1)* children[i].height / 4.0)
                    prevPos += (i % 2 ? 1.0: 0.8) * children[i].width
                } else if (deployDirection === "top"){
                    children[i].height = height
                    children[i].set_position(width, prevPos - (height * (children.length / 2.0 - 1)))
                    prevPos += children[i].height
                }
            }

            cpt = cpt + 1
        }
    }
}
