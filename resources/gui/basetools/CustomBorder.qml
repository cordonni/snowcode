import QtQuick 2.0

Rectangle
{

    property var target : parent
    property bool commonBorder : true

    property int lBorderwidth : 3
    property int rBorderwidth : 3
    property int tBorderwidth : 0
    property int bBorderwidth : 0

    property int commonBorderWidth : 1

    z : -1

    property string borderColor : "#a7d8ed"

    color: borderColor

    anchors
    {
        left: target.left
        right: target.right
        top: target.top
        bottom: target.bottom

        topMargin    : commonBorder ? -commonBorderWidth : -tBorderwidth
        bottomMargin : commonBorder ? -commonBorderWidth : -bBorderwidth
        leftMargin   : commonBorder ? -commonBorderWidth : -lBorderwidth
        rightMargin  : commonBorder ? -commonBorderWidth : -rBorderwidth
    }
}
