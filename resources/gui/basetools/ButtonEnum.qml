import QtQuick 2.0

Item {
    id:root
    height:30
    width:1
    
    function button_clicked(button)
    {
        var node = this
        if (children.length === 1) { // if first direct child is a layout
            node = children[0]
        }

        for (var i = 0; i < node.children.length; ++i) {
            if (node.children[i].objectName === "BaseButton") {
                if (node.children[i] !== button) {
                    node.children[i].pressed = false
                } else if (node.children[i].pressed === false) {
                    // it's an enum, so it's a nonsense if every buttons are unclicked.
                    node.children[i].pressed = true
                }
            }
        }
    }
    
    Component.onCompleted: {
        var node = this
        if (children.length === 1) { // if first direct child is a layout
            node = children[0]
        }

        for (var i = 0; i < node.children.length; ++i) {
            if (node.children[i].objectName === "BaseButton") {
                node.children[i].clicked.connect(root.button_clicked);
            }
        }
    }
}
