//  Edit box (with caption), editing a number value
import QtQuick 2.2
import "../colorpicker/content"

Row {
    id:root
    property alias  caption: captionBox.text
    property alias  value: inputBox.text
    property alias  min: numValidator.bottom
    property alias  max: numValidator.top
    property alias  decimals: numValidator.decimals
    property alias  inputBox: inputBox
    property bool constant : false

    signal accepted()

    width: 50
    height: 15
    Text {
        id: captionBox
        height: parent.height
        color: "#000000"
        font.pixelSize: 11; //font.bold: true
        horizontalAlignment: Text.AlignRight;
        verticalAlignment: Text.AlignBottom
        anchors.bottomMargin: 3
    }
    PanelBorder {
        height: parent.height
        anchors.leftMargin: 4;
        color:"white"
        TextInput {
            id: inputBox
            anchors.leftMargin: 4;
            anchors.topMargin: 1;
            anchors.fill: parent
            color: "#000000"; selectionColor: "#FF7777AA"
            font.pixelSize: 11            
            maximumLength: 10
            focus: true
            selectByMouse: true
            readOnly:root.constant
            validator: DoubleValidator {
                id: numValidator
                bottom: -100000000; top: 1000000000; decimals: 2
                notation: DoubleValidator.StandardNotation
            }
            onAccepted: { valueChanged(value); root.accepted(); }
        }
    }
}



