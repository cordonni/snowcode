import QtQuick 2.0

Rectangle {
    property string title:""
    color: "#a7d8ed"
    border.color: "#73acc9"
    border.width:3.0
    radius: 1
    width:parent.width
    anchors.horizontalCenter: parent.horizontalCenter

    property alias title_rect : _title_rect

    Rectangle {
        id: _title_rect
        width:parent.width
        height:15
        color: "#a7d8ed"
        border.color: "#73acc9"
        Text {
            text:title
            anchors.fill:parent
            horizontalAlignment: Text.AlignHCenter
        }
        border.width: 15
    }
}
