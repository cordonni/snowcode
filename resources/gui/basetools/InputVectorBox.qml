//  Edit box (with caption), editing a vector value (3 InputNumberBox)

import QtQuick 2.0

Item {
    id: root
    property string caption:"value"
    property vector3d value:Qt.vector3d(0.0, 0.0, 0.0)
//    property var target
    property int decimals:2
    property string caption_x:"x :"
    property string caption_y:"y :"
    property string caption_z:"z :"
    property bool constant:false

    signal accepted()
    
    Row {
        Text {
            text:caption
        }

        InputNumberBox {
            caption:root.caption_x
            value:root.value.x.toFixed(root.decimals)
            constant:root.constant
            onAccepted: {
                root.value.x = value
                root.accepted()
            }
        }
        InputNumberBox {
            caption:root.caption_y
            value:root.value.y.toFixed(root.decimals)
            constant:root.constant
            onAccepted: {
                root.value.y = value
                root.accepted()
            }
        }
        InputNumberBox {
            caption:root.caption_z
            value:root.value.z.toFixed(root.decimals)
            constant:root.constant
            onAccepted: {
                root.value.z = value
                root.accepted()
            }
        }
    }
}


