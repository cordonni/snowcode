import QtQuick.Controls 1.0
import QtQuick.Window 2.0
import QtQuick 2.2
import Expressive 1.0

Item {
    id: keyFramesContainer

    property var rectWidth : 2

    Component {
        id: rectComponnent
        Rectangle {
            color: "green"
        }
    }


    Component.onCompleted:
    {
        var framelist = simulationManager.keyframes()
        var numFrames = simulationManager.maxFrame()-1
        for (var i = 0; i<framelist.length; i++)
        {
            var x = keyFramesContainer.width * framelist[i] / numFrames - rectWidth/2
            rectComponnent.createObject(keyFramesContainer, {
                                            "x" : x,
                                            "y" : keyFramesContainer.y,
                                            "height" : keyFramesContainer.height,
                                            "width" : keyFramesContainer.rectWidth
                                        })
        }
    }
}
