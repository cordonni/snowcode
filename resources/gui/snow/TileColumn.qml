import QtQuick 2.0
import QtQuick.Controls 1.2

import "../basetools"
import "../basetools/tiles"

Rectangle {
    id:rect
    width:300
    height:0.0//parent.height / 2.0
    color:"transparent"

    function addTile(tile) {
        tile.parent = col
        tile.visibleChanged.connect(recomputeVisibility)
    }

    function isChildVisible() {
        for (var i=0; i < col.children.length; ++i) {
            if (col.children[i].visible) {
                return true;
            }
        }
        return false;
    }

    function recomputeVisibility() {
        height = isChildVisible() ? parent.height / 2.0 : 0.0
    }

    Component.onCompleted: {
        if (parent) {
            parent.heightChanged.connect(recomputeVisibility)
        }
    }

    MouseArea {
        anchors.fill:parent
        hoverEnabled:true//isChildVisible()//true
        onWheel: {
            wheel.accepted = true
        }
    }

    ScrollView {
        id:root
        anchors.fill:parent
        horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
        verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff

        Column {
            id:col
            width:root.width
            anchors.horizontalCenter: parent.horizontalCenter
            spacing : 2

            add:Transition {
                NumberAnimation {
                    properties: "y"
                    easing.type: Easing.InOutQuad
                    duration:500
                }
            }
            move: Transition {
                NumberAnimation {
                    properties: "y"
                    easing.type: Easing.InOutQuad
                    duration:500
                }
            }
        }
    }
}
