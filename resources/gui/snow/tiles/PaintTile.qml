import QtQuick 2.7
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.1

import "../../basetools"
import "layers"
import Expressive 1.0

Tile {
    id : layers_tile
    width: parent.width
    height: title_rect.height + radiiLabel.height + amountLabel.height + 6
    title: "Paint"

    Text
    {
        id: radiiLabel
        text: "Radii (m)"
        height:20
        anchors.top: title_rect.bottom
        anchors.topMargin: 2
        anchors.left: parent.left
        anchors.leftMargin: 5
    }

    Rectangle
    {
        id: r0Bg
        height: 20
        width: 40
        radius: 7
        anchors.top : title_rect.bottom
        anchors.topMargin: 2
        anchors.left: radiiLabel.right
        anchors.leftMargin: 5

        color: "transparent"
        border.color: "black"

        TextInput {
            id: radius0
            text: "30"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 2
            readOnly: false
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            selectByMouse: true


            onEditingFinished: {simulationManager.user_set_edit_radius_0(text)}

            validator: DoubleValidator {bottom:0}
        }

    }
    Rectangle
    {
        id: r1Bg
        height: 20
        width: 40
        radius: 7
        anchors.top : title_rect.bottom
        anchors.topMargin: 2
        anchors.left: r0Bg.right
        anchors.leftMargin: 5

        color: "transparent"
        border.color: "black"

        TextInput {
            id: radius1
            text: "50"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 2
            readOnly: false
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            selectByMouse: true

            onEditingFinished: {simulationManager.user_set_edit_radius_1(text)}

            validator: DoubleValidator {bottom:0}
        }

    }

    Text
    {
        id: amountLabel
        text: "Amount (m)"
        height:20
        anchors.top: radiiLabel.bottom
        anchors.topMargin: 2
        anchors.left: parent.left
        anchors.leftMargin: 5
    }

    Rectangle
    {
        id: amountBg
        height: 20
        width: 40
        radius: 7
        anchors.top : radiiLabel.bottom
        anchors.topMargin: 2
        anchors.left: amountLabel.right
        anchors.leftMargin: 5

        color: "transparent"
        border.color: "black"

        TextInput {
            id: amountText
            text: "1"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 2
            readOnly: false
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            selectByMouse: true

            onEditingFinished: {simulationManager.user_set_edit_amount(text)}

            validator: DoubleValidator {}
        }

    }


}
