import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.1

import "../../basetools"
import "params"
import Expressive 1.0

Tile {
    id : parameter_tile
    width:parent.width
    height: title_rect.height + globalParams.height + eventView.height
    title:"Parameters"

    Collapsable {
        id: globalParams
        width:parent.width
        anchors.top: title_rect.bottom
        Text {
            anchors.top:parent.top
            anchors.left: parent.leftanchor
            anchors.leftMargin: 2
            text: "Globals:"
            height: 20
        }
        delegate: Parameter{ width:globalParams.width }
        viewmodel: simulationManager.parameters.globals
    }



    ListView {
        id: eventView
        property var childHeights : []
        interactive:false
        model: simulationManager.parameters.events
        width:parent.width
        anchors.top:globalParams.bottom
        function updateHeight(i, nh) {
            if(childHeights.length < count)
            {
                for (var j = 0; j < count; ++j)
                    childHeights[j] = 20
                height = count*20
            }
            else
            {
                height = height - childHeights[i] + nh
                childHeights[i] = nh
            }
        }
        delegate: Event {
            viewmodel: model.parameters
            width:eventView.width
            onHeightChanged: {eventView.updateHeight(index, height)}
        }
    }
}
