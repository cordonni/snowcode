import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.1

import "../../basetools"
import "layers"
import Expressive 1.0

Tile {
    id : layers_tile
    width: parent.width
    height: title_rect.height + layersView.height
    title: "Layers"



    ListView {
        id: layersView
        interactive:false
        model: simulationManager.layers.layers
        width:parent.width
        height: 20*count
        anchors.top:title_rect.bottom

        delegate: Layer {
            width:layersView.width
        }

    }
}
