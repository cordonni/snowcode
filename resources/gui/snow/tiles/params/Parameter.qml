import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.4

import "../../"
import Expressive 1.0

Rectangle {
    id: delegateItem

    height:20

    color: "transparent"

    Rectangle {
        id: leftRect
        anchors.left: delegateItem.left
        anchors.leftMargin: 13
        anchors.verticalCenter: delegateItem.verticalCenter
        height: parent.height
        color : "#505050"
        width: 2
    }

    CheckBox {
        id: lockCheckbox
        anchors.left: leftRect.right
        anchors.leftMargin: 5
        anchors.verticalCenter: delegateItem.verticalCenter
        onClicked: model.locked = checked

        Timer {
            interval: 100; running:true; repeat: true
            onTriggered:  parent.checked = model.locked
        }

        style: CheckBoxStyle {
            indicator:
                Image {
                y: 1
                width:18
                height:18
                source:control.checked ? "../../images/ICO_LOCKED.svg" : "../../images/ICO_UNLOCKED.svg"
            }
        }
    }

    Text {
        id: textArea
        anchors.left: lockCheckbox.right
        anchors.verticalCenter: delegateItem.verticalCenter
        anchors.leftMargin: 5
        text: name
        font.pixelSize:11
    }

    TextInput {
        id: valueArea
        text: value.toPrecision(5)
        anchors.left: textArea.right
        anchors.leftMargin: 5
        anchors.verticalCenter: delegateItem.verticalCenter

        readOnly: false
        inputMethodHints: Qt.ImhFormattedNumbersOnly
        selectByMouse: true

        onEditingFinished: {value = text}

        validator: DoubleValidator {}
    }


}
