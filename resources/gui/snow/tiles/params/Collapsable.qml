import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.4

import "../../"
import Expressive 1.0

Rectangle {
    id: mainRectangle

    property bool collapsed:true
    property alias viewmodel: view.model
    property alias delegate: view.delegate
    property alias leftanchor: rect.right

    height: 20 + (mainRectangle.collapsed ? 0 : 20*view.count)

    color:"transparent"

    Rectangle {
        id:rect
        width:15
        height:15
        color:"transparent"
        anchors.top:parent.top
        anchors.left: parent.left
        anchors.leftMargin: 6

        Image {
            anchors.fill:parent
            anchors.margins: 3
            source: mainRectangle.collapsed ? "../../images/ICO_ARROW_RIGHT.svg" : "../../images/ICO_ARROW_BOTTOM.svg"
        }
        MouseArea {
            anchors.fill:parent
            onClicked:{
            mainRectangle.collapsed = !mainRectangle.collapsed
            }
        }
    }


    ListView {
        id:view
        anchors.top : rect.bottom
        anchors.topMargin: 3
        anchors.bottom: parent.bottom
        interactive:false
        visible:!mainRectangle.collapsed
        width: parent.width
        height:count * 20
    }
}
