import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.4

import "../../"
import Expressive 1.0

Collapsable {
    id: mainRect

    delegate: Parameter{ width: mainRect.width }

    Timer {
        interval: 100; running:true; repeat: true
        onTriggered:
        {
            lockedCheckBox.checked = model.locked
            //timestepInput.text = model.timestep.toPrecision(5)
        }
    }

    CheckBox {
        id: lockedCheckBox
        onClicked: model.locked = checked
        anchors.top:parent.top
        anchors.left: parent.leftanchor
        anchors.leftMargin: 5



        style: CheckBoxStyle {
            indicator:
                Image {
                width:20
                height:20
                source:control.checked ? "../../images/ICO_LOCKED.svg" : "../../images/ICO_UNLOCKED.svg"
            }
        }
    }
    Text {
        id: nameText
        text: name
        anchors.top:parent.top
        anchors.left: lockedCheckBox.right
        anchors.leftMargin: 2
    }

    TextInput {
        id: timestepInput
        text: timestep.toPrecision(5)
        anchors.left: nameText.right
        anchors.leftMargin: 5
        anchors.verticalCenter: nameText.verticalCenter

        readOnly: false
        inputMethodHints: Qt.ImhFormattedNumbersOnly
        selectByMouse: true

        onEditingFinished: {timestep = text}

        validator: DoubleValidator {}
    }

}
