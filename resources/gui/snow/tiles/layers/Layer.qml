import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.4

import "../../"
import Expressive 1.0

Rectangle {
    id: delegateItem

    height:20

    color: "transparent"

    CheckBox {
        id: viewCheckBox
        anchors.left: delegateItem.left
        anchors.leftMargin: 5
        anchors.verticalCenter: delegateItem.verticalCenter
        onClicked: model.shown = checked

        Timer {
            interval: 100; running:true; repeat: true
            onTriggered:  parent.checked = model.shown
        }

        style: CheckBoxStyle {
            indicator:
                Image {
                y: 1
                sourceSize.width:  18
                sourceSize.height: 10
                source:control.checked ? "../../images/ICO_VISIBLE.svg" : "../../images/ICO_INVISIBLE.svg"
            }
        }
    }

    CheckBox {
        id: editCheckBox
        anchors.left: viewCheckBox.right
        anchors.leftMargin: 5
        anchors.verticalCenter: delegateItem.verticalCenter
        onClicked: model.edited = checked

        Timer {
            interval: 100; running:true; repeat: true
            onTriggered:  parent.checked = model.edited
        }

        style: CheckBoxStyle {
            indicator:
                Rectangle {
                    width:18
                    height:18
                    radius: 6
                    color : "transparent"
                    border.color: control.checked ? "black" : "transparent"
                    Image {
                        x : parent.width /2 -width/2
                        y: parent.height /2 - height /2
                        width: 10
                        height: 10
                        source:"../../images/ICO_PAINT.svg"
                    }
            }
        }
    }

    Text {
        id: textArea
        anchors.left: editCheckBox.right
        anchors.verticalCenter: delegateItem.verticalCenter
        anchors.leftMargin: 5
        text: name
        font.pixelSize:11
    }



}
