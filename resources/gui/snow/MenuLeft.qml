import QtQuick 2.0               //import the main Qt QML module
import QtGraphicalEffects 1.0

import "../basetools"
import "../basetools/tiles"

Rectangle {
    id: rectangle1
    color:"#00000000"
    border.color: "#00000000"
    anchors.fill:parent

    property var tile_container
    property bool menu_opened : false

    function closeAllTabs(source) {
        for (var i = 0; i < children.length; ++i) {
            if (children[i] !== source && children[i].automaticClose) {
                children[i].show_tabs(false)
            }
        }
    }

    Image {
        id: banner
        x: 1
        y: 0
        width:70
        height:parent.height * 0.8
        scale: 1
        opacity: 0.7
        source: "images/MENU_BACKGROUND_LEFT.svg"
    }


    MenuButton {
        id: globalMenuButton
//        x: 47
        y: 150
//        height:50
        width:140
        automaticClose : false
        pressed: menu_opened
        caption:"Menu"
        fontSize: 12

        TileButton {
            id: loadButton
            buttonColor:"#73acc9"
            caption:"Load"
            onClicked: simulationManager.user_load()
        }
        TileButton {
            id: saveButton
            buttonColor:"#8fcdcf"
            caption:"Save"
            onClicked: simulationManager.user_save()
        }
        TileButton {
            id: importButton
            buttonColor:"#73acc9"
            caption:"Import"
            onClicked: simulationManager.user_importTER()
        }
        TileButton {
            id: importLayersButton
            buttonColor:"#73acc9"
            caption:"Layers"
            onClicked: simulationManager.user_importLayers()
        }
        TileButton {
            id: exportButton
            buttonColor:"#8fcdcf"
            caption:"Export"
            onClicked: simulationManager.user_exportTER()
        }
        TileButton {
            id: exportAnimButton
            buttonColor:"#8fcdcf"
            caption:"Export anim"
            onClicked: simulationManager.user_exportAnim()
        }
        TileButton {
            id: exitButton
//            x: 330
//            y: -24
//            z:1
            buttonColor:"#73acc9"
            caption:"Exit"
            onClicked: Qt.quit()
        }
    }
}
