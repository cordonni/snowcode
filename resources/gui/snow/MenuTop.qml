import QtQuick 2.0               //import the main Qt QML module
import QtGraphicalEffects 1.0

import "../basetools/"

Rectangle {
    id: rectangle1
    color:"#00000000"
    border.color: "#00000000"
    width:parent.width
    height:banner.height

    Image {
        id: banner
        x: 1
        y: -2
        width:500
        scale: 1
        opacity: 0.7
        source: "images/MENU_BACKGROUND_TOP.png"
    }

    Rectangle {
        id: fpsArea
        x: 80
        y: 10

        height: 20
        width: 70

        radius: 7

        border.color: "#bdbebf"

        Text {
            id: fpstext
            Connections {target: simulationManager; onUpdateFPS : {fpstext.text = "FPS: " + simulationManager.fps()}}
            x: 5
            text: "FPS: "
        }
    }

    Rectangle {
        id: simuTimeArea
        anchors.left:fpsArea.right
        anchors.leftMargin: 30
        y: 10

        height: 20
        width: 185

        radius: 7

        border.color: "#bdbebf"

        Text {
            id: simuTimetext
            Connections {target: simulationManager; onUpdateInterfaceComputeFrame : {

                        simuTimetext.enabled = true
                        simuTimeArea.enabled = true
                        simuTimetext.text = "Simulation time: " + simulationManager.simu_time() + " ms"

                }}
            x: 5
            text: "Simulation time:"
        }
    }

    Rectangle {
        id: terrainResolArea
        anchors.left:fpsArea.left
        anchors.top: fpsArea.bottom
        anchors.topMargin: 5

        height: 20
        width: 185

        radius: 7

        border.color: "#bdbebf"

        Text {
            id: terrainResoltext
            Connections {target: simulationManager; onResolutionChanged : {
                        terrainResoltext.text = "Terrain resolution: " + simulationManager.terrain_width() + "x"
                                                                       + simulationManager.terrain_height()

                }}
            x: 5
            text: "Terrain resolution: 9999x9999"
        }
    }

    Rectangle {
        id: terrainRangeArea
        anchors.left:terrainResolArea.right
        anchors.top: terrainResolArea.top
        anchors.leftMargin: 30

        height: 20
        width: 160

        radius: 7

        border.color: "#bdbebf"

        Text {
            id: terrainRangetext
            Connections {target: simulationManager; onTerranRangeChanged : {

                        terrainRangetext.text = "Range: " + simulationManager.terrain_min_alt().toFixed(1) + " : " +
                                                            simulationManager.terrain_max_alt().toFixed(1)

                }}
            x: 5
            text: "Range: 99999.9:99999.9"
        }
    }


    /*Image {
        id: imagexpressiveLogo
        anchors.verticalCenter: banner.verticalCenter
        anchors.verticalCenterOffset: -5
        anchors.right: expressiveLogo.left
//        x:971
//        y:4
        width: 92
        height: 92
        fillMode: Image.PreserveAspectFit
        source: "images/ICO_EXPRESSIVE.svg"
    }

    Text{
        id:expressiveLogo
        anchors.verticalCenter: banner.verticalCenter
        anchors.verticalCenterOffset: -5
        anchors.right: banner.right
        anchors.rightMargin: 50
        text: "ExpressiveLab"
        style: Text.Sunken
        font.family: "Verdana"
        font.pointSize: 30
        color: "#5e5e5e"

    }*/
}
