import QtQuick 2.7               //import the main Qt QML module
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.0

import "../basetools/"

Rectangle {
    id: menuBottomRect
    color:"#00000000"
    border.color: "#00000000"
    width:parent.width
    height:timeline_bg.height

    function updateSliderCur()
    {
        timelineSlider.value = simulationManager.currentFrame() / (simulationManager.maxFrame()-1)
    }


    function updateSliderCompute()
    {
        timelineSlider.computed = (simulationManager.computedFrames()-1) / (simulationManager.maxFrame()-1)
    }

    Connections { target: simulationManager;
        onUpdateInterfaceCurFrame: {
            updateSliderCur()
            timelineValue.text = simulationManager.currentFrame()
        }
        onUpdateInterfaceMaxFrame: {
            updateSliderCur()
            updateSliderCompute()
            maxFrameValue.text = simulationManager.maxFrame()-1
            timelineSlider.updateKeyFrames()
        }
        onUpdateInterfaceRuning: {
            playPauseButtonImg.source = simulationManager.isRunning() ? playPauseButton.pause_image_source : playPauseButton.play_image_source
        }
        onUpdateInterfaceComputeFrame: {
            updateSliderCompute()
        }
        onUpdateReviewSpeed: {
            reviewSpeedValue.text = simulationManager.reviewSpeed()
        }
    }

    Rectangle {
        id: timeline_bg
        anchors.bottom: menuBottom.bottom
        anchors.bottomMargin: 5
        anchors.horizontalCenter: menuBottom.horizontalCenter
        width: parent.width-10
        height: 60
        opacity: 0.7
        radius: 10
        color: "#ff8fbfcf"
        y:-5
    }

    Slider {
        id: timelineSlider

        anchors.top: timeline_bg.top
        anchors.topMargin: 4
        anchors.horizontalCenter: menuBottom.horizontalCenter
        width: parent.width -50
        height: handle.height

        property var computed: 0



        MouseArea {
            property var active: false
            anchors.fill: parent
            onPressed: active = true
            onReleased: active = false


            onClicked: simulationManager.user_set_frame(Math.round(mouseX / width * (simulationManager.maxFrame()-1)))

            Timer {
                interval: 50; running: parent.active; repeat: true
                onTriggered: simulationManager.user_set_frame(Math.round(parent.mouseX / parent.width * (simulationManager.maxFrame()-1)))
            }

        }


        background: Rectangle {
            x: timelineSlider.leftPadding
            y: timelineSlider.topPadding + timelineSlider.availableHeight / 2 - height / 2
            width: timelineSlider.availableWidth
            height: 4
            radius: 2
            color: "#bdbebf"

            Rectangle {
                width: timelineSlider.computed * parent.width
                height: parent.height
                color: "#8c4747"
                radius: 2
            }
            Rectangle {
                width: timelineSlider.visualPosition * parent.width
                height: parent.height
                color: "#288bb9"
                radius: 2
            }

        }
        handle: Rectangle {
            x: timelineSlider.leftPadding + timelineSlider.visualPosition * (timelineSlider.availableWidth) - width/2
            y: timelineSlider.topPadding + timelineSlider.availableHeight / 2 - height / 2
            width: 6
            height: 20
            radius: 2
            color: timelineSlider.pressed ? "#cccccc" : "#f6f6f6"
            border.color: "#bdbebf"
        }

        function updateKeyFrames() {
            if(frameContainer)
                frameContainer.destroy()
            var height = 18
            var y = timelineSlider.topPadding + timelineSlider.availableHeight / 2 - height / 2
            frameContainer = frameContComponent.createObject(timelineSlider,
                                                             {
                                                                 "anchors.left":timelineSlider.background.left,
                                                                 "anchors.right":timelineSlider.background.right,
                                                                 "y": y,
                                                                 "height" : height
                                                             })
        }

        property var frameContComponent : Qt.createComponent("KeyFramesContainer.qml")
        property var frameContainer : updateKeyFrames()


        Connections {
            target: simulationManager
            onFramesChanged: timelineSlider.updateKeyFrames()
        }



    }

    Rectangle {
        id: timelineValueBg
        implicitWidth: 100
        implicitHeight: 20
        anchors.top: timelineSlider.bottom
        anchors.topMargin: 4
        anchors.horizontalCenter: menuBottom.horizontalCenter
        height: 20
        border.color: "#bdbebf"
        radius: 7

        TextInput {
            id: timelineValue
            z: 2
            text: "0"

            font.pixelSize: 11
            color: "#000000"
            selectionColor: "#288bb9"
            selectedTextColor: "#ffffff"
            horizontalAlignment: Qt.AlignHCenter
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 3
            height: parent.height

            readOnly: false
            validator: IntValidator{}
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            selectByMouse: true

            onEditingFinished: {simulationManager.user_set_frame(text)}
        }


        Rectangle {
            implicitWidth: 60
            implicitHeight: 20
            height: parent.height
            x: 20
            border.color: "#bdbebf"
        }

        Rectangle {
            anchors.left: parent.left
            height: parent.height
            implicitWidth: 20
            implicitHeight: 20

            color: "#00000000"

            Text {
                text: "-"
                font.pixelSize: timelineValue.font.pixelSize
                //color: parent.enabled ? "#000000" : "#909090"
                color: "#000000"
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {simulationManager.user_set_frame(simulationManager.currentFrame()-1)}
            }


        }
        Rectangle {
            anchors.right: parent.right
            height: parent.height
            implicitWidth: 20
            implicitHeight: 20
            color: "#00000000"

            Text {
                text: "+"
                font.pixelSize: timelineValue.font.pixelSize
                //color: parent.enabled ? "#000000" : "#909090"
                color: "#000000"
                anchors.fill: parent
                fontSizeMode: Text.Fit
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {simulationManager.user_set_frame(simulationManager.currentFrame()+1)}
            }
        }

    }

    Button {
        id: playPauseButton
        width: 20
        height: 20

        anchors.right: timelineValueBg.left
        anchors.verticalCenter: timelineValueBg.verticalCenter
        anchors.rightMargin: 5

        property var play_image_source: "images/TIMELINE_PLAY.svg"
        property var pause_image_source: "images/TIMELINE_PAUSE.svg"


        background: Rectangle {
            id: playPauseButtonBg
            width:  parent.width
            height: parent.height
            radius: 7
            border.color: "#bdbebf"
        }
        Image {
            id: playPauseButtonImg
            width: 20
            height: 20
            fillMode: Image.PreserveAspectFit
            source: "images/TIMELINE_PAUSE.svg"
        }

        MouseArea {
            anchors.fill: playPauseButton
            onPressed:  { playPauseButtonBg.color = "#a0a0a0" }
            onReleased: { playPauseButtonBg.color = "#ffffff" }
            onClicked: {simulationManager.user_togglePlay()}
        }
    }

    Button {
        id: firstFrameButton
        width: 20
        height: 20

        anchors.right: playPauseButton.left
        anchors.verticalCenter: timelineValueBg.verticalCenter
        anchors.rightMargin: 5

        background: Rectangle {
            id: firstFrameButtonBg
            width:  parent.width
            height: parent.height
            radius: 7
            border.color: "#bdbebf"
        }
        Image {
            width: 20
            height: 20
            fillMode: Image.PreserveAspectFit
            source: "images/TIMELINE_START.svg"
        }
        MouseArea {
            anchors.fill: firstFrameButton
            onPressed:  { firstFrameButtonBg.color = "#a0a0a0" }
            onReleased: { firstFrameButtonBg.color = "#ffffff" }
            onClicked: {simulationManager.user_set_frame(0)}
        }
    }

    Button {
        id: lastFrameButton
        width: 20
        height: 20

        anchors.left: timelineValueBg.right
        anchors.verticalCenter: timelineValueBg.verticalCenter
        anchors.leftMargin: 5

        background: Rectangle {
            id: lastFrameButtonBg
            width:  parent.width
            height: parent.height
            radius: 7
            border.color: "#bdbebf"
        }
        Image {
            width: 20
            height: 20
            fillMode: Image.PreserveAspectFit
            source: "images/TIMELINE_END.svg"
        }
        MouseArea {
            anchors.fill: lastFrameButton
            onPressed:  { lastFrameButtonBg.color = "#a0a0a0" }
            onReleased: { lastFrameButtonBg.color = "#ffffff" }
            onClicked: {simulationManager.user_set_frame(simulationManager.computedFrames())}
        }
    }

    Button {
        id: restartSimuButton
        width: 20
        height: 20

        anchors.left: timelineSlider.left
        anchors.leftMargin: (timelineSlider.x + firstFrameButton.x) / 2 - 3
        anchors.verticalCenter: timelineValueBg.verticalCenter

        background: Rectangle {
            id: restartSimuButtonBg
            width:  parent.width
            height: parent.height
            radius: 7
            border.color: "#bdbebf"
        }

        Image {
            width: 20
            height: 20
            fillMode: Image.PreserveAspectFit
            source: "images/TIMELINE_RESTART.svg"
        }
        MouseArea {
            anchors.fill: restartSimuButton
            onPressed:  { restartSimuButtonBg.color = "#a0a0a0" }
            onReleased: { restartSimuButtonBg.color = "#ffffff" }
            onClicked: {simulationManager.user_restart();}
        }
    }

    Button {
        id: recordSimuButton
        width: 20
        height: 20
        checked: true
        checkable: true

        anchors.right: restartSimuButton.left
        anchors.leftMargin: 20
        anchors.verticalCenter: timelineValueBg.verticalCenter

        background: Rectangle {
            id: recordSimuButtonBg
            width:  parent.width
            height: parent.height
            radius: 7
            border.color: "#bdbebf"
        }

        Image {
            id: recordSimuImage
            width: 20
            height: 20
            fillMode: Image.PreserveAspectFit
            source: "images/TIMELINE_RECORD_ON.svg"
        }
        onCheckedChanged: {
            if(checked)
                recordSimuImage.source = "images/TIMELINE_RECORD_ON.svg"
            else
                recordSimuImage.source = "images/TIMELINE_RECORD_OFF.svg"
            simulationManager.user_record(checked);
        }

        MouseArea {
            anchors.fill: recordSimuButton
            onClicked: {recordSimuButton.checked = !recordSimuButton.checked;}
        }
    }

    Rectangle {
        id: maxFrameValueBg
        implicitWidth: 60
        implicitHeight: 20
        radius: 7
        height: timelineValueBg.height
        anchors.verticalCenter: timelineValueBg.verticalCenter
        anchors.right: timelineSlider.right
        anchors.rightMargin: 20
        border.color: "#bdbebf"
        TextInput {
            id: maxFrameValue
            text: "999"

            anchors.horizontalCenter: maxFrameValueBg.horizontalCenter

            font: timelineValue.font
            color: "#000000"
            selectionColor: "#288bb9"
            selectedTextColor: "#ffffff"
            horizontalAlignment: Qt.AlignHCenter
            anchors.top: maxFrameValueBg.top
            anchors.topMargin: 3
            height: timelineValueBg.height

            readOnly: false
            validator: IntValidator{}
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            selectByMouse: true

            onEditingFinished: {simulationManager.user_set_max_frame(text*1+1)}
        }
    }

    Rectangle {
        id: reviewSpeed
        implicitWidth: 60
        implicitHeight: 20
        radius: 7
        height: timelineValueBg.height
        anchors.verticalCenter: timelineValueBg.verticalCenter
        anchors.right: maxFrameValueBg.left
        anchors.rightMargin: 20
        border.color: "#bdbebf"
        TextInput {
            id: reviewSpeedValue
            text: "40"

            anchors.horizontalCenter: reviewSpeed.horizontalCenter

            font: timelineValue.font
            color: "#000000"
            selectionColor: "#288bb9"
            selectedTextColor: "#ffffff"
            horizontalAlignment: Qt.AlignHCenter
            anchors.top: reviewSpeed.top
            anchors.topMargin: 3
            height: timelineValueBg.height

            readOnly: false
            validator: IntValidator{}
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            selectByMouse: true

            onEditingFinished: {simulationManager.user_set_speed(text*1)}
        }
    }
}
