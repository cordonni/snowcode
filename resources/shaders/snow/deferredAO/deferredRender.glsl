#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 data;

uniform camera {
    vec3 worldCameraPos;
    uniform mat4 worldToScreen;
};

uniform float showIso;

const vec3 lightDir = normalize(vec3 ( 1.0, 0.3, 0.0 ));
const vec3 fogColor = vec3(192.0, 231.0, 231.0) / 255.0;
vec3 camPos = worldCameraPos;


// deferredInput
void getDeferredInput(out vec3 pos, out vec4 color, out vec3 normal, out float material);

// deferredAO
float ao(vec3 pos, vec3 norm, float dist);

// sky
vec3 getSky(vec3 dir);

// fog
float fog(float viewDist);

// folds

void main()
{
    // get deffered inputs
    vec4 color;
    vec3 normal, pos;
    float material;

    getDeferredInput(pos, color, normal, material);

    // detect materials
    float sky = 1.0 - step(0.5, material);
    float river = clamp(material - 1.0, 0.0, 1.0) * step(0.5,material) * step(material, 2.5);
    float wire  = step(99.5, material);
    float arrow = step(9.5, material) * step(material, 10.5);
    float cutMat = step(19.5, material) * step(material, 20.5);

    data = vec4(color.rgb, 1.0);

    // fast wiring. Branching is fine here
    if(wire > 0.5)
    {
        data.rgb = vec3(0.1);
        return;
    }


    // ambient occlusion
    vec3 viewVec = pos -camPos;
    float viewDist = length(viewVec);
    vec3 viewDir = viewVec / viewDist;

    float ambient = clamp(1.0-pow(ao(pos, normal, viewDist), 0.6), 0.0, 1.0);

    // lighting
    vec3 light_sun = 1.0 * vec3(255.0,255.0,251.0)/255.0*max(dot(lightDir, normal), 0.0);
    vec3 light_ao = 0.5 * vec3(201.0,226.0,255.0)/255.0*(ambient);
    vec3 light_sun_back = 0.2 * vec3(255.0,255.0,251.0)/255.0 * max(dot(normal, -normalize(vec3(lightDir.x, 0, lightDir.z))), 0.0);

    data.rgb *= (light_sun + light_ao + light_sun_back);

    // water reflextions

    // view ray
    vec3 viewRay = sky * normalize(pos) + (1.0 - sky) * viewDir;
    vec3 refl = viewRay;
    refl.y *= -1.0;
    data.rgb += 0.5*(river) * getSky(refl);
    vec3 colorRiver = vec3(1.0);
    data.rgb = data.rgb*(1.0-river)+river*colorRiver;

    //if(river>0.5)data.rgb = vec3(1.0);


    //arrow
    vec3 arrowray = reflect(viewRay, normal);
    //if(arrowray.y>0.0)
        data.rgb += 0.3 * arrow *  getSky(arrowray) * smoothstep(-0.3,0.0,arrowray.y);

// cut
        data.rgb = cutMat * color.rgb + data.rgb * (1.0-cutMat);


    // fog
    float fog_factor = 1.0-0.0001*fog(viewDist);
    data.rgb = mix(fogColor, data.rgb, fog_factor);

    //iso lines
    float d = 0.1;
    float s=normal.y;
    //float isoLines = (0.4+0.6*d + (0.6-0.6*d) * step(mod(pos.y, 100.0), 98.0-30.0*d)) ;
    float isoLines = clamp(min(0.5+0.5*step(mod(pos.y, 200.0), 200-50/s), 0.3+0.7*step(mod(pos.y, 1000.0), 1000-75/s)), 0.0,1.0);
    data.rbg *= mix(1.0, isoLines, (1.0-arrow)*showIso* step(1.0, pos.y));

    // replace mat by sky
    data.rgb = sky * getSky(viewRay) + (1.0 - sky) * data.rgb;

    data.rgb = 1.0 - exp(-1.0*data.rgb);
    data.rgb = data.rgb*(1.0-river)+river*colorRiver;


    // finally adjust gamma
    data.rgb = pow ( data.rgb, vec3(1.0/2.2) );

}
