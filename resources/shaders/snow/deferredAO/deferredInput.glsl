#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec2 uv;

uniform camera {
    vec3 worldCameraPos;
    uniform mat4 worldToScreen;
};

uniform sampler2D colorTex;
uniform sampler2D normalTex;
uniform sampler2D posTex;

vec3 getPos(vec2 tc)
{
    return texture(posTex, tc, -100.0).xyz;
}

vec4 getColor()
{
    return texture(colorTex, uv, -100.0);
}

vec4 getNormalMat()
{
    return texture(normalTex, uv, -100.0);
}

void getDeferredInput(out vec3 pos, out vec4 color, out vec3 normal, out float material)
{
    pos = getPos(uv);
    color = getColor();

    vec4 nm = getNormalMat();
    normal = normalize(nm.xyz);
    material = nm.w;
}

ivec2 posTextureSize()
{
    return textureSize(posTex, 0);
}
