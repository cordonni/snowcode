#extension GL_ARB_separate_shader_objects : enable

uniform float fog_scale;

float fog(float viewDist)
{

    const float LOG2 = 1.442695;
    float density = 0.15 / fog_scale;
    float fogFactor = exp2( -density * density * viewDist *viewDist *LOG2);

    return  clamp(fogFactor, 0.0, 1.0);
}
