#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec2 uv;

uniform sampler2D randomTex;

uniform float ao_scale;

// deferredInput

vec3 getPos(vec2);
ivec2 posTextureSize();

// AO consts

float AO_SCALE = 0.001/ao_scale*50000;
float AO_RADIUS = 500.0*ao_scale/50000;
const float AO_BIAS = 0.01;
const float AO_INTENSITY = 4.0;

// random tex

vec2 getRandom(vec2 factor)
{
    return normalize(texture(randomTex, uv * factor ).xy * 2.0 - 1.0);
}

// neighbor contribution to ambient occlusion

float doAmbientOcclusion(vec2 duv, vec3 pos, vec3 norm)
{
    vec3 diff = getPos(uv + duv) - pos;
    float l = length(diff);
    diff /= l;
    l*= AO_SCALE;
    return max(0.0,dot(norm,diff)-AO_BIAS)*(1.0/(1.0+l));
}

// ambient occlusion at 'pos', of normal 'norm', at a distance 'dist' from view

float ao(vec3 pos, vec3 norm, float dist)
{
    vec2 vec[4] = vec2[](vec2(1,0),vec2(-1,0),
                            vec2(0,1),vec2(0,-1));


    vec2 rndfactor = vec2(posTextureSize()) / vec2(textureSize(randomTex, 0));
    vec2 rnd = getRandom(rndfactor);

    float rao = 0.0;

    float radius = AO_RADIUS / dist;

    const int iterations = 4;
    for (int j = 0; j < iterations; ++j)
    {
        vec2 coord1 = reflect(vec[j],rnd)*radius;
        vec2 coord2 = vec2(coord1.x*0.707 - coord1.y*0.707,
                               coord1.x*0.707 + coord1.y*0.707);

        rao += doAmbientOcclusion(coord1*0.25, pos, norm);
        rao += doAmbientOcclusion(coord2*0.5, pos, norm);
        rao += doAmbientOcclusion(coord1*0.75, pos, norm);
        rao += doAmbientOcclusion(coord2, pos, norm);
    }

    rao/=float(iterations)*4.0;
    return rao*AO_INTENSITY;
}
