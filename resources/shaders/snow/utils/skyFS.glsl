#extension GL_ARB_separate_shader_objects : enable


uniform sampler2D skyTex;

vec3 getSky(vec3 dir)
{
    vec2 tc = vec2(0.5, dir.y/(length(dir.xz)+dir.y));
    return texture(skyTex, tc, -100).xyz;
}
