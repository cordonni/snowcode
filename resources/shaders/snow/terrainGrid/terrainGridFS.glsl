#extension GL_ARB_separate_shader_objects : enable


layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 uv;
layout (location = 2) in vec3 outRenderTriDist;

layout(location = 0) out vec4 outcolor;
layout(location = 1) out vec4 outpos;
layout(location = 2) out vec4 outnormal;

uniform float visible;
uniform float wireframe;

uniform float uShowLayer;
uniform float uEditLayer;
uniform vec2 uMousePoint;
uniform vec2 uEditRadii;

uniform sampler2D colormap;
uniform sampler2D normalmap;
uniform sampler2D skitracks;

const float material_out = 0.0; // never use that ... prefere discard
const float material_ground = 1.0;
const float material_river = 2.0;
const float material_wire = 100.0;

const vec3 water_color = vec3(25.0, 62.0, 104.0)/104.0*0.2;

void computeColor(float curve);

float sig (float x, float l)
{
    return 1.0 / (1.0 + exp(-l*x));
}

void main()
{

    // wireframe
    const float linewidth = 0.05;
    float wire = wireframe * float(any(lessThan(outRenderTriDist, vec3(linewidth))));
    wire = wireframe * float(any(lessThan(mod(uv, vec2(1.0f/128.0f)), vec2(0.001))));

    if(visible<0.5 && wire < 0.5)
        discard;

    // send geometry data as g buffer
    outpos = vec4(pos, 1.0);
    vec4 texnorm = texture(normalmap, uv);
    outnormal = vec4(texnorm.xyz, 0.0);

    computeColor(texnorm.w);

    //set material as wireframe
    outnormal.a = wire * material_wire + (1.0-wire)* outnormal.a;
}


void computeColor(float curve)
{
    vec4 color_tex = texture(colormap, uv);

    vec3 stdcolor = color_tex.rgb;
    stdcolor *= 1.0 + sig(curve, 3.0);

    //outcolor.rgb = mix(stdcolor, clamp(color_tex.a, 0.0,1.0)*vec3(0.5,0.4,0.3), uShowLayer);

    vec3 layer_color = /*clamp(stdcolor, 0.0, 1.0) * 0.2 + 0.8 **/ vec3(clamp(color_tex.a, 0.0,1.0));

    outcolor.rgb = mix(stdcolor, layer_color, uShowLayer);

    float dist_to_mouse = distance(pos.xz, uMousePoint);


    float inner = 1.0-smoothstep(1.0,2.0,abs(dist_to_mouse - uEditRadii.x));
    float outer = 1.0 - smoothstep(1.0,2.0,abs(dist_to_mouse - uEditRadii.y));
    float s = min(1.0,(inner+ outer));

    outcolor.rgb = mix(outcolor.rgb, vec3(1.0,0.0,0.0), uEditLayer * s);
    float alpha = 0.0;
    for (int y = -1; y <= 1; y++) {
      for (int x = -1; x <= 1; x++) {
        alpha += texture(skitracks, clamp(uv + vec2(x, y) / 8192, 0, 1)).x / 9.0;
      }
    }
    alpha = min(alpha * 2, 1.0);
    outcolor.rgb = alpha * vec3(0.6) + (1.0 - alpha) * outcolor.rgb;

    outnormal.a = material_ground;


//    // compute lac
//    float lh = color.g;
//    float river = 0.0*pow(max(0.0,color.r) / terrainSize/terrainSize, 0.333);
//    if(/*(color.g>=pos.y && pos.y != 0.0 ) ||*/ color.b< 0.0)
//    {
//        river = 1.0; //lh - wpos.y;
//        outnormal.xyz = vec3(0.0,1.0,0.0);
//    }

//    float do_rever = smoothstep(0.0, 0.6, river);
//    outcolor.rgb = water*do_rever + (1.0-do_rever) * outcolor.rgb;

//    //set material
//    outnormal.a = material_ground + do_rever * (material_river - material_ground);
}
