layout(quads, fractional_odd_spacing, ccw) in;

layout (location=0) in vec2 uv[];

layout (location = 0) out vec3 outpos;
layout (location = 1) out vec2 outuv;

uniform camera {
    vec3 worldCameraPos;
    uniform mat4 worldToScreen;
};

uniform sampler2D heightmap;

uniform vec2 uTerrainSize;
uniform float uVerticalScale;
uniform float uOnEarth;

float t_height(vec2 uv, float dist)
{
    float h = 0.0;
    float n = 0.0;
    for (int i = -3; i<=3; ++i)
        for(int j = -3; i<=3; ++i)
        {
            vec2 dx = vec2(float(i), float(j));
            float k = exp(-0.5*dot(dx, dx));
            h += k * texture(heightmap, outuv + dist * dx *0.33333).x;
            n += k;
        }
    return h/n;
}


void main()
{

    vec2 a = mix(uv[0], uv[3], gl_TessCoord.x);
    vec2 b = mix(uv[1], uv[2], gl_TessCoord.x);
    outuv = mix(b, a, gl_TessCoord.y);

    //float height = uVerticalScale * texture(heightmap, outuv).x;
    float height = uVerticalScale * t_height(outuv, 2.0/255.0);

    outpos = vec3(uTerrainSize * (outuv-0.5), height).xzy;

    // earth radius
    float er = 6.378e6;

    //earth angle
    float angle = atan(length(outpos.xz), er);
    // displacement
    float dz = uOnEarth * er * (cos(angle)-1.0);

    gl_Position   = worldToScreen  * vec4(outpos+vec3(0.0,dz, 0.0),1.0);
}
