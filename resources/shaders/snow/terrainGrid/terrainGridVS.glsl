#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec2 inuv;
layout (location = 0) out vec2 outuv;

uniform camera {
  vec3 worldCameraPos;
    uniform mat4 worldToScreen;
};

uniform vec2 uTerrainSize;

void main() {
    outuv = inuv;
    vec3 outpos = vec3(uTerrainSize * (outuv-0.5), 0.0).xzy;
    gl_Position   = worldToScreen  * vec4(outpos,1.0);
}
