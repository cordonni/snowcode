// Phong tess patch data

layout(vertices=4) out;

// attributes of the input CPs
layout (location = 0) in vec2 uv[];
layout (location = 0) out vec2 outuv[];

uniform camera {
    vec3 worldCameraPos;
    uniform mat4 worldToScreen;
};

uniform sampler2D heightmap;

uniform float uVerticalScale;
uniform float uTessESize;
uniform vec2 uTerrainSize;


float GetTessLevel(vec4 p0, vec4 p1)
{
    // edge size
    float sz = distance(p0.xy, p1.xy);

    return clamp(sz / uTessESize +1.0, 1.0, 16.0);

}


void main()
{
    if (gl_InvocationID == 0)
    {

        float h0 = uVerticalScale*texture(heightmap, uv[0]).x;
        float h1 = uVerticalScale*texture(heightmap, uv[1]).x;
        float h2 = uVerticalScale*texture(heightmap, uv[2]).x;
        float h3 = uVerticalScale*texture(heightmap, uv[3]).x;

        vec3 P0 = vec3(uTerrainSize*(uv[0]-0.5), h0).xzy;
        vec3 P1 = vec3(uTerrainSize*(uv[1]-0.5), h1).xzy;
        vec3 P2 = vec3(uTerrainSize*(uv[2]-0.5), h2).xzy;
        vec3 P3 = vec3(uTerrainSize*(uv[3]-0.5), h3).xzy;

        vec4 S0 = worldToScreen  * vec4(P0,1.0);
        vec4 S1 = worldToScreen  * vec4(P1,1.0);
        vec4 S2 = worldToScreen  * vec4(P2,1.0);
        vec4 S3 = worldToScreen  * vec4(P3,1.0);

        S0 /= S0.w;
        S1 /= S1.w;
        S2 /= S2.w;
        S3 /= S3.w;

        if (S0.z <= 0.0 &&
            S1.z <= 0.0 &&
            S2.z <= 0.0 &&
            S3.z <= 0.0 && false)
        {
            gl_TessLevelOuter[0] = 0.0;
            gl_TessLevelOuter[1] = 0.0;
            gl_TessLevelOuter[2] = 0.0;
            gl_TessLevelOuter[3] = 0.0;
        }
        else
        {
            // tesselate
            float t0 = GetTessLevel(S0, S1);
            float t1 = GetTessLevel(S1, S2);
            float t2 = GetTessLevel(S2, S3);
            float t3 = GetTessLevel(S0, S3);
            gl_TessLevelOuter[0] = t0;
            gl_TessLevelOuter[1] = t1;
            gl_TessLevelOuter[2] = t2;
            gl_TessLevelOuter[3] = t3;
            gl_TessLevelInner[0] = min(t1,t3);
            gl_TessLevelInner[1] = min(t0,t2);
        }
    }

    // get data
    outuv[gl_InvocationID] = uv[gl_InvocationID];
}
