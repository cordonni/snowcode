layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

layout (location = 0) in vec3 vertpos[];
layout (location = 1) in vec2 uv[];

layout (location = 0) out vec3 outpos;
layout (location = 1) out vec2 outuv;
layout (location = 2) out vec3 outRenderTriDist;

void main() {

    gl_Position = gl_in[0].gl_Position;
    outpos = vertpos[0];
    outuv = uv[0];
    outRenderTriDist = vec3(1.0, 0.0, 0.0);
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    outpos = vertpos[1];
    outuv = uv[1];
    outRenderTriDist = vec3(0.0, 1.0, 0.0);
    EmitVertex();

    gl_Position = gl_in[2].gl_Position;
    outpos = vertpos[2];
    outuv = uv[2];
    outRenderTriDist = vec3(0.0, 0.0, 1.0);
    EmitVertex();

    EndPrimitive();

}
