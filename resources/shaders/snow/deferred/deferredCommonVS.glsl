#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 pos;
layout (location = 0) out vec2 outpos_uv;



void main() {
    gl_Position = vec4(2.0*pos-1.0, 1.0);
    outpos_uv = pos.xy;
}
