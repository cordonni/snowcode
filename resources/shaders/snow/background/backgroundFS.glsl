#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec2 outpos_uv;

layout(location = 0) out vec4 outcolor;
layout(location = 1) out vec4 outpos;
layout(location = 2) out vec4 outnormal;

uniform camera {
    vec3 worldCameraPos;
    uniform mat4 worldToScreen;
};
uniform mat4 screenToWorld;

const float material_sky = 0.0;
const float material_ground = 1.0;
const float material_river = 2.0;
const float material_wire = 100.0;

const vec3 water_color = vec3(25.0, 62.0, 104.0)/104.0*0.2;

void computeColor();

// qt nasty coloring ...
vec3 camPos = worldCameraPos;

// return ray from view
vec3 getViewRay()
{
    vec4 target = screenToWorld * vec4(outpos_uv*2.0-1.0, 1.0, 1.0);
    target/=target.w;
    return normalize(target.xyz - camPos);
}

void main()
{
    // get view ray
    vec3 viewRay = getViewRay();

    // horizon
    float aboveHorizon = step(0.0,viewRay.y);

    // compute ray intersection with ocean
    float ocean_dist = -camPos.y/viewRay.y;

    // store viewRay for sky, ocean pos else
    vec3 pos = aboveHorizon * viewRay + (camPos + ocean_dist * viewRay) * (1.0 - aboveHorizon);

    // similarly, normal is null for sky, vertical for water
    vec3 normal = (1.0-aboveHorizon) * vec3(0.0,1.0,0.0);

    // same for color
    vec3 color = (1.0-aboveHorizon) * water_color;

    // and material
    float mat = (1.0-aboveHorizon) * material_river;

    // send geometry data as g buffer
    outpos = vec4(pos, 1.0);
    outcolor = vec4(color, 1.0);
    outnormal = vec4(normal, mat);
}

