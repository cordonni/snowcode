uniform mouse {
    vec2 screenSize;
    vec2 screenMousePos;
    vec3 worldMousePos;
};

#ifdef _FRAGMENT_

// Returns distance of current fragment to mouse position.
float distanceToMouse() {
    vec2 inversedMousePos = vec2(screenMousePos.x, screenSize.y - screenMousePos.y);
    return length(gl_FragCoord.xy / screenSize - inversedMousePos / screenSize);
}

// Returns distance of given point to mouse position.
float distanceToMouse(vec2 coord) {
    return length(coord / screenSize - screenMousePos / screenSize);
}

#endif
