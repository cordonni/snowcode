#extension GL_ARB_separate_shader_objects : enable

uniform camera {
    vec3 worldCameraPos;
    uniform mat4 worldToScreen;
};

layout (location = 0) in vec3 wpos;
layout (location = 1) in vec3 wnormal;
layout (location = 2) in vec4 fcolor;
layout (location = 3) in vec3 triDist;
layout (location = 4) in float minTriDist;

uniform float selected;
uniform float hovered;
uniform float visible;
uniform float wireframe;
float colored = 1;

uniform vec4 hoveredColor;
uniform vec4 selectedColor;
// background  AAABAA -> F0F1F0
// normal      5696c1
// selected    e7001b
// hovered     ff6f54
// uncolored   0.6 0.6 0.6 1.0

layout(location = 0) out vec4 data;

vec3 viewDir(vec3 worldP);

float illuminance(vec3 worldP, vec3 worldN, out vec3 worldL);
float phong(vec3 worldP, vec3 worldN, out vec3 worldL);

void main() {

    vec3 wl;
    vec3 wn = normalize(wnormal);

    data = mix(vec4(0.6, 0.6, 0.6, 1.0), fcolor, colored); // initializes color to initial object color.

    // compute distance to camera : This distance is used to change the width of the selection contour.
    // It will be thin when close, thicker when far, in order to remain visible.
    float distToCamera = min(length(worldCameraPos - wpos) / 10.0, 1.0);
    float stepMin = clamp(1.0 - distToCamera, 0.3, 0.5); // Inverse the distance ratio, and clamp it to acceptable "normal" values.

    // selectedNorm determines whether we are inside the contour or not. This mostly depends on the normal of the point relatively to the
    // direction of the camera. If their dot product is high (they are almost perpendicular), we are in the contour.
    // stepMin determines the step at which we decide we are in the contour or not, depending on the distance to the camera.
    float selectedNorm = step(stepMin, 1.0 - max(-dot(viewDir(wpos), normalize(wn)), 0.0));

    // Choose initial, selected or hovered color.
    data = mix(data, selectedColor, selected);
    data = mix(data, data * hoveredColor, hovered);

    // Update color to add light.
    //data += vec4(vec3(0.3 * phong(wpos, wn, wl)), 0.0);
    data.rgb = data.rgb * 0.7 + 0.3 * phong(wpos, wn, wl);
//    data.rgb = fcolor.rgb;
//    data.rgb = vec3(illuminance(wpos, wn, wl));


    // Update contour if necessary
    data.rgb = mix(data.rgb, selectedColor.rgb, selectedNorm * selected);

    // todo make this faster - remove 'ifs' (use stuff like lines 29-30)
    // Draw the wireframe
    float linewidth = 0.07;
    if (wireframe == 1) { // if WF is enabled, we draw the fragments that are close to the triangle border.
        if (any(lessThan(triDist, vec3(linewidth)))) {
            data.rgb = vec3(0.8);
        } else { // for any other point, check if it should be visible or not.
            if (visible == 0)
            {
                discard; // discard allows for transparency even for items drawn outside of the transparency scope.
            }
        }
    } else if (visible == 0) { // Else, check if object is visible. Ideally, we shouldn't be here.
        discard;
    }


//    data.a *= visible;

}
