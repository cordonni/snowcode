#ifdef _VERTEX_

layout (location = 0) in vec3 vertex;
layout (location = 3) in vec4 color;

out vec4 fcolor;

uniform vec2 pos;
uniform vec2 scale;

void main() {
    gl_Position = vec4(vertex, 1.0) * vec4(scale, 1.0, 1.0) + vec4(pos, 0.0, 0.0);
    fcolor = color;
}

#endif

#ifdef _FRAGMENT_

in vec4 fcolor;
uniform float visible;

layout (location = 0) out vec4 data;

void main() {
    data = fcolor;
    data.a *= visible;
}

#endif
