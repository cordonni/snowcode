uniform light {
    vec3 worldLightPos;
    vec3 worldLightDir;
    vec2 spotlightAngle;
};

vec3 backlightPos = vec3(0.0, 50.0, -50.0);
vec3 backlightDir = vec3(0.0, 0.0, 1.0);

const float backlightFactor = 0.3;

float illuminance(vec3 worldP, vec3 worldN, out vec3 worldL) {
    worldL = normalize(worldLightPos - worldP);
    vec3 backworldL = normalize(backlightPos - worldP);
//    worldL = normalize(-worldLightPos - worldP);
    float falloff = 1.0 - smoothstep(spotlightAngle.x, spotlightAngle.y, acos(dot(worldLightDir, -worldL)));
    return max(dot(worldN, worldL), 0.0) * falloff + backlightFactor*max(dot(worldN, backworldL), 0.0);
}

float phong(vec3 worldP, vec3 worldN, out vec3 worldL) {
    worldL = normalize(worldLightPos - worldP);
    vec3 backworldL = normalize(backlightPos - worldP);
    return max(dot(worldN, worldL), 0.0) + backlightFactor*max(dot(worldN, backworldL), 0.0);
//    return abs(dot(worldN, worldL));// + abs(dot(worldN, backworldL));
}

#ifdef _VERTEX_
#endif

#ifdef _FRAGMENT_
#endif
