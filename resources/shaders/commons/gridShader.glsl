#ifdef _VERTEX_

layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

out vec3 wpos;
out vec3 wnormal;
out vec2 fuv;

uniform int selected;
uniform int hovered;
uniform int visible;
uniform int wireframe;

void projection(vec3 normal, out vec3 worldPos, out vec3 worldNormal);

void main() {
    projection(normal, wpos, wnormal);
    fuv = uv;
}

#endif

#ifdef _FRAGMENT_

uniform camera {
    vec3 worldCameraPos;
    uniform mat4 worldToScreen;
};

in vec3 wpos;
in vec2 fuv;

layout(location = 0) out vec4 data;

vec3 viewDir(vec3 worldP);

float illuminance(vec3 worldP, vec3 worldN, out vec3 worldL);

float PI = 3.14159265358979323846264;
vec4 gridColor = vec4(0.0, 0.0, 0.0, 1.0);

void main()
{
    float l = length(wpos);

    //// PARAMETERS
    float freq0 =  0.1;
    float freq1 =  1.0;
    float freq2 =  5.0;
    float freq3 = 10.0;

    float width0 = 0.1;
    float width1 = 0.1;
    float width2 = 0.1;
    float width3 = 0.1;

    float area0 =  5.0;
    float area1 = 20.0;
    float area2 = 50.0;
    float area3 = 100.0;

    float viewDist0 = 10.0;
    float viewDist1 = 50.0;
    float viewDist2 = 100.0;
    float viewDist3 = 500.0;

    float minViewDist0 = 0.0;
    float minViewDist1 = 1.0;
    float minViewDist2 = 5.0;
    float minViewDist3 = 20.0;

    vec4 color0 = vec4(0.0, 0.0, 0.0, 1.0);
    vec4 color1 = vec4(0.0, 0.0, 0.0, 1.0);
    vec4 color2 = vec4(0.0, 0.0, 0.0, 1.0);
    vec4 color3 = vec4(0.0, 0.0, 0.0, 1.0);

    float dist = length(worldCameraPos - wpos);

    vec2 val0 = step(1.0 - width0, abs(mod(wpos.xz / freq0, 1.0) * 2.0 - 1.0));
    vec2 val1 = step(1.0 - width1, abs(mod(wpos.xz / freq1, 1.0) * 2.0 - 1.0));
    vec2 val2 = step(1.0 - width2, abs(mod(wpos.xz / freq2, 1.0) * 2.0 - 1.0));
    vec2 val3 = step(1.0 - width3, abs(mod(wpos.xz / freq3, 1.0) * 2.0 - 1.0));

    float val0Alpha = max(0.0, (1.0 - l / area0)) * max((1.0 - dist / viewDist0), 0.0);
    float val1Alpha = max(0.0, (1.0 - l / area1)) * max((1.0 - dist / viewDist1), 0.0);
    float val2Alpha = max(0.0, (1.0 - l / area2)) * max((1.0 - dist / viewDist2), 0.0);
    float val3Alpha = max(0.0, (1.0 - l / area3)) * max((1.0 - dist / viewDist3), 0.0);

    float p1 = (dist - minViewDist0) / (viewDist0 - minViewDist0);
    float p2 = (dist - minViewDist1) / (viewDist1 - minViewDist1);
    float p3 = (dist - minViewDist2) / (viewDist2 - minViewDist2);
    float p4 = (dist - minViewDist3) / (viewDist3 - minViewDist3);
    val0Alpha = max(0.0, (1.0 - l / area0)) * max(4.0*(p1*p1*-2.0 + p1), 0.0);
    val1Alpha = max(0.0, (1.0 - l / area1)) * max(4.0*(p2*p2*-2.0 + p2), 0.0);
    val2Alpha = max(0.0, (1.0 - l / area2)) * max(4.0*(p3*p3*-1.0 + p3), 0.0);
    val3Alpha = max(0.0, (1.0 - l / area3)) * max(4.0*(p4*p4*-1.0 + p4), 0.0);
//    vec2 val = val0 * val0Alpha + val1 * lowTransparency + val2 * midTransparency + val3 * highTransparency;
//    val = val0 + val1 + val2 + val3;
    vec2 val = max(max(max(val0 * val0Alpha, val1 * val1Alpha), val2 * val2Alpha), val3 * val3Alpha);

    data = gridColor;
    data.a = max(val.x, val.y) * 0.5;
    data.a *= smoothstep(0.0, 1.0, 1.0 - dist * 0.0001); // todo make this better. maybe use dFdx/dFdy
//    data.a = 1.0;
//    data.rg = val3 * val3Alpha;
//    data.r = 1.0 - dist/ viewDist3;/*1.0 - l /area3*/;
    //data.rg = step(0.1, abs(mod(wpos.xz / freq3, 1.0) * 2.0 - 1.0));
}

#endif
