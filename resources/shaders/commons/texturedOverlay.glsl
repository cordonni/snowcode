#ifdef _VERTEX_

layout (location = 0) in vec4 vertex;
layout (location = 2) in vec2 uv;
layout (location = 3) in vec4 color;

out vec2 fuv;
out vec4 fcolor;

uniform vec2 pos;
uniform vec2 scale;

void main() {
    gl_Position = vertex * vec4(scale, 1.0, 1.0) + vec4(pos, 0.0, 0.0);
//    gl_Position.z = 0.5;
    fcolor = color;
    fuv = uv;
}

#endif

#ifdef _FRAGMENT_

in vec2 fuv;
in vec4 fcolor;

uniform sampler2D tex;
uniform float visible;

layout (location = 0) out vec4 data;

void main() {
    data = fcolor * texture(tex, fuv);
//    data = vec4(0.0);
//    data = vec4(fuv, .0f, .0f);

    data.a *= visible;
}

#endif
