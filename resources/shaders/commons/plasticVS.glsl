#extension GL_ARB_separate_shader_objects : enable

layout(location = 1) in vec3 normal;
layout(location = 3) in vec4 color;

layout (location = 0) out vec3 vg_wpos;
layout (location = 1) out vec3 vg_wnormal;
layout (location = 2) out vec4 vg_fcolor;
layout (location = 3) out vec3 vg_triDist;

void projection(vec3 normal, out vec3 worldPos, out vec3 worldNormal);

void main() {
    projection(normal, vg_wpos, vg_wnormal);
    vg_fcolor = color;
    vg_triDist = vec3(0.0);
}
