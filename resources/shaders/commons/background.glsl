#ifdef _VERTEX_

layout (location = 0) in vec4 vertex;
layout (location = 2) in vec2 uv;
layout (location = 3) in vec4 color;

out vec2 fuv;

void main() {
    gl_Position = vertex * vec4(2.0, 2.0, 1.0, 1.0) + vec4(-1.0, -1.0, 0.0, 0.0);
    fuv = uv;
}

#endif

#ifdef _FRAGMENT_

in vec2 fuv;

//uniform sampler2D tex;
//uniform float visible;
//uniform mat4 localToWorld;

uniform camera {
    vec3 worldCameraPos;
    uniform mat4 worldToScreen;
};


uniform light {
    vec3 worldLightPos;
    vec3 worldLightDir;
    vec2 spotlightAngle;
};


uniform mouse {
    vec2 screenSize;
    vec2 screenMousePos;
    vec3 worldMousePos;
};



layout (location = 0) out vec4 data;

const vec3 groundColor = vec3(.95);
//const vec3 groundColor = vec3(0.99);
//const vec3 skyColor = vec3(.85, .85, .99);
const vec3 skyColor = vec3(135, 150, 250) / 256.0f * 1.6;
const vec3 sunColor = vec3(255,255,224) / 256.0f * 1.0;

const float PI = 3.1415972f;
//const float alpha0 = 1.5f;
//const float alpha1 = 5.0f;
const float alpha0 = 0.5f;
const float alpha1 = 5.0f;

vec3 colormap(float v)
{
    return (1.0f - v) * groundColor + v * skyColor;
}

float stepFunction(float x)
{
    if(x < .0f)
        return .0f;

    if(x > 1.0f)
        return 1.0f;

    return 1.0 - cos(x * PI * 0.5) * cos(x * PI * 0.5);
}

vec3 fragDir(vec2 uv)
{
    vec4 p0 = inverse(worldToScreen) * vec4(uv * 2.0 - 1.0, -1.0f, 1.0f);
    vec4 p1 = inverse(worldToScreen) * vec4(uv * 2.0 - 1.0,  1.0f, 1.0f);

    p0 /= p0.w;
    p1 /= p1.w;
    return normalize(p1 - p0).xyz;
}

float random(vec2 seed)
{
    float dot_product = dot(vec4(seed, seed), vec4(12.9898,78.233,45.164,94.673));
    return fract(sin(dot_product) * 43758.5453);
}

vec2 random2(vec2 seed)
{
//    return seed;
    return vec2(random(seed), random(seed.yx * 5.35435 + vec2(0.135432413547, 4648.154687)));
}


void main()
{
    vec4 p0 = inverse(worldToScreen) * vec4(fuv * 2.0 - 1.0, -1.0f, 1.0f);
    vec4 p1 = inverse(worldToScreen) * vec4(fuv * 2.0 - 1.0,  1.0f, 1.0f);

    p0 /= p0.w;
    p1 /= p1.w;
    vec3 d = normalize(p1 - p0).xyz;

    float t = dot(d, vec3(0,1,0));
    t = t * (alpha1 - alpha0) + alpha0;
    t = stepFunction(t);

    float t2 = dot(d, -worldLightDir);
    t2 = (t2 - 0.9)*10.0;
    t2 = stepFunction(t2);
    vec3 skyColor2 = (1.0f - t2) * skyColor + t2 * sunColor;
//    vec3 skyColor2 = skyColor;

    data.rgb = (1.0f - t) * groundColor + t * skyColor2;

    data.a = 1.0;// + 0.001*(visible + texture(tex, fuv).r  + localToWorld[0].x);
    return;

//    return;

    if(dot(d, vec3(0,1,0)) < 0 && worldCameraPos.y > 0 || dot(d, vec3(0,1,0)) > 0 && worldCameraPos.y < 0)
    {

        vec3 baseColor = data.rgb;

        vec3 n = vec3(0,1,0);

        float AA = 5.0f;

        data.rgb = vec3(0.0);

        float tt;

        for(float xx = 0.0; xx < AA; xx += 1.0f)
        {
            for(float yy = 0.0; yy < AA; yy += 1.0f)
            {
//                vec2 aa = vec2(xx, yy) / screenSize / AA;

                vec2 aa = vec2(xx, yy) / AA;
                aa = random2(aa);
                aa -= vec2(0.5);
                aa = aa / screenSize;

                vec3 d2 = fragDir(fuv + aa);
                tt = -dot(worldCameraPos, n) / dot(d2, n);
                vec3 p = worldCameraPos + tt * d2;
                vec2 pos2d = p.xz;

                float freq = 5.0;
                float val_ref = 1.0;
                float width = 0.03;// * tt;
                width = min(width, 0.1);

                float val = 0;

                for(int i = 0;  i < 4; i++)
                {
                    if(mod(pos2d.x, 1) > 1.0 - width/2 || mod(pos2d.x, 1) < width/2)
                        val = max(val, val_ref);
                    if(mod(pos2d.y, 1) > 1.0 - width/2 || mod(pos2d.y, 1) < width/2)
                        val = max(val, val_ref);

                    pos2d *= freq;
                    width /= freq;
                }


                data.rgb += vec3(1 - val) / AA / AA;
            }
        }

        data.rgb = mix(data.rgb, baseColor, 1.0 - exp(-abs(tt) / 50));
    }
}

#endif
