layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

layout (location = 0) in vec3 vg_wpos[];
layout (location = 1) in vec3 vg_wnormal[];
layout (location = 2) in vec4 vg_fcolor[];
layout (location = 3) in vec4 vg_triDist[];

layout (location = 0) out vec3 wpos;
layout (location = 1) out vec3 wnormal;
layout (location = 2) out vec4 fcolor;
layout (location = 3) out vec3 triDist;
layout (location = 4) out float minTriDist;

float DistToLine(vec3 a, vec3 b, vec3 p)
{
    return 1.0;
    vec3 ap = p - a;
    vec3 ab = b - a;
    float dotprod = dot(ap, ab);
    float lenAP = length(ap);
    float lenAB = length(ab);

    float projLenSq = dotprod * dotprod / (lenAB*lenAB);

    return abs(lenAP*lenAP - projLenSq);
}

//float DistToLine (vec3 p, vec3 segA,vec3 segB) {
////    return 1.0;
//        vec2 p2 = vec2(segB.x - segA.x,segB.y - segA.y);
//        float num = p2.x * p2.x + p2.y * p2.y;
//        float u = ((p.x - segA.x) * p2.x + (p.y - segA.y) * p2.y) / num;

//        if (u > 1.0 ) {
//                u = 1.0;
//        }
//        else if (u < 0.0) {
//                u = 0.0;
//        }

//        float x = segA.x + u * p2.x;
//        float y = segA.y + u * p2.y;

//        float dx = x - p.x;
//        float dy = y - p.y;

//        return sqrt(dx*dx + dy*dy);
//}

void main() {

    float triDist1 = DistToLine(vg_wpos[1], vg_wpos[2], vg_wpos[0]);
    float triDist2 = DistToLine(vg_wpos[1], vg_wpos[2], vg_wpos[0]);
    float triDist3 = DistToLine(vg_wpos[1], vg_wpos[2], vg_wpos[0]);

    minTriDist = min(triDist1, min(triDist2, triDist3));
//    minTriDist = triDist1 * length(vg_wpos[1] - vg_wpos[2]) / 2.0;

    gl_Position = gl_in[0].gl_Position;
    wpos    = vg_wpos[0];
    wnormal = vg_wnormal[0];
    fcolor  = vg_fcolor[0];
    triDist = vec3(DistToLine(vg_wpos[1], vg_wpos[2], vg_wpos[0]), 0.0, 0.0);
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    wpos    = vg_wpos[1];
    wnormal = vg_wnormal[1];
    fcolor  = vg_fcolor[1];
    triDist = vec3(0.0, DistToLine(vg_wpos[1], vg_wpos[2], vg_wpos[0]), 0.0);
    EmitVertex();

    gl_Position = gl_in[2].gl_Position;
    wpos    = vg_wpos[2];
    wnormal = vg_wnormal[2];
    fcolor  = vg_fcolor[2];
    triDist = vec3(0.0, 0.0, DistToLine(vg_wpos[1], vg_wpos[2], vg_wpos[0]));
    EmitVertex();

    EndPrimitive();

}
