#-------------------------------------------------
#
# Project pluginTools qmake file
#
#-------------------------------------------------

DEFINES += BUILD_CONVOL

PROJECT_NAME = convol
TEMPLATE = lib
#CONFIG += lib
CONFIG += shared #staticlib

# Dependencies
DEPENDS_INTERNAL += ork
DEPENDS_INTERNAL += commons
DEPENDS_INTERNAL += core
DEPENDS_EXTERNAL += Boost
#DEPENDS_EXTERNAL += OpenMesh

QT -= gui core

# Common define and config that are re-used everywhere
lessThan(QT_MAJOR_VERSION, 5) {
    load(../GlobalConfiguration.prf)
} else {
    include(../GlobalConfiguration.prf)
}

#---------------------------------------------------------------
#
#   Source du projet
#
#---------------------------------------------------------------

SOURCES_PATH = $${EXPRESSIVE_HOME_PATH}/src/$${PROJECT_NAME}

SOURCES =   \
            ../../src/convol/ConvolObject.cpp \
            ../../src/convol/ImplicitSurface.cpp \
            ../../src/convol/ScalarField.cpp \
            # BlobtreeNode
            ../../src/convol/blobtreenode/BlobtreeBasket.cpp \
            ../../src/convol/blobtreenode/BlobtreeNode.cpp \
            ../../src/convol/blobtreenode/BlobtreeRoot.cpp \
            ../../src/convol/blobtreenode/NodeScalarField.cpp \
            ../../src/convol/blobtreenode/NodeOperatorT.cpp \
            ../../src/convol/blobtreenode/NodeProceduralDetailT.cpp \
            ../../src/convol/blobtreenode/NodeProceduralDetailParameterT.cpp \
                # node operator
                ../../src/convol/blobtreenode/nodeoperator/CorrectedSumIntegralSurfaceT.cpp \
                ../../src/convol/blobtreenode/nodeoperator/GradientBasedIntegralSurfaceT.cpp \
#                ../../src/convol/blobtreenode/nodeoperator/NodeOperatorNCorrectedConvolutionT.h \
#                ../../src/convol/blobtreenode/nodeoperator/NodeOperatorNWitnessedBlendT.cpp \
                ../../src/convol/blobtreenode/nodeoperator/IntegralSurfaceCorrector.cpp \
            # factories
            ../../src/convol/factories/BlobtreeNodeFactory.cpp \
            ../../src/convol/factories/ISCorrectorFactory.cpp \
            # functors
                # kernels
                ../../src/convol/functors/kernels/Kernel.cpp \
                ../../src/convol/functors/kernels/SemiNumericalHomotheticConvolT.cpp \
                ../../src/convol/functors/kernels/HomotheticDistanceT.cpp \
                    #CompactPolynomial
                    ../../src/convol/functors/kernels/compactpolynomial/CompactPolynomial.cpp \
                    ../../src/convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.cpp \
                    ../../src/convol/functors/kernels/compactpolynomial/HornusCompactPolynomial.cpp \
                    ../../src/convol/functors/kernels/compactpolynomial/StandardCompactPolynomial.cpp \
                    #Inverse&Cauchy
                    ../../src/convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.cpp \
                    ../../src/convol/functors/kernels/cauchyinverse/HomotheticCauchy.cpp \
                    ../../src/convol/functors/kernels/cauchyinverse/HomotheticInverse.cpp \
                    ../../src/convol/functors/kernels/cauchyinverse/HornusCauchy.cpp \
                    ../../src/convol/functors/kernels/cauchyinverse/HornusInverse.cpp \
                    ../../src/convol/functors/kernels/cauchyinverse/StandardCauchy.cpp \
                    ../../src/convol/functors/kernels/cauchyinverse/StandardInverse.cpp \
                # operators
                ../../src/convol/functors/operators/BlendMax.cpp \
                ../../src/convol/functors/operators/BlendSum.cpp \
                ../../src/convol/functors/operators/BlendRicci.cpp \
                ../../src/convol/functors/operators/DistanceIntegralSurface.cpp \
                # noises
                ../../src/convol/functors/noises/BandLimitedGaborSurfaceNoise.cpp \
                ../../src/convol/functors/noises/ControllableGaborSurfaceNoise.cpp \
                ../../src/convol/functors/noises/NoiseGaborSurface.cpp \
                ../../src/convol/functors/noises/PolynomialDeformationMap.cpp \
            # primitives
            ../../src/convol/primitives/NodePointSFieldT.cpp \
            ../../src/convol/primitives/NodeSegmentSFieldT.cpp \
            ../../src/convol/primitives/NodeTriangleSFieldT.cpp \
            ../../src/convol/primitives/NodeISSubdivisionCurveT.cpp \
            # properties
            ../../src/convol/properties/Properties0D.cpp \
            ../../src/convol/properties/Properties1D.cpp \
            # skeleton
            ../../src/convol/skeleton/Skeleton.cpp \
            # tools
            ../../src/convol/tools/ConvergenceTools.cpp \
            ../../src/convol/tools/FBVHKernelEval.cpp \
            ../../src/convol/tools/PictureScalarFieldSlice.cpp \
                # polygonizer
                ../../src/convol/tools/polygonizer/BasicBloomenthalPolygonizerStructureT.cpp \
../../src/convol/tools/polygonizer/DCOctree/DCOctreeStructures.cpp \
../../src/convol/tools/polygonizer/DCOctree/qef.cpp \
                # updating
                ../../src/convol/tools/updating/AreaUpdatingPolygonizerHandler.cpp \
                ../../src/convol/tools/updating/SurfaceCreationBackgroundTask.cpp \
                ../../src/convol/tools/updating/AreaUpdatingBloomenthalPolygonizer/AreaBPBackgroundTasks.cpp \
                ../../src/convol/tools/updating/AreaUpdatingBloomenthalPolygonizer/AreaUpdatingBloomenthalPolygonizerHandler.cpp \
                ../../src/convol/tools/updating/AreaUpdatingDualContouring/AreaDCBackgroundTasks.cpp \
                ../../src/convol/tools/updating/AreaUpdatingDualContouring/AreaUpdatingDualContouringHandler.cpp \
        # xmlloaders
        ../../src/convol/xmlloaders/SkeletonLoader.cpp \
        ../../src/convol/xmlloaders/DefaultPrimitiveXmlLoaderT.cpp \
            # primitive loaders
            ../../src/convol/xmlloaders/primitives/NodeOLDSegmentSFieldLoader.cpp \
            ../../src/convol/xmlloaders/primitives/NodeOLDTriangleSFieldLoader.cpp \
            # blobtreenode loaders
            ../../src/convol/xmlloaders/blobtreenode/BlobtreeNodeLoader.cpp \
            ../../src/convol/xmlloaders/blobtreenode/BlobtreeRootLoader.cpp \
            ../../src/convol/xmlloaders/blobtreenode/TemplatedNodeOperatorLoader.cpp \
            ../../src/convol/xmlloaders/blobtreenode/NodeScalarFieldLoader.cpp \
            ../../src/convol/xmlloaders/blobtreenode/NodeIntegralSurfaceOperatorLoader.cpp \
    ../../src/convol/functors/operators/BlendDiff.cpp \
    ../../src/convol/functors/operators/BlendBulge.cpp \
    ../../src/convol/blobtreenode/BlobtreeCommands.cpp \
    ../../src/convol/xmlloaders/primitives/NodeOLDPointSFieldLoader.cpp \
    ../../src/convol/tools/updating/BMeshPolygonizer/BMeshPolygonizerHandler.cpp \
    ../../src/convol/tools/updating/BMeshPolygonizer/BMBackgroundTasks.cpp \
    ../../src/convol/tools/polygonizer/BMeshPolygonizer.cpp



HEADERS =   \
            ../../src/convol/ConvolObject.h \
            ../../src/convol/ImplicitSurface.h \
            ../../src/convol/ScalarField.h \
            # BlobtreeNode
            ../../src/convol/blobtreenode/BlobtreeBasket.h \
            ../../src/convol/blobtreenode/BlobtreeNode.h \
            ../../src/convol/blobtreenode/BlobtreeRoot.h \
            ../../src/convol/blobtreenode/NodeScalarField.h \
            ../../src/convol/blobtreenode/NodeOperatorT.h \
            ../../src/convol/blobtreenode/NodeProceduralDetailT.h \
            ../../src/convol/blobtreenode/NodeProceduralDetailT.hpp \
            ../../src/convol/blobtreenode/NodeProceduralDetailParameterT.h \
            ../../src/convol/blobtreenode/NodeProceduralDetailParameterT.hpp \
                # BlobtreeNode Listeners
                ../../src/convol/blobtreenode/listeners/BlobtreeNodeListener.h \
                ../../src/convol/blobtreenode/listeners/BlobtreeRootListener.h \
                ../../src/convol/blobtreenode/listeners/NodeScalarFieldListener.h \
                # NodeOperator
                ../../src/convol/blobtreenode/nodeoperator/NodeSumOptimized.h \
                ../../src/convol/blobtreenode/nodeoperator/NodeSumOptimized.hpp \
                ../../src/convol/blobtreenode/nodeoperator/CorrectedSumIntegralSurfaceT.h \
                ../../src/convol/blobtreenode/nodeoperator/CorrectedSumIntegralSurfaceT.hpp \
                ../../src/convol/blobtreenode/nodeoperator/GradientBasedIntegralSurfaceT.h \
                ../../src/convol/blobtreenode/nodeoperator/GradientBasedIntegralSurfaceT.hpp \
                ../../src/convol/blobtreenode/nodeoperator/NodeOperatorNCorrectedConvolutionT.h \
                ../../src/convol/blobtreenode/nodeoperator/NodeOperatorNWitnessedBlendT.h \
                ../../src/convol/blobtreenode/nodeoperator/NodeOperatorNWitnessedBlendT.hpp \
                ../../src/convol/blobtreenode/nodeoperator/IntegralSurfaceCorrector.h \
                ../../src/convol/blobtreenode/nodeoperator/IntegralSurfaceCorrector.hpp \
                ../../src/convol/blobtreenode/nodeoperator/IntegralSurfaceOperator.h \
           # factories
            ../../src/convol/factories/BlobtreeNodeFactory.h \
            ../../src/convol/factories/ISCorrectorFactory.h \
           # kernels
           ../../src/convol/functors/kernels/Kernel.h \
                #
                ../../src/convol/functors/kernels/SemiNumericalHomotheticConvolT.h \
                ../../src/convol/functors/kernels/SemiNumericalHomotheticConvolT.hpp \
                ../../src/convol/functors/kernels/HomotheticDistanceT.h \
                ../../src/convol/functors/kernels/HomotheticDistanceT.hpp \
                #CauchyInverse
                ../../src/convol/functors/kernels/cauchyinverse/Cauchy.h \
                ../../src/convol/functors/kernels/cauchyinverse/Cauchy.hpp \
                ../../src/convol/functors/kernels/cauchyinverse/CauchyIsoValueAtDistance.h \
                ../../src/convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.h \
                ../../src/convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.hpp \
                ../../src/convol/functors/kernels/cauchyinverse/Inverse.h \
                ../../src/convol/functors/kernels/cauchyinverse/Inverse.hpp \
                ../../src/convol/functors/kernels/cauchyinverse/InverseIsoValueAtDistance.h \
                    ../../src/convol/functors/kernels/cauchyinverse/HomotheticCauchy.h \
                    ../../src/convol/functors/kernels/cauchyinverse/HomotheticCauchy.hpp \
                    ../../src/convol/functors/kernels/cauchyinverse/HomotheticInverse.h \
                    ../../src/convol/functors/kernels/cauchyinverse/HomotheticInverse.hpp \
                    ../../src/convol/functors/kernels/cauchyinverse/HomotheticCauchyAndInverseOptimizedFunctions.h \
                    ../../src/convol/functors/kernels/cauchyinverse/HornusCauchy.h \
                    ../../src/convol/functors/kernels/cauchyinverse/HornusCauchy.hpp \
                    ../../src/convol/functors/kernels/cauchyinverse/HornusInverse.h \
                    ../../src/convol/functors/kernels/cauchyinverse/HornusInverse.hpp \
                    ../../src/convol/functors/kernels/cauchyinverse/StandardCauchy.h \
                    ../../src/convol/functors/kernels/cauchyinverse/StandardCauchy.hpp \
                    ../../src/convol/functors/kernels/cauchyinverse/StandardInverse.h \
                    ../../src/convol/functors/kernels/cauchyinverse/StandardInverse.hpp \
                    #CompactPolynomial
                    ../../src/convol/functors/kernels/compactpolynomial/CompactPolynomial.h \
                    ../../src/convol/functors/kernels/compactpolynomial/CompactPolynomial.hpp \
                    ../../src/convol/functors/kernels/compactpolynomial/CompactPolynomialIsoValueAtDistance.h \
                    ../../src/convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.h \
                    ../../src/convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.hpp \
                    ../../src/convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomialOptimizedFunctions.h \
                    ../../src/convol/functors/kernels/compactpolynomial/HornusCompactPolynomial.h \
                    ../../src/convol/functors/kernels/compactpolynomial/HornusCompactPolynomial.hpp \
                    ../../src/convol/functors/kernels/compactpolynomial/StandardCompactPolynomial.h \
                    ../../src/convol/functors/kernels/compactpolynomial/StandardCompactPolynomial.hpp \
                # operators
                ../../src/convol/functors/operators/BlendMax.h \
                ../../src/convol/functors/operators/BlendOperator.h \
                ../../src/convol/functors/operators/BlendSum.h \
                ../../src/convol/functors/operators/BlendSumOptimized.h \
                ../../src/convol/functors/operators/BlendRicci.h \
                ../../src/convol/functors/operators/DistanceIntegralSurface.h \
                # noises
                ../../src/convol/functors/noises/NoiseGaborSurface.h \
                ../../src/convol/functors/noises/BandLimitedGaborSurfaceNoise.h \
                ../../src/convol/functors/noises/ControllableGaborSurfaceNoise.h \
                ../../src/convol/functors/noises/PseudoRandomNumberGenerator.h \
                ../../src/convol/functors/noises/PolynomialDeformationMap.h \
            # primitives
            ../../src/convol/primitives/NodePointSFieldT.h \
            ../../src/convol/primitives/NodePointSFieldT.hpp \
            ../../src/convol/primitives/NodeSegmentSFieldT.h \
            ../../src/convol/primitives/NodeSegmentSFieldT.hpp \
            ../../src/convol/primitives/NodeTriangleSFieldT.h \
            ../../src/convol/primitives/NodeTriangleSFieldT.hpp \
            ../../src/convol/primitives/NodeISSubdivisionCurveT.h \
            ../../src/convol/primitives/NodeISSubdivisionCurveT.hpp \
#            ../../src/convol/SpecialSegmentSFieldT.h \
#            ../../src/convol/TriangleSFieldT.h \
            # properties
            ../../src/convol/properties/Properties0D.h \
            ../../src/convol/properties/Properties1D.h \
            # skeleton
            ../../src/convol/skeleton/Skeleton.h \
            # tools
            ../../src/convol/tools/Array3DT.h \
            ../../src/convol/tools/ConvergenceTools.h \
            ../../src/convol/tools/GradientEvaluationTools.h \
            ../../src/convol/tools/FittedBVH.h \
            ../../src/convol/tools/FittedBVH.hpp \
            ../../src/convol/tools/FBVHKernelEval.h \
            ../../src/convol/tools/SolvingEquation4T.h \  #TODO:@todo : a supprimer ou ameliorer ...
            ../../src/convol/tools/PictureScalarFieldSlice.h \
                # polygonizer
                ../../src/convol/tools/polygonizer/AreaUpdatingPolygonizer.h \
                ../../src/convol/tools/polygonizer/AreaUpdatingBloomenthalPolygonizerT.h \
                ../../src/convol/tools/polygonizer/BasicBloomenthalPolygonizerStructureT.h \
                ../../src/convol/tools/polygonizer/MarchingCubesTables.h \
                ../../src/convol/tools/polygonizer/VertexProcessorT.h \
../../src/convol/tools/polygonizer/AreaUpdatingDualContouringT.h \
../../src/convol/tools/polygonizer/DCOctree/DCOctree.h \
../../src/convol/tools/polygonizer/DCOctree/DCOctreeFindIntersectionT.h \
../../src/convol/tools/polygonizer/DCOctree/DCOctreeNode.h \
../../src/convol/tools/polygonizer/DCOctree/DCOctreeStructures.h \
../../src/convol/tools/polygonizer/DCOctree/DCPrecisionOctree.h \
../../src/convol/tools/polygonizer/DCOctree/qef.h \
                # updating
                ../../src/convol/tools/updating/SurfaceCreationBackgroundTask.h \
                ../../src/convol/tools/updating/AreaUpdatingPolygonizerHandler.h \
                ../../src/convol/tools/updating/AreaUpdatingBloomenthalPolygonizer/AreaBPBackgroundTasks.h \
                ../../src/convol/tools/updating/AreaUpdatingBloomenthalPolygonizer/AreaUpdatingBloomenthalPolygonizerHandler.h \
                ../../src/convol/tools/updating/AreaUpdatingDualContouring/AreaDCBackgroundTasks.h \
                ../../src/convol/tools/updating/AreaUpdatingDualContouring/AreaUpdatingDualContouringHandler.h \
        # xmlloaders
        ../../src/convol/xmlloaders/SkeletonLoader.h \
        ../../src/convol/xmlloaders/DefaultPrimitiveXmlLoaderT.h \
        ../../src/convol/xmlloaders/primitives/NodeOLDSegmentSFieldLoader.h \
        ../../src/convol/xmlloaders/primitives/NodeOLDTriangleSFieldLoader.h \
# blobtreenode loaders (provisoir en attendant meilleur solution)
#../../src/convol/xmlloaders/blobtreenode/BlobtreeBasketLoader.h \
../../src/convol/xmlloaders/blobtreenode/BlobtreeNodeLoader.h \
../../src/convol/xmlloaders/blobtreenode/BlobtreeRootLoader.h \
../../src/convol/xmlloaders/blobtreenode/TemplatedNodeOperatorLoader.h \
../../src/convol/xmlloaders/blobtreenode/NodeScalarFieldLoader.h \
../../src/convol/xmlloaders/blobtreenode/NodeIntegralSurfaceOperatorLoader.h \
    #
    ../../src/convol/ConvolRequired.h \
    ../../src/convol/functors/operators/BlendDiff.h \
    ../../src/convol/functors/operators/BlendBulge.h \
    ../../src/convol/blobtreenode/BlobtreeCommands.h \
    ../../src/convol/xmlloaders/primitives/NodeOLDPointSFieldLoader.h \
    ../../src/convol/tools/updating/BMeshPolygonizer/BMeshPolygonizerHandler.h \
    ../../src/convol/tools/updating/BMeshPolygonizer/BMBackgroundTasks.h \
    ../../src/convol/tools/polygonizer/BMeshPolygonizer.h
