#-------------------------------------------------
#
# Project Core qmake file
#
#-------------------------------------------------

PROJECT_NAME = core
TEMPLATE = lib
CONFIG += shared

DEFINES += BUILD_CORE
QT -= gui core

# Dependencies
DEPENDS_INTERNAL += ork
DEPENDS_INTERNAL += commons
DEPENDS_EXTERNAL += Boost
#DEPENDS_EXTERNAL += OpenMesh

# Common define and config that are re-used everywhere
lessThan(QT_MAJOR_VERSION, 5) {
    load(../GlobalConfiguration.prf)
} else {
    include(../GlobalConfiguration.prf)
}


#---------------------------------------------------------------
#
#   Source du projet
#
#---------------------------------------------------------------

SOURCES_PATH = $${EXPRESSIVE_HOME_PATH}/src/$${PROJECT_NAME}

SOURCES += \
    ../../src/core/ExpressiveEngine.cpp \
    ../../src/core/ExpressiveTraits.cpp \
    ../../src/core/MeshTraits.cpp \
    ../../src/core/VectorTraits.cpp \
    # commands
    ../../src/core/commands/CommandManager.cpp \
    ../../src/core/commands/ScriptPlayer.cpp \
    # functor
    ../../src/core/functor/FunctorFactory.cpp \
    ../../src/core/functor/EnumFunctorType.cpp \
    ../../src/core/functor/Functor.cpp \
    # geometry
    ../../src/core/geometry/AABBox.cpp \
    ../../src/core/geometry/AdvancedVectorTools.cpp \
    ../../src/core/geometry/BasicTriMeshDefines.cpp \
    ../../src/core/geometry/BasicTriMesh.cpp \
    ../../src/core/geometry/BasicRay.cpp \
    ../../src/core/geometry/BoundingSphere.cpp \
    ../../src/core/geometry/CsteWeightedSegmentWithDensity.cpp \
    ../../src/core/geometry/Frame.cpp \
    ../../src/core/geometry/GeometryPrimitive.cpp \
    ../../src/core/geometry/OptWeightedSegment.cpp \
    ../../src/core/geometry/OptWeightedTriangle.cpp \
    ../../src/core/geometry/PrecisionAxisBoundingBox.cpp \
    ../../src/core/geometry/Primitive.cpp \
    ../../src/core/geometry/PrimitiveFactory.cpp \
    ../../src/core/geometry/PointPrimitive.cpp \
    ../../src/core/geometry/Segment.cpp \
    ../../src/core/geometry/SubdivisionCurve.cpp \
    ../../src/core/geometry/Triangle.cpp \
    ../../src/core/geometry/VectorTools.cpp \
    ../../src/core/geometry/WeightedSegment.cpp \
    ../../src/core/geometry/WeightedSegmentWithDensity.cpp \
    ../../src/core/geometry/WeightedTriangle.cpp \
    ../../src/core/geometry/WeightedPoint.cpp \
    ../../src/core/geometry/WeightedPointWithDensity.cpp \
    ../../src/core/geometry/InterpolationCurve.cpp \
    # graph
    ../../src/core/graph/ARAPDeformer.cpp \
    ../../src/core/graph/Graph.cpp \
    ../../src/core/graph/GraphCommands.cpp \
    ../../src/core/graph/GraphHierarchyMap.cpp \
    ../../src/core/graph/LaplacianDeformer.cpp \
    ../../src/core/graph/Voronoi.cpp \
    ../../src/core/graph/algorithms/GraphAlgorithms.cpp \
    ../../src/core/graph/algorithms/FeatureDetection.cpp \
    ../../src/core/graph/algorithms/SATFiltering.cpp \
    # image
    ../../src/core/image/Skeletonizer.cpp \
    # maths
    ../../src/core/maths/SparseMatrix.cpp \
    # scenegraph
    ../../src/core/scenegraph/Camera.cpp \
    ../../src/core/scenegraph/ControlFrame.cpp \
    ../../src/core/scenegraph/ControlSegment.cpp \
    ../../src/core/scenegraph/Light.cpp \
    ../../src/core/scenegraph/ObjectPrimitive.cpp \
    ../../src/core/scenegraph/OctreeNode.cpp \
    ../../src/core/scenegraph/Overlay.cpp \
    ../../src/core/scenegraph/SceneCommands.cpp \
    ../../src/core/scenegraph/SceneManager.cpp \
    ../../src/core/scenegraph/SceneNode.cpp \
    ../../src/core/scenegraph/SceneNodeFactory.cpp \
    ../../src/core/scenegraph/SceneNodeManipulator.cpp \
    # scenegraph#graph
    ../../src/core/scenegraph/graph/GeometryPrimitiveSceneNode.cpp \
    ../../src/core/scenegraph/graph/GraphElementSceneNode.cpp \
    ../../src/core/scenegraph/graph/GraphHierarchyMapSceneNode.cpp \
    ../../src/core/scenegraph/graph/GraphSceneNode.cpp \
    ../../src/core/scenegraph/graph/GraphSceneNode2D.cpp \
    ../../src/core/scenegraph/graph/PointSceneNode.cpp \
    ../../src/core/scenegraph/graph/SurfaceSceneNode.cpp \
    # utils
    ../../src/core/utils/AssertionHandler.cpp \
    ../../src/core/utils/BasicsToXml.cpp \
    ../../src/core/utils/ColorTools.cpp \
    ../../src/core/utils/ConfigLoader.cpp \
    ../../src/core/utils/InputController.cpp \
    ../../src/core/utils/LeakDetector.cpp \
    ../../src/core/utils/MemoryTools.cpp \
    ../../src/core/utils/PointListLoader.cpp \
    ../../src/core/utils/Serializable.cpp \
    ../../src/core/utils/StringUtils.cpp \
    ../../src/core/utils/XmlToBasics.cpp \
    # xmlloader
    ../../src/core/xmlloaders/GeometryPrimitiveLoader.cpp \
    ../../src/core/xmlloaders/WeightedPointLoader.cpp \
    ../../src/core/xmlloaders/DefaultWeightedGeometryLoader.cpp \
    ../../src/core/xmlloaders/geometry/WeightedSegmentLoader.cpp \
    ../../src/core/xmlloaders/geometry/WeightedTriangleLoader.cpp \
    # workerthread
    ../../src/core/workerthread/WorkerThread.cpp \
    ../../src/core/scenegraph/graph/DrawGraphTask.cpp \
#    ../../src/core/scenegraph/PlaneNode.cpp \
    ../../src/core/graph/algorithms/EDFVisitor.cpp \
    ../../src/core/graph/GraphProfile.cpp \
    ../../src/core/geometry/Polygon2D.cpp \
    ../../src/core/geometry/MeshTools.cpp \
    ../../src/core/geometry/AbstractTriMesh.cpp \
    ../../src/core/scenegraph/TerrainNode.cpp \
    ../../src/core/scenegraph/listeners/SceneManagerListener.cpp \
    ../../src/core/scenegraph/listeners/SceneNodeListener.cpp \
    ../../src/core/scenegraph/SceneNodeCallbacks.cpp \
    ../../src/core/image/ImageRessource.cpp

    #../../src/core/utils/Octree_old.cpp

HEADERS += \
#    ../../src/core/CoreDefaultDef.h \
    ../../src/core/CoreRequired.h \
    ../../src/core/ExpressiveEngine.h \
    ../../src/core/ExpressiveTraits.h \
    ../../src/core/MeshTraits.h \
    ../../src/core/VectorTraits.h \
    ../../src/core/VectorTraits.hpp \
#    ../../src/tinyxml/tinyxml.h \
    # commands
    ../../src/core/commands/CommandManager.h \
    ../../src/core/commands/ScriptPlayer.h \
    # functor
    ../../src/core/functor/EnumFunctorType.h \
    ../../src/core/functor/Functor.h \
    ../../src/core/functor/FunctorFactory.h \
    ../../src/core/functor/FunctorTypeMapT.h \
    ../../src/core/functor/MACRO_FUNCTOR_TYPE.h \
    # geometry
    ../../src/core/geometry/AABBox.h \
    ../../src/core/geometry/AdvancedVectorTools.h \
    ../../src/core/geometry/BasicRay.h \
#    ../../src/core/geometry/BasicTriMesh.hpp \
    ../../src/core/geometry/BasicTriMeshDefines.h \
    ../../src/core/geometry/BasicTriMeshDefines.hpp \
    ../../src/core/geometry/BoundingSphere.h \
    ../../src/core/geometry/BoundingSphere.hpp \
    ../../src/core/geometry/CsteWeightedSegmentWithDensity.h \
    ../../src/core/geometry/OptWeightedSegment.h \
    ../../src/core/geometry/OptWeightedTriangle.h \
    ../../src/core/geometry/PointCloud.h \
    ../../src/core/geometry/PrecisionAxisBoundingBox.h \
    ../../src/core/geometry/Sphere.h \
    ../../src/core/geometry/Segment.h \
    ../../src/core/geometry/SubdivisionCurve.h \
    ../../src/core/geometry/Triangle.h \
    ../../src/core/geometry/VectorTools.h \
    ../../src/core/geometry/WeightedPoint.h \
    ../../src/core/geometry/WeightedPoint.hpp \
    ../../src/core/geometry/WeightedPointWithDensity.h \
    ../../src/core/geometry/WeightedSegment.h \
    ../../src/core/geometry/WeightedSegmentWithDensity.h \
    ../../src/core/geometry/WeightedTriangle.h \
    ../../src/core/geometry/GeometryPrimitive.h \
    ../../src/core/geometry/GeometryTools.h \
    ../../src/core/geometry/PrimitiveFactory.h \
    ../../src/core/geometry/Primitive.h \
    ../../src/core/geometry/PointPrimitive.h \
    ../../src/core/geometry/PointPrimitive.hpp \
    ../../src/core/geometry/Polygon2D.h \
    ../../src/core/geometry/Frame.h \
    ../../src/core/geometry/InterpolationCurve.h \
    # graph
    ../../src/core/graph/ARAPDeformer.h \
    ../../src/core/graph/Graph.hpp \
    ../../src/core/graph/Vertex.hpp \
    ../../src/core/graph/Edge.hpp \
    ../../src/core/graph/GraphCommands.h \
    ../../src/core/graph/GraphCommands.hpp \
    ../../src/core/graph/LaplacianDeformer.h \
    ../../src/core/graph/Voronoi.h \
    ../../src/core/graph/Graph.h \
    ../../src/core/graph/GraphHierarchyMap.h \
    ../../src/core/graph/GraphProfile.h \
    ../../src/core/graph/deformer/BasicDeformer.h \
    ../../src/core/graph/deformer/BasicDeformer.hpp \
    ../../src/core/graph/deformer/SkeletalARAPDeformer.h \
    ../../src/core/graph/deformer/SkeletalARAPDeformer.hpp \
    ../../src/core/graph/deformer/igl/polar_svd.h \
    ../../src/core/graph/deformer/igl/polar_svd.hpp \
    ../../src/core/graph/algorithms/GraphAlgorithms.h \
    ../../src/core/graph/algorithms/EDFVisitor.h \
    ../../src/core/graph/algorithms/SATFiltering.h \
    ../../src/core/graph/algorithms/FeatureDetection.h \
    # image
    ../../src/core/image/ImageT.h \
    ../../src/core/image/Skeletonizer.h \
    ../../src/core/image/DistanceImageTools.h \
    # maths
    ../../src/core/maths/SparseMatrix.h \
    # scenegraph
    ../../src/core/scenegraph/Camera.h \
    ../../src/core/scenegraph/ControlFrame.h \
    ../../src/core/scenegraph/ControlSegment.h \
    ../../src/core/scenegraph/Light.h \
    ../../src/core/scenegraph/ObjectPrimitive.h \
    ../../src/core/scenegraph/OctreeNode.h \
    ../../src/core/scenegraph/Overlay.h \
    ../../src/core/scenegraph/SceneCommands.h \
    ../../src/core/scenegraph/SceneManager.h \
    ../../src/core/scenegraph/SceneNode.h \
    ../../src/core/scenegraph/SceneNodeFactory.h \
    ../../src/core/scenegraph/SceneNodeManipulator.h \
    ../../src/core/scenegraph/listeners/SceneManagerListener.h \
    # scenegraph#graph
    ../../src/core/scenegraph/graph/DrawGraphTask.h \
    ../../src/core/scenegraph/graph/GeometryPrimitiveSceneNode.h \
    ../../src/core/scenegraph/graph/GraphElementSceneNode.h \
    ../../src/core/scenegraph/graph/GraphHierarchyMapSceneNode.h \
    ../../src/core/scenegraph/graph/GraphSceneNode.h \
    ../../src/core/scenegraph/graph/GraphSceneNode2D.h \
    ../../src/core/scenegraph/graph/PointSceneNode.h \
    ../../src/core/scenegraph/graph/SurfaceSceneNode.h \
    # utils
    ../../src/core/utils/AbstractIterator.h \
    ../../src/core/utils/Array2DT.h \
    ../../src/core/utils/Array3DT.h \
    ../../src/core/utils/AssertionHandler.h \
    ../../src/core/utils/BasicsToXml.h \
    ../../src/core/utils/BoundingVolumeHierarchy.h \
    ../../src/core/utils/ColorTools.h \
    ../../src/core/utils/ConfigLoader.h \
    ../../src/core/utils/InputController.h \
    ../../src/core/utils/KeyBindingsList.h \
    ../../src/core/utils/LeakDetector.h \
    ../../src/core/utils/ListenerT.h \
    ../../src/core/utils/MemoryTools.h \
    ../../src/core/utils/PointListLoader.h \
    ../../src/core/utils/Printers.h \
    ../../src/core/utils/Serializable.h \
    ../../src/core/utils/Singleton.h \
    ../../src/core/utils/StringUtils.h \
    ../../src/core/utils/XmlToBasics.h \
    # xmlloader
    ../../src/core/xmlloaders/GeometryPrimitiveLoader.h \
    ../../src/core/xmlloaders/WeightedPointLoader.h \
    ../../src/core/xmlloaders/TraitsXmlQuery.h \
    ../../src/core/xmlloaders/DefaultWeightedGeometryLoader.h \
    ../../src/core/xmlloaders/geometry/WeightedSegmentLoader.h \
    ../../src/core/xmlloaders/geometry/WeightedTriangleLoader.h \
    # workerthread
    ../../src/core/workerthread/WorkerThread.h \
    ../../src/core/workerthread/AbstractBackgroundTask.h \
    ../../src/core/geometry/MeshTools.h \
    ../../src/core/geometry/MeshTools.hpp \
    ../../src/core/geometry/AbstractTriMesh.h \
    ../../src/core/geometry/BasicTriMesh.h \
    ../../src/core/geometry/BasicTriMesh.hpp \
    ../../src/core/scenegraph/TerrainNode.h \
    ../../src/core/utils/SparseContainer.h \
    #../../src/core/utils/Octree_old.h \
    ../../src/core/utils/Octree.h \
    ../../src/core/utils/Octree.hpp \
    ../../src/core/utils/IOctree.h \
    ../../src/core/utils/BVHabstract.h \
    ../../src/core/scenegraph/listeners/SceneNodeListener.h \
    ../../src/core/scenegraph/SceneNodeCallbacks.h \


##OTHER_FILES += \
##    ../../src/core/static_script.c
