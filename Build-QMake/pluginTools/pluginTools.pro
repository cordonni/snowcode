#-------------------------------------------------
#
# Project pluginTools qmake file
#
#-------------------------------------------------

PROJECT_NAME = pluginTools
TEMPLATE = lib
CONFIG += shared


DEFINES += BUILD_PLUGINTOOLS

# Dependencies
DEPENDS_INTERNAL += ork
DEPENDS_INTERNAL += commons
DEPENDS_INTERNAL += qtTools
DEPENDS_INTERNAL += convol
DEPENDS_INTERNAL += core
DEPENDS_EXTERNAL += Boost

# Common define and config that are re-used everywhere
lessThan(QT_MAJOR_VERSION, 5) {
    load(../GlobalConfiguration.prf)
} else {
    include(../GlobalConfiguration.prf)
}

QT += core gui xml opengl
QT += qml quick widgets quickwidgets

windows {
    LIBS += -lopengl32
}

#unix {
#    !macx {
#        QMAKE_LFLAGS +=  -Wl,-rpath,./
#        QMAKE_LFLAGS +=  -Wl,-rpath,/usr/local/lib
#    }
#}

#---------------------------------------------------------------
#
#   Source du projet
#
#---------------------------------------------------------------

SOURCES_PATH = $${EXPRESSIVE_HOME_PATH}/src/$${PROJECT_NAME}

SOURCES =   \
    ../../src/pluginTools/PluginsInterface.cpp \
    ../../src/pluginTools/GLExpressiveEngine.cpp \
    # blobtree management
    ../../src/pluginTools/blobtreemanagement/BlobtreeSelectionView.cpp \
    ../../src/pluginTools/blobtreemanagement/AddChildrenWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/ChangeNodeOperatorWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/BlobtreeModel.cpp \
    ../../src/pluginTools/blobtreemanagement/BlobtreeView.cpp \
    ../../src/pluginTools/blobtreemanagement/BlobtreeViewWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/NodeOperatorInsertionWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/NodeSelectionWidget.cpp \
    # blobtree management # widgets
    ../../src/pluginTools/blobtreemanagement/widgets/BlobtreeNodeWidgetFactory.cpp \
    ../../src/pluginTools/blobtreemanagement/widgets/ChooseNodeOperatorWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/widgets/GradientBasedIntegralSurfaceWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/widgets/NodeOperatorNArityWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/widgets/NodeProceduralDetailParameterWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/widgets/NodeProceduralDetailWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/widgets/FunctorChoiceWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/ControllableGaborSurfaceNoiseWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/ControllableGaborSurfaceNoiseParameterWidget.cpp \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/TestControllableGaborSurfaceNoiseWidgetInterpolation.cpp \
    # convol
    ../../src/pluginTools/convol/ImplicitSurfaceSceneNode.cpp \
    ../../src/pluginTools/convol/ComputePrimitiveDirection.cpp \
    # convol widgets
    ../../src/pluginTools/convolwidgets/tools/DisplayScalarFieldSliceWidget.cpp \
    ../../src/pluginTools/convolwidgets/FunctorWidgetFactory.cpp \
    ../../src/pluginTools/convolwidgets/BasicOperatorWidget.cpp \
    ../../src/pluginTools/convolwidgets/BlendRicciWidget.cpp \
    ../../src/pluginTools/convolwidgets/HomotheticInverseWidget.cpp \
    # gui
#    ../../src/pluginTools/gui/QOrkWidget.cpp \
#    ../../src/pluginTools/gui/QOgreBackgroundWidget.cpp \
    ../../src/pluginTools/gui/MainWindow.cpp \
    ../../src/pluginTools/gui/SetMouseTransformsTask.cpp \
    # gui # qml
    ../../src/pluginTools/gui/qml/QmlCameraHandler.cpp \
    ../../src/pluginTools/gui/qml/QmlConfigHandler.cpp \
    ../../src/pluginTools/gui/qml/QmlGlWidget.cpp \
    ../../src/pluginTools/gui/qml/QmlHandlerBase.cpp \
    ../../src/pluginTools/gui/qml/QmlKeyBindingsManager.cpp \
#    ../../src/pluginTools/gui/qml/GlRenderer.cpp \
    ../../src/pluginTools/gui/qml/QmlTypeRegisterer.cpp \
    ../../src/pluginTools/gui/qml/QmlValueHandler.cpp \
    # input controller
    ../../src/pluginTools/inputcontroller/QInputController.cpp \
    ../../src/pluginTools/inputcontroller/MouseInputController.cpp \
    ../../src/pluginTools/inputcontroller/SimulatedMTInputController.cpp \
    ../../src/pluginTools/inputcontroller/MultiTouchInputController.cpp \
    ../../src/pluginTools/inputcontroller/KeyboardInputController.cpp \
    ../../src/pluginTools/inputcontroller/PenInputController.cpp \
    # scene manipulator
    ../../src/pluginTools/scenemanipulator/CameraManipulator.cpp \
    ../../src/pluginTools/scenemanipulator/DefaultSceneNodeManipulator.cpp \
    ../../src/pluginTools/scenemanipulator/GraphSceneNodeManipulator.cpp \
    ../../src/pluginTools/scenemanipulator/DeadlineSceneManipulatorListener.cpp \
    ../../src/pluginTools/scenemanipulator/SceneManipulator.cpp \
    ../../src/pluginTools/scenemanipulator/SceneManipulatorListener.cpp \
    ../../src/pluginTools/scenemanipulator/SymmetryManipulator.cpp \
    ../../src/pluginTools/inputcontroller/InputControllerViewer.cpp \
    ../../src/pluginTools/inputcontroller/PenOverlay.cpp \
    ../../src/pluginTools/inputcontroller/TouchOverlay.cpp \
    # sketching
    ../../src/pluginTools/sketching/DrawSketchTask.cpp \
    ../../src/pluginTools/sketching/SketchingModeler.cpp \
    # statemachine
    ../../src/pluginTools/statemachine/StateMachine.cpp \
    ../../src/pluginTools/statemachine/ImplicitSurfaceState.cpp \
    # utils
    ../../src/pluginTools/utils/SimpleConvolObject.cpp \
    ../../src/pluginTools/inputcontroller/SelectionOverlay.cpp \
    ../../src/pluginTools/sketching/qml/QmlSketchingHandler.cpp \
    ../../src/pluginTools/mesh/LoadTinyOBJ.cpp \
    ../../src/pluginTools/mesh/tiny_obj_loader.cpp \
    ../../src/pluginTools/gui/qml/QmlSceneManager.cpp \
    ../../src/pluginTools/gui/qml/QmlSceneNodeInfoFactory.cpp \
    ../../src/pluginTools/gui/qml/SceneNodeInfo/QmlImplicitSurfaceSceneNodeInfo.cpp
#    ../../src/pluginTools/scenemanipulator/FirstPersonCameraManipulator.cpp

HEADERS =   \
    ../../src/pluginTools/GLExpressiveEngine.h \
    ../../src/pluginTools/PluginToolsRequired.h \
    ../../src/pluginTools/PluginsInterface.h \
    #blobtreemanagement
    ../../src/pluginTools/blobtreemanagement/AddChildrenWidget.h \
    ../../src/pluginTools/blobtreemanagement/BlobtreeModel.h \
    ../../src/pluginTools/blobtreemanagement/BlobtreeSelectionView.h \
    ../../src/pluginTools/blobtreemanagement/BlobtreeView.h \
    ../../src/pluginTools/blobtreemanagement/BlobtreeViewWidget.h \
    ../../src/pluginTools/blobtreemanagement/ChangeNodeOperatorWidget.h \
    ../../src/pluginTools/blobtreemanagement/NodeOperatorInsertionWidget.h \
    ../../src/pluginTools/blobtreemanagement/NodeSelectionWidget.h \
    #blobtreemanagement#widgets
    ../../src/pluginTools/blobtreemanagement/widgets/AbstractNodeOperatorWidget.h \
    ../../src/pluginTools/blobtreemanagement/widgets/BlobtreeNodeWidgetFactory.h \
    ../../src/pluginTools/blobtreemanagement/widgets/ChooseNodeOperatorWidget.h \
    ../../src/pluginTools/blobtreemanagement/widgets/FunctorChoiceWidget.h \
    ../../src/pluginTools/blobtreemanagement/widgets/GradientBasedIntegralSurfaceWidget.h \
    ../../src/pluginTools/blobtreemanagement/widgets/NodeOperatorNArityWidget.h \
    ../../src/pluginTools/blobtreemanagement/widgets/NodeProceduralDetailParameterWidget.h \
    ../../src/pluginTools/blobtreemanagement/widgets/NodeProceduralDetailWidget.h \
    #blobtreemanagement#widgets#noisewidgets
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/ControllableGaborSurfaceNoiseWidget.h \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/ControllableGaborSurfaceNoiseParameterWidget.h \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/DisplayGaborSurfaceNoiseWidget.h \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/PolynomialDeformationMapWidget.h \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/TestControllableGaborSurfaceNoiseWidgetInterpolation.h \
    #convol
    ../../src/pluginTools/convol/ComputePrimitiveDirection.h \
    ../../src/pluginTools/convol/ImplicitSurfaceSceneNode.h \
    # convolwidgets
    ../../src/pluginTools/convolwidgets/BasicOperatorWidget.h \
    ../../src/pluginTools/convolwidgets/BlendRicciWidget.h \
    ../../src/pluginTools/convolwidgets/FunctorWidgetFactory.h \
    ../../src/pluginTools/convolwidgets/FunctorWidget.h \
    ../../src/pluginTools/convolwidgets/HomotheticInverseWidget.h \
    #convolwidgets#tools
    ../../src/pluginTools/convolwidgets/tools/DisplayScalarFieldSliceWidget.h \
    #gui
    ../../src/pluginTools/gui/MainWindow.h \
    ../../src/pluginTools/gui/SetMouseTransformsTask.h \
    #gui#qml
    ../../src/pluginTools/gui/qml/QmlCameraHandler.h \
    ../../src/pluginTools/gui/qml/QmlConfigHandler.h \
    ../../src/pluginTools/gui/qml/QmlGlWidget.h \
    ../../src/pluginTools/gui/qml/QmlHandlerBase.h \
    ../../src/pluginTools/gui/qml/QmlKeyBindingsManager.h \
    ../../src/pluginTools/gui/qml/QmlTypeRegisterer.h \
    ../../src/pluginTools/gui/qml/QmlValueHandler.h \
    #inputcontroller
    ../../src/pluginTools/inputcontroller/InputControllerViewer.h \
    ../../src/pluginTools/inputcontroller/KeyboardInputController.h \
    ../../src/pluginTools/inputcontroller/MouseInputController.h \
    ../../src/pluginTools/inputcontroller/MultiTouchInputController.h \
    ../../src/pluginTools/inputcontroller/PenInputController.h \
    ../../src/pluginTools/inputcontroller/PenOverlay.h \
    ../../src/pluginTools/inputcontroller/QInputController.h \
    ../../src/pluginTools/inputcontroller/SimulatedMTInputController.h \
    ../../src/pluginTools/inputcontroller/TouchOverlay.h \
    ../../src/pluginTools/inputcontroller/SelectionOverlay.h \
    #scenemanipulator
    ../../src/pluginTools/scenemanipulator/CameraManipulator.h \
    ../../src/pluginTools/scenemanipulator/DeadlineSceneManipulatorListener.h \
    ../../src/pluginTools/scenemanipulator/DefaultSceneNodeManipulator.h \
    ../../src/pluginTools/scenemanipulator/GraphSceneNodeManipulator.h \
    ../../src/pluginTools/scenemanipulator/SceneManipulator.h \
    ../../src/pluginTools/scenemanipulator/SceneManipulatorListener.h \
    ../../src/pluginTools/scenemanipulator/SymmetryManipulator.h \
    #sketching
    ../../src/pluginTools/sketching/DrawSketchTask.h \
    ../../src/pluginTools/sketching/SketchingModeler.h \
    ../../src/pluginTools/sketching/qml/QmlSketchingHandler.h \
    #statemachine
    ../../src/pluginTools/statemachine/ImplicitSurfaceState.h \
    ../../src/pluginTools/statemachine/StateMachine.h \
    #utils
    ../../src/pluginTools/utils/SimpleConvolObject.h \
    #mesh
    ../../src/pluginTools/mesh/tiny_obj_loader.h \
    ../../src/pluginTools/mesh/LoadTinyOBJ.h \
    ../../src/pluginTools/ground/CameraGround.h \
    ../../src/pluginTools/gui/qml/QmlSceneManager.h \
    ../../src/pluginTools/gui/qml/QmlSceneNodeInfoFactory.h \
    ../../src/pluginTools/gui/qml/SceneNodeInfo/QmlImplicitSurfaceSceneNodeInfo.h

FORMS += \
    $${SOURCES_PATH}/gui/mainwindow.ui \
    ../../src/pluginTools/blobtreemanagement/AddChildrenWidget.ui \
    ../../src/pluginTools/blobtreemanagement/BlobtreeViewWidget.ui \
    ../../src/pluginTools/blobtreemanagement/ChangeNodeOperatorWidget.ui \
    ../../src/pluginTools/blobtreemanagement/NodeOperatorInsertionWidget.ui \
    ../../src/pluginTools/blobtreemanagement/NodeSelectionWidget.ui \
    ../../src/pluginTools/blobtreemanagement/widgets/ChooseNodeOperatorWidget.ui\
    ../../src/pluginTools/blobtreemanagement/widgets/GradientBasedIntegralSurfaceWidget.ui \
    ../../src/pluginTools/blobtreemanagement/widgets/NodeOperatorNArityWidget.ui \
    ../../src/pluginTools/blobtreemanagement/widgets/NodeProceduralDetailParameterWidget.ui \
    ../../src/pluginTools/blobtreemanagement/widgets/NodeProceduralDetailWidget.ui \
    ../../src/pluginTools/blobtreemanagement/widgets/FunctorChoiceWidget.ui \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/DisplayGaborSurfaceNoiseWidget.ui \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/ControllableGaborSurfaceNoiseWidget.ui \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/ControllableGaborSurfaceNoiseParameterWidget.ui \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/PolynomialDeformationMapWidget.ui \
    ../../src/pluginTools/blobtreemanagement/widgets/noisewidgets/TestControllableGaborSurfaceNoiseWidgetInterpolation.ui \
    #
    ../../src/pluginTools/convolwidgets/BlendRicciWidget.ui \
    ../../src/pluginTools/convolwidgets/InverseWidget.ui \
    ../../src/pluginTools/convolwidgets/tools/DisplayScalarFieldSliceWidget.ui
