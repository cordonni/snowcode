#-------------------------------------------------
#
# Project created by QtCreator 2015-06-29T15:53:36
#
#-------------------------------------------------
PROJECT_NAME = landscape
TEMPLATE = lib
CONFIG += shared


DEFINES += BUILD_LANDSCAPE

# Dependencies


DEPENDS_INTERNAL += ork
DEPENDS_INTERNAL += commons
DEPENDS_INTERNAL += qtTools
DEPENDS_INTERNAL += convol
DEPENDS_INTERNAL += pluginTools
DEPENDS_INTERNAL += core
DEPENDS_INTERNAL += libtiff

# Common define and config that are re-used everywhere
lessThan(QT_MAJOR_VERSION, 5) {
    load(../GlobalConfiguration.prf)
} else {
    include(../GlobalConfiguration.prf)
}

QT += core gui xml opengl
QT += qml quick


#---------------------------------------------------------------
#
#   Source du projet
#
#---------------------------------------------------------------

QML2_IMPORT_PATH += ../resources/gui/

SOURCES_PATH = $${EXPRESSIVE_HOME_PATH}/src/landscape

HEADERS += \
    ../../src/landscape/Utils/Profile.h \
    ../../src/landscape/Utils/misc.h \
    ../../src/landscape/Utils/ProfileGL.h \
    ../../src/landscape/Grid/TerrainGrid.h \
    ../../src/landscape/Grid/GeoArray.h \
    ../../src/landscape/Grid/GridCommon.h \
    ../../src/landscape/Grid/GeoArray.hpp \
    ../../src/landscape/Grid/TerrainOperations.h \
    ../../src/landscape/Utils/Eval.h \
    ../../src/landscape/Grid/SimulationManager.h \
    ../../src/landscape/Grid/ExpressiveSimulationManager.h \
    ../../src/landscape/Render/TextureDownload.h \
    ../../src/landscape/Grid/ParameterUI.h \
    ../../src/landscape/Grid/LayersUI.h \
    ../../src/landscape/Grid/SunExposure/acceleration_structure.h \
    ../../src/landscape/Grid/SunExposure/sunexposure.h \
    ../../src/landscape/Grid/SunExposure/geom.h \
    ../../src/landscape/Grid/StochasticSimulationBase.h \
    ../../src/landscape/Grid/SimulationData.h \
    ../../src/landscape/Grid/SimulationDataBase.h \
    ../../src/landscape/LandscapeRequired.h \
    ../../src/landscape/User/LandscapeCameraManipulator.h \
    ../../src/landscape/User/LandscapeCameraTask.h

SOURCES += \
    ../../src/landscape/Grid/TerrainGrid.cpp \
    ../../src/landscape/Grid/TerrainOperations.cpp \
    ../../src/landscape/Grid/SimulationManager.cpp \
    ../../src/landscape/Grid/ExpressiveSimulationManager.cpp \
    ../../src/landscape/Render/TextureDownload.cpp \
    ../../src/landscape/Grid/ParameterUI.cpp \
    ../../src/landscape/Grid/LayersUI.cpp \
    ../../src/landscape/Grid/SunExposure/acceleration_structure.cpp \
    ../../src/landscape/Grid/SunExposure/sunexposure.cpp \
    ../../src/landscape/Grid/SunExposure/geom.cpp \
    ../../src/landscape/User/LandscapeCameraManipulator.cpp \
    ../../src/landscape/User/LandscapeCameraTask.cpp



