#----------------------------------------------------------
# This is the main .pro of eroveg lab application
#----------------------------------------------------------

TEMPLATE = subdirs
#CONFIG = subdirs
CONFIG += ordered   # needed to ensure subprojects are built in the right order
BUILD_PLUGINS = true

DEFINES += EXPRESSIVE_MAIN=\\\"expressiveLab\\\"

# load User configuration file if it exists
exists(UserConfiguration.pri) {
    include (UserConfiguration.pri)
}


###########################
# List of subject project #
###########################

SUBDIRS =   commons\
            ork \
            core \
            qtTools \
            convol \
            pluginTools \
            libtiff \
            landscape \
            snow \
            snowBins
