#-------------------------------------------------
#
# Project Core qmake file
#
#-------------------------------------------------

PROJECT_NAME = commons
TEMPLATE = lib
CONFIG += shared

windows {
    LIBS += -lopengl32
}


DEFINES += BUILD_COMMONS

# Common define and config that are re-used everywhere
lessThan(QT_MAJOR_VERSION, 5) {
    load(../GlobalConfiguration.prf)
} else {
    include(../GlobalConfiguration.prf)
}

CONFIG(shared) {
    DEFINES += GLEW_BUILD
    DEFINES += PTW32_BUILD
} else {
    DEFINES += GLEW_STATIC
    DEFINES += PTW32_STATIC_LIB
}

#---------------------------------------------------------------
#
#   Source du projet
#
#---------------------------------------------------------------

SOURCES_PATH = $${EXPRESSIVE_HOME_PATH}/src/$${PROJECT_NAME}

SOURCES += \
#    ../../src/ExpressiveDefaultDef.cpp \
    ../../src/commons/EigenVectorTraits.cpp \
    ../../src/ExpressiveRequired.cpp \
#    ../../src/commons/ExpressiveTraits.cpp \
#    ../../src/MeshTraits.cpp \
#    ../../src/VectorTraits.cpp \
#    ../../src/commons/AABBox.cpp \
#    ../../src/commons/VectorTools.cpp \
    ../../libs/tinyxml/tinyxmlparser.cpp \
    ../../libs/tinyxml/tinyxmlerror.cpp \
    ../../libs/tinyxml/tinyxml.cpp \
    ../../libs/GL/glew.c# \
#    ../../src/OrkVectorTraits.cpp
    

HEADERS += \
    ../../src/commons/EigenVectorTraits.h \
#    ../../src/ExpressiveDefaultDef.h \
    ../../src/ExpressiveRequired.h \
#    ../../src/ExpressiveTraits.h \
#    ../../src/MeshTraits.h \
#    ../../src/VectorTraits.h \
    # commons
#    ../../src/commons/AABBox.h  \
#    ../../src/commons/VectorTools.h \
    ../../src/commons/CommonsRequired.h \
    ../../src/commons/Maths.h \
    ../../libs/tinyxml/tinyxml.h \
    ../../libs/GL/glew.h# \
#    ../../src/OrkVectorTraits.h

#PRECOMPILED_HEADER = ../../src/expressive_pch.h

*-msvc* {
    SOURCES += \
                ../../libs/pthread/pthread_attr_getstackaddr.c \
                ../../libs/pthread/pthread_attr_getstacksize.c \
                ../../libs/pthread/pthread_attr_init.c \
                ../../libs/pthread/pthread_attr_setdetachstate.c \
                ../../libs/pthread/pthread_attr_setinheritsched.c \
                ../../libs/pthread/pthread_attr_setschedparam.c \
                ../../libs/pthread/pthread_attr_setschedpolicy.c \
                ../../libs/pthread/pthread_attr_setscope.c \
                ../../libs/pthread/pthread_attr_setstackaddr.c \
                ../../libs/pthread/pthread_attr_setstacksize.c \
                ../../libs/pthread/pthread_barrier_destroy.c \
                ../../libs/pthread/pthread_barrier_init.c \
                ../../libs/pthread/pthread_barrier_wait.c \
                ../../libs/pthread/pthread_barrierattr_destroy.c \
                ../../libs/pthread/pthread_barrierattr_getpshared.c \
                ../../libs/pthread/pthread_barrierattr_init.c \
                ../../libs/pthread/pthread_barrierattr_setpshared.c \
                ../../libs/pthread/pthread_cancel.c \
                ../../libs/pthread/pthread_cond_destroy.c \
                ../../libs/pthread/pthread_cond_init.c \
                ../../libs/pthread/pthread_cond_signal.c \
                ../../libs/pthread/pthread_cond_wait.c \
                ../../libs/pthread/pthread_condattr_destroy.c \
                ../../libs/pthread/pthread_condattr_getpshared.c \
                ../../libs/pthread/pthread_condattr_init.c \
                ../../libs/pthread/pthread_condattr_setpshared.c \
                ../../libs/pthread/pthread_delay_np.c \
                ../../libs/pthread/pthread_detach.c \
                ../../libs/pthread/pthread_equal.c \
                ../../libs/pthread/pthread_exit.c \
                ../../libs/pthread/pthread_getconcurrency.c \
                ../../libs/pthread/pthread_getschedparam.c \
                ../../libs/pthread/pthread_getspecific.c \
                ../../libs/pthread/pthread_getunique_np.c \
                ../../libs/pthread/pthread_getw32threadhandle_np.c \
                ../../libs/pthread/pthread_join.c \
                ../../libs/pthread/pthread_key_create.c \
                ../../libs/pthread/pthread_key_delete.c \
                ../../libs/pthread/pthread_kill.c \
                ../../libs/pthread/pthread_mutex_consistent.c \
                ../../libs/pthread/pthread_mutex_destroy.c \
                ../../libs/pthread/pthread_mutex_init.c \
                ../../libs/pthread/pthread_mutex_lock.c \
                ../../libs/pthread/pthread_mutex_timedlock.c \
                ../../libs/pthread/pthread_mutex_trylock.c \
                ../../libs/pthread/pthread_mutex_unlock.c \
                ../../libs/pthread/pthread_mutexattr_destroy.c \
                ../../libs/pthread/pthread_mutexattr_getkind_np.c \
                ../../libs/pthread/pthread_mutexattr_getpshared.c \
                ../../libs/pthread/pthread_mutexattr_getrobust.c \
                ../../libs/pthread/pthread_mutexattr_gettype.c \
                ../../libs/pthread/pthread_mutexattr_init.c \
                ../../libs/pthread/pthread_mutexattr_setkind_np.c \
                ../../libs/pthread/pthread_mutexattr_setpshared.c \
                ../../libs/pthread/pthread_mutexattr_setrobust.c \
                ../../libs/pthread/pthread_mutexattr_settype.c \
                ../../libs/pthread/pthread_num_processors_np.c \
                ../../libs/pthread/pthread_once.c \
                ../../libs/pthread/pthread_rwlock_destroy.c \
                ../../libs/pthread/pthread_rwlock_init.c \
                ../../libs/pthread/pthread_rwlock_rdlock.c \
                ../../libs/pthread/pthread_rwlock_timedrdlock.c \
                ../../libs/pthread/pthread_rwlock_timedwrlock.c \
                ../../libs/pthread/pthread_rwlock_tryrdlock.c \
                ../../libs/pthread/pthread_rwlock_trywrlock.c \
                ../../libs/pthread/pthread_rwlock_unlock.c \
                ../../libs/pthread/pthread_rwlock_wrlock.c \
                ../../libs/pthread/pthread_rwlockattr_destroy.c \
                ../../libs/pthread/pthread_rwlockattr_getpshared.c \
                ../../libs/pthread/pthread_rwlockattr_init.c \
                ../../libs/pthread/pthread_rwlockattr_setpshared.c \
                ../../libs/pthread/pthread_self.c \
                ../../libs/pthread/pthread_setcancelstate.c \
                ../../libs/pthread/pthread_setcanceltype.c \
                ../../libs/pthread/pthread_setconcurrency.c \
                ../../libs/pthread/pthread_setschedparam.c \
                ../../libs/pthread/pthread_setspecific.c \
                ../../libs/pthread/pthread_spin_destroy.c \
                ../../libs/pthread/pthread_spin_init.c \
                ../../libs/pthread/pthread_spin_lock.c \
                ../../libs/pthread/pthread_spin_trylock.c \
                ../../libs/pthread/pthread_spin_unlock.c \
                ../../libs/pthread/pthread_testcancel.c \
                ../../libs/pthread/pthread_timechange_handler_np.c \
                ../../libs/pthread/pthread_win32_attach_detach_np.c \
                ../../libs/pthread/ptw32_calloc.c \
                ../../libs/pthread/ptw32_callUserDestroyRoutines.c \
                ../../libs/pthread/ptw32_cond_check_need_init.c \
                ../../libs/pthread/ptw32_getprocessors.c \
                ../../libs/pthread/ptw32_is_attr.c \
                ../../libs/pthread/ptw32_MCS_lock.c \
                ../../libs/pthread/ptw32_mutex_check_need_init.c \
                ../../libs/pthread/ptw32_new.c \
#                ../../libs/pthread/ptw32_OLL_lock.c \
                ../../libs/pthread/ptw32_processInitialize.c \
                ../../libs/pthread/ptw32_processTerminate.c \
                ../../libs/pthread/ptw32_relmillisecs.c \
                ../../libs/pthread/ptw32_reuse.c \
                ../../libs/pthread/ptw32_rwlock_cancelwrwait.c \
                ../../libs/pthread/ptw32_rwlock_check_need_init.c \
                ../../libs/pthread/ptw32_semwait.c \
                ../../libs/pthread/ptw32_spinlock_check_need_init.c \
                ../../libs/pthread/ptw32_threadDestroy.c \
                ../../libs/pthread/ptw32_threadStart.c \
                ../../libs/pthread/ptw32_throw.c \
                ../../libs/pthread/ptw32_timespec.c \
                ../../libs/pthread/ptw32_tkAssocCreate.c \
                ../../libs/pthread/ptw32_tkAssocDestroy.c \
                ../../libs/pthread/rwlock.c \
                ../../libs/pthread/sched.c \
                ../../libs/pthread/sched_get_priority_max.c \
                ../../libs/pthread/sched_get_priority_min.c \
                ../../libs/pthread/sched_getscheduler.c \
                ../../libs/pthread/sched_setscheduler.c \
                ../../libs/pthread/sched_yield.c \
                ../../libs/pthread/sem_close.c \
                ../../libs/pthread/sem_destroy.c \
                ../../libs/pthread/sem_getvalue.c \
                ../../libs/pthread/sem_init.c \
                ../../libs/pthread/sem_open.c \
                ../../libs/pthread/sem_post.c \
                ../../libs/pthread/sem_post_multiple.c \
                ../../libs/pthread/sem_timedwait.c \
                ../../libs/pthread/sem_trywait.c \
                ../../libs/pthread/sem_unlink.c \
                ../../libs/pthread/sem_wait.c \
                ../../libs/pthread/semaphore.c \
                ../../libs/pthread/signal.c \
                ../../libs/pthread/spin.c \
                ../../libs/pthread/sync.c \
                ../../libs/pthread/tsd.c \
                ../../libs/pthread/w32_CancelableWait.c \
                ../../libs/pthread/attr.c \
                ../../libs/pthread/autostatic.c \
                ../../libs/pthread/barrier.c \
                ../../libs/pthread/cancel.c \
                ../../libs/pthread/cleanup.c \
                ../../libs/pthread/condvar.c \
                ../../libs/pthread/create.c \
                ../../libs/pthread/dll.c \
                ../../libs/pthread/errno.c \
                ../../libs/pthread/exit.c \
                ../../libs/pthread/fork.c \
                ../../libs/pthread/global.c \
                ../../libs/pthread/misc.c \
                ../../libs/pthread/mutex.c \
                ../../libs/pthread/nonportable.c \
                ../../libs/pthread/private.c \
                ../../libs/pthread/pthread_attr_destroy.c \
                ../../libs/pthread/pthread_attr_getdetachstate.c \
                ../../libs/pthread/pthread_attr_getinheritsched.c \
                ../../libs/pthread/pthread_attr_getschedparam.c \
                ../../libs/pthread/pthread_attr_getschedpolicy.c \
                ../../libs/pthread/pthread_attr_getscope.c \
                ../../libs/pthread/pthread.c

HEADERS += \
    ../../libs/pthread/pthread.h \
    ../../libs/pthread/sched.h \
    ../../libs/pthread/semaphore.h
}


