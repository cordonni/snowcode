#-------------------------------------------------
#
# Project snow qmake file
#
#-------------------------------------------------

PROJECT_NAME = snowBins
TEMPLATE = app
CONFIG += app


DEPENDS_INTERNAL += commons
DEPENDS_INTERNAL += ork
DEPENDS_INTERNAL += core
DEPENDS_INTERNAL += qtTools
DEPENDS_INTERNAL += convol
DEPENDS_INTERNAL += pluginTools
DEPENDS_INTERNAL += libtiff
DEPENDS_INTERNAL += landscape
DEPENDS_INTERNAL += snow

DEPENDS_EXTERNAL += Boost

TARGET = snowBin

# Dependencies

macx{
#    LIBS += -framework glut
}else{
#    LIBS += -lglut -lGLU
    windows {
        LIBS += -lglu32
    } else {
        LIBS += -lGLU
    }
}

# Common define and config that are re-used everywhere
lessThan(QT_MAJOR_VERSION, 5) {
    load(../GlobalConfiguration.prf)
} else {
    include(../GlobalConfiguration.prf)
}

# define part of qt to be used
QT += core gui xml opengl
QT += qml quick quickwidgets# declarative


SOURCES =   \
     ../../src/expressiveLab/main.cpp \
     ../../src/expressiveLab/ExpressiveApplication.cpp \
    ../../src/expressiveLab/QBoxAssertionHandler.cpp \
    ../../src/expressiveLab/QmlView.cpp \
    ../../src/landscape/snowmain.cpp

HEADERS += \
    ../../src/expressiveLab/ExpressiveApplication.h \
    ../../src/expressiveLab/QBoxAssertionHandler.h \
    ../../src/expressiveLab/main.h \
    ../../src/expressiveLab/QmlView.h




