#-------------------------------------------------
#
# Project Qt-Tools qmake file
#
#-------------------------------------------------

PROJECT_NAME = qtTools

TEMPLATE = lib
CONFIG += shared


DEFINES += BUILD_QTTOOLS

# Dependencies
DEPENDS_INTERNAL += ork
DEPENDS_INTERNAL += commons
DEPENDS_INTERNAL += core
#DEPENDS_EXTERNAL += OpenMesh

# Common define and config that are re-used everywhere
lessThan(QT_MAJOR_VERSION, 5) {
    load(../GlobalConfiguration.prf)
} else {
    include(../GlobalConfiguration.prf)
}

#unix {
#    !macx {
#        QMAKE_LFLAGS +=  -Wl,-rpath,./
#        QMAKE_LFLAGS +=  -Wl,-rpath,/usr/local/lib
#    }
#}


# define part of qt to be used
QT += core gui xml widgets
lessThan(QT_MINOR_VERSION, 3) {
} else {
#    QT += declarative
}


#---------------------------------------------------------------
#
#   Source du projet
#
#---------------------------------------------------------------

SOURCES_PATH = $${EXPRESSIVE_HOME_PATH}/src/$${PROJECT_NAME}

SOURCES =   $${SOURCES_PATH}/QtTools.cpp \
    ../../src/qtTools/gui/TestGraphicsView.cpp

HEADERS =   $${SOURCES_PATH}/QtTools.h \
    ../../src/qtTools/gui/TestGraphicsView.h \
    ../../src/qtTools/gui/QWidgetHandler.h \
    ../../src/qtTools/QtToolsRequired.h



