# ----------------------------------------------------
# Librairie Ogre
#
# Linux & Windows :
#   If the environment variable OGRE_HOME is
#   defined, include path and lib path will be looked
#   up in :
#   $(OGRE_HOME)/include
#   $(OGRE_HOME)/lib
#   Otherwise we look up in the standard local lib directory
#
# MacOSX :
#   If the environment variable OGRE_HOME is
#   defined, framework will be looked up in :
#   $(OGRE_HOME)
#   Otherwise we look up in the standard local
#   framework directory : Library/Framework
# ----------------------------------------------------

unix {
    # WARNING : for now link only with release binaries
    macx {
        # on mac os x , we consider (for now) that Ogre framework is installed in /Library/Frameworks
        QMAKE_CXXFLAGS  += -iframework/Library/Frameworks
        QMAKE_LFLAGS    += -F/Library/Frameworks
        LIBS += -framework Ogre -framework OgreOverlay -framework RenderSystem_GL

        # this is required in order to include .h of OgreOverlay ... (the include do not follow the syntax that is
        # handled with the frameworks : #include "OgrePrerequisites.h" in OgreOverlayPrerequisites.h is not recognized)
        INCLUDEPATH += /Library/Frameworks/Ogre.framework/Headers

        # TODO : @todo : this part should be improved, do a clean install of the dependencies ???
        # Path to include file of Ogre dependencies such as boost
        OGRE_DEPENDENCIES_TEST = $$(OGRE_DEPENDENCIES)
        isEmpty(OGRE_DEPENDENCIES_TEST) {
            error(OGRE_DEPENDENCIES is not set as an environment variable. Please set it to continue)
        }
        INCLUDEPATH += $$(OGRE_DEPENDENCIES)/include

#        QMAKE_CXXFLAGS += -isystem /Library/Frameworks/Ogre.framework
#        QMAKE_CXXFLAGS += --system-header-prefix=OGRE/
#        QMAKE_CXXFLAGS += -isystem-prefix -Xclang Library/Frameworks/

    } else {
        # suppose it's linux
        OGRE_HOME_TEST = $$(OGRE_HOME)
        isEmpty(OGRE_HOME_TEST) {
            INCLUDEPATH *= /usr/local/include \
                           /usr/local/include/OGRE/ \
                           /usr/include/OGRE/ # this is needed for the include of Ogre itself ...

            LIBS += -L/usr/local/lib/ -L/usr/lib/OGRE/ -lOgreMain -lOgreOverlay
        } else {
            INCLUDEPATH *= $$(OGRE_HOME)/include/OGRE/
            LIBS += -L$$(OGRE_HOME)/lib/OGRE -L$$(OGRE_HOME)/lib/ -lOgreMain -lOgreOverlay
        }
    }
}


windows {
    OGRE_HOME_TEST = $$(OGRE_HOME)
    isEmpty(OGRE_HOME_TEST) { # basic windows installation made with dependencies project.
        EXPRESSIVE_DEPENDENCIES_DIRECTORY_TEST = $${EXPRESSIVE_DEPENDENCIES_DIRECTORY}

        isEmpty(EXPRESSIVE_DEPENDENCIES_DIRECTORY_TEST) {
            error(OGRE_HOME and EXPRESSIVE_DEPENDENCIES_DIRECTORY are not set as an environment variable. Please set any of them to continue)
        }
        OGRE_HOME = $${EXPRESSIVE_DEPENDENCIES_DIRECTORY}/

        OGRE_INCLUDE = $${OGRE_HOME}/include/OGRE/
        QMAKE_CXXFLAGS += -isystem $${OGRE_HOME}
        QMAKE_CXXFLAGS += -isystem $$OGRE_INCLUDE/
        QMAKE_CXXFLAGS += -isystem $$OGRE_INCLUDE/Overlay/
        QMAKE_CXXFLAGS += -isystem $$OGRE_INCLUDE/RenderSystems/GL/

        LIBS += -L$${OGRE_HOME}/lib -llibOgreMain -llibOgreOverlay
    } else { # remi's installation.

        QMAKE_CXXFLAGS += -isystem $$(OGRE_HOME)/include/
        QMAKE_CXXFLAGS += -isystem $$(OGRE_HOME)/include/OGRE/

        INCLUDEPATH += $$(OGRE_HOME)/include/
        INCLUDEPATH += $$(OGRE_HOME)/include/OGRE/

        build_pass:CONFIG(debug, debug|release) {
            LIBS += -L$$(OGRE_HOME)/lib/debug/ -L$$(OGRE_HOME)/bin/debug/ -lOgreMain_d -lOgreOverlay_d
        }

        build_pass:CONFIG(release, debug|release) {
            LIBS += -L$$(OGRE_HOME)/lib/Release/ -L$$(OGRE_HOME)/bin/Release/ -lOgreMain -lOgreOverlay
        }
    }
    LIBS += -L$$(OGRE_HOME)/lib

}


