#-------------------------------------------------
#
# Project pluginTools qmake file
#
#-------------------------------------------------

DEFINES += BUILD_ORK

PROJECT_NAME = ork
TEMPLATE = lib
#CONFIG += lib
CONFIG += shared #staticlib

windows {
    LIBS += -lopengl32
}

macx{
    LIBS += -framework glut
}else{
    windows {
        LIBS += -lglu32
    } else {
        LIBS += -lGLU
    }
}

# Dependencies
DEPENDS_INTERNAL += commons
DEPENDS_EXTERNAL += Boost
#DEPENDS_EXTERNAL += OpenGL

# Common define and config that are re-used everywhere
lessThan(QT_MAJOR_VERSION, 5) {
    load(../GlobalConfiguration.prf)
} else {
    include(../GlobalConfiguration.prf)
}

#---------------------------------------------------------------
#
#   Source du projet
#
#---------------------------------------------------------------

SOURCES_PATH = $${EXPRESSIVE_HOME_PATH}/src/$${PROJECT_NAME}

SOURCES =   \
            ../../src/ork/OrkVectorTraits.cpp \
            ../../src/ork/stbi/stb_image.cpp \
            ../../src/ork/core/Logger.cpp \
            ../../src/ork/core/FileLogger.cpp \
            ../../src/ork/core/Object.cpp \
            ../../src/ork/core/Timer.cpp \
            ../../src/ork/math/half.cpp \
            ../../src/ork/render/AttributeBuffer.cpp \
            ../../src/ork/render/Buffer.cpp \
            ../../src/ork/render/CPUBuffer.cpp \
            ../../src/ork/render/FrameBuffer.cpp \
            ../../src/ork/render/GPUBuffer.cpp \
            ../../src/ork/render/MeshBuffers.cpp \
            ../../src/ork/render/Module.cpp \
            ../../src/ork/render/Program.cpp \
            ../../src/ork/render/Query.cpp \
            ../../src/ork/render/RenderBuffer.cpp \
            ../../src/ork/render/Sampler.cpp \
            ../../src/ork/render/Texture1DArray.cpp \
            ../../src/ork/render/Texture1D.cpp \
            ../../src/ork/render/Texture2DArray.cpp \
            ../../src/ork/render/Texture2D.cpp \
            ../../src/ork/render/Texture2DMultisampleArray.cpp \
            ../../src/ork/render/Texture2DMultisample.cpp \
            ../../src/ork/render/Texture3D.cpp \
            ../../src/ork/render/TextureBuffer.cpp \
            ../../src/ork/render/Texture.cpp \
            ../../src/ork/render/TextureCubeArray.cpp \
            ../../src/ork/render/TextureCube.cpp \
            ../../src/ork/render/TextureRectangle.cpp \
            ../../src/ork/render/TransformFeedback.cpp \
            ../../src/ork/render/Types.cpp \
            ../../src/ork/render/Uniform.cpp \
            ../../src/ork/render/Value.cpp \
            ../../src/ork/resource/CompiledResourceLoader.cpp \
            ../../src/ork/resource/ResourceCompiler.cpp \
            ../../src/ork/resource/Resource.cpp \
            ../../src/ork/resource/ResourceDescriptor.cpp \
            ../../src/ork/resource/ResourceFactory.cpp \
            ../../src/ork/resource/ResourceLoader.cpp \
            ../../src/ork/resource/ResourceManager.cpp \
            ../../src/ork/resource/XMLResourceLoader.cpp \
            ../../src/ork/scenegraph/AbstractTask.cpp \
            ../../src/ork/scenegraph/CallMethodTask.cpp \
            ../../src/ork/scenegraph/DrawMeshTask.cpp \
            ../../src/ork/scenegraph/LoopTask.cpp \
            ../../src/ork/scenegraph/Method.cpp \
            ../../src/ork/scenegraph/SceneManager.cpp \
            ../../src/ork/scenegraph/SceneNode.cpp \
            ../../src/ork/scenegraph/SequenceTask.cpp \
            ../../src/ork/scenegraph/SetProgramTask.cpp \
            ../../src/ork/scenegraph/SetStateTask.cpp \
            ../../src/ork/scenegraph/SetTargetTask.cpp \
            ../../src/ork/scenegraph/SetTransformsTask.cpp \
            ../../src/ork/scenegraph/ShowInfoTask.cpp \
            ../../src/ork/scenegraph/ShowLogTask.cpp \
            ../../src/ork/taskgraph/MultithreadScheduler.cpp \
            ../../src/ork/taskgraph/Scheduler.cpp \
            ../../src/ork/taskgraph/Task.cpp \
            ../../src/ork/taskgraph/TaskFactory.cpp \
            ../../src/ork/taskgraph/TaskGraph.cpp \
            ../../src/ork/util/Font.cpp


HEADERS =   \
            ../../src/ork/OrkVectorTraits.h \
            ../../src/ork/stbi/stb_image.h \
            ../../src/ork/OrkRequired.h \
            ../../src/ork/core/Atomic.h \
            ../../src/ork/core/Iterator.h \
            ../../src/ork/core/Logger.h \
            ../../src/ork/core/FileLogger.h \
            ../../src/ork/core/Object.h \
            ../../src/ork/core/Timer.h \
            ../../src/ork/math/box2.h \
            ../../src/ork/math/box3.h \
            ../../src/ork/math/half.h \
            ../../src/ork/math/mat2.h \
            ../../src/ork/math/mat3.h \
            ../../src/ork/math/mat4.h \
            ../../src/ork/math/quat.h \
            ../../src/ork/math/vec2.h \
            ../../src/ork/math/vec3.h \
            ../../src/ork/math/vec4.h \
            ../../src/ork/render/AttributeBuffer.h \
            ../../src/ork/render/Buffer.h \
            ../../src/ork/render/CPUBuffer.h \
            ../../src/ork/render/FrameBuffer.h \
            ../../src/ork/render/GPUBuffer.h \
            ../../src/ork/render/MeshBuffers.h \
            ../../src/ork/render/Mesh.h \
            ../../src/ork/render/Module.h \
            ../../src/ork/render/Program.h \
            ../../src/ork/render/Query.h \
            ../../src/ork/render/RenderBuffer.h \
            ../../src/ork/render/Sampler.h \
            ../../src/ork/render/Texture1DArray.h \
            ../../src/ork/render/Texture1D.h \
            ../../src/ork/render/Texture2DArray.h \
            ../../src/ork/render/Texture2D.h \
            ../../src/ork/render/Texture2DMultisampleArray.h \
            ../../src/ork/render/Texture2DMultisample.h \
            ../../src/ork/render/Texture3D.h \
            ../../src/ork/render/TextureBuffer.h \
            ../../src/ork/render/Texture.h \
            ../../src/ork/render/TextureCubeArray.h \
            ../../src/ork/render/TextureCube.h \
            ../../src/ork/render/TextureRectangle.h \
            ../../src/ork/render/TransformFeedback.h \
            ../../src/ork/render/Types.h \
            ../../src/ork/render/Uniform.h \
            ../../src/ork/render/Value.h \
            ../../src/ork/resource/CompiledResourceLoader.h \
            ../../src/ork/resource/ResourceCompiler.h \
            ../../src/ork/resource/Resource.h \
            ../../src/ork/resource/ResourceDescriptor.h \
            ../../src/ork/resource/ResourceFactory.h \
            ../../src/ork/resource/ResourceLoader.h \
            ../../src/ork/resource/ResourceManager.h \
            ../../src/ork/resource/ResourceTemplate.h \
            ../../src/ork/resource/XMLResourceLoader.h \
            ../../src/ork/scenegraph/AbstractTask.h \
            ../../src/ork/scenegraph/CallMethodTask.h \
            ../../src/ork/scenegraph/DrawMeshTask.h \
            ../../src/ork/scenegraph/LoopTask.h \
            ../../src/ork/scenegraph/Method.h \
            ../../src/ork/scenegraph/SceneManager.h \
            ../../src/ork/scenegraph/SceneNode.h \
            ../../src/ork/scenegraph/SequenceTask.h \
            ../../src/ork/scenegraph/SetProgramTask.h \
            ../../src/ork/scenegraph/SetStateTask.h \
            ../../src/ork/scenegraph/SetTargetTask.h \
            ../../src/ork/scenegraph/SetTransformsTask.h \
            ../../src/ork/scenegraph/ShowInfoTask.h \
            ../../src/ork/scenegraph/ShowLogTask.h \
            ../../src/ork/taskgraph/MultithreadScheduler.h \
            ../../src/ork/taskgraph/Scheduler.h \
            ../../src/ork/taskgraph/TaskFactory.h \
            ../../src/ork/taskgraph/TaskGraph.h \
            ../../src/ork/taskgraph/Task.h \
            ../../src/ork/util/Font.h


