#-------------------------------------------------
#
# Project created by QtCreator 2015-06-29T15:53:36
#
#-------------------------------------------------
PROJECT_NAME = snow
TEMPLATE = lib
CONFIG += shared


DEFINES += BUILD_SNOW

# Dependencies

DEPENDS_INTERNAL += landscape

# Common define and config that are re-used everywhere
lessThan(QT_MAJOR_VERSION, 5) {
    load(../GlobalConfiguration.prf)
} else {
    include(../GlobalConfiguration.prf)
}

QT += core gui xml opengl
QT += qml quick

QMAKE_CXXFLAGS += -fPIC


#---------------------------------------------------------------
#
#   Source du projet
#
#---------------------------------------------------------------

QML2_IMPORT_PATH += ../resources/gui/

SOURCES_PATH = $${EXPRESSIVE_HOME_PATH}/src/landscape

HEADERS += \
    ../../src/landscape/Grid/CUDA/StochasticSimulationCUDA.h \
    ../../src/landscape/Grid/Snow/SnowRequired.h \
    ../../src/landscape/Grid/Snow/SnowData.h \
    ../../src/landscape/Grid/Snow/SnowDataIO.h \
    ../../src/landscape/Grid/Snow/RegisterSnow.h \
    ../../src/landscape/Grid/CUDA/CudaMemory.h \
    ../../src/landscape/Grid/CUDA/CudaRand.h \
    ../../src/landscape/Grid/CUDA/CudaGridUtils.h

SOURCES += \
    ../../src/landscape/Grid/CUDA/StochasticSimulationCUDA.cpp \
    ../../src/landscape/Grid/Snow/SnowData.cpp \
    ../../src/landscape/Grid/Snow/SnowDataIO.cpp \
    ../../src/landscape/Grid/Snow/RegisterSnow.cpp


OTHER_FILES += \
    ../../src/landscape/Grid/CUDA/CudaMemory.cu \
    ../../src/landscape/Grid/CUDA/CudaRand.cu  \
    ../../src/landscape/Grid/CUDA/CudaGridUtils.cu \
    ../../src/landscape/Grid/Snow/SnowRenderFunc.cu \
    ../../src/landscape/Grid/Snow/SnowEventPrecip.cu  \
    ../../src/landscape/Grid/Snow/SnowEventMelt.cu  \
    ../../src/landscape/Grid/Snow/SnowEventDiffusion.cu \
    ../../src/landscape/Grid/Snow/SnowEventIllumination.cu \
    ../../src/landscape/Grid/Snow/SnowEventWind.cu \
    ../../src/landscape/Grid/Snow/SnowEventWindPressure.cu \
    ../../src/landscape/Grid/Snow/SnowEventAvPropag.cu \
    ../../src/landscape/Grid/Snow/SnowEventAvFall.cu \
    ../../src/landscape/Grid/Snow/SnowEventAvFall2.cu \
    ../../src/landscape/Grid/Snow/SnowEventWindProcedural.cu \
    ../../src/landscape/Grid/Snow/SnowEventSpawnSkier.cu \
    ../../src/landscape/Grid/Snow/SnowEventMoveSkier.cu \
    ../../src/landscape/Grid/Snow/SnowEventPaths.cu \
    ../../src/landscape/Grid/Snow/SnowEventHideTracks.cu




#---------------------------------------------------------------
#
#   CUDA settings
#
#---------------------------------------------------------------

CUDA_SOURCES += \
    ../../src/landscape/Grid/CUDA/CudaMemory.cu \
    ../../src/landscape/Grid/CUDA/CudaRand.cu \
    ../../src/landscape/Grid/CUDA/CudaGridUtils.cu \
    ../../src/landscape/Grid/Snow/SnowRenderFunc.cu \
    ../../src/landscape/Grid/Snow/SnowEventPrecip.cu  \
    ../../src/landscape/Grid/Snow/SnowEventMelt.cu \
    ../../src/landscape/Grid/Snow/SnowEventDiffusion.cu \
    ../../src/landscape/Grid/Snow/SnowEventIllumination.cu \
    ../../src/landscape/Grid/Snow/SnowEventWind.cu \
    ../../src/landscape/Grid/Snow/SnowEventAvPropag.cu \
    ../../src/landscape/Grid/Snow/SnowEventAvFall.cu \
    ../../src/landscape/Grid/Snow/SnowEventWindProcedural.cu \
    ../../src/landscape/Grid/Snow/SnowEventSpawnSkier.cu \
    ../../src/landscape/Grid/Snow/SnowEventMoveSkier.cu \
    ../../src/landscape/Grid/Snow/SnowEventPaths.cu \
    ../../src/landscape/Grid/Snow/SnowEventHideTracks.cu

SYSTEM_NAME = x64         # Depending on your system either 'Win32', 'x64', or 'Win64'
SYSTEM_TYPE = 64            # '32' or '64', depending on your system
CUDA_ARCH = sm_30           # Type of CUDA architecture, for example 'compute_10', 'compute_11', 'sm_10'
NVCC_OPTIONS = --compiler-options -fno-strict-aliasing -use_fast_math --ptxas-options=-v --std c++11

# include paths
INCLUDEPATH += $$CUDA_DIR/include $$CUDA_SAMPLES/common/inc

# Add the necessary libraries
LIBS += -lcuda -lcudart

#nvcc don't understand -f
LIBS_BK = $$LIBS
contains(LIBS, -fopenmp)
{
    LIBS -= -fopenmp
}

# The following makes sure all path names (which often include spaces) are put between quotation marks
CUDA_INC = $$join(INCLUDEPATH,'" -I"','-I"','"')

*-msvc*{
    MSVCRT_LINK_FLAG_DEBUG = "/MDd"
    MSVCRT_LINK_FLAG_RELEASE = "/MD"
    # library directories
    QMAKE_LIBDIR += $$CUDA_DIR/lib/$$SYSTEM_NAME

} else {
    !macx{
        # library directories
        QMAKE_LIBDIR += $$CUDA_DIR/lib64
    }
}


# Configuration of the Cuda compiler
CONFIG(debug, debug|release) {
    # Debug mode
    cuda_d.input = CUDA_SOURCES
    cuda_d.output = cuda/${QMAKE_FILE_BASE}_cuda.o
*-msvc*{
    cuda_d.commands = $$CUDA_DIR/bin/nvcc.exe -D_DEBUG $$NVCC_OPTIONS $$CUDA_INC $$LIBS \
                      --machine $$SYSTEM_TYPE -arch=$$CUDA_ARCH \
                      --compile -cudart static -g \
                      -Xcompiler $$MSVCRT_LINK_FLAG_DEBUG -Xcompiler "/FS" \
                      -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME}
}
unix {
    cuda_d.commands = $$CUDA_DIR/bin/nvcc -D_DEBUG -m64 -g -arch=$$CUDA_ARCH -c $$NVCC_OPTIONS -shared -Xcompiler -fPIC \
                    $$CUDA_INC $$LIBS  ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT} \
                    2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2
}
    cuda_d.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda_d
}
else {
    # Release mode
    cuda.input = CUDA_SOURCES
    cuda.output = cuda/${QMAKE_FILE_BASE}_cuda.o
*-msvc*{
    cuda.commands = $$CUDA_DIR/bin/nvcc.exe $$NVCC_OPTIONS $$CUDA_INC $$LIBS \
                    --machine $$SYSTEM_TYPE -arch=$$CUDA_ARCH \
                      --compile -cudart static -O3 \
                      -Xcompiler $$MSVCRT_LINK_FLAG_RELEASE -Xcompiler "/FS"  \
                      -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME}
}
unix {
    cuda.commands = $$CUDA_DIR/bin/nvcc -m64 -O3 -arch=$$CUDA_ARCH -c $$NVCC_OPTIONS -shared -Xcompiler -fPIC \
                    $$CUDA_INC $$LIBS  ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT} \
                    2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2
}
    cuda.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda
}

LIBS = $$LIBS_BK

DISTFILES += \
    ../../src/landscape/Grid/Snow/SnowEventSpawnSkier.cu \
    ../../src/landscape/Grid/Snow/SnowEventSpawnSkier.cu \
    ../../src/landscape/Grid/Snow/SnowEventSpawnSkier.cu \
    ../../src/landscape/Grid/Snow/SnowEventMoveSkier.cu \
    ../../src/landscape/Grid/Snow/SnowEventPaths.cu \
    ../../src/landscape/Grid/Snow/SnowEventHideTracks.cu

