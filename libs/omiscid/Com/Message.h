/**
 * @file Message.h
 * @ingroup Com
 * @brief Moved to System, only for portability
 */

#ifndef __COM_MESSAGE_H__
#define __COM_MESSAGE_H__

#include <Com/ConfigCom.h>
#include <System/Message.h>

#endif // __COM_MESSAGE_H__

