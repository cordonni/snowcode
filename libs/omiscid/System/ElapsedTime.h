/**
 * @file System/ElapsedTime.h
 * @ingroup System
 * @brief Definition of ElapsedTime class
 */

#ifndef __ELAPSED_TIME_H__
#define __ELAPSED_TIME_H__

#include <System/ConfigSystem.h>
#include <System/Portage.h>

namespace Omiscid {

/**
 * @class ElapsedTime ElapsedTime.cpp System/ElapsedTime.h
 * @brief Enable to compute the elapse time between two calls in milliseconds.
 *
 * @author Dominique Vaufreydaz, Grenoble Alpes University, Inria
 */
class ElapsedTime
{
public:
	/** @brief Constructor */
	ElapsedTime();

	/** @brief Virtual destructor */
	virtual ~ElapsedTime();

	/** @brief reset the start time */
	void Reset();

	/** @brief Get the elapsed time in ms since construction or last reset.
	 * @param DoReset says if we need to reset the object with a new start time
	 * @return the Elapsed Time in ms since the
	 */
	unsigned int Get(bool DoReset = false);

	/** @brief Get the elapsed time in second since construction or last reset.
	* @param DoReset says if we need to reset the object with a new start time
	* @return the Elapsed Time in second (and microsecond) since the
	*/
	float GetInSeconds(bool DoReset = false );

	/** @brief Get the current time in ms
	 */
	static unsigned int GetCurrentTime();

protected:
	unsigned int StartTime; /*!< A value to store start time in millisecond  */
};

/**
 * @class ElapsedTime ElapsedTime.cpp System/ElapsedTime.h
 * @brief Enable to compute the elapse time between two calls in milliseconds.
 *
 * @author Dominique Vaufreydaz, Grenoble Alpes University, Inria
 */
class PerfElapsedTime
{
public:
	/** @brief Constructor */
	PerfElapsedTime();

	/** @brief Virtual destructor */
	virtual ~PerfElapsedTime();

	/** @brief reset the start time */
	void Reset();

	/** @brief Get the elapsed time in micro second since construction or last reset.
	 * @param DoReset says if we need to reset the object with a new start time
	 * @return the Elapsed Time in ms since the
	 */
	unsigned long long Get(bool DoReset = false);


	/** @brief Get the elapsed time in seconds since construction or last reset.
	 * @param DoReset says if we need to reset the object with a new start time
	 * @return the Elapsed Time in seconds (and microsecond) since the
	 */
	double GetInSeconds(bool DoReset = false);

	/** @brief Get the current time in micro second
	 */
	static unsigned long long GetCurrentTime();

protected:
#ifdef OMISCID_ON_WINDOWS
	LARGE_INTEGER StartTime; /*!< A value to store start time in millisecond  */
	LARGE_INTEGER Frequency; /*!< Frequency of the performance timer */
#else
	unsigned long long StartTime;
#endif
};

} // namespace Omiscid

#endif // __ELAPSED_TIME_H__

