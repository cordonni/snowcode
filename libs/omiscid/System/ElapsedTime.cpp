// ElapsedTime.cpp: implementation of the ElapsedTime class.
//
//////////////////////////////////////////////////////////////////////


#include <System/ElapsedTime.h>

using namespace Omiscid;

/** @brief Constructor */
ElapsedTime::ElapsedTime()
{
	// init StartTime
	Reset();
}

/** @brief Destructor */
ElapsedTime::~ElapsedTime()
{
}

/** @brief reset start time */
void ElapsedTime::Reset()
{
	// Compute the time in ms since epoque
	StartTime = GetCurrentTime();
}

/** @brief Get the elapsed time in ms since construction or last reset.
 * @param DoReset says if we need to reset the object with a new start time
 * @return the Elapsed Time in ms since the
 */
unsigned int ElapsedTime::Get(bool DoReset /* = false */)
{
	unsigned int   CurrentTime;
	unsigned int   CurrentElapsedTime;

	// Compute the time in ms since epoque
	CurrentTime = GetCurrentTime();

	CurrentElapsedTime = CurrentTime - StartTime;

	if ( DoReset == true )
	{
		// Reset the Time and return elapsed time
		StartTime = CurrentTime;
	}

	return CurrentElapsedTime;
}

/** @brief Get the elapsed time in second since construction or last reset.
	* @param DoReset says if we need to reset the object with a new start time
	* @return the Elapsed Time in second (and microsecond) since the
	*/
float ElapsedTime::GetInSeconds(bool DoReset /* = false */ )
{
	unsigned int ElapsedTime = Get(DoReset);
	return (float)ElapsedTime/1000.0f;
}


/** @brief Get the current time in ms
 */
unsigned int ElapsedTime::GetCurrentTime()
{
	struct timeval CurrentTimeOfDay;
	unsigned int   CurrentTime;

	// retrieve the current time
	gettimeofday(&CurrentTimeOfDay, NULL);

	// Compute the time in ms since epoque
	CurrentTime = CurrentTimeOfDay.tv_sec * 1000 + CurrentTimeOfDay.tv_usec/1000;

	return CurrentTime;
}

/** @brief Constructor */
PerfElapsedTime::PerfElapsedTime()
{
#ifdef OMISCID_ON_WINDOWS
	// Get performance timer frequency
	QueryPerformanceFrequency(&Frequency);
#endif

	// init StartTime
	Reset();
}

/** @brief Destructor */
PerfElapsedTime::~PerfElapsedTime()
{
}

/** @brief reset start time */
void PerfElapsedTime::Reset()
{
#ifdef OMISCID_ON_WINDOWS
	QueryPerformanceCounter(&StartTime);
#else
	StartTime = GetCurrentTime();
#endif
}

/** @brief Get the elapsed time in ms since construction or last reset.
 * @param DoReset says if we need to reset the object with a new start time
 * @return the Elapsed Time in micro second since the
 */
unsigned long long PerfElapsedTime::Get(bool DoReset /* = false */)
{
#ifdef OMISCID_ON_WINDOWS
	LARGE_INTEGER CurrentElapsedTime, CurrentTime;
	
	QueryPerformanceCounter(&CurrentTime);
	CurrentElapsedTime.QuadPart = CurrentTime.QuadPart - StartTime.QuadPart;
	CurrentElapsedTime.QuadPart *= 1000000;
	CurrentElapsedTime.QuadPart /= Frequency.QuadPart;

	if ( DoReset == true )
	{
		// Reset the Time and return elapsed time
		StartTime = CurrentTime;
	}

	return (unsigned long long)(CurrentElapsedTime.QuadPart);
#else
	unsigned long long CurrentTime;
	unsigned long long CurrentElapsedTime;

	// Compute the time in ms since epoque
	CurrentTime = GetCurrentTime();

	CurrentElapsedTime = CurrentTime - StartTime;

	if ( DoReset == true )
	{
		// Reset the Time and return elapsed time
		StartTime = CurrentTime;
	}

	return CurrentElapsedTime;
#endif
}


/** @brief Get the current time in ms
 */
unsigned long long PerfElapsedTime::GetCurrentTime()
{
#ifdef OMISCID_ON_WINDOWS
	LARGE_INTEGER CurrentTime;
	LARGE_INTEGER Frequency;

	// Get performance timer frequency
	QueryPerformanceFrequency(&Frequency);
	// Get the time	
	QueryPerformanceCounter(&CurrentTime);
	CurrentTime.QuadPart *= 1000000;
	CurrentTime.QuadPart /= Frequency.QuadPart;

	return (unsigned long long)(CurrentTime.QuadPart);
#else
	struct timeval CurrentTimeOfDay;
	unsigned int   CurrentTime;

	// retrieve the current time
	gettimeofday(&CurrentTimeOfDay, NULL);

	// Compute the time in ms since epoque
	CurrentTime = CurrentTimeOfDay.tv_sec * 1000 + CurrentTimeOfDay.tv_usec/1000;

	return CurrentTime;
#endif
}

/** @brief Get the elapsed time in second since construction or last reset.
	* @param DoReset says if we need to reset the object with a new start time
	* @return the Elapsed Time in second (and microsecond) since the
	*/
double PerfElapsedTime::GetInSeconds(bool DoReset /* = false */ )
{
	unsigned long long ElapsedTime = Get(DoReset);
	return (double)(ElapsedTime)/1000000.0;
}




