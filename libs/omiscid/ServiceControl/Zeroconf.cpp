
#include <ServiceControl/Zeroconf.h>

#ifdef OMISCID_USE_AVAHI

using namespace Omiscid;

Mutex ZeroConfSubsystem::AvahiLocker;

#endif // OMISCID_USE_AVAHI