#ifndef __ZEROCONF_H__
#define __ZEROCONF_H__

#ifdef OMISCID_USE_MDNS
	#include <dns_sd.h>
#else
#ifdef OMISCID_USE_AVAHI
	// include specific stuff fro browsing
	#include <avahi-client/client.h>
	#include <avahi-client/lookup.h>

	#include <avahi-common/simple-watch.h>
	#include <avahi-common/malloc.h>
	#include <avahi-common/error.h>
#endif
#endif

#include <System/LockManagement.h>
#include <System/Mutex.h>

namespace Omiscid {

class ZeroConfSubsystem
{
public:
	ZeroConfSubsystem() {}

	virtual ~ZeroConfSubsystem() {}

#ifdef OMISCID_USE_AVAHI

public:
	static Omiscid::Mutex AvahiLocker;

#endif // OMISCID_USE_AVAHI
};

} // namespace Omiscid

#endif // __ZEROCONF_H__
