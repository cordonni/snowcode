#pragma once
#ifndef LIBNOISE_REQUIRED_H_
#define LIBNOISE_REQUIRED_H_

#if !defined(BUILD_STATIC) && (defined ( _WIN32 ) || defined( _WIN64 ))
#   if defined( BUILD_LIBNOISE )
#        define LIBNOISE_API __declspec( dllexport )
#    else
#        define LIBNOISE_API __declspec( dllimport )
#    endif
#else //Probably Linux
#   ifdef __APPLE__
#       define LIBNOISE_API
#   else // Linux probably
#        define LIBNOISE_API
#   endif
#endif


#endif // LIBNOISE_REQUIRED_H_
