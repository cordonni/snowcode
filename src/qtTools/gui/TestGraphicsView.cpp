#include <qtTools/gui/TestGraphicsView.h>

#include <ork/core/Logger.h>
#include <qtTools/QtTools.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QEvent>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

using namespace ork;

namespace expressive
{

namespace qttools
{

TestGraphicsView::TestGraphicsView(QWidget *parent) : QWidget(parent)
{
    Logger::LOG_DEBUG("GUI", "%s", QT_VERSION_STR);
    setAttribute(Qt::WA_AcceptTouchEvents);
    setAttribute(Qt::WA_StaticContents);
    //viewport()->setAttribute(Qt::WA_AcceptTouchEvents);
}

bool TestGraphicsView::event(QEvent *event)
{
    Logger::LOG_DEBUG("GUI", "EVENT : %s", QEventToString(event).c_str());
    switch(event->type()) {
    case QEvent::TouchBegin:
        Logger::LOG_DEBUG("GUI", "TouchBegin");
        break;
    case QEvent::TouchUpdate:
        Logger::LOG_DEBUG("GUI", "TouchUpdate");
        break;
    case QEvent::TouchEnd:
        Logger::LOG_DEBUG("GUI", "TouchEnd");
        break;
    default:
        break;
    }
    return true;
}

} // namespace qttools

} // namespace expressive
