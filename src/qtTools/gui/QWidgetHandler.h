/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QWIDGETHANDLER_H
#define QWIDGETHANDLER_H

#include <core/utils/Serializable.h>
#include <qtTools/QtToolsRequired.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include <QWidget>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

namespace qttools
{

/**
 * This class allows to load QWidgets with our loadResource mechanisms
 * without using smart pointers on them. Doing so would make them to be deleted
 * twice (once by us, and once by Qt).
 */
class QWidgetHandler : public core::Serializable
{
public:
    QWidgetHandler() : Serializable("QWidgetHandler"){}

    virtual ~QWidgetHandler() {}

    inline QWidget *getWidget() const { return widget; }

    virtual void init(QWidget *w) { this->widget = w;}

    virtual void swap(std::shared_ptr<QWidgetHandler> t)
    {
        std::swap(widget, t->widget);
    }

protected:
    QWidget *widget;
};

} // namespace qttools

} // namespace expressive

#endif // QWIDGETHANDLER_H
