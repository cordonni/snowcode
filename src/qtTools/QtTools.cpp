#include <ork/core/Logger.h>
#include <core/utils/StringUtils.h>
#include <qtTools/QtTools.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include <QEvent>
#include <QColor>
#include <QSize>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

using namespace expressive::core;
using namespace std;

namespace expressive
{

namespace qttools
{

string QEventToString(QEvent *event)
{
    string name;
    switch(event->type()) {
        case QEvent::MouseButtonPress:
            name = "QEvent::MouseButtonPress";
            break;
        case QEvent::MouseButtonRelease:
            name = "QEvent::MouseButtonRelease";
            break;
        case QEvent::MouseButtonDblClick:
            name = "QEvent::MouseButtonDblClick";
            break;
        case QEvent::MouseMove:
            name = "QEvent::MouseMove";
            break;
        case QEvent::Enter:
            name = "QEvent::Enter";
            break;
        case QEvent::Paint:
            name = "QEvent::Paint";
            break;
        case QEvent::Move:
            name = "QEvent::Move";
            break;
        case QEvent::Resize:
            name = "QEvent::Resize";
            break;
        case QEvent::Show:
            name = "QEvent::Show";
            break;
        case QEvent::Hide:
            name = "QEvent::Hide";
            break;
        case QEvent::WindowActivate:
            name = "QEvent::WindowActivate";
            break;
        case QEvent::WindowDeactivate:
            name = "QEvent::WindowDeactivate";
            break;
        case QEvent::ShowToParent:
            name = "QEvent::ShowToParent";
            break;
        case QEvent::PolishRequest:
            name = "QEvent::PolishRequest";
            break;
        case QEvent::Polish:
            name = "QEvent::Polish";
            break;
        default:
            name = string_format("%d EVENT", event->type());
            break;
    }

    return name;
}

QColor ColorToQt(const Color &c)
{
    return QColor(int(255.f * c[0]), int(255.f * c[1]) , int(255.f * c[2]), int(255.f * c[3]));
}

Color QtToColor(const QColor &c)
{
    return Color(c.red(), c.green(), c.blue(), c.alpha());
}

Vector2i QtToVector(const QSize &s)
{
    return Vector2i(s.width(), s.height());
}

QSize VectorToQt(const Vector2i &s)
{
    return QSize(s[0], s[1]);
}

Vector2d QtToVector(const QVector2D &v)
{
    return Vector2d(v.x(), v.y());
}

Vector3d QtToVector(const QVector3D &v)
{
    return Vector3d(v.x(), v.y(), v.z());
}

Vector4d QtToVector(const QVector4D &v)
{
    return Vector4d(v.x(), v.y(), v.z(), v.w());
}

QVector4D VectorToQt(const Vector4d &v)
{
    return QVector4D((float) v[0], (float) v[1], (float) v[2], (float) v[3]);
}

QVector3D VectorToQt(const Vector3d &v)
{
    return QVector3D((float) v[0], (float) v[1], (float) v[2]);
}

QVector2D VectorToQt(const Vector2d &v)
{
    return QVector2D((float) v[0], (float) v[1]);
}

}

}

