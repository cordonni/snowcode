/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <string> 

#include <core/CoreRequired.h>
#include <qtTools/QtToolsRequired.h>

#include "core/VectorTraits.h"

class QEvent;
class QColor;
class QSize;
class QVector2D;
class QVector3D;
class QVector4D;

namespace expressive 
{

namespace qttools
{

std::string QTTOOLS_API QEventToString(QEvent *event);
QColor QTTOOLS_API ColorToQt(const Color &c);
Color QTTOOLS_API QtToColor(const QColor &c);

Vector2i QTTOOLS_API QtToVector(const QSize &s);
Vector2d QTTOOLS_API QtToVector(const QVector2D &s);
Vector3d QTTOOLS_API QtToVector(const QVector3D &s);
Vector4d QTTOOLS_API QtToVector(const QVector4D &s);

QSize QTTOOLS_API VectorToQt(const Vector2i &v);
QVector4D QTTOOLS_API VectorToQt(const Vector4d &v);
QVector3D QTTOOLS_API VectorToQt(const Vector3d &v);
QVector2D QTTOOLS_API VectorToQt(const Vector2d &v);

}

}

