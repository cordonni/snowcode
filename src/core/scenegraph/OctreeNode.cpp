#include "core/scenegraph/OctreeNode.h"


#include "core/geometry/BasicTriMesh.h"
#include "core/geometry/BasicRay.h"


using namespace std;

namespace expressive
{

namespace core
{

OctreeNode::OctreeNode(core::SceneManager* scene_manager,
                               SceneNode *parent,
                               const string &name,
                               std::shared_ptr<BVHabstractTopology> octree,
                               const string &type,
                               Vector scale) :
    SceneNode(scene_manager, parent, name, type, scale),
    ListenerT<BVHabstractTopology>(octree.get()),
    octree_(octree),
    current_index_(0)
{
}

OctreeNode::~OctreeNode()
{
}

void OctreeNode::UpdateMesh()
{
    current_index_ = 0;

    if (tri_mesh_ == nullptr) {
        printf("update mesh\n");
        std::shared_ptr<BasicTriMesh> mesh = std::make_shared<BasicTriMesh>(ork::TRIANGLES);

        AddOctreeToMesh(octree_, mesh);
        set_tri_mesh(mesh);
    }
    SceneNode::UpdateMesh();

}

void OctreeNode::AddOctreeToMesh(std::shared_ptr<BVHabstractTopology> octree, std::shared_ptr<BasicTriMesh> mesh)
{

    std::vector<AABBox> b_list = octree->getAABBvector();

    for(AABBox& b : b_list)
    {
        const Point &mi = b.min();
        const Point &ma = b.max();

        BasicTriMesh::VertexHandle v0 = mesh->add_vertex(DefaultMeshVertex(mi[0], mi[1], mi[2], 0, 255, 0)); // 0
        BasicTriMesh::VertexHandle v1 = mesh->add_vertex(DefaultMeshVertex(mi[0], mi[1], ma[2], 0, 255, 0)); // 1
        BasicTriMesh::VertexHandle v2 = mesh->add_vertex(DefaultMeshVertex(mi[0], ma[1], mi[2], 0, 255, 0)); // 2
        BasicTriMesh::VertexHandle v3 = mesh->add_vertex(DefaultMeshVertex(mi[0], ma[1], ma[2], 0, 255, 0)); // 3
        BasicTriMesh::VertexHandle v4 = mesh->add_vertex(DefaultMeshVertex(ma[0], mi[1], mi[2], 0, 255, 0)); // 4
        BasicTriMesh::VertexHandle v5 = mesh->add_vertex(DefaultMeshVertex(ma[0], mi[1], ma[2], 0, 255, 0)); // 5
        BasicTriMesh::VertexHandle v6 = mesh->add_vertex(DefaultMeshVertex(ma[0], ma[1], mi[2], 0, 255, 0)); // 6
        BasicTriMesh::VertexHandle v7 = mesh->add_vertex(DefaultMeshVertex(ma[0], ma[1], ma[2], 0, 255, 0)); // 7


        mesh->add_line(v0, v1);
        mesh->add_line(v0, v2);
        mesh->add_line(v1, v3);
        mesh->add_line(v2, v3);
        mesh->add_line(v0, v4);
        mesh->add_line(v4, v5);
        mesh->add_line(v1, v5);
        mesh->add_line(v4, v6);
        mesh->add_line(v2, v6);
        mesh->add_line(v7, v5);
        mesh->add_line(v7, v3);
        mesh->add_line(v7, v6);

    }
}

void OctreeNode::updated(core::BVHabstractTopology* /*octree*/)
{
    UpdateMesh();
}

} // namespace core

} // namespace expressive
