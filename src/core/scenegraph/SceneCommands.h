/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GRAPHCOMMANDS_H
#define GRAPHCOMMANDS_H

#include <core/scenegraph/Camera.h>
#include <core/commands/CommandManager.h>

namespace expressive
{

namespace core
{

class CORE_API MoveNodeCommand : public Command
{
public:
    MoveNodeCommand(SceneManager *scene, const SceneManager::NodeId &id, const Point &pos, const Point &target, const Point &original_pos, const Point &original_target, Command *parent = nullptr);

    virtual ~MoveNodeCommand();

    virtual bool execute();

    virtual bool undo();

    virtual std::shared_ptr<Command> getPart(double begin = 0, double end = 100);

protected:
    SceneManager *m_scene;

    SceneManager::NodeId m_id;

    Point m_pos;

    Point m_target;

    Point m_original_pos;

    Point m_original_target;
};

/**
 * This file contains SceneNode related commands.
 */
/**
 * @brief The RotateNodeCommand class rotates a (list of) SceneNodes.
 */
class CORE_API RotateNodeCommand : public Command
{
public:
    RotateNodeCommand(SceneManager *scene, const SceneManager::NodeId &id, const Point &up, const Point &right, const Point &cent, Scalar valX, Scalar valY, Command *parent = nullptr);

    RotateNodeCommand(SceneManager *scene, const std::set<SceneManager::NodeId> &ids, const Point &up, const Point &right, const Point &cent, Scalar valX, Scalar valY, Command *parent = nullptr);

    virtual ~RotateNodeCommand();

    virtual bool execute();

    virtual bool undo();

    virtual std::shared_ptr<Command> getPart(double begin = 0, double end = 100);
protected:
    SceneManager *scene_;

    std::set<SceneManager::NodeId> node_ids_;

    Point up_;

    Point right_;

    Point cent_;

    Scalar valX_;

    Scalar valY_;

};

/**
 * @brief The ScaleNodeCommand class scales a (list of) SceneNodes.
 */
class CORE_API ScaleNodeCommand : public Command
{
public:
    ScaleNodeCommand(SceneManager *scene, const SceneManager::NodeId &id, Point scale, Command *parent = nullptr);
    ScaleNodeCommand(SceneManager *scene, const SceneManager::NodeId &id, Scalar scaleX, Scalar scaleY, Scalar scaleZ, Command *parent = nullptr);

    ScaleNodeCommand(SceneManager *scene, const std::set<SceneManager::NodeId> &ids, Point scale, Command *parent = nullptr);
    ScaleNodeCommand(SceneManager *scene, const std::set<SceneManager::NodeId> &ids, Scalar scaleX, Scalar scaleY, Scalar scaleZ, Command *parent = nullptr);

    virtual ~ScaleNodeCommand();

    virtual bool execute();

    virtual bool undo();

    virtual std::shared_ptr<Command> getPart(double begin = 0, double end = 100);
protected:
    SceneManager *scene_;

    std::set<SceneManager::NodeId> node_ids_;

    Point scale_;

};

/**
 * This task is used to call the MeshCommand in the proper Opengl context time step.
 * When updating Mesh, one should inherit from MeshCommand and overload the executeFromTask method.
 */
class CORE_API UpdateMeshCommand : public Command
{
public:
    class DummyTask : public ork::Task
    {
    public:
        DummyTask(std::shared_ptr<UpdateMeshCommand> owner, bool undo = false);

        virtual ~DummyTask();

        virtual bool run();

    protected:
        std::shared_ptr<UpdateMeshCommand> owner_;

        bool undo_;
    };

//    UpdateMeshCommand(std::shared_ptr<AbstractTriMesh> mesh, const char *name = "UpdateMeshCommand");
    UpdateMeshCommand(SceneManager *scene, const SceneManager::NodeId &id, const char *name = "UpdateMeshCommand", Command *parent = nullptr);
    UpdateMeshCommand(SceneManager *scene, const std::set<SceneManager::NodeId> &ids, const char *name = "UpdateMeshCommand", Command *parent = nullptr);

    virtual ~UpdateMeshCommand();

    virtual bool execute();

    virtual bool undo();

    virtual std::shared_ptr<Command> getPart(double begin = 0,double end = 100);

    virtual bool executeFromTask();

    virtual bool undoFromTask();

protected:
    SceneManager* scene_;

    std::set<SceneManager::NodeId> ids_;

    bool lock_for_update_;

    bool do_garbage_collect_;
//    std::shared_ptr<AbstractTriMesh> mesh_;

};

/**
 * @brief The TransformMeshCommand class Applies a given transformation to a mesh.
 */
class CORE_API TransformMeshCommand : public UpdateMeshCommand
{
public:
    TransformMeshCommand(SceneManager *scene, const SceneManager::NodeId &id, const Transform& t, const Matrix &trot, Command *parent = nullptr);

    virtual ~TransformMeshCommand();

    virtual bool executeFromTask();

    virtual bool undoFromTask();

protected:
    Transform transform_;

    Matrix rotation_transform_;
};

} // namespace core

} // namespace expressive

#endif // SCENECOMMANDS_H
