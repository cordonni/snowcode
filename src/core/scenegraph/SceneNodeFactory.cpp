#include "core/scenegraph/SceneNodeFactory.h"
#include "ork/core/Logger.h"

using namespace ork;
using namespace expressive::core;
using namespace std;

namespace expressive
{

namespace core
{

SceneNodeFactory *SceneNodeFactory::GetInstance()
{
    static SceneNodeFactory INSTANCE = SceneNodeFactory();
    return &INSTANCE;
}

void SceneNodeFactory::AddType(const std::string &type, createFunc f)
{
    types[type] = f;
}

void SceneNodeFactory::AddType(const std::string &type, createDynamicableFunc f)
{
    dynamicable_types[type] = f;
}

std::shared_ptr<SceneNode> SceneNodeFactory::Create(const string &type, core::SceneManager *owner, SceneNode *parent, const string &name, std::shared_ptr<void> param, const Vector &scale, bool dynamic, const string &fallbackType)
{
    return GetInstance()->CreateNode(type, owner, parent, name, param, scale, dynamic, fallbackType);
}

std::shared_ptr<SceneNode> SceneNodeFactory::Create(const string &type, core::SceneManager *owner, SceneNode *parent, const string &name, std::shared_ptr<void> param, const Vector &scale, const string &fallbackType)
{
    return GetInstance()->CreateNode(type, owner, parent, name, param, scale, false, fallbackType);
}

 std::shared_ptr<SceneNode> SceneNodeFactory::CreateNode(const std::string &type, core::SceneManager* owner, SceneNode *parent, const string &name, std::shared_ptr<void> param, const Vector &scale, bool dynamic, const string &fallbackType)
{
    auto it = dynamicable_types.find(type);

    if (it != dynamicable_types.end()) {
        return it->second(owner, parent, name, param, scale, dynamic);
    } else {
        auto it2 = types.find(type);
        if (it2 != types.end()) {
            return it2->second(owner, parent, name, param, scale);
        }

        if (fallbackType != "" ) {
            return CreateNode(fallbackType, owner, parent, name, param, scale, dynamic);
        }
        if (ork::Logger::ERROR_LOGGER != nullptr) {
            ork::Logger::ERROR_LOGGER->log("SCENEGRAPH", "Unknown SceneNode type '" + type + "'");
        }
        throw exception();
    }
}

} // namespace core

} // namespace expressive
