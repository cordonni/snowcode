#include "core/scenegraph/ControlFrame.h"
#include "core/geometry/MeshTools.h"
#include "core/scenegraph/SceneManager.h"

#define X_CONE_NAME this->name() + X_CONE_EXT
#define Y_CONE_NAME this->name() + Y_CONE_EXT
#define Z_CONE_NAME this->name() + Z_CONE_EXT
#define X_CYLINDRE_NAME this->name() + X_CYLINDRE_EXT
#define Y_CYLINDRE_NAME this->name() + Y_CYLINDRE_EXT
#define Z_CYLINDRE_NAME this->name() + Z_CYLINDRE_EXT
#define SPHERE_NAME this->name() + SPHERE_EXT


using namespace std;

namespace expressive
{

namespace core
{




const string ControlFrame::X_CONE_EXT("_ConeX");
const string ControlFrame::Y_CONE_EXT("_ConeY");
const string ControlFrame::Z_CONE_EXT("_ConeZ");
const string ControlFrame::X_CYLINDRE_EXT("_CylinderX");
const string ControlFrame::Y_CYLINDRE_EXT("_CylinderY");
const string ControlFrame::Z_CYLINDRE_EXT("_CylinderZ");
const string ControlFrame::SPHERE_EXT("_Sphere");

const double ControlFrame::RADIUS = 0.1;


ControlFrame::ControlFrame(core::SceneManager *scene_manager, SceneNode *parent, const string &name, std::shared_ptr<core::SceneNodeManipulator> manipulator, core::Frame f):
    ObjectPrimitive(scene_manager,
                    parent,
                    name,
                    manipulator,
                    nullptr,
                    f),
    X_cone_selected_(false),
    Y_cone_selected_(false),
    Z_cone_selected_(false),
    base_selected_(false)
{
    double  lx = f.px().norm(),
            ly = f.py().norm(),
            lz = f.pz().norm();
    Vector  p0 = f.p0(),
            dx = f.px().normalized(),
            dy = f.py().normalized(),
            dz = f.pz().normalized();

//    frame_.normalize();

    shared_ptr<ObjectPrimitive> to_add;
    std::shared_ptr<AbstractTriMesh> m;

    m = MeshTools::CreateCylinder(Frame(p0, -dz * RADIUS, dy * RADIUS, dx*lx));
    m->FillColor(Color::Red());
    to_add = make_shared<ObjectPrimitive>(scene_manager,
                                          this,
                                          X_CYLINDRE_NAME,
                                          manipulator,
                                          m);
    to_add->interactable() = false;
    this->AddChild(to_add);
    X_cylindre_node_id_ = to_add->id();

    m = MeshTools::CreateDisk(Frame(p0 + dx*lx, -dz*RADIUS*2, dy * RADIUS*2, dx*RADIUS*4));
//    m->AddMesh(*MeshTools::CreateCone(Frame(p0 + dx*lx, -dz * RADIUS*2, dy * RADIUS*2, dx*RADIUS*4)));
    m->FillColor(Color::Red());
    to_add = make_shared<ObjectPrimitive>(scene_manager,
                                          this,
                                          X_CONE_NAME,
                                          manipulator,
                                          m);

//    to_add->set_selected_material("RedMaterial");
//    to_add->set_hovered_material ("RedMaterial");
//    to_add->set_selected_material("LightMaterial_1");
//    to_add->set_hovered_material ("LightMaterial_2"); // TODO OPENGL

    this->AddChild(to_add);
    X_cone_node_id_ = to_add->id();



    m = MeshTools::CreateCylinder(Frame(p0, -dx * RADIUS, dz * RADIUS, dy * ly));
    m->FillColor(Color::Green());

    to_add = make_shared<ObjectPrimitive>(scene_manager,
                                          this,
                                          Y_CYLINDRE_NAME,
                                          manipulator,
                                          m);

//    to_add->set_selected_material("GreenMaterial");
//    to_add->set_hovered_material ("GreenMaterial");
//    to_add->set_selected_material("LightMaterial_1");
//    to_add->set_hovered_material ("LightMaterial_2");// TODO OPENGL

    to_add->interactable() = false;
    this->AddChild(to_add);
    Y_cylindre_node_id_ = to_add->id();

    m = MeshTools::CreateDisk(Frame(p0 + dy*ly, -dx * RADIUS*2, dz * RADIUS*2, dy*RADIUS*4));
//    m->AddMesh(*MeshTools::CreateCone(Frame(p0 + dy*ly, -dx * RADIUS*2, dz * RADIUS*2, dy*RADIUS*4)));
    m->FillColor(Color::Green());

    to_add = make_shared<ObjectPrimitive>(scene_manager,
                                          this,
                                          Y_CONE_NAME,
                                          manipulator,
                                          m);

//    to_add->set_selected_material("GreenMaterial");
//    to_add->set_hovered_material ("GreenMaterial");
//    to_add->set_selected_material("LightMaterial_1");
//    to_add->set_hovered_material ("LightMaterial_2"); // TODO OPENGL

    this->AddChild(to_add);
    Y_cone_node_id_ = to_add->id();



    m = MeshTools::CreateCylinder(Frame(p0, dx * RADIUS, dy * RADIUS, dz * lz));
    m->FillColor(Color::Blue());

    to_add = make_shared<ObjectPrimitive>(scene_manager,
                                          this,
                                          Z_CYLINDRE_NAME,
                                          manipulator,
                                          m);

//    to_add->set_selected_material("BlueMaterial");
//    to_add->set_hovered_material ("BlueMaterial");
//    to_add->set_selected_material("LightMaterial_1");
//    to_add->set_hovered_material ("LightMaterial_2");

    to_add->interactable() = false;
    this->AddChild(to_add);
    Z_cylindre_node_id_ = to_add->id();

    m = MeshTools::CreateDisk(Frame(p0 + dz*lz, dx * RADIUS*2, dy * RADIUS*2, dz*RADIUS*4));
//    m->AddMesh(*MeshTools::CreateCone(Frame(p0 + dz*lz, dx * RADIUS*2, dy * RADIUS*2, dz*RADIUS*4)));
    m->FillColor(Color::Blue());

    to_add = make_shared<ObjectPrimitive>(scene_manager,
                                          this,
                                          Z_CONE_NAME,
                                          manipulator,
                                          m);

//    to_add->set_selected_material("BlueMaterial");
//    to_add->set_hovered_material ("BlueMaterial");
//    to_add->set_selected_material("LightMaterial_1");
//    to_add->set_hovered_material ("LightMaterial_2");

    this->AddChild(to_add);
    Z_cone_node_id_ = to_add->id();



    m = MeshTools::CreateSphere(Frame(p0, dx * RADIUS * 2, dy * RADIUS * 2, dz*RADIUS*2));
    m->FillColor(Color::Gray());

    to_add = make_shared<ObjectPrimitive>(scene_manager,
                                          this,
                                          SPHERE_NAME,
                                          manipulator,
                                          m);

//    to_add->set_selected_material("GrayMaterial");
//    to_add->set_hovered_material ("GrayMaterial");
//    to_add->set_selected_material("LightMaterial_1");
//    to_add->set_hovered_material ("LightMaterial_2");

    this->AddChild(to_add);
    sphere_node_id_ = to_add->id();
}


bool ControlFrame::ChildSelected(const uint id)
{
//    owner_->get_node(id)->set_selected();

    if(X_cone_node_id_ == id)
    {
        X_cone_selected_ = true;
        owner_->AddToSelection(id);
//        owner_->get_node(X_cone_node_id_)->set_selected();
        return true;
    }
    if(Y_cone_node_id_ == id)
    {
        Y_cone_selected_ = true;
        owner_->get_node(Y_cone_node_id_)->set_selected();
        return true;
    }
    if(Z_cone_node_id_ == id)
    {
        Z_cone_selected_ = true;
        owner_->get_node(Z_cone_node_id_)->set_selected();
        return true;
    }
    if(sphere_node_id_ == id)
    {
        base_selected_ = true;
        owner_->get_node(sphere_node_id_)->set_selected();
        return true;
    }
    return false;
}

void ControlFrame::Deselect()
{
    X_cone_selected_ = false;
    Y_cone_selected_ = false;
    Z_cone_selected_ = false;
    base_selected_ = false;

    owner_->get_node(X_cone_node_id_)->set_selected(false);
    owner_->get_node(Y_cone_node_id_)->set_selected(false);
    owner_->get_node(Z_cone_node_id_)->set_selected(false);
    owner_->get_node(sphere_node_id_)->set_selected(false);
//    this->set_selected(false);

    std::set<SceneManager::NodeId> ids;
    ids.insert(this->id());
    ids.insert(X_cone_node_id_);
    ids.insert(Y_cone_node_id_);
    ids.insert(Z_cone_node_id_);
    ids.insert(sphere_node_id_);
    owner_->RemoveFromSelection(ids);
}


bool ControlFrame::Translate(const Vector &v)
{
    if(X_cone_selected_)
    {
        Vector v_proj = v.dot(frame_.px().normalized()) * frame_.px().normalized();

        Vector old_px = frame_.px();
        Vector px = frame_.px() + v_proj;

        if(px.dot(old_px.normalized()) < RADIUS * 2)
            return false;

        static_pointer_cast<ObjectPrimitive>(owner_->get_node(X_cone_node_id_))->Translate(v_proj);

//        Eigen::Matrix3d m; m. << px.norm() / old_px.norm() << .0 << .0 << .0 << .1 << .0 << .0 << .0 << .1;
//        expressive::Transform t2(m);
        expressive::Transform t = expressive::Transform::Identity();
        Vector scal(px.norm() / old_px.norm(),1,1);
        t = frame_.internal_transform().inverse() * t;
        t.prescale(scal);
//        t.scale(scal);
//        t = t2 * t;
        t = frame_.internal_transform() * t;
//        t = t * frame_.internal_transform();

        static_pointer_cast<ObjectPrimitive>(owner_->get_node(X_cylindre_node_id_))->Transform(t);
        frame_.transform(t);

        return true;
    }
    if(Y_cone_selected_)
    {
        Vector v_proj = v.dot(frame_.py().normalized()) * frame_.py().normalized();

        Vector old_py = frame_.py();
        Vector py = frame_.py() + v_proj;

        if(py.dot(old_py.normalized()) < RADIUS * 2)
            return false;

        static_pointer_cast<ObjectPrimitive>(owner_->get_node(Y_cone_node_id_))->Translate(v_proj);

        expressive::Transform t = expressive::Transform::Identity();
        Vector scal(1,py.norm() / old_py.norm(),1);
        t = frame_.internal_transform().inverse() * t;
        t.prescale(scal);
//        t.scale(scal);
        t = frame_.internal_transform() * t;

        static_pointer_cast<ObjectPrimitive>(owner_->get_node(Y_cylindre_node_id_))->Transform(t);
        frame_.transform(t);

        return true;
    }
    if(Z_cone_selected_)
    {
        Vector v_proj = v.dot(frame_.pz().normalized()) * frame_.pz().normalized();

        Vector old_pz = frame_.pz();
        Vector pz = frame_.pz() + v_proj;

        if(pz.dot(old_pz.normalized()) < RADIUS * 2)
            return false;

        static_pointer_cast<ObjectPrimitive>(owner_->get_node(Z_cone_node_id_))->Translate(v_proj);

        expressive::Transform t = expressive::Transform::Identity();
        Vector scal(1,1,pz.norm() / old_pz.norm());
        t = frame_.internal_transform().inverse() * t;
        t.prescale(scal);
//        t.scale(scal);
        t = frame_.internal_transform() * t;

        static_pointer_cast<ObjectPrimitive>(owner_->get_node(Z_cylindre_node_id_))->Transform(t);
        frame_.transform(t);

        return true;
    }
    if(base_selected_)
    {
        static_pointer_cast<ObjectPrimitive>(owner_->get_node(X_cone_node_id_))     ->Translate(v);
        static_pointer_cast<ObjectPrimitive>(owner_->get_node(X_cylindre_node_id_)) ->Translate(v);
        static_pointer_cast<ObjectPrimitive>(owner_->get_node(Y_cone_node_id_))     ->Translate(v);
        static_pointer_cast<ObjectPrimitive>(owner_->get_node(Y_cylindre_node_id_)) ->Translate(v);
        static_pointer_cast<ObjectPrimitive>(owner_->get_node(Z_cone_node_id_))     ->Translate(v);
        static_pointer_cast<ObjectPrimitive>(owner_->get_node(Z_cylindre_node_id_)) ->Translate(v);
        static_pointer_cast<ObjectPrimitive>(owner_->get_node(sphere_node_id_))     ->Translate(v);

        frame_.translate(v);

        return true;
    }

    return false;
}


bool ControlFrame::Rotate(const Vector &v)
{
    Vector angleaxis(0,0,0);

    if(X_cone_selected_)
        angleaxis = v.dot(frame_.px().normalized()) * Vector(1,0,0);
    if(Y_cone_selected_)
        angleaxis = v.dot(frame_.py().normalized()) * Vector(0,1,0);
    if(Z_cone_selected_)
        angleaxis = v.dot(frame_.pz().normalized()) * Vector(0,0,1);

    if(angleaxis.norm() < 1e-5)
        return false;

    expressive::Transform t = expressive::Transform::Identity();
    t = frame_.normalized().internal_transform().inverse() * t;
    t.prerotate(Quaternion(angleaxis.normalized(), angleaxis.norm()).toMat4());
    t = frame_.normalized().internal_transform() * t;

    static_pointer_cast<ObjectPrimitive>(owner_->get_node(X_cone_node_id_))     ->Transform(t);
    static_pointer_cast<ObjectPrimitive>(owner_->get_node(X_cylindre_node_id_)) ->Transform(t);
    static_pointer_cast<ObjectPrimitive>(owner_->get_node(Y_cone_node_id_))     ->Transform(t);
    static_pointer_cast<ObjectPrimitive>(owner_->get_node(Y_cylindre_node_id_)) ->Transform(t);
    static_pointer_cast<ObjectPrimitive>(owner_->get_node(Z_cone_node_id_))     ->Transform(t);
    static_pointer_cast<ObjectPrimitive>(owner_->get_node(Z_cylindre_node_id_)) ->Transform(t);
    static_pointer_cast<ObjectPrimitive>(owner_->get_node(sphere_node_id_))     ->Transform(t);

    frame_.transform(t);

    return true;
}


} // namespace core

} // namespace expressive
