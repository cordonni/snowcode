#include "core/scenegraph/SceneNode.h"
#include "core/scenegraph/SceneManager.h"
#include "core/scenegraph/SceneNodeManipulator.h"
#include "core/scenegraph/SceneCommands.h"
#include "core/scenegraph/listeners/SceneNodeListener.h"

#include "core/geometry/AbstractTriMesh.h"
#include "core/geometry/BasicRay.h"
#include "core/geometry/VectorTools.h"

#include "ork/render/FrameBuffer.h"

#include "core/utils/ConfigLoader.h"
#include "core/utils/MemoryTools.h"

using namespace ork;
using namespace std;

namespace expressive
{

namespace core
{

SceneNode::SceneNode(const std::string &type) :
    ListenableT<SceneNode>(),
    Serializable(type),
    owner_(nullptr),
    parent_(nullptr),
    node_(new OrkSceneNode(type.c_str())),
    manipulator_proxy_(nullptr),
    manipulator_(nullptr),
    id_(-1),
    tri_mesh_(nullptr),
    scale_(1.0, 1.0, 1.0),
    selected_(false),
    hovered_(false),
    visible_(true),
    wireframe_(false),
    adjust_size_to_params_(true),
    selected_state_changed_(false),
    is_background_(false)

{
    name_ = (type + to_string((uintptr_t) this)).c_str();

    position_ = Matrix4::Identity();
    orientation_ = Matrix4::Identity();
    scaleTransform_ = Matrix4::Identity();

    parentToLocal_ = Matrix4::Identity();
    worldToLocal_ = Matrix4::Identity();
    localToParent_ = Matrix4::Identity();
    localToWorld_ = Matrix4::Identity();
}

SceneNode::SceneNode(SceneManager *owner, SceneNode *parent, const string &name, const string &type, const Vector &scale, bool addDefaultNodeFlags) :
    ListenableT<SceneNode>(),
    Serializable(type),
    owner_(nullptr),
    parent_(nullptr),
    node_(new OrkSceneNode(type.c_str())),
    id_(-1),
    tri_mesh_(nullptr),
    scale_(1.0, 1.0, 1.0),
    selected_(false),
    hovered_(false),
    visible_(parent == nullptr ? true : parent->visible()),
    wireframe_(false),
    adjust_size_to_params_(true),
    selected_state_changed_(false),
    is_background_(false)
{
    Init(name, owner, parent, addDefaultNodeFlags);
    this->Scale(scale);
}

void SceneNode::Init(const std::string &name, SceneManager *owner, SceneNode* parent, bool _addDefaultResources)
{
    if (owner != nullptr) {
        id_ = owner->next_node_id();
        owner_ = owner;
    }


//    name_ = (name + "_" + to_string(id_)).c_str();
    name_ = name;   // WARNING : Ulysse made this modif -> reverse if it breaks someone's pipeline.

    parent_ = parent;
    manipulator_ = nullptr;//new SceneNodeManipulator();
    tri_mesh_ = nullptr;

    position_ = Matrix4::Identity();
    orientation_ = Matrix4::Identity();
    scaleTransform_ = Matrix4::Identity();

    parentToLocal_ = Matrix4::Identity();
    worldToLocal_ = Matrix4::Identity();
    localToParent_ = Matrix4::Identity();
    localToWorld_ = Matrix4::Identity();

    if (_addDefaultResources) {
        AddDefaultNodeFlags();
    }
}

void SceneNode::AddDefaultNodeFlags()
{
    if (Config::getBoolParameter("disable_ork") == false) {
        node_->addFlag("object");
        node_->addModule("material", owner_->resource_manager()->loadResource(Config::getStringParameter("default_scene_node_module")).cast<Module>());
        node_->addMethod("draw", new Method(owner_->resource_manager()->loadResource(Config::getStringParameter("default_scene_node_method")).cast<TaskFactory>()));

        set_hovered(false);
        set_selected(false);
        set_visible(true);
        DisplayWireframe(false);
    }
}

SceneNode::~SceneNode()
{
    children_.clear();
}

void SceneNode::NotifySelectedStateChanged() const
{
    for (auto listener : listeners_)
    {
        static_cast<SceneNodeListener*>(listener)->SceneNodeSelectedStateChanged(id());
    }
}

void SceneNode::NotifyHoveredStateChanged() const
{
    for (auto listener : listeners_)
    {
        static_cast<SceneNodeListener*>(listener)->SceneNodeHoveredStateChanged(id());
    }
}

void SceneNode::NotifyVisibilityStateChanged() const
{
    for (auto listener : listeners_)
    {
        static_cast<SceneNodeListener*>(listener)->SceneNodeVisibilityStateChanged(id());
    }
}

void SceneNode::NotifyWireframeStateChanged() const
{
    for (auto listener : listeners_)
    {
        static_cast<SceneNodeListener*>(listener)->SceneNodeWireframeStateChanged(id());
    }
}

void SceneNode::NotifyAdjustSizeToParamsChanged() const
{
    for (auto listener : listeners_)
    {
        static_cast<SceneNodeListener*>(listener)->SceneNodeAdjustSizeToParamsChanged(id());
    }
}

SceneManager *SceneNode::owner() const
{
    return owner_;
}

SceneNode *SceneNode::parent() const
{
    return parent_;
}

SceneNode *SceneNode::ancestor() const
{
    return parent_ == nullptr ? (SceneNode*)this : parent_->ancestor();
}

ork::ptr<ork::SceneNode> SceneNode::internal_node() const
{
    return node_;
}

SceneNode::NodeId SceneNode::id() const
{
    return id_;
}

std::shared_ptr<AbstractTriMesh> SceneNode::tri_mesh() const
{
    return tri_mesh_;
}

SceneNode::ChildIterator SceneNode::children() const
{
    return ChildIterator(children_.begin(), children_.end());
}

uint SceneNode::get_child_count() const
{
    return (uint) children_.size();
}

bool SceneNode::has_children() const
{
    return children_.size() != 0;
}

bool SceneNode::has_child(const NodeId &id) const
{
    return children_ids_.find(id) != children_ids_.end();
}

bool SceneNode::has_mesh() const
{
    return tri_mesh_ != nullptr;
}

bool SceneNode::adjust_size_to_params() const
{
    return adjust_size_to_params_;
}

unsigned int SceneNode::manipulation_handler_id() const
{
    return id_;
}

std::shared_ptr<SceneNode> SceneNode::manipulator_proxy() const
{
    return manipulator_proxy_;
}

bool SceneNode::HasManipulatorProxy() const
{
    return manipulator_proxy_ != nullptr;
}

std::shared_ptr<SceneNodeManipulator> SceneNode::manipulator() const
{
    return manipulator_;
}

std::string SceneNode::name() const
{
    return name_;
}

bool SceneNode::selected() const {

    if (HasManipulatorProxy()) {
        if (manipulator_proxy_->selected()) { // here, we just don't want to interfere with proxy in case this one could actually manipulate the node.
            return false;
        }
    }
    return selected_;
}

bool SceneNode::hovered() const
{
    if (HasManipulatorProxy()) {
        if (manipulator_proxy_->hovered()) { // here, we just don't want to interfere with proxy in case this one could actually manipulate the node.
            return false;
        }
    }
    return hovered_;
}

bool SceneNode::HasSelectedManipulator() const
{
    if (HasManipulatorProxy()) {
        if (manipulator_proxy()->selected() || manipulator_proxy()->HasSelectedChild()) {
            return true;
        }
    }
    return false;
}

bool SceneNode::HasHoveredChild() const
{
    for (auto child : children_ ) {
        if (child->hovered() || child->HasHoveredChild()) {
            return true;
        }
    }
    return false;
}

bool SceneNode::HasSelectedChild() const
{
    if (HasManipulatorProxy()) {
        if (manipulator_proxy()->selected() || manipulator_proxy()->HasSelectedChild()) {
            return false;
        }
    }
//        if (selected() ) {
//            return true;
//        }

    for (auto child : children_ ) {
        if (child->selected() || child->HasSelectedChild()) {
            return true;
        }
    }
    return false;
}

void SceneNode::set_parent(SceneNode *new_parent)
{
    parent_ = new_parent;
}

void SceneNode::set_tri_mesh(std::shared_ptr<AbstractTriMesh> mesh, const std::string &mesh_tag)
{
//    require_rebuild_ = true;//tri_mesh_ != mesh;
    /// TODO: change tri_mesh to list of tri meshes
    tri_mesh_ = mesh;
    if (mesh != nullptr) {
        node_->addMesh(mesh_tag, mesh->mesh_buffers());
        node_->setLocalBounds(mesh->bounding_box().toBox3());
    }
}

void SceneNode::set_manipulator_proxy(std::shared_ptr<SceneNode> proxy)
{
    manipulator_proxy_ = proxy;
}

void SceneNode::set_manipulator(std::shared_ptr<SceneNodeManipulator> manipulator)
{
    manipulator_ = manipulator;
}

void SceneNode::RemoveAllChildren()
{
    for (auto child : children_) {
        RemoveChild(child, false);
    }
}

void SceneNode::CleanAllChildren()
{
    children_.clear();
    children_ids_.clear();
}

void SceneNode::set_selected(bool selected, bool hover_parent)
{
    selected_state_changed_ = selected_ != selected;
    selected_ = selected;
    node_->addValue(new Value1f("selected", selected_));
    if (hover_parent && parent_ != nullptr) {
        parent_->set_hovered(selected ? hover_parent : false);
    }

    NotifySelectedStateChanged();
}

void SceneNode::set_hovered(bool hovered, bool hover_parent)
{
    // If we only need to hover this (and we don't need to take care of the hover state of children)
    // OR
    // if children are selected, we need to keep this one hovered even if asked otherwise.
    if (!hover_parent || !(this->HasSelectedChild() || this->HasSelectedManipulator()) || hovered) {
        hovered_ = hovered;
        node_->addValue(new Value1f("hovered", hovered_));
        // if hovering / selecting this, we need to make sure that parent is still hovered so this will still be visible.
        if (parent_ != nullptr && hover_parent && (hovered || !selected_)) {
            parent_->set_hovered(hovered, hover_parent);
        }
        NotifyHoveredStateChanged();
    }
}

bool SceneNode::IsChildOf(SceneNode *parent) const
{
    SceneNode *ancestor = parent_;
    while (ancestor != nullptr) {
        if (ancestor == parent) {
            return true;
        }
        ancestor = ancestor->parent();
    }

    return false;
}

SceneNode *SceneNode::IsIntersectingRay(const BasicRay &ray, Scalar &dist, bool proxy /* = true */) const
{
    if (is_background_) {
        return nullptr;
    }
    if (proxy && HasManipulatorProxy()) {
        SceneNode * n = manipulator_proxy_->IsIntersectingRay(ray, dist);
        if (n != nullptr) {
            return n;
        }
    }

//    BasicRay currentRay(ray.starting_point() - GetPointInWorld(), ray.direction());
    // TODO CHECK HOW TO BYPASS PROXY FOR MOUSE CASE
    if (proxy) {
        dist = expressive::kScalarMax;
        Scalar curDistance = 0.0;
        SceneNode *res = nullptr;
        for (auto child : children_) {
            SceneNode *n = child->IsIntersectingRay(ray, curDistance);
//            if (n != nullptr) {
//            }
            if (curDistance <= dist) {
                res = n;
                dist = curDistance;
            }
        }

        if (res != nullptr) {
            return res;
        }
    }

    if (tri_mesh_ != nullptr) {
        BasicRay currentRay = GetRayInLocal(ray);
        dist = tri_mesh_->FindIntersection(currentRay, expressive::kScalarMax );
        if (dist != expressive::kScalarMax) {
            return (SceneNode*)this;
        }
    }
    return nullptr;
}

BasicRay SceneNode::GetRayInLocal(const BasicRay &ray) const
{
    Point start = GetPointInLocal(ray.starting_point());
    Point end   = GetPointInLocal(ray.starting_point() + ray.direction());
    return BasicRay(start, (end - start).normalized());
}

Point SceneNode::GetRealUp         () const
{
    UpdateTransformationMatrices();
    return localToWorld_ * Vector(0.0, 1.0, 0.0) - GetPointInWorld();
}

Point SceneNode::GetRealRight      () const
{
    UpdateTransformationMatrices();
    return localToWorld_ * Vector(1.0, 0.0, 0.0) - GetPointInWorld();
}

Point SceneNode::GetRealDirection  () const
{
    UpdateTransformationMatrices();
    return localToWorld_ * Vector(0.0, 0.0, -1.0) - GetPointInWorld();
}

void SceneNode::RemoveChild(SceneNode *child, bool undoable)
{
    // a null_deleter is used so we can actually create a shared_ptr without worrying with
    // child being deleted too soon.
    RemoveChild(std::shared_ptr<SceneNode>(child, null_deleter()), undoable);
}

void SceneNode::RemoveChild(std::shared_ptr<SceneNode> child, bool undoable)
{
    // These were no longer undoable due to changes in scene behavior.
    // So i simplified them.
    UNUSED(undoable);
//    if (undoable) {
//        owner_->RemoveNode(child->id());
//    } else {
    owner_->InternalRemove(child);
//    }
    // Now done in SceneManager#InternalRemove*.
//    children_.erase(child);
}

void SceneNode::AddChild(std::shared_ptr<SceneNode> child, bool undoable)
{
    UNUSED(undoable);
    if (child->parent() != nullptr) {
        child->parent()->RemoveChild(child.get());
    }
    child->set_parent(this);

    if (/*(owner_->has(id()) == true) && */owner_->has(child->id()) == false) {
        owner_->InternalAdd(child);
    }

    if (child->manipulator() == nullptr) {
        child->set_manipulator(manipulator());
    }
}

void SceneNode::SaveToObjFile(const std::string &name)
{
    if (tri_mesh_ != nullptr) {
        tri_mesh_->SaveToObjFile(name);
    }
}

void SceneNode::set_visible(bool visible, bool applyToChildren)
{
    visible_ = visible;
    node_->addValue(new Value1f("visible", visible_));
    if (applyToChildren) {
        auto it = children();
        while (it.HasNext()) {
            auto c = it.Next();
            c->set_visible(visible, true);
        }
    }
    NotifyVisibilityStateChanged();
}

void SceneNode::SetBackground(bool background, bool applyToChildren)
{
    is_background_ = background;
    if (applyToChildren) {
        auto it = children();
        while (it.HasNext()) {
            auto c = it.Next();
            c->SetBackground(background);
        }
    }
}

void SceneNode::ToggleWireframe(bool applyToChildren)
{
    DisplayWireframe(!wireframe_, false);
    if (applyToChildren) {
        auto it = children();
        while (it.HasNext()) {
            auto c = it.Next();
            c->ToggleWireframe(true);
        }
    }
}

void SceneNode::DisplayWireframe(bool visible, bool applyToChildren)
{
    wireframe_ = visible;
    node_->addValue(new Value1f("wireframe", wireframe_));
    if (applyToChildren) {
        auto it = children();
        while (it.HasNext()) {
            auto c = it.Next();
            c->DisplayWireframe(visible, true);
        }
    }
    NotifyWireframeStateChanged();
}

void SceneNode::ToggleVisible(bool applyToChildren)
{
    set_visible(!visible_, false);

    if (applyToChildren) {
        auto it = children();
        while (it.HasNext()) {
            auto c = it.Next();
            c->ToggleVisible(true);
        }
    }
}

void SceneNode::ShowBoundingBox(bool visible, bool applyToChildren)
{
    show_boundingbox_ = visible;
    if (applyToChildren) {
        auto it = children();
        while (it.HasNext()) {
            auto c = it.Next();
            c->ShowBoundingBox(visible);
        }
    }
}


bool SceneNode::visible() const
{
    return visible_;
}

bool SceneNode::wireframe() const
{
    return wireframe_;
}

void SceneNode::set_adjust_size_to_param(bool b) {
    adjust_size_to_params_ = b;

    auto it = children();
    while (it.HasNext()) {
        auto child = it.Next();
        child->set_adjust_size_to_param(b);
    }
    NotifyAdjustSizeToParamsChanged();
}

SceneNode* SceneNode::closest_ancestor(const SceneNode* target)
{
    SceneNode* output = this;

    while(output->parent() != nullptr)
    {
        if(target->IsChildOf(output))
            return output;

        output = output->parent();
    }

    return nullptr;
}

bool SceneNode::is_ancestor(const SceneNode* target) const
{
    while(target->parent() != nullptr)
    {
        if(target->parent() == this) {
            return true;
        }
        target = target->parent();
    }

    return false;
}

Vector SceneNode::GetScale() const
{
    return scale_;
}

Point SceneNode::GetPointInLocal(const Point &p) const
{
    UpdateTransformationMatrices();
    return worldToLocal_ * p;
}

Point SceneNode::GetPointInWorld(const Point &p) const
{
    UpdateTransformationMatrices();
    return localToWorld_ * p;
}

Matrix4 SceneNode::GetLocalToWorld() const
{
    UpdateTransformationMatrices();
    return localToWorld_;// * Matrix4::Identity();
}

Matrix4 SceneNode::GetWorldToLocal() const
{
    UpdateTransformationMatrices();
    return worldToLocal_;// * Matrix4::Identity();
}

Matrix4 SceneNode::GetLocalToParent() const
{
    UpdateTransformationMatrices();
    return localToParent_;// * Matrix4::Identity();
}

Matrix4 SceneNode::GetParentToLocal() const
{
    UpdateTransformationMatrices();
    return parentToLocal_;// * Matrix4::Identity();
}

Matrix4 SceneNode::GetLocalToCamera() const
{
    UpdateTransformationMatrices();
    return localToCamera_;// * Matrix4::Identity();
}

Matrix4 SceneNode::GetCameraToLocal() const
{
    UpdateTransformationMatrices();
    return localToCamera_;// * Matrix4::Identity();
}

Matrix4 SceneNode::GetWorldToParent() const
{
    if (parent_ == nullptr) {
        return Matrix4::Identity();
    }
    return parent_->GetWorldToLocal();
}

Matrix4 SceneNode::GetParentToWorld() const
{
    if (parent_ == nullptr) {
        return Matrix4::Identity();
    }
    return parent_->GetLocalToWorld();
}

void SceneNode::ResetOrientation()
{
    orientation_ = Transform::Identity();
}

Transform SceneNode::GetLocalToWorldTransform() const
{
    UpdateTransformationMatrices();
    return localToWorld_;
}

Transform SceneNode::GetLocalToParentTransform() const
{
    UpdateTransformationMatrices();
    return localToParent_;
}

Transform SceneNode::GetLocalToCameraTransform() const
{
    UpdateTransformationMatrices();
    return localToCamera_;
}

Transform SceneNode::GetWorldToLocalTransform() const
{
    UpdateTransformationMatrices();
    return worldToLocal_;
}

Transform SceneNode::GetParentToLocalTransform() const
{
    UpdateTransformationMatrices();
    return parentToLocal_;
}

Transform SceneNode::GetCameraToLocalTransform() const
{
    UpdateTransformationMatrices();
    return cameraToLocal_;
}

void SceneNode::LookAt(const Point &p, TransformSpace relativeTo)
{
    Point origin;
    Point target;
    switch(relativeTo)
    {
    default:
    case TS_WORLD:
        target = parent_ == nullptr ? p : parent_->GetPointInLocal(p);
        break;
    case TS_PARENT:
        target = p;
        break;
    case TS_LOCAL:
        target = parent_ == nullptr ? GetPointInWorld() : parent_->GetPointInLocal(GetPointInWorld(p));
        break;
    };

    origin = parent_ == nullptr ? GetPointInWorld() : parent_->GetPointInLocal(GetPointInWorld());

    Vector dir = (target - origin);

    Vector forward = dir.normalized();
    Vector up = Vector::UnitY();
    Vector right = forward.cross(up).normalized();
    if (1.0 - std::abs(forward.dot(up)) < 0.000001) { // if forward == UnitY or forward == -UnitY, issues will arise when computing right.
        right = Vector::UnitX(); // it would be (0,0,0), so we force it to be (1, 0, 0) to avoid issues when recomputing "up" vector.
        up = Vector::UnitZ();
    } else {
        up = right.cross(forward).normalized();
    }

    Matrix4 m = VectorTools::LookAtMatrix(origin, target, up).inverse();
    orientation_ = m;

    InvalidateTransformationMatrices();
}

void SceneNode::SetPos(const Point &p, TransformSpace relativeTo)
{
    Transform toParent;
    switch(relativeTo)
    {
    default:
    case TS_WORLD:
        if (parent_ == nullptr) {
            toParent = Transform::Identity();
        } else {
            toParent = parent_->GetWorldToLocalTransform();
        }
        break;
    case TS_PARENT:
        toParent = Transform::Identity();
        break;
    case TS_LOCAL:
        toParent = localToParent_;
        break;
    }

    position_ = VectorTools::TranslationMatrix(toParent * p);

    InvalidateTransformationMatrices();
}

void SceneNode::SetPos(Scalar x, Scalar y, Scalar z, TransformSpace relativeTo)
{
    return SetPos(Vector(x, y, z), relativeTo);
}

void SceneNode::Translate(const Point &v, TransformSpace relativeTo)
{
    Transform toParent;
    Vector origInParent = Point::Zero();
    Vector destInParent = v;
    switch(relativeTo)
    {
    default:
    case TS_WORLD:
        if (parent_ == nullptr) {
            toParent = Transform::Identity();
        } else {
            toParent = parent_->GetWorldToLocalTransform();
        }

        origInParent = toParent * GetPointInWorld();
        destInParent = toParent * (v + GetPointInWorld());
        break;
    case TS_PARENT:
        origInParent = localToParent_ * Point::Zero();
        destInParent = origInParent + v;
        break;
    case TS_LOCAL:
        origInParent = localToParent_ * Point::Zero();
        destInParent = localToParent_ * v;
        break;
    }

    Point parentV = destInParent - origInParent;
    position_ = Transform(VectorTools::TranslationMatrix(parentV)) * position_;

    InvalidateTransformationMatrices();
}

void SceneNode::SetScale(Scalar s)
{
    SetScale(Point(s, s, s));
}

void SceneNode::SetScale(const Point &s)
{
    scale_ = s;
    scaleTransform_ = Transform::scale(s);//Eigen::Scaling(s) * Transform::Identity();
    InvalidateTransformationMatrices();
}

void SceneNode::Scale(Scalar s)
{
    for (unsigned int i = 0; i < Point::RowsAtCompileTime; ++i) {
        scale_[i] *= s;
    }

    scaleTransform_ = Transform::scale(Point::Constant(s)) * scaleTransform_;//Eigen::Scaling(Point::Ones() * s) * scaleTransform_;
    InvalidateTransformationMatrices();
}

void SceneNode::Scale(const Point &s)
{
    for (unsigned int i = 0; i < Point::RowsAtCompileTime; ++i) {
        scale_[i] *= s[i];
    }

    scaleTransform_ = Transform::scale(s) * scaleTransform_;
    InvalidateTransformationMatrices();
}

void SceneNode::Scale(const Point &s, const Point &c)
{
    for (unsigned int i = 0; i < Point::RowsAtCompileTime; ++i) {
        scale_[i] *= s[i];
    }

    scaleTransform_ = Transform::scale(s) * scaleTransform_;


    Transform pToWorld = position_.inverse(); // localToParent;
    if (parent_ != nullptr) {
        pToWorld = parent_->GetLocalToWorldTransform() * pToWorld;
    }

    Point center = Vector::Zero();
    Point wpos = pToWorld * center; //GetPointInWorld();

    Point offset = c - wpos;
    offset[0] = (1.0 - s[0]) * offset[0];
    offset[1] = (1.0 - s[1]) * offset[1];
    offset[2] = (1.0 - s[2]) * offset[2];
    Translate(offset, TS_WORLD);


    InvalidateTransformationMatrices();

}

void SceneNode::Rotate(const Vector &a, Scalar th, TransformSpace relativeTo)
{
    Transform toLocal;
    Vector axis = Vector::UnitX();
    switch(relativeTo)
    {
    default:
    case TS_WORLD:
        if (parent_ == nullptr) {
            toLocal = Transform::Identity();
        } else {
            toLocal = parent_->GetWorldToLocalTransform();
        }
        axis = toLocal * a;
        break;
    case TS_PARENT:
//        toLocal = Transform::Identity();
        axis = a;
        break;
    case TS_LOCAL:
        if (parent_ == nullptr) {
            axis = (GetPointInWorld(a) - GetPointInWorld());
        } else {
            toLocal = GetLocalToParentTransform();
            axis = ((toLocal * a) - (toLocal * Point::Zero()));
        }
        break;
    }

//    AngleAxis rotation = AngleAxis(th, axis);
    Transform rotation = Quaternion(axis, th).toMat4();
    orientation_ = rotation * orientation_;

    InvalidateTransformationMatrices();
}

void SceneNode::Rotate(const Vector &a, const Point &c, Scalar th, TransformSpace relativeTo)
{
    Transform pToParent;
    switch(relativeTo)
    {
    default:
    case TS_WORLD:
        pToParent = parent_ == nullptr ? Transform::Identity() : parent_->GetWorldToLocalTransform();
        break;
    case TS_PARENT:
        pToParent = Transform::Identity();
        break;
    case TS_LOCAL:
        pToParent = parent_ == nullptr ? GetLocalToWorldTransform() : GetLocalToParentTransform();
        break;
    }

    Point posInParent = GetLocalToParentTransform() * Vector::Zero();
    Point centerInParent = pToParent * c;
    Point axisInParent = pToParent * (a + c) - centerInParent;

    Vector off = centerInParent/* - posInParent*/;

    Quaternion r = Quaternion(axisInParent, th);

    Point cur_point = posInParent;
    cur_point  = cur_point - off;

    Transform transform = Transform::Identity();
    transform.prerotate(r.toMat4());
    cur_point  = transform * cur_point;


    cur_point = cur_point + off;

    // TODO: Without this, the following line crashes on windows in release mode. Why ?
    char plop[129];
    plop[128] = 0;
    UNUSED(plop);


    position_ = VectorTools::TranslationMatrix<Scalar>(cur_point);;

    orientation_ = r.toMat4() * orientation_;

    InvalidateTransformationMatrices();
}

void SceneNode::RotateAround(const Point &target, const Vector &t, TransformSpace relativeTo)
{
    Transform parentToFrame;
    Transform localToFrame;
    Transform frameToParent;
    switch(relativeTo)
    {
    default: break;
    case TS_WORLD:
        parentToFrame = parent_ == nullptr ? Transform::Identity() : parent_->GetLocalToWorldTransform();
        localToFrame = parentToFrame * position_.inverse();
        frameToParent = parentToFrame.inverse();
        break;
    case TS_PARENT:
        parentToFrame = Transform::Identity();
        localToFrame = localToParent_;
        frameToParent = Transform::Identity();
        break;
    case TS_LOCAL:
        parentToFrame = parentToLocal_;
        localToFrame = Transform::Identity();
        frameToParent = localToParent_;
        break;
    }

    Point targetInParent = frameToParent * target;
    Vector tInParent = frameToParent * t - frameToParent * Point::Zero();

    Transform transform;
    transform = VectorTools::TranslationMatrix<Scalar>(-targetInParent); // target is centered on origin.
    Quaternion q = Quaternion(tInParent.normalized(), tInParent.norm());
    transform = q.toMat4() * transform;
    transform = Transform(VectorTools::TranslationMatrix(targetInParent)) * transform;

    Point newP = transform * localToParent_ * Vector::Zero();
    position_ = VectorTools::TranslationMatrix(newP);
    orientation_ = transform * orientation_;

    InvalidateTransformationMatrices();
}


void SceneNode::Yaw(Scalar r, TransformSpace relativeTo)
{
    Rotate(Vector::UnitY(), r, relativeTo);
}

void SceneNode::Pitch(Scalar r, TransformSpace relativeTo)
{
    Rotate(Vector::UnitX(), r, relativeTo);
}

void SceneNode::Roll(Scalar r, TransformSpace relativeTo)
{
    Rotate(Vector::UnitZ(), r, relativeTo);
}

void SceneNode::InvalidateTransformationMatrices()
{
    InvalidateChildrenTransformationMatrices();
    matricesUpdated_ = false;
}

void SceneNode::InvalidateChildrenTransformationMatrices()
{
    for (auto child : children_) {
        child->InvalidateTransformationMatrices();
    }
}

void SceneNode::UpdateTransformationMatrices(bool update_children) const
{
    if (matricesUpdated_ == false) {
        localToParent_ = position_ * orientation_ * scaleTransform_;
        parentToLocal_ = localToParent_.inverse();

        Transform parentToWorld = Transform::Identity();
        if (parent_ != nullptr) {
            parentToWorld = parent_->GetLocalToWorld();
        }

        localToWorld_ = parentToWorld * localToParent_;
        worldToLocal_ = localToWorld_.inverse();

        node_->setLocalToParent(localToParent_);

        if (update_children) {
            auto nodes = children();
            while (nodes.HasNext()) {
                std::shared_ptr<SceneNode> n = nodes.Next();
                n->UpdateTransformationMatrices(update_children);
            }
        }

        matricesUpdated_ = true;
    }
}

void SceneNode::UpdateMesh()
{
    if(tri_mesh_)
    {
        node_->addMesh("geometry", tri_mesh_->mesh_buffers());
        node_->setLocalBounds(tri_mesh_->bounding_box().toBox3());
    }
}

void SceneNode::Update(bool update_children)
{
    UpdateTransformationMatrices(false);
    UpdateMesh();

    if (update_children) {
        auto nodes = children();
        while (nodes.HasNext()) {
            std::shared_ptr<SceneNode> n = nodes.Next();
            n->Update(true);
        }
    }
}

AABBox SceneNode::bounding_box() const
{
    AABBox res;
    if (tri_mesh_ != nullptr) {
        res.Enlarge(tri_mesh_->bounding_box());
        res.Scale(scale_);
    }
    auto cit = children();
    while(cit.HasNext()) {
        auto child = cit.Next();
        res.Enlarge(child->bounding_box());
    }

    return res;
}

void SceneNode::TransferSceneDeformationToModel()
{
}

void SceneNode::SetModule(ork::ptr<Module> material, const std::string &name)
{
    node_->addModule(name, material);
}

} // namespace core

} // namespace expressive
