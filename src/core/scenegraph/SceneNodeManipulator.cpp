
#include <core/scenegraph/SceneNodeManipulator.h>
#include "core/utils/KeyBindingsList.h"

//#include <procedural/deformable/DeformableAdaptativeHouse.h>


namespace expressive {


namespace core {

// Constructor / Destructor
SceneNodeManipulator::SceneNodeManipulator(){}
SceneNodeManipulator::~SceneNodeManipulator()
{}

// Mouse Events
bool SceneNodeManipulator::wheelEvent        (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/)
{
    return false;
}

bool SceneNodeManipulator::mousePressed      (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/)
{
    return false;
}

bool SceneNodeManipulator::mouseReleased     (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/)
{
    return false;
}

bool SceneNodeManipulator::mouseMoved        (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/)
{
    return false;
}

bool SceneNodeManipulator::mouseDoubleClicked(std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/)
{
    return false;
}

// Keyboard Events
bool SceneNodeManipulator::keyPressed (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> n)
{

    UNUSED(n);
//    procedural::DeformableAdaptativeHouse::State s = std::dynamic_pointer_cast<procedural::DeformableAdaptativeHouse>(n)->get_state();
//    s = static_cast<procedural::DeformableAdaptativeHouse::State>( (1+static_cast<int>(s))%(static_cast<int>(procedural::DeformableAdaptativeHouse::State::Count)) );

//    std::dynamic_pointer_cast<procedural::DeformableAdaptativeHouse>(n)->set_state(s);

    return false;
}

bool SceneNodeManipulator::keyReleased(std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/)
{
    return false;
}

// Touch Events
bool SceneNodeManipulator::touchBegin (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/)
{
    return false;
}

bool SceneNodeManipulator::touchUpdate(std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/)
{
    return false;
}

bool SceneNodeManipulator::touchEnd   (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/)
{
    return false;
}

bool SceneNodeManipulator::penBegin (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/, Scalar /*pen_size*/)
{
    return false;
}

bool SceneNodeManipulator::penUpdate(std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/, Scalar /*pen_size*/)
{
    return false;
}

bool SceneNodeManipulator::penEnd   (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/)
{
    return false;
}

bool SceneNodeManipulator::redisplay(double t, double dt)
{
    UNUSED(t);
    UNUSED(dt);
    return false;
}

KeyBindingsList SceneNodeManipulator::GetKeyBindings() const
{
    KeyBindingsList k;
    return k;
}

} // namespace core

} // namespace expressive

