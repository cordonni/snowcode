#include "core/scenegraph/ControlSegment.h"
#include "core/geometry/MeshTools.h"
#include "core/scenegraph/SceneManager.h"

#define CYLINDRE_NAME this->name() + CYLINDRE_EXT
#define START_SPHERE_NAME this->name() + START_SPHERE_EXT
#define END_SPHERE_NAME this->name() + END_SPHERE_EXT

using namespace std;

namespace expressive
{

namespace core
{

const string ControlSegment::CYLINDRE_EXT("_Cylinder");
const string ControlSegment::START_SPHERE_EXT("_SSphere");
const string ControlSegment::END_SPHERE_EXT("_ESphere");
const double ControlSegment::RADIUS = 0.1;


ControlSegment::ControlSegment(core::SceneManager *scene_manager, SceneNode *parent, const string &name, std::shared_ptr<core::SceneNodeManipulator> manipulator, core::Frame f):
    ObjectPrimitive(scene_manager,
                    parent,
                    name,
                    manipulator,
                    nullptr,
                    f)
{
    double  lx = f.px().norm();
//            ly = f.py().norm(),
//            lz = f.pz().norm();
    Vector  p0 = f.p0(),
            dx = f.px().normalized(),
            dy = f.py().normalized(),
            dz = f.pz().normalized();

//    frame_.normalize();

    shared_ptr<ObjectPrimitive> to_add;
    std::shared_ptr<AbstractTriMesh> m;

    m = MeshTools::CreateCylinder(Frame(p0, -dz * RADIUS, dy * RADIUS, dx*lx));
    m->FillColor(Color::Gray());
    to_add = make_shared<ObjectPrimitive>(scene_manager,
                                          this,
                                          CYLINDRE_NAME,
                                          manipulator,
                                          m);
    to_add->interactable() = false;
    this->AddChild(to_add);
    cylindre_node_id_ = to_add->id();


    m = MeshTools::CreateSphere(Frame(p0, dx * RADIUS * 2, dy * RADIUS * 2, dz*RADIUS*2));
//    m->FillColor(Color::Gray());
    m->FillColor(Color(0.0,0.0,0.0,0.0));

    to_add = make_shared<ObjectPrimitive>(scene_manager,
                                          this,
                                          START_SPHERE_NAME,
                                          manipulator,
                                          m);
//    to_add->set_material("TransparentMaterial");
//    to_add->set_selected_material("TransparentMaterial");
//    to_add->set_hovered_material ("TransparentMaterial");
////    to_add->set_selected_material("GrayMaterial");
////    to_add->set_hovered_material ("GrayMaterial");// TODO OPENGL
    this->AddChild(to_add);
    start_sphere_node_id_ = to_add->id();


    m = MeshTools::CreateSphere(Frame(p0 + dx*lx, dx * RADIUS * 2, dy * RADIUS * 2, dz*RADIUS*2));
//    m->FillColor(Color::Gray());
    m->FillColor(Color(0.0,0.0,0.0,0.0));

    to_add = make_shared<ObjectPrimitive>(scene_manager,
                                          this,
                                          END_SPHERE_NAME,
                                          manipulator,
                                          m);
//    to_add->set_material("TransparentMaterial");
//    to_add->set_selected_material("TransparentMaterial");
//    to_add->set_hovered_material ("TransparentMaterial");
////    to_add->set_selected_material("GrayMaterial");
////    to_add->set_hovered_material ("GrayMaterial");// TODO OPENGL
    this->AddChild(to_add);
    end_sphere_node_id_ = to_add->id();

    Deselect();
}


bool ControlSegment::ChildSelected(const uint id)
{
    owner_->get_node(id)->set_selected();

    if(start_sphere_node_id_ == id)
    {
        start_sphere_selected_ = true;
        return true;
    }
    if(end_sphere_node_id_ == id)
    {
        end_sphere_selected_ = true;
        return true;
    }
    return false;
}

void ControlSegment::Deselect()
{
    start_sphere_selected_ = false;
    end_sphere_selected_ = false;

    owner_->get_node(start_sphere_node_id_)->set_selected(false);
    owner_->get_node(end_sphere_node_id_)->set_selected(false);
    this->set_selected(false);
}


bool ControlSegment::Translate(const Vector &v)
{
    if(start_sphere_selected_)
    {
//        Vector v_proj = v;

//        Vector old_px = frame_.px();
//        Vector px = frame_.px() + v_proj;

//        if(px.dot(old_px.normalized()) < RADIUS * 2)
//            return false;

//        static_pointer_cast<ObjectPrimitive>(owner_->get_node(start_sphere_node_id_))->Translate(v_proj);

//        expressive::Transform t = expressive::Transform::Identity();
//        Vector scal(px.norm() / old_px.norm(),1,1);
//        t = frame_.internal_transform().inverse() * t;
//        t.prescale(scal);

//        t = frame_.internal_transform() * t;

//        static_pointer_cast<ObjectPrimitive>(owner_->get_node(start_sphere_node_id_))->Transform(t);
//        frame_.transform(t);


        MoveStartHandle(v);

        return true;
    }
    if(end_sphere_selected_)
    {
//        Vector v_proj = v;

//        Vector old_px = frame_.px();
//        Vector px = frame_.px() + v_proj;

//        if(px.dot(old_px.normalized()) < RADIUS * 2)
//            return false;

//        static_pointer_cast<ObjectPrimitive>(owner_->get_node(end_sphere_node_id_))->Translate(v_proj);

//        expressive::Transform t = expressive::Transform::Identity();
//        Vector scal(px.norm() / old_px.norm(),1,1);
//        t = frame_.internal_transform().inverse() * t;
//        t.prescale(scal);

//        t = frame_.internal_transform() * t;

//        static_pointer_cast<ObjectPrimitive>(owner_->get_node(start_sphere_node_id_))->Transform(t);
//        frame_.transform(t);

        MoveEndHandle(v);

        return true;
    }

    return false;
}

void ControlSegment::MoveStartHandle(const Vector &v)
{
    static_pointer_cast<ObjectPrimitive>(owner_->get_node(start_sphere_node_id_))->Translate(v);

//    Vector old_p0 = frame_.p0();
//    Vector new_p0 = old_p0 + v;
//    Vector px = frame_.p0() + frame_.px();



//    double r = (px-new_p0).norm() / (px-old_p0).norm();
//    expressive::Transform t = expressive::Transform::Identity();
//    t.prescale(Vector(r,1,1));
//    t.prerotate(Quaternion::FromTwoVectors(px-old_p0, px-new_p0));
//    t.pretranslate(v);

//    static_pointer_cast<ObjectPrimitive>(owner_->get_node(cylindre_node_id_))->Transform(t, true);
//    frame_.transform(frame_.internal_transform() * t * frame_.internal_transform().inverse());



    // todo : use MoveEndHandle

    static_pointer_cast<ObjectPrimitive>(owner_->get_node(end_sphere_node_id_))->Translate(v);
    static_pointer_cast<ObjectPrimitive>(owner_->get_node(cylindre_node_id_))  ->Translate(v);
    frame_.translate(v);

    MoveEndHandle(-v);
}

void ControlSegment::MoveEndHandle(const Vector &v)
{
    static_pointer_cast<ObjectPrimitive>(owner_->get_node(end_sphere_node_id_))->Translate(v);

    Vector p0 = frame_.p0();
    Vector old_px = frame_.p0() + frame_.px();
    Vector new_px = old_px + v;


//    Vector p0(0,0,0);
//    Vector old_px(1,0,0);
//    Vector new_px = old_px + v;

    double r = (new_px-p0).norm() / (old_px-p0).norm();
    expressive::Transform t = expressive::Transform::Identity();
    t = frame_.internal_transform().inverse();
    t.prescale(Vector(r,1,1));

//    t.prerotate(Quaternion::FromTwoVectors(frame_.internal_transform() * (old_px-p0), frame_.internal_transform() * (new_px-p0)));
//    t.prerotate(Quaternion::FromTwoVectors(frame_.internal_transform().inverse() * (old_px), frame_.internal_transform().inverse() * (new_px)));
//    t.rotate(Quaternion::FromTwoVectors(frame_.internal_transform().inverse() *old_px, frame_.internal_transform().inverse() *new_px));


//    t = frame_.internal_transform().linear() * t;
//    t.prerotate(Quaternion::FromTwoVectors(old_px-p0, new_px-p0));
//    t = frame_.internal_transform().linear().inverse() * t;


//    t.prerotate(Quaternion::FromTwoVectors(old_px-p0, new_px-p0));

    t = frame_.internal_transform() * t;


    t.pretranslate(-p0);
    t.prerotate(expressive::Quaternion(old_px-p0, new_px-p0).toMat4());
    t.pretranslate(p0);


//    t = frame_.internal_transform().inverse();
//    Vector rot = (old_px-p0).normalized().cross((new_px-p0).normalized());
//    t.prerotate(Eigen::AngleAxisd(asin(rot.norm()), frame_.internal_transform() * rot.normalized()));
//    t = frame_.internal_transform() * t;


//    t.pretranslate(v);

//    Vector rot = (old_px-p0).normalized().cross((new_px-p0).normalized());
//    cout << rot.normalized().transpose() << endl;
//    t.prerotate(Eigen::AngleAxisd(asin(rot.norm()), rot.normalized()));
//    t.prerotate(Eigen::AngleAxisd(acos((old_px-p0).normalized().dot((new_px-p0).normalized())), rot.normalized()));
//    t.prerotate(Eigen::AngleAxisd(atan2(rot.norm(), (old_px-p0).normalized().dot((new_px-p0).normalized())), rot.normalized()));
//    t.prerotate(Quaternion::FromTwoVectors(old_px-p0, new_px-p0));

//    static_pointer_cast<ObjectPrimitive>(owner_->get_node(cylindre_node_id_))->Transform(t, true);
//    frame_.transform(frame_.internal_transform() * t * frame_.internal_transform().inverse());

    static_pointer_cast<ObjectPrimitive>(owner_->get_node(cylindre_node_id_))->Transform(t);
    frame_.transform(t);
}


bool ControlSegment::Rotate(const Vector &/*v*/)
{
//    Vector angleaxis(0,0,0);

//    if(start_sphere_selected_)
//        angleaxis = v.dot(frame_.px().normalized()) * Vector(1,0,0);
//    if(end_sphere_selected_)
//        angleaxis = v.dot(frame_.py().normalized()) * Vector(0,1,0);

//    if(angleaxis.norm() < 1e-5)
//        return false;

//    expressive::Transform t = expressive::Transform::Identity();
//    t = frame_.normalized().internal_transform().inverse() * t;
//    t.prerotate(Eigen::AngleAxisd(angleaxis.norm(), angleaxis.normalized()));
//    t = frame_.normalized().internal_transform() * t;

//    static_pointer_cast<ObjectPrimitive>(owner_->get_node(start_sphere_node_id_))->Transform(t);
//    static_pointer_cast<ObjectPrimitive>(owner_->get_node(end_sphere_node_id_))  ->Transform(t);
//    frame_.transform(t);

//    return true;

    return false;
}


} // namespace core

} // namespace expressive
