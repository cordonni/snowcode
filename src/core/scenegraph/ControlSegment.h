/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef CONTROLSEGMENT_H
#define CONTROLSEGMENT_H

#include "core/scenegraph/ObjectPrimitive.h"

namespace expressive
{

namespace core
{

/**
 * @brief The ControlSegment class draws a segment.
 */
class CORE_API ControlSegment : public ObjectPrimitive
{
public:
    
    EXPRESSIVE_MACRO_NAME("ControlSegment")

    ControlSegment( core::SceneManager* scene_manager = nullptr,
                    SceneNode *parent = nullptr,
                    const std::string &name = std::string("ControlSegment_") + std::to_string(instance_counter_++),
                    std::shared_ptr<core::SceneNodeManipulator> manipulator = nullptr,
                    core::Frame f = core::Frame());


    virtual bool ChildSelected(const uint id);
    virtual void Deselect();


    virtual bool Translate(const Vector &v);
    virtual bool Rotate(const Vector &v) ;


    void MoveStartHandle(const Vector &v);
    void MoveEndHandle(const Vector &v);

    inline bool HasSelectedSubparts() const {return (start_sphere_selected_ || end_sphere_selected_);}



private:

    bool start_sphere_selected_;
    bool end_sphere_selected_;

    uint start_sphere_node_id_;
    uint end_sphere_node_id_;
    uint cylindre_node_id_;


    static const std::string START_SPHERE_EXT;
    static const std::string END_SPHERE_EXT;
    static const std::string CYLINDRE_EXT;

    static const double RADIUS;

};


} // namespace core

} // namespace expressive


#endif // CONTROLSEGMENT_H
