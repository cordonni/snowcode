//#include "core/scenegraph/PlaneNode.h"
//#include "core/scenegraph/SceneManager.h"
//#include "core/utils/ConfigLoader.h"

//#include "core/geometry/BasicTriMesh.h"

//using namespace ork;

//namespace expressive {

//namespace core {

//PlaneNode::PlaneNode(SceneManager *scene_manager) :
//    SceneNode(scene_manager, nullptr, "plane", "PlaneNode")
//{
//    Init();
//}

//PlaneNode::PlaneNode() :
//    SceneNode("PlaneNode")
//{
//}

//PlaneNode::~PlaneNode()
//{
//}

//void PlaneNode::Init()
//{
//    std::shared_ptr<BasicTriMesh> plane = std::make_shared<BasicTriMesh>();
//    plane->addVertex(DefaultMeshVertex(-100, 0, +100, 0, 0, +1, 0, 0, 248, 166, 10, 0));
//    plane->addVertex(DefaultMeshVertex(+100, 0, +100, 0, 0, +1, 1, 0, 248, 166, 10, 0));
//    plane->addVertex(DefaultMeshVertex(+100, 0, -100, 0, 0, +1, 1, 1, 248, 166, 10, 0));
//    plane->addVertex(DefaultMeshVertex(+100, 0, -100, 0, 0, +1, 1, 1, 248, 166, 10, 0));
//    plane->addVertex(DefaultMeshVertex(-100, 0, -100, 0, 0, +1, 0, 1, 248, 166, 10, 0));
//    plane->addVertex(DefaultMeshVertex(-100, 0, +100, 0, 0, +1, 0, 0, 248, 166, 10, 0));
//    set_tri_mesh(plane);
//    node_->addFlag("transparent");
//    node_->addModule("material", owner_->resource_manager()->loadResource("gridShader").cast<Module>());
//    node_->addMethod("draw", new Method(owner_->resource_manager()->loadResource(Config::DEFAULT_LIGHT_DRAW_METHOD).cast<TaskFactory>()));
//}

//SceneNode* PlaneNode::IsIntersectingRay(const BasicRay &ray, Scalar &dist, bool proxy) const
//{
//    // we don't want this plane to be intersected with.
//    UNUSED(ray);
//    UNUSED(dist);
//    UNUSED(proxy);
//    return nullptr;
//}

//}

//}
