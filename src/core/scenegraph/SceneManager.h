/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_SCENEMANAGER_H_
#define EXPRESSIVE_SCENEMANAGER_H_

#include "core/scenegraph/SceneNode.h"
//#include "core/scenegraph/Overlay.h"
#include "core/commands/CommandManager.h"
#include "core/commands/ScriptPlayer.h"
#include "core/utils/AbstractIterator.h"

#include "ork/scenegraph/SceneManager.h"

#include <set>


namespace ork
{
    class ResourceManager;
}

namespace expressive
{

namespace core
{

class Camera;
class Overlay;
class SceneManager;

typedef ork::SceneManager OrkSceneManager;

/**
 * @brief The SceneChanges struct
 * Contains infos on which nodes have been added, deleted, selected or unselected.
 */
struct CORE_API SceneChanges
{
    SceneChanges(std::shared_ptr<SceneNode> added_node = nullptr, std::shared_ptr<SceneNode> removed_node = nullptr, std::shared_ptr<SceneNode> added_to_selection = nullptr, std::shared_ptr<SceneNode> removed_from_selection = nullptr);
    std::set<std::shared_ptr<SceneNode> > added_nodes_;
    std::set<std::shared_ptr<SceneNode> > removed_nodes_;
    std::set<std::shared_ptr<SceneNode> > added_to_selection_;
    std::set<std::shared_ptr<SceneNode> > removed_from_selection_;
};

/**
 * @brief The UpdateSceneCommand class
 * Contains the basics for scene changes (new nodes, deleted nodes, selection changes).
 *
 * Note that SceneNodes are NOT deleted immediately when removed from the scene.
 * Instead, they are stored in case we want them back in the scene. They will be deleted either
 * when the CommandManager's history is full, or when the application is closed. In order to avoid
 * huge memory usage, CommandManager should DEFINITLY have a size limit.
 * Additional note : user must be cautious about selection when removing nodes.
 * In that case, if node was selected, it HAS to be added to #SceneChanges::removed_from_selection_
 */
class CORE_API UpdateSceneCommand : public Command
{
public:
    UpdateSceneCommand(SceneManager *scene, SceneChanges changes);

    virtual ~UpdateSceneCommand() {}

    virtual bool execute();

    virtual bool undo();

protected:
    SceneManager *scene_;

    SceneChanges changes_;
};

#define NULL_NODE_ID (uint)-1

/**
 * @brief The SceneManager class
 * This class manages our scene.
 */
class CORE_API SceneManager: /*public OrkSceneManager, */public ListenableT<SceneManager>, public ScriptListener, public Serializable
{
public:
    typedef SceneNode::NodeId NodeId;

    typedef std::map<NodeId, std::shared_ptr<SceneNode> > NodePool;
    typedef std::map<NodeId, std::shared_ptr<SceneNode> > OverlayPool;


    class NodeSetIterator : public AbstractIteratorT<std::set<NodeId>, std::shared_ptr<SceneNode> >
    {
    public:
        NodeSetIterator(const SceneManager *owner, const std::set<NodeId>& container)  : AbstractIteratorT(container.cbegin(), container.cend()), owner_(owner)
        {
        }

        virtual std::shared_ptr<SceneNode> Next()
        {
            return ((SceneManager*)owner_)->get_node(*(iterator++));
        }

        virtual NodeId NextId()
        {
            return  *(iterator++);
        }

    protected:
        const SceneManager *owner_;
    };

    typedef AbstractIteratorBaseT<std::shared_ptr<SceneNode> > NodeIterator;
    typedef AbstractMapIterator<NodePool, std::shared_ptr<SceneNode> > NodeMapIterator;
    typedef std::shared_ptr<NodeIterator> NodeIteratorPtr;
    typedef std::shared_ptr<NodeSetIterator> NodeSetIteratorPtr;
    typedef std::shared_ptr<NodeMapIterator> NodeMapIteratorPtr;

    SceneManager(std::shared_ptr<CommandManager> command_manager);

    virtual ~SceneManager();

    std::shared_ptr<CommandManager> command_manager() const;

    virtual void clear(bool clearOverlays = false, bool undoable = false);

    virtual void AddNode(std::shared_ptr<SceneNode> node);

    virtual void AddOverlay(std::shared_ptr<SceneNode> overlay);

    virtual void RemoveAll();

    virtual void RemoveAllOverlays();

    virtual void RemoveNode(NodeId id);

    virtual void RemoveOverlay(NodeId id);

    virtual void AddToSelection(NodeId id);
    virtual void AddToSelection(std::set<NodeId> &ids);

    virtual void RemoveFromSelection(NodeId id);
    virtual void RemoveFromSelection(std::set<NodeId> &ids);

    virtual void RemoveAllFromSelection();

    virtual void RemoveSelectedNodes();

    virtual void SetSelection(NodeId id);
    virtual void SetSelection(std::set<NodeId> &ids);

    void SetHoveredNode(SceneNode *hover);

// TODO : I'm not sure we should use this method.
//    void RemoveNode(std::shared_ptr<SceneNode> node);
//    void AddToSelection(std::shared_ptr<SceneNode> node);
//    void RemoveFromSelection(std::shared_ptr<SceneNode> node);

    NodeId next_node_id();

    NodeId const_next_node_id();

    unsigned int node_count() const;

    unsigned int selected_node_count() const;

//    inline std::pair<NodePool::iterator, NodePool::iterator> non_const_nodes()
//    {
//        return make_pair(node_pool_.begin(), node_pool_.end());
//    }

//    inline std::pair<NodePool::const_iterator, NodePool::const_iterator> nodes()
//    {
//        return make_pair(node_pool_.cbegin(), node_pool_.cend());
//    }

//    inline std::pair<std::set<NodeId>::iterator, std::set<NodeId>::iterator> non_const_selected_nodes()
//    {
//        return make_pair(selected_nodes_.begin(), selected_nodes_.end());
//    }

//    inline std::pair<std::set<NodeId>::const_iterator, std::set<NodeId>::const_iterator> selected_nodes()
//    {
//        return make_pair(selected_nodes_.cbegin(), selected_nodes_.cend());
//    }

    NodeIteratorPtr non_const_nodes();

    const NodeIteratorPtr nodes() const;

    NodeSetIteratorPtr non_const_root_nodes();

    const NodeSetIteratorPtr root_nodes() const;

    NodeSetIteratorPtr non_const_selected_nodes();

    const NodeSetIteratorPtr selected_nodes() const;

    const std::set<NodeId> &selected_node_ids() const;

    NodeId hovered_node_id() const;

    std::shared_ptr<SceneNode> hovered_node() const;

    std::shared_ptr<SceneNode> get_node(NodeId id) const;

    std::shared_ptr<SceneNode> get_node(std::string name) const;

    std::shared_ptr<SceneNode> get_overlay(NodeId id) const;

    std::shared_ptr<SceneNode> get_overlay(std::string name) const;

    bool is_selected(NodeId id) const;

    bool has(NodeId id) const;

    virtual void SceneNodeAdded(NodeId id);
    virtual void SceneNodeRemoved(NodeId id);
    virtual void SceneNodeUpdated(NodeId id);
    virtual void SceneNodeSelected(NodeId id);
    virtual void SceneNodeUnselected(NodeId id);
//    virtual void SelectionUpdated();

    /**
     * Finds the intersection between a ray and the items of this scene.
     * @param camera A Camera from which the ray will start.
     * @param x x position on screen space in camera;
     * @param y y position on screen space in camera;
     * @param[out] target the position of the intersection of this ray to the node returned.
     * @param object_only if true, the ray will only be tested to root nodes.
     * @return The closest intersecting scene node. nullptr if no intersection.
     */
    inline SceneNode *GetCameraRayIntersection(Camera *camera, Scalar x, Scalar y, Point& target, bool object_only = false) { Scalar dist; return GetCameraRayIntersection(camera, x, y, target, dist, object_only); }
    inline SceneNode *GetRayIntersection(const core::BasicRay &ray, Point& target, bool object_only = false) { Scalar dist; return GetRayIntersection(ray, target, dist, object_only); }

    /**
     * Finds the intersection between a ray and the items of this scene.
     * @param ray The Ray in camera space
     * @param target The point where intersection occured.
     * @param dist The distance at which the intersection occured.
     * @param object_only if true, the ray will only be tester to root nodes.
     * @return The closest intersecting scene node. nullptr if no intersection.
     */

    virtual SceneNode *GetRayIntersection(const core::BasicRay &ray, Point& target, Scalar &dist, bool object_only = false);
    virtual SceneNode *GetCameraRayIntersection(Camera *camera, Scalar x, Scalar y, Point& target, Scalar &dist, bool object_only = false);

    /**
     * Same as GetRayIntersection, but returns the elements that are inside the screen box.
     * @return the number of selected items.
     */
    virtual unsigned int GetCameraBoxIntersection(Camera *camera, const AABBox2D &box, std::set<NodeId> &selected_nodes);
    unsigned int GetCameraBoxIntersection(Camera *camera, const AABBox2D &box, std::set<NodeId> &selected_nodes, std::shared_ptr<SceneNode> n); // same as previous, but for a given node and it's children.
    virtual unsigned int GetCameraBrushIntersection(Camera *camera, const Point2 &center, Scalar radius, std::set<NodeId> &selected_nodes);
    unsigned int GetCameraBrushIntersection(Camera *camera, const Point2 &center, Scalar radius, std::set<NodeId> &selected_nodes, std::shared_ptr<SceneNode> n); // same as previous, but for a given node and it's children.

    bool intersectsSelection(Camera *camera, Scalar x, Scalar y);
    bool intersectsSelection(Camera *camera, const Point2 &p);
    bool intersects(SceneManager::NodeId id, Camera *camera, Scalar x, Scalar y);
    bool intersects(SceneManager::NodeId id, Camera *camera, const Point2 &p);

    virtual SceneNode *GetCameraRayIntersection(Scalar x, Scalar y, Point& target, bool object_only = false);
    virtual unsigned int GetCameraBoxIntersection(const AABBox2D &box, std::set<NodeId> &selected_nodes);
    unsigned int GetCameraBoxIntersection(const AABBox2D &box, std::set<NodeId> &selected_nodes, std::shared_ptr<SceneNode> n); // same as previous, but for a given node and it's children.
    virtual unsigned int GetCameraBrushIntersection(const Point2 &center, Scalar radius, std::set<NodeId> &selected_nodes);
    unsigned int GetCameraBrushIntersection(const Point2 &center, Scalar radius, std::set<NodeId> &selected_nodes, std::shared_ptr<SceneNode> n); // same as previous, but for a given node and it's children.

    bool intersectsSelection(Scalar x, Scalar y);
    bool intersectsSelection(const Point2 &p);
    bool intersects(SceneManager::NodeId id, Scalar x, Scalar y);
    bool intersects(SceneManager::NodeId id, const Point2 &p);

    virtual void AddXmlAttributes(TiXmlElement *element) const;

    virtual TiXmlElement * ToXml(const char *name = nullptr) const;

    virtual std::shared_ptr<SceneNode> AddNodeFromSerializable(std::shared_ptr<Serializable> new_children, bool dynamic = false);

    std::shared_ptr<Camera> current_camera() const;

    void set_current_camera(std::shared_ptr<Camera> cam);

    ork::ptr<ork::ResourceManager> resource_manager() const;

    void set_resource_manager(ork::ptr<ork::ResourceManager> resource_manager);

    ork::ptr<ork::Scheduler> scheduler() const;

    void set_scheduler(ork::ptr<ork::Scheduler> scheduler);

    virtual void NotifyCleared();

    Point GetSceneBarycenter() const;

    Point GetSelectionBarycenter() const;

//    virtual void setRoot(ptr<SceneNode> root);
    virtual void Draw();

    virtual void Update(double t, double dt);

    /**
     * Takes a screenshot of the currently displayed framebuffer. See #saveToFile.
     */
    static void screenshot(const std::string &filename = "");

    /**
     * Callback for ScriptPlayer. When script has finished command, this will take a screenshot.
     */
    virtual void frameDoneCallback(int frame);

protected:
    virtual void init(std::shared_ptr<CommandManager> command_manager);

    virtual void InternalAdd(std::shared_ptr<SceneNode> node, bool firstAdd = true);

    virtual void InternalRemove(std::shared_ptr<SceneNode> node, bool firstRemoval = true);

    virtual void InternalAddToSelection(std::shared_ptr<SceneNode> node);

    virtual void InternalRemoveFromSelection(std::shared_ptr<SceneNode> node);

    ork::ptr<OrkSceneManager> ork_scene_;

    std::shared_ptr<CommandManager> command_manager_;

    std::shared_ptr<Camera> current_camera_manager_;

    NodeId next_node_id_;

    NodePool node_pool_;

    OverlayPool overlay_pool_;

    std::set<NodeId> selected_nodes_;

    NodeId hovered_node_;

    std::set<NodeId> root_nodes_;

    friend class UpdateSceneCommand;

    friend class SceneNode;

};

} // namespace core

} // namespace expressive

#endif // EXPRESSIVE_SCENEMANAGER_H_
