#include "core/scenegraph/listeners/SceneNodeListener.h"

#include "core/scenegraph/SceneNode.h"

namespace expressive {
namespace core {

SceneNodeListener::SceneNodeListener(SceneNode* node)
    : ListenerT(node)
{
}

SceneNodeListener::~SceneNodeListener()
{
}

SceneNodeListener::SceneNodeListener() :
    ListenerT<SceneNode>()
{
}

void SceneNodeListener::SceneNodeSelectedStateChanged(NodeId /*node*/) {}
void SceneNodeListener::SceneNodeHoveredStateChanged(NodeId /*node*/) {}
void SceneNodeListener::SceneNodeVisibilityStateChanged(NodeId /*node*/) {}
void SceneNodeListener::SceneNodeWireframeStateChanged(NodeId /*node*/) {}
void SceneNodeListener::SceneNodeAdjustSizeToParamsChanged(NodeId /*node*/) {}

}

}
