/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_SCENEMANAGER_LISTENER_H_
#define EXPRESSIVE_SCENEMANAGER_LISTENER_H_

#include <core/CoreRequired.h>
#include <core/utils/ListenerT.h>

namespace expressive {
namespace core {

class SceneManager;

/**
 * @brief The SceneManagerListener class handles what is done when the scene was updated.
 */
class CORE_API SceneManagerListener : public ListenerT<SceneManager> {
public:
    SceneManagerListener(SceneManager* manager);

    virtual ~SceneManagerListener();

    typedef unsigned int NodeId;

    virtual void SceneNodeAdded(NodeId node) = 0;
    virtual void SceneNodeRemoved(NodeId node) = 0;
    virtual void cleared(SceneManager *manager) = 0;
    virtual void SceneNodeSelected(NodeId node) = 0;
    virtual void SceneNodeUnselected(NodeId node) = 0;
//    virtual void SelectionUpdated(SceneManager* manager) = 0;

//    virtual void SceneNodeUpdated(NodeId node) = 0;
protected:
    SceneManagerListener();
};

} // namespace core
} // namespace expressive

#endif // EXPRESSIVE_SCENENODE_LISTENER_H_
