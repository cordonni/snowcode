#include "core/scenegraph/listeners/SceneManagerListener.h"

#include "core/scenegraph/SceneManager.h"

namespace expressive {
namespace core {

SceneManagerListener::SceneManagerListener(SceneManager* manager)
    : ListenerT(manager)
{
}

SceneManagerListener::~SceneManagerListener()
{
}

SceneManagerListener::SceneManagerListener() :
    ListenerT<SceneManager>()
{
}

}

}
