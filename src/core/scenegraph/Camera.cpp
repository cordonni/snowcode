#include "core/scenegraph/Camera.h"
#include "commons/Maths.h"
#include "core/geometry/VectorTools.h"

#include "ork/render/FrameBuffer.h"

//#include "core/geometry/AbstractTriMesh.h"
#include "core/geometry/BasicRay.h"
#include "core/geometry/MeshTools.h"
#include "core/utils/ConfigLoader.h"

using namespace std;
using namespace ork;

namespace expressive {
namespace core {

Camera::Camera() :
    SceneNode(StaticName()),
    framebuffer_(Config::getBoolParameter("disable_ork") ? nullptr : FrameBuffer::getDefault()),
    initial_position_(Point::Zero()),
    initial_target_(Point::Zero()),
    target_(Point::Zero()),
    target_node_(nullptr),
    cameraToScreen_(Matrix4::Identity()),
    screenToCamera_(Matrix4::Identity()),
    screenToWorld_(Matrix4::Identity()),
    worldToScreen_(Matrix4::Identity()),
    viewplaneWorldOrigin_(Vector::Zero()),
    viewplaneXVector_(Vector::Zero()),
    viewplaneYVector_(Vector::Zero()),
    viewplaneUpdated_(false),
    near_clip_distance_(0.1),
    far_clip_distance_(1000.0),
    fov_(80.0),
    recordedPosition_(Point::Zero()),
    recordedTarget_(Point::Zero())
{
}

Camera::Camera(SceneManager* scene_manager, const Vector &position, const Vector &target) :
    SceneNode(scene_manager, nullptr, "camera", StaticName(), Vector::Ones(), false),
    framebuffer_(Config::getBoolParameter("disable_ork") ? nullptr : FrameBuffer::getDefault()),
    initial_position_  (position),
    initial_target_    (target),
    target_            (target),
    target_node_(nullptr),
    cameraToScreen_(Matrix4::Identity()),
    screenToCamera_(Matrix4::Identity()),
    screenToWorld_(Matrix4::Identity()),
    worldToScreen_(Matrix4::Identity()),
    viewplaneWorldOrigin_(Vector::Zero()),
    viewplaneXVector_(Vector::Zero()),
    viewplaneYVector_(Vector::Zero()),
    viewplaneUpdated_(false),
    near_clip_distance_(0.1),
    far_clip_distance_(1000.0),
    fov_(80.0),
    recordedPosition_(position),
    recordedTarget_(target)
{
    SetPos(position);
    LookAt(target);
    Init(true);

//    scene_manager->set_current_camera(this);
}

Camera::~Camera()
{
//    scene_manager_ = nullptr;
}

void Camera::Init(bool add_default_resources)
{
//    SceneNode::Init(name, owner, parent, false);

    if (add_default_resources && Config::getBoolParameter("disable_ork") == false) {
        node_->addFlag("camera");
        node_->addModule("material", owner_->resource_manager()->loadResource(Config::getStringParameter("default_camera_module")).cast<Module>());
        node_->addModule("mousematerial", owner_->resource_manager()->loadResource(Config::getStringParameter("default_camera_mouse_module")).cast<Module>());
        node_->addMethod("draw", new Method(owner_->resource_manager()->loadResource(Config::getStringParameter("default_camera_method")).cast<TaskFactory>()));

        // Initializes the target SceneNode.
        target_node_ = std::make_shared<SceneNode>(owner(), nullptr, "camera_target", "CameraTarget");
        owner()->AddOverlay(target_node_);
        target_node_->SetPos(target());

        auto mesh = MeshTools::CreateSphere(Frame(0.1));
    //    auto mesh = MeshTools::CreateCross(Frame(0.1)); // why is there an error when using LINES ???
        mesh->FillColor(Color(0, 128, 128, 128));
        target_node_->set_tri_mesh(mesh);
        target_node_->internal_node()->addFlag("transparent");
    }

    UpdateCameraToScreen();
}

unsigned int Camera::GetActualWidth () const
{
    if (framebuffer_ != nullptr) {
        Vector4i v = framebuffer_->getViewport();
        return v[2] - v[0];
    }
    return 0;
}

unsigned int Camera::GetActualHeight() const
{
    if (framebuffer_ != nullptr) {
        Vector4i v = framebuffer_->getViewport();
        return v[3] - v[1];
    }
    return 0;
}

const Point& Camera::target() const
{
    return target_  ;
}

const ptr<FrameBuffer> Camera::framebuffer() const
{
    return framebuffer_;
}

Scalar Camera::near_distance() const
{
    return near_clip_distance_;
}

Scalar Camera::far_clip_distance() const
{
    return far_clip_distance_;
}

Scalar Camera::fov() const
{
    return fov_;
}

Scalar Camera::vfov() const
{
    Scalar width  = (Scalar) GetActualWidth();
    Scalar height = (Scalar) GetActualHeight();
    return  degrees(2.0 * atan((height / width) * tan(radians(fov_ / 2.0))));;
}

bool Camera::target_visibility() const
{
    return target_node_->visible();
}

void Camera::set_fov(Scalar fov)
{
    fov_ = fov;
    UpdateCameraToScreen();
}

void Camera::set_far_clip_distance(Scalar dist)
{
    far_clip_distance_ = dist;
    UpdateCameraToScreen();
}

void Camera::set_near_distance(Scalar dist)
{
    near_clip_distance_ = dist;
    UpdateCameraToScreen();
}

void Camera::set_target_distance(Scalar dist)
{
    Point target = GetPointInLocal(target_);
    target[2] = dist;
    target_ = GetPointInWorld(target);

    target_node_->SetPos(target_);
}

void Camera::set_target(const Point &p, TransformSpace relativeTo)
{
    switch(relativeTo) {
    default:
    case TS_WORLD:
        target_ = p;
        break;
    case TS_PARENT:
        target_ = parent_ == nullptr ? p : parent_->GetLocalToWorldTransform() * p;
        break;
    case TS_LOCAL:
        target_ = GetLocalToWorldTransform() * p;
        break;
    }
    LookAt(p, relativeTo);
}

void Camera::set_target_visibility(bool b)
{
    target_node_->set_visible(b);
}

void Camera::Translate(const Point &v, TransformSpace relativeTo)
{
    switch(relativeTo) {
    default:
    case TS_WORLD:
        target_ += v;
        break;
    case TS_PARENT:
        target_ += parent_ == nullptr ? v : parent_->GetLocalToWorldTransform() * v - parent_->GetPointInWorld();
        break;
    case TS_LOCAL:
        target_ += GetLocalToWorldTransform() * v - GetPointInWorld();
        break;
    }

    return SceneNode::Translate(v, relativeTo);
}

void Camera::LookAt(const Point &p, TransformSpace relativeTo)
{
    switch(relativeTo) {
    default:
    case TS_WORLD:
        target_ = p;
        break;
    case TS_PARENT:
        target_ = parent_ == nullptr ? p : parent_->GetLocalToWorldTransform() * p;
        break;
    case TS_LOCAL:
        target_ = GetLocalToWorldTransform() * p;
        break;
    }

    return SceneNode::LookAt(p, relativeTo);
}

void Camera::RotateAround(const Point &target, const Vector &d_vector, TransformSpace relativeTo)
{
    switch(relativeTo) {
    default:
    case TS_WORLD:
        target_ = target;
        break;
    case TS_PARENT:
        target_ = parent_ == nullptr ? target : parent_->GetLocalToWorldTransform() * target;
        break;
    case TS_LOCAL:
        target_ = GetLocalToWorldTransform() * target;
        break;
    }
    target_ = target;
    return SceneNode::RotateAround(target, d_vector);
}

void Camera::set_framebuffer(ork::ptr<ork::FrameBuffer> framebuffer)
{
    framebuffer_ = framebuffer;
}

//std::shared_ptr<SceneNodeManipulator> Camera::camera_manipulator() const
//{
//    return camera_manipulator_;
//}

Scalar Camera::GetObjectDistance() const
{
    return (target_-GetPointInWorld()).norm();
}

Point Camera::GetObjectDirection() const
{
    return (target_ - GetPointInWorld()) / GetObjectDistance();
}

Scalar Camera::GetRatioWorldView()
{
    Point p1;
    p1[0] = target_[0];
    p1[1] = target_[1];
    p1[2] = target_[2];
    auto pV = WorldToViewplane(p1);
    pV -= WorldToViewplane(p1 + GetRealRight());
    return (pV).norm()/(GetRealRight()).norm();
}

Vector Camera::ViewplaneToWorld(Scalar x, Scalar y)
{
    UpdateViewplane();

    Scalar ratioX = x/(Scalar)(GetActualWidth ());
    Scalar ratioY = 1.0 - (y/(Scalar)(GetActualHeight()));

    return viewplaneWorldOrigin_ + ratioX * viewplaneXVector_ + ratioY * viewplaneYVector_;
}

Vector Camera::ViewplaneToWorld(const Point2 &p)
{
    return ViewplaneToWorld(p[0], p[1]);
}

Point2 Camera::WorldToViewplane(const Point &p){
    Vector4 c = GetWorldToScreen() * Point4(p[0], p[1], p[2], 1.0);
    Scalar x = ((1.0 + (c[0] / c[3])) * GetActualWidth() / 2.0);
    Scalar y = ((1.0 - (c[1] / c[3])) * GetActualHeight() / 2.0);
    return Point2(x, y);
}

Point2 Camera::WorldToNormalizedViewplane(const Point &p) {
    Vector4 c = GetWorldToScreen() * Point4(p[0], p[1], p[2], 1.0);
    return Point2(c[0] / c[3], c[1] / c[3]);
}

Point2 Camera::GetNormalizedScreenCoordinate(const Point2 &p)
{
    Scalar X = (Scalar)(p[0] / GetActualWidth()) * 2.0 - 1.0;
    Scalar Y = -(Scalar)((p[1] / GetActualHeight()) * 2.0 - 1.0);
    return Point2(X, Y);
}

Point2 Camera::GetWindowCoordinate(const Point2 &p)
{
    Scalar X = (p[0] + 1.0) * GetActualWidth() / 2.0;
    Scalar Y = (p[1] + 1.0) * GetActualHeight() / 2.0;
    return Point2(X, Y);
}

//void Camera::set_camera_manipulator(std::shared_ptr<SceneNodeManipulator> camera_manipulator)
//{
//    camera_manipulator_ = camera_manipulator;
//}

BasicRay Camera::GetBasicRay(Point2 p)
{
    return GetBasicRay(p[0], p[1]);
}


void Camera::center_target()
{
//    Point position = GetPointInWorld();
//    Scalar d = (target_ - position).dot(GetRealDirection());
////    set_target(position + GetRealDirection() * d);
//    ResetOrientation();
//    LookAt(position + GetRealDirection() * d, SceneNode::TS_WORLD);
}


BasicRay Camera::GetBasicRay(Scalar x, Scalar y)
{
    Point position = GetPointInWorld();

    Vector aux = ViewplaneToWorld(x, y) - position;
    aux.normalize();

    // Create the Ray
    return BasicRay(position, aux);
}

Point Camera::GetPointInParallelWorldPlane(const Point &p, Scalar x, Scalar y)
{
    Point n = GetRealDirection(); //position() - GetPointInWorld(Point(0.0, 0.0, -1.0)); // normal of the camera in world space
    BasicRay r = GetBasicRay(x, y);
    Scalar dist = 0.0;
    if (r.IntersectsPlane(p, n, dist)) {
        return GetPointInWorld() + r.direction() * dist;
    }

    return Point::Zero();
}

void Camera::ZoomOnTarget(Scalar dist)
{
    // Store Target
    Point T = target_;

    // Translate
    Vector dir = (GetPointInWorld()-target_);
    if (std::abs(dir.norm() - dist) < 0.000001) { // If translation would bring camera on its target, make sure it doesn't happen.
        dist *= 2.0;
    }
    Translate(dist * dir.normalized(), TS_WORLD);
//    pretty_print(dist * -GetPointInLocal(target_).normalized());
//    Translate((1.-ratio)*(GetPointInWorld() - target_), TS_WORLD);
//    Translate((1.-ratio)*(position_ - target_), true);

    // Pop Target
    target_ = T;
}

void Camera::reset()
{
    target_ = initial_target_;
    SetPos(initial_position_);
    LookAt(initial_target_);
}

void Camera::SetFrontView ()
{
    SetPos(GetFrontView());
    LookAt(target_);
}

void Camera::SetBackView  ()
{
    SetPos(GetBackView());
    LookAt(target_);
}

void Camera::SetTopView   ()
{
    SetPos(GetTopView());
    LookAt(target_);
}

void Camera::SetBottomView()
{
    SetPos(GetBottomView());
    LookAt(target_);
}

void Camera::SetLeftView  ()
{
    SetPos(GetLeftView());
    LookAt(target_);
}

void Camera::SetRightView ()
{
    SetPos(GetRightView());
    LookAt(target_);
}

void Camera::SetRecordedView()
{
    SetPos(recordedPosition_);
    LookAt(recordedTarget_);

}

Scalar Camera::GetDistanceToTarget() const
{
    return (GetPointInWorld() - target_).norm();
}

Point Camera::GetDefaultView () const { return initial_position_; }
Point Camera::GetFrontView   () const { return Point(target_[0], target_[1], target_[2] + GetDistanceToTarget()); }
Point Camera::GetBackView    () const { return Point(target_[0], target_[1], target_[2] - GetDistanceToTarget()); }
Point Camera::GetTopView     () const { return Point(target_[0], target_[1] + GetDistanceToTarget(), target_[2]); }
Point Camera::GetBottomView  () const { return Point(target_[0], target_[1] - GetDistanceToTarget(), target_[2]); }
Point Camera::GetLeftView    () const { return Point(target_[0] - GetDistanceToTarget(), target_[1], target_[2]); }
Point Camera::GetRightView   () const { return Point(target_[0] + GetDistanceToTarget(), target_[1], target_[2]); }
Point Camera::GetRecordedView() const { return recordedPosition_; }

void Camera::RecordCurrentView()
{
    recordedPosition_ = this->GetPointInWorld();
    recordedTarget_ = target_;

}

void Camera::Yaw(Scalar r, TransformSpace relativeTo)
{
    Scalar distToTarget = (GetPointInWorld() - target()).norm();
    SceneNode::Yaw(r, relativeTo);
//    target_ = GetPointInWorld(Point(0.0, 0.0, -distToTarget));
    target_ = localToWorld_ * Point(0.0, 0.0, -distToTarget);
}

void Camera::Pitch(Scalar r, TransformSpace relativeTo)
{
    Scalar distToTarget = (GetPointInWorld() - target()).norm();
    SceneNode::Pitch(r, relativeTo);
//    target_ = GetPointInWorld(Point(0.0, 0.0, -distToTarget));
    target_ = localToWorld_ * Point(0.0, 0.0, -distToTarget);
}

void Camera::Roll(Scalar r, TransformSpace relativeTo)
{
    Scalar distToTarget = (GetPointInWorld() - target()).norm();
    SceneNode::Roll(r, relativeTo);
//    target_ = GetPointInWorld(Point(0.0, 0.0, -distToTarget));
    target_ = localToWorld_ * Point(0.0, 0.0, -distToTarget);
}

Matrix4 Camera::GetCameraToScreen() const
{
    UpdateTransformationMatrices();
    return cameraToScreen_;
}

ProjectiveTransform Camera::GetCameraToScreenTransform() const
{
    UpdateTransformationMatrices();
    return ProjectiveTransform(cameraToScreen_);
}

Matrix4 Camera::GetWorldToScreen() const
{
    UpdateTransformationMatrices();
    return worldToScreen_;
}

ProjectiveTransform Camera::GetWorldToScreenTransform() const
{
    UpdateTransformationMatrices();
    return ProjectiveTransform(worldToScreen_);
}

Matrix4 Camera::GetScreenToWorld() const
{
    UpdateTransformationMatrices();
    return screenToWorld_;
}

ProjectiveTransform Camera::GetScreenToWorldTransform() const
{
    UpdateTransformationMatrices();
    return ProjectiveTransform(screenToWorld_);
}

Matrix4 Camera::GetScreenToCamera() const
{
    UpdateTransformationMatrices();
    return screenToCamera_;
}

ProjectiveTransform Camera::GetScreenToCameraTransform() const
{
    UpdateTransformationMatrices();
    return ProjectiveTransform(screenToCamera_);
}

void Camera::InvalidateTransformationMatrices()
{
    viewplaneUpdated_ = false;
    SceneNode::InvalidateTransformationMatrices();
}

void Camera::UpdateViewplane()
{
    if (! viewplaneUpdated_) {
        Vector4 c0tmp = GetScreenToWorld() * Vector4( 1.0,-1.0, near_clip_distance_, 1.0);
        Vector4 c1tmp = screenToWorld_ * Vector4(-1.0,-1.0, near_clip_distance_, 1.0);
        Vector4 c2tmp = screenToWorld_ * Vector4(-1.0, 1.0, near_clip_distance_, 1.0);
    //    Vector c3 = screenToWorld_ * Vector( 1.0,-1.0, -near_clip_distance_);

        viewplaneWorldOrigin_ = c1tmp.xyzw();

        Vector3 c0 = c0tmp.xyzw();
        Vector3 c2 = c2tmp.xyzw();

        viewplaneXVector_ = (c0 - viewplaneWorldOrigin_);
        viewplaneYVector_ = (c2 - viewplaneWorldOrigin_);
    }
    viewplaneUpdated_ = true;
}

void Camera::UpdateTransformationMatrices(bool update_children) const
{
    if (!matricesUpdated_) {
        if (target_node_ != nullptr) {
            target_node_->SetPos(target_);
        }

        SceneNode::UpdateTransformationMatrices(update_children);
        UpdateCameraToScreen();
    }
}

void Camera::UpdateCameraToScreen() const
{
    Scalar width  = (Scalar) GetActualWidth();
    Scalar height = (Scalar) GetActualHeight();

    Scalar vfov = degrees(2.0 * atan((height / width) * tan(radians(fov_ / 2.0))));
    cameraToScreen_ = VectorTools::PerspectiveProjectionMatrix(vfov, width / height, near_clip_distance_, far_clip_distance_);
    screenToCamera_ = cameraToScreen_.inverse();

    screenToWorld_ = GetLocalToWorld() * GetScreenToCamera();
    worldToScreen_ = GetCameraToScreen() * GetWorldToLocal();//screenToWorld_.inverse();
//    worldToScreen_ = cameraToScreen_ * worldToLocal_;


}

} // namespace core

} // namespace expressive
