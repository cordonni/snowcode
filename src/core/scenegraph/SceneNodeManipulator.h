/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_SCENENODEMANIPULATOR_H_
#define EXPRESSIVE_SCENENODEMANIPULATOR_H_

#include "core/CoreRequired.h"

namespace expressive {

namespace core {

class InputController;
class KeyBindingsList;
class SceneNode;

class CORE_API SceneNodeManipulator {
public:
    enum TouchState { T_IDLE, T_TRANSLATION, T_ROTATION, T_TR_AND_ROT, T_SCALING, T_2H_TRANSLATION, T_2H_SCALING};
    EXPRESSIVE_MACRO_NAME("SceneNodeManipulator")

    // Constructor / Destructor
    SceneNodeManipulator();
    virtual ~SceneNodeManipulator();

    // Mouse Events
    virtual bool wheelEvent        (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr);
    virtual bool mousePressed      (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr);
    virtual bool mouseReleased     (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr);
    virtual bool mouseMoved        (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr);
    virtual bool mouseDoubleClicked(std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr);

    // Keyboard Events
    virtual bool keyPressed (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr);
    virtual bool keyReleased(std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr);

    // Touch Events
    virtual bool touchBegin (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr);
    virtual bool touchUpdate(std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr);
    virtual bool touchEnd   (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr);

    virtual bool penBegin (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr, Scalar /*pen_size*/ =-1);
    virtual bool penUpdate(std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr, Scalar /*pen_size*/ =-1);
    virtual bool penEnd   (std::shared_ptr<InputController> /*event*/, std::shared_ptr<SceneNode> /*n*/ = nullptr);

    virtual bool redisplay(double t, double dt);
    virtual KeyBindingsList GetKeyBindings() const;

};

} // namespace core

} // namespace expressive

#endif // EXPRESSIVE_SCENENODEMANIPULATOR_H_
