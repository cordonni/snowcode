#include "core/scenegraph/Overlay.h"
#include "core/scenegraph/SceneManager.h"

#include "core/ExpressiveEngine.h"
#include "core/geometry/MeshTools.h"

#include "ork/render/Texture2D.h"

using namespace ork;

namespace expressive
{

namespace core
{

Overlay::Overlay() : SceneNode("Overlay")
{
}

Overlay::Overlay(SceneManager *owner, const std::string &name, OverlayLocation loc, const std::string &method, const std::string &tex)
    : SceneNode(owner, nullptr, name, StaticName(), Vector::Ones(), false)
{
    Init(loc, method, tex);
}

Overlay::Overlay(SceneManager *owner, const std::string &name, OverlayLocation loc, ork::ptr<ork::TaskFactory> method, ork::ptr<ork::Texture2D> tex)
    : SceneNode(owner, nullptr, name, StaticName(), Vector::Ones(), false)
{
    Init(loc, method, tex);
}

Overlay::~Overlay()
{
}

void Overlay::Init(OverlayLocation loc, const std::string &method, const std::string &texName)
{
    ork::ptr<ork::TaskFactory> m = owner_->resource_manager()->loadResource(method).cast<TaskFactory>();
    ork::ptr<ork::Texture2D> t   = (texName.size() != 0) ? owner_->resource_manager()->loadResource(texName).cast<Texture2D>() : NULL;
    return Init(loc, m, t);
}

void Overlay::Init(OverlayLocation loc, ork::ptr<ork::TaskFactory> method, ork::ptr<ork::Texture2D> tex)
{
    this->set_tri_mesh(MeshTools::CreateQuad(Point::Zero()));
    this->tri_mesh()->FillColor(Color::White());

    switch(loc) {
    case O_BACKGROUND:
        node_->addFlag("background");
        break;
    case O_3DOVERLAY:
        node_->addFlag("overlay3d");
        break;
    case O_FOREGROUND:
        node_->addFlag("overlay");
        break;
    default:
        break;
    }

    node_->addMethod("draw", new Method(method));
    if (tex != nullptr) {
        node_->addValue(new ValueSampler(ork::VEC2F, "tex", tex));
        SetModule(ExpressiveEngine::GetSingleton()->resource_manager()->loadResource("texturedOverlay").cast<Module>());
    } else {
        SetModule(ExpressiveEngine::GetSingleton()->resource_manager()->loadResource("overlay").cast<Module>());
    }
    internal_node()->addValue(new Value2f("pos", Vector2f(-1.0, -1.0)));
    internal_node()->addValue(new Value2f("scale", Vector2f(2.0, 2.0)));
    set_visible(true);
}

void Overlay::removeDefaultValues()
{
    internal_node()->removeValue("pos");
    internal_node()->removeValue("scale");
    internal_node()->removeValue("visible");
}

}

}
