/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef CAMERAMANAGER_H
#define CAMERAMANAGER_H

#include <core/CoreRequired.h>
#include <core/utils/ListenerT.h>
#include <core/scenegraph/SceneManager.h>

namespace expressive
{

namespace core
{

typedef ListenerT<Camera> CameraListener;

/**
 * @brief The Camera class is a specific scene node that also handles a Projection matrix.
 * It has a position and a target.
 * A CameraManipulator will then be used to edit the camera (move, rotate, lookat, ...).
 */
class CORE_API Camera : public SceneNode {
public:
    
    EXPRESSIVE_MACRO_NAME("Camera")

    //////////////////////////////
    // Constructeur/Destructeur //
    //////////////////////////////
    Camera(SceneManager* scene_manager, const Vector &position, const Vector &target);

    virtual ~Camera();

    ///////////////
    // Getters   //
    ///////////////

    virtual unsigned int GetActualWidth () const;
    virtual unsigned int GetActualHeight() const;

//    virtual Point position() const { return position_; }
    const Point& target  () const;

    const ork::ptr<ork::FrameBuffer> framebuffer() const;

    Scalar fov() const;

    Scalar vfov() const;

    Scalar near_distance() const;

    Scalar far_clip_distance() const;

    bool target_visibility() const;

    virtual Point  GetObjectDirection() const;
    virtual Scalar GetObjectDistance () const;

    virtual Scalar GetRatioWorldView ();

    virtual Vector ViewplaneToWorld(Scalar x, Scalar y);
    virtual Vector ViewplaneToWorld(const Point2 &p);
    virtual Point2 WorldToViewplane          (const Point &p);
    virtual Point2 WorldToNormalizedViewplane(const Point &p);

    /**
     * Normalizes coordinates from window size.
     * ranges : [0-800; 0-640] -> [-1,1; -1;1]
     */
    virtual Point2 GetNormalizedScreenCoordinate(const Point2 &p);
    virtual Point2 GetWindowCoordinate(const Point2 &p);

    ///////////////
    // Setters   //
    ///////////////

    // Target
    virtual void set_target_distance(Scalar dist);
    virtual void set_target(const Point &target, TransformSpace relativeTo = TS_WORLD);
    virtual void set_framebuffer(ork::ptr<ork::FrameBuffer> framebuffer);
    virtual void center_target();
    virtual void set_far_clip_distance(Scalar dist);
    virtual void set_near_distance(Scalar dist);
    virtual void set_fov(Scalar fov);
    void set_target_visibility(bool b);

    //////////////
    // Movement //
    //////////////

    /**
     * Rotates this SceneNode to have it "look at" a given position, completely ignoring previous rotation.
     * @param p The point to look at.
     * @param local if true, #p will be considered as in local coordinates. Otherwise, it will be converted from world coordinates to local coordinates.
     */
    virtual void LookAt   (const Point &p, TransformSpace relativeTo = TS_WORLD);

    /**
     * Translates this SceneNode by given Vector.
     * @param v The vector used to translate this SceneNode.
     * @param local If true, translation will occur in local space. Otherwise, in world space.
     */
    virtual void Translate(const Point &v, TransformSpace relativeTo = TS_PARENT);

    // Rotation
    virtual void RotateAround(const Point &target, const Vector &d_vector, TransformSpace relativeTo = TS_WORLD);

    // Zoom
    void ZoomOnTarget(Scalar ratio);

    BasicRay GetBasicRay(Scalar x, Scalar y);
    BasicRay GetBasicRay(Point2 p);

    /**
     * @brief GetPointInParallelWorldPlane Returns the coordinates of screen point x;y in
     * world coordinates that is on the plane parralel to this node & passing through p.
     * @param p
     * @param x
     * @param y
     * @return
     */
    Point GetPointInParallelWorldPlane(const Point &p, Scalar x, Scalar y);

    //////////////
    // View     //
    //////////////

    virtual void reset();
    virtual void SetFrontView ();
    virtual void SetBackView  ();
    virtual void SetTopView   ();
    virtual void SetBottomView();
    virtual void SetLeftView  ();
    virtual void SetRightView ();
    virtual void SetRecordedView();
    virtual void RecordCurrentView();

    Scalar GetDistanceToTarget() const;
    Point GetDefaultView () const;
    Point GetFrontView   () const;
    Point GetBackView    () const;
    Point GetTopView     () const;
    Point GetBottomView  () const;
    Point GetLeftView    () const;
    Point GetRightView   () const;
    Point GetRecordedView() const;

    virtual void Yaw(Scalar r, TransformSpace relativeTo = TS_LOCAL);
    virtual void Pitch(Scalar r, TransformSpace relativeTo = TS_LOCAL);
    virtual void Roll(Scalar r, TransformSpace relativeTo = TS_LOCAL);

    /**
     * See SceneNode#UpdateTransformationMatrices().
     * Updates Camer/Screen and World/Screen stuff too.
     */
    virtual void UpdateTransformationMatrices(bool update_children = false) const;

    /**
     * Updates the CameraToScreen transform.
     * Should be called when resizing framebuffer's size.
     */
    void UpdateCameraToScreen() const;

    /**
     * Returns the transformation matrix from this camera to the screen.
     */
    virtual Matrix4 GetCameraToScreen() const;

    /**
     * Returns the transformation from this camera to the screen.
     */
    virtual ProjectiveTransform GetCameraToScreenTransform() const;

    /**
     * Returns the transformation matrix from the root node to the screen.
     */
    virtual Matrix4 GetWorldToScreen() const;

    /**
     * Returns the transformation from the root node to the screen.
     */
    virtual ProjectiveTransform GetWorldToScreenTransform() const;

    /**
     * Returns the transformation matrix from the screen to the root node.
     */
    virtual Matrix4 GetScreenToWorld() const;

    /**
     * Returns the transformation from the screen to the root node.
     */
    virtual ProjectiveTransform GetScreenToWorldTransform() const;

    /**
     * Returns the transformation matrix from the screen to this Camera.
     */
    virtual Matrix4 GetScreenToCamera() const;

    /**
     * Returns the transformation from the screen to this Camera.
     */
    virtual ProjectiveTransform GetScreenToCameraTransform() const;

    /**
      * Invalidates this node's current transformation matrices.
      */
     void InvalidateTransformationMatrices();

protected:
    Camera();

    virtual void Init(bool add_default_resources = true);

    virtual void UpdateViewplane();

    ork::ptr<ork::FrameBuffer> framebuffer_;

    Point initial_position_; // in world coordinates.
    Point initial_target_; // in world coordinates.
    Point target_; // in world coordinates.

    std::shared_ptr<SceneNode> target_node_; //< If visible, target will be displayed as a small sphere.

    mutable Matrix4 cameraToScreen_;

    mutable Matrix4 screenToCamera_;

    mutable Matrix4 screenToWorld_;

    mutable Matrix4 worldToScreen_;

    mutable Vector viewplaneWorldOrigin_;

    mutable Vector viewplaneXVector_;

    mutable Vector viewplaneYVector_;

    mutable bool viewplaneUpdated_;

    Scalar near_clip_distance_;

    Scalar far_clip_distance_;

    Scalar fov_;

    Vector recordedPosition_;
    Vector recordedTarget_;

};

} // namespace core

} // namespace expressive

#endif // CAMERAMAN_H
