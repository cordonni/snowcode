/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef CONTROLFRAME_H
#define CONTROLFRAME_H

#include <core/scenegraph/ObjectPrimitive.h>

namespace expressive
{

namespace core
{

/**
 * @brief The ControlFrame class is a SceneNode used to draw Frames.
 */
class CORE_API ControlFrame : public ObjectPrimitive
{
public:
    
    EXPRESSIVE_MACRO_NAME("ControlFrame")

    ControlFrame(core::SceneManager* scene_manager = nullptr,
                 SceneNode *parent = nullptr,
                 const std::string &name = std::string("ControlFrame_") + std::to_string(instance_counter_++),
                 std::shared_ptr<core::SceneNodeManipulator> manipulator = nullptr,
                 core::Frame f = core::Frame());


    virtual bool ChildSelected(const uint id);
    virtual void Deselect();


    virtual bool Translate(const Vector &v);
    virtual bool Rotate(const Vector &v) ;

    inline bool HasSelectedSubparts() const {return (X_cone_selected_ || Y_cone_selected_ || Z_cone_selected_ || base_selected_);}


    expressive::Transform CreateTranslation() const;
    expressive::Transform CreateRotation() const;


private:

    bool X_cone_selected_;
    bool Y_cone_selected_;
    bool Z_cone_selected_;
    bool base_selected_;

    uint X_cone_node_id_;
    uint Y_cone_node_id_;
    uint Z_cone_node_id_;
    uint X_cylindre_node_id_;
    uint Y_cylindre_node_id_;
    uint Z_cylindre_node_id_;
    uint sphere_node_id_;


    static const std::string X_CONE_EXT;
    static const std::string Y_CONE_EXT;
    static const std::string Z_CONE_EXT;
    static const std::string X_CYLINDRE_EXT;
    static const std::string Y_CYLINDRE_EXT;
    static const std::string Z_CYLINDRE_EXT;
    static const std::string SPHERE_EXT;

    static const double RADIUS;

};


} // namespace core

} // namespace expressive


#endif // CONTROLFRAME_H
