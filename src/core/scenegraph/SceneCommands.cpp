#include "core/scenegraph/SceneCommands.h"
#include "core/ExpressiveEngine.h"

#include "ork/taskgraph/MultithreadScheduler.h"


#include "core/geometry/AbstractTriMesh.h"
#include "core/geometry/BasicRay.h"

using namespace ork;
using namespace std;

namespace expressive
{

namespace core
{

MoveNodeCommand::MoveNodeCommand(SceneManager *scene, const SceneManager::NodeId &id, const Point &pos, const Point &target, const Point &original_pos, const Point &original_target, Command *parent) :
    Command("MoveNodeCommand", parent),
    m_scene(scene),
    m_id(id),
    m_pos(pos),
    m_target(target),
    m_original_pos(original_pos),
    m_original_target(original_target)
{
}

MoveNodeCommand::~MoveNodeCommand()
{
}

bool MoveNodeCommand::execute()
{
    auto n = m_scene->get_node(m_id);
    if (n == nullptr && m_id == m_scene->current_camera()->id()) {
        n = m_scene->current_camera();
    }
    if (n == nullptr) {
        return false;
    }

    n->SetPos(m_pos);
    n->LookAt(m_target);

    return true;
}

bool MoveNodeCommand::undo()
{
    auto n = m_scene->get_node(m_id);
    if (n == nullptr && m_id == m_scene->current_camera()->id()) {
        n = m_scene->current_camera();
    }
    if (n == nullptr) {
        return false;
    }
    n->SetPos(m_original_pos);
    n->LookAt(m_target);

    return true;
}

std::shared_ptr<Command> MoveNodeCommand::getPart(double begin, double end)
{
    Point begin_pos = m_original_pos + (m_pos - m_original_pos) * begin / 100.0;
    Point end_pos = m_original_pos + (m_pos - m_original_pos) * end / 100.0;
    Point begin_target = m_original_target + (m_target - m_original_target) * begin / 100.0;
    Point end_target  = m_original_target + (m_target - m_original_target) * end / 100.0;
    if ((end_pos - end_target).norm() < 0.00001) {
        end_pos = begin_pos;
    }
    return std::make_shared<MoveNodeCommand>(m_scene, m_id, end_pos, end_target, begin_pos, begin_target, this);
}

RotateNodeCommand::RotateNodeCommand(SceneManager *scene, const SceneManager::NodeId &id, const Point &up, const Point &right, const Point &cent, Scalar valX, Scalar valY, Command *parent) :
    Command("RotateNodeCommand", parent),
    scene_(scene),
    up_(up),
    right_(right),
    cent_(cent),
    valX_(valX),
    valY_(valY)
{
    node_ids_.insert(id);
}

RotateNodeCommand::RotateNodeCommand(SceneManager *scene, const std::set<SceneManager::NodeId> &ids, const Point &up, const Point &right, const Point &cent, Scalar valX, Scalar valY, Command *parent) :
    Command("RotateNodeCommand", parent),
    scene_(scene),
    node_ids_(ids),
    up_(up),
    right_(right),
    cent_(cent),
    valX_(valX),
    valY_(valY)
{
}

RotateNodeCommand::~RotateNodeCommand()
{
}

bool RotateNodeCommand::execute()
{
    for (SceneManager::NodeId id : node_ids_) {
        std::shared_ptr<SceneNode> n = scene_->get_node(id);
        n->Rotate(up_, cent_, valX_, SceneNode::TS_WORLD);
        n->Rotate(right_, cent_, valY_, SceneNode::TS_WORLD);
        scene_->NotifyUpdate();
    }
    return true;
}

bool RotateNodeCommand::undo()
{
    for (SceneManager::NodeId id : node_ids_) {
        std::shared_ptr<SceneNode> n = scene_->get_node(id);
        n->Rotate(right_, cent_, -valY_, SceneNode::TS_WORLD);
        n->Rotate(up_, cent_, -valX_, SceneNode::TS_WORLD);
        scene_->NotifyUpdate();
    }
    return true;
}

std::shared_ptr<Command> RotateNodeCommand::getPart(double begin, double end)
{
    Scalar x = (end - begin) * valX_ / 100.0;
    Scalar y = (end - begin) * valY_ / 100.0;
    return std::make_shared<RotateNodeCommand>(scene_, node_ids_, up_, right_, cent_, x, y, this);
}

ScaleNodeCommand::ScaleNodeCommand(SceneManager *scene, const SceneManager::NodeId &id, Point scale, Command *parent ) :
    Command("ScaleNodeCommand", parent),
    scene_(scene),
    scale_(scale)
{
    node_ids_.insert(id);
}

ScaleNodeCommand::ScaleNodeCommand(SceneManager *scene, const SceneManager::NodeId &id, Scalar scaleX, Scalar scaleY, Scalar scaleZ, Command *parent ) :
    Command("ScaleNodeCommand", parent),
    scene_(scene),
    scale_(scaleX, scaleY, scaleZ)
{
    node_ids_.insert(id);
}

ScaleNodeCommand::ScaleNodeCommand(SceneManager *scene, const std::set<SceneManager::NodeId> &ids, Point scale, Command *parent ) :
    Command("ScaleNodeCommand", parent),
    scene_(scene),
    node_ids_(ids),
    scale_(scale)
{
}

ScaleNodeCommand::ScaleNodeCommand(SceneManager *scene, const std::set<SceneManager::NodeId> &ids, Scalar scaleX, Scalar scaleY, Scalar scaleZ, Command *parent) :
    Command("ScaleNodeCommand", parent),
    scene_(scene),
    node_ids_(ids),
    scale_(scaleX, scaleY, scaleZ)
{
}

ScaleNodeCommand::~ScaleNodeCommand()
{
}

bool ScaleNodeCommand::execute()
{
    for (SceneManager::NodeId id : node_ids_) {
        std::shared_ptr<SceneNode> n = scene_->get_node(id);
        n->Scale(scale_/*, n->bounding_box().ComputeCenter()*/);
//        std::cout << "SCALE AROUND \n" << n->bounding_box().ComputeCenter() << std::endl;
        scene_->NotifyUpdate();
    }
    return true;
}

bool ScaleNodeCommand::undo()
{
    for (SceneManager::NodeId id : node_ids_) {
        std::shared_ptr<SceneNode> n = scene_->get_node(id);
        n->Scale(-scale_, n->bounding_box().ComputeCenter());
        scene_->NotifyUpdate();
    }
    return true;
}

std::shared_ptr<Command> ScaleNodeCommand::getPart(double begin, double end)
{
    Scalar x = (end - begin) * scale_[0] / 100.0;
    Scalar y = (end - begin) * scale_[1] / 100.0;
    Scalar z = (end - begin) * scale_[2] / 100.0;
    return std::make_shared<ScaleNodeCommand>(scene_, node_ids_, Point(x, y, z), this);
}

UpdateMeshCommand::DummyTask::DummyTask(std::shared_ptr<UpdateMeshCommand> owner, bool undo) :
    ork::Task("UpdateMeshTask", false, 0),
    owner_(owner),
    undo_(undo)
{
}

UpdateMeshCommand::DummyTask::~DummyTask()
{
}

bool UpdateMeshCommand::DummyTask::run()
{
    if (undo_) {
        owner_->undoFromTask();
    } else {
        owner_->executeFromTask();
    }
    return true;
}

//UpdateMeshCommand::UpdateMeshCommand(std::shared_ptr<AbstractTriMesh> mesh, const char *name) :
//    Command(name),
//    mesh_(mesh)
//{
//}

UpdateMeshCommand::UpdateMeshCommand(SceneManager *scene, const SceneManager::NodeId &id, const char *name, Command *parent) :
    Command(name, parent),
    scene_(scene),
    lock_for_update_(true),
    do_garbage_collect_(true)
{
    m_undoable = false;
    ids_.insert(id);
}

UpdateMeshCommand::UpdateMeshCommand(SceneManager *scene, const std::set<SceneManager::NodeId> &ids, const char *name, Command *parent) :
    Command(name, parent),
    scene_(scene),
    ids_(ids),
    lock_for_update_(true),
    do_garbage_collect_(true)
{
    m_undoable = false;
}

UpdateMeshCommand::~UpdateMeshCommand()
{
}

bool UpdateMeshCommand::execute()
{
    ExpressiveEngine::GetSingleton()->scheduler()->schedule(new DummyTask(std::static_pointer_cast<UpdateMeshCommand>(shared_from_this())));
    return true;
}

bool UpdateMeshCommand::undo()
{
    ExpressiveEngine::GetSingleton()->scheduler()->schedule(new DummyTask(std::static_pointer_cast<UpdateMeshCommand>(shared_from_this()), true));
    return true;
}

std::shared_ptr<Command> UpdateMeshCommand::getPart(double begin, double end)
{
    UNUSED(begin);
    UNUSED(end);
    return shared_from_this();
}

bool UpdateMeshCommand::executeFromTask()
{
    for (const SceneManager::NodeId &id : ids_) {
        std::shared_ptr<SceneNode> n = scene_->get_node((id));
        if (n != nullptr) {
            if (n->tri_mesh() != nullptr) {
                if (lock_for_update_) {
                    n->tri_mesh()->get_buffers_mutex().lock();
                }
                if (do_garbage_collect_) {
                    n->tri_mesh()->garbage_collection();
                }
                n->internal_node()->setLocalBounds(n->tri_mesh()->bounding_box().toBox3());
                n->tri_mesh()->ForceUpdate();
                n->set_tri_mesh(n->tri_mesh());

                if (lock_for_update_) {
                    n->tri_mesh()->get_buffers_mutex().unlock();
                }
            }
        }
    }
    return true;
//    return false;
}

bool UpdateMeshCommand::undoFromTask()
{
    return executeFromTask();
}

TransformMeshCommand::TransformMeshCommand(SceneManager *scene, const SceneManager::NodeId &id, const Transform& t, const Matrix &trot, Command *parent) :
    UpdateMeshCommand(scene, id, "TransformMeshCommand", parent),
    transform_(t),
    rotation_transform_(trot)
{
    lock_for_update_ = false;
}

TransformMeshCommand::~TransformMeshCommand()
{
}

bool TransformMeshCommand::executeFromTask()
{
    std::shared_ptr<SceneNode> n = scene_->get_node(*(ids_.begin()));
    if (n != nullptr) {
        std::shared_ptr<AbstractTriMesh> m = n->tri_mesh();
        if (m != nullptr) {
            {
                std::lock_guard<std::mutex> mlock(m->get_buffers_mutex());
                AABBox b;

                for(AbstractTriMesh::VertexIter i = m->vertices_begin(); i != m->vertices_end(); i++)
                {
                    Point p = transform_ * m->point(*i);
                    b.Enlarge(p);
                    m->set_point(*i, p);

                    m->set_normal(*i, rotation_transform_ * m->normal(*i));
            //        tri_mesh_->set_normal(*i, t_normal * tri_mesh_->normal(*i));
                }
                m->update_bounding_box(b);
                UpdateMeshCommand::executeFromTask();
            }
//            n->force_rebuild();
            return true;
        }
    }
    return false;
}

bool TransformMeshCommand::undoFromTask()
{
    ork::Logger::ERROR_LOGGER->log("PROCEDURAL", "No getPart yet for TransformMesh commands !");
    return false;
}

} // namespace core

} // namespace expressive
