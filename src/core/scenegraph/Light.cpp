#include "core/scenegraph/Light.h"
#include "core/scenegraph/SceneManager.h"
#include "core/utils/ConfigLoader.h"


using namespace ork;

namespace expressive {
namespace core {

Light::Light(SceneManager *scene_manager, const Vector &position, const Vector &target) :
    SceneNode(scene_manager, nullptr, "light", "Light")
{
    SetPos(position);
    LookAt(target);
    Init();
}

Light::Light() :
    SceneNode("Light")
{
}

Light::~Light()
{
}

void Light::Init()
{
    node_->addFlag("light");
    node_->addModule("material", owner_->resource_manager()->loadResource(Config::getStringParameter("default_light_module")).cast<Module>());
    node_->addMethod("draw", new Method(owner_->resource_manager()->loadResource(Config::getStringParameter("default_light_method")).cast<TaskFactory>()));
}

}

}
