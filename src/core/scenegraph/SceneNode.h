/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_SCENENODE_H_
#define EXPRESSIVE_SCENENODE_H_

// core dependencies
#include "core/utils/Serializable.h"
#include "core/utils/AbstractIterator.h"
#include "core/utils/ListenerT.h"
//#include "core/geometry/BasicRay.h"
#include "core/geometry/AABBox.h"

#include "ork/scenegraph/SceneNode.h"

namespace expressive
{

namespace core
{

class AbstractTriMesh;
class SceneNode;
class SceneManager;
class SceneNodeManipulator;

typedef ork::SceneNode OrkSceneNode;

//template<class T> class BasicTriMeshT;
//class DefaultMeshVertex;
//typedef BasicTriMeshT<DefaultMeshVertex> BasicTriMesh;
class BasicRay;

class CORE_API SceneNode : public ListenableT<SceneNode>, public Serializable
{
public:
    EXPRESSIVE_MACRO_NAME("SceneNode")

    /** Enumeration denoting the spaces which a transform can be relative to.
    */
    enum TransformSpace
    {
        /// Transform is relative to the local space
        TS_LOCAL,
        /// Transform is relative to the space of the parent node
        TS_PARENT,
        /// Transform is relative to world space
        TS_WORLD
    };

    /**
     * An SceneNode identifier : a unique number assigned to each SceneNode.
     */
    typedef unsigned int NodeId;

    /**
     * A SceneNode container.
     */
    typedef std::set<std::shared_ptr<SceneNode> > NodePtrSet;

    /**
     * An iterator to iterate over a set of SceneNodes
     */
    typedef core::AbstractIterator<std::set<std::shared_ptr<SceneNode> >, std::shared_ptr<SceneNode> > ChildIterator;

    /**
     * Creates a new SceneNode.
     * @param owner The SceneManager that contains this SceneNode.
     * @param parent An optionnal parent SceneNode. When moved, this SceneNode will move too.
     * @param name The name of this SceneNode.
     * @param type The type of this SceneNode.
     * @param scale The scaling factor for this node.
     * @param addDefaultNodeFlags True if this node is a basicly drawn node : just a mesh. false otherwise : lights, camera...).
     */
    SceneNode(SceneManager *owner, SceneNode *parent, const std::string &name, const std::string &type = StaticName(), const Vector &scale = Vector::Ones(), bool addDefaultNodeFlags = true);

    /**
     * Deletes this SceneNode.
     */
    virtual ~SceneNode();

    ///////
    /// Listener stuff
    ///
    void NotifySelectedStateChanged()  const;
    void NotifyHoveredStateChanged()   const;
    void NotifyVisibilityStateChanged() const;
    void NotifyWireframeStateChanged() const;
    void NotifyAdjustSizeToParamsChanged() const;

    ///////
    /// ACCESSORS
    ///
    /**
     * @brief owner Returns this SceneNode's SceneManager.
     */
    SceneManager *owner() const;

    /**
     * Returns this SceneNode's parent.
     */
    SceneNode *parent() const;

    /**
     * Returns this SceneNode's ancestor (parent of parent of parent... until parent is null).
     */
    SceneNode *ancestor() const;

	/**
	 * Returns this SceneNode's closest common ancestor with target.
	 */
    SceneNode* closest_ancestor(const SceneNode* target);

    /**
     * Returns true if this node is the ancestor of target.
     */
    bool is_ancestor(const SceneNode *target) const;

    /**
     * Returns ork's internal node used to represent this node.
     */
    ork::ptr<OrkSceneNode> internal_node() const;

    /**
     * Returns this SceneNode's id.
     */
    NodeId id() const;

    /**
     * Returns this SceneNode's AbstractTriMesh.
     */
    std::shared_ptr<AbstractTriMesh> tri_mesh() const;

    /**
     * Returns an iterator on this SceneNode's children.
     */
    ChildIterator children() const;

    /**
     * Returns this SceneNode's number of children.
     */
    uint get_child_count() const;

    /**
     * Returns true if this SceneNode has children.
     */
    bool has_children() const;

    /**
     * Returns true if this node has the given child.
     */
    bool has_child(const NodeId &id) const;

    /**
     * Returns true if this SceneNode has a mesh.
     */
    bool has_mesh() const;

    /**
     * Returns true if this SceneNode adapts it's size to its parameters.
     */
    bool adjust_size_to_params() const;

    /**
     * Returns true if this SceneNode is visible.
     */
    virtual bool visible() const;

    /**
     * Returns true if this SceneNode's wireframe is visible.
     */
    virtual bool wireframe() const;

    /**
     * Returns this SceneNode's manipulation handler id.
     * This is the NodeId of the SceneNode that handles the manipulation of this SceneNode.
     * Default is this SceneNode, but can be overloaded for complex structures where an object will handle it's children.
     */
    virtual unsigned int manipulation_handler_id() const;

    /**
     * Returns this SceneNode's manipulator proxy : i.e. the SceneNode that is used to manipulate this SceneNode
     * For example, a GraphSceneNode is used to manipulate an ImplicitSurfaceSceneNode.
     */
    std::shared_ptr<SceneNode> manipulator_proxy() const;

    /**
     * Returns true if this SceneNode has a manipulator proxy.
     */
    bool HasManipulatorProxy() const;

    /**
     * Returns this SceneNode's SceneNodeManipulator.
     */
    virtual std::shared_ptr<SceneNodeManipulator> manipulator() const;

    /**
     * Returns this SceneNode's bounding box (AABBoxT<Scalar>).
     */
    virtual AABBox bounding_box() const;

    /**
     ** Returns this SceneNode's name.
     */
    std::string name() const;

    /**
     * Returns true if this SceneNode is selected. Returns false if both this SceneNode and its manipulator proxy are selected.
     */
    bool selected() const;

    /**
     * Returns true if this SceneNode is hovered. Returns false if both this SceneNode and its manipulator proxy are hovered.
     */
    bool hovered() const;

    /**
     * Returns true if the manipulator proxy or any of its children are selected.
     */
    bool HasSelectedManipulator() const;

    /**
     * Returns true if any of this SceneNode's children is hovered.
     */
    bool HasHoveredChild() const;

    /**
     * Returns true if any of this SceneNode's children is selected.
     */
    bool HasSelectedChild() const;

    //////
    /// SETTERS
    ///

    /**
     * Sets this node's parent. Caution : Make sure that parent's children are also updated.
     */
    void set_parent(SceneNode *new_parent);

    /**
     * Sets this SceneNode's AbstractTriMesh.
     * @param mesh The AbstractTriMesh attached to this SceneNode.
     */
    void set_tri_mesh(std::shared_ptr<AbstractTriMesh> mesh, const std::string &mesh_tag = "geometry");

    /**
     * Sets the selected state of this SceneNode.
     *
     * @param selected the selected state. Default is true.
     * @param hover_parent Sets the hovered state of the parent SceneNode.
     */
    virtual void set_selected(bool selected = true, bool hover_parent = true);

    /**
     * @brief set_hovered Sets the hovered state of this SceneNode.
     * @param hovered the hovered state. Default is true.
     * @param hover_parent Sets the hovered state of the parent SceneNode.
     */
    virtual void set_hovered(bool hovered = true, bool hover_parent = true);

    /**
     * @brief set_visible Sets the visible state of this SceneNode.
     * @param visible if true, this SceneNode will be displayed. Default is true.
     * @param applyToChildren  if true, new state will also be applied to children. Default is true.
     */
    virtual void set_visible(bool visible = true, bool applyToChildren = true);

    /**
     * @brief set_adjust_size_to_param Sets the adjust_size_to_params_ state of this SceneNode.
     * @param b if true, SceneNode will adapt its size to its parameters. Default is true.
     * Default behavior is no changes.
     */
    virtual void set_adjust_size_to_param(bool b = true);

    /**
     * @brief set_manipulator_proxy Sets the SceneNode used as a proxy for the manipulations.
     * @param proxy The proxy SceneNode.
     */
    void set_manipulator_proxy(std::shared_ptr<SceneNode> proxy);

    /**
     * @brief set_manipulator Sets the manipulator used to manipulate this SceneNode (or its proxy).
     * @param manipulator The SceneNodeManipulator used to manipulate or edit this SceneNode.
     */
    void set_manipulator(std::shared_ptr<SceneNodeManipulator> manipulator);

    /**
     * Sets the background state of this node. See #is_background.
     * @param backgroundif true, this SceneNode will not be interactible. Default is true.
     * @param applyToChildren  if true, new state will also be applied to children. Default is true.
     */
    void SetBackground(bool background = true, bool applyToChildren = true);

    /**
     * @brief ToggleWireframe Sets the wireframe state of this SceneNode.
     * @param visible if true, this SceneNode's wireframe will be displayed. Default is true.
     * @param applyToChildren  if true, new state will also be applied to children. Default is true.
     */
    virtual void ToggleWireframe(bool applyToChildren = true);
    virtual void DisplayWireframe(bool visible = true, bool applyToChildren = true);

    /**
     * @brief ToggleVisible Sets the wireframe state of this SceneNode.
     * @param visible if true, this SceneNode's wireframe will be displayed. Default is true.
     * @param applyToChildren  if true, new state will also be applied to children. Default is true.
     */
    virtual void ToggleVisible(bool applyToChildren = true);

    /**
     * @brief ShowBoundingbox Displays the bounding box of this SceneNode.
     * @param visible if true, this SceneNode's boundingbox will be displayed. Default is true.
     * @param applyToChildren  if true, new state will also be applied to children. Default is true.
     */
    virtual void ShowBoundingBox(bool visible = true, bool applyToChildren = true);

    ////
    /// Children stuff
    ///
    /**
     * @brief AddChild Makes an existing SceneNode a child of this SceneNode. Note this does NOT update #child's parent.
     * @param child A SceneNode. Make sure #child is not an ancestor of this SceneNode.
     * @param undoable Whether this action is undoable or not. This will create a SceneCommand undoable via ctrl+z. Default is true.
     */
    void AddChild(std::shared_ptr<SceneNode> child, bool undoable = true);

    /**
     * @brief RemoveChild Removes an existing SceneNode from the Scene.
     * @param child A SceneNode.
     * @param undoable Whether this action is undoable or not. This will create a SceneCommand undoable via ctrl+z. Default is true.
     */
    void RemoveChild(std::shared_ptr<SceneNode> child, bool undoable = true);

    /**
     * @brief RemoveChild Removes an existing SceneNode from the Scene.
     * See #RemoveChild.
     */
    void RemoveChild(SceneNode *child, bool undoable = true);

    /**
     * @brief RemoveAllChildren Removes every children of this SceneNode from the Scene.
     */
    void RemoveAllChildren();

    /**
     * @brief IsChildOf Returns true if this SceneNode is a child of given SceneNode.
     * @param parent The SceneNode to test against.
     */
    bool IsChildOf(SceneNode *parent) const;

//    /**
//     * @brief FindChild Returns an iterator containing this child
//     * @param to_find
//     * @return
//     */
//    ChildIterator FindChild(std::shared_ptr<SceneNode> to_find) { return ChildIterator(children_.find(to_find), children_.end()); }

    //////
    /// 3D positioning
    ///
    /**
     * Returns the transformation matrix from this node to the root node.
     */
    virtual Matrix4 GetLocalToWorld() const;

    /**
     * Returns the transformation matrix from this SceneNode to its parent (or to origin if this SceneNode has no parent)
     */
    virtual Matrix4 GetLocalToParent() const;

    /**
     * Returns the transformation matrix from this SceneNode to the camera.
     */
    virtual Matrix4 GetLocalToCamera() const;

    /**
     * Returns the transformation matrix from the root node to this SceneNode.
     */
    virtual Matrix4 GetWorldToLocal() const;

    /**
     * Returns the transformation matrix from #parent to this SceneNode.
     */
    virtual Matrix4 GetParentToLocal() const;

    /**
     * Returns the transformation matrix from the camera node to this SceneNode.
     */
    virtual Matrix4 GetCameraToLocal() const;

    /**
     * Returns the transformation matrix from this SceneNode's parent to the root node.
     */
    virtual Matrix4 GetParentToWorld() const;

    /**
     * Returns the transformation matrix from the root node to this SceneNode's parent.
     */
    virtual Matrix4 GetWorldToParent() const;

    /**
     * Returns the transformation from this node to the root node.
     */
    virtual Transform GetLocalToWorldTransform() const;

    /**
     * Returns the transformation from this SceneNode to its parent (or to origin if this SceneNode has no parent)
     */
    virtual Transform GetLocalToParentTransform() const;

    /**
     * Returns the transformation from this SceneNode to the camera.
     */
    virtual Transform GetLocalToCameraTransform() const;

    /**
     * Returns the transformation from the root node to this SceneNode.
     */
    virtual Transform GetWorldToLocalTransform() const;

    /**
     * Returns the transformation from #parent to this SceneNode.
     */
    virtual Transform GetParentToLocalTransform() const;

    /**
     * Returns the transformation from the camera node to this SceneNode.
     */
    virtual Transform GetCameraToLocalTransform() const;

    /**
     * Returns the node that intersects a given ray. (this node or one of its children).
     * @param ray A BasicRay, in world coordinates.
     * @param[out] dist Will contain the distance at which intersection is found, or expressive::kScalarMax if no intersection is found.
     * @param proxy If true, computation will also involve this SceneNode's manipulation proxy.
     * @return nullptr if no intersection found. Otherwise, if this node or one of its children is intersecting the ray, it is returned.
     */
    virtual SceneNode* IsIntersectingRay(const BasicRay &ray, Scalar &dist, bool proxy = true) const;

    /**
     * Returns this node's scale.
     */
    virtual Vector GetScale() const;

    /**
     * Returns the given World coordinates transformed in local coordinates.
     * @param p a Point in world coordinates. Default is Point::Zero().
     */
    virtual Point      GetPointInLocal  (const Point &p = Point::Zero()) const;

    /**
     * Returns the given local coordinates transformed in world coordinates.
     * @param p a Point in local coordinates. Default is Point::Zero().
     */
    virtual Point      GetPointInWorld  (const Point &p = Point::Zero()) const;

    /**
     * Returns the given BasicRay transformed in local coordinates.
     * @param ray A BasicRay in world coordinates.
     */
    virtual BasicRay   GetRayInLocal    (const BasicRay &ray) const;

    /**
     * Returns the Up vector transformed in world coordinates.
     * Vector(0.0, 0.0, 1.0).
     */
    virtual Point  GetRealUp         () const;

    /**
     * Returns the Right vector transformed in world coordinates.
     * Vector(1.0, 0.0, 0.0).
     */
    virtual Point  GetRealRight      () const;

    /**
     * Returns the direction vector transformed in world coordinates.
     * Vector(0.0, 1.0, 0.0).
     */
    virtual Point  GetRealDirection  () const;

    // Global Manipulation
    /**
     * Resets SceneNode rotation matrix, maintaining its position.
     */
    virtual void ResetOrientation();

    /**
     * Rotates this SceneNode to have it "look at" a given position, completely ignoring previous rotation.
     * @param p The point to look at.
     * @param relativeTo determines in which space the new target point should be considered.
     */
    virtual void LookAt   (const Point &p, TransformSpace relativeTo = TS_WORLD);

    /**
     * Moves SceneNode to a given position. Orientation is not changed.
     * @param p The point to move to.
     * @param local if true, #p will be considered as in local coordinates. Otherwise, it will be converted from world coordinates to local coordinates.
     */
    virtual void SetPos   (const Point &p, TransformSpace relativeTo = TS_WORLD);

    /**
     * Moves SceneNode to a given position. Orientation is not changed.
     * @param x x coordinates to move to.
     * @param y y coordinates to move to.
     * @param z z coordinates to move to.
     * @param local if true, #p will be considered as in local coordinates. Otherwise, it will be converted from world coordinates to local coordinates.
     */
    virtual void SetPos   (Scalar x, Scalar y, Scalar z, TransformSpace relativeTo = TS_WORLD);

    /**
     * Sets this SceneNode's scale along every direction by the same factor. Does not depend on current scale.
     * @param s the final scale.
     */
    virtual void SetScale (Scalar s);

    /**
     * Sets this SceneNode's scale along every direction.  Does not depend on current scale.
     */
    virtual void SetScale (const Point &s);

    /**
     * Resizes this SceneNode along every direction by the same factor.
     * @param s The factor that must be applied to this SceneNode's dimensions, on all 3 axis.
     */
    virtual void Scale    (Scalar s);

    /**
     * Resizes this SceneNode along every direction.
     * @param s A Point that contains, for each local axis, the resize factor.
     */
    virtual void Scale    (const Point &s);

    /**
     * Resizes this SceneNode along every direction. If #c is not the center of this SceneNode, this will also move the point away from / to #c. Mostly used when scaling multiple item at once and maintaining their relative positions.
     * @param s A Vector that contains, for each local axis, the resize factor.
     * @param c The center of the scaling operation.
     */
    virtual void Scale    (const Point &s, const Point &c);

    /**
     * Rotates this SceneNode around a given axis, by a given angle.
     * @param a The Axis to rotate around.
     * @param th The angle (in radians) of rotation around #a.
     */
    virtual void Rotate   (const Vector&a, Scalar th, TransformSpace relativeTo = TS_LOCAL);

    /**
     * Rotates this Scene around an axis, centered on a point.
     * @param a The axis to rotate around.
     * @param c The Point to Rotate around. In world coordinates.
     * @param th The angle of rotation.
     */
    virtual void Rotate   (const Vector&a, const Point &c, Scalar th, TransformSpace relativeTo = TS_LOCAL);

    /**
     * Rotates this scene node around a point, with angles defined in a defined vector.
     * @param target the point to rotate around
     * @param t the vector containing the angle values.
     */
    virtual void RotateAround(const Point &target, const Vector &t, TransformSpace relativeTo = TS_WORLD);

    /**
     * Rotates this scene node around its z axis.
     * @param r the angle (in radians) or rotation around the axis.
     */
    virtual void Yaw  (Scalar r, TransformSpace relativeTo = TS_LOCAL);

    /**
     * Rotates this scene node around its y axis.
     * @param r the angle (in radians) or rotation around the axis.
     */
    virtual void Pitch(Scalar r, TransformSpace relativeTo = TS_LOCAL);

    /**
     * Rotates this scene node around its x axis.
     * @param r the angle (in radians) or rotation around the axis.
     */
    virtual void Roll (Scalar r, TransformSpace relativeTo = TS_LOCAL);

    /**
     * Translates this SceneNode by given Vector.
     * @param v The vector used to translate this SceneNode.
     * @param local If true, translation will occur in local space. Otherwise, in world space.
     */
    virtual void Translate(const Point &v, TransformSpace relativeTo = TS_PARENT);

    /**
     * Invalidates this node's current transformation matrices.
     */
    virtual void InvalidateTransformationMatrices();

    /**
     * Invalidates this node's children's current transformation matrices.
     */
    virtual void InvalidateChildrenTransformationMatrices();

    /**
     * Updates transformation matrices.
     * By default, nodes have 3 transformation matrices :
     * - Rotation
     * - Translation
     * - Scale
     * All of them are set independently, relatively to the parent node (or root if no parent is set).
     * Once one is modified, User must update all other transformations (localToWorld, localToCamera, parentToLocal, ...).
     * Calling this method is mandatory when updating transformation. These should be updated upon request of concerned
     * transformations : The #matricesUpdated_ flag helps to keep this data updated with minimal recomputation required.
     */
    virtual void UpdateTransformationMatrices(bool update_children = false) const;

    /**
     * Updates the internal mesh if this node was updated.
     * i.e. if flag #required_rebuild is set to true.
     */
    virtual void UpdateMesh();

    /**
     * Updates every required stuff before drawing the SceneNode. This includes :
     * - The transformation matrices.
     * - The AbstractTriMesh's internal buffers. // < TODO : not true yet. Still looking for the optimal thing to do.
     */
    virtual void Update(bool update_children = true);

    /**
     * Saves this SceneNode to a given .obj file : only saves the mesh content.
     * @param name The file to save the mesh to.
     */
    virtual void SaveToObjFile(const std::string &name);

    /**
     * Forces the mesh to be recentered around it's bounding box's center (which might not always be the case).
     */
    virtual void TransferSceneDeformationToModel();

    /**
     * Adds the given module for this SceneNode.
     * @param module a module to add.
     * @param name the name of the module inside this node. default is material.
     */
    virtual void SetModule(ork::ptr<ork::Module> module, const std::string &name = "material");

protected:
    /**
     * Creates an empty SceneNode.
     * @param type The type of this SceneNode.
     */
    SceneNode(const std::string &type = StaticName());

    /**
     * Initialized this SceneNode's fields.
     * See SceneNode#SceneNode().
     */
    virtual void Init(const std::string &name, SceneManager *owner, SceneNode *parent, bool AddDefaultNodeFlags = true);

    /**
     * This removes every children from a given node. However, this doesn't remove the children from the scene, nor does it remove this node from it's children.
     * todo opengl : check if removeAllChildren is not what Ulysse wanted here.
     */
    void CleanAllChildren();

    /**
     * Adds default information on ork's SceneNode to have this SceneNode drawn correctly.
     * This includes :
     * - setting the "object" flag
     * - setting a proper "material" (i.e. a module).
     * - setting a proper "draw" method.
     */
    virtual void AddDefaultNodeFlags();

    /**
     * This SceneNode's SceneManager.
     */
    SceneManager *owner_;

    /**
     * This SceneNode's parent.
     */
    SceneNode *parent_;

    /**
     * Internal ork::SceneNode used to represent this node inside ork.
     */
    ork::ptr<OrkSceneNode> node_;

    /**
     * This SceneNode's manipulation proxy : Null by default.
     * When null, the default manipulation behavior is to simply apply rigid deformations to current SceneNode.
     * If one wants to create more advanced behavior, for example to update an item by updating it's skeleton, this is the way to go.
     * The proxy is the SceneNode that is actually manipulated when interacting with a SceneNode. The SceneNode is then supposed
     * to be updated depending on the status of the proxy.
     */
    std::shared_ptr<SceneNode> manipulator_proxy_;

    /**
     * This SceneNode's SceneNodeManipulator.
     * A SceneNodeManipulator is used to manipulate a SceneNode. The default one only applies rigid deformations on a given SceneNode,
     * but one can imagine many other type of manipulations.
     */
    std::shared_ptr<SceneNodeManipulator> manipulator_;

    /**
     * This node's name.
     */
    std::string name_;

    /**
     * This node's id. If 0, it means that it doesn't have an owner, or hasn't been initialized
     * (which should not happen !).
     */
    unsigned int id_;

    /**
     * This SceneNode's BasicTriMesh : a list of triangles used to display this SceneNode.
     */
    std::shared_ptr<AbstractTriMesh> tri_mesh_;

    //////
    /// 3D positioning

    /**
     * Only parentToLocal_, localRotation and scaleTransform_ are set manually, every other matrices are computed automatically.
     * To avoid unecessary computations, the matricesUpdated_ flag is set to false when position is updated.
     * When detecting this, when requesting a matrix, the SceneNode will update it's matrices and set the flag back to true.
     */
    mutable bool matricesUpdated_;

    /**
     * The translation Transform of the node relative to it's parent.
     */
    Transform position_;

    /**
     * The rotation Transform of the node relative to it's parent.
     */
    Transform orientation_;

    /**
     * The scale Transform of the node relative to it's parent.
     */
    Transform scaleTransform_;

    /**
     * Current scaling factors along each local axis.
     */
    Vector scale_;

    /**
     * The transformation from this SceneNode to it's parent.
     */
    mutable Transform localToParent_;

    /**
     * The transformation from this SceneNode to root node.
     */
    mutable Transform localToWorld_;

    /**
     * The transformation from this SceneNode to the camera.
     */
    mutable Transform localToCamera_;

    /**
     * The transformation from parent to this.
     */
    mutable Transform parentToLocal_;

    /**
     * The tranformation from root node to this.
     */
    mutable Transform worldToLocal_;

    /**
     * The transformation from camera node to this.
     */
    mutable Transform cameraToLocal_;

    /**
     * True if SceneNode is selected.
     */
    bool selected_;

    /**
     * True if SceneNode is hovered.
     */
    bool hovered_;

    /**
     * True if SceneNode is visible.
     */
    bool visible_;

    /**
     * True if wireframe is visible (also make the item non visible if you only want the wireframe).
     */
    bool wireframe_;

    /**
     * True if wireframe is visible (also make the item non visible if you only want the wireframe).
     */
    bool show_boundingbox_;

    /**
     * True if SceneNode is adjusted depending on its parameters.
     */
    bool adjust_size_to_params_;

    /**
     * True if selection state has changed since last rebuild.
     */
    bool selected_state_changed_;

    /**
     * True if this node is part of background : It won't be clickable or intersectable.
     */
    bool is_background_;

    /**
     * The list of this SceneNode's children.
     */
    NodePtrSet children_;

    /**
     * The list of this SceneNode's children ids.
     */
    std::set<NodeId> children_ids_;

    friend class SceneManager;
};

} // namespace core
} // namespace expressive

#endif // EXPRESSIVE_SCENENODE_H_
