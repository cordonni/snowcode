/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_OGRESCENENODE_FACTORY_H_
#define EXPRESSIVE_OGRESCENENODE_FACTORY_H_

#include "core/scenegraph/SceneNode.h"

namespace expressive
{

namespace core
{

// TODO : Generalize this !!!!
/**
 * @brief The SceneNodeFactory class
 * This factory is used to create SceneNodes from a given class type.
 * Types are defined as templates, which take two parameters : The source class,
 * and the class that allows to draw it in Ork's scene.
 * Note that the drawing class must have the following constructor parameters available :
 * ctor(const std::string &type, SceneManager* owner, core::SceneNode *parent, const string &name, std::shared_ptr<void> param);
 */
class CORE_API SceneNodeFactory
{
public:
    /*typedef const std::string (*nameFunc) ();*/
    typedef std::shared_ptr<SceneNode> (*createFunc) (core::SceneManager* owner, core::SceneNode *parent, const std::string &name, std::shared_ptr<void> param, const Vector &scale);
    typedef std::shared_ptr<SceneNode> (*createDynamicableFunc) (core::SceneManager* owner, core::SceneNode *parent, const std::string &name, std::shared_ptr<void> param, const Vector &scale, bool dynamic);

    static SceneNodeFactory *GetInstance();

    void AddType(const std::string &type, createFunc f);

    void AddType(const std::string &type, createDynamicableFunc f);

    std::shared_ptr<SceneNode> CreateNode(const std::string &type, core::SceneManager* owner, core::SceneNode *parent, const std::string &name, std::shared_ptr<void> param, const Vector &scale, bool dynamic = false, const std::string &fallbackType = "");

    static std::shared_ptr<SceneNode> Create(const std::string &type, core::SceneManager* owner, core::SceneNode *parent, const std::string &name, std::shared_ptr<void> param, const Vector &scale, bool dynamic, const std::string &fallbackType = "");
    static std::shared_ptr<SceneNode> Create(const std::string &type, core::SceneManager* owner, core::SceneNode *parent, const std::string &name, std::shared_ptr<void> param, const Vector &scale, const std::string &fallbackType = "");

    template <class S, class T>
    class Type
    {
    public:
        static std::shared_ptr<SceneNode> ctor (core::SceneManager* owner, core::SceneNode *parent, const std::string &name, std::shared_ptr<void> param, const Vector &scale)
        {
            return std::make_shared<T>(owner, parent, name, param, scale);
        }

        Type()
        {
            SceneNodeFactory::GetInstance()->AddType(S::StaticName(), ctor);
        }
    };

    template <class S, class T>
    class DynamicableType
    {
    public:
        static std::shared_ptr<SceneNode> ctor (core::SceneManager* owner, core::SceneNode *parent, const std::string &name, std::shared_ptr<void> param, const Vector &scale, bool dynamic = false)
        {
            return std::make_shared<T>(owner, parent, name, param, scale, dynamic);
        }

        DynamicableType()
        {
            SceneNodeFactory::GetInstance()->AddType(S::StaticName(), ctor);
        }
    };

protected:
    std::map<std::string, createFunc> types;
    std::map<std::string, createDynamicableFunc> dynamicable_types;

};


} // namespace core

} // namespace expressive



#endif // EXPRESSIVE_OGRESCENENODE_FACTORY_H_
