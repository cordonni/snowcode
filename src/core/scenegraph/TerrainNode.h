/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef TERRAINNODE_H
#define TERRAINNODE_H

#include "SceneNode.h"

namespace expressive
{

namespace core
{

/**
 * @brief The Terrain class is a specific type of SceneNode.
 * It is set as background (not interactable), and is not hoverable / selectable
 */


class CORE_API TerrainNode : public SceneNode
{
public:

    /**
     * Creates a new TerrainNode.
     * @param owner The SceneManager that contains this SceneNode.
     * @param name The name of this SceneNode.
     */

    TerrainNode(SceneManager *owner, const std::string &name = "Terrain", const std::string &type = "Terrain", const Vector &scale = Vector::Ones());

    /**
     * Deletes this SceneNode.
     */
    virtual ~TerrainNode() {}

    /**
     * Sets the selected state of this SceneNode.
     *
     * This overload is to cancel the selection behavior: the terrain cannot be selected
     *
     */
    virtual void set_selected(bool selected = true, bool hover_parent = true)
        {UNUSED(selected); UNUSED(hover_parent);}

    /**
     *
     * This overload is to cancel the selection behavior: the terrain cannot be selected
     *
     */
    virtual void set_hovered(bool hovered = true, bool hover_parent = true)
        {UNUSED(hovered); UNUSED(hover_parent);}

    /**
     * Get the height at a given position
     *
     * Returns:
     *
     * a bool   : existance of ground at that place
     * a double  : height of the ground
     *
     */

    virtual std::pair<bool, double> height(Vector2);

};


}
}

#endif // TERRAINNODE_H
