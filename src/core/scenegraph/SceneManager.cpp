#include "core/scenegraph/SceneManager.h"

// commons dependencies
#include "core/geometry/AABBox.h"

// core dependencies
#include "core/geometry/VectorTools.h"
#include "core/scenegraph/Camera.h"
#include "core/scenegraph/Overlay.h"
#include "core/scenegraph/SceneNodeFactory.h"
#include "core/scenegraph/listeners/SceneManagerListener.h"
#include "core/utils/Serializable.h"

#include "ork/core/Timer.h"
#include "ork/render/FrameBuffer.h"
#include "ork/resource/ResourceManager.h"

//#include "core/geometry/AbstractTriMesh.h"
#include "core/geometry/BasicRay.h"

using namespace ork;
using namespace std;

namespace expressive
{

namespace core
{

SceneChanges::SceneChanges(std::shared_ptr<SceneNode> added_node, std::shared_ptr<SceneNode> removed_node, std::shared_ptr<SceneNode> added_to_selection, std::shared_ptr<SceneNode> removed_from_selection)
{
    if (added_node != nullptr) {
        added_nodes_.insert(added_node);
    }
    if (removed_node != nullptr) {
        removed_nodes_.insert(removed_node);
    }
    if (added_to_selection != nullptr) {
        added_to_selection_.insert(added_to_selection);
    }
    if (removed_from_selection != nullptr) {
        removed_from_selection_.insert(removed_from_selection);
    }
}

UpdateSceneCommand::UpdateSceneCommand(SceneManager *scene, SceneChanges changes) : Command("UpdateSceneCommand"), scene_(scene), changes_(changes)
{
}

bool UpdateSceneCommand::execute()
{
    // remove selected nodes before removing actual nodes (just in case).
    for (std::shared_ptr<SceneNode> node : changes_.removed_from_selection_) {
        scene_->InternalRemoveFromSelection(node);
    }

    for (std::shared_ptr<SceneNode> node : changes_.removed_nodes_) {
        scene_->InternalRemove(node);
    }

    // Add nodes before actually selecting them.
    for (std::shared_ptr<SceneNode> node : changes_.added_nodes_) {
        scene_->InternalAdd(node);
    }
    for (std::shared_ptr<SceneNode> node : changes_.added_to_selection_) {
        scene_->InternalAddToSelection(node);
    }

//    if (changes_.added_to_selection_.size() > 0 || changes_.removed_from_selection_.size() > 0) {
//        scene_->SelectionUpdated();
//    }

    return true;
}

bool UpdateSceneCommand::undo()
{
    /// Inverted version of #execute().
    // see #execute() for comments.
    for (std::shared_ptr<SceneNode> node : changes_.added_to_selection_) {
        scene_->InternalRemoveFromSelection(node);
    }

    for (std::shared_ptr<SceneNode> node : changes_.added_nodes_) {
        scene_->InternalRemove(node);
    }

    for (std::shared_ptr<SceneNode> node : changes_.removed_nodes_) {
        scene_->InternalAdd(node);
    }
    for (std::shared_ptr<SceneNode> node : changes_.removed_from_selection_) {
        scene_->InternalAddToSelection(node);
    }

//    if (changes_.added_to_selection_.size() > 0 || changes_.removed_from_selection_.size() > 0) {
//        scene_->SelectionUpdated();
//    }

    return true;
}

SceneManager::SceneManager(std::shared_ptr<CommandManager> command_manager) :
    ListenableT<SceneManager>(),
    Serializable("SceneManager"),
    hovered_node_(NULL_NODE_ID)
{
    init(command_manager);
}

void SceneManager::init(std::shared_ptr<CommandManager> command_manager)
{
    command_manager_ = command_manager;
    current_camera_manager_ = nullptr;
    next_node_id_ = 0;
    if (command_manager == nullptr) {
        Logger::ERROR_LOGGER->log("SCENEGRAPH", "Provided Command Manager must be non-null");
        assert(command_manager != nullptr);
    }

    ork_scene_ = new OrkSceneManager();
    ork_scene_->setRoot(new OrkSceneNode());
    ork_scene_->setCameraNode("camera");
    ork_scene_->setCameraMethod("draw");
//    ork_scene_->getRoot()->setLocalBounds(AABBox(Point3(-10.0, -10.0, -10.0), Point3(10.0, 10.0, 10.0)));
}

SceneManager::~SceneManager()
{
    clear(true, false);
    command_manager_ = nullptr;
    current_camera_manager_ = nullptr;

    ork_scene_->setRoot(nullptr);
    ork_scene_ = nullptr;
}

std::shared_ptr<CommandManager> SceneManager::command_manager() const
{
    return command_manager_;
}

void SceneManager::clear(bool clearOverlays, bool undoable)
{
    if (undoable) {
        RemoveAll();
        if (clearOverlays) {
            RemoveAllOverlays();
        }
    } else {
        if (clearOverlays) {
            overlay_pool_   . clear();
        }
        node_pool_      . clear();
        root_nodes_     . clear();
        selected_nodes_ . clear();
        command_manager_->clear();
        for (int i = (int) ork_scene_->getRoot()->getChildrenCount() - 1; i >= 0; --i) {
            ork_scene_->getRoot()->removeChild(i);
        }
    }
    NotifyCleared();
}

void SceneManager::AddXmlAttributes(TiXmlElement *element) const
{
    auto children = ((SceneManager*)(this))->nodes();
    while (children->HasNext()) {
        auto child = children->Next();
        TiXmlElement* e = child->ToXml();
        if (e != nullptr) {
            element->LinkEndChild(e);
        }
    }
}

TiXmlElement *SceneManager::ToXml(const char *name) const
{
    TiXmlElement*element = Serializable::ToXml(name == nullptr ? SerializablePool::StaticName().c_str() : name);

    return element;
}

//void SceneManager::LoadFile(const std::string &str, std::shared_ptr<ResourceManager> manager)
//{
//    std::shared_ptr<Serializable> res = manager->loadFromFile(str);
//    Add(res);


//    NotifyUpdate();
//}

void SceneManager::AddNode(std::shared_ptr<SceneNode> node)
{
//    node_pool_.insert(make_pair(node->id(), node));
    if (!has(node->id())) {
        command_manager_->run(std::make_shared<UpdateSceneCommand>(this, SceneChanges(node)));
    }
}

void SceneManager::AddOverlay(std::shared_ptr<SceneNode> overlay)
{
    overlay_pool_.insert(make_pair(overlay->id(), overlay));
    ork_scene_->getRoot()->addChild(overlay->node_);
}

void SceneManager::RemoveAll()
{
    SceneChanges c;
    for (NodePool::iterator it = node_pool_.begin(); it != node_pool_.end(); ++it) {
        c.removed_nodes_.insert(it->second);
    }
    for (set<NodeId>::iterator it = selected_nodes_.begin(); it != selected_nodes_.end(); ++it) {
        c.removed_from_selection_.insert(get_node(*it));
    }
    command_manager_->run(std::make_shared<UpdateSceneCommand>(this, c));

//    selected_nodes_.clear();
//    node_pool_.clear();
}

void SceneManager::RemoveAllOverlays()
{
    for (pair<unsigned int, std::shared_ptr<SceneNode> > p : overlay_pool_) {
        ork_scene_->getRoot()->removeChild((p.second)->node_);
    }
    overlay_pool_.clear();
}

void SceneManager::RemoveNode(NodeId id)
{
    if (has(id) && command_manager_ != nullptr) {
        command_manager_->run(std::make_shared<UpdateSceneCommand>(this, SceneChanges(nullptr, get_node(id))));
//        selected_nodes_.erase(id);
//        node_pool_.erase(id);
    } else if (command_manager_ == nullptr) {
        InternalRemove(get_node(id));
    }
}

void SceneManager::RemoveOverlay(NodeId id)
{
    OverlayPool::iterator it = overlay_pool_.find(id);
    if (it != overlay_pool_.end()) {
        ork_scene_->getRoot()->removeChild((it->second)->node_);
        overlay_pool_.erase(it);
    }
}

void SceneManager::AddToSelection(NodeId id)
{
    if (has(id)) {
        command_manager_->run(std::make_shared<UpdateSceneCommand>(this, SceneChanges(nullptr, nullptr, get_node(id))));
        //    selected_nodes_.insert(id);
    }
}

void SceneManager::AddToSelection(std::set<NodeId> &ids)
{
    if (ids.size() > 0) {
        SceneChanges c;

        for (set<NodeId>::iterator it = ids.begin(); it != ids.end(); ++it) {
            c.added_to_selection_.insert(get_node(*it));
        }
        command_manager_->run(std::make_shared<UpdateSceneCommand>(this, c));
    }
}

void SceneManager::RemoveFromSelection(NodeId id)
{
    if (has(id)) {
        command_manager_->run(std::make_shared<UpdateSceneCommand>(this, SceneChanges(nullptr, nullptr, nullptr, get_node(id))));
//            selected_nodes_.erase(id);
    }
}

void SceneManager::RemoveFromSelection(std::set<NodeId> &ids)
{
    if (ids.size() > 0) {
        SceneChanges c;
        for (set<NodeId>::iterator it = ids.begin(); it != ids.end(); ++it) {
            c.removed_from_selection_.insert(get_node(*it));
        }
        command_manager_->run(std::make_shared<UpdateSceneCommand>(this, c));
    }
}

void SceneManager::RemoveAllFromSelection()
{
    SceneChanges c;

    for (set<NodeId>::iterator it = selected_nodes_.begin(); it != selected_nodes_.end(); ++it) {
        c.removed_from_selection_.insert(get_node(*it));
    }

    current_camera_manager_->center_target();
    command_manager_->run(std::make_shared<UpdateSceneCommand>(this, c));

//    selected_nodes_.clear();
}

void SceneManager::RemoveSelectedNodes()
{
    SceneChanges c;

    for (set<NodeId>::iterator it = selected_nodes_.begin(); it != selected_nodes_.end(); ++it) {
        c.removed_nodes_.insert(get_node(*it));
    }
    command_manager_->run(std::make_shared<UpdateSceneCommand>(this, c));

//    selected_nodes_.clear();
}

void SceneManager::SetSelection(NodeId id)
{
    if (has(id)) {
        SceneChanges c;
        for (set<NodeId>::iterator it = selected_nodes_.begin(); it != selected_nodes_.end(); ++it) {
            c.removed_from_selection_.insert(get_node(*it));
        }

        c.added_to_selection_.insert(get_node(id));
        command_manager_->run(std::make_shared<UpdateSceneCommand>(this, c));
    }
}

void SceneManager::SetSelection(std::set<NodeId> &ids)
{
    SceneChanges c;

    for (set<NodeId>::iterator it = ids.begin(); it != ids.end(); ++it) {
        c.added_to_selection_.insert(get_node(*it));
    }
    for (set<NodeId>::iterator it = selected_nodes_.begin(); it != selected_nodes_.end(); ++it) {
        c.removed_from_selection_.insert(get_node(*it));
    }

    command_manager_->run(std::make_shared<UpdateSceneCommand>(this, c));
}

void SceneManager::SetHoveredNode(SceneNode *hover)
{
    NodeId newId = NULL_NODE_ID;

    if (hover != nullptr) {
        newId = hover->id();
    }
    if (newId != hovered_node_) {

        if (hovered_node_ != NULL_NODE_ID) {
            get_node(hovered_node_)->set_hovered(false);
            SceneNodeUpdated(hovered_node_);
        }

        if (newId != NULL_NODE_ID) {
            hover->set_hovered(true);
            SceneNodeUpdated(newId);
        }
    }
    hovered_node_ = newId;
}

bool SceneManager::is_selected(NodeId id) const
{
    return selected_nodes_.find(id) != selected_nodes_.end();
}

bool SceneManager::has(NodeId id) const
{
    return node_pool_.find(id) != node_pool_.end();
}

SceneManager::NodeId SceneManager::next_node_id()
{
    return next_node_id_++;
}

SceneManager::NodeId SceneManager::const_next_node_id()
{
    return next_node_id_;
}

unsigned int SceneManager::node_count() const
{
    return (uint) node_pool_.size();
}

unsigned int SceneManager::selected_node_count() const
{
    return (uint) selected_nodes_.size();
}

//    inline std::pair<NodePool::iterator, NodePool::iterator> non_const_nodes()
//    {
//        return make_pair(node_pool_.begin(), node_pool_.end());
//    }

//    inline std::pair<NodePool::const_iterator, NodePool::const_iterator> nodes()
//    {
//        return make_pair(node_pool_.cbegin(), node_pool_.cend());
//    }

//    inline std::pair<std::set<NodeId>::iterator, std::set<NodeId>::iterator> non_const_selected_nodes()
//    {
//        return make_pair(selected_nodes_.begin(), selected_nodes_.end());
//    }

//    inline std::pair<std::set<NodeId>::const_iterator, std::set<NodeId>::const_iterator> selected_nodes()
//    {
//        return make_pair(selected_nodes_.cbegin(), selected_nodes_.cend());
//    }

SceneManager::NodeIteratorPtr SceneManager::non_const_nodes()
{
    return std::make_shared<NodeMapIterator>(node_pool_.begin(), node_pool_.end());
}

const SceneManager::NodeIteratorPtr SceneManager::nodes() const
{
    return std::make_shared<NodeMapIterator>(node_pool_.cbegin(), node_pool_.cend());
}

SceneManager::NodeSetIteratorPtr SceneManager::non_const_root_nodes()
{
    return std::make_shared<NodeSetIterator>(this, root_nodes_);
}

const SceneManager::NodeSetIteratorPtr SceneManager::root_nodes() const
{
    return std::make_shared<NodeSetIterator>(this, root_nodes_);
}

SceneManager::NodeSetIteratorPtr SceneManager::non_const_selected_nodes()
{
    return std::make_shared<NodeSetIterator>(this, selected_nodes_);
}

const SceneManager::NodeSetIteratorPtr SceneManager::selected_nodes() const
{
    return std::make_shared<NodeSetIterator>((SceneManager*)this, selected_nodes_);
}

const std::set<SceneManager::NodeId> &SceneManager::selected_node_ids() const
{
    return selected_nodes_;
}

SceneManager::NodeId SceneManager::hovered_node_id() const
{
    return hovered_node_;
}

std::shared_ptr<SceneNode> SceneManager::hovered_node() const
{
    return get_node(hovered_node_);
}

shared_ptr<SceneNode> SceneManager::get_node(NodeId id) const
{
   if (id == NULL_NODE_ID) {
       return nullptr;
   }
   if (id == current_camera_manager_->id()) {
       return current_camera_manager_;
   }
   NodePool::const_iterator i = node_pool_.find(id);
   if (i == node_pool_.end()) {
       return nullptr;
   }
   return i->second;
}

std::shared_ptr<SceneNode> SceneManager::get_node(std::string name) const
{
    for(const auto& elem : node_pool_)
        if(elem.second->name() == name)
            return elem.second;
    return nullptr;
}

shared_ptr<SceneNode> SceneManager::get_overlay(NodeId id) const
{
   if (id == NULL_NODE_ID) {
       return nullptr;
   }
   OverlayPool::const_iterator i = overlay_pool_.find(id);
   if (i == overlay_pool_.end()) {
       return nullptr;
   }
   return i->second;
}

std::shared_ptr<SceneNode> SceneManager::get_overlay(std::string name) const
{
    for(const auto& elem : overlay_pool_)
        if(elem.second->name() == name)
            return elem.second;
    return nullptr;
}

void SceneManager::SceneNodeAdded(NodeId id)
{
    for (auto listener : listeners_)
    {
        static_cast<SceneManagerListener*>(listener)->SceneNodeAdded(id);
    }
}

void SceneManager::SceneNodeRemoved(NodeId id)
{
    for (auto listener : listeners_)
    {
        static_cast<SceneManagerListener*>(listener)->SceneNodeRemoved(id);
    }
}

void SceneManager::SceneNodeSelected(NodeId id)
{
    for (auto listener : listeners_)
    {
        static_cast<SceneManagerListener*>(listener)->SceneNodeSelected(id);
    }
}

void SceneManager::SceneNodeUnselected(NodeId id)
{
    for (auto listener : listeners_)
    {
        static_cast<SceneManagerListener*>(listener)->SceneNodeUnselected(id);
    }
}

void SceneManager::SceneNodeUpdated(NodeId id)
{
    assert(has(id));
    get_node(id)->NotifyUpdate();
}

SceneNode *SceneManager::GetCameraRayIntersection(Camera *camera, Scalar x, Scalar y, Point& target, Scalar &dist, bool object_only /* = false */) {
    BasicRay ray = camera->GetBasicRay(x, y);

    SceneNode *res = GetRayIntersection(ray, target, dist, object_only);

    return res;
}

SceneNode *SceneManager::GetRayIntersection(const core::BasicRay &ray, Point& target, Scalar &dist, bool object_only /* = false */) {
    // Init
    dist = 1e10;
    SceneNode* res = nullptr;
    Scalar tmpDist = dist;

    // Scene Node is intersecting ray?
    auto children = root_nodes();
    while (children->HasNext()) {
//    for (auto child : node_pool_) {
        auto child = children->Next();
        if (object_only && child->parent()) {
            continue;
        }
        SceneNode *r = child->IsIntersectingRay(ray, tmpDist, !object_only);

        // If Intersecting, and closer?
        if (tmpDist < dist) {
            res = r;
            dist = tmpDist;
        }
    }

    if (res) {
        target = ray.starting_point() + dist*ray.direction();
    }

    return res;
}

unsigned int SceneManager::GetCameraBoxIntersection(Camera *camera, const AABBox2D &box, std::set<NodeId> &selected_nodes, std::shared_ptr<SceneNode> n)
{
    unsigned int selected_children = 0;
    if (n->has_children()) {
        auto children = n->children();
        while (children.HasNext()) {
            auto child = children.Next();
            selected_children += GetCameraBoxIntersection(camera, box, selected_nodes, child);
        }
    }
    if (selected_children == 0) {
        Point2 p = camera->WorldToViewplane(n->GetPointInWorld(n->bounding_box().ComputeCenter()));
        if (box.Contains(p)) {
           selected_nodes.insert(n->id());
           selected_children = 1;
        }
    }
    return selected_children;
}

unsigned int SceneManager::GetCameraBoxIntersection(Camera *camera, const AABBox2D &box, std::set<NodeId> &selected_nodes)
{
    auto children = root_nodes();
    while (children->HasNext()) {
        auto child = children->Next();
        GetCameraBoxIntersection(camera, box, selected_nodes, child);
    }

    return (uint) selected_nodes.size();
}

unsigned int SceneManager::GetCameraBrushIntersection(Camera *camera, const Point2 &center, Scalar radius, std::set<NodeId> &selected_nodes, std::shared_ptr<SceneNode> n)
{
    unsigned int selected_children = 0;
    if (n->has_children()) {
        auto children = n->children();
        while (children.HasNext()) {
            auto child = children.Next();
            selected_children += GetCameraBrushIntersection(camera, center, radius, selected_nodes, child);
        }
    }
    if (selected_children == 0) {
        Point2 p = camera->WorldToViewplane(n->GetPointInWorld(n->bounding_box().ComputeCenter()));
        if ((center - p).norm() < radius) {
           selected_nodes.insert(n->id());
           selected_children = 1;
        }
    }
    return selected_children;
}

unsigned int SceneManager::GetCameraBrushIntersection(Camera *camera, const Point2 &center, Scalar radius, std::set<NodeId> &selected_nodes)
{
    auto children = root_nodes();
    while (children->HasNext()) {
        auto child = children->Next();
        GetCameraBrushIntersection(camera, center, radius, selected_nodes, child);
    }

    return (uint) selected_nodes.size();
}

bool SceneManager::intersects(SceneManager::NodeId id, Camera *camera, const Point2 &p)
{
    return intersects(id, camera, p[0], p[1]);
}

bool SceneManager::intersects(SceneManager::NodeId id, Camera *camera, Scalar x, Scalar y)
{
    BasicRay ray = camera->GetBasicRay(x, y);
    std::shared_ptr<SceneNode> n = get_node(id);

    Scalar dist;

    bool res = n->IsIntersectingRay(ray, dist) != nullptr;
    return res;
}

bool SceneManager::intersectsSelection(Camera *camera, const Point2 &p)
{
    return intersectsSelection(camera, p[0], p[1]);
}

bool SceneManager::intersectsSelection(Camera *camera, Scalar x, Scalar y)
{
    auto selection = selected_nodes();
    while (selection->HasNext()) {
        bool res = intersects(selection->NextId(), camera, x, y);
        if (res) {
            return true;
        }
    }
    return false;
}


SceneNode *SceneManager::GetCameraRayIntersection(Scalar x, Scalar y, Point& target, bool object_only /* = false */)
{
    return GetCameraRayIntersection(current_camera_manager_.get(), x, y, target, object_only);
}

unsigned int SceneManager::GetCameraBoxIntersection(const AABBox2D &box, std::set<NodeId> &selected_nodes, std::shared_ptr<SceneNode> n)
{
    return GetCameraBoxIntersection(current_camera_manager_.get(), box, selected_nodes, n);
}

unsigned int SceneManager::GetCameraBoxIntersection(const AABBox2D &box, std::set<NodeId> &selected_nodes)
{
    return GetCameraBoxIntersection(current_camera_manager_.get(), box, selected_nodes);
}

unsigned int SceneManager::GetCameraBrushIntersection(const Point2 &center, Scalar radius, std::set<NodeId> &selected_nodes, std::shared_ptr<SceneNode> n)
{
    return GetCameraBrushIntersection(current_camera_manager_.get(), center, radius, selected_nodes, n);
}

unsigned int SceneManager::GetCameraBrushIntersection(const Point2 &center, Scalar radius, std::set<NodeId> &selected_nodes)
{
    return GetCameraBrushIntersection(current_camera_manager_.get(), center, radius, selected_nodes);
}

bool SceneManager::intersects(SceneManager::NodeId id, const Point2 &p)
{
    return intersects(id, current_camera_manager_.get(), p);
}

bool SceneManager::intersects(SceneManager::NodeId id, Scalar x, Scalar y)
{
    return intersects(id, current_camera_manager_.get(), x, y);
}

bool SceneManager::intersectsSelection(const Point2 &p)
{
    return intersectsSelection(current_camera_manager_.get(), p);
}

bool SceneManager::intersectsSelection(Scalar x, Scalar y)
{
    return intersectsSelection(current_camera_manager_.get(), x, y);
}

void SceneManager::InternalAdd(std::shared_ptr<SceneNode> node, bool firstAdd)
{
    /// add_node / undo remove_node :
    /// - (re)selects the node if needed
    /// - (re)add it to its parent
    /// - (re)add it to the scene
    /// - Refresh
    if (node->parent() != nullptr) {
        if (!node->parent()->has_child(node->id())) {
            node->parent()->children_.insert(node);
            node->parent()->children_ids_.insert(node->id());
            node->parent()->node_->addChild(node->node_);
        }
    } else {
        root_nodes_.insert(node->id());
            ork_scene_->getRoot()->addChild(node->node_);
    }

    node_pool_.insert(make_pair(node->id(), node));

    // if redoing, and node was selected before, we reselect it
    if (node->selected()) {
        selected_nodes_.insert(node->id());
    }

    auto children = node->children();
    while (children.HasNext()) {
        std::shared_ptr<SceneNode> child = children.Next();
        InternalAdd(child, false);
    }

    if (firstAdd) {
        SceneNodeAdded(node->id());
    }
}

void SceneManager::InternalRemove(std::shared_ptr<SceneNode> node, bool firstRemoval)
{
    /// remove_node / undo add_node :
    /// - Remove children if any
    /// - unselects the node if selected
    /// - add it its parent
    /// - Remove it from the scene
    /// - Refresh
    ///

    if (hovered_node_ == node->id()) {
        hovered_node_ = NULL_NODE_ID;
    }
    auto children = node->children();
    while (children.HasNext())
    {
        std::shared_ptr<SceneNode> child = children.Next();
        InternalRemove(child, false);
    }
    if (node->selected()) {
        // Wer are not using node->set_selected(false) on purpose. This will allow us to reset the
        // selection afterwards when redoing/undoing.
        selected_nodes_.erase(node->id());
    }

    if (node->parent() != nullptr && firstRemoval)  {
        // same with parent.
        node->parent()->children_.erase(node);
//        if (!node->isVirtualNode_) {
            node->parent()->node_->removeChild(node->node_);
//        }
    } else {
        root_nodes_.erase(node->id());
//        if (!node->isVirtualNode_) {
            ork_scene_->getRoot()->removeChild(node->node_);
//        }
    }

//    if (firstRemoval) {
        SceneNodeRemoved(node->id());
//    }

    node_pool_.erase(node->id());
}

void SceneManager::InternalAddToSelection(std::shared_ptr<SceneNode> node)
{
    selected_nodes_.insert(node->id());
    node->set_selected(true);

    SceneNodeUpdated(node->id());
    SceneNodeSelected(node->id());
}

void SceneManager::InternalRemoveFromSelection(std::shared_ptr<SceneNode> node)
{
    selected_nodes_.erase(node->id());
    node->set_selected(false);

    SceneNodeUpdated(node->id());
    SceneNodeUnselected(node->id());
}

void SceneManager::NotifyCleared()
{
    for (auto listener : listeners_) {
        dynamic_cast<SceneManagerListener*>(listener)->cleared(this);
    }
}

Point ComputeBarycenter(const vector<Point> &points, vector<Scalar> &weights)
{
    Point res;
    Scalar tot = 0.0;
    for (unsigned int i = 0; i < points.size(); ++i) {
        res += points[i] * weights[i];
        tot += weights[i];
    }

    if (tot > 0.0) {
        res /= tot;
    }
    return res;
}

Point SceneManager::GetSceneBarycenter() const
{
    vector<Point> points;
    vector<Scalar> weights;
    auto allnodes = nodes();
    while (allnodes->HasNext()) {
        auto n = allnodes->Next();
        AABBox b = n->bounding_box();
//        points.push_back(b.ComputeCenter());
        points.push_back(b.ComputeCenter());
        weights.push_back(b.GetLargestDimensionValue());
    }
    return ComputeBarycenter(points, weights);
}

Point SceneManager::GetSelectionBarycenter() const
{
    vector<Point> points;
    vector<Scalar> weights;
    auto allnodes = selected_nodes();
    while(allnodes->HasNext()) {
        auto n = allnodes->Next();
        AABBox b = n->bounding_box();
//        points.push_back(b.ComputeCenter());
        points.push_back(b.ComputeCenter());
        weights.push_back(b.GetLargestDimensionValue());
    }
    if (points.size() == 0) {
        return GetSceneBarycenter();
    }
    return ComputeBarycenter(points, weights);
}

std::shared_ptr<SceneNode> SceneManager::AddNodeFromSerializable(std::shared_ptr<Serializable> new_children, bool dynamic)
{
    std::shared_ptr<SceneNode> n = nullptr;
    if (new_children != nullptr && new_children->Name() == SerializablePool::StaticName()) {
        std::shared_ptr<SerializablePool> pool = dynamic_pointer_cast<SerializablePool>(new_children);
        assert(pool != nullptr);
        for (unsigned int i = 0; i < pool->serializables_.size(); ++i) {
            ork::ptr<Serializable> child = pool->serializables_[i];
            n = SceneNodeFactory::Create(child->Name(), this, nullptr, child->Name() + to_string(this->next_node_id_), child, Vector::Ones(), dynamic);
            AddNode(n);
        }
    } else {
        n = SceneNodeFactory::Create(new_children->Name(), this, nullptr, new_children->Name() + to_string(this->next_node_id_), new_children, Vector::Ones(), dynamic);
        AddNode(n);
    }
    NotifyUpdate();

    return n;
}

std::shared_ptr<Camera> SceneManager::current_camera() const
{
    return current_camera_manager_;
}

void SceneManager::set_current_camera(std::shared_ptr<Camera> cam)
{
    if (current_camera_manager_ != nullptr) {
        ork_scene_->getRoot()->removeChild(current_camera_manager_->node_);
    }
    current_camera_manager_ = cam;
    if (cam != nullptr) {
        ork_scene_->getRoot()->addChild(cam->node_);
    }
}

ork::ptr<ork::ResourceManager> SceneManager::resource_manager() const
{
    return ork_scene_->getResourceManager();
}

ork::ptr<ork::Scheduler> SceneManager::scheduler() const
{
    return ork_scene_->getScheduler();
}

void SceneManager::set_resource_manager(ork::ptr<ork::ResourceManager> resource_manager)
{
    ork_scene_->setResourceManager(resource_manager);
}

void SceneManager::set_scheduler(ork::ptr<ork::Scheduler> scheduler)
{
    ork_scene_->setScheduler(scheduler);
}

void SceneManager::Draw()
{
    ork_scene_->draw();
}

void SceneManager::Update(double t, double dt)
{
    auto nodes = this->nodes();
    if (current_camera_manager_ != nullptr) {
        ork_scene_->setCameraToScreen(current_camera_manager_->GetCameraToScreen());
    }
    while (nodes->HasNext()) {
        std::shared_ptr<SceneNode> n = nodes->Next();
        n->Update(true);
//        n->UpdateTransformationMatrices(true);
    }
    auto overlays = std::make_shared<AbstractMapIterator<OverlayPool, std::shared_ptr<SceneNode> >>(overlay_pool_.cbegin(), overlay_pool_.cend());
    while (overlays->HasNext()) {
        std::shared_ptr<SceneNode> ov = overlays->Next();
        ov->Update(true);
    }

//    ork_scene_->getRoot()->updateLocalToWorld(nullptr);
//    ork_scene_->getRoot()->updateLocalToCamera(this->current_camera()->GetWorldToLocalTransform(), this->current_camera()->getCameraToScreenTransform());

    ork_scene_->update(t, dt);
}

void SceneManager::screenshot(const string &filename)
{
    std::string f = filename;
    if (filename.length() == 0) {
        char name[256];
        char stime[256];
        ork::Timer::getDateTimeString(stime, 256);
        sprintf(name, "/tmp/expressive_screenshot_%s.tga", stime);
        f = name;
    }
    OrkSceneManager::getCurrentFrameBuffer()->saveToFile(f);
}

void SceneManager::frameDoneCallback(int /*frame*/)
{
    SceneManager::screenshot();
}

} // namespace core

} // namespace expressive
