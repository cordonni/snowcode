#include "TerrainNode.h"

#include "core/geometry/AbstractTriMesh.h"
#include "core/geometry/BasicRay.h"

namespace expressive
{

namespace core
{


TerrainNode::TerrainNode(SceneManager *owner, const std::string &name /*= "Terrain"*/, const std::string &type /*= "Terrain"*/, const Vector &scale /* = Vector::Ones()*/) :
    SceneNode(owner, nullptr, name, type, scale, true)
{
    node_->removeValue("hovered");
    node_->removeValue("selected");

    SetBackground();
}

std::pair<bool, double> TerrainNode::height(Vector2 p)
{
    // lowest point
    Scalar min_h = bounding_box().min()[1] -1.0;

    if (tri_mesh_ != nullptr) {

        Scalar dist = tri_mesh_->FindIntersection(BasicRay(Point(p[0], min_h, p[1]), Vector(0.0,1.0,0.0)), expressive::kScalarMax );
        if (dist != expressive::kScalarMax) {
            return {true, dist};
        }

    }
    return {false, 0.0};
}

}
}
