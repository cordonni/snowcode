#include "ork/core/Logger.h"

#include "core/scenegraph/ObjectPrimitive.h"
#include "core/scenegraph/SceneCommands.h"

#include "core/geometry/AbstractTriMesh.h"
#include "core/geometry/MeshTools.h"

#include <sstream>

using namespace std;
using namespace expressive::core;

namespace expressive
{

namespace core
{

unsigned int ObjectPrimitive::instance_counter_ = 0;

ObjectPrimitive::ObjectPrimitive(core::SceneManager* scene_manager,
                                 SceneNode *parent,
                                 const string &name,
                                 std::shared_ptr<SceneNodeManipulator> manipulator,
                                 std::shared_ptr<AbstractTriMesh> mesh,
                                 core::Frame f) :
    SceneNode(scene_manager, parent, name),
    frame_(f),
    interactable_(true)
{
    set_manipulator(manipulator);
    if(mesh == nullptr)
        set_tri_mesh(MeshTools::Empty());
    else
    {
        set_tri_mesh(mesh);
    }
}

ObjectPrimitive::ObjectPrimitive(const ObjectPrimitive& to_copy):
    SceneNode(to_copy),
    frame_(to_copy.frame()),
    interactable_(to_copy.interactable())
{
    name_ = name_ + "_";
    set_manipulator(to_copy.manipulator());
    set_tri_mesh(to_copy.tri_mesh());
}


ObjectPrimitive::~ObjectPrimitive()
{
}


//Point ObjectPrimitive::GetPointInLocal   (const Point &p) const
//{
//    return p; // TODO OPENGL : check this
////    return internal_transform_.inverse() * p;
//}


//Point ObjectPrimitive::GetPointInWorld   (const Point &p) const
//{
//    return p; // TODO OPENGL : check this
////    return internal_transform_ * p;
//}


void ObjectPrimitive::Scale(Scalar s, const bool local)
{
    Scale(s * Point::Ones(), frame_.p0(), local);
}


void ObjectPrimitive::Scale(const Point &s, const Point &c, const bool local)
{
    expressive::Transform t = expressive::Transform::Identity();
    t.pretranslate(-c);
    t.prescale(s);
    t.pretranslate(c);

    Transform(t, local);
    return;
}


void ObjectPrimitive::Rotate   (const Point &a, const Point &c, Scalar th, const bool local)
{
    expressive::Transform t = expressive::Transform::Identity();
    t.pretranslate(-c);
    t.prerotate(Quaternion(a, th).toMat4());
    t.pretranslate(c);

    Transform(t, local);
    return;
}


void ObjectPrimitive::Translate(const Point &v, const bool local)
{
    //*

    expressive::Transform t = expressive::Transform::Identity();
    t.pretranslate(v);

    Transform(t, local);
    return;

    /*/



    //*/

}


void ObjectPrimitive::Transform(const expressive::Transform& t, const bool local)
{
//    expressive::Transform t_normal = t.linearExt();
//    expressive::Matrix t_normal = t.linear();
//    t_normal.matrix() = t_normal.matrix().transposed();

    expressive::Transform t2;

    if(local)
        t2 = frame_.internal_transform() * t * frame_.internal_transform().inverse();
    else
        t2 = t;

    auto t2_rot = t2.rotation();

    if(tri_mesh_ != nullptr)
    {
        AABBox b;
        for(AbstractTriMesh::VertexIter i = tri_mesh_->vertices_begin(); i != tri_mesh_->vertices_end(); i++)
        {
            Point p = t2 * tri_mesh_->point(*i);
            b.Enlarge(p);

            tri_mesh_->set_point(*i, p);
            tri_mesh_->set_normal(*i, t2_rot * tri_mesh_->normal(*i));
        }
        tri_mesh_->update_bounding_box(b);
    }

    frame_.internal_transform() = t2 * frame_.internal_transform();
}



void ObjectPrimitive::update_bounding_box()
{
    AABBox b;
    for(AbstractTriMesh::VertexIter i = tri_mesh_->vertices_begin(); i != tri_mesh_->vertices_end(); i++)
    {
        Point p = tri_mesh_->point(*i);
        b.Enlarge(p);
    }
    this->tri_mesh_->update_bounding_box(b);
}


void ObjectPrimitive::set_selected(bool selected, bool hover_parent)
{
    if(interactable_)
    {
//        owner_->AddToSelection(this->id());
        SceneNode::set_selected(selected, hover_parent);




//        shared_ptr<ObjectPrimitive> parent = make_shared<ObjectPrimitive>(*parent_);
//        ObjectPrimitive* parent = static_cast<ObjectPrimitive*>(parent_);
//        if(parent != nullptr)
//            parent->ChildSelected(id_);



//        for(SceneNode::ChildIterator i = children(); i.HasNext(); i.Next())
//        {
//            static_pointer_cast<ObjectPrimitive>(*(i.iterator))->set_selected(selected, hover_parent);

//        }

//        if(selected)
//            owner()->AddToSelection(id());
//        else
//            owner()->RemoveFromSelection(id());

    }

    ShowBoundingBox(false);
}


void ObjectPrimitive::set_hovered(bool hovered, bool hover_parent)
{
    if(interactable_)
        SceneNode::set_hovered(hovered, hover_parent);
}

bool ObjectPrimitive::ChildSelected(const uint /*id*/)
{
    return false;
}

void ObjectPrimitive::Deselect()
{
    set_selected(false);
}

void ObjectPrimitive::TranslateChild(const uint /*id*/, const Vector /*&v*/)
{}

// access function
bool ObjectPrimitive::interactable() const
{
    return interactable_;
}

bool& ObjectPrimitive::interactable()
{
    return interactable_;
}

const core::Frame& ObjectPrimitive::frame() const
{
    return frame_;
}

core::Frame& ObjectPrimitive::frame()
{
    return frame_;
}

double ObjectPrimitive::frame_radius() const
{
    return min(min(frame_.px().norm(), frame_.py().norm()), frame_.pz().norm());
}

Vector ObjectPrimitive::frame_centre() const
{
    return frame_.p0();
}// + (frame_.px() + frame_.py() + frame_.pz()) / 2.0; }



} // namespace core

} // namespace expressive

