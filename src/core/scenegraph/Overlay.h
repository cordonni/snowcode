/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef OVERLAY_H
#define OVERLAY_H

#include "core/scenegraph/SceneNode.h"


namespace ork
{
    class Texture2D;
}

namespace expressive
{

namespace core
{

/**
 * @brief The Overlay class is a specific type of SceneNodes : Either 2D or 3D, they are
 * drawn after (possibly ontop of) any other SceneNode, which makes them ideal for GUI items.
 * Some overlays can be drawn before every other scene nodes, if they are tagged as BACKGROUND.
 */
class CORE_API  Overlay : public SceneNode
{
public:
    EXPRESSIVE_MACRO_NAME("Overlay")

    enum OverlayLocation {
        O_BACKGROUND,
        O_3DOVERLAY,
        O_FOREGROUND
    };

    Overlay(SceneManager *scene_manager, const std::string &name, OverlayLocation loc, const std::string & method, const std::string &texture = "");
    Overlay(SceneManager *scene_manager, const std::string &name, OverlayLocation loc, ork::ptr<ork::TaskFactory> method, ork::ptr<ork::Texture2D> texture = nullptr);

    virtual ~Overlay();

    void removeDefaultValues();

protected:
    Overlay();

    virtual void Init(OverlayLocation loc, const std::string &method, const std::string &tex = "");
    virtual void Init(OverlayLocation loc, ork::ptr<ork::TaskFactory> method, ork::ptr<ork::Texture2D> tex =  nullptr);
};

} // core

} // expressive

#endif // OVERLAY_H
