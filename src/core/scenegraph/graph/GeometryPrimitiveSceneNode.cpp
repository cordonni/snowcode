#include "core/scenegraph/graph/GeometryPrimitiveSceneNode.h"

// core dependencies
#include <core/geometry/WeightedSegmentWithDensity.h>
#include <core/geometry/VectorTools.h>
#include <core/utils/ConfigLoader.h>
#include <core/graph/GraphCommands.h>
#include "core/scenegraph/SceneNodeFactory.h"

// std dependencies
#include <stdlib.h>

using namespace std;
using namespace expressive::core;

namespace expressive
{

namespace core
{


static SceneNodeFactory::Type<WeightedSegment, GeometryPrimitiveSceneNodeT<WeightedPoint>> WeightedSegmentSceneNodeType;
static SceneNodeFactory::Type<GeometryPrimitive, GeometryPrimitiveSceneNodeT<PointPrimitive>> SegmentSceneNodeType;

} // namespace core

} // namespace expressive


