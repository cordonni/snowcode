/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GraphElementSceneNode_H
#define GraphElementSceneNode_H

// core dependencies
#include "core/graph/Graph.h"
#include "core/scenegraph/SceneNode.h"
#include "core/graph/GraphCommands.h"

namespace expressive
{

namespace core
{

template<typename VertexType>
class GraphSceneNodeT;

/**
 * @brief The GraphElementSceneNodeT class is the base representation type for
 * types contained in a geometric graph. It also controls the behavior on some
 * operations.
 */
template<typename VertexType>
class GraphElementSceneNodeT : public SceneNode
{
public:
    typedef GraphSceneNodeT<VertexType> GraphSceneNode;
    typedef core::GraphT<VertexType> GraphType;

    GraphElementSceneNodeT(core::SceneManager* scene_manager,
                  SceneNode *parent,
                  const std::string &name,
                  const std::string &type, const Vector &scale = Vector::Ones());

    /**
     * Returns the SceneNode that contains this Graph elment.
     */
    inline GraphSceneNode *displayed_graph() const { return graph_node_; }

    /**
     * Returns the SceneNode that should be used to edit this SceneNode : The Graph.
     */
    virtual inline unsigned int manipulation_handler_id() const { return dynamic_cast<SceneNode*>(graph_node_)->id(); }

    /**
     * Changes the displayed scale of this SceneNode.
     */
    virtual void set_displayed_scale (Scalar s);

    /**
     * Returns the list of vertices associated to this Graph element : Either a point, 2 points for a segment, or every points along the primitive.
     */
    virtual void GetVertices(std::set<typename GraphType::VertexId> &vertices) const = 0;

    /**
     * Subdivides this SceneNode at a given position. Depending on the type of primitive handled by this graph element,
     * the effect wont be the same : It can either
     * - Split a segment in 2 segments
     * - Add a new segment from a given point
     * - Split a triangle into 3 new triangles
     * - others to be defined.
     * @param pos position of the new point
     * @param weight weight of the new point
     * @return One of the new primitives created from this subdivision.
     */
    virtual std::shared_ptr<SceneNode> Subdivide(const typename VertexType::BasePoint &pos, Scalar weight = 0) = 0;

    /**
     * Simply returns this node's manipulator.
     */
    virtual std::shared_ptr<core::SceneNodeManipulator> manipulator() const = 0;

    /**
     * Called from the GraphSceneNode when the graph has changed and this Scenenode must be redrawn.
     */
    virtual void GraphUpdated() = 0;

    /**
     * Displays or hides the boundingbox of this SceneNode.
     * This empty function makes sure nothing is displayed here.
     */
    virtual void ShowBoundingBox(bool /*show */= true) {}

    /**
     * Rotates this SceneNode to have it "look at" a given position, completely ignoring previous rotation.
     * @param p The point to look at.
     * @param relativeTo determines in which space the new target point should be considered.
     */
    virtual void LookAt   (const Point &p, TransformSpace relativeTo = TS_WORLD);

    /**
     * Moves SceneNode to a given position. Orientation is not changed.
     * @param p The point to move to.
     * @param local if true, #p will be considered as in local coordinates. Otherwise, it will be converted from world coordinates to local coordinates.
     */
    using SceneNode::SetPos;
    virtual void SetPos   (const Point &p, TransformSpace relativeTo = TS_WORLD);

    virtual void set_adjust_size_to_param(bool b);

protected:
    GraphSceneNode *graph_node_;
};

template<typename VertexType>
class GraphElementList
{
public:

    typedef core::AbstractMapIterator<std::map<typename core::GraphT<VertexType>::VertexId, std::shared_ptr<GraphElementSceneNodeT<VertexType> > >, std::shared_ptr<GraphElementSceneNodeT<VertexType> > > VertexIterator;
    typedef core::AbstractMapIterator<std::map<typename core::GraphT<VertexType>::EdgeId, std::shared_ptr<GraphElementSceneNodeT<VertexType> > >, std::shared_ptr<GraphElementSceneNodeT<VertexType> > > EdgeIterator;
    typedef core::AbstractMapIterator<std::map<typename core::GraphT<VertexType>::EdgeType*, std::shared_ptr<GraphElementSceneNodeT<VertexType> > >, std::shared_ptr<GraphElementSceneNodeT<VertexType> > > SurfaceIterator;

    /**
     * @brief vertex_nodes_ Stores the indices of each child corresponding to each Vertex from the graph.
     */
    std::map<typename core::GraphT<VertexType>::VertexId, std::shared_ptr<GraphElementSceneNodeT<VertexType> > > vertex_nodes_;

    /**
     * @brief edge_nodes_ Stores the indices of each child corresponding to each Edge from the graph.
     */
    std::map<typename core::GraphT<VertexType>::EdgeId, std::shared_ptr<GraphElementSceneNodeT<VertexType> > > edge_nodes_;

    /**
     * @brief surface_nodes_ Stores the indices of each child corresponding to each Surface from the graph.
     */
    std::map<typename GraphT<VertexType>::EdgeType*, std::shared_ptr<GraphElementSceneNodeT<VertexType> > > surface_nodes_;

    void clear();

    inline VertexIterator vertices() { return VertexIterator(vertex_nodes_.begin(), vertex_nodes_.end()); }

    inline EdgeIterator edges() { return EdgeIterator(edge_nodes_.begin(), edge_nodes_.end()); }

    inline SurfaceIterator surfaces() { return SurfaceIterator(surface_nodes_.begin(), surface_nodes_.end()); }
};

template<typename VertexType>
GraphElementSceneNodeT<VertexType>::GraphElementSceneNodeT(core::SceneManager* scene_manager,
              SceneNode *parent,
              const std::string &name,
//                  CameraMan * camera_man,
//                  bool update = true, // why this parameter ?
              const std::string &type, const Vector &scale) :
    SceneNode(scene_manager, parent, name, type, scale, false),
    graph_node_(dynamic_cast<GraphSceneNode*>(parent_))
{
}

//template<typename VertexType>
//std::shared_ptr<SceneNodeManipulator> GraphElementSceneNodeT<VertexType>::manipulator() const
//{
//    return graph_node_->manipulator();
//}

template<typename VertexType>
void GraphElementSceneNodeT<VertexType>::set_displayed_scale(Scalar s)
{
    // Scale
//    main_object_node_->scale(s / scale_[0], s / scale_[1], s / scale_[2]);

    for (unsigned int i = 0; i < Point::RowsAtCompileTime; ++i)
    {
        scale_[i] = s;
    }
}

template<typename VertexType>
void GraphElementList<VertexType>::clear()
{
    vertex_nodes_.clear();
    edge_nodes_.clear();
    surface_nodes_.clear();
}

template<typename VertexType>
void GraphElementSceneNodeT<VertexType>::LookAt   (const Point &/*p*/, TransformSpace /*relativeTo*/)
{
//    SceneNode::LookAt(p, relativeTo);
}

template<typename VertexType>
void GraphElementSceneNodeT<VertexType>::SetPos   (const Point &p, TransformSpace relativeTo)
{
//    SceneNode::SetPos(p, relativeTo);
    Transform toParent;
    switch(relativeTo)
    {
    default:
    case TS_WORLD:
        if (parent_ == nullptr) {
            toParent = Transform::Identity();
        } else {
            toParent = parent_->GetWorldToLocalTransform();
        }
        break;
    case TS_PARENT:
        toParent = Transform::Identity();
        break;
    case TS_LOCAL:
        toParent = localToParent_;
        break;
    }

    Point real_pos = toParent * p;

    std::set<typename GraphType::VertexId> ids;
    GetVertices(ids);
    for (auto id : ids) {
        graph_node_->graph()->MovePoint(id, real_pos);
        break; // just move the first node, the rest should be updated manually later. This function is just added to handle direct displacement via x/y/z gui.
    }
    graph_node_->graph()->NotifyUpdate();

}

template<typename VertexType>
void GraphElementSceneNodeT<VertexType>::set_adjust_size_to_param(bool b)
{
    SceneNode::set_adjust_size_to_param(b);
    GraphUpdated();
}

typedef GraphElementSceneNodeT<core::WeightedPoint> WeightedGraphElementSceneNode;

} // namespace core

} // namespace expressive

#endif // GraphElementSceneNode_H
