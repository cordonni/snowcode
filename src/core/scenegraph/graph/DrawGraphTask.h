/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GRAPHDRAWER_H
#define GRAPHDRAWER_H

// core dependencies
#include "core/scenegraph/graph/GraphElementSceneNode.h"
#include "core/scenegraph/Camera.h"
#include "core/geometry/AbstractTriMesh.h"

// ork dependencies
#include "ork/render/FrameBuffer.h"
#include "ork/scenegraph/AbstractTask.h"
#include "ork/scenegraph/SceneManager.h"

namespace expressive
{

namespace core
{

/**
 * A Task that draws a graph.
 * It first draws every vertices, then every edge, and then every surfaces.
 * This improves drastically the drawing speed.
 */
template<typename VertexType>
class DrawGraphTTask :
        public ork::AbstractTask

{
public:
    DrawGraphTTask(std::shared_ptr<GraphElementList<VertexType> > graph_elements, std::shared_ptr<ork::Program> program);

    virtual ~DrawGraphTTask();

    virtual ork::ptr<ork::Task> getTask(ork::ptr<ork::Object> context);

protected:
    DrawGraphTTask();

    void init(std::shared_ptr<GraphElementList<VertexType> > graph_elements, std::shared_ptr<ork::Program> program);

    void swap(ork::ptr<DrawGraphTTask> t);

    std::shared_ptr<GraphElementList<VertexType> > graph_elements_;

    std::shared_ptr<ork::Program> program_;

    class Impl : public ork::Task
    {
    public:
        DrawGraphTTask* owner_;

        Impl(DrawGraphTTask* owner);

        virtual ~Impl();

        virtual bool run();
    };

    friend class Impl;
};

template<typename VertexType>
DrawGraphTTask<VertexType>::DrawGraphTTask() : ork::AbstractTask("DrawGraphTask")
{
}

template<typename VertexType>
DrawGraphTTask<VertexType>::DrawGraphTTask(std::shared_ptr<GraphElementList<VertexType> > graph_elements, std::shared_ptr<ork::Program> program) :
    ork::AbstractTask("DrawGraphTask")
{
    init(graph_elements, program);
}

template<typename VertexType>
void DrawGraphTTask<VertexType>::init(std::shared_ptr<GraphElementList<VertexType> > graph_elements, std::shared_ptr<ork::Program> program)
{
    this->graph_elements_ = graph_elements;
    this->program_ = program;
}

template<typename VertexType>
DrawGraphTTask<VertexType>::~DrawGraphTTask()
{
}

template<typename VertexType>
ork::ptr<ork::Task> DrawGraphTTask<VertexType>::getTask(ork::ptr<ork::Object> /*context*/)
{
    return new Impl(this);
}

template<typename VertexType>
void DrawGraphTTask<VertexType>::swap(ork::ptr<DrawGraphTTask<VertexType> > t)
{
    std::swap(graph_elements_, t->graph_elements_);
    std::swap(program_, t->program_);
}

template<typename VertexType>
DrawGraphTTask<VertexType>::Impl::Impl(DrawGraphTTask<VertexType> *owner) :
    ork::Task("DrawGraph", true, 0), owner_(owner)
{
}

template<typename VertexType>
DrawGraphTTask<VertexType>::Impl::~Impl()
{
}

template<typename VertexType>
bool DrawGraphTTask<VertexType>::Impl::run()
{
    if (ork::Logger::DEBUG_LOGGER != NULL) {
        ork::Logger::DEBUG_LOGGER->log("SCENEGRAPH", "DrawGraph");
    }
    typename GraphElementList<VertexType>::VertexIterator vertices = owner_->graph_elements_->vertices();
    typename GraphElementList<VertexType>::EdgeIterator edges = owner_->graph_elements_->edges();
    typename GraphElementList<VertexType>::SurfaceIterator surfaces = owner_->graph_elements_->surfaces();

//    owner_->program_ = ork::SceneManager::getCurrentProgram();

    ork::ptr<ork::UniformMatrix4f> localToWorld = owner_->program_->getUniformMatrix4f("localToWorld");
    ork::ptr<ork::Uniform1f> selected = owner_->program_->getUniform1f("selected");
    ork::ptr<ork::Uniform1f> hovered = owner_->program_->getUniform1f("hovered");
    ork::ptr<ork::Uniform1f> visible = owner_->program_->getUniform1f("visible");
    ork::ptr<ork::Uniform1f> wireframe = owner_->program_->getUniform1f("wireframe");
//    ork::ptr<ork::UniformMatrix4f> localToScreen = prog->getUniformMatrix4f("localToScreen");

    if (owner_->graph_elements_->vertex_nodes_.size() == 0) {
        return true;
    }
    std::shared_ptr<Camera> camera = (owner_->graph_elements_->vertex_nodes_.begin()->second)->owner()->current_camera();

//    ork::SceneManager::getCurrentFrameBuffer()->setPolygonMode(ork::LINE, ork::LINE);
    while (vertices.HasNext()) {
        std::shared_ptr<GraphElementSceneNodeT<VertexType> > vertex = vertices.Next();
        if (vertex->has_mesh()) {
//            ork::ptr<ork::SceneNode> node = vertex->internal_node();
            Matrix4 tmp = vertex->GetLocalToWorld();
            localToWorld->setMatrix(tmp.cast<float>());

            if (selected)  selected->set(vertex->selected());
            if (hovered)   hovered->set(vertex->hovered());
            if (visible)   visible->set(vertex->visible());
            if (wireframe) wireframe->set(vertex->wireframe());

            std::shared_ptr<AbstractTriMesh> m = vertex->tri_mesh();
            ork::SceneManager::getCurrentFrameBuffer()->draw(owner_->program_, *m->mesh_buffers(), m->mesh_mode(), 0, GLsizei(m->n_indices() == 0 ? m->n_vertices() : m->n_indices()), 1, 0);
//            ork::SceneManager::getCurrentFrameBuffer()->draw(owner_->program_, *vertex->tri_mesh()->internal_mesh());
        }
    }
    while (edges.HasNext()) {
        std::shared_ptr<GraphElementSceneNodeT<VertexType> > edge = edges.Next();
        if (edge->has_mesh()) {
//            ork::ptr<ork::SceneNode> node = edge->internal_node();
            Matrix4 tmp = edge->GetLocalToWorld();
            localToWorld->setMatrix(tmp.cast<float>());
            if (selected)  selected->set(edge->selected());
            if (hovered)   hovered->set(edge->hovered());
            if (visible)   visible->set(edge->visible());
            if (wireframe) wireframe->set(edge->wireframe());

            std::shared_ptr<AbstractTriMesh> m = edge->tri_mesh();
            ork::SceneManager::getCurrentFrameBuffer()->draw(owner_->program_, *m->mesh_buffers(), m->mesh_mode(), 0, GLsizei(m->n_indices() == 0 ? m->n_vertices() : m->n_indices()), 1, 0);
//            ork::SceneManager::getCurrentFrameBuffer()->draw(owner_->program_, *edge->tri_mesh()->internal_mesh());
        }
    }
    while (surfaces.HasNext()) {
        std::shared_ptr<GraphElementSceneNodeT<VertexType> > surface = surfaces.Next();
        if (surface->has_mesh()) {
//            ork::ptr<ork::SceneNode> node = surface->internal_node();
            Matrix4 tmp = surface->GetLocalToWorld();
            localToWorld->setMatrix(tmp.cast<float>());
            if (selected)  selected->set(surface->selected());
            if (hovered)   hovered->set(surface->hovered());
            if (visible)   visible->set(surface->visible());
            if (wireframe) wireframe->set(surface->wireframe());

            std::shared_ptr<AbstractTriMesh> m = surface->tri_mesh();
            ork::SceneManager::getCurrentFrameBuffer()->draw(owner_->program_, *m->mesh_buffers(), m->mesh_mode(), 0, GLsizei(m->n_indices() == 0 ? m->n_vertices() : m->n_indices()), 1, 0);
//            ork::SceneManager::getCurrentFrameBuffer()->draw(owner_->program_, *surface->tri_mesh()->internal_mesh());
        }
    }


    return true;
}

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_DRAWGRAPHTASK
extern template class DrawGraphTTask<WeightedPoint>;
#endif
#endif

}

}

#endif // GRAPHDRAWER_H
