/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GraphSceneNode2D_H
#define GraphSceneNode2D_H

// core dependencies
#include "core/geometry/BasicTriMesh.h"
//#include "core/geometry/WeightedSegment.h"
//#include "core/geometry/WeightedTriangle.h"
#include "core/graph/Graph.h"
#include "core/scenegraph/Overlay.h"
#include "core/utils/ColorTools.h"

#include "ork/render/Value.h"

#include <fstream>
#include <sstream>

namespace expressive
{

namespace core
{

/**
 * @brief The GraphSceneNode2D class
 * Allows to display a Graph as an overlay.
 * It can also export graphs to svg format, and draw the distance map for the graph.
 * (used for wish).
 */
template<typename GraphT>
class GraphSceneNode2DT : public core::Overlay
{
public:
    typedef typename GraphT::template GraphDataMapT<Scalar> WeightMap;

    GraphSceneNode2DT(const std::string &name, SceneManager* scene_manager, std::shared_ptr<GraphT> graph, Scalar scale = 1.f, const Color &color = Color(0.0, 140.0 / 255.0, 190.0 / 255.0, 1.0));
    GraphSceneNode2DT(const std::string &name, SceneManager* scene_manager, std::shared_ptr<GraphT> graph, std::shared_ptr<WeightMap> weights, Scalar maxWeight, Scalar scale = 1.f);
    virtual ~GraphSceneNode2DT();

    void DrawGraph(std::shared_ptr<GraphT> graph, const Color &color, Scalar thickness, std::fstream &str);
    void DrawSegment(const Vector2 &orig, const Vector2 &dest, const Color &color, Scalar thicknessOrig, Scalar thicknessDest, std::fstream &str);
    void DrawSegment(const Vector2 &orig, const Vector2 &desc, const Color &color, const Color &colorDest, Scalar thicknessOrig, Scalar thicknessDest, std::fstream &str);
    void DrawCircle(const Vector2 &pos, const Color &color, Scalar thickness, std::fstream &str);

    void DrawGraphDistances(std::shared_ptr<GraphT> graph, std::shared_ptr<WeightMap> weights, Scalar maxWeight, Scalar thickness, std::fstream &str);

    void AddSVGFooter(std::fstream &str);
    void AddSVGHeader(std::fstream &str, std::shared_ptr<GraphT> graph, Scalar sizeRatio);
    void ExportGraphToSVG(std::fstream &str, std::shared_ptr<GraphT> graph, const Color &color, Scalar sizeRatio);
    void AddSquareToSVG(std::fstream &str, const Vector2 &origLeft, const Vector2 &destLeft, const Vector2 &destRight, const Vector2 &origRight, const std::string &color);
    void AddSegmentToSVG(std::fstream &str, const Vector2 &orig, Scalar thicknessOrig, const Vector2 &dest, Scalar thicknessDest, const std::string &color);
    void AddSegmentToSVG(std::fstream &str, const Vector2 &orig, const Vector2 &dest, const std::string &color);
    void AddCircleToSVG(std::fstream &str, const Vector2 &point, const std::string &color, Scalar radius);

protected:
    void InitOverlay(const std::string &name);

    Color getDistanceColor(double distance);

    std::shared_ptr<GraphT> graph_;

    std::shared_ptr<WeightMap> weights_;

    Scalar maxWeight_;

    int m_SVGScale;

    Scalar m_EPSILON;

    double m_colorStep;

    int m_pathId;
};

template<typename GraphT>
GraphSceneNode2DT<GraphT>::GraphSceneNode2DT(const std::string &name, SceneManager* scene_manager, std::shared_ptr<GraphT> graph, Scalar scale, const Color &color) :
//    Overlay(scene_manager, name, Overlay::O_3DOVERLAY, "overlayMethod"),
    Overlay(scene_manager, name, Overlay::O_3DOVERLAY, "overlayMethod"),
    graph_(graph),
    weights_(nullptr),
    maxWeight_(-1.0f),
    m_SVGScale(500),
    m_EPSILON(0.0000005),
    m_colorStep(0.01),
    m_pathId(0)
{
    internal_node()->addValue(new ork::Value2f("scale", Vector2f(0.5, -0.5)));
    internal_node()->addValue(new ork::Value2f("pos", Vector2f(0.0, 0.0)));

    set_tri_mesh(std::make_shared<core::BasicTriMesh>());

    this->set_tri_mesh(this->tri_mesh_);

//    int SVGScale = 500;
//    float EPSILON = 0.0000005;
//    const double colorStep = 0.01;
//    int pathId = 0;

    std::fstream str;
    std::string n = "/tmp/" + name + ".svg";
    str.open(n.c_str(), std::ios::in | std::ios::out | std::ios::trunc);
    AddSVGHeader(str, graph, 2.0);
    DrawGraph(graph, color, scale, str);
    AddSVGFooter(str);
    str.close();
}

template<typename GraphT>
GraphSceneNode2DT<GraphT>::GraphSceneNode2DT(const std::string &name, SceneManager *scene_manager, std::shared_ptr<GraphT> graph, std::shared_ptr<WeightMap> weights, Scalar maxWeight, Scalar scale) :
    Overlay(scene_manager, name, Overlay::O_FOREGROUND, "overlayMethod"),
    graph_(graph),
    weights_(weights),
    maxWeight_(maxWeight),
    m_SVGScale(500),
    m_EPSILON(0.0000005),
    m_colorStep(0.01),
    m_pathId(0)
{
//    pathId = 0;
    internal_node()->addValue(new ork::Value2f("scale", Vector2f(0.5, -0.5)));
    internal_node()->addValue(new ork::Value2f("pos", Vector2f(0.0, 0.0)));

    set_tri_mesh(std::make_shared<BasicTriMesh>());

    std::fstream str;
    std::string n = "/tmp/" + name + ".svg";
    str.open(n.c_str(), std::ios::in | std::ios::out | std::ios::trunc);
    AddSVGHeader(str, graph, 2.0);
    DrawGraphDistances(graph, weights, maxWeight, scale, str);
    AddSVGFooter(str);
    str.close();
    this->set_tri_mesh(this->tri_mesh_);
}

template<typename GraphT>
GraphSceneNode2DT<GraphT>::~GraphSceneNode2DT()
{
}

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::DrawGraph(std::shared_ptr<GraphT> graph, const Color &color, Scalar thickness, std::fstream &str)
{
    AABBox2D box = graph->axis_bounding_box();
    float length = std::max(box.width(), box.height()) * 1.2;
//    Point2D pOrig = box.min();
    Point2 pMid = box.ComputeCenter();

    typename GraphT::EdgeIteratorPtr edges = graph->GetEdges();
    while (edges->HasNext()) {
        std::shared_ptr<typename GraphT::Edge> edge = edges->Next();

        std::shared_ptr<typename GraphT::Vertex> start = edge->GetStart();
        std::shared_ptr<typename GraphT::Vertex> end = edge->GetEnd();

        Point2 spos = ((start->pos() - pMid) * 2.0 / length);
        Point2 epos = ((end->pos() - pMid) * 2.0 / length);
        DrawSegment(spos, epos, color, thickness, thickness, str);

//        DrawCircle(spos, color, start->pos().weight() * thickness, str);
//        DrawCircle(epos, color, end->pos().weight() * thickness, str);
    }
}

template<>
void CORE_API GraphSceneNode2DT<core::WeightedGraph2D>::DrawGraph(std::shared_ptr<core::WeightedGraph2D> graph, const Color &color, Scalar thickness, std::fstream &str);

template<typename GraphT>
Color GraphSceneNode2DT<GraphT>::getDistanceColor(double distance)
{
    if (distance > 1.0) {
        return Color(0.7, 0.7, 0.7);
    }
    HSV input;
    input[0] = (1.f - (float) distance) * 240.f;
    input[1] = 0.8f;
    input[2] = 0.8f;
    return hsv2rgb(input);

//    double newHue = distance;// * 1.0/4.0;// to have red as highest value and dark blue as lowest
//    return HSLToRGB(newHue, 0.8, 0.8);
}

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::DrawGraphDistances(std::shared_ptr<GraphT> graph, std::shared_ptr<WeightMap> weights, Scalar maxWeight, Scalar thickness, std::fstream &str)
{
    const double colorStep = 0.01;

    AABBox2D box = graph->axis_bounding_box();
    float length = std::max(box.width(), box.height()) * 1.2;
    Point2 pMid = box.ComputeCenter();

    typename GraphT::EdgeIteratorPtr edges = graph->GetEdges();
    while (edges->HasNext()) {
        typename GraphT::EdgePtr edge = edges->Next();


        for (unsigned int i = 0; i< edge->GetSize(); ++i) {
            typename GraphT::VertexPtr vertex = edge->GetVertex(i);
            float width = thickness;
            double dist = weights->at(vertex->id()) / maxWeight;
            DrawCircle((vertex->pos() - pMid) * 2.0 / length, getDistanceColor(dist), width, str);
        }

        std::shared_ptr<typename GraphT::Vertex> prev = edge->GetVertex(0);
        std::shared_ptr<typename GraphT::Vertex> next = edge->GetVertex(1);

        Scalar dprev = weights->at(prev->id()) / maxWeight;
        Scalar dnext = weights->at(next->id()) / maxWeight;
        Scalar dedge = weights->at(edge->id()) / maxWeight;


        if (dprev < dnext) {
        } else {
            auto tmp = next;
            next = prev;
            prev = tmp;
            dprev = dnext;
        }
        Scalar wprev = thickness;
        Scalar wnext = thickness;

        Point2 dir = (next->pos() - prev->pos());
        double len = dir.norm();
        double nbSegments = std::round(len / colorStep);
        dir = dir / nbSegments;

        Scalar distOffset = (dedge - dprev) / nbSegments;
        Scalar widthOffset = (wnext - wprev) / nbSegments;

        for (int j = 1; j < round(nbSegments) + 1; ++j) {
            Vector2 p = prev->pos() + dir * (j - 1);
            Vector2 c = prev->pos() + dir * j;
            double dp = dprev + distOffset * (j - 1);
            double dc = dprev + distOffset * j;
            double wp = wprev + widthOffset * (j - 1);
            double wc = wprev + widthOffset * j;


            p = (p - pMid) * 2.0 / length; // rescale so the whole graph fits in the screen.
            c = (c - pMid) * 2.0 / length;

            DrawSegment(p, c, getDistanceColor(dp), getDistanceColor(dc), wp, wc, str);
        }
    }
}

template<>
void CORE_API GraphSceneNode2DT<core::WeightedGraph2D>::DrawGraphDistances(std::shared_ptr<core::WeightedGraph2D> graph, std::shared_ptr<WeightMap> weights, Scalar maxWeight, Scalar thickness, std::fstream &str);

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::DrawSegment(const Vector2 &orig, const Vector2 &dest, const Color &color, Scalar thicknessOrig, Scalar thicknessDest, std::fstream &str)
{
    return DrawSegment(orig, dest, color, color, thicknessOrig, thicknessDest, str);
}

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::DrawSegment(const Vector2 &orig, const Vector2 &dest, const Color &color, const Color &colorDest, Scalar thicknessOrig, Scalar thicknessDest, std::fstream &str)
{
    Vector2 dir = (dest - orig).normalized();
    Vector2 norm = Vector2(dir[1], -dir[0]);

    Vector2 origLeft  = orig - norm * thicknessOrig;
    Vector2 origRight = orig + norm * thicknessOrig;
    Vector2 destLeft  = dest - norm * thicknessDest;
    Vector2 destRight = dest + norm * thicknessDest;
    double B = VectorTools::angle2d(destLeft - destRight, destLeft - origLeft);
    if (B > M_PI) {
        B -= M_PI;
    }


    Vector v = VectorTools::Rotate (Vector(dir[0], dir[1], 0.0), Vector(0.0, 0.0, 1.0), B);
    origLeft = orig + Vector2(v[0], v[1]) * thicknessOrig;
    destLeft = dest + Vector2(v[0], v[1]) * thicknessDest;
    v = VectorTools::Rotate (Vector(dir[0], dir[1], 0.0), Vector(0.0, 0.0, 1.0), -B);
    origRight = orig + Vector2(v[0], v[1]) * thicknessOrig;
    destRight = dest + Vector2(v[0], v[1]) * thicknessDest;

    BasicTriMesh::VertexHandle vh1 = tri_mesh_->add_point(Point(origLeft[0], origLeft[1], 0.0));
    tri_mesh_->set_color(vh1, color);
    BasicTriMesh::VertexHandle vh2 = tri_mesh_->add_point(Point(origRight[0], origRight[1], 0.0));
    tri_mesh_->set_color(vh2, color);
    BasicTriMesh::VertexHandle vh3 = tri_mesh_->add_point(Point(destLeft[0], destLeft[1], 0.0));
    tri_mesh_->set_color(vh3, colorDest);
    BasicTriMesh::VertexHandle vh4 = tri_mesh_->add_point(Point(destRight[0], destRight[1], 0.0));
    tri_mesh_->set_color(vh4, colorDest);

    tri_mesh_->add_face(vh1, vh2, vh3);
    tri_mesh_->add_face(vh2, vh3, vh4);

    if (str.is_open()) {
//        AddSquareToSVG(str, origLeft, destLeft, destRight, origRight, ColorToString(color[0]));
        AddSegmentToSVG(str, orig, dest, ColorToString(color));
    }
}

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::DrawCircle(const Vector2 &pos, const Color &color, Scalar thickness, std::fstream &str)
{
    int loopRes = 360 / 10;//thickness > 0.1 ? 100 : 64;

    Scalar alpha = 360.0 / loopRes;
    Scalar theta = 360.0;

    std::shared_ptr<AbstractTriMesh> mesh = tri_mesh();
    BasicTriMesh::VertexHandle vh_center = mesh->add_point(Point(pos[0], pos[1], 0.0));
    mesh->set_color(vh_center, color);

    std::vector<BasicTriMesh::VertexHandle> handles;

    for (int i = 0; i <= loopRes; ++i) {
        theta -= alpha;
        Scalar x = std::sin(theta * M_PI / 180.0) * thickness;
        Scalar y = std::cos(theta * M_PI / 180.0) * thickness;

        BasicTriMesh::VertexHandle vh = mesh->add_point(Point(pos[0] + x, pos[1] + y, 0.0));
        mesh->set_color(vh, color);
        handles.push_back(vh);
    }
    for (unsigned int i = 0; i < handles.size() - 1; ++i) {
        mesh->add_face(vh_center, handles[i], handles[i + 1]);
    }
    mesh->add_face(vh_center, handles[handles.size() - 1], handles[0]);


    if (str.is_open()) {
        AddCircleToSVG(str, pos, ColorToString(color)/*QColor(color[0] * 255, color[1] * 255, color[2] * 255).name().toStdString()*/, thickness);
    }
}

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::AddCircleToSVG(std::fstream &str, const Vector2 &point, const std::string &color, Scalar radius)
{
    Point2 p = ((point + Vector2(1.0, 1.0)) *  m_SVGScale / 2.0) + Vector2(1.0, 1.0);
    radius = radius * m_SVGScale / 2.0;
    str << "            <circle ";
    str << "cx=\"" << p[0] << "\" ";
    str << "cy=\"" << p[1] << "\" ";
    str << "r=\"" << radius << "\" ";
    str << "stroke=\"none\" ";
    str << "fill=\"" << color << "\"";
    str << "/>" << std::endl;
}

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::AddSegmentToSVG(std::fstream &str, const Vector2 &orig, Scalar thicknessOrig, const Vector2 &dest, Scalar thicknessDest, const std::string &color)
{
    Vector2 dir = (dest - orig).normalized();
    Vector2 norm = Vector2(dir[1], -dir[0]);

    Vector2 origLeft  = orig - norm * thicknessOrig;
    Vector2 origRight = orig + norm * thicknessOrig;
    Vector2 destLeft  = dest - norm * thicknessDest;
    Vector2 destRight = dest + norm * thicknessDest;
    double B = VectorTools::angle2d(destLeft - destRight, destLeft - origLeft);
    if (B > M_PI) {
        B -= M_PI;
    }


    Vector v = VectorTools::Rotate (Vector(dir[0], dir[1], 0.0), Vector(0.0, 0.0, 1.0), B);
    origLeft = orig + Vector2(v[0], v[1]) * thicknessOrig;
    destLeft = dest + Vector2(v[0], v[1]) * thicknessDest;
    v = VectorTools::Rotate (Vector(dir[0], dir[1], 0.0), Vector(0.0, 0.0, 1.0), -B);
    origRight = orig + Vector2(v[0], v[1]) * thicknessOrig;
    destRight = dest + Vector2(v[0], v[1]) * thicknessDest;

    str << "            <path " << std::endl;
    // style
    str << "                style=\"fill-opacity:1;";
    str << "stroke:none;";
    str << "fill:" << color <<  ";";
    str << "\"" << std::endl;

    // points
    origLeft = (origLeft + Vector2(1.0, 1.0)) *  m_SVGScale / 2.0;
    origRight = (origRight + Vector2(1.0, 1.0)) *  m_SVGScale / 2.0;
    destRight = (destRight + Vector2(1.0, 1.0)) *  m_SVGScale / 2.0;
    destLeft = (destLeft + Vector2(1.0, 1.0)) *  m_SVGScale / 2.0;
    str << "                d=\"M ";
    str << origLeft[0]  << "," << origLeft[1] << " ";
    str << origRight[0] << "," << origRight[1] << " ";
    str << destRight[0] << "," << destRight[1] << " ";
    str << destLeft[0] << "," << destLeft[1] << " ";
    str<< "z\"" << std::endl;

    str << "                id=\"path" << m_pathId++ << "\"" << std::endl;
    str << "                inkscape:connector-curvature=\"0\""<< std::endl;
    str << "                />" << std::endl;
}

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::AddSegmentToSVG(std::fstream &str, const Vector2 &orig, const Vector2 &dest, const std::string &color){
//    Vector2 dir = (dest - orig).normalized();
//    Vector2 norm = Vector2(dir[1], -dir[0]);

    str << "            <path " << std::endl;
    // style
    str << "                style=\"fill-opacity:1;";
    str << "fill:none;";
//    str << "stroke-width:1px;";
    str << "stroke-opacity:1;";
    str << "stroke:" << color <<  ";";
    str << "\"" << std::endl;

    // points
    Vector2 orig2 = (orig + Vector2(1.0, 1.0)) *  m_SVGScale / 2.0;
    Vector2 dest2 = (dest + Vector2(1.0, 1.0)) *  m_SVGScale / 2.0;
    str << "                d=\"M ";
    str << orig2[0]  << "," << orig2[1] << " ";
    str << dest2[0] << "," << dest2[1] << " ";
    str<< "z\"" << std::endl;

    str << "                id=\"path" << m_pathId++ << "\"" << std::endl;
    str << "                inkscape:connector-curvature=\"0\""<< std::endl;
    str << "                />" << std::endl;
}

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::AddSquareToSVG(std::fstream &str, const Vector2 &origLeftInput, const Vector2 &destLeftInput, const Vector2 &destRightInput, const Vector2 &origRightInput, const std::string &color)
{
    str << "            <path " << std::endl;
    // style
    str << "                style=\"fill-opacity:1;";
    str << "stroke:none;";
    str << "fill:" << color <<  ";";
    str << "\"" << std::endl;

    // points
    Vector2 origLeft = (origLeftInput + Vector2(1.0, 1.0)) *  m_SVGScale / 2.0;
    Vector2 origRight = (origRightInput + Vector2(1.0, 1.0)) *  m_SVGScale / 2.0;
    Vector2 destRight = (destRightInput + Vector2(1.0, 1.0)) *  m_SVGScale / 2.0;
    Vector2 destLeft = (destLeftInput + Vector2(1.0, 1.0)) *  m_SVGScale / 2.0;
    str << "                d=\"M ";
    str << origLeft[0] + 1.0 << "," << origLeft[1] + 1.0 << " ";
    str << origRight[0] + 1.0 << "," << origRight[1] + 1.0 << " ";
    str << destRight[0] + 1.0 << "," << destRight[1] + 1.0 << " ";
    str << destLeft[0]  + 1.0 << "," << destLeft[1] + 1.0 << " ";
    str << "z\"" << std::endl;

    str << "                id=\"path" << m_pathId++ << "\"" << std::endl;
    str << "                inkscape:connector-curvature=\"0\""<< std::endl;
    str << "                />" << std::endl;
}

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::ExportGraphToSVG(std::fstream &str, std::shared_ptr<GraphT> graph, const Color &color, Scalar sizeRatio)
{
//    QColor col(color[0] * 255, color[1] * 255, color[2] * 255);
    std::string colName = ColorToString(color);
    typename GraphT::EdgeIteratorPtr edges = graph->GetEdges();
    while (edges->HasNext()) {
        typename GraphT::EdgePtr edge = edges->Next();
        typename GraphT::VertexPtr s = edge->GetStart();
        typename GraphT::VertexPtr e = edge->GetEnd();

        AddCircleToSVG(str, s->pos(), colName, sizeRatio);
        AddCircleToSVG(str, e->pos(), colName, sizeRatio);
        AddSegmentToSVG(str, s->pos(), sizeRatio, e->pos(), sizeRatio, colName);
    }
}

template<>
void CORE_API GraphSceneNode2DT<core::WeightedGraph2D>::ExportGraphToSVG(std::fstream &str, std::shared_ptr<core::WeightedGraph2D> graph, const Color &color, Scalar sizeRatio);

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::AddSVGHeader(std::fstream &str, std::shared_ptr<GraphT> graph, Scalar /*sizeRatio*/)
{
    int SVGScale = 500;

    AABBox2D b = graph->axis_bounding_box();
//    b.enlarge(sizeRatio);
//    b.enlarge(500);
    str << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" << std::endl;
    str << "<!-- Created with Inkscape (http://www.inkscape.org/) --> " << std::endl;
    str << "    <svg" << std::endl;
    str << "        xmlns:dc=\"http://purl.org/dc/elements/1.1/\" " << std::endl;
    str << "        xmlns:cc=\"http://creativecommons.org/ns#\" " << std::endl;
    str << "        xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" " << std::endl;
    str << "        xmlns:svg=\"http://www.w3.org/2000/svg\" " << std::endl;
    str << "        xmlns=\"http://www.w3.org/2000/svg\" " << std::endl;
    str << "        xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\" " << std::endl;
    str << "        xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" " << std::endl;
//    str << "        width=\"744.09448819\" " << std::endl;
//    str << "        height=\"1052.3622047\" " << std::endl;
    str << "        width=\"" << SVGScale << "\" " << std::endl;
    str << "        height=\"" << SVGScale << "\" " << std::endl;
    str << "        id=\"svg2\" " << std::endl;
    str << "        version=\"1.1\" " << std::endl;
    str << "        inkscape:version=\"0.48.3.1 r9886\" " << std::endl;
    str << "        sodipodi:docname=\"New document 1\"> " << std::endl;
    str << "        <defs " << std::endl;
    str << "            id=\"defs4\" /> " << std::endl;
    str << "        <sodipodi:namedview " << std::endl;
    str << "            id=\"base\" " << std::endl;
    str << "            pagecolor=\"#ffffff\" " << std::endl;
    str << "            bordercolor=\"#666666\" " << std::endl;
    str << "            borderopacity=\"1.0\" " << std::endl;
    str << "            inkscape:pageopacity=\"0.0\" " << std::endl;
    str << "            inkscape:pageshadow=\"2\" " << std::endl;
    str << "            inkscape:zoom=\"0.05\" " << std::endl;
    str << "            inkscape:cx=\""<< SVGScale / 2.0 << "\" " << std::endl;
    str << "            inkscape:cy=\""<< SVGScale / 2.0 << "\" " << std::endl;
    str << "            inkscape:document-units=\"px\" " << std::endl;
    str << "            inkscape:current-layer=\"layer1\" " << std::endl;
    str << "            showgrid=\"false\" " << std::endl;
    str << "            inkscape:window-width=\"1920\" " << std::endl;
    str << "            inkscape:window-height=\"1124\" " << std::endl;
    str << "            inkscape:window-x=\"0\" " << std::endl;
    str << "            inkscape:window-y=\"24\" " << std::endl;
    str << "            inkscape:window-maximized=\"1\" /> " << std::endl;
    str << "        <metadata " << std::endl;
    str << "            id=\"metadata7\"> " << std::endl;
    str << "            <rdf:RDF> " << std::endl;
    str << "                <cc:Work " << std::endl;
    str << "                    rdf:about=\"\"> " << std::endl;
    str << "                    <dc:format>image/svg+xml</dc:format> " << std::endl;
    str << "                    <dc:type " << std::endl;
    str << "                        rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" /> " << std::endl;
    str << "                    <dc:title></dc:title> " << std::endl;
    str << "                </cc:Work> " << std::endl;
    str << "            </rdf:RDF> " << std::endl;
    str << "        </metadata> " << std::endl;
    str << "        <g " << std::endl;
    str << "            inkscape:label=\"Layer 1\" " << std::endl;
    str << "            inkscape:groupmode=\"layer\" " << std::endl;
    str << "            id=\"layer1\"> " << std::endl;
}

template<typename GraphT>
void GraphSceneNode2DT<GraphT>::AddSVGFooter(std::fstream &str)
{
    str << "        </g> " << std::endl;
    str << "    </svg> " << std::endl;
}

typedef GraphSceneNode2DT<core::Graph2D> GraphSceneNode2D;
typedef GraphSceneNode2DT<core::WeightedGraph2D> WeightedGraphSceneNode2D;

} // namespace core

} // namespace expressive


#endif // GraphSceneNode2D_H

