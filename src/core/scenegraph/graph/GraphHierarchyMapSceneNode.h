/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GraphHierarchyMapSceneNode_H
#define GraphHierarchyMapSceneNode_H

#include "core/graph/GraphHierarchyMap.h"
#include "core/scenegraph/SceneNode.h"

namespace expressive
{

namespace core
{

class SceneManager;

/**
 * @brief The GraphHierarchyMapSceneNode class inherits from core::GraphHierarchyMap to add
 * the ability to draw it ontop of a given graph.
 * TODO : Add it as a child of the given graph.
 */
class CORE_API GraphHierarchyMapSceneNode : public core::GraphHierarchyMap, public core::SceneNode
{
public:
    /**
     * @brief GraphHierarchyMapSceneNode Simply creates the hierarchy map. Visualization will be added when it will actually be computed.
     * @param scene The scene in which the hierarchy has to be displayed. // todo : replace that by the parent.
     * @param graph The Graph for which the hierarchy has to be computed.
     */
    GraphHierarchyMapSceneNode(SceneManager* scene, core::WeightedGraph* graph);

    virtual ~GraphHierarchyMapSceneNode();

//    virtual void Notify(const core::Graph::GraphChanges &changes);
    /**
     * @brief AddAnchor Adds an anchor point for the given vertex on the given parent and make it visible.
     * @param vertex VertexId of the child.
     * @param parent EdgeId of the parent.
     * @param anchor NodeAnchor that links the 2 previous parameters : contains the relative position of %vertex in %parent.
     */
    virtual void AddAnchor(VertexId vertex, EdgeId parent, const NodeAnchor &anchor);

    /**
     * @brief RemoveAnchor
     * @param vertex
     */
    virtual void RemoveAnchor(VertexId vertex);

protected:
    /**
     * @brief RemoveSceneNode Removes the link between a vertex and it's parent.
     * @param vertex The attached vertex.
     * @param parent The parent.
     */
    void RemoveSceneNode(VertexId vertex, EdgeId parent);

//    std::shared_ptr<SceneManager> scene_;  /*< The scene in which nodes have to be drawn. */

//    std::shared_ptr<SceneNode> main_node_;  /*< This node's main node. Every link will actually be children of this node. */

    std::map<std::pair<VertexId, core::EdgeId>, std::shared_ptr<SceneNode > > scene_nodes_;  /*< The nodes that represents the links between vertices and edges. */
};

} // namespace core

} // namespace expressive

#endif // GraphHierarchyMapSceneNode_H
