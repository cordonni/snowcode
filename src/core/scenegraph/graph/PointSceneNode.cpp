#include "core/scenegraph/graph/PointSceneNode.h"

// core dependencies
#include <core/graph/GraphCommands.h>
#include <core/utils/ConfigLoader.h>
#include "core/scenegraph/SceneNodeFactory.h"
#include "core/scenegraph/graph/GraphSceneNode.h"

#include "core/ExpressiveEngine.h"

using namespace std;

namespace expressive
{

namespace core
{

template<>
void PointSceneNodeT<core::WeightedPoint>::GraphUpdated()
{
    const core::WeightedPoint &p = vertex_->pos();
    SceneNode::SetPos(p, SceneNode::TS_PARENT);

    Scalar weight = this->adjust_size_to_params_ ? p.weight() : 1.0;
    this->SetScale(weight * this->graph_node_->point_scale());
//    this->Scale(weight/* / this->scale_[0]*/);

    this->set_visible(this->visible_);
}

template<>
core::SceneNode *PointSceneNodeT<core::WeightedPoint>::IsIntersectingRay (const core::BasicRay &ray, Scalar &dist, bool proxy) const
{
    UNUSED(proxy);
    core::BasicRay currentRay = this->GetRayInLocal(ray);
//    currentRay.set_direction(currentRay.direction().normalized());
    Scalar weight = this->adjust_size_to_params_ ? this->vertex_->pos().weight() : 1.0;
//    if (currentRay.IntersectsSphere(Point::Zero(), /*scale_[0] * weight*/1.0, dist)) {
    if (ray.IntersectsSphere(GetPointInWorld(), weight * this->graph_node_->point_scale(), dist)) {

        return (core::SceneNode*) this;
    }

    return nullptr;
}

static SceneNodeFactory::Type<WeightedPoint, PointSceneNodeT<WeightedPoint>> WeightedPointSceneNodeType;
static SceneNodeFactory::Type<PointPrimitive, PointSceneNodeT<PointPrimitive>> PointSceneNodeType;

} // namespace core

} // namespace expressive


