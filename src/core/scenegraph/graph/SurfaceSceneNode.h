/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DISPLAYEDWEIGHTEDSURFACE_H
#define DISPLAYEDWEIGHTEDSURFACE_H

// core dependencies
#include <core/graph/Graph.h>
#include <core/graph/GraphCommands.h>
#include <core/geometry/BasicTriMesh.h>
#include <core/geometry/WeightedTriangle.h>
#include <core/geometry/BasicRay.h>
#include "core/scenegraph/SceneNode.h"
#include "core/scenegraph/SceneManager.h"
#include "core/scenegraph/graph/GraphSceneNode.h"

namespace expressive
{

namespace core
{

/**
 * @brief The DisplayedWeightedPoint class
 * Draw a Graph's WeightedSegment.
 */
template<typename VertexType>
class CORE_API SurfaceSceneNodeT : public GraphElementSceneNodeT<VertexType>
{
public:
    EXPRESSIVE_MACRO_NAME("SurfaceSceneNode")
    typedef GraphElementSceneNodeT<VertexType> GraphElementSceneNode;
    typedef core::GraphT<VertexType> GraphType;

    SurfaceSceneNodeT(core::SceneManager*owner, core::SceneNode *parent, const std::string &name, const std::shared_ptr<void> param, const Vector &scale = Vector::Ones());

    virtual ~SurfaceSceneNodeT();

    inline std::shared_ptr<core::WeightedEdge> edge() const { return edge_; }

    virtual void GetVertices(std::set<typename GraphType::VertexId> &vertices) const;

    virtual std::shared_ptr<core::SceneNode> Subdivide(const typename VertexType::BasePoint &pos, Scalar weight = 0);

    virtual std::shared_ptr<core::SceneNodeManipulator> manipulator() const { return this->graph_node_->manipulator(); }

    std::shared_ptr<core::BasicTriMesh> CreateSurfaceObject();

    virtual void set_selected(bool selected, bool hover_parent = true);

    virtual void GraphUpdated();

protected:
    std::shared_ptr<typename GraphType::Edge> edge_;

    typename GraphType::EdgeType *primitive_;

};

template<typename VertexType>
SurfaceSceneNodeT<VertexType>::SurfaceSceneNodeT(core::SceneManager *owner, core::SceneNode *parent, const std::string &name, const std::shared_ptr<void> param, const Vector &scale)
    : GraphElementSceneNodeT<VertexType>(owner, parent, name, StaticName(), scale), edge_(std::static_pointer_cast<typename GraphType::Edge>(param)), primitive_(edge_->edgeptr().get())
{
    SceneNode::set_tri_mesh(this->CreateSurfaceObject());
}

template<typename VertexType>
SurfaceSceneNodeT<VertexType>::~SurfaceSceneNodeT()
{
}

template<typename VertexType>
std::shared_ptr<core::BasicTriMesh> SurfaceSceneNodeT<VertexType>::CreateSurfaceObject()
{
    std::shared_ptr<core::BasicTriMesh> mesh = std::make_shared<core::BasicTriMesh>();

    Color col = this->graph_node_->geometry_color();

    Point p1 = primitive_->GetPoint(0);
    Point p2 = primitive_->GetPoint(1);
    Point p3 = primitive_->GetPoint(2);

    Vector v1 = (p2 - p1).normalized();
    Vector v2 = (p3 - p2).normalized();
    Vector n = v1.cross(v2).normalized();

    core::BasicTriMesh::VertexHandle vh0 = mesh->add_vertex(primitive_->GetCenter());
    std::vector<core::BasicTriMesh::VertexHandle> handles;
    mesh->set_normal(vh0, n);
    mesh->set_color(vh0, col);

    for (unsigned int i = 0; i < primitive_->GetSize(); ++i) {
        handles.push_back(mesh->add_vertex(primitive_->GetPoint(i)));
        mesh->set_normal(handles.back(), n);
        mesh->set_color(handles.back(), col);
    }
    handles.push_back(handles[0]);

    for (unsigned int i = 1; i < handles.size(); ++i) {
        mesh->add_face(vh0, handles[i - 1], handles[i]);

    }


    return mesh;

}

template<typename VertexType>
void SurfaceSceneNodeT<VertexType>::GraphUpdated()
{
    SceneNode::set_tri_mesh(this->CreateSurfaceObject());
}

template<typename VertexType>
void SurfaceSceneNodeT<VertexType>::set_selected(bool selected, bool hover_parent)
{
    typename GraphType::EdgeIteratorPtr it = this->graph_node_->graph()->GetEdges(edge_->edgeptr());
    while(it->HasNext()) {
        std::shared_ptr<typename GraphType::Edge> e = it->Next();
        this->graph_node_->graph()->set_selected(e->id(), selected);
    }

    GraphElementSceneNode::set_selected(selected, hover_parent);
}

template<typename VertexType>
std::shared_ptr<core::SceneNode> SurfaceSceneNodeT<VertexType>::Subdivide(const typename VertexType::BasePoint &pos, Scalar /*weight*/)
{
    VertexType p(pos);
    std::shared_ptr<core::SubdivideEdgeCommand<VertexType> > command = std::make_shared<core::SubdivideEdgeCommand<VertexType> >(this->graph_node_->graph(), edge_->id(), -1, p);

    this->owner_->command_manager()->run(command);

    return this->graph_node_->GetNode(command->vertex_id_);
}

template<>
std::shared_ptr<core::SceneNode> SurfaceSceneNodeT<core::WeightedPoint>::Subdivide(const typename core::WeightedPoint::BasePoint &pos, Scalar weight)
{
    if (weight == 0){
        for (unsigned int i = 0; i < edge_->edge().GetSize(); ++i) {
            weight += edge_->nonconst_edge().GetPoint(i).weight();
        }
        weight /= (Scalar) edge_->edge().GetSize();
    }
    core::WeightedPoint p(pos, weight);
    std::shared_ptr<core::SubdivideEdgeCommand<core::WeightedPoint> >command = std::make_shared<core::SubdivideEdgeCommand<core::WeightedPoint> >(this->graph_node_->graph(), edge_->id(), -1, p);

    this->owner_->command_manager()->run(command);

    return this->graph_node_->GetNode(command->vertex_id_);
}

template<typename VertexType>
void SurfaceSceneNodeT<VertexType>::GetVertices(std::set<typename GraphType::VertexId> &vertices) const
{
    typename GraphType::EdgeIteratorPtr it = this->graph_node_->graph()->GetEdges(edge_->edgeptr());
    while (it->HasNext()) {
        std::shared_ptr<typename GraphType::Edge> e = it->Next();
        vertices.insert(e->GetStart()->id());
        vertices.insert(e->GetEnd()->id());
    }
}

} // namespace core

} // namespace expressive

#endif // DISPLAYEDWEIGHTEDSURFACE_H
