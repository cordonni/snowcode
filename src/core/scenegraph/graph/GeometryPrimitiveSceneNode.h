/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DisplayedWeightedGeometryPrimitive_H
#define DisplayedWeightedGeometryPrimitive_H

// core dependencies
#include <core/graph/Graph.h>
#include <core/graph/GraphCommands.h>
#include <core/geometry/WeightedSegment.h>
#include <core/geometry/BasicRay.h>
#include <core/geometry/VectorTools.h>
#include "core/scenegraph/SceneNode.h"
#include "core/scenegraph/graph/GraphSceneNode.h"
#include "core/geometry/MeshTools.h"

namespace expressive
{

namespace core
{

/**
 * @brief The DisplayedWeightedPoint class
 * Draw a Graph's WeightedSegment.
 */
template<typename VertexType>
class GeometryPrimitiveSceneNodeT : public GraphElementSceneNodeT<VertexType>
{
public:
    EXPRESSIVE_MACRO_NAME("GeometryPrimitiveSceneNode")
    typedef GraphSceneNodeT<VertexType> GraphSceneNode;
    typedef GraphElementSceneNodeT<VertexType> GraphElementSceneNode;
    typedef core::GraphT<VertexType> GraphType;

    GeometryPrimitiveSceneNodeT(core::SceneManager*owner, core::SceneNode *parent, const std::string &name, const std::shared_ptr<void> param, const Vector &scale = Vector::Ones());

    virtual ~GeometryPrimitiveSceneNodeT();

    virtual core::SceneNode *IsIntersectingRay (const core::BasicRay &ray, Scalar &dist, bool proxy = true) const;

    inline std::shared_ptr<typename GraphType::Edge> edge() const { return edge_; }

    virtual void set_selected(bool selected, bool hover_parent = true);

    virtual void GetVertices(std::set<typename GraphType::VertexId> &vertices) const;

    virtual std::shared_ptr<core::SceneNode> Subdivide(const typename VertexType::BasePoint &pos, Scalar weight = 0);

    virtual std::shared_ptr<core::SceneNodeManipulator> manipulator() const { return this->graph_node_->manipulator(); }

    virtual void set_displayed_scale(Scalar s);

    virtual void GraphUpdated();

protected:
    struct Transformation
    {
        

        Transformation()
            : axis_rot_(Vector::Zero()), translation_(Vector::Zero()),
              scale_(Vector::Zero()), angle_(0.0){}

        Vector axis_rot_;
        Vector translation_;
        Vector scale_;
        Scalar angle_;
    };

    Scalar GetStartWeight() const;
    Scalar GetEndWeight() const;

//    void ComputeMeshModelTransformation(const core::WeightedGeometryPrimitive &primitive);

    Transformation ComputeMeshModelTransformation(const VertexType &start, const VertexType &end);

    virtual core::SceneNode *IsIntersectingRay (const core::BasicRay &ray, const Transformation &trans, const VertexType &start, const VertexType &end, Scalar &dist) const;

    // Transform the given ray from the parent's frame to get it in the primitive's frame
    core::BasicRay GetRayInObjectFrame(const core::BasicRay& ray, const Transformation &trans) const;

//    virtual void set_internal_material(const string &name);

    void CreateMesh();

    void ApplyTransformations();

    std::shared_ptr<typename GraphType::Edge> edge_;

    Scalar weight1_;

    Scalar weight2_;

    Transformation transformation_;

};

template<typename VertexType>
GeometryPrimitiveSceneNodeT<VertexType>::GeometryPrimitiveSceneNodeT(core::SceneManager *owner, core::SceneNode *parent, const std::string &name, const std::shared_ptr<void> param, const Vector &scale)
    : GraphElementSceneNodeT<VertexType>(owner, parent, name, StaticName(), scale), edge_(std::static_pointer_cast<typename GraphType::Edge>(param))
{
    CreateMesh();
}

template<typename VertexType>
GeometryPrimitiveSceneNodeT<VertexType>::~GeometryPrimitiveSceneNodeT()
{
}

template<typename VertexType>
Scalar GeometryPrimitiveSceneNodeT<VertexType>::GetStartWeight() const
{
    return 1.0;
}

template<typename VertexType>
Scalar GeometryPrimitiveSceneNodeT<VertexType>::GetEndWeight() const
{
    return 1.0;
}

template<>
Scalar GeometryPrimitiveSceneNodeT<core::WeightedPoint>::GetStartWeight() const
{
    return edge_->GetStart()->pos().weight();
}

template<>
Scalar GeometryPrimitiveSceneNodeT<core::WeightedPoint>::GetEndWeight() const
{
    return edge_->GetEnd()->pos().weight();
}

template<typename VertexType>
void GeometryPrimitiveSceneNodeT<VertexType>::CreateMesh()
{
    const VertexType &prev = edge_->GetStart()->pos();
    const VertexType &cur = edge_->GetEnd()->pos();

    if ((prev-cur).squaredNorm() < 0.000001) { // if the 2 points are exactly the same, we don't want to draw anything
        return;
    }

    weight1_ = GetStartWeight() * this->scale_[1];
    weight2_ = GetEndWeight() * this->scale_[1];

    ApplyTransformations();
    auto m = core::MeshTools::CreateCylinder(expressive::Config::getIntParameter("graph_default_segment_faces"), weight2_ / weight1_, Point::Zero(), Vector::UnitY() * weight1_, Vector::UnitZ() * weight1_, Vector::UnitX() * transformation_.scale_[0]);
    m->FillColor(this->graph_node_->geometry_color());
    SceneNode::set_tri_mesh(m);

 }

template<typename VertexType>
void GeometryPrimitiveSceneNodeT<VertexType>::GraphUpdated()
{
    CreateMesh();
}

template<typename VertexType>
void GeometryPrimitiveSceneNodeT<VertexType>::ApplyTransformations()
{
    const VertexType &prev = edge_->GetStart()->pos();
    const VertexType &cur = edge_->GetEnd()->pos();

    transformation_ = ComputeMeshModelTransformation(prev, cur);

    this->SceneNode::SetPos(transformation_.translation_, SceneNode::TS_PARENT);

//    this->Scale(Vector(transformation_.scale_[0] / this->scale_[0], transformation_.scale_[1] / this->scale_[1], transformation_.scale_[2] / this->scale_[2]));
    this->ResetOrientation();
    this->Rotate(transformation_.axis_rot_, transformation_.angle_, SceneNode::TS_PARENT);

    this->set_visible(this->visible_);
}

template<typename VertexType>
typename GeometryPrimitiveSceneNodeT<VertexType>::Transformation GeometryPrimitiveSceneNodeT<VertexType>::ComputeMeshModelTransformation(const VertexType &start, const VertexType &end)
{
    Transformation res;

    res.translation_ = start;
    Vector unit_dir = (end - start);
    res.scale_[0] = unit_dir.norm();
    unit_dir.normalize();
    res.scale_[1] = 1.0;//(Scalar) this->model_size_;
    res.scale_[2] = 1.0;//(Scalar) this->model_size_;
    res.axis_rot_ = Vector(1.0,0.0,0.0).cross(unit_dir);

    Scalar rot_norm = std::min(res.axis_rot_.norm(), (Scalar)1.0);
    if (rot_norm != 0.0)
    {
        res.axis_rot_ = (1.0/rot_norm)*res.axis_rot_; // Normalize
        res.angle_ = asin(rot_norm);
        if(unit_dir[0] < 0.0)    // == dot product between translation_ and (1.0,0.0,0.0)
        {
            res.angle_ = M_PI - res.angle_;
        }
    }
    else
    {
        res.axis_rot_ = Vector(0.0,0.0,1.0);
        if(unit_dir[0] < 0.0)
        {
            res.angle_ = M_PI;
        }
        else
        {
            res.angle_ = 0.0;
        }
    }

//    Transform res2 = VectorTools::ComputeRotation(Vector(1.0, 0.0, 0.0), unit_dir);
//    cout << "Compute transformation : " << endl;
//    cout << "res2:" << endl << res2.matrix() << endl;
//    cout << "res orig:" << endl << res.axis_rot_ << endl << "ROT:" << res.angle_ << endl;

    return res;
}

template<typename VertexType>
core::SceneNode *GeometryPrimitiveSceneNodeT<VertexType>::IsIntersectingRay (const core::BasicRay &ray, Scalar &dist, bool proxy) const
{
    UNUSED(proxy);
    const VertexType &prev = edge_->GetStart()->pos();
    const VertexType &cur = edge_->GetEnd()->pos();

    core::BasicRay currentRay = this->parent_ != nullptr ? this->parent_->GetRayInLocal(ray) : ray;
//    currentRay = ray;

    return this->IsIntersectingRay(currentRay, transformation_, prev, cur, dist);
}

template<typename VertexType>
core::SceneNode *GeometryPrimitiveSceneNodeT<VertexType>::IsIntersectingRay (const core::BasicRay &ray, const Transformation &trans, const VertexType &/*prev*/, const VertexType &/*cur*/, Scalar &dist) const
{
    core::BasicRay trans_ray = this->GetRayInObjectFrame(ray, trans);
    trans_ray = this->GetRayInLocal(ray);
//    trans_ray = ray;

    // Constant
    Scalar a, b, c;
    const Scalar w1 = GetStartWeight() * this->scale_[1];// / 2.0;
    const Scalar w2 = GetEndWeight() * this->scale_[1];// / 2.0;// * trans.scale_[0]; // get the real extremities radiuses.

    if (this->adjust_size_to_params_) {
        // Variable radius Cylinder Case

        Scalar len = trans.scale_[0];
        Scalar coneLen = std::abs((len * w1) / (w2 - w1));
//        printf("len : %f / %f [%f:%f]\n", len, coneLen, w1, w2);
        a = trans_ray.direction()[1]*trans_ray.direction()[1] + trans_ray.direction()[2]*trans_ray.direction()[2] - (w1/coneLen)*(w1/coneLen)*trans_ray.direction()[0]*trans_ray.direction()[0];
        b = 2.0*(trans_ray.starting_point()[1]*trans_ray.direction()[1] + trans_ray.starting_point()[2]*trans_ray.direction()[2]);
        b += -2.0 * (w1 / coneLen) * (w1 / coneLen) * trans_ray.starting_point()[0] * trans_ray.direction()[0] + ((w1 * w1) / coneLen) * trans_ray.direction()[0];
        c = trans_ray.starting_point()[1]*trans_ray.starting_point()[1] + trans_ray.starting_point()[2]*trans_ray.starting_point()[2];
        c += -(w1 / coneLen) * (w1 / coneLen) * trans_ray.starting_point()[0] * trans_ray.starting_point()[0] + ((w1*w1) / coneLen) * trans_ray.starting_point()[0] - (w1 * w1);

//        a = (w2-w1)*trans_ray.direction()[0];
//        a = trans_ray.direction()[1]*trans_ray.direction()[1] + trans_ray.direction()[2]*trans_ray.direction()[2] - a*a;
//        b = 2.0*(trans_ray.starting_point()[1]*trans_ray.direction()[1] + trans_ray.starting_point()[2]*trans_ray.direction()[2] +(w1-w2)*trans_ray.direction()[0]*(w1+(w2-w1)*trans_ray.starting_point()[0]));
//        c = (w2-w1)*trans_ray.starting_point()[0]+w1;
//        c = trans_ray.starting_point()[1]*trans_ray.starting_point()[1] + trans_ray.starting_point()[2]*trans_ray.starting_point()[2] - c*c;

    } else {
        // Constant radius (=1) Cylinder Case
        a = trans_ray.direction()[1]*trans_ray.direction()[1] + trans_ray.direction()[2]*trans_ray.direction()[2];
        b = 2.0*(trans_ray.starting_point()[1]*trans_ray.direction()[1] + trans_ray.starting_point()[2]*trans_ray.direction()[2]);
        c = trans_ray.starting_point()[1]*trans_ray.starting_point()[1] + trans_ray.starting_point()[2]*trans_ray.starting_point()[2] - 1.0;
    }

    dist = expressive::kScalarMax;

    const Scalar b2_4ac = b*b - 4.0*a*c;

    if(b2_4ac < 0.0)
    {
        return nullptr;
    }
    else
    {
        Scalar sqrt_b2_4ac = sqrt(b2_4ac);
        Scalar t1 = (-b - sqrt_b2_4ac)/(2.0*a);
        Scalar t2 = (-b + sqrt_b2_4ac)/(2.0*a);
//        t1 *= trans.scale_[0];
//        t2 *= trans.scale_[0];

        Scalar x1 = trans_ray.starting_point()[0] + t1*trans_ray.direction()[0];
        Scalar x2 = trans_ray.starting_point()[0] + t2*trans_ray.direction()[0];

//        printf("%f:%f --> %f:%f\n", t1, t2, x1, x2);
//        pretty_print(this->scale_);
//        pretty_print(trans.scale_);

        Scalar maxLen = trans.scale_[0];// 1.0;

        if( (x1>maxLen && x2>maxLen) || (x1<0.0 && x2<0.0) )
        {
            return nullptr;
        }
        else
        {
            if(x1 > maxLen)
            {
                t1 = expressive::kScalarMax;//(1.0 - trans_ray.starting_point()[0])/trans_ray.direction()[0];
            }
            if(x1 < 0.0)
            {
                t1 = expressive::kScalarMax;//- trans_ray.starting_point()[0]/trans_ray.direction()[0];
            }
            if(x2 > maxLen)
            {
                t2 = expressive::kScalarMax;//(1.0 - trans_ray.starting_point()[0])/trans_ray.direction()[0];
            }
            if(x2 < 0.0)
            {
                t2 = expressive::kScalarMax;//- trans_ray.starting_point()[0]/trans_ray.direction()[0];
            }


            if(t1<0.0)
            {
                t1 = expressive::kScalarMax;
            }
            if(t2<0.0)
            {
                t2 = expressive::kScalarMax;
            }

            dist = std::min(t1, t2); // dist in local space

            Point ip = this->GetPointInWorld(trans_ray.starting_point() + trans_ray.direction() * dist); // intersection point at dist in world space
            dist = (ray.starting_point() - ip).norm(); // distance in world space !

            return (dist < expressive::kScalarMax) ? (core::SceneNode*) this : nullptr;
        }
    }
}

// Transform the given ray to get it in the primitive's frame
template<typename VertexType>
core::BasicRay GeometryPrimitiveSceneNodeT<VertexType>::GetRayInObjectFrame(const core::BasicRay& ray, const Transformation &t) const {
    Point trans_starting_point = ray.starting_point() - t.translation_;
    Vector trans_dir = ray.direction();
    if(t.angle_ != 0.0 && (t.axis_rot_[0] != 0.0 || t.axis_rot_[1] != 0.0 || t.axis_rot_[2] != 0.0))
    {
        trans_starting_point = expressive::VectorTools::Rotate(trans_starting_point, t.axis_rot_, -t.angle_);
        trans_dir = expressive::VectorTools::Rotate(ray.direction(), t.axis_rot_, -t.angle_);
    }
    trans_dir[0] *= 1.0/t.scale_[0];
    trans_dir[1] *= 1.0/t.scale_[1];
    trans_dir[2] *= 1.0/t.scale_[2];
    trans_starting_point[0] *= 1.0/t.scale_[0];
    trans_starting_point[1] *= 1.0/t.scale_[1];
    trans_starting_point[2] *= 1.0/t.scale_[2];
    trans_dir.normalize();

    return core::BasicRay (trans_starting_point, trans_dir);
}

template<typename VertexType>
void GeometryPrimitiveSceneNodeT<VertexType>::set_selected(bool selected, bool hover_parent)
{
    this->graph_node_->graph()->set_selected(edge_->id(), selected);
    GraphElementSceneNode::set_selected(selected, hover_parent);
}

template<typename VertexType>
void GeometryPrimitiveSceneNodeT<VertexType>::GetVertices(std::set<typename GraphType::VertexId> &vertices) const
{
    for (unsigned int i = 0; i < edge_->GetSize(); ++i) {
        vertices.insert(edge_->GetVertex(i)->id());
    }
}

template<typename VertexType>
std::shared_ptr<core::SceneNode> GeometryPrimitiveSceneNodeT<VertexType>::Subdivide(const typename VertexType::BasePoint &pos, Scalar /*weight*/)
{
    VertexType p(pos);
    std::shared_ptr<core::SubdivideEdgeCommand<VertexType> > command = std::make_shared<core::SubdivideEdgeCommand<VertexType> >(this->graph_node_->graph(), edge_->id(), edge_->index(), p);

//    this->require_rebuild_ = true;
    this->owner_->command_manager()->run(command);

    return this->graph_node_->GetNode(command->vertex_id_);
}

template<>
std::shared_ptr<core::SceneNode> GeometryPrimitiveSceneNodeT<core::WeightedPoint>::Subdivide(const core::WeightedPoint::BasePoint &pos, Scalar weight)
{
    if (weight == 0) {
        weight = (edge_->edge().GetStart().weight() + edge_->edge().GetEnd().weight()) / 2.0;
    }
    core::WeightedPoint p(pos, weight);
    std::shared_ptr<core::SubdivideEdgeCommand<core::WeightedPoint> >command = std::make_shared<core::SubdivideEdgeCommand<core::WeightedPoint> >(this->graph_node_->graph(), edge_->id(), edge_->index(), p);

//    this->require_rebuild_ = true;
    this->owner_->command_manager()->run(command);

    return this->graph_node_->GetNode(command->vertex_id_);
}

template<typename VertexType>
void GeometryPrimitiveSceneNodeT<VertexType>::set_displayed_scale(Scalar s)
{
    // Scale
//    this->highres_object_node_->scale(1.0, s / this->scale_[1], s / this->scale_[1]);

    for (unsigned int i = 0; i < Point::RowsAtCompileTime; ++i)
    {
        this->scale_[i] = s;
    }
}

} // namespace core

} // namespace expressive

#endif // DisplayedWeightedGeometryPrimitive_H
