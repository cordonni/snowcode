#include "core/scenegraph/graph/GraphHierarchyMapSceneNode.h"

#include "core/geometry/BasicTriMesh.h"
#include "core/scenegraph/SceneManager.h"

using namespace expressive::core;

namespace expressive
{

namespace core
{

GraphHierarchyMapSceneNode::GraphHierarchyMapSceneNode(SceneManager *scene, WeightedGraph* graph) :
    GraphHierarchyMap(graph),
    SceneNode(scene, nullptr, "GraphHierarchy" + graph->Name(), "GraphHierarchy", Vector::Ones(), false)
//    scene_(scene)
{
}

GraphHierarchyMapSceneNode::~GraphHierarchyMapSceneNode()
{
}

//void GraphHierarchyMapSceneNode::Notify(const core::Graph::GraphChanges &changes)
//{
//    GraphHierarchyMap::Notify(changes);
//}

void GraphHierarchyMapSceneNode::AddAnchor(VertexId vertex, EdgeId parent, const NodeAnchor &anchor)
{
    std::cout << "add" << vertex << std::endl;
    GraphHierarchyMap::AddAnchor(vertex, parent, anchor);

    VertexPtr v = graph_->Get(vertex);
    EdgePtr e = graph_->Get(parent);

    RemoveSceneNode(vertex, parent);

    std::shared_ptr<SceneNode> n = std::make_shared<SceneNode>(owner_, this, "hierarchyanchor" + std::to_string((uintptr_t)v.get()) + ":" + std::to_string((uintptr_t)e.get()));
    n->internal_node()->addModule("material", owner_->resource_manager()->loadResource("plasticBase").cast<ork::Module>());

    std::shared_ptr<BasicTriMesh> mesh = std::make_shared<BasicTriMesh>(ork::LINES);
    BasicTriMesh::VertexHandle v0 = mesh->add_point(v->pos());
    BasicTriMesh::VertexHandle v1 = mesh->add_point((e->GetStart()->pos() + e->GetEnd()->pos()) / 2.0);
    mesh->set_color(v0, Color(1.0, 0.0, 0.0, 1.0));
    mesh->set_color(v1, Color(0.0, 1.0, 0.0, 1.0));
    n->set_tri_mesh(mesh);

    AddChild(n);
    scene_nodes_[std::make_pair(vertex, parent)] = n;
}

void GraphHierarchyMapSceneNode::RemoveAnchor(VertexId vertex)
{

    std::cout << "remove" << vertex << std::endl;
    AnchorMap::iterator it = anchors_.find(vertex);
    if (it != anchors_.end()) {
        for (auto m : it->second) {
            RemoveSceneNode(vertex, m.first);
        }
    }
    GraphHierarchyMap::RemoveAnchor(vertex);
}

void GraphHierarchyMapSceneNode::RemoveSceneNode(VertexId vertex, EdgeId parent)
{
    auto p = std::make_pair(vertex, parent);
    std::shared_ptr<SceneNode> n = scene_nodes_[p];
    if (n != nullptr) {
        RemoveChild(n, false);
//        n->removeAndDestroyAllChildren();
//        n->getCreator()->destroySceneNode(n);
    }
    scene_nodes_.erase(p);
}

} // namespace core

} // namespace expressive
