/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GraphSceneNode_H
#define GraphSceneNode_H

// core dependencies
#include "core/graph/Graph.h"
#include "core/scenegraph/graph/GraphElementSceneNode.h"
#include "core/scenegraph/graph/DrawGraphTask.h"
#include "core/scenegraph/SceneNodeFactory.h"
#include "core/utils/ConfigLoader.h"
#include "core/utils/ColorTools.h"

#include "core/geometry/WeightedTriangle.h"

namespace expressive
{

namespace core
{

/**
TODO ::
x Nettoyer Graph
- Commenter manipulators
- Commenter SceneNode
- Commenter DisplayedGraph*
x Templater GraphCommands
x Templater DisplayedGraph*
x - Avec specialization pour Weighted.
x - Avec specialization pour 2D(vide)
- bouger subdivide de Displayed* -> les passer dans le manipulator ou dans graph (getCommand...)
x Renommer displayedWeighted* en Dispalyed*
- Utiliser les primitives d'ulysse dans les Displayed au lieu de CreateSphere...
- Retirer Transformation de DisplayedWeightedGeometry et ajouter des Matrix::Transformation
- Templater SymmetryManipulator ?
- Templater GrapManipulator ?
*/

/**
 * @brief The GraphSceneNode class
 * Allows to display a Graph in the Ork renderer. Inherits from Graph.
 * Basically, it creates children SceneNodes for each Vertex & Edge, based on their type.
 * this uses a factory : SceneNodeFactory.
 */
template<typename VertexType>
class GraphSceneNodeT :
        public SceneNode,
        public core::GraphT<VertexType>::GraphListener
{
public:
    
    EXPRESSIVE_MACRO_NAME("GraphSceneNode")

    typedef GraphElementSceneNodeT<VertexType> GraphElementSceneNode;

    typedef core::GraphT<VertexType> GraphType;

    /**
     * @brief GraphSceneNode ctor
     * @param scene_manager The SceneManager that handles this SceneNode
     * @param parent An optionnal parent. SceneManager's root will be used if this is nullptr.
     * @param name The name of this SceneNode (must be unique).
     */
//    GraphSceneNode(core::SceneManager* scene_manager, SceneNode *parent, const string &name, std::shared_ptr<expressive::core::WeightedGraph> graph, Scalar point_scale, Scalar geometry_scale);
    GraphSceneNodeT(core::SceneManager* scene_manager, SceneNode *parent, const std::string &name, std::shared_ptr<GraphType> graph, Scalar point_scale, Scalar geometry_scale);

    virtual ~GraphSceneNodeT();

    virtual void Notify(const typename GraphType::GraphChanges &changes);

    /**
     * @brief AddEdge Adds an edge.
     * Updates Ork's 3d scene
     */
    virtual void AddEdge(typename GraphType::EdgeId);
    /**
     * @brief UpdateEdge Updates an edge
     * Updates Ork's 3d scene
     */
    virtual void UpdateEdge(typename GraphType::EdgeId);
    /**
     * @brief RemoveEdge Removes an edge
     * Updates Ork's 3d scene
     */
    virtual void RemoveEdge(typename GraphType::EdgeId);
    /**
     * @brief UpdateVertex Updates a vertex
     * Updates Ork's 3d scene
     */
    virtual void UpdateVertex(typename GraphType::VertexId);
    /**
     * @brief AddVertex Adds a vertex
     * Updates Ork's 3d scene
     */
    virtual void AddVertex(typename GraphType::VertexId);
    /**
     * @brief RemoveVertex Removes a vertex
     * Updates Ork's 3d scene
     */
    virtual void RemoveVertex(typename GraphType::VertexId);

    virtual void updated(GraphType * /*g*/) {}

    std::shared_ptr<GraphElementSceneNode> GetNode(const typename GraphType::VertexId &id);

    std::shared_ptr<GraphElementSceneNode> GetNode(const typename GraphType::EdgeId &id);

    /**
     * @brief BuildVertexNode Creates an SceneNode from a given Vertex and adds it as a child.
     * @param v the Vertex for which we want to create an SceneNode.
     * @return the new SceneNode
     */
    std::shared_ptr<SceneNode> BuildVertexNode(std::shared_ptr<typename GraphType::Vertex> v);

    /**
     * @brief BuildEdgeNode Creates an SceneNode from a given Edge and adds it as a child.
     * @param e the Edge for which we want to create an SceneNode.
     * @return the new SceneNode
     */
    std::shared_ptr<SceneNode> BuildEdgeNode(std::shared_ptr<typename GraphType::Edge> e);

    std::shared_ptr<SceneNode> BuildSurfaceNode(std::shared_ptr<typename GraphType::Edge> e);

    virtual core::SceneNode *IsIntersectingRay (const core::BasicRay &ray, Scalar &dist, bool proxy = true) const;


    /////////////////
    /// getters   ///
    /////////////////
    inline Scalar point_scale() const { return point_scale_; }
    inline Scalar geometry_scale() const { return geometry_scale_; }
    inline Color point_color() const { return point_color_; }
    inline Color geometry_color() const { return geometry_color_; }

    /////////////////
    /// modifiers ///
    /////////////////
    virtual void set_selected(bool selected, bool hover_parent = true)
    {
        SceneNode::set_selected(selected, hover_parent);
    }

    void set_point_scale(Scalar s);
    void set_geometry_scale(Scalar s);

    void set_point_color(Color c);
    void set_geometry_color(Color c);

protected:
    GraphSceneNodeT();

    virtual void Init(std::shared_ptr<GraphType> graph, Scalar point_scale, Scalar geometry_scale);

    /**
     * @brief RemovedIndex Updates #vertex_nodes_ & #edge_nodes_ indices when a vertex or an edge is removed
     * It mostly removes 1 from every id equal or above the given one.
     * @param id Id of the child that was removed.
     */
    void RemovedIndex(unsigned int id);

    void CreateChildrenNodes();

    inline std::shared_ptr<SceneNode> CreateVertexNode(std::shared_ptr<typename GraphType::Vertex> vertex);
    inline std::shared_ptr<SceneNode> CreateEdgeNode(std::shared_ptr<typename GraphType::Edge> edge);
    inline std::shared_ptr<SceneNode> CreateSurfaceNode(std::shared_ptr<typename GraphType::Edge> surface);

//    std::shared_ptr<core::Graph> graph_;

    ////
    /// Graph design related stuff.
    Scalar point_scale_;
    Scalar geometry_scale_;

    Color point_color_;
    Color geometry_color_;

    std::shared_ptr<GraphElementList<VertexType> > graph_elements_;
//    /**
//     * @brief vertex_nodes_ Stores the indices of each child corresponding to each Vertex from the graph.
//     */
//    std::map<typename core::GraphT<VertexType>::VertexId, std::shared_ptr<GraphElementSceneNode> > vertex_nodes_;

//    /**
//     * @brief edge_nodes_ Stores the indices of each child corresponding to each Edge from the graph.
//     */
//    std::map<typename core::GraphT<VertexType>::EdgeId, std::shared_ptr<GraphElementSceneNode> > edge_nodes_;

//    /**
//     * @brief surface_nodes_ Stores the indices of each child corresponding to each Surface from the graph.
//     */
//    std::map<typename GraphType::EdgeType*, std::shared_ptr<GraphElementSceneNode> > surface_nodes_;

    std::map<core::EdgeId, core::GeometryPrimitiveT<VertexType>*> primitive_ids_;
};

template<typename VertexType>
GraphSceneNodeT<VertexType>::GraphSceneNodeT() :
    SceneNode(StaticName()),
    GraphType::GraphListener(),
    point_scale_(1.0),
    geometry_scale_(1.0),
    point_color_(HexToColor(Config::getIntParameter("graph_default_vertex_color"))),
    geometry_color_(HexToColor(Config::getIntParameter("graph_default_edge_color")))
{
}

template<typename VertexType>
GraphSceneNodeT<VertexType>::GraphSceneNodeT(core::SceneManager* scene_manager, SceneNode *parent, const std::string &name, std::shared_ptr<GraphType> graph, Scalar point_scale, Scalar geometry_scale) :
    SceneNode(scene_manager, parent, name, StaticName(), Vector::Ones(), false),
    GraphType::GraphListener(),
    point_color_(HexToColor(Config::getIntParameter("graph_default_vertex_color"))),
    geometry_color_(HexToColor(Config::getIntParameter("graph_default_edge_color")))
{
    Init(graph, point_scale, geometry_scale);
}

template<typename VertexType>
GraphSceneNodeT<VertexType>::~GraphSceneNodeT()
{
    graph_elements_->clear();
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::Init(std::shared_ptr<GraphType> graph, Scalar point_scale, Scalar geometry_scale)
{
    GraphType::GraphListener::Init(graph.get());
    point_scale_ = point_scale;
    geometry_scale_ = geometry_scale;

    graph_elements_ = std::make_shared<GraphElementList<VertexType> >();

    node_->addFlag("object");
    node_->addModule("material", owner_->resource_manager()->loadResource(Config::getStringParameter("graph_default_module")).cast<ork::Module>());
#ifdef __APPLE__
    node_->addMethod("draw", new ork::Method(new DrawGraphTTask<VertexType>(this->graph_elements_, owner_->resource_manager()->loadResource("camera;mouse;spotlight;plastic_MAC;").cast<ork::Program>())));
#else
    node_->addMethod("draw", new ork::Method(new DrawGraphTTask<VertexType>(this->graph_elements_, owner_->resource_manager()->loadResource("camera;mouse;spotlight;plastic;").cast<ork::Program>())));
#endif
    CreateChildrenNodes();
}

template<typename VertexType>
std::shared_ptr<GraphElementSceneNodeT<VertexType>> GraphSceneNodeT<VertexType>::GetNode(const typename GraphType::VertexId &id)
{
    auto it = graph_elements_->vertex_nodes_.find(id);
    if (it != graph_elements_->vertex_nodes_.end()) {
        return it->second;
    }
    return nullptr;
}

template<typename VertexType>
std::shared_ptr<GraphElementSceneNodeT<VertexType>> GraphSceneNodeT<VertexType>::GetNode(const typename GraphType::EdgeId &id)
{
    auto it = graph_elements_->edge_nodes_.find(id);
    if (it != graph_elements_->edge_nodes_.end()) {
        return it->second;
    }
    return nullptr;
}

template<typename VertexType>
std::shared_ptr<SceneNode> GraphSceneNodeT<VertexType>::CreateVertexNode(std::shared_ptr<typename GraphType::Vertex> vertex)
{
    return SceneNodeFactory::Create(vertex->pos().Name(), owner_, this, "HighresVertexNode" + std::to_string(owner_->const_next_node_id()), vertex, Vector(point_scale_, point_scale_, point_scale_), VertexType::StaticName());
}

template<typename VertexType>
std::shared_ptr<SceneNode> GraphSceneNodeT<VertexType>::CreateEdgeNode(std::shared_ptr<typename GraphType::Edge> edge)
{
    return SceneNodeFactory::Create(edge->edgeptr()->Name(), owner_, this, "HighresEdgeNode" + std::to_string(owner_->const_next_node_id()), edge, Vector(geometry_scale_, geometry_scale_, geometry_scale_), GraphType::EdgeType::StaticName());
}

template<typename VertexType>
std::shared_ptr<SceneNode> GraphSceneNodeT<VertexType>::CreateSurfaceNode(std::shared_ptr<typename GraphType::Edge> edge)
{
    return SceneNodeFactory::Create(edge->edgeptr()->Name(), owner_, this, "HighresEdgeNode" + std::to_string(owner_->const_next_node_id()), edge, Vector::Ones()/*Vector(geometry_scale_, geometry_scale_, geometry_scale_)*/, core::TriangleT<VertexType>::StaticName());
}

template<>
std::shared_ptr<SceneNode> CORE_API GraphSceneNodeT<core::WeightedPoint>::CreateVertexNode(std::shared_ptr<typename GraphType::Vertex> vertex);

template<>
std::shared_ptr<SceneNode> CORE_API GraphSceneNodeT<core::WeightedPoint>::CreateEdgeNode(std::shared_ptr<typename GraphType::Edge> edge);

template<>
std::shared_ptr<SceneNode> CORE_API GraphSceneNodeT<core::WeightedPoint>::CreateSurfaceNode(std::shared_ptr<typename GraphType::Edge> edge);

template<typename VertexType>
std::shared_ptr<SceneNode> GraphSceneNodeT<VertexType>::BuildVertexNode(std::shared_ptr<typename GraphType::Vertex> v)
{
    auto it = graph_elements_->vertex_nodes_.find(v->id());
    if (it != graph_elements_->vertex_nodes_.end()) {
        RemoveChild(it->second);
        graph_elements_->vertex_nodes_.erase(v->id());
    }

//    const VertexType &p = v->pos();

    std::shared_ptr<SceneNode> o = CreateVertexNode(v);//SceneNodeFactory::Create(p.Name(), owner_, this, "HighresVertexNode" + to_string(owner_->const_next_node_id()), v, Vector(point_scale_, point_scale_, point_scale_), VertexType::StaticName());
//    o->tri_mesh()->FillColor(point_color_);
    AddChild(o, false);
    graph_elements_->vertex_nodes_[v->id()] = std::dynamic_pointer_cast<GraphElementSceneNode>(o);

    return o;
}

template<typename VertexType>
std::shared_ptr<SceneNode> GraphSceneNodeT<VertexType>::BuildEdgeNode(std::shared_ptr<typename GraphType::Edge>e)
{
    auto it = graph_elements_->edge_nodes_.find(e->id());
    if (it != graph_elements_->edge_nodes_.end()) {
        RemoveChild(it->second);
        graph_elements_->edge_nodes_.erase(e->id());
    }

//    const typename GraphType::EdgeType &p = e->edge();
    std::shared_ptr<SceneNode> o = CreateEdgeNode(e);//SceneNodeFactory::Create(e->edgeptr()->Name(), owner_, this, "HighresEdgeNode" + to_string(owner_->const_next_node_id()), e, Vector(geometry_scale_, geometry_scale_, geometry_scale_), GraphType::EdgeType::StaticName());
    auto mesh = o->tri_mesh();
//    if (mesh != nullptr) {
//        mesh->FillColor(geometry_color_);
//    }
    AddChild(o, false);
    graph_elements_->edge_nodes_[e->id()] = std::dynamic_pointer_cast<GraphElementSceneNode>(o);

    return o;
}

template<typename VertexType>
std::shared_ptr<SceneNode> GraphSceneNodeT<VertexType>::BuildSurfaceNode(std::shared_ptr<typename GraphType::Edge> e)
{
    typename GraphType::EdgeType *s = &(e->nonconst_edge());
    auto it = graph_elements_->surface_nodes_.find(s);
    if (it != graph_elements_->surface_nodes_.end()) {
        RemoveChild(it->second);
        graph_elements_->surface_nodes_.erase(s);
    }

    std::shared_ptr<SceneNode> o = CreateSurfaceNode(e);//SceneNodeFactory::Create(s->Name(), owner_, this, "HighresEdgeNode" + to_string(owner_->const_next_node_id()), e, Vector(geometry_scale_, geometry_scale_, geometry_scale_), core::TriangleT<VertexType>::StaticName());
    auto mesh = o->tri_mesh();
    if (mesh != nullptr) {
        mesh->FillColor(geometry_color_);
    }
    AddChild(o, false);
    graph_elements_->surface_nodes_[s] = std::dynamic_pointer_cast<GraphElementSceneNode>(o);
    primitive_ids_[e->id()] = s;

    return o;
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::CreateChildrenNodes()
{
    typename GraphType::VertexIteratorPtr vertices = this->target_->GetVertices();
    while (vertices->HasNext()) {
        std::shared_ptr<typename GraphType::Vertex> v = vertices->Next();
        BuildVertexNode(v);
    }

    std::map<typename GraphType::EdgeType *, std::shared_ptr<typename GraphType::Edge>> surfaces;

    typename GraphType::EdgeIteratorPtr edges = this->target_->GetEdges();
    while (edges->HasNext()) {
        std::shared_ptr<typename GraphType::Edge> e = edges->Next();
        BuildEdgeNode(e);
        if (e->edge().isFilled()) {
            surfaces[e->edgeptr().get()] = e;
        }
    }

    for (auto &s : surfaces) {
        BuildSurfaceNode(s.second);
    }

    node_->setLocalBounds(this->target_->axis_bounding_box().toBox3());
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::AddEdge(typename GraphType::EdgeId id)
{
    if (graph_elements_->edge_nodes_.find(id) == graph_elements_->edge_nodes_.end()) {
        std::shared_ptr<typename GraphType::Edge> e = this->target_->Get(id);
        BuildEdgeNode(e);
        typename GraphType::EdgeType *s = e->edgeptr().get();
        if (e->edge().isFilled()) {
            if (graph_elements_->surface_nodes_.find(s) == graph_elements_->surface_nodes_.end()) {
                BuildSurfaceNode(e);
            }
        }
    }
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::UpdateEdge(typename GraphType::EdgeId id)
{
    auto it = graph_elements_->edge_nodes_.find(id);
    if (it != graph_elements_->edge_nodes_.end()) {
        it->second->GraphUpdated();
        typename GraphType::EdgeType *s = this->graph()->Get(id)->edgeptr().get();
        auto it2 = graph_elements_->surface_nodes_.find(s);
        if (it2 != graph_elements_->surface_nodes_.end()) {
            it2->second->GraphUpdated();
        }
    }
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::RemoveEdge(typename GraphType::EdgeId id)
{
    auto it = graph_elements_->edge_nodes_.find(id);
    if (it == graph_elements_->edge_nodes_.end()) {
        ork::Logger::ERROR_LOGGER->logf("SCENEGRAPH", "Missing edge in GraphSceneNode : %d", id);
        assert(false);
        return;
    }

    RemoveChild(it->second, false); // todo check if this is truly necessary when deleting surfaces.
    graph_elements_->edge_nodes_.erase(id);
    auto it_prim = primitive_ids_.find(id);
    if (it_prim != primitive_ids_.end()) {
        typename GraphType::EdgeType *s = it_prim->second;
        auto it2 = graph_elements_->surface_nodes_.find(s);
        if (it2 != graph_elements_->surface_nodes_.end()) {
            RemoveChild(it2->second, false);
            graph_elements_->surface_nodes_.erase(s);
            primitive_ids_.erase(id);
        }
    }
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::AddVertex(typename GraphType::VertexId id)
{
    if (graph_elements_->vertex_nodes_.find(id) == graph_elements_->vertex_nodes_.end()) {
        std::shared_ptr<typename GraphType::Vertex> v = this->target_->Get(id);
        BuildVertexNode(v);
    }
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::UpdateVertex(typename GraphType::VertexId id)
{
    auto it = graph_elements_->vertex_nodes_.find(id);
    if (it != graph_elements_->vertex_nodes_.end()) {
        it->second->GraphUpdated();
    }
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::RemoveVertex(typename GraphType::VertexId id)
{
    auto it = graph_elements_->vertex_nodes_.find(id);
    if (it == graph_elements_->vertex_nodes_.end()) {
        ork::Logger::ERROR_LOGGER->logf("SCENEGRAPH", "Missing vertex in GraphSceneNode : %d", id);
        assert(false);
        return;
    }
//    RemoveChild(it->second, false);
    graph_elements_->vertex_nodes_.erase(id);
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::Notify(const typename GraphType::GraphChanges &changes)
{
    for (auto e : changes.removed_edges_) {
         RemoveEdge(e);
    }
    for (auto v : changes.removed_vertices_) {
        RemoveVertex(v);
    }

    for (auto e : changes.added_vertices_) {
        AddVertex(e);
    }

    for (auto v : changes.added_edges_) {
        AddEdge(v);
    }

    for (auto e : changes.updated_edges_) {
        UpdateEdge(e);
    }
    for (auto v : changes.updated_vertices_) {
        UpdateVertex(v);
    }
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::set_point_scale(Scalar s)
{
    point_scale_ = s;
    for (auto it : graph_elements_->vertex_nodes_)
    {
        it.second->set_displayed_scale(point_scale_);
        it.second->GraphUpdated();
    }
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::set_geometry_scale(Scalar s)
{
    geometry_scale_ = s;
    for (auto it : graph_elements_->edge_nodes_)
    {
        it.second->set_displayed_scale(geometry_scale_);
        it.second->GraphUpdated();
    }
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::set_point_color(Color c)
{
    point_color_ = c;
    for (auto it : graph_elements_->vertex_nodes_)
    {
        it.second->tri_mesh()->FillColor(c);
    }
}

template<typename VertexType>
void GraphSceneNodeT<VertexType>::set_geometry_color(Color c)
{
    geometry_color_ = c;
    for (auto it : graph_elements_->edge_nodes_)
    {
        it.second->tri_mesh()->FillColor(c);
    }
}

template<typename VertexType>
core::SceneNode *GraphSceneNodeT<VertexType>::IsIntersectingRay (const core::BasicRay &ray, Scalar &dist, bool proxy) const
{
    dist = expressive::kScalarMax;
    Scalar tmpDist = dist;
    core::SceneNode* res = nullptr;
    for (auto it : graph_elements_->vertex_nodes_) {
        core::SceneNode* tmpRes = it.second->IsIntersectingRay(ray, tmpDist, proxy);
        if (tmpRes != nullptr && tmpDist < dist) {
            res = tmpRes;
            dist = tmpDist;
        }
    }
    for (auto it : graph_elements_->edge_nodes_) {
        core::SceneNode* tmpRes = it.second->IsIntersectingRay(ray, tmpDist, proxy);
        if (tmpRes != nullptr && tmpDist < dist) {
            res = tmpRes;
            dist = tmpDist;
        }
    }
    for (auto it : graph_elements_->surface_nodes_) {
        core::SceneNode* tmpRes = it.second->IsIntersectingRay(ray, tmpDist, proxy);
        if (tmpRes != nullptr && tmpDist < dist) {
            res = tmpRes;
            dist = tmpDist;
        }
    }

    return res;
}

typedef GraphSceneNodeT<core::WeightedPoint> WeightedGraphSceneNode;
typedef GraphSceneNodeT<core::PointPrimitive> GraphSceneNode;

} // namespace core

} // namespace expressive


#endif // GraphSceneNode_H

