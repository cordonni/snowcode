#include "core/scenegraph/graph/SurfaceSceneNode.h"

// core dependencies
#include "core/scenegraph/SceneNodeFactory.h"

using namespace std;

namespace expressive
{

namespace core
{

static SceneNodeFactory::Type<WeightedTriangle, SurfaceSceneNodeT<WeightedPoint>> WeightedSurfaceSceneNodeType;
static SceneNodeFactory::Type<Triangle, SurfaceSceneNodeT<PointPrimitive>> SurfaceSceneNodeType;

} // namespace core

} // namespace expressive


