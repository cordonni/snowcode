/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DISPLAYEDWEIGHTEDPOINT_H
#define DISPLAYEDWEIGHTEDPOINT_H

// core dependencies
#include <core/graph/Graph.h>
#include <core/graph/GraphCommands.h>
#include <core/geometry/WeightedPoint.h>
#include <core/geometry/BasicRay.h>
#include "core/scenegraph/SceneNode.h"
#include "core/scenegraph/SceneManager.h"
#include "core/scenegraph/graph/GraphElementSceneNode.h"
#include "core/geometry/MeshTools.h"

namespace expressive
{

namespace core
{

/**
 * @brief The PointSceneNode class
 * Draw a Graph's WeightedPoint.
 */
template<typename VertexType>
class PointSceneNodeT : public GraphElementSceneNodeT<VertexType>
{
public:
    EXPRESSIVE_MACRO_NAME("PointSceneNode")

    typedef GraphElementSceneNodeT<VertexType> GraphElementSceneNode;
    typedef core::GraphT<VertexType> GraphType;

    PointSceneNodeT(core::SceneManager *owner, core::SceneNode *parent, const std::string &name, const std::shared_ptr<void> param, const Vector &scale);

    virtual ~PointSceneNodeT();

    void CreateMesh();

    virtual core::SceneNode *IsIntersectingRay (const core::BasicRay &ray, Scalar &dist, bool proxy = true) const;

    inline std::shared_ptr<typename GraphType::Vertex> vertex() const { return vertex_; }

    virtual std::shared_ptr<core::SceneNode> Subdivide(const typename VertexType::BasePoint &pos, Scalar weight);

    virtual std::shared_ptr<core::SceneNodeManipulator> manipulator() const { return this->graph_node_->manipulator(); }

    virtual void GetVertices(std::set<core::VertexId> &vertices) const;

    virtual void set_selected(bool selected, bool hover_parent = true);

    virtual void GraphUpdated();

protected:

    std::shared_ptr<typename GraphType::Vertex> vertex_;

    static std::shared_ptr<AbstractTriMesh> default_sphere();

};

template<typename VertexType>
std::shared_ptr<AbstractTriMesh> PointSceneNodeT<VertexType>::default_sphere()
{
    static ork::static_ptr<AbstractTriMesh> sphere = MeshTools::CreateSphere(10, 10);
    return sphere;
}

template<typename VertexType>
PointSceneNodeT<VertexType>::PointSceneNodeT(core::SceneManager *owner, core::SceneNode *parent, const std::string &name, const std::shared_ptr<void> param, const Vector &scale)
    : GraphElementSceneNodeT<VertexType>(owner, parent, name, StaticName(), scale), vertex_(std::static_pointer_cast<typename GraphType::Vertex>(param))//, entity_(nullptr)
{
    CreateMesh();
}

template<typename VertexType>
PointSceneNodeT<VertexType>::~PointSceneNodeT()
{
    vertex_ = nullptr;
}

template<typename VertexType>
void PointSceneNodeT<VertexType>::CreateMesh()
{
    SceneNode::set_tri_mesh(PointSceneNodeT::default_sphere());
    this->tri_mesh()->FillColor(this->graph_node_->point_color());

    this->GraphUpdated();
}

template<typename VertexType>
core::SceneNode *PointSceneNodeT<VertexType>::IsIntersectingRay (const core::BasicRay &ray, Scalar &dist, bool proxy) const
{
    UNUSED(proxy);
    core::BasicRay currentRay = this->GetRayInLocal(ray);
    if (currentRay.IntersectsSphere(Point::Zero(), /*scale_[0] * weight*/ 1.0, dist)) {
        dist = fabs(dist * this->graph_node_->point_scale());
        return (core::SceneNode*) this;
    }

    return nullptr;
}

template<>
CORE_API core::SceneNode * PointSceneNodeT<core::WeightedPoint>::IsIntersectingRay (const core::BasicRay &ray, Scalar &dist, bool proxy) const;

template<typename VertexType>
void PointSceneNodeT<VertexType>::set_selected(bool selected, bool hover_parent)
{
    this->graph_node_->graph()->set_selected(vertex_->id(), selected);
    GraphElementSceneNode::set_selected(selected, hover_parent);
}

template<typename VertexType>
void PointSceneNodeT<VertexType>::GetVertices(std::set<core::VertexId> &vertices) const
{
    vertices.insert(vertex_->id());
}

template<typename VertexType>
std::shared_ptr<core::SceneNode> PointSceneNodeT<VertexType>::Subdivide(const typename VertexType::BasePoint &/*pos*/, Scalar weight)
{
//    this->require_rebuild_ = true;
    std::shared_ptr<core::AddEdgeCommand<VertexType> >command = std::make_shared<core::AddEdgeCommand<VertexType> >(this->graph_node_->graph(), vertex_->id(), vertex_->id(), weight);
    this->owner_->command_manager()->run(command);

    return this->graph_node_->GetNode(command->end_);
}

template<typename VertexType>
void PointSceneNodeT<VertexType>::GraphUpdated()
{
    const VertexType &p = vertex_->pos();
    SceneNode::SetPos(p[0], p[1], p[2], SceneNode::TS_PARENT);

    this->set_visible(this->visible_);
}

typedef PointSceneNodeT<core::WeightedPoint> WeightedPointSceneNode;
typedef PointSceneNodeT<core::PointPrimitive> PointSceneNode;
typedef PointSceneNodeT<core::WeightedPoint2D> WeightedPointSceneNode2D;
typedef PointSceneNodeT<core::PointPrimitive2D> PointSceneNode2D;

} // namespace core

} // namespace expressive

#endif // DISPLAYEDWEIGHTEDPOINT_H
