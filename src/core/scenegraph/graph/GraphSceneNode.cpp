#include "core/scenegraph/graph/GraphSceneNode.h"

// ork dependencies
#include "ork/core/Logger.h"

// core dependencies
#include "core/geometry/WeightedSegment.h"
#include "core/geometry/WeightedTriangle.h"
#include "core/scenegraph/SceneManager.h"

#include <sstream>

using namespace std;
using namespace expressive::core;

namespace expressive
{

namespace core
{

template<>
std::shared_ptr<SceneNode> GraphSceneNodeT<core::WeightedPoint>::CreateVertexNode(std::shared_ptr<typename GraphType::Vertex> vertex)
{
    return SceneNodeFactory::Create(vertex->pos().Name(), owner_, this, "HighresVertexNode" + to_string(owner_->const_next_node_id()), vertex, Vector(point_scale_, point_scale_, point_scale_), core::WeightedPoint::StaticName());
}

template<>
std::shared_ptr<SceneNode> GraphSceneNodeT<core::WeightedPoint>::CreateEdgeNode(std::shared_ptr<typename GraphType::Edge> edge)
{
    return SceneNodeFactory::Create(edge->edgeptr()->Name(), owner_, this, "HighresEdgeNode" + to_string(owner_->const_next_node_id()), edge, Vector(1.0, geometry_scale_, geometry_scale_), core::WeightedSegment::StaticName());
}

template<>
std::shared_ptr<SceneNode> GraphSceneNodeT<core::WeightedPoint>::CreateSurfaceNode(std::shared_ptr<typename GraphType::Edge> edge)
{
    return SceneNodeFactory::Create(edge->edgeptr()->Name(), owner_, this, "HighresEdgeNode" + to_string(owner_->const_next_node_id()), edge, Vector::Ones()/*Vector(geometry_scale_, geometry_scale_, geometry_scale_)*/, core::WeightedTriangle::StaticName());
}

//static SceneNodeFactory::Type<Graph, GraphSceneNode> GraphSceneNodeType;

} // namespace core

} // namespace expressive

