#include "core/scenegraph/graph/GraphSceneNode2D.h"


// ork dependencies
#include "ork/core/Logger.h"

// core dependencies
#include "core/scenegraph/SceneManager.h"
#include "core/scenegraph/SceneNodeFactory.h"

using namespace std;
using namespace ork;
using namespace expressive::core;

namespace expressive
{

namespace core
{

template<>
void GraphSceneNode2DT<core::WeightedGraph2D>::DrawGraph(std::shared_ptr<core::WeightedGraph2D> graph, const Color &color, Scalar thickness, std::fstream &str)
{
    AABBox2D box = graph->axis_bounding_box();
    Scalar length = std::max(box.width(), box.height()) * 1.2;
//    Point2D pOrig = box.min();
    Point2 pMid = box.ComputeCenter();

    core::WeightedGraph2D::EdgeIteratorPtr edges = graph->GetEdges();
    while (edges->HasNext()) {
        std::shared_ptr<core::WeightedEdge2D> edge = edges->Next();

        std::shared_ptr<core::WeightedVertex2D> start = edge->GetStart();
        std::shared_ptr<core::WeightedVertex2D> end = edge->GetEnd();

        Point2 spos = ((start->pos() - pMid) * 2.0 / length);
        Point2 epos = ((end->pos() - pMid) * 2.0 / length);
        DrawSegment(spos, epos, color, start->pos().weight() * thickness, end->pos().weight() * thickness, str);

//        DrawCircle(spos, color, start->pos().weight() * thickness, str);
//        DrawCircle(epos, color, end->pos().weight() * thickness, str);
    }
}

template<>
void GraphSceneNode2DT<core::WeightedGraph2D>::DrawGraphDistances(std::shared_ptr<core::WeightedGraph2D> graph, std::shared_ptr<WeightMap> weights, Scalar maxWeight, Scalar thickness, std::fstream &str)
{
    const double colorStep = 0.01;

    AABBox2D box = graph->axis_bounding_box();
    Scalar length = std::max(box.width(), box.height()) * 1.2;
    Point2 pMid = box.ComputeCenter();

    core::WeightedGraph2D::EdgeIteratorPtr edges = graph->GetEdges();
    while (edges->HasNext()) {
        core::WeightedGraph2D::EdgePtr edge = edges->Next();


        for (unsigned int i = 0; i< edge->GetSize(); ++i) {
            core::WeightedGraph2D::VertexPtr vertex = edge->GetVertex(i);
            Scalar width = vertex->pos().weight() * thickness;
            double dist = weights->at(vertex->id()) / maxWeight;
            DrawCircle((vertex->pos() - pMid) * 2.0 / length, getDistanceColor(dist), width, str);
        }

        std::shared_ptr<core::WeightedVertex2D> prev = edge->GetVertex(0);
        std::shared_ptr<core::WeightedVertex2D> next = edge->GetVertex(1);

        Scalar dprev = weights->at(prev->id()) / maxWeight;
        Scalar dnext = weights->at(next->id()) / maxWeight;
        Scalar dedge = weights->at(edge->id()) / maxWeight;


        if (dprev < dnext) {
        } else {
            auto tmp = next;
            next = prev;
            prev = tmp;
            dprev = dnext;
        }
        Scalar wprev = prev->pos().weight() * thickness;
        Scalar wnext = next->pos().weight() * thickness;

        Point2 dir = (next->pos() - prev->pos());
        double len = dir.norm();
        double nbSegments = std::round(len / colorStep);
        dir = dir / nbSegments;

        Scalar distOffset = (dedge - dprev) / nbSegments;
        Scalar widthOffset = (wnext - wprev) / nbSegments;

        for (int j = 1; j < round(nbSegments) + 1; ++j) {
            Vector2 p = prev->pos() + dir * (j - 1);
            Vector2 c = prev->pos() + dir * j;
            double dp = dprev + distOffset * (j - 1);
            double dc = dprev + distOffset * j;
            double wp = wprev + widthOffset * (j - 1);
            double wc = wprev + widthOffset * j;


            p = (p - pMid) * 2.0 / length; // rescale so the whole graph fits in the screen.
            c = (c - pMid) * 2.0 / length;

            DrawSegment(p, c, getDistanceColor(dp), getDistanceColor(dc), wp, wc, str);
        }
    }
}

template<>
void GraphSceneNode2DT<core::WeightedGraph2D>::ExportGraphToSVG(std::fstream &str, std::shared_ptr<core::WeightedGraph2D> graph, const Color &color, Scalar sizeRatio)
{
//    QColor col(color[0] * 255, color[1] * 255, color[2] * 255);
    std::string colName = ColorToString(color);
    core::WeightedGraph2D::EdgeIteratorPtr edges = graph->GetEdges();
    while (edges->HasNext()) {
        core::WeightedGraph2D::EdgePtr edge = edges->Next();
        core::WeightedGraph2D::VertexPtr s = edge->GetStart();
        core::WeightedGraph2D::VertexPtr e = edge->GetEnd();

        AddCircleToSVG(str, s->pos(), colName, s->pos().weight() * sizeRatio);
        AddCircleToSVG(str, e->pos(), colName, e->pos().weight() * sizeRatio);
        AddSegmentToSVG(str, s->pos(), s->pos().weight() * sizeRatio, e->pos(), e->pos().weight() * sizeRatio, colName);
    }
}

//static SceneNodeFactory::Type<Graph, GraphSceneNode> GraphSceneNodeType;

} // namespace core

} // namespace expressive

