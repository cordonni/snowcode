/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef OBJECTPRIMITIVE_H
#define OBJECTPRIMITIVE_H

// core dependencies
#include <core/geometry/Frame.h>
//#include <core/scenegraph/SceneNodeManipulator.h>
#include <core/scenegraph/SceneNode.h>

namespace expressive
{

namespace core
{

class SceneNodeManipulator;

/**
 * @brief The ObjectPrimitive class inherits from SceneNode
 * and overloads the transformations for representing them
 * with Eigen::Transform
 */
class CORE_API ObjectPrimitive : public SceneNode
{
public:
    
    EXPRESSIVE_MACRO_NAME("ObjectPrimitive")


    /**
     * @brief ObjectPrimitive ctor
     * @param scene_manager The SceneManager that handles this SceneNode
     * @param parent An optionnal parent. SceneManager's root will be used if this is nullptr.
     * @param name The name of this SceneNode (must be unique).
     */
    ObjectPrimitive(core::SceneManager* scene_manager = nullptr,
                    SceneNode *parent = nullptr,
                    const std::string &name = std::string("ObjectPrimitive_") + std::to_string(instance_counter_++),
                    std::shared_ptr<core::SceneNodeManipulator> manipulator = nullptr,
                    std::shared_ptr<core::AbstractTriMesh> mesh = nullptr,
                    core::Frame f = core::Frame());

   ObjectPrimitive(const ObjectPrimitive &to_copy);

    virtual ~ObjectPrimitive();




    //overload
//    virtual Point GetPointInLocal   (const Point &p = Point::Zero()) const ;
//    virtual Point GetPointInWorld   (const Point &p = Point::Zero()) const ;
    virtual void Scale    (Scalar s, const bool local = false);
    virtual void Scale    (const Point &s, const Point &c, const bool local = false) ;
    virtual void Rotate   (const Point &a, const Point &c, Scalar th, const bool local = false) ;
    virtual void Translate(const Point &v, const bool local = false);

    virtual void Transform(const expressive::Transform& t, const bool local = false);

    void update_bounding_box();

    virtual void set_selected(bool selected, bool hover_parent = true);
    virtual void set_hovered(bool hovered, bool hover_parent = true);


    virtual bool ChildSelected(const uint /*id*/);
    virtual void Deselect();

    virtual void TranslateChild(const uint /*id*/, const Vector /*&v*/) ;



    // access function
    bool interactable() const;
    bool& interactable();

    const core::Frame& frame() const;
    core::Frame& frame();

    double frame_radius() const;
    Vector frame_centre() const;


protected:
    static unsigned int instance_counter_;
    core::Frame frame_;

    bool interactable_;
};


} // namespace core

} // namespace expressive


#endif // OBJECTPRIMITIVE_H

