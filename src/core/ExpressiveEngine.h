/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_GLOBALS_MANAGER_H_
#define EXPRESSIVE_GLOBALS_MANAGER_H_

// core dependencies
#include <core/CoreRequired.h>

#include <core/commands/CommandManager.h>
#include <core/commands/ScriptPlayer.h>
#include <core/scenegraph/SceneManager.h>
#include <core/scenegraph/Camera.h>

namespace ork
{
    class ResourceManager;
    class XMLResourceLoader;
    class MultithreadScheduler;
}

namespace expressive
{

namespace core
{

class CORE_API ExpressiveEngine : public Singleton<ExpressiveEngine>
{
public:
    ExpressiveEngine(const std::string &config_file/*, std::shared_ptr<SceneManager> scene_manager = nullptr*/);

    virtual ~ExpressiveEngine();

    std::shared_ptr<Camera> camera_manager() const;
    std::shared_ptr<CommandManager> command_manager() const;
    std::shared_ptr<SceneManager> scene_manager() const;
    ork::ptr<ork::ResourceManager> resource_manager() const;
    ork::ptr<ork::XMLResourceLoader> resource_loader() const;
    ork::ptr<ork::MultithreadScheduler> scheduler() const;
    std::shared_ptr<ScriptPlayer> script_player() const;

    /**
     * Returns the mouse position in screen space.
     */
    Point2 mouse_screen_position() const;

    /**
     * Sets the current mouse position in screen space.
     */
    void set_mouse_screen_position(const Point2 &pos);

protected:
    ExpressiveEngine();

    virtual void Init(const std::string &config_file/*, std::shared_ptr<SceneManager> scene_manager = nullptr*/);

    virtual void set_scene_manager(std::shared_ptr<SceneManager> scene_manager);

    std::shared_ptr<Camera> camera_manager_;
    std::shared_ptr<CommandManager> command_manager_;
    std::shared_ptr<SceneManager> scene_manager_;
    ork::ptr<ork::ResourceManager> resource_manager_;
    ork::ptr<ork::XMLResourceLoader> resource_loader_;
    ork::ptr<ork::MultithreadScheduler> scheduler_;
    std::shared_ptr<ScriptPlayer> script_player_;

    Point2 mouse_screen_pos_; // < Mouse screen position in 2D.

    friend class Singleton<ExpressiveEngine>;
};

} // namespace core

} // namespace expressive

#endif // EXPRESSIVE_GLOBALS_MANAGER_H_
