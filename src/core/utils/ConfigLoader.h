/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_CONFIGLOADER_H
#define EXPRESSIVE_CONFIGLOADER_H

#include <core/CoreRequired.h>
#include <core/utils/Serializable.h>

namespace expressive
{

/**
 * @brief The Config class is used to store global variables easily.
 * It stores float, strings, and Object* paramters, each mapped to a string to be able
 * to store and retrieve it easily.
 * Everything above that is just deprecated.
 */
class CORE_API Config
{
public:
//    // Global configurations
//    /**
//      * Default file used for QML Interface
//      **/
//    static std::string DEFAULT_GUI_FILE;

//    /**
//     * Default path for saving/loading examples.
//     */
//    static std::string DEFAULT_EXAMPLES_PATH;


//    // Scenegraph related
//    /**
//      * Default scene node material, used for any node in our scene.
//      */
////    static std::string DEFAULT_SCENE_NODE_MESH;
//    static std::string DEFAULT_CAMERA_DRAW_METHOD;
//    static std::string DEFAULT_CAMERA_MODULE;
//    static std::string DEFAULT_CAMERA_MOUSE_MODULE;
//    static std::string DEFAULT_LIGHT_DRAW_METHOD;
//    static std::string DEFAULT_LIGHT_MODULE;
//    static std::string DEFAULT_SCENE_NODE_DRAW_METHOD;
//    static std::string DEFAULT_SCENE_NODE_MODULE;

//    // scene manipulation related
//    /**
//      * Maximum number of undo /redo commands that are registered.
//      * Default is 10.
//      **/
//    static int EXPRESSIVE_MAX_UNDO_STEPS;

//    /**
//     * @brief DEFAULT_MOUSE_ROTATE_MODE
//     * default mouse rotation mode. For available modes, see CameraManipulator.
//     */
//    static int DEFAULT_MOUSE_ROTATE_MODE;

//    // Graph related.
//    /**
//      * Number of segment faces in drawn graph's edges.
//      * default is 50.
//      */
//    static int GRAPH_DEFAULT_SEGMENT_FACES;

//    /**
//     * @brief Config::GRAPH_DEFAULT_VERTEX_SIZE
//     * default vertex size (ratio to actual vertex size).
//     */
//    static float GRAPH_DEFAULT_VERTEX_SIZE;
//    /**
//     * @brief Config::GRAPH_DEFAULT_EDGE_SIZE
//     * default edge size (ratio to actual edge size).
//     */
//    static float GRAPH_DEFAULT_EDGE_SIZE;

//    /**
//     * Default materials used for graphs.
//     * default is BaseMaterial for each.
//     */
//    static std::string DEFAULT_GRAPH_MODULE;

//    static int GRAPH_DEFAULT_VERTEX_COLOR;
//    static int GRAPH_DEFAULT_EDGE_COLOR;
//    // # 8dcbff // normal blue
//    // # 3d6bcd // selected blue
//    // # e27737 // hovered orange

//    /**
//     * Surface polygonizer parameters.
//     */
//    static int POLYGONIZER_MAX_DEPTH;
//    static float POLYGONIZER_DEFAULT_RESOLUTION;
//    static float POLYGONIZER_DEFAULT_EPSILON;

//    // Mesh related stuff
//    static bool USE_MESH_OCTREE;
//    static bool USE_DUAL_CONTOURING;

//    // Set to true if no Ork opengl context is necessary (for unit tests for example).
//    static bool DISABLE_ORK;

//    static void setParameter(const std::string &name, int param); //< registers a float global variable
//    static void setParameter(const std::string &name, bool param); //< registers a bool global variable
    static void setParameter(const std::string &name, float param); //< registers a float global variable
    static void setParameter(const std::string &name, const std::string &param);  //< registers a string global variable
    static void setParameter(const std::string &name, ork::ptr<ork::Object> param); //< registers a ptr global variable

    static int getIntParameter(const std::string &name, int default_value = 0); //< Returns the variable mapped to name.
    static bool getBoolParameter(const std::string &name, bool default_value = false); //< Returns the variable mapped to name.
    static float getFloatParameter(const std::string &name, float default_value = 0.f); //< Returns the variable mapped to name.
    static std::string getStringParameter(const std::string &name, const std::string &default_value = ""); //< Returns the variable mapped to name.
    static ork::ptr<ork::Object> getObjectParameter(const std::string &name, ork::ptr<ork::Object> default_value = nullptr); //< Returns the variable mapped to name.

//    static const std::map<std::string, int> &getIntParameters() const;
//    static const std::map<std::string, bool> &getBoolParameters() const;
    static const std::map<std::string, float> &getFloatParameters();
    static const std::map<std::string, std::string> &getStringParameters();
    static const std::map<std::string, ork::ptr<ork::Object> > &getObjectParameters();

    static void registerParameter(const std::string &name, const std::string &value);

protected:

    // Should we use a map<std::string, void*> instead ?
//    static std::map<std::string, int> intParameters;
//    static std::map<std::string, bool> boolParameters;
    static std::map<std::string, float> floatParameters;
    static std::map<std::string, std::string> stringParameters;
    static std::map<std::string, ork::ptr<ork::Object> > objectParameters;

};

/**
 * @brief The ConfigLoader class is able to load a Config from a given .xml file.
 */
class CORE_API ConfigLoader : public expressive::core::Serializable
{
public:
    ConfigLoader();
    virtual ~ConfigLoader();

    virtual TiXmlElement *ToXml(const char *) const;
    virtual void swap(ork::ptr<ConfigLoader> &/*c*/);
};

} // namespace expressive

#endif // EXPRESSIVE_CONFIGLOADER_H
