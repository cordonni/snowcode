#include <core/utils/ColorTools.h>

namespace expressive
{

HSV rgb2hsv(const Color &in)
{
    HSV out;
    float min, max, delta;
    min = std::min(in[0], std::min(in[1], in[2]));
    max = std::max(in[0], std::max(in[1], in[2]));

    out[2] = max;                               // v
    delta = max - min;
    if (max > 0.f) {
        out[1] = (delta / max);                 // s
    } else {
        out[1] = 0.f;                           // s = 0, v is undefined
        out[0] = NAN;
        return out;
    }
    if( in.R() >= max ) {                         // > is bogus, just keeps compilor happy
        out[0] = ( in.G() - in.B() ) / delta;        // between yellow & magenta
    } else {
        if( in.G() >= max ) {
            out[0] = 2.f + ( in.B() - in.R() ) / delta;  // between cyan & yellow
        } else {
            out[0] = 4.f + ( in.R() - in.G() ) / delta;  // between magenta & cyan
        }
    }

    out[0] *= 60.0;                              // degrees

    if( out[0] < 0.0 )
        out[0] += 360.0;

    return out;
}

Color hsv2rgb(const HSV &in)
{
    float  hh, p, q, t, ff;
    long        i;
    Color out;

    if(in[1] <= 0.f) {       // < is bogus, just shuts up warnings
        out.R() = in[2];
        out.G() = in[2];
        out.B() = in[2];
        return out;
    }
    hh = in[0];
    if(hh >= 360.f) {
        hh = 0.f;
    }

    hh /= 60.0;
    i = (long)hh;
    ff = hh - i;
    p = in[2] * (1.f - in[1]);
    q = in[2] * (1.f - (in[1] * ff));
    t = in[2] * (1.f - (in[1] * (1.f - ff)));

    switch(i) {
    case 0:
        out.R() = in[2];
        out.G() = t;
        out.B() = p;
        break;
    case 1:
        out.R() = q;
        out.G() = in[2];
        out.B() = p;
        break;
    case 2:
        out.R() = p;
        out.G() = in[2];
        out.B() = t;
        break;

    case 3:
        out.R() = p;
        out.G() = q;
        out.B() = in[2];
        break;
    case 4:
        out.R() = t;
        out.G() = p;
        out.B() = in[2];
        break;
    case 5:
    default:
        out.R() = in[2];
        out.G() = p;
        out.B() = q;
        break;
    }
    return out;
}

//rgb hex2rgb(int val)
//{
//    rgb res;

//    res.r = ((val >> 16) & 0xFF) / 255.0;
//    res.g = ((val >> 8) & 0xFF) / 255.0;
//    res.b = ((val) & 0xFF) / 255.0;

//    return res;
//}

Color HexToColor(int val, bool alpha_at_end)
{
    Color res;

    if (alpha_at_end) {
        res[0] = ((val >> 24) & 0xFF) / 255.f;
        res[1] = ((val >> 16) & 0xFF) / 255.f;
        res[2] = ((val >>  8) & 0xFF) / 255.f;
        res[3] = ((val >>  0) & 0xFF) / 255.f;
    } else {
        res[3] = ((val >> 24) & 0xFF) / 255.f;
        res[0] = ((val >> 16) & 0xFF) / 255.f;
        res[1] = ((val >>  8) & 0xFF) / 255.f;
        res[2] = ((val >>  0) & 0xFF) / 255.f;
    }

    return res;
}

std::string ColorToString(const Color &c)
{
    char res[8];
    sprintf(res, "#%02x%02x%02x", (unsigned int) (c[0] * 255), (unsigned int)(c[1]* 255), (unsigned int)(c[2] * 255));
    return std::string(res);
}

} // namespace expressive
