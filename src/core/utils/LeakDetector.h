/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef _EXPRESSIVE_LEAK_DETECTOR_H_
#define _EXPRESSIVE_LEAK_DETECTOR_H_

// core dependencies
#include <core/CoreRequired.h>

#include <map>
#include <string> 

namespace expressive
{

namespace core
{

/**
 * @brief The LeakDetector class allows a class to embed it's own usage counter.
 * When using those, ExpressiveLab will raise an error if the counter has not decreased to 0 (i.e. if not all items were deleted).
 */
class CORE_API LeakDetector
{
public:
    LeakDetector(const std::string &class_type = nullptr);
    
    virtual ~LeakDetector();

    const std::string class_type() const;
    
    static void exit();

protected:
#ifdef CHECK_EXPRESSIVE_LEAKS
    const std::string class_type_;
#endif

//    static unsigned int count_;

    static std::map<std::string, int> counts_;

};

} // namespace core

} // namespace expressive

#endif // _EXPRESSIVE_LEAK_DETECTOR_H_
