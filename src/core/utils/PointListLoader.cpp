#include <core/utils/PointListLoader.h>
#include <cstdlib>
#include <fstream>

#ifdef _WIN32
#include <ctime>
#endif

using namespace std;

namespace expressive
{

namespace core
{


std::shared_ptr<SubdivisionCurveT<WeightedPoint> > LoadWeightedSubdivisionCurveFromList(const std::string &filename, bool random_weights)
{
    srand((uint)time(0));
    string line;
    ifstream myfile(filename);
    double x, y, z;
    double baseW = 3.0;
    vector<WeightedPoint> points;
    vector<expressive::Scalar> weights;
    vector<unsigned int> windices;

    for (unsigned int i = 0; i < 16; ++i) {
        Scalar w = baseW;// + float((rand() % 5 - 2.5));
        if (random_weights) {
            baseW += float((rand() % 10 - 5.0) / 5.0);
            baseW = min(baseW, 5.5);
            baseW = max(baseW, 1.0);
        }
        weights.push_back(w);
    }

    int i = 0;
    int diff = 1;
    if (myfile.is_open()) {
        while(getline(myfile, line)) {
            sscanf(line.c_str(), "[%lf, %lf, %lf]", &x, &y, &z);
            Scalar w = weights[i];
            windices.push_back(i);

            points.push_back(WeightedPoint(x, y, z, w));
            i += diff;
            if (i == 16) {
                diff = -1;
                i += diff;
            }
            if (i == -1) {
                diff = 1;
                i += diff;
            }
        }
        myfile.close();
    }

    return std::make_shared<SubdivisionCurveT<WeightedPoint> >(points, true);

}

} // namespace core

} // namespace expressive
