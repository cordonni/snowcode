/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_PRINTERS_H
#define EXPRESSIVE_PRINTERS_H

#include <iostream>
#include <iomanip>

namespace expressive
{

/**
 * pretty-prints a given matrix-based eigen structure
 */

template<typename VECTOR>
typename std::enable_if<(VECTOR::ColsAtCompileTime == 1),void>::type
pretty_print(const VECTOR &vec)
{
    for (unsigned int index = 0; index < VECTOR::RowsAtCompileTime; ++index) {
        std::cout << std::setw(10) << std::setprecision(4) << vec[index];
        if (index == VECTOR::RowsAtCompileTime - 1) {
            std::cout << ", ";
        }
    }
    std::cout << std::endl;
}

template<typename MATRIX>
typename std::enable_if<(MATRIX::ColsAtCompileTime != 1),void>::type
pretty_print(const MATRIX &mat)
{
    for (unsigned int row = 0; row < MATRIX::RowsAtCompileTime; ++row) {
        for (unsigned int col = 0; col < MATRIX::ColsAtCompileTime; ++col) {
            std::cout << std::setw(10) << std::setprecision(4) << mat(row, col);
            if (col < MATRIX::ColsAtCompileTime - 1) {
                std::cout << ", ";
            }
        }
        if (MATRIX::ColsAtCompileTime > 1 || row == MATRIX::RowsAtCompileTime - 1) {
            std::cout << std::endl;
        } else if (MATRIX::ColsAtCompileTime == 1) {
            std::cout << ", ";
        }
    }
}


}

#endif // EXPRESSIVE_PRINTERS_H

