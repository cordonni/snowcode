#ifndef Octree_HPP
#define Octree_HPP

#include "Octree.h"
#include <queue>

namespace expressive
{

namespace core
{

template<class C>
void Octree<C>::copyDataFrom(const Octree& other)
{
    max_depth_ = other.max_depth_;

    v_content_ = other.v_content_;
    v_leaves_  = other.v_leaves_;
    v_nodes_   = other.v_nodes_;
    v_voxels_  = other.v_voxels_;

    v_ext_content_ = other.v_ext_content_;
    v_pos_in_ext_  = other.v_pos_in_ext_;

    v_overlfow = other.v_overlfow;
}

template<class C>
void Octree<C>::clear()
{
    AABBox bbox;
    if(v_voxels_.size())
        bbox = v_voxels_[root()].aabbox;

    // clear buffers
    v_content_.clear();
    v_leaves_.clear();
    v_nodes_.clear();
    v_voxels_.clear();

    v_ext_content_.clear();
    v_pos_in_ext_.clear();

    v_overlfow.clear();

    // create the root
    createRoot();
    set_boundingBox(bbox);
}

template<class C>
void Octree<C>::createRoot()
{
    v_voxels_.add(Voxel()); // add it at position 0 (root)
    v_voxels_[root()].isLeaf = true;
    v_voxels_[root()].parent = SparseContainer<Voxel>::invalid;
    v_voxels_[root()].leaf = v_leaves_.add(VoxelLeaf());
}

template<class C>
typename Octree<C>::ContentHandle Octree<C>::add(const C& c)
{
    ContentHandle ch = v_content_.add(c);
    add_ch(ch);
    return ch;
}

template<class C>
typename Octree<C>::ContentHandle Octree<C>::add(C&& c)
{
    ContentHandle ch = v_content_.add(std::move(c));
    add_ch(ch);
    return ch;
}

template<class C>
void  Octree<C>::add_ch(ContentHandle ch)
{
    using ParseInfo = std::pair<VoxelHandle, uint>;

    std::stack<ParseInfo> parse;
    parse.push({root(), 0});

    AABBox chaabbox = v_content_[ch].bounding_box();
    if(!v_voxels_[root()].aabbox.ContainsOrEquals(chaabbox))
    {
        if(v_pos_in_ext_.size() <= ch)
            v_pos_in_ext_.resize(ch+1);
        v_pos_in_ext_[ch] = v_ext_content_.add(ch);
        return;
    }

    while(!parse.empty())
    {
        Octree::VoxelHandle cur_h = parse.top().first;
        uint level = parse.top().second;

        parse.pop();

        SparseContainer<Voxel>::iterator voxel_cur_it = v_voxels_.toIterator(cur_h);

        // if we are in a leaf
        if(voxel_cur_it->isLeaf)
        {
            SparseContainer<VoxelLeaf>::iterator leaf_it = v_leaves_.toIterator(voxel_cur_it->leaf);

            // check if the leaf is full
            if(leaf_it->isFull())
            {
                if(level != max_depth_)
                    subdivideLeaf(cur_h); // now the leaf becomes a node, and is parsed afterward as other nodes
                else
                {
                    // at max depth, we are forced to store the content in an external vector
                    if(leaf_it->overflow == VoxelOverflowContainer::invalid)
                        leaf_it->overflow = v_overlfow.add(std::vector<ContentHandle>());
                    v_overlfow[leaf_it->overflow].push_back(ch);
                }
            }
            else
            {
                leaf_it->add(ch);
                continue;
            }
        }

        // if we are in a node, add the good childs to the parse queue
        if(!voxel_cur_it->isLeaf)
        {
            SparseContainer<VoxelNode>::iterator node_it = v_nodes_.toIterator(voxel_cur_it->node);
            for(int i =0; i<8; i++)
                if(v_voxels_[node_it->childs_[i]].aabbox.Intersect(chaabbox))
                    parse.push({node_it->childs_[i], level+1});
        }
    }
}

template<class C>
void Octree<C>::subdivideLeaf(VoxelHandle handle)
{
    SparseContainer<Voxel>::iterator voxel_it = v_voxels_.toIterator(handle);

    // copy the leaves elements
    VoxelHandle elems[LEAF_CAPACITY];
    for(int i = 0; i< LEAF_CAPACITY; i++)
        elems[i] = v_leaves_[voxel_it->leaf].cells[i];

    // clear the leaf and set it as the first leaf of the new node
    v_leaves_[voxel_it->leaf].size = 0;

    // create the node and its sub voxels
    VoxelNodeHandle node_h = v_nodes_.add(VoxelNode());
    for(int i = 0; i<8; i++)
    {
        v_nodes_[node_h].childs_[i] = v_voxels_.add(Voxel());
        v_voxels_[v_nodes_[node_h].childs_[i]].parent = handle;
        v_voxels_[v_nodes_[node_h].childs_[i]].isLeaf = true;

        AABBox bbox = child_aabb(voxel_it->aabbox, i);
        v_voxels_[v_nodes_[node_h].childs_[i]].aabbox = bbox;

        // place the original leaf as first child and create the others
        v_voxels_[v_nodes_[node_h].childs_[i]].leaf = (i)?v_leaves_.add(VoxelLeaf()) : voxel_it->leaf;
        SparseContainer<VoxelLeaf>::iterator leaf_it = v_leaves_.toIterator(v_voxels_[v_nodes_[node_h].childs_[i]].leaf);

        for(int j = 0; j<LEAF_CAPACITY; j++)
            if(v_content_[elems[j]].bounding_box().Intersect(bbox))
                leaf_it->add(elems[j]);
    }

    // set the node as voxel node
    voxel_it->isLeaf = false;
    voxel_it->node = node_h;
}


template<class C>
inline AABBox Octree<C>::child_aabb(AABBox aabb, int i)
{
    Point mid = 0.5 * (aabb.max() + aabb.min());

    switch (i) {
    case 0:
        return AABBox(aabb.min(), mid);
    case 1:
        return AABBox(Point(mid[0], aabb.min()[1], aabb.min()[2]), Point(aabb.max()[0], mid[1], mid[2]));
    case 2:
        return AABBox(Point(aabb.min()[0], mid[1], aabb.min()[2]), Point(mid[0], aabb.max()[1], mid[2]));
    case 3:
        return AABBox(Point(mid[0], mid[1], aabb.min()[2]), Point(aabb.max()[0], aabb.max()[1], mid[2]));
    case 4:
        return AABBox(Point(aabb.min()[0], aabb.min()[1], mid[2]), Point(mid[0], mid[1], aabb.max()[2]));
    case 5:
        return AABBox(Point(mid[0], aabb.min()[1], mid[2]), Point(aabb.max()[0], mid[1], aabb.max()[2]));
    case 6:
        return AABBox(Point(aabb.min()[0], mid[1], mid[2]), Point(mid[0], aabb.max()[1], aabb.max()[2]));
    case 7:
        return AABBox(mid, aabb.max());
    default:
        break;
    }
    return AABBox();
}

template<class C>
inline uint Octree<C>::reorder(uint c, Vector ray_dir)
{
    // order of childs is ZYX in binary
    // in xyz, it is:
    // (-,-,-) (+,-,-) (-,+,-) (+,+,-)
    // (-,-,+) (+,-,+) (-,+,+) (+,+,+)

    bool bx = ray_dir[0] < 0;
    bool by = ray_dir[1] < 0;
    bool bz = ray_dir[2] < 0;

    uint mask = (bx <<2) | (by << 1) | (bz << 0);

    return c ^ mask;

}


template<class C>
void Octree<C>::remove(ContentHandle ch)
{
    std::stack<VoxelHandle> parse;
    parse.push(root());

    AABBox chaabbox = v_content_[ch].bounding_box();

    if(!v_voxels_[root()].aabbox.ContainsOrEquals(chaabbox))
    {
        v_ext_content_.rm(v_pos_in_ext_[ch]);
        v_content_.rm(ch);
        return;
    }

    while(!parse.empty())
    {
        Octree::VoxelHandle cur_h = parse.top();
        parse.pop();

        SparseContainer<Voxel>::iterator voxel_cur_it = v_voxels_.toIterator(cur_h);

        if(voxel_cur_it->isLeaf)
        {
            if(!v_leaves_[voxel_cur_it->leaf].rm(ch) && v_leaves_[voxel_cur_it->leaf].overflow != VoxelOverflowContainer::invalid)
            {
                // maybe the node is in overflow
                std::vector<ContentHandle>& ov = v_overlfow[v_leaves_[voxel_cur_it->leaf].overflow];
                typename std::vector<ContentHandle>::iterator it = std::find(ov.begin(), ov.end(), ch);
                if(it != ov.end())
                {
                    *it = ov.back();
                    ov.resize(ov.size()-1);

                    if(ov.empty())
                    {
                        v_overlfow.rm(v_leaves_[voxel_cur_it->leaf].overflow);
                        v_leaves_[voxel_cur_it->leaf].overflow = VoxelOverflowContainer::invalid;
                    }
                }
            }

        }
        else
        {
            SparseContainer<VoxelNode>::iterator node_it = v_nodes_.toIterator(voxel_cur_it->node);
            for(int i =0; i<8; i++)
                if(v_voxels_[node_it->childs_[i]].aabbox.Intersect(chaabbox))
                    parse.push(node_it->childs_[i]);
        }
    }
    v_content_.rm(ch);
}

template<class C>
void Octree<C>::clean()
{
    // parse all nodes, from leaves to root
    // this is a breadth first search to make sure that all leaves have been parsed before looking for parents
    std::queue<VoxelHandle> parse;
    std::vector<bool> in_parse_list(v_voxels_.size(), false);

    for(SparseContainer<Voxel>::iterator it = v_voxels_.begin(); it != v_voxels_.end(); ++it)
        if(it->isLeaf)
            parse.push(it.Handle());

    while(!parse.empty())
    {
        VoxelHandle cur_h = parse.front();
        parse.pop();

        SparseContainer<Voxel>::iterator voxel_cur_it = v_voxels_.toIterator(cur_h);
        VoxelHandle parent = voxel_cur_it->parent;

        if(!voxel_cur_it->isLeaf)
        {

            in_parse_list[cur_h] = false;

            SparseContainer<VoxelNode>::iterator node_it = v_nodes_.toIterator(voxel_cur_it->node);

            // check if the node is collapsable
            int num_content = 0;


            bool collapsable = true;
            for(int i =0; i<8 && collapsable; i++)
            {
                if(!v_voxels_[node_it->childs_[i]].isLeaf)
                    collapsable = false;
                else
                {
                    num_content += v_leaves_[v_voxels_[node_it->childs_[i]].leaf].size;
                    collapsable = collapsable && num_content<= LEAF_CAPACITY && v_leaves_[v_voxels_[node_it->childs_[i]].leaf].overflow == VoxelOverflowContainer::invalid;
                }
            }

            if(collapsable)
                collapseNode(cur_h);

            // the collapsed node becomes a leaf and is parsed as such in the next part of the loop

        }
        if(voxel_cur_it->isLeaf)
        {
            if(parent != SparseContainer<Voxel>::invalid && !in_parse_list[parent])
            {
                in_parse_list[parent] = true;
                parse.push(parent);
            }
        }
    }
}

// collapse a node into a leaf
template<class C>
void Octree<C>::collapseNode(VoxelHandle handle)
{
    SparseContainer<Voxel>::iterator voxel_it = v_voxels_.toIterator(handle);
    SparseContainer<VoxelNode>::iterator node_it = v_nodes_.toIterator(voxel_it->node);

    // copy the leaves elements into first leaf and delete the other ones
    VoxelLeafHandle firstLeafHandle = v_voxels_[node_it->childs_[0]].leaf;
    SparseContainer<VoxelLeaf>::iterator firstLeaf_it = v_leaves_.toIterator(firstLeafHandle);
    for(int i = 1; i< 8; i++)
    {
        SparseContainer<VoxelLeaf>::iterator nextLeaf_it = v_leaves_[v_voxels_[node_it->childs_[i]].leaf];
        for(int j = 0; j< nextLeaf_it->size; j++)
            firstLeaf_it->add(nextLeaf_it->cells[j]);

        v_leaves_.rm(v_voxels_[node_it->childs_[i]].leaf);
        v_voxels_.rm(node_it->childs_[i]);
    }

    // delete first child
    v_voxels_.rm(node_it->childs_[0]);

    // delete node
    v_nodes_.rm(voxel_it->node);

    // update voxel as leaf
    voxel_it->isLeaf = true;
    voxel_it->leaf = firstLeafHandle;
}

template<class C>
Scalar Octree<C>::FindIntersection(const BasicRay& ray, Scalar dist_max)
{
    Scalar res = dist_max;

    // check exterior intersection ... may be slow, make sure the most triangles possible are interior to the aabb
    for(auto h : v_ext_content_)
        res = std::min(res, v_content_[h].FindIntersection(ray, dist_max));

    std::stack<VoxelHandle> parse;
    parse.push(root());

    while(!parse.empty())
    {
        Octree::VoxelHandle cur_h = parse.top();
        parse.pop();

        SparseContainer<Voxel>::iterator cur_it = v_voxels_.toIterator(cur_h);


        if(res < cur_it->aabbox.FindIntersection(ray.starting_point(), ray.direction(), dist_max))
            continue;

        if(cur_it->isLeaf)
        {
            SparseContainer<VoxelLeaf>::iterator leaf_it = v_leaves_.toIterator(cur_it->leaf);
            for(size_t i =0; i<leaf_it->size; i++)
                res = std::min(res, v_content_[leaf_it->cells[i]].FindIntersection(ray, dist_max));

            if(leaf_it->overflow != VoxelOverflowContainer::invalid)
            {
                for(auto ch : v_overlfow[leaf_it->overflow])
                    res = std::min(res, v_content_[ch].FindIntersection(ray, dist_max));
            }
        }
        else
        {
            for(int i = 0; i<8; i++)
            {
                int c = reorder(i, ray.direction());
                if(!v_voxels_[v_nodes_[cur_it->node].childs_[c]].aabbox.isBehind(ray.starting_point(), ray.direction()))
                    parse.push(v_nodes_[cur_it->node].childs_[c]);
            }
        }
    }

    return res;
}

template<class C>
void Octree<C>::optimizeBoundingBox()
{
    // parse order from leaves to node
    std::vector<VoxelHandle> handle_parse;
    handle_parse.reserve(v_voxels_.size());


    std::queue<VoxelHandle> parse;
    parse.push(root());

    while(!parse.empty())
    {
        VoxelHandle handle = parse.front();
        parse.pop();

        handle_parse.push_back(handle);

        if(!v_voxels_[handle].leaf)
            for(int i =0; i<8; ++i)
                parse.push(v_nodes_[v_voxels_[handle].node].childs_[i]);
    }

    for(int i =(int)(handle_parse.size()-1); i>=0; --i)
    {
        VoxelHandle handle = handle_parse[i];

        SparseContainer<Voxel>::iterator voxel_it = v_voxels_.toIterator(handle);

        AABBox contentBB;

        if(voxel_it->isLeaf)
        {
            SparseContainer<VoxelLeaf>::iterator leaf_it = v_leaves_.toIterator(voxel_it->leaf);
            for(int i = 0; i<leaf_it->size; i++)
                contentBB.Enlarge(v_content_[leaf_it->cells[i]].bounding_box());

            if(leaf_it->overflow != VoxelOverflowContainer::invalid)
            {
                for(auto ch : v_overlfow[leaf_it->overflow])
                    contentBB.Enlarge(v_content_[ch].bounding_box());
            }
        }
        else
        {
            for(int i = 0; i<8; i++)
                contentBB.Enlarge(v_voxels_[v_nodes_[voxel_it->node].childs_[i]].aabbox);
        }
        voxel_it->aabbox = AABBox::Intersection(voxel_it->aabbox, contentBB);
    }

}

template<class C>
void Octree<C>::recomputeBoundingBox(AABBox rootBBox)
{
    v_voxels_[root()].aabbox = rootBBox;

    std::stack<VoxelHandle> parse;
    parse.push(root());

    while(!parse.empty())
    {
        VoxelHandle handle = parse.top();
        parse.pop();

        SparseContainer<Voxel>::iterator voxel_it = v_voxels_.toIterator(handle);

        if(voxel_it->isLeaf)
            continue;

        SparseContainer<VoxelNode>::iterator node_it = v_nodes_.toIterator(voxel_it->node);
        for(int i = 0; i<8; i++)
        {
            parse.push(node_it->childs_[i]);
            v_voxels_[node_it->childs_[i]].aabbox = child_aabb(voxel_it->aabbox, i);
        }
    }
}

// get a vector of cells
template<class C>
std::vector<AABBox> Octree<C>::getAABBvector()
{
    std::vector<AABBox> ret;
    ret.reserve(v_voxels_.size());

    std::stack<VoxelHandle> parse;
    parse.push(root());

    while(!parse.empty())
    {
        VoxelHandle handle = parse.top();
        parse.pop();

        SparseContainer<Voxel>::iterator voxel_it = v_voxels_.toIterator(handle);

        ret.push_back(voxel_it->aabbox);

        if(voxel_it->isLeaf)
            continue;

        for(int i = 0; i<8; i++)
            parse.push(v_nodes_[voxel_it->node].childs_[i]);
    }

    return ret;
}


}
}



#endif // Octree_HPP
