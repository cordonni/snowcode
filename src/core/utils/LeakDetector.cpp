// core dependencies
#include <core/utils/LeakDetector.h>
#include <ork/core/Logger.h>

using namespace std;
using namespace ork;

namespace expressive
{

namespace core
{

//unsigned int LeakDetector::count_ = 0;
map<string, int> LeakDetector::counts_;

LeakDetector::LeakDetector(const string &class_type)
#ifdef CHECK_EXPRESSIVE_LEAKS
    : class_type_(class_type)
{
    map<string, int>::iterator classCount = counts_.find(class_type);
    if (classCount == counts_.end()) {
        counts_[class_type] = 1;
    } else {
        counts_[class_type] += 1;
    }

    if (Logger::DEBUG_LOGGER != NULL) {
        Logger::DEBUG_LOGGER->logf("LEAK_CHECKING", "'%s' created", class_type_.c_str());
    }
#else
{
    UNUSED(class_type);
#endif // CHECK_EXPRESSIVE_LEAKS
}

LeakDetector::~LeakDetector()
{
#ifdef CHECK_EXPRESSIVE_LEAKS
    counts_[class_type_]--;

    if (Logger::DEBUG_LOGGER != NULL) {
        Logger::DEBUG_LOGGER->logf("LEAK_CHECKING", "'%s' deleted", class_type_.c_str());
    }
#endif // CHECK_EXPRESSIVE_LEAKS
}

const string LeakDetector::class_type() const
{
#ifdef CHECK_EXPRESSIVE_LEAKS
    return class_type_;
#else
    return "";
#endif // CHECK_EXPRESSIVE_LEAKS
}

void LeakDetector::exit()
{
#ifdef CHECK_EXPRESSIVE_LEAKS
    int cpt = 0;

    Logger::ERROR_LOGGER->log("LEAK_CHECKING", "Exit signal called. Watching classes : ");
    for (auto it : counts_) {
        Logger::ERROR_LOGGER->logf("LEAK_CHECKING", " - %s", it.first.c_str());
    }
    for (auto it : counts_) {
        if (it.second != 0) {
            Logger::ERROR_LOGGER->logf("LEAK_CHECKING", "%d remaining instance(s) of '%s'", it.second, it.first.c_str());
            cpt++;
        }
    }
    if (cpt) {
        Logger::ERROR_LOGGER->log("LEAK_CHECKING", "Some instances were not destroyed. Check bellow if they are deleted. If not, you probably have a memory leak on those classes.");
    } else {
        Logger::ERROR_LOGGER->log("LEAK_CHECKING", "No leaks detected amongst watched classes.");
    }
#endif // CHECK_EXPRESSIVE_LEAKS
}

} // namespace core

} // namespace expressive
