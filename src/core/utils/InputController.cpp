#include <core/utils/InputController.h>

namespace expressive
{

namespace core
{

///////////////////////////////////////////////////
// Constructeurs/Destructeurs
///////////////////////////////////////////////////
InputController::InputController (uint width , uint height ) : timestamp_(0), restart_(true), max_old_input_(1), input_width_(width), input_height_(height), pressed_(false), input_3D_ready_(false) {}
InputController::~InputController() {}

///////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////
Point InputController::input2DTo3D (Camera *camera, const Point &ref_pt, unsigned i /* = 0 */) {
    // Input
    Point2 pt = inputs_2D_[i];

    // Result
    Point cameraPos = camera->GetPointInWorld();
    Scalar cam_ray_length = (ref_pt - cameraPos).norm();
    inputs_3D_[i] = (camera->ViewplaneToWorld(pt[0], pt[1]) - cameraPos) * cam_ray_length + cameraPos;

    input_3D_ready_ = true;

    return inputs_3D_[i];
    // return (inputs_3D_[i] = cam_ori + (cam_dir.dotProduct(ref_pt - cam_ori))/(cam_dir.dotProduct(v_near)) * v_near);
}

void InputController::allInput2DTo3D (Camera *camera, const Point &ref_pt, bool old /* = false */) {
    // Camera Data
    Vector v_dir  = camera->GetRealDirection();

    // Inputs3D
    Point cameraPos = camera->GetPointInWorld();
    for (unsigned i=0; i<inputs_2D_.size(); i++) {
        // Input
        Point2 pt = inputs_2D_[i];

        // Camera Near Data
        Vector v_near = camera->ViewplaneToWorld(pt[0], pt[1]) - cameraPos;

        // Result
        Scalar length = (ref_pt-cameraPos).dot(v_dir)/(v_near.dot(v_dir));
        inputs_3D_[i] = cameraPos + length * v_near;
	}

    // OldInputs3D
	if (old) {
		for (unsigned i=0; i<old_inputs_2D_.size(); i++) {
			for (unsigned j=0; j<old_inputs_2D_[i].size(); j++) {
				// Input
                Point2 pt = old_inputs_2D_[i][j];

                // Camera Near Data
                Vector v_near = camera->ViewplaneToWorld(pt[0], pt[1]) - cameraPos;

                // Result
                Scalar length        = (ref_pt-cameraPos).dot(v_dir)/(v_near.dot(v_dir));
                old_inputs_3D_[i][j] = cameraPos + length * v_near;
            }
		}
    }
    input_3D_ready_ = true;
}

} // namespace core

} // namespace expressive
