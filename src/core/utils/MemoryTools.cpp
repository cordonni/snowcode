#include "core/utils/MemoryTools.h"

namespace expressive {

    void null_deleter::operator()(void const *) const {}

}
