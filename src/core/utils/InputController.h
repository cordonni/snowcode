/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: InputController.h

   Language: C++

   License: Convol Licence

   \author: Remi Brouet
   E-Mail: remi.brouet@inria.fr

   Description: Header file for input controllers (Mouse, Keyboard, MT, Pen, ...)

   Platform Dependencies: None
*/

#ifndef INPUT_CONTROLLER_H
#define INPUT_CONTROLLER_H

// core dependencies
#include<core/scenegraph/Camera.h>

// Standard
#include <vector>

namespace expressive
{

namespace core
{

/**
 * @brief The InputController class stores gui related data.
 * It contains 2D & 3D positions, timestamps, and a few other helpful things.
 * It stores position for the last and previous events too.
 * It is the base class for multitouch, mouse, keyboard and pen events.
 */
class CORE_API InputController {
public:
    

	// Constructeurs / Destructeurs
    InputController         (uint width = 1, uint height = 1);
	virtual ~InputController();
	
	// Getters
    inline unsigned long timestamp() const { return timestamp_; }
    inline virtual bool isRestart() const { return restart_; }

    inline bool is3DInputReady() const { return input_3D_ready_; }

    inline unsigned size() const { return (unsigned int) inputs_2D_.size(); }

    inline Point2 get2DInput(unsigned i = 0) const { return inputs_2D_[i]; }
    inline Point2 getNormalized2DInput(unsigned i = 0) const {
        Point2 res = inputs_2D_[i];
        res[0] /= input_width_;
        res[1] /= input_height_;
        return res;
    }

    inline Point   get3DInput(unsigned i = 0) const { return inputs_3D_[i]; }

    inline Point2 getOld2DInput(unsigned i = 0, unsigned int backward_steps = 0) const {
        if (old_inputs_2D_.size() == 0) {
            return get2DInput(i);
        }
        if (backward_steps >= old_inputs_2D_.size()) {
            assert(false && "request for a previous input that is not available.");
            backward_steps = (unsigned int) old_inputs_2D_.size() - 1;
        }

        return old_inputs_2D_[old_inputs_2D_.size() - backward_steps - 1][i];
    }

    inline Point getOld3DInput(unsigned i = 0, unsigned int backward_steps = 0) const {
        if (old_inputs_3D_.size() == 0) {
            return get3DInput(i);
        }
        if (backward_steps >= old_inputs_3D_.size()) {
            assert(false && "request for a previous input that is not available.");
            backward_steps = (uint) old_inputs_3D_.size() - 1;
        }
        return old_inputs_3D_[old_inputs_3D_.size() - backward_steps - 1][i];
    }
	
    inline Point2 get2DDelta(unsigned i = 0) const { return inputs_2D_[i] - old_inputs_2D_.back()[i]; }
    inline Point   get3DDelta(unsigned i = 0) const { return inputs_3D_[i] - old_inputs_3D_.back()[i]; }

    inline bool getPressed() const { return pressed_; }

	// Setters
	inline void setRestart    (bool restart) { restart_ = restart; }
	inline void setMaxOldInput(unsigned max) { max_old_input_ = max; }
    inline void setInputSize(uint width, uint height) {
        input_width_ = width;
        input_height_ = height;
    }

	// Functions
    virtual Point input2DTo3D   (core::Camera *camera, const Point &ref_pt, unsigned i  =  0);
    virtual void  allInput2DTo3D(core::Camera *camera, const Point &ref_pt, bool old = false);

protected:
    // Input Data
    std::vector<Point2> inputs_2D_;
    std::vector<Point>   inputs_3D_;
    unsigned long timestamp_;

	// Old Data
	bool     restart_;
	unsigned max_old_input_;

    std::vector< std::vector<Point2> > old_inputs_2D_;
    std::vector< std::vector<Point>   > old_inputs_3D_;

    // reference input size.
    // used to return normalized coordinates.
    uint input_width_;
    uint input_height_;

    bool pressed_;
    bool input_3D_ready_;
};

} // namespace core

} // namespace expressive


#endif // INPUT_CONTROLLER_H
