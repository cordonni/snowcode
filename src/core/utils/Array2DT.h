/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: Array2DT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: 2D array.

   Limitations: 2D only

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_ARRAY_2D_T_H_
#define CONVOL_ARRAY_2D_T_H_

// Convol dependencies
#include <core/CoreRequired.h>

#include "core/VectorTraits.h"

#include<assert.h>

namespace expressive
{

namespace core
{

/** 
* 
*/
template < typename value_type >
class Array2DT
{
public:

    Array2DT(unsigned int width, unsigned int height)
        : width_(width), height_(height)
    {
        tab_ = new value_type[height_*width_];
    }
    Array2DT(const Array2DT& other)
        : width_(other.width()), height_(other.height())
    {
        tab_ = new value_type[height_*width_];
        for(unsigned int i = 0; i<width_*height_; i++)
            tab_[i] = other.tab()[i];
    }

    ~Array2DT()
    {
        delete [] tab_;
    }

    void Init(value_type val)
    {
        for(unsigned int i = 0; i < height_*width_; ++i)
        {
            tab_[i] = val;
        }
    }

    inline value_type operator() (Point2 &p) const
    {
        return (*this)(p[0], p[1]);
    }

    inline value_type& operator() (Point2 &p)
    {
        return (*this)((unsigned int) p[0], (unsigned int) p[1]);
    }

    inline value_type operator () (unsigned int x, unsigned int y) const
    {
        assert(x<width_ && y<height_);
        //return tab_[ x + size_[0]*y + size_[0]*size_[1]*z ];
        return tab_[ x + width_*y ];
    }

    inline value_type& operator () (unsigned int x, unsigned int y)
    {
        assert(x<width_ && y<height_);
        return tab_[ x + width_*y ];
    }

    Array2DT& operator = (const Array2DT& other)
    {
        if(height_ != other.height_ || width_ != other.width_)
        {
            height_ = other.height_;
            width_ = other.width_;

            delete [] tab_;
            tab_ = new value_type[height_*width_];
        }

        for(unsigned int i = 0; i < this->height_*this->width_; ++i)
        {
            this->tab_[i] = other.tab_[i];
        }
        return *this;
    }


    ///////////////
    // Accessors //
    ///////////////

    inline unsigned int width()  const { return width_; }
    inline unsigned int height() const { return height_; }

    // WARNING : This one is used ONLY for improved efficiency.
    //           Use with EXTREME caution.
    inline value_type const* tab() const { return tab_; }
    inline value_type* nonconst_tab() { return tab_; }

protected:

    Array2DT() {}

    unsigned int width_;
    unsigned int height_;

    value_type *tab_;
};

} // namespace core

} // namespace expressive

#endif // CONVOL_ARRAY_2D_T_H_
