/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: Array3DT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: 3D array.

   Limitations: 3D only

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_ARRAY_3D_T_H_
#define CONVOL_ARRAY_3D_T_H_

#include<core/CoreRequired.h>

namespace expressive
{

namespace core
{

/** 
*   @todo optimize it by using only 1 array.
*   @todo copy assignement copy ptr by default !!
*/
template < typename value_type >
class Array3DT
{
public:

    Array3DT( const std::vector<unsigned int>& size)
        : size_(size)
    {
        // Allocate memory
        assert(size_.size() == 3); // Remove this assert and rewrite the allocation if you want a N dimensionnal grid
                tab_ = new value_type[size_[0]*size_[1]*size_[2]];
    }
    Array3DT( unsigned int size_x, unsigned int size_y, unsigned int size_z)
    {
        size_.push_back(size_x);
        size_.push_back(size_y);
        size_.push_back(size_z);
        // Allocate memory
        assert(size_.size() == 3); // Remove this assert and rewrite the allocation if you want a N dimensionnal grid
        tab_ = new value_type[size_[0]*size_[1]*size_[2]];
    }
    ~Array3DT()
    {
        assert(size_.size() == 3); // Remove this assert and rewrite the de-allocation if you want a N dimensionnal grid
                delete [] tab_;
    }

    void Init(value_type val)
    {
        for(unsigned int z = 0; z < size_[2]; ++z)
        {
            for(unsigned int y = 0; y < size_[1]; ++y)
            {
                for(unsigned int x = 0; x < size_[0]; ++x)
                {
                    //tab_[ x + size_[0]*y + size_[0]*size_[1]*z ] = val;
                    tab_[ x + size_[0]*(y + size_[1]*z) ] = val;
                }
            }
        }
    }

    inline const value_type& operator () (unsigned int x, unsigned int y, unsigned int z) const
    {
        assert(size_.size() == 3); // Remove this assert and rewrite the operator if you want a N dimensionnal grid
        assert(x<size_[0] && y<size_[1] && z<size_[2]);
        //return tab_[ x + size_[0]*y + size_[0]*size_[1]*z ];
        return tab_[ x + size_[0]*(y + size_[1]*z) ];
    }

    inline value_type& operator () (unsigned int x, unsigned int y, unsigned int z)
    {
        assert(size_.size() == 3); // Remove this assert and rewrite the operator if you want a N dimensionnal grid
        assert(x<size_[0] && y<size_[1] && z<size_[2]);
        //return tab_[ x + size_[0]*y + size_[0]*size_[1]*z ];
        return tab_[ x + size_[0]*(y + size_[1]*z) ];
    }

    ///////////////
    // Accessors //
    ///////////////
    
    const std::vector<unsigned int>& size() const { return size_; }

private:
    std::vector<unsigned int> size_;
    value_type *tab_;
};

} // namespace core

} // namespace expressive

#endif // CONVOL_ARRAY_3D_T_H_
