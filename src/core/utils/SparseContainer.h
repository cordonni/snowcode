/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SPARSECONTAINER_H
#define SPARSECONTAINER_H

#include <vector>
#include <stack>

namespace expressive
{

namespace core
{

/** SparseContainer is a simple extension of vector that allows for
 *   constant time deletion by flagging an object as invalid
 *   It is interfaced by handles (uint) for optimization, but they can be converted to iterators
 *   Iterator and handles are valid while there is no deletion
 *
 **/

template<class T>
class SparseContainer
{
public:

    // Handle
    using handle = uint;
    static const handle invalid = (handle)(-1);

    // iterator (TODO: const version)
    class iterator
    {
    public:
        T& operator *() {return (*cont_)[h_];}
        T* operator -> () {return &((*cont_)[h_]);}
        iterator& operator ++() {h_ = cont_->next(h_); return *this;}
        bool operator != (const iterator& other) {return h_ != other.h_;}

        handle Handle() {return h_;}

    private:
        iterator(SparseContainer* c, handle h) : cont_ {c}, h_ {h} {}
        SparseContainer* cont_;
        handle h_;

        friend class SparseContainer;
    };

    // Garbage collect.
    // Performs garbage collection at construction
    // then call operator [] on a handle to obtain the new handle value
    class GarbageCollect
    {
    public:
        GarbageCollect(SparseContainer&);
        virtual ~GarbageCollect() {}

        // new handle value. This is only valid for undeleted handle, there is no safecheck overwise !
        handle operator []  (handle h) {return new_handles_[h]; }
    private:
        std::vector<handle> new_handles_;
    };

    SparseContainer() {}
    virtual ~SparseContainer() {}

    // clear the container
    void clear();

    // add an element at a free space an return a handle
    handle add(const T&);
    handle add(T&&);

    // remove an element and at free space
    void rm(handle);
    void rm(iterator);

    // size
    size_t size() {return storage_.size() - free_storage_.size();}

    // check if the given position is valid. Should be taken with real care:
    //   there can be a valid - invalid - valid sequence at that handle position after a remove
    //
    bool isValid(handle h)   {return valid_[h];}

    // getter
    T& operator [] (handle h) {return storage_[h];}
    const T& operator [] (handle h) const  {return storage_[h];}

    // begin and end iterators
    iterator begin();
    iterator end() {return iterator(this, invalid); }

    // convert a iterator to a handle
    iterator toIterator(handle h) {return iterator(this, h); }

    // get next handle
    handle next(handle);

protected:
    std::vector<T> storage_;
    std::vector<bool> valid_;
    std::stack<handle> free_storage_;
};

///////////////////////////////////////////////////// Implementation /////////////////////////////////////////////////


/// ------------------- SparseContainer::GarbageCollect -------------------

// perfrom garbage collection on the given sparse container
template<class C>
SparseContainer<C>::GarbageCollect::GarbageCollect(SparseContainer& container) :
    new_handles_(container.size())
{
    // get indice of the last valid object of containers
    handle last = container.storage_.size();

    int signed_last = last;
    while(signed_last > 0 && !container.isValid(--signed_last));

    if(signed_last < 0)
    {
        container.clear();
        return;
    }

    last = signed_last;

    new_handles_.resize(last+1, invalid);

    while(!container.free_storage_.empty())
    {
        handle free_s = container.free_storage_.top();
        container.free_storage_.pop();

        // free space at the end of the storage, nothing to do
        if(free_s > last)
            continue;

        // else move the last element to the empty space
        std::swap(container.storage_[free_s], container.storage_[last]);
        new_handles_[last--] = free_s;
        container.valid_[free_s] = true;
    }

    //resize storage
    container.storage_.resize(last+1);
    container.valid_.resize(last+1);

    // check handle names: invlaid in new_handles_ means unchanged
    for(handle h = 0; h < new_handles_.size(); h++)
        if(new_handles_[h] == invalid)
            new_handles_[h] = h;
}

/// ------------------- SparseContainer -------------------

template<class T>
void SparseContainer<T>::clear()
{
    storage_.clear();
    valid_.clear();
    free_storage_ = std::stack<handle>();
}

template<class T>
inline typename SparseContainer<T>::handle SparseContainer<T>::add(const T& t)
{
    if(free_storage_.empty())
    {
        storage_.push_back(t);
        valid_.push_back(true);
        return handle(storage_.size()-1);
    }
    else
    {
        handle ret = free_storage_.top();
        free_storage_.pop();
        storage_[ret] = t;
        valid_[ret] = true;
        return ret;
    }
}
template<class T>
inline typename SparseContainer<T>::handle SparseContainer<T>::add(T&& t)
{
    if(free_storage_.empty())
    {
        storage_.push_back(std::move(t));
        valid_.push_back(true);
        return handle(storage_.size()-1);
    }
    else
    {
        handle ret = free_storage_.top();
        free_storage_.pop();
        storage_[ret] = std::move(t);
        valid_[ret] = true;
        return ret;
    }
}

template<class T>
inline void SparseContainer<T>::rm(handle h)
{
    free_storage_.push(h);
    valid_[h] = false;
}

template<class T>
inline typename SparseContainer<T>::iterator SparseContainer<T>::begin()
{
    handle h = 0;
    while(h < storage_.size() && !isValid(h++));
    return (h==storage_.size()) ? end() : iterator(this, h-1);
}

template<class T>
inline typename SparseContainer<T>::handle SparseContainer<T>::next(handle h)
{
    while(h < storage_.size() && !isValid(++h));
    return (h==storage_.size()) ? invalid : h;
}

}
}

#endif // SPARSECONTAINER_H
