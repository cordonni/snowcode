/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef COLORMANIPULATIONS_H
#define COLORMANIPULATIONS_H

#include <core/CoreRequired.h>
#include "core/VectorTraits.h"

/**
 * This file contains color related functions.
 */
namespace expressive
{

//    typedef struct {
//        double r;       // percent
//        double g;       // percent
//        double b;       // percent
//    } rgb;

//    typedef struct {
//        double h;       // angle in degrees
//        double s;       // percent
//        double v;       // percent
//    } HSV;

    /**
     * Transforms a color from RGB to HSV space.
     */
    HSV CORE_API rgb2hsv(const Color &in);

    /**
     * Transforms a color from HSV to RGB space.
     */
    Color CORE_API hsv2rgb(const HSV &in);

//    rgb CORE_API hex2rgb(int val);

    /**
     * Transforms a color from Hexadecimal to RGB space.
     */
    Color CORE_API HexToColor(int val, bool alpha_at_end = true);

    /**
     * Transforms a color from RGB to string.
     */
    std::string CORE_API ColorToString(const Color &c);

} // namespace expressive


#endif // COLORMANIPULATIONS_H
