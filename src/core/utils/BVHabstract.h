/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef BVHABSTRACT_H
#define BVHABSTRACT_H

#include <core/CoreRequired.h>
#include <core/utils/ListenerT.h>

namespace expressive {
namespace core {

class BasicRay;

class CORE_API BVHabstractTopology : public ListenableT<BVHabstractTopology>
{
protected:
    BVHabstractTopology() {}
public:
    virtual ~BVHabstractTopology () {}

    // get vecor of bb
    virtual std::vector<AABBox> getAABBvector() = 0;
};

// pure virtual class BVH

template<class Content>
class CORE_API BVHabstract : public BVHabstractTopology
{
protected:
    BVHabstract() {}

public:

    // types for contents
    using ContentHandle = uint;

    virtual ~BVHabstract () {}

    // clear the BVH
    virtual void clear() = 0;

    // the elements are added using one of the
    // folowing methods, and stored before recomputing the BVH
    virtual ContentHandle add(const Content&) = 0;
    virtual ContentHandle add(Content&&) = 0;

    // remove cells from the list
    // parse the BVH to remove the cell from the right place
    virtual void remove(ContentHandle) = 0;

    // get intrersections with a ray
    virtual Scalar FindIntersection(const BasicRay& ray, Scalar dist_max) =0;


    // recompute the BVH from the stored list
    virtual void compute() =0;

};

}
}

#endif // BVHABSTRACT_H
