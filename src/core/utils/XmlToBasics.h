/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#ifndef XML_TO_BASICS_H_
#define XML_TO_BASICS_H_

#include<core/CoreRequired.h>
//#include<core/geometry/Point.h>

// tinyxml dependencies
#include<tinyxml/tinyxml.h>
#include "core/VectorTraits.h"

namespace expressive {

namespace core {

    /** \brief This function initialize a Point from the given Xml element.
     *         The xml must show the following attributes :
     *         dim == Dimension of the point, which is required to match the dimension of Point
     *         x0,x1...xn == coordinate values for the point.
     *
     *    \param element_point A reference to the Xml description of the Point to be created.
     *    \param res Result if everything was right.
     *
     *  \return Return true if the load is a success, false otherwise.
     */
    bool CORE_API LoadPoint(const TiXmlElement* element_point, Point& res);

} // Close namespace core

} // Close namespace expressive


#endif // BASICS_TO_XML_H_
