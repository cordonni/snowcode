/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef ABSTRACTITERATOR_H
#define ABSTRACTITERATOR_H

#include <vector>
#include <set>
#include <map>

namespace expressive
{

namespace core
{

template<typename TYPE>
class AbstractIteratorBaseT
{
public:
    virtual TYPE Next() = 0;
    virtual bool HasNext() = 0;
};

template<typename CONTAINER, typename TYPE>
class AbstractIteratorT : public AbstractIteratorBaseT<TYPE>
{
public:
    AbstractIteratorT(const std::pair<typename CONTAINER::const_iterator, typename CONTAINER::const_iterator> p) :
        iterator(p.first), end(p.second)
    {
    }

    AbstractIteratorT(typename CONTAINER::const_iterator begin, typename CONTAINER::const_iterator end) :
        iterator(begin), end(end)
    {
    }

    AbstractIteratorT(const CONTAINER &container) :
        iterator(container.cbegin()), end(container.cend())
    {
    }

    virtual ~AbstractIteratorT() {}

    virtual TYPE Next() = 0;

    virtual bool HasNext()
    {
        return iterator != end;
    }

    typename CONTAINER::const_iterator iterator;

    typename CONTAINER::const_iterator end;
};

template<typename CONTAINER, typename TYPE>
class AbstractIterator : public AbstractIteratorT<CONTAINER, TYPE>
{
public:
    AbstractIterator(const std::pair<typename CONTAINER::const_iterator, typename CONTAINER::const_iterator> p) :
        AbstractIteratorT<CONTAINER, TYPE>(p)
    {
    }

    AbstractIterator(typename CONTAINER::const_iterator begin, typename CONTAINER::const_iterator end) :
        AbstractIteratorT<CONTAINER, TYPE>(begin, end)
    {
    }

    AbstractIterator(const CONTAINER &container) :
        AbstractIteratorT<CONTAINER, TYPE>(container)
    {
    }

    virtual TYPE Next()
    {
        TYPE res = *(this->iterator++);
//        this->iterator++;
        return res;
    }

    AbstractIterator& operator++()
    {
        Next();
        return *this ;
    }

};

template<typename CONTAINER, typename TYPE>
class AbstractMapIterator : public AbstractIteratorT<CONTAINER, TYPE>
{
public:
    AbstractMapIterator(const std::pair<typename CONTAINER::const_iterator, typename CONTAINER::const_iterator> p) :
        AbstractIteratorT<CONTAINER, TYPE>(p)
    {
    }

    AbstractMapIterator(typename CONTAINER::const_iterator begin, typename CONTAINER::const_iterator end) :
        AbstractIteratorT<CONTAINER, TYPE>(begin, end)
    {
    }

    AbstractMapIterator(const CONTAINER &container) :
        AbstractIteratorT<CONTAINER, TYPE>(container)
    {
    }

    virtual TYPE Next()
    {
        TYPE res = (this->iterator++)->second;
//        this->iterator++;
        return res;
    }
};

//template<typename TYPE>
//class VectorIterator : public AbstractIteratorT<std::vector<TYPE>, TYPE>
//{
//    typedef AbstractIteratorT<std::vector<TYPE>, TYPE> AbstractIterator;

//    VectorIterator(const std::pair<
//                   std::vector<TYPE>::iterator,
//                   std::vector<TYPE>::iterator> p);
////    :
////        AbstractIterator(p)
////    {}

//    VectorIterator(typename std::vector::iterator begin, std::vector::iterator end) :
//        AbstractIterator(begin, end)
//    {}

//    VectorIterator(const std::vector<TYPE> &container) :
//        AbstractIterator(container)
//    {}
//};

//template<typename TYPE>
//class SetIterator : public AbstractIteratorT<std::set<TYPE>, TYPE>
//{
//    typedef AbstractIteratorT<std::set<TYPE>, TYPE> AbstractIterator;

//    SetIterator(const std::pair<
//                   std::set<TYPE>::iterator,
//                   std::set<TYPE>::iterator> p);
////    :
////        AbstractIterator(p)
////    {}

//    SetIterator(typename std::set::iterator begin, std::set::iterator end) :
//        AbstractIterator(begin, end)
//    {}

//    SetIterator(const std::set<TYPE> &container) :
//        AbstractIterator(container)
//    {}
//};

//template<typename TYPEID, typename TYPE>
//class MapIterator : public AbstractIteratorT<std::map<TYPEID, TYPE>, TYPE>
//{
//    typedef AbstractIteratorT<std::map<TYPEID, TYPE>, TYPE> AbstractIterator;

//    MapIterator(const std::pair<
//                   std::map<TYPEID, TYPE>::iterator,
//                   std::map<TYPEID, TYPE>::iterator> p);
////    :
////        AbstractIterator(p)
////    {}

//    MapIterator(typename std::map::iterator begin, std::map::iterator end) :
//        AbstractIterator(begin, end)
//    {}

//    MapIterator(const std::map<TYPEID, TYPE> &container) :
//        AbstractIterator(container)
//    {}
//};

//template<typename TYPE>
//struct Iterators
//{
//    typedef AbstractIteratorT<std::vector<TYPE>, TYPE> VectorIterator;

//    typedef AbstractIteratorT<std::set<TYPE>, TYPE> SetIterator;
//};

// TODO : upgrade to GCC 4.7++, and use templated aliases.
//template <typename TYPE>
//using SetIterator2 = AbstractIteratorT<std::set<TYPE>, TYPE>;

} // namespace core

} // namespace expressive

#endif // ABSTRACTITERATOR_H
