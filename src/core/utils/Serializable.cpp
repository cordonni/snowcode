#include <core/utils/Serializable.h>

// tinyxml dependencies
#include "tinyxml/tinyxml.h"
#include <exception>

#include <ork/resource/ResourceTemplate.h>

using namespace ork;

namespace expressive
{

namespace core
{

Serializable::Serializable() :
    ork::Object(Name().c_str())
{
}

Serializable::Serializable(const std::string &type) :
    ork::Object(type == "" ? Name().c_str() : type.c_str())
{
}

Serializable::~Serializable()
{
}

TiXmlElement * Serializable::ToXml(const char *name) const
{
    TiXmlElement * element_base;
    if(name == NULL) {
        element_base = new TiXmlElement( this->Name() );
    } else {
        element_base = new TiXmlElement( name );
    }

    AddXmlAttributes(element_base);

    return element_base;
}

void Serializable::AddXmlAttributes(TiXmlElement * /*element*/) const
{ }

bool Serializable::SaveToXmlFile(const std::string &file_name, const char *name) const
{

    try {
        TiXmlDocument doc;

        // Create the "standart" statement at the begining of the xml file
        // <?xml version="1.0" ?>
        TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
        doc.LinkEndChild( decl );

        TiXmlElement * root_element = ToXml(name);
        doc.LinkEndChild(root_element);

        // Save the XmlDocument to file_name
        doc.SaveFile(file_name);
        return true;
    } catch(std::exception &e ) {
        ork::Logger::ERROR_LOGGER->logf("SERIALIZE", "Exception caught while saving %s : %s", Name().c_str(), e.what());
        return false;
    }
}

void Serializable::swap(ork::ptr<Serializable> &/*t*/)
{
    assert(false && "You should redefine the swap method for this class");
}

SerializablePool::SerializablePool() :
    Serializable(Name())
{
}

SerializablePool::~SerializablePool()
{
}

void SerializablePool::swap(ork::ptr<SerializablePool> &/*t*/)
{
    assert(false && "You should redefine the swap method for this class");
}


/// @cond RESOURCES

class SerializablePoolResource : public ResourceTemplate<30, SerializablePool>
{
public:
    SerializablePoolResource(ork::ptr<ResourceManager> manager, const std::string &name,
                         ork::ptr<ResourceDescriptor> desc, const TiXmlElement *e = NULL)
       : ResourceTemplate<30, SerializablePool>(manager, name, desc)
    {
        e = e == NULL ? desc->descriptor : e;
        checkParameters(desc, e, "name,");

        const TiXmlElement *element_current = e->FirstChildElement();
        if (element_current == nullptr) {
            return;
        }

        do
        {
            serializables_.push_back(manager->loadResource(desc, element_current).cast<Serializable>());
            element_current = element_current->NextSiblingElement();
        } while (element_current != nullptr);

    }
};

extern const char serializablePool[] = "Objects";

static ResourceFactory::Type<serializablePool, SerializablePoolResource> SerializablePoolType;

/// @endcond



} // namespace core

} // namesapce expressive

