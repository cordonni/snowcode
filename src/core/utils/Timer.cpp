#include "core/utils/Timer.h"
#include "ork/core/Logger.h"

using namespace ork;

namespace expressive
{

namespace core
{
Timer::Timer() :
    started_(false), total_duration_(0), n_stops_(0)
{
}

std::chrono::high_resolution_clock::time_point Timer::start()
{
    start_ = std::chrono::high_resolution_clock::now();
    started_ = true;

    return start_;
}

void Timer::reset()
{
    n_stops_ = 0;
    started_ = false;
//    total_duration_ = ;
}

//std::chrono::milliseconds Timer::stop()
int64_t Timer::stop()
{
    last_stop_ = std::chrono::high_resolution_clock::now();

    if (started_) {
        std::chrono::milliseconds res = std::chrono::duration_cast<std::chrono::milliseconds>(last_stop_ - start_);
        total_duration_ += res;
        start_ = last_stop_;
        n_stops_++;

        return res.count();
    }
    Logger::ERROR_LOGGER->log("CORE", "Logger stopped but not started");

    return total_duration_.count();
}

//std::chrono::milliseconds Timer::average()
int64_t Timer::average()
{
    return (total_duration_ /= n_stops_).count();
}

} // namespace core

} //namespace expressive
