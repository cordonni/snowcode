/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef TIMER_H
#define TIMER_H

#include <chrono>

namespace expressive
{

namespace core
{

/**
 * @brief The Timer class
 * Quick timer class to count time & average execution times.
 */
class Timer
{
public:
    Timer();

    /**
     * @brief start Starts chronometer.
     * @return current time.
     */
    std::chrono::high_resolution_clock::time_point start();

    /**
     * @brief reset Resets stats for this timer.
     */
    void reset();

    /**
     * @brief stop Stops chronometer.
     * @return duration since last start() or previous #stop(). Resets only when calling #reset().
     */
    int64_t stop();
//    std::chrono::milliseconds stop();

    /**
     * @brief average Counts average duration between each stop() after last start().
     */
    int64_t average();
//    std::chrono::milliseconds average();

protected:
    bool started_;

    std::chrono::high_resolution_clock::time_point start_;

    std::chrono::high_resolution_clock::time_point last_stop_;

    std::chrono::milliseconds total_duration_;

    unsigned int n_stops_;
};


} // namespace core

} //namespace expressive

#endif // TIMER_H
