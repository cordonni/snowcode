/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef LISTENABLE_H
#define LISTENABLE_H

#include <vector>
#include <memory>

#include <set>
#include <map>

namespace expressive
{

namespace core
{

template<typename T>
class ListenableT;

template<typename Target>
class ListenerT
{
public:
    ListenerT(Target *target)
    {
        Init(target);
    }

    virtual ~ListenerT()
    {
        if (target_ != nullptr) {
            target_->RemoveListener(this);
            target_ = nullptr;
        }
    }

    virtual void updated(Target * /*t*/ = nullptr) {}

protected:
    ListenerT() : target_(nullptr) {}

    virtual void Init(Target* target) {
        target_ = target;
        if (target != nullptr) {
            target->AddListener(this);
        }
    }

    Target* target_;

    friend class ListenableT<Target>;
};

template<typename T>
class ListenableT
{
public:
    ListenableT()
    {
    }

    ListenableT(const ListenableT<T> &other) :
        listeners_(other.listeners_),
        listener_pointers_(other.listener_pointers_)
    {
    }

    virtual ~ListenableT()
    {
        for (auto listener : listeners_) {
            listener->target_ = nullptr;
        }
        listeners_.clear();
    }

    virtual void NotifyUpdate()
    {
        for (auto listener : listeners_) {
            listener->updated(static_cast<T*>(this));
        }
    }

    virtual void AddListener(std::shared_ptr<ListenerT<T> > listener)
    {
        listeners_.insert(listener.get());
        listener_pointers_.insert(std::make_pair(listener.get(), listener));
    }

    virtual void AddListener(ListenerT<T> *listener)
    {
        listeners_.insert(listener);
    }

    virtual void RemoveListener(std::shared_ptr<ListenerT<T> > listener)
    {
        listeners_.erase(listener.get());
        listener_pointers_.erase(listener.get());
    }

    virtual void RemoveListener(ListenerT<T> * listener)
    {
        listeners_.erase(listener);
        listener_pointers_.erase(listener);
    }

    unsigned int GetListenerCount() const
    {
        return (unsigned int) listeners_.size();
    }

    const std::set<ListenerT<T> *> & GetListeners() const
    {
        return listeners_;
    }

protected:
    virtual void swap(ListenableT &t)
    {
        std::swap(listeners_, t.listeners_);
        std::swap(listener_pointers_, t.listener_pointers_);
    }

    std::set<ListenerT<T> * > listeners_;

private:
    std::map<ListenerT<T>*, std::shared_ptr<ListenerT<T> > > listener_pointers_;
};

//template<typename T>
//class WeakListenableT
//{
//public:
//    WeakListenableT()
//    {
//    }

//    virtual ~WeakListenableT()
//    {
//        listeners_.clear();
//    }

//    virtual void NotifyUpdate()
//    {
//        for (auto listener : listeners_) {
//            listener->updated(static_cast<T*>(this));
//        }
//    }

//    virtual void AddListener(ListenerT<T> * listener)
//    {
//        listeners_.insert(listener);
//    }

//    virtual void RemoveListener(ListenerT<T> * listener)
//    {
//        listeners_.erase(listener);
//    }

//    unsigned int GetListenerCount() const
//    {
//        return listeners_.size();
//    }

//protected:
//    virtual void swap(WeakListenableT &t) { std::swap(listeners_, t.listeners_); }

//    std::set<ListenerT<T> * > listeners_;
//};

} // namespace core

} // namespace expressive

#endif // LISTENABLE_H
