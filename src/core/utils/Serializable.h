/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#ifndef SERIALIZABLE_H_
#define SERIALIZABLE_H_

//#include "ork/core/Logger.h"
#include "ork/core/Object.h"

#include "core/CoreRequired.h"
//#include<core/utils/LeakDetector.h>


// std dependencies
#include <vector>

class TiXmlElement;

namespace expressive {

namespace core {

class CORE_API Serializable : public ork::Object
{
public:
//    
    EXPRESSIVE_MACRO_NAME("Serializable")

    Serializable();
    Serializable(const std::string &type);

    virtual ~Serializable();

    virtual TiXmlElement * ToXml(const char *name = nullptr) const;

    virtual void AddXmlAttributes(TiXmlElement * /*element*/) const;

    virtual bool SaveToXmlFile(const std::string &file_name, const char *name = nullptr) const;

    virtual void swap(ork::ptr<Serializable> &/*t*/);

protected:
//    virtual void LoadFromXml() = 0;
};

class CORE_API SerializablePool : public Serializable
{
public:
    EXPRESSIVE_MACRO_NAME("Objects")
    SerializablePool();
    virtual ~SerializablePool();

    virtual void swap(ork::ptr<SerializablePool> &/*t*/);

    std::vector<std::shared_ptr<Serializable> > serializables_;
};

} // Close namespace core

} // Close namespace expressive


#endif //SERIALIZABLE_H_

