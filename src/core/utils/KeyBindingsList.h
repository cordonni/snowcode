/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef KEYBINDINGLIST_H
#define KEYBINDINGLIST_H

#include <map>

namespace expressive
{

namespace core
{

/**
 * @brief The KeyBinding struct maps a key to a function.
 * It contains a small description and a longer description.
 */
struct KeyBinding
{
    std::string key;
    std::string description;
    std::string long_description;
};

// Todo : Add callback functions too, so we can actually press the buttons \o/ !
// Todo : Add an optionnal toggle state on that button too
// Todo : Link with TileParameter ?
// Todo : How to make Vector3 in TileParameters ?

/**
 * @brief The KeyBindingsList class contains a list of KeyBinding for each module it handles.
 */
class KeyBindingsList
{
public:
    typedef std::map<std::string, std::map<std::string, KeyBinding> > KeyContainer;

    KeyBindingsList() {}

    void AddKey(const std::string &module, const std::string &key, const std::string &description, const std::string &long_description = "")
    {
        m_bindings[module][key] = KeyBinding{key, description, long_description};
    }

    const KeyContainer & bindings() const { return m_bindings; }

    KeyBindingsList& MergeWith(const KeyBindingsList &other)
    {
        for (auto it : other.m_bindings) {
            for (auto it2 : it.second) {
                AddKey(it.first, it2.first, it2.second.description, it2.second.long_description);
            }
        }
        return *this;
    }

    KeyContainer m_bindings;
};

}

}

#endif // KEYBINDING_H
