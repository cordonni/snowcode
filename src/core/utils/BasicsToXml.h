/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#ifndef BASICS_TO_XML_H_
#define BASICS_TO_XML_H_

#include<core/CoreRequired.h>

// tinyxml dependencies
#include<tinyxml/tinyxml.h>

namespace expressive {

namespace core {

//TODO:@todo : could be templated to work with all kind of Eigen matrices ...
template<typename PointT>
TiXmlElement * PointToXml(const PointT& point, const char *name = nullptr)
{
    TiXmlElement * element;
    if(name == NULL)
        element = new TiXmlElement( "Point" );
    else
        element = new TiXmlElement( name );

    unsigned int n = (uint) PointT::RowsAtCompileTime;

    element->SetAttribute("n", n);

    if (n == 2 ) {
            element->SetDoubleAttribute("x",point[0]);
            element->SetDoubleAttribute("y",point[1]);
    } else if (n == 3) {
            element->SetDoubleAttribute("x",point[0]);
            element->SetDoubleAttribute("y",point[1]);
            element->SetDoubleAttribute("z",point[2]);
    } else if (n == 4) {
            element->SetDoubleAttribute("x",point[0]);
            element->SetDoubleAttribute("y",point[1]);
            element->SetDoubleAttribute("z",point[2]);
            element->SetDoubleAttribute("w",point[3]);
    } else {
        std::ostringstream name;
        for(unsigned int i = 0; i < n; ++i)
        {
            name.flush();
            name << "x" << i;
            element->SetDoubleAttribute(name.str(),point[i]);
        }
    }
    return element;
}

} // Close namespace core

} // Close namespace expressive


#endif // BASICS_TO_XML_H_
