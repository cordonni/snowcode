#include <core/utils/AssertionHandler.h>

namespace expressive
{

std::shared_ptr<AssertionHandler> AssertionHandler::DEFAULT_HANDLER(std::make_shared<AssertionHandler>());

}
