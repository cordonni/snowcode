/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef Octree_H
#define Octree_H

#include <core/CoreRequired.h>
#include <core/geometry/AABBox.h>
#include <core/geometry/BasicRay.h>
#include <core/utils/BVHabstract.h>

#include <core/utils/SparseContainer.h>

namespace expressive {
namespace core {

namespace ns_Octree {

struct VoxelLeaf;
struct VoxelNode;
struct Voxel;

// handles
using ContentHandle = uint; // should be something like template<class C> SparseContainer<C>::handle, but htis saves a lot of template
using VoxelNodeHandle = SparseContainer<VoxelNode>::handle;
using VoxelLeafHandle = SparseContainer<VoxelLeaf>::handle;
using VoxelHandle = SparseContainer<Voxel>::handle;
using VoxelOverflowContainer = SparseContainer<std::vector<ContentHandle>>;
using VoxelOverflowHandle = VoxelOverflowContainer::handle;



// Voxel leaf : contain handles to content
// Contains LEAF_CAPACITY elemtens. if their are more of them, they are redirected to a vector
// handled by overflow
#define LEAF_CAPACITY 16
struct CORE_API VoxelLeaf
{
    VoxelLeaf() : size{0}, overflow{VoxelOverflowContainer::invalid} {}
    bool isFull(){return size == LEAF_CAPACITY;}
    bool isEmpty() {return !size;}
    void add(ContentHandle ch) {cells[size++] = ch;}
    bool rm(ContentHandle ch) {uint i = 0; for(;i<size && cells[i]!=ch;++i); if(i==size)return false; cells[i] = cells[--size]; return true;}

    ContentHandle cells[LEAF_CAPACITY];
    uint size;

    VoxelOverflowHandle overflow;
};

// Voxel Node: point towards its childs
struct CORE_API VoxelNode
{
   VoxelHandle childs_[8];
};

// Voxel : either a node or a leaf, has a AABB
struct CORE_API Voxel
{
    bool isLeaf;

    union{
        VoxelLeafHandle leaf;
        VoxelNodeHandle node;
    };

    VoxelHandle parent;

    AABBox aabbox;
};

}


//C needs the two function:
// Scalar FindIntersection(const BasicRay &ray, Scalar dist_max);
// AABBox bounding_box()


template<class C>
class Octree : public BVHabstract<C>
{
public:

    using VoxelLeaf = ns_Octree::VoxelLeaf;
    using VoxelNode = ns_Octree::VoxelNode;
    using Voxel = ns_Octree::Voxel;

    using ContentHandle = typename BVHabstract<C>::ContentHandle;
    using VoxelNodeHandle = ns_Octree::VoxelNodeHandle;
    using VoxelLeafHandle = ns_Octree::VoxelLeafHandle;
    using VoxelHandle = ns_Octree::VoxelHandle;

    using VoxelOverflowContainer = ns_Octree::VoxelOverflowContainer;
    using VoxelOverflowHandle = ns_Octree::VoxelOverflowHandle;


    Octree(unsigned int max_depth = 16, const AABBox &bounding_box = AABBox(Vector::Constant(expressive::kScalarMin*0.25),
                                                                              Vector::Constant(expressive::kScalarMax*0.25)))
        : max_depth_ {max_depth} { clear(); set_boundingBox(bounding_box); }

    Octree(const Octree&);

    virtual ~Octree() {}

    void copyDataFrom(const Octree&);

    void clear();

    ContentHandle add(const C&);
    ContentHandle add(C&&);

    void remove(ContentHandle);

    // if nodes have been removed, parse the octree and collapse all nodes
    // whose sum of sub leaf content is lower than leaf capacity
    void clean();

    // optimise bounding boxes of each nodes as the intersection between actual bouding box and content bouding box
    // need to call recomputeBoudingBox before all changes in the octree
    void optimizeBoundingBox();

    // reset the bouding boxes as the origincal octree's one
    // if optimizeBoundingBox has not been called, it is useless
    void recomputeBoundingBox(AABBox);


    Scalar FindIntersection(const BasicRay& ray, Scalar dist_max);

    // make sure to call that before all addition
    void set_boundingBox(AABBox aabb) {v_voxels_[root()].aabbox = aabb; }

    // get a vector of cells
    std::vector<AABBox> getAABBvector();

    void compute() {}

protected:

    // root
    VoxelHandle root() {return VoxelHandle(0);}

    // add a ContentHandle in the voxel
    void add_ch(ContentHandle);

    // create the root
    void createRoot();

    //subdivide a voxel leaf and turn it into a node
    // should be called only for leaves at full capacity
    void subdivideLeaf(VoxelHandle);

    // collapse a node into a leaf
    //  should be called only when all childs are leaves and the sum of
    //  sub cells is bellow capacity
    void collapseNode(VoxelHandle);

    // get the child bouding box
    AABBox child_aabb(AABBox, int);

    // change the parse order of the nodes to have nearest first along ray
    uint reorder(uint, Vector);

    SparseContainer<C>         v_content_;
    SparseContainer<VoxelLeaf> v_leaves_;
    SparseContainer<VoxelNode> v_nodes_;
    SparseContainer<Voxel>     v_voxels_;

    SparseContainer<ContentHandle> v_ext_content_;
    std::vector<typename SparseContainer<ContentHandle>::handle> v_pos_in_ext_;

    VoxelOverflowContainer v_overlfow;

    unsigned int max_depth_;
};

}
}

#include "Octree.hpp"


#endif // Octree_H
