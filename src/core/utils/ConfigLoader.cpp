#include "ConfigLoader.h"

#include <ork/resource/ResourceTemplate.h>

using namespace ork;

using namespace expressive::core;

using namespace std;

namespace expressive
{

//std::map<std::string, bool> Config::boolParameters;
std::map<std::string, float> Config::floatParameters;
std::map<std::string, std::string> Config::stringParameters;
std::map<std::string, ork::ptr<ork::Object> > Config::objectParameters;

namespace DefaultParams
{

    enum ECamRotationMode {
        E_BasicRotationMode,
        E_TrackballRotationMode,
        E_BlenderLike,              //ToDo
        E_AxisLockedRotationMode
    };

    std::string DEFAULT_GUI_FILE = "../../resources/gui/QmlMainWindow-GL.qml";
    std::string DEFAULT_EXAMPLES_PATH = "../../examples/";

    std::string DEFAULT_CAMERA_MOUSE_MODULE= "mouse";
    std::string DEFAULT_CAMERA_MODULE= "camera";
    std::string DEFAULT_CAMERA_DRAW_METHOD= "cameraMethod";
    std::string DEFAULT_LIGHT_MODULE= "spotlight";
    std::string DEFAULT_LIGHT_DRAW_METHOD= "lightMethod";
#ifdef __APPLE__
    std::string DEFAULT_SCENE_NODE_MODULE= "plastic_MAC";
#else
    std::string DEFAULT_SCENE_NODE_MODULE= "plastic";
#endif
    std::string DEFAULT_SCENE_NODE_DRAW_METHOD= "objectMethod";

    int EXPRESSIVE_MAX_UNDO_STEPS = 10;
    int DEFAULT_MOUSE_ROTATE_MODE = E_AxisLockedRotationMode;

    int GRAPH_DEFAULT_SEGMENT_FACES = 10;
    float GRAPH_DEFAULT_VERTEX_SIZE = 0.7f;
    float GRAPH_DEFAULT_EDGE_SIZE = 0.5f;

#ifdef __APPLE__
    std::string DEFAULT_GRAPH_MODULE= "plastic_MAC";
#else
    std::string DEFAULT_GRAPH_MODULE= "plastic";
#endif

    int GRAPH_DEFAULT_VERTEX_COLOR = 0x00A0EEFF;
    int GRAPH_DEFAULT_EDGE_COLOR = 0x00aeFFFF;

    int POLYGONIZER_MAX_DEPTH = 10;
    float POLYGONIZER_DEFAULT_RESOLUTION = 0.1f;
    float POLYGONIZER_DEFAULT_EPSILON = -1.0f;
    bool USE_MESH_OCTREE = false;
    bool USE_DUAL_CONTOURING = false;

    bool DISABLE_ORK = false;
}

// DO IT WITH XMLLOADER !!
/// @cond RESOURCES

//void Config::setParameter(const std::string &name, bool param)
//{
//    Config::boolParameters[name] = param;
//}

void Config::setParameter(const std::string &name, float param)
{
    Config::floatParameters[name] = param;
}

void Config::setParameter(const std::string &name, const std::string &param)
{
    Config::stringParameters[name] = param;
}

void Config::setParameter(const std::string &name, ork::ptr<ork::Object> param)
{
    Config::objectParameters[name] = param;
}

int Config::getIntParameter(const std::string &name, int default_value)
{
    return (int) Config::getFloatParameter(name, (float) default_value);
}

bool Config::getBoolParameter(const std::string &name, bool default_value)
{
    return Config::getFloatParameter(name, (float) default_value) > 0.5f;
}

float Config::getFloatParameter(const std::string &name, float default_value)
{
    if (Config::floatParameters.find(name) == Config::floatParameters.end()) {
        Config::floatParameters[name] = default_value;
    }
    return Config::floatParameters[name];
}

std::string Config::getStringParameter(const std::string &name, const std::string &default_value)
{
    if (Config::stringParameters.find(name) == Config::stringParameters.end()) {
        Config::stringParameters[name] = default_value;
    }
    return Config::stringParameters[name];
}

ork::ptr<ork::Object> Config::getObjectParameter(const std::string &name, ork::ptr<ork::Object> default_value)
{
    if (Config::objectParameters.find(name) == Config::objectParameters.end()) {
        Config::objectParameters[name] = default_value;
    }
    return Config::objectParameters[name];
}

//const std::map<std::string, bool> &Config::getBoolParameters()
//{
//    return Config::boolParameters;
//}

const std::map<std::string, float> &Config::getFloatParameters()
{
    return Config::floatParameters;
}

const std::map<std::string, std::string> &Config::getStringParameters()
{
    return Config::stringParameters;
}

const std::map<std::string, ork::ptr<ork::Object> > &Config::getObjectParameters()
{
    return Config::objectParameters;
}

void Config::registerParameter(const std::string &name, const std::string &value)
{
    if (value == "true" || value == "false") {
        return setParameter(name, value == "true");
    }
    try {
        float f = std::stof(value);
        return setParameter(name, f);
    } catch(...) {
        return setParameter(name, value);
    }
}

ConfigLoader::ConfigLoader()
    : Serializable("ConfigLoader")
{
}

ConfigLoader::~ConfigLoader()
{
}

TiXmlElement *ConfigLoader::ToXml(const char *) const
{
    assert (false && "This should be coded");
    return nullptr;
}

void ConfigLoader::swap(ork::ptr<ConfigLoader> &/*c*/)
{
}

class ConfigLoaderResource : public ResourceTemplate<1, ConfigLoader>
{
public:
    ConfigLoaderResource(ptr<ResourceManager> manager, const string &name, ptr<ResourceDescriptor> desc, const TiXmlElement *e = NULL) :
        ResourceTemplate<1, ConfigLoader>(manager, name, desc)
    {
        e = e == NULL ? desc->descriptor : e;

//        checkParameters(desc, e, "name,default_mouse_rotate_mode,expressive_max_undo_steps,skeleton_segment_faces,graph_default_vertex_material,graph_default_edge_material,default_scene_node_material,");

        ////////
        /// GLOBAL STUFF
        Config::setParameter("default_gui_file", DefaultParams::DEFAULT_GUI_FILE);

        ////////
        /// SCENE STUFF
        Config::setParameter("default_examples_path", DefaultParams::DEFAULT_EXAMPLES_PATH);
        Config::setParameter("default_camera_mouse_module", DefaultParams::DEFAULT_CAMERA_MOUSE_MODULE);
        Config::setParameter("default_camera_module", DefaultParams::DEFAULT_CAMERA_MODULE);
        Config::setParameter("default_camera_method", DefaultParams::DEFAULT_CAMERA_DRAW_METHOD);
        Config::setParameter("default_light_module", DefaultParams::DEFAULT_LIGHT_MODULE);
        Config::setParameter("default_light_method", DefaultParams::DEFAULT_LIGHT_DRAW_METHOD);
        Config::setParameter("default_scene_node_module", DefaultParams::DEFAULT_SCENE_NODE_MODULE);
        Config::setParameter("default_scene_node_method", DefaultParams::DEFAULT_SCENE_NODE_DRAW_METHOD);

        ////////
        /// SCENE MANIPULATION STUFF
        Config::setParameter("expressive_max_undo_steps", (float) DefaultParams::EXPRESSIVE_MAX_UNDO_STEPS);
        Config::setParameter("default_mouse_rotate_mode", (float) DefaultParams::DEFAULT_MOUSE_ROTATE_MODE);

//        ////////
//        /// GRAPH STUFF
        Config::setParameter("graph_default_segment_faces", (float) DefaultParams::GRAPH_DEFAULT_SEGMENT_FACES);
        Config::setParameter("graph_default_vertex_size", DefaultParams::GRAPH_DEFAULT_VERTEX_SIZE);
        Config::setParameter("graph_default_edge_size", DefaultParams::GRAPH_DEFAULT_EDGE_SIZE);
        Config::setParameter("graph_default_module", DefaultParams::DEFAULT_GRAPH_MODULE);
        Config::setParameter("graph_default_vertex_color", (float) DefaultParams::GRAPH_DEFAULT_VERTEX_COLOR);
        Config::setParameter("graph_default_edge_color", (float) DefaultParams::GRAPH_DEFAULT_EDGE_COLOR);

//        ////////
//        /// POLYGONIZER STUFF
        Config::setParameter("polygonizer_max_depth", (float) DefaultParams::POLYGONIZER_MAX_DEPTH);
        Config::setParameter("polygonizer_default_resolution", DefaultParams::POLYGONIZER_DEFAULT_RESOLUTION);
        Config::setParameter("polygonizer_default_epsilon", DefaultParams::POLYGONIZER_DEFAULT_EPSILON);
        Config::setParameter("use_mesh_octree", DefaultParams::USE_MESH_OCTREE);
        Config::setParameter("use_dual_contouring", DefaultParams::USE_DUAL_CONTOURING);

        Config::setParameter("disable_ork", DefaultParams::DISABLE_ORK);


        const TiXmlAttribute* attribute = e->FirstAttribute();
        while (attribute != nullptr) {
            Config::registerParameter(attribute->NameTStr(), attribute->ValueStr());
            attribute = attribute->Next();
        }
    }
};

extern const char configLoader[] = "config";

static ResourceFactory::Type<configLoader, ConfigLoaderResource> ConfigLoaderType;

/// @endcond


} // namespace expressive
