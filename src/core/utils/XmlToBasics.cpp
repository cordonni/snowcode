
#include<core/utils/XmlToBasics.h>

// std dependencies
#include<sstream>

using namespace std;

namespace expressive {

namespace core {

/** \brief This function initialize a Point from the given Xml element.
     *         The xml must show the following attributes :
     *         dim == Dimension of the point, which is required to match the dimension of Point
     *         x0,x1...xn == coordinate values for the point.
     *
     *    \param element_point A reference to the Xml description of the Point to be created.
     *    \param res Result if everything was right.
     *
     *  \return Return true if the load is a success, false otherwise.
     */
//template<typename PointT>
bool LoadPoint(const TiXmlElement* element_point, Point& res)
{
    int dim = 3; //WARNING : if no dimension attributs "n", assume it is 3
    element_point->QueryIntAttribute( std::string("n"), &dim );
    if(dim != res.rows())
    {
        assert(false);
        std::cout << "ERROR in TraitsXmlLoaderT::LoadPoint : element to load and loader have incompatible dimensions." << std::endl;
        return false;
    }

    bool test = true;
    switch (res.rows())
    {
    case 2 :
        test = ( (element_point->QueryScalarAttribute("x",&(res[0])) != TIXML_NO_ATTRIBUTE)
                && (element_point->QueryScalarAttribute("y",&(res[1])) != TIXML_NO_ATTRIBUTE) ) ;
        break;
    case 3 :
        test = ( (element_point->QueryScalarAttribute("x",&(res[0])) != TIXML_NO_ATTRIBUTE)
                && (element_point->QueryScalarAttribute("y",&(res[1])) != TIXML_NO_ATTRIBUTE)
                && (element_point->QueryScalarAttribute("z",&(res[2])) != TIXML_NO_ATTRIBUTE) ) ;
        break;
    case 4 :
        test = ( (element_point->QueryScalarAttribute("x",&(res[0])) != TIXML_NO_ATTRIBUTE)
                && (element_point->QueryScalarAttribute("y",&(res[1])) != TIXML_NO_ATTRIBUTE)
                && (element_point->QueryScalarAttribute("z",&(res[2])) != TIXML_NO_ATTRIBUTE)
                && (element_point->QueryScalarAttribute("w",&(res[3])) != TIXML_NO_ATTRIBUTE) ) ;
        break;
    default :
    {
        std::ostringstream name;
        for(int i = 0; i<res.rows(); ++i)
        {
            char n[5];
            sprintf(n, "_%d", i);
            test = test
                    && ( element_point->QueryScalarAttribute(n,&(res[i]))
                         != TIXML_NO_ATTRIBUTE);
            //                    res[i] = (typename Point::value_type) x_i;
        }
    }
    }
    if (!test) { // second chance with a name equal to "_0", "_1", etc...
        test = true;
        for(int i = 0; i<res.rows(); ++i)
        {
            char n[5];
            sprintf(n, "_%d", i);
            test = test
                    && ( element_point->QueryScalarAttribute(n,&(res[i]))
                         != TIXML_NO_ATTRIBUTE);
            //                    res[i] = (typename Point::value_type) x_i;
        }
    }

    if(!test)
    {
        std::cout << "ERROR in TraitsXmlLoaderT::LoadPoint : coordinate attribute not found." << std::endl;
        assert(false);
        return false;
    }
    else return true;
}

} // Close namespace core

} // Close namespace expressive

