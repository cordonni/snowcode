#include "core/ExpressiveTraits.h"

namespace expressive {

//TODO:@todo : a changer par un array ...
    // Define default properties
    const ExpressiveTraits3d::Scalar ExpressiveTraits3d::kSFScalarPropertiesDefault[ExpressiveTraits3d::SF_SCALAR_PROPERTIES_CARDINAL]
                                                                            = {
                                                                                ExpressiveTraits3d::Scalar(0.0),  // E_Red
                                                                                ExpressiveTraits3d::Scalar(0.0),  // E_Green
                                                                                ExpressiveTraits3d::Scalar(0.0)   // E_Blue
                                                                              };
    const ExpressiveTraits3d::Vector ExpressiveTraits3d::kSFVectorPropertiesDefault[SF_VECTOR_PROPERTIES_CARDINAL]
                                                                            = {
                                                                                ExpressiveTraits3d::Vector::Zero() // E_Direction
                                                                              };
    // Correspondance for naming the properties
    std::vector<std::string> ExpressiveTraits3d::kScalarPropertiesToName = ExpressiveTraits3d::Init_kScalarPropertiesToName();
    std::map<std::string, ExpressiveTraits3d::ESFScalarProperties> ExpressiveTraits3d::kNameToScalarProperties = ExpressiveTraits3d::Init_kNameToScalarProperties();
    std::vector<std::string> ExpressiveTraits3d::kVectorPropertiesToName = ExpressiveTraits3d::Init_kVectorPropertiesToName();
    std::map<std::string, ExpressiveTraits3d::ESFVectorProperties> ExpressiveTraits3d::kNameToVectorProperties = ExpressiveTraits3d::Init_kNameToVectorProperties();

    // Initialize vectors containing all properties. For optimization.
    // This is useful to use for functions which need a vector with a subset of those properties ID.
    // for Scalar
    std::vector<ExpressiveTraits3d::ESFScalarProperties> ExpressiveTraits3d::Init_kAllScalarProperties()
    {
        std::vector<ExpressiveTraits3d::ESFScalarProperties> res(ExpressiveTraits3d::SF_SCALAR_PROPERTIES_CARDINAL);
        for(unsigned long i = 0; i<ExpressiveTraits3d::SF_SCALAR_PROPERTIES_CARDINAL; ++i)
        {
            res[i] = static_cast<ExpressiveTraits3d::ESFScalarProperties>(i);
        }
        return res;
    }
    const std::vector<ExpressiveTraits3d::ESFScalarProperties> ExpressiveTraits3d::kAllScalarProperties = ExpressiveTraits3d::Init_kAllScalarProperties();
    // For vector
    std::vector<ExpressiveTraits3d::ESFVectorProperties> ExpressiveTraits3d::Init_kAllVectorProperties()
    {
        std::vector<ExpressiveTraits3d::ESFVectorProperties> res(ExpressiveTraits3d::SF_VECTOR_PROPERTIES_CARDINAL);
        for(unsigned long i = 0; i<ExpressiveTraits3d::SF_VECTOR_PROPERTIES_CARDINAL; ++i)
        {
            res[i] = static_cast<ExpressiveTraits3d::ESFVectorProperties>(i);
        }
        return res;
    }
    const std::vector<ExpressiveTraits3d::ESFVectorProperties> ExpressiveTraits3d::kAllVectorProperties = ExpressiveTraits3d::Init_kAllVectorProperties();
    // ------------------------------------------------------------------------

    const std::string & ExpressiveTraits3d::getNameFromScalarProperty(ESFScalarProperties prop)
    {
        assert(kScalarPropertiesToName[prop].length() != 0); // this is a clue that you did not registered
        return kScalarPropertiesToName[prop];
    }

    const ExpressiveTraits3d::ESFScalarProperties& ExpressiveTraits3d::getScalarPropertyFromName(const std::string& prop_name)
    {
        assert(kNameToScalarProperties.find(prop_name) != kNameToScalarProperties.end());
        return kNameToScalarProperties.find(prop_name)->second;
    }

    const std::string& ExpressiveTraits3d::getNameFromVectorProperty(ExpressiveTraits3d::ESFVectorProperties prop)
    {
        assert(kVectorPropertiesToName[prop].length() != 0); // this is a clue that you did not registered
        return kVectorPropertiesToName[prop];
    }
    const ExpressiveTraits3d::ESFVectorProperties& ExpressiveTraits3d::getVectorPropertyFromName(const std::string& prop_name)
    {
        assert(kNameToVectorProperties.find(prop_name) != kNameToVectorProperties.end());
        return kNameToVectorProperties.find(prop_name)->second;
    }

    std::vector<std::string> ExpressiveTraits3d::Init_kScalarPropertiesToName(){
        std::vector<std::string> res(SF_SCALAR_PROPERTIES_CARDINAL);
        res[E_Red]   = "red";
        res[E_Green] = "green";
        res[E_Blue]  = "blue";
        return res;
    }
    std::map<std::string, ExpressiveTraits3d::ESFScalarProperties> ExpressiveTraits3d::Init_kNameToScalarProperties(){
        std::map<std::string, ESFScalarProperties> res;
        res.insert(std::pair<std::string, ESFScalarProperties>("red"  , E_Red  ));
        res.insert(std::pair<std::string, ESFScalarProperties>("green", E_Green));
        res.insert(std::pair<std::string, ESFScalarProperties>("blue" , E_Blue ));
        return res;
    }
    std::vector<std::string> ExpressiveTraits3d::Init_kVectorPropertiesToName()
    {
        std::vector<std::string> res(SF_VECTOR_PROPERTIES_CARDINAL);
        res[E_Direction]   = "direction";
        return res;
    }
    std::map<std::string, ExpressiveTraits3d::ESFVectorProperties> ExpressiveTraits3d::Init_kNameToVectorProperties(){
        std::map<std::string, ESFVectorProperties> res;
        res.insert(std::pair<std::string, ESFVectorProperties>("direction"  , E_Direction ));
        return res;
    }

} // Close namespace expressive



