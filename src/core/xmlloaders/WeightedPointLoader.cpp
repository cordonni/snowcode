#include <core/xmlloaders/WeightedPointLoader.h>

#include <core/utils/XmlToBasics.h>

#include <core/xmlloaders/GeometryPrimitiveLoader.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

using namespace expressive::core;
using namespace std;

namespace expressive {

namespace core {

void WeightedPointLoader::Load(const TiXmlElement *e, WeightedPoint &wp)
{
    ///TODO:@todo : FOR NOW THIS FUNCTION INITIALIZE A WEIGHTED POINT FROM A <SkelBasicVertexHandle>

    TraitsXmlQuery::QueryPointAttribute("SkelBasicVertexHandleXmlLoaderT",e,"position",wp);     //TODO:@todo : *this is a WeightedPoint that derive from Point so it should be ok ...

    Scalar weight = 0.0;
    TraitsXmlQuery::QueryScalarAttribute("SkelBasicVertexHandleXmlLoaderT",e,"weight",&weight);

    //TODO:@todo : this is to load data with StandardKernel from Convol, to be removed as soon as as convol is completely rebuild in expressive
    double normalization_factor = -1.0;
    if(e->QueryDoubleAttribute("n_factor",&normalization_factor) == TIXML_NO_ATTRIBUTE)
        wp.set_weight(weight);
    else
        wp.set_weight(weight*normalization_factor);

//    int id;
//    TraitsXmlQuery::QueryIntAttribute("SkelBasicVertexHandleXmlLoaderT",element_vh,"id",&id);

//    map_id_skel_objects.insert(std::pair<unsigned int,SkeletonElement*>((unsigned int) id, vh_added));
}


std::shared_ptr<WeightedPoint> WeightedPointLoader::Create(const TiXmlElement *e, std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex)
{
    //TODO:@todo : this is a temporary solution waiting for final choice on Point primitive representation in the graph.
    if(e->ValueStr().compare("NodePointSField") == 0) {
        WeightedPoint wp;
        WeightedPointLoader::Load(e, wp);
        return std::make_shared<WeightedPoint>(wp);
    }else{
        // VertexHandle 0
        int id = -1;
        TraitsXmlQuery::QueryIntAttribute("WeightedPointLoader", e, "id_vh0", &id);
        auto it = map_id_vertex.find((unsigned long) id);
        if(it == map_id_vertex.end())
        {
            std::cout << "ERROR in WeightedPointLoader::CreateFromXml : the following id is invalid : " << id << std::endl;
            throw std::exception();
        }
        //    return std::make_shared<WeightedSegment>(it_vh0->second->pos(),it_vh1->second->pos());
        return it->second->nonconst_posptr();
    }
}

extern const char weighted_point[] = "WeightedPoint";
static GeometryPrimitiveLoader::XmlType<weighted_point, WeightedPointLoader> WeightedPointType;


//TODO:@todo : this kind of stuff should be done if we introduce a more general PointLoader that can load all kind of Point
////extern const char weighted_point[] = "WeightedPoint";
//extern const char skel_basic_vertex_handle[] = "SkelBasicVertexHandle";

//static ResourceFactory::Type<skel_basic_vertex_handle, WeightedPointResource> WeightedPointType;

} // Close namespace core

} // Close namespace expressive

