/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: WeightedPointLoader.h

   Language: C++

   License: Convol license

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Header file for loading a Skeleton from a TiXmlElement.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_WEIGHTED_POINT_LOADER_H_
#define CONVOL_WEIGHTED_POINT_LOADER_H_

#include <core/geometry/WeightedPoint.h>

#include <core/graph/Graph.h>

namespace expressive {

namespace core {

class CORE_API WeightedPointLoader
{
public:
    static void Load(const TiXmlElement *e, WeightedPoint &wp);

    static std::shared_ptr<WeightedPoint> Create(const TiXmlElement *e,
                                                          std::map<unsigned long, std::shared_ptr<core::WeightedVertex>>& map_id_vertex);
};

} // Close namespace core

} // Close namespace expressive


//#include <ork/resource/ResourceTemplate.h>

//namespace expressive {

//namespace convol {

///// @cond RESOURCES

//class WeightedPointResource : public ResourceTemplate<50, core::WeightedPoint>
//{
//public:
//    WeightedPointResource(std::shared_ptr<ResourceManager> manager, const std::string &name, std::shared_ptr<ResourceDescriptor> desc, const TiXmlElement *e = nullptr);
//};

///// @endcond

//} // Close namespace core

//} // Close namespace expressive

#endif // CONVOL_WEIGHTED_POINT_LOADER_H_

