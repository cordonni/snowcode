#include <core/xmlloaders/DefaultWeightedGeometryLoader.h>

#include <core/xmlloaders/GeometryPrimitiveLoader.h>

// Header file of geometry primitives to be registered inside the GeometryPrimitiveFactory
#include<core/geometry/SubdivisionCurve.h>
#include<core/geometry/WeightedTriangle.h>

namespace expressive {

namespace core {

extern const char weighted_triangle[] = "WeightedTriangle";
static GeometryPrimitiveLoader::XmlType<weighted_triangle, DefaultWeightedGeometryLoader<WeightedTriangle> > WeightedTriangleType;

extern const char weighted_subdivision_curve[] = "SubdivisionCurve";
static GeometryPrimitiveLoader::XmlType<weighted_subdivision_curve, DefaultWeightedGeometryLoader<SubdivisionCurveT<WeightedPoint> > > WeightedSubdivisionCurveType;


} // Close namespace core

} // Close namespace expressive


