/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_GEOMETRY_PRIMITIVE_LOADER_H_
#define EXPRESSIVE_GEOMETRY_PRIMITIVE_LOADER_H_

#include <core/geometry/GeometryPrimitive.h>
#include <core/geometry/WeightedPoint.h>

#include <core/graph/Graph.h>

//////////////////////////////////////////////////////////////////////////////////////////
///TODO:@todo : this class should probably be templated by the kind of point !
//////////////////////////////////////////////////////////////////////////////////////////

namespace expressive {

namespace core {

/**
 * @brief The GeometryPrimitiveLoader class
 * This factory is used to create GeometryPrimitives from data contained
 * in a TiXmlElement.
 * Types are defined as templates, which take two parameters : The first one
 * is a string corresponding to a Xml tag, the second one is a loader class
 * for a given GeometryPrimitive.
 * Note that the loader class must have the following function available :
 * static std::shared_ptr<T> CreateFromXml(const TiXmlElement *e, std::map<unsigned long, std::shared_ptr<Vertex>>& map_id_vertex);
 */
class CORE_API GeometryPrimitiveLoader
{
public:
//    typedef GeometryPrimitiveT<WeightedPoint> WeightedGeometryPrimitive;

    typedef std::shared_ptr<Primitive> (*createFuncXml) (const TiXmlElement *e,
                                                                      std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex);

//    typedef std::shared_ptr<WeightedGeometryPrimitive> (*createFunc) ();

    static GeometryPrimitiveLoader *GetInstance();

    void AddType(const std::string &type, createFuncXml f);

//    void AddType(const std::string &type, createFunc f);

    std::shared_ptr<Primitive> Create(const TiXmlElement *e,
                                                      std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex);

    std::shared_ptr<Primitive> Create(const std::string& name,
                                                      const TiXmlElement* e,
                                                      std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex);

//    std::shared_ptr<Primitive> Create(const std::string &name);

    template <const char* t, class T>
    class XmlType
    {
    public:
        static std::shared_ptr<Primitive> ctor (const TiXmlElement *e, std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex)
        {
            return T::Create(e, map_id_vertex);
        }

        XmlType()
        {
            GeometryPrimitiveLoader::GetInstance()->AddType(std::string(t), ctor);
        }
    };

//    template <const char* t, class T>
//    class Type
//    {
//    public:
//        static std::shared_ptr<WeightedGeometryPrimitive> ctor ()
//        {
//            return T::Create();
//        }

//        Type()
//        {
//            GeometryPrimitiveLoader::GetInstance()->AddType(std::string(t), ctor);
//        }
//    };

protected:
    std::map<std::string, createFuncXml> xmlTypes;

//    std::map<std::string, createFunc> types;
};


} // namespace core

} // namespace expressive

#endif // EXPRESSIVE_GEOMETRY_PRIMITIVE_LOADER_H_
