#include <core/xmlloaders/GeometryPrimitiveLoader.h>

#include <ork/core/Logger.h>

using namespace ork;

namespace expressive {

namespace core {

GeometryPrimitiveLoader *GeometryPrimitiveLoader::GetInstance()
{
    static GeometryPrimitiveLoader INSTANCE = GeometryPrimitiveLoader();
    return &INSTANCE;
}

//void GeometryPrimitiveLoader::AddType(const std::string &type, createFunc f)
//{
//    types[type] = f;
//}


void GeometryPrimitiveLoader::AddType(const std::string &type, createFuncXml f)
{
    xmlTypes[type] = f;
}

auto GeometryPrimitiveLoader::Create(const TiXmlElement* e, std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex) -> std::shared_ptr<Primitive>
{
    auto it = xmlTypes.find(e->ValueStr());

    if (it != xmlTypes.end()) {
        return it->second(e, map_id_vertex);
    } else {
        if (Logger::ERROR_LOGGER != nullptr) {
            Logger::ERROR_LOGGER->log("CORE", "Unknown GeometryPrimitive xml descriptor '" + e->ValueStr() + "'");
        }
        throw std::exception();
    }
}


auto GeometryPrimitiveLoader::Create(const std::string& name, const TiXmlElement* e, std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex) -> std::shared_ptr<Primitive>
{
    auto it = xmlTypes.find(name);

    if (it != xmlTypes.end()) {
        return it->second(e, map_id_vertex);
    } else {
        if (Logger::ERROR_LOGGER != nullptr) {
            Logger::ERROR_LOGGER->log("CORE", "Unknown GeometryPrimitive xml descriptor '" + name + "'");
        }
        throw std::exception();
    }
}


//auto GeometryPrimitiveLoader::Create(const std::string &name) -> std::shared_ptr<Primitive>
//{
//    auto it = types.find(name);

//    if (it != types.end()) {
//        return it->second();
//    } else {
//        if (Logger::ERROR_LOGGER != nullptr) {
//            Logger::ERROR_LOGGER->log("CORE", "Unknown GeometryPrimitive descriptor '" + name + "'");
//        }
//        throw std::exception();
//    }
//}

} // namespace core

} // namespace expressive
