/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: DefaultWeightedGeometryLoader

   Language: C++

   License: Convol license

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Header file for loading a WeightedGeometry from a TiXmlElement.
                It uses a factory to create it

   Platform Dependencies: None
*/

#pragma once
#ifndef EXPRESSIVE_DEFAULT_WEIGHTED_GEOMETRY_LOADER_H_
#define EXPRESSIVE_DEFAULT_WEIGHTED_GEOMETRY_LOADER_H_

#include <core/graph/Graph.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

#include <exception>

namespace expressive {

namespace core {

template<typename TWeightedGeometry>
class DefaultWeightedGeometryLoader
{

public:
    static std::shared_ptr<TWeightedGeometry> Create(const TiXmlElement *e,
                                                     std::map<unsigned long, std::shared_ptr<core::WeightedVertex>>& map_id_vertex)
    {
        static std::string loader_name = TWeightedGeometry::StaticName()+"XmlLoader";

        std::vector<WeightedPoint> vec_pos;
        int i=0 , id_vh;
        std::ostringstream name;
        name << "id_vh" << i;
        while(e->QueryIntAttribute(name.str().c_str(), &id_vh) != TIXML_NO_ATTRIBUTE)
        {
            auto it_vh = map_id_vertex.find((unsigned long) id_vh);
            if(it_vh == map_id_vertex.end())
            {
                std::cout << "ERROR in " << loader_name << "::CreateFromXml : the following id_vh is invalid : " << id_vh << std::endl;
                throw std::exception();
            }

            vec_pos.push_back(it_vh->second->pos());

            ++i;
            name.str("");
            name << "id_vh" << i;
        }

        auto w_geom = std::make_shared<TWeightedGeometry>(vec_pos);
        w_geom->InitFromXml(e);

        return w_geom;
    }
};

} // Close namespace core

} // Close namespace expressive

#endif // EXPRESSIVE_DEFAULT_WEIGHTED_GEOMETRY_LOADER_H_

