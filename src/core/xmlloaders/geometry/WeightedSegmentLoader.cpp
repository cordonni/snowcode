#include <core/xmlloaders/geometry/WeightedSegmentLoader.h>

#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/GeometryPrimitiveLoader.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

#include <exception>

namespace expressive {

namespace core {

std::shared_ptr<WeightedSegment> WeightedSegmentLoader::Create(const TiXmlElement *e, std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex)
{
    // VertexHandle 0
    int id_vh0 = -1;
    TraitsXmlQuery::QueryIntAttribute("WeightedSegmentLoader", e, "id_vh0", &id_vh0);
    auto it_vh0 = map_id_vertex.find((unsigned long) id_vh0);
    if(it_vh0 == map_id_vertex.end())
    {
        std::cout << "ERROR in WeightedSegmentLoader::CreateFromXml : the following id_vh0 is invalid : " << id_vh0 << std::endl;
        throw std::exception();
    }

    // SkelVertexHandle 1
    int id_vh1 = -1;
    TraitsXmlQuery::QueryIntAttribute("WeightedSegmentXmlLoader", e, "id_vh1", &id_vh1);
    auto it_vh1 = map_id_vertex.find((unsigned long) id_vh1);
    if(it_vh1 == map_id_vertex.end())
    {
        std::cout << "ERROR in WeightedSegmentLoader::CreateFromXml : the following id_vh1 is invalid : " << id_vh1 << std::endl;
        throw std::exception();
    }

    return std::make_shared<WeightedSegment>(it_vh0->second->pos(),it_vh1->second->pos());
}

extern const char weighted_segment[] = "WeightedSegment";
static GeometryPrimitiveLoader::XmlType<weighted_segment, WeightedSegmentLoader> WeightedSegmentType;

} // Close namespace core

} // Close namespace expressive


