#include <core/xmlloaders/geometry/WeightedTriangleLoader.h>

#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/GeometryPrimitiveLoader.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

#include <exception>

namespace expressive {

namespace core {

std::shared_ptr<WeightedTriangle> WeightedTriangleLoader::Create(const TiXmlElement *e, std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex)
{
    // VertexHandle 0
    int id_vh0 = -1;
    TraitsXmlQuery::QueryIntAttribute("WeightedTriangleLoader", e, "id_vh0", &id_vh0);
    auto it_vh0 = map_id_vertex.find((unsigned long) id_vh0);
    if(it_vh0 == map_id_vertex.end())
    {
        std::cout << "ERROR in WeightedTriangleLoader::CreateFromXml : the following id_vh0 is invalid : " << id_vh0 << std::endl;
        throw std::exception();
    }

    // SkelVertexHandle 1
    int id_vh1 = -1;
    TraitsXmlQuery::QueryIntAttribute("WeightedTriangleXmlLoader", e, "id_vh1", &id_vh1);
    auto it_vh1 = map_id_vertex.find((unsigned long) id_vh1);
    if(it_vh1 == map_id_vertex.end())
    {
        std::cout << "ERROR in WeightedTriangleLoader::CreateFromXml : the following id_vh1 is invalid : " << id_vh1 << std::endl;
        throw std::exception();
    }

    // SkelVertexHandle 2
    int id_vh2 = -1;
    TraitsXmlQuery::QueryIntAttribute("WeightedTriangleXmlLoader", e, "id_vh2", &id_vh2);
    auto it_vh2 = map_id_vertex.find((unsigned long) id_vh2);
    if(it_vh2 == map_id_vertex.end())
    {
        std::cout << "ERROR in WeightedTriangleLoader::CreateFromXml : the following id_vh2 is invalid : " << id_vh2 << std::endl;
        throw std::exception();
    }

    //TODO:@todo : argh :
    std::vector<WeightedPoint> vec_point;
    vec_point.push_back(it_vh0->second->pos());
    vec_point.push_back(it_vh1->second->pos());
    vec_point.push_back(it_vh2->second->pos());
    return std::make_shared<WeightedTriangle>(vec_point);
//    return std::make_shared<WeightedTriangle>(it_vh0->second->pos(),it_vh1->second->pos(), it_vh2->second->pos());
}

extern const char weighted_triangle_old[] = "WeightedTriangle";
static GeometryPrimitiveLoader::XmlType<weighted_triangle_old, WeightedTriangleLoader> WeightedTriangleType;

} // Close namespace core

} // Close namespace expressive


