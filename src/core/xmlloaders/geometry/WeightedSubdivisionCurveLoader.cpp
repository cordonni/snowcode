#include <core/xmlloaders/geometry/WeightedSubdivisionCurveLoader.h>

#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/GeometryPrimitiveLoader.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

#include <exception>

namespace expressive {

namespace core {

auto WeightedSubdivisionCurveLoader::Create(const TiXmlElement *e, std::map<unsigned long, std::shared_ptr<Vertex>>& map_id_vertex)
-> std::shared_ptr<WeightedSubdivisionCurve>
{
    // Get the number of points
    int nb_points = -1;
    TraitsXmlQuery::QueryIntAttribute("WeightedSubdivisionCurveLoader", e, "nb_points", &nb_points);

    std::vector<WeightedPoint> vec_pos;
    std::ostringstream name;
    for(int i = 0; i < nb_points; ++i)
    {
        name.str("");
        name << "id_vh" << i;

        int id_vh = -1;
        TraitsXmlQuery::QueryIntAttribute("WeightedSubdivisionCurveLoader", e, name.str().c_str(), &id_vh);

        auto it_vh = map_id_vertex.find((unsigned long) id_vh);
        if(it_vh == map_id_vertex.end())
        {
            std::cout << "ERROR in WeightedSubdivisionCurveLoader::CreateFromXml : the following id_vh is invalid : " << id_vh << std::endl;
            throw std::exception();
        }

        vec_pos.push_back(it_vh->second->pos());
    }

    auto w_curve = std::make_shared<WeightedSubdivisionCurve>(vec_pos);

    int nb_subdiv = 0;
    TraitsXmlQuery::QueryIntAttribute("WeightedSubdivisionCurveLoader", e, "nb_subdiv", &nb_subdiv);
    w_curve->set_nb_subdiv(nb_subdiv);

    return w_curve;

}

extern const char weighted_subdivision_curve[] = "WeightedSubdivisionCurve";
static GeometryPrimitiveLoader::XmlType<weighted_subdivision_curve, WeightedSubdivisionCurveLoader> WeightedSubdivisionCurveType;

} // Close namespace core

} // Close namespace expressive


