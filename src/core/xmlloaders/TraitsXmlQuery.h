/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: MACRO_QUERY_ATTRIBUTE.h
 
 Language: C++
 
 License: Convol license
 
 \author: Cedric Zanni
 E-Mail: cedric.zanni@inrialpes.fr
 \author: Maxime Quiblier
 E-Mail: maxime.quiblier@inrialpes.fr
 \author: Amaury Jung
 E-Mail: amaury.jung@inrialpes.fr
 
 Description: Some macro to load element from XmlElement.
 
 Platform Dependencies: None
 */


#pragma once
#ifndef MACRO_QUERY_ATTRIBUTE_H_
#define MACRO_QUERY_ATTRIBUTE_H_

// core dependencies
#include<core/CoreRequired.h>
    // utils
    #include<core/utils/XmlToBasics.h>
    // tinyxml dependencies
    #include<tinyxml/tinyxml.h>

// std dependencies
#include <string>
#include <exception>

namespace expressive {

namespace core {

class TraitsXmlQuery 
{
public:

    static const TiXmlElement* QueryFirstChildElement(const char* classname, const TiXmlElement* elmt, const char* str = nullptr)
    {
        const TiXmlElement* new_element;

        if(str == nullptr)
            new_element = elmt->FirstChildElement();
        else
            new_element = elmt->FirstChildElement(str);

        if(new_element == nullptr)
        {
            std::cout << "ERROR in " << classname << "::Load : " << str << " child not found." << std::endl;
            throw std::exception();
        }
        return new_element;
    }

    static void QueryScalarAttribute(const char* classname, const TiXmlElement* elmt, const char* str, Scalar *ptr)
    {
        if(elmt->QueryScalarAttribute(str, ptr) == TIXML_NO_ATTRIBUTE)
        {
            std::cout << "ERROR in " << classname << "::Load : " << str << " attribute not found." << std::endl;
            throw std::exception();
        }
    }

    static void QueryDoubleAttribute(const char* classname, const TiXmlElement* elmt, const char* str, double *ptr)
    {
        if(elmt->QueryDoubleAttribute(str, ptr) == TIXML_NO_ATTRIBUTE)
        {
            std::cout << "ERROR in " << classname << "::Load : " << str << " attribute not found." << std::endl;
            throw std::exception();
        }
    }

    static void QueryIntAttribute(const char *classname, const TiXmlElement *elmt, const char *str, int *ptr)
    {
        if(elmt->QueryIntAttribute(str, ptr) == TIXML_NO_ATTRIBUTE)
        {
            std::cout << "ERROR in " << classname << "::Load : " << str << " attribute not found." << std::endl;
            throw std::exception();
        }
    }

    static void QueryPointAttribute(const char *classname, const TiXmlElement *elmt, const char *str, Point& point)
    {
        if(!QueryPoint(elmt, str, point))
        {
            std::cout << "ERROR in " << classname << "::Load : " << str << " attribute not found." << std::endl;
            throw std::exception();
        }
    }


//    static bool QueryVector(TiXmlElement* parent_element, std::string vector_name, Vector& vector_res)
//    {
//        TiXmlElement* element_vector = parent_element->FirstChildElement(vector_name);
//        return TraitsXmlLoaderT<Traits>::LoadVector(element_vector, vector_res);
//    }

    static bool QueryPoint(const TiXmlElement* parent_element, std::string point_name, Point& point_res)
    {
        const TiXmlElement* element_point = parent_element->FirstChildElement(point_name);
        return core::LoadPoint(element_point, point_res);
    }

//    static bool QueryNormal(TiXmlElement* parent_element, std::string normal_name, Normal& normal_res)
//    {
//        TiXmlElement* element_normal = parent_element->FirstChildElement(normal_name);
//        return LoadNormal(element_normal, normal_res);
//    }

};

} // Close namespace core

} // Close namespace expressive

#endif
