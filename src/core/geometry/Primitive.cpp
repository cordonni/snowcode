#include <core/geometry/Primitive.h>

namespace expressive
{

namespace core
{

//template<class BasePointT>
TiXmlElement *Primitive/*T<BasePointT>*/::ToXml(const char *name) const
{
    TiXmlElement * element;
    if(name == NULL)
        element = new TiXmlElement( this->Name() );
    else
        element = new TiXmlElement( name );

    AddXmlAttributes(element);
    return element;
}

}

}
