#define TEMPLATE_INSTANTIATION_POINTPRIMITIVE
#include "core/geometry/PointPrimitive.h"

namespace expressive
{

namespace core
{

// explicit instantiation
template class core::PointPrimitiveT<expressive::Point>;
template class core::PointPrimitiveT<expressive::Point2>;
template class core::PointPrimitiveT<expressive::Point4>;

}

}
