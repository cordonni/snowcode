/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef POLYGON2D_H
#define POLYGON2D_H

#include "core/CoreRequired.h"
#include "core/geometry/AABBox.h"

#include <vector>


namespace expressive
{

namespace core
{

/**
 * A basic 2D polygon with easy creation functions.
 * [DEPRECATED] This class should probably be moved into Subdivision Curve.
 */
class CORE_API Polygon2D
{
public:

    EXPRESSIVE_MACRO_NAME("Polygon2D")

    Polygon2D(){}
    Polygon2D(std::vector<Vector2> vertices);
    Polygon2D(const Polygon2D& to_copy) : m_vertices(to_copy.vertices()), bounding_box_(to_copy.bounding_box_){}

    virtual ~Polygon2D() {}

    const std::vector<Vector2>& vertices() const {return m_vertices;}
    std::vector<Vector2>& vertices() {return m_vertices;}

    void add_vertex(const Vector2 &v);

    inline uint nb_vert() const {return (uint) m_vertices.size();}

    AABBox2D bounding_box() const;

    double area() const;

    double minX() const;

    double maxX() const;

    double minY() const;

    double maxY() const;

    bool contains(const Vector2& v) const;

    static Polygon2D Ngon(const uint N);
    inline static Polygon2D triangle() { return Ngon(3); }
    inline static Polygon2D square()   { return Ngon(4); }
    inline static Polygon2D pentagon() { return Ngon(5); }
    inline static Polygon2D hexagon()  { return Ngon(6); }


    inline static std::shared_ptr<Polygon2D> make_Ngon(const uint N) { return std::make_shared<Polygon2D>(Ngon(N)); }
    inline static std::shared_ptr<Polygon2D> make_triangle() { return std::make_shared<Polygon2D>(triangle()); }
    inline static std::shared_ptr<Polygon2D> make_square()   { return std::make_shared<Polygon2D>(square()); }
    inline static std::shared_ptr<Polygon2D> make_pentagon() { return std::make_shared<Polygon2D>(pentagon()); }
    inline static std::shared_ptr<Polygon2D> make_hexagon()  { return std::make_shared<Polygon2D>(hexagon()); }

private:
    std::vector<Vector2> m_vertices;

    AABBox2D bounding_box_;
};

}

}


#endif // POLYGON2D_H
