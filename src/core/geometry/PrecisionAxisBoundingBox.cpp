#define TEMPLATE_INSTANTIATION_PRECISIONAXISBOUNDINGBOX

#include "core/geometry/PrecisionAxisBoundingBox.h"

namespace expressive {

namespace core {

// explicit instantiation
template class PrecisionAxisBoundingBoxT<expressive::Point>;

}

}
