
#include<core/geometry/WeightedTriangle.h>

namespace expressive {

namespace core {

// Convenience functions, should not be used on non linear triangle
void WeightedTriangle::set_weights(Scalar w1, Scalar w2, Scalar w3)
{
    points_[0].set_weight(w1);
    points_[1].set_weight(w2);
    points_[2].set_weight(w3);

    ComputeHelpVariables();
}
void WeightedTriangle::set_weight_p1(Scalar w1)
{
    points_[0].set_weight(w1);

    ComputeHelpVariables();
}
void WeightedTriangle::set_weight_p2(Scalar w2)
{
    points_[1].set_weight(w2);

    ComputeHelpVariables();
}
void WeightedTriangle::set_weight_p3(Scalar w3)
{
    points_[2].set_weight(w3);

    ComputeHelpVariables();
}

///////////////////
// Xml functions //
///////////////////

TiXmlElement * WeightedTriangle::ToXml(bool embed_vertices, const char *name) const
{
    TiXmlElement * element = TriangleT<WeightedPoint>::ToXml(embed_vertices, name);

    if (embed_vertices) {
        element->SetDoubleAttribute("weight_p1",points_[0].weight());
        element->SetDoubleAttribute("weight_p2",points_[1].weight());
        element->SetDoubleAttribute("weight_p3",points_[2].weight());
    }
    return element;
}

///////////////
// Accessors //
///////////////

// Help variables for divers computation on the triangle.
// This will be used to improve computation speed in divers cases.
void WeightedTriangle::ComputeHelpVariables()
{
    TriangleT<WeightedPoint>::ComputeHelpVariables();

    opt_w_tri_.set_parameters(*this);
}

} // Close namespace core

} // Close namespace expressive

