/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: OptWeightedTriangle.h

   Language: C++

   License: Expressive license

   \author: Cedric Zanni
   E-Mail:  cedric.zanni@inria.fr

   Description: Header for linearly weighted triangle which information are optimized for 
		some computation (convolution surfaces for instance).

   Platform Dependencies: None
*/

#pragma once
#ifndef OPT_WEIGHTED_TRIANGLE_H_
#define OPT_WEIGHTED_TRIANGLE_H_

// core dependencies
#include<core/CoreRequired.h>
    #include <core/geometry/PrecisionAxisBoundingBox.h>

#include <list>

namespace expressive {

namespace core {

// geometry
class WeightedTriangle; //#include<core/geometry/WeightedTriangle.h>

class CORE_API OptWeightedTriangle
{
public:

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    OptWeightedTriangle();

    /*! \brief Build a OptWeightedSegment from a classical one.
     */
    OptWeightedTriangle(const WeightedTriangle &w_tri);

    // Copy constructor
    OptWeightedTriangle(const OptWeightedTriangle &rhs);


    virtual ~OptWeightedTriangle();

    Point Barycenter() const;

    virtual void GetPrecisionAxisBoundingBoxes(Scalar /*value*/, std::list<core::PrecisionAxisBoundingBox> &pboxes) const;

    /////////////////
    /// Modifiers ///
    /////////////////
 
    void set_parameters(const WeightedTriangle &rhs);

    /////////////////
    /// Accessors ///
    /////////////////

    // Accessors to Help variables
    inline const Point& point_min() const { return point_min_; }
    inline Scalar weight_min()const { return weight_min_; }
    inline const Normal& main_dir()   const { return main_dir_; }
    inline const Normal& ortho_dir()  const { return ortho_dir_; }
    inline Scalar unit_delta_weight() const { return unit_delta_weight_; }
    inline Scalar coord_max()       const { return coord_max_; }
    inline Scalar coord_middle()    const { return coord_middle_; }
    inline Scalar max_seg_length()  const { return max_seg_length_; }
    inline const Vector& longest_dir()const { return longest_dir_; }
    inline const Vector& half_dir_1() const { return half_dir_1_; }
    inline const Vector& half_dir_2() const { return half_dir_2_; }
    inline const Point& point_half()  const { return point_half_; }
    inline const Vector& longest_dir_special() const { return longest_dir_special_; }
    inline const Normal& unit_normal() const { return unit_normal_; }

    ///////////////
    // Attributs //
    ///////////////

protected:
    // Convenient attributes used during some computations
    Point point_min_;
    Scalar weight_min_;
    Normal main_dir_, ortho_dir_; // direction of max var of radius and min var of radius
    Scalar unit_delta_weight_; // maximal variation of weight per unit on the triangle
    Scalar coord_max_; // maximal coordinate along main_dir_ of triangle points
    Scalar coord_middle_;
    Scalar max_seg_length_; // Correspond to the length of the longest segment in the 1D integration step
    Vector longest_dir_, half_dir_1_, half_dir_2_;
    Point point_half_;
    Vector longest_dir_special_;
    Normal unit_normal_;

};

} // Close namespace core

} // Close namespace expressive


#endif // OPT_WEIGHTED_TRIANGLE_H_
