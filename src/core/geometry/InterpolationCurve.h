/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef INTERPOLATIONCURVE_H
#define INTERPOLATIONCURVE_H
#include<core/CoreRequired.h>
#include<core/geometry/GeometryPrimitive.h>
#include <algorithm>
#include <math.h>
namespace expressive {

namespace core {


/**
 * A Interpolation curve. Based on GeometryPrimitive, this one offers a few more functions for
 * interpolating, position and length computation, etc...
 */

#define DEFAULT_INTERPOLATION_TENSION 0.5f
#define DEFAULT_TANGENT_MULTIPLIER 1.0f
#define DEFAULT_INTERPOLATION_STEPS 10

//TODO Choose Tension
template<typename PointT>
class InterpolationCurveT: public GeometryPrimitiveT<PointT>
{
public:
    EXPRESSIVE_MACRO_NAME("InterpolationCurve")

    typedef typename PointT::BasePoint BasePoint;

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////
    InterpolationCurveT()
        : GeometryPrimitiveT<PointT>(),
          m_interpolation_step(DEFAULT_INTERPOLATION_STEPS),
          m_global_tangent_multiplier(DEFAULT_TANGENT_MULTIPLIER),
          m_is_loop(false)
    {

    }

    InterpolationCurveT(const std::vector<PointT>& points, bool is_loop = false)
        : GeometryPrimitiveT<PointT>(),
          m_interpolation_step(DEFAULT_INTERPOLATION_STEPS),
          m_global_tangent_multiplier(DEFAULT_TANGENT_MULTIPLIER),
          m_is_loop(is_loop)
    {
        GeometryPrimitiveT<PointT>::points_ = points;
        HermiteCurveInterpolation(DEFAULT_INTERPOLATION_TENSION);
        //UpdateInterpolation(0, 0, 0, this->points_.size() - 1);
    }

    virtual ~InterpolationCurveT(){}

    virtual std::shared_ptr<core::GeometryPrimitiveT<PointT> > clone(unsigned int /*start*/ = -1, unsigned int /*end*/ = -1) const
    {
        return std::make_shared<InterpolationCurveT>(*this);
    }


    //Cardinal Spline Interpolation. Interpolation_tension is the tension parameter and should be in [0,1]
//    template<PointT>
    void HermiteCurveInterpolation(float interpolation_tension)
    {
        m_interpolation.clear();
        if(GeometryPrimitiveT<PointT>::points_.size() > 1)
        {
            Scalar offset = 0.0;
            for(unsigned int i = 0 ; i < GeometryPrimitiveT<PointT>::points_.size() - 1; i++)
            {
                ComputeInterpolationBetweenTwoPoints(i, interpolation_tension,offset);
//                ComputeInterpolationBetweenTwoPoints(GeometryPrimitiveT<PointT>::points_[i],GeometryPrimitiveT<PointT>::points_[i+1],offset);
                offset = 1.0/static_cast<Scalar>((m_interpolation_step + 1));
            }
        }
    }

    virtual void updated()
    {
        GeometryPrimitiveT<PointT>::updated();
        HermiteCurveInterpolation(DEFAULT_INTERPOLATION_TENSION);
        //ComputeSubdivision();
    }

    /////////////////
    /// Attributs ///
    /////////////////
//    virtual unsigned int Split(unsigned int index, const PointT &pos, std::set<std::shared_ptr<GeometryPrimitiveT<PointT> > > &newPrimitives) { return 0; }

    virtual void AddPoint(PointT p, unsigned int index = -1)
    {
        GeometryPrimitiveT<PointT>::AddPoint(p, index);

        int real_index = index;
        if (real_index == -1) {
            real_index = this->points_.size() - 1;
        }

        UpdateInterpolation(std::max((int)((real_index - 2)*(m_interpolation_step + 1)),0),
                            std::min(uint(m_interpolation.size() == 0 ? 0 : m_interpolation.size() - 1),(real_index + 2)*(m_interpolation_step + 1)),
                            std::max(real_index - 2, 0),
                            std::min((uint)real_index + 3, (uint)GeometryPrimitiveT<PointT>::points_.size() - 1));
    }

    virtual void RemovePoint(unsigned int index)
    {
        GeometryPrimitiveT<PointT>::RemovePoint(index);
        UpdateInterpolation(std::max((index - 2)*(m_interpolation_step + 1),(unsigned int)(0))
                            ,std::min((uint)m_interpolation.size() -1,(index + 2)*(m_interpolation_step + 1))
                            ,std::max(index - 2,(unsigned int)(0))
                            ,std::min(index+1,(uint)GeometryPrimitiveT<PointT>::points_.size() - 1));
    }

    virtual void RemovePoints()
    {
        GeometryPrimitiveT<PointT>::RemovePoints();
        m_interpolation.clear();
    }

    virtual void CopyPoints(const GeometryPrimitiveT<PointT> &other, unsigned int reversed)
    {
        GeometryPrimitiveT<PointT>::CopyPoints(other, reversed);
        HermiteCurveInterpolation(DEFAULT_INTERPOLATION_TENSION);
    }

    virtual void SetPoint(unsigned int index, const PointT &p)
    {
        GeometryPrimitiveT<PointT>::SetPoint(index, p);
        UpdateInterpolation(std::max((index - 2)*(m_interpolation_step + 1),(unsigned int)(0))
                            ,std::min((uint)m_interpolation.size() -1,(index + 2)*(m_interpolation_step + 1))
                            ,std::max(index - 2,(unsigned int)(0))
                            ,std::min(index+2,(uint)GeometryPrimitiveT<PointT>::points_.size() - 1));
    }

    void SetPoints(const std::vector<PointT> &points);

    virtual Scalar GetLength() const;

    virtual BasePoint getPointAtLength(Scalar l);

    const std::vector<PointT> & interpolated_points() const;

protected:
    PointT CardinalTangent(PointT start_point, PointT end_point, Scalar tension)
    {
        PointT n = end_point - start_point;
        return (m_global_tangent_multiplier * (1.0 - tension)*0.5*(n)) * 2.0;
    }


    PointT HermitePointInterpolation(const PointT& start_point,const PointT& end_point,const PointT& start_tangent,const PointT& end_tangent, float inbetween_weight)
    {
        return (2*pow(inbetween_weight,3) - 3*inbetween_weight*inbetween_weight + 1)*start_point
                + (pow(inbetween_weight,3) - 2*inbetween_weight*inbetween_weight + inbetween_weight)*(start_tangent)
                + (pow(inbetween_weight,3) - inbetween_weight*inbetween_weight)*(end_tangent)
                + (-2*pow(inbetween_weight,3) + 3*pow(inbetween_weight,2))*end_point;
    }


    void UpdateInterpolation(unsigned int start_index_for_remove, unsigned int end_index_for_remove, unsigned int start_index_interpolate, unsigned int end_index_interpolate)
    {
        if (m_interpolation_step == 0 || this->points_.size() < 2) {
            m_interpolation.clear();
            return;
        }

        float interpolation_tension = DEFAULT_INTERPOLATION_TENSION;


        bool remove_last_element = false;
        if (end_index_for_remove == this->m_interpolation.size() - 1) {
            remove_last_element = true;
        }
//            m_interpolation.erase(m_interpolation.begin() + start_index_for_remove, m_interpolation.end());
//        } else {
            m_interpolation.erase(m_interpolation.begin() + start_index_for_remove, m_interpolation.begin() + end_index_for_remove);
//        }

        unsigned int curr_index_interpolate = start_index_interpolate;
        //We use an offset to avoid point duplication for extremities
        //For the first call of compute interpolation we add the two extremities of the interpolated segment
        Scalar offset = 0.0;
        while(curr_index_interpolate < end_index_interpolate)
        {
            ComputeInterpolationBetweenTwoPoints(curr_index_interpolate, interpolation_tension, offset);
            offset = 1.0/static_cast<Scalar>(m_interpolation_step + 1);

            curr_index_interpolate++;
        }

        if (remove_last_element) {
            m_interpolation.pop_back();
        }

    }

    //Compute interpolation between
    void ComputeInterpolationBetweenTwoPoints(unsigned int index, float interpolation_tension, float offset=0.0f)
    {
        //std::vector<PointT> interpolatedPoints;
        PointT start_tangent;
        PointT end_tangent;
        if(index == 0)
        {
            start_tangent = (GeometryPrimitiveT<PointT>::points_[1] - GeometryPrimitiveT<PointT>::points_[0]);
            if(GeometryPrimitiveT<PointT>::points_.size() == 2)
                end_tangent = -start_tangent;
            else
                end_tangent = CardinalTangent(GeometryPrimitiveT<PointT>::points_[0], GeometryPrimitiveT<PointT>::points_[2], interpolation_tension);
        }
        else if(index == GeometryPrimitiveT<PointT>::points_.size() - 2)
        {
            start_tangent = CardinalTangent(GeometryPrimitiveT<PointT>::points_[index-1], GeometryPrimitiveT<PointT>::points_[index+1], interpolation_tension);
            end_tangent = (GeometryPrimitiveT<PointT>::points_[index+1] - GeometryPrimitiveT<PointT>::points_[index]);
        }
        else
        {
            start_tangent = CardinalTangent(GeometryPrimitiveT<PointT>::points_[index-1], GeometryPrimitiveT<PointT>::points_[index+1], interpolation_tension);
            end_tangent = CardinalTangent(GeometryPrimitiveT<PointT>::points_[index], GeometryPrimitiveT<PointT>::points_[index+2], interpolation_tension);
        }

        m_interpolation.reserve((m_interpolation_step + 1) * this->points_.size());
        unsigned int count = round(offset*(m_interpolation_step + 1));
        for(float f = offset ; f <= 1 ; f += 1.0/static_cast<Scalar>(m_interpolation_step + 1))
        {
//            PointT p = HermitePointInterpolation(GeometryPrimitiveT<PointT>::points_[index]
//                                                                                                                                        ,GeometryPrimitiveT<PointT>::points_[index+1]
//                                               ,start_tangent
//                                               ,end_tangent
//                                               ,f);

            int insertion_index = index*(m_interpolation_step + 1) + count;

            PointT p2 = HermitePointInterpolation(GeometryPrimitiveT<PointT>::points_[index] ,
                                              GeometryPrimitiveT<PointT>::points_[index+1],
                                              start_tangent,
                                              end_tangent,
                                              f);


            if (insertion_index >= m_interpolation.size()) {
                m_interpolation.push_back(p2);
//                m_interpolation.reserve(m_interpolation.size() * 2);
            } else {
                m_interpolation.insert((m_interpolation.begin() + insertion_index), p2);
            }
            count++;
        }

    }

protected:
    //TODO if m_interpolation_step is updated we have to recompute all the interpolation curve because it will alter add interpolation
    std::vector<PointT> m_interpolation;
    unsigned int m_interpolation_step;

    float m_global_tangent_multiplier;

    bool m_is_loop;
};

template<typename PointT>
Scalar InterpolationCurveT<PointT>::GetLength() const
{
    Scalar l = 0.0;
    for (unsigned int i = 1; i < m_interpolation.size(); ++i) {
        const PointT &p = m_interpolation[i - 1];
        const PointT &c = m_interpolation[i];
        l += (c - p).norm();
    }
    return l;
}


template<typename PointT>
typename PointT::BasePoint InterpolationCurveT<PointT>::getPointAtLength(Scalar l)
{
    assert(m_interpolation.size() > 1);
    if (l <= 0.0) {
        typename PointT::BasePoint p2 = m_interpolation[0];
        return m_interpolation[0];
    } else {
        Scalar totalLength = this->GetLength();
        if (l >= totalLength) {
            return m_interpolation[m_interpolation.size() - 1];
        }

        Scalar curLen = 0.0;
        Scalar prevLen = 0.0;
        unsigned int index = 1;
        for (index = 1; index < m_interpolation.size(); ++index) {
            const PointT &p = m_interpolation[index - 1];
            const PointT &c = m_interpolation[index];
            prevLen = curLen;
            curLen += (c - p).norm();
            if (curLen >= l) { // we've reached the interval on which the point we need is.
                Scalar ratio = (l - prevLen) / (curLen - prevLen);

                return m_interpolation[index - 1] * (1.0  - ratio) + c * ratio;
            }
        }
        return m_interpolation[m_interpolation.size() - 1];
    }
}

template<typename PointT>
void InterpolationCurveT<PointT>::SetPoints(const std::vector<PointT> &points)
{
    this->points_ = points;
    this->m_interpolation.clear();
    HermiteCurveInterpolation(DEFAULT_INTERPOLATION_TENSION);
}

template<typename PointT>
const std::vector<PointT> &InterpolationCurveT<PointT>::interpolated_points() const
{
    return m_interpolation;
}

}
}
#endif // INTERPOLATIONCURVE_H
