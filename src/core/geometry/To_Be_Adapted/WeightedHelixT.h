/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: WeightedHelixT.h

   Language: C++

   License: Convol license

   \author: Cedric Zanni, Remi Brouet
   E-Mail:  cedric.zanni@inria.fr,
            remi.brouet@inria.fr

   Description: Header for weighted helix.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_WEIGHTED_HELIX_H_
#define CONVOL_WEIGHTED_HELIX_H_

// Convol dependencies
//#include<Convol/include/ConvolRequired.h>

// Convol: Geometry
#include<Convol/include/geometry/AxisBoundingBoxT.h>
#include<Convol/include/geometry/BoundingSphereT.h>

// Convol: Tools (For Rotation around Axis)
#include<Convol/include/tools/VectorTools.h>

// Others dependencies
#include<vector>
#include<sstream>
#include<assert.h>

namespace Convol {

template< typename TTraits >
class WeightedHelixT {
public:

    typedef TTraits Traits;
	
    typedef typename Traits::Point  Point;
    typedef typename Traits::Scalar Scalar;
    typedef typename Traits::Vector Vector;

    typedef BoundingSphereT <Traits> BoundingSphere;
    typedef AxisBoundingBoxT<Traits> AxisBoundingBox;

    /*! \Brief Build An Helix
     */
    WeightedHelixT()
        : eps_    (-1.0), 
		angle_    ( 0.0),
		radius_   ( 1.0), 
		nb_helix_ ( 1.0),
		p1_       (-1.0, 0.0, 0.0),
		p2_       ( 1.0, 0.0, 0.0),
		weight_p1_( 0.1),
		weight_p2_( 0.1) {

		// Create Helix
        ComputeHelpVariables(true);
    }
    
    /*! \Brief Build a Helix based on the axis
     *   \param
     */
    WeightedHelixT(
		bool   dextral,
		Scalar angle,
		Scalar radius,
		Scalar nb_helix, 
		Scalar weight_p1,
		Scalar weight_p2,
		const Point& axis_origin,
		const Point& axis_end)
		: eps_      (dextral?-1.: 1.), 
		angle_      (angle          ),
		radius_     (radius         ), 
		nb_helix_   (nb_helix       ),
		weight_p1_  (weight_p1      ),
		weight_p2_  (weight_p2      ),
		axis_origin_(axis_origin    ),
		axis_end_   (axis_end       ) {

        ComputeHelpVariables(false);
    }

	/*! \Brief Build a Helix based on the extrema
     *   \param
     */
    WeightedHelixT(
		bool   dextral,
		Scalar angle,
		Scalar radius,
		Scalar nb_helix, 
		const Point& p1,
		const Point& p2,
		Scalar weight_p1,
		Scalar weight_p2)	
        : eps_    (dextral?-1.: 1.), 
		angle_    (angle          ),
		radius_   (radius         ), 
		nb_helix_ (nb_helix       ),
		p1_       (p1             ),
		p2_       (p2             ),
		weight_p1_(weight_p1      ),
		weight_p2_(weight_p2      ) {

        ComputeHelpVariables(true);
    }

    ~WeightedHelixT(){}
    
    AxisBoundingBox GetAxisBoundingBox() const {
        // TODO:@todo : we could do better boudning ...
        BoundingSphere boudning_sphere = GetBoundingSphere();

        return AxisBoundingBox(
			boudning_sphere.center_ - boudning_sphere.radius_*Vector(1.0,1.0,1.0),
			boudning_sphere.center_ + boudning_sphere.radius_*Vector(1.0,1.0,1.0));
    }

    BoundingSphere GetBoundingSphere() const {
        // TODO:@todo : we could do better boudning ...
        BoundingSphere res;
        
        Scalar angle = length_ / sqrt_r2s2_;
        Scalar half_height = 0.5 * stretch_ * angle;

        res.center_ = (axis_origin_ + axis_end_)/2.;
        res.radius_ = sqrt(half_height*half_height + radius_*radius_);

        return res;
    }

    
    ///////////////
    // Setters   //
    ///////////////

	// Setters: User-Friendly Parameters
	void set_dextral (bool    d, bool p_fixed = true) { eps_ = (d?-1.:1.); ComputeHelpVariables(p_fixed); }
	void set_angle   (Scalar  a, bool p_fixed = true) { angle_ = a;        ComputeHelpVariables(p_fixed); }
	void set_radius  (Scalar  r, bool p_fixed = true) { radius_ = r;       ComputeHelpVariables(p_fixed); }
	void set_nb_helix(Scalar nb, bool p_fixed = true) { nb_helix_ = nb;    ComputeHelpVariables(p_fixed); }

	// Setters: Point Data
	void set_p1(const Point& p) { 
		if (!spring_) {
			Scalar d1 = (p1_-p2_).length();
			Scalar d2 = (p  -p2_).length();
			radius_ *= d2/d1;
		}
		p1_ = p; 
		ComputeHelpVariables(true); 
	}

	void set_p2(const Point& p) { 
		if (!spring_) {
			Scalar d1 = (p1_-p2_).length();
			Scalar d2 = (p1_-p  ).length();
			radius_ *= d2/d1;
		}
		p2_ = p; 
		ComputeHelpVariables(true); 
	}
	
	void set_points(const Point& p1, const Point& p2) { 		
		if (!spring_) {
			Scalar d1 = (p1_-p2_).length();
			Scalar d2 = (p1 -p2 ).length();
			radius_ *= d2/d1;
		}
		p1_ = p1; p2_ = p2; 
		ComputeHelpVariables(true); 
	}
	
    void set_weight_p1(Scalar w) { weight_p1_ = w; }
    void set_weight_p2(Scalar w) { weight_p2_ = w; }

    void set_weights  (Scalar w1, Scalar w2) { weight_p1_ = w1; weight_p2_ = w2; }

	// Setters: Axis Data
	void set_axis_end   (const Point& p) { axis_end_    = p; ComputeHelpVariables(false); }
	void set_axis_origin(const Point& p) { axis_origin_ = p; ComputeHelpVariables(false); }
	
	void set_axis_points(const Point& p1, const Point& p2) { axis_origin_ = p1; axis_end_ = p2; ComputeHelpVariables(true); }

	// Setters: Helix Data
	void set_spring(bool b) { spring_ = b; }

    ///////////////
    // Getters   //
    ///////////////

	// Getters: User-Friendly Parameters
    bool   dextral () const { return (eps_<0.);  }
    Scalar angle   () const { return angle_;    }
    Scalar radius  () const { return radius_;   }
    Scalar nb_helix() const { return nb_helix_; }

	// Getters: Point Data
	const Point& p1() const { return p1_; }
	const Point& p2() const { return p2_; }
	
    Scalar weight_p1() const { return weight_p1_; }
    Scalar weight_p2() const { return weight_p2_; }

	// Getters: Primitive Data
	const Vector& dir     () const { return dir_;      }
    const Vector& unit_dir() const { return unit_dir_; }
	
    Scalar length   () const { return length_;    }
    Scalar sqrlength() const { return sqrlength_; }
    Scalar invlength() const { return invlength_; }

	// Getters: Axis Data
	const Point&  axis_end   () const { return axis_end_;    }
	const Point&  axis_origin() const { return axis_origin_; }
	
	const Scalar axis_length() const { return (axis_end_ - axis_origin_).length(); }

	// Getters: Helix Frame
	const Vector& axis_p1    () const { return axis_p1_;     }
	const Vector& axis_orth  () const { return axis_orth_;   }
	const Vector& axis_vector() const { return axis_vector_; }

	// Getters: Helix Data
    bool spring() const { return spring_; }

    const Scalar stretch     () const { return stretch_;      }
    const Scalar sqrt_r2s2   () const { return sqrt_r2s2_;    }
	const Scalar helix_length() const { return helix_length_; }

private:
	// User-Friendly Parameters
	Scalar eps_;                                // Dextral (-1.) or Sinistral (1.)
	Scalar angle_;                              // Rotation around the p1-p2 axis
	Scalar radius_;                             // Radius: Distance from axis
	Scalar nb_helix_;                           // Number of Helix

	// Point Data
	Point p1_;
	Point p2_;

	Scalar weight_p1_;
	Scalar weight_p2_;

	// Primitive Data
	Vector dir_;
	Vector unit_dir_;
    Scalar length_;
    Scalar sqrlength_;
    Scalar invlength_;

	// Axis Data
    Point axis_origin_;
    Point axis_end_;

	// Helix Frame
	Vector axis_p1_;
	Vector axis_orth_;
	Vector axis_vector_;

	// Helix Data
	bool spring_;

	Scalar stretch_;
	Scalar sqrt_r2s2_;
	Scalar helix_length_;

    inline void ComputeHelpVariables(bool p_fixed = true) {
		if (p_fixed) { ComputeHelix_PointFixed(); }
		else         { ComputeHelix_AxisFixed (); }

		// Helix Frame
		axis_vector_ = axis_end_ - axis_origin_; 
		axis_p1_     = p1_ - axis_origin_;
		axis_orth_   = axis_vector_ % axis_p1_;
		
		axis_p1_    .normalize();
		axis_orth_  .normalize();
		axis_vector_.normalize();

		// Primitive Data
		dir_       = p2_ - p1_;
        length_    = dir_.norm();
        sqrlength_ = dir_.sqrnorm();
        invlength_ = 1.0/length_;
        unit_dir_  = dir_;
        unit_dir_.normalize();
    }
	
	inline void ComputeHelix_PointFixed() {
		// Init Data
		Scalar nbh_2PI = 2.*M_PI*nb_helix_;

		// P1 - P2 Data
		dir_      = p2_ - p1_;
		length_   = dir_.norm();
		unit_dir_ = dir_; unit_dir_.normalize();

		// Init Stretch:
		stretch_ = (length_*length_ - radius_*radius_*(2.-2.*cos(nbh_2PI)))/(nbh_2PI*nbh_2PI);
		if (stretch_<0.) { stretch_ =             0.; radius_ = sqrt(length_*length_/(2. - 2.*cos(nbh_2PI))); } 
		else             { stretch_ = sqrt(stretch_); }

		// Vector: Canonique Bases:
		Vector u_c = Point(radius_*cos(eps_*nbh_2PI + angle_), radius_*sin(eps_*nbh_2PI + angle_), stretch_*nbh_2PI) - Point(radius_*cos(angle_), radius_*sin(angle_), 0.);
		Vector v_c = u_c; v_c.normalize();

		// Rotation Data
		Vector n1 = v_c % unit_dir_; n1.normalize();
		Vector n2 = v_c + unit_dir_; n2.normalize();
		
		// Vector n
		Vector n;
		Scalar th;
		if (n2[2] == 0.) { 
			// Axe of rotation (which is perpendicular to 0, 0, 1 axis).
			n = n2; n.normalize();

			// Remark: if n1.z == 0, we still take n2
			th = acos(-1.);  
		} else { 
			// Axe of rotation (which is perpendicular to 0, 0, 1 axis).
			n = n1 - (n1[2]/n2[2])*n2; n.normalize();

			// Angle of Frame rotation
			Scalar cth = ((dir_ | u_c) - (n | u_c)*(n | u_c))/((u_c | u_c) - (n | u_c)*(n | u_c));
			th = ( (dir_ | (n % u_c)) < 0.? -1.:1. )*acos(cth);
		}

		// Axis Data
        axis_origin_ = p1_          - tools::VectorToolsT<Traits>::Rotate(Point( radius_*cos(angle_), radius_*sin(angle_),               0.), n, th);
		axis_end_    = axis_origin_ + tools::VectorToolsT<Traits>::Rotate(Point(                  0.,                  0., stretch_*nbh_2PI), n, th);

		// Helix Data
		sqrt_r2s2_    = sqrt(radius_*radius_ + stretch_*stretch_);
		helix_length_ = nbh_2PI * sqrt_r2s2_;
	}

	inline void ComputeHelix_AxisFixed() {
		// Init Data
		Scalar nbh_2PI = 2.*M_PI*nb_helix_;
		Scalar axis_l  = axis_length();

		// Init Stretch:
		stretch_ = axis_l / nbh_2PI;

		// Helix Data
		sqrt_r2s2_    = sqrt(radius_*radius_ + stretch_*stretch_);
		helix_length_ = nbh_2PI * sqrt_r2s2_;

		// Vector: 
		Vector v_cano = Point(0., 0., 1.);
		Vector v_axis = axis_end_ - axis_origin_; v_axis.normalize();

		// Rotation Data
		Vector n  = v_cano % v_axis; n.normalize();
		Scalar th = acos(v_cano | v_axis);

		// Points Data
		p1_ = axis_origin_ + tools::VectorToolsT<Traits>::Rotate(Point(radius_*cos(               angle_), radius_*sin(               angle_),     0.) , n, th);
		p2_ = axis_origin_ + tools::VectorToolsT<Traits>::Rotate(Point(radius_*cos(eps_*nbh_2PI + angle_), radius_*sin(eps_*nbh_2PI + angle_), axis_l) , n, th);
	}
    
};

} // Close namespace Convol

#endif // CONVOL_WEIGHTED_HELIX_H_
