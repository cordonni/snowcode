/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: WeightedCircleT.h

   Language: C++

   License: Convol license

   \author: Cédric Zanni
   E-Mail:  cedric.zanni@inrialpes.fr

   Description: Header for polynomially weighted circle.
   WARNING : TODO:@todo : this class is only valide when opening < pi, should add the case opening > pi

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_WEIGHTED_CIRCLE_H_
#define CONVOL_WEIGHTED_CIRCLE_H_

// Convol dependencies
#include<Convol/include/ConvolRequired.h>
    // geometry
    #include<Convol/include/geometry/AxisBoundingBoxT.h>
    #include<Convol/include/geometry/BoundingSphereT.h>

#include<math.h>

// std dependencies
#include<sstream>
#include<vector>
// Assertion
#include<assert.h>

namespace Convol {

template< typename TTraits >
class WeightedCircleT
{
public:

    typedef TTraits Traits;

    typedef typename Traits::Scalar Scalar;
    typedef typename Traits::Point Point;
    typedef typename Traits::Vector Vector;
    typedef typename Traits::Normal Normal;

    typedef BoundingSphereT<Traits> BoundingSphere;
    typedef AxisBoundingBoxT<Traits> AxisBoundingBox;

    /*! \brief Build a constant 1.0 weighted circle in the plane xy
     */
    WeightedCircleT()
        :   radius_(1.0), opening_(0.5*M_PI),
            center_(0.0,0.0,0.0), dir_middle_(1.0,0.0,0.0),
            ortho_dir_(0.0,0.0,1.0),
            weight_p1_(1.0), weight_p2_(1.0)
    {
        ComputeHelpVariables();
    }
    WeightedCircleT(Scalar radius, Scalar opening,
                    const Point& center, const Normal& dir_middle, const Normal& ortho_dir,
                    Scalar weight_p1, Scalar weight_p2)
        :   radius_(radius), opening_(opening),
            center_(center), dir_middle_(dir_middle),
            ortho_dir_(ortho_dir),
            weight_p1_(weight_p1), weight_p2_(weight_p2)
    {
        //TODO:@todo : we should check that dir_middle_ and ortho_dir_ are orthogonal and normalized
        //TODO:@todo : instead we ensure that they are orthogonal... this should not be done here ...
        dir_middle_.normalize();
        ortho_dir_ = ortho_dir_ - (ortho_dir_|dir_middle_) * dir_middle_;
        ortho_dir_.normalize();

        ComputeHelpVariables();
    }
    ~WeightedCircleT(){}

    AxisBoundingBox GetAxisBoundingBox() const
    {
        // TODO:@todo : we could do better boudning ...
        BoundingSphere boudning_sphere = GetBoundingSphere();

        return AxisBoundingBox(
                        boudning_sphere.center_ - boudning_sphere.radius_*Vector(1.0,1.0,1.0),
                        boudning_sphere.center_ + boudning_sphere.radius_*Vector(1.0,1.0,1.0)
                    );
    }
    BoundingSphere GetBoundingSphere() const
    {
        //TODO:@todo : we could do better...
        if(opening_<M_PI)
        {
            return BoundingSphere(0.5*(p1_+p2_), (p1_-p2_).norm());
        }
        else
        {
            return BoundingSphere(center_, radius_);
        }
    }

    ///////////////
    // Modifiers //
    ///////////////

    //..................................................................................

    void set_radius(Scalar radius)
    {
        radius_ = radius;
        ComputeHelpVariables();
    }
    void set_opening(Scalar opening)
    {
        opening_ = opening;
        ComputeHelpVariables();
    }
    void set_center(const Point& center)
    {
        center_ = center;
        ComputeHelpVariables();
    }

    //TODO:@todo: this is not a good idea to seperate the 2 following setting function
    void set_ortho_dir(const Normal& ortho_dir)
    {
        ortho_dir_ = ortho_dir;
        ortho_dir_.normalize();
        //TODO:@todo : we should check that dir_middle_ and ortho_dir_ are orthogonal and normalized
        //TODO:@todo : instead we ensure that they are orthogonal... this should not be done here ...
        dir_middle_ = dir_middle_ - (dir_middle_|ortho_dir_) * ortho_dir_;
        dir_middle_.normalize();
        ComputeHelpVariables();
    }
    void set_dir_middle(const Normal& dir_middle)
    {
        dir_middle_ = dir_middle;
        dir_middle_.normalize();
        //TODO:@todo : we should check that dir_middle_ and ortho_dir_ are orthogonal and normalized
        //TODO:@todo : instead we ensure that they are orthogonal... this should not be done here ...
        ortho_dir_ = ortho_dir_ - (ortho_dir_|dir_middle_) * dir_middle_;
        ortho_dir_.normalize();

        ComputeHelpVariables();
    }

    void set_weights(const Scalar w1, const Scalar w2)
    { 
        weight_p1_ = w1; 
        weight_p2_ = w2; 
        ComputeHelpVariables();
    }
    void set_weight_p1(const Scalar w1)                  
    { 
        weight_p1_ = w1; 
        ComputeHelpVariables();
    }
    void set_weight_p2(const Scalar w2)                  
    { 
        weight_p2_ = w2; 
        ComputeHelpVariables();
    }

    ///////////////
    // Accessors //
    ///////////////

    Scalar radius()   const { return radius_; }
    Scalar opening()  const { return opening_; }

    const Point& center()       const { return center_; }
    const Vector& ortho_dir()   const { return ortho_dir_; }
    const Vector& dir_middle()  const { return dir_middle_; }

    const Point& p1() const { return p1_; }
    const Point& p2() const { return p2_; }

    Scalar weight_p1() const { return weight_p1_; }
    Scalar weight_p2() const { return weight_p2_; }

    const Scalar* weight_coeff()  const { return weight_coeff_; }
    const Scalar weight_coeff(int i) const { return weight_coeff_[i]; }

    Scalar t() const { return t_; }
    Scalar length() const { return length_; }

private:

    // This variable are a minimal set to represent an arc of circle
    Scalar radius_;
    Scalar opening_;
    Point  center_;
    Vector dir_middle_;
    Vector ortho_dir_; // direction orthogonal to the plane containing the circle
                       //TODO:@todo: this should be a quaternion

    Scalar weight_p1_; // weight corresponding to -t
    Scalar weight_p2_; // weight corresponding to +t

    // Convenience attributes
    // extrem point of the arc of circle
    Point p1_; // point corresponding to -t
    Point p2_; // point corresponding to +t
    // length
    Scalar length_;
    // the rational parametrization range is [-t;t] with t=tan(opening/4.0)
    Scalar t_;
    // polynomial coeff of weighting function in the range [-t;t] (that correspond to rational parametrization)
    Scalar weight_coeff_[2]; //TODO:@todo : should be replaced by an std::array

    inline void ComputeHelpVariables()
    {
        Point circle_middle_point = center_+radius_*dir_middle_;
        p1_ = Rotation(circle_middle_point, center_, ortho_dir_, -0.5*opening_);
        p2_ = Rotation(circle_middle_point, center_, ortho_dir_, 0.5*opening_);

        length_ = opening_*M_PI;
        t_ = tan(opening_/4.0);

        weight_coeff_[0] = 0.5*(weight_p2_+weight_p1_);
        weight_coeff_[1] = 0.5*(weight_p2_-weight_p1_) / t_;
    }

    const Point Rotation(const Point& point, const Point& center, const Vector& axis, Scalar angle) const
    { // TODO:@todo : should add an AffineTransform class ...
        const Vector& local_point = point-center;
        Scalar coord_normal =  (axis|local_point);

        return center   +   coord_normal *  axis
                        +   cos(angle) * (local_point - coord_normal * axis)
                        +   sin(angle) * (axis % local_point);
    }

};

} // Close namespace Convol

#endif // CONVOL_WEIGHTED_CIRCLE_H_
