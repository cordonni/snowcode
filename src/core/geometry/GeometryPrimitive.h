/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GEOMETRYPRIMITIVE_H
#define GEOMETRYPRIMITIVE_H

// core dependencies
#include<core/CoreRequired.h>
//#include <core/geometry/Point.h>
    // geometry
    #include<core/geometry/AABBox.h>
    #include<core/geometry/BoundingSphere.h>
    // utils
    #include <core/utils/BasicsToXml.h>
    #include<core/utils/Serializable.h>

// use for WeightedGeometryPrimitive typedef
#include <core/geometry/WeightedPoint.h>

namespace expressive
{

namespace core
{

template<typename PointT>
/**
 * The GeometryPrimitiveT class represents any kind of polygon. It offers methods to
 * add/remove points, intersection tests, center computation, split...
 */
class GeometryPrimitiveT : public Primitive/*: public Serializable*/
{
public:
    
    EXPRESSIVE_MACRO_NAME("GeometryPrimitiveT")

    typedef typename PointT::BasePoint BasePoint;

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////
    GeometryPrimitiveT() : Primitive(), type_(0) {}

    GeometryPrimitiveT(const GeometryPrimitiveT &other, unsigned int start = -1, unsigned int end = -1);

    virtual ~GeometryPrimitiveT() {}

    virtual void updated() {}

//    template<typename T>
//    static T *instanced_clone(const T &other) { return new T(other); }

//    template<typename T>
//    static T clone(const T &other, unsigned int start = -1, unsigned int end = -1) { return T(other, start, end); }

    /**
     * Returns a new pointer that contains the same data as this GeometryPrimitive.
     * The return GeometryPrimitive can be a subpart of this one.
     * @param start The first Point to copy.
     * @param end The last point to copy.
     */
    virtual std::shared_ptr<GeometryPrimitiveT<PointT> > clone(unsigned int start = - 1, unsigned int end = -1) const = 0;

//    virtual std::shared_ptr<GeometryPrimitiveT<PointT> > cloneType() const = 0;

    virtual std::string ToString() const;

    /**
     * Returns the bounding box surrounding this GeometryPrimitive
     */
    virtual AABBoxT<BasePoint> GetAxisBoundingBox() const;

    /**
     * Returns the bounding sphere surrounding this GeometryPrimitive.
     */
    virtual BoundingSphereT<BasePoint> GetBoundingSphere() const;

    /**
     * Returns a serialized xml version of this primitive.
     * @param embed_vertices if true, the vertices will be stored entirely inside the GeometryPrimitive.
     *                       otherwise, they will only be refered to (as indices) and saved outside the GP.
     * @param name The n ame of the xml tag.
     */
    virtual TiXmlElement * ToXml(bool embed_vertices = true, const char *name = NULL) const;

    virtual void AddXmlAttributes(TiXmlElement* /*element*/) const {}

    /**
     * Add a point to this polygon.
     * @param p the position of the new point.
     * @param index the index of the new point.
     */
    virtual void AddPoint(const PointT &p, unsigned int index = -1);

    /**
     * Removes a point from this polygon.
     * @param index the index of the point that should be removed.
     */
    virtual void RemovePoint(unsigned int index);

    /**
     * Removes every points from this GeometryPrimitive.
     */
    virtual void RemovePoints();

    /**
     * Copy points from a given GeometryPrimitive to this one.
     * @param other An arbitrary GeometryPrimitive.
     * @param reversed If true, points will be copied in reverse order.
     */
    virtual void CopyPoints(const GeometryPrimitiveT &other, unsigned int reversed);

    /**
     * Sets the position of point at given index to the given position.
     * @param index index of the point to be replaced.
     * @param p the new value of that point.
     */
    virtual void SetPoint(unsigned int index, const PointT &p);

    /**
     * Returns an editable vector to this GeometryPrimitive's points.
     */
    std::vector<PointT> &GetPoints();

    /**
     * Returns an editable vector to this GeometryPrimitive's points.
     */
    const std::vector<PointT> &GetPoints() const;

    /**
     * Returns an editable version of a point of this GeometryPrimitive.
     * @param index index of the point to be returned.
     */
    PointT &GetPoint(unsigned int index);

    /**
     * Returns an editable version of a point of this GeometryPrimitive.
     * @param index index of the point to be returned.
     */
    const PointT &GetPoint(unsigned int index) const;

    unsigned int GetSize() const;

    /**
     * Returns the first point of this GeometryPrimitive.
     */
    virtual PointT &GetStart();

    /**
     * Returns the first point of this GeometryPrimitive.
     */
    virtual const PointT &GetStart() const;

    /**
     * Returns the last point of this GeometryPrimitive.
     */
    virtual PointT &GetEnd();

    /**
     * Returns the last point of this GeometryPrimitive.
     */
    virtual const PointT &GetEnd() const;

    virtual Scalar GetLength() const;

    /**
     * Returns tye type flag from this GeometryPrimitive.
     */
    inline int getType() const {return type_;}

//    /**
//     * Returns true if this GeometryPrimitive intersects p.
//     */
//    bool intersects(const PointT &p);

//    /**
//     * Returns true if this GeometryPrimitive intersects p.
//     */
//    bool intersects(const GeometryPrimitiveT &p);

//    /**
//     * Returns true if this GeometryPrimitive contains p.
//     */
//    bool contains(const PointT &p);

//    /**
//     * Returns true if this GeometryPrimitive contains p.
//     */
//    bool contains(const GeometryPrimitiveT &p);

//    /**
//     * Returns the curvilinear length of this GeometryPrimitive at a given index.
//     * @param index a point in this GeometryPrimitive. Default is -1, which means full length.
//     */
//    Scalar getCurvilinearLength(int index = -1);

    /**
     * Returns the interpolated length at a curvilinear length in this GeometryPrimitive.
     * @param l a Scalar between 0.0 and the #getCurvilinearLength().
     */
    virtual BasePoint getPointAtLength(Scalar l);

    /**
     * Comparison operator between 2 GeometryPrimitives.
     * Mandatory for std containers insertions.
     */
    friend bool operator<(const GeometryPrimitiveT& g1, const GeometryPrimitiveT& g2)
    {
        if (g1.points_.size() != g2.points_.size()) {
            return g1.points_.size() < g2.points_.size();
        }
        for (unsigned int i = 0; i < g1.points_.size(); ++i) {
//            if (points_[i] != g2.points_[i]) {
            if (g1.points_[i] != g2.points_[i] && g1.points_[i] != g2.points_[g2.points_.size() - 1 - i])
            {
                //return (g1.points_[i].array() < g2.points_[i].array()).all();
                return g1.points_[i] < g2.points_[i];
            }
        }
        return false;
    }

    /**
     * Comparison operator between 2 GeometryPrimitives.
     * Mandatory for std containers insertions.
     */
    bool operator==(const GeometryPrimitiveT& g2) const
    {
        if (points_.size() != g2.points_.size()) {
            return false;
        }
        for (unsigned int i = 0; i < points_.size(); ++i) {
            if (points_[i] != g2.points_[i] && points_[i] != g2.points_[g2.points_.size() - 1 - i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Reverse this GeometryPrimitive's points.
     */
    void Invert()
    {
        std::reverse(points_.begin(), points_.end());
//        std::vector<PointT> tmp(points_.rbegin(), points_.rend());
//        tmp.swap(points_);
    }

    /**
     * Splits this GeometryPrimitive at a given point. Results will vary depending on the type of GeometryPrimitive.
     * A segment will return a new segment. A splitted triangle will return 2 new triangles. etc...
     * @param index index at which the split should occur. Point after this position will be moved to pos, and every other points moved to new primitives.
     * @param pos The new position of the point where the split occured.
     * @param newPrimitives the list of primitives created by this splitting.
     * @return the index of the moved position, which is also the first point of other primitives (index + 1 for segments).
     */
    virtual unsigned int Split(unsigned int index, const PointT &pos, std::set<std::shared_ptr<GeometryPrimitiveT> > &newPrimitives);

    /**
     * Returns the mid point of this GeometryPrimitive.
     */
    virtual BasePoint GetCenter();

protected:
    std::vector<PointT> points_; //< The list of points contained in this polygon.

    int type_; //< an optional type, used for custom algorithms to differentiate GeometryPrimitives.

};

template<typename PointT>
GeometryPrimitiveT<PointT>::GeometryPrimitiveT(const GeometryPrimitiveT &rhs, unsigned int start, unsigned int end) : Primitive(), type_(rhs.type_)
{
    start = std::max(0, (int)start);
    end = std::max((int) rhs.GetSize(), (int)end);
    points_.insert(points_.begin(), rhs.points_.begin() + start, rhs.points_.begin() + end);
}

template<typename PointT>
std::string GeometryPrimitiveT<PointT>::ToString() const
{
    std::string s = Name();
    s += " - points:";
    for (uint i = 0; i < points_.size(); ++i) {
        s += "[";
        s += std::to_string(points_[i][0]);
        s += ",";
        s += std::to_string(points_[i][1]);
        s += ",";
        s += std::to_string(points_[i][2]);
        s += "]";
    }
    s += " - type:" + std::to_string(type_);
//    s += " - loop:" + std::to_string(loop_);

    return s;
}

template<typename PointT>
AABBoxT<typename PointT::BasePoint> GeometryPrimitiveT<PointT>::GetAxisBoundingBox() const
{
    AABBoxT<BasePoint> b;
    for (const PointT &p : points_) {
        b.Enlarge(p);
    }
    return b;
}

template<typename PointT>
auto GeometryPrimitiveT<PointT>::GetBoundingSphere() const -> BoundingSphereT<BasePoint>
{
    BoundingSphereT<BasePoint> res;
//    res.radius_ = 0.5*length_;
    res.center_ = points_[0];
    res.radius_ = 0.0;
    for (unsigned int i = 1; i < points_.size(); ++i) {
        res.center_ += points_[i];
    }
    res.center_ /= (float) points_.size();


    for (unsigned int i = 0; i < points_.size(); ++i) {
        res.radius_ = std::max((points_[i] - res.center_).norm(), res.radius_);
    }

    return res;
}

template<typename PointT>
TiXmlElement * GeometryPrimitiveT<PointT>::ToXml(bool embed_vertices, const char *name) const
{
    TiXmlElement *element = Primitive::ToXml(name);
    if (embed_vertices) {
        for (unsigned int i = 0; i < points_.size(); ++i) {
            char n[10];
            sprintf(n, "p%d", i);
            TiXmlElement * element_p = PointToXml(points_[i], n);
            element->LinkEndChild(element_p);
        }
    }

//    AddXmlAttributes(element);

    return element;
}

template<typename PointT>
void GeometryPrimitiveT<PointT>::RemovePoint(unsigned int index)
{
    assert(index < points_.size());
    points_.erase(points_.begin() + index);
}

template<typename PointT>
void GeometryPrimitiveT<PointT>::RemovePoints()
{
    points_.clear();
}

template<typename PointT>
void GeometryPrimitiveT<PointT>::AddPoint(const PointT &p, unsigned int index)
{
    if (index == (unsigned int) -1) {
        points_.push_back(p);
    } else {
        points_.insert(points_.begin() + index, p);
    }
}

template<typename PointT>
PointT &GeometryPrimitiveT<PointT>::GetPoint(unsigned int index)
{
    assert(index < points_.size() && (int)index >= 0 && "invalid index provided in geometry primitive");
    return points_[index];
}

template<typename PointT>
const PointT &GeometryPrimitiveT<PointT>::GetPoint(unsigned int index) const
{
    assert(index < points_.size() && (int)index >= 0 && "invalid index provided in geometry primitive");
    return points_[index];
}

template<typename PointT>
std::vector<PointT> & GeometryPrimitiveT<PointT>::GetPoints()
{
    return points_;
}

template<typename PointT>
const std::vector<PointT> & GeometryPrimitiveT<PointT>::GetPoints() const
{
    return points_;
}

template<typename PointT>
void GeometryPrimitiveT<PointT>::CopyPoints(const GeometryPrimitiveT &other, unsigned int reversed)
{
    points_.clear();
    if (reversed) {
        points_.insert(points_.begin(), other.points_.rbegin(), other.points_.rend());
    } else {
        points_.insert(points_.begin(), other.points_.begin(), other.points_.end());
    }
}

template<typename PointT>
void GeometryPrimitiveT<PointT>::SetPoint(unsigned int index, const PointT &p)
{
    if (index < GetSize()) {
        points_[index] = p;
    } else {
        if (index == GetSize() && isLoop()) {
            points_[0] = p;
        } else {
            points_.push_back(p);
        }
    }
}

template<typename PointT>
unsigned int GeometryPrimitiveT<PointT>::GetSize() const
{
    return (unsigned int) points_.size();
}

template<typename PointT>
PointT &GeometryPrimitiveT<PointT>::GetStart()
{
    assert(points_.size() > 0);
    return points_[0];
}

template<typename PointT>
const PointT &GeometryPrimitiveT<PointT>::GetStart() const
{
    assert(points_.size() > 0);
    return points_[0];
}

template<typename PointT>
PointT &GeometryPrimitiveT<PointT>::GetEnd()
{
    assert(points_.size() > 0);
    return points_[points_.size() - 1];
}

template<typename PointT>
const PointT &GeometryPrimitiveT<PointT>::GetEnd() const
{
    assert(points_.size() > 0);
    return points_[points_.size() - 1];
}

template<typename PointT>
Scalar GeometryPrimitiveT<PointT>::GetLength() const
{
    Scalar l = 0.0;
    for (unsigned int i = 1; i < points_.size(); ++i) {
        const PointT &p = points_[i - 1];
        const PointT &c = points_[i];
        l += (c - p).norm();
    }
    return l;
}

template<typename PointT>
unsigned int GeometryPrimitiveT<PointT>::Split(unsigned int index, const PointT &pos, std::set<std::shared_ptr<GeometryPrimitiveT> > &newPrimitives)
{
    std::shared_ptr<GeometryPrimitiveT> end = clone();
    if (isLoop()) {
        set_is_loop(false);
        end->set_is_loop(false);
        end->AddPoint(GetPoint(0), end->GetSize());
    }

    for (unsigned int i = index + 1; i < points_.size() - 1; ++i) {
        RemovePoint(index);
    }
    for (unsigned int i = 1; i < index + 1; ++i) {
        end->RemovePoint(0);
    }

    SetPoint(index + 1, pos);
    end->SetPoint(0, pos);

    newPrimitives.insert(end);
    return index + 1;
}

template<typename PointT>
typename PointT::BasePoint GeometryPrimitiveT<PointT>::GetCenter()
{
    BasePoint res = BasePoint::Zero();
    for (unsigned int i = 0; i < points_.size(); ++i) {
        res += points_[i];
    }
    res = res * 1.0 / (Scalar) points_.size();
    return res;
}

template<typename PointT>
typename PointT::BasePoint GeometryPrimitiveT<PointT>::getPointAtLength(Scalar l)
{
    assert(points_.size() > 1);
    if (l <= 0.0) {
        return points_[0];
    } else if (l > GetLength()) {
        return points_[points_.size() - 1];
    } else {
        Scalar curLen = 0.0;
        Scalar prevLen = 0.0;
        const PointT &p = points_[0];
        unsigned int index = 1;
        for (index = 1; index < points_.size(); ++index) {
            const PointT &c = points_[index];
            prevLen = curLen;
            curLen += (c - p).norm();
            if (curLen >= l) { // we've reached the interval on which the point we need is.
                Scalar ratio = (l - prevLen) / (curLen - prevLen);
                return points_[index - 1] * (1.0  - ratio) + c * ratio;
            }
        }
        return points_[points_.size() - 1];
    }
}

//template<typename PointT>
//bool GeometryPrimitiveT<PointT>::Equals(const GeometryPrimitiveT<PointT> &g2) const
//{
//    if (type_ != g2.type_ || points_.size() != g2.points_.size()) {
//        return false;
//    }
//    for (unsigned int i = 0; i < points_.size(); ++i) {
//        if (points_[i] != g2.points_[i]) {
//            return false;
//        }
//    }
//    return true;
//}

//typedef GeometryPrimitiveT<Point> GeometryPrimitive;
typedef GeometryPrimitiveT<WeightedPoint> WeightedGeometryPrimitive;
typedef GeometryPrimitiveT<WeightedPoint2D> WeightedGeometryPrimitive2D;
typedef GeometryPrimitiveT<PointPrimitive> GeometryPrimitive;
typedef GeometryPrimitiveT<PointPrimitive2D> GeometryPrimitive2D;

}

}

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_GEOMETRYPRIMITIVE
extern template class expressive::core::GeometryPrimitiveT<expressive::core::PointPrimitiveT<expressive::Point> > ;
extern template class expressive::core::GeometryPrimitiveT<expressive::core::PointPrimitiveT<expressive::Point2> >;
extern template class expressive::core::GeometryPrimitiveT<expressive::core::WeightedPointT<expressive::Point> > ;
extern template class expressive::core::GeometryPrimitiveT<expressive::core::WeightedPointT<expressive::Point2> >;
#endif
#endif

#endif // GEOMETRYPRIMITIVE_H
