/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef FRAME_H
#define FRAME_H

#include <core/CoreRequired.h>
#include "core/VectorTraits.h"

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 175 )
#endif

#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {



namespace core {

/**
 * @brief The Frame class
 * Represents an orthogonal frame from a given affine transformation
 */

class CORE_API Frame
{

public:

    // ///////////////////////////////////////////
    // Constructors :
    // ///////////////////////////////////////////

    /**
     * @brief Frame direct constructor
     * @param transform : internal affine transformation
     */
    Frame(const Transform& transform /*= Transform::Identity()*/);

    /**
     * @brief Frame copy constructor
     * @param to_copy : Frame to be copied
     */
    Frame(const Frame& to_copy);

    /**
     * @brief Frame explicit Vector constructor
     * @param p0 : origin of the Frame
     * @param px : X axis of the frame
     * @param py : Y axis of the frame
     * @param pz : Z axis of the frame
     */
    Frame(const Point& p0 = DEF_P0, const Vector& px = DEF_PX, const Vector& py = DEF_PY, const Vector& pz = DEF_PZ);

    /**
     * @brief Frame 3D scaling constructor
     * @param lx : elongation along X axis
     * @param ly : elongation along Y axis
     * @param lz : elongation along Z axis
     */
    Frame(double lx, double ly, double lz);


    /**
     * @brief Frame 3D scaling constructor
     * @param l : elongation along X axis
     */
    Frame(double l);

protected:
    void Init(const Point& p0, const Vector& px, const Vector& py, const Vector& pz);

public:

    // ///////////////////////////////////////////
    // Axis Access functions :
    // ///////////////////////////////////////////

    /**
     * @brief p0
     * @return origin of the frame
     */
    Point  p0() const;

    /**
     * @brief px
     * @return X axis of the frame
     */
    Vector px() const;

    /**
     * @brief py
     * @return Y axis of the frame
     */
    Vector py() const;

    /**
     * @brief pz
     * @return Z axis of the frame
     */
    Vector pz() const;



    // ///////////////////////////////////////////
    // Transformation functions :
    // ///////////////////////////////////////////

    /**
     * @brief translate
     * @param v : parameter of the translation
     */
    Frame& translate(const Vector& v);

    /**
     * @brief rotate
     * @param axis : axis of the rotation
     * @param angle : angle of th rotation
     * @return reference to the frame
     */
    Frame& rotate(const Vector& axis, const Scalar angle);

    /**
     * @brief rotate
     * @param normaxis : vector containing the axis of the rotation (through normalization) and the angle (through module)
     * @return reference to the frame
     */
    Frame& rotate(const Vector& normaxis);

    /**
     * @brief rotate
     * @param q : quaternion representing the rotation
     * @return reference to the frame
     */
    Frame& rotate(const Quaternion& q);

    /**
     * @brief scale
     * @param s : amplitude of the scaling
     * @return reference to the frame
     */
    Frame& scale(const Scalar& s);

    /**
     * @brief scale
     * @param s : vector representing the elongation along each axis
     * @return reference to the frame
     */
    Frame& scale(const Vector& s);

    /**
     * @brief transform
     * @param t : transformation to be applied
     * @return reference to the frame
     */
    Frame& transform(const Transform& t);


    /**
     * @brief movePz
     * @param v : desired displacement of the Pz point
     * @return reference to the frame
     */
    Frame& movePz(const Vector& v);



    // ///////////////////////////////////////////
    // Normalization functions :
    // ///////////////////////////////////////////

    /**
     * @brief normalized
     * @return a normalized copy of thef frame
     */
    Frame normalized(const double lx = 1.0, const double ly = 1.0, const double lz = 1.0) const;

    /**
     * @brief normalize the frame *in place*
     * @return reference to the frame
     */
    Frame& normalize(const Vector& l);
    Frame& normalize(const double lx, const double ly, const double lz = 1.0);
    Frame& normalize(const double l = 1.0);



    // ///////////////////////////////////////////
    // Attribute access functions :
    // ///////////////////////////////////////////

    /**
     * @brief internal_transform : read-only access to the internal transformation of the frame
     * @return const ref on the actual attribute
     */
    const Transform& internal_transform() const;

    /**
     * @brief internal_transform : read & write access to the internal transformation of the frame
     * @return ref on the actual attribute
     */
    Transform& internal_transform();



    // ///////////////////////////////////////////
    // Embeddings:
    // ///////////////////////////////////////////

    /**
     * @brief Embed : embed a 3D point from the cardinal space into the space defined by the frame
     * @param p3d : 3D point to be embedded
     * @return : 3D point
     */
    Vector Embed(const expressive::Point &p3d) const;

    /**
     * @brief UnEmbed : unembed a 3D point from the space defined by the frame into the cardinal space
     * @param p3d : 3D point to be unembedded
     * @return : 3D point
     */
    Vector UnEmbed(const expressive::Point &p3d) const;

    /**
     * @brief EmbedXY : embed a 2D point into a cardinal plane of the frame
     * @param p2d : 2D point to be embedded
     * @return : 3D point
     */
    Vector EmbedXY(const expressive::Point2 &p2d) const;

    /**
     * @brief EmbedYZ : embed a 2D point into a cardinal plane of the frame
     * @param p2d : 2D point to be embedded
     * @return : 3D point
     */
    Vector EmbedYZ(const expressive::Point2 &p2d) const;

    /**
     * @brief EmbedZX : embed a 2D point into a cardinal plane of the frame
     * @param p2d : 2D point to be embedded
     * @return : 3D point
     */
    Vector EmbedZX(const expressive::Point2 &p2d) const;



    // ///////////////////////////////////////////
    // Projections:
    // ///////////////////////////////////////////

    Point ProjectX(const Vector &p3d) const;
    Point ProjectY(const Vector &p3d) const;
    Point ProjectZ(const Vector &p3d) const;

    /**
     * @brief ProjectXY : project a 3D point onto a cardinal plane of the frame
     * @param p3d : 3D point to be projected
     * @return : 2D point
     */
    Point2 ProjectXY(const Vector &p3d) const;

    /**
     * @brief ProjectYZ : project a 3D point onto a cardinal plane of the frame
     * @param p3d : 3D point to be projected
     * @return : 2D point
     */
    Point2 ProjectYZ(const Vector &p3d) const;

    /**
     * @brief ProjectZX : project a 3D point onto a cardinal plane of the frame
     * @param p3d : 3D point to be projected
     * @return : 3D point
     */
    Point2 ProjectZX(const Vector &p3d) const;



    // ///////////////////////////////////////////
    // Intrpolations:
    // ///////////////////////////////////////////


    /**
     * @brief InterpolateSimple : interpolates between two frames, orientation interpolation is naively linear
     * @param f0 first frame to interpolate
     * @param f1 second frame to interpolate
     * @param t  interpolation parameter
     * @return : Interpolated Frame
     */
    static Frame InterpolateSimple(const Frame& f0, const Frame& f1, const Scalar t = 0.5);

    /**
     * @brief InterpolateSlerp : interpolates between two frames, orientation interpolation uses slerp
     * @param f0 first frame to interpolate
     * @param f1 second frame to interpolate
     * @param t  interpolation parameter
     * @return : Interpolated Frame
     */
    static Frame InterpolateSlerp(const Frame& f0, const Frame& f1, const Scalar t = 0.5);


private:

    /**
     * @brief internal_transform_ holds the inner transformation of the frame
     */
    Transform internal_transform_;

    /**
     * @brief DEF_P0 default origin of the frame [CONST STATIC]
     */
    static const Point  DEF_P0;

    /**
     * @brief DEF_PX default X axis of the frame [CONST STATIC]
     */
    static const Vector DEF_PX;

    /**
     * @brief DEF_PY default Y axis of the frame [CONST STATIC]
     */
    static const Vector DEF_PY;

    /**
     * @brief DEF_PZ default Z axis of the frame [CONST STATIC]
     */
    static const Vector DEF_PZ;
};


} // end of namespace core

} // end of namespace expressive

#endif // FRAME_H
