/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: VectorTools.h

   Language: C++

   License: Expressive license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: General function concerning Vectors.

   Platform Dependencies: None
*/

#pragma once
#ifndef EXPRESSIVE_VECTOR_TOOLS_H_
#define EXPRESSIVE_VECTOR_TOOLS_H_

#include "core/VectorTraits.h"
#include "commons/Maths.h"

#include <vector>

namespace expressive {

namespace VectorTools {

/**
 * [DEPRECATED] This should disappear and be replaced by a Matrix or Transform.
 */
struct Transformation
{
    Vector axis_rot_;
    Vector translation_;
    Vector scale_;
    Scalar angle_;
};

/**
 * Returns the angle between this vector and the given vector. The
 * returned angle is in the 0 2.PI interval.
 */
double CORE_API angle2d(const Point2 &u, const Point2 &v);

/** \brief return a random orthogonal vector to the given vector.
*        Implemented for dimension 2,3 and 4.
*        For dimension 2, this gives a right handed plane frame.
*        @todo Implement the rest for dimension N
*   \param other
*   \return  random orthogonal vector to other
*/
Vector2 CORE_API GetOrthogonalRandVect(const Vector2& other);
Vector CORE_API GetOrthogonalRandVect(const Vector& other);
Vector4 CORE_API GetOrthogonalRandVect(const Vector4& other);

/**
 * Computes the intersection line between two planes, if those are not parrallel.
 * @param p1 a point on plane 1
 * @param dir1 the normal of plane 1
 * @param p2 a point on plane 2
 * @param dir2 the normal of plane 2
 * @param[out] out_pos a point on the intersection line
 * @param[out] out_dir the direction vector of the intersection line.
 * @return true if an intersection was found. False otherwise.
 */
bool CORE_API ComputePlanePlaneIntersection(const Point &p1, const Vector &dir1, const Point &p2, const Vector &dir2, Point &out_pos, Point &out_dir);

// Rotate vector U around N (oriented) by angle (rad).
// Formula taken from http://fr.wikipedia.org/wiki/Rotation_vectorielle
// N must be normalized
template < typename T>  // T can be either Point or Vector
T Rotate (T U, const Vector& N, Scalar angle)
{
    assert(N.squaredNorm()<1.001 && N.squaredNorm()>0.999);

    Scalar cosangle = cos(angle);
    return cosangle*U + ((1.0-cosangle)*U.dot(N))*N + sin(angle) * N.cross(U);
}

/**
 * Computes the distance of p from the line defined by points a & b.
 */
template<typename PointT>
Scalar lineDistSq(PointT a, PointT b, PointT p)
{
    auto ap = p - a;
    auto ab = b - a;
    Scalar dotprod = ab.dot(ap);
    Scalar projLenSq = dotprod * dotprod / ab.squaredNorm();

    return ap.squaredNorm() - projLenSq;
}

/**
 * Computes the distance of p from the segment defined by points a & b.
 * */
template<typename PointT>
Scalar segmentDistSq(PointT a, PointT b, PointT p, Scalar &len)
{
    auto ap = p - a;
    auto ab = b - a;
    Scalar dotprod = ab.dot(ap);
    Scalar projLenSq;
    len = 0.0;

    if (dotprod <= 0.0) {
        projLenSq = 0.0;
    } else {
        ap = ab - ap;
        dotprod = ab.dot(ap);
        if (dotprod <= 0.0) {
            projLenSq = 0.0;
            len = 1.0;
        } else {
            projLenSq = dotprod * dotprod / ab.squaredNorm();
            len = dotprod;
        }
    }

    return ap.squaredNorm() - projLenSq;
}



/**
 * Smoothing algorithm based on Savitzky-Goley filter implementation.
 * The window size is set to 5.
 * @param input_points the list of points we want to smooth. Can be a loop.
 * @param[out] res the list of points for the smoothed curve.
 */
template<typename PointT>
void Smooth(std::vector<PointT> &input_points, std::vector<PointT> &res)
{
    if (input_points.size() < 5) {
        res = input_points;
        return;
    }

    // The coefficient of the smoothing are referenced in a table but
    // We should check if there is a way to compute them
    for(unsigned int i = 0 ; i < input_points.size() ; i++){
        switch (i) {
        case 0:
            res.push_back((17*input_points[i] + 12*input_points[i+1] - 3*input_points[i+2]));
            break;
        case 1:
            res.push_back((12*input_points[i-1] + 17*input_points[i] + 12*input_points[i+1] - 3*input_points[i+2]));
            break;
        case input_points.size() - 2:
            res.push_back((-3.0*input_points[i-2] + 12*input_points[i-1] + 17*input_points[i] + 12*input_points[i+1]));
            break;
        case input_points.size() -1:
            res.push_back((-3.0*input_points[i-2] + 12*input_points[i-1] + 17*input_points[i]));
            break;
        default:
            res.push_back((-3.0*input_points[i-2] + 12*input_points[i-1] + 17*input_points[i] + 12*input_points[i+1] - 3*input_points[i+2]));
            break;
        }

    }
}


/**
 * Simplification algorithm based on Ramer-Douglas-Peucker implementation.
 * @param input_points the list of points we want to simplify. Can be a loop.
 * @param[out] res the list of points for the simplified curve.
 * @param epsilon the maximum error allowed between the resulting curve and the original one.
 */
template<typename PointT>
void Simplify(std::vector<PointT> &input_points, std::vector<PointT> &res, Scalar epsilon)
{
    if (input_points.size() < 3) {
        res = input_points;
        return;
    }

    PointT first = input_points[0];
    PointT last = input_points[input_points.size() - 1];

    int index_furthest = -1;
    double max_distance = -1.0;
    double dist = 0.0;
    double distToLine = 0.0;

    if (first == last) { // if we are in a loop, split the curve in 2 parts.
        for (unsigned int i = 1; i < input_points.size() - 1; ++i) {
            dist = (first - input_points[i]).squaredNorm();
            if (dist > max_distance) {
                index_furthest = i;
                max_distance = dist;
            }
        }
    } else {
        // finding furthest outlier
        for (unsigned int i = 1; i < input_points.size() - 1; i++) {

            dist = segmentDistSq(first, last, input_points[i], distToLine);
            if (dist > max_distance) {
                index_furthest = i;
                max_distance = dist;
            }
        }
    }

    if (max_distance > epsilon) {

        // simplify recursively

        std::vector<PointT> firstPart(input_points.begin() + 0, input_points.begin() + index_furthest + 1);
        std::vector<PointT> secondPart(input_points.begin() + index_furthest, input_points.end());

        std::vector<PointT> res1, res2;

        Simplify(firstPart, res1, epsilon);
        Simplify(secondPart, res2, epsilon);

        // concatenate results minus end/startpoint that will be the same
        res.insert(res.end(), res1.begin(), res1.end());
        res.insert(res.end(), res2.begin()+1, res2.end());
    } else {
        // simplification : on supprime les points intermédiaires
        res.push_back(first);
        res.push_back(last);
    }
}
//    template<typename PointT>
//    static void Simplify(std::vector<PointT> &input_points, std::vector<PointT> &res, Scalar epsilon)
//    {
//        std::vector<uint> indexes;
//        Simplify(input_points, indexes, epsilon);
//        for (uint i = 0; i < indexes.size())
//    }

/**
 * Computes the rotation transformation between 2 vectors.
 */
Transform CORE_API ComputeRotation(const Vector &source, const Vector &target);

/**
 * Computes the transformation between a point and another. It should only return a translation matrix.
 */
Transformation CORE_API ComputeTransformation(const Point &start, const Point &end);

/**
 * Create a 4x4 Matrix. (Created because eigen is stupid and doesn't offer a Matrix(xxxxxxx) ctor.
 */
template<typename T>
inline Matrix4T<T> CreateMatrix4(T x00, T x01, T x02, T x03,
                                      T x10, T x11, T x12, T x13,
                                      T x20, T x21, T x22, T x23,
                                      T x30, T x31, T x32, T x33)
{
    return Matrix4T<T> (
            x00, x01, x02, x03,
            x10, x11, x12, x13,
            x20, x21, x22, x23,
            x30, x31, x32, x33);

}

/**
 * Create a 3x3 Matrix. (Created because eigen is stupid and doesn't offer a Matrix(xxxxxxx) ctor.
 */
template<typename T>
inline Matrix3T<T> CreateMatrix3(T x00, T x01, T x02,
                                      T x10, T x11, T x12,
                                      T x20, T x21, T x22)
{
    return Matrix3T<T> (
            x00, x01, x02,
            x10, x11, x12,
            x20, x21, x22);
}

/**
 * Create a 2x2 Matrix. (Created because eigen is stupid and doesn't offer a Matrix(xxxxxxx) ctor.
 */
template<typename T>
inline Matrix2T<T> CreateMatrix2(T x00, T x01,
                                      T x10, T x11)
{
    return Matrix2T<T> (x00, x01,
                        x10, x11);
}

/**
 * Transform the given point to get it in the primitive's frame
 */
Point CORE_API GetPointInObjectFrame(const Point& p, const Transformation &t);

/**
 * Returns the 2D scaling matrix corresponding to the given scale vector.
 */

template<typename T = Scalar>
inline Matrix3T<T> ScalingMatrix2D(T s)
{
    return Matrix3T<T> (
        s, 0, 0,
        0, s, 0,
        0, 0, 1);
}

template<typename T = Scalar>
inline Matrix3T<T> ScalingMatrix2D(const Vector2T<T> &v)
{
    return Matrix3T<T> (
        v[0],    0, 0,
           0, v[1], 0,
           0,    0, 1);
}

/**
 * Returns the 3D scaling matrix corresponding to the given scale vector.
 */
template<typename T = Scalar>
inline Matrix4T<T> ScalingMatrix(T s)
{
    return Matrix4T<T> (
           s, 0, 0, 0,
           0, s, 0, 0,
           0, 0, s, 0,
           0, 0, 0, 1);
}

template<typename T = Scalar>
inline Matrix4T<T> ScalingMatrix(const Vector3T<T> &v)
{
    return Matrix4T<T> (
        v[0],    0,    0, 0,
           0, v[1],    0, 0,
           0,    0, v[2], 0,
           0,    0,    0, 1);
}

/**
 * Returns the translation matrix corresponding to the given translation
 * vector.
 */
template<typename T = Scalar>
inline Matrix4T<T> TranslationMatrix(const Vector3T<T> &v)
{
    return Matrix4T<T> (
        1, 0, 0, v[0],
        0, 1, 0, v[1],
        0, 0, 1, v[2],
        0, 0, 0, 1);
}

template<typename T = Scalar>
inline Matrix3T<T> TranslationMatrix(const Vector2T<T> &v)
{
    return Matrix3T<T> (
        1, 0, v[0],
        0, 1, v[1],
        0, 0, 1);
}

/**
 * Returns the translation matrix corresponding to the given translation
 * vector.
 */
template<typename T = Scalar>
inline Matrix4T<T> TranslationMatrix(T x, T y, T z)
{
    return Matrix4T<T> (
        1, 0, 0, x,
        0, 1, 0, y,
        0, 0, 1, z,
        0, 0, 0, 1.);
}

/**
 * Returns the 2D rotation matrix corresponding to the rotation with the given angle.
 *
 * @param angle rotation angle in degrees.
 */
template<typename T = Scalar>
inline Matrix3T<T> RotationMatrix2D(T angle)
{
    T ca = (T) (cos(angle * M_PI / 180.0));
    T sa = (T) (sin(angle * M_PI / 180.0));

    return Matrix3T<T>(
                ca, -sa, 0,
                sa,  ca, 0,
                 0,   0, 1
                );
}

/**
 * Returns the rotation matrix corresponding to the rotation around the x
 * axis with the given angle.
 *
 * @param angle rotation angle in degrees.
 */
template<typename T = Scalar>
inline Matrix4T<T> RotationMatrixX(T angle)
{
    T ca = (T) (cos(angle * M_PI / 180.0));
    T sa = (T) (sin(angle * M_PI / 180.0));

    return Matrix4T<T> (
        1., 0., 0., 0.,
        0., ca,-sa, 0.,
        0., sa, ca, 0.,
        0., 0., 0., 1.);
}

/**
 * Returns the rotation matrix corresponding to the rotation around the y
 * axis with the given angle.
 *
 * @param angle rotation angle in degrees.
 */
template<typename T = Scalar>
inline Matrix4T<T> RotationMatrixY(T angle)
{
    T ca = (T) (cos(angle * M_PI / 180.0));
    T sa = (T) (sin(angle * M_PI / 180.0));

    return Matrix4T<T> (
        ca, 0., sa, 0.,
        0., 1., 0., 0.,
       -sa, 0., ca, 0.,
        0., 0., 0., 1.);
}

/**
 * Returns the rotation matrix corresponding to the rotation around the z
 * axis with the given angle.
 *
 * @param angle rotation angle in degrees.
 */
template<typename T = Scalar>
inline Matrix4T<T> RotationMatrixZ(T angle)
{
    T ca = (T) (cos(angle * M_PI / 180.0));
    T sa = (T) (sin(angle * M_PI / 180.0));

    return Matrix4T<T> (
        ca,-sa, 0., 0.,
        sa, ca, 0., 0.,
        0., 0., 1., 0.,
        0., 0., 0., 1.);
}

/**
 * Returns the perspective projection matrix corresponding to the given
 * projection parameters.
 *
 * @param fovy vertical field of view in degrees.
 * @param aspect aspect ratio of the projection window.
 * @param zNear near clipping plane.
 * @param zFar far clipping plane.
 */
template<typename T = Scalar>
inline Matrix4T<T> PerspectiveProjectionMatrix(T fovy, T aspect, T zNear, T zFar)
{
    T f = (T) (1.0 / std::tan(radians(fovy) / 2.0));
    return Matrix4T<T> (
        f / aspect, 0., 0., 0.,
        0., f , 0., 0.,
        0., 0., (zFar + zNear) / (zNear - zFar), (2.*zFar*zNear) / (zNear - zFar),
        0., 0., -1., 0.);
}

/**
 * Returns the orthographic projection matrix corresponding to the given
 * projection parameters.
 *
 * @param xRight right clipping plane.
 * @param xLeft left clipping plane.
 * @param yTop top clipping plane.
 * @param yBottom bottom clipping plane.
 * @param zNear near clipping plane.
 * @param zFar far clipping plane.
 */
template<typename T = Scalar>
inline Matrix4T<T> OrthoProjectionMatrix(T xRight, T xLeft, T yTop, T yBottom, T zNear, T zFar)
{
    T tx, ty, tz;
    tx = - (xRight + xLeft) / (xRight - xLeft);
    ty = - (yTop + yBottom) / (yTop - yBottom);
    tz = - (zFar + zNear) / (zFar - zNear);
    return Matrix4T<T> (
        2. / (xRight - xLeft), 0., 0., tx,
        0., 2. / (yTop - yBottom),  0., ty,
        0., 0., -2. / (zFar - zNear)  , tz,
        0., 0., 0., 10.);
}

/**
 * Create a LookAtMatrix.
 * @param eye the origin of the camera.
 * @param center the target of the camera.
 * @param up the up vector of the camera. Necessary to get a proper orientation.
 */
template<typename T = Scalar>
inline Matrix4T<T> LookAtMatrix(const Vector3T<T> &eye, const Vector3T<T> &center, const Vector3T<T> &up)
{
    Vector3T<T> f = (center - eye).normalized();
    Vector3T<T> u = up.normalized();
    Vector3T<T> s = f.cross(u).normalized();
    u = s.cross(f);

    return Matrix4T<T> (s.x, s.y, s.z, 0.0,//-s.dot(eye),
             u.x, u.y, u.z, 0.0, //-u.dot(eye),
            -f.x,-f.y,-f.z, 0.0, //f.dot(eye),
            0,0,0,1);
}

} // close namespace VectorTools

} // Close namespace expressive

#endif // EXPRESSIVE_VECTOR_TOOLS_H_
