
#include <core/geometry/OptWeightedTriangle.h>

#include<core/geometry/WeightedTriangle.h>

#include <math.h>

//TODO:@todo : this is a temporary solution for the PrecisionBoundingBox
#include <core/geometry/OptWeightedSegment.h>

namespace expressive {

namespace core {

OptWeightedTriangle::OptWeightedTriangle(){} //TODO:@todo : if this is used, set_parameters should be called later ...

/*! \brief Build a OptWeightedSegment from a classical one.
 */
OptWeightedTriangle::OptWeightedTriangle(const WeightedTriangle &w_tri)
{
    set_parameters(w_tri);
}

// Copy constructor
OptWeightedTriangle::OptWeightedTriangle(const OptWeightedTriangle &rhs)
    : point_min_(rhs.point_min_),
      weight_min_(rhs.weight_min_),
      main_dir_(rhs.main_dir_),
      ortho_dir_(rhs.ortho_dir_),
      unit_delta_weight_(rhs.unit_delta_weight_),
      coord_max_(rhs.coord_max_),
      coord_middle_(rhs.coord_middle_),
      max_seg_length_(rhs.max_seg_length_),
      longest_dir_(rhs.longest_dir_),
      half_dir_1_(rhs.half_dir_1_),
      half_dir_2_(rhs.half_dir_2_),
      point_half_(rhs.point_half_),
      longest_dir_special_(rhs.longest_dir_special_),
      unit_normal_(rhs.unit_normal_)
{}


OptWeightedTriangle::~OptWeightedTriangle(){}

Point OptWeightedTriangle::Barycenter() const
{
    Scalar weight_middle = weight_min_+coord_middle_*unit_delta_weight_;
    Scalar weight_max = weight_min_+coord_max_*unit_delta_weight_;

    Point p_max = point_min_ + longest_dir_;
    Point p_middle = point_min_ + half_dir_1_;

    return (1.0/(weight_max+weight_middle+weight_min_)) * (weight_max*p_max + weight_middle*p_middle+weight_min_*point_min_);
}



void OptWeightedTriangle::set_parameters(const WeightedTriangle &w_tri)
{
    //TODO:@todo : This could probably be simplified !
    Scalar weight_1, weight_2;
    Point point_1, point_2;
    if (w_tri.weight_p1() < w_tri.weight_p2())
    {
        if (w_tri.weight_p1() < w_tri.weight_p3())
        {
            point_min_ = w_tri.p1();
            weight_min_ = w_tri.weight_p1();

            point_1 = w_tri.p2();
            weight_1 = w_tri.weight_p2();

            point_2 = w_tri.p3();
            weight_2 = w_tri.weight_p3();
        }
        else
        {
            point_min_ = w_tri.p3();
            weight_min_ = w_tri.weight_p3();

            point_1 = w_tri.p1();
            weight_1 = w_tri.weight_p1();

            point_2 = w_tri.p2();
            weight_2 = w_tri.weight_p2();
        }
    }
    else
    {
        if (w_tri.weight_p2() < w_tri.weight_p3())
        {
            point_min_ = w_tri.p2();
            weight_min_ = w_tri.weight_p2();

            point_1 = w_tri.p3();
            weight_1 = w_tri.weight_p3();

            point_2 = w_tri.p1();
            weight_2 = w_tri.weight_p1();
        }
        else
        {
            point_min_ = w_tri.p3();
            weight_min_ = w_tri.weight_p3();

            point_1 = w_tri.p1();
            weight_1 = w_tri.weight_p1();

            point_2 = w_tri.p2();
            weight_2 = w_tri.weight_p2();
        }
    }

    const Vector dir_1 = point_1 - point_min_;
    const Vector dir_2 = point_2 - point_min_;

    const Scalar delta_1 = weight_1 - weight_min_;
    const Scalar delta_2 = weight_2 - weight_min_;
    // this two value are always <= 0.0

    const Scalar numerical_eps = 0.0001; // threshold under which we consider weights to be equals, this is to avoid numerical unstability (at the cost of accuracy)
    if(fabs(delta_1) < numerical_eps || fabs(delta_2) < numerical_eps)
    {
        if(delta_1 < delta_2)
        { //delta_1 is closer to 0
            ortho_dir_ = dir_1;
            ortho_dir_.normalize();

            // direction of fastest variation of weight
            main_dir_ = ortho_dir_.cross(w_tri.unit_normal());
            main_dir_.normalize();
            if( main_dir_.dot(dir_2) < 0.0)
                main_dir_ *= -1.0;
        }
        else
        { //delta_2 is closer to 0
            ortho_dir_ = dir_2;
            ortho_dir_.normalize();

            // direction of fastest variation of weight
            main_dir_ = ortho_dir_.cross(w_tri.unit_normal());
            main_dir_.normalize();
            if( main_dir_.dot(dir_1) < 0.0)
                main_dir_ *= -1.0;
        }
    }
    else
    { // WARNING : numerically instable if delta_ close to zero !
        // find the point were weight equal zero along the two edges that leave from point_min_
        Scalar coord_iso_zero_dir1 = - weight_min_ / delta_1;
        Point point_iso_zero_dir1 = point_min_ + coord_iso_zero_dir1*dir_1;
        Scalar coord_iso_zero_dir2 = - weight_min_ / delta_2;
        Point point_iso_zero_dir2 = point_min_ + coord_iso_zero_dir2*dir_2;

        // along ortho_dir the weight are const
        ortho_dir_ = point_iso_zero_dir2 - point_iso_zero_dir1;
        ortho_dir_.normalize();

        // direction of fastest variation of weight
        main_dir_ = ortho_dir_.cross(w_tri.unit_normal());
        main_dir_.normalize();
        if( main_dir_.dot(dir_1) < 0.0 || main_dir_.dot(dir_2) < 0.0)
            main_dir_ *= -1.0;
    }

    Scalar coord_1 = main_dir_.dot(dir_1);    // not normalized !
    Scalar coord_2 = main_dir_.dot(dir_2);    // not normalized !

    // due to previous approximation for stability
    coord_1 = (coord_1<0.0) ? 0.0 : coord_1;
    coord_2 = (coord_2<0.0) ? 0.0 : coord_2;

    if(coord_1 > coord_2)
    {
        longest_dir_ = dir_1;

        half_dir_1_ = dir_2;
        point_half_ = point_2;
        half_dir_2_ = point_1 - point_2;

        coord_max_ = coord_1;
        coord_middle_ = (coord_2/coord_1) * coord_max_;

        unit_delta_weight_ = delta_1 / coord_max_;
    }
    else
    {
        longest_dir_ = dir_2;

        half_dir_1_ = dir_1;
        point_half_ = point_1;
        half_dir_2_ = point_2 - point_1;

        coord_max_ = coord_2;
        coord_middle_ = (coord_1/coord_2) * coord_max_;

        unit_delta_weight_ = delta_2 / coord_max_;
    }

    longest_dir_special_ = longest_dir_ / coord_max_;

    // Length of the longest segment during numerical integration
    Vector tmp = half_dir_1_ - coord_middle_*longest_dir_special_;
    max_seg_length_ = tmp.norm();
    if( ortho_dir_.dot(tmp) < 0.0 ) ortho_dir_ *= -1.0;

    unit_normal_ = w_tri.unit_normal();
}


auto UnwarpAbscissa (const core::OptWeightedTriangle& tri, Scalar t) -> Scalar
{
    // Compute approx of (exp(d*l)-1)/d
    const Scalar dt = t * tri.unit_delta_weight();
    return t * ( 1.0 + dt *( 1.0/2.0 + dt * ( 1.0/6.0 + dt * ( 1.0/24.0 + dt * ( 1.0/120.0 + dt * 1.0/720.0 ))))) ;
}
auto WarpAbscissa (const core::OptWeightedTriangle& tri, Scalar t) -> Scalar
{
    // Compute approx of ln(d*l+1)/d
    const Scalar dt = t * tri.unit_delta_weight();
    const Scalar inv_dtp2 = 1.0 / (dt + 2.0);
    Scalar sqr_dt_divdlp2 = dt * inv_dtp2;
    sqr_dt_divdlp2 *= sqr_dt_divdlp2;
    const Scalar serie_approx = 1.0 + sqr_dt_divdlp2*(
                                   (1.0/3.0) + sqr_dt_divdlp2*(
                                        (1.0/5.0) + sqr_dt_divdlp2*(
                                            (1.0/7.0) + sqr_dt_divdlp2*(
                                                (1.0/9.0) + sqr_dt_divdlp2*(
                                                    (1.0/11.0) + sqr_dt_divdlp2*(1.0/13.0) )))));
    return 2.0 * t * inv_dtp2 * serie_approx;
}
void ComputeLinePrecisionBoundingBoxes(const core::OptWeightedTriangle& tri, Scalar t,
                                       Scalar /*value*/, std::list<core::PrecisionAxisBoundingBox> &/*pboxes*/)
{
////    // should create the opt weighted segment corresponding to the abs ...
    Scalar weight = tri.weight_min() + t * tri.unit_delta_weight();

    Point p_1 = tri.point_min() + t * tri.longest_dir_special();

    Scalar length = (t<tri.coord_middle()) ? (t/tri.coord_middle()) * tri.max_seg_length()
                                           : ((tri.coord_max()-t)/(tri.coord_max() - tri.coord_middle())) * tri.max_seg_length();

    OptWeightedSegment opt_w_seg(WeightedPoint(p_1,weight), WeightedPoint(p_1+length*tri.ortho_dir(),weight));
////    opt_w_seg.GetPrecisionAxisBoundingBoxes(value,pboxes);
//////    return point_contribution_.Eval( p_1, weight, tri.ortho_dir(), length, p);

//    Point b = opt_w_seg.Barycenter();
//    AABBox current_box;
//    for(unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i)
//    {
//        current_box.min_[i] = b[i] - weight;
//        current_box.max_[i] = b[i] + weight;
//    }
//    if(weight<0.1)
//        pboxes.push_back(core::PrecisionAxisBoundingBox(current_box,0.1,0.5*0.1));
//    else
//        pboxes.push_back(core::PrecisionAxisBoundingBox(current_box,weight,0.5*weight));

}

void OptWeightedTriangle::GetPrecisionAxisBoundingBoxes(Scalar value, std::list<core::PrecisionAxisBoundingBox> &pboxes) const
{
//        //TODO:@todo : first a simple implementation using the code of OptWeightedSegment ...
// TODO:@todo : really problematic since it lead to too much memory allocation (in the management of pboxes AND the precision octree in DMC)

    // Compute the number of required samples
//    unsigned int N = 2;
    Scalar s_N = (Scalar) 2.0;

    // Compute local warp coordinates
    Scalar w_local = weight_min_;
    Scalar local_t_max = WarpAbscissa(*this, coord_max_ / w_local );

    // Compute the required number of sample
    unsigned int nb_samples = 2 * ((unsigned int)(0.5*s_N*local_t_max+1.0));
    Scalar d_step_size = local_t_max  / ((Scalar)nb_samples);

    ComputeLinePrecisionBoundingBoxes(*this, 0.0, value, pboxes);

    Scalar t = d_step_size;
    d_step_size *= 2.0;
    for(unsigned int i = 1; i < nb_samples; ++i)
    {

        ComputeLinePrecisionBoundingBoxes(*this,  UnwarpAbscissa(*this, t) * w_local + 0.0, value, pboxes);
        t += d_step_size ;
    }
//    std::cout<<nb_samples<<std::endl;
    ComputeLinePrecisionBoundingBoxes(*this, coord_max_, value, pboxes);
}


} // Close namespace core

} // Close namespace expressive
