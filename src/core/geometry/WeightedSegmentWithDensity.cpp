
#include<core/geometry/WeightedSegmentWithDensity.h>


namespace expressive {

namespace core {

///////////////
// Modifiers //
///////////////

void WeightedSegmentWithDensity::set_densities(Scalar d1, Scalar d2)
{
    density_p1_ = d1;
    density_p2_ = d2;

    ComputeDensityHelpVariables();
}

void WeightedSegmentWithDensity::set_density_p1(Scalar d1)
{
    density_p1_ = d1;

    ComputeDensityHelpVariables();
}

void WeightedSegmentWithDensity::set_density_p2(Scalar d2)
{
    density_p2_ = d2;

    ComputeDensityHelpVariables();
}

///////////////////////////////
// Homothetic space function //
///////////////////////////////

////TODO:@todo : this function is just a copy of the one in classic segment but with different function call
//Scalar WeightedSegmentWithDensity::HomotheticClippedLengthWithSharpness(const Sphere &clipping_sphere) const
//{
//    // Compute closest dist to approximate the sphere variation in function of distance for sharp skeleton
//    Scalar min_dist = this->HomotheticDistance(clipping_sphere.center_); // computation shoudl definitely be optimized
////    Scalar tmp_radius = (clipping_sphere.radius_ >= 1.0) ?
////                              1.0 + density_coeff(0)*(clipping_sphere.radius_-1.0)
////                          :   pow(clipping_sphere.radius_ , density_coeff(0));
//    Scalar coeff_tmp = 1.0/(1+density_coeff_[0]*(min_dist-1.0)*(min_dist-1.0));
//    Scalar tmp_radius = pow(clipping_sphere.radius_, (1.0-coeff_tmp)/density_coeff_[0] + 1.0 * coeff_tmp);
//            //clipping_sphere.radius_ * (1.0 / (1.0 +  density_coeff_[0] * (min_dist-1.0)) ); // this is a first test : shoUld later be improved...
//    const Scalar radius_sqr = tmp_radius * tmp_radius;

//    const Scalar density_modif = 1.0;//min_dist / (1.0 + density_coeff_[0] * (min_dist - 1.0));


////    const Scalar radius_sqr = clipping_sphere.radius_ * clipping_sphere.radius_;
//    const Vector p1_to_center = clipping_sphere.center_ - this->points_[0];

//    // we search solution t \in [0,1] such that at^2-2bt+c<=0
//    const Scalar a = this->sqrlength_             - radius_sqr*this->weight_coeff_[1]*this->weight_coeff_[1];
//    const Scalar b = (this->dir_ | p1_to_center)  + radius_sqr*this->weight_coeff_[1]*this->weight_coeff_[0];
//    const Scalar c = p1_to_center.sqrnorm() - radius_sqr*this->weight_coeff_[0]*this->weight_coeff_[0];

//    Scalar delta = b*b - a*c;
//    if(delta>=0.0)
//    {
//        if(a>0)
//        {
//            const Scalar main_root = c / (b+sqrt(delta));    // WARNING : possible instablity here ... when a -> 0 && b -> 0 (and of course a=0 and b=0)
//            if(main_root < 1.0)
//            {
//                const Scalar a_r = a*main_root;
//                if(c*a_r>0.0)
//                { // it is the same as testing the sign of the second root minus numerical instability
//                    if(main_root<0.0)
//                    {
//                        return (this->length()/(this->weight_coeff_[0]))
//                                  * ( (density_coeff_[0] * density_modif  ) * ((c<a_r) ? 1.0 : c/a_r) );
////                        return IntervalHomotheticLengthWithSharpness(0.0, (c<a_r) ? 1.0 : c/a_r);
//                    }
//                    else
//                    {
//                        return (this->length()/(this->weight_coeff_[0]))
//                                * ( (density_coeff_[0] * density_modif  ) * ( ((c>a_r) ? 1.0 : c/a_r) - main_root) );
//                        //return IntervalHomotheticLengthWithSharpness(main_root, (c>a_r) ? 1.0 : c/a_r);
//                    }
//                }
//                else
//                {
//                    return 0.0;
//                }
//            }
//            else
//            {
//                return 0.0;
//            }
//        }
//        else
//        { // if(a<=0)
//            if(this->weight_coeff_[1]>0.0)
//            {
//                const Scalar main_root = c / (b+sqrt(delta));    // WARNING : possible instablity here ... when a -> 0 && b -> 0 (and of course a=0 and b=0)
//                if(main_root < 1.0)
//                {
//                    return (this->length()/(this->weight_coeff_[0]))
//                            * ( (density_coeff_[0] * density_modif  ) * ( 1.0 - ((main_root<0.0) ? 0.0 : main_root)) );
////                    return IntervalHomotheticLengthWithSharpness((main_root<0.0) ? 0.0 : main_root, 1.0);
//                }
//                else
//                {
//                    return 0.0;
//                }
//            }
//            else
//            {
//                const Scalar main_root = c / (b-sqrt(delta));    // WARNING : possible instablity here ... when a -> 0 && b -> 0 (and of course a=0 and b=0)
//                if(main_root > 0.0)
//                {
//                    return (this->length()/(this->weight_coeff_[0]))
//                            * ( (density_coeff_[0] * density_modif ) * ( ((main_root>1.0) ? 1.0 : main_root)) );
////                    return IntervalHomotheticLengthWithSharpness(0.0, (main_root>1.0) ? 1.0 : main_root);
//                }
//                else
//                {
//                    return 0.0;
//                }
//            }
//        }
//    }
//    else
//    {
//        // there is nothing to be clipped
//        return 0.0;
//    }
//}


//Scalar WeightedSegmentWithDensity::IntervalHomotheticLengthWithSharpness(Scalar l1, Scalar l2) const
//{
//    const Scalar factor =     this->weight_coeff_[1] * density_coeff_[0]
//                            - this->weight_coeff_[0] * density_coeff_[1];


//    const Scalar abs_delta_w = (this->weight_coeff_[1] > 0.0) ? this->weight_coeff_[1] : -this->weight_coeff_[1];
//    if(abs_delta_w < 0.001)  //TODO:@todo : the limit should be studied ...
//    {
//        // Use a finit expansion of the logarithm to remove the div by weight_coeff_[1]
//        return (this->length()/(this->weight_coeff_[0]*this->weight_coeff_[0]))
//                  * (   this->weight_coeff_[0] * density_coeff_[0] * (l2-l1)
//                      - 0.5*factor * (l1*l1-l2*l2) );
//    }
//    else
//    {
//        const Scalar weight1 = this->ComputeLocalWeight(l1);
//        const Scalar weight2 = this->ComputeLocalWeight(l2);
//        return this->length_ * ( factor * log(weight2/weight1)
//                                 + this->weight_coeff_[1] * density_coeff_[1] * (l2-l1))
//                / (this->weight_coeff_[1]*this->weight_coeff_[1]);
//     }
//}



} // Close namespace core

} // Close namespace expressive
