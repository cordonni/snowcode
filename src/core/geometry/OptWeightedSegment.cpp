
#include <core/geometry/OptWeightedSegment.h>

#include<core/geometry/WeightedSegment.h>
#include<core/geometry/Sphere.h>

#include <math.h>

namespace expressive {

namespace core {

void OptWeightedSegment::set_parameters(const WeightedPoint &w_p1, const WeightedPoint &w_p2)
{
   const Scalar delta_weight =  w_p2.weight() - w_p1.weight();

    if(delta_weight<0.0)
    {
        p_min_ = w_p2;
        weight_min_ = w_p2.weight();
        inv_weight_min_ = 1.0/weight_min_;

        increase_unit_dir_ = w_p1 - w_p2;
        length_ = increase_unit_dir_.norm();
        increase_unit_dir_.normalize();

        unit_delta_weight_ = -delta_weight/length_;

        orientation_ = false;
    }
    else
    {
        p_min_ = w_p1;
        weight_min_ = w_p1.weight();
        inv_weight_min_ = 1.0/weight_min_;

        increase_unit_dir_ = w_p2 - w_p1;
        length_ = increase_unit_dir_.norm();
        increase_unit_dir_.normalize();

        unit_delta_weight_ = delta_weight/length_;

        orientation_ = true;
    }
}

void OptWeightedSegment::set_parameters(const WeightedSegment &w_seg)
{
   const Scalar delta_weight =  w_seg.weight_p2() - w_seg.weight_p1();

    if(delta_weight<0.0)
    {
        p_min_ = w_seg.p2();
        length_ = w_seg.length();
        increase_unit_dir_ = -w_seg.unit_dir();
        weight_min_ = w_seg.weight_p2();
        inv_weight_min_ = 1.0/weight_min_;
        unit_delta_weight_ = -delta_weight/length_;

        orientation_ = false;
    }
    else
    {
        p_min_ = w_seg.p1();
        length_ = w_seg.length();
        increase_unit_dir_ = w_seg.unit_dir();
        weight_min_ = w_seg.weight_p1();
        inv_weight_min_ = 1.0/weight_min_;
        unit_delta_weight_ = delta_weight/length_;

        orientation_ = true;
    }
}

///////////////////////////////
// Homothetic space function //
///////////////////////////////

Scalar OptWeightedSegment::HomotheticDistance(const Vector& p_min_to_point, Scalar uv, Scalar d2) const
{
    Scalar denum = length_ * ( weight_min_ + uv*unit_delta_weight_);
    Scalar t = 1.0;
    if(denum > 0.0)
    {
        t = (uv * weight_min_ + d2 * unit_delta_weight_) /denum;
        t = (t<0.0) ? 0.0 : ((t>1.0) ? 1.0 : t) ; // clipping (nearest point on segment not line)
    }

    const Point proj_to_point = t * length_ * increase_unit_dir_ - p_min_to_point;
    return proj_to_point.norm() / LocalWeight(t);
}

Scalar OptWeightedSegment::HomotheticDistance(const Point& point) const
{
    // Documentation : see DistanceHomothetic.pdf in convol/Documentation/Convol-Core/
    const Vector p_min_to_point = point-p_min_;
    const Scalar uv = increase_unit_dir_.dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();

    return HomotheticDistance(p_min_to_point, uv, d2);
}

void OptWeightedSegment::HomotheticDistanceAndGrad(const Vector& p_min_to_point, Scalar uv, Scalar d2,
                                                   Scalar& hdist, Vector& hgrad) const
{
    Scalar denum = length_ * ( weight_min_ + uv*unit_delta_weight_);
    Scalar t = 1.0;
    if(denum > 0.0)
    {
        t = (uv * weight_min_ + d2 * unit_delta_weight_) /denum;
        t = (t<0.0) ? 0.0 : ((t>1.0) ? 1.0 : t) ; // clipping (nearest point on segment not line)
    }

    const Point proj_to_point = p_min_to_point - t * length_ * increase_unit_dir_;
    Scalar local_w = LocalWeight(t);
    Scalar dist = proj_to_point.norm();
    hgrad = (1.0/(dist*local_w)) * proj_to_point;
    hdist = dist / local_w;
}

Scalar OptWeightedSegment::HomotheticClippedLength(const Sphere &clipping_sphere) const
{
    const Scalar radius_sqr = clipping_sphere.radius_ * clipping_sphere.radius_;
    const Vector pmin_to_center( clipping_sphere.center_ - p_min_ );

    // we search solution t \in [0,1] such that at^2-2bt+c<=0
    const Scalar a = 1.0             - radius_sqr*unit_delta_weight_*unit_delta_weight_;
    const Scalar b = increase_unit_dir_.dot(pmin_to_center)  + radius_sqr*unit_delta_weight_*weight_min_;
    const Scalar c = pmin_to_center.squaredNorm() - radius_sqr*weight_min_*weight_min_;

    Scalar delta = b*b - a*c;
    if(delta>=0.0)
    {
        const Scalar b_p_sqrt_delta = b+sqrt(delta);
        if( (b_p_sqrt_delta<0.0) || (length_*b_p_sqrt_delta<c) )
        {
            return 0.0;
        }
        else
        {
            const Scalar main_root = c / b_p_sqrt_delta;
            const Scalar a_r = a*main_root;
            return IntervalHomotheticLength( (main_root<0.0) ? 0.0 : main_root,
                                             (2.0*b<a_r+a*length_) ? c/(a_r) : length_);
        }
    }
    return 0.0;
}

inline Scalar OptWeightedSegment::IntervalHomotheticLength(const Scalar l1, const Scalar l2) const
{
    Scalar special_l = (l2-l1) / (weight_min_ + l1 * unit_delta_weight_);
    if(unit_delta_weight_ < 0.001) //TODO:@todo : the limit should be studied ...
    {
        // Use a finit expansion of the logarithm to remove the div by unit_delta_weight_
        return special_l - 0.5*(special_l*special_l)*unit_delta_weight_ ;
    }
    else
    {
        return log(unit_delta_weight_*special_l + 1.0)
               / unit_delta_weight_;
     }
}


bool OptWeightedSegment::HomotheticClippedLengthAndDerivative(const Sphere &clipping_sphere,
                                                                         Scalar uv, Scalar d2,
                                                                         Scalar &length, Scalar &length_derivative) const
{
    const Scalar alpha = unit_delta_weight_*unit_delta_weight_;
    const Scalar beta  = unit_delta_weight_*weight_min_;
    const Scalar gamma = weight_min_*weight_min_;

    const Scalar radius = clipping_sphere.radius_;
    const Scalar radius_sqr = radius*radius;

    // we search solution t \in [0,1] such that at^2-2bt+c<=0
    const Scalar a = 1.0 - radius_sqr*alpha;
    const Scalar b = uv  + radius_sqr*beta;
    const Scalar c = d2  - radius_sqr*gamma;

//    const Vector pmin_to_center( clipping_sphere.center_ - p_min_ );
//    const Scalar a = 1.0                                          - radius_sqr*alpha;
//    const Scalar b = (increase_unit_dir_ | pmin_to_center)  + radius_sqr*beta;
//    const Scalar c = pmin_to_center.sqrnorm()                     - radius_sqr*gamma;

    Scalar delta = b*b - a*c;
    if(delta>=0.0)
    {
        const Scalar sqrt_delta = sqrt(delta);
        const Scalar b_p_sqrt_delta = b+sqrt_delta;
        if( (b_p_sqrt_delta<0.0) || (length_*b_p_sqrt_delta<c) )
        {
            return false;
        }
        else
        {
            const Scalar main_root = c / b_p_sqrt_delta;
            const Scalar a_r = a*main_root;

            Scalar tmp_deriv = beta + (0.5*(alpha*c+gamma*a)+beta*b) / sqrt_delta;

            Scalar l1   = 0.0;
            Scalar d_l1 = 0.0;
            //(main_root<0.0) ? 0.0 : main_root
            if(main_root>0.0)
            {
                l1 = main_root;
                d_l1 = (gamma + l1*tmp_deriv)*(-2.0*radius/b_p_sqrt_delta);
            }

            Scalar l2   = length_;
            Scalar d_l2 = 0.0;
            //(2.0*b<a_r+a*length_) ? c/(a_r) : length_
            if(2.0*b<a_r+a*length_)
            {
                l2 = c/(a_r);
                d_l2 = (tmp_deriv+alpha*l2)*(2.0*radius/a);
            }

            IntervalHomotheticLengthAndDerivative(l1, d_l1, l2, d_l2,
                                                  length, length_derivative);
            return true;
        }
    }
    return false;
}

void OptWeightedSegment::IntervalHomotheticLengthAndDerivative(const Scalar l1, const Scalar d_l1,
                                                                            const Scalar l2, const Scalar d_l2,
                                                                            Scalar &length, Scalar &length_derivative) const
{
    const Scalar inv_w_min = 1.0 / (weight_min_ + l1 * unit_delta_weight_);
    const Scalar special_l = (l2-l1) * inv_w_min;
    const Scalar var_w = 1.0 + unit_delta_weight_ * special_l;
    if(unit_delta_weight_ < 0.001) //TODO:@todo : the limit should be studied ...
    {
        // Use a finit expansion of the logarithm to remove the div by unit_delta_weight_
        length =  special_l - 0.5*(special_l*special_l)*unit_delta_weight_ ;
        length_derivative = ( 1.0 - unit_delta_weight_ * special_l ) * (d_l2 - var_w * d_l1) * inv_w_min;
    }
    else
    {
        length = log(var_w) / unit_delta_weight_;
        length_derivative = (d_l2 / var_w - d_l1) * inv_w_min;
     }
}


bool OptWeightedSegment::HomotheticClippingSpecial(const Scalar w[3], Scalar &clipped_l1, Scalar &clipped_l2) const
{
    // we search solution t \in [0,1] such that at^2-2bt+c<=0
    const Scalar a = -w[2];
    const Scalar b = -w[1];
    const Scalar c = -w[0];

    Scalar delta = b*b - a*c;
    if(delta>=0.0)
    {
        const Scalar b_p_sqrt_delta = b+sqrt(delta);
        if( (b_p_sqrt_delta<0.0) || (length_*b_p_sqrt_delta<c) )
        {
            return false;
        }
        else
        {
            const Scalar main_root = c / b_p_sqrt_delta;
            clipped_l1 = (main_root<0.0) ? 0.0 : main_root;
            const Scalar a_r = a*main_root;
            clipped_l2 = (2.0*b<a_r+a*length_) ? c/(a_r) : length_;
            return true;
        }
    }
    return false;
}


//TODO:@todo : WARNING : recopie de code !!!
bool OptWeightedSegment::HomotheticClippingSpecial(const Scalar w[3], Scalar length, Scalar &clipped_l1, Scalar &clipped_l2)
{
    // we search solution t \in [0,1] such that at^2-2bt+c<=0
    const Scalar a = -w[2];
    const Scalar b = -w[1];
    const Scalar c = -w[0];

    Scalar delta = b*b - a*c;
    if(delta>=0.0)
    {
        const Scalar b_p_sqrt_delta = b+sqrt(delta);
        if( (b_p_sqrt_delta<0.0) || (length*b_p_sqrt_delta<c) )
        {
            return false;
        }
        else
        {
            const Scalar main_root = c / b_p_sqrt_delta;
            clipped_l1 = (main_root<0.0) ? 0.0 : main_root;
            const Scalar a_r = a*main_root;
            clipped_l2 = (2.0*b<a_r+a*length) ? c/(a_r) : length;
            return true;
        }
    }
    return false;
}


AABBox OptWeightedSegment::GetAxisBoundingBox() const
{
    //TODO:@todo : use Eigen correctly
    AABBox res;

    for(unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i)
    {
        if(increase_unit_dir_[i]>0.0)
        {
            res.min_[i] = p_min_[i];
            res.max_[i] = p_min_[i] + length_*increase_unit_dir_[i];
        }
        else
        {
            res.min_[i] = p_min_[i] + length_*increase_unit_dir_[i];
            res.max_[i] = p_min_[i];
        }
    }
    return res;
}



} // Close namespace core

} // Close namespace expressive
