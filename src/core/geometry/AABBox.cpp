#define TEMPLATE_INSTANTIATION_AABBOX

#include "core/geometry/AABBox.h"

namespace expressive
{

namespace core
{

// explicit instantiation
template class AABBoxT<Point>;
template class AABBoxT<Point2>;

}

}
