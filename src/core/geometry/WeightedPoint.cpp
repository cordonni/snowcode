#define TEMPLATE_INSTANTIATION_WEIGHTEDPOINT
#include "core/geometry/WeightedPoint.h"

namespace expressive
{

namespace core
{

// explicit instantiation
template class WeightedPointT<expressive::Point>;
template class WeightedPointT<expressive::Point2>;

}

}
