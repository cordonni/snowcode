#define TEMPLATE_INSTANTIATION_GEOMETRYPRIMITIVE
#include <core/geometry/GeometryPrimitive.h>

//#include <ork/core/Logger.h>
//#include <core/utils/BasicsToXml.h>

//using namespace std;

namespace expressive
{

namespace core
{

// explicit instantiation
template class GeometryPrimitiveT<WeightedPoint>;
template class GeometryPrimitiveT<WeightedPoint2D>;
template class GeometryPrimitiveT<PointPrimitive>;
template class GeometryPrimitiveT<PointPrimitive2D>;

//GeometryPrimitive::GeometryPrimitive(const GeometryPrimitive &rhs) : type_(rhs.type_), loop_(rhs.loop_)
//{
//    points_.insert(points_.begin(), rhs.points_.begin(), rhs.points_.end());
//}

//std::string GeometryPrimitive::ToString() const
//{
//    std::string s = Name();
//    s += " - points:";
//    for (uint i = 0; i < points_.size(); ++i) {
//        s += "[";
//        s += to_string(points_[i][0]);
//        s += ",";
//        s += to_string(points_[i][1]);
//        s += ",";
//        s += to_string(points_[i][2]);
//        s += "]";
//    }
//    s += " - type:" + to_string(type_);
//    s += " - loop:" + to_string(loop_);

//    return s;
//}

//AABBox GeometryPrimitive::GetAxisBoundingBox() const
//{
//    AABBox b;
//    for (const Point &p : points_) {
//        b.Enlarge(p);
//    }
//    return b;
//}

//BoundingSphere GeometryPrimitive::GetBoundingSphere() const
//{
//    BoundingSphere res;
////    res.radius_ = 0.5*length_;
//    res.center_ = points_[0];
//    res.radius_ = 0.0;
//    for (unsigned int i = 1; i < points_.size(); ++i) {
//        res.center_ += points_[i];
//    }
//    res.center_ /= points_.size();


//    for (unsigned int i = 0; i < points_.size(); ++i) {
//        res.radius_ = std::max((points_[i] - res.center_).length(), res.radius_);
//    }

//    return res;
//}

//TiXmlElement * GeometryPrimitive::ToXml(const char *name) const
//{
//    TiXmlElement * element;
//    if(name == NULL)
//        element = new TiXmlElement( this->Name() );
//    else
//        element = new TiXmlElement( name );

//    for (unsigned int i = 0; i < points_.size(); ++i) {
//        char n[10];
//        sprintf(n, "p%d", i);
//        TiXmlElement * element_p = PointToXml(points_[i], n);
//        element->LinkEndChild(element_p);
//    }

//    return element;
//}

////virtual TiXmlElement * ToXml(const char *name = NULL) const;

//const std::vector<Point> & GeometryPrimitive::getPoints() const
//{
//    return points_;
//}

////const std::pair<const Point&, const Point&>  GeometryPrimitive::getExtremities() const
////{
////    if (points_.size() > 1 ) {
////        return make_pair(points_[0], points_[points_.size() - 1]);
////    } else if (points_.size() == 0) {
////        return make_pair(points_[0], points_[0]);
////    } else {

////        assert(false && "Geom primitives MUST be defined at least with one point.");
////        Logger::ERROR_LOGGER->log("CORE::GEOMETRY", "Geom primitives MUST be defined at least with one point.");
//////        return (pair<const Point&, const Point&>());
////    }
////}

//const Point &GeometryPrimitive::GetStart() const
//{
//    return points_[0];
//}

//const Point &GeometryPrimitive::GetEnd() const
//{
//    return points_[points_.size() - 1];
//}

////Scalar GeometryPrimitive::getLength() const;

////void GeometryPrimitive::pointHasMoved(int index = 0);

////bool GeometryPrimitive::intersects(Point &p);
////bool GeometryPrimitive::intersects(GeometryPrimitive &p);

////bool GeometryPrimitive::contains(Point &p);
////bool GeometryPrimitive::contains(GeometryPrimitive &p);

////Scalar GeometryPrimitive::getCurvilinearLength(int index = -1);
////Scalar GeometryPrimitive::getPointAtLength(Scalar l);

}

}
