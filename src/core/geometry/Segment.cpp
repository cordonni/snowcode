#define TEMPLATE_INSTANTIATION_SEGMENT

#include <core/geometry/Segment.h>


namespace expressive {

namespace core {

// explicit instantiation
template class SegmentT<PointPrimitiveT<Point> >;
template class SegmentT<PointPrimitiveT<Point2> >;
template class SegmentT<WeightedPointT<Point> >;
template class SegmentT<WeightedPointT<Point2> >;

/////////////////////
//// Xml functions //
/////////////////////

//TiXmlElement * Segment::ToXml(const char *name) const
//{
//    TiXmlElement * element = GeometryPrimitive::ToXml(name);

//    return element;
//}

/////////////////////////////
//// Bounding-box function //
/////////////////////////////

//AABBox Segment::GetAxisBoundingBox()  const
//{
//    AABBox res;

//    for(unsigned int i = 0; i < Vector::size_; ++i)
//    {
//        if(p1_[i] < p2_[i])
//        {
//            res.min_[i] = p1_[i];
//            res.max_[i] = p2_[i];
//        }
//        else
//        {
//            res.min_[i] = p2_[i];
//            res.max_[i] = p1_[i];
//        }
//    }
//    return res;
//}

//BoundingSphere Segment::GetBoundingSphere() const
//{
//    BoundingSphere res;
//    res.radius_ = 0.5*length_;
//    res.center_ = 0.5*(p1_+p2_);

//    return res;
//}



/////////////////
//// Modifiers //
/////////////////

//void Segment::set_points(const Point& p1, const Point& p2)
//{
//    p1_ = p1;
//    p2_ = p2;
//    ComputeHelpVariables();
//}

//void Segment::set_p1(const Point& p1)
//{
//    p1_ = p1;
//    ComputeHelpVariables();
//}

//void Segment::set_p2(const Point& p2){
//    p2_ = p2;
//    ComputeHelpVariables();
//}


} // Close namespace core

} // Close namespace expressive

