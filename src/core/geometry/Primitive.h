/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef PRIMITIVE_H
#define PRIMITIVE_H

// core dependencies
#include<core/CoreRequired.h>
    // utils
    #include <core/utils/BasicsToXml.h>
    #include<core/utils/Serializable.h>

namespace expressive
{

namespace core
{

//template<typename BasePointT>
/**
 * Abstract class to represent any kind of geometry.
 * PointPrimitive and polygons (GeometryPrimitive) will inherit from this class.
 * It offers basic interface for interaction with geometry (mostly getters).
 */
class CORE_API Primitive
{
public:
    EXPRESSIVE_MACRO_NAME("Primitive")

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////
    Primitive() {}

    virtual ~Primitive() {}

    /**
     * Called when any point of this primitive has been updated.
     * This will force update for when primitives are a bit more complex than usual.
     * (for example, a segment was modified, and the point's positions need to be updated too).
     */
    virtual void updated() {}

    /**
     * Returns a serialized version of this primitive.
     */
    virtual std::string ToString() const = 0;

    /**
     * Returns a serialized version of this primitive, as an xml file.
     * @param name the name of the xml tag.
     */
    virtual TiXmlElement *ToXml(const char *name = nullptr) const;

    /**
     * The actual XML constructing function. This is probably the one you need to overload
     * if you're trying to save a new type of Primitive. It will add
     * this primitive's fields to the xml element.
     */
    virtual void AddXmlAttributes(TiXmlElement* /*element*/) const = 0;

//    virtual std::vector<BasePointT> &GetPoints() = 0;

//    virtual BasePointT &GetPoint(unsigned int index) = 0;

    /**
     * Returns the number of points of this primitive.
     */
    virtual unsigned int GetSize() const = 0;

    /**
     * Returns the euclidian length of this primitive.
     */
    virtual Scalar GetLength() const = 0;

    /**
     * Returns true if this primitive is a loop.
     */
    inline virtual bool isLoop() const {return false;}

    /**
     * Sets this primitive's loop state. end points won't be duplicated.
     */
    inline virtual void set_is_loop(bool /*loop */= true) {}

    /**
     * Returns true if this primitive is considered filled.
     */
    virtual bool isFilled() const { return false; }

    /**
     * Sets this primitive's fill state.
     */
    inline virtual void set_is_filled(bool /* isfilled*/ = true) {}

//    virtual void pointHasMoved(int index = 0) = 0;

};

}

}

#endif // PRIMITIVE_H
