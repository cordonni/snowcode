
#include "core/geometry/AdvancedVectorTools.h"

namespace expressive {

namespace VectorTools {

Point ProjectionToSegment(const Point& P, const Point& c, const Vector v, bool proj_to_primitive) {
    Scalar k = (P - c).dot(v)/(v.dot(v));

    if (proj_to_primitive) {
        if (k>1.) { return c + v; }
        if (k<0.) { return c; }
    }

    return c + k*v;
}

}

}
