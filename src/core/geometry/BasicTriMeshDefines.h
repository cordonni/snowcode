/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef BASIC_TRI_MESH_DEFINES_H
#define BASIC_TRI_MESH_DEFINES_H

#include "core/CoreRequired.h"
#include "core/utils/ListenerT.h"

#include "core/geometry/AABBox.h"

//#include "ork/render/Mesh.h"
#include "core/MeshTraits.h"

#include <list>

namespace expressive
{

namespace core
{

class AbstractTriMesh;
class BasicRay;

// TODO MESH : split in different files
// TODO MESH : check if basictrimesh::point is slower than before or not (because of overloading).
namespace basictrimesh
{

struct CORE_API Face {
    public:
    uint idx_[3];
    Face(uint id1, uint id2, uint id3);
    Face();
    ~Face();

    bool contains(const uint i) const;
};

struct CORE_API Edge {
public:
    uint idx_[2];

    Edge(uint id1, uint id2);

    Edge();

    ~Edge();

    bool contains(const uint i) const;

};

Face CORE_API operator+(const Face& f, const uint stride);
Edge CORE_API operator+(const Edge& e, const uint stride);

class CORE_API VertexHandle {
public:
    uint idx() const;
    VertexHandle(uint idx = -1);
    ~VertexHandle();
    bool is_valid() const;
    void invalidate();
    bool operator ==(const VertexHandle &rhs)const;

    bool operator !=(const VertexHandle &rhs)const;

    bool operator <(const VertexHandle &fhs)const;

    bool operator >(const VertexHandle &fhs)const;

//private :
    uint idx_;

    // this is to be used only by the iterators
    void __increment();
    void __decrement();
};

class CORE_API FaceHandle {
public:
    uint idx_;
    uint idx() const;
    FaceHandle(uint idx = -1);
    ~FaceHandle();
    bool is_valid() const;
    bool operator ==(const FaceHandle &fhs)const;
    bool operator <(const FaceHandle &fhs)const;
    bool operator >(const FaceHandle &fhs)const;
    // this is to be used only by the iterators
    void __increment();
    void __decrement();
};

class CORE_API EdgeHandle {
public:
    int idx_;
    int idx() const;
    EdgeHandle(int idx = -1);
    ~EdgeHandle();
    void invalidate();
    bool is_valid() const;
    bool operator ==(const EdgeHandle &fhs)const;
    bool operator <(const EdgeHandle &fhs)const;
    bool operator >(const EdgeHandle &fhs)const;

    // this is to be used only by the iterators
    void __increment();
    void __decrement();
};

class CORE_API VertexStatus {
public:
    int nb_face_for_vertex_;
    std::list<unsigned int> adjacent_faces_; // store all the index of faces ajacent to a vertex : note sure it should be here ...
    std::list<int> adjacent_edges_; // store all the index of edges ajacent to a vertex : note sure it should be here ...
    // should probably be a list of unsigned int ...

    VertexStatus();
    VertexStatus(const VertexStatus &vs);
    ~VertexStatus();
    void add_a_face(unsigned int index);

    void remove_a_face(unsigned int index, bool destroy_if_lonely);

    void add_an_edge(int index);

    void remove_an_edge(int index);

    void destroy();

    bool deleted() const;

    unsigned int bits() const;

    bool contains_face(const uint i) const;

    bool contains_edge(const uint i) const;
};

class CORE_API FaceStatus
{
public:
    bool is_destroyed_;
    std::list<int> adjacent_edges_;
    FaceStatus();
    ~FaceStatus();

    void destroy();

    bool deleted() const;

    unsigned int bits() const;

    void add_an_edge(int index);

    void remove_an_edge(int index);

    bool contains_edge(const uint i) const;
};

class CORE_API EdgeStatus
{
public:
    bool is_destroyed_;
    std::list<int> adjacent_faces_;

    EdgeStatus();
    ~EdgeStatus();
    void destroy();
    bool deleted() const;
    unsigned int bits() const;

    void add_a_face(int index);

    void remove_a_face(int index, bool destroy_if_lonely);

    bool contains_face(const uint i) const;
};

class CORE_API HalfedgeHandle
{   // USELESS : just here to have a garbage with the same signature as the one of openmesh ...
    HalfedgeHandle();
    ~HalfedgeHandle();
};

// Iterator
template<typename Mesh>
class VertexIter : public OpenMesh::Iterators::GenericIteratorT<Mesh, VertexHandle, Mesh, &Mesh::has_vertex_status, &Mesh::n_vertices>
{
public:
    typedef OpenMesh::Iterators::GenericIteratorT<Mesh, VertexHandle, Mesh, &Mesh::has_vertex_status, &Mesh::n_vertices> Base;

    VertexIter();
    VertexIter(const Mesh &_mesh, VertexHandle _hnd, bool _skip=false);
};

template<typename Mesh>
class FaceIter : public OpenMesh::Iterators::GenericIteratorT<Mesh, FaceHandle, Mesh, &Mesh::has_face_status, &Mesh::n_faces>
{
public:
    typedef OpenMesh::Iterators::GenericIteratorT<Mesh, FaceHandle, Mesh, &Mesh::has_face_status, &Mesh::n_faces> Base;

    FaceIter();
    FaceIter(const Mesh &_mesh, FaceHandle _hnd, bool _skip=false);
};

template<typename Mesh>
class EdgeIter : public OpenMesh::Iterators::GenericIteratorT<Mesh, EdgeHandle, Mesh, &Mesh::has_edge_status, &Mesh::n_edges>
{
public:
    typedef OpenMesh::Iterators::GenericIteratorT<Mesh, EdgeHandle, Mesh, &Mesh::has_edge_status, &Mesh::n_edges> Base;

    EdgeIter();
    EdgeIter(const Mesh &_mesh, EdgeHandle _hnd, bool _skip=false);
};

// Circulator
template<class Mesh>
class ConstVertexFaceIter
{
public:
    ConstVertexFaceIter();
    ConstVertexFaceIter(const Mesh &mesh, VertexHandle vh);
    ConstVertexFaceIter(const ConstVertexFaceIter<Mesh> &cvf_it);

    ConstVertexFaceIter &operator=(const ConstVertexFaceIter<Mesh> &cvf_it);

    bool operator==(const ConstVertexFaceIter<Mesh> &cvf_it) const;

    bool operator!=(const ConstVertexFaceIter<Mesh> &cvf_it) const;

    ConstVertexFaceIter &operator++();

    ConstVertexFaceIter &operator--();

    /// Standard dereferencing operator.
    const FaceHandle& operator*() const;

    /// Standard pointer operator.
    FaceHandle* operator->() const;

    bool is_valid() const;

private:
    Mesh const *mesh_;

    std::list<unsigned int>::const_iterator cur_, end_;

    FaceHandle f_handle_; // we are obliged to store the handle in order to be able to return it by reference (compliance to openmesh.3.0 mesh interface)
};

// Circulator
template<class Mesh>
class ConstFaceVertexIter
{
public:
    ConstFaceVertexIter();

    ConstFaceVertexIter(const Mesh &mesh, FaceHandle fh);

    ConstFaceVertexIter(const ConstFaceVertexIter<Mesh> &cfv_it);

    ConstFaceVertexIter &operator=(const ConstFaceVertexIter<Mesh> &cfv_it);

    bool operator==(const ConstFaceVertexIter<Mesh> &cfv_it) const;

    bool operator!=(const ConstFaceVertexIter<Mesh> &cfv_it) const;

    ConstFaceVertexIter &operator++();

    ConstFaceVertexIter &operator--();

    /// Standard dereferencing operator.
    const VertexHandle& operator*() const;

    /// Standard pointer operator.
    VertexHandle* operator->() const;

private:
    Mesh const *mesh_;

    Face const *face_;

    uint idx_;

    VertexHandle v_handle_; // we have to store the handle in order to be able to return it by reference (compliance to openmesh.3.0 mesh interface)
};


} // namespace basictrimesh

struct DefaultMeshVertex : public Vector
{
    Normal m_normal;
    Point2 m_uv;
    Color m_color;

    CORE_API DefaultMeshVertex();

    CORE_API DefaultMeshVertex(const Point &pos, const Vector &normal);

    CORE_API DefaultMeshVertex(const Point &pos, const Color &color);

    CORE_API DefaultMeshVertex(const Point &pos, const Vector &normal, const Color &color);

    CORE_API DefaultMeshVertex(const Point &pos, const Vector &normal, const Point2 &uv, const Color &color);

    CORE_API DefaultMeshVertex(Scalar x, Scalar y, Scalar z, Scalar nx, Scalar ny, Scalar nz, Scalar u, Scalar v, unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);

    CORE_API DefaultMeshVertex(Scalar x, Scalar y, Scalar z, Scalar nx, Scalar ny, Scalar nz, unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);

    CORE_API DefaultMeshVertex(Scalar x, Scalar y, Scalar z, unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);

    CORE_API DefaultMeshVertex(const DefaultMeshVertex &v);

    CORE_API DefaultMeshVertex(const Vector &v);

    CORE_API DefaultMeshVertex& set_color(unsigned char r, unsigned char g, unsigned char b, unsigned char a);

    CORE_API DefaultMeshVertex& set_color(unsigned char r, unsigned char g, unsigned char b);

    CORE_API DefaultMeshVertex& set_color(const Color &c);

    CORE_API DefaultMeshVertex& set_UV(Scalar u, Scalar v);

    CORE_API DefaultMeshVertex& set_UV(const Point2 &uv);

    CORE_API DefaultMeshVertex& set_normal(Scalar nx, Scalar ny, Scalar nz);

    CORE_API DefaultMeshVertex& set_normal(const Normal &n);

    CORE_API DefaultMeshVertex& set_pos(const Point &p);

    CORE_API DefaultMeshVertex& set_pos(Scalar x, Scalar y, Scalar z);

    CORE_API const Vector& normal() const;
    CORE_API const Point2& uv    () const;
    CORE_API const Color&  color () const;

    CORE_API Vector& normal();
    CORE_API Point2& uv    ();
    CORE_API Color&  color ();

    template<typename MESH>
    static void RegisterAttributeTypes(MESH mesh);

    CORE_API static bool has_vertex_colors();
    CORE_API static bool has_vertex_normals();
};

class CORE_API AbstractTriMeshListener : public ListenerT<AbstractTriMesh>
{
public:
    AbstractTriMeshListener(AbstractTriMesh *m = nullptr) : ListenerT<AbstractTriMesh>(m) {}
    virtual ~AbstractTriMeshListener() {}

    virtual void garbage_collection_done(const std::map<basictrimesh::VertexHandle, basictrimesh::VertexHandle> &vertex_handles, const std::map<basictrimesh::EdgeHandle, basictrimesh::EdgeHandle> &edge_handles, const std::map<basictrimesh::FaceHandle, basictrimesh::FaceHandle> &face_handles) = 0;
};

class CORE_API BasicTriMeshCell// : public Octree::Cell
{
public:
    BasicTriMeshCell(AbstractTriMesh *mesh, unsigned int face_idx);

    Scalar FindIntersection(const BasicRay &ray, Scalar dist_max);
    AABBox bounding_box() const;

protected:
    //AbstractTriMesh* mesh_;
    AABBox bounding_box_;
    //unsigned int face_idx_;
    Point verts[3];

    friend class AbstractTriMesh;
};

//template class ListenerT<AbstractTriMesh>;
}

}

#include "core/geometry/BasicTriMeshDefines.hpp"

#endif
