/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: PrecisionAxisBoundingBox.h

   Language: C++

   License: Convol license

   \author: Amaury Jung
   E-Mail:  amaury.jung@inrialpes.fr

   Description: Header for an axis aligned bounding box with 2 precisions values classes.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_PRECISION_AXIS_BOUNDING_BOX_T_H_
#define CONVOL_PRECISION_AXIS_BOUNDING_BOX_T_H_

#include "core/CoreRequired.h"
#include "core/geometry/AABBox.h"

namespace expressive {

namespace core {

template<typename PointT>
/**
 * TODO:@todo : comment this.
 */
class PrecisionAxisBoundingBoxT : public AABBoxT<PointT>
{
public:
    PrecisionAxisBoundingBoxT( const AABBoxT<PointT> &bbox,
                              Scalar feature_prec,
                              Scalar detail_prec)
        : AABBoxT<PointT>(bbox),
          feature_precision(feature_prec),
          detail_precision(detail_prec)
    {}

    Scalar feature_precision, detail_precision;
};

typedef PrecisionAxisBoundingBoxT<Point> PrecisionAxisBoundingBox;

} // Close namespace core

} // Close namespace expressive

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_PRECISIONAXISBOUNDINGBOX
extern template class expressive::core::PrecisionAxisBoundingBoxT<expressive::Point>;
#endif
#endif

#endif // CONVOL_PRECISION_AXIS_BOUNDING_BOX_T_H_
