#include "Frame.h"

namespace expressive {

namespace core {


const Point  Frame::DEF_P0 = Point (0,0,0);
const Vector Frame::DEF_PX = Vector(1,0,0);
const Vector Frame::DEF_PY = Vector(0,1,0);
const Vector Frame::DEF_PZ = Vector(0,0,1);


Frame::Frame(const Transform& transform):
    internal_transform_(transform)
{

}


Frame::Frame(const Frame& to_copy):
    internal_transform_(to_copy.internal_transform())
{
}


Frame::Frame(const Point& p0, const Vector& px, const Vector& py, const Vector& pz)
{
    Init(p0,px,py,pz);
}


Frame::Frame(double lx, double ly, double lz)
{
    Init(Vector(0,0,0), Vector(lx,0,0), Vector(0,ly,0), Vector(0,0,lz));
}

Frame::Frame(double l):
    Frame(l,l,l)
{
}


void Frame::Init(const Point& p0, const Vector& px, const Vector& py, const Vector& pz)
{
//    internal_transform_ = Transform::Identity();
//    internal_transform_.prescale(Vector(px.norm(), py.norm(), pz.norm()));
//    Matrix m; m.setZero(); m.col(0) = px.normalized();  m.col(1) = py.normalized();  m.col(2) = pz.normalized();
//    internal_transform_.prerotate(Quaternion(m));   /// This line constraint the frame to be orthonormal
////    internal_transform_.prerotate(m);             /// This one doesn't
//    internal_transform_.pretranslate(p0);
    auto npx = px.normalized();
    auto npy = py.normalized();
    auto npz = pz.normalized();
    Matrix3 m = Matrix3(npx.x, npy.x, npz.x,
                        npx.y, npy.y, npz.y,
                        npx.z, npy.z, npz.z);
//    m.col(0) = px.normalized();  m.col(1) = py.normalized();  m.col(2) = pz.normalized();
    internal_transform_ = Transform::translate(p0) * Quaternion(m).toMat4() * Transform::scale(Vector(px.norm(), py.norm(), pz.norm()));
}

Point  Frame::p0() const
{
    return internal_transform_.translation();
}


Vector Frame::px() const
{
    return internal_transform_ * Vector(1,0,0) - p0();
}


Vector Frame::py() const
{
    return internal_transform_ * Vector(0,1,0) - p0();
}


Vector Frame::pz() const
{
    return internal_transform_ * Vector(0,0,1) - p0();
}


Frame& Frame::translate(const Vector& v)
{
    internal_transform_.pretranslate(v);
    return *this;
}


Frame& Frame::rotate(const Vector& axis, const Scalar angle)
{
    internal_transform_.prerotate(Quaternion(axis.normalized(), angle).toMat4());
    //internal_transform_.prerotate(Eigen::AngleAxis<Scalar>(angle, axis.normalized()));
    return *this;
}


Frame& Frame::rotate(const Vector& normaxis)
{
    internal_transform_.prerotate(Quaternion(normaxis.normalized(), normaxis.norm()).toMat4());
    //internal_transform_.prerotate(Eigen::AngleAxis<Scalar>(normaxis.norm(), normaxis.normalized()));
    return *this;
}


Frame& Frame::rotate(const Quaternion& q)
{
    internal_transform_.prerotate(q.toMat4());
    return *this;
}


Frame& Frame::scale(const Scalar& s)
{
    internal_transform_.prescale(s);
    return *this;
}


Frame& Frame::scale(const Vector& s)
{
    internal_transform_.prescale(s);
    return *this;
}


Frame& Frame::transform(const Transform& t)
{
    internal_transform_ = t * internal_transform_;
    return *this;
}


Frame& Frame::movePz(const Vector& v)
{
    Vector m_p0 = p0();
    Vector old_px = m_p0 + pz();
    Vector new_px = old_px + v;

    double r = (new_px-m_p0).norm() / (old_px-m_p0).norm();

    expressive::Transform t = expressive::Transform::Identity();

    t = internal_transform().inverse();
    t.prescale(Vector(1,1,r));
    t = internal_transform() * t;

    t.pretranslate(-m_p0);
    t.prerotate(Quaternion(old_px-m_p0, new_px-m_p0).toMat4());
    t.pretranslate(m_p0);

    return transform(t);
}


const Transform& Frame::internal_transform() const
{
    return internal_transform_;
}


Transform& Frame::internal_transform()
{
    return internal_transform_;
}


Frame Frame::normalized(const double lx, const double ly, const double lz) const
{
    return Frame(p0(), px().normalized() * lx, py().normalized() * ly, pz().normalized() * lz);
}

Frame &Frame::normalize(const Vector& l)
{
    return normalize(l[0], l[1], l[2]);
}

Frame &Frame::normalize(const double lx, const double ly, const double lz)
{
    *this = normalized(lx, ly, lz);

    return *this;
}


Frame &Frame::normalize(const double l)
{
    return normalize(l,l,l);
}

Vector Frame::Embed(const expressive::Point &p3d) const
{
    return p0() + px()*p3d[0] + py()*p3d[1] + pz()*p3d[2];
}

Vector Frame::UnEmbed(const expressive::Point &p3d) const
{
    return Vector(p3d.dot(px()), p3d.dot(py()), p3d.dot(pz()));
}

Vector Frame::EmbedXY(const expressive::Point2 &p2d) const
{
    return p0() + px()*p2d[0] + py()*p2d[1];
}


Vector Frame::EmbedYZ(const expressive::Point2 &p2d) const
{
    return p0() + py()*p2d[0] + pz()*p2d[1];
}


Vector Frame::EmbedZX(const Point2 &p2d) const
{
    return p0() + pz()*p2d[0] + px()*p2d[1];
}


Point Frame::ProjectX(const Vector &p3d) const
{
    return p0() + (p3d - p0()).dot(px().normalized()) * px().normalized();
}


Point Frame::ProjectY(const Vector &p3d) const
{
    return p0() + (p3d - p0()).dot(py().normalized()) * py().normalized();
}


Point Frame::ProjectZ(const Vector &p3d) const
{
    return p0() + (p3d - p0()).dot(pz().normalized()) * pz().normalized();
}


Point2 Frame::ProjectXY(const Vector &p3d) const
{
//    Point2 X(px()[0], px()[1]);
//    Point2 Y(py()[0], py()[1]);
    Point2 X(1, 0);
    Point2 Y(0, 1);

    double nx = px().norm();
    double ny = py().norm();

    Vector P = p3d - p0();
//    return P.dot(px()) * X + P.dot(py()) * Y;
    return P.dot(px()) * X /nx/nx + P.dot(py()) * Y /ny/ny;
}


Point2 Frame::ProjectYZ(const Vector &p3d) const
{
    Point2 Y(py()[0], py()[1]);
    Point2 Z(pz()[0], pz()[1]);
    Vector P = p3d - p0();
    return P.dot(py()) * Y + P.dot(pz()) * Z;
}


Point2 Frame::ProjectZX(const Vector &p3d) const
{
    Point2 Z(pz()[0], pz()[1]);
    Point2 X(px()[0], px()[1]);
    Vector P = p3d - p0();
    return P.dot(pz()) * Z + P.dot(px()) * X;
}



Frame Frame::InterpolateSimple(const Frame& f0, const Frame& f1, const Scalar t)
{
    Vector p0_0 = f0.p0();
    Scalar lx_0 = f0.px().norm();
    Scalar ly_0 = f0.py().norm();
    Scalar lz_0 = f0.pz().norm();

    Vector p0_1 = f1.p0();
    Scalar lx_1 = f1.px().norm();
    Scalar ly_1 = f1.py().norm();
    Scalar lz_1 = f1.pz().norm();

    Vector px = ((1.0 - t) * f0.px() + t * f1.px()).normalized() * ((1.0 - t) * lx_0 + t * lx_1);
    Vector py = ((1.0 - t) * f0.py() + t * f1.py()).normalized() * ((1.0 - t) * ly_0 + t * ly_1);
    Vector pz = ((1.0 - t) * f0.pz() + t * f1.pz()).normalized() * ((1.0 - t) * lz_0 + t * lz_1);
    Vector p0 = (1.0 - t) * p0_0 + t * p0_1;

    expressive::Transform trans(px.x, py.x, pz.x, p0.x,
                                px.y, py.y, pz.y, p0.y,
                                px.z, py.z, pz.z, p0.z,
                                0.0, 0.0, 0.0, 1.0);

//    trans.matrix().col(0) = px;
//    trans.matrix().col(1) = py;
//    trans.matrix().col(2) = pz;
//    trans.matrix().col(3) = p0;

    return Frame(trans);
}


Frame Frame::InterpolateSlerp(const Frame& f0, const Frame& f1, const Scalar t)
{
    Vector p0_0 = f0.p0();
    Scalar lx_0 = f0.px().norm();
    Scalar ly_0 = f0.py().norm();
    Scalar lz_0 = f0.pz().norm();

    Vector p0_1 = f1.p0();
    Scalar lx_1 = f1.px().norm();
    Scalar ly_1 = f1.py().norm();
    Scalar lz_1 = f1.pz().norm();

    expressive::Transform trans = expressive::Transform::Identity();

    trans.scale((1.0 - t) * Vector(lx_0, ly_0, lz_0) + t * Vector(lx_1, ly_1, lz_1));

    Quaternion q0(f0.internal_transform().rotation());
    Quaternion q1(f1.internal_transform().rotation());
    trans = trans * q0.slerp(t, q1).toMat4();

    trans.translate((1.0 - t) * p0_0 + t * p0_1);

    return Frame(trans);
}


} // end of namespace core

} // end of namespace expressive
