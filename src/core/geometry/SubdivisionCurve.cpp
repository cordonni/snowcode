#include "core/geometry/SubdivisionCurve.h"

namespace expressive
{

namespace core
{

template<>
void SubdivisionCurveT<WeightedPoint>::ComputeSubdivision()
{ //TODO:@todo : naive implementation ..., can be easily changed to obtain another averaging mask
    subdivision_.clear();

    std::vector<WeightedPoint> res_points = GeometryPrimitiveT<WeightedPoint>::points_;
    std::vector<WeightedPoint> tmp_points;
    for(int i = 0; i<nb_subdiv_; ++i)
    {
        // subdivision process
        tmp_points.resize(res_points.size()*2-1);
        for(unsigned int j = 0; j<tmp_points.size(); ++j)
        {
            int j_div2 = j/2;
            if(j%2 == 0) {
                tmp_points[j].set_pos(res_points[j_div2]);
                tmp_points[j].set_weight(res_points[j_div2].weight());
            } else {
                tmp_points[j].set_pos(0.5*(res_points[j_div2] + res_points[j_div2+1]));
                tmp_points[j].set_weight(0.5*(res_points[j_div2].weight() + res_points[j_div2+1].weight()));
            }

        }

        //averaging process
        res_points.resize(tmp_points.size());
        res_points.front() = tmp_points.front();
        for(unsigned int j = 1; j<tmp_points.size()-1; ++j)
        {
            res_points[j].set_pos(0.5*(tmp_points[j] + tmp_points[j+1]));

            res_points[j].set_weight(0.5*(tmp_points[j].weight() + tmp_points[j+1].weight()));
        }
        res_points.back() = tmp_points.back();
    }

    // should use a move operator ?
    subdivision_ = res_points;
}

template class SubdivisionCurveT<PointPrimitiveT<Point> >;
template class SubdivisionCurveT<PointPrimitiveT<Point2> >;
template class SubdivisionCurveT<WeightedPointT<Point> >;
template class SubdivisionCurveT<WeightedPointT<Point2> >;


}

}
