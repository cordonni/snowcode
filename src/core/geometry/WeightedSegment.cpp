
#include <core/geometry/WeightedSegment.h>
#include <core/geometry/WeightedPoint.h>

using namespace std;

namespace expressive {

namespace core {


/*! \brief Build a constant 1.0-weighted segment along the first axis.
 */
WeightedSegment::WeightedSegment() :
      SegmentT()
//          p1_.weight()(1.0), p2_.weight()(1.0)
{
    points_[0].set_weight(1.0);
    points_[1].set_weight(1.0);
    weight_coeff_[0] = 1.0;
    weight_coeff_[1] = 0.0;

    ComputeHelpVariables();
//        opt_w_seg_.set_parameters(*this);
}

/*! \brief Build a linear weighted segment,
 *         if weights are not given create a 1.0-weighted segment
 *   \param p1 First segment point
 *   \param p2 Second segment point
 *   \param weight1 Weight for first segment point (default value is 1.0)
 *   \param weight2 Weight for second segment point (default value is 1.0)
 */
WeightedSegment::WeightedSegment(const Point& p1, const Point& p2,
                Scalar weight1, Scalar weight2) :
      SegmentT(WeightedPoint(p1, weight1), WeightedPoint(p2, weight2))
//          p1_.weight()(weight1), p2_.weight()(weight2)
{
    weight_coeff_[0] = weight1;
    weight_coeff_[1] = weight2 - weight1;

    ComputeHelpVariables();
//        opt_w_seg_.set_parameters(*this);
}

WeightedSegment::WeightedSegment(const WeightedPoint& p1, const WeightedPoint& p2) :
      SegmentT(p1, p2)
//          p1_.weight()(p1.weight()), p2_.weight()(p2.weight())
{
    weight_coeff_[0] = p1.weight();
    weight_coeff_[1] = p2.weight() - p1.weight();
    ComputeHelpVariables();
//        opt_w_seg_.set_parameters(*this);
}

// Copy constructor
WeightedSegment::WeightedSegment(const WeightedSegment &rhs, unsigned int start, unsigned int end) :
        SegmentT(rhs, start, end),
        weight_coeff_(rhs.weight_coeff_),
//            weight_p1_(rhs.weight_p1_), weight_p2_(rhs.weight_p2_),
        opt_w_seg_(rhs.opt_w_seg_)
{}

WeightedSegment::~WeightedSegment(){}

std::shared_ptr<GeometryPrimitiveT<WeightedPoint> > WeightedSegment::clone(unsigned int start, unsigned int end) const {
    return std::make_shared<WeightedSegment>(*this, start, end);
} // Todo : yew.. this is ugly.

void WeightedSegment::ComputeHelpVariables()
{
    SegmentT::ComputeHelpVariables();

    weight_coeff_[0] = points_[0].weight();
    weight_coeff_[1] = points_[1].weight() - points_[0].weight();
    opt_w_seg_.set_parameters(*this);
}


///////////////////
// Xml functions //
///////////////////

std::string WeightedSegment::ToString() const
{
    std::string s = SegmentT::ToString();
    s += " - weights: [";
    s += to_string(points_[0].weight()) + "," + to_string(points_[1].weight());
    s += "]";

    return s;
}

TiXmlElement * WeightedSegment::ToXml(bool embed_vertices, const char *name) const
{
    TiXmlElement * element = SegmentT::ToXml(embed_vertices, name);

    if (embed_vertices) {
        element->SetDoubleAttribute("weight_p1",points_[0].weight());
        element->SetDoubleAttribute("weight_p2",points_[1].weight());
    }

    return element;
}

///////////////
// Modifiers //
///////////////

void WeightedSegment::set_weight_coeff( std::array<Scalar,2>& weight_coeff)
{
    weight_coeff_ = weight_coeff;
    points_[0].set_weight(weight_coeff_[0]);

    Scalar weight = 0.0;
    for(auto it = weight_coeff_.begin(); it != weight_coeff_.end(); ++it)
    {
        weight += (*it);
    }
    points_[1].set_weight(weight);

    opt_w_seg_.set_parameters(*this);
}

void WeightedSegment::set_weights(Scalar w1, Scalar w2)
{
    points_[0].set_weight(w1);
    points_[1].set_weight(w2);
    weight_coeff_[0] = points_[0].weight();
    weight_coeff_[1] = points_[1].weight() - points_[0].weight();

    opt_w_seg_.set_parameters(*this);
}

void WeightedSegment::set_weight_p1(Scalar w1)
{
    points_[0].set_weight(w1);
    weight_coeff_[0] = points_[0].weight();
    weight_coeff_[1] = points_[1].weight() - points_[0].weight();

    opt_w_seg_.set_parameters(*this);
}

void WeightedSegment::set_weight_p2(Scalar w2)
{
    points_[1].set_weight(w2);
    weight_coeff_[0] = points_[0].weight();
    weight_coeff_[1] = points_[1].weight() - points_[0].weight();

    opt_w_seg_.set_parameters(*this);
}

void WeightedSegment::updated()
{
    GeometryPrimitiveT<WeightedPoint>::updated();
    ComputeHelpVariables(); //TODO:@todo : there is an asymetrie between the way point are modified and other parameters...
}

///////////////////////////////
// Homothetic space function //
///////////////////////////////

Scalar WeightedSegment::HomotheticDistance(const Point& point) const
{
    // Documentation : see DistanceHomothetic.pdf in convol/Documentation/Convol-Core/
    const Vector p1_to_p = point - points_[0];

    const Scalar p1_p_scal_dir = p1_to_p.dot(dir_);
    const Scalar p1_p_sqr = p1_to_p.squaredNorm();

    Scalar denum = sqrlength_ * weight_coeff_[0] + p1_p_scal_dir * weight_coeff_[1];
    Scalar t = (weight_coeff_[1]<0.0) ? 0.0 : 1.0;
    if(denum > 0.0)
    {
        t = (p1_p_scal_dir * weight_coeff_[0] + p1_p_sqr * weight_coeff_[1]) /denum;
        t = (t<0.0) ? 0.0 : ((t>1.0) ? 1.0 : t) ; // clipping (nearest point on segment not line)
    }

    const Point proj_to_point = t * dir_ - p1_to_p;
    return proj_to_point.norm() / ComputeLocalWeight(t);
}

bool WeightedSegment::HomotheticClipping(const Sphere &clipping_sphere, Scalar &clipped_l1, Scalar &clipped_l2) const
{
    const Scalar radius_sqr = clipping_sphere.radius_ * clipping_sphere.radius_;
    const Vector p1_to_center = clipping_sphere.center_ - points_[0];
    Scalar special_coeff[3] = {radius_sqr*weight_coeff_[0]*weight_coeff_[0]   - p1_to_center.squaredNorm() ,
                               - radius_sqr*weight_coeff_[1]*weight_coeff_[0] - dir_.dot(p1_to_center) ,
                               radius_sqr*weight_coeff_[1]*weight_coeff_[1]   - sqrlength_ };

    return HomotheticClipping(special_coeff, clipped_l1, clipped_l2);
}

bool WeightedSegment::HomotheticClipping(const Scalar w[3], Scalar &clipped_l1, Scalar &clipped_l2) const
{
    // we search solution t \in [0,1] such that at^2-2bt+c<=0
    const Scalar a = -w[2];
    const Scalar b = -w[1];
    const Scalar c = -w[0];

    Scalar delta = b*b - a*c;
    if(delta>=0.0)
    {
        if(a>0)
        {
            const Scalar main_root = c / (b+sqrt(delta));    // WARNING : possible instablity here ... when a -> 0 && b -> 0 (and of course a=0 and b=0)
            if(main_root < 1.0)
            {
                const Scalar a_r = a*main_root;
                if(c*a_r>0.0)
                { // it is the same as testing the sign of the second root minus numerical instability
                    if(main_root<0.0)
                    {
                        clipped_l1 = 0.0;
                        clipped_l2 = (c<a_r) ? 1.0 : c/a_r; // prevent instability ...
                        return true;
                    }
                    else
                    {
                        clipped_l1 = main_root;
                        clipped_l2 = (c>a_r) ? 1.0 : c/a_r; // prevent instability ...
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        { // if(a<=0)
            if(this->weight_coeff_[1]>0.0)
            {
                const Scalar main_root = c / (b+sqrt(delta));    // WARNING : possible instablity here ... when a -> 0 && b -> 0 (and of course a=0 and b=0)
                if(main_root < 1.0)
                {
                    clipped_l1 = (main_root<0.0) ? 0.0 : main_root;
                    clipped_l2 = 1.0;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                const Scalar main_root = c / (b-sqrt(delta));    // WARNING : possible instablity here ... when a -> 0 && b -> 0 (and of course a=0 and b=0)
                if(main_root > 0.0)
                {
                    clipped_l1 = 0.0;
                    clipped_l2 = (main_root>1.0) ? 1.0 : main_root;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
    else
    {
        // there is nothing to be clipped
        return false;
    }
}


} // Close namespace core

} // Close namespace expressive
