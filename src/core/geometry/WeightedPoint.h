/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: WeightedPoint.h

   Language: C++

   License: Expressive license

   \author: Galel Koraa
   E-Mail:  galel.koraa@inria.fr

   Description: Header for a a very simple class of a weighted point.

   Platform Dependencies: None
*/

#pragma once
#ifndef WEIGHTED_POINT_H_
#define WEIGHTED_POINT_H_


// core dependencies
#include<core/CoreRequired.h>
    // geometry
    #include<core/geometry/AABBox.h>
    #include<core/geometry/BoundingSphere.h>
    #include<core/geometry/PointPrimitive.h>
    // utils
    #include<core/utils/Serializable.h>

namespace expressive {

namespace core {

/**
 * WeightedPoint adds a weight to a point. It can then be used as a radius,
 * a barycenter weight, or any other type of info.
 * A Segment using 2 WeightedPoints is a cut cylinder if both have the same weight, of a cut cone otherwise.
 */
template<typename BasePointT>
class WeightedPointT : public PointPrimitiveT<BasePointT>
{
public:
    EXPRESSIVE_MACRO_NAME("WeightedPoint")

    typedef BasePointT BasePoint;

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    WeightedPointT() : PointPrimitiveT<BasePoint>(), weight_(1.0) { }

    WeightedPointT(const PointPrimitiveT<BasePoint>& pt, Scalar weight) : PointPrimitiveT<BasePoint>(pt), weight_(weight) { }

//    WeightedPoint(Scalar x) : PointT(x), weight_(1.0) {}

    //TODO:@todo : argh : only work in dim 3 and 2
    WeightedPointT(Scalar x, Scalar y, Scalar z, Scalar weight) : PointPrimitiveT<BasePoint>(x, y, z), weight_(weight) { }
    WeightedPointT(Scalar x, Scalar y, Scalar weight) : PointPrimitiveT<BasePoint>(x, y), weight_(weight) { }

    WeightedPointT(const WeightedPointT &rhs) : PointPrimitiveT<BasePoint>(rhs), weight_(rhs.weight_) { }

    virtual ~WeightedPointT(){ }

    virtual void updated() {}

    virtual std::shared_ptr<PointPrimitiveT<BasePoint> > clone() const {return std::make_shared<WeightedPointT<BasePoint> >(*this); }
//    virtual std::shared_ptr<WeightedPointT<BasePoint> > clone() const {return std::make_shared<WeightedPointT<BasePoint> >(*this); }

    ///////////////////
    // Xml functions //
    ///////////////////

    virtual std::string ToString() const;

    virtual TiXmlElement * ToXml(const char *name = NULL) const;

    ///////////////////////////
    // Bounding-box function //
    ///////////////////////////

    virtual AABBoxT<BasePointT> GetAxisBoundingBox() const;

    virtual BoundingSphereT<BasePoint> GetBoundingSphere() const;

    ///////////////
    // Modifiers //
    ///////////////    

//    void set_positon(const Point& position) { position_ = position; }
    void set_weight (Scalar weight)   { weight_ = weight; }

    ///////////////
    // Accessors //
    ///////////////

//    const Point& position() const { return position_; }
    Scalar weight()  const { return weight_; }


    bool operator==(const WeightedPointT& p) const
    {
//        return (weight_ == p.weight_) && (this->array() == p.array()).all();
        if (weight_ != p.weight_) {
            return false;
        }
        for (unsigned int i = 0; i < BasePoint::RowsAtCompileTime; ++i) {
            if ((*this)[i] != p[i]) {
                return false;
            }
        }
        return true;
    }

    bool operator<(const WeightedPointT& p) const
    {
        for (unsigned int i = 0; i < BasePoint::RowsAtCompileTime; ++i) {
            if ((*this)[i] < p[i]) {
                return true;
            } else if ((*this)[i] > p[i]) {
                return false;
            }
        }

        return ((*this).weight() < p.weight());
    }

    WeightedPointT& operator=(const PointPrimitiveT<BasePoint>& p)
    {
        this->PointPrimitiveT<BasePoint>::operator=(p);
        return *this;
    }

    WeightedPointT& operator=(const WeightedPointT& p)
    {
        this->BasePoint::operator=(p);
        weight_ = p.weight_;
        return *this;
    }

    void set_pos(const PointPrimitiveT<BasePoint> &p)
    {
        this->array() = p.array();
    }

    void set_pos(const WeightedPointT &p)
    {
        this->array() = p.array();
        set_weight(p.weight());
    }

    virtual WeightedPointT& MoveToBarycenter(const std::set<WeightedPointT> &primitives)
    {
        for (unsigned int i = 0; i < BasePoint::RowsAtCompileTime; ++i) {
            (*this)[i] *= weight_;
        }

        for (auto p : primitives) {
            for (unsigned int i = 0; i < BasePoint::RowsAtCompileTime; ++i) {
                (*this)[i] += p[i] * p.weight_;
            }

            weight_ += p.weight_;
        }

        if (weight_ != 0.0) {
            for (unsigned int i = 0; i < BasePoint::RowsAtCompileTime; ++i) {
                (*this)[i] /= weight_;
            }
        }
        weight_ /= (Scalar) primitives.size() + 1.0;

        return *this;
    }

protected:
//    Point position_;
    Scalar weight_;
};

typedef WeightedPointT<expressive::Point> WeightedPoint;
typedef WeightedPointT<expressive::Point2> WeightedPoint2D;
//typedef PrimitiveT<WeightedPoint> WeightedPrimitive;

} // Close namespace core

} // Close namespace expressive

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_WEIGHTEDPOINT
    extern template class expressive::core::WeightedPointT<expressive::Point>;
    extern template class expressive::core::WeightedPointT<expressive::Point2>;
#endif
#endif

// implementation file
#include<core/geometry/WeightedPoint.hpp>

#endif // WEIGHTED_POINT_H_
