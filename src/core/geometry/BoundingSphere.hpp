
namespace expressive {

namespace core {

// Returns the smallest sphere that contains the intersection of this and other
template<typename PointT>
BoundingSphereT<PointT> BoundingSphereT<PointT>::Intersection(const BoundingSphereT<PointT>& other)
{
    BoundingSphere res;

    Scalar l = (center_ - other.center_).norm();

    if(radius_ >= other.radius_ + l)
    {
        res = other;
    }
    else if ( other.radius_ >= radius_ + l)
    {
        res = (*this);
    }
    else if (radius_ + other.radius_ <= l)
    {
        // return a 0.0 radius sphere centered at equal distance from center_ and other.center_
        res.center_ = ((0.5*(l+radius_-other.radius_))/l)*(other.center_ - center_);
        res.radius_ = 0.0;
    }
    else
    {
        Scalar lprime = (l*l + radius_*radius_ - other.radius_*other.radius_)/(2.0*l);
        res.center_ = (lprime/l)*(other.center_ - center_);
        res.radius_ = sqrt(radius_*radius_ - lprime*lprime);
    }

    return res;
}

// Returns the smallest sphere that contains the intersection of this and other
template<typename PointT>
BoundingSphereT<PointT> BoundingSphereT<PointT>::Union(const BoundingSphereT<PointT>& other)
{
    BoundingSphere res;

    Vector unit_vect = other.center_ - center_;
    res.radius_ = 0.5*(radius_ + other.radius_ + unit_vect.norm());
    unit_vect.normalize();
    res.center_ = 0.5*(center_ - radius_*unit_vect + other.center_ + other.radius_*unit_vect);

    return res;
}

// Very special distance given as the max of the distance between 1 point on this and 1 point on other
template<typename PointT>
Scalar BoundingSphereT<PointT>::HierarchyDistance(const BoundingSphereT<PointT>& other)
{
    Vector unit_vect = other.center_ - center_;
    unit_vect.normalize();
    return (center_- other.center_ - (radius_ + other.radius_)*unit_vect ).norm(); //( (center_ - radius_*unit_vect) - (other.center_ + other.radius_*unit_vect) );
}

} // Close namespace core

} // Close namespace expressive

