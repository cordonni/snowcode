/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: GeometryTools.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Geometry tools (intersections, etc..)

   Platform Dependencies: None
*/


#pragma once
#ifndef CONVOL_GEOMETRY_TOOLS_H_
#define CONVOL_GEOMETRY_TOOLS_H_

#include<core/CoreRequired.h>

#include<core/geometry/Sphere.h>

namespace expressive {

namespace core {

class Geometry
{
public:

/** \brief Find intersection between a line and a sphere.
*
*    \param sphere The sphere to check against.
*    \param start A point on the line, which we consider as the "start"
*    \param direction Direction of the line
*    \param inter_1 Resulting first intersection if does exist such. The actual intersection point is at start+inter_1*direction.
*    \param inter_2 Resulting second intersection if does exist such. The actual intersection point is at start+inter_2*direction.
*
*    \return number of intersection (0,1 or 2)
*/
static int Intersection( const Sphere& sphere,
                         const Point& start,
                         const Vector direction,
                         Scalar& inter_1,
                         Scalar& inter_2)
{
    Scalar a = direction.squaredNorm();
    Scalar b = 0.0;
    Scalar c = -sphere.radius_*sphere.radius_;
    for(unsigned int i = 0; i<Point::RowsAtCompileTime; ++i)
    {
        Scalar aux = start[i]-sphere.center_[i];
        b += direction[i]*aux;
        c += aux*aux;
    }
    b *= 2.0;

    Scalar delta = b*b - 4.0*a*c;

    if(delta<0.0)
    {
        return 0;
    }
    else if (delta==0.0)
    {
        inter_1 = -b/(2.0*a);
        return 1;
    }
    else
    {
        Scalar sqrt_delta = sqrt(delta);
        inter_1 = (-b-sqrt_delta)/(2.0*a);
        inter_2 = (-b+sqrt_delta)/(2.0*a);
        return 2;
    }
}


}; // End class Geometry declaration

} // Close namespace core
} // Close namespace expressive

#endif // CONVOL_GEOMETRY_TOOLS_H_
