/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: Segment.h

   Language: C++

   License: Expressive license

   \author: Maxime Quiblier, Cedric Zanni
   E-Mail:  maxime.quiblier@inrialpes.fr, cedric.zanni@inria.fr

   Description: Header for Segment.

   Platform Dependencies: None
*/

#pragma once
#ifndef SEGMENT_H_
#define SEGMENT_H_

// core dependencies
#include<core/CoreRequired.h>
    // geometry
    #include<core/geometry/AABBox.h>
    #include<core/geometry/BoundingSphere.h>
    #include<core/geometry/GeometryPrimitive.h>
    // utils
    #include<core/utils/Serializable.h>

#include <math.h>

namespace expressive {

namespace core {

template<typename PointT>
/**
 * A segment : A GeometryPrimitive with only 2 points. Mostly present to improve computations
 * for intersections and length.
 * It stores "helping variable" that allows to not recompute stuff like length etc, which can be costly if
 * done many times, and that we don't want to see in basic GeometryPrimitives
 */
class SegmentT : public GeometryPrimitiveT<PointT>//public Serializable
{
public:
    EXPRESSIVE_MACRO_NAME("Segment")

    typedef typename PointT::BasePoint BasePoint;
    typedef typename PointT::BasePoint BaseVector;
    typedef PointT VectorT;

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    /*! \brief Build a segment along the first axis.
     */
    SegmentT() :
        GeometryPrimitiveT<PointT>()
    {
        //TODO:@todo : kind of ugly but dont see other solution ...
        GeometryPrimitiveT<PointT>::points_.push_back(PointT());
        GeometryPrimitiveT<PointT>::points_.push_back(PointT());
        GeometryPrimitiveT<PointT>::points_[1][0] = 1.0;
        ComputeHelpVariables();
    }

    /*! \brief Build a segment,
     *   \param p1 First segment point
     *   \param p2 Second segment point
     */
    SegmentT(const PointT& p1, const PointT& p2) :
        GeometryPrimitiveT<PointT>()
    {
        GeometryPrimitiveT<PointT>::points_.push_back(p1);
        GeometryPrimitiveT<PointT>::points_.push_back(p2);
        ComputeHelpVariables();
    }

    SegmentT(const SegmentT &rhs, unsigned int start = -1, unsigned int end = -1)
        :   GeometryPrimitiveT<PointT>(rhs, start, end)
    {
        ComputeHelpVariables();
    }

//    using GeometryPrimitiveT<PointT>::operator delete; // necessary because of eigen's macro.
    virtual ~SegmentT(){}

    virtual std::shared_ptr<GeometryPrimitiveT<PointT> > clone(unsigned int start = - 1, unsigned int end = -1) const {
        return std::make_shared<SegmentT<PointT> >(*this, start, end);
    } // Todo : yew.. this is ugly.

//    virtual const PointT &GetStart() const { return GeometryPrimitiveT<PointT>::points_[0]; }

//    virtual const PointT &GetEnd() const { return GeometryPrimitiveT<PointT>::points_[1]; }

    ///////////////////
    // Xml functions //
    ///////////////////

//    virtual TiXmlElement * ToXml(bool embed_vertices = false, const char *name = NULL) const;

    ///////////////////////////
    // Bounding-box function //
    ///////////////////////////

    AABBoxT<BasePoint> GetAxisBoundingBox() const;
    BoundingSphereT<BasePoint> GetBoundingSphere() const;

    ///////////////
    // Modifiers //
    ///////////////

    void set_points(const PointT& p1, const PointT& p2);
    void set_p1(const PointT& p1);
    void set_p2(const PointT& p2);

    ///////////////
    // Accessors //
    ///////////////

    const PointT& p1()    const { return GeometryPrimitiveT<PointT>::points_[0]; }
    const PointT& p2()    const { return GeometryPrimitiveT<PointT>::points_[1]; }

    const BaseVector& dir()         const { return dir_;        }
    const BaseVector& unit_dir()    const { return unit_dir_;   }

    Scalar length()       const { return length_;     }
    Scalar sqrlength()    const { return sqrlength_;  }
    Scalar invlength()    const { return invlength_;  }

    ///////////////
    // Attributs //
    ///////////////

//    friend bool operator<(const SegmentT& g1, const SegmentT& g2)
//    {
//        return g1.IsInferiorTo(g2);
//    }

//    virtual bool IsInferiorTo(const SegmentT& g2) const
//    {
//        if ((GeometryPrimitiveT<PointT>::points_[0] != g2.GeometryPrimitiveT<PointT>::points_[0] || GeometryPrimitiveT<PointT>::points_[1] != g2.GeometryPrimitiveT<PointT>::points_[1]) && (GeometryPrimitiveT<PointT>::points_[0] != g2.GeometryPrimitiveT<PointT>::points_[1] || GeometryPrimitiveT<PointT>::points_[1] != g2.GeometryPrimitiveT<PointT>::points_[0])) {
//            return (GeometryPrimitiveT<PointT>::points_[0] != g2.GeometryPrimitiveT<PointT>::points_[0]) ? GeometryPrimitiveT<PointT>::points_[0] < g2.GeometryPrimitiveT<PointT>::points_[0] : GeometryPrimitiveT<PointT>::points_[1] < g2.GeometryPrimitiveT<PointT>::points_[1];
//        }
//        return GeometryPrimitiveT<PointT>::IsInferiorTo(g2);
//    }

    friend bool operator==(const SegmentT& g1, const SegmentT& g2)
    {
        if ((g1.GeometryPrimitiveT<PointT>::points_[0] != g2.GeometryPrimitiveT<PointT>::points_[0] || g1.GeometryPrimitiveT<PointT>::points_[1] != g2.GeometryPrimitiveT<PointT>::points_[1]) && (g1.GeometryPrimitiveT<PointT>::points_[0] != g2.GeometryPrimitiveT<PointT>::points_[1] || g1.GeometryPrimitiveT<PointT>::points_[1] != g2.GeometryPrimitiveT<PointT>::points_[0])) {
            return false;
        }
        return (g1.sqrlength_ == g2.sqrlength_ &&
                g1.unit_dir_ == g2.unit_dir_);
    }

protected:

//    PointT GeometryPrimitiveT<PointT>::points_[0];
//    PointT GeometryPrimitiveT<PointT>::points_[1];

    // Help variables for divers computation on the segment.
    // This will be used to improve computation speed in divers cases.

    BaseVector dir_; // should be VectorT
    BaseVector unit_dir_;
    Scalar length_;
    Scalar sqrlength_;
    Scalar invlength_;

    virtual void ComputeHelpVariables()
    {
        dir_        = GeometryPrimitiveT<PointT>::points_[1] - GeometryPrimitiveT<PointT>::points_[0];
        length_        = dir_.norm();
        sqrlength_    = dir_.squaredNorm();
        invlength_    = 1.0/length_;
        unit_dir_    = dir_;
        unit_dir_.normalize();
    }
};

//template<typename PointT>
//TiXmlElement * SegmentT<PointT>::ToXml(bool embed_vertices, const char *name) const
//{
//    TiXmlElement * element = GeometryPrimitiveT<PointT>::ToXml(embed_vertices, name);

//    return element;
//}

///////////////////////////
// Bounding-box function //
///////////////////////////

template<typename PointT>
AABBoxT<typename PointT::BasePoint> SegmentT<PointT>::GetAxisBoundingBox()  const
{
//TODO:@todo : use eigen correcly ...
    AABBoxT<BasePoint> res;

    for(unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i)
    {
        if(GeometryPrimitiveT<PointT>::points_[0][i] < GeometryPrimitiveT<PointT>::points_[1][i])
        {
            res.min_[i] = GeometryPrimitiveT<PointT>::points_[0][i];
            res.max_[i] = GeometryPrimitiveT<PointT>::points_[1][i];
        }
        else
        {
            res.min_[i] = GeometryPrimitiveT<PointT>::points_[1][i];
            res.max_[i] = GeometryPrimitiveT<PointT>::points_[0][i];
        }
    }
    return res;
}

template<typename PointT>
auto SegmentT<PointT>::GetBoundingSphere() const -> BoundingSphereT<BasePoint>
{
    BoundingSphereT<BasePoint> res;
    res.radius_ = 0.5*length_;
    res.center_ = 0.5*(GeometryPrimitiveT<PointT>::points_[0]+GeometryPrimitiveT<PointT>::points_[1]);
//    res.center_ = 0.5*(GeometryPrimitiveT<PointT>::points_[0]+GeometryPrimitiveT<PointT>::points_[1]);
    //TODO:@todo : the bounding sphere of a weighted segment contains a WeightedPoint instead of a point ....

    return res;
}



///////////////
// Modifiers //
///////////////

template<typename PointT>
void SegmentT<PointT>::set_points(const PointT& p1, const PointT& p2)
{
    GeometryPrimitiveT<PointT>::points_[0] = p1;
    GeometryPrimitiveT<PointT>::points_[1] = p2;
    ComputeHelpVariables();
}

template<typename PointT>
void SegmentT<PointT>::set_p1(const PointT& p1)
{
    GeometryPrimitiveT<PointT>::points_[0] = p1;
    ComputeHelpVariables();
}

template<typename PointT>
void SegmentT<PointT>::set_p2(const PointT& p2) {
    GeometryPrimitiveT<PointT>::points_[1] = p2;
    ComputeHelpVariables();
}

typedef SegmentT<core::PointPrimitive> Segment;
typedef SegmentT<core::PointPrimitive2D> Segment2D;

} // Close namespace core

} // Close namespace expressive

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_SEGMENT
extern template class expressive::core::SegmentT<expressive::core::WeightedPointT<expressive::Point> >;
extern template class expressive::core::SegmentT<expressive::core::WeightedPointT<expressive::Point2> >;
extern template class expressive::core::SegmentT<expressive::core::PointPrimitiveT<expressive::Point> >;
extern template class expressive::core::SegmentT<expressive::core::PointPrimitiveT<expressive::Point2> >;
#endif
#endif

#endif // SEGMENT_H_
