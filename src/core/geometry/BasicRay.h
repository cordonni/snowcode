/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BasicRayT.h

   Language: C++

   License: Expressive license

   \author: Maxime Quiblier
   E-Mail:  maxime.quiblier@inrialpes.fr

   Description: Header for basic rays.

   Platform Dependencies: None
*/

#pragma once
#ifndef BASIC_RAY_H
#define BASIC_RAY_H

// core dependencies
#include <core/CoreRequired.h>
#include "core/geometry/AABBox.h"
//#include <core/geometry/Point.h>

namespace expressive {

namespace core {

/**
 * A ray, represented by it's starting point and it's direction.
 * Moslty used to compute intersections.
 */
class CORE_API BasicRay
{
public:
    

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    /**
     * Default ctor, creates a ray starting from origin, oriented towards UnitX().
     */
    BasicRay();

    /**
     * Ctor.
     * @param starting_point origin of the ray.
     * @param direction direction of the ray.
     */
    BasicRay(const Point& starting_point,const Vector& direction);

    ~BasicRay();

    ///////////////
    // Modifiers //
    ///////////////

    /**
     * Sets this ray's starting point.
     * @param starting_point a Point in space.
     */
    void set_starting_point(const Point& starting_point);

    /**
     * Sets this ray's direction.
     * @param direction  a direction in space.
     */
    void set_direction(const Vector& direction);
    
    ///////////////
    // Accessors //
    ///////////////

    const Point& starting_point() const; //< returns this ray's starting point.
    const Vector& direction() const; //< returns this ray's direction


    ///////////////
    //  Utility  //
    ///////////////

    /**
     * Computes the intersection between this ray and the given sphere.
     * @param center the center of the sphere.
     * @param radius radius of the sphere.
     * @param[out] dist the distance at which this ray intersects the given sphere.
     * @return true if this intersects with the sphere.
     */
    bool IntersectsSphere(const Point &center, Scalar radius, Scalar &dist) const;

    /**
     * Computes the intersection between this ray and the given plane.
     * @param point a point on the given plane.
     * @param normal the normal of the plane.
     * @param[out] dist the distance at which this ray intersects the plane.
     * @return true if this intersects with the plane.
     */
    bool IntersectsPlane(const Point &point, const Point &normal, Scalar &dist) const;

    /**
     * Computes the intersection between this ray and the given bounding box.
     * @param box a bounding box of the same dimension as this ray.
     * @param[out] dist the distance at which this ray intersects the given box.
     * @return true if this intersects with the bbox.
     */
    bool IntersectsBoundingBox(const AABBox &box, Scalar &dist) const;

    /**
     * Returns the closest point of this ray to another ray.
     * @param other_ray any other ray.
     * @return the intersection point between the 2 rays. If they're not intersecting,
     * the return value will be the closest point on this ray to the other ray.
     */
    Point ClosestPoint(const BasicRay& other_ray) const;

    /**
     * Returns the middle point of this ray and another ray.
     * @param other_ray any other ray.
     * @return the intersection point between the 2 rays. If they're not intersecting,
     * the return value will be the middle point between the 2 closests points of each related to the other.
     */
    Point MidPoint(const BasicRay& other_ray) const;

    /**
     * Returns the minimum distance between this ray and a given point.
     */
    Scalar Distance(const Point& pt) const;

    /**
     * Returns the projection of a given point on this ray.
     */
    Point Project(const Point& p) const;



    ////////////////
    // Attributes //
    ////////////////

private:
    Point starting_point_;

    Vector direction_;
};

} // Close namespace core

} // Close namespace expressive

#endif // BASIC_RAY_H
