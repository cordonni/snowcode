/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: SkelPrimitiveFactoryT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for a primitive skeleton factory (created object should 
                derive both from GeometryPrimitive and NodeScalarField). 
                This factory can create the required object from a given GeometryPrimitive
                and a Kernel.

                // TODO : refactor the map structure using MapFunctorTypeTreeT.h

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_SKELETON_PRIMITIVE_FACTORY_T_H_
#define CONVOL_SKELETON_PRIMITIVE_FACTORY_T_H_

// core dependencies
#include <core/CoreRequired.h>
    // functor
    #include <core/functor/Functor.h>
    #include <core/functor/FunctorTypeMapT.h>
    // geometry
    #include <core/geometry/GeometryPrimitive.h>
    #include <core/geometry/WeightedPoint.h>

#include "ork/core/Logger.h"

namespace expressive {

namespace core {

/*! \brief ...
 *  WARNING : Primitives registered in this factory should derive from a WeightedGeometryPrimitive
 *            and should contains a typedef defining BaseGeometry (cf in NodeSegmentSField).
 *            Furthermore primtives should be templated by a kernel and define FunctorKernel by a typedef.
 */
/**
 * This factory allows to create Primitives of any type, even Templated primitives, from a simple string or from partly-defined
 * types. It creates a primitive from a base primitive of a parent type, and possibly a kernel functor. Mostly used to create Implicit surfaces.
 * In order to be "creatable", types must be registered beforehand. When adding a new type, one should define a static PrimitiveFactory::Type with
 * the proper defines inside. See examples from code.
 */
class CORE_API PrimitiveFactory
{
public:
//    typedef core::GeometryPrimitiveT<core::WeightedPoint> WeightedGeometryPrimitive;
    
    typedef std::shared_ptr<Primitive>
        (*BuilderFunction) (const std::shared_ptr<Primitive> geom, const core::Functor &ker);

#ifdef _MSC_VER
    typedef core::FunctorTypeMapT<BuilderFunction> GeomFactory;
    typedef std::map<std::string, GeomFactory> MapperType; // 1 GeomFactory per kind of primitive registered
#else
    typedef typename core::FunctorTypeMapT<BuilderFunction> GeomFactory;
    typedef typename std::map<std::string, GeomFactory> MapperType; // 1 GeomFactory per kind of primitive registered
#endif

    static PrimitiveFactory& GetInstance();

    /**
     * Sets the default primitive used to determine how to complete incomplete types.
     * Default for the whole application is WeightedSegment, which creates a NodeSegmentSFieldT<HomotheticCompactPolynomial6>.
     */
    void SetDefaultPrimitive(std::shared_ptr<Primitive> geom);

    /**
     * Creates a new primitive based on what is in the default geom.
     */
    std::shared_ptr<Primitive> CreateNewPrimitive();

    /**
     * Creates a new primitive based on what is in the default geom, and sets its functor to match ker.
     */
    std::shared_ptr<Primitive> CreateNewPrimitive(const core::Functor &ker);

    /**
     * Creates a new primitive of a type depending on geom, using #default_geom_'s kernel
     */
    std::shared_ptr<Primitive> CreateNewPrimitive(const std::shared_ptr<Primitive> geom);

    /**
     * Creates a new primitive whose types depends on geom and whose functor is ker.
     */
    std::shared_ptr<Primitive> CreateNewPrimitive(const std::shared_ptr<Primitive> geom,
                                                  const core::Functor &ker );

    template<typename TFunctor, typename TGeometry>
    void RegisterTemplatedPrimitive(BuilderFunction f);

    template <class TPrimitive>
    class Type
    {
    public:
        typedef typename TPrimitive::BaseGeometry BaseGeometry;
        typedef typename TPrimitive::FunctorKernel FunctorKernel;

        static std::shared_ptr<Primitive> ctor (const std::shared_ptr<Primitive> geom,
                                                                const core::Functor &ker)
        {
            const std::shared_ptr<BaseGeometry> casted_geom = std::dynamic_pointer_cast<BaseGeometry>(geom);
            if (casted_geom == nullptr) {
                return nullptr;
            }
            FunctorKernel const* casted_ker = dynamic_cast<FunctorKernel const*>(&ker);
            return std::make_shared<TPrimitive>(*casted_geom, *casted_ker);
        }
        
        Type()
        {
            PrimitiveFactory::GetInstance().RegisterTemplatedPrimitive<FunctorKernel,BaseGeometry>(ctor);
        }
    };

private:
    MapperType types;

    std::shared_ptr<Primitive> default_geom_;

};
    


template <typename TFunctor, typename TGeometry>
void PrimitiveFactory::RegisterTemplatedPrimitive(BuilderFunction f)
{
    const core::FunctorTypeTree& type_tree = TFunctor::StaticType();
    ork::Logger::DEBUG_LOGGER->logf("FACTORY", "Registering primitive %s : %s", TGeometry::StaticName().c_str(), type_tree.type_.c_str());

    if(!types[TGeometry::StaticName()].insert(type_tree,f))
    {
        ork::Logger::ERROR_LOGGER->logf("FACTORY", "PrimitiveFactory : Primitive<Kernel> already registered : %s||%s", TGeometry::StaticName().c_str(), type_tree.type_.c_str() );
        throw std::exception();
    }
}

//    
//    //////////////////////
//    // SkelBasicSegment //
//    //////////////////////
//
//    /** \brief Create a new SkelBasicSegment with the given kernel.
//    *
//    *   \param skel skeleton to which the SkelBasicSegment will be added.
//    *   \param vh0 first SkelVertexHandle for the segment to be created.
//    *   \param vh1 second SkelVertexHandle for the segment to be created.
//    *   \param kernel The kernel for the segment to be created.
//    *   \return The newly created SkelBasicSegment.
//    */
//    static SkelBasicSegment* CreateNewSkelBasicSegment(Skeleton& skel, SkelVertexHandle* vh0, SkelVertexHandle* vh1, const Functor& kernel)
//    {
//        SkelBasicSegBuilderFunction builder = NULL;
//        const FunctorTypeTree ker_type = kernel.Type();
//
//        MapperTypeIterator it;
//        if(recursive_mapper_finder(StaticMapper(), ker_type, it))
//        {
//            builder = it->second.skel_basic_segment_builder_;
//        }
//
//        if(builder != NULL)
//        {
//            return builder(skel, vh0, vh1, kernel);
//        }
//        else
//        {
//            assert(false);  // The given kernel must not be registered for SkelBasicSegment... Check it out !
//            return NULL;
//        }
//    }
//
//    /** \brief This function is made for the factory (same as Create of SkelBasicSegment but has the same signature for all kernels).
//    */
//    template< typename Kernel>
//    static SkelBasicSegment* GenericCreateSkelBasicSegment(Skeleton& skel, SkelVertexHandle* vh0, SkelVertexHandle* vh1, const Functor& ker)
//    {
//        Kernel const* casted_ker = dynamic_cast<Kernel const*>(&ker);
//        if(casted_ker != NULL)
//        {
//            return SkelBasicSegment::Create(skel,vh0,vh1,*casted_ker);
//        }
//        else
//        {
//            assert(false);
//            return NULL;
//        }
//    }
//
//    /** \brief Register a kernel for creating SkelBasicSegments
//    */
//    template<typename Kernel>
//    static void RegisterSkelBasicSegmentKernel()
//    {
//        const FunctorTypeTree& type_tree = Kernel::StaticType();
//
//        MapperTypeIterator it = recursive_mapper_builder(StaticMapper(), type_tree);
//
//        assert(it->second.skel_basic_segment_builder_ == NULL); // Kernel already registered.
//
//        it->second.skel_basic_segment_builder_ = &GenericCreateSkelBasicSegment<Kernel>;
//
//        // Register also the corresponding Scalar Field 
//        SegmentSFFactory::template RegisterKernel<Kernel>();
//    }

    } // Close namespace core

    } // Close namespace expressive

#endif // CONVOL_SKELETON_PRIMITIVE_FACTORY_T_H_
