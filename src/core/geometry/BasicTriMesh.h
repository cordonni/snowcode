/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef BASIC_TRI_MESH_T_H
#define BASIC_TRI_MESH_T_H

#include "core/geometry/AbstractTriMesh.h"

#include "ork/render/Mesh.h"

// external dependencies
#include <OpenMesh/Core/Mesh/IteratorsT.hh>

namespace expressive {

namespace core {

template <typename VERTEX>
class BasicTriMeshT : public AbstractTriMesh
{
public :


    typedef ork::ptr<ork::Mesh<VERTEX, unsigned int> > MESHPTR;
    typedef ork::Mesh<VERTEX, unsigned int > MESH;

    // Constructor / Destructor
    BasicTriMeshT(ork::MeshMode mesh_mode = ork::TRIANGLES, ork::MeshUsage usage = ork::GPU_DYNAMIC);

    BasicTriMeshT(const BasicTriMeshT &rhs, bool copy_content = true, bool share_data = true);

    virtual ~BasicTriMeshT();

    void copyDataFrom(const BasicTriMeshT &mesh);

    virtual void copyDataFrom(const AbstractTriMesh *mesh);

    virtual std::shared_ptr<AbstractTriMesh> clone(bool copy_content = false) const;

    virtual void clear();

    virtual size_t n_vertices() const;
    virtual size_t n_indices () const;

    virtual bool has_vertex_normals() const;
    virtual bool has_vertex_colors () const;



    const VERTEX&         vertex(VertexHandle vh) const;

    virtual const Point&    point (VertexHandle vh) const;
    virtual const Normal&   normal(VertexHandle vh) const;
    virtual const Vector2&  uv    (VertexHandle vh) const;
    virtual const Color&    color (VertexHandle vh) const;


    VERTEX&           vertex(VertexHandle vh);

    virtual Point&    point (VertexHandle vh);
    virtual Normal&   normal(VertexHandle vh);
    virtual Vector2&  uv    (VertexHandle vh);
    virtual Color&    color (VertexHandle vh);


    virtual ork::ptr<ork::MeshBuffers> mesh_buffers() const;
    virtual ork::MeshMode mesh_mode() const;
    virtual ork::MeshUsage mesh_usage() const;
    inline MESHPTR internal_mesh() const;
    virtual std::mutex& get_buffers_mutex() const;

    virtual void set_vertex(VertexHandle vh, const VERTEX  &point );
    virtual void set_point (VertexHandle vh, const Point   &point );
    virtual void set_normal(VertexHandle vh, const Vector  &normal);
    virtual void set_uv    (VertexHandle vh, const Vector2 &uv    );
    virtual void set_color (VertexHandle vh, const Color   &color );

    // Setters: Vertex
//    /**
//     * Adds a vertex to this mesh.
//     * This version is here for compliance with ork::mesh's api.
//     */
//    template<typename OTHERVERTEX>
//    VertexHandle addVertex(const OTHERVERTEX& p, const VertexStatus& status = VertexStatus());

    /**
     * @brief add_point creates a new vertex from a Point.
     * @param p the point to add
     * @param status an optionnal VertexStatus that will be used to store the vertex neighborhood data
     * @return the VertexHandle of the new Vertex.
     */
    virtual VertexHandle add_point(const Point &p, const VertexStatus& status = VertexStatus());
    virtual VertexHandle add_point(const Point &p, const Color& color, const VertexStatus& status = VertexStatus());
    virtual VertexHandle add_point(const Point &p, const Vector &normal, const Color& color = Color(), const VertexStatus& status = VertexStatus());
    virtual VertexHandle add_point(const Point &p, const Vector &normal, const Vector2 &uv, const Color& color, const VertexStatus& status = VertexStatus());

    /**
     * Adds a vertex to this mesh.
     * This version is here for compliance with openmesh's api.
     */
    template<typename OTHERVERTEX>
    VertexHandle add_vertex(const OTHERVERTEX& p, const VertexStatus& status = VertexStatus());

    virtual EdgeHandle add_line(VertexHandle v1, VertexHandle v2);

    virtual FaceHandle add_face(VertexHandle v1, VertexHandle v2, VertexHandle v3);

//    VertexHandle add_vertex(const Vector& p, const VertexStatus& status = VertexStatus());

    // Operators
//    BasicTriMesh &operator=(const BasicTriMesh &rhs);

    virtual void ForceUpdate();

    //inverse the normals
    void inverseNormals();


protected:
    virtual void setVertexInPlace(int oldVertex, int newVertex, std::vector<int> &vh_map);
    virtual void setVertexCount(unsigned int n_vertices);
    virtual void updateFaceIndices(const std::vector<int> &vh_map);

    MESHPTR mesh_;
};

typedef BasicTriMeshT<DefaultMeshVertex> BasicTriMesh;
}

}

#include "core/geometry/BasicTriMesh.hpp"

#endif
