#define TEMPLATE_INSTANTIATION_WEIGHTEDPOINTWITHDENSITY
#include "core/geometry/WeightedPointWithDensity.h"

namespace expressive {

namespace core {

template class WeightedPointWithDensityT<Point>;

} // Close namespace core
} // Close namespace expressive
