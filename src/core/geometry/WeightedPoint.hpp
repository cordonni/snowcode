
#include <core/utils/BasicsToXml.h>

namespace expressive {

namespace core {

template<typename BasePoint>
std::string WeightedPointT<BasePoint>::ToString() const
{
    //    string s = PointT::ToString();
    std::string s = Name();

    s+= "[";
    for (unsigned int i = 0; i < BasePoint::RowsAtCompileTime; ++i) {
        s+= std::to_string((*this)[i]) + ":";
    }
    s+= "]";

    s += " - weight:" + std::to_string(weight_);

    return s;
}
///////////////////
// Xml functions //
///////////////////

template<typename BasePoint>
TiXmlElement * WeightedPointT<BasePoint>::ToXml(const char *name) const
{
    TiXmlElement *element = PointPrimitiveT<BasePoint>::ToXml(name);

    element->SetDoubleAttribute("weight",weight_);

    return element;
}

///////////////////////////
// Bounding-box function //
///////////////////////////

template<typename BasePoint>
AABBoxT<BasePoint> WeightedPointT<BasePoint>::GetAxisBoundingBox() const
{
    AABBoxT<BasePoint> res;
    res.min_ = *this;
    res.max_ = *this;
    for (unsigned int i = 0; i < BasePoint::RowsAtCompileTime; ++i) {
        res.min_[i] -= weight_;
        res.max_[i] += weight_;
    }

    return res;
}

template<typename BasePoint>
BoundingSphereT<BasePoint> WeightedPointT<BasePoint>::GetBoundingSphere() const
{
    BoundingSphereT<BasePoint> res(*this, weight_);
    return res;
}


} // Close namespace core

} // Close namespace expressive
