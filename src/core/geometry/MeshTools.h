/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef MESHTOOLS_H
#define MESHTOOLS_H

#include "core/CoreRequired.h"

#include "core/geometry/AbstractTriMesh.h"

namespace expressive {

namespace core {

template<typename VERTEX>
class MeshToolsT
{
public:

    /**
     * Creates a mesh from a given obj file.
     * @param filename the name of the file to load from.
     */
    static std::shared_ptr<AbstractTriMesh> CreateFromObj(const std::string &filename);
    /////////////////////////////////////////////////////////////////////////
    ////
    ////        Geometric shapes creation functions
    ////
    /////////////////////////////////////////////////////////////////////////
    /**
     * @brief CreateCross creates a mesh of a cross
     * @param frame on which the cross will be set
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateCross(const core::Frame& f);
    static std::shared_ptr<AbstractTriMesh> CreateCross(const Point& p0  = Point (0,0,0),
                                                    const Vector& dx = Vector(1,0,0),
                                                    const Vector& dy = Vector(0,1,0),
                                                    const Vector& dz = Vector(0,0,1) );

    /**
     * @brief CreateCube creates a mesh of a cube
     * @param frame on which the cube will be set
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateCube(const core::Frame& f);
    static std::shared_ptr<AbstractTriMesh> CreateCube(const Point& p0  = Point (0,0,0),
                                                    const Vector& dx = Vector(1,0,0),
                                                    const Vector& dy = Vector(0,1,0),
                                                    const Vector& dz = Vector(0,0,1) );

    /**
     * @brief CreateTetrahedron creates a mesh of a tetrahedron
     * @param frame on which the tetrahedron will be set
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateTetrahedron(const core::Frame& f);
    static std::shared_ptr<AbstractTriMesh> CreateTetrahedron(const Point& p0  = Point (0,0,0),
                                                           const Vector& dx = Vector(1,0,0),
                                                           const Vector& dy = Vector(0,1,0),
                                                           const Vector& dz = Vector(0,0,1) );

    /**
     * @brief CreateCylinder creates a mesh of a cylinder of revolution axis Z
     * @param frame on which the cube will be set
     * @param ratio indicates the ratio of the radii of the upper cicle in regard of the lower's
     * @param subdiv indicates the number f points along the circles of the cylindre
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateCylinder(const core::Frame& f, const uint subdiv = 20, const double ratio = 1);
    static std::shared_ptr<AbstractTriMesh> CreateCylinder(const uint subdiv = 20,
                                                        const double ratio = 1,
                                                        const Point& p0  = Point (0,0,0),
                                                        const Vector& dx = Vector(1,0,0),
                                                        const Vector& dy = Vector(0,1,0),
                                                        const Vector& dz = Vector(0,0,1));

    /**
      * @brief CreateCapsule creates a mesh of a capsule (cylinder + extremity sphere) of revolution axis Z
      * @param frame on which the cube will be set
      * @param ratio indicates the ratio of the radii of the upper cicle in regard of the lower's
      * @param subdiv indicates the number f points along the circles of the cylindre
      * @return shared pointer of the created mesh
      */
     static std::shared_ptr<AbstractTriMesh> CreateCapsule(const core::Frame& f, const uint subdiv = 20, const double ratio = 1);
     static std::shared_ptr<AbstractTriMesh> CreateCapsule(const uint subdiv = 20,
                                                        const double ratio = 1,
                                                        const Point& p0  = Point (0,0,0),
                                                        const Vector& dx = Vector(1,0,0),
                                                        const Vector& dy = Vector(0,1,0),
                                                        const Vector& dz = Vector(0,0,1));

    /**
     * @brief CreateCone creates a mesh of a cone of revolution axis Z
     * @param frame on which the cube will be setFrame
     * @param subdiv indicates the number f points along the circle of the cone
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateCone(const core::Frame& f, const uint subdiv = 20);
    static std::shared_ptr<AbstractTriMesh> CreateCone(const uint subdiv = 20,
                                                    const Point& p0  = Point (0,0,0),
                                                    const Vector& dx = Vector(1,0,0),
                                                    const Vector& dy = Vector(0,1,0),
                                                    const Vector& dz = Vector(0,0,1));

    /**
     * @brief CreateDisk creates a mesh of a disk in the XY plane
     * @param subdiv indicates the number f points along the circle of the disk
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateDisk(const core::Frame& f, const uint subdiv = 20);
    static std::shared_ptr<AbstractTriMesh> CreateDisk(const uint subdiv = 20,
                                                    const Point& p0  = Point (0,0,0),
                                                    const Vector& dx = Vector(1,0,0),
                                                    const Vector& dy = Vector(0,1,0),
                                                    const Vector& dz = Vector(0,0,1));

    /**
     * @brief CreateQuad creates a mesh of a quad in the XY plane
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateQuad(const core::Frame& f);
    static std::shared_ptr<AbstractTriMesh> CreateQuad(const Point& p0  = Point (0,0,0),
                                                    const Vector& dx = Vector(1,0,0),
                                                    const Vector& dy = Vector(0,1,0),
                                                    const Vector& dz = Vector(0,0,1));

    /**
     * @brief CreateSphere creates a mesh of a sphere
     * @param subdiv indicates the number f points along the circle of the disk
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateSphere(const core::Frame& f, const uint subdiv_u = 20, const uint subdiv_v = 10);
    static std::shared_ptr<AbstractTriMesh> CreateSphere(const uint subdiv_u = 20,
                                                      const uint subdiv_v = 10,
                                                      const Point& p0  = Point (0,0,0),
                                                      const Vector& dx = Vector(1,0,0),
                                                      const Vector& dy = Vector(0,1,0),
                                                      const Vector& dz = Vector(0,0,1));

    /**
     * @brief CreateSphere creates a mesh of a sphere
     * @param subdiv indicates the number subdivisions
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateIcosahedron(const core::Frame& f);
    static std::shared_ptr<AbstractTriMesh> CreateIcosahedron(const Point& p0  = Point (0,0,0),
                                                           const Vector& dx = Vector(1,0,0),
                                                           const Vector& dy = Vector(0,1,0),
                                                           const Vector& dz = Vector(0,0,1));

    /**
     * @brief CreateSphere creates a mesh of a sphere
     * @param subdiv indicates the number subdivisions
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateIcoSphere(const core::Frame& f, const uint subdiv = 0);
    static std::shared_ptr<AbstractTriMesh> CreateIcoSphere(const uint subdiv = 0,
                                                         const Point& p0  = Point (0,0,0),
                                                         const Vector& dx = Vector(1,0,0),
                                                         const Vector& dy = Vector(0,1,0),
                                                         const Vector& dz = Vector(0,0,1));

    /**
     * Creates a Quasi Uniform Sphere.
     * @param edge_length is the length of edges, which will be used all around the sphere.
     * @return shared pointer of the created mesh.
     */
    static std::shared_ptr<AbstractTriMesh> CreateQUSphere(const core::Frame& f, const Scalar edge_length);
    static std::shared_ptr<AbstractTriMesh> CreateQUSphere(const Scalar edge_length,
                                                         const Point& p0  = Point (0,0,0),
                                                         const Vector& dx = Vector(1,0,0),
                                                         const Vector& dy = Vector(0,1,0),
                                                         const Vector& dz = Vector(0,0,1));

    /**
     * @brief CreateSphere creates a mesh of a sphere
     * @param subdiv indicates the number f points along the circle of the disk
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateGrid(const core::Frame& f, const uint subdiv_u = 10, const uint subdiv_v = 10);
    static std::shared_ptr<AbstractTriMesh> CreateGrid(const uint subdiv_u = 10,
                                                    const uint subdiv_v = 10,
                                                    const Point& p0  = Point (0,0,0),
                                                    const Vector& dx = Vector(1,0,0),
                                                    const Vector& dy = Vector(0,1,0),
                                                    const Vector& dz = Vector(0,0,1));

    /**
     * @brief CreateFrame creates a mesh of a frame
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreateFrame(const core::Frame& f);
    static std::shared_ptr<AbstractTriMesh> CreateFrame(const Point& p0  = Point (0,0,0),
                                                     const Vector& dx = Vector(1,0,0),
                                                     const Vector& dy = Vector(0,1,0),
                                                     const Vector& dz = Vector(0,0,1));

    /**
     * Creates a patch : a quad inside another quad.
     */
    static std::shared_ptr<AbstractTriMesh> CreatePatch(const core::Frame& f);
    static std::shared_ptr<AbstractTriMesh> CreatePatch(const Point& p0  = Point (0,0,0),
                                                     const Vector& dx = Vector(1,0,0),
                                                     const Vector& dy = Vector(0,1,0),
                                                     const Vector& dz = Vector(0,0,1));

    /**
     * Creates a lattice of size Nx x Ny
     */
    static std::shared_ptr<AbstractTriMesh> CreateLattice(
            const core::Frame& f,
            const uint Nx = 10,
            const uint Ny = 10);

    static std::shared_ptr<AbstractTriMesh> CreateLattice(const uint Nx = 10,
            const uint Ny = 10,
            const Point& p0  = Point (0,0,0),
            const Vector& dx = Vector(1,0,0),
            const Vector& dy = Vector(0,1,0),
            const Vector& dz = Vector(0,0,1));




    /**
     * @brief PrimitiveType enum distinguishes different types of primitives which can be created directly
     */
    enum PrimitiveType
    {
        NoneType,
        CubeType,
        CylinderType,
        ConeType,
        DiskType,
        SphereType,
        FrameType,
        PrimitiveTypeCount
    };


    /**
     * @brief CreatePrimitive, static function, creates a mesh of the specified type
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> CreatePrimitive(const PrimitiveType& type, const core::Frame& f);
    static std::shared_ptr<AbstractTriMesh> CreatePrimitive(const PrimitiveType& type,
                                                         const Point& p0  = Point (0,0,0),
                                                         const Vector& dx = Vector(1,0,0),
                                                         const Vector& dy = Vector(0,1,0),
                                                         const Vector& dz = Vector(0,0,1));

    /**
     * @brief Empty, static function, creates an empty mesh
     * @return shared pointer of the created mesh
     */
    static std::shared_ptr<AbstractTriMesh> Empty();
};

typedef MeshToolsT<DefaultMeshVertex> MeshTools;

}

}

#include "core/geometry/MeshTools.hpp"

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_MESHTOOLS
extern template class expressive::core::MeshToolsT<expressive::core::DefaultMeshVertex>;
#endif
#endif

#endif // MESHTOOLS_H
