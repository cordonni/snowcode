/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: PointCloud.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Point cloud structure

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_POINT_CLOUD_T_
#define CONVOL_POINT_CLOUD_T_

// core dependencies
#include<core/CoreRequired.h>

#include<assert.h>

// std dependencies
#include<list>

namespace expressive {

namespace core {

/**
 * Handles a list of points with a radius.
 * TODO:@todo add more stuff about why this was used.
 */
class /*CORE_API*/ PointCloud
{
public:
    struct PointHandle{
        

        PointHandle(const Point& pp, const Normal& nn)
            : p(pp), n(nn), c(Color::Constant(1.0)) {}

        Point p;
        Normal n;
        Color c;
    };

    typedef std::list<PointHandle>::iterator iterator;
    typedef std::list<PointHandle>::const_iterator const_iterator;

    PointCloud() : point_size_(0.02) {}

    ~PointCloud(){}

    ///////////////
    // Modifiers //
    ///////////////

    void clear()
    {
        points_.clear();
    }

    void insert(const Point& p, const Normal& n){
        points_.push_back(PointHandle(p,n));
    }

    iterator erase(iterator position){
        return points_.erase(position);
    }

    void splice(PointCloud& other)
    {
        points_.splice(points_.end(), other.points_);
    }

    ///////////////
    // Accessors //
    ///////////////

    Scalar point_size() const { return point_size_; }

    unsigned int size() const { return (unsigned int) points_.size(); }

    bool empty() const { return points_.empty(); }

          iterator begin()       { return points_.begin();    }
    const_iterator begin() const { return points_.begin();    }
          iterator end()         { return points_.end();      }
    const_iterator end()   const { return points_.end();      }

          Point&  point (      PointHandle& ph)       { return ph.p;  } 
    const Point&  point (const PointHandle& ph) const { return ph.p;  } 
          Normal& normal(      PointHandle& ph)       { return ph.n; } 
    const Normal& normal(const PointHandle& ph) const { return ph.n; } 
          Color& color(      PointHandle& ph)       { return ph.c; } 
    const Color& color(const PointHandle& ph) const { return ph.c; } 

private:
    std::list<PointHandle> points_;
    
    // Size of a point, to change if we want to compute the size an other way
    Scalar point_size_;
};

} // Close namespace core

} // Close namespace expressive

#endif // CONVOL_POINT_CLOUD_T_
