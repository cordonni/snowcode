/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BoundingSphere.h

   Language: C++

   License: Expressive license

   \author: Maxime Quiblier
   E-Mail:  maxime.quiblier@inrialpes.fr

   Description: Header for bounding sphere classes.

   Platform Dependencies: None
*/

#pragma once
#ifndef BOUNDING_SPHERE_T_H_
#define BOUNDING_SPHERE_T_H_

// core dependencies
#include <core/CoreRequired.h>
#include "core/VectorTraits.h"

namespace expressive {

namespace core {

template <typename PointT>
/**
 * The BoundingSphereT class is basically a sphere centered on any item.
 * It it also completed with a few intersection functions.
 */
class BoundingSphereT
{
public:


    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    /**
     * Ctor.
     * @param center center of the sphere
     * @param radius radius of the sphere.
     */
    BoundingSphereT(const PointT& center = PointT::Zero(), Scalar radius = 0.0) :  radius_(radius), center_(center) {}
    ~BoundingSphereT() {}

    /**
     * Returns the smallest sphere that contains the intersection of this and other
     */
    BoundingSphereT Intersection(const BoundingSphereT& other);

    /**
     * Returns the smallest sphere that contains the intersection of this and other
     */
    BoundingSphereT Union(const BoundingSphereT& other);

    /**
     * Very special distance given as the max of the distance between 1 point on this and 1 point on other
     */
    Scalar HierarchyDistance(const BoundingSphereT& other);


    ///////////////
    // Attributs //
    ///////////////

    Scalar radius_;
    PointT center_;
};


typedef BoundingSphereT<Point> BoundingSphere;

} // Close namespace core

} // Close namespace expressive

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_BOUNDINGSPHERE
extern template class expressive::core::BoundingSphereT<expressive::Point>;
#endif
#endif

#include <core/geometry/BoundingSphere.hpp>

#endif // BOUNDING_SPHERE_H_
