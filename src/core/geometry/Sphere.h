/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: SphereT.h

   Language: C++

   License: Expressive license

   \author: Maxime Quiblier
   E-Mail:  maxime.quiblier@inrialpes.fr

   Description: Header for simple sphere class.

   Platform Dependencies: None
*/

#pragma once
#ifndef SPHERE_H_
#define SPHERE_H_

// core dependencies
#include<core/CoreRequired.h>
    // utils
    #include<core/utils/Serializable.h>
    #include<tinyxml/tinyxml.h>

#include "core/VectorTraits.h"

namespace expressive {

namespace core {

// Simple sphere
// TODO : Make this inherit from GeometryPrimitive
/**
 * A Sphere.
 * [DEPRECATED] Because unused right now.
 */
class CORE_API Sphere
{
public:
    

    static std::string StaticName()
    {
        return std::string("Sphere");
    }

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    Sphere() : radius_(0.f), center_(Point::Zero()) {}
    Sphere(const Scalar radius, const Point center) : radius_(radius), center_(center) {}
    ~Sphere() {}

    ///////////////////
    // Xml functions //
    ///////////////////

    TiXmlElement * ToXml(bool embed_vertices = false, const char *name = NULL) const
    {
        UNUSED(embed_vertices);
        TiXmlElement * element;
        if(name == NULL)
            element = new TiXmlElement( this->StaticName() );
        else
            element = new TiXmlElement( name );

        if (embed_vertices) {
            //TODO:@todo : this should be done independently from the point dimension ...
            TiXmlElement * element_center = new TiXmlElement( "center" );
            element_center->SetDoubleAttribute("x",center_[0]);
            element_center->SetDoubleAttribute("y",center_[1]);
            element_center->SetDoubleAttribute("z",center_[2]);
            element->LinkEndChild(element_center);
            element->SetDoubleAttribute("radius",radius_);
        }


        return element;
    }

    ///////////////
    // Attributs //
    ///////////////

    Scalar radius_;
    Point center_;
};

} // Close namespace core

} // Close namespace expressive

#endif // SPHERE_H_
