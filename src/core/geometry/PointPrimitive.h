/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: WeightedPoint.h

   Language: C++

   License: Expressive license

   \author: Galel Koraa
   E-Mail:  galel.koraa@inria.fr

   Description: Header for a a very simple class of a weighted point.

   Platform Dependencies: None
*/

#pragma once
#ifndef POINT_H_
#define POINT_H_


// core dependencies
#include<core/CoreRequired.h>
    // geometry
    #include<core/geometry/AABBox.h>
    #include<core/geometry/BoundingSphere.h>
//    #include<core/geometry/Point.h>
    // utils
    #include<core/utils/Serializable.h>
#include <core/geometry/Primitive.h>
#include <core/geometry/VectorTools.h>

namespace expressive {

namespace core {

template<typename BasePointT>
/**
 * @brief The PointPrimitiveT class allows to store any type of point in GeometryPrimitives.
 * The main purpose is to enable to embed custom data to points.
 * For example, a common use is to add a weight to a point. This allows to use any kind of point
 * as a base for GeometryPrimitives. PointPrimitive ans GeometryPrimitive share a common ancestor (Primitive) so
 * that points can be used almost everywhere as GeometryPrimitives.
 *
 */
class PointPrimitiveT : public BasePointT, public Primitive
{
public:

    EXPRESSIVE_MACRO_NAME("PointPrimitive")

    typedef BasePointT BasePoint;

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    PointPrimitiveT() : BasePoint(BasePoint::Zero()) { }

//    template<typename OtherDerived>
//    PointPrimitiveT(const Eigen::MatrixBase<OtherDerived> &pt) : BasePoint(pt) { }

    PointPrimitiveT(const BasePointT &pt) : BasePointT(pt) { }

    template<typename OtherType>
    PointPrimitiveT(const OtherType& p)
    {
        assert(PointPrimitiveT::RowsAtCompileTime == p.rows());

        for (unsigned int i = 0; i < PointPrimitiveT::RowsAtCompileTime; ++i) {
            (*this)[i] = p[i];
        }
    }

    PointPrimitiveT(Scalar value) : BasePoint(BasePoint::Ones() * value) { }

    //TODO:@todo : argh : only work in dim 2
    /**
     * 2D Ctor.
     */
    PointPrimitiveT(Scalar x, Scalar y) : BasePoint() { // : BasePoint(x, y) { }
        (*this)[0] = x;
        (*this)[1] = y;
    }

    //TODO:@todo : argh : only work in dim 3
    /**
     * 3D Ctor.
     */
    PointPrimitiveT(Scalar x, Scalar y, Scalar z) : BasePoint() { //: BasePoint(x, y, z) { }
        (*this)[0] = x;
        (*this)[1] = y;
        (*this)[2] = z;
    }

    PointPrimitiveT(const PointPrimitiveT &rhs) : BasePoint(rhs) { }

    virtual ~PointPrimitiveT(){ }

    virtual void updated() {}

    template<typename T>
    /**
     * Creates a clone of this PointPrimitive and returns it as a T.
     */
    std::shared_ptr<T> cloneTo() const
    {
        return std::dynamic_pointer_cast<T>(clone());
    }

    /**
     * Returns a new pointer that contains the same data as this PointPrimitive.
     */
    virtual std::shared_ptr<PointPrimitiveT<BasePoint> > clone() const {return std::make_shared<PointPrimitiveT<BasePoint> >(*this); }

    ///////////////////
    // Xml functions //
    ///////////////////

    virtual std::string ToString() const;

    virtual void AddXmlAttributes(TiXmlElement* /*element*/) const;

    ///////////////////////////
    // Bounding-box function //
    ///////////////////////////

    /**
     * Returns the bounding box surrounding this PointPrimitive
     */
    virtual AABBoxT<BasePoint> GetAxisBoundingBox() const;

    /**
     * Returns the bounding sphere surrounding this PointPrimitive.
     */
    virtual BoundingSphereT<BasePoint> GetBoundingSphere() const;

    ///////////////
    // Accessors //
    ///////////////
//    virtual std::vector<PointT<BasePointT> > &GetPoints();

    /**
     * Returns an editable version of a point of this GeometryPrimitive.
     * @param index index of the point to be returned.
     */
    virtual PointPrimitiveT &GetPoint(unsigned int index);

    virtual unsigned int GetSize() const;

    virtual Scalar GetLength() const;

    ///////////////
    // Modifiers //
    ///////////////    
//    void set_positon(const PointPrimitiveT& position) { position_ = position; }

    ///////////////
    // Accessors //
    ///////////////
//    const PointPrimitiveT& position() const { return position_; }


    /**
     * Comparison operator between 2 PointPrimitives.
     * Mandatory for std containers insertions.
     */
    bool operator==(const PointPrimitiveT& p) const
    {
        for (unsigned int i = 0; i < PointPrimitiveT::RowsAtCompileTime; ++i) {
            if ((*this)[i] != p[i]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Comparison operator between 2 PointPrimitives.
     * Mandatory for std containers insertions.
     */
    bool operator<(const PointPrimitiveT& p) const
    {
        for (unsigned int i = 0; i < PointPrimitiveT::RowsAtCompileTime; ++i) {
            if ((*this)[i] > p[i]) {
                return false;
            }
        }

        for (unsigned int i = 0; i < PointPrimitiveT::RowsAtCompileTime; ++i) {
            if ((*this)[i] < p[i]) {
                return true;
            }
        }

        return false;
    }

    /**
     * Copy operator with a PointPrimitive.
     */
    PointPrimitiveT& operator=(const PointPrimitiveT& p)
    {
        assert(BasePoint::rows() == p.rows());

        for (unsigned int i = 0; i < PointPrimitiveT::RowsAtCompileTime; ++i) {
            (*this)[i] = p[i];
        }

        return *this;
    }

    /**
     * Copy operator with any type of points (should still be compatible with this one).
     */
    template<typename OtherType>
    PointPrimitiveT& operator=(const OtherType& p)
    {
        assert(BasePoint::rows() == p.rows());

        for (unsigned int i = 0; i < PointPrimitiveT::RowsAtCompileTime; ++i) {
            (*this)[i] = p[i];
        }

//        this->BasePoint::operator=(p);
        return *this;
    }

    /**
     * Sets the position of this PointPrimitive.
     */
    void set_pos(const PointPrimitiveT &p)
    {
        //TODO:@todo : ugly
        for (unsigned int i = 0; i < PointPrimitiveT::RowsAtCompileTime; ++i) {
            (*this)[i] = p[i];
        }
    }

    /**
     * Sets the position of this PointPrimitive to be at the barycenter of this point AND the ones in primitives.
     * @param primitives a list of PointPrimitives.
     * @return this.
     */
    virtual PointPrimitiveT& MoveToBarycenter(const std::set<PointPrimitiveT> &primitives)
    {
        for (auto p : primitives) {
            for (unsigned int i = 0; i < PointPrimitiveT::RowsAtCompileTime; ++i) {
                (*this)[i] += p[i];
            }
        }
        for (unsigned int i = 0; i < PointPrimitiveT::RowsAtCompileTime; ++i) {
            (*this)[i] /= (Scalar) primitives.size() + 1.0;
        }

        return *this;
    }

    /**
     * @brief intersects tests if current point intersects the segment represented by two given points. radius can be defined at each extremity.
     * @param start segment start point.
     * @param end segment end point.
     * @param rs Radius at start point.
     * @param re Radius at end point. Default : rs.
     * @param rp Radius at current point. Default : 0.
     * @return
     */
    virtual bool intersects(const BasePoint &start, const BasePoint &end, Scalar rs, Scalar re = -1.0, Scalar rp = 0.0) const
    {
        Scalar len;
        Scalar d = VectorTools::segmentDistSq(start, end, *(BasePoint*)this, len);
        Scalar r = rs;
        if (re < 0.0) {
            re = rs;
        }
        if (len >= 0.0 && len <= 1.0) {
            r = rs + (re - rs) * len;
        } else if (len > 1.0) {
            r = re;
        }
        return (d - r - rp) <= 0.0;
    }

protected:
//    Point position_;
};

typedef core::PointPrimitiveT<expressive::Point> PointPrimitive;
//typedef core::PointPrimitiveT<expressive::Vector> Vector;
//typedef core::PointPrimitiveT<expressive::Normal> Normal;
//typedef core::PointPrimitiveT<expressive::Color> Color;
typedef core::PointPrimitiveT<expressive::Point2> PointPrimitive2D;
typedef core::PointPrimitiveT<expressive::Point4> PointPrimitive4D;

} // Close namespace core

} // Close namespace expressive

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_POINTPRIMITIVE
extern template class expressive::core::PointPrimitiveT<expressive::Point>;
extern template class expressive::core::PointPrimitiveT<expressive::Point2>;
extern template class expressive::core::PointPrimitiveT<expressive::Point4>;
#endif
#endif

// implementation file
#include<core/geometry/PointPrimitive.hpp>

#endif // POINT_H_
