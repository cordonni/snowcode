#ifndef BASICTRIMESH_DEFINES_HPP
#define BASICTRIMESH_DEFINES_HPP

//#include "core/geometry/BasicTriMeshDefines.h"
#include "ork/render/Types.h"

namespace expressive
{

namespace core
{

namespace basictrimesh
{

///////////////////////////
/////// VertexIter
template<typename Mesh>
VertexIter<Mesh>::VertexIter()
{
}

template<typename Mesh>
VertexIter<Mesh>::VertexIter(const Mesh &_mesh, VertexHandle _hnd, bool _skip) :
    Base(_mesh,_hnd,_skip)
{
}

///////////////////////////
/////// FaceIter
template<typename Mesh>
FaceIter<Mesh>::FaceIter()
{
}

template<typename Mesh>
FaceIter<Mesh>::FaceIter(const Mesh &_mesh, FaceHandle _hnd, bool _skip) :
    Base(_mesh,_hnd,_skip)
{
}

template<typename Mesh>
EdgeIter<Mesh>::EdgeIter()
{
}

template<typename Mesh>
EdgeIter<Mesh>::EdgeIter(const Mesh &_mesh, EdgeHandle _hnd, bool _skip) :
    Base(_mesh, _hnd, _skip)
{
}

///////////////////////////
/////// ConstVertexFaceIter
template<typename Mesh>
ConstVertexFaceIter<Mesh>::ConstVertexFaceIter() :
    mesh_(NULL)
{}


template<typename Mesh>
ConstVertexFaceIter<Mesh>::ConstVertexFaceIter(const Mesh &mesh, VertexHandle vh) :
    mesh_(&mesh),
    cur_(mesh.status(vh).adjacent_faces_.begin()),
    end_(mesh.status(vh).adjacent_faces_.end()),
    f_handle_(*cur_)
{}


template<typename Mesh>
ConstVertexFaceIter<Mesh>::ConstVertexFaceIter(const ConstVertexFaceIter<Mesh> &cvf_it) :
    mesh_(cvf_it.mesh_), cur_(cvf_it.cur_), end_(cvf_it.end_) , f_handle_(cvf_it.f_handle_)
{}

template<typename Mesh>
ConstVertexFaceIter<Mesh> &ConstVertexFaceIter<Mesh>::operator=(const ConstVertexFaceIter<Mesh> &cvf_it)
{
    mesh_ = cvf_it.mesh_;
    cur_ = cvf_it.cur_;
    end_ = cvf_it.end_;
    f_handle_ = cvf_it.f_handle_;
    return *this;
}

template<typename Mesh>
bool ConstVertexFaceIter<Mesh>::operator==(const ConstVertexFaceIter<Mesh> &cvf_it) const
{
    return ((mesh_ == cvf_it.mesh_) &&
            (cur_ == cvf_it.cur_));
}

template<typename Mesh>
bool ConstVertexFaceIter<Mesh>::operator!=(const ConstVertexFaceIter<Mesh> &cvf_it) const
{
    return !operator ==(cvf_it);
}

template<typename Mesh>
ConstVertexFaceIter<Mesh> &ConstVertexFaceIter<Mesh>::operator++()
{
    assert(mesh_);
    ++cur_;
    if (cur_ != end_) {
        f_handle_.idx_ = *cur_;
    }
    return *this;
}

template<typename Mesh>
ConstVertexFaceIter<Mesh> &ConstVertexFaceIter<Mesh>::operator--()
{
    assert(mesh_);
    --cur_;
    if (cur_ != end_) {
        f_handle_.idx_ = *cur_;
    }
    return *this;
}

/// Standard dereferencing operator.
template<typename Mesh>
const FaceHandle& ConstVertexFaceIter<Mesh>::operator*() const {
    return f_handle_;
}

/// Standard pointer operator.
template<typename Mesh>
FaceHandle* ConstVertexFaceIter<Mesh>::operator->() const {
    return &f_handle_;
}

template<typename Mesh>
bool ConstVertexFaceIter<Mesh>::is_valid() const {
    return (mesh_!=NULL && cur_!=end_);
}

// // Removed to stay coherent with the interface of openmesh.3.0 meshes & co ...
//            FaceHandle handle() const {
//                assert(mesh_);
//                return mesh_->face_handle(*cur_);
//            }

//            operator FaceHandle() const {
//                assert(mesh_);
//                return mesh_->face_handle(*cur_);
//            }

//            operator bool() const {
//                return (mesh_!=NULL && cur_!=end_);
//            }


///////////////////////////
/////// ConstFaceVertexIter
template<typename Mesh>
ConstFaceVertexIter<Mesh>::ConstFaceVertexIter() :
    mesh_(NULL), face_(NULL), idx_(0), v_handle_(0)
{}

template<typename Mesh>
ConstFaceVertexIter<Mesh>::ConstFaceVertexIter(const Mesh &mesh, FaceHandle fh) :
    mesh_(&mesh), face_(&(mesh.face(fh))), idx_(0), v_handle_(face_->idx_[0])
{}

template<typename Mesh>
ConstFaceVertexIter<Mesh>::ConstFaceVertexIter(const ConstFaceVertexIter<Mesh> &cfv_it) :
    mesh_(cfv_it.mesh_), face_(cfv_it.face_), idx_(cfv_it.idx_), v_handle_(face_->idx_[idx_])
{}


template<typename Mesh>
ConstFaceVertexIter<Mesh> &ConstFaceVertexIter<Mesh>::operator=(const ConstFaceVertexIter<Mesh> &cfv_it)
{
    mesh_ = cfv_it.mesh_;
    face_ = cfv_it.face_;
    idx_ = cfv_it.idx_;
    v_handle_ = *cfv_it;
    return *this;
}

template<typename Mesh>
bool ConstFaceVertexIter<Mesh>::operator==(const ConstFaceVertexIter<Mesh> &cfv_it) const
{
    return ((mesh_ == cfv_it.mesh_) &&
            (face_ == cfv_it.face_) &&
            (idx_ == cfv_it.idx_));
//                        (v_handle_ == *cfv_it)); // should not be needed ...
}

template<typename Mesh>
bool ConstFaceVertexIter<Mesh>::operator!=(const ConstFaceVertexIter<Mesh> &cfv_it) const
{
    return !operator ==(cfv_it);
}

template<typename Mesh>
ConstFaceVertexIter<Mesh> &ConstFaceVertexIter<Mesh>::operator++()
{
    assert(mesh_);
    ++idx_;
    assert(idx_ < 3 && idx_ >= 0);
    //this line can throw seg fault
    v_handle_.idx_ = face_->idx_[idx_];
    return *this;
}

template<typename Mesh>
ConstFaceVertexIter<Mesh> &ConstFaceVertexIter<Mesh>::operator--()
{
    assert(mesh_);
    --idx_;
    assert(idx_ < 3 && idx_ >= 0);
    v_handle_.idx_ = face_->idx_[idx_];
    return *this;
}

// // Removed to stay coherent with the interface of openmesh.3.0 meshes & co ...
//            VertexHandle handle() const {
//                assert(mesh_);
//                return mesh_->vertex_handle(face_->idx_[idx_]);
//            }

/// Standard dereferencing operator.
template<typename Mesh>
const VertexHandle& ConstFaceVertexIter<Mesh>::operator*() const {
    return v_handle_;
}

/// Standard pointer operator.
template<typename Mesh>
VertexHandle* ConstFaceVertexIter<Mesh>::operator->() const {
    return &v_handle_;
}

//            operator VertexHandle() const {
//                assert(mesh_);
//                return mesh_->vertex_handle(face_->idx_[idx_]);
//            }

//            operator bool() const {
//                return (mesh_!=NULL && face_!=NULL && idx_ < 3);
//            }

}

///////////////////////////
/////// DefaultMeshVertex
template<typename MESH>
void DefaultMeshVertex::RegisterAttributeTypes(MESH mesh)
{
    if (sizeof(Scalar) == sizeof(double)) {
        mesh->addAttributeType(0, 3, ork::A64F, false);
        mesh->addAttributeType(1, 3, ork::A64F, true);
        mesh->addAttributeType(2, 2, ork::A64F, false);
    } else if (sizeof(Scalar) == sizeof(float)) {
        mesh->addAttributeType(0, 3, ork::A32F, false);
        mesh->addAttributeType(1, 3, ork::A32F, false);
        mesh->addAttributeType(2, 2, ork::A32F, false);
    }

    mesh->addAttributeType(3, 4, ork::A32F, false);
}

}

}

#endif
