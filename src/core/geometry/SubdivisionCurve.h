/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: SubdivisionCurve.h

   Language: C++

   License: Expressive license

   \author: Cedric Zanni
   E-Mail:  cedric.zanni@inria.fr

   Description: Header for SubdivisionCurve.

   Platform Dependencies: None
*/

//#pragma once
#ifndef EXPRESSIVE_SUBDIVISION_CURVE_H_
#define EXPRESSIVE_SUBDIVISION_CURVE_H_

// core dependencies
#include<core/CoreRequired.h>
    // geometry
    #include<core/geometry/AABBox.h>
    #include<core/geometry/BoundingSphere.h>
    #include<core/geometry/GeometryPrimitive.h>
    // utils
    #include<core/utils/Serializable.h>
    #include <core/xmlloaders/TraitsXmlQuery.h>

namespace expressive {

namespace core {

//TODO:@todo : WARNING : for now, assume that the template parameter are WeightedPoint
// this is due to the fact that it would be dangerous to overload the + and * operator in weighted point
// on the other end we could provide a barycenter method ?

template<typename PointT>
/**
 * A Subdivision curve. Based on GeometryPrimitive, this one offers a few more functions for
 * subdivision, position and length computation, etc...
 */
class SubdivisionCurveT : public GeometryPrimitiveT<PointT>//public Serializable
{
public:
    EXPRESSIVE_MACRO_NAME("SubdivisionCurve")

    typedef typename PointT::BasePoint BasePoint;

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    /*! \brief Build a SubdivisionCurve consisting of a unique segment along the first axis.
     */
    SubdivisionCurveT()
        : GeometryPrimitiveT<PointT>(), nb_subdiv_(0), loop_(false)
    {
        //TODO:@todo : kind of ugly but dont see other solution ...
        GeometryPrimitiveT<PointT>::points_.push_back(PointT());
        GeometryPrimitiveT<PointT>::points_.push_back(PointT());
        GeometryPrimitiveT<PointT>::points_[1][0] = 1.0;
//        ComputeHelpVariables();

        //TODO:@todo : add a subdivision threshold and use it to initialize
        subdivision_ = GeometryPrimitiveT<PointT>::points_;
    }

    SubdivisionCurveT(const std::vector<PointT>& points, bool isLoop = false)
        : GeometryPrimitiveT<PointT>(), nb_subdiv_(0), loop_(isLoop)
    {
//        GeometryPrimitiveT<PointT>::set_is_loop(isLoop);
        GeometryPrimitiveT<PointT>::points_ = points;
//        ComputeHelpVariables();

        //TODO:@todo : add a subdivision threshold and use it to initialize
        subdivision_ = GeometryPrimitiveT<PointT>::points_;
    }

    SubdivisionCurveT(const SubdivisionCurveT &rhs, unsigned int start = -1, unsigned int end = -1)
        :   GeometryPrimitiveT<PointT>(rhs, start, end), subdivision_(rhs.subdivision_.begin() + (start == (uint)-1 ? 0 : start), rhs.subdivision_.begin() + (end == (uint)-1 ? 0 : end)), nb_subdiv_(rhs.nb_subdiv_), loop_(rhs.loop_)
    {
        //////////////////////////////////////////////////////////////////////////////////////
                //TODO:@todo : hack because subdivision is not copied correctly ...
                this->subdivision_ = rhs.subdivision_;
        //////////////////////////////////////////////////////////////////////////////////////

    }

//    using GeometryPrimitiveT<PointT>::operator delete; // necessary because of eigen's macro.
    virtual ~SubdivisionCurveT(){}

    virtual std::shared_ptr<core::GeometryPrimitiveT<PointT> > clone(unsigned int /*start*/ = -1, unsigned int /*end*/ = -1) const
    {
        return std::make_shared<SubdivisionCurveT>(*this);
    }

    ///////////
    /// Xml ///
    ///////////

    void InitFromXml(const TiXmlElement *element)
    {
        int nb_subdiv = 0;
        TraitsXmlQuery::QueryIntAttribute("SubdivisionCurve::InitFromXml", element, "nb_subdiv", &nb_subdiv);
        set_nb_subdiv(nb_subdiv);
    }

    unsigned int GetSubdividedSize() const;

    virtual Scalar GetLength() const;
    /////////////////

    // TODO:@todo : this function should call a functor ...
    virtual void ComputeSubdivision()
    { //TODO:@todo : naive implementation ..., can be easily changed to obtain another averaging mask
        subdivision_.clear();

        std::vector<PointT> res_points = GeometryPrimitiveT<PointT>::points_;
        std::vector<PointT> tmp_points;
        if (res_points.size() > 1) {
            for(int i = 0; i<nb_subdiv_; ++i)
            {
                // subdivision process
                tmp_points.resize(res_points.size()*2-1);
                for(unsigned int j = 0; j<tmp_points.size(); ++j)
                {
                    int j_div2 = j/2;
                    if(j%2 == 0) {
                        tmp_points[j].set_pos(res_points[j_div2]);
                    } else {
                        tmp_points[j].set_pos(0.5*(res_points[j_div2] + res_points[j_div2+1]));
                    }

                }

                //averaging process
                res_points.resize(tmp_points.size());
                res_points.front() = tmp_points.front();
                for(unsigned int j = 1; j<tmp_points.size()-1; ++j)
                {
                    res_points[j].set_pos(0.5*(tmp_points[j] + tmp_points[j+1]));
                }
                res_points.back() = tmp_points.back();
            }
        }

        // should use a move operator ?
        subdivision_ = res_points;
    }

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_points(const std::vector<PointT>& points)
    {
        GeometryPrimitiveT<PointT>::points_ = points;
        ComputeSubdivision();
    }

    /**
     * Sets the number of subdivision levels for this curve.
     */
    void set_nb_subdiv(int n)
    {
        nb_subdiv_ = n;
        ComputeSubdivision();
    }

    virtual void updated()
    {
        GeometryPrimitiveT<PointT>::updated();
        ComputeSubdivision();
    }

    /////////////////
    /// Attributs ///
    /////////////////
    virtual unsigned int Split(unsigned int index, const PointT &pos, std::set<std::shared_ptr<GeometryPrimitiveT<PointT> > > &newPrimitives);

    virtual void AddPoint(PointT p, unsigned int index = -1)
    {
        GeometryPrimitiveT<PointT>::AddPoint(p, index);
        ComputeSubdivision();
    }

    virtual void RemovePoint(unsigned int index)
    {
        GeometryPrimitiveT<PointT>::RemovePoint(index);
        ComputeSubdivision();
    }

    virtual void RemovePoints()
    {
        GeometryPrimitiveT<PointT>::RemovePoints();
        ComputeSubdivision();
    }

    virtual void CopyPoints(const GeometryPrimitiveT<PointT> &other, unsigned int reversed)
    {
        GeometryPrimitiveT<PointT>::CopyPoints(other, reversed);
        ComputeSubdivision();
    }

    virtual void SetPoint(unsigned int index, const PointT &p)
    {
        GeometryPrimitiveT<PointT>::SetPoint(index, p);
        ComputeSubdivision();
    }

    inline virtual bool isLoop() const {return loop_ ;}

    inline virtual void set_is_loop(bool loop = true) { loop_ = loop; }

    virtual bool isFilled() const { return false; }

    virtual BasePoint getPointAtLength(Scalar l);
protected :
    std::vector<PointT> subdivision_;
    
    int nb_subdiv_; //TODO:@todo : provisoir

    bool loop_;
    
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
public:
//    virtual const PointT &GetStart() const { return GeometryPrimitiveT<PointT>::points_[0]; }

//    virtual const PointT &GetEnd() const { return GeometryPrimitiveT<PointT>::points_[1]; }

    ///////////////////
    // Xml functions //
    ///////////////////

    virtual TiXmlElement * ToXml(bool embed_vertices = false, const char *name = NULL) const;

//    ///////////////////////////
//    // Bounding-box function //
//    ///////////////////////////
//
//    AABBox GetAxisBoundingBox() const;
//    BoundingSphere GetBoundingSphere() const;
//
//    ///////////////
//    // Modifiers //
//    ///////////////
//
////    void set_points(const PointT& p1, const PointT& p2);
////    void set_p1(const PointT& p1);
////    void set_p2(const PointT& p2);
//
//    ///////////////
//    // Accessors //
//    ///////////////
//
//    const PointT& p1()    const { return GeometryPrimitiveT<PointT>::points_[0]; }
//    const PointT& p2()    const { return GeometryPrimitiveT<PointT>::points_[1]; }
//
//    const Vector& dir()         const { return dir_;        }
//    const Vector& unit_dir()    const { return unit_dir_;   }
//
//    Scalar length()       const { return length_;     }
//    Scalar sqrlength()    const { return sqrlength_;  }
//    Scalar invlength()    const { return invlength_;  }
//
//    ///////////////
//    // Attributs //
//    ///////////////
//
//    friend bool operator<(const SegmentT& g1, const SegmentT& g2)
//    {
//        return g1.IsInferiorTo(g2);
//    }
//
//    virtual bool IsInferiorTo(const SegmentT& g2) const
//    {
//        if ((GeometryPrimitiveT<PointT>::points_[0] != g2.GeometryPrimitiveT<PointT>::points_[0] || GeometryPrimitiveT<PointT>::points_[1] != g2.GeometryPrimitiveT<PointT>::points_[1]) && (GeometryPrimitiveT<PointT>::points_[0] != g2.GeometryPrimitiveT<PointT>::points_[1] || GeometryPrimitiveT<PointT>::points_[1] != g2.GeometryPrimitiveT<PointT>::points_[0])) {
//            return (GeometryPrimitiveT<PointT>::points_[0] != g2.GeometryPrimitiveT<PointT>::points_[0]) ? GeometryPrimitiveT<PointT>::points_[0] < g2.GeometryPrimitiveT<PointT>::points_[0] : GeometryPrimitiveT<PointT>::points_[1] < g2.GeometryPrimitiveT<PointT>::points_[1];
//        }
//        return GeometryPrimitiveT<PointT>::IsInferiorTo(g2);
//    }
//
//    friend bool operator==(const SegmentT& g1, const SegmentT& g2)
//    {
//        return g1.Equals(g2);
//    }
//
//    virtual bool Equals(const SegmentT& g2) const
//    {
//        if ((GeometryPrimitiveT<PointT>::points_[0] != g2.GeometryPrimitiveT<PointT>::points_[0] || GeometryPrimitiveT<PointT>::points_[1] != g2.GeometryPrimitiveT<PointT>::points_[1]) && (GeometryPrimitiveT<PointT>::points_[0] != g2.GeometryPrimitiveT<PointT>::points_[1] || GeometryPrimitiveT<PointT>::points_[1] != g2.GeometryPrimitiveT<PointT>::points_[0])) {
//            return false;
//        }
//        return GeometryPrimitiveT<PointT>::Equals(g2);
//    }
//
//protected:
//
////    PointT GeometryPrimitiveT<PointT>::points_[0];
////    PointT GeometryPrimitiveT<PointT>::points_[1];
//
//    // Help variables for divers computation on the segment.
//    // This will be used to improve computation speed in divers cases.
//    Vector dir_;
//    Vector unit_dir_;
//    Scalar length_;
//    Scalar sqrlength_;
//    Scalar invlength_;
//
//    virtual void ComputeHelpVariables()
//    {
//        dir_        = GeometryPrimitiveT<PointT>::points_[1] - GeometryPrimitiveT<PointT>::points_[0];
//        length_        = dir_.norm();
//        sqrlength_    = dir_.sqrnorm();
//        invlength_    = 1.0/length_;
//        unit_dir_    = dir_;
//        unit_dir_.normalize();
//    }
};

template<typename PointT>
TiXmlElement * SubdivisionCurveT<PointT>::ToXml(bool embed_vertices, const char *name) const
{
    TiXmlElement * element = GeometryPrimitiveT<PointT>::ToXml(embed_vertices, name);

    element->SetAttribute("nb_subdiv", nb_subdiv_);

    return element;
}

template<typename PointT>
unsigned int SubdivisionCurveT<PointT>::Split(unsigned int index, const PointT &pos, std::set<std::shared_ptr<GeometryPrimitiveT<PointT> > > &newPrimitives)
{
    UNUSED(newPrimitives);
    AddPoint(pos, index + 1);
    return index + 1;
}

template<typename PointT>
unsigned int SubdivisionCurveT<PointT>::GetSubdividedSize() const
{
    return (unsigned int) subdivision_.size();
}

template<typename PointT>
Scalar SubdivisionCurveT<PointT>::GetLength() const
{
    Scalar l = 0.0;
    for (unsigned int i = 1; i < subdivision_.size(); ++i) {
        const PointT &p = subdivision_[i - 1];
        const PointT &c = subdivision_[i];
        l += (c - p).norm();
    }
    return l;
}

template<>
void CORE_API SubdivisionCurveT<WeightedPoint>::ComputeSubdivision();


template<typename PointT>
typename PointT::BasePoint SubdivisionCurveT<PointT>::getPointAtLength(Scalar l)
{

    assert(subdivision_.size() > 1);
    if (l <= 0.0) {
        return subdivision_[0];
    } else {
        Scalar totalLength = this->GetLength();
        if (l >= totalLength) {
            return subdivision_[subdivision_.size() - 1];
        }

        Scalar curLen = 0.0;
        Scalar prevLen = 0.0;
        unsigned int index = 1;
        for (index = 1; index < subdivision_.size(); ++index) {
            const PointT &p = subdivision_[index - 1];
            const PointT &c = subdivision_[index];
            prevLen = curLen;
            curLen += (c - p).norm();
            if (curLen >= l) { // we've reached the interval on which the point we need is.
                Scalar ratio = (l - prevLen) / (curLen - prevLen);
                return subdivision_[index - 1] * (1.0  - ratio) + c * ratio;
            }
        }
        return subdivision_[subdivision_.size() - 1];
    }
}

/////////////////////////////
//// Bounding-box function //
/////////////////////////////
//
//template<typename PointT>
//AABBox SegmentT<PointT>::GetAxisBoundingBox()  const
//{
//    AABBox res;
//
//    for(unsigned int i = 0; i < Vector::size_; ++i)
//    {
//        if(GeometryPrimitiveT<PointT>::points_[0][i] < GeometryPrimitiveT<PointT>::points_[1][i])
//        {
//            res.min_[i] = GeometryPrimitiveT<PointT>::points_[0][i];
//            res.max_[i] = GeometryPrimitiveT<PointT>::points_[1][i];
//        }
//        else
//        {
//            res.min_[i] = GeometryPrimitiveT<PointT>::points_[1][i];
//            res.max_[i] = GeometryPrimitiveT<PointT>::points_[0][i];
//        }
//    }
//    return res;
//}
//
//template<typename PointT>
//BoundingSphere SegmentT<PointT>::GetBoundingSphere() const
//{
//    BoundingSphere res;
//    res.radius_ = 0.5*length_;
//    res.center_ = 0.5*(GeometryPrimitiveT<PointT>::points_[0]+GeometryPrimitiveT<PointT>::points_[1]);
//
//    return res;
//}
//
//
//
/////////////////
//// Modifiers //
/////////////////
//
//template<typename PointT>
//void SegmentT<PointT>::set_points(const PointT& p1, const PointT& p2)
//{
//    GeometryPrimitiveT<PointT>::points_[0] = p1;
//    GeometryPrimitiveT<PointT>::points_[1] = p2;
//    ComputeHelpVariables();
//}
//
//template<typename PointT>
//void SegmentT<PointT>::set_p1(const PointT& p1)
//{
//    GeometryPrimitiveT<PointT>::points_[0] = p1;
//    ComputeHelpVariables();
//}
//
//template<typename PointT>
//void SegmentT<PointT>::set_p2(const PointT& p2){
//    GeometryPrimitiveT<PointT>::points_[1] = p2;
//    ComputeHelpVariables();
//}
//
//typedef SegmentT<Point> Segment;

typedef SubdivisionCurveT<PointPrimitive> SubdivisionCurve;
typedef SubdivisionCurveT<PointPrimitive2D> SubdivisionCurve2D;

typedef SubdivisionCurveT<WeightedPoint> WeightedSubdivisionCurve;
typedef SubdivisionCurveT<WeightedPoint2D> WeightedSubdivisionCurve2D;

} // Close namespace core

} // Close namespace expressive


#endif // SEGMENT_H_
