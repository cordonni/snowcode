/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: WeightedPointWithDensity.h

   Language: C++

   License: Expressive license

   \author: Cedric Zanni
   E-Mail:  cedric.zanni@inria.fr

   Description: Header for a a very simple class of a weighted point
                with an additional density (scalar) information

   Platform Dependencies: None
*/

#pragma once
#ifndef WEIGHTED_POINT_WITH_DENSITY_H_
#define WEIGHTED_POINT_WITH_DENSITY_H_


// core dependencies
#include<core/CoreRequired.h>
    // geometry
    #include<core/geometry/WeightedPoint.h>
    // utils
    #include<core/utils/Serializable.h>

namespace expressive {

namespace core {

template<typename BasePoint>
class WeightedPointWithDensityT : public WeightedPointT<BasePoint> {
public:
    EXPRESSIVE_MACRO_NAME("WeightedPointWithDensity")

    typedef WeightedPointT<BasePoint> WeightedPoint;

    //////////////////////////////
    /// Constructors/destructor ///
    //////////////////////////////

    WeightedPointWithDensityT() : WeightedPointT<BasePoint>(), density_(1.0) { }

    WeightedPointWithDensityT(const WeightedPointT<BasePoint>& w_pt, Scalar density)
        : WeightedPointT<BasePoint>(w_pt), density_(density) { }

    //TODO:@todo : argh : only work in dim 3
    WeightedPointWithDensityT(Scalar x, Scalar y, Scalar z, Scalar weight, Scalar density)
        : WeightedPointT<BasePoint>(x, y, z, weight), density_(density) { }

    WeightedPointWithDensityT(const WeightedPointWithDensityT &rhs)
        : WeightedPointT<BasePoint>(rhs), density_(rhs.density_) { }

    virtual ~WeightedPointWithDensityT(){ }

    virtual void updated() {}

    virtual std::shared_ptr<PointPrimitiveT<BasePoint> > clone() const {return std::make_shared<WeightedPointWithDensityT<BasePoint> >(*this); }

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual TiXmlElement * ToXml(const char *name = nullptr) const
    {
        TiXmlElement * element = WeightedPointT<BasePoint>::ToXml(name);
        element->SetDoubleAttribute("density",density_);
        return element;
    }

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_density (Scalar density)   { density_ = density; }

    /////////////////
    /// Accessors ///
    /////////////////

    Scalar density()  const { return density_; }


    bool operator==(const WeightedPointWithDensityT& p) const
    {
//        return (density_ == p.density_) && (this->weight_ == p.weight_) && (this->array() == p.array()).all();
        if (density_ != p.density_ || this->weight_ != p.weight_) {
            return false;
        }
        for (unsigned int i = 0; i < BasePoint::RowsAtCompileTime; ++i) {
            if ((*this)[i] != p[i]) {
                return false;
            }
        }
        return true;
    }

    bool operator<(const WeightedPointWithDensityT& p) const
    {
        //TODO:@todo : to be checked and cleaned ...
        for (unsigned int i = 0; i < BasePoint::RowsAtCompileTime; ++i) {
            if ((*this)[i] < p[i]) {
                return true;
            } else if ((*this)[i] > p[i]) {
                return false;
            }
        }

        return ((*this).weight() < p.weight()) && ((*this).density() < p.density());
    }

    WeightedPointWithDensityT& operator=(const WeightedPointT<BasePoint>& p)
    {
        this->WeightedPointT<BasePoint>::operator=(p);
        return *this;
    }

    WeightedPointWithDensityT& operator=(const WeightedPointWithDensityT& p)
    {
        this->WeightedPointT<BasePoint>::operator=(p);
        density_ = p.density_;
        return *this;
    }

    void set_pos(const WeightedPointWithDensityT &p)
    {
        this->array() = p.array();
        this->weight_ = p.weight_;
        density_ = p.density_;
    }

protected:
    Scalar density_;
};

typedef WeightedPointWithDensityT<expressive::Point> WeightedPointWithDensity;

} // Close namespace core
} // Close namespace expressive

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_WEIGHTEDPOINTWITHDENSITY
extern template class expressive::core::WeightedPointWithDensityT<expressive::Point>;
#endif
#endif

#endif // WEIGHTED_POINT_WITH_DENSITY_H_
