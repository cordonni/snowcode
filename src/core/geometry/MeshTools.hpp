#ifndef MESHTOOLS_HPP
#define MESHTOOLS_HPP

#include <core/geometry/MeshTools.h>
#include <core/geometry/BasicTriMesh.h>

#include <core/geometry/Frame.h>

namespace expressive {

namespace core {

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateFromObj(const std::string &filename)
{
    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();
    mesh->LoadFromObjFile(filename);

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateCross(const core::Frame& f)
{
    return CreateCross(f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateCross(const Point& p0, const Vector &dx, const Vector & dy, const Vector & dz)
{
    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >(ork::LINES);

    AbstractTriMesh::VertexHandle h;

    AbstractTriMesh::VertexHandle h0 = mesh->add_vertex(VERTEX(p0     ,  Normal::UnitY()));   // 0
    AbstractTriMesh::VertexHandle h1 = mesh->add_vertex(VERTEX(p0 + dx,  Normal::UnitX()));   // 1
    AbstractTriMesh::VertexHandle h2 = mesh->add_vertex(VERTEX(p0 - dx, -Normal::UnitX()));   // 2
    AbstractTriMesh::VertexHandle h3 = mesh->add_vertex(VERTEX(p0 + dy,  Normal::UnitY()));   // 3
    AbstractTriMesh::VertexHandle h4 = mesh->add_vertex(VERTEX(p0 - dy, -Normal::UnitY()));   // 4
    AbstractTriMesh::VertexHandle h5 = mesh->add_vertex(VERTEX(p0 + dz,  Normal::UnitZ()));   // 5
    AbstractTriMesh::VertexHandle h6 = mesh->add_vertex(VERTEX(p0 - dz, -Normal::UnitZ()));   // 6

    mesh->add_line(h0, h1);
    mesh->add_line(h0, h2);
    mesh->add_line(h0, h3);
    mesh->add_line(h0, h4);
    mesh->add_line(h0, h5);
    mesh->add_line(h0, h6);


    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateCube(const core::Frame& f)
{
    return CreateCube(f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateCube(const Point& p0, const Vector &dx, const Vector & dy, const Vector & dz)
{
    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();

    AbstractTriMesh::VertexHandle h;
    Scalar x = sqrt(3);

    h = mesh->add_vertex(VERTEX(p0               , Normal(-x, -x, -x)));   // 0
    h = mesh->add_vertex(VERTEX(p0 + dx          , Normal( x, -x, -x)));   // 1
    h = mesh->add_vertex(VERTEX(p0      + dy     , Normal(-x,  x, -x)));   // 2
    h = mesh->add_vertex(VERTEX(p0 + dx + dy     , Normal( x,  x, -x)));   // 3
    h = mesh->add_vertex(VERTEX(p0           + dz, Normal(-x, -x,  x)));   // 4
    h = mesh->add_vertex(VERTEX(p0 + dx      + dz, Normal( x, -x,  x)));   // 5
    h = mesh->add_vertex(VERTEX(p0      + dy + dz, Normal(-x,  x,  x)));   // 6
    h = mesh->add_vertex(VERTEX(p0 + dx + dy + dz, Normal( x,  x,  x)));   // 7


    mesh->add_face( 0, 3, 1);
    mesh->add_face( 0, 2, 3);

    mesh->add_face( 0, 6, 2);
    mesh->add_face( 0, 4, 6);

    mesh->add_face( 0, 5, 4);
    mesh->add_face( 0, 1, 5);


    mesh->add_face( 7, 6, 4);
    mesh->add_face( 7, 4, 5);

    mesh->add_face( 7, 5, 1);
    mesh->add_face( 7, 1, 3);

    mesh->add_face( 7, 3, 2);
    mesh->add_face( 7, 2, 6);


    //    mesh->ComputeNormals();


    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateTetrahedron(const core::Frame& f)
{
    return CreateTetrahedron(f.p0(), f.px(), f.py(), f.pz());
}
template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateTetrahedron(const Point& p0, const Vector &dx, const Vector & dy, const Vector & dz)
{
    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();

    AbstractTriMesh::VertexHandle h;

    mesh->add_vertex(p0 - dx - dy + dz);
    mesh->add_vertex(p0 + dx + dy + dz);
    mesh->add_vertex(p0 + dx - dy - dz);
    mesh->add_vertex(p0 - dx + dy - dz);

    //    mesh->add_face(0, 1, 2);
    //    mesh->add_face(0, 2, 3);
    //    mesh->add_face(0, 3, 1);
    //    mesh->add_face(2, 1, 3);

    mesh->add_face(1, 0, 2);
    mesh->add_face(2, 0, 3);
    mesh->add_face(3, 0, 1);
    mesh->add_face(1, 2, 3);

    mesh->ComputeNormals();

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateCylinder(const core::Frame& f, const uint subdiv, const double ratio)
{
    return CreateCylinder(subdiv, ratio, f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateCylinder(const uint subdiv,
                                                                           const double ratio,
                                                                           const Point& p0,
                                                                           const Vector& dx,
                                                                           const Vector& dy,
                                                                           const Vector& dz)
{

    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX>>();

    auto vh = mesh->add_vertex(p0 + dx);
    mesh->set_normal(vh, dx.normalized());
    mesh->set_uv(vh, Point2(0,0));

    double z = dz.norm() * 2;

    vh = mesh->add_vertex(p0 + dx * ratio + dz);
    mesh->set_normal(vh, dx.normalized());
    mesh->set_uv(vh, Point2(0,z));

    //    double d = std::sqrt(dx.squaredNorm() + dy.squaredNorm());

    for(uint i = 1; i <= subdiv; i++)
    {
        double alpha = -2 * M_PI * i / (subdiv-1);
        Point p = dx * cos(alpha) + dy * sin(alpha);
        Normal n = p.normalized();
        //        BasicTriMeshT<VERTEX>::VertexHandle vh = mesh->add_vertex(p0 + p);
        auto vh = mesh->add_vertex(p0 + p);
        mesh->set_normal(vh, n);
        mesh->set_uv(vh, Point2(float(i)/(subdiv-1),0));

        vh = mesh->add_vertex(p0 + p * ratio + dz);
        mesh->set_normal(vh, n);
        mesh->set_uv(vh, Point2(float(i)/(subdiv-1),z));

        mesh->add_face(2 * (i-1), 2 * (i-1) + 1, 2 * i);
        mesh->add_face(2 * i, 2 * (i-1) + 1, 2 * i + 1);
    }

    //    mesh->add_face(2 * (subdiv-1), 2 * (subdiv-1) + 1, 0);
    //    mesh->add_face(0, 2 * (subdiv-1) + 1, 1);


    //    mesh->ComputeNormals();

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateCapsule(const core::Frame& f, const uint subdiv, const double ratio)
{
    return CreateCapsule(subdiv, ratio, f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateCapsule(const uint subdiv,
                                                                          const double ratio,
                                                                          const Point& p0,
                                                                          const Vector& dx,
                                                                          const Vector& dy,
                                                                          const Vector& dz)
{

    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();


    for(uint i = 0; i < subdiv; i++)
    {
        double alpha = -2 * M_PI * i / subdiv;
        mesh->add_vertex(p0 + dx * cos(alpha) + dy * sin(alpha));
    }

    for(uint i = 0; i < subdiv; i++)
    {
        double alpha = -2 * M_PI * i / subdiv;
        mesh->add_vertex(p0 + (dx * cos(alpha) + dy * sin(alpha)) * ratio + dz);
    }

    for(uint i = 0; i < subdiv; i++)
    {
        mesh->add_face(i, (i+1)%subdiv, i+subdiv);
        mesh->add_face(i+subdiv, (i+1)%subdiv, (i+1)%subdiv+subdiv);
    }


    Vector dzN = dz.normalized();
    double r = dx.norm();

    uint N = 4;

    for(uint j = 1; j < N; j++)
    {
        double beta = 0.5 * M_PI * double(j) / N;
        for(uint i = 0; i < subdiv; i++)
        {
            double alpha = -2 * M_PI * double(i) / subdiv;

            mesh->add_vertex(p0
                             + dx * cos(alpha) * cos(beta)
                             + dy * sin(alpha) * cos(beta)
                             + dzN * r*sin(beta)
                             + dz);
        }
    }

    mesh->add_vertex(p0 + dzN * r + dz);

    for(uint j = 0; j < N - 1; j++)
    {
        for(uint i = 0; i < subdiv; i++)
        {
            mesh->add_face(subdiv * (j+1) + i,
                           subdiv * (j+1) + (i+1)%subdiv,
                           subdiv * (j+2) + i);
            mesh->add_face(subdiv * (j+2) + i,
                           subdiv * (j+1) + (i+1)%subdiv,
                           subdiv * (j+2) + (i+1)%subdiv);
        }
    }

    for(uint i = 0; i < subdiv; i++)
    {
        mesh->add_face(subdiv * (N) + i,
                       subdiv * (N) + (i+1)%subdiv,
                       subdiv * (N+1));
    }


    mesh->ComputeNormals();

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateCone(const core::Frame& f, const uint subdiv)
{
    return CreateCone(subdiv, f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateCone(const uint subdiv,
                                                                       const Point& p0,
                                                                       const Vector& dx,
                                                                       const Vector& dy,
                                                                       const Vector& dz)
{
    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();

    mesh->add_vertex(p0 + dz);
    mesh->add_vertex(p0 + dx);
    for(uint i = 2; i < subdiv+1; i++)
    {
        double alpha = -2 * M_PI * (i-1) / subdiv;
        mesh->add_vertex(p0 + dx * cos(alpha) + dy * sin(alpha));

        mesh->add_face(i-1, 0, i);
    }
    mesh->add_face(subdiv, 0, 1);

    mesh->ComputeNormals();

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateDisk(const core::Frame& f, const uint subdiv)
{
    return CreateDisk(subdiv, f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateDisk(const uint subdiv,
                                                                       const Point& p0,
                                                                       const Vector& dx,
                                                                       const Vector& dy,
                                                                       const Vector& /*dz*/)
{
    return CreateCone(subdiv, p0, dx, -dy, Vector(0,0,0));
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateQuad(const core::Frame& f)
{
    return CreateQuad(f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateQuad(
        const Point& p0,
        const Vector& dx,
        const Vector& dy,
        const Vector& /*dz*/)
{

    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();

    AbstractTriMesh::VertexHandle h;


    h = mesh->add_vertex(p0);   // 0
    mesh->set_uv(h, Point2(0.0, 0.0));
    //    mesh->set_normal(h, -dz);
    h = mesh->add_vertex(p0 + dx);   // x
    mesh->set_uv(h, Point2(1.0, 0.0));
    //    mesh->set_normal(h, -dz);
    h = mesh->add_vertex(p0 + dx + dy);   // xy
    mesh->set_uv(h, Point2(1.0, 1.0));
    //    mesh->set_normal(h, -dz);
    h = mesh->add_vertex(p0 + dy);   // y
    mesh->set_uv(h, Point2(0.0, 1.0));
    //    mesh->set_normal(h, -dz);
    mesh->add_face( 0, 1, 2); //   0 x xy
    mesh->add_face( 0, 2, 3); //   0 xy y

    mesh->ComputeNormals();

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateSphere(const core::Frame& f, const uint subdiv_u, const uint subdiv_v)
{
    return CreateSphere(subdiv_u, subdiv_v, f.p0(), f.px(), f.py(), f.pz());
}

//template<typename VERTEX>
//std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateSphere(const uint subdiv_u,
//                                                        const uint subdiv_v,
//                                                        const Point& p0,
//                                                        const Vector& dx,
//                                                        const Vector& dy,
//                                                        const Vector& dz)
//{
//    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();


//    mesh->add_vertex(p0 + dz);
//    for(uint j = 1; j < subdiv_v - 1; j++)
//    {
//        double phi = M_PI * j / (subdiv_v-1);
//        for(uint i = 0; i < subdiv_u; i++)
//        {
//            double theta = -2 * M_PI * i / subdiv_u;
//            mesh->add_vertex(p0 + (dx * cos(theta) + dy * sin(theta)) * sin(phi) + dz * cos(phi));
//        }
//    }
//    mesh->add_vertex(p0 - dz);


//    for(uint i = 0; i < subdiv_u; i++)
//    {
//        mesh->add_face( i+1,
//                        0,
//                       (i+1)%subdiv_u+1);
//    }


//    for(uint j = 1; j < subdiv_v - 2; j++)
//    {
//        for(uint i = 0; i < subdiv_u; i++)
//        {
//            mesh->add_face( i             + 1 + (j-1) * subdiv_u,
//                           (i+1)%subdiv_u + 1 + (j-1) * subdiv_u,
//                            i             + 1 +  j    * subdiv_u);

//            mesh->add_face( i             + 1 +  j    * subdiv_u,
//                           (i+1)%subdiv_u + 1 + (j-1) * subdiv_u,
//                           (i+1)%subdiv_u + 1 +  j    * subdiv_u);
//        }
//    }

//    for(uint i = 0; i < subdiv_u; i++)
//    {
//        mesh->add_face((subdiv_v-3) * subdiv_u + i + 1,
//                       (subdiv_v-3) * subdiv_u + (i+1)%subdiv_u + 1,
//                       (subdiv_v-2) * subdiv_u + 1);
//    }


//    mesh->ComputeNormals();

//    return mesh;
//}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateSphere(const uint subdiv_u,
                                                                         const uint subdiv_v,
                                                                         const Point& p0,
                                                                         const Vector& dx,
                                                                         const Vector& dy,
                                                                         const Vector& dz)
{
    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();

    for(uint j = 0; j < subdiv_v; j++)
    {
        double v = double(j) / (subdiv_v-1);
        double phi = M_PI * v;

        for(uint i = 0; i < subdiv_u; i++)
        {
            double u = double(i) / (subdiv_u-1);
            double theta = -2 * M_PI * u;

            Vector n = (dx * cos(theta) + dy * sin(theta)) * sin(phi) + dz * cos(phi);

            auto vh = mesh->add_vertex(p0 + n);
            mesh->set_normal(vh, n.normalized());
            mesh->set_uv(vh, Vector2(u, v));
        }
    }

    for(uint j = 0; j < subdiv_v-1; j++)
    {
        for(uint i = 0; i < subdiv_u-1; i++)
        {
            mesh->add_quad_face(i   +  j    * subdiv_u,
                                i+1 +  j    * subdiv_u,
                                i+1 + (j+1) * subdiv_u,
                                i   + (j+1) * subdiv_u);
        }
    }

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateIcosahedron(const core::Frame& f)
{
    return CreateIcosahedron(f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateIcosahedron(const Point& p0,
                                                                              const Vector& dx,
                                                                              const Vector& dy,
                                                                              const Vector& dz)
{
    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();

    // create 12 vertices of an icosahedron
    Scalar t = (1.0 + sqrt(5.0)) / 2.0;

    mesh->add_vertex(VERTEX(Point(-1,  t,  0)));
    mesh->add_vertex(VERTEX(Point( 1,  t,  0)));
    mesh->add_vertex(VERTEX(Point(-1, -t,  0)));
    mesh->add_vertex(VERTEX(Point( 1, -t,  0)));

    mesh->add_vertex(VERTEX(Point( 0, -1,  t)));
    mesh->add_vertex(VERTEX(Point( 0,  1,  t)));
    mesh->add_vertex(VERTEX(Point( 0, -1, -t)));
    mesh->add_vertex(VERTEX(Point( 0,  1, -t)));

    mesh->add_vertex(VERTEX(Point( t,  0, -1)));
    mesh->add_vertex(VERTEX(Point( t,  0,  1)));
    mesh->add_vertex(VERTEX(Point(-t,  0, -1)));
    mesh->add_vertex(VERTEX(Point(-t,  0,  1)));


    mesh->add_face(0, 11, 5);
    mesh->add_face(0, 5, 1);
    mesh->add_face(0, 1, 7);
    mesh->add_face(0, 7, 10);
    mesh->add_face(0, 10, 11);

    mesh->add_face(1, 5, 9);
    mesh->add_face(5, 11, 4);
    mesh->add_face(11, 10, 2);
    mesh->add_face(10, 7, 6);
    mesh->add_face(7, 1, 8);

    mesh->add_face(3, 9, 4);
    mesh->add_face(3, 4, 2);
    mesh->add_face(3, 2, 6);
    mesh->add_face(3, 6, 8);
    mesh->add_face(3, 8, 9);

    mesh->add_face(4, 9, 5);
    mesh->add_face(2, 4, 11);
    mesh->add_face(6, 2, 10);
    mesh->add_face(8, 6, 7);
    mesh->add_face(9, 8, 1);


    mesh->EmbedInFrame(Frame(p0, dx, dy, dz));

    mesh->ComputeNormals();

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateIcoSphere(const core::Frame& f, const uint subdiv)
{
    return CreateIcoSphere(subdiv, f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateIcoSphere(const uint subdiv,
                                                                            const Point& p0,
                                                                            const Vector& dx,
                                                                            const Vector& dy,
                                                                            const Vector& dz)
{
    std::shared_ptr<AbstractTriMesh> mesh = CreateIcosahedron();

    for(uint i = 0; i < subdiv; i++)
        mesh = mesh->Subdivide();

    for(AbstractTriMesh::ConstVertexIter v_it = mesh->vertices_begin(); v_it != mesh->vertices_end(); v_it++)
        mesh->set_point(*v_it, mesh->point(*v_it).normalized());


    mesh->EmbedInFrame(Frame(p0, dx, dy, dz));

    mesh->ComputeNormals();

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateQUSphere(const core::Frame& f, const Scalar edge_length)
{
    return CreateQUSphere(edge_length, f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateQUSphere(const Scalar edge_length,
                                                                           const Point& p0,
                                                                           const Vector& dx,
                                                                           const Vector& dy,
                                                                           const Vector& dz)
{
    std::shared_ptr<AbstractTriMesh > mesh = CreateIcosahedron(p0, dx, dy, dz);

    Scalar l = dx.norm();

    while(mesh->EdgeLength(0) > edge_length)
    {
        mesh = mesh->Subdivide();

        for(AbstractTriMesh::ConstVertexIter v_it = mesh->vertices_begin(); v_it != mesh->vertices_end(); v_it++)
            mesh->set_point(*v_it, ((mesh->point(*v_it) - p0).normalized()) * l + p0);
    }

    mesh->ComputeNormals();

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateGrid(const core::Frame& f, const uint subdiv_u, const uint subdiv_v)
{
    return CreateGrid(subdiv_u, subdiv_v, f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh> MeshToolsT<VERTEX>::CreateGrid(const uint subdiv_u,
                                                                      const uint subdiv_v,
                                                                      const Point& p0,
                                                                      const Vector& dx,
                                                                      const Vector& dy,
                                                                      const Vector& dz)
{
    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();


    Point point;
    Normal normal;

    for(uint j = 0; j < subdiv_v; j++)
    {
        Scalar v = Scalar(j) / (subdiv_v-1);
        for(uint i = 0; i < subdiv_u; i++)
        {
            Scalar u = Scalar(i) / (subdiv_u-1);

            point = p0 + dx * u + dy * v;
            normal = dz;
            AbstractTriMesh::VertexHandle vert = mesh->add_vertex(point);
            mesh->set_normal(vert, dz);
        }
    }

    for(uint j = 0; j < subdiv_v - 1; j++)
    {
        for(uint i = 0; i < subdiv_u - 1; i++)
        {
            mesh->add_quad_face(i   +  j    * subdiv_u,
                                i+1 +  j    * subdiv_u,
                                i+1 + (j+1) * subdiv_u,
                                i   + (j+1) * subdiv_u);
        }
    }

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateFrame(const core::Frame& f)
{
    return CreateFrame(f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateFrame(const Point& p0,
                                                                        const Vector& dx_,
                                                                        const Vector& dy_,
                                                                        const Vector& dz_)
{
    std::shared_ptr<AbstractTriMesh > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();

    double cylinder_radius = 0.1;
    uint cylinder_subd = 10;
    double sphere_radius = 0.2;

    double lx = dx_.norm(), ly = dy_.norm(), lz = dz_.norm();
    Vector dx = dx_.normalized(), dy = dy_.normalized(), dz = dz_.normalized();

    std::shared_ptr<AbstractTriMesh > to_add;



    to_add = CreateCylinder(cylinder_subd, 1.0, p0, -dz * cylinder_radius, dy * cylinder_radius, dx * lx);
    to_add->FillColor(Color::Red());
    mesh->AddMesh(*to_add);

    to_add = CreateCone(cylinder_subd, p0 + dx * 0.9 * lx, -dz * sphere_radius, dy * sphere_radius, dx * sphere_radius * 2);
    to_add->FillColor(Color::Red());
    mesh->AddMesh(*to_add);

    to_add = CreateDisk(cylinder_subd, p0 + dx * 0.9 * lx, -dz * sphere_radius, dy * sphere_radius, dx * sphere_radius * 2);
    to_add->FillColor(Color::Red());
    mesh->AddMesh(*to_add);



    to_add = CreateCylinder(cylinder_subd, 1.0, p0, -dx * cylinder_radius, dz * cylinder_radius, dy * ly);
    to_add->FillColor(Color::Green());
    mesh->AddMesh(*to_add);

    to_add = CreateCone(cylinder_subd, p0 + dy * 0.9 * ly, -dx * sphere_radius, dz * sphere_radius, dy * sphere_radius * 2);
    to_add->FillColor(Color::Green());
    mesh->AddMesh(*to_add);

    to_add = CreateDisk(cylinder_subd, p0 + dy * 0.9 * ly, -dx * sphere_radius, dz * sphere_radius, dy * sphere_radius * 2);
    to_add->FillColor(Color::Green());
    mesh->AddMesh(*to_add);



    to_add = CreateCylinder(cylinder_subd, 1.0, p0, dx * cylinder_radius, dy * cylinder_radius, dz * lz);
    to_add->FillColor(Color::Blue());
    mesh->AddMesh(*to_add);

    to_add = CreateCone(cylinder_subd, p0 + dz * 0.9 * lz, dx * sphere_radius, dy * sphere_radius, dz * sphere_radius * 2);
    to_add->FillColor(Color::Blue());
    mesh->AddMesh(*to_add);

    to_add = CreateDisk(cylinder_subd, p0 + dz * 0.9 * lz, dx * sphere_radius, dy * sphere_radius, dz * sphere_radius * 2);
    to_add->FillColor(Color::Blue());
    mesh->AddMesh(*to_add);



    to_add = CreateSphere(cylinder_subd * 2, cylinder_subd, p0, dx * sphere_radius, dy * sphere_radius, dz * sphere_radius);
    //    to_add->ColorFromNormal();
    to_add->FillColor(Color::Gray());
    mesh->AddMesh(*to_add);



    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreatePatch(const core::Frame& f)
{
    return CreatePatch(f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreatePatch(const Point & p0,
                                                                        const Vector& dx_,
                                                                        const Vector& dy_,
                                                                        const Vector& dz_)
{
    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();

    Normal n = dz_.normalized();

    mesh->add_vertex(VERTEX(p0                           , n, Color::Gray( 0.0 / 11))); // vertex 0
    mesh->add_vertex(VERTEX(p0 + dx_ * 0.5               , n, Color::Gray( 1.0 / 11))); // vertex 1
    mesh->add_vertex(VERTEX(p0 + dx_                     , n, Color::Gray( 2.0 / 11))); // vertex 2
    mesh->add_vertex(VERTEX(p0 + dy_ * 0.5               , n, Color::Gray( 3.0 / 11))); // vertex 3
    mesh->add_vertex(VERTEX(p0 + dy_ * 0.5 + dx_         , n, Color::Gray( 4.0 / 11))); // vertex 4
    mesh->add_vertex(VERTEX(p0 + dy_                     , n, Color::Gray( 5.0 / 11))); // vertex 5
    mesh->add_vertex(VERTEX(p0 + dy_ + dx_ * 0.5         , n, Color::Gray( 6.0 / 11))); // vertex 6
    mesh->add_vertex(VERTEX(p0 + dy_ + dx_               , n, Color::Gray( 7.0 / 11))); // vertex 7
    mesh->add_vertex(VERTEX(p0 + dx_ * 0.33 + dy_ * 0.33 , n, Color::Gray( 8.0 / 11))); // vertex 8
    mesh->add_vertex(VERTEX(p0 + dx_ * 0.66 + dy_ * 0.33 , n, Color::Gray( 9.0 / 11))); // vertex 9
    mesh->add_vertex(VERTEX(p0 + dx_ * 0.33 + dy_ * 0.66 , n, Color::Gray(10.0 / 11))); // vertex 10
    mesh->add_vertex(VERTEX(p0 + dx_ * 0.66 + dy_ * 0.66 , n, Color::Gray(11.0 / 11))); // vertex 11

    mesh->add_face(0, 1, 8);
    mesh->add_face(1, 9, 8);
    mesh->add_face(1, 2, 9);
    mesh->add_face(2, 4, 9);
    mesh->add_face(0, 8, 3);
    mesh->add_face(8, 10, 3);
    mesh->add_face(8, 9, 10);
    mesh->add_face(9, 11, 10);
    mesh->add_face(9, 4, 11);
    mesh->add_face(4, 7, 11);
    mesh->add_face(3, 10, 5);
    mesh->add_face(10, 6, 5);
    mesh->add_face(10, 11, 6);
    mesh->add_face(11, 7, 6);

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateLattice(
        const core::Frame& f,
        const uint Nx,
        const uint Ny)
{
    return CreateLattice(Nx, Ny, f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreateLattice(const uint Nx,
                                                                          const uint Ny,
                                                                          const Point & p0,
                                                                          const Vector& dx_,
                                                                          const Vector& dy_,
                                                                          const Vector& dz_)
{
    std::shared_ptr<BasicTriMeshT<VERTEX> > mesh = std::make_shared<BasicTriMeshT<VERTEX> >();

    Normal n = dz_.normalized();

    for(uint i = 0; i < Nx; i++)
    {
        for(uint j = 0; j < Ny; j++)
        {
            mesh->add_vertex(VERTEX(p0 + i * dx_ + j * dy_, n, Color(float(i)/Nx, float(j)/Ny, 0.5f)));
        }
    }

    for(uint i = 0; i < Nx-1; i++)
    {
        for(uint j = 0; j < Ny-1; j++)
        {
            mesh->add_quad_face(i*Ny+j, (i+1)*Ny+j, (i+1)*Ny+j+1, i*Ny+j+1);
            //            mesh->add_face(i*Ny+j, i*Ny+j+1, (i+1)*Ny+j);
        }
    }

    return mesh;
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreatePrimitive(const PrimitiveType& type, const core::Frame& f)
{
    return CreatePrimitive(type, f.p0(), f.px(), f.py(), f.pz());
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::CreatePrimitive(const typename MeshToolsT<VERTEX>::PrimitiveType& type, const Point& p0, const Vector& dx, const Vector& dy, const Vector& dz)
{
    switch(type)
    {
    case CubeType:
        return CreateCube(p0, dx, dy, dz);
    case CylinderType:
        return CreateCylinder(20, 1.0, p0, dx, dy, dz);
        break;
    case ConeType:
        return CreateCone(20, p0, dx, dy, dz);
        break;
    case DiskType:
        return CreateDisk(20, p0, dx, dy, dz);
        break;
    case SphereType:
        return CreateSphere(20, 10, p0, dx, dy, dz);
        break;
    case FrameType:
        return CreateFrame(p0, dx, dy, dz);
        break;
    default:
        return Empty();
        break;
    }
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh > MeshToolsT<VERTEX>::Empty()
{
    std::shared_ptr<BasicTriMeshT<VERTEX> > output = std::make_shared<BasicTriMeshT<VERTEX> >();
    return output;
}

}

}

#endif // MESHTOOLS_HPP
