#include <core/geometry/BasicTriMeshDefines.h>
#include "core/geometry/AbstractTriMesh.h"

#include "core/geometry/BasicRay.h"

#include <fstream>

using namespace ork;
using namespace std;

namespace expressive {

namespace core {

namespace basictrimesh
{

#ifdef CONVOL_WIN_PLATFORM
Face::Face(uint id1, uint id2, uint id3) {
    idx_[0] = id1;
    idx_[1] = id2;
    idx_[2] = id3;
}
#else
Face::Face(uint id1, uint id2, uint id3) : idx_{id1,id2,id3} {}
#endif

Face::Face(){}

Face::~Face(){}


bool Face::contains(const uint i) const {
    if(idx_[0] == i) return true;
    if(idx_[1] == i) return true;
    if(idx_[2] == i) return true;
    return false;
}

#ifdef CONVOL_WIN_PLATFORM
Edge::Edge(uint id1, uint id2) {
    idx_[0] = id1;
    idx_[1] = id2;
}
#else
Edge::Edge(uint id1, uint id2) : idx_{id1,id2} {}
#endif

Edge::Edge(){}
Edge::~Edge(){}

bool Edge::contains(const uint i) const {
    if(idx_[0] == i) return true;
    if(idx_[1] == i) return true;
    return false;
}

Face operator+(const Face& f, const uint stride)
{
    return Face(f.idx_[0] + stride, f.idx_[1] + stride, f.idx_[2] + stride);
}

Edge operator+(const Edge& e, const uint stride)
{
    return Edge(e.idx_[0] + stride, e.idx_[1] + stride);
}

uint VertexHandle::idx() const
{
    return idx_;
}

VertexHandle::VertexHandle(uint idx) :
    idx_(idx)
{}

VertexHandle::~VertexHandle()
{}

bool VertexHandle::is_valid() const
{
    return idx_ != (uint)-1;
}

void VertexHandle::invalidate()
{
    idx_ = -1;
}

bool VertexHandle::operator ==(const VertexHandle &rhs)const
{
    return idx_ == rhs.idx_;
}

bool VertexHandle::operator !=(const VertexHandle &rhs)const
{
    return !(idx_ == rhs.idx_);
}

bool VertexHandle::operator <(const VertexHandle &fhs)const
{
    return idx_ < fhs.idx_;
}

bool VertexHandle::operator >(const VertexHandle &fhs)const
{
    return idx_ > fhs.idx_;
}

void VertexHandle::__increment()
{
    ++idx_;
}

void VertexHandle::__decrement()
{
    --idx_;
}

// FaceHandle
uint FaceHandle::idx() const
{ return idx_; }

FaceHandle::FaceHandle(uint idx) :
    idx_(idx)
{}

FaceHandle::~FaceHandle()
{}

bool FaceHandle::is_valid() const
{ return idx_ != (uint)-1;}

bool FaceHandle::operator ==(const FaceHandle &fhs)const
{
    return idx_ == fhs.idx_;
}

bool FaceHandle::operator <(const FaceHandle &fhs)const
{
    return idx_ < fhs.idx_;
}

bool FaceHandle::operator >(const FaceHandle &fhs)const
{
    return idx_ > fhs.idx_;
}

// this is to be used only by the iterators
void FaceHandle::__increment()
{ ++idx_; }

void FaceHandle::__decrement()
{ --idx_; }

int EdgeHandle::idx() const
{
    return idx_;
}

EdgeHandle::EdgeHandle(int idx) :
    idx_(idx)
{}

EdgeHandle::~EdgeHandle()
{}

void EdgeHandle::invalidate()
{
    idx_ = -1;
}

bool EdgeHandle::is_valid() const
{
    return idx_ != -1;
}

bool EdgeHandle::operator ==(const EdgeHandle &fhs)const
{
    return idx_ == fhs.idx_;
}

bool EdgeHandle::operator <(const EdgeHandle &fhs)const
{
    return idx_ < fhs.idx_;
}
bool EdgeHandle::operator >(const EdgeHandle &fhs)const
{
    return idx_ > fhs.idx_;
}

// this is to be used only by the iterators
void EdgeHandle::__increment() { ++idx_; }
void EdgeHandle::__decrement() { --idx_; }

// VertexSTatus
VertexStatus::VertexStatus() : nb_face_for_vertex_(0) {}

VertexStatus::VertexStatus(const VertexStatus &vs) :
    nb_face_for_vertex_(vs.nb_face_for_vertex_),
    adjacent_faces_(vs.adjacent_faces_),
    adjacent_edges_(vs.adjacent_edges_){}

VertexStatus::~VertexStatus(){}

void VertexStatus::add_a_face(unsigned int index)
{
    adjacent_faces_.push_back(index);
    nb_face_for_vertex_++;
}

void VertexStatus::remove_a_face(unsigned int index, bool destroy_if_lonely)
{
    auto it = std::find(adjacent_faces_.begin(),adjacent_faces_.end(), index);
    if(it != adjacent_faces_.end())
    { // is it possible that the face do not exist ?
        adjacent_faces_.erase(it);

        --nb_face_for_vertex_;
        if(destroy_if_lonely && nb_face_for_vertex_ == 0)
        {
            nb_face_for_vertex_ = -1;
        }
    }
}

void VertexStatus::add_an_edge(int index)
{
    auto it = std::find(adjacent_edges_.begin(),adjacent_edges_.end(), index);
    if(it == adjacent_edges_.end())
    {
        adjacent_edges_.push_back(index);
    }
}

void VertexStatus::remove_an_edge(int index)
{
    auto it = std::find(adjacent_edges_.begin(),adjacent_edges_.end(), index);
    if(it != adjacent_edges_.end())
    {
        adjacent_edges_.erase(it);
    }
}

void VertexStatus::destroy()
{
    nb_face_for_vertex_ = -1;
    adjacent_faces_.clear();
}

bool VertexStatus::deleted() const
{
    return nb_face_for_vertex_==-1;
}

unsigned int VertexStatus::bits() const
{
    return (deleted() ? OpenMesh::Attributes::DELETED : 0);
}

bool VertexStatus::contains_face(const uint i) const {
    for(unsigned int f : adjacent_faces_)
        if(f == i)
            return true;
    return false;
}

bool VertexStatus::contains_edge(const uint i) const {
    for(unsigned int e : adjacent_edges_)
        if(e == i)
            return true;
    return false;
}

FaceStatus::FaceStatus() :
    is_destroyed_(false)
{}

FaceStatus::~FaceStatus()
{}

void FaceStatus::destroy()
{
    is_destroyed_ = true;
}

bool FaceStatus::deleted() const
{
    return is_destroyed_;
}

unsigned int FaceStatus::bits() const
{
    return (deleted() ? OpenMesh::Attributes::DELETED : 0);
}

void FaceStatus::add_an_edge(int index)
{
    auto it = std::find(adjacent_edges_.begin(),adjacent_edges_.end(), index);
    if(it == adjacent_edges_.end())
    {
        adjacent_edges_.push_back(index);
    }
}

void FaceStatus::remove_an_edge(int index)
{
    auto it = std::find(adjacent_edges_.begin(),adjacent_edges_.end(), index);
    if(it != adjacent_edges_.end())
    {
        adjacent_edges_.erase(it);
    }
}

bool FaceStatus::contains_edge(const uint i) const {
    for(unsigned int e : adjacent_edges_)
        if(e == i)
            return true;
    return false;
}

EdgeStatus::EdgeStatus() :
    is_destroyed_(false)
{}

EdgeStatus::~EdgeStatus()
{}

void EdgeStatus::destroy()
{
    is_destroyed_ = true;
}

bool EdgeStatus::deleted() const
{
    return is_destroyed_;
}

unsigned int EdgeStatus::bits() const
{
    return (deleted() ? OpenMesh::Attributes::DELETED : 0);
}


void EdgeStatus::add_a_face(int index)
{

    auto it = std::find(adjacent_faces_.begin(),adjacent_faces_.end(), index);
    if(it == adjacent_faces_.end())
    {
        adjacent_faces_.push_back(index);
    }
}

void EdgeStatus::remove_a_face(int index, bool destroy_if_lonely)
{
    auto it = std::find(adjacent_faces_.begin(),adjacent_faces_.end(), index);
    if(it != adjacent_faces_.end())
    {
        adjacent_faces_.erase(it);
    }

    if(destroy_if_lonely && adjacent_faces_.size() == 0)
    {
        destroy();
    }
}

bool EdgeStatus::contains_face(const uint i) const {
    for(unsigned int f : adjacent_faces_)
        if(f == i)
            return true;
    return false;
}

HalfedgeHandle::HalfedgeHandle(){}
HalfedgeHandle::~HalfedgeHandle(){}

} // namespace basictrimesh

///////////////////////////
/////// DefaultMeshVertex

DefaultMeshVertex::DefaultMeshVertex() :
    Vector(0.0, 0.0, 0.0),
    m_normal(0.0, 0.0, 0.0),
    m_uv(0.0, 0.0),
    m_color(1.0, 1.0, 1.0, 1.0)
{
}

DefaultMeshVertex::DefaultMeshVertex(const Point &pos, const Color &color) :
    Vector(pos),
    m_normal(0.0, 0.0, 0.0),
    m_uv(0.0, 0.0),
    m_color(color)
{
}

DefaultMeshVertex::DefaultMeshVertex(const Point &pos, const Vector &normal) :
    Vector(pos),
    m_normal(normal),
    m_uv(0.0, 0.0),
    m_color(1.0, 1.0, 1.0, 1.0)
{
}

DefaultMeshVertex::DefaultMeshVertex(const Point &pos, const Vector &normal, const Color &color) :
    Vector(pos),
    m_normal(normal),
    m_uv(0.0, 0.0),
    m_color(color)
{
}

DefaultMeshVertex::DefaultMeshVertex(const Point &pos, const Vector &normal, const Point2 &uv, const Color &color) :
    Vector(pos),
    m_normal(normal),
    m_uv(uv),
    m_color(color)
{
}

DefaultMeshVertex::DefaultMeshVertex(Scalar x, Scalar y, Scalar z, Scalar nx, Scalar ny, Scalar nz, Scalar u, Scalar v, unsigned char r, unsigned char g, unsigned char b, unsigned char a) :
    Vector(x, y, z),
    m_normal(nx, ny, nz),
    m_uv(u, v),
    m_color(r, g, b, a)
{
}

DefaultMeshVertex::DefaultMeshVertex(Scalar x, Scalar y, Scalar z, Scalar nx, Scalar ny, Scalar nz, unsigned char r, unsigned char g, unsigned char b, unsigned char a) :
    Vector(x, y, z),
    m_normal(nx, ny, nz),
    m_uv(0.0, 0.0),
    m_color(r, g, b, a)
{
}

DefaultMeshVertex::DefaultMeshVertex(Scalar x, Scalar y, Scalar z, unsigned char r, unsigned char g, unsigned char b, unsigned char a) :
    Vector(x, y, z),
    m_normal(0.0, 0.0 ,0.0),
    m_uv(0.0, 0.0),
    m_color(r, g, b, a)
{
}

DefaultMeshVertex::DefaultMeshVertex(const DefaultMeshVertex &v) :
    Vector(v),
    m_normal(v.m_normal),
    m_uv(v.m_uv),
    m_color(v.m_color)
{
}

DefaultMeshVertex::DefaultMeshVertex(const Vector &v) :
    Vector(v),
    m_normal(0.0, 0.0, 0.0),
    m_uv(0.0, 0.0),
    m_color(Color())
{
}

DefaultMeshVertex& DefaultMeshVertex::set_color(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
    m_color = Color(r, g, b, a);

    return *this;
}

DefaultMeshVertex& DefaultMeshVertex::set_color(unsigned char r, unsigned char g, unsigned char b)
{
    m_color = Color(r, g, b, (unsigned char)(m_color.A() * 255));

    return *this;
}

DefaultMeshVertex& DefaultMeshVertex::set_color(const Color &c)
{
    m_color = c;

    return *this;
}

DefaultMeshVertex& DefaultMeshVertex::set_UV(Scalar u, Scalar v)
{
    m_uv = Point2(u,v);

    return *this;
}

DefaultMeshVertex& DefaultMeshVertex::set_UV(const Point2 &uv)
{
    m_uv = uv;

    return *this;
}

DefaultMeshVertex& DefaultMeshVertex::set_normal(Scalar nx, Scalar ny, Scalar nz)
{
    m_normal = Normal(nx, ny, nz);

    return *this;
}

DefaultMeshVertex& DefaultMeshVertex::set_normal(const Normal &n)
{
    m_normal = n;

    return *this;
}

DefaultMeshVertex& DefaultMeshVertex::set_pos(const Point &p)
{
    (*this)[0] = p[0];
    (*this)[1] = p[1];
    (*this)[2] = p[2];

    return *this;
}

DefaultMeshVertex& DefaultMeshVertex::set_pos(Scalar x, Scalar y, Scalar z)
{
    (*this)[0] = x;
    (*this)[1] = y;
    (*this)[2] = z;

    return *this;
}


const Vector& DefaultMeshVertex::normal() const
{
    return m_normal;
}

const Point2& DefaultMeshVertex::uv    () const
{
    return m_uv;
}

const Color&  DefaultMeshVertex::color () const
{
    return m_color;
}

Vector& DefaultMeshVertex::normal()
{
    return m_normal;
}

Point2& DefaultMeshVertex::uv    ()
{
    return m_uv;
}

Color&  DefaultMeshVertex::color ()
{
    return m_color;
}



bool DefaultMeshVertex::has_vertex_colors()
{
    return true;
}

bool DefaultMeshVertex::has_vertex_normals()
{
    return true;
}

///////////////////////////
/////// BasicTriMeshCell
BasicTriMeshCell::BasicTriMeshCell(AbstractTriMesh *mesh, unsigned int face_idx)
{
    AbstractTriMesh::ConstFaceVertexIter fv_it = mesh->cfv_begin(face_idx);

    verts[0] = mesh->point(*fv_it); ++fv_it;
    verts[1] = mesh->point(*fv_it); ++fv_it;
    verts[2] = mesh->point(*fv_it);

    bounding_box_.Enlarge(verts[0]);
    bounding_box_.Enlarge(verts[1]);
    bounding_box_.Enlarge(verts[2]);
}

Scalar BasicTriMeshCell::FindIntersection(const BasicRay &ray, Scalar dist_max)
{
    Scalar dist = dist_max;

    // trans_ray direction should be normalized
    Vector u = verts[1] - verts[0];
    Vector v = verts[2] - verts[0];
    Scalar delta = -(u.cross(v)).dot(ray.direction());
    if( delta != 0.0  )
    {
        Vector w = ray.starting_point() - verts[0];
        Scalar a = - (w.cross(v)).dot(ray.direction()) / delta;
        Scalar b = - (u.cross(w)).dot(ray.direction()) / delta;
        Scalar t = (u.cross(v)).dot(w) / delta;
        if(t>0.0 && a >=0.0 && b>=0.0 && a+b<=1
           && t<dist)
        {
            dist = t;
        }
    }
    return dist;
}

AABBox BasicTriMeshCell::bounding_box() const
{
    return bounding_box_;
}

} // end of namespace core

} // end of namespace expressive

