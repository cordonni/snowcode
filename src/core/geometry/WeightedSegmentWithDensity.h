/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: WeightedSegmentWithDensityT.h

   Language: C++

   License: Expressive license

   \author: Zanni Cedric
   E-Mail:  cedric.zanni@inria.fr

   Description: Header for polynomially weighted segment (for now, only linear)
                with a density (additional linear parameter defined on the skeleton).

   Platform Dependencies: None
*/

#pragma once
#ifndef WEIGHTED_SEGMENT_WITH_DENSITY_H_
#define WEIGHTED_SEGMENT_WITH_DENSITY_H_

// core dependencies
#include<core/CoreRequired.h>
    // geometry
    #include<core/geometry/WeightedSegment.h>

namespace expressive {

namespace core {

class CORE_API WeightedSegmentWithDensity : public WeightedSegment
{
public:
    EXPRESSIVE_MACRO_NAME("WeightedSegmentWithDensity")

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////


    /*! \brief Build a constant 1.0-weighted segment along the first axis.
     */
    WeightedSegmentWithDensity() :
        WeightedSegment(),
        density_p1_(1.0),
        density_p2_(1.0)
    {
        ComputeDensityHelpVariables();
    }

    /*! \brief Build a linear weighted segment (if last params are ignored, it will be a 1.0-weighted segment)
     *
     *   \param p1 First segment point
     *   \param p2 Second segment point
     *   \param weight1 Weight for first segment point
     *   \param weight2 Weight for second segment point
     *   \param density1 Density for first segment point
     *   \param density2 Density for second segment point
     */
    WeightedSegmentWithDensity(const Point& p1, const Point& p2,
                               Scalar weight1 = 1.0, Scalar weight2 = 1.0,
                               Scalar density1 = 1.0, Scalar density2 = 1.0) :
        WeightedSegment(p1, p2, weight1, weight2), density_p1_(density1), density_p2_(density2)
    {
        ComputeDensityHelpVariables();
    }

    WeightedSegmentWithDensity(const WeightedSegmentWithDensity &rhs, unsigned int start = -1, unsigned int end = -1) :
        WeightedSegment(rhs, start, end), density_p1_(rhs.density_p1_), density_p2_(rhs.density_p2_),
          density_special_coeff_(rhs.density_special_coeff_)
    {}

    ~WeightedSegmentWithDensity(){}

    virtual std::shared_ptr<GeometryPrimitiveT<WeightedPoint> > clone(unsigned int start = - 1, unsigned int end = -1) const {
        return std::make_shared<WeightedSegmentWithDensity>(*this, start, end);
    } // Todo : yew.. this is ugly.

    ///////////////
    // Modifiers //
    ///////////////

    void set_densities(Scalar d1, Scalar d2);
    void set_density_p1(Scalar d1);
    void set_density_p2(Scalar d2);

    ///////////////
    // Accessors //
    ///////////////

    // Convenience accessors to get extreme densities value
    Scalar density_p1()    const { return density_p1_; }
    Scalar density_p2()    const { return density_p2_; }
    //
    const std::array<Scalar,2>& density_special_coeff()  const { return density_special_coeff_; }
    Scalar density_special_coeff(int i) const { return density_special_coeff_[i]; }

    ///////////////////////////////
    // Homothetic space function //
    ///////////////////////////////

//    /** \brief Compute the "normalized" length of segment inside a clipping sphere in the homothetic space,
//     *         the length is modified by the density parameter of the segment
//     *
//     *  \param clipping_sphere ...
//     *
//     *  \return the normalized length of skeleton inside the sphere
//     */
//    Scalar HomotheticClippedLengthWithSharpness(const Sphere &clipping_sphere) const;

//    Scalar IntervalHomotheticLengthWithSharpness(Scalar l1, Scalar l2) const;


    ///////////////
    // Attributs //
    ///////////////

private:

    Scalar density_p1_;
    Scalar density_p2_;

    // Convenience attributes
    std::array<Scalar,2> density_special_coeff_;
    // polynomial coeff of the density with segment parametrized

    inline void ComputeDensityHelpVariables()
    {
        if(this->weight_coeff_[1]<0.0)
        {
            density_special_coeff_[0] = density_p2_;
            density_special_coeff_[1] = (density_p1_ - density_p2_) * this->points_[1].weight() / this->length_;
        }
        else
        {
            density_special_coeff_[0] = density_p1_;
            density_special_coeff_[1] = (density_p2_ - density_p1_) * this->points_[0].weight() / this->length_;
        }
    }

    virtual void ComputeHelpVariables()
    {
        WeightedSegment::ComputeHelpVariables();

        ComputeDensityHelpVariables();
    }

};

/*
// TODO : check that it is the exact same implementation has HomotheticClipping in  PolyWeightedSegment
bool ComputeClipping(const Sphere &clipping_sphere, Scalar &clipped_l1, Scalar &clipped_l2) const
{
    const Scalar radius_sqr = clipping_sphere.radius_ * clipping_sphere.radius_;
    const Vector p1_to_center = clipping_sphere.center_ - p1_;
    Scalar special_coeff[3] = {radius_sqr*weight_coeff_[0]*weight_coeff_[0]   - p1_to_center.sqrnorm() ,
                               - radius_sqr*weight_coeff_[1]*weight_coeff_[0] - (dir_ | p1_to_center) ,
                               radius_sqr*weight_coeff_[1]*weight_coeff_[1]   - sqrlength_ };

    return ComputeClipping(special_coeff, clipped_l1, clipped_l2);
}
// Same as function above but avoid some computation in common with the eval itself by using a common special clipping
inline bool ComputeClipping(const Scalar w[3], Scalar &clipped_l1, Scalar &clipped_l2) const;
*/
/*
template< typename TTraits >
bool HomotheticWeightedSegmentT<TTraits>::ComputeClipping(const Scalar w[3], Scalar &clipped_l1, Scalar &clipped_l2) const
{
    // we search solution t \in [0,1] such that at^2-2bt+c<=0
    const Scalar a = -w[2];
    const Scalar b = -w[1];
    const Scalar c = -w[0];

    Scalar delta = b*b - a*c;
    if(delta>=0.0)
    {
        if(a>0)
        {
            const Scalar main_root = c / (b+sqrt(delta));    // WARNING : possible instablity here ... when a -> 0 && b -> 0 (and of course a=0 and b=0)
            if(main_root < 1.0)
            {
                const Scalar a_r = a*main_root;
                if(c*a_r>0.0)
                { // it is the same as testing the sign of the second root minus numerical instability
                    if(main_root<0.0)
                    {
                        clipped_l1 = 0.0;
                        clipped_l2 = (c<a_r) ? 1.0 : c/a_r; // prevent instability ...
                        return true;
                    }
                    else
                    {
                        clipped_l1 = main_root;
                        clipped_l2 = (c>a_r) ? 1.0 : c/a_r; // prevent instability ...
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        { // if(a<=0)
            if(this->weight_coeff_[1]>0.0)
            {
                const Scalar main_root = c / (b+sqrt(delta));    // WARNING : possible instablity here ... when a -> 0 && b -> 0 (and of course a=0 and b=0)
                if(main_root < 1.0)
                {
                    clipped_l1 = (main_root<0.0) ? 0.0 : main_root;
                    clipped_l2 = 1.0;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                const Scalar main_root = c / (b-sqrt(delta));    // WARNING : possible instablity here ... when a -> 0 && b -> 0 (and of course a=0 and b=0)
                if(main_root > 0.0)
                {
                    clipped_l1 = 0.0;
                    clipped_l2 = (main_root>1.0) ? 1.0 : main_root;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
    else
    {
        // there is nothing to be clipped
        return false;
    }
}
*/


} // Close namespace core

} // Close namespace expressive

#endif // WEIGHTED_SEGMENT_WITH_DENSITY_H_
