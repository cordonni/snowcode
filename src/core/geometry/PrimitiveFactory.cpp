#include <core/geometry/PrimitiveFactory.h>

// core dependencies
#include <ork/core/Logger.h>
#include <core/functor/FunctorFactory.h>

using namespace ork;

namespace expressive {

namespace core {

PrimitiveFactory& PrimitiveFactory::GetInstance()
{
    static PrimitiveFactory INSTANCE = PrimitiveFactory();
    return INSTANCE;
}

void PrimitiveFactory::SetDefaultPrimitive(std::shared_ptr<Primitive> geom)
{
    default_geom_ = geom;
}


auto PrimitiveFactory::CreateNewPrimitive()
-> std::shared_ptr<Primitive>
{
    // TODO this seems to badly initialize stuff when actually adding points afterwards
    std::shared_ptr<Functor> f = FunctorFactory::GetInstance().CreateDefaultFunctor();
    if (f != nullptr) {
        return CreateNewPrimitive(*f);
    }
    Logger::ERROR_LOGGER->log("CORE", "No Default Functor registered in FunctorFactory.");
    return nullptr;
}

auto PrimitiveFactory::CreateNewPrimitive(const core::Functor &ker)
-> std::shared_ptr<Primitive>
{
    if (default_geom_ != nullptr) {
        return CreateNewPrimitive(default_geom_, ker);
    }
    Logger::ERROR_LOGGER->log("CORE", "No Default Primitive registered in PrimitiveFactory.");
    return nullptr;
}

auto PrimitiveFactory::CreateNewPrimitive(const std::shared_ptr<Primitive> geom)
-> std::shared_ptr<Primitive>
{
    std::shared_ptr<Functor> f = FunctorFactory::GetInstance().CreateDefaultFunctor();
    if (f != nullptr) {
        return CreateNewPrimitive(geom, *f);
    }
    Logger::ERROR_LOGGER->log("CORE", "No Default Functor registered in FunctorFactory.");
    return nullptr;
}

auto PrimitiveFactory::CreateNewPrimitive(const std::shared_ptr<Primitive> geom,
                                                  const core::Functor &ker)
-> std::shared_ptr<Primitive>
{
    auto it = types.find(geom->Name());
    if(it != types.end())
    {
        BuilderFunction builder = nullptr;
        const core::FunctorTypeTree ker_type = ker.Type();
        
        if(it->second.find(ker_type, builder))
        {
            return builder(geom,ker);
        }
        else
        {
            Logger::ERROR_LOGGER->logf("GEOMETRY", "PrimitiveFactory : Kernel %s is not registered for the primitive %s", ker.Name().c_str(), geom->Name().c_str());
            throw std::exception();
        }
    }
    else
    {
        Logger::ERROR_LOGGER->logf("GEOMETRY", "PrimitiveFactory : Primitive %s is not registered", geom->Name().c_str());
        throw std::exception();
    }
//    return nullptr; // unreachable
}
            

} // Close namespace core

} // Close namespace expressive

