#include <core/geometry/CsteWeightedSegmentWithDensity.h>

#include <math.h>

namespace expressive {

namespace core {

CsteWeightedSegmentWithDensity::CsteWeightedSegmentWithDensity()
{
}

CsteWeightedSegmentWithDensity::CsteWeightedSegmentWithDensity(const WeightedPoint &w_p1, const Vector& unit_dir, Scalar length, Scalar density)
{
    p1_ = w_p1;
    weight_ = w_p1.weight();

    unit_dir_ = unit_dir;
    unit_dir_.normalize(); // for security
    length_ = length;
    density_ = density;
}

// Copy constructor
CsteWeightedSegmentWithDensity::CsteWeightedSegmentWithDensity(const CsteWeightedSegmentWithDensity &rhs)
    :   p1_(rhs.p1_), unit_dir_(rhs.unit_dir_), length_(rhs.length_),
        weight_(rhs.weight_), density_(rhs.density_)
{}

CsteWeightedSegmentWithDensity::~CsteWeightedSegmentWithDensity(){}


Point CsteWeightedSegmentWithDensity::Barycenter() const
{
    return p1_ + (length_*0.5)*unit_dir_ ;
}

AABBox CsteWeightedSegmentWithDensity::GetAxisBoundingBox() const
{
    //TODO:@todo : use Eigen correctly
    AABBox res;
    for(unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i)
    {
        if(unit_dir_[i]>0.0)
        {
            res.min_[i] = p1_[i];
            res.max_[i] = p1_[i] + length_*unit_dir_[i];
        }
        else
        {
            res.min_[i] = p1_[i] + length_*unit_dir_[i];
            res.max_[i] = p1_[i];
        }
    }
    return res;
}

} // Close namespace core

} // Close namespace expressive
