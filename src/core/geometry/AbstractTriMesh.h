/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef BASIC_TRI_MESH_H
#define BASIC_TRI_MESH_H

// core dependencies
#include "core/geometry/BasicTriMeshDefines.h"
#include "core/utils/Serializable.h"
#include <mutex>

namespace ork
{
    class MeshBuffers;
}

namespace expressive {

namespace core {

class Frame;
template <class C> class Octree;

class CORE_API AbstractTriMesh : public Serializable, public ListenableT<AbstractTriMesh>
{
public :
    EXPRESSIVE_MACRO_NAME("AbstractTriMesh")

    // Typedef
    typedef basictrimesh::VertexHandle VertexHandle;
    typedef basictrimesh::VertexStatus VertexStatus;

    typedef basictrimesh::Face         Face;
    typedef basictrimesh::FaceHandle   FaceHandle;
    typedef basictrimesh::FaceStatus   FaceStatus;

    typedef basictrimesh::Edge         Edge;
    typedef basictrimesh::EdgeStatus   EdgeStatus;
    typedef basictrimesh::EdgeHandle   EdgeHandle;

    typedef basictrimesh::HalfedgeHandle HalfedgeHandle;

    // Typedef: Iter
    typedef basictrimesh::VertexIter<AbstractTriMesh> VertexIter;
    typedef basictrimesh::VertexIter<AbstractTriMesh> ConstVertexIter;

    typedef basictrimesh::FaceIter<AbstractTriMesh> FaceIter;
    typedef basictrimesh::FaceIter<AbstractTriMesh> ConstFaceIter;

    typedef basictrimesh::EdgeIter<AbstractTriMesh> EdgeIter;
    typedef basictrimesh::EdgeIter<AbstractTriMesh> ConstEdgeIter;

    typedef basictrimesh::ConstFaceVertexIter<AbstractTriMesh> ConstFaceVertexIter;
    typedef basictrimesh::ConstVertexFaceIter<AbstractTriMesh> ConstVertexFaceIter;

public:
    // Constructor / Destructor
    AbstractTriMesh();

    AbstractTriMesh(const AbstractTriMesh &rhs, bool copy_content = true, bool shared_data = true);

    virtual ~AbstractTriMesh();

    void copyDataFrom(const AbstractTriMesh &mesh);

    virtual void copyDataFrom(const AbstractTriMesh *mesh) = 0;

    virtual std::shared_ptr<AbstractTriMesh> clone(bool copy_content = true) const = 0;

    // --- deletion ---
    void garbage_collection();

    // Same as above but ensure that the given set of handles are still
    // valid after the garbage collection
    void garbage_collection(std::vector<VertexHandle*>&   vh_to_update,
                            std::vector<HalfedgeHandle*>& hh_to_update,
                            std::vector<FaceHandle*>&     fh_to_update,
                            std::vector<EdgeHandle*>&     eh_to_update,
                            bool _v=true, bool _h=true, bool _f=true, bool _e=true);

    virtual void clear();

    // Getters
    size_t n_faces   () const;
    virtual size_t n_vertices() const = 0;
    size_t n_edges   () const;
    virtual size_t n_indices () const = 0;

    bool has_vertex_status () const;
    bool has_face_status   () const;
    bool has_edge_status   () const;
    virtual bool has_vertex_normals() const = 0;
    virtual bool has_vertex_colors () const = 0;

    // Getters: Status
    const VertexStatus& status(VertexHandle v_h) const;
    const FaceStatus&   status(FaceHandle   f_h) const;
    const EdgeStatus&   status(EdgeHandle   e_h) const;

    // Getters: Status
//    VertexStatus& non_const_status(VertexHandle v_h);
//    FaceStatus&   non_const_status(FaceHandle   f_h);
//    EdgeStatus&   non_const_status(EdgeHandle   e_h);
    VertexStatus& status(VertexHandle v_h);
    FaceStatus&   status(FaceHandle   f_h);
    EdgeStatus&   status(EdgeHandle   e_h);

    // Getters: Iterator for Vertices
    VertexIter vertices_begin();
    VertexIter vertices_end  ();

    ConstVertexIter vertices_begin() const;
    ConstVertexIter vertices_end  () const;

    // Getters: Iterator for Faces
    FaceIter faces_begin();
    FaceIter faces_end  ();

    ConstFaceIter faces_begin() const;
    ConstFaceIter faces_end  () const;

    // Getters: Iterator for Edges
    EdgeIter edges_begin();
    EdgeIter edges_end  ();

    ConstEdgeIter edges_begin() const;
    ConstEdgeIter edges_end  () const;

    // Getters: Iterator for Circulator
    ConstFaceVertexIter cfv_begin(FaceHandle   _fh) const;
    ConstVertexFaceIter cvf_begin(VertexHandle _vh) const;
    //TODO:@todo : should add the end version

    // Getters: Vertex Handle Data
    VertexHandle vertex_handle(uint index) const;
    //  VertexHandle vertex_handle(uint _i) const;

    virtual const Point&    point (VertexHandle vh) const = 0;
    virtual const Normal&   normal(VertexHandle vh) const = 0;
    virtual const Vector2&  uv    (VertexHandle vh) const = 0;
    virtual const Color&    color (VertexHandle vh) const = 0;

    virtual Point&    point (VertexHandle vh) = 0;
    virtual Normal&   normal(VertexHandle vh) = 0;
    virtual Vector2&  uv    (VertexHandle vh) = 0;
    virtual Color&    color (VertexHandle vh) = 0;

    virtual ork::ptr<ork::MeshBuffers> mesh_buffers() const = 0;
    virtual ork::MeshMode mesh_mode() const = 0;

    AABBox  bounding_box() const;

    // Getters: Face Handle Data
    FaceHandle face_handle(uint index) const;

    AbstractTriMesh::Face& face(FaceHandle fh);

    const AbstractTriMesh::Face& face(FaceHandle fh) const;

    AbstractTriMesh::Edge& edge(EdgeHandle eh);

    const AbstractTriMesh::Edge& edge(EdgeHandle eh) const;

    std::shared_ptr<Octree<BasicTriMeshCell>> octree() const;

    const std::set<VertexHandle> &garbage_vertices() const;

    const std::set<EdgeHandle> &garbage_edges() const;

    const std::set<FaceHandle> &garbage_faces() const;

    // Setters
    virtual VertexHandle add_point(const Point &p, const VertexStatus& status = VertexStatus()) = 0;
    virtual VertexHandle add_point(const Point &p, const Color& color, const VertexStatus& status = VertexStatus()) = 0;
    virtual VertexHandle add_point(const Point &p, const Vector &normal, const Color& color = Color(), const VertexStatus& status = VertexStatus()) = 0;
    virtual VertexHandle add_point(const Point &p, const Vector &normal, const Vector2 &uv, const Color& color, const VertexStatus& status = VertexStatus()) = 0;


//    void set_vertex(VertexHandle vh, const VERTEX& point );
    virtual void set_point (VertexHandle vh, const Point & point ) = 0;
    virtual void set_normal(VertexHandle vh, const Vector& normal) = 0;
    virtual void set_uv    (VertexHandle vh, const Vector2& uv   ) = 0;
    virtual void set_color (VertexHandle vh, const Color & color ) = 0;

    void set_status(VertexHandle vh, const VertexStatus& status);
    void set_status(FaceHandle fh,   const FaceStatus&  status );

    void update_bounding_box(); // < Warning, this one is time consuming, since it will recompute entirely every positions.
    void update_bounding_box(const AABBox &box);

    void delete_vertex(VertexHandle v_h, bool delete_isolated_vertices);

    virtual EdgeHandle add_line(VertexHandle v1, VertexHandle v2) = 0;

    // Setters: Face
    virtual FaceHandle add_face(VertexHandle v1, VertexHandle v2, VertexHandle v3) = 0;

    EdgeHandle get_face_edge(FaceHandle fh, VertexHandle v0, VertexHandle v1);

    std::pair<FaceHandle, FaceHandle> add_quad_face(VertexHandle v1, VertexHandle v2, VertexHandle v3, VertexHandle v4);

    FaceHandle add_face(const Face& f);

    EdgeHandle add_edge(VertexHandle v1, VertexHandle v2);

    EdgeHandle add_edge(const Edge& e);

    Scalar length(const EdgeHandle eh) const;

    void delete_face(FaceHandle f_h, bool delete_isolated_vertices, bool delete_isolated_edges = true);

    void delete_edge(EdgeHandle eh);

    // Operators
//    AbstractTriMesh &operator=(const AbstractTriMesh &rhs);

    // Locks or unlocks this tri_mesh : Makes sure that the mesh is not accessed when
    // it is being updated.
    void lock();
    void unlock();

    void lockBuffers();
    void unlockBuffers();
    bool tryLockBuffers();
    virtual std::mutex& get_buffers_mutex() const = 0;

    // return the Scalar such that ray.starting_point + dist*ray.direction() = intersection point.
    //         In the case there is no intersection, it returns Traits::kScalarMax
    // @todo optimize it
    Scalar FindIntersection(const BasicRay& ray, const Scalar dist_max) const;
    Scalar FindIntersection(const BasicRay& ray, const Scalar dist_max, FaceHandle &fh) const;

    // Save the mesh in a file with .obj format, return false if some problem...
    bool SaveToObjFile(const std::string &filename) const;

    // Loads the mesh from an .obj file. Returns false if there is any problem.
    // Careful:  groups of faces are not handled properly.
    bool LoadFromObjFile(const std::string &filename);

    /////////////////////////////////////////////////////////////////////////
    ////
    ////    Utility functions for computing local attirbutes on vertices and faces
    ////
    /////////////////////////////////////////////////////////////////////////

    //    /**
    //     * @brief GetFaceNormal Computes the normal of the face fh
    //     * @return normal of fh
    //     */
    //    Normal GetFaceNormal(const AbstractTriMesh::FaceHandle fh) const;

    /**
     * @brief GetFacesNormals Computes the normal of each face of the mesh
     * @return map associating each face to its normal
     */
    std::map<FaceHandle, Normal> GetFacesNormals() const;

    /**
     * @brief GetVerticesStars Computes the star (ie. 1-ring) of each vertex of the mesh
     * @return map associating each vertex to its star
     */
    std::map<VertexHandle, std::set<VertexHandle> > GetVerticesStars() const;

    /**
     * @brief GetVerticesFaces Computes the incident faces for each vertex of the mesh
     * @return map associating each vertex to its set of incident faces
     */
    std::map<VertexHandle, std::set<FaceHandle> > GetVerticesFaces() const;

    /**
     * @brief GetVerticesNormals Computes the normal of each vertex of the meshbased on the normal of its incident faces
     * @return map associating each vertex to its normal
     */
    std::map<VertexHandle, Normal> GetVerticesNormals() const;

    FaceHandle GetOppositeFace(FaceHandle fh, VertexHandle vh0, VertexHandle vh1) const;
    FaceHandle GetOppositeFace(FaceHandle fh, EdgeHandle eh) const;

    /**
     * @brief ComputeNormals Sets the normal of each vertex to its value as return by GetVerticesNormals
     */
    void ComputeNormals();


    /**
     * @brief GetFaceNormal Computes the normal of a face of the mesh
     * @param fh : FaceHandle to the target face
     * @return normal of the disignated face
     */
    Normal GetFaceNormal                (const FaceHandle   fh) const;

    /**
     * @brief GetFaceArea Computes the area of a face of the mesh
     * @param fh : FaceHandle to the target face
     * @return area of the disignated face
     */
    Scalar GetFaceArea(const AbstractTriMesh::FaceHandle fh) const;

    /**
     * @brief GetFaceCentroid Computes the centroid of a face of the mesh
     * @param fh : FaceHandle to the target face
     * @return centroid of the disignated face
     */
    Point GetFaceCentroid(const AbstractTriMesh::FaceHandle fh) const;

    /**
     * @brief GetVertexStarUnordered Computes the star (ie. 1-ring) of a vertex of the mesh
     * @param vh : VertexHandle to the target vertex
     * @return star of the disignated vertex
     */
    std::set<VertexHandle> GetVertexStarUnordered(const VertexHandle vh) const;

    /**
     * @brief GetVertexStar Computes the star (ie. 1-ring) of a vertex of the mesh
     * @param vh : VertexHandle to the target vertex
     * @return star of the disignated vertex
     */
    std::vector<VertexHandle> GetVertexStar(const VertexHandle vh) const;

    /**
     * @brief GetVertexNormal Computes the normal of a vertex of the mesh
     * @param vh : VertexHandle to the target vertex
     * @return normal of the disignated vertex
     */
    std::set<FaceHandle> GetFaceSurrounding(const FaceHandle fh) const;

    /**
     * @brief GetNextVertex Computes the vertex following the given vertex on the given face
     * @param vh : VertexHandle to the target vertex
     * @param fh : VertexHandle to the target face
     * @return successor vertex handle
     */
    AbstractTriMesh::VertexHandle GetNextVertex(VertexHandle vh, FaceHandle fh) const;
    AbstractTriMesh::VertexHandle GetPrevVertex(VertexHandle vh, FaceHandle fh) const;

    /**
     * @brief GetVertexFaces Computes the set of incident faces of a vertex of the mesh
     * @param vh : VertexHandle to the target vertex
     * @return set of incident faces of the disignated vertex
     */
    std::set<FaceHandle> GetVertexFaces (const VertexHandle vh) const;

    /**
     * @brief GetVertexFacesOrdered Computes the set of incident faces of a vertex of the mesh
     * @param vh : VertexHandle to the target vertex
     * @return vector of incident faces of the designated vertex
     */
    std::vector<FaceHandle> GetVertexFacesOrdered (const VertexHandle vh) const;

    /**
     * @brief GetVertexNormal Computes the normal of a vertex of the mesh
     * @param vh : VertexHandle to the target vertex
     * @return normal of the disignated vertex
     */
    Normal GetVertexNormal              (const VertexHandle vh) const;

    /**
     * @brief RecomputeVertexRingNormal Recomputes the normal of a vertex and its 1-ring
     * @param vh : VertexHandle to the target vertex
     */
    void RecomputeVertexRingNormal      (const VertexHandle vh);

    /**
     * @brief AddMesh adds a mesh to another (ie. concatenate them)
     * @param to_add is the mesh to be added
     * @return shared pointer of the created mesh
     */
    AbstractTriMesh &AddMesh(const AbstractTriMesh &to_add);

    std::shared_ptr<AbstractTriMesh> Subdivide() const;

    /////////////////////////////////////////////////////////////////////////
    ////
    ////        Color management functions
    ////
    /////////////////////////////////////////////////////////////////////////

    /**
     * @brief FillColor sets the same color for alls the vertices of the mesh
     * @param c color to be set
     */
    void FillColor(const Color& c);

    /**
     * @brief ColorFromNormal sets a color depending on the normal of each vertex
     */
    void ColorFromNormal(const Color& Xcolor = Color::Red(), const Color& Ycolor = Color::Green(), const Color& Zcolor = Color::Blue());

    /**
     * @brief ColorFromCoord sets a color depending on the position of each vertex (relatively to the BB)
     */
    void ColorFromCoord(const Color& Xcolor = Color::Red(), const Color& Ycolor = Color::Green(), const Color& Zcolor = Color::Blue());

    /////////////////////////////////////////////////////////////////////////
    ////
    ////        Misc
    ////
    /////////////////////////////////////////////////////////////////////////

    /**
     * @brief ComputeBB computes the axis-aligned bounding box of the mesh
     * @return the computed bounding box
     */
    AABBox ComputeBB() const;

    /**
     * Returns the center point of this mesh.
     */
    Vector Center() const;
    Vector Centre() const; // damn uk.
    double Radius() const;
    Color MeanColor() const;

    virtual void ForceUpdate() = 0;

    std::set<AbstractTriMesh::FaceHandle> CommonFaces(const VertexHandle v0, const VertexHandle v1) const;
    std::set<AbstractTriMesh::VertexHandle> CommonVertices(const FaceHandle v0, const FaceHandle v1) const;

    void EdgeCollapse(VertexHandle v0, VertexHandle v1);
    VertexHandle EdgeCollapse(EdgeHandle eh);
//    std::pair<std::set<AbstractTriMesh::FaceHandle>, std::set<AbstractTriMesh::FaceHandle>> EdgeCollapse(VertexHandle v0, VertexHandle v1);

    void EdgeSplit(VertexHandle v0, VertexHandle v1);
    void FaceSplit(FaceHandle fh, VertexHandle v0, VertexHandle v1, VertexHandle v_new);
    void FaceSplit(FaceHandle fh, EdgeHandle eh, VertexHandle v_new);
    VertexHandle EdgeSplit(EdgeHandle eh);

    VertexHandle GetOppositeVertex(FaceHandle fh, EdgeHandle eh) const;

    bool IsBorderEdge(EdgeHandle eh);
    bool IsManifoldEdge(EdgeHandle eh);
    bool IsBorderVertex(VertexHandle vh);
    bool IsManifoldVertex(VertexHandle vh);
    bool IsFace(VertexHandle v0, VertexHandle v1, VertexHandle v2);
    bool IsEdgeCollapsible(EdgeHandle eh);

    void EmbedInFrame(const Frame& f);
    void UnembedInFrame(const Frame& f);

    Scalar EdgeLength(const EdgeHandle eh) const;
    Scalar MinEdgeLength() const;
    Scalar MaxEdgeLength() const;
    Scalar MeanEdgeLength() const;

    /////
    // Listenable
    virtual void notify_garbage_collection_done(const std::map<VertexHandle, VertexHandle> &vertex_handles, const std::map<EdgeHandle, EdgeHandle> &edge_handles, const std::map<FaceHandle, FaceHandle> &face_handles);

    /////
    // Octree

    // (re)Create and fill an octree containing the mesh.
    // By default, the octree bouding box is created from the mesh octree,
    // but it may be wise to specify a larger bouding box as adding triangles outside of the octree
    // may slow down the computations
    void createOctree(AABBox aabb = AABBox());

    // if a face changed of position, it may need to be updated in the octree
    void updateOctreeTriangle(FaceHandle);

    void deleteOctree() {octree_ = nullptr; }

protected:
    // Typedef: Vector
    typedef std::vector<Point>        VertexContainer;
    typedef std::vector<Normal>       NormalContainer;
    typedef std::vector<Color>        ColorContainer;
    typedef std::vector<VertexStatus> VertexStatusContainer;

    typedef std::vector<Face>         FaceContainer;
    typedef std::vector<FaceStatus>   FaceStatusContainer;

    typedef std::vector<Edge>         EdgeContainer;
    typedef std::vector<EdgeStatus>   EdgeStatusContainer;

protected:
    ////////
    /// Methods that should be overloaded by non-abstract TriMesh class.
    virtual void setVertexInPlace(int oldVertex, int newVertex, std::vector<int> &vh_map);
    virtual void setVertexCount(unsigned int n_vertices);
    virtual void updateFaceIndices(const std::vector<int> &vh_map) = 0;

    /**
     * @brief LoadFromObjFilePrivate Mostly here to ensure file is closed even if errors occur and exception catching.
     * @param filename
     * @return
     */
    bool LoadFromObjFilePrivate(std::ifstream &file_stream);

    // Vertex Data
    VertexStatusContainer v_status_;

    // Face Data
    FaceContainer       faces_;
    FaceStatusContainer f_status_;

    // Edge Data
    EdgeContainer       edges_;
    EdgeStatusContainer e_status_;

    // Other Data
    size_t n_edges_;
    mutable std::mutex mutex_;
    bool locked_;

    /**
     * True if garbage collection should be run.
     */
    bool has_garbage_;

    /**
     * True if bounding_box should be recomputed.
     */
    bool dirty_bbox_;

    /**
     * Contains the list of vertices that are garbage.
     */
    std::set<VertexHandle> garbage_vertices_;

    /**
     * Contains the list of edges that are garbage.
     */
    std::set<EdgeHandle> garbage_edges_;

    /**
     * Contains the list of faces that are garbage.
     */
    std::set<FaceHandle> garbage_faces_;

    /**
     * Current bounding box for this mesh.
     */
    AABBox bounding_box_;

    // acceleration structure for intersection intersection
    std::shared_ptr<Octree<BasicTriMeshCell>> octree_;
    std::vector<uint> octree_cells_; // contain the storage position of each triangles (uint for Octree<BasicTriMeshCell>::ContentHandle, but let's save compile time ...)
    //std::map<unsigned int, std::shared_ptr<BasicTriMeshCell> > octree_cells_;

    friend class BasicTriMeshCell;
};

} // end of namespace core

} // end of namespace expressive

#endif // BASIC_TRI_MESH_H
