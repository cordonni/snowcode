/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: WeightedSegmentT.h

   Language: C++

   License: Expressive license

   \author: Maxime Quiblier, Cedric Zanni
   E-Mail:  maxime.quiblier@inrialpes.fr, cedric.zanni@inria.fr

   Description: Header for linearly weighted segment.

                It contains various computation in homothetic space (distance, clipping, ...)
                which could be used for both Homothetic surfaces and Hornus surfaces.
                "Homothetic space" is the space where all distance to skeleton point are renormalized by the
                weigth of the skeleton point.

   Platform Dependencies: None
*/

#pragma once
#ifndef WEIGHTED_SEGMENT_H_
#define WEIGHTED_SEGMENT_H_

// core dependencies
#include<core/CoreRequired.h>
    // geometry
    #include<core/geometry/OptWeightedSegment.h>
    #include<core/geometry/Segment.h>
    #include<core/geometry/Sphere.h>
    #include<core/geometry/WeightedPoint.h>

#include <math.h>

// std dependencies
#include <array>

/////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : stocker les informations présentes dans le vecteur de coeff des poids de manière
/// différentes (puisque on restreint maintenant ce type de segment au poids linéaire)
/// TODO:@todo : on pourra rajouter un segment avec des poids cubique (dérivée des poids au extrémité...)
/// TODO:@todo : faire le ménage dans les fonctions de clipping ...
/////////////////////////////////////////////////////////////////////////////////////////

namespace expressive {

namespace core {

/**
 * A Segment using WeightedPoints instead of PointPrimitive.
 */
class CORE_API WeightedSegment : public SegmentT<WeightedPoint>
{
public:
    EXPRESSIVE_MACRO_NAME("WeightedSegment")

    typedef SegmentT<WeightedPoint> Base;
    typedef GeometryPrimitiveT<WeightedPoint> GeometryBase;

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    /*! \brief Build a constant 1.0-weighted segment along the first axis.
     */
    WeightedSegment();

    /*! \brief Build a linear weighted segment,
     *         if weights are not given create a 1.0-weighted segment
     *   \param p1 First segment point
     *   \param p2 Second segment point
     *   \param weight1 Weight for first segment point (default value is 1.0)
     *   \param weight2 Weight for second segment point (default value is 1.0)
     */
    WeightedSegment(const Point& p1, const Point& p2,
                    Scalar weight1 = 1.0, Scalar weight2 = 1.0);

    WeightedSegment(const WeightedPoint& p1, const WeightedPoint& p2);

    // Copy constructor
    WeightedSegment(const WeightedSegment &rhs, unsigned int start = -1, unsigned int end = -1);

    virtual ~WeightedSegment();

    virtual std::shared_ptr<GeometryPrimitiveT<WeightedPoint> > clone(unsigned int start = - 1, unsigned int end = -1) const; // Todo : yew.. this is ugly.

    ///////////////////
    // Xml functions //
    ///////////////////

    virtual std::string ToString() const;

    virtual TiXmlElement * ToXml(bool embed_vertices = false, const char *name = NULL) const;

    ///////////////
    // Modifiers //
    ///////////////
    void set_weight_coeff( std::array<Scalar,2>& weight_coeff);

    // Convenience functions, should not be used on non linear segment
    void set_weights(Scalar w1, Scalar w2);
    void set_weight_p1(Scalar w1);
    void set_weight_p2(Scalar w2);

    virtual void updated();

    ///////////////
    // Accessors //
    ///////////////

    inline const std::array<Scalar,2>& weight_coeff()   const { return weight_coeff_; }
    inline Scalar weight_coeff(int i) const { return weight_coeff_[i]; }

    // Convenience accessors to get extreme weights value
    inline Scalar weight_p1()    const { return points_[0].weight(); }
    inline Scalar weight_p2()    const { return points_[1].weight(); }

//    virtual Point GetStart() const;
//    virtual Point GetEnd() const;


    // Parameter should be between 0.0 (p1_) and 1.0 (p2_)
    inline Scalar ComputeLocalWeight(Scalar l) const
    {
        return weight_coeff_[1]*l + weight_coeff_[0];
    }

    inline const OptWeightedSegment& opt_w_seg() const { return opt_w_seg_; }
//    const Point& p_min() const { return p_min_; }
//    const Vector& increase_unit_dir() const { return increase_unit_dir_; }
//    Scalar weight_min() const { return weight_min_; }
//    Scalar inv_weight_min() const { return inv_weight_min_; }
//    Scalar unit_delta_weight() const { return unit_delta_weight_; }

    ///////////////////////////////
    // Homothetic space function //
    ///////////////////////////////

    //TODO:@todo : Should probably mutualize some code ...

    /** \brief Compute the homothetic distance to the closest point on the segment.
     *
     *    \param point The computation point.
     *
     *    \return return homothetic distance to the segment
     */
     Scalar HomotheticDistance(const Point& point) const;


    /** \brief "Select" the part of a segment that is inside (in the homothetic space) of a clipping "sphere".
     *          Done by solving a second degree equation (with geometric consideration insuring existance of a unique interval),
     *          try to avoid numerical instability in the case where second degree coeff tend toward zero.
     *          Returned value are between 0.0 and 1.0
     *
     *    \param clipping_sphere The sphere to check against.
     *    \param clipped_l1      Begining of the clipped part if does exist such. The actual intersection point is at p1_+inter_1*dir_.
     *    \param clipped_l2      End of the clipped part if does exist such. The actual intersection point is at p1_+inter_1*dir_.
     *
     *    \return return true if a clipping was find
     */
    bool HomotheticClipping(const Sphere &clipping_sphere, Scalar &clipped_l1, Scalar &clipped_l2) const;

    /** \brief "Select" the part of a segment that is inside (in the homothetic space) of a clipping "sphere".
     *          This function use precomputed values given as parameter (prevent redundant computation during convolution
     *          computation for instance). Returned value are between 0.0 and 1.0
     */
    inline bool HomotheticClipping(const Scalar w[3], Scalar &clipped_l1, Scalar &clipped_l2) const;

//    /** \brief "Select" the part of a segment that is inside (in the homothetic space) of a clipping "sphere".
//     *          This function use precomputed values given as parameter (prevent redundant computation during convolution
//     *          computation for instance)
//     *          This function is used in Eval function of CompactPolynomial kernel which use a different parametrization for a greater stability.
//     *          Returned value are between 0.0 and length/weight_min
//     */
//    //inline : not inlined anymore : check the influence on computational time ...
//    bool HomotheticClippingSpecial(const Scalar w[3], Scalar &clipped_l1, Scalar &clipped_l2) const;
//    //TODO:@todo : WARNING : recopie de code !!!
//    static bool HomotheticClippingSpecial(const Scalar w[3], Scalar length, Scalar &clipped_l1, Scalar &clipped_l2);

//    /** \brief Compute the "normalized" length of segment inside a clipping sphere in the homothetic space
//      */
//    Scalar HomotheticClippedLength(const Sphere &clipping_sphere) const;

//    /** \brief Compute the "normalized" length of segment inside a clipping sphere in the homothetic space
//     *         as well as the derivative ot the length in function of the sphere radius :
//     *         Need some precomputed values (uv and d2) as parameter.
//     */
//    bool HomotheticClippedLengthAndDerivative(const Sphere &clipping_sphere,
//                                              Scalar uv, Scalar d2,
//                                              Scalar &length, Scalar &length_derivative) const;

//    /** \brief Compute the normalized length of a parameter interval of the segment
//     *
//     *  \param clipped_l1      First interval bound > 0.0
//     *  \param clipped_l2      Second interval bound < 1.0
//     *
//     *  \return return the length (in the homothetic space) of the interval
//     */
//    inline Scalar IntervalHomotheticLength(const Scalar l1, const Scalar l2) const;

//    inline void IntervalHomotheticLengthAndDerivative(  const Scalar l1, const Scalar d_l1,
//                                                        const Scalar l2, const Scalar d_l2,
//                                                        Scalar &length, Scalar &length_derivative) const;

    ///////////////
    // Compare   //
    ///////////////
//    virtual bool IsInferiorTo(const WeightedSegment& g2) const
//    {
//        if (p1_.weight() != g2.p1_.weight() && p1_.weight() != g2.p2_.weight()) {
//            return p1_.weight() < g2.p2_.weight();
//        }
//        if (p2_.weight() != g2.p1_.weight() && p2_.weight() != g2.p2_.weight()) {
//            return p2_.weight() < g2.p1_.weight();
//        }
//        return SegmentT::IsInferiorTo(g2);
//    }

    bool operator==(const WeightedSegment& g2) const
    {
        if (points_[0].weight() != g2.points_[0].weight() && points_[0].weight() != g2.points_[1].weight()) {
            return false;
        }
        if (points_[1].weight() != g2.points_[0].weight() && points_[1].weight() != g2.points_[1].weight()) {
            return false;
        }
        return SegmentT::operator==(g2);
    }

    ///////////////
    // Attributs //
    ///////////////

protected:

    //TODO:@todo : this should probably be removed ...
    std::array<Scalar,2> weight_coeff_;

//    // Convenience attributes
//    Scalar p1_.weight();
//    Scalar p2_.weight();

    // Help variables for divers computation on the segment.
    // This will be used to improve computation speed in divers cases.
    OptWeightedSegment opt_w_seg_;

    virtual void ComputeHelpVariables();

};

} // Close namespace core

} // Close namespace expressive


#endif // WEIGHTED_SEGMENT_H_
