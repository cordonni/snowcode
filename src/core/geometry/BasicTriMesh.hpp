#ifndef BASIC_TRI_MESH_HPP
#define BASIC_TRI_MESH_HPP

#include "core/geometry/BasicTriMesh.h"
#include "core/utils/Octree.h"

namespace expressive
{

namespace core
{

///////////////////////////
/////// BasicTriMeshT
//template<>
//inline BasicTriMeshT<Point>::BasicTriMeshT(ork::MeshMode mesh_mode, ork::MeshUsage usage) :
//    AbstractTriMesh(),
//    mesh_(new MESH(mesh_mode, usage))
//{
//    mesh_->addAttributeType(0, 3, ork::A32F, false);
//}

//template<>
//template<typename VERTEX>
//inline typename BasicTriMeshT<VERTEX>::VertexHandle BasicTriMeshT<VERTEX>::add_vertex<Point>(const Point& p, const VertexStatus& status = VertexStatus()) {
//    VERTEX tmp = p;
//    mesh_->addVertex(tmp);
//    v_status_.push_back(status);
//    bounding_box_.Enlarge(p);

//    return VertexHandle(mesh_->getVertexCount() - 1);
//}

template<typename VERTEX>
BasicTriMeshT<VERTEX>::BasicTriMeshT(ork::MeshMode mesh_mode, ork::MeshUsage usage) :
    AbstractTriMesh(),
    mesh_(new MESH(mesh_mode, usage))
{
    VERTEX::RegisterAttributeTypes(mesh_);
}

//template<>
//inline BasicTriMeshT<Point>::BasicTriMeshT(const BasicTriMeshT &rhs, bool copy_content, bool share_data) :
//      AbstractTriMesh(rhs, copy_content, share_data),
//      mesh_((copy_content && share_data) ? rhs.mesh_ : new MESH(rhs.mesh_->getMode(), rhs.mesh_->getUsage()))
//{
//    if (copy_content && !share_data) {
//        copyDataFrom(rhs);
//    }
//    mesh_->addAttributeType(0, 3, ork::A32F, false);
//}

template<typename VERTEX>
BasicTriMeshT<VERTEX>::BasicTriMeshT(const BasicTriMeshT &rhs, bool copy_content, bool share_data) :
      AbstractTriMesh(rhs, copy_content, share_data),
      mesh_((copy_content && share_data) ? rhs.mesh_ : new MESH(rhs.mesh_->getMode(), rhs.mesh_->getUsage()))
{
    if (copy_content && !share_data) {
        copyDataFrom(rhs);
    }
    VERTEX::RegisterAttributeTypes(mesh_);
}

template<typename VERTEX>
std::shared_ptr<AbstractTriMesh> BasicTriMeshT<VERTEX>::clone(bool copy_content) const
{
    return std::make_shared<BasicTriMeshT>(*this, copy_content, false);
}

template<typename VERTEX>
void BasicTriMeshT<VERTEX>::copyDataFrom(const BasicTriMeshT &rhs)
{
    mesh_->copyDataFrom(*rhs.mesh_);
    AbstractTriMesh::copyDataFrom(rhs);
}

template<typename VERTEX>
void BasicTriMeshT<VERTEX>::copyDataFrom(const AbstractTriMesh *mesh)
{
    const BasicTriMeshT* m = dynamic_cast<const BasicTriMeshT*>(mesh);
    if (m != nullptr) {
        copyDataFrom(*m);
    } else {
        AbstractTriMesh::copyDataFrom(*mesh);
    }
}

template<typename VERTEX>
BasicTriMeshT<VERTEX>::~BasicTriMeshT()
{
    clear();
}

template<typename VERTEX>
void BasicTriMeshT<VERTEX>::clear()
{
    mesh_->clear();
    AbstractTriMesh::clear();
}

template<typename VERTEX>
size_t BasicTriMeshT<VERTEX>::n_vertices() const
{
    return mesh_->getVertexCount();
}

template<typename VERTEX>
size_t BasicTriMeshT<VERTEX>::n_indices () const
{
    return mesh_->getIndiceCount();
}

//// Special cases for when VERTEX == Point
//template<>
//inline bool BasicTriMeshT<Point>::has_vertex_normals() const
//{
//    return false;
//}

//template<>
//inline bool BasicTriMeshT<Point>::has_vertex_colors () const
//{
//    return false;
//}

//template<>
//inline Normal&  BasicTriMeshT<Point>::normal(VertexHandle vh)
//{
//    UNUSED(vh);
//    assert(false);
//    return Normal::Zero();
//}

//template<>
//inline Vector2& BasicTriMeshT<Point>::uv(VertexHandle vh)
//{
//    UNUSED(vh);
//    assert(false);
//    return Vector2::Zero();
//}

//template<>
//inline Color&   BasicTriMeshT<Point>::color (VertexHandle vh)
//{
//    UNUSED(vh);
//    assert(false);
//    return Color::Zero();
//}

//// Setters
//template<>
//inline void BasicTriMeshT<Point>::set_vertex(VertexHandle vh, const Point& point )
//{
//    Point &v = vertex(vh);
//    for (unsigned int i = 0; i < Point::RowsAtCompileTime; ++i) {
//        if (v[i] == bounding_box_.min()[i] || v[i] == bounding_box_.max()[i]) {
//            dirty_bbox_ = true;
//        }
//    }
//    v = point;
//    mesh_->InvalidateData();
//    bounding_box_.Enlarge(point);
//}

//template<>
//inline void BasicTriMeshT<Point>::set_point (VertexHandle vh, const Point & point )
//{
//    set_vertex(vh, point);
////    vertex(vh).set_pos   (point ); mesh_->InvalidateData();
//}

//template<>
//inline void BasicTriMeshT<Point>::set_normal(VertexHandle vh, const Vector& normal)
//{
//    UNUSED(vh);
//    UNUSED(normal);
//    assert(false && "Error : trying to set data on a BasicTriMesh defined with only a Point.");
//}

//template<>
//inline void BasicTriMeshT<Point>::set_uv    (VertexHandle vh, const Vector2& uv   )
//{
//    UNUSED(vh);
//    UNUSED(uv);
//    assert(false && "Error : trying to set data on a BasicTriMesh defined with only a Point.");
//}

//template<>
//inline void BasicTriMeshT<Point>::set_color (VertexHandle vh, const Color & color )
//{
//    UNUSED(vh);
//    UNUSED(color);
//    assert(false && "Error : trying to set data on a BasicTriMesh defined with only a Point.");
//}

template<typename VERTEX>
bool BasicTriMeshT<VERTEX>::has_vertex_normals() const
{
    return VERTEX::has_vertex_normals();
}

template<typename VERTEX>
bool BasicTriMeshT<VERTEX>::has_vertex_colors () const
{
    return VERTEX::has_vertex_colors ();
}


template<typename VERTEX>
const VERTEX& BasicTriMeshT<VERTEX>::vertex(VertexHandle vh) const
{
    return mesh_->getVertex(vh.idx_);
}

template<typename VERTEX>
const Point& BasicTriMeshT<VERTEX>::point (VertexHandle vh) const
{
    return mesh_->getVertex(vh.idx_);
}

template<typename VERTEX>
const Normal&  BasicTriMeshT<VERTEX>::normal(VertexHandle vh) const
{
    return vertex(vh).normal();
}

template<typename VERTEX>
const Vector2& BasicTriMeshT<VERTEX>::uv(VertexHandle vh) const
{
    return vertex(vh).uv();
}

template<typename VERTEX>
const Color&   BasicTriMeshT<VERTEX>::color (VertexHandle vh) const
{
    return vertex(vh).color();
}

template<typename VERTEX>
VERTEX& BasicTriMeshT<VERTEX>::vertex(VertexHandle vh)
{
    return mesh_->getVertex(vh.idx_);
}

template<typename VERTEX>
Point& BasicTriMeshT<VERTEX>::point (VertexHandle vh)
{
    return mesh_->getVertex(vh.idx_);
}

template<typename VERTEX>
Normal&  BasicTriMeshT<VERTEX>::normal(VertexHandle vh)
{
    return vertex(vh).normal();
}

template<typename VERTEX>
Vector2& BasicTriMeshT<VERTEX>::uv(VertexHandle vh)
{
    return vertex(vh).uv();
}

template<typename VERTEX>
Color&   BasicTriMeshT<VERTEX>::color (VertexHandle vh)
{
    return vertex(vh).color();
}



template<typename VERTEX>
typename BasicTriMeshT<VERTEX>::MESHPTR BasicTriMeshT<VERTEX>::internal_mesh() const
{
    return mesh_;
}

template<typename VERTEX>
ork::ptr<ork::MeshBuffers> BasicTriMeshT<VERTEX>::mesh_buffers() const
{
    return internal_mesh()->getBuffers();
}

template<typename VERTEX>
ork::MeshUsage BasicTriMeshT<VERTEX>::mesh_usage() const
{
    return internal_mesh()->getUsage();
}

template<typename VERTEX>
ork::MeshMode BasicTriMeshT<VERTEX>::mesh_mode() const
{
    return internal_mesh()->getMode();
}

template<typename VERTEX>
std::mutex& BasicTriMeshT<VERTEX>::get_buffers_mutex() const
{
    return internal_mesh()->getMutex();
}

// Setters
template<typename VERTEX>
void BasicTriMeshT<VERTEX>::set_vertex(VertexHandle vh, const VERTEX& point )
{
    VERTEX &v = vertex(vh);
    for (unsigned int i = 0; i < VERTEX::RowsAtCompileTime; ++i) {
        if (v[i] == bounding_box_.min()[i] || v[i] == bounding_box_.max()[i]) {
            dirty_bbox_ = true;
        }
    }
    v.set_pos   (point );
    mesh_->InvalidateData();
    bounding_box_.Enlarge(point);
}

template<typename VERTEX>
void BasicTriMeshT<VERTEX>::set_point (VertexHandle vh, const Point & point )
{
    set_vertex(vh, point);
//    vertex(vh).set_pos   (point ); mesh_->InvalidateData();
}

template<typename VERTEX>
void BasicTriMeshT<VERTEX>::set_normal(VertexHandle vh, const Vector& normal)
{
    vertex(vh).set_normal(normal); mesh_->InvalidateData();
}

template<typename VERTEX>
void BasicTriMeshT<VERTEX>::set_uv    (VertexHandle vh, const Vector2& uv   )
{
    vertex(vh).set_UV    (uv);     mesh_->InvalidateData();
}

template<typename VERTEX>
void BasicTriMeshT<VERTEX>::set_color (VertexHandle vh, const Color & color )
{
    vertex(vh).set_color (color ); mesh_->InvalidateData();
}

// Setters: Vertex
///**
// * Adds a vertex to this mesh.
// * This version is here for compliance with ork::mesh's api.
// */
//template<typename VERTEX>
//template<typename OTHERVERTEX>
//typename BasicTriMeshT<VERTEX>::VertexHandle BasicTriMeshT<VERTEX>::addVertex(const OTHERVERTEX& p, const typename BasicTriMeshT<VERTEX>::VertexStatus& status) {
//    VERTEX tmp(p);
//    mesh_->addVertex(tmp);
//    v_status_.push_back(status);
//    bounding_box_.Enlarge(p);

//    return VertexHandle(mesh_->getVertexCount() - 1);
//}

template<typename VERTEX>
typename BasicTriMeshT<VERTEX>::VertexHandle BasicTriMeshT<VERTEX>::add_point(const Point& p, const VertexStatus& status)
{
    return add_vertex(p, status);
}

//template<>
//inline BasicTriMeshT<Point>::VertexHandle BasicTriMeshT<Point>::add_point(const Point &p, const Color& color, const VertexStatus& status)
//{
//    UNUSED(color);
//    return add_vertex(p, status);
//}

//template<>
//inline BasicTriMeshT<Point>::VertexHandle BasicTriMeshT<Point>::add_point(const Point &p, const Vector &normal, const Color& color, const VertexStatus& status)
//{
//    UNUSED(normal);
//    UNUSED(color);
//    return add_vertex(p, status);
//}

//template<>
//inline BasicTriMeshT<Point>::VertexHandle BasicTriMeshT<Point>::add_point(const Point &p, const Vector &normal, const Vector2 &uv, const Color& color, const VertexStatus& status)
//{
//    UNUSED(normal);
//    UNUSED(uv);
//    UNUSED(color);
//    return add_vertex(p, status);
//}

template<typename VERTEX>
typename BasicTriMeshT<VERTEX>::VertexHandle BasicTriMeshT<VERTEX>::add_point(const Point &p, const Color& color, const VertexStatus& status)
{
    return add_vertex(VERTEX(p, color), status);
}

template<typename VERTEX>
typename BasicTriMeshT<VERTEX>::VertexHandle BasicTriMeshT<VERTEX>::add_point(const Point &p, const Vector &normal, const Color& color, const VertexStatus& status)
{
    return add_vertex(VERTEX(p, normal, color), status);
}

template<typename VERTEX>
typename BasicTriMeshT<VERTEX>::VertexHandle BasicTriMeshT<VERTEX>::add_point(const Point &p, const Vector &normal, const Vector2 &uv, const Color& color, const VertexStatus& status)
{
    return add_vertex(VERTEX(p, normal, uv, color), status);
}

/**
 * Adds a vertex to this mesh.
 * This version is here for compliance with openmesh's api.
 */
template<typename VERTEX>
template<typename OTHERVERTEX>
typename BasicTriMeshT<VERTEX>::VertexHandle BasicTriMeshT<VERTEX>::add_vertex(const OTHERVERTEX& p, const VertexStatus& status) {
    VERTEX tmp(p);
    mesh_->addVertex(tmp);
    v_status_.push_back(status);
    bounding_box_.Enlarge(p);

    return VertexHandle(mesh_->getVertexCount() - 1);
}

//    VertexHandle add_vertex(const Vector& p, const VertexStatus& status = VertexStatus()) {
//        VERTEX tmp(p);
//        mesh_->addVertex(tmp);
//        v_status_.push_back(status);
//        bounding_box_.Enlarge(p);

//        return VertexHandle(mesh_->getVertexCount() - 1);
//    }

template<typename VERTEX>
typename BasicTriMeshT<VERTEX>::EdgeHandle BasicTriMeshT<VERTEX>::add_line(VertexHandle v1, VertexHandle v2)
{
    // TODO opengl merge with ulysse's work on edges ?
    assert(mesh_->getMode() == ork::LINES);
    mesh_->addIndice(v1.idx_);
    mesh_->addIndice(v2.idx_);

    return EdgeHandle(mesh_->getIndiceCount() / 2 - 1);
}

// Setters: Face
template<typename VERTEX>
typename BasicTriMeshT<VERTEX>::FaceHandle BasicTriMeshT<VERTEX>::add_face(VertexHandle v1, VertexHandle v2, VertexHandle v3) {
    unsigned int idx = (unsigned int) n_faces();
    faces_.push_back(Face(v1.idx_,v2.idx_,v3.idx_));//faces_.emplace_back(v1.idx_,v2.idx_,v3.idx_);
    v_status_[v1.idx_].add_a_face(idx);
    v_status_[v2.idx_].add_a_face(idx);
    v_status_[v3.idx_].add_a_face(idx);

    // updating face status
//    ++n_faces_;
    f_status_.resize(idx + 1);//f_status_.emplace_back();

    mesh_->addIndice(v1.idx_);
    mesh_->addIndice(v2.idx_);
    mesh_->addIndice(v3.idx_);

    FaceHandle f_h(-1);
    EdgeHandle e_h(-1);

    f_h = GetOppositeFace(idx, v1, v2);
    if(f_h.idx() == (unsigned int)-1)
        e_h = add_edge(v1, v2);
    else
        e_h = get_face_edge(f_h,v1, v2);

    f_status_[idx].add_an_edge(e_h.idx());
    e_status_[e_h.idx()].add_a_face(idx);
    v_status_[v1.idx()].add_an_edge(e_h.idx());
    v_status_[v2.idx()].add_an_edge(e_h.idx());

    f_h = GetOppositeFace(idx, v2, v3);
    if(f_h.idx() == (unsigned int) -1)
        e_h = add_edge(v2, v3);
    else
        e_h = get_face_edge(f_h,v2, v3);
    e_status_[e_h.idx()].add_a_face(idx);
    f_status_[idx].add_an_edge(e_h.idx());
    v_status_[v2.idx()].add_an_edge(e_h.idx());
    v_status_[v3.idx()].add_an_edge(e_h.idx());

    f_h = GetOppositeFace(idx, v3, v1);
    if(f_h.idx() == (unsigned int)-1)
        e_h = add_edge(v3, v1);
    else
        e_h = get_face_edge(f_h,v3, v1);
    e_status_[e_h.idx()].add_a_face(idx);
    f_status_[idx].add_an_edge(e_h.idx());
    v_status_[v3.idx()].add_an_edge(e_h.idx());
    v_status_[v1.idx()].add_an_edge(e_h.idx());

    if (octree_ != nullptr)
    {
        if(octree_cells_.size()<=idx)
            octree_cells_.resize(idx+1);
        octree_cells_[idx] = octree_->add(BasicTriMeshCell(this, idx));
    }
    return FaceHandle(idx);
}

template<typename VERTEX>
void  BasicTriMeshT<VERTEX>::inverseNormals()
{
    if(!has_vertex_normals())
        return;

    for(VertexIter it = vertices_begin(); it != vertices_end(); it++)
        vertex(*it).normal() *= -1.0;
}


template<typename VERTEX>
void BasicTriMeshT<VERTEX>::setVertexInPlace(int oldVertex, int newVertex, std::vector<int> &vh_map)
{
    //            vertices_[i0] = vertices_[i1];
    mesh_->setVertex(oldVertex, mesh_->getVertex(newVertex));
    AbstractTriMesh::setVertexInPlace(oldVertex, newVertex, vh_map);
}

template<typename VERTEX>
void BasicTriMeshT<VERTEX>::setVertexCount(unsigned int n_vertices)
{
    mesh_->setVertexCount(n_vertices);
}

template<typename VERTEX>
void BasicTriMeshT<VERTEX>::updateFaceIndices(const std::vector<int> &vh_map)
{
    mesh_->clearIndices();
    // Update of faces indices : do not understand why we can't do it during previous loop
    for(auto f_it = faces_.begin(); f_it != faces_.end(); ++f_it)
    {
        f_it->idx_[0] = ((unsigned int) vh_map[f_it->idx_[0]]);
        f_it->idx_[1] = ((unsigned int) vh_map[f_it->idx_[1]]);
        f_it->idx_[2] = ((unsigned int) vh_map[f_it->idx_[2]]);
        mesh_->addIndice(f_it->idx_[0]);
        mesh_->addIndice(f_it->idx_[1]);
        mesh_->addIndice(f_it->idx_[2]);
    }

}



template<typename VERTEX>
void BasicTriMeshT<VERTEX>::ForceUpdate()
{
    mesh_->InvalidateData();
}

//// Operators
//template<typename VERTEX>
//BasicTriMeshT<VERTEX> &BasicTriMeshT<VERTEX>::operator=(const BasicTriMeshT &rhs)
//{
//    mesh_ = rhs.mesh_; // maybe we should copy this ? For now just share mesh data, so maybe we can force users to do some instancing against their will.
//    BasicTriMesh::operator =(rhs);

//    return *this;
//}

}

}

#endif
