/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: CsteWeightedSegmentWithDensity.h

   Language: C++

   License: Expressive license

   \author: Cedric Zanni
   E-Mail:  cedric.zanni@inria.fr

   Description: Header for a segment with constant weight and density which information are optimized for 
                some computation (convolution surfaces for instance). This is mainly use for segment corrector
                in convol library.

                It contains various computation in homothetic space (distance, clipping, ...)
                which could be used for both Homothetic surfaces and Hornus surfaces.
                "Homothetic space" is the space where all distance to skeleton point are renormalized by the
                weigth of the skeleton point.

   Platform Dependencies: None
*/

#pragma once
#ifndef CSTE_WEIGHTED_SEGMENT_WITH_DENSITY_H_
#define CSTE_WEIGHTED_SEGMENT_WITH_DENSITY_H_

// core dependencies
#include <core/CoreRequired.h>
    #include <core/geometry/WeightedPoint.h>
    #include <core/geometry/PrecisionAxisBoundingBox.h>

#include <list>

/////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : some of the information present in this segment should not be present anymore in
///              the original WeightedSegment (length_ for instance ...)
/// TODO:@todo : check if clipping function can be simplified
/// TODO:@todo : Why doesn't this inherit from WeightedSegment ?
/////////////////////////////////////////////////////////////////////////////////////////


namespace expressive {

namespace core {

/**
 * A special type of WeightedSegment, complemented with a density member.
 */
class CORE_API CsteWeightedSegmentWithDensity
{
public:
    

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    CsteWeightedSegmentWithDensity(); //TODO:@todo : if this is used, set_parameters should be called later ...

    /*! \brief Build a CsteWeightedSegmentWithDensity from a classical one.
     */
    CsteWeightedSegmentWithDensity(const WeightedPoint &w_p1, const Vector& unit_dir, Scalar length, Scalar density);

    // Copy constructor
    CsteWeightedSegmentWithDensity(const CsteWeightedSegmentWithDensity &rhs);

    ~CsteWeightedSegmentWithDensity();


    Point Barycenter() const;


    /////////////////
    /// Accessors ///
    /////////////////

    inline const Point& p1() const { return p1_; }
    inline const Vector& unit_dir() const { return unit_dir_; }
    inline Scalar length() const { return length_; }
    inline Scalar weight() const { return weight_; }
    inline Scalar density() const { return density_; }

    /////////////////////////////
    /// Bounding-box function ///
    /////////////////////////////

    AABBox GetAxisBoundingBox() const;

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    Point p1_;
    Vector unit_dir_;
    Scalar length_;
    Scalar weight_;
    Scalar density_;
};

} // Close namespace core

} // Close namespace expressive


#endif // OPT_WEIGHTED_SEGMENT_H_
