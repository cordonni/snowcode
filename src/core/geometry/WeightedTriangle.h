/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: WeightedTriangle.h

   Language: C++

   License: Expressive license

   \author: Maxime Quiblier
   E-Mail:  maxime.quiblier@inrialpes.fr

   Description: Header for weighted triangle.

   Platform Dependencies: None
*/

#pragma once
#ifndef WEIGHTED_TRIANGLE_H_
#define WEIGHTED_TRIANGLE_H_

// Convol dependencies
#include<core/CoreRequired.h>
    // geometry
    #include<core/geometry/OptWeightedTriangle.h>
    #include<core/geometry/Triangle.h>

namespace expressive {

namespace core {

/**
 * A Triangle using WeightedPoints instead of PointPrimitives.
 */
class CORE_API WeightedTriangle : public TriangleT<WeightedPoint>
{
public:
    EXPRESSIVE_MACRO_NAME("WeightedTriangle")

    typedef TriangleT<WeightedPoint> Base;
    typedef GeometryPrimitiveT<WeightedPoint> GeometryBase;

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    /*! \brief Build a constant 1.0-weighted triangle along the first axis.
     */
    WeightedTriangle() : Base() //, weight_p1_(1.0), weight_p2_(1.0), weight_p3_(1.0)
    {
        points_[0].set_weight(1.0);
        points_[1].set_weight(1.0);
        points_[2].set_weight(1.0);

        ComputeHelpVariables();
    }
    
    /*! \brief Build a linear weighted triangle
     *   \param p1 First triangle point
     *   \param p2 Second triangle point
     *   \param p3 Third triangle point
     *   \param weight1 Weight for first triangle point
     *   \param weight2 Weight for second triangle point
     *   \param weight3 Weight for second triangle point
     */
    WeightedTriangle(const Point& p1, const Point& p2, const Point& p3,
                     Scalar weight1 = 1.0, Scalar weight2 = 1.0, Scalar weight3 = 1.0)
//        : Triangle(p1,p2,p3), weight_p1_(weight1), weight_p2_(weight2), weight_p3_(weight3)
        : Base(WeightedPoint(p1, weight1), WeightedPoint(p2, weight2), WeightedPoint(p3, weight3))
    {
        ComputeHelpVariables();
    }

    WeightedTriangle(const std::vector<WeightedPoint>& points) :
        Base(points)
    {
        ComputeHelpVariables();
    }

    // Copy constructor
    WeightedTriangle(const WeightedTriangle &rhs) :
            Base(rhs),
//            weight_p1_(rhs.weight_p1_), weight_p2_(rhs.weight_p2_), weight_p3_(rhs.weight_p3_),
            opt_w_tri_(rhs.opt_w_tri_)
    {

    }

    virtual ~WeightedTriangle(){}

    ///////////
    /// Xml ///
    ///////////

    virtual TiXmlElement * ToXml(bool embed_vertices = false, const char *name = NULL) const;

    void InitFromXml(const TiXmlElement * /*element*/) {}

    ///////////////
    // Modifiers //
    ///////////////

    // Convenience functions, should not be used on non linear triangle
    void set_weights(Scalar w1, Scalar w2, Scalar w3);
    void set_weight_p1(Scalar w1);
    void set_weight_p2(Scalar w2);
    void set_weight_p3(Scalar w3);

    virtual void updated()
    {
        GeometryPrimitiveT<WeightedPoint>::updated();
        ComputeHelpVariables(); //TODO:@todo : there is an asymetrie between the way point are modified and other parameters...
    }

    ///////////////
    // Accessors //
    ///////////////

    inline const OptWeightedTriangle& opt_w_tri() const { return opt_w_tri_; }

    Scalar weight_p1() const { return points_[0].weight(); }
    Scalar weight_p2() const { return points_[1].weight(); }
    Scalar weight_p3() const { return points_[2].weight(); }

    ///////////////
    // Attributs //
    ///////////////

protected:
    // Help variables for divers computation on the segment.
    // This will be used to improve computation speed in divers cases.
    OptWeightedTriangle opt_w_tri_;

    virtual void ComputeHelpVariables();


};

} // Close namespace core

} // Close namespace expressive

#endif // WEIGHTED_TRIANGLE_H_
