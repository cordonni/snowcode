/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: OptWeightedSegment.h

   Language: C++

   License: Expressive license

   \author: Cedric Zanni
   E-Mail:  cedric.zanni@inria.fr

   Description: Header for linearly weighted segment which information are optimized for 
		some computation (convolution surfaces for instance).

                It contains various computation in homothetic space (distance, clipping, ...)
                which could be used for both Homothetic surfaces and Hornus surfaces.
                "Homothetic space" is the space where all distance to skeleton point are renormalized by the
                weigth of the skeleton point.

   Platform Dependencies: None
*/

#pragma once
#ifndef OPT_WEIGHTED_SEGMENT_H_
#define OPT_WEIGHTED_SEGMENT_H_

// core dependencies
#include <core/CoreRequired.h>
    #include <core/geometry/WeightedPoint.h>
    #include <core/geometry/PrecisionAxisBoundingBox.h>

#include <list>

/////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : some of the information present in this segment should not be present anymore in
///              the original WeightedSegment (length_ for instance ...)
/// TODO:@todo : check if clipping function can be simplified
/////////////////////////////////////////////////////////////////////////////////////////

namespace expressive {

namespace core {

// geometry
class WeightedSegment; //#include<core/geometry/WeightedSegment.h>
class Sphere; //#include<core/geometry/Sphere.h>


class CORE_API OptWeightedSegment
{
public:
    

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    OptWeightedSegment(){} //TODO:@todo : if this is used, set_parameters should be called later ...

    /*! \brief Build a OptWeightedSegment from a classical one.
     */
    OptWeightedSegment(const WeightedSegment &w_seg)
    {
        set_parameters(w_seg);
    }

    OptWeightedSegment(const WeightedPoint &w_p1, const WeightedPoint &w_p2)
    {
        set_parameters(w_p1, w_p2);
    }

    // Copy constructor
    OptWeightedSegment(const OptWeightedSegment &rhs)
        :   p_min_(rhs.p_min_), increase_unit_dir_(rhs.increase_unit_dir_), length_(rhs.length_),
            weight_min_(rhs.weight_min_), inv_weight_min_(rhs.inv_weight_min_),
            unit_delta_weight_(rhs.unit_delta_weight_), orientation_(rhs.orientation_)
    {}

    ~OptWeightedSegment(){}


    inline Scalar LocalWeight(Scalar s) const { return weight_min_ + unit_delta_weight_*length_*s; }
    inline Scalar WeightMax() const { return weight_min_ + unit_delta_weight_*length_; }

    Point Barycenter() const
    {
        Scalar weight_max = WeightMax();
        Scalar sum_weight = weight_min_ + weight_max;
        return p_min_ + (length_*weight_max/sum_weight)*increase_unit_dir_ ;
    }


    /////////////////
    /// Modifiers ///
    /////////////////
 
    void set_parameters(const WeightedPoint &w_p1, const WeightedPoint &w_p2);
    void set_parameters(const WeightedSegment &w_seg);

    /////////////////
    /// Accessors ///
    /////////////////

    // Parameter should be between 0.0 (p1_) and 1.0 (p2_)
//    inline Scalar ComputeLocalWeight(Scalar l) const
//    {
//        return weight_coeff_[1]*l + weight_coeff_[0];
//    }

    inline const Point& p_min() const { return p_min_; }
    inline const Vector& increase_unit_dir() const { return increase_unit_dir_; }
    inline Scalar length() const { return length_; }
    inline Scalar weight_min() const { return weight_min_; }
    inline Scalar inv_weight_min() const { return inv_weight_min_; }
    inline Scalar unit_delta_weight() const { return unit_delta_weight_; }
    inline bool orientation() const { return orientation_; }

    /////////////////////////////////
    /// Homothetic space function ///
    /////////////////////////////////


    /** \brief Compute the homothetic distance to the closest point on the segment.
     *
     *    \param point The computation point.
     *
     *    \return return homothetic distance to the segment
     */
    Scalar HomotheticDistance(const Point& point) const;
    Scalar HomotheticDistance(const Vector& p_min_to_point, Scalar uv, Scalar d2) const;

    void HomotheticDistanceAndGrad(const Vector& p_min_to_point, Scalar uv, Scalar d2,
                                   Scalar& hdist, Vector& hgrad) const;

    /** \brief "Select" the part of a segment that is inside (in the homothetic space) of a clipping "sphere".
     *          This function use precomputed values given as parameter (prevent redundant computation during convolution
     *          computation for instance)
     *          This function is used in Eval function of CompactPolynomial kernel which use a different parametrization for a greater stability.
     *          Returned value are between 0.0 and length/weight_min
     */
    //inline : not inlined anymore : check the influence on computational time ...
    bool HomotheticClippingSpecial(const Scalar w[3], Scalar &clipped_l1, Scalar &clipped_l2) const;
    //TODO:@todo : WARNING : recopie de code !!!
    static bool HomotheticClippingSpecial(const Scalar w[3], Scalar length, Scalar &clipped_l1, Scalar &clipped_l2);

    /** \brief Compute the "normalized" length of segment inside a clipping sphere in the homothetic space
      */
    Scalar HomotheticClippedLength(const Sphere &clipping_sphere) const;

    /** \brief Compute the "normalized" length of segment inside a clipping sphere in the homothetic space
     *         as well as the derivative ot the length in function of the sphere radius :
     *         Need some precomputed values (uv and d2) as parameter.
     */
    bool HomotheticClippedLengthAndDerivative(const Sphere &clipping_sphere,
                                              Scalar uv, Scalar d2,
                                              Scalar &length, Scalar &length_derivative) const;

    /** \brief Compute the normalized length of a parameter interval of the segment
     *
     *  \param clipped_l1      First interval bound > 0.0
     *  \param clipped_l2      Second interval bound < 1.0
     *
     *  \return return the length (in the homothetic space) of the interval
     */
    inline Scalar IntervalHomotheticLength(const Scalar l1, const Scalar l2) const;

    inline void IntervalHomotheticLengthAndDerivative(  const Scalar l1, const Scalar d_l1,
                                                        const Scalar l2, const Scalar d_l2,
                                                        Scalar &length, Scalar &length_derivative) const;

    /////////////////////////////
    /// Bounding-box function ///
    /////////////////////////////

    AABBox GetAxisBoundingBox() const;

    void GetPrecisionAxisBoundingBoxes(Scalar /*value*/, std::list<core::PrecisionAxisBoundingBox> &pboxes) const
    {
        //TODO:@todo : the way bounding box radius is computed has been hacked : the main reason was that
        // it gave horrible results for primitive with non compact support : we should find a better way to proceed

        //TODO:@todo : we could add a function that compute radius from weight in the kernels and use it here (otherwise this code will only work for homothetic)
        const Scalar max_factor_sqr = 2.5*2.5;

        const Scalar radius_factor = 1.25; //1.5; //2.0;

        Scalar current_t = 0.0;
        Point current_point = p_min_;
        Scalar current_weight = weight_min_;
        Scalar current_box_radius = radius_factor*weight_min_; //= kernel_.GetEuclidDistBound1D(current_weight, value);
        Scalar delta_w = length_*unit_delta_weight_;

        const Vector dir = length_ * increase_unit_dir_; // use an arc length parametrization and do not compute this use directly increase_unit_dir_ instead

        // Get bounding box of the segment
        AABBox seg_box = GetAxisBoundingBox();
        // Get two minimal length
        Vector diff = seg_box.max() - seg_box.min();
        Scalar d1, d2;
        if(diff[0]>diff[1]) {
            if(diff[0]>diff[2]) {
                d1 = diff[1]; d2 = diff[2];
            } else {
                d1 = diff[1]; d2 = diff[0];
            }
        } else {
            if(diff[1]>diff[2]) {
                d1 = diff[0]; d2 = diff[2];
            } else {
                d1 = diff[0]; d2 = diff[1];
            }
        }

        Point next_point;
        Scalar next_weight, next_box_radius;
        while(current_t < 1.0)
        {
            // Compute the acceptable "length" of segment (total "length" is 1) to be used to create a bounding box

            const Scalar eps = 0.01*current_box_radius;
            Scalar dt = 0.0;
            if(d1>eps || d2>eps)
            {
                const Scalar d1_div_r = d1 / current_box_radius;
                const Scalar d2_div_r = d2 / current_box_radius;
                // coeff of a*x^2 + 2*b*x + c <= 0 to solve (in [0,1-curr_t]) to find the length of seg to used in a BBox
                // a and b are positif, so the solution of interest are [0;something]
                Scalar a = d1_div_r*d1_div_r + d2_div_r*d2_div_r;
                Scalar b = 2.0*(d1_div_r+d2_div_r)*current_box_radius;
                Scalar c = 4.0*(2.0-max_factor_sqr);

                Scalar delta = b*b - a*c;
                if(delta>0.0)
                { // find root of interest and "clip" it to [0,1-curr_t]
                    dt = (-b + sqrt(delta)) / a ;
                    dt = (dt>(1.0-current_t)) ? (1.0-current_t) : dt;
                }
                else
                { // it is impossible to create a bounding-box
                    assert(false);
                }
            }
            else
            {
                // we considere that the BBox is sufficiently axis-aligned
                dt = 1.0 - current_t;
            }

            next_point = current_point + dt * dir;
            next_weight     = current_weight + dt * delta_w;
            next_box_radius = radius_factor*next_weight;//kernel_.GetEuclidDistBound1D(next_weight, value);

            if(length_ > delta_w)
            {
                // make sure that the radius variation is not to fast
                if(next_weight/current_weight > 2.0)
                {
                    // we approximate a new current position
                    dt = current_weight * dt / (next_weight - current_weight);

                    next_point = current_point + dt * dir;
                    next_weight     = current_weight + dt * delta_w;
                    next_box_radius = radius_factor*next_weight;//kernel_.GetEuclidDistBound1D(next_weight, value);
                }
            }
            else
            {
                // we are in the case of high variation of radii along the segment which should be treated
                // otherwise to prevent to have to much useless precision bounding box

                //TODO:@todo : for now do the brute force (and stupid) way
                dt = 1.0 - current_t;

                next_point = current_point + dt * dir;
                next_weight     = current_weight + dt * delta_w;
                next_box_radius = radius_factor*next_weight;//kernel_.GetEuclidDistBound1D(next_weight, value);
            }

            //TODO:@todo : rajouter un test pour savoir si l'on peut finir d'un cout ou pas ...

            // Create the effective bounding box with precision of its min radius and push it
            AABBox current_box;
            for(unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i)
            {
                if(current_point[i] < next_point[i]){
                    current_box.max_[i] = next_point[i] + next_box_radius;
                    if(current_point[i] - current_box_radius < next_point[i] - next_box_radius) {
                        current_box.min_[i] = current_point[i] - current_box_radius;
                    }else{
                        current_box.min_[i] = next_point[i] - next_box_radius;
                    }
                } else {
                    current_box.min_[i] = next_point[i] - next_box_radius;
                    if(current_point[i] + current_box_radius > next_point[i] + next_box_radius) {
                        current_box.max_[i] = current_point[i] + current_box_radius;
                    }else{
                        current_box.max_[i] = next_point[i] + next_box_radius;
                    }
                }
            }
            pboxes.push_back(core::PrecisionAxisBoundingBox(current_box,current_weight,0.5*current_weight));

            // Update current point information
            current_t += dt;
            current_point = next_point;
            current_weight = next_weight;
            current_box_radius = next_box_radius;
        }
    }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    Point p_min_;
    Vector increase_unit_dir_;
    Scalar length_;
    Scalar weight_min_;
    Scalar inv_weight_min_;
    Scalar unit_delta_weight_;
    bool orientation_; //TODO:@todo : I do not like it but it is useful when using only OptWeightedSegment outside of a WeightedSegment !
};

} // Close namespace core

} // Close namespace expressive


#endif // OPT_WEIGHTED_SEGMENT_H_
