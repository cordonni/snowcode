#include <core/geometry/AbstractTriMesh.h>

#include "ork/core/Logger.h"

#include "core/geometry/Frame.h"
#include "core/utils/ConfigLoader.h"
#include "core/utils/Octree.h"

#include <cmath>
#include <fstream>
#include <deque>


using namespace ork;
using namespace std;

namespace expressive {

namespace core {

AbstractTriMesh::AbstractTriMesh() :
    Serializable(StaticName()),
    ListenableT<AbstractTriMesh>(),
    n_edges_(0),
    locked_(false),
    has_garbage_(false),
    dirty_bbox_(false),
    bounding_box_(AABBox()),
    octree_(nullptr)
{
    if (Config::getBoolParameter("use_mesh_octree")) {
        octree_ = std::make_shared<Octree<BasicTriMeshCell>>();
    }
}

AbstractTriMesh::AbstractTriMesh(const AbstractTriMesh &rhs, bool copy_content, bool share_data)
    :
      Serializable(StaticName()),
      ListenableT<AbstractTriMesh>(rhs),
      n_edges_(0),
      locked_(false),
      has_garbage_(false),
      dirty_bbox_(false),
      bounding_box_(AABBox()),
      octree_(nullptr)
{
    UNUSED(share_data);
    if (copy_content) {
        v_status_ = rhs.v_status_;
        f_status_ = rhs.f_status_;
        e_status_ = rhs.e_status_;
        faces_ = rhs.faces_;
        edges_= rhs.edges_;
        n_edges_ = rhs.n_edges_;
        has_garbage_ = rhs.has_garbage_;
        garbage_vertices_ = rhs.garbage_vertices_;
        garbage_edges_ = rhs.garbage_edges_;
        garbage_faces_ = rhs.garbage_faces_;
        bounding_box_ = rhs.bounding_box_;
        if (rhs.octree_)
        {
            if(!octree_)
                octree_ = std::make_shared<Octree<BasicTriMeshCell>>();
            octree_->copyDataFrom(*rhs.octree_);
            octree_cells_ = rhs.octree_cells_;
        }
    } else {
        if (Config::getBoolParameter("use_mesh_octree")) {
            octree_ = std::make_shared<Octree<BasicTriMeshCell>>();
        }
    }
}

void AbstractTriMesh::copyDataFrom(const AbstractTriMesh &rhs)
{
    if (v_status_.size() != rhs.v_status_.size() || f_status_.size() != rhs.f_status_.size() || faces_.size() != rhs.faces_.size() ||
            edges_.size() != rhs.edges_.size() || e_status_.size() != rhs.e_status_.size() ) {
        v_status_ = rhs.v_status_;
        edges_ = rhs.edges_;
        e_status_ = rhs.e_status_;
        faces_ = rhs.faces_;
        f_status_ = rhs.f_status_;
        has_garbage_ = rhs.has_garbage_;
        garbage_vertices_ = rhs.garbage_vertices_;
        garbage_edges_= rhs.garbage_edges_;
        garbage_faces_ = rhs.garbage_faces_;
        bounding_box_ = rhs.bounding_box_;
    }
    if (rhs.octree_)
    {
        if(!octree_)
            octree_ = std::make_shared<Octree<BasicTriMeshCell>>();
        octree_->copyDataFrom(*rhs.octree_);
        octree_cells_ = rhs.octree_cells_;
    }
}

AbstractTriMesh::~AbstractTriMesh()
{
    clear();
}

void AbstractTriMesh::garbage_collection()
{
    std::vector<VertexHandle*> empty_vh;
    std::vector<HalfedgeHandle*> empty_hh;
    std::vector<FaceHandle*> empty_fh;
    std::vector<EdgeHandle*> empty_eh;
    garbage_collection(empty_vh, empty_hh, empty_fh, empty_eh, true, true, true, true);
}


void AbstractTriMesh::garbage_collection(std::vector<VertexHandle*>& vh_to_update,
                                         std::vector<HalfedgeHandle*>& /*empty_hh*/,
                                         std::vector<FaceHandle*>& fh_to_update,
                                         std::vector<EdgeHandle*>& eh_to_update,
                                         bool _v,
                                         bool /*_h*/,
                                         bool _f,
                                         bool _e)
{
    if (!has_garbage_) {
        if (dirty_bbox_) {
            this->update_bounding_box();
        }
        return;
    }
    bounding_box_ = AABBox();
    int i, i0, i1, nV((int)n_vertices()), nF((int)n_faces()), nE((int)n_edges_);

    std::vector<int> vh_map;
    std::vector<int> fh_map;
    std::vector<int> eh_map;

    std::map<VertexHandle, VertexHandle> updated_vertices;
    std::map<EdgeHandle, EdgeHandle> updated_edges;
    std::map<FaceHandle, FaceHandle> updated_faces;

    // setup handle mapping:
    vh_map.reserve(nV);
    for (i=0; i<nV; ++i) {
        vh_map.push_back(i); //vh_map.emplace_back(i);
    }

    fh_map.reserve(nF);
    for (i=0; i<nF; ++i) {
        fh_map.push_back(i); //fh_map.emplace_back(i);
    }

    eh_map.reserve(nE);
    for (i=0; i<nE; ++i) {
        eh_map.push_back(i);
    }

    // remove deleted vertices
    if (_v && n_vertices() > 0)
    {
        i0=0;  i1=nV-1;

        while (1)
        {
            // find 1st deleted and last un-deleted
            while (!v_status_[i0].deleted() && i0 < i1) {
                bounding_box_.Enlarge(point(i0));
                ++i0;
            }
            while ( v_status_[i1].deleted() && i0 < i1)  --i1;
            if (i0 >= i1) break;

            bounding_box_.Enlarge(point(i1));

            // vertex i0 will be destroyed : we don't have to save its information
            setVertexInPlace(i0, i1, vh_map);
            updated_vertices[VertexHandle(i0)] = VertexHandle();
            updated_vertices[VertexHandle(i1)] = VertexHandle(i0);
        };

        ork::Logger::DEBUG_LOGGER->logf("MESHDEBUG", "AbstractTriMesh::garbage collection : %d deleted vertices", n_vertices() - (v_status_[i0].deleted() ? i0 : i0+1));
        //        vertices_.resize(v_status_[i0].deleted() ? i0 : i0+1);
        unsigned int n_vertices = v_status_[i0].deleted() ? i0 : i0 + 1;
        setVertexCount(n_vertices);
    }

    // remove deleted faces
    if (_f && n_faces() > 0)
    {
        i0=0;  i1=nF-1;

        while (1)
        {
            // find 1st deleted and last un-deleted
            while (!f_status_[i0].deleted() && i0 < i1)  ++i0;
            while ( f_status_[i1].deleted() && i0 < i1)  --i1;
            if (i0 >= i1) break;

            // face i0 will be destroyed : we don't have to save its information
            faces_[i0] = faces_[i1];
            std::swap(fh_map[i0], fh_map[i1]); // but we have to update the mapping
            std::swap(f_status_[i0], f_status_[i1]);
            updated_faces[FaceHandle(i0)] = FaceHandle(-1);
            updated_faces[FaceHandle(i1)] = FaceHandle(i0);
        };

        ork::Logger::DEBUG_LOGGER->logf("MESHDEBUG", "AbstractTriMesh::garbage_collection : %d deleted faces", faces_.size() - (f_status_[i0].deleted() ? i0 : i0+1));
        faces_.resize(f_status_[i0].deleted() ? i0 : i0+1);
        //        n_faces_ = faces_.size();
        f_status_.resize(n_faces());
    }

    // remove deleted edges
    if (_e && n_edges_ > 0)
    {
        i0=0;  i1=nE-1;

        while (1)
        {
            // find 1st deleted and last un-deleted
            while (!e_status_[i0].deleted() && i0 < i1)  ++i0;
            while ( e_status_[i1].deleted() && i0 < i1)  --i1;
            if (i0 >= i1) break;

            // edge i0 will be destroyed : we don't have to save its information
            edges_[i0] = edges_[i1];
            std::swap(eh_map[i0], eh_map[i1]); // but we have to update the mapping
            std::swap(e_status_[i0], e_status_[i1]);
            updated_edges[EdgeHandle(i0)] = EdgeHandle(-1);
            updated_edges[EdgeHandle(i1)] = EdgeHandle(i0);
        };

        ork::Logger::DEBUG_LOGGER->logf("MESHDEBUG", "AbstractTriMesh::garbage_collection : %d deleted edges", edges_.size() - (e_status_[i0].deleted() ? i0 : i0+1));

        edges_.resize(e_status_[i0].deleted() ? i0 : i0+1);
        n_edges_ = edges_.size();
        e_status_.resize(n_edges_);
    }

    updateFaceIndices(vh_map);

    // Update of edges indices
    for(auto e_it = edges_.begin(); e_it != edges_.end(); ++e_it)
    {
        e_it->idx_[0] = ((unsigned int) vh_map[e_it->idx_[0]]);
        e_it->idx_[1] = ((unsigned int) vh_map[e_it->idx_[1]]);
    }

    //TODO : @todo : ...
    // Update connectivity of vertices : should be as easy to reconstruct them from scratch ... avoid some swaping before...
    auto v_status_it(v_status_.begin()), v_status_it_end(v_status_.end());
    for(; v_status_it != v_status_it_end; ++v_status_it)
    {
        auto it_face(v_status_it->adjacent_faces_.begin()), it_end(v_status_it->adjacent_faces_.end());
        for(; it_face != it_end; ++it_face)
        {
            (*it_face) = ((unsigned int) fh_map[*it_face]);
        }

        auto it_edge(v_status_it->adjacent_edges_.begin()), it_edge_end(v_status_it->adjacent_edges_.end());
        for(; it_edge != it_edge_end; ++it_edge)
        {
            (*it_edge) = ((unsigned int) eh_map[*it_edge]);
        }
    }

    // Update of face status
    auto f_status_it(f_status_.begin()), f_status_it_end(f_status_.end());
    for(; f_status_it != f_status_it_end; ++f_status_it)
    {
        auto it_edge(f_status_it->adjacent_edges_.begin()), it_edge_end(f_status_it->adjacent_edges_.end());
        for(; it_edge != it_edge_end; ++it_edge)
        {
            (*it_edge) = ((unsigned int) eh_map[*it_edge]);
        }
    }

    // Update of edge status
    auto e_status_it(e_status_.begin()), e_status_it_end(e_status_.end());
    for(; e_status_it != e_status_it_end; ++e_status_it)
    {
        auto it_face(e_status_it->adjacent_faces_.begin()), it_face_end(e_status_it->adjacent_faces_.end());
        for(; it_face != it_face_end; ++it_face)
        {
            (*it_face) = ((unsigned int) fh_map[*it_face]);
        }
    }

    // Update of Handles ...
    auto v_it(vh_to_update.begin()), v_it_end(vh_to_update.end());
    for(; v_it != v_it_end; ++v_it)
    {
        (*v_it)->idx_ = vh_map[(*v_it)->idx_];
    }

    auto fh_it(fh_to_update.begin()), fh_it_end(fh_to_update.end());
    for(; fh_it != fh_it_end; ++fh_it)
    {
        (*fh_it)->idx_ = fh_map[(*fh_it)->idx_];
    }

    auto eh_it(eh_to_update.begin()), eh_it_end(eh_to_update.end());
    for(; eh_it != eh_it_end; ++eh_it)
    {
        (*eh_it)->idx_ = fh_map[(*eh_it)->idx_];
    }

    // update octree node list
    if (octree_ != nullptr) {
        for(auto f : updated_faces) // f contains pairs of (old, new) values
            if(f.second.is_valid())
                octree_cells_[f.second.idx()] = octree_cells_[f.first.idx()];
        octree_cells_.resize(n_faces());
    }

    garbage_vertices_.clear();
    garbage_edges_.clear();
    garbage_faces_.clear();
    has_garbage_ = false;
    notify_garbage_collection_done(updated_vertices, updated_edges, updated_faces);
}

/////////////////////////////////////////////////////////////////////:
//    // check correctness of the data structure ...
//    auto f_st = f_status_.begin();
//    for(auto f_it = faces_.begin(); f_it != faces_.end(); ++f_it)
//    {
//        if(!f_st->deleted()){
//            int index;
//            index = f_it->idx_v1_;
//            if(v_status_[index].deleted())
//            {
//                assert(false);
//            }
//            index = f_it->idx_v2_;
//            if(v_status_[index].deleted())
//            {
//                assert(false);
//            }
//            index = f_it->idx_v2_;
//            if(v_status_[index].deleted())
//            {
//                assert(false);
//            }
//        }
//        ++f_st;
//    }
//    for(auto v_it = v_status_.begin(); v_it != v_status_.end(); ++v_it)
//    {
//        if(v_it->deleted())
//        {
//            assert(v_it->adjacent_faces_.empty());
//        }
//        else
//        {
//            auto it_face(v_it->adjacent_faces_.begin()), it_end(v_it->adjacent_faces_.end());
//            for(; it_face != it_end; ++it_face)
//            {
//                assert(!f_status_[*it_face].deleted());
//            }
//        }
//    }
/////////////////////////////////////////////////////////////////////:

void AbstractTriMesh::clear()
{
    n_edges_ = 0;

    faces_.clear();
    edges_.clear();
    v_status_.clear();
    e_status_.clear();
    f_status_.clear();
    if (octree_ != nullptr) {
        octree_->clear();
    }
}

size_t AbstractTriMesh::n_faces   () const
{
    return faces_.size();
}

size_t AbstractTriMesh::n_edges() const
{
    return edges_.size();
}

bool AbstractTriMesh::has_vertex_status () const
{
    return v_status_.size() != 0;
}

bool AbstractTriMesh::has_face_status   () const
{
    return f_status_.size() != 0;
}

bool AbstractTriMesh::has_edge_status   () const
{
    return e_status_.size() != 0;
}

// Getters: Status
const AbstractTriMesh::VertexStatus& AbstractTriMesh::status(VertexHandle v_h) const
{
    return v_status_[v_h.idx_];
}

const AbstractTriMesh::FaceStatus&   AbstractTriMesh::status(FaceHandle   f_h) const
{
    return f_status_[f_h.idx_];
}

const AbstractTriMesh::EdgeStatus&   AbstractTriMesh::status(EdgeHandle   e_h) const
{
    return e_status_[e_h.idx_];
}

// Non const Getters: Status
AbstractTriMesh::VertexStatus& AbstractTriMesh::status(VertexHandle v_h)
{
    return v_status_[v_h.idx_];
}

AbstractTriMesh::FaceStatus&   AbstractTriMesh::status(FaceHandle   f_h)
{
    return f_status_[f_h.idx_];
}

AbstractTriMesh::EdgeStatus&   AbstractTriMesh::status(EdgeHandle   e_h)
{
    return e_status_[e_h.idx_];
}

// Getters: Iterator for Vertices
AbstractTriMesh::VertexIter AbstractTriMesh::vertices_begin()
{
    return VertexIter(*this, VertexHandle(0));
}

AbstractTriMesh::VertexIter AbstractTriMesh::vertices_end  ()
{
    return VertexIter(*this, VertexHandle((uint)n_vertices()));
}

AbstractTriMesh::ConstVertexIter AbstractTriMesh::vertices_begin() const
{
    return ConstVertexIter(*this, VertexHandle(0));
}

AbstractTriMesh::ConstVertexIter AbstractTriMesh::vertices_end  () const
{
    return ConstVertexIter(*this, VertexHandle((uint)n_vertices()));
}

// Getters: Iterator for Faces
AbstractTriMesh::FaceIter AbstractTriMesh::faces_begin()
{
    return FaceIter(*this, FaceHandle(0));
}

AbstractTriMesh::FaceIter AbstractTriMesh::faces_end  ()
{
    return FaceIter(*this, FaceHandle((uint)n_faces()));
}

AbstractTriMesh::ConstFaceIter AbstractTriMesh::faces_begin() const
{
    return ConstFaceIter(*this, FaceHandle(0));
}

AbstractTriMesh::ConstFaceIter AbstractTriMesh::faces_end  () const
{
    return ConstFaceIter(*this, FaceHandle((uint)n_faces()));
}

// Getters: Iterator for Edges
AbstractTriMesh::EdgeIter AbstractTriMesh::edges_begin()
{
    return EdgeIter(*this, EdgeHandle(0));
}

AbstractTriMesh::EdgeIter AbstractTriMesh::edges_end  ()
{
    return EdgeIter(*this, EdgeHandle((uint)n_edges()));
}

AbstractTriMesh::ConstEdgeIter AbstractTriMesh::edges_begin() const
{
    return ConstEdgeIter(*this, EdgeHandle(0));
}

AbstractTriMesh::ConstEdgeIter AbstractTriMesh::edges_end  () const
{
    return ConstEdgeIter(*this, EdgeHandle((uint)n_edges()));
}

// Getters: Iterator for Circulator
AbstractTriMesh::ConstFaceVertexIter AbstractTriMesh::cfv_begin(FaceHandle   _fh) const
{
    return ConstFaceVertexIter(*this, _fh);
}

AbstractTriMesh::ConstVertexFaceIter AbstractTriMesh::cvf_begin(VertexHandle _vh) const
{
    return ConstVertexFaceIter(*this, _vh);
}
//TODO:@todo : should add the end version

// Getters: Vertex Handle Data
AbstractTriMesh::VertexHandle AbstractTriMesh::vertex_handle(uint index) const
{
    return VertexHandle(index);
} // be carefull the vertex could not be valid anymore

//  VertexHandle vertex_handle(uint _i) const { return (_i < n_vertices()) ? handle( vertices_[_i] ) : VertexHandle(); }

AABBox  AbstractTriMesh::bounding_box() const
{
    return bounding_box_;
}

// Getters: Face Handle Data
AbstractTriMesh::FaceHandle AbstractTriMesh::face_handle(uint index) const
{
    return FaceHandle(index);
} // be carefull the face could not be valid anymore

AbstractTriMesh::Face& AbstractTriMesh::face(FaceHandle fh)
{
    return faces_[fh.idx_];
}

const AbstractTriMesh::Face& AbstractTriMesh::face(FaceHandle fh) const
{
    return faces_[fh.idx_];
}

AbstractTriMesh::Edge& AbstractTriMesh::edge(EdgeHandle eh)
{
    return edges_[eh.idx_];
}

const AbstractTriMesh::Edge& AbstractTriMesh::edge(EdgeHandle eh) const
{
    return edges_[eh.idx_];
}

std::shared_ptr<Octree<BasicTriMeshCell>> AbstractTriMesh::octree() const
{
    return octree_;
}

const std::set<AbstractTriMesh::VertexHandle> &AbstractTriMesh::garbage_vertices() const
{
    return garbage_vertices_;
}

const std::set<AbstractTriMesh::EdgeHandle> &AbstractTriMesh::garbage_edges() const
{
    return garbage_edges_;
}

const std::set<AbstractTriMesh::FaceHandle> &AbstractTriMesh::garbage_faces() const
{
    return garbage_faces_;
}

void AbstractTriMesh::set_status(VertexHandle vh, const VertexStatus& status)
{
    v_status_[vh.idx_] = status;
}

void AbstractTriMesh::set_status(FaceHandle fh,   const FaceStatus&  status )
{
    f_status_[fh.idx_] = status;
}

void AbstractTriMesh::update_bounding_box()
{
    AABBox b;
    for (VertexIter i = vertices_begin(); i != vertices_end(); ++i) {
        Point p = point(*i);
        b.Enlarge(p);
    }

    bounding_box_ = b;
    dirty_bbox_ = false;
}

void AbstractTriMesh::update_bounding_box(const AABBox &box)
{
    bounding_box_ = box;
    dirty_bbox_ = false;
}

void AbstractTriMesh::delete_vertex(VertexHandle v_h, bool delete_isolated_vertices) {
    // delete adjacent faces
    std::list<unsigned int> &list_face = v_status_[v_h.idx_].adjacent_faces_;
    while(!list_face.empty()) { // do not use iterator since they can be invalidated ...
        //        std::cout << "delete face " << list_face.size() << "::" << list_face.back() << std::endl;
        delete_face(FaceHandle(list_face.back()), delete_isolated_vertices);
    }

    // mark vertex as destroyed
    v_status_[v_h.idx_].destroy();
    garbage_vertices_.insert(v_h);
    has_garbage_ = true;
}

AbstractTriMesh::EdgeHandle AbstractTriMesh::get_face_edge(FaceHandle fh, VertexHandle v0, VertexHandle v1)
{
    for(int& e : f_status_[fh.idx()].adjacent_edges_)
    {
        if(((edges_[e].idx_[0] == (uint)v0.idx()) && (edges_[e].idx_[1] == (uint)v1.idx()))
                || ((edges_[e].idx_[0] == (uint)v1.idx()) && (edges_[e].idx_[1] == (uint)v0.idx())))
        {
            return EdgeHandle(e);
        }
    }

    ork::Logger::ERROR_LOGGER->logf("MESHDEBUG", "Non valid edge index ! face: %d, edge: %d-%d", fh.idx(), v0.idx(), v1.idx());

    return EdgeHandle(-1);
}

std::pair<AbstractTriMesh::FaceHandle, AbstractTriMesh::FaceHandle> AbstractTriMesh::add_quad_face(VertexHandle v1, VertexHandle v2, VertexHandle v3, VertexHandle v4)
{
    FaceHandle f1 = add_face(v1, v2, v3);
    FaceHandle f2 = add_face(v1, v3, v4);

    return std::make_pair(f1, f2);
}

AbstractTriMesh::FaceHandle AbstractTriMesh::add_face(const Face& f) {
    FaceHandle F = add_face(f.idx_[0], f.idx_[1], f.idx_[2]);

    return F;
    //    return add_face(f.idx_[0], f.idx_[1], f.idx_[2]);
}

AbstractTriMesh::EdgeHandle AbstractTriMesh::add_edge(VertexHandle v1, VertexHandle v2) {
    edges_.push_back(Edge(v1.idx_,v2.idx_));

    n_edges_ = edges_.size();
    int idx = (uint) n_edges_ - 1;

    v_status_[v1.idx_].add_an_edge(idx);
    v_status_[v2.idx_].add_an_edge(idx);

    e_status_.resize(n_edges_);


    return EdgeHandle(idx);
}

AbstractTriMesh::EdgeHandle AbstractTriMesh::add_edge(const Edge& e)
{
    return add_edge(e.idx_[0], e.idx_[1]);
}

Scalar AbstractTriMesh::length(const EdgeHandle eh) const
{
    return (point(edge(eh).idx_[0]) - point(edge(eh).idx_[1])).norm();
}

void AbstractTriMesh::delete_face(FaceHandle f_h, bool delete_isolated_vertices, bool delete_isolated_edges)
{

    if (octree_ != nullptr) {
        octree_->remove(octree_cells_[f_h.idx_]);
    }

    //        const Face& face = faces_[f_h.idx_];
    // vertex status update
    for(int i = 0; i < 3; i++)
    {
        // face adjacency update
        v_status_[faces_[f_h.idx_].idx_[i]].remove_a_face(f_h.idx_, delete_isolated_vertices);

        // edge adjacency update
        for(int ei : f_status_[f_h.idx()].adjacent_edges_)
            v_status_[faces_[f_h.idx_].idx_[i]].remove_an_edge(ei);
    }

    // edge status update
    for(auto& e :f_status_[f_h.idx()].adjacent_edges_)
    {
        e_status_[e].remove_a_face(f_h.idx_, delete_isolated_edges);
    }

    // mark faces as destroyed
    f_status_[f_h.idx_].destroy();

    f_status_[f_h.idx_].adjacent_edges_.clear();
    garbage_faces_.insert(f_h);
    has_garbage_ = true;
}

void AbstractTriMesh::delete_edge(EdgeHandle eh)
{
    v_status_[edge(eh).idx_[0]].adjacent_edges_.remove(eh.idx());
    v_status_[edge(eh).idx_[1]].adjacent_edges_.remove(eh.idx());

    for(int f : e_status_[eh.idx()].adjacent_faces_)
    {
        f_status_[f].adjacent_edges_.remove(eh.idx());
    }

    e_status_[eh.idx()].destroy();
    garbage_edges_.insert(eh);
    has_garbage_ = true;
}

//// Operators
//AbstractTriMeshT<VERTEX> &AbstractTriMesh::operator=(const AbstractTriMeshT &rhs)
//{
//    // Vertex Data
////        vertices_ = rhs.vertices_;
////        normals_  = rhs.normals_;
////        colors_   = rhs.colors_;
//    v_status_ = rhs.v_status_;

//    // Face Data
//    faces_    = rhs.faces_;
//    f_status_ = rhs.f_status_;

////        // Other Data
////        n_vertices_ = rhs.n_vertices_;
////        n_faces_    = rhs.n_faces_;

//    octree_ = rhs.octree_;
//    octree_cells_ = rhs.octree_cells_;

//    return *this;
//}

void AbstractTriMesh::lock()
{
    //    mutex().lock();
    std::lock_guard<std::mutex> mlock(mutex_);
    locked_ = true;
}

void AbstractTriMesh::unlock()
{
    /*if (octree_ != nullptr) {
        octree_->Rebuild();
    }*/
    //    mutex().unlock();
    std::lock_guard<std::mutex> mlock(mutex_);
    locked_ = false;
}

void AbstractTriMesh::lockBuffers()
{
    get_buffers_mutex().lock();
}

void AbstractTriMesh::unlockBuffers()
{
    get_buffers_mutex().unlock();
}

bool AbstractTriMesh::tryLockBuffers()
{
    bool res = get_buffers_mutex().try_lock();
    //    locked_ = true;
    return res;
}

Scalar AbstractTriMesh::FindIntersection(const BasicRay& ray,const Scalar dist_max) const {
    FaceHandle fh(-1);
    return FindIntersection(ray, dist_max, fh);
}

Scalar AbstractTriMesh::FindIntersection(const BasicRay& ray,const Scalar dist_max, FaceHandle& fh) const {
    Scalar dist = dist_max;

    {
        //        std::lock_guard<std::mutex> mlock(mutex_);
        if (locked_) {
            return dist;
        }
    }

    if (octree_ != nullptr) {
        dist = octree_->FindIntersection(ray, dist_max);
        return dist;
    }

    // First check if bounding box is intersecting the ray. Otherwise, it's useless to actually compare to every faces.
    if (!ray.IntersectsBoundingBox(bounding_box(), dist)) {
        return dist_max;
    }

    dist = dist_max;

    std::lock_guard<std::mutex> mlock(mutex_);
    Point vertex1, vertex2, vertex3;

    for (ConstFaceIter f_it = faces_begin(); f_it != faces_end(); ++f_it) {

        if(!f_it->is_valid() || status(*f_it).deleted()) {
            continue;
        }

        // first test if p is closer to this face than to the previous
        ConstFaceVertexIter fv_it = cfv_begin(*f_it);

        vertex1 = point(*fv_it); ++fv_it;
        vertex2 = point(*fv_it); ++fv_it;
        vertex3 = point(*fv_it);


        // trans_ray direction should be normalized
        Vector u = vertex2 - vertex1;
        Vector v = vertex3 - vertex1;

        Scalar delta = -(u.cross(v)).dot(ray.direction());
        if( delta != 0.0  )
        {
            Vector w = ray.starting_point() - vertex1;
            Scalar a = - (w.cross(v)).dot(ray.direction()) / delta;
            Scalar b = - (u.cross(w)).dot(ray.direction()) / delta;
            Scalar t = (u.cross(v)).dot(w) / delta;
            if(t>0.0 && a >=0.0 && b>=0.0 && a+b<=1
                    && t<dist)
            {
                fh = *f_it;
                dist = t;
            }
        }
    }

    return dist;
}

std::shared_ptr<AbstractTriMesh> AbstractTriMesh::Subdivide() const
{
    std::shared_ptr<AbstractTriMesh> output = this->clone(false);

    std::map<VertexHandle, VertexHandle> v_vert;
    for(ConstVertexIter v_it = vertices_begin(); v_it != vertices_end(); v_it++)
    {
        VertexHandle vh = output->add_point(point(*v_it), normal(*v_it), color(*v_it));
        v_vert[*v_it] = vh;
    }

    std::map<EdgeHandle, VertexHandle> e_vert;
    for(ConstEdgeIter e_it = edges_begin(); e_it != edges_end(); e_it++)
    {
        VertexHandle vh0 = edge(*e_it).idx_[0];
        VertexHandle vh1 = edge(*e_it).idx_[1];

        Point p = 0.5 * (point(vh0) + point(vh1));
        Normal n = (normal(vh0) + normal(vh1)).normalized();
        Color c = Color::interpolate(color(vh0), color(vh1));

        VertexHandle vh = output->add_point(p, n, c);
        e_vert[*e_it] = vh;
    }

    for(ConstFaceIter f_it = faces_begin(); f_it != faces_end(); f_it++)
    {
        VertexHandle v0(face(*f_it).idx_[0]);
        VertexHandle v1(face(*f_it).idx_[1]);
        VertexHandle v2(face(*f_it).idx_[2]);

        std::list<int>::const_iterator it = status(*f_it).adjacent_edges_.begin();
        VertexHandle e0 = e_vert[*it];
        it++;
        VertexHandle e1 = e_vert[*it];
        it++;
        VertexHandle e2 = e_vert[*it];

        output->add_face(v0, e0, e2);
        output->add_face(v1, e1, e0);
        output->add_face(v2, e2, e1);
        output->add_face(e0, e1, e2);
    }

    return output;
}

bool AbstractTriMesh::SaveToObjFile(const std::string &filename) const
{
    if (locked_) {
        ork::Logger::ERROR_LOGGER->logf("CORE", "Error while exporting mesh : %s", "Mesh is not ready yet. Try again when it's ready.");
        return false;
    }

    //TODO:@todo : will not work properly if garbage collection has not been run ...
    if (has_garbage_)
        //    if(     n_vertices_ != vertices_.size()
        //            || n_vertices_ != colors_.size()
        //            || n_vertices_ != normals_.size()
        //            || n_vertices_ != v_status_.size()
        //            || n_faces_ != faces_.size()
        //            || n_faces_ != f_status_.size() )
    {
        ork::Logger::ERROR_LOGGER->logf("CORE", "Error while exporting mesh : %s", "Invalid mesh size : mesh is probably corrupted.");
    }

    // run garbage collection in order to have just the required data
    // and simply interate over the vector : problem : would invalidate stored vertex handle...

    // Open stream ...
    std::ofstream file_stream(filename);
    if(!file_stream.is_open())
    {
        return false;
    }

    for(unsigned int index = 0; index < n_vertices(); ++index)
    {
        AbstractTriMesh::VertexHandle vh = vertex_handle(index);
        const Point &p = this->point(vh);
        const Vector &n = this->normal(vh);
        file_stream << "v ";

        if(!(std::isfinite(p[0]) && std::isfinite(p[1]) && std::isfinite(p[2]))) {
            const VertexStatus& v_stat = v_status_[index]; //only true if their is no garbage in the mesh ...
            Vector new_pos = Vector::Zero();
            if(v_stat.nb_face_for_vertex_ != 0) {
                ork::Logger::ERROR_LOGGER->logf("CORE", "Error while exporting mesh : %s", "Position is broken : temporary solution");

                Vector new_pos = Vector::Zero();
                unsigned nb_point_used = 0;
                for(int f_id : v_stat.adjacent_faces_) {
                    const Vector& tmp_p0 = this->point(this->faces_[f_id].idx_[0]);
                    if((std::isfinite(tmp_p0[0]) &&  std::isfinite(tmp_p0[1]) &&  std::isfinite(tmp_p0[2])))
                    {
                        new_pos += tmp_p0;
                        ++nb_point_used;
                    }
                    const Vector& tmp_p1 = this->point(this->faces_[f_id].idx_[1]);
                    if((std::isfinite(tmp_p1[0]) &&  std::isfinite(tmp_p1[1]) &&  std::isfinite(tmp_p1[2])))
                    {
                        new_pos += tmp_p1;
                        ++nb_point_used;
                    }
                    const Vector& tmp_p2 = this->point(this->faces_[f_id].idx_[2]);
                    if((std::isfinite(tmp_p2[0]) &&  std::isfinite(tmp_p2[1]) &&  std::isfinite(tmp_p2[2])))
                    {
                        new_pos += tmp_p2;
                        ++nb_point_used;
                    }
                }
                new_pos *= 1.0/((Scalar) nb_point_used) ;
                if(!(std::isfinite(new_pos[0]) &&  std::isfinite(new_pos[1]) &&  std::isfinite(new_pos[2]))) {
                    ork::Logger::ERROR_LOGGER->logf("CORE", "Error while exporting mesh : %s", "Position is broken : infinite coordinate.");
                    new_pos = Vector::Zero();
                }
            }
            for (unsigned int i = 0; i < Point::RowsAtCompileTime; ++i) {
                file_stream << new_pos[i] << " ";
            }
        } else {
            for (unsigned int i = 0; i < Point::RowsAtCompileTime; ++i) {
                file_stream << p[i] << " ";
            }
        }

        file_stream << std::endl;
        file_stream << "vn ";
        if(!(std::isfinite(n[0]) &&  std::isfinite(n[1]) &&  std::isfinite(n[2]))) {
            //TODO:@todo : this is a temporary solution => find the source of the problem
            Vector new_normal = Vector::Zero();
            const VertexStatus& v_stat = v_status_[index]; //only true if their is no garbage in the mesh ...
            if(v_stat.nb_face_for_vertex_ != 0) {
                ork::Logger::ERROR_LOGGER->logf("CORE", "Error while exporting mesh : %s", "Normal is broken : temporary solution");

                for(int f_id : v_stat.adjacent_faces_) {
                    const Vector& tmp_n0 = this->normal(this->faces_[f_id].idx_[0]);
                    if((std::isfinite(tmp_n0[0]) &&  std::isfinite(tmp_n0[1]) &&  std::isfinite(tmp_n0[2])))
                    {
                        ork::Logger::ERROR_LOGGER->logf("MESHDEBUG", "%f %f %f", tmp_n0[0], tmp_n0[1], tmp_n0[2]);
                        new_normal += tmp_n0;
                    }
                    const Vector& tmp_n1 = this->normal(this->faces_[f_id].idx_[1]);
                    if((std::isfinite(tmp_n1[0]) &&  std::isfinite(tmp_n1[1]) &&  std::isfinite(tmp_n1[2])))
                    {
                        ork::Logger::ERROR_LOGGER->logf("MESHDEBUG", "%f %f %f", tmp_n1[0], tmp_n1[1], tmp_n1[2]);
                        new_normal += tmp_n1;
                    }
                    const Vector& tmp_n2 = this->normal(this->faces_[f_id].idx_[2]);
                    if((std::isfinite(tmp_n2[0]) &&  std::isfinite(tmp_n2[1]) &&  std::isfinite(tmp_n2[2])))
                    {
                        ork::Logger::ERROR_LOGGER->logf("MESHDEBUG", "%f %f %f", tmp_n2[0], tmp_n2[1], tmp_n2[2]);
                        new_normal += tmp_n2;
                    }
                }
                new_normal.normalize();
                if(!(std::isfinite(n[0]) &&  std::isfinite(n[1]) &&  std::isfinite(n[2]))) {
                    ork::Logger::ERROR_LOGGER->logf("CORE", "Error while exporting mesh : %s", "Normal is broken : infinite coordinate.");

                    new_normal = Vector::Constant(1.0);
                }
            } else {
                new_normal = Vector::Constant(1.0);
            }
            for (unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i) {
                file_stream << new_normal[i] << " ";
            }
        } else {
            for (unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i) {
                file_stream << n[i] << " ";
            }
        }
        file_stream << std::endl;
    }

    for(unsigned int index = 0; index < n_faces(); ++index)
    {

        AbstractTriMesh::FaceHandle fh = face_handle(index);
        if(!status(fh).deleted())
        {
            const AbstractTriMesh::Face& face = this->face(fh);
            file_stream << "f " << face.idx_[0]+1 << "//" << face.idx_[0]+1
                        <<  " " << face.idx_[1]+1 << "//" << face.idx_[1]+1
                         <<  " " << face.idx_[2]+1 << "//" << face.idx_[2]+1 << std::endl;
        }
    }
    file_stream.close();
    return true;
}

bool AbstractTriMesh::LoadFromObjFilePrivate(std::ifstream &file_stream)
{
    std::vector<Point> points;
    std::vector<Vector> normals;
    std::vector<Vector2> uvs;
    std::vector<int> face_point_indices;
    std::vector<int> face_normal_indices;

    std::string line;
    double x, y, z;
    unsigned int v1 = 0, vn1 = 0;
    unsigned int v2 = 0, vn2 = 0;
    unsigned int v3 = 0, vn3 = 0;
    unsigned int cpt = 0;
    unsigned int res = 0;
    bool error = false;

    while(getline(file_stream, line)) {
        if (line.size() == 0) {
            continue;
        }
        char c = line.at(0);
        char c2;
        switch (c) {
        case 'v':
            c2 = line.at(1);
            switch(c2) {
            case ' ':
                sscanf(line.c_str(), "v %lf %lf %lf", &x, &y, &z);
                points.push_back(Point(x, y, z));
                break;
            case 't':
                sscanf(line.c_str(), "vt %lf %lf", &x, &y);
                uvs.push_back(Vector2(x, y));
                break;
            case 'n':
                sscanf(line.c_str(), "vn %lf %lf %lf", &x, &y, &z);
                normals.push_back(Vector(x, y, z));
                break;
            default:
                break;
            }

            break;
        case 'f':
            cpt = (uint)std::count(line.begin(), line.end(), '/');
            if (cpt >= 3) {
                if (line.find("//") != std::string::npos) {
                    res = sscanf(line.c_str(), "f %d//%d %d//%d %d//%d", &v1, &vn1, &v2, &vn2, &v3, &vn3);
                    if (res != 6) {
                        error = true;
                    }
                } else if (cpt == 6){
                    res = sscanf(line.c_str(), "f %d/%*d/%d %d/%*d/%d %d/%*d/%d", &v1, &vn1, &v2, &vn2, &v3, &vn3);
                    if (res != 6) {
                        error = true;
                    }
                } else if (cpt == 3) {
                    res = sscanf(line.c_str(), "f %d/%d %d/%d %d/%d", &v1, &vn1, &v2, &vn2, &v3, &vn3);
                    if (res != 6) {
                        error = true;
                    }
                }
                if (error) {
                    ork::Logger::ERROR_LOGGER->log("SERIALIZABLE", "Error loading mesh : file is badly constructed.");
                    assert(false);
                    return false;
                }
                face_point_indices.push_back(v1);
                face_point_indices.push_back(v2);
                face_point_indices.push_back(v3);
                face_normal_indices.push_back(vn1);
                face_normal_indices.push_back(vn2);
                face_normal_indices.push_back(vn3);
            } else {
                res = sscanf(line.c_str(), "f %d %d %d", &v1, &v2, &v3);
                if (res != 3) {
                    ork::Logger::ERROR_LOGGER->log("SERIALIZABLE", "Error loading mesh : file is badly constructed.");
                    assert(false);
                    return false;
                }
                face_point_indices.push_back(v1);
                face_point_indices.push_back(v2);
                face_point_indices.push_back(v3);
            }
            break;
        case 'o':
        case 'g':
        case 'u':
        case '#':
        default:
            break;
        }
    }
    if (points.size() == 0 || face_point_indices.size() == 0) {
        ork::Logger::ERROR_LOGGER->log("SERIALIZABLE", "Error loading mesh : file is empty.");
        return false;
    }

    //    if ((normals.size() == points.size() || normals.size() == 0))
    {// && (uvs.size() == points.size () || uvs.size() == 0)) {
        std::vector<VertexHandle> handles;
        for (unsigned int i = 0; i < points.size(); ++i) {
            handles.push_back(add_point(points[i]));
        }

        for (unsigned int i = 0; i < face_point_indices.size(); i += 3) {
            v1 = face_point_indices[i] - 1;
            v2 = face_point_indices[i+1] - 1;
            v3 = face_point_indices[i+2] - 1;
            if (v1 > handles.size() || v2 > handles.size() || v3 > handles.size()) {
                ork::Logger::ERROR_LOGGER->logf("SERIALIZABLE", "Error loading mesh : file is badly constructed : issues in face indices [%d:%d:%d].", v1, v2, v3);
                return false;
            }
            add_face(handles[v1], handles[v2], handles[v3]);
        }

        if (normals.size() == points.size())
            for (unsigned int i = 0; i < normals.size(); ++i) {
                set_normal(handles[i], normals[i]);
            }
        else
            ComputeNormals();

        if (uvs.size() == points.size())
            for (unsigned int i = 0; i < uvs.size(); ++i) {
                set_uv(handles[i], uvs[i]);
            }



    } /*else { // case where every vertex must be duplicated (A single vertex may have different normals/uv textures on each face).
        // Since I didn't find any detail on how uvs could be defined the same way as normals, we only handle different normals so far.
        if (face_normal_indices.size() == 0) {
            ork::Logger::ERROR_LOGGER->logf("SERIALIZABLE", "Error loading mesh : file is badly constructed : mismatch between index & normal counts, and no normals defined for faces.");
            return false;
        }
        for (unsigned int i = 0; i < face_point_indices.size(); i += 3) {
            v1 = face_point_indices[i] - 1;
            v2 = face_point_indices[i+1] - 1;
            v3 = face_point_indices[i+2] - 1;
            if (v1 > points.size() || v2 > points.size() || v3 > points.size()) {
                ork::Logger::ERROR_LOGGER->logf("SERIALIZABLE", "Error loading mesh : file is badly constructed : issues in face indices [%d:%d:%d].", v1, v2, v3);
                return false;
            }
            VertexHandle vh1 = add_point(points[v1]);
            VertexHandle vh2 = add_point(points[v2]);
            VertexHandle vh3 = add_point(points[v3]);
            set_normal(vh1, normals[face_normal_indices[i] - 1]);
            set_normal(vh2, normals[face_normal_indices[i+1] - 1]);
            set_normal(vh3, normals[face_normal_indices[i+2] - 1]);
            if (uvs.size() > 0) {
                set_uv(vh1, uvs[v1]);
                set_uv(vh2, uvs[v2]);
                set_uv(vh3, uvs[v3]);
            }
            add_face(vh1,  vh2, vh3);
        }
    }*/

    /*if (octree_ != nullptr) {
        octree_->Rebuild();
    }*/

    return true;
}

bool AbstractTriMesh::LoadFromObjFile(const std::string &filename)
{
    this->clear();
    //    // Open stream ...
    std::ifstream file_stream(filename);
    if(!file_stream.is_open()) {
        return false;
    }
    bool res = LoadFromObjFilePrivate(file_stream);
    file_stream.close();
    return res;
}

Scalar AbstractTriMesh::GetFaceArea(const FaceHandle fh) const
{
    Point vertex0 = point(VertexHandle(face(fh).idx_[0]));
    Point vertex1 = point(VertexHandle(face(fh).idx_[1]));
    Point vertex2 = point(VertexHandle(face(fh).idx_[2]));

    Scalar area = ((vertex1 - vertex0).cross(vertex2 - vertex0)).norm();

    return area;
}

Point AbstractTriMesh::GetFaceCentroid(const FaceHandle fh) const
{
    Point vertex0 = point(VertexHandle(face(fh).idx_[0]));
    Point vertex1 = point(VertexHandle(face(fh).idx_[1]));
    Point vertex2 = point(VertexHandle(face(fh).idx_[2]));

    return (vertex0 + vertex1 + vertex2) / 3.0f;
}

std::map<AbstractTriMesh::FaceHandle, Normal> AbstractTriMesh::GetFacesNormals() const
{
    std::map<FaceHandle, Normal> output;


    for(ConstFaceIter i = faces_begin(); i != faces_end(); ++i)
    {
        //        ConstFaceVertexIter j = cfv_begin(*i);

        //        //this line can return seg fault
        //        Point vertex0 = point(*j);
        //        ++j;

        //        Point vertex1 = point(*j);
        //        ++j;

        //        Point vertex2 = point(*j);
        //        ++j;

        //        try
        //        {
        //            output.insert(std::pair<FaceHandle, Normal>(*i,((vertex1-vertex0).normalized().cross((vertex2-vertex0).normalized())).normalized()));
        //        }
        //        catch(std::exception e)
        //        {
        //            ork::Logger::LOG_DEBUG("Mesh", "Degenerated polygon failed normal computation !");
        //            output[*i] = Normal(0,0,0);
        //        }
        output[*i] = GetFaceNormal(*i);
    }

    return output;
}

Normal AbstractTriMesh::GetFaceNormal(const FaceHandle fh) const
{
    Normal output (0,0,0);

    if(!fh.is_valid())
    {
        ork::Logger::ERROR_LOGGER->log("MESHDEBUG", "AbstractTriMesh::GetFaceNormal : non valid face handle !");
    }

    //    cout << face(fh).idx_[0] << " " << face(fh).idx_[1] << " " << face(fh).idx_[2] << endl;
    Point vertex0 = point(VertexHandle(face(fh).idx_[0]));
    Point vertex1 = point(VertexHandle(face(fh).idx_[1]));
    Point vertex2 = point(VertexHandle(face(fh).idx_[2]));

    output = ((vertex1-vertex0).normalized().cross((vertex2-vertex0).normalized())).normalized();

#ifndef NDEBUG
    if (std::isnan(output[0]))
    {
        ork::Logger::LOG_DEBUG("Mesh", "Degenerated polygon failed normal computation !");
    }
#endif

    return output;
}

////Normal AbstractTriMesh::GetFaceNormal(const FaceHandle fh) const
//{
//    ConstFaceVertexIter j = cfv_begin(fh);

//    //this line can return seg fault
//    Point vertex0 = point(*j);
//    ++j;

//    Point vertex1 = point(*j);
//    ++j;

//    Point vertex2 = point(*j);
//    ++j;

//    Normal output;

//    try
//    {
//        output = ((vertex1-vertex0).normalized().cross((vertex2-vertex0).normalized())).normalized();
//    }
//    catch(std::exception e)
//    {
//        ork::Logger::LOG_DEBUG("Mesh", "Degenerated polygon failed normal computation !");
//        output = Normal(0,0,0);
//    }

//    return output;
//}

std::map<AbstractTriMesh::VertexHandle, std::set<AbstractTriMesh::FaceHandle> > AbstractTriMesh::GetVerticesFaces() const
{
    std::map<VertexHandle, std::set<FaceHandle> > output;

    //    for(ConstVertexIter i = vertices_begin(); i != vertices_end(); ++i)
    //    {
    //        output.insert(std::pair<VertexHandle, std::set<FaceHandle> >(*i, std::set<FaceHandle>()));
    //    }

    //    for(ConstFaceIter i = faces_begin(); i != faces_end(); ++i)
    //    {
    //        ConstFaceVertexIter j = cfv_begin(*i);

    //        output[*j].insert(*i);
    //        ++j;
    //        output[*j].insert(*i);
    //        ++j;
    //        output[*j].insert(*i);
    //    }
    for(ConstVertexIter i = vertices_begin(); i != vertices_end(); ++i)
    {
        output[*i] = GetVertexFaces(*i);
    }

    return output;
}

// WARNING : do not use for serialization
std::set<AbstractTriMesh::FaceHandle> AbstractTriMesh::GetVertexFaces(const VertexHandle vh) const
{
    std::set<FaceHandle> output;

    // // WARNING : do not use for serialization
    //    for(ConstFaceIter i = faces_begin(); i != faces_end(); ++i)
    //    {
    //        ConstFaceVertexIter j = cfv_begin(*i);

    //        if(*j == vh)
    //            output.insert(*i);
    //        ++j;

    //        if(*j == vh)
    //            output.insert(*i);
    //        ++j;

    //        if(*j == vh)
    //            output.insert(*i);
    //    }

    for(ConstVertexFaceIter i = cvf_begin(vh); i.is_valid(); ++i)
    {
        output.insert(*i);
    }

    return output;
}

// WARNING : do not use for serialization
std::vector<AbstractTriMesh::FaceHandle> AbstractTriMesh::GetVertexFacesOrdered(const VertexHandle vh) const
{
    std::set<FaceHandle> f_surrounding =  GetVertexFaces(vh);
    std::vector<FaceHandle> f_surrounding_sorted;
    f_surrounding_sorted.reserve(f_surrounding.size());

    std::set<FaceHandle>::iterator it = f_surrounding.begin();
    f_surrounding_sorted.push_back(*it);
    f_surrounding.erase(it);

    // while there is faces to be processed
    while(f_surrounding.size() > 0)
    {
        // the current face
        FaceHandle cur_face = f_surrounding_sorted.back();

        // for all unprocessed faces
        it = f_surrounding.begin();
        while(it != f_surrounding.end())
        {
            // detect common vertices (for adjacency)
            // Warning : this do not enforce fan direction
            std::set<VertexHandle> com_vert = CommonVertices(cur_face, *it);

            // if faces have 2 common vertices, they share an edge -> they are subsequent in the fan
            if(com_vert.size() == 2
                    )
                break;

            it++;
        }

        if(it == f_surrounding.end())
        {
            std::cerr << "Warning in face star computation : no subsequent face found." << std::endl;
            break;
        }

        f_surrounding_sorted.push_back(*it);
        f_surrounding.erase(it);
    }

    return f_surrounding_sorted;
}

std::map<AbstractTriMesh::VertexHandle, std::set<AbstractTriMesh::VertexHandle> > AbstractTriMesh::GetVerticesStars() const
{
    std::map<VertexHandle, std::set<FaceHandle> > vertice_face = GetVerticesFaces();

    std::map<VertexHandle, std::set<VertexHandle> > output;

    for(ConstVertexIter i = vertices_begin(); i != vertices_end(); ++i)
    {
        output.insert(std::pair<VertexHandle, std::set<VertexHandle> >(*i, std::set<VertexHandle>()));

        for (auto j = vertice_face[*i].cbegin(); j != vertice_face[*i].cend(); j++)
        {
            ConstFaceVertexIter k = cfv_begin(*j);

            if(!(*k == *i))
                output[*i].insert(*k);
            ++k;

            if(!(*k == *i))
                output[*i].insert(*k);
            ++k;

            if(!(*k == *i))
                output[*i].insert(*k);
        }
    }

    return output;
}

std::set<AbstractTriMesh::VertexHandle> AbstractTriMesh::GetVertexStarUnordered(const VertexHandle vh) const
{
    std::set<VertexHandle> output;

    if(!vh.is_valid())
        return output;

    std::set<FaceHandle> faces = GetVertexFaces(vh);

    for(auto& f : faces)
    {
        if(!f.is_valid())
            continue;

        for(auto& v : this->face(f).idx_)
        {
            if(v == (unsigned int) -1 || v == vh.idx_)
                continue;

            output.insert(VertexHandle(v));
        }
    }

    return output;
}

std::vector<AbstractTriMesh::VertexHandle> AbstractTriMesh::GetVertexStar(const VertexHandle vh) const
{
    /*
    std::vector<VertexHandle> output;
    for(int i : v_status_[vh.idx_].adjacent_edges_)
    {
        int i0 = edges_[i].idx_[0];
        if(i0 == vh.idx_)
            i0 = edges_[i].idx_[1];
        output.push_back(VertexHandle(i0));
    }
    return output;

/*/




    //    set<FaceHandle> vertice_face = GetVertexFaces(vh);

    //    set<VertexHandle> output;

    //    for (auto j = vertice_face.cbegin(); j != vertice_face.cend(); j++)
    //    {
    //        ConstFaceVertexIter k = cfv_begin(*j);

    //        if(!(*k == vh))
    //            output.insert(*k);
    //        ++k;

    //        if(!(*k == vh))
    //            output.insert(*k);
    //        ++k;

    //        if(!(*k == vh))
    //            output.insert(*k);
    //    }

    //    return output;


    std::vector<VertexHandle> output;
    if(!vh.is_valid()) {
        return output;
    }

    std::set<FaceHandle> vf = GetVertexFaces(vh);

    FaceHandle cur_face = *vf.begin();
    VertexHandle cur_vert = GetPrevVertex(vh, cur_face);
    if(!cur_vert.is_valid()) {
        return output;
    }

    output.push_back(cur_vert);

    vf.erase(vf.begin());

    while(vf.size() > 0)
    {
        if(!cur_face.is_valid())
        {
            ork::Logger::ERROR_LOGGER->log("MESHDEBUG", "AbstractTriMesh::GetVertexStar : Warning : face not valid ! ");
            continue;
        }

        if(!cur_vert.is_valid() || (cur_vert == *output.begin() && output.size() > 1))
        {
            break;
        }
        cur_face = GetOppositeFace(cur_face, vh, cur_vert);

        //cur_vert = GetNextVertex(vh, cur_face);
        cur_vert = GetPrevVertex(vh, cur_face);

        if(vf.find(cur_face) == vf.end())
            break;

        output.push_back(cur_vert);

        vf.erase(cur_face);
    }

    return output;


    //*/
}

std::set<AbstractTriMesh::FaceHandle> AbstractTriMesh::GetFaceSurrounding(const FaceHandle fh) const
{
    std::set<FaceHandle> output;

    for(int vi : face(fh).idx_)
    {
        VertexHandle vh(vi);
        for(auto& cur_fh : GetVertexFaces(vh))
        {
            if(cur_fh.idx_ != fh.idx_)
                output.insert(cur_fh);
        }
    }

    return output;
}

AbstractTriMesh::VertexHandle AbstractTriMesh::GetNextVertex(AbstractTriMesh::VertexHandle vh, AbstractTriMesh::FaceHandle fh) const
{
    if(fh == -1)
        return -1;

    Face f = this->face(fh);

    if(vh.idx() == f.idx_[0])
    {
        return VertexHandle(f.idx_[1]);
    }
    if(vh.idx() == f.idx_[1])
    {
        return VertexHandle(f.idx_[2]);
    }
    if(vh.idx() == f.idx_[2])
    {
        return VertexHandle(f.idx_[0]);
    }


    ork::Logger::ERROR_LOGGER->logf("MESHDEBUG", "AbstractTriMesh::GetNextVertex : vertex %d not found on face %d", vh.idx(), fh.idx());
    return VertexHandle(-1);
}

AbstractTriMesh::VertexHandle AbstractTriMesh::GetPrevVertex(AbstractTriMesh::VertexHandle vh, AbstractTriMesh::FaceHandle fh) const
{
    if(fh == -1)
        return -1;

    Face f = this->face(fh);

    if(vh.idx() == f.idx_[0])
    {
        return VertexHandle(f.idx_[2]);
    }
    if(vh.idx() == f.idx_[1])
    {
        return VertexHandle(f.idx_[0]);
    }
    if(vh.idx() == f.idx_[2])
    {
        return VertexHandle(f.idx_[1]);
    }

    ork::Logger::ERROR_LOGGER->logf("MESHDEBUG", "AbstractTriMesh::GetPrevVertex : vertex %d not found on face %d", vh.idx(), fh.idx());
    return VertexHandle(-1);
}

AbstractTriMesh::FaceHandle AbstractTriMesh::GetOppositeFace(AbstractTriMesh::FaceHandle fh, AbstractTriMesh::VertexHandle vh0, AbstractTriMesh::VertexHandle vh1) const
{
    std::set<FaceHandle> common = CommonFaces(vh0, vh1);

    // the vertices have no face in common
    if(common.size() == 0)
    {
        ork::Logger::ERROR_LOGGER->logf("MESHDEBUGADVANCED", "AbstractTriMesh::GetOppositeFace : inconsistent edge %d-%d", vh0.idx(), vh1.idx());
        return FaceHandle(-1);
    }

    if(common.size() == 1)
    {
        ork::Logger::ERROR_LOGGER->logf("MESHDEBUGADVANCED", "AbstractTriMesh::GetOppositeFace : border edge %d-%d", vh0.idx(), vh1.idx());
        return FaceHandle(-1);
    }

    if(common.size() > 2)
    {
        ork::Logger::ERROR_LOGGER->logf("MESHDEBUGADVANCED", "AbstractTriMesh::GetOppositeFace : Non manifold edge %d-%d. %d indicent faces", vh0.idx(), vh1.idx(), common.size());
        return FaceHandle(-1);
    }

    FaceHandle f0 = *common.begin();
    FaceHandle f1 = *(++common.begin());

    if(fh ==f0)
        return f1;

    //    if(fh ==f1)
    return f0;
}

AbstractTriMesh::FaceHandle AbstractTriMesh::GetOppositeFace(AbstractTriMesh::FaceHandle fh, AbstractTriMesh::EdgeHandle eh) const
{
    // the vertices have no face in common
    if(e_status_[eh.idx()].adjacent_faces_.size() == 0)
    {
        std::cerr << "AbstractTriMesh::GetOppositeFace : edge #" << eh.idx() << " is solitary." << std::endl;
        return FaceHandle(-1);
    }

    if(e_status_[eh.idx()].adjacent_faces_.size() == 1)
    {
        std::cerr << "AbstractTriMesh::GetOppositeFace : edge #" << eh.idx() << " is on the border." << std::endl;
        return FaceHandle(-1);
    }

    if(e_status_[eh.idx()].adjacent_faces_.size() > 2)
    {
        std::cerr << "AbstractTriMesh::GetOppositeFace : edge #" << eh.idx() << " is non manifold. " << e_status_[eh.idx()].adjacent_faces_.size() << " incident faces." << std::endl;
        return FaceHandle(-1);
    }

    auto it = e_status_[eh.idx()].adjacent_faces_.begin();
    FaceHandle f0 = *it;
    it++;
    FaceHandle f1 = *it;

    if(fh ==f0)
        return f1;

    if(fh ==f1)
        return f0;

    std::cerr << "AbstractTriMesh::GetOppositeFace : edge #" << eh.idx() << " is not adjacent to face #" << fh.idx() << "." << std::endl;
    return FaceHandle(-1);
}

std::map<AbstractTriMesh::VertexHandle, Normal> AbstractTriMesh::GetVerticesNormals() const
{
    //    map<FaceHandle, Normal> normal_polygon = GetFacesNormals(); // const !!
    //    map<VertexHandle, set<FaceHandle> > vertices_faces = GetVerticesFaces();

    std::map<VertexHandle, Normal> output;


    //    for(ConstVertexIter i = vertices_begin(); i != vertices_end(); ++i)
    //    {
    //        Normal temp_normal(0,0,0);

    //        for(auto j = vertices_faces[*i].cbegin(); j != vertices_faces[*i].cend(); ++j) {
    //            temp_normal += normal_polygon[*j];
    //        }

    //        try {
    //            output.insert( pair<VertexHandle, Normal>(*i, temp_normal.normalized()));
    //        }
    //        catch(exception e) {
    //            Logger::LOG_DEBUG("Mesh", "Degenerated polygon failed normal computation !");
    //            output[*i] = Normal(0,0,0);
    //        }
    //    }


    for(ConstVertexIter i = vertices_begin(); i != vertices_end(); ++i)
    {
        output[*i] = GetVertexNormal(*i);
    }


    return output;
}

Normal AbstractTriMesh::GetVertexNormal(const VertexHandle vh) const
{
    Normal output(0,0,0);

    for(ConstVertexFaceIter i = cvf_begin(vh); i.is_valid(); ++i)
    {
        FaceHandle fh = *i;

        Vector p0 = point(vh);
        Vector p1 = point(GetPrevVertex(vh, fh));
        Vector p2 = point(GetNextVertex(vh, fh));

        Vector cr = (p1 - p0).normalized().cross((p2 - p0).normalized());

        Scalar alpha = asin(cr.norm());

        output += GetFaceNormal(fh) * alpha;
    }

    output.normalize();

#ifndef NDEBUG
    if (std::isnan(output[0])) {
        ork::Logger::LOG_DEBUG("Mesh", "Degenerated vertex failed normal computation !");
    }
#endif

    return output;
}

void AbstractTriMesh::ComputeNormals()
{
    //    map<VertexHandle, Normal> vertices_normal = GetVerticesNormals();

    for(ConstVertexIter i = vertices_begin(); i != vertices_end(); ++i)
    {
        //        set_normal(*i, vertices_normal[*i]);
        set_normal(*i, GetVertexNormal(*i));

        //        cout << i->idx_ << "\t"
        //             << point          (*i)[0] << " " << point          (*i)[1] << " " << point          (*i)[2] << "\t"
        //             << vertices_normal[*i][0] << " " << vertices_normal[*i][1] << " " << vertices_normal[*i][2] << endl;

    }

}

AbstractTriMesh& AbstractTriMesh::AddMesh(const AbstractTriMesh& to_add)
{
    uint nb_vert = (uint) n_vertices();

    for(ConstVertexIter i = to_add.vertices_begin(); i != to_add.vertices_end(); ++i) {
        VertexHandle vh = add_point(to_add.point(*i));

        if(to_add.has_vertex_status ())
            set_status(vh, to_add.status(*i));

        if(to_add.has_vertex_normals ())
            set_normal(vh, to_add.normal(*i));

        if(to_add.has_vertex_colors ())
            set_color (vh, to_add.color(*i));
    }


    for(ConstFaceIter i = to_add.faces_begin(); i != to_add.faces_end(); ++i) {
        FaceHandle fh = add_face(to_add.face(*i) + nb_vert);

        if(to_add.has_face_status ())
            set_status(fh, to_add.status(*i));
    }

    return *this;
}


void AbstractTriMesh::FillColor(const Color& c)
{
    for(ConstVertexIter i = vertices_begin(); i != vertices_end(); ++i) {
        set_color(*i, c);
    }
}

void AbstractTriMesh::ColorFromNormal(const Color &Xcolor, const Color &Ycolor, const Color &Zcolor)
{
    for(ConstVertexIter i = vertices_begin(); i != vertices_end(); ++i) {
        //        set_color(*i, Color( // ));
        //                      abs(normal(*i).dot(Vector(1,0,0))),
        //                      abs(normal(*i).dot(Vector(0,1,0))),
        //                      abs(normal(*i).dot(Vector(0,0,1))), 1.0 ));

        Color c (Xcolor * (float)abs(normal(*i).dot(Vector(1,0,0))) +
                 Ycolor * (float)abs(normal(*i).dot(Vector(0,1,0))) +
                 Zcolor * (float)abs(normal(*i).dot(Vector(0,0,1))));
        set_color(*i,  c);
    }
}

void AbstractTriMesh::ColorFromCoord(const Color &Xcolor, const Color &Ycolor, const Color &Zcolor)
{
    AABBox BB = bounding_box();//ComputeBB();
    for(ConstVertexIter i = vertices_begin(); i != vertices_end(); ++i) {
        //        set_color(*i, Color( // ));
        //                      (point(*i).dot(Vector(1,0,0)) - BB.min()[0]) / (BB.max()[0] - BB.min()[0]),
        //                      (point(*i).dot(Vector(0,1,0)) - BB.min()[1]) / (BB.max()[1] - BB.min()[1]),
        //                      (point(*i).dot(Vector(0,0,1)) - BB.min()[2]) / (BB.max()[2] - BB.min()[2]), 1.0));

        Color c ( Xcolor * float((point(*i).dot(Vector(1,0,0)) - BB.min()[0]) / (BB.max()[0] - BB.min()[0])) +
                Ycolor * float((point(*i).dot(Vector(0,1,0)) - BB.min()[1]) / (BB.max()[1] - BB.min()[1])) +
                Zcolor * float((point(*i).dot(Vector(0,0,1)) - BB.min()[2]) / (BB.max()[2] - BB.min()[2])) );

        set_color(*i,  c);
    }
}

AABBox AbstractTriMesh::ComputeBB() const
{
    AABBox aabb;
    if(n_vertices() == 0)
        return aabb;

    for(ConstVertexIter i = vertices_begin(); i != vertices_end(); ++i)
        aabb.Enlarge(point(*i));

    return aabb;
}

Vector AbstractTriMesh::Center() const
{
    AABBox bb = bounding_box();//ComputeBB();
    return (bb.min() + bb.max()) / 2.0;
}

Vector AbstractTriMesh::Centre() const
{
    return Center();
}


double AbstractTriMesh::Radius() const
{
    AABBox bb = ComputeBB();
    return (bb.min() - bb.max()).norm() / sqrt(3.0) / 2.0;
}

Color AbstractTriMesh::MeanColor() const
{
    Color output(0, 0, 0, 0);
    for(ConstVertexIter i = vertices_begin(); i != vertices_end(); ++i) {
        output += color(*i);
    }

    return Color(output / (float)n_vertices());
}

std::set<AbstractTriMesh::FaceHandle> AbstractTriMesh::CommonFaces(const AbstractTriMesh::VertexHandle v0, const AbstractTriMesh::VertexHandle v1)const
{
    std::set<AbstractTriMesh::FaceHandle> f0 = GetVertexFaces(v0);
    std::set<AbstractTriMesh::FaceHandle> f1 = GetVertexFaces(v1);

    std::set<AbstractTriMesh::FaceHandle> output;

    for(auto& i0 : f0)
    {
        for(auto& i1 : f1)
        {
            if(i0 == i1)
            {
                output.insert(i0);
            }
        }
    }

    return output;
}

std::set<AbstractTriMesh::VertexHandle> AbstractTriMesh::CommonVertices(const FaceHandle f0, const FaceHandle f1) const
{
    std::set<AbstractTriMesh::VertexHandle> output;

    for(auto& i0 : faces_[f0.idx_].idx_)
    {
        for(auto& i1 : faces_[f1.idx_].idx_)
        {
            if(i0 == i1)
            {
                output.insert(VertexHandle(i0));
            }
        }
    }

    return output;
}

/// WARNING : this method DO NOT work for edges of the border
///
void AbstractTriMesh::EdgeCollapse(VertexHandle v0, VertexHandle v1)
{
    if(!v0.is_valid() || !v1.is_valid())
        return;

    //    cout << "  Collapsing edge " << v0.idx() << "-" << v1.idx() << " ... " << endl;

    // new vertex attributes computation
    Point  p = (this->point(v0)  + this->point(v1) ) * 0.5;
    Color  c = Color::interpolate(this->color(v0), this->color(v1));
    Normal n = this->normal(v0) + this->normal(v1);
    n.normalize();

    std::vector<AbstractTriMesh::VertexHandle> n0 = GetVertexStar(v0);

    //    cout << "  neighbors of " << v0.idx() << ":" << endl;
    //    for(auto ii:n0)
    //        cout << "  " << ii.idx() << endl;

    std::deque<AbstractTriMesh::VertexHandle> neib0(n0.begin(), n0.end());


    uint nb = (uint) std::count (neib0.begin(), neib0.end(), v1);

    //    if(find(n0.begin(), n0.end(),v1) == n0.end())
    if(nb == 0)
    {
        // supposed to raise exception...

        std::cerr << "AbstractTriMesh::EdgeCollapse : vertices " << v0.idx() << " and " << v1.idx() << " are not forming an edge !" << std::endl;
        return;
    }

    if(nb > 1)
    {
        std::cerr << "AbstractTriMesh::EdgeCollapse : edge " << v0.idx() << "-" << v1.idx() << " is not collapsible !" << std::endl;
        return;
    }


    // set v1 to be the first element of neib0
    while(neib0[0] != v1)
    {
        neib0.push_back(neib0[0]);
        neib0.pop_front();
    }

    // removes first and second element
    neib0.pop_front();
    neib0.pop_front();

    std::vector<VertexHandle> n1 = GetVertexStar(v1);

    //    cout << "  neighbors of " << v1.idx() << ":" << endl;
    //    for(auto ii:n1)
    //        cout << "  " << ii.idx() << endl;

    std::deque<VertexHandle> neib1(n1.begin(), n1.end());

    nb = (uint)std::count (neib1.begin(), neib1.end(), v0);

    //    if(find(n1.begin(), n1.end(),v0) == n1.end())
    if(nb == 0)
    {
        // supposed to raise exception...

        std::cerr << "AbstractTriMesh::EdgeCollapse : vertices " << v0.idx() << " and " << v1.idx() << " are not forming an edge !" << std::endl;
        return;
    }
    if(nb > 1)
    {
        std::cerr << "AbstractTriMesh::EdgeCollapse : edge " << v0.idx() << "-" << v1.idx() << " is not collapsible !" << std::endl;
        return;
    }

    // set v0 to be the first element of neib1
    while(neib1[0] != v0)
    {
        neib1.push_back(neib1[0]);
        neib1.pop_front();
    }

    // removes first and second element
    neib1.pop_front();
    neib1.pop_front();

    // concatenate neighborhoods for
    std::vector<VertexHandle> neib;
    neib.insert(neib.end(), neib0.begin(), neib0.end());
    neib.insert(neib.end(), neib1.begin(), neib1.end());


    if(std::count (neib.begin(), neib.end(), v0) > 0
            || std::count (neib.begin(), neib.end(), v1) > 0)
    {
        std::cerr << "AbstractTriMesh::EdgeCollapse : edge " << v0.idx() << "-" << v1.idx() << " is not collapsible !" << std::endl;
        return;
    }


    for(auto& i : neib)
    {
        if(std::count (neib.begin(), neib.end(), i) > 1)
        {
            std::cerr << "AbstractTriMesh::EdgeCollapse : edge " << v0.idx() << "-" << v1.idx() << " is not collapsible !" << std::endl;
            return;
        }
    }


    //    cout << "  neighbors of the pair:" << endl;
    //    for(auto ii:neib)
    //        cout << "  " << ii.idx() << endl;

    if(neib.size() < 2)
        return;


    delete_vertex(v0, false);
    delete_vertex(v1, false);


    VertexHandle vh = add_point(p, n, c);

    for(uint i=0; i < neib.size(); i++)
    {
        add_face(neib[i], neib[(i+1)%neib.size()], vh);
    }
}

AbstractTriMesh::VertexHandle AbstractTriMesh::GetOppositeVertex(FaceHandle fh, EdgeHandle eh) const
{
    VertexHandle vh;

    if(edge(eh).idx_[0] == face(fh).idx_[0])
    {
        if(edge(eh).idx_[1] == face(fh).idx_[1])
        {
            vh.idx_ = face(fh).idx_[2];
        }
        else if(edge(eh).idx_[1] == face(fh).idx_[2])
        {
            vh.idx_ = face(fh).idx_[1];
        }
        else
        {
            std::cerr << "Edge #" << eh.idx() << " is not adjacent to face #" << fh.idx() << "." << std::endl;
        }
    }
    else if(edge(eh).idx_[0] == face(fh).idx_[1])
    {
        if(edge(eh).idx_[1] == face(fh).idx_[2])
        {
            vh.idx_ = face(fh).idx_[0];
        }
        else if(edge(eh).idx_[1] == face(fh).idx_[0])
        {
            vh.idx_ = face(fh).idx_[2];
        }
        else
        {
            std::cerr << "Edge #" << eh.idx() << " is not adjacent to face #" << fh.idx() << "." << std::endl;
        }
    }
    else if(edge(eh).idx_[0] == face(fh).idx_[2])
    {
        if(edge(eh).idx_[1] == face(fh).idx_[0])
        {
            vh.idx_ = face(fh).idx_[1];
        }
        else if(edge(eh).idx_[1] == face(fh).idx_[1])
        {
            vh.idx_ = face(fh).idx_[0];
        }
        else
        {
            std::cerr << "Edge #" << eh.idx() << " is not adjacent to face #" << fh.idx() << "." << std::endl;
        }
    }
    else
    {
        std::cerr << "Edge #" << eh.idx() << " is not adjacent to face #" << fh.idx() << "." << std::endl;
    }

    return vh;
}

AbstractTriMesh::VertexHandle AbstractTriMesh::EdgeCollapse(AbstractTriMesh::EdgeHandle eh)
{
    //    cout << endl << "collapsing edge #" << eh.idx() << endl;

    VertexHandle v0(edge(eh).idx_[0]), v1(edge(eh).idx_[1]);

    //    if(!IsBorderEdge(eh) && IsBorderVertex(v0) && IsBorderVertex(v1))
    if(!IsEdgeCollapsible(eh))
    {

        ork::Logger::ERROR_LOGGER->logf("MESHDEBUG", "Not collapsing edge #%d to preserve topology", eh.idx());
        return VertexHandle(-1);
    }

    // new vertex attributes computation
    Point  p = (this->point(v0)  + this->point(v1) ) * 0.5;             // position

    if(IsBorderVertex(v0) && !IsBorderVertex(v1))
    {
        p = this->point(v0);
    }
    else if(!IsBorderVertex(v0) && IsBorderVertex(v1))
    {
        p = this->point(v1);
    }

    Color  c = Color::interpolate(this->color(v0), this->color(v1));    // color
    Normal n = this->normal(v0) + this->normal(v1);                     // normal
    n.normalize();

    VertexStatus vs; // status:

    // face adjacency recopy
    for(int i : v_status_[v0.idx()].adjacent_edges_)
        vs.add_an_edge(i);
    for(int i : v_status_[v1.idx()].adjacent_edges_)
        vs.add_an_edge(i);

    vs.remove_an_edge(eh.idx());
    vs.remove_an_edge(eh.idx());

    // edge adjacency recopy
    for(int i : v_status_[v0.idx()].adjacent_faces_)
        if(!f_status_[i].deleted())
            vs.add_a_face(i);
    for(int i : v_status_[v1.idx()].adjacent_faces_)
        if(!f_status_[i].deleted())
            vs.add_a_face(i);


    // new vertex creation
    VertexHandle vh = add_point(p, n, c, vs);


    // for all faces adjacent to eh
    while(e_status_[eh.idx()].adjacent_faces_.size()>0)
    {
        int i = *(e_status_[eh.idx()].adjacent_faces_.begin());


        // get the opposite vertex
        VertexHandle opposite = GetOppositeVertex(FaceHandle(i), eh);

        if(!opposite.is_valid())
        {
            ork::Logger::ERROR_LOGGER->log("MESH", "Error during edge collapse: opposite vertex not found.");
            break;
        }

        // create a nex edge from it (replacing the current face)
        EdgeHandle new_edge = add_edge(vh.idx(), opposite.idx());

        // remove the current face of the vertex status
        v_status_[opposite.idx()].remove_a_face(i, false);
        // and add the new edge
        //        v_status_[opposite.idx()].add_an_edge(new_edge.idx());

        //        v_status_[vh.idx()].add_an_edge(new_edge.idx());


        // for all adjacent edges of the current face
        for(int ei : f_status_[i].adjacent_edges_)
        {

            if(ei == new_edge.idx())
                continue;

            // except for eh
            if(ei == eh.idx())
                continue;

            v_status_[vh.idx()].remove_an_edge(ei);
            v_status_[vh.idx()].remove_a_face(i, false);

            // remove the edge from the vertex status
            v_status_[opposite.idx()].remove_an_edge(ei);

            // get the opposite face
            FaceHandle fh = GetOppositeFace(FaceHandle(i), EdgeHandle(ei));

            if(! fh.is_valid())
                continue;

            // replace the edge in the face status
            f_status_[fh.idx()].remove_an_edge(ei);
            f_status_[fh.idx()].add_an_edge(new_edge.idx());


            e_status_[new_edge.idx()].add_a_face(fh.idx());

            // update the vertex in the face
            for (unsigned int vi=0; vi < 3; vi++)

                if(face(fh).idx_[vi] == (uint)v0.idx() || face(fh).idx_[vi] == (uint)v1.idx())
                {
                    face(fh).idx_[vi] = (uint)vh.idx();
                }

            e_status_[ei].adjacent_faces_.clear();
            delete_edge(EdgeHandle(ei));
        }

        delete_face(FaceHandle(i), false);
    }



    // adjacent elements update

    // for both edge extremity
    for(int k=0; k < 2; ++k)
    {
        // for all adjacent faces
        for(int i : v_status_[edge(eh).idx_[k]].adjacent_faces_)
        {
            // for all face vertex
            for(int j=0; j<3; ++j)
            {
                // update index
                if(face(FaceHandle(i)).idx_[j] == edge(eh).idx_[k])
                {
                    faces_[i].idx_[j] = vh.idx();
                }
            }
        }
        v_status_[edge(eh).idx_[k]].adjacent_faces_.clear();

        // for all adjacent edges
        for(int i : v_status_[edge(eh).idx_[k]].adjacent_edges_)
        {
            // for all edge vertex
            for(int j=0; j<2; ++j)
            {
                // update index
                if(edge(EdgeHandle(i)).idx_[j] == edge(eh).idx_[k])
                {
                    edges_[i].idx_[j] = vh.idx();
                }
            }
        }
        v_status_[edge(eh).idx_[k]].adjacent_edges_.clear();
    }

    v_status_[v0.idx()].adjacent_faces_.clear();
    v_status_[v0.idx()].adjacent_edges_.clear();
    v_status_[v0.idx()].nb_face_for_vertex_ = 0;
    v_status_[v1.idx()].adjacent_faces_.clear();
    v_status_[v1.idx()].adjacent_edges_.clear();
    v_status_[v0.idx()].nb_face_for_vertex_ = 0;

    // extremity vertices destruction
    delete_vertex(v0, false);
    delete_vertex(v1, false);

    // edge destruction
    delete_edge(eh);

    return vh;
}



//*/

void AbstractTriMesh::FaceSplit(AbstractTriMesh::FaceHandle fh, AbstractTriMesh::VertexHandle v0, AbstractTriMesh::VertexHandle v1, AbstractTriMesh::VertexHandle v_new)
{
    std::deque<VertexHandle> vh;
    vh.push_back(face(fh).idx_[0]);
    vh.push_back(face(fh).idx_[1]);
    vh.push_back(face(fh).idx_[2]);

    while(vh[0] == v0 || vh[0] == v1)
    {
        vh.push_back(vh[0]);
        vh.pop_front();
    }

    delete_face(fh, false);

    add_face(vh[0], vh[1], v_new);
    add_face(vh[0], v_new, vh[2]);
}

void AbstractTriMesh::FaceSplit(AbstractTriMesh::FaceHandle fh, AbstractTriMesh::EdgeHandle eh, AbstractTriMesh::VertexHandle v_new)
{
    UNUSED(fh);
    UNUSED(eh);
    UNUSED(v_new);
    //    VertexHandle opposite = GetOppositeVertex(fh, eh);

    //    for(int e : f_status_[fh.idx()].adjacent_edges_)
    //    {
    //        if(e == eh.idx())
    //            continue;

    //        int v = edges_[e].idx_[0];
    //        if(v == opposite.idx())
    //            v = edges_[e].idx_[1];

    //        Face new_f();
    //    }
}

void AbstractTriMesh::EdgeSplit(AbstractTriMesh::VertexHandle v0, AbstractTriMesh::VertexHandle v1)
{
    // Interpolated vertex :
    Point  p = (this->point(v0)  + this->point(v1) ) * 0.5;
    Color  c = Color::interpolate(this->color(v0), this->color(v1));
    //    Color  c = Color::Red(rand());
    Normal n = this->normal(v0) + this->normal(v1);
    n.normalize();
    VertexHandle vh = add_point(p, n, c);


    // Incident faces splitting
    std::set<AbstractTriMesh::FaceHandle> faces = CommonFaces(v0, v1);

    for(auto& f : faces)
    {
        FaceSplit(f, v0, v1, vh);
    }
}

AbstractTriMesh::VertexHandle AbstractTriMesh::EdgeSplit(AbstractTriMesh::EdgeHandle eh)
{
    //    cout << "splitting edge #" << eh.idx() << endl;

    VertexHandle v0(edge(eh).idx_[0]), v1(edge(eh).idx_[1]);

    // Interpolated vertex :
    Point  p = (this->point(v0)  + this->point(v1) ) * 0.5;
    Color  c = Color::interpolate(this->color(v0), this->color(v1));
    //    Color  c = Color::Red(rand());
    Normal n = this->normal(v0) + this->normal(v1);
    n.normalize();
    VertexHandle vh = add_point(p, n, c);

    std::list<int> l = e_status_[eh.idx()].adjacent_faces_;

    while(l.size() > 0)
    {
        FaceHandle fh(*l.begin());
        l.pop_front();
        FaceSplit(fh, v0, v1, vh);
    }

    return vh;
}

bool AbstractTriMesh::IsBorderEdge(AbstractTriMesh::EdgeHandle eh)
{
    return e_status_[eh.idx()].adjacent_faces_.size() != 2;
}

bool AbstractTriMesh::IsManifoldEdge(AbstractTriMesh::EdgeHandle eh)
{
    return e_status_[eh.idx()].adjacent_faces_.size() <= 2;
}

bool AbstractTriMesh::IsBorderVertex(AbstractTriMesh::VertexHandle vh)
{
    for(int e : v_status_[vh.idx()].adjacent_edges_)
    {
        if(IsBorderEdge(EdgeHandle(e)))
            return true;
    }
    return false;
}

bool AbstractTriMesh::IsManifoldVertex(AbstractTriMesh::VertexHandle vh)
{
    for(int e : v_status_[vh.idx()].adjacent_edges_)
    {
        if(!IsManifoldEdge(EdgeHandle(e)))
            return false;
    }
    return true;
}

bool AbstractTriMesh::IsFace(AbstractTriMesh::VertexHandle v0, AbstractTriMesh::VertexHandle v1, AbstractTriMesh::VertexHandle v2)
{
    std::set<FaceHandle> f01 = CommonFaces(v0, v1);
    std::set<FaceHandle> f12 = CommonFaces(v1, v2);
    std::set<FaceHandle> f20 = CommonFaces(v2, v0);

    std::vector<FaceHandle> f;

    for(FaceHandle fh01 : f01)
    {
        for(FaceHandle fh12 : f12)
        {
            for(FaceHandle fh20 : f20)
            {
                if(fh01 == fh12 && fh12 == fh20)
                    return true;
            }

        }
    }

    return false;
}

bool AbstractTriMesh::IsEdgeCollapsible(AbstractTriMesh::EdgeHandle eh)
{
    VertexHandle v0(edge(eh).idx_[0]), v1(edge(eh).idx_[1]);



    // check if the edge extremity have a common neighbor while not forming a face with it
    std::vector<VertexHandle> neib_v0 = GetVertexStar(v0);
    std::vector<VertexHandle> neib_v1 = GetVertexStar(v1);
    std::vector<VertexHandle> neib;

    for(VertexHandle vh0 : neib_v0)
    {
        for(VertexHandle vh1 : neib_v1)
        {
            if(vh0 == vh1)
                neib.push_back(vh0);
        }
    }

    //    cout << neib.size() << endl;

    //    if(neib.size() > 2)
    //        cout << "problematic edge !" << endl;


    for(VertexHandle vh : neib)
    {
        if(!IsFace(vh, v0, v1))
            return false;
    }


    // check if the edge is connecting two borders of the mesh (or one border non locally)
    if(!IsBorderEdge(eh) && IsBorderVertex(v0) && IsBorderVertex(v1))
        return false;

    return true;
}

void AbstractTriMesh::EmbedInFrame(const Frame& f)
{
    AABBox box;
    for(VertexIter vh = vertices_begin(); vh != vertices_end(); ++vh)
    {
        Vector pos = point(*vh);
        pos = f.Embed(pos);
        set_point(*vh, pos);
        box.Enlarge(pos);
    }
    update_bounding_box(box);
}

void AbstractTriMesh::UnembedInFrame(const Frame& f)
{
    AABBox box;
    for(VertexIter vh = vertices_begin(); vh != vertices_end(); ++vh)
    {
        Vector pos = point(*vh);
        pos = f.UnEmbed(pos);
        set_point(*vh, pos);
        box.Enlarge(pos);
    }
    update_bounding_box(box);
}

Scalar AbstractTriMesh::EdgeLength(const EdgeHandle eh) const
{
    return (point(edges_[eh.idx()].idx_[0]) - point(edges_[eh.idx()].idx_[1])).norm();
}

Scalar AbstractTriMesh::MeanEdgeLength() const
{
    Scalar output = 0;
    for(ConstEdgeIter ei = edges_begin(); ei != edges_end(); ei++)
    {
        Scalar length = EdgeLength(*ei);
        output += length;
    }
    output /= edges_.size();
    return output;
}

Scalar AbstractTriMesh::MaxEdgeLength() const
{
    Scalar output = -1;
    for(ConstEdgeIter ei = edges_begin(); ei != edges_end(); ei++)
    {
        Scalar length = EdgeLength(*ei);
        output = std::max(output, length);
    }
    return output;
}

Scalar AbstractTriMesh::MinEdgeLength() const
{
    Scalar output = -1;
    for(ConstEdgeIter ei = edges_begin(); ei != edges_end(); ei++)
    {
        Scalar length = EdgeLength(*ei);
        if(output < 0)
        {
            output = length;
            continue;
        }
        output = std::min(output, length);
    }
    return output;
}

void AbstractTriMesh::notify_garbage_collection_done(const std::map<VertexHandle, VertexHandle> &vertex_handles, const std::map<EdgeHandle, EdgeHandle> &edge_handles, const std::map<FaceHandle, FaceHandle> &face_handles)
{
    for (auto listener : ListenableT<AbstractTriMesh>::listeners_) {
        dynamic_cast<AbstractTriMeshListener*>(listener)->garbage_collection_done(vertex_handles, edge_handles, face_handles);
    }
}

void AbstractTriMesh::setVertexInPlace(int oldVertex, int newVertex, std::vector<int> &vh_map)
{
    std::swap(vh_map[oldVertex],  vh_map[newVertex]); // update the mapping
    std::swap(v_status_[oldVertex], v_status_[newVertex]);
}

void AbstractTriMesh::setVertexCount(unsigned int n_vertices)
{
    v_status_.resize(n_vertices);
}

void AbstractTriMesh::createOctree(AABBox aabb)
{
    if(!octree_)
        octree_ = std::make_shared<Octree<BasicTriMeshCell>>();

    octree_->clear();

    garbage_collection();

    if(aabb.IsEmpty())
        octree_->set_boundingBox(bounding_box());
    else
        octree_->set_boundingBox(aabb);

    octree_cells_.resize(n_faces());

    for(auto it = faces_begin(); it != faces_end(); it++)
        octree_cells_[it->idx()] = octree_->add(BasicTriMeshCell(this, it->idx()));

}

void AbstractTriMesh::updateOctreeTriangle(FaceHandle handle)
{
    if(!octree_)
        return;

    octree_->remove(octree_cells_[handle.idx()]);
    octree_cells_[handle.idx()] = octree_->add(BasicTriMeshCell(this, handle.idx()));
}

} // end of namespace core

} // end of namespace expressive

