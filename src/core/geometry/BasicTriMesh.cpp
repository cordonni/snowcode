#define TEMPLATE_INSTANTIATION_BASICTRIMESH
#include <core/geometry/BasicTriMesh.h>

namespace expressive {

namespace core {

// explicit instantiation

template class BasicTriMeshT<DefaultMeshVertex>;

}

}
