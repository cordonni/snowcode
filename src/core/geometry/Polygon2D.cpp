#include <core/geometry/Polygon2D.h>

using namespace std;

namespace expressive {

namespace core {

Polygon2D::Polygon2D(std::vector<Vector2> vertices) : m_vertices(vertices)
{
    for (uint i = 0; i < nb_vert(); ++i) {
        bounding_box_.Enlarge(m_vertices[i]);
    }
}

void Polygon2D::add_vertex(const Vector2 &v)
{
    m_vertices.push_back(v);
    bounding_box_.Enlarge(v);
}

AABBox2D Polygon2D::bounding_box() const
{
    return bounding_box_;
}

double Polygon2D::area() const
{
    double output(0);
    for(uint i=0; i<nb_vert(); i++)
    {
        output += m_vertices[i][0]*m_vertices[(i+1)%nb_vert()][1];
        output -= m_vertices[i][1]*m_vertices[(i+1)%nb_vert()][0];
    }
    return output * 0.5;
}

double Polygon2D::minX() const
{
    return bounding_box_.min()[0];
}

double Polygon2D::maxX() const
{
    return bounding_box_.max()[0];
}

double Polygon2D::minY() const
{
    return bounding_box_.min()[1];
}

double Polygon2D::maxY() const
{
    return bounding_box_.max()[1];
}

bool Polygon2D::contains(const Vector2& v) const
{
    if(v[0] < minX() || v[0] > maxX() || v[1] < minY() || v[1] > maxY())
        return false;

    bool c = false;

    for (unsigned int i = 0; i < nb_vert(); i++)
    {
      int j = (i+1)%nb_vert();
      if ( ((m_vertices[i][1]>v[1]) != (m_vertices[j][1]>v[1])) &&
       (v[0] < (m_vertices[j][0] - m_vertices[i][0]) * (v[1]-m_vertices[i][1]) / (m_vertices[j][1]-m_vertices[i][1]) + m_vertices[i][0]))
         c = !c;
    }

    return c;
}


Polygon2D Polygon2D::Ngon(const uint N)
{
    float radius = 10.0;
    vector<Vector2> v;
    for(uint i=0; i<N; i++)
    {
        float t = float(i) / N * 2 * (float) M_PI;

        v.push_back(Vector2(cos(t),sin(t)) * radius);
    }

    return Polygon2D(v);
}

}

}
