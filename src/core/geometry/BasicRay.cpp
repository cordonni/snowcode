#include <core/geometry/BasicRay.h>

//#include <core/geometry/Point.h>

using namespace std;

namespace expressive
{

namespace core
{

BasicRay::BasicRay() :
    starting_point_(Point::Zero()), direction_(Vector::UNIT_X) {}

BasicRay::BasicRay(const Point& starting_point,const Vector& direction)
    : starting_point_(starting_point), direction_(direction) {}

BasicRay::~BasicRay() {}

void BasicRay::set_starting_point(const Point& starting_point)
{
    starting_point_ = starting_point;
}

void BasicRay::set_direction(const Vector& direction)
{
    direction_ = direction;
}

const Point& BasicRay::starting_point() const
{
    return starting_point_;
}

const Vector& BasicRay::direction() const
{
    return direction_;
}

bool BasicRay::IntersectsSphere(const Point &center, Scalar radius, Scalar &dist) const
{
    Vector ray = starting_point_ - center;
    Scalar alpha, beta, gamma;

    alpha = (direction_[0] * direction_[0] + direction_[1] * direction_[1] + direction_[2] * direction_[2]);
    beta = 2.0 * ((direction_[0] * ray[0]) + (direction_[1] * ray[1]) + (direction_[2] * ray[2]));
    gamma = starting_point_[0] * starting_point_[0] + starting_point_[1] * starting_point_[1] + starting_point_[2] * starting_point_[2] + center[0] * center[0] + center[1] * center[1] + center[2] * center[2] - 2.0 * (starting_point_[0] * center[0] + starting_point_[1] * center[1] + starting_point_[2] * center[2]) - radius * radius;

    Scalar delta = (beta * beta - 4 * alpha * gamma);

    if (delta > 0) {
        Scalar x1 = (-beta - sqrt(delta)) / (2.0 * alpha);
        Scalar x2 = (-beta + sqrt(delta)) / (2.0 * alpha);
        dist = std::min(x1, x2);
    } else if (delta == 0.0) {
        dist = -beta / (2.0 * alpha);
    } else {
        dist = expressive::kScalarMax;
    }

    return  delta >= 0;
}

bool BasicRay::IntersectsPlane(const Point &point, const Point &normal, Scalar &dist) const
{
    dist = expressive::kScalarMax;
    Scalar distToOrigin = normal.dot(point);

    Scalar nDotA = normal.dot(starting_point_);
    Scalar nDotBA = normal.dot(direction_);
    if (abs(nDotBA) < 0.0000001) {
        return false;
    }

    dist = ((distToOrigin - nDotA)/ nDotBA);
    return true;
}

bool BasicRay::IntersectsBoundingBox(const AABBox &box, Scalar &dist) const
{
    Vector t0 = (box.min() - starting_point_);// / direction_;
    Vector t1 = (box.max() - starting_point_);// / direction_;
    for (unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i) {
        t0[i] /= direction_[i];
        t1[i] /= direction_[i];
    }

    Scalar tmin = max(max(min(t0[0], t1[0]), min(t0[1], t1[1])), min(t0[2], t1[2]));
    Scalar tmax = min(min(max(t0[0], t1[0]), max(t0[1], t1[1])), max(t0[2], t1[2]));

    // if tmax < 0 ray is intersecting box, but whole box is behind us.
    if (tmax < 0) {
        dist = expressive::kScalarMax;
        return false;
    }

    // if tmin > tmax, ray doesn't intersect box.
    if (tmin > tmax) {
        dist = expressive::kScalarMax;
        return false;
    }
    dist = tmin;
    return true;
}


Point BasicRay::ClosestPoint(const BasicRay& other_ray) const
{
    Point  p1(starting_point_), p2(other_ray.starting_point());
    Vector n1(direction_), n2(other_ray.direction());
    n1.normalize();
    n2.normalize();

    Vector diff = p2 - p1;
    Scalar scal = n1.dot(n2);

    double t1 = (n1.dot(diff) - scal*n2.dot(diff)) / (1.0 - scal*scal);

    return starting_point_ + n1 * t1;
}


Point BasicRay::MidPoint(const BasicRay& other_ray) const
{
    return 0.5*(ClosestPoint(other_ray) + other_ray.ClosestPoint(*this));
}


Scalar BasicRay::Distance(const Point& pt) const
{
    Vector d = pt - this->starting_point_;
    Vector t = d.dot(this->direction_.normalized()) * this->direction_.normalized();

    return (d - t).norm();
}


Point BasicRay::Project(const Point& p) const
{
    Vector d = p - this->starting_point_;
    Vector t = d.dot(this->direction_.normalized()) * this->direction_.normalized();

    return this->starting_point_ + t;
}

} // namespace core

} // namespace expressive
