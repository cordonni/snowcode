/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef ADVANCEDVECTORTOOLS_H
#define ADVANCEDVECTORTOOLS_H

#include <core/CoreRequired.h>

// Dependencies: Primitive
#include <core/geometry/Segment.h>
#include <core/geometry/Triangle.h>
#include <core/geometry/VectorTools.h>


namespace expressive {

namespace VectorTools {

/** \brief return the projection of P to the support Segment S
*   \param P Point to project on S
*   \param c First Segment Extremities
*   \param v Segment Vector (not normalised)
*   \param proj_to_primitive: if true, we restrain the result to the segment
*   \return  Projected Point
*/
Point CORE_API ProjectionToSegment(const Point& P, const Point& c, const Vector v, bool proj_to_primitive = false);

/** \brief return the projection of P to the support Segment S
*   \param P Point to project on S
*   \param S Support Segment
*   \param proj_to_primitive: if true, we restrain the result to the segment
*   \return  Projected Point
*/
template < typename T >
static inline Point ProjectionToSegment(const Point& P, const core::SegmentT<T>& S, bool proj_to_primitive = false) {
    return ProjectionToSegment(P, S.p1(), S.dir(), proj_to_primitive);
}

/** \brief return the projection of P to the support Triangle T
*   \param P Point to project on T
*   \param T Support Triangle
*   \param proj_to_primitive: if true, we restrain the result to the triangle area
*   \return  Projected Point
*/
template < typename T >
static inline Vector ProjectionToTriangle(const Vector& P, const core::TriangleT<T>& Tr, bool proj_to_primitive = false) {
    Point  c = Tr.p1         ();
    Vector n = Tr.unit_normal();
    Scalar k = (c - P).dot(n); // /(n.dot(n)); n is unit;

    // Projection
    Point p = P + k*n;

    if (proj_to_primitive) {
        Vector v12 = Tr.unit_p1_to_p2();
        Vector v13 =-Tr.unit_p3_to_p1();
        Vector vcp = p - c;

        Scalar n2_v12  = v12.dot(v12);
        Scalar n2_v13  = v13.dot(v13);
        Scalar v12_v13 = v12.dot(v13);

        Scalar k12 = (vcp.dot(n2_v12*v12 - v12_v13*v13))/(n2_v12*n2_v13 - v12_v13*v12_v13);
        Scalar k13 = (vcp.dot(v13) - k12*(v12_v13))/n2_v13;

        if      (k12       < 0.) { return ProjectionToSegment(p, c, v13, true); }
        else if (k13       < 0.) { return ProjectionToSegment(p, c, v12, true); }
        else if (k12 + k13 > 1.) { return ProjectionToSegment(p, Tr.p2(), Tr.unit_p2_to_p3(), true); }
    }

    return p;
}

} // CLose namespace VectorTools;

} // Close namespace expressive


#endif // ADVANCEDVECTORTOOLS_H
