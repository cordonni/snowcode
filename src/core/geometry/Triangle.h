/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: Triangle.h

   Language: C++

   License: Expressive license

   \author: Maxime Quiblier
   E-Mail:  maxime.quiblier@inrialpes.fr

   Description: Header for triangle.

   Platform Dependencies: None
*/

#pragma once
#ifndef TRIANGLE_H_
#define TRIANGLE_H_

// Convol dependencies
#include<core/CoreRequired.h>
    // geometry
    #include<core/geometry/AABBox.h>
    #include<core/geometry/BoundingSphere.h>
    #include<core/geometry/GeometryPrimitive.h>
    // utils
    #include<core/utils/Serializable.h>

namespace expressive {

namespace core {

template<typename PointT>
/**
 * A Triangle GeometryPrimitive.
 * Just as Segment, this one offers a few helping variables in order to improve computation time
 * when getting intersection, length, area, etc.
 * Triangles are always loops.
 */
class TriangleT : public GeometryPrimitiveT<PointT>
{
public:
    EXPRESSIVE_MACRO_NAME("Triangle")

    typedef GeometryPrimitiveT<PointT> Base;
    typedef typename PointT::BasePoint BasePoint;
    typedef typename PointT::BasePoint BaseVector;

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    /*! \brief Build a triangle "along" the first axis.
     */
    TriangleT() :
        GeometryPrimitiveT<PointT>()
    {
        //TODO:@todo : kind of ugly but dont see other solution ...
        GeometryPrimitiveT<PointT>::points_.push_back(PointT());
        GeometryPrimitiveT<PointT>::points_.push_back(PointT());
        GeometryPrimitiveT<PointT>::points_.push_back(PointT());
        GeometryPrimitiveT<PointT>::points_[1][0] = 1.0;
        GeometryPrimitiveT<PointT>::points_[2][1] = 1.0;
        ComputeHelpVariables();
    }
    
    /*! \brief Build a triangle
     *   \param p1 First triangle point
     *   \param p2 Second triangle point
     *   \param p3 Third triangle point
     */
    TriangleT(const PointT& p1, const PointT& p2, const PointT& p3) :
        GeometryPrimitiveT<PointT>()
    {
        GeometryPrimitiveT<PointT>::points_.push_back(p1);
        GeometryPrimitiveT<PointT>::points_.push_back(p2);
        GeometryPrimitiveT<PointT>::points_.push_back(p3);
        ComputeHelpVariables();
    }

    TriangleT(const std::vector<PointT>& points) :
        GeometryPrimitiveT<PointT>()
    {
        assert(points.size() == 3);
        GeometryPrimitiveT<PointT>::points_ = points;
        ComputeHelpVariables();
    }

//    using GeometryPrimitiveT<PointT>::operator delete; // necessary because of eigen's macro.
    ~TriangleT(){}

    TriangleT(const TriangleT &rhs, unsigned int start = -1, unsigned int end = -1)
        :   GeometryPrimitiveT<PointT>(rhs, start, end)
    {
        ComputeHelpVariables();
    }

    virtual std::shared_ptr<GeometryPrimitiveT<PointT> > clone(unsigned int start = - 1, unsigned int end = -1) const {
        return std::make_shared<TriangleT<PointT> >(*this, start, end);
    } // Todo : yew.. this is ugly.

    ///////////////////
    // Xml functions //
    ///////////////////

//    virtual TiXmlElement * ToXml(bool embed_vertices = false, const char *name = NULL) const;

    ///////////////////////////
    // Bounding-box function //
    ///////////////////////////

    AABBoxT<BasePoint> GetAxisBoundingBox() const;
    BoundingSphereT<BasePoint> GetBoundingSphere() const;

    ///////////////
    // Modifiers //
    ///////////////

    void set_points(const PointT& p1, const PointT& p2, const PointT& p3);
    void set_p1(const PointT& p1);
    void set_p2(const PointT& p2);
    void set_p3(const PointT& p3);

    ///////////////
    // Accessors //
    ///////////////

    const PointT& p1()    const { return GeometryPrimitiveT<PointT>::points_[0]; }
    const PointT& p2()    const { return GeometryPrimitiveT<PointT>::points_[1]; }
    const PointT& p3()    const { return GeometryPrimitiveT<PointT>::points_[2]; }

    const BaseVector& unit_p1_to_p2()   const { return unit_p1_to_p2_; }
    const BaseVector& unit_p2_to_p3()   const { return unit_p2_to_p3_; }
    const BaseVector& unit_p3_to_p1()   const { return unit_p3_to_p1_; }
    const BaseVector& unit_normal()     const { return unit_normal_; }

    Scalar norm_p1_to_p2()   const { return norm_p1_to_p2_; }
    Scalar norm_p2_to_p3()   const { return norm_p2_to_p3_; }
    Scalar norm_p3_to_p1()   const { return norm_p3_to_p1_; }


    ///////////////
    // Attributs //
    ///////////////

    virtual unsigned int Split(unsigned int index, const PointT &pos, std::set<std::shared_ptr<GeometryPrimitiveT<PointT> > > &newPrimitives);

    inline virtual bool isLoop() const {return true;}

//    inline virtual void set_is_loop(bool /*loop*/ = true) {}

    virtual bool isFilled() const { return true; }
protected :

//    Point p1_;
//    Point p2_;
//    Point p3_;

    // Convenience attributes
    BaseVector unit_p1_to_p2_;
    BaseVector unit_p2_to_p3_;
    BaseVector unit_p3_to_p1_;
    BaseVector unit_normal_;    // Computed using cross product p1->p2 cross p2->p3

    Scalar norm_p1_to_p2_;
    Scalar norm_p2_to_p3_;
    Scalar norm_p3_to_p1_;

    // Help variables for divers computation on the triangle.
    // This will be used to improve computation speed in divers cases.
    virtual void ComputeHelpVariables();
};

template<>
inline void TriangleT<PointPrimitive2D>::ComputeHelpVariables()
{
    unit_p1_to_p2_ = GeometryPrimitive2D::points_[1] - GeometryPrimitive2D::points_[0];
    unit_p2_to_p3_ = GeometryPrimitive2D::points_[2] - GeometryPrimitive2D::points_[1];
    unit_p3_to_p1_ = GeometryPrimitive2D::points_[0] - GeometryPrimitive2D::points_[2];
    unit_normal_ = Point2::Zero();

    norm_p1_to_p2_ = unit_p1_to_p2_.norm();
    unit_p1_to_p2_ /= norm_p1_to_p2_;
    norm_p2_to_p3_ = unit_p2_to_p3_.norm();
    unit_p2_to_p3_ /= norm_p2_to_p3_;
    norm_p3_to_p1_ = unit_p3_to_p1_.norm();
    unit_p3_to_p1_ /= norm_p3_to_p1_;
}

//template<>
//inline void TriangleT<WeightedPoint2D>::ComputeHelpVariables()
//{
//    unit_p1_to_p2_ = WeightedGeometryPrimitive2D::points_[1] - WeightedGeometryPrimitive2D::points_[0];
//    unit_p2_to_p3_ = WeightedGeometryPrimitive2D::points_[2] - WeightedGeometryPrimitive2D::points_[1];
//    unit_p3_to_p1_ = WeightedGeometryPrimitive2D::points_[0] - WeightedGeometryPrimitive2D::points_[2];
//    unit_normal_ = Point2::Zero();

//    norm_p1_to_p2_ = unit_p1_to_p2_.norm();
//    unit_p1_to_p2_ /= norm_p1_to_p2_;
//    norm_p2_to_p3_ = unit_p2_to_p3_.norm();
//    unit_p2_to_p3_ /= norm_p2_to_p3_;
//    norm_p3_to_p1_ = unit_p3_to_p1_.norm();
//    unit_p3_to_p1_ /= norm_p3_to_p1_;
//}

template<typename PointT>
inline void TriangleT<PointT>::ComputeHelpVariables()
{
    unit_p1_to_p2_ = GeometryPrimitiveT<PointT>::points_[1] - GeometryPrimitiveT<PointT>::points_[0];
    unit_p2_to_p3_ = GeometryPrimitiveT<PointT>::points_[2] - GeometryPrimitiveT<PointT>::points_[1];
    unit_p3_to_p1_ = GeometryPrimitiveT<PointT>::points_[0] - GeometryPrimitiveT<PointT>::points_[2];
    unit_normal_ = (unit_p1_to_p2_).cross(-unit_p3_to_p1_);
    unit_normal_.normalize();

    norm_p1_to_p2_ = unit_p1_to_p2_.norm();
    unit_p1_to_p2_ /= norm_p1_to_p2_;
    norm_p2_to_p3_ = unit_p2_to_p3_.norm();
    unit_p2_to_p3_ /= norm_p2_to_p3_;
    norm_p3_to_p1_ = unit_p3_to_p1_.norm();
    unit_p3_to_p1_ /= norm_p3_to_p1_;
}


template<typename PointT>
void TriangleT<PointT>::set_points(const PointT& p1, const PointT& p2, const PointT& p3)
{
    GeometryPrimitiveT<PointT>::points_[0] = p1;
    GeometryPrimitiveT<PointT>::points_[1] = p2;
    GeometryPrimitiveT<PointT>::points_[2] = p3;
    ComputeHelpVariables();
}

template<typename PointT>
void TriangleT<PointT>::set_p1(const PointT& p1)
{
    GeometryPrimitiveT<PointT>::points_[0] = p1;
    ComputeHelpVariables();
}

template<typename PointT>
void TriangleT<PointT>::set_p2(const PointT& p2)
{
    GeometryPrimitiveT<PointT>::points_[1] = p2;
    ComputeHelpVariables();
}

template<typename PointT>
void TriangleT<PointT>::set_p3(const PointT& p3)
{
    GeometryPrimitiveT<PointT>::points_[2] = p3;
    ComputeHelpVariables();
}

//template<typename PointT>
//TiXmlElement * TriangleT<PointT>::ToXml(bool embed_vertices, const char *name) const
//{
//    TiXmlElement * element = GeometryPrimitiveT<PointT>::ToXml(embed_vertices, name);

//    return element;
//}

///////////////////////////
// Bounding-box function //
///////////////////////////

template<typename PointT>
AABBoxT<typename PointT::BasePoint> TriangleT<PointT>::GetAxisBoundingBox() const
{
    //TODO:@todo : use eigen correctly
    AABBoxT<BasePoint> res;

    res.min_ = Base::points_[0];
    res.max_ = Base::points_[0];
    for(unsigned int i = 0; i < Point::RowsAtCompileTime; ++i)
    {
        if(Base::points_[1][i] > res.max_[i])
        {
            res.max_[i] = Base::points_[1][i];
        }
        if(Base::points_[2][i] > res.max_[i])
        {
            res.max_[i] = Base::points_[2][i];
        }
        if(Base::points_[1][i] < res.min_[i])
        {
            res.min_[i] = Base::points_[1][i];
        }
        if(Base::points_[2][i] < res.min_[i])
        {
            res.min_[i] = Base::points_[2][i];
        }
    }
    return res;
}

template<typename PointT>
auto TriangleT<PointT>::GetBoundingSphere() const -> BoundingSphereT<BasePoint>
{
    BoundingSphereT<BasePoint> res;
    res.radius_ = (res.center_ - Base::points_[0]).norm();
    res.center_ = (1.0/3.0) * (Base::points_[0]+Base::points_[1]+Base::points_[2]);
    //TODO:@todo : the bounding sphere of a weighted triangle contains a WeightedPoint instead of a Point ....

    return res;
}

template<typename PointT>
unsigned int TriangleT<PointT>::Split(unsigned int index, const PointT &pos, std::set<std::shared_ptr<GeometryPrimitiveT<PointT> > > &newPrimitives)
{
    if (index == (uint)-1) {
        std::shared_ptr<GeometryPrimitiveT<PointT> > t1 = clone();
        std::shared_ptr<GeometryPrimitiveT<PointT> > t2 = clone();
        this->SetPoint(0, pos);
        t1->SetPoint(1, pos);
        t2->SetPoint(2, pos);

        newPrimitives.insert(t1);
        newPrimitives.insert(t2);
        return 0;
    } else {
        std::shared_ptr<GeometryPrimitiveT<PointT> > triangle = clone();
        unsigned int i = (index + 1) % 3;
        triangle->SetPoint(i, pos);
        this->SetPoint(index, pos);
        newPrimitives.insert(triangle);

        return i;
    }
}

typedef TriangleT<PointPrimitive> Triangle;
typedef TriangleT<PointPrimitive2D> Triangle2D;

} // Close namespace core

} // Close namespace expressive

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_TRIANGLE
extern template class expressive::core::TriangleT<expressive::core::PointPrimitiveT<expressive::Point> >;
extern template class expressive::core::TriangleT<expressive::core::PointPrimitiveT<expressive::Point2> >;
//extern template class expressive::core::TriangleT<expressive::core::WeightedPointT<expressive::Point> >;
//extern template class expressive::core::TriangleT<expressive::core::WeightedPointT<expressive::Point2> >;
#endif
#endif

#endif // TRIANGLE_H_
