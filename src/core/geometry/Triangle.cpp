#define TEMPLATE_INSTANTIATION_TRIANGLE

#include<core/geometry/Triangle.h>

#include <core/utils/BasicsToXml.h>

namespace expressive {

namespace core {

// explicit instantiation
template class TriangleT<PointPrimitive>;
//template class TriangleT<WeightedPoint>;
template class TriangleT<PointPrimitive2D>;
//template class TriangleT<WeightedPoint2D>;


///////////////////
// Xml functions //
///////////////////

//TiXmlElement * Triangle::ToXml(const char *name) const
//{
//    TiXmlElement * element;
//    if(name == NULL)
//        element = new TiXmlElement( this->StaticName() );
//    else
//        element = new TiXmlElement( name );

//    TiXmlElement * element_p1 = PointToXml(p1_, "p1");
//    element->LinkEndChild(element_p1);

//    TiXmlElement * element_p2 = PointToXml(p2_, "p2");
//    element->LinkEndChild(element_p2);

//    TiXmlElement * element_p3 = PointToXml(p3_, "p3");
//    element->LinkEndChild(element_p3);

//    return element;
//}

/////////////////////////////
//// Bounding-box function //
/////////////////////////////

//AABBox Triangle::GetAxisBoundingBox() const
//{
//    AABBox res;

//    res.min_ = p1_;
//    res.max_ = p1_;
//    for(unsigned int i = 0; i < Vector::size_; ++i)
//    {
//        if(p2_[i] > res.max_[i])
//        {
//            res.max_[i] = p2_[i];
//        }
//        if(p3_[i] > res.max_[i])
//        {
//            res.max_[i] = p3_[i];
//        }
//        if(p2_[i] < res.min_[i])
//        {
//            res.min_[i] = p2_[i];
//        }
//        if(p3_[i] < res.min_[i])
//        {
//            res.min_[i] = p3_[i];
//        }
//    }
//    return res;
//}

//BoundingSphere Triangle::GetBoundingSphere() const
//{
//    BoundingSphere res;

//    res.center_ = (1.0/3.0) * (p1_+p2_+p3_);
//    res.radius_ = (res.center_ - p1_).norm();

//    return res;
//}

} // Close namespace core

} // Close namespace expressive
