
#include <core/utils/BasicsToXml.h>

namespace expressive {

namespace core {

template<typename BasePointT>
std::string PointPrimitiveT<BasePointT>::ToString() const
{
    //    string s = PointT::ToString();
    std::string s = Name();

    s+= "[";
    for (unsigned int i = 0; i < BasePointT::RowsAtCompileTime; ++i) {
        s+= std::to_string((*this)[i]) + ":";
    }
    s+= "]";

    return s;
}
///////////////////
// Xml functions //
///////////////////

template<typename BasePointT>
void PointPrimitiveT<BasePointT>::AddXmlAttributes(TiXmlElement* element) const
{
    TiXmlElement * element_position = PointToXml(*this, "position");
    element->LinkEndChild(element_position);
}

template<typename BasePoint>
PointPrimitiveT<BasePoint> &PointPrimitiveT<BasePoint>::GetPoint(unsigned int /*index*/)
{
    return *this;
}

template<typename BasePoint>
unsigned int PointPrimitiveT<BasePoint>::GetSize() const
{
    return 1;
}

template<typename BasePoint>
Scalar PointPrimitiveT<BasePoint>::GetLength() const
{
    return 0.0;
}

///////////////////////////
// Bounding-box function //
///////////////////////////

template<typename BasePoint>
AABBoxT<BasePoint> PointPrimitiveT<BasePoint>::GetAxisBoundingBox() const
{
    AABBoxT<BasePoint> res;
    res.min_ = *this;
    res.max_ = *this;

    return res;
}

template<typename BasePoint>
BoundingSphereT<BasePoint> PointPrimitiveT<BasePoint>::GetBoundingSphere() const
{
    BoundingSphereT<BasePoint> res(*this, 0.0);
    return res;
}

} // Close namespace core

} // Close namespace expressive
