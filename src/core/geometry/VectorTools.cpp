#include <core/geometry/VectorTools.h>

namespace expressive {

namespace VectorTools {

double angle2d(const Point2 &u, const Point2 &v)
{
    double t = atan2(u[0] * v[1] - u[1] * v[0], u[0] * v[0] + u[1] * v[1]);
    return t > 0 ? t : t + 2 * (double)(M_PI);
}

Vector2 GetOrthogonalRandVect(const Vector2& other)
{
    Vector2 res;
    res[0] = -other[1];
    res[1] = other[0];
    return res;
}

Vector GetOrthogonalRandVect(const Vector& other)
{
    Vector res;
    Vector base_1 = Vector::Zero();
    base_1[0] = 1.0;
    res = base_1.cross(other);

    if(res.squaredNorm() < 0.001)
    {
        Vector base_2 = Vector::Zero();
        base_2[1] = 1.0;
        return base_2.cross(other);
    }
    else
    {
        return res;
    }
}

Vector4 GetOrthogonalRandVect(const Vector4& other)
{
    Vector4 res;
    res = Vector::Zero();
    res[0] = -other[1];
    res[1] = other[0];
    res[2] = -other[3];
    res[3] = other[2];
    return res;
}

bool ComputePlanePlaneIntersection(const Point &p1, const Vector &in_dir1, const Point &p2, const Vector &in_dir2, Point &out_pos, Point &out_dir)
{
    // First compute the normal of the plane
    Vector dir1 = in_dir1.normalized();
    Vector dir2 = in_dir2.normalized();
    out_dir = dir1.cross(dir2); // direction of the intersecting line is just the normal to both plane normals.

    // Then we need to find a point that is on both planes.

    // plane equation is ax + by + cz + d = 0, where (a,b,c) is the plane's normal, and d is the distance to the origin.
    // So we have to solve the following :
    // plane 1: a1x + b1y + c1z + d1 = 0
    // plane 2: a2x + b2y + c2z + d2 = 0
    // Just pick any X, and find the other values.
    // However, this gives a potential issue when planes are parallels, which needs to be tested.

    Scalar d1 = p1.norm(); // distance to origin for plane 1
    Scalar d2 = p2.norm(); // distance to origin for plane 2

    Scalar det = out_dir.squaredNorm(); // determinant of this vector.

    if (std::abs(det) < 0.0000005) { // checking if planes are parallel.
        out_pos = Point::Zero();
        out_dir = Vector::Zero();
        return false;
    }

    // The intersection point is simply :
    out_pos = ((dir2.cross(out_dir) * d1) + (out_dir.cross(dir1) * d2)) / det;
    out_dir.normalize();

    return true;
}

Transform ComputeRotation(const Vector &source, const Vector &target)
{
    Vector v = source.cross(target);
    Scalar c = source.dot(target);
    Scalar k = 1.0 / (1.0 + c);

    return Transform(v[0] * v[0] * k + c,    v[1] * v[0] * k - v[2], v[2] * v[0] * k + v[1], 0.0,
           v[0] * v[1] * k + v[2], v[1] * v[1] * k + c,    v[2] * v[1] * k - v[0], 0.0,
           v[0] * v[2] * k - v[1], v[1] * v[2] * k + v[0], v[2] * v[2] * k + c, 0.0,
           0.0, 0.0, 0.0, 1.0);
}

Transformation ComputeTransformation(const Point &start, const Point &end)
{
    Transformation res;

    res.translation_ = start;
    Vector unit_dir = (end - start);
    res.scale_[0] = unit_dir.norm();
    unit_dir.normalize();
    res.scale_[1] = 1.0;//(Scalar) this->model_size_;
    res.scale_[2] = 1.0;//(Scalar) this->model_size_;
    res.axis_rot_ = Vector(1.0,0.0,0.0).cross(unit_dir);
    Scalar rot_norm = res.axis_rot_.norm();
    if(rot_norm != 0.0)
    {
        res.axis_rot_ = (1.0/rot_norm)*res.axis_rot_; // Normalize
        res.angle_ = asin(rot_norm);
        if(unit_dir[0] < 0.0)    // == dot product between translation_ and (1.0,0.0,0.0)
        {
            res.angle_ = M_PI - res.angle_;
        }
    }
    else
    {
        res.axis_rot_ = Vector(0.0,0.0,1.0);
        if(unit_dir[0] < 0.0)
        {
            res.angle_ = M_PI;
        }
        else
        {
            res.angle_ = 0.0;
        }
    }

    return res;
}

Point GetPointInObjectFrame(const Point& p, const Transformation &t) {
    Point trans_starting_point = p - t.translation_;

    if(t.angle_ != 0.0 && (t.axis_rot_[0] != 0.0 || t.axis_rot_[1] != 0.0 || t.axis_rot_[2] != 0.0))
    {
        trans_starting_point = VectorTools::Rotate(trans_starting_point, t.axis_rot_, -t.angle_);
    }
    trans_starting_point[0] *= 1.0/t.scale_[0];
    trans_starting_point[1] *= 1.0/t.scale_[1];
    trans_starting_point[2] *= 1.0/t.scale_[2];

    return trans_starting_point;
}

} // namespace VectorTools

} // namespace expressive
