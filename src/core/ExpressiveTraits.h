/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_TRAITS_H_
#define EXPRESSIVE_TRAITS_H_

#include "CoreRequired.h"
#include "core/VectorTraits.h"

#include <vector>
#include <map>

namespace expressive 
{
    /*! \brief Default Traits structure for Expressive in double precision. Based on OpenMesh classes.
    */
    struct CORE_API ExpressiveTraits3d
    {
        typedef expressive::Scalar Scalar;
        typedef expressive::Vector Vector;

        // This enumerate type defines Scalar and Vector properties that will be present in all ScalarField.
        enum ESFScalarProperties {
            E_Red   = 0,
            E_Green = 1,
            E_Blue  = 2,
            SF_SCALAR_PROPERTIES_CARDINAL = 3  // This is useful to have the number of element in this enum type, keep it up to date.
        };
        enum ESFVectorProperties {
            E_Direction = 0, SF_VECTOR_PROPERTIES_CARDINAL = 1 // This is useful to have the number of element in this enum type, keep it up to date.
        };

        //TODO:@todo C++0x? : remove that and maybe use a std::array?
        // Scalar
        static const std::vector<ESFScalarProperties> kAllScalarProperties;
        static std::vector<ESFScalarProperties> Init_kAllScalarProperties();
        // Vector
        static const std::vector<ESFVectorProperties> kAllVectorProperties;
        static std::vector<ESFVectorProperties> Init_kAllVectorProperties();

    public:
        // The 2 following functions help to get the enum rank from a string or the string from the enum rank.
        // Scalar
        static const std::string& getNameFromScalarProperty(ESFScalarProperties prop);

        static const ESFScalarProperties& getScalarPropertyFromName(const std::string& prop_name);

        // Vector
        static const std::string& getNameFromVectorProperty(ESFVectorProperties prop);
        static const ESFVectorProperties& getVectorPropertyFromName(const std::string& prop_name);

        // Arrays containing default values for Properties. See .cpp for more details.
        static const Scalar kSFScalarPropertiesDefault[SF_SCALAR_PROPERTIES_CARDINAL];
        static const Vector kSFVectorPropertiesDefault[SF_VECTOR_PROPERTIES_CARDINAL];


        // Correspondance for naming the Scalar and Vector properties
        // Here are structures to map a property to a string.
    private:
        static std::vector<std::string> kScalarPropertiesToName;
        static std::vector<std::string> Init_kScalarPropertiesToName();


        static std::map<std::string, ESFScalarProperties> kNameToScalarProperties;
        static std::map<std::string, ESFScalarProperties> Init_kNameToScalarProperties();
        static std::vector<std::string> kVectorPropertiesToName;
        static std::vector<std::string> Init_kVectorPropertiesToName();
        static std::map<std::string, ESFVectorProperties> kNameToVectorProperties;
        static std::map<std::string, ESFVectorProperties> Init_kNameToVectorProperties();
    };

    //TODO:@todo : the way we handle properties on implicit surfaces should probably be changed ...
    typedef ExpressiveTraits3d::ESFScalarProperties ESFScalarProperties;
    typedef ExpressiveTraits3d::ESFVectorProperties ESFVectorProperties;

    typedef ExpressiveTraits3d ExpressiveTraits;
}

#endif // EXPRESSIVE_TRAITS_H_
