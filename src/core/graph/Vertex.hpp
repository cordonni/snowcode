namespace expressive
{

namespace core
{

template<typename VertexType>
VertexT<VertexType>::VertexT() : owner_(nullptr), pos_(nullptr) {}

template<typename VertexType>
VertexT<VertexType>::VertexT(GraphT<VertexType> *owner, typename GraphT<VertexType>::InternalVertexId internal_id, std::shared_ptr<VertexType> pos) : owner_(owner), internal_id_(internal_id), pos_(pos) {}

template<typename VertexType>
VertexT<VertexType>::VertexT(GraphT<VertexType> *owner, typename GraphT<VertexType>::InternalVertexId internal_id, const VertexType &pos) :
    owner_(owner),
    internal_id_(internal_id),
    pos_(pos.clone())
{
}

template<typename VertexType>
VertexT<VertexType>::~VertexT()
{
    pos_ = nullptr;
}

template<typename VertexType>
const std::shared_ptr<VertexType> VertexT<VertexType>::posptr() const
{
    return pos_;
}

template<typename VertexType>
std::shared_ptr<VertexType> VertexT<VertexType>::nonconst_posptr()
{
    return pos_;
}

template<typename VertexType>
bool VertexT<VertexType>::operator==(const VertexT &other ) const
{
    return pos() == other.pos();
}

template<typename VertexType>
std::string VertexT<VertexType>::to_string() const
{
    std::ostringstream oss;
    oss << "Vertex [" << id() << ":" << internal_id_ << "]: " << (pos())[0];
    for (unsigned int i = 1; i < VertexType::RowsAtCompileTime; ++i) {
        oss << ":"<< (pos())[i];
    }

    return oss.str();
}

template<typename VertexType>
const VertexType &VertexT<VertexType>::pos() const { return *pos_; }

template<typename VertexType>
VertexType &VertexT<VertexType>::nonconst_pos() const { return *pos_; }

template<typename VertexType>
inline typename GraphT<VertexType>::VertexId VertexT<VertexType>::id() const { return id_; }

template<typename VertexType>
inline typename GraphT<VertexType>::InternalVertexId VertexT<VertexType>::internal_id() const { return internal_id_; }

template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr  VertexT<VertexType>::GetEdges() const {
    return std::make_shared<typename GraphT<VertexType>::OutEdgeIterator>(owner_, out_edges(id_, *owner_));
}

template<typename VertexType>
std::pair<typename GraphT<VertexType>::BoostAdjacentVertexIterator, typename GraphT<VertexType>::BoostAdjacentVertexIterator> VertexT<VertexType>::GetAdjacentVerticesId() const
{
    return adjacent_vertices(id_, *owner_);
}

template<typename VertexType>
typename GraphT<VertexType>::VertexIteratorPtr VertexT<VertexType>::GetAdjacentVertices() const
{
    return std::make_shared<typename GraphT<VertexType>::AdjacentVertexIterator>(owner_, adjacent_vertices(id_, *owner_));
}

template<typename VertexType>
unsigned int VertexT<VertexType>::GetEdgeCount() const { return (uint) out_degree(id_, *owner_);}

template<typename VertexType>
unsigned int VertexT<VertexType>::GetEdgeCountWithout(std::set<typename GraphT<VertexType>::EdgeId> excluded) const {
    typename Graph::EdgeIteratorPtr edges = GetEdges();
    unsigned int res = 0;
    while (edges->HasNext()) {
        auto id = edges->NextId();
        if (excluded.find(id) == excluded.end()) {
            res++;
        }
    }
    return res;
}

template<typename VertexType>
void VertexT<VertexType>::set_pos(const VertexType &pos)
{
    pos_.array() = pos.array();
//    pos_ = std::dynamic_pointer_cast<VertexType>(pos.clone());
}

template<typename VertexType>
void VertexT<VertexType>::updated()
{
    pos_->updated();
}

template<typename VertexType>
GraphT<VertexType> *VertexT<VertexType>::owner() const
{
    return owner_;
}

} // namespace core

} // namespace expressive
