#define TEMPLATE_INSTANTIATION_GRAPHPROFILE
#include "core/graph/GraphProfile.h"
#include "ork/resource/ResourceTemplate.h"

using namespace ork;
using namespace std;

namespace expressive
{

namespace core
{

GraphProfile::GraphProfile() :
    Serializable("GraphProfile")
{
    has_weights_ = false;
    is_contour_graph_ = false;
    is_loop_contour_ = true;
    default_weight_ = 1.0;
    vertex_starter_ = "";
    edge_starter_ = "";
    delimiter_ = " ";
    dimensions_ = 3;
    index_offset_ = 0; // used when index numerotation started at 1 instead of 0 in the graph file... probably useful for loading graph parts too.
    x_offset_ = 0.0;
    y_offset_ = 0.0;
    z_offset_ = 0.0;
    x_scale_ = 1.0;
    y_scale_ = 1.0;
    z_scale_ = 1.0;
    w_scale_ = 1.0;
}

GraphProfile::~GraphProfile()
{
}

std::istream& GraphProfile::safeGetline(std::istream& is, std::string& t)
{
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case EOF:
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(std::ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }
//    return is;
}

void GraphProfile::swap(std::shared_ptr<GraphProfile> &other)
{
    std::swap(has_weights_, other->has_weights_);
    std::swap(is_contour_graph_, other->is_contour_graph_);
    std::swap(is_loop_contour_, other->is_loop_contour_);
    std::swap(default_weight_, other->default_weight_);
    std::swap(vertex_starter_, other->vertex_starter_);
    std::swap(edge_starter_, other->edge_starter_);
    std::swap(delimiter_, other->delimiter_);
    std::swap(dimensions_, other->dimensions_);
    std::swap(index_offset_, other->index_offset_);
    std::swap(x_offset_, other->x_offset_);
    std::swap(y_offset_, other->y_offset_);
    std::swap(z_offset_, other->z_offset_);
    std::swap(x_scale_, other->x_scale_);
    std::swap(y_scale_, other->y_scale_);
    std::swap(z_scale_, other->z_scale_);
    std::swap(w_scale_, other->w_scale_);
}

template<>
WeightedGraph2D::VertexPtr GraphProfile::AddVertex<WeightedGraph2D>(std::shared_ptr<WeightedGraph2D> graph, Scalar x, Scalar y, Scalar /*z*/, Scalar w)
{
    return graph->Add(WeightedPoint2D(x + x_offset_, y + y_offset_, w), false);
}

template<>
Graph2D::VertexPtr GraphProfile::AddVertex<Graph2D>(std::shared_ptr<Graph2D> graph, Scalar x, Scalar y, Scalar /*z*/, Scalar /*w*/)
{
    return graph->Add(Point2(x + x_offset_, y + y_offset_), false);
}

template<>
WeightedGraph::VertexPtr GraphProfile::AddVertex<WeightedGraph>(std::shared_ptr<WeightedGraph> graph, Scalar x, Scalar y, Scalar z, Scalar w)
{
    return graph->Add(WeightedPoint(x + x_offset_, y + y_offset_, z + z_offset_, w), false);
}

template<>
Graph::VertexPtr GraphProfile::AddVertex<Graph>(std::shared_ptr<Graph> graph, Scalar x, Scalar y, Scalar z, Scalar /*w*/)
{
    return graph->Add(Point(x + x_offset_, y + y_offset_, z + z_offset_), false);
}

template<>
Scalar GraphProfile::GetWeight<WeightedGraph2D>(WeightedGraph2D::VertexPtr vertex)
{
    return vertex->pos().weight();
}

template<>
Scalar GraphProfile::GetWeight<WeightedGraph>(WeightedGraph::VertexPtr vertex)
{
    return vertex->pos().weight();
}

//std::shared_ptr<Serializable> GraphProfile::LoadGraph(const string &filename)
//{
//    string line;
//    ifstream myfile(filename);
//    unsigned int n_vertices;
//    unsigned int n_edges;

//    if (myfile.is_open()) {
//        bool readSizes = (vertex_starter_ != 'v');

//        if (readSizes) {
//            getline(myfile, line);
//            sscanf(line.c_str(), "%d", &n_vertices);
//            getline(myfile, line);
//            sscanf(line.c_str(), "%d", &n_edges);
//            if (n_vertices == 0) {
//                Logger::ERROR_LOGGER->log("GRAPH", "Error : no vertices read from file");
//                return res;
//            }
//        }

//        if (dimensions_ == 2) {
//            std::shared_ptr<WeightedGraph2D> res = make_shared<WeightedGraph2D>();
//            std::vector<WeightedGraph2D::VertexId> points;

//        } else if (dimensions == 3) {

//        }

//        for (unsigned int i = 0; i < n_vertices; ++i) {
//            if(getline(myfile, line)) {
//                float x, y, z, w;
//                UNUSED(z);
//                if (has_weights_) {
//                    sscanf(line.c_str(), "%f,%f,%f", &x, &y, &w); // TODO (antoine) : use regex expressions here.
//                } else {
//                    sscanf(line.c_str(), "%f,%f", &x, &y);
//                    w = default_weight_;
//                }
//                auto v = res->Add(WeightedPoint2D(x + x_offset_, y + y_offset_, w), false);
//                points.push_back(v->id());
//            }
//        }

//        for (unsigned int i = 0; i < n_edges; ++i) {
//            if (getline(myfile, line)) {
//                int i, j;
//                sscanf(line.c_str(), "%d,%d", &i, &j);
//                auto s = res->AddSegment(points[i - index_offset_], points[j - index_offset_], false);
//            }
//        }
//    } else {
//        Logger::ERROR_LOGGER->logf("GRAPH", "Error : Couldn't open file %s", filename.c_str());
//    }

//    return res;
//}

/// @cond RESOURCES
class GraphProfileResource : public ResourceTemplate<20, GraphProfile>
{
public:
    GraphProfileResource(ptr<ResourceManager> manager, const string &name, ptr<ResourceDescriptor> desc, const TiXmlElement *e = NULL) :
        ResourceTemplate<20, GraphProfile>(manager, name, desc)
    {
        e = e == NULL ? desc->descriptor : e;

        checkParameters(desc, e, "name,default_weight,has_weights,dimensions,delimiter,vertex_starter,edge_starter,n_vertices,n_edges,index_offset,x_offset,y_offset,x_scale,y_scale,z_scale,w_scale,is_contour_graph,is_loop_contour,");

        /**
         * This method loads a Graph Profile.
         * - Some tags can be added to the xml node :
         * - file must refer to a txt file containing the graph vertices, in ascii format.
         * - has_weights & default_weights : Those are used if weights are included in the file or if they must be set to a default value.
         * - delimiter, vertex_starter, edge_starter : It's also able to get input from various formats (starting with 'v' or 'e' for vertices and edges, or if it's not, it must have either in the xml or in the .txt file
         * the amount of vertices and edges.
         */

        default_weight_ = 1.0;
        has_weights_ = false;
        is_contour_graph_ = false;
        is_loop_contour_ = true;
        dimensions_ = 3;
        delimiter_ = " ";
        vertex_starter_ = "";
        edge_starter_ = "";
        index_offset_ = 0; // used when index numerotation started at 1 instead of 0 in the graph file... probably useful for loading graph parts too.
        x_offset_ = 0.0;
        y_offset_ = 0.0;
        z_offset_ = 0.0;
        x_scale_ = 1.0;
        y_scale_ = 1.0;
        z_scale_ = 1.0;
        w_scale_ = 1.0;


        if (e->Attribute("has_weights") != nullptr) {
            has_weights_ = getParameter(desc, e, "has_weights") == "true";
        } else if (e->Attribute("default_weight") != nullptr) {
            getFloatParameter(desc, e, "default_weight", &default_weight_);
        }


        if (e->Attribute("is_contour_graph") != nullptr) {
            is_contour_graph_ = getParameter(desc, e, "is_contour_graph") == "true";
            if (e->Attribute("is_loop_contour") != nullptr) {
                is_loop_contour_= getParameter(desc, e, "is_loop_contour") == "true";
            }
        }

        if (e->Attribute("vertex_starter") != nullptr) {
            vertex_starter_ = e->Attribute("vertex_starter");
        }

        if (e->Attribute("edge_starter") != nullptr) {
            edge_starter_ = e->Attribute("edge_starter");
        }

        if (e->Attribute("delimiter") != nullptr) {
            delimiter_ = e->Attribute("delimiter");
        }

        if (e->Attribute("dimensions") != nullptr) {
            getIntParameter(desc, e, "dimensions", &dimensions_);
        }

        if (e->Attribute("index_offset") != nullptr) {
            getIntParameter(desc, e, "index_offset", &index_offset_);
        }

        if (e->Attribute("x_offset") != nullptr) {
            getFloatParameter(desc, e, "x_offset", &x_offset_);
        }

        if (e->Attribute("y_offset") != nullptr) {
            getFloatParameter(desc, e, "y_offset", &y_offset_);
        }

        if (e->Attribute("z_offset") != nullptr) {
            getFloatParameter(desc, e, "z_offset", &z_offset_);
        }

        if (e->Attribute("x_scale") != nullptr) {
            getFloatParameter(desc, e, "x_scale", &x_scale_);
        }

        if (e->Attribute("y_scale") != nullptr) {
            getFloatParameter(desc, e, "y_scale", &y_scale_);
        }

        if (e->Attribute("z_scale") != nullptr) {
            getFloatParameter(desc, e, "z_scale", &z_scale_);
        }

        if (e->Attribute("w_scale") != nullptr) {
            getFloatParameter(desc, e, "w_scale", &w_scale_);
        }

    }
};

extern const char graphProfile[] = "graphProfile";

static ResourceFactory::Type<graphProfile, GraphProfileResource> GraphProfileType;

/// @endcond

} // namespace core

} // namespace expressive
