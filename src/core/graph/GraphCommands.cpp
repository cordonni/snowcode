#define TEMPLATE_INSTANTIATION_GRAPHCOMMANDS
#include <core/graph/GraphCommands.h>

#ifndef _WIN32
template class expressive::core::AddEdgeCommand<expressive::core::WeightedPoint>;
template class expressive::core::AddEdgeCommand<expressive::core::WeightedPoint2D>;
template class expressive::core::AddEdgeCommand<expressive::core::PointPrimitive>;
template class expressive::core::AddEdgeCommand<expressive::core::PointPrimitive2D>;

template class expressive::core::ChangeVerticesWeightCommand<expressive::core::WeightedPoint>;
template class expressive::core::ChangeVerticesWeightCommand<expressive::core::WeightedPoint2D>;

template class expressive::core::DeletePrimitiveCommand<expressive::core::WeightedPoint>;
template class expressive::core::DeletePrimitiveCommand<expressive::core::WeightedPoint2D>;
template class expressive::core::DeletePrimitiveCommand<expressive::core::PointPrimitive>;
template class expressive::core::DeletePrimitiveCommand<expressive::core::PointPrimitive2D>;

template class expressive::core::GraphCommand<expressive::core::WeightedPoint>;
template class expressive::core::GraphCommand<expressive::core::WeightedPoint2D >;
template class expressive::core::GraphCommand<expressive::core::PointPrimitive>;
template class expressive::core::GraphCommand<expressive::core::PointPrimitive2D>;

template class expressive::core::MergeVerticesCommand<expressive::core::WeightedPoint>;
template class expressive::core::MergeVerticesCommand<expressive::core::WeightedPoint2D>;
template class expressive::core::MergeVerticesCommand<expressive::core::PointPrimitive>;
template class expressive::core::MergeVerticesCommand<expressive::core::PointPrimitive2D>;

template class expressive::core::MovePointCommand<expressive::core::WeightedPoint>;
template class expressive::core::MovePointCommand<expressive::core::WeightedPoint2D>;
template class expressive::core::MovePointCommand<expressive::core::PointPrimitive>;
template class expressive::core::MovePointCommand<expressive::core::PointPrimitive2D>;

template class expressive::core::SubdivideEdgeCommand<expressive::core::WeightedPoint>;
template class expressive::core::SubdivideEdgeCommand<expressive::core::WeightedPoint2D>;
template class expressive::core::SubdivideEdgeCommand<expressive::core::PointPrimitive>;
template class expressive::core::SubdivideEdgeCommand<expressive::core::PointPrimitive2D>;

#endif

namespace expressive
{

namespace core
{

template<>
std::shared_ptr<core::GeometryPrimitiveT<core::WeightedPoint>> GraphCommand<core::WeightedPoint>::CreateSegment(const WeightedPoint &start, const WeightedPoint &end)
{
    return std::make_shared<core::WeightedSegment>(start, end);
}

template<>
bool AddEdgeCommand<core::WeightedPoint>::execute()
{
    std::shared_ptr<typename GraphType::Vertex> start = this->graph_->Get(start_);
    std::shared_ptr<typename GraphType::Vertex> end = nullptr;
    if (single_point_) {
        end = this->graph_->Add(start->pos());
        end_ = end->id();
    } else {
        end = this->graph_->Get(end_);
    }

    if (weight_ > 0) {
        core::WeightedPoint pos = end->nonconst_pos();
        pos.set_weight(weight_);
        this->graph_->MovePoint(end->id(), pos);
    }

    std::shared_ptr<core::WeightedGeometryPrimitive> newG  = nullptr;

    bool found = false;
    if (start->GetEdgeCount() > 0) {
        GraphType::EdgeIteratorPtr edges = start->GetEdges();
        while (edges->HasNext()) {

            std::shared_ptr<WeightedEdge> e = edges->Next();
            if (e->edgeptr()->GetSize() == 2) {
                newG = e->edgeptr()->clone();
                found = true;
                break;
            }
        }
    }
    if (!found) {
        std::shared_ptr<core::GeometryPrimitiveT<WeightedPoint>> wseg = this->CreateSegment(start->pos(), end->pos());
        std::shared_ptr<Functor> func = FunctorFactory::GetInstance().CreateDefaultFunctor();
        newG = std::dynamic_pointer_cast<GeometryPrimitiveT<core::WeightedPoint>>(PrimitiveFactory::GetInstance().CreateNewPrimitive(wseg, *func));
    }

    newG->RemovePoints();
    newG->AddPoint(start->pos(), 0);
    newG->AddPoint(end->pos(), 1);

    std::vector<std::shared_ptr<typename GraphType::Vertex> > vertices = {start, end};
    GraphType::EdgeIteratorPtr newEdges = this->graph_->Add(newG, vertices);

    if (newEdges->HasNext()) {
        edge_ = newEdges->Next()->id();
    }
    this->graph_->NotifyUpdate();
    return true;
}

} // namespace core

} // namespace expressive

