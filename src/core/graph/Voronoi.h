/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef VORONOIDIAGRAM_H
#define VORONOIDIAGRAM_H

#include <core/CoreRequired.h>
#include <core/geometry/Segment.h>
#include <core/geometry/Triangle.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#ifndef Q_MOC_RUN
#include <boost/polygon/voronoi.hpp>
#endif

#ifdef _MSC_VER
#pragma warning( pop )
#endif

////////////////////////////
/// TODO:@todo : should be templated by a type of point (for dimensionality)
///              and should use eigen type instead of openmesh
////////////////////////////

namespace expressive
{

namespace core
{

typedef boost::polygon::voronoi_diagram<double> boost_vd;

//struct VPoint {
//    int a;
//    int b;
//    VPoint(int x, int y) : a(x), b(y) {}
//};

//struct VSegment {
//    VPoint p0;
//    VPoint p1;
//    VSegment(int x1, int y1, int x2, int y2) : p0(x1, y1), p1(x2, y2) {}
//    VSegment(const boost_vd::edge_type *edge) :
//        p0(edge->vertex0()->x(), edge->vertex0()->y()), p1(edge->vertex1()->x(), edge->vertex1()->y()) {}
//};

}

}

namespace boost {

namespace polygon {

    template <>
    struct geometry_concept<expressive::core::PointPrimitive2D> { typedef point_concept type; };

    template <>
    struct point_traits<expressive::core::PointPrimitive2D> {
        typedef expressive::Scalar coordinate_type;

        static inline coordinate_type get(const expressive::core::PointPrimitive2D& point, orientation_2d orient) {
            return (orient == HORIZONTAL) ? point[0] : point[1];
        }
        static inline coordinate_type get(const expressive::core::PointPrimitive2D& point, orientation_3d orient) {
            int i = orient.to_int();
            return (i == HORIZONTAL) ? point[0] : (i == VERTICAL) ? point[1] : point[2];
        }
    };

    template <>
    struct geometry_concept<expressive::core::SegmentT<expressive::core::PointPrimitive2D> > { typedef segment_concept type; };

    template <>
    struct segment_traits<expressive::core::SegmentT<expressive::core::PointPrimitive2D> > {
        typedef expressive::Scalar coordinate_type;
        typedef expressive::core::PointPrimitive2D point_type;

        static inline point_type get(const expressive::core::SegmentT<expressive::core::PointPrimitive2D>& segment, direction_1d dir) {
            return dir.to_int() ? segment.p2() : segment.p1();
        }
    };
} // namespace polygon

} // namespace boost


namespace expressive {

namespace core
{

// TODO : How to add templated stuff here?
/**
  * VoronoiDiagram encapsulates boost voronoi diagrams.
  * It offers a simple interface to describe a "graph" (a set of points & vertices) and
  * create a voronoi diagram from it.
  */
class CORE_API VoronoiDiagram
{
public:
    typedef boost_vd::cell_type Cell;
    typedef boost_vd::edge_type Edge;
    typedef boost_vd::vertex_type Vertex;


    typedef boost_vd::cell_type::source_category_type CellSourceCategory;
    typedef boost_vd::cell_type::source_index_type CellSourceIndex;

    VoronoiDiagram();

    ~VoronoiDiagram();

    void addPoint(const expressive::core::PointPrimitive2D& p);

    void addSegment(const expressive::core::PointPrimitive2D& p1, const expressive::core::PointPrimitive2D& p2);

    void construct(std::vector<Segment2D> &res); //< Constructs the voronoi diagram.

    void triangulate(std::vector<core::Triangle2D> &res); //< Triangulates the points based on a voronoi diagram.

    boost_vd &result(); //< onces the construct or triangulate have been called, this will yield the results.

    expressive::core::PointPrimitive2D getCellSourcePos(const Cell *cell);

protected:

    boost_vd vd_;

    std::vector<expressive::core::PointPrimitive2D> points_;

    std::vector<Segment2D> segments_;
}; // VoronoiDiagram;

} // namespace core

} // namespace expressive

#endif // VORONOIDIAGRAM_H
