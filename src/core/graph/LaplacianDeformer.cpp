#include "LaplacianDeformer.h"

#include "commons/EigenVectorTraits.h"

namespace expressive
{

namespace core
{

const std::string LaplacianDeformer::LaplacianConstrainedAttributeName = "LaplacianConstrained";
const std::string LaplacianDeformer::LaplacianWeightAttributeName = "LaplacianWeight";

LaplacianDeformer::LaplacianDeformer(std::shared_ptr<Graph> graph) :
    graph_(graph),
    L_(),
    Bx_(),
    By_(),
    Bz_()
{
    NewGraph(graph);
}

LaplacianDeformer::~LaplacianDeformer()
{
}

void LaplacianDeformer::NewGraph(std::shared_ptr<Graph> graph)
{
    graph_ = graph;

    unsigned int n = graph->GetVertexCount();

    Bx_.resize(n);
    By_.resize(n);
    Bz_.resize(n);

    L_.resize(n, n);

//    ComputeB(graph_, Bx_, By_, Bz_);
//    ComputeL(graph_, L_);
}

void LaplacianDeformer::DeformGraph(unsigned int nIterations)
{
    UNUSED(nIterations);
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> LWithConstraint = L_;
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> BxWithConstraint = Bx_;
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> ByWithConstraint = By_;
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> BzWithConstraint = Bz_;

    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> A_;
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> B_;

    VertexPtr v = graph_->GetVertices()->Next();
    ComputeA(v, A_);
    ComputeB(v, B_);

//    for (unsigned i = 0; i < nIterations; ++i)
//    {
////        ComputeRotation(graph_);
//        ComputeNewPositions(graph_, L_, Bx_, By_, Bz_);
//    }
////    return graph_;
}

void LaplacianDeformer::ComputeB(std::shared_ptr<Vertex> v, MatXX1 &b)
{
    b.resize(3 + v->GetEdgeCount() * 3);
    b.block(0, 0, 3, 1) << EigenVectorTraits::Vector(v->pos()[0], v->pos()[1], v->pos()[2]);

    unsigned int i = 1;
    Graph::VertexIteratorPtr it = v->GetAdjacentVertices();
    while (it->HasNext()) {
        VertexPtr v2 = it->Next();
        b.block(3 * i, 0, 3, 1) << EigenVectorTraits::Vector(v2->pos()[0], v2->pos()[1], v2->pos()[2]);
        ++i;
    }

    cout << "b\n" << b << endl;
}

void LaplacianDeformer::ComputeA(std::shared_ptr<Vertex> v, MatXXX &a)
{
    unsigned int n = v->GetEdgeCount();
    a.resize((n + 1) * 3, 7);

    const Point &tmpP = v->pos();
    EigenVectorTraits::Vector p(tmpP[0], tmpP[1], tmpP[2]);

    a.block(0, 0, 3, 1) << p;
    a.block(0, 1, 3, 3) <<      0,  p[2], -p[1],
                            -p[2],     0,  p[0],
                             p[1], -p[1],     0;
    a.block(0, 4, 3, 3) = Eigen::Matrix<Scalar, 3, 3>::Identity();

    unsigned int i = 1;
    Graph::VertexIteratorPtr it = v->GetAdjacentVertices();
    while (it->HasNext()) {
        VertexPtr v2 = it->Next();
        const Point &tmpP2 = v2->pos();
        EigenVectorTraits::Vector p2(tmpP2[0], tmpP2[1], tmpP2[2]);
        a.block(i * 3, 0, 3, 1) << p2;
        a.block(i * 3, 1, 3, 3) <<       0,  p2[2], -p2[1],
                                    -p2[2],      0,  p2[0],
                                     p2[1], -p2[1],     0;
        a.block(i * 3, 4, 3, 3) = Eigen::Matrix<Scalar, 3, 3>::Identity();
    }
}

//void LaplacianDeformer::ComputeL(std::shared_ptr<Graph> graph, MatXXX &l)
//{
//    unsigned int n = graph->GetVertexCount();
//    l.resize(n, n);
//    for (unsigned int i = 0; i < n; ++i) {
//        for (unsigned int j = 0; j < n; ++j) {
//            l(i, j) = 0.f;
//        }
//    }

//    Graph::VertexIterator vit = graph->GetVertices();
//    while (vit.HasNext()) {
//        VertexPtr v = vit.Next();
//        long int id = (long int) v->id();

//        l(id, id) = 1;
//        float nNeighbor = v->GetEdgeCount();

//        Graph::AdjacentVertexIterator it = v->GetAdjacentVertices();
//        while (it.HasNext()) {
//            VertexPtr v2 = it.Next();
//            long int id2 = (long int) v2->id();
//            l(id, id2) = -1.0 / nNeighbor;
//        }
//    }
//}

//struct solve_data
//{
//    Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& p;
//    const Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& LWithConstraint;
//    const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& BWithConstraint;
//};

//static void* solve_function(void* data)
//{
//    solve_data* d = (solve_data*) data;
//    d->p = d->LWithConstraint.ldlt().solve(d->BWithConstraint);
//    return NULL;
//}

//template<typename G>
//class MinOneConstraintDFS : public boost::default_dfs_visitor
//{
//private :
//    G& m_graph;
//    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& m_L;
//    Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& m_Bx;
//    Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& m_By;
//    bool m_isOneConstraint;
//public :
//    MinOneConstraintDFS(G& graph,
//            Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& L,
//            Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& Bx,
//            Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& By) :
//                m_graph(graph), m_L(L), m_Bx(Bx), m_By(By), m_isOneConstraint(false) {}

//    template < typename Vertex, typename Graph >
//    void discover_vertex(Vertex u, const Graph& g)
//    {
//        if (m_graph[u]->GetAttribute(LaplacianDeformer::LaplacianConstrainedAttributeName) > 0.0)
//        {
//            m_isOneConstraint = true;
//            m_L(u, u) += 1.f;
//            m_Bx(u) += m_graph[u]->pos()(0);
//            m_By(u) += m_graph[u]->pos()(1);
//        }
//    }

//    template < typename Vertex, typename Graph >
//    void start_vertex(Vertex u, const Graph& g)
//    {
//        m_isOneConstraint = false;
//    }

//    template < typename Vertex, typename Graph >
//    void finish_vertex(Vertex u, const Graph& g)
//    {
//        if (!m_isOneConstraint)
//        {
//            m_L(u, u) += 1;
//            m_Bx(u) += m_graph[u]->pos()(0);
//            m_By(u) += m_graph[u]->pos()(1);
//        }
//    }
//};

//void LaplacianDeformer::ComputeNewPositions(std::shared_ptr<Graph> graph, MatXXX &l, MatXX1 &Bx, MatXX1 &By, MatXX1 &Bz )
//{
//    // Computing new coordinates.
//    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> LWithConstraint = l;
//    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> BxWithConstraint = Bx;
//    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> ByWithConstraint = By;
//    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> BzWithConstraint = Bz;

//    ComputeB(graph, BxWithConstraint, ByWithConstraint, BzWithConstraint);

//    unsigned int nbVertices = graph->GetVertexCount();

////    MinOneConstraintDFS<Graph> mocdfs(*(graph.get()), LWithConstraint, BxWithConstraint, ByWithConstraint);
////    boost::depth_first_search(*(graph.get()), visitor(mocdfs));

//    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> px; px.resize(nbVertices);
//    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> py; py.resize(nbVertices);
//    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> pz; pz.resize(nbVertices);

//    pthread_t solvex;
//    solve_data datax = {px,  LWithConstraint, BxWithConstraint};
//    pthread_create(&solvex,NULL, &solve_function, &datax);

//    pthread_t solvey;
//    solve_data datay = {py,  LWithConstraint, ByWithConstraint};
//    pthread_create(&solvey,NULL, &solve_function, &datay);

//    pthread_t solvez;
//    solve_data dataz = {pz,  LWithConstraint, BzWithConstraint};
//    pthread_create(&solvez,NULL, &solve_function, &dataz);

//    pthread_join(solvex, NULL);
//    pthread_join(solvey, NULL);
//    pthread_join(solvez, NULL);

//    Graph::VertexIterator vit = graph->GetVertices();
//    while (vit.HasNext()) {
//        VertexPtr v = vit.Next();
//        long int id = (long int) v->id();
//        if (v->GetAttribute(LaplacianDeformer::LaplacianConstrainedAttributeName) < 1.0) {
//            Point &p = v->nonconst_pos();
//            p[0] = px[id];
//            p[1] = py[id];
//            p[2] = pz[id];
//        }
//    }
//}

//void LaplacianDeformer::ComputeWeights(std::shared_ptr<Graph> graph, MatXX1 &Bx, MatXX1 &By, MatXX1 &Bz)
//{
//    Graph::VertexIterator vit = graph->GetVertices();
//    while (vit.HasNext()) {
//        VertexPtr v = vit.Next();
//        Point p = Point();
//        Graph::AdjacentVertexIterator it = v->GetAdjacentVertices();
//        while (it.HasNext()) {
//            VertexPtr n = it.Next();
//            p += n->pos();
//        }

//        p = v->pos() - (p / v->GetEdgeCount());
//        Bx[(long int)v->id()] = p[0];
//        By[(long int)v->id()] = p[1];
//        Bz[(long int)v->id()] = p[2];
//    }
//}

//void LaplacianDeformer::ComputeB(std::shared_ptr<Graph> graph, MatXX1 &Bx, MatXX1 &By, MatXX1 &Bz)
//{
//    Graph::VertexIterator vit = graph->GetVertices();
//    while (vit.HasNext()) {
//        VertexPtr v = vit.Next();
//        const Point &p = v->pos();
//        Bx[(long int)v->id()] = p[0];
//        By[(long int)v->id()] = p[1];
//        Bz[(long int)v->id()] = p[2];
//    }
//}

} // namespace core

} // namespace expressive
