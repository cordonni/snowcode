/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GRAPHPROFILE_H
#define GRAPHPROFILE_H

// core dependencies
#include "core/graph/Graph.h"

// std dependencies
#include <cstring>
#include <fstream>

namespace expressive
{

namespace core
{

/**
 * The GraphProfile class describes how a graph is stored in a file.
 * It can then be used to actually load or save a graph file.
 */
class CORE_API GraphProfile : public Serializable
{
public:
    GraphProfile();

    virtual ~GraphProfile();

    template<typename GraphT>
    std::shared_ptr<GraphT> LoadGraph(const std::string &filename); //< Loads a graph using this GraphProfile.

    template<typename GraphT>
    void SaveGraph(const std::string &filename, std::shared_ptr<GraphT> graph); //< Saves a graph using this GraphProfile.

    void swap(std::shared_ptr<GraphProfile> &);

protected:
    /**
     * Adds a vertex to the loaded graph.
     */
    template<typename GraphT>
    typename GraphT::VertexPtr AddVertex(std::shared_ptr<GraphT> graph, Scalar x, Scalar y, Scalar z, Scalar w) { UNUSED(x);UNUSED(y);UNUSED(z);UNUSED(w);}

    /**
     * Returns the weight of a given vertex.
     */
    template<typename GraphT>
    Scalar GetWeight(typename GraphT::VertexPtr vertex) { UNUSED(vertex); return 1.0; }

    std::istream& safeGetline(std::istream& is, std::string& t);

    bool has_weights_; //< true if vertices have a weight.

    bool is_contour_graph_; //< true if the graph vertices are ordered in the file.

    bool is_loop_contour_; //< true if the graph should be closed.

    float default_weight_; //< default weight value.

    std::string vertex_starter_; //< string that should appear in the file before every vertex.

    std::string edge_starter_; //< string that should appear in the file before every edge.

    std::string delimiter_; //< string that should appear between each value

    int dimensions_; //< dimension of the graph's space.

    int index_offset_;  //< if vertices numbering doesn't start at 0, this is used to mark it.

    float x_offset_; //< to recenter the graph

    float y_offset_; //< to recenter the graph

    float z_offset_; //< to recenter the graph

    float x_scale_; //< to scale the graph

    float y_scale_; //< to scale the graph

    float z_scale_; //< to scale the graph

    float w_scale_; //< to scale the graph
};

template<typename GraphT>
std::shared_ptr<GraphT> GraphProfile::LoadGraph(const std::string &filename)
{
    std::string line;
    std::ifstream myfile(filename, std::ios::binary);
    unsigned int n_vertices = 0;
    unsigned int n_edges = 0;
    std::shared_ptr<GraphT> res = std::make_shared<GraphT>();
    if (myfile.is_open()) {
        bool readSizes = (vertex_starter_.size() == 0);

        if (readSizes) {
//            std::getline(myfile, line);
            safeGetline(myfile, line);
            sscanf(line.c_str(), "%d", &n_vertices);
            safeGetline(myfile, line);
            sscanf(line.c_str(), "%d", &n_edges);
        } else {
            while (!myfile.eof()) {
                safeGetline(myfile, line);
                if (line.find(vertex_starter_) != std::string::npos) {
                    n_vertices++;
                }
                else if (line.find(edge_starter_) != std::string::npos) {
                    n_edges++;
                }
            }
            myfile.clear();
            myfile.seekg(0, myfile.beg);
        }
        if (n_vertices == 0) {
            ork::Logger::ERROR_LOGGER->log("GRAPH", "Error : Could not get vertex/edge count from file. Check vertex_starter value or add vertex count in file");
            return res;
        }


        std::vector<typename GraphT::VertexId> points;
        assert(dimensions_ == GraphT::BasePoint::RowsAtCompileTime);

        std::string vertex_format = "";
        std::string edge_format = "";
        vertex_format += this->vertex_starter_;
        vertex_format += std::string("%f");
        for (int i = 1; i < dimensions_; ++i) {
            vertex_format += delimiter_ + std::string("%f");
        }
        if (has_weights_) {
            vertex_format += delimiter_ + std::string("%f");
        }

        edge_format += this->edge_starter_;
        edge_format += std::string("%d") + delimiter_ + std::string("%d");

        ork::Logger::INFO_LOGGER->logf("GRAPH", "Loading %d vertices & %d edges.", n_vertices, n_edges);
        ork::Logger::INFO_LOGGER->logf("GRAPH", "Vertex format : %s - Edge format : %s", vertex_format.c_str(), edge_format.c_str());

        for (unsigned int i = 0; i < n_vertices; ++i) {
            if(safeGetline(myfile, line)) {
                float x = 0.f, y = 0.f, z = 0.f, w = 0.f;
                typename GraphT::VertexPtr v = nullptr;
                if (dimensions_ == 2 && has_weights_) {
                    sscanf(line.c_str(), vertex_format.c_str(), &x, &y, &w);
                } else if (dimensions_ == 2 && !has_weights_) {
                    sscanf(line.c_str(), vertex_format.c_str(), &x, &y);
                    w = default_weight_;
                } else if (dimensions_ == 3 && has_weights_)  {
                    sscanf(line.c_str(), vertex_format.c_str(), &x, &y, &z, &w);
                } else if (dimensions_ == 3 && !has_weights_) {
                    sscanf(line.c_str(), vertex_format.c_str(), &x, &y, &z);
                    w = default_weight_;
                } else {
                    ork::Logger::ERROR_LOGGER->logf("GRAPH", "Error : graph dimension not handled yet (%d).", dimensions_);
                }

                v = this->AddVertex<GraphT>(res, x * x_scale_, y * y_scale_, z * z_scale_, w * w_scale_);
                points.push_back(v->id());
            }
        }

        for (unsigned int i = 0; i < n_edges; ++i) {
            if (safeGetline(myfile, line)) {
                int i, j;
                sscanf(line.c_str(), edge_format.c_str(), &i, &j);
                auto s = res->AddSegment(points[i - index_offset_], points[j - index_offset_], true);
            }
        }
        if (points.size() == 0) {
            ork::Logger::ERROR_LOGGER->logf("GRAPH", "Error while loading %s : no points loaded", filename.c_str());
        }
        if (n_edges == 0 && is_contour_graph_) {
            for (unsigned int i = 1; i < points.size(); ++i) {
                res->AddSegment(points[i - 1], points[i], true);
            }
            if (is_loop_contour_) {
                res->AddSegment(points[points.size() - 1], points[0], true);
            }
        }
    } else {
        ork::Logger::ERROR_LOGGER->logf("GRAPH", "Error : Couldn't open file %s", filename.c_str());
    }

    return res;
}

template<typename GraphT>
void GraphProfile::SaveGraph(const std::string &filename, std::shared_ptr<GraphT> graph)
{
    std::string line;
    std::ofstream myfile(filename);
    unsigned int n_vertices = graph->GetVertexCount();
    unsigned int n_edges = graph->GetEdgeCount();

    std::map<typename GraphT::VertexId, unsigned int> points;
    if (myfile.is_open()) {
        bool readSizes = (vertex_starter_.size() == 0);

        if (readSizes) {
            myfile << n_vertices << std::endl;
            myfile << n_edges << std::endl;
        }

        ork::Logger::INFO_LOGGER->logf("GRAPH", "Saving %d vertices & %d edges.", n_vertices, n_edges);

        typename GraphT::VertexIteratorPtr vertices = graph->GetVertices();
        unsigned int index = 0;
        while (vertices->HasNext()) {
            typename GraphT::VertexPtr vertex = vertices->Next();
            myfile << this->vertex_starter_;
            for (int i = 0; i < dimensions_; ++i) {
                myfile << vertex->pos()[i];
                if (i < dimensions_ - 1) {
                    myfile << delimiter_;
                }
            }
            if (has_weights_) {
                myfile << delimiter_ << GetWeight<GraphT>(vertex);
            }
            myfile << std::endl;
            points[vertex->id()] = index;
            index++;
        }

        typename GraphT::EdgeIteratorPtr edges = graph->GetEdges();
        while (edges->HasNext()) {
            typename GraphT::EdgePtr edge = edges->Next();
            myfile << edge_starter_ << points[edge->GetStartId()] + index_offset_ << delimiter_ << points[edge->GetEndId()] + index_offset_ << std::endl;
        }
        myfile.close();
    } else {
        ork::Logger::ERROR_LOGGER->logf("GRAPH", "Error : Couldn't open file %s", filename.c_str());
    }
}

template<>
WeightedGraph2D::VertexPtr CORE_API GraphProfile::AddVertex<WeightedGraph2D>(std::shared_ptr<WeightedGraph2D> graph, Scalar x, Scalar y, Scalar /*z*/, Scalar w);

template<>
Graph2D::VertexPtr CORE_API GraphProfile::AddVertex<Graph2D>(std::shared_ptr<Graph2D> graph, Scalar x, Scalar y, Scalar /*z*/, Scalar /*w*/);
template<>
WeightedGraph::VertexPtr CORE_API GraphProfile::AddVertex<WeightedGraph>(std::shared_ptr<WeightedGraph> graph, Scalar x, Scalar y, Scalar z, Scalar w);

template<>
Graph::VertexPtr CORE_API GraphProfile::AddVertex<Graph>(std::shared_ptr<Graph> graph, Scalar x, Scalar y, Scalar z, Scalar /*w*/);

template<>
Scalar CORE_API GraphProfile::GetWeight<WeightedGraph2D>(WeightedGraph2D::VertexPtr vertex);

template<>
Scalar CORE_API GraphProfile::GetWeight<WeightedGraph>(WeightedGraph::VertexPtr vertex);

    //template<typename GraphT>
//Scalar GraphProfile::GetWeight(typename GraphT::VertexPtr /*vertex*/)
//{
//    return default_weight_;
//}

//template<>
//Scalar GraphProfile::GetWeight<WeightedGraph2D>(WeightedGraph2D::VertexPtr vertex);

//template<>
//Scalar GraphProfile::GetWeight<WeightedGraph>(WeightedGraph::VertexPtr vertex);

} // namespace core

} // namespace expressive

#endif // GRAPHPROFILE_H
