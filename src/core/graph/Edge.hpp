#ifndef EDGE_H
#define EDGE_H

namespace expressive
{

namespace core
{

template<typename VertexType>
EdgeT<VertexType>::EdgeT() :
    owner_(nullptr),
    edge_(nullptr),
    internal_id_(-1),
    index_(-1),
    start_(-1),
    end_(-1),
    type_(E_BASIC)
{
}

template<typename VertexType>
EdgeT<VertexType>::EdgeT(Graph *owner, const std::shared_ptr<EdgeType> edge, typename Graph::InternalEdgeId internalId, unsigned int index, typename Graph::VertexId start, typename Graph::VertexId end) :
    owner_(owner),
    edge_(edge),
    internal_id_(internalId),
    index_(index),
    start_(start),
    end_(end),
    type_(E_BASIC)
{
//    for (unsigned int i = 0; i < edge_->GetSize() - 2; ++i) {
//        point_attributes_.push_back(std::map<std::string, Scalar>());
//    }
}

//template<typename VertexType>
//EdgeT<VertexType>::EdgeT(Graph *owner, std::shared_ptr<EdgeType> edge) :
//    owner_(owner),
//    edge_(edge)
//{
//  //TODO : Where are start_ and end_ ???!!
//}

template<typename VertexType>
EdgeT<VertexType>::~EdgeT()
{
    edge_ = nullptr;
}

template<typename VertexType>
bool EdgeT<VertexType>::operator==(const EdgeT &other ) const
{
    return edge() == other.edge();
}

template<typename VertexType>
std::string EdgeT<VertexType>::to_string() const
{
    std::ostringstream oss;
    oss << "Edge[" << id() << ":" << internal_id_ << "] : [" << start_ << ":"<< end_ << "]";

    return oss.str();
}

template<typename VertexType>
inline typename EdgeT<VertexType>::Graph* EdgeT<VertexType>::owner() const
{
    return owner_;
}

template<typename VertexType>
inline unsigned int EdgeT<VertexType>::index() const
{
    return index_;
}

template<typename VertexType>
inline typename EdgeT<VertexType>::EdgeFlag EdgeT<VertexType>::type() const
{
    return type_;
}

template<typename VertexType>
inline void EdgeT<VertexType>::set_type(const typename EdgeT<VertexType>::EdgeFlag &type)
{
    type_ = type;
}

template<typename VertexType>
inline typename GraphT<VertexType>::EdgeId EdgeT<VertexType>::id() const { return id_; }

template<typename VertexType>
inline typename GraphT<VertexType>::InternalEdgeId EdgeT<VertexType>::internal_id() const { return internal_id_; }

template<typename VertexType>
inline const typename EdgeT<VertexType>::EdgeType &EdgeT<VertexType>::edge() const { return *edge_; }

template<typename VertexType>
inline const std::shared_ptr<typename EdgeT<VertexType>::EdgeType> EdgeT<VertexType>::edgeptr() const { return edge_; }

template<typename VertexType>
inline std::shared_ptr<typename EdgeT<VertexType>::EdgeType> EdgeT<VertexType>::nonconst_edgeptr() const { return edge_; }

template<typename VertexType>
inline typename EdgeT<VertexType>::EdgeType &EdgeT<VertexType>::nonconst_edge() const { return *edge_; }

template<typename VertexType>
unsigned int EdgeT<VertexType>::GetIndex(typename Graph::VertexId start, unsigned int index) const
{
//    return (start == start_) ? index : edge_->GetSize() - 1 - index;
    return (start == start_ ) ? index : 1 - index;
}

template<typename VertexType>
unsigned int EdgeT<VertexType>::GetPrimitiveIndex(unsigned int index) const
{
    return index_ + index;
}

template<typename VertexType>
unsigned int EdgeT<VertexType>::GetPrimitiveIndex(typename Graph::VertexId start, unsigned int index) const
{
    return index_ + GetIndex(start, index);
}

template<typename VertexType>
std::shared_ptr<typename GraphT<VertexType>::Vertex> EdgeT<VertexType>::GetStart() const
{
    return owner_->Get(start_);
}

template<typename VertexType>
std::shared_ptr<typename GraphT<VertexType>::Vertex> EdgeT<VertexType>::GetEnd() const
{
    return owner_->Get(end_);
}

template<typename VertexType>
typename GraphT<VertexType>::VertexId EdgeT<VertexType>::GetStartId() const
{
    return start_;
}

template<typename VertexType>
typename GraphT<VertexType>::VertexId EdgeT<VertexType>::GetEndId() const
{
    return end_;
}

template<typename VertexType>
typename GraphT<VertexType>::VertexId EdgeT<VertexType>::GetOppositeId(typename Graph::VertexId id) const
{
    return id == start_ ? end_ : start_;
}

template<typename VertexType>
std::shared_ptr<typename GraphT<VertexType>::Vertex> EdgeT<VertexType>::GetOpposite(typename Graph::VertexId id) const
{
    return owner_->Get(id == start_ ? end_ : start_);
}

template<typename VertexType>
std::shared_ptr<typename GraphT<VertexType>::Vertex> EdgeT<VertexType>::GetOpposite(std::shared_ptr<typename Graph::Vertex> n) const
{
    return owner_->Get(n->id() == start_ ? end_ : start_);
}

template<typename VertexType>
unsigned int EdgeT<VertexType>::GetSize() const { return edge_->GetSize(); }

template<typename VertexType>
VertexType &EdgeT<VertexType>::GetPoint(unsigned int index) const
{
    return edge_->GetPoint(index_ + index);
}

template<typename VertexType>
VertexType &EdgeT<VertexType>::GetPoint(typename Graph::VertexId start, unsigned int index) const
{
    return edge_->GetPoint(GetIndex(start, index));
}

template<typename VertexType>
typename VertexType::BasePoint EdgeT<VertexType>::GetInterpolatedPoint(Scalar ratio) const
{
    const VertexType &s = edge_->GetPoint(index_);
    const VertexType &e = edge_->GetPoint(index_+1);

    return s + (e - s) * ratio;
}

template<typename VertexType>
std::shared_ptr<typename GraphT<VertexType>::Vertex> EdgeT<VertexType>::GetVertex(unsigned int index) const
{
//    assert((int)index >= 0 && index < 2 && "Invalid index supplied for attribute");

    return owner_->Get(index == 0 ? start_ : end_);
}

template<typename VertexType>
std::shared_ptr<typename GraphT<VertexType>::Vertex> EdgeT<VertexType>::GetVertex(typename Graph::VertexId start, unsigned int index) const
{
    return GetVertex(GetIndex(start, index));
}

template<typename VertexType>
Scalar EdgeT<VertexType>::ComputeCurvilinearLength() const
{
    float l = 0.0;

    curvilinear_length_.clear();
    curvilinear_length_.push_back(l);
    VertexType a = GetStart()->pos();
    for (unsigned int i = 0; i < GetSize() - 0; i++) {
        VertexType b = GetPoint(i);//Vector2((*vertices)[i][0], (*vertices)[i][1]);
        l += (float)(b - a).norm();
        curvilinear_length_.push_back(l);
        a = b;
    }
//    l += (GetEnd()->pos() - a).norm();
    return l;
}

template<typename VertexType>
Scalar EdgeT<VertexType>::GetCurvilinearCoordinate(Scalar l, VertexType *p) const
{
    if (l <= 0) {
        if (p != NULL) {
            *p = GetStart()->pos();
//            if (n != NULL) {
//                VertexType a = GetPoint(0);
//                VertexType b = GetPoint(1);
////                *n = VertexType(a[1] - b[1], b[0] - a[0]);
//            }
        }
        return 0.0;
    } else {
        *p = GetEnd()->pos();
        return 1.0;
    }
//    if (l >= this->l) {
//        if (p != NULL) {
//            *p = getEnd()->getPos();
//            if (n != NULL) {
//                Vec2d a = getXY(getSize() - 2);
//                Vec2d b = getXY(getSize() - 1);
//                *n = Vec2d(a[1] - b[1], b[0] - a[0]);
//            }
//        }
//        return this->s1;
//    }
//    int i0 = 0;
//    int i1 = getSize() - 1;
//    float s0 = this->s0;
//    float s1 = this->s1;
//    float l0 = 0;
//    float l1 = this->l;
//    while (i1 > i0 + 1) {
//        int im = (i0 + i1) / 2;
//        float sm = getS(im);
//        float lm = getL(im);
//        if (l < lm) {
//            i1 = im;
//            s1 = sm;
//            l1 = lm;
//        } else {
//            i0 = im;
//            s0 = sm;
//            l0 = lm;
//        }
//    }
//    float c = l1 == l0 ? l0 : (l - l0) / (l1 - l0);
//    if (p != NULL) {
//        Vec2d a = getXY(i0);
//        Vec2d b = getXY(i1);
//        *p = a + (b - a) * c;
//        if (n != NULL) {
//            *n = Vec2d(a[1] - b[1], b[0] - a[0]);
//        }
//    }
//    return s0 + c * (s1 - s0);
}

template<typename VertexType>
Scalar EdgeT<VertexType>::GetL(unsigned int index) const
{
    if (curvilinear_length_.size() != GetSize()) {
        ComputeCurvilinearLength();
    }
    return curvilinear_length_[index];
}

template<typename VertexType>
Scalar EdgeT<VertexType>::GetL(typename Graph::VertexId start, unsigned int index) const
{
    return GetL(GetIndex(start, index));
}


template<typename VertexType>
void EdgeT<VertexType>::updated()
{
    edge_->updated();
}

} // namespace core

} // namespace expressive

#endif // EDGE_H
