/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef HIERARCHYTREE_H
#define HIERARCHYTREE_H

#include <core/graph/Graph.h>
#include <core/graph/algorithms/GraphAlgorithms.h>
#include <core/geometry/VectorTools.h>


namespace expressive
{

namespace core
{

template<typename GraphType>
/**
 * @brief The GraphHierarchyMapT class creates a hierarchy for a given graph, by creating a link between unconnected parts.
 * The hierarchy is created when calling GraphHierarchyMapT#ComputeAnchors().
 * First, it finds the connected components.
 * Then, it searches if 2 connected components actually intersect/get in contact.
 * If yes, it will compute what we call an "Anchor Point" for those 2 (basically, it's just a set of parameters to
 * define the child's location in it's parent's frame).
 *
 * So far, a few questions are still asked :
 * 1- Should children be anchored to their parents only once or more ?
 * 2- Should we clip a child if it really only intersects it's parent ? (for example a T-posed character).
 * 3- We probably want to be able to handle multiple behaviour for children :
 * 3-1- When just intersecting, or just one point is linked to the parent, the child's other points should be moved in the anchored point's frame.
 * 3-2- Otherwise, every point will probably have their own anchor point, and thus, their own manipulation frame, allowing for shapes that are colinear with others.
 * 4- An important point to note is that hierarchy will be transitive : A child having 2 parents will certainly have one of it's parents eat the other one.
 * 5- Another current issue is that the hierarchy is computed using primitive's radius, instead of using the iso-value, to check if 2 nodes intersect. This creates inconsistencies on steep angles for example.
 * 6- How to determine which node should be the parent and which should be the child ? Currently done by comparing volume approximations.
 *
 * TODO : use Incremental connected components.
 */
class GraphHierarchyMapT : public GraphType::GraphListener
{
public:
    typedef typename GraphType::EdgeId EdgeId;
    typedef typename GraphType::VertexId VertexId;
    typedef typename GraphType::EdgePtr EdgePtr;
    typedef typename GraphType::VertexPtr VertexPtr;
    typedef typename GraphType::EdgeType EdgeType;
    typedef typename GraphType::VertexType VertexType;

//    typedef typename GraphType::template GraphDataMapT<int> ComponentMap;

    /**
     * @brief The NodeAnchor struct the position of a node inside another node's frame.
     */
    struct NodeAnchor {
        NodeAnchor() :
            s(0.0),
            theta(0.0),
            iso(0.0)
        {}
        NodeAnchor(Scalar s, Scalar theta, Scalar iso) :
            s(s),
            theta(theta),
            iso(iso)
        {}

        Scalar s; /*< The curvilinear position along the parent. */
        Scalar theta; /*< The angle around the parent. */
        Scalar iso; /*< Distance to the parent. */
    };

    /**
     * @brief The NodeInfo struct is a representation of a given node's frame.
     */
    struct NodeInfo {
        NodeInfo() {}

        NodeInfo(expressive::VectorTools::Transformation t) :
            t(t)
        {}

//        bool operator<(const NodeInfo& rhs) const
//        {
//            return id < rhs.id;
//        }

        /**
         * @brief AddChild Adds a new child to this node.
         * @param child
         */
        void AddChild(VertexId child)
        {
            children.insert(child);
        }

        /**
         * @brief RemoveChild Removes an existing child from this node.
         * @param child
         */
        void RemoveChild(VertexId child)
        {
            children.erase(child);
        }

//        EdgeId id;
        expressive::VectorTools::Transformation t; /*< Represents current node's position in world space (it's own frame). TODO : replace it by using ulysse's class. */
        std::set<VertexId> children; /*< List of children */
    };

//    /**
//     * @brief The ComponentData struct is used to represent a connected component.
//     * It contains every vertex and edge, as well as its bounding box and an approximated volume.
//     * The volume is used to determine which connected component should be the parent/child.
//     */
//    struct ComponentData
//    {
//        ComponentData() :
//            volume(0.0) {}

//        void clear()
//        {
//            vertices.clear();
//            edges.clear();
//            volume = 0.0;
//            bounding_box = AABBoxT<typename GraphType::BasePoint>();
//        }

//        std::set<VertexId> vertices; /*< List of vertices included in this component*/
//        std::set<EdgeId > edges; /*< List of edges included in this component*/
//        AABBoxT<typename GraphType::BasePoint> bounding_box;/*< Component's AABBox.*/
//        Scalar volume; /*< Component's approximated volume. */
//    };

    typedef std::map<VertexId, std::map<EdgeId, NodeAnchor> > AnchorMap;
    typedef std::map<EdgeId, NodeInfo> NodeInfos;
//    typedef std::map<EdgeId, std::set<VertexId> > ChildMap;
//    typedef std::map<EdgeId, expressive::VectorTools::Transformation>

    /**
     * @brief GraphHierarchyMapT Constructs a new GraphHierarchyMapT
     * @param graph The graph to organize.
     */
    GraphHierarchyMapT(GraphType* graph);

    virtual ~GraphHierarchyMapT() {}

    //////
    /// GETTERS
    ///
    inline bool update_children() const { return update_children_; } /*< returns true if editing a node has an impact on its children */

    //////
    /// SETTERS
    ///
    /**
     * @brief set_update_children Sets the behavior of children when editing a parent node.
     * @param update_children If true, children will move relatively to their parent when editing the parent.
     * Otherwise, this will only update the possible links and may add or remove children to the updated node.
     */
    inline void set_update_children(bool update_children) { update_children_ = update_children; }

    //////
    /// OTHERS
    ///
    /**
     * @brief Notify Updates the hierarchy map when the graph changes.
     * @param changes The list of changes.
     */
    virtual void Notify(const typename GraphType::GraphChanges &changes);

    /**
     * @brief AddAnchor Adds a new Anchor of vertex on parent, with the given parameters.
     * @param vertex The attached vertex.
     * @param parent The parent on which vertex gets attached to.
     * @param s The curvilinear coordinate of vertex along parent.
     * @param theta The angle of vertex around parent.
     * @param iso The distance of vertex to parent.
     * @see NodeAnchor.
     */
    void AddAnchor(VertexId vertex, EdgeId parent, Scalar s, Scalar theta, Scalar iso);

    /**
     * @brief AddAnchor See #AddAnchor(). Overload this method if needed.
     * @param vertex The attached vertex.
     * @param parent The parent on which vertex gets attached to.
     * @param anchor The anchor parameters.
     * @see NodeAnchor.
     */
    virtual void AddAnchor(VertexId vertex, EdgeId parent, const NodeAnchor &anchor);

    /**
     * @brief RemoveAnchor Removes every anchors for a given vertex.
     * Should be used when removing a vertex.
     */
    virtual void RemoveAnchor(VertexId vertex);

//    /**
//     * @brief UpdateAnchors Updates the anchors of a given vertex.
//     * Should be used when moving a vertex.
//     * @param vertex the updated vertex.
//     */
//    void UpdateAnchors(VertexId vertex);

    /**
     * @brief UpdateAnchors Updates the anchors attached to a given edge.
     * Should be used when moving a whole segment with attached nodes.
     * @param edge The updated edge.
     */
    void UpdateAnchors(EdgeId edge);

    /**
     * @brief RemoveNode Removes a node from the hierarchy, and deletes the anchors attached to it. Optionnaly, it can also remove it's children recursively.
     * @param edge The removed edge.
     * @param remove_children If set to true, it will remove attached children from the hierarchy too.
     */
    void RemoveNode(EdgeId edge, bool remove_children = true);

//    /**
//     * @brief ExtractComponents Extracts the connected components.
//     * Computes and fills #components_ map. Those are then used to determine where Anchor Points must be computed.
//     * @return the number of connected components.
//     */
//    unsigned int ExtractComponents();

//    /**
//     * @brief UpdateComponentVolume Updates a given component's volume.
//     * Useful when a vertex has been moved.
//     * @param component index of the component to be updated.
//     */
//    void UpdateComponentVolume(unsigned int component);

    /**
     * @brief ComputeAnchors Entry point for this class
     * Makes the hierarchy map computing the anchor points, by using the algorithm described in GraphHierachyMap.
     * @see GraphHierarchyMapT.
     */
    void ComputeAnchors();

    /**
     * @brief ComputeAnchors Computes anchors for the given component.
     * This will compare the given component with every other components and add anchors if required.
     * @param child_component Id of the selected component.
     * @param check_previous_components If true, this will compare anchors of this component to every other components.
     *                                  Otherwise, it will only check for components with an higher index (small improvement when computing EVERY anchors).
     */
    void ComputeAnchors(unsigned int child_component, bool check_previous_components = true);

    /**
     * @brief ComputeAnchors Same as above, but only for a given parent
     * @param child_component Id of the child component.
     * @param parent_component Id of the parent component.
     */
    void ComputeAnchors(unsigned int child_component, unsigned int parent_component);

    /**
     * @brief ComputeAnchors Computes anchors for the given vertex.
     * @param id Vertex' id.
     */
    void ComputeAnchors(VertexId id);

    /**
     * @brief ComputeAnchor Computes NodeAnchor for the given vertex on the given edge.
     * @param vertex A Vertex.
     * @param edge An Edge.
     * @return A NodeAnchor that represents vertex in edge's frame.
     */
    NodeAnchor ComputeAnchor(VertexPtr vertex, EdgePtr edge);

protected:
//    typedef std::map<typename GraphType::VertexId, int> CompMap;
//    typedef std::map<typename GraphType::EdgeId, int> EdgeCompMap;

    GraphType* graph_; /*< The graph for which hierarchy has to be computed.*/
    AnchorMap anchors_; /*< List of anchors for every vertex.*/
    NodeInfos nodes_; /*< List of frames of every edge. */

    typename GraphAlgorithms<GraphType>::ConnectedComponents connected_components_;
//    CompMap comp_map; /*< property map for boost's connected component visitor. */
//    EdgeCompMap edge_comp_map; /*< property map that contains each edge's connected component's index. */
//    boost::associative_property_map<CompMap> componentMap; /*< property map for boost's connected component visitor. */

//    std::vector<ComponentData > components_; /*< List of every connected components in this graph. */

    /**
     * @brief update_children_ If true, children will move relatively to their parent when editing the parent.
     * Otherwise, this will only update the possible links and may add or remove children to the updated node.
     */
    bool update_children_;
};

template<typename GraphType>
GraphHierarchyMapT<GraphType>::GraphHierarchyMapT(GraphType* graph) :
    GraphType::GraphListener(graph),
    graph_(graph),
//    comp_map(),
//    componentMap(comp_map),
    connected_components_(nullptr),
    update_children_(false)
{
//    auto vertices = graph->GetVertices();
//    while (vertices->HasNext()) {
//        auto v = vertices->NextId();
//        boost::put(componentMap, v, -1);
//    }
}

template<typename GraphType>
void GraphHierarchyMapT<GraphType>::Notify(const typename GraphType::GraphChanges &changes)
{
    printf("Notify\n");
    std::set<int> updated_components;
    std::set<int> removed_components;

    for (auto id : changes.removed_vertices_) { // gather components that will be updated / deleted.
        unsigned int compo = boost::get(connected_components_.componentMap, id);
        connected_components_.components_[compo].vertices.erase(id);
        if (connected_components_.components_[compo].vertices.size() == 0) {
            removed_components.insert(compo);
        } else {
            updated_components.insert(compo);
        }
        RemoveAnchor(id);
    }
    for (auto id : changes.removed_edges_) {
        unsigned int compo = connected_components_.edge_comp_map[id];
        connected_components_.components_[compo].edges.erase(id);
        connected_components_.edge_comp_map.erase(id);
        if (connected_components_.components_[compo].edges.size() == 0) {
            removed_components.insert(compo);
        } else {
            updated_components.insert(compo);
        }
        RemoveNode(id);
    }

    for (auto id : changes.updated_vertices_) { // Remove stuff that will be recomputed when updating component.
        updated_components.insert(boost::get(connected_components_.componentMap, id));
        printf("Remove Anchor %u (comp %d)\n", (uint) graph_->Get(id)->internal_id(), boost::get(connected_components_.componentMap, id));
        RemoveAnchor(id);

        // Also update direct neighbors, just in case (ie. if we got closer than before, this might change priority : to check !).
        auto v  = graph_->Get(id);
        auto neighbors = v->GetAdjacentVertices();
        while (neighbors->HasNext()) {
            RemoveAnchor(neighbors->Next()->id());
        }
    }
    for (auto id : changes.updated_edges_) {
        if (update_children()) {
            printf("Update Node %u (comp %d)\n", (uint) graph_->Get(id)->internal_id(), connected_components_.edge_comp_map[id]);
            UpdateAnchors(id);
        } else {
            printf("Remove Node %u (comp %d)\n", (uint) graph_->Get(id)->internal_id(), connected_components_.edge_comp_map[id]);
            updated_components.insert(boost::get(connected_components_.componentMap, graph_->Get(id)->GetStartId()));
            RemoveNode(id);
        }
    }

    updated_components.erase(removed_components.begin(), removed_components.end()); // don't update components that will be removed.

    for (int i : removed_components) {
        connected_components_.components_[i].clear();
    }

    for (int i : updated_components) { // update volumes.
        printf("Recompute Anchor %d\n", i);
        connected_components_.UpdateComponentVolume(i);

    }

    for (auto id : changes.added_vertices_) { // Add new vertices
        boost::put(connected_components_.componentMap, id, -1);
        ComputeAnchors(id);
    }

    for (int i : updated_components) { // update the rest.
        printf("Recompute Anchor %d\n", i);
        ComputeAnchors(i);
    }
}

template<typename GraphType>
void GraphHierarchyMapT<GraphType>::AddAnchor(VertexId vertex, EdgeId parent, const NodeAnchor &anchor)
{
    nodes_[parent].AddChild(vertex);
    anchors_[vertex][parent] = anchor; // also erases and deletes the previous anchor on parent if it's already existing.

}

template<typename GraphType>
void GraphHierarchyMapT<GraphType>::AddAnchor(VertexId vertex, EdgeId parent, Scalar s, Scalar theta, Scalar iso)
{
    return AddAnchor(vertex, parent, NodeAnchor(s, theta, iso));
}

template<typename GraphType>
void GraphHierarchyMapT<GraphType>::RemoveAnchor(VertexId vertex)
{
    typename AnchorMap::iterator it = anchors_.find(vertex);
    for (auto m : it->second) {
        nodes_[m.first].children.erase(vertex);
//        const NodeAnchor &anchor = m->second;
//        nodes_[anchor.parent].children.erase(vertex);
    }
    anchors_.erase(vertex);
}

//template<typename GraphType>
//void GraphHierarchyMapT<GraphType>::UpdateAnchors(VertexId vertex)
//{
//    RemoveAnchor(vertex);
//    ComputeAnchors(vertex);
//}

template<typename GraphType>
void GraphHierarchyMapT<GraphType>::UpdateAnchors(EdgeId /*edge*/)
{
}

template<typename GraphType>
void GraphHierarchyMapT<GraphType>::RemoveNode(EdgeId edge, bool remove_children)
{
    typename NodeInfos::iterator it = nodes_.find(edge);//nodes_.find(edge);

    if (it != nodes_.end()) {
        printf("Found node %d (%d children)\n", (int) graph_->Get(edge)->internal_id(), (int) nodes_[edge].children.size());
        for (VertexId id : nodes_[edge].children) {
            if (remove_children) {
                anchors_.erase(id);
            } else {
                anchors_[id].erase(edge);
            }
        }

        nodes_.erase(edge);
    }
}

//template<typename GraphType>
//unsigned int GraphHierarchyMapT<GraphType>::ExtractComponents()
//{
//    typename GraphAlgorithms<GraphType>::ComputeVolumeVisitor volumer;

//    comp_map.clear();
//    edge_comp_map.clear();

//    unsigned int num = boost::connected_components(*graph_, componentMap, boost::color_map(graph_->vertex_color_property_map()).vertex_index_map(graph_->vertex_index_property_map()));
//    // todo use disjoint_sets to improve reusability. Why not done yet ? Because vertex/edge removal is not handled in those.

//    // get every individual components and the list of vertices of each.
//    components_.clear();
//    components_.resize(num);

//    typename GraphType::VertexIteratorPtr vertices = graph_->GetVertices();
//    while (vertices->HasNext()) {
//        VertexPtr vertex = vertices->Next();
//        int c = boost::get(componentMap, vertex->id());
//        components_[c].vertices.insert(vertex->id());
//        components_[c].bounding_box.Enlarge(vertex->pos(), vertex->pos().weight());
//        typename GraphType::EdgeIteratorPtr edges = vertex->GetEdges();
//        while (edges->HasNext()) {
//            EdgeId id = edges->NextId();
//            components_[c].edges.insert(id);
//            edge_comp_map[id] = c;
//        }
//    }

//    for (unsigned int i = 0; i < num; ++i) {
//        components_[i].volume = volumer.GetVolume(graph_, *components_[i].vertices.begin());
//    }

//    return num;

//}

//template<typename GraphType>
//void GraphHierarchyMapT<GraphType>::UpdateComponentVolume(unsigned int component)
//{
//    if (components_[component].vertices.size() > 0) {
//        typename GraphAlgorithms<GraphType>::ComputeVolumeVisitor volumer;
//        components_[component].volume = volumer.GetVolume(graph_, *components_[component].vertices.begin());
//    }
//}


template<typename GraphType>
void GraphHierarchyMapT<GraphType>::ComputeAnchors()
{
    unsigned int num = connected_components_.ExtractComponents(graph_);
    printf("Graph has %d components\n", num);
    for (unsigned int i = 0; i < num; ++i) {
        ComputeAnchors(i, false);
    }
}

template<typename GraphType>
void GraphHierarchyMapT<GraphType>::ComputeAnchors(unsigned int component, bool check_previous_components)
{
    Scalar epsilon = 0.1;
    unsigned int j = check_previous_components ? 0 : component + 1;
    printf("Component %d has %d elements. boundingbox is %s. volume is %f\n", component, (int)connected_components_.components_[component].vertices.size(), connected_components_.components_[component].bounding_box.to_string().c_str(), connected_components_.components_[component].volume);
    for (; j < connected_components_.components_.size(); ++j) {
        if (j == component) { // don't compare current component with itself.
            continue;
        }
        if (connected_components_.components_[component].bounding_box.Intersect(connected_components_.components_[j].bounding_box)) {
            printf("component %d intersects component %d\n", component, j);
            Scalar vi = connected_components_.components_[component].volume;
            Scalar vj = connected_components_.components_[j].volume;

            unsigned int smaller = vi < vj ? component : j;
            unsigned int bigger = vi < vj ? j : component;

            // Only compute anchor points if the 2 volumes are really inequal.
            // We can't yet determine which is the parent for 2 component of same size.
            if ((connected_components_.components_[smaller].volume / connected_components_.components_[bigger].volume) < (1.0 - epsilon)) {
                ComputeAnchors(smaller, bigger);
            }
        }
    }
}

template<typename GraphType>
void GraphHierarchyMapT<GraphType>::ComputeAnchors(unsigned int child_component, unsigned int parent_component)
{
    unsigned int cpt = 0;
    for (VertexId vid : connected_components_.components_[child_component].vertices) {
        VertexPtr vertex = graph_->Get(vid);
        const VertexType &p = vertex->pos();
        for (EdgeId eid : connected_components_.components_[parent_component].edges) {
            EdgePtr edge = graph_->Get(eid);
            const VertexType &s = edge->GetStart()->pos();
            const VertexType &e = edge->GetEnd()->pos();
            if (p.intersects(s, e, s.weight(), e.weight(), p.weight())) { // todo test intersections better than that : using IS support for example, instead of just tubes intersection, whichare not correct.
                AddAnchor(vid, eid, ComputeAnchor(vertex, edge));
                cpt++;
            }
        }
    }
    printf("Found %d anchors\n", cpt);
    if (cpt == 0) {
        printf("Testing Parent anchors\n");
        // no anchor found, we will check edge intersections
        // todo : Improve that test - so f...ing slow and inefficient.
        // Should do the math to find intersection between 2 cones.
        for (EdgeId eid : connected_components_.components_[child_component].edges) {
            EdgePtr edge = graph_->Get(eid);
            Scalar w = std::min(edge->GetPoint(0).weight(), edge->GetPoint(1).weight());
            VertexPtr s = edge->GetStart();
            VertexPtr e = edge->GetEnd();

            for (EdgeId pid : connected_components_.components_[parent_component].edges) {
                EdgePtr parent = graph_->Get(pid);
                Scalar l = (parent->GetPoint(0) + parent->GetPoint(1)).norm();
                Scalar wps = parent->GetPoint(0).weight();
                Scalar wpe = parent->GetPoint(1).weight();
                if (w > l / 3.0) {
                    continue;
                }

                Scalar n = l / w;
                Scalar step = 1.0 / n;
                printf("%f steps (%f:%f:%f)\n", n, l, w, step);
                for (Scalar i = step; i < 1.0; i+= step)
                {
                    Scalar curWeight = wps + (wpe - wps) * i;
                    VertexType p = VertexType(parent->GetInterpolatedPoint(i), curWeight);
                    if (p.intersects(s->pos(), e->pos(), s->pos().weight(), e->pos().weight(), curWeight)) {
                        if ((p-s->pos()).norm() < (p-e->pos()).norm()) {
//                        if (i < 0.5) {
                            AddAnchor(s->id(), parent->id(), ComputeAnchor(s, parent));
                            cpt++;
                        } else {
                            AddAnchor(e->id(), parent->id(), ComputeAnchor(e, parent));
                            cpt++;
                        }
                        break;
                    }
                }
            }
        }
        printf("Found %d anchors\n", cpt);
    }
}

template<typename GraphType>
void GraphHierarchyMapT<GraphType>::ComputeAnchors(VertexId id)
{
    int cmp = boost::get(connected_components_.componentMap, id);
    if (cmp == -1) {
        connected_components_.ExtractComponents(graph_);
        cmp = boost::get(connected_components_.componentMap, id);
    }

    printf("ComputeAnchors : vertex %u -> comp %d\n", (uint) graph_->Get(id)->internal_id(), cmp);

    ComputeAnchors(cmp);


//    VertexPtr vertex = graph_->Get(id);
//    const VertexType &p = vertex->pos();
//    for (unsigned int i = 0;
//    for (EdgeId eid : components_[parent_component].edges) {
//        EdgePtr edge = graph_->Get(eid);
//        const VertexType &s = edge->GetStart()->pos();
//        const VertexType &e = edge->GetEnd()->pos();
//        if (p.intersects(s, e, s.weight(), e.weight(), p.weight())) { // todo test intersections better than that : using IS support for example, instead of just tubes intersection, whichare not correct.
//            AddAnchor(id, eid, ComputeAnchor(vertex, edge));
//            cpt++;
//        }
//    }
//    PRINT_TRACE;

//    typedef std::map<typename GraphType::VertexId, int> CompMap;
//    CompMap comp_map;
//    boost::associative_property_map<CompMap> componentMap(comp_map);

//    int num = boost::connected_components(*graph_, componentMap, boost::vertex_index_map(graph_->propmapIndex).color_map(graph_->propmapColor));

//    printf("%d components found\n", num);

//    VertexPtr v = graph_->Get(id);
//    const VertexType &p = v->pos();

//    int vertex_component = boost::get(componentMap, id);

//    typename GraphType::EdgeIteratorPtr edges = graph_->GetEdges();
//    while (edges->HasNext()) { // todo add octree check here.
//        EdgePtr edge = edges->Next();
//        if (boost::get(componentMap, edge->GetStartId()) == vertex_component) {
//            continue;
//        }
//        const VertexType &s = edge->GetStart()->pos();
//        const VertexType &e = edge->GetEnd()->pos();
//        if (p.intersects(s, e, s.weight(), e.weight(), p.weight())) {
//            printf("v[%d] intersects edge[%d]\n", v->internal_id_, edge->internal_id_);
//            AddAnchor(id, edge->id(), ComputeAnchor(v, edge));
//        }
//    }
}

template<typename GraphType>
typename GraphHierarchyMapT<GraphType>::NodeAnchor GraphHierarchyMapT<GraphType>::ComputeAnchor(const VertexPtr vertex, const EdgePtr edge)
{
    typename NodeInfos::iterator it = nodes_.find(edge->id());
    VectorTools::Transformation t;

    if (it != nodes_.end()) {
        t = it->second.t;
    } else {
        t = VectorTools::ComputeTransformation(edge->GetStart()->pos(), edge->GetEnd()->pos());
        nodes_[edge->id()] = NodeInfo(t);
    }
    Point p = VectorTools::GetPointInObjectFrame(vertex->pos(), t);
    Scalar s = p[0];
    Scalar iso = p.norm();
    Point n = p / iso;
    Scalar theta = acos(n[2]);
    if (n[1] < 0) {
        theta = -theta;
    }
    
//    printf("v[%d] e[%d] ::: S: %f   iso : %f   theta : %f\n", vertex->internal_id_, edge->internal_id_, s, iso, theta);
    return NodeAnchor(s, theta, iso);

}

typedef GraphHierarchyMapT<WeightedGraph> GraphHierarchyMap;

} // namespace core

} // namespace expressive

#endif // HIERARCHYTREE_H
