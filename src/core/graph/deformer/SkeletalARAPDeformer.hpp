
#ifndef SKELETAL_ARAP_DEFORMER_HPP_
#define SKELETAL_ARAP_DEFORMER_HPP_

#include <core/graph/deformer/SkeletalARAPDeformer.h>

#include <core/graph/deformer/igl/polar_svd.hpp>


#include <core/geometry/VectorTools.h>

namespace expressive {
namespace core {

template<typename GraphType>
void SkeletalARAPDeformerT<GraphType>::UpdateHandle(const GraphType* graph,
                                                    const std::set<VertexId>& handles)
{
    //TODO:@todo : done in a suboptimal way...

    Eigen::MatrixXd tmp = original_vertices_;

    int i = 0;
    for(auto it = handles.begin(); it!=handles.end(); ++it) {
        auto s = v_map.find(*it);
        assert(s != v_map.end());
        int j = s->second; // index of the constraint !
                           // should be equalt to constraint_indices_(i) !
        auto p = graph->Get(*it)->pos();
        position_constraints_.block<1,3>(i,0) = Eigen::Vector3d(p[0], p[1], p[2]).transpose();
        tmp.block<1,3>(j,0) = position_constraints_.block<1,3>(i,0);
        //original_vertices_.block<1,3>(j,0);
        ++i;
    }

    m_right_constraint_ = M_right_ * tmp;
}

template<typename GraphType>
void SkeletalARAPDeformerT<GraphType>::Precompute(const GraphType* graph,
                                                  const std::set<VertexId>& handles)
{
///TODO:@todo : for now assume there is only segment primitive in the graph ...
///TODO:@todo : This is higly inefficient, for large skeleton this can become really prohibitive.
    std::map<EdgeId,int> e_map;

    // Create a mapping between VertexId and index in the data structure of this class
    auto vertices = graph->GetVertices();
    int i = 0;
    while (vertices->HasNext()) {
        auto v_id = vertices->NextId();
        v_map.emplace(v_id, i);
        ++i;
    }
    // Same thing for EdgeId
    auto edges = graph->GetEdges();
    i = 0;
    while (edges->HasNext()) {
        auto e_id = edges->NextId();
        e_map.emplace(e_id, i);
        ++i;
    }

///TODO:@todo should detect the segment that are fully constrained since
/// their rotation sould be optimized differently !!!

    int nb_handles = (int) handles.size();

    nb_vertices_ = graph->GetVertexCount();
    nb_segments_ = graph->GetEdgeCount();

    new_vertices_.resize(nb_vertices_, 3);
    original_vertices_.resize(nb_vertices_, 3);

    segments_.resize(nb_segments_,2);
    adjacency_.resize(nb_segments_, nb_segments_);

    rotated_w_edges_.resize(nb_segments_,3);
    w_edges_.resize(nb_segments_, 3);


    vertex_weight_.resize(nb_vertices_);
    segment_weight_.resize(nb_segments_);

    std::vector<T> adjacency_coeffs;
//    adjacency_coeffs.reserve(nb_segments_); //TODO:@todo : how many should we reserve ???

    // in the following, the comment :
    //      TODO:@todo : the div by 0.2 blablabla
    // is here because I have made my original test on the influence of the weight
    // in the adjacancy list on skeleton with radius equalt to 0.2 ... to be removed later

    vertices = graph->GetVertices();
    i = 0;
    while (vertices->HasNext()) {
        auto v = vertices->Next();
        original_vertices_.block<1,3>(i,0) = Eigen::Vector3d(v->pos()[0], v->pos()[1], v->pos()[2]).transpose();

        vertex_weight_(i) = v->pos().weight() / 0.2; //TODO:@todo : the div by 0.2 blablabla

        // create vertex adjacency list now
        auto v_edges = v->GetEdges();
        while (v_edges->HasNext()) {
            auto e = v_edges->Next();
            auto s1 = e_map.find(e->id());
            assert(s1 != e_map.end());
            int j1 = s1->second;

            // will be computed several time but doesnt matter for now ...
            segment_weight_(j1) = 0.5*(graph->Get(e->GetStartId())->pos().weight()
                                 + graph->Get(e->GetEndId())->pos().weight()) / 0.2; //TODO:@todo : the div by 0.2 blablabla;

//            Scalar length =
//                (graph->Get(e->GetStartId())->pos()
//                - graph->Get(e->GetEndId())->pos()).norm();

            auto v_edges_2 = v->GetEdges();
            while (v_edges_2->HasNext()) {
                auto e2 = v_edges_2->Next();
                auto s2 = e_map.find(e2->id());
                assert(s2 != e_map.end());
                int j2 = s2->second;

                if(j1!=j2) {
                    // if a length is to be used, it is the one of j1 (that can be retrieved before the while loop)
                    Scalar weight = 0.1;
                    // 0.5 : permet un peu de compression : plus on augmente plus c'est permis
                    // 0.01 : garde assez bien les longueur tout en présentant quelque chose de bien "lisse"
                    // 0.001 : still a lot of smoothness
                    // 0.00001 : become rigid by part
                    weight = 0.01 * vertex_weight_(i)*vertex_weight_(i);//0.01;
                    adjacency_coeffs.push_back(T(j1,j2,weight));
                }
            }
        }
        ++i;
    }
    new_vertices_ = original_vertices_;
    adjacency_.setFromTriplets(adjacency_coeffs.begin(), adjacency_coeffs.end());

    is_fixed_edge_.resize(nb_segments_);

    // Build data structure corresponding to segments

    edges = graph->GetEdges();
    i = 0;
    while (edges->HasNext()) {
        auto e = edges->Next();

        auto s1 = v_map.find(e->GetStartId());
        assert(s1 != v_map.end());
        int i1 = s1->second;

        auto s2 = v_map.find(e->GetEndId());
        assert(s2 != v_map.end());
        int i2 = s2->second;

        segments_(i,0) = i1;
        segments_(i,1) = i2;

//        Scalar radius = ;
//graph->get(e->GetStartId())

        //TODO:@todo : this weight are also the one used in matrix A
        Scalar length = ((original_vertices_.block<1,3>(i1,0)
                         - original_vertices_.block<1,3>(i2,0))).transpose().norm();
        Scalar weight = 1.0/length *  segment_weight_(i)*segment_weight_(i); //TODO:@todo : to be adapted later one ...
        w_edges_.block<1,3>(i,0) = weight * (original_vertices_.block<1,3>(i1,0)
                                             - original_vertices_.block<1,3>(i2,0));

        // Brute force detection of fixed edges (they are not handle really properly yet...)
        is_fixed_edge_(i,0) = (handles.find(e->GetStartId()) != handles.end())
                            && (handles.find(e->GetEndId()) != handles.end());
        //TODO:@todo : do not like

        ++i;
    }

    S_.resize(3*nb_segments_, 3);
    R_.resize(3*nb_segments_, 3);
    for(int i = 0; i<nb_segments_; ++i) {
        R_.block<3,3>(3*i,0) = Eigen::Matrix3d::Identity();
    }

    // Initialize all value obtained from handles
    constraint_indices_.resize(nb_handles);
    position_constraints_.resize(nb_handles,3);
    i = 0;
    for(auto it = handles.begin(); it!=handles.end(); ++it) {
        auto s = v_map.find(*it);
        assert(s != v_map.end());
        int j = s->second;
        constraint_indices_(i)=j;
        position_constraints_.block<1,3>(i,0) = original_vertices_.block<1,3>(j,0);
        ++i;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////TODO:@todo : from this point we do not use the input of the function anymore but only blablabla...
///////////////////////////////////////////////////////////////////////////////////////////////////////////

    // PreCompute all that can be for the linear system
    // (from this point we only use internal data structure)

    m_right_constraint_.resize(nb_segments_, 3);
    m_right_.resize(nb_segments_, 3);

    // compute matrix for linear system
    Eigen::SparseMatrix<double> A(nb_vertices_,nb_vertices_); // it should be symetric
///TODO:@todo : if building A become to expensive, see if we can create it directly...

    std::vector<T> A_coeffs;
    A_coeffs.reserve(4*nb_vertices_);
    for(int k = 0; k<nb_segments_; ++k) {
        int i = segments_(k,0);
        int j = segments_(k,1);
        Scalar length = ((original_vertices_.block<1,3>(i,0)
                         - original_vertices_.block<1,3>(j,0))).transpose().norm();
        Scalar w_k = 1.0/length * segment_weight_(k)*segment_weight_(k); //TODO:@todo : it is function of the current edge !!!

        A_coeffs.push_back(T(i,i,w_k));
        A_coeffs.push_back(T(i,j,-w_k));
        A_coeffs.push_back(T(j,i,-w_k));
        A_coeffs.push_back(T(j,j,w_k));
    }
    A.setFromTriplets(A_coeffs.begin(), A_coeffs.end());

    Eigen::SparseMatrix<double> I(nb_vertices_,nb_vertices_);
    I.setIdentity();
    Eigen::SparseMatrix<double> C(nb_vertices_,nb_vertices_);

    std::vector<T> C_coeffs;
    C_coeffs.reserve(nb_vertices_);
    for(int i = 0; i<constraint_indices_.rows(); ++i) {
//        m_right_constraint_.block<1,3>(constraint_indices_(i),0) ?=? position_constraints_.block<1,3>(i,0)
        C_coeffs.push_back(T(constraint_indices_(i), constraint_indices_(i), 1.0));
    }
    C.setFromTriplets(C_coeffs.begin(), C_coeffs.end());

    M_right_ = A*C;
    M_right_.makeCompressed(); // this might already be the case...
//    m_right_constraint_ = A*C * original_vertices_;
//TODO:@todo this should be recomputed from new handle position...

    A = (I-C)*A*(I-C)+C; // this modify rows and columns of A corresponding to constraints

    solver_ldlt.compute(A);
    assert( (solver_ldlt.info() == Eigen::Success) && "Problem with linear solver! One common issue is that it require at list one handle per unconnected part");
}




template<typename GraphType>
void SkeletalARAPDeformerT<GraphType>::RotationsRelaxation()
{
//Since it is a relaxation process with a 1 neighborhood it will definitely benefits from
//a hierarchical optimisation (as well as a good first guess for matrices ...)

///TODO:@todo : construction of S could be done using only
/// matrix operation, it only require to construct two sparse matrices.
/// Efficiency should be compared : sparse matrix may remove one indirection (to be check,
/// because it depend on the implementation of multiplication of a sparse matrix by a dense vector)
/// but then they require more operations.

    for(int i = 0; i<nb_segments_; ++i)
    {
        S_.block<3,3>(3*i,0) = w_edges_.block<1,3>(i,0).transpose()
                * (new_vertices_.block<1,3>(segments_(i,0),0) - new_vertices_.block<1,3>(segments_(i,1),0));

        if(!is_fixed_edge_(i,0)) {
            for(Eigen::SparseMatrix<double,Eigen::RowMajor>::InnerIterator it(adjacency_,i); it; ++it) {
                //assert(it.row()==i && it.col()==it.index());
                S_.block<3,3>(3*i,0) += it.value() * R_.block<3,3>(3*it.col(),0).transpose();
            }
        }
        //TODO:@todo : this test ensure that rotation matrix corresponding to constrained edges are not optimized,
        //this solution is NOT perfect, among the know flaws we can note : it is only correct for a single segment
        // and not for several neighboring constrained segment... !!!!

        //libigl : THIS NORMALIZATION IS IMPORTANT TO GET SINGLE PRECISION SVD CODE TO WORK CORRECTLY.
        S_ /= S_.array().abs().maxCoeff();
    }

    Eigen::Matrix<double,3,3> si; //libigl
    Eigen::Matrix<double,3,3> ri; //libigl
    Eigen::Matrix<double,3,3> ti,ui,vi; //libigl
    Eigen::Matrix<double,3,1>  _; //libigl
    for(int i = 0; i<nb_segments_; ++i)
    {
        // perform the SVD of S(i) to compute R(i)

        if(is_fixed_edge_(i,0)) {
            //TODO:@todo : this is unstable under alignement : in this case : blend toward identity ...
            Eigen::Vector3d sourceTmp = w_edges_.block<1,3>(i,0).transpose();
            Vector source = Vector(sourceTmp[0], sourceTmp[1], sourceTmp[2]);
            source.normalize();
            Eigen::Vector3d targetTmp = (new_vertices_.block<1,3>(segments_(i,0),0) - new_vertices_.block<1,3>(segments_(i,1),0)).transpose();
            Vector target = Vector(targetTmp[0], targetTmp[1], targetTmp[2]);
            target.normalize();

            Vector v = source.cross(target);
            Scalar c = source.dot(target);
            Scalar k;
            if(v.norm()<0.05)
                k = 1.0;//(1.0-c) / (1.0 - c*c);
            else
                k = 1.0 / (1.0 + c);
            //TODO:@todo : their is a discontinuity...

            R_.block<3,3>(3*i,0) << v[0] * v[0] * k + c,    v[1] * v[0] * k - v[2], v[2] * v[0] * k + v[1],
                    v[0] * v[1] * k + v[2], v[1] * v[1] * k + c,    v[2] * v[1] * k - v[0],
                    v[0] * v[2] * k - v[1], v[1] * v[2] * k + v[0], v[2] * v[2] * k + c ;
//                R_.block<3,3>(3*i,0).setIdentity(); //unstable when aligned
        } else {

//TODO:@todo : could we just work with the block instead of creating new matrices ... ?
        si = S_.block<3,3>(3*i,0);
////        if(single_precision){
//////TODO:@todo : I assume this function is faster, to be used later...
////          polar_svd3x3(si, ri); //it seems to be a faster one -> look at it when arap becomes the limiting factor!!!
////        }else{
        igl::polar_svd(si,ri,ti,ui,_,vi);
////        }
//        assert(ri.determinant() >= 0); // attention au sign du determinant !!!
        R_.block<3,3>(3*i,0) = ri.transpose();
//TODO:@todo : it seems that igl provide AVX based implementation of polar_svd3x3
        }

    }
}

template<typename GraphType>
void SkeletalARAPDeformerT<GraphType>::Solve(bool use_previous_step)
{
    // Initial guess of position and rotation matrix

    if(!use_previous_step) {
        new_vertices_ = original_vertices_;
        for(int i = 0; i<nb_segments_; ++i) {
            R_.block<3,3>(3*i,0) = Eigen::Matrix3d::Identity();
        }
    }
    // else use previous minimizer as initial guess (already stored where it should be)

///TODO:@todo : later start with smarter initialization (mix between previous position
/// deformed by skinning or laplacian or ??? and rotation minimizing with respect to previous frame)

    int iter = 0;
    while(iter < max_iter_)
    {
        // Local Step : Rotations Relaxation

        RotationsRelaxation();

        // Global Step : Linear Solver

        for(int i = 0; i<nb_segments_; ++i) {
            rotated_w_edges_.block<1,3>(i,0)
                    = w_edges_.block<1,3>(i,0) * R_.block<3,3>(3*i,0).transpose();
        }

        // compute right term

        m_right_ = -m_right_constraint_; // see if we include the - in the construction of this term

        for(int i = 0; i<nb_segments_; ++i) {
            m_right_.block<1,3>(segments_(i,0),0) += rotated_w_edges_.block<1,3>(i,0);
            m_right_.block<1,3>(segments_(i,1),0) -= rotated_w_edges_.block<1,3>(i,0);
        }

        for(int i = 0; i<position_constraints_.rows(); ++i) {
            m_right_.block<1,3>(constraint_indices_(i),0) = position_constraints_.block<1,3>(i,0);
        }

        new_vertices_ = solver_ldlt.solve(m_right_);

        ++iter;
    }

///TODO:@todo : the results (stored in new_vertices_) should be used by the "person" that have called Solve

    //TODO:@todo : return something that can be used in the bidule that make the function call
    // such that it perform move point command ?

//    std::cout << "maximal error : "<< (new_vertices_-original_vertices_).array().abs().maxCoeff() <<std::endl;

//    for(int i = 0; i<nb_segments_; ++i)
//    {
//        if(is_fixed_edge_(i,0)) {
//            std::cout << " Fixed rotation i="<<i<<std::endl;
//            std::cout << R_.block<3,3>(3*i,0) <<std::endl;
//        }
//    }
}


} // namespace core
} // namespace expressive



#endif //SKELETAL_ARAP_DEFORMER_HPP_
