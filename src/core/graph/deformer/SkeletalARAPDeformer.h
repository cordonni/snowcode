/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


/*!
   \file: SkeletalARAPDeformer.h

   Language: C++

   License: Expressive license

   \author: Cedric Zanni
   E-Mail:  cedric.zanni@inria.fr

   Description: Header for an As-Rigid-As Possible deformation of a skeleton
                representing a volume.

                Note : the SVD used is the one from libigl (see License information),
                some parts are inspired by the igl implementation of ARAP,
                this is always indicated in comment //igl : ...

                TODO:@todo : should check that the license of igl is acceptable for us.

   Platform Dependencies: None
*/

////////////////////////////////////////////////////////////////////////////////
///TODO:@todo : triangle skeleton are not yet taken into account, it should not be too
/// difficult/long to implement them...
///TODO:@todo : combining segment and triangle energies should not be problematic
//// since we have intrinsic energies that are describe in function of the local
//// volume
///TODO:@todo : There are some cases where a numerical instability arise
/// cause great problems ...
/// TODO:@todo : due to our skeleton data structure, some things are far from optimal...
////////////////////////////////////////////////////////////////////////////////


#ifndef SKELETAL_ARAP_DEFORMER_H_
#define SKELETAL_ARAP_DEFORMER_H_

#include <core/CoreRequired.h>

#include <Eigen/Sparse>

namespace expressive {
namespace core {

/*!
 * Important notice : currently the parameter
 *      const std::set<VertexId>& handles
 * should never change since the code currently assume that
 * an iteration over handles will always iterate over vertices
 * in the same order.
 */
template <class GraphType>
class SkeletalARAPDeformerT
{
    typedef Eigen::Triplet<double> T;
    typedef typename GraphType::VertexId VertexId;
    typedef typename GraphType::EdgeId EdgeId;
public:

    SkeletalARAPDeformerT(int max_iter)
        : max_iter_(max_iter)
    {}

    /*!
     * \brief Precompute Assemble the linear system required to solve iteratively the energy
     * \param graph the skeleton we want to deform
     * \param handles the vertices which position are not optimized during deformation
     */
    void Precompute(const GraphType* graph, const std::set<VertexId>& handles);
    /*!
     * \brief UpdateHandle update data structure that are associated to constraint of the system,
     * ie. associated to the right-hand side of the linear system.
     * \param graph the skeleton we want to deform
     * \param handles the vertices which position are not optimized during deformation
     */
    void UpdateHandle(const GraphType* graph, const std::set<VertexId>& handles);

    /*!
     * \brief RotationsRelaxation compute new rotation matrices using a kind of diffusion process
     */
    void RotationsRelaxation();
    /*!
     * \brief Solve compute new vertices position (require Precompute to be called before,
     * as well as UpdateHandle if Handle position has been modified).
     * Important notice : does not update the skeleton, it should be done by hand using result stored
     * in new_vertices_
     * \param use_previous_step start from previous optimized position
     */
    void Solve(bool use_previous_step = false);

    // Access to result of ARAP after a call of Solve
    const Eigen::MatrixXd& new_vertices() {return new_vertices_; }

protected :
    /**
      * IMPORTANT : all matrices that are used to store point or vector
      * store them in their transpose form (i.e. as a row instead of a column).
      *
      * #V number of vertices
      * #S number of segment skeletons
      * #T number of triangles skeletons
      *
      * #CV number of constrained vertices
      */

    int nb_segments_; //! equal to #S
    int nb_vertices_; //! equal to #V

    Eigen::MatrixXi segments_;  //! vertex indices (i,j) for each segment # (constant size #S)
    Eigen::SparseMatrix<double, Eigen::RowMajor> adjacency_; //! matrix of adjacency (with encoded weight information) (constant size #S*#S)

    Eigen::MatrixXd w_edges_; //! edges modified by weighting coeff (constant size #S)
    Eigen::MatrixXd rotated_w_edges_; //! edges (after application of associated rotation stored in R_) modified by weighting coeff

    Eigen::MatrixXd R_; //! store rotation matrices (constant size 3*3#S)
    Eigen::MatrixXd S_; //! store matrix to be decomposed (constant size 3*3#S)

    int max_iter_; //! maximal number of iteration

    //! Constraints (i.e. handles) information
    Eigen::VectorXi constraint_indices_; //! indices of constrained vertices (constant size #VC)
    Eigen::MatrixXd position_constraints_; //! (constant size 3*#VC)
    ///TODO:@todo : we should have several kind of constraint with different degree of freedom
    ///TODO:@todo : we should have both constrainted vertices and edges

    /// Linear system informations
    Eigen::MatrixXd m_right_; //! value used to store the right hand side of linear system to be solved
    Eigen::MatrixXd m_right_constraint_; //!
    Eigen::SimplicialLDLT< Eigen::SparseMatrix<double> > solver_ldlt; //! precomputed matrix decomposition to solve linear system

    Eigen::MatrixXd original_vertices_; //! contains original vertices : TODO:@todo : might always be useful
    Eigen::MatrixXd new_vertices_; //! storage that will contains resulting position, it can also be used to improve first guess

    Eigen::SparseMatrix<double> M_right_; //! used to compute m_right_constraint_ from handle position

    Eigen::Matrix<bool,Eigen::Dynamic,1> is_fixed_edge_;

    Eigen::VectorXd vertex_weight_;     //! contains weight of vertices  (constant size #V)
    Eigen::VectorXd segment_weight_;    //! contains mean value of weight defined on the segment skeleton (constant size #S)

public :
    //TODO:@todo : I would like to get rid of this ...
    std::map<VertexId,int> v_map; // mapping between VertexId of handles and index in the data structures of this class
};

} // namespace core
} // namespace expressive


#endif //SKELETAL_ARAP_DEFORMER_H_
