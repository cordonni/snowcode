/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef BASICDEFORMER_H
#define BASICDEFORMER_H

// Dependencies: Core
#include <core/CoreRequired.h>

#include <core/graph/Graph.h>
#include <core/graph/GraphCommands.h>

// Dependencies: Boost
#include <boost/graph/breadth_first_search.hpp>

// Dependencies: Others
#include <map>

namespace expressive {
namespace core {

////////////////////////////////////////////////////////////////////////
/// searchShortestPath_BFS_V
////////////////////////////////////////////////////////////////////////
/**
 * This file defines a few visitors used for touch interaction.
 */
/**
 * This visitor will find the shortest path between 2 vertices in the given graph.
 * Attributes will contain the list of chosen vertices's ids mapped to the id of the next on the path.
 */
template <class GraphType>
class searchShortestPath_BFS_V: public boost::default_bfs_visitor {
public:

    typedef typename GraphType::VertexId VertexId;
    typedef typename GraphType::EdgeId EdgeId;
    typedef std::map <VertexId, VertexId> Attributes;

    // Constructor / Destructor
    searchShortestPath_BFS_V(GraphType& graph, VertexId start, VertexId end, Attributes& attributes);

    // Boost Function
    void tree_edge(EdgeId e, const GraphType& g);

protected:
    // Graph Data
    GraphType&   graph_;
    VertexId start_;
    VertexId end_;

    // Attributes Data
    Attributes& attributes_;
};

////////////////////////////////////////////////////////////////////////
/// rotate_BFS_V
////////////////////////////////////////////////////////////////////////
/**
 * This visitor is able to apply a rotation command on a point that will have an influence over a fixed
 * set of vertices : every vertices between start_ & center_. They will each have a specific transformation,
 * which is determined as a linear interpolation between the Identity matrix and the given interpolation, based on the distance
 * between the stationary point and the moving one.
 */
template <class GraphType>
class rotate_BFS_V: public boost::default_bfs_visitor {
public:
    typedef typename GraphType::VertexId VertexId;
    typedef typename GraphType::EdgeId   EdgeId;
    typedef typename GraphType::VertexColorPropertyMap ColorMap;

    // Constructor / Destructor
    rotate_BFS_V(GraphType& graph, std::shared_ptr<MovePointCommand<typename GraphType::VertexType> > command, std::set<VertexId>& ids, VertexId center, VertexId start, Scalar angle, Vector axis);

    // Boost Function
    void examine_vertex(VertexId v, const GraphType& g);

protected:
    // Graph Data
    GraphType&   graph_;
    VertexId center_;
    VertexId start_;

    // Command Data
    std::shared_ptr<MovePointCommand<typename GraphType::VertexType> >   command_;
    std::set<VertexId>& ids_;

    // Rotation Data
    Point  rotate_center_;
    Matrix rotate_matrix_;

    // Graph Properties
    ColorMap colorMap_;
};

////////////////////////////////////////////////////////////////////////
/// scale_BFS_V
////////////////////////////////////////////////////////////////////////
/**
 * This visitor is able to apply a scaling command on a point that will have an influence over a fixed
 * set of vertices : every vertices between start_ & center_. They will each have a specific transformation,
 * which is determined as a linear interpolation between the Identity matrix and the given interpolation, based on the distance
 * between the stationary point and the moving one.
 */
template <class GraphType>
class scale_BFS_V: public boost::default_bfs_visitor {
public:
    typedef typename GraphType::VertexId VertexId;
    typedef typename GraphType::EdgeId   EdgeId;
    typedef typename GraphType::VertexColorPropertyMap ColorMap;

    typedef typename std::map<VertexId, bool>   Performed;
    typedef typename std::map<VertexId, Vector> Attributes;

    // Constructor / Destructor
    scale_BFS_V(GraphType& graph, std::shared_ptr<MovePointCommand<typename GraphType::VertexType> > command, std::set<VertexId>& ids, VertexId center, VertexId start, Scalar scale, std::set<VertexId> scaled_ids);

    // Boost Function
    void initialize_vertex(VertexId v, const GraphType& g);
    void examine_vertex   (VertexId v, const GraphType& g);
    void tree_edge        (EdgeId   e, const GraphType& g);

protected:
    // Graph Data
    GraphType&             graph_;
    VertexId           center_;
    VertexId           start_;
    std::set<VertexId> scaled_ids_;

    // Command Data
    std::shared_ptr<MovePointCommand<typename GraphType::VertexType> >   command_;
    std::set<VertexId>& ids_;

    // Scale Data
    Point  scale_center_;
    Scalar scale_;

    // Graph Properties
    ColorMap colorMap_;

    // Other Data
    Performed  performed_;
    Attributes attributes_;
};

////////////////////////////////////////////////////////////////////////
/// twist_BFS_V
////////////////////////////////////////////////////////////////////////
/**
 * This visitor is able to apply a twist command on a point that will have an influence over a fixed
 * set of vertices : every vertices between start_ & center_. They will each have a specific transformation,
 * which is determined as a linear interpolation between the Identity matrix and the given interpolation, based on the distance
 * between the stationary point and the moving one.
 */
template <class GraphType>
class twist_BFS_V: public boost::default_bfs_visitor {
public:
    typedef typename GraphType::EdgeId   EdgeId;
    typedef typename GraphType::VertexId   VertexId;
    typedef typename GraphType::VertexColorPropertyMap ColorMap;

    typedef typename std::map<VertexId, bool>   Performed;
    typedef typename std::map<VertexId, Scalar> Attributes;

    // Constructor / Destructor
    twist_BFS_V(GraphType& graph, std::shared_ptr<MovePointCommand<typename GraphType::VertexType> > command, std::set<VertexId>& ids, VertexId center, VertexId start, Scalar angle, Vector axis, std::set<VertexId> scaled_ids);

    // Boost Function

    void initialize_vertex(VertexId v, const GraphType& g);
    void examine_vertex   (VertexId v, const GraphType& g);
    void tree_edge        (EdgeId   e, const GraphType& g);

protected:
    // Graph Data
    GraphType&             graph_;
    VertexId           center_;
    VertexId           start_;
    std::set<VertexId> twist_ids_;

    // Command Data
    std::shared_ptr<MovePointCommand<typename GraphType::VertexType> >   command_;
    std::set<VertexId>& ids_;

    // Rotation Data
    Point  twist_start_;
    Scalar angle_;
    Vector axis_;

    // Graph Properties
    ColorMap colorMap_;

    // Other Data
    Performed  performed_;
    Attributes attributes_;
};

} // namespace core
} // namespace expressive

#include <core/graph/deformer/BasicDeformer.hpp>

#endif // BASICDEFORMER_H
