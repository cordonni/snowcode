#ifndef BASIC_DEFORMER_HPP
#define BASIC_DEFORMER_HPP

namespace expressive {
namespace core {

////////////////////////////////////////////////////////////////////////
/// searchShortestPath_BFS_V
////////////////////////////////////////////////////////////////////////

template<typename GraphType>
searchShortestPath_BFS_V<GraphType>::searchShortestPath_BFS_V(
        GraphType&      graph,
        VertexId    start,
        VertexId    end,
        Attributes& attributes)
    : graph_     (graph),
      start_     (start),
      end_       (end),
      attributes_(attributes) {
    // Init Attributes
    attributes_.clear();
    attributes_[start_] = start_;
}

template<typename GraphType>
void searchShortestPath_BFS_V<GraphType>::tree_edge(EdgeId e, const GraphType& g) {
    // Init Data
    VertexId s = boost::source(e, g);
    VertexId t = boost::target(e, g);

    // Link Parent
    attributes_[t] = s;

    // GraphType Research End?
    if (t == end_) { throw(42); }
}

////////////////////////////////////////////////////////////////////////
/// rotate_BFS_V
////////////////////////////////////////////////////////////////////////

template<typename GraphType>
rotate_BFS_V<GraphType>::rotate_BFS_V(
        GraphType&              graph,
        std::shared_ptr<MovePointCommand<typename GraphType::VertexType> >   command,
        std::set<VertexId>& ids,
        VertexId            center,
        VertexId            start,
        Scalar              angle,
        Vector              axis)
    : graph_   (graph),
      center_  (center),
      start_   (start),
      command_ (command),
      ids_     (ids),
      colorMap_(graph_.vertex_color_property_map()) {
    // Register Rotation Matrix
    axis.normalize();
    rotate_center_ = graph_[center]->pos();
    rotate_matrix_ = Quaternion(axis, angle).toMat3();//Eigen::AngleAxis<Scalar>(angle, axis);
}

template<typename GraphType>
void rotate_BFS_V<GraphType>::examine_vertex(typename GraphType::VertexId v, const GraphType&) {
    if (v == start_) {
        // Change color of Center
        boost::put(colorMap_, center_, boost::black_color);
    }

    // Rotate Point: Calcul
    WeightedPoint pos       = graph_[v]->pos();
    Point         start_pt  = pos                     - rotate_center_;
    Point         rotate_pt = rotate_matrix_*start_pt + rotate_center_;

    // Rotate Point: Move
    pos      .set_pos  (rotate_pt);
    ids_     .insert   (v);
    command_->MovePoint(graph_[v], pos);
}

////////////////////////////////////////////////////////////////////////
/// scale_BFS_V
////////////////////////////////////////////////////////////////////////

template<typename GraphType>
scale_BFS_V<GraphType>::scale_BFS_V(
        GraphType&              graph,
        std::shared_ptr<MovePointCommand<typename GraphType::VertexType> >   command,
        std::set<VertexId>& ids,
        VertexId            center,
        VertexId            start,
        Scalar              scale,
        std::set<VertexId>  scaled_ids)
    : graph_     (graph),
      center_    (center),
      start_     (start),
      scaled_ids_(scaled_ids),
      command_   (command),
      ids_       (ids),
      scale_     (scale),
      colorMap_  (graph_.vertex_color_property_map()) {
    // Register Scale Data
    scale_center_ = graph_[center]->pos();
}

template<typename GraphType>
void scale_BFS_V<GraphType>::initialize_vertex(VertexId v, const GraphType&) {
    performed_ [v] = false;
    attributes_[v] = Vector::Zero();
}

template<typename GraphType>
void scale_BFS_V<GraphType>::examine_vertex(VertexId v, const GraphType&) {
    if (v == start_) {
        // Color Map
        boost::put(colorMap_, center_, boost::black_color);

        // Init Attributes Data
        attributes_.clear();
        for (auto id : scaled_ids_) {
            WeightedPoint pos = graph_[id]->pos();
            Point         res = scale_center_ + scale_*(pos - scale_center_);

            // Init Data
            performed_ [id] = true;
            attributes_[id] = res - pos;
        }
    }

    // Register Point Data
    WeightedPoint pos = graph_[v]->pos();
    Point         res = pos + attributes_[v];

    // Scale Point: Move
    pos      .set_pos  (res);
    ids_     .insert   (v);
    command_->MovePoint(graph_[v], pos);
}

template<typename GraphType>
void scale_BFS_V<GraphType>::tree_edge(EdgeId e, const GraphType& g) {
    // Init Data
    VertexId s = boost::source(e, g);
    VertexId t = boost::target(e, g);

    // Link Parent
    if (!performed_[t]) {
        performed_ [t] = true;
        attributes_[t] = attributes_[s];
    }
}

////////////////////////////////////////////////////////////////////////
/// twist_BFS_V
////////////////////////////////////////////////////////////////////////

template<typename GraphType>
twist_BFS_V<GraphType>::twist_BFS_V(
        GraphType&              graph,
        std::shared_ptr<MovePointCommand<typename GraphType::VertexType> >   command,
        std::set<VertexId>& ids,
        VertexId            center,
        VertexId            start,
        Scalar              angle,
        Vector              axis,
        std::set<VertexId>  twist_ids)
    : graph_    (graph),
      center_   (center),
      start_    (start),
      twist_ids_(twist_ids),
      command_  (command),
      ids_      (ids),
      angle_    (angle),
      axis_     (axis),
      colorMap_ (graph_.vertex_color_property_map()){
    // Register Twist Data
    twist_start_ = graph_[center]->pos();
}

template<typename GraphType>
void twist_BFS_V<GraphType>::initialize_vertex(VertexId v, const GraphType&) {
    performed_ [v] = false;
    attributes_[v] = 0.;
}

template<typename GraphType>
void twist_BFS_V<GraphType>::examine_vertex(VertexId v, const GraphType&) {
    if (v == start_) {
        // Change color of Center
        boost::put(colorMap_, center_, boost::black_color);

        // Init Attributes Data
        attributes_.clear();
        for (auto id : twist_ids_) {
            Scalar k     = (graph_[id]->pos() - twist_start_).dot(axis_) / axis_.dot(axis_);
            Scalar angle = angle_*(k>1.?1.:(k<0.?0.:k));

            // Init Data
            performed_ [id] = true;
            attributes_[id] = angle;
        }
    }

    // Rotation Matrix
    Matrix rotate_matrix_;
    Vector axis = axis_; axis.normalize();
    rotate_matrix_ = Quaternion(axis, attributes_[v]).toMat3();//Eigen::AngleAxis<Scalar>(attributes_[v], axis);

    // Rotate Point: Calcul
    WeightedPoint pos       = graph_[v]->pos();
    Point         start_pt  = pos                     - twist_start_;
    Point         rotate_pt = rotate_matrix_*start_pt + twist_start_;

    // Scale Point: Move
    pos      .set_pos  (rotate_pt);
    ids_     .insert   (v);
    command_->MovePoint(graph_[v], pos);
}

template<typename GraphType>
void twist_BFS_V<GraphType>::tree_edge(EdgeId e, const GraphType& g) {
    // Init Data
    VertexId s = boost::source(e, g);
    VertexId t = boost::target(e, g);

    // Link Parent
    if (!performed_[t]) {
        performed_ [t] = true;
        attributes_[t] = attributes_[s];
    }
}

} // namespace core
} // namespace expressive

#endif // EDGE_H
