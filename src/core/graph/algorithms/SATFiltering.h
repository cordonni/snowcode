/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SATFILTERING_H
#define SATFILTERING_H

// core dependencies
#include <core/graph/algorithms/GraphAlgorithms.h>

namespace expressive
{

namespace core
{

/**
 * @brief The SATSegmentFilteringVisitor class
 * This visitor takes care of the SAT algorithm. It constructs a GraphDataMap that contains, for each absorbed vertex & edge,
 * the vertex that occludes it with respect to the given ratio (and the distance map if required).
 */
template<typename GraphT>
class SATSegmentFilteringVisitor : public boost::default_bfs_visitor
{
public:
    typedef typename GraphT::template GraphDataMapT<Scalar> WeightMap;
    typedef typename GraphT::template GraphDataMapT<typename GraphT::VertexId> HierarchyMap;

    typedef std::shared_ptr<GraphT> GraphPtr;
    typedef std::shared_ptr<typename GraphT::Edge> EdgePtr;
    typedef std::shared_ptr<typename GraphT::Vertex> VertexPtr;


    SATSegmentFilteringVisitor(std::shared_ptr<GraphT> graph, Scalar ratio, bool keepGraphConnected_= false, WeightMap* weights = nullptr);
    SATSegmentFilteringVisitor(const SATSegmentFilteringVisitor &other);

    virtual ~SATSegmentFilteringVisitor();

    void examine_vertex(typename GraphT::VertexId v, const GraphT& g);

    /**
     * @brief GetFilteredGraph Returns a filtered graph, based on the ratio given in the ctor.
     * @param getResiduals if true, the function will return the erased part of the graph. Otherwise, the leftovers.
     */
    std::shared_ptr<GraphT> GetFilteredGraph(bool getResiduals = false);

protected:
    std::shared_ptr<GraphT> graph_;
    Scalar ratio_;
    WeightMap *weights_;

    std::shared_ptr<HierarchyMap> occlusions_;

    bool keepGraphConnected_;
};

template<typename GraphT>
SATSegmentFilteringVisitor<GraphT>::SATSegmentFilteringVisitor(std::shared_ptr<GraphT> graph, Scalar ratio, bool keepGraphConnected, WeightMap* weights) :
    boost::default_bfs_visitor(),
    graph_(graph),
    ratio_(ratio),
    weights_(weights),
    occlusions_(std::make_shared<HierarchyMap>()),
    keepGraphConnected_(keepGraphConnected)
{
}

template<typename GraphT>
SATSegmentFilteringVisitor<GraphT>::SATSegmentFilteringVisitor(const SATSegmentFilteringVisitor &other) :
    boost::default_bfs_visitor(),
    graph_(other.graph_),
    ratio_(other.ratio_),
    weights_(other.weights_),
    occlusions_(other.occlusions_),
    keepGraphConnected_(other.keepGraphConnected_)
{
}

template<typename GraphT>
SATSegmentFilteringVisitor<GraphT>::~SATSegmentFilteringVisitor()
{
    occlusions_ = nullptr;
}

template<typename GraphT>
void SATSegmentFilteringVisitor<GraphT>::examine_vertex(typename GraphT::VertexId v, const GraphT& /*g*/)
{
    // Preprocess : weights must be previously normalized by the highest weight (and every  vertex that is considered on the main axis must have a weight = 1.0).
    Scalar weight = weights_ == nullptr ? 1.0 : (*weights_)[v];

    typename GraphT::VertexPtr vertex = graph_->Get(v);
    auto point = vertex->pos();
    Scalar radius = point.weight() * ratio_ * weight;

    typename GraphT::VertexIteratorPtr neighbors = vertex->GetAdjacentVertices();
    bool inside = false;
    unsigned int unoccludedNeighbors = 0;
    unsigned int nNeighbors = vertex->GetEdgeCount();

    if (keepGraphConnected_ && nNeighbors > 1) {
        while (neighbors->HasNext()) {
            if (!occlusions_->contains(neighbors->NextId())) {
                unoccludedNeighbors++;
            }
        }
        neighbors = vertex->GetAdjacentVertices();
    }

    if (unoccludedNeighbors < 2) {
        while (neighbors->HasNext()) {
            VertexPtr n = neighbors->Next();

            if (occlusions_->contains(n->id())) {
                continue;
            }
            Scalar nweight = weights_ == nullptr ? 1.0 : (*weights_)[n->id()];
            Scalar nradius = n->pos().weight() * ratio_ * nweight;
            Point2 dir = point - n->pos();
            if (dir.norm() + radius < nradius) {
                (*occlusions_)[v] = n->id();
                inside = true;
                break;
            }
            if (nNeighbors == 1) {
                typename GraphT::EdgeIteratorPtr neighborEdges = n->GetEdges();
                while (neighborEdges->HasNext()) {
                    typename GraphT::EdgePtr e = neighborEdges->Next();
                    int index;
                    if (e->GetOppositeId(n->id()) != v && GraphAlgorithms<GraphT>::IsInside(e, ratio_, n->pos(), weight, index)) {
                        (*occlusions_)[v] = index == -1 ? n->id() : e->GetVertex(index)->id();
                        inside = true;
                        break;
                    }
                }
            }
        }
    } else {
        inside = false;
    }

    if (!inside) {
        throw(1);
    }
}

template<typename GraphT>
std::shared_ptr<GraphT> SATSegmentFilteringVisitor<GraphT>::GetFilteredGraph(bool getResiduals)
{
    std::shared_ptr<GraphT> res = std::make_shared<GraphT> ();
    std::map<typename GraphT::VertexId, typename GraphT::VertexId> ids;
    auto edges = graph_->GetEdges();
    while (edges->HasNext()) {
        auto edge = edges->Next();
        if (getResiduals != (occlusions_->contains(edge->GetStartId()) || occlusions_->contains(edge->GetEndId()))) {
            continue;
        }
        typename GraphT::VertexId v1;
        typename GraphT::VertexId v2;

        auto it1 = ids.find(edge->GetStartId());
        auto it2 = ids.find(edge->GetEndId());
        if (it1 == ids.end()) {
            v1 = res->Add(edge->GetStart()->pos())->id();
            ids[edge->GetStartId()] = v1;
        } else {
            v1 = it1->second;
        }
        if (it2 == ids.end()) {
            v2 = res->Add(edge->GetEnd()->pos())->id();
            ids[edge->GetEndId()] = v2;
        } else {
            v2 = it2->second;
        }
        res->Add(edge->edge().clone(), v1, v2);
    }

    return res;
}

} // namespace core

} // namespace expressive

#endif // SATFILTERING_H
