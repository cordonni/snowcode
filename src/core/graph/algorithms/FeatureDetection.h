/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GRAPH_FEATUREFILTERING_H
#define GRAPH_FEATUREFILTERING_H

// core dependencies
#include <core/graph/algorithms/GraphAlgorithms.h>

namespace expressive
{

namespace core
{

/**
 * A Visitor that implements the Wish paper (Hierarchical detail detection, conference Women in shape).
 * It uses multiple criterion to detect which part of a graph are details, and which are not.
 */
template<typename GraphT>
class GeometricFeatureDetectionVisitor : public boost::default_dfs_visitor
{
public:
    typedef typename GraphT::template GraphDataMapT<Scalar> WeightMap;
    typedef typename GraphT::template GraphDataMapT<bool> FeatureMap; // will contain for each vertex a bool (value : true if it's a feature, false if it's part of the main shape).

    typedef std::shared_ptr<GraphT> GraphPtr;
    typedef std::shared_ptr<typename GraphT::Edge> EdgePtr;
    typedef std::shared_ptr<typename GraphT::Vertex> VertexPtr;
    /**
     * Ctor
     * @param graph the graph to browse.
     * @param radius_delta The radius offset above which 2 segments are considered to be different.
     * @param thickness_delta The thickness offset above which 2 segments are considered to be different.
     * @param orientation_delta The orientation offset above which 2 segments are considered to be different.
     */
    GeometricFeatureDetectionVisitor(std::shared_ptr<GraphT> graph, Scalar radius_delta, Scalar thickness_delta, Scalar orientation_delta);

    /**
     * @brief Preprocess
     * - Simplifies the graph : creates subdivision curves where possible.
     * - Splits those subdivision curves when radius variation is too strong (i.e. above radius_delta).
     */
    void PreprocessCurves(Scalar radius_delta_);
    void CompareBranchRadius(Scalar thickness_delta);
    void CompareBranchOrientation(Scalar orientation_delta);

    /**
     * Extracts the medial axis from the graph, based on every detail detection set before.
     * @param getResiduals if true, this will return a graph containing only the details. Otherwise, the main graph.
     */
    std::shared_ptr<Graph> ExtractMedialAxis(bool getResiduals = false);

protected:
    std::shared_ptr<GraphT> graph_;

    Scalar radius_delta_;
    Scalar thickness_delta_;
    Scalar orientation_delta_;

    std::shared_ptr<FeatureMap> features_;

};

template<typename GraphT>
GeometricFeatureDetectionVisitor<GraphT>::GeometricFeatureDetectionVisitor(std::shared_ptr<GraphT> graph, Scalar radius_delta, Scalar thickness_delta, Scalar orientation_delta) :
    boost::default_dfs_visitor(),
    graph_(graph),
    radius_delta_(radius_delta),
    thickness_delta_(thickness_delta),
    orientation_delta_(orientation_delta),
    features_(std::make_shared<FeatureMap>())
{
}

//template<typename GraphT>
//void GeometricFeatureDetectionVisitor<GraphT>::PreprocessCurves(std::shared_ptr<GraphT> graph, Scalar radius_delta)
//{
//    // todo : add this on the input.
//    typename GraphAlgorithms<GraphT>::SimplifyVisitor visitor;
//    visitor.Simplify(graph, 0.0);

//    std::map<typename GraphT::EdgeId, unsigned int> clips;
//    const typename GraphT::PrimitiveMap &primitives = graph->GetPrimitives();

//    for (typename GraphT::PrimitiveMap::iterator it = primitives.begin(); it != primitives.end(); ++it) {
//        std::shared_ptr<GraphT::EdgeType> prim = it.first;

//        for (unsigned int i = 1; i < prim->GetSize(); ++i) {

//        }
//    }
//}

template<typename GraphT>
void GeometricFeatureDetectionVisitor<GraphT>::CompareBranchRadius(Scalar thickness_delta)
{
}

template<typename GraphT>
void GeometricFeatureDetectionVisitor<GraphT>::CompareBranchOrientation(Scalar orientation_delta)
{
}

template<typename GraphT>
std::shared_ptr<Graph> GeometricFeatureDetectionVisitor<GraphT>::ExtractMedialAxis(bool getResiduals)
{
    PreprocessCurves(radius_delta_);
    CompareBranchRadius(thickness_delta_);
    CompareBranchOrientation(orientation_delta_);

    std::shared_ptr<Graph> res = nullptr;
    return res;
}

} // namespace core

} // namespace expressive

#endif // GRAPH_FEATUREFILTERING_H
