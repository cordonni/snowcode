/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EDFVISITOR_H
#define EDFVISITOR_H

// core dependencies
#include <core/graph/algorithms/GraphAlgorithms.h>

namespace expressive
{

namespace core
{

/**
 * @brief The EDFVisitor class takes care of computing the extended distance function for a given graph,
 * as well as it's EMA's center.
 * It also defines a WeightMap (a GraphDataMap<Scalar>) to store weighting values for each vertex/edge.
 */
template<typename GraphT>
class EDFVisitor : public boost::default_dfs_visitor
{
public:
    typedef typename GraphT::template GraphDataMapT<Scalar> WeightMap;
    typedef typename GraphT::VertexPtr VPtr;
    typedef typename GraphT::EdgePtr EPtr;
    typedef typename std::shared_ptr<GraphT> GPtr;
    typedef GraphT GRAPH;

    struct ExtremityContainer
    {
        ExtremityContainer(bool useWeightedLengths = true) :
            useWeightedLengths(useWeightedLengths),
            EMA(nullptr),
            maxLen(0.0)
        {
            extremities.clear();
            new_extremities.clear();
            visited_edges.clear();
        }

        std::set<WeightedGraph2D::VertexId> extremities;  //< graph traversing data.
        std::set<WeightedGraph2D::VertexId> new_extremities;  //< graph traversing data.
        std::set<WeightedGraph2D::EdgeId> visited_edges;  //< graph traversing data.

        bool useWeightedLengths; //< If true, lengths will be weighted by the radius of the vertices.

        VPtr EMA; //< The vertex that is the EMA point.

        double maxLen; //< Maximum length at EMA.

        typename GraphT::VertexId previous_vertex; //< graph traversing data.

        typename GraphT::VertexId current_extremity; //< graph traversing data.
    };

    /**
     * Creates a new visitor for the given graph.
     * @param graph the graph for which we want to compute EDF or WEDF.
     */
    EDFVisitor(std::shared_ptr<GraphT> graph);
    EDFVisitor(const EDFVisitor &other);

    virtual ~EDFVisitor();

    std::shared_ptr<WeightMap> ComputeEDFOld(bool useWeightedLengths, typename GraphT::BasePoint &EMA, double &maxLen);

    std::shared_ptr<WeightMap> ComputeEDF(bool useWeightedLengths, typename GraphT::BasePoint &EMA, double &maxLen);

    void tree_edge(typename GraphT::EdgeId v, const GraphT& g);
protected:
    /**
     * Computes the longest path in the given graph.
     * @param graph The graph to traverse
     * @param start the initial point where to traverse.
     * @param edge The initial edge on which the traversal must start.
     * @param weights The weightmap used to ponderate the length.
     * @param[out] ids the ids of the vertices that form the longest path.
     * @return the length of the longest path.
     */
    Scalar GetLongestPath(std::shared_ptr<GraphT> graph, VPtr start, EPtr edge, std::shared_ptr<WeightMap> weights, std::vector<typename GraphT::VertexId> &ids);

    /**
     * Updates the weightMap so that it stops at maxW, and makes the rest of the graph coherent with that. This might result in the cration of a new vertex.
     * @param weights theh list of weights for each verrtex/edge.
     * @param ids the list of vertices to update.
     * @param startW the weight of the starting weight.
     * @param maxW the maximum weight factor (used to compute EMA)
     * @return EMA.
     */
    VPtr UpdateWeights(std::shared_ptr<WeightMap> weights, const std::vector<typename GraphT::VertexId> &ids, Scalar startW, Scalar maxW);

    /**
     * Initializes the weight of a point. If it's an extremity, it must be the area of the extremity.
     * Otherwise, it's set to infinity, as it will be changed later.
     * @param v The point to compute.
     * @return  The weight at that point.
     */
    Scalar GetInitWeight(VPtr v);

    /**
     * Computes the weight of the given edge, using specific vertices.
     * @param e the edge to compute.
     * @param start the vertex whose radius will be used.
     * @param end the vertes whose radius will be used.
     * @return the weight of e.
     */
    Scalar GetWeight(EPtr e, VPtr start, VPtr end);

    std::shared_ptr<GraphT> graph_;

    std::shared_ptr<WeightMap> weights_;

    std::shared_ptr<ExtremityContainer> container_;
};

template<typename GraphT>
EDFVisitor<GraphT>::EDFVisitor(std::shared_ptr<GraphT> graph) :
    graph_(graph),
    weights_(std::make_shared<WeightMap>()),
    container_(std::make_shared<EDFVisitor<GraphT>::ExtremityContainer>())
{
    std::cout << ":::" << container_->extremities.size() << std::endl;
}

template<typename GraphT>
EDFVisitor<GraphT>::EDFVisitor(const EDFVisitor &other) :
    boost::default_dfs_visitor(other),
    graph_(other.graph_),
    weights_(other.weights_),
    container_(other.container_)
{
}

template<typename GraphT>
EDFVisitor<GraphT>::~EDFVisitor()
{
}

template<typename GraphT>
std::shared_ptr<typename EDFVisitor<GraphT>::WeightMap> EDFVisitor<GraphT>::ComputeEDFOld(bool useWeightedLengths, typename GraphT::BasePoint &EMA, double &maxLen)
{
    UNUSED(useWeightedLengths);
    UNUSED(EMA);
    UNUSED(maxLen);
    return nullptr;
}

template<typename GraphT>
std::shared_ptr<typename EDFVisitor<GraphT>::WeightMap> EDFVisitor<GraphT>::ComputeEDF(bool useWeightedLengths, typename GraphT::BasePoint &EMA, double &maxLen)
{
    UNUSED(useWeightedLengths);
    UNUSED(EMA);
    UNUSED(maxLen);
    return nullptr;
}


#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_EDFVISITOR
extern template class EDFVisitor<WeightedGraph2D>;
#endif
#endif

}

}

#endif // EDFVISITOR_H
