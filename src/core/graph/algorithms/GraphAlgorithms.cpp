#define TEMPLATE_INSTANTIATION_GRAPHALGORITHMS
#include <core/graph/algorithms/GraphAlgorithms.h>

// dummy file to force compilation in lib to check compilation errors.
namespace expressive
{

namespace core
{

//template class GraphAlgorithms<Graph>;
//template class GraphAlgorithms<Graph2D>;
template class GraphAlgorithms<WeightedGraph>;
template class GraphAlgorithms<WeightedGraph2D>;

} // namespace core

} // namespace expressive



//#include <boost/graph/planar_face_traversal.hpp>
//#include <boost/graph/boyer_myrvold_planar_test.hpp>

//using namespace boost;

//typedef WeightedGraph2D graph_type;

//typedef graph_type::edge_descriptor EDGEID;
//typedef graph_type::vertex_descriptor VERTEXID;

//struct output_visitor : public planar_face_traversal_visitor
//{
//   void begin_face() { std::cout << "New face: "; }
//   void end_face() { std::cout << std::endl; }
//};



//struct vertex_output_visitor : public output_visitor
//{
//   vertex_output_visitor(const graph_type& g_):g(g_){}
//   template <typename VertexDesc>
//   void next_vertex(VertexDesc v)
//   {
//      //std::cout << g[v].i << " "; //This would do what you wanted if you initialize "i"
////      std::cout << get(vertex_index,g,v) << " ";
//       std::cout << g.Get(v)->pos() << std::endl;
//   }

//   const graph_type& g;
//};

//struct edge_output_visitor : public output_visitor
//{
//   edge_output_visitor(const graph_type& g_):g(g_){}
//   template <typename EdgeDesc>
//   void next_edge(EdgeDesc e)
//   {
//      //std::cout << g[e].i << " "; //This would do what you wanted
//      std::cout << source(e,g) << "-" << target(e,g) << " ";
//   }

//   const graph_type& g;
//};


//// Create a graph - this is a biconnected, 3 x 3 grid.
//// It should have four small (four vertex/four edge) faces and
//// one large face that contains all but the interior vertex
////    std::shared_ptr<Graph2D> g = std::make_shared<Graph2D>();


//typedef WeightedGraph2D::VertexId VertexDescriptor;
//typedef WeightedGraph2D::EdgeId EdgeDescriptor;

////    VertexDescriptor A = g->Add(Point2(0.0, 0.0))->id();//add_vertex(g);
////    VertexDescriptor B = g->Add(Point2(1.0, 1.0))->id();//add_vertex(g);
////    VertexDescriptor C = g->Add(Point2(-1.0,-1.0))->id();//add_vertex(g);
////    VertexDescriptor D = g->Add(Point2(0.0, -1.0))->id();//add_vertex(g);
////    VertexDescriptor E = g->Add(Point2(-1.0, 0.0))->id();//add_vertex(g);

////    g->AddSegment(A, B);
////    g->AddSegment(A, C);
////    g->AddSegment(A, D);
////    g->AddSegment(A, E);
////    g->AddSegment(B, D);
////    g->AddSegment(B, E);
////    g->AddSegment(C, D);
////    g->AddSegment(C, E);
////    g->AddSegment(D, E);

//std::shared_ptr<WeightedGraph2D> contour = std::make_shared<WeightedGraph2D>();
//auto v1 = contour->Add(WeightedPoint2D( 0.,  0., 0.0));
//auto v2 = contour->Add(WeightedPoint2D( 5.,  0., 0.0));
//auto v3 = contour->Add(WeightedPoint2D( 5.,  5., 0.0));
//auto v4 = contour->Add(WeightedPoint2D(10.,  5., 0.0));
//auto v5 = contour->Add(WeightedPoint2D(10., 10., 0.0));
//auto v6 = contour->Add(WeightedPoint2D( 0., 10., 0.0));

//contour->AddSegment(v2,v1);
//contour->AddSegment(v3,v2);
//contour->AddSegment(v4,v3);
//contour->AddSegment(v5,v4);
//contour->AddSegment(v6,v5);
//contour->AddSegment(v1,v6);

//WeightedGraph2D::EdgeIteratorPtr edges = contour->GetEdges();


//while (edges->HasNext()) {
//    WeightedGraph2D::EdgePtr edge = edges->Next();
//    std::cout << (edge != nullptr) << std::endl;

//}
//std::shared_ptr<WeightedGraph2D> g = contour;
//WeightedGraph2D::VertexIndexMap indices = g->vertex_index_map();
//std::cout << "SIZE:" << indices.size() << std::endl;

//g->Remove(v2->id());
////    std::set<WeightedGraph2D::VertexId> vs;
////    vs.insert(v1->id());
////    vs.insert(v2->id());
////    g->MergeVertices(vs);

//GraphAlgorithms<WeightedGraph2D>::PlanarFacesVisitor pfv;
//std::shared_ptr<GraphAlgorithms<WeightedGraph2D>::PlanarFacesVisitor::FaceList> flist=  pfv.GetFaces(contour.get());
//std::cout << "Found " << flist->faces.size() << "lists "<< std::endl;
//for (unsigned int i = 0; i < flist->faces.size(); ++i) {
//    std::cout << "Size : " << flist->faces[i].size() << std::endl;
////        for (unsigned int j = 0; j < flist->faces[i].size(); ++j) {
////        }
//}

////    Graph2D::EdgeIndexMap my_edge_index_map = g->edge_index_map();

////    typedef std::vector< std::vector<Graph2D::EdgeId> > edge_permutation_storage_t;
////    typedef boost::iterator_property_map<edge_permutation_storage_t::iterator, Graph2D::VertexIndexMap> edge_permutation_t;

////    edge_permutation_storage_t ST (g->GetVertexCount());
////    edge_permutation_t embedding(ST.begin(), g->vertex_index_map());

//////    std::map<, vec_t>  embedding;//(num_vertices(g));
////    std::cout <<  g->GetVertexCount() << std::endl;

////    //if you use the parameter "kuratowski_subgraph" you'll need to pass also my_edge_index_map
////    if (boyer_myrvold_planarity_test(boyer_myrvold_params::graph = *dynamic_cast<Graph2D::GraphType*>(g.get()),
////                                     boyer_myrvold_params::embedding =
////                                         embedding
////                                     )
////        )
////      std::cout << "Input graph is planar" << std::endl;
////    else
////      std::cout << "Input graph is not planar" << std::endl;

////    std::cout << ST.size() <<"lines" << std::endl;
////    for (unsigned int i = 0; i < ST.size(); ++i) {
////        std::cout << i << "::" << ST[i].size() << std::endl;
////        for (unsigned int j = 0; j < ST[i].size(); ++j) {
////            printf("\t%d", my_edge_index_map[ST[i][j]]);
////        }
////        printf("\n");
////    }
////    fflush(0);


////    std::cout << std::endl << "Vertices on the faces: " << std::endl;
////    vertex_output_visitor v_vis(*g);
////    planar_face_traversal(*g, embedding, v_vis, /*boost::get(boost::edge_index, g)*/my_edge_index_map);

////    std::cout << std::endl << "Edges on the faces: " << std::endl;
////    edge_output_visitor e_vis(*g);
////    planar_face_traversal(*g, embedding, e_vis, /*boost::get(boost::edge_index, g)*/my_edge_index_map);

////    exit(0);
