/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GRAPHALGORITHMS_H
#define GRAPHALGORITHMS_H

#include <core/graph/Graph.h>
#include <core/geometry/VectorTools.h>
#include <core/geometry/SubdivisionCurve.h>

// Dependencies: Boost
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/planar_face_traversal.hpp>
#include <boost/graph/boyer_myrvold_planar_test.hpp>
#include <boost/property_map/property_map.hpp>
// BOOST :: connected components
#include <boost/graph/connected_components.hpp>
#include <boost/graph/incremental_components.hpp>
#include <boost/pending/disjoint_sets.hpp>

namespace expressive
{

namespace core
{

/**
 * This class contains a few common graph algorithms :
 * - A simplifier : merges points in a graph if they are too close to each other.
 * - A volume computer
 * - A Planar faces visitor : used for some algorithms for boost.
 * - A ConnectedComponents visitor : Creates a component map for each connected subgraphs.
 * - A contain and an isInside method.
 */
template<typename GraphT>
class GraphAlgorithms
{
public:
    typedef typename GraphT::template GraphDataMapT<Scalar> WeightMap;
    typedef typename GraphT::template GraphDataMapT<typename GraphT::VertexId> HierarchyMap;

    typedef std::shared_ptr<GraphT> GraphPtr;
    typedef std::shared_ptr<typename GraphT::Edge> EdgePtr;
    typedef std::shared_ptr<typename GraphT::Vertex> VertexPtr;

    /**
     * Structure used for most algorithms here.
     * Data may not be copied between each visitor instance this way, which saves a LOT of memory and effort trying to copy all this.
     */
    class EdgeList
    {
    public:
        EdgeList() : current_index_(0)
        {
//            propmapPredecessor = boost::associative_property_map<PredecessorMap>(predecessorMap);
        }


//        typedef std::map<VertexId, VertexId> PredecessorMap;
//        PredecessorMap predecessorMap;
//        boost::associative_property_map<PredecessorMap> propmapPredecessor;
        unsigned int current_index_;
        std::vector<std::vector<VertexPtr> > edges_;
        std::set<EdgeId> removed_edges_;
        std::set<VertexId> extremities_;
        std::set<VertexId> checked_extremities_;
        std::map<VertexId, unsigned int> counts_; //< number of vertices usage.

        typename GraphT::VertexColorPropertyMap color_map_;

        VertexId previous_vertex_;

    };

    /**
     * @brief The SimplifyVisitor class simplifies a graph based on distance closeness instead of
     * graph closeness. Use the Simplify method.
     */
    class SimplifyVisitor : public boost::default_dfs_visitor
    {
    public:

        SimplifyVisitor();
        SimplifyVisitor(const SimplifyVisitor &other);

        virtual ~SimplifyVisitor() { data_ = nullptr; }

//        void start_vertex(Vertex u, const Graph &g) {}
        /**
         * Overloaded traversal function used by boost graph.
         */
        void discover_vertex(typename GraphT::VertexId v, const GraphT& g);

        /**
         * Overloaded traversal function used by boost graph.
         */
        void tree_edge(typename GraphT::EdgeId e, const GraphT &g);

        /**
         * @brief Simplify simplifies the given graph.
         * @param graph a graph to simplify
         * @param distance the euclidian distance under which 2 vertices should be merged.
         */
        void Simplify(std::shared_ptr<GraphT> graph, Scalar distance);

    protected:
        std::shared_ptr<EdgeList> data_;
    };

    /**
     * @brief The ComputeVolumeVisitor class computes the volume of the given graph.
     * Use GetVolume directly.
     */
    class ComputeVolumeVisitor : public boost::default_dfs_visitor
    {
    public:
        struct VolumeData
        {
            VolumeData() :
                volume(0.0) {}

            Scalar volume;
        };

        ComputeVolumeVisitor();
        ComputeVolumeVisitor(const ComputeVolumeVisitor &other);

        virtual ~ComputeVolumeVisitor() { data_ = nullptr; }

        /**
         * @brief GetVolume returns the volume of the connected component that contains id.
         * @param graph the graph whose volume must be computed.
         * @param id the vertex to start at.
         * @return the volume.
         */
        Scalar GetVolume(GraphT* graph, typename GraphT::VertexId id);
        Scalar GetVolume(GraphT* graph);

        void examine_edge(typename GraphT::EdgeId e, const GraphT &g);

    protected:
        std::shared_ptr<VolumeData> data_;
//        Scalar computed_volume_;
//        Scalar &tmp_computed_volume_;
    };

    /**
     * @brief The PlanarFacesVisitor class is mostly used for some boost algorithms.
     * It computes every planar faces (i.e. the largest area as possible that is flat).
     * They will be stored as list of vertices.
     */
    class PlanarFacesVisitor : public boost::planar_face_traversal_visitor
    {
    public:
        struct FaceList
        {
            std::vector<std::vector<typename GraphT::VertexId> > faces;
        };

        typedef std::vector<std::vector<typename GraphT::EdgeId> > planar_embedding_storage_t;
        typedef boost::iterator_property_map<typename planar_embedding_storage_t::iterator,
                                              typename GraphT::VertexIndexPropertyMap> planar_embedding_t;


        PlanarFacesVisitor();
        PlanarFacesVisitor(const PlanarFacesVisitor &other);
        virtual ~PlanarFacesVisitor();

        /**
         * @brief GetFaces Returns a FaceList containing every faces's vertices.
         */
        std::shared_ptr<FaceList> GetFaces(GraphT *graph);
        void begin_face();
        void next_vertex(typename GraphT::VertexId v);

    protected:
        std::shared_ptr<FaceList> faces_;
    };


//    typedef typename GraphT::template GraphDataMapT<int> ComponentMap;
    /**
     * Computes the connected components of a graph.
     * A connected component is the list of every vertices that are linked together.
     * When no path links 2 vertices, directly or indirectcly, they are in 2 different components.
     */
    class ConnectedComponents
    {
    public:
        /**
         * @brief The ComponentData struct is used to represent a connected component.
         * It contains every vertex and edge, as well as its bounding box and an approximated volume.
         * The volume is used to determine which connected component should be the parent/child.
         */
        struct ComponentData
        {
            ComponentData() :
                volume(0.0) {}

            void clear()
            {
                vertices.clear();
                edges.clear();
                volume = 0.0;
                bounding_box = AABBoxT<typename GraphT::BasePoint>();
            }

            std::set<typename GraphT::VertexId> vertices; /*< List of vertices included in this component*/
            std::set<typename GraphT::EdgeId > edges; /*< List of edges included in this component*/
            AABBoxT<typename GraphT::BasePoint> bounding_box;/*< Component's AABBox.*/
            Scalar volume; /*< Component's approximated volume. */
        };
        typedef std::map<typename GraphT::VertexId, int> CompMap;
        typedef std::map<typename GraphT::EdgeId, int> EdgeCompMap;

        /**
         * @brief ExtractComponents Extracts the connected components.
         * Computes and fills #components_ map. Those are then used to determine where Anchor Points must be computed.
         * @return the number of connected components.
         */
        ConnectedComponents(GraphT *graph) :
            graph_(graph),
            componentMap(comp_map)
        {
            ExtractComponents(graph);
        }

        /**
         * @brief ExtractComponents extracts the list of components, and store it in comp_map.
         */
        unsigned int ExtractComponents(GraphT *graph)
        {
            graph_ = graph;

            comp_map.clear();
            edge_comp_map.clear();
            unsigned int num = 0;

            if (graph != nullptr) {
//                typename GraphT::VertexIteratorPtr vertices = graph->GetVertices();
//                while (vertices->HasNext()) {
//                    auto v = vertices->NextId();
//                    boost::put(componentMap, v, -1);
//                }

                typename GraphAlgorithms<GraphT>::ComputeVolumeVisitor volumer;

                num = boost::connected_components(*graph_, componentMap, boost::color_map(graph_->vertex_color_property_map()).vertex_index_map(graph_->vertex_index_property_map()));
                // todo use disjoint_sets to improve reusability. Why not done yet ? Because vertex/edge removal is not handled in those.

                // get every individual components and the list of vertices of each.
                components_.clear();
                components_.resize(num);

                typename GraphT::VertexIteratorPtr vertices = graph_->GetVertices();
                while (vertices->HasNext()) {
                    typename GraphT::VertexPtr vertex = vertices->Next();
                    int c = boost::get(componentMap, vertex->id());
                    components_[c].vertices.insert(vertex->id());
                    components_[c].bounding_box.Enlarge(vertex->pos(), vertex->pos().weight());
                    typename GraphT::EdgeIteratorPtr edges = vertex->GetEdges();
                    while (edges->HasNext()) {
                        typename GraphT::EdgeId id = edges->NextId();
                        components_[c].edges.insert(id);
                        edge_comp_map[id] = c;
                    }
                }

                for (unsigned int i = 0; i < num; ++i) {
                    components_[i].volume = volumer.GetVolume(graph_, *components_[i].vertices.begin());
                }
            }

            return num;
        }

        /**
         * @brief UpdateComponentVolume Updates a given component's volume.
         * Useful when a vertex has been moved.
         * @param component index of the component to be updated.
         */
        void UpdateComponentVolume(unsigned int component)
        {
            if (components_[component].vertices.size() > 0) {
                typename GraphAlgorithms<GraphT>::ComputeVolumeVisitor volumer;
                components_[component].volume = volumer.GetVolume(graph_, *components_[component].vertices.begin());
            }
        }

        /**
         * Returns the component with the largest volume.
         */
        ComponentData &GetLargestComponent()
        {
            Scalar max_volume = 0.0;
            int max_index = -1;
            for (unsigned int i = 0; i < components_.size(); ++i) {
                if (components_[i].volume > max_volume) {
                    max_volume = components_[i].volume;
                    max_index = i;
                }
            }
            assert(max_index != -1);
            return components_[max_index];
        }

        GraphT* graph_; /*< The graph for which hierarchy has to be computed.*/
        CompMap comp_map; /*< property map for boost's connected component visitor. */
        EdgeCompMap edge_comp_map; /*< property map that contains each edge's connected component's index. */
        boost::associative_property_map<CompMap> componentMap; /*< property map for boost's connected component visitor. */

        std::vector<ComponentData > components_; /*< List of every connected components in this graph. */

    };

    /////////////
    //// Computation methods
    /////////////

    /**
      * Checks if point p, with radius rp, is inside Edge ab, of radius varying from ra to rb.
      */
    static bool Contains(const typename GraphT::BasePoint &a, double ra, const typename GraphT::BasePoint &b, double rb, const typename GraphT::BasePoint& p, double rp) ;

    /**
     * @brief GraphAlgorithms<GraphT>::IsInside checks if a point with given radius is inside a dilated Edge.
     * @param c The edge to check from
     * @param ratio the dilatation factor applied on the edge.
     * @param p position of the point to be checked
     * @param radius radius of the point
     * @param[out] index the index of the point that contains p on c. -1 if not inside or not in any point.
     * @return true if p is inside dilated Edge.
     */
    static bool IsInside(std::shared_ptr<typename GraphT::Edge> c, double ratio, const typename GraphT::BasePoint &p, Scalar radius, int &index) ;

//    /**
//      * Applies SAT to a given Graph.
//      * Removed segments are added to #res.
//      * @param graph the graph to filter.
//      * @param ratio SAT dilatation factor.
//      * @param res the Graph that will contain the removed segments of #graph.
//      * @param maxEdfWeight default to 0. If different than 0, SAT will apply each node's weight factors to the dilatation factor.
//      */
//    static void ComputeSAT(std::shared_ptr<GraphT> graph, Scalar ratio, std::shared_ptr<GraphT> res, WeightMap *weights = nullptr);

    static Scalar EPSILON;
};

template<typename GraphT>
Scalar GraphAlgorithms<GraphT>::EPSILON(0.00001);

////////////////////////////////////////////////////////////////////////////////////////////////
///             SIMPLIFY VISITOR
////////////////////////////////////////////////////////////////////////////////////////////////
template<typename GraphT>
GraphAlgorithms<GraphT>::SimplifyVisitor::SimplifyVisitor() :
    boost::default_dfs_visitor(),
    data_(std::make_shared<EdgeList>())
//    boost::predecessor_recorder<typename EdgeList::PredecessorMap, boost::on_tree_edge()>(data_->propmapPredecessor)
{
}

template<typename GraphT>
GraphAlgorithms<GraphT>::SimplifyVisitor::SimplifyVisitor(const SimplifyVisitor &other) :
    boost::default_dfs_visitor(),
//    boost::predecessor_recorder<typename EdgeList::PredecessorMap, boost::on_tree_edge()>(other.data_->propmapPredecessor),
    data_(other.data_)
{
}

template<typename GraphT>
void GraphAlgorithms<GraphT>::SimplifyVisitor::Simplify(std::shared_ptr<GraphT> graph, Scalar distance)
{
    data_->extremities_.clear();
    data_->checked_extremities_.clear();
    graph->GetExtremities(data_->extremities_);

    data_->edges_.clear();
    data_->removed_edges_.clear();
    data_->current_index_ = 0;

    data_->color_map_ = graph->vertex_color_property_map();

    auto vertices = graph->GetVertices();
    while (vertices->HasNext()) {
        auto v = vertices->NextId();
        boost::put(data_->color_map_, v, boost::white_color);
    }

    while (data_->extremities_.size() > 0) {

        typename GraphT::VertexId it = *data_->extremities_.begin();
        try {
            // if branch was not visited already
            if (boost::get(data_->color_map_, it) != boost::black_color) {
                data_->edges_.resize(data_->edges_.size() + 1);
                // browse it
                boost::depth_first_visit(*graph, it, *this, data_->color_map_);

                // This is "non-normal" end : when it was interrupted without meeting an intersection.
                if (data_->edges_[data_->current_index_].size() == 1) {
                    data_->edges_[data_->current_index_].clear();
                } else {
                    data_->current_index_++;
                }

            } else {
                // Remove the extremity we've just been working on.
                data_->extremities_.erase(it);
            }

        } catch (int err) {
            if (err != 1) {
                throw (err);
            }

            // This is expected end
            data_->current_index_++;

            VertexPtr v = graph->Get(it);
            if (v->GetEdgeCount() < 2) {
                data_->extremities_.erase(it);
            }
        }

    }

    for (unsigned int i = 0; i < data_->edges_.size(); ++i) {
        uint s = (unsigned int) data_->edges_[i].size();
        if (s < 2) {
            continue;
        }
        // create a subdivision curve from the list of edges defined

        std::vector<typename GraphT::VertexType> input;
        std::vector<typename GraphT::VertexType> output;
        std::vector<typename GraphT::VertexPtr> outputVertices;

        for (unsigned int j = 0; j < s; ++j) {
            outputVertices.push_back(data_->edges_[i][j]);
            input.push_back(outputVertices[j]->pos());
        }
        VectorTools::Simplify(input, output, distance);
        uint index = 1; // =0;
        for (unsigned int j = 1; j < s - 1; ++j) { // we don't need to compare the extremities as we want to keep them.
            if ((output[index] - input[j]).norm() > 0.0001) { // Comparing distance is more stable than comparing float positions.
//            if (output[index] != input[j]) {
                outputVertices.erase(outputVertices.begin() + index);
            } else {
                index++;
            }
        }

        assert(outputVertices.size() == output.size());
        assert(outputVertices.size() >= 1);

        if (outputVertices.size() == 1) {
            std::shared_ptr<typename GraphT::VertexType > point = std::make_shared<typename GraphT::VertexType >(output[0]);
            // Todo : add it with CreateNewPrimitive. issue : how to convert from 2D to 3D.
            auto res = PrimitiveFactory::GetInstance().CreateNewPrimitive(point);
            if (res == nullptr) {
                graph->Add(std::dynamic_pointer_cast<typename GraphT::EdgeType> (point), outputVertices);
            } else {
                graph->Add(std::dynamic_pointer_cast<typename GraphT::EdgeType> (res), outputVertices);
            }
        } else {
            std::shared_ptr<SubdivisionCurveT<typename GraphT::VertexType> > curve = std::make_shared<SubdivisionCurveT<typename GraphT::VertexType> >(output);
            // Todo : add it with CreateNewPrimitive. issue : how to convert from 2D to 3D.
            auto res = PrimitiveFactory::GetInstance().CreateNewPrimitive(curve);
            if (res == nullptr) {
                graph->Add(std::dynamic_pointer_cast<typename GraphT::EdgeType> (curve), outputVertices);
            } else {
                graph->Add(std::dynamic_pointer_cast<typename GraphT::EdgeType> (res), outputVertices);
            }
        }
    }
    for (typename GraphT::EdgeId e : data_->removed_edges_) {
        graph->Remove(e);
    }

}

template<typename GraphT>
void GraphAlgorithms<GraphT>::SimplifyVisitor::discover_vertex(typename GraphT::VertexId v, const GraphT& /*graph*/)
{
    data_->previous_vertex_ = v;
}

template<typename GraphT>
void GraphAlgorithms<GraphT>::SimplifyVisitor::tree_edge(typename GraphT::EdgeId e, const GraphT& graph)
{
    typename GraphT::EdgePtr edge = graph.Get(e);
    typename GraphT::VertexPtr vertex = data_->previous_vertex_ == edge->GetStartId() ? edge->GetEnd() : edge->GetStart();
    typename GraphT::VertexPtr prevVertex = graph.Get(data_->previous_vertex_);

    data_->counts_[vertex->id()]++;
    data_->counts_[data_->previous_vertex_]++;

    if (data_->edges_[data_->current_index_].size() == 0) { // if new segment
        data_->edges_[data_->current_index_].push_back(prevVertex);
        if (prevVertex->GetEdgeCount() <= data_->counts_[data_->previous_vertex_]) { // make sure vertex is marked as visited when required.
            put(data_->color_map_, data_->previous_vertex_, boost::black_color);
        }
    }
    if (vertex->GetEdgeCount() <= data_->counts_[vertex->id()]) { // make sure vertex is marked as visited when required.
        put(data_->color_map_, vertex->id(), boost::black_color);
    }

    // add vertex to list
    unsigned int i = vertex->GetEdgeCount();
    data_->edges_[data_->current_index_].push_back(vertex);
    data_->removed_edges_.insert(e);
//    typename GraphT::EdgeIteratorPtr edges = vertex->GetEdges(); // TODO : why were all segments removed here ?Why not only #edge ?
//    while (edges->HasNext()) {
//        std::shared_ptr<typename GraphT::Edge> e = edges->Next();
//        data_->removed_edges_.insert(e->id());
//    }

    // if joined an intersection, finish the current branch, register the intersection as a new extremity from which we need to search the graph.
    if (i > 2 && data_->edges_[data_->current_index_].size() > 1) {
        data_->extremities_.insert(vertex->id());
        throw(1);
    }

    // if joined an end point, just finish the current branch. It should be marked as searched.
    if (i == 1 && data_->edges_[data_->current_index_].size() > 1) {
        throw(1);
    }
}

template<typename GraphT>
GraphAlgorithms<GraphT>::ComputeVolumeVisitor::ComputeVolumeVisitor() :
    boost::default_dfs_visitor(),
    data_(std::make_shared<VolumeData>())
{
}

template<typename GraphT>
GraphAlgorithms<GraphT>::ComputeVolumeVisitor::ComputeVolumeVisitor(const ComputeVolumeVisitor &other) :
    boost::default_dfs_visitor(other),
    data_(other.data_)
{
}

template<typename GraphT>
Scalar GraphAlgorithms<GraphT>::ComputeVolumeVisitor::GetVolume(GraphT* graph, typename GraphT::VertexId vertex)
{
    data_->volume = 0.0;
    boost::depth_first_visit(*graph, vertex, *this, graph->vertex_color_property_map());

    return data_->volume;
}

template<typename GraphT>
Scalar GraphAlgorithms<GraphT>::ComputeVolumeVisitor::GetVolume(GraphT* graph)
{
    data_->volume = 0.0;
    boost::depth_first_search(*graph, boost::visitor(*this).color_map(graph->vertex_color_property_map()).vertex_index_map(graph->vertex_index_property_map()));

    return data_->volume;
}

template<typename GraphT>
void GraphAlgorithms<GraphT>::ComputeVolumeVisitor::examine_edge(typename GraphT::EdgeId e, const GraphT &g)
{
    // todo add extremity volume too.
    typename GraphT::EdgePtr edge = g.Get(e);
    auto start = edge->GetStart()->pos();
    auto end = edge->GetEnd()->pos();
    data_->volume += (start + end).norm() * (start.weight() + end.weight()) / 2.0;
}

////////////////////////////////////////////////////////////////////////////////////////////////
///             VARIOUS FUNCTIONS
////////////////////////////////////////////////////////////////////////////////////////////////

template<typename GraphT>
bool GraphAlgorithms<GraphT>::Contains(const typename GraphT::BasePoint &a, double ra, const typename GraphT::BasePoint &b, double rb, const typename GraphT::BasePoint& p, double rp)
{
    typename GraphT::BasePoint ap = p - a;
    typename GraphT::BasePoint dir = b - a;
    Scalar dotprod = dir.dot(ap);
    double projlenSq;

    if (dotprod <= 0.0) {
        return false;
    } else {
        ap = dir - ap;
        dotprod = dir.dot(ap);//dot(dir,ap);
        if (dotprod <= 0.0) {
            return false;
        } else {

            Scalar n = dir.norm();
            projlenSq = (dotprod * dotprod / (n * n));
            Scalar projlen = sqrt(projlenSq);
            double w = rb + (ra - rb) * (projlen / n) - rp;
            if (w < 0.0) {
                return false;
            }
            return ap.squaredNorm() - projlenSq < w*w;
        }
    }
}

template<typename GraphT>
bool GraphAlgorithms<GraphT>::IsInside(std::shared_ptr<typename GraphT::Edge> c, double ratio, const typename GraphT::BasePoint &p, Scalar radius, int &res)
{
    res = -1;
    for (unsigned int i = 0; i < c->GetSize() - 1; ++i) {
        auto &a = c->GetPoint(i);
        auto &b = c->GetPoint(i + 1);
        Scalar ra = a.weight() * ratio;
        Scalar rb = b.weight() * ratio;

        // check extremities.
        Scalar distA = (a - p).norm();
        if (distA + radius < ra) {
            res = i;
            return true;
        }
        Scalar distB = (b - p).norm();
        if (distB + radius < rb) {
            res = i + 1;
            return true;
        }

        // check interior segment.
        if (Contains(a, ra, b, rb, p, radius)) {
            return true;
        }
    }
    return false;
}

template<typename GraphT>
GraphAlgorithms<GraphT>::PlanarFacesVisitor::PlanarFacesVisitor() :
    boost::planar_face_traversal_visitor(),
    faces_(std::make_shared<FaceList>())
{
}

template<typename GraphT>
GraphAlgorithms<GraphT>::PlanarFacesVisitor::PlanarFacesVisitor(const PlanarFacesVisitor &other) :
    boost::planar_face_traversal_visitor(),
    faces_(other.faces_)
{
}

template<typename GraphT>
GraphAlgorithms<GraphT>::PlanarFacesVisitor::~PlanarFacesVisitor()
{
}

template<typename GraphT>
std::shared_ptr<typename GraphAlgorithms<GraphT>::PlanarFacesVisitor::FaceList> GraphAlgorithms<GraphT>::PlanarFacesVisitor::GetFaces(GraphT *graph)
{
    UNUSED(graph);
//    planar_embedding_storage_t storage(graph->GetVertexCount());
//    planar_embedding_t embedding(storage.begin(), graph->vertex_index_property_map());

//    faces_->faces.clear();

//    if (boost::boyer_myrvold_planarity_test(boost::boyer_myrvold_params::graph = *static_cast<typename GraphT::GraphType*>(graph),
//                                            boost::boyer_myrvold_params::embedding = embedding/*,
//                                            boost::boyer_myrvold_params::edge_index_map = graph->edge_index_property_map(),
//                                            boost::boyer_myrvold_params::vertex_index_map = graph->vertex_index_property_map() */
//                                     )) {
//      std::cout << "Input graph is planar" << std::endl;
//      boost::planar_face_traversal(*graph, embedding, *this, graph->edge_index_property_map());
//    } else {
//      std::cout << "Input graph is not planar" << std::endl;
//    }


    return faces_;
}

template<typename GraphT>
void GraphAlgorithms<GraphT>::PlanarFacesVisitor::begin_face()
{
    faces_->faces.push_back(std::vector<typename GraphT::VertexId>());
}

template<typename GraphT>
void GraphAlgorithms<GraphT>::PlanarFacesVisitor::next_vertex(typename GraphT::VertexId v)
{
    faces_->faces.back().push_back(v);
}

} // namespace core

} // namespace expressive

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_GRAPHALGORITHMS
extern template class expressive::core::GraphAlgorithms<expressive::core::WeightedGraph>;
extern template class expressive::core::GraphAlgorithms<expressive::core::WeightedGraph2D>;
#endif
#endif

#endif // GRAPHALGORITHMS_H
