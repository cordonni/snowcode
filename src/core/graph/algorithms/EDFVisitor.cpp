#define TEMPLATE_INSTANTIATION_EDFVISITOR
#include "core/graph/algorithms/EDFVisitor.h"

using namespace std;

namespace expressive {

namespace core {

const double EPSILON = 0.00005;

template<>
std::shared_ptr<EDFVisitor<WeightedGraph2D>::WeightMap> EDFVisitor<WeightedGraph2D>::ComputeEDFOld(bool useWeightedLengths, WeightedGraph2D::BasePoint &EMAPoint, double &maxLen)
{
    // Initialize the list of nodes to be visited.
    // - We want to start computing lengths at extremities, so we first add them there.
    // - We also initialize the weight values for each node.
    set<WeightedGraph2D::VertexPtr> nodes;
    WeightedGraph2D::VertexIteratorPtr ni = graph_->GetVertices();
    while (ni->HasNext()) {
        VPtr n = ni->Next();
        if (n->GetEdgeCount() == 1) {
            Scalar initValue = n->pos().weight();
            if (useWeightedLengths) {
                initValue *= initValue * M_PI;
            }
            (*weights_)[n->id()] = initValue;
            nodes.insert(n);
        } else {
            (*weights_)[n->id()] = expressive::kScalarMax;
        }
    }

    bool found = true;
    maxLen = 0.0;
    VPtr EMA = nullptr;

    set<VPtr> extremities;
    set<GRAPH::EdgeId> visited;

    while (found == true) {
        found = false;
        extremities.clear();

        for (auto it = nodes.begin(); it != nodes.end(); ++it) {
            VPtr n = *it;
            GRAPH::EdgeIteratorPtr edges = n->GetEdges();
            while (edges->HasNext()) {
                EPtr c = edges->Next();
                if (visited.find(c->id()) != visited.end()) {
                    continue;
                }

                // o contains the end of the current extremity branch.
                VPtr o = c->GetOpposite(n);

                visited.insert(c->id());

                double L = c->ComputeCurvilinearLength();

//                if (c->getArea1() != NULL) { // if we are on a loop, skip the curve.
////                    cout << "SKIP CURVE" << endl;
//                    continue;
//                }

                double ui = (*weights_)[o->id()];
                double nui = (*weights_)[n->id()];
                // If we need to use the radius of the curve to update EDF, we compute the integral and apply it to the length
                if (useWeightedLengths) {
                    double ow = o->pos().weight();
                    double nw = n->pos().weight();
                    L *= (ow + nw) / 2.0;
                }
                // current length is length of current curve + length of previous curves along that "axis".
                double len = L + nui;

                // if o wasn't initialized before, or if our current length is higher than the previous curves, we consider this as being the
                // highest value.
                // EMA is only temporary the highest value : it's only used to determine which segment is the midle one.
                // It will be updated afterwards.
                if (len > ui || ui == expressive::kScalarMax) {
                    (*weights_)[o->id()] = len;

                    if (len > maxLen) {
                        maxLen = len;
                        EMA = o;
                    }
                    //maxLen = max(maxLen, len);
                }

                // update current curve's length.

                weights_->at(c->id()) = len;

                // update the length value for every vertex of the current curve.
                for (unsigned int i = 1; i < c->GetSize() - 1; ++i ) {
                    double Li = c->GetL(n->id(), i);
                    // Maybe apply the weight factor.
                    if (useWeightedLengths) {
                        double ow = c->GetPoint(i - 1).weight();
                        double nw = c->GetPoint(i).weight();
                        Li *= (ow + nw) / 2.0;
                    }
                    weights_->at(c->GetVertex(n->id(), i)->id()) = n == c->GetStart() ? nui + Li : len - Li;
                }

                // Check if o is now a new extremity if we remove #c from the graph.
                if (o->GetEdgeCountWithout(visited) == 1) {
                    extremities.insert(o);
                    found = true;
                }
            }
        }
        // if we found new extremities, loop again until the end.
        if (found) {
            nodes.clear();
            nodes = extremities;
        }
    }

    if (EMA == nullptr) {
        return weights_;
    }
    // Now, we browse every curves around the node with the highest value.
    // If the node is not the _real_ max (i.e. there are not 2 curves with the same value around it),
    // we get the curve with highest value and split it at the correct position, and compute the correct value.
    int nbMax = 0;
    double secondMaxLen = 0.0;
    EPtr cMax = nullptr;
    EPtr cSecondMax = nullptr;
    GRAPH::EdgeIteratorPtr edges = EMA->GetEdges();
    while (edges->HasNext()) {
        EPtr c = edges->Next();
        double d = weights_->at(c->id());
        // compare curves' length value to our max value, and count it.
        if (abs(d - maxLen) < EPSILON) {
            nbMax++;
            cMax = c;
        } else {
            // if it's not the max, we want to keep the second highest value around that node. This will help to determine
            // #EMA's real value.
            if (d > secondMaxLen) {
                cSecondMax = c;
                secondMaxLen = d;
            }
        }
    }
//    cout << "nbMAx" << nbMax << endl;
    if (nbMax == 1 /*&& secondMaxLen > 0.0*/) { // if only 1 curve has that highest value, we need to clip it and update the length values.
        // check the direction of the curve.
        bool reverse = EMA == cMax->GetEnd();
        UNUSED(reverse);
        // orig is the lowest value on that curve
        double orig = weights_->at(cMax->GetVertex(EMA->id(), cMax->GetSize() - 1)->id());
//        int offset = reverse ? 1 : -1;
//        if (!reverse) {
//            cout << "INVERT §!!!!!" << endl;
//            cMax->invert();
//        } // forced EMA to be cMax->getStart();
        cMax->ComputeCurvilinearLength();

        // Get the proper EMA's value and position along the curve.
        double newMax = (maxLen + secondMaxLen) / 2.0;
        double ratio = (newMax - orig) / (maxLen - orig);

//        if (!reverse) {
//            ratio = 1.0 - ratio;
//        }

        WeightedPoint2D newV;
        // get curve's length, and get curvilinear length value at EMA, as well as its position.
        Scalar L = cMax->GetL(cMax->GetSize() - 1) * ratio;
        cMax->GetCurvilinearCoordinate(L, &newV);
        EMAPoint = newV;

        // Get the index where the new vertex should be inserted.
        unsigned int index;
        for (index = 1; index < cMax->GetSize(); ++index) {
            if (cMax->GetL(index) > L) {
                break;
            }
        }

        // Now getting the proper weights and correcting the distances of the other points of the curve.
        // First, update the weight of the new vertex.
        ratio = (newMax - weights_->at(cMax->GetVertex(index + 1)->id())) / (newMax - weights_->at(cMax->GetVertex(EMA->id(), index /*- 1*/)->id()));

        double prevWeight = cMax->GetVertex(index + 1)->pos().weight();
        double nextWeight = cMax->GetVertex(index - 1)->pos().weight();

        // Then, set it's weight to the new max value.
        double w = prevWeight + (nextWeight - prevWeight) * ratio;
//        cMax->AddPoint(, index - 1);
        VertexId newPointId = graph_->SplitEdge(cMax->id(), index - 1, WeightedPoint2D(newV, w));
        weights_->at(newPointId) = secondMaxLen;
        edges = graph_->Get(newPointId)->GetEdges();
        while (edges->HasNext()) {
            EPtr e = edges->Next();
            weights_->at(e->id()) = secondMaxLen;
        }
//        index++;


        // Update the Curve's value to be it's extremity's value (not the actual maximum value !! This way, the drawer will know that we are on the main axis).
        weights_->at(cMax->id()) = secondMaxLen;
        weights_->at(EMA->id()) = secondMaxLen;

        // update every other vertex between EMA and fake EMA.

        cMax->ComputeCurvilinearLength();
//        double LEMA = cMax->GetL(cMax->GetSize() - 1);
//        if (!reverse) {
//            LEMA = 0.0;
//        }
        L = cMax->GetL(index);
//        for (unsigned int i = 0; i < cMax->GetSize(); i++) {
//            double Li = cMax->GetL(i);
//            cout << "LLL: " << Li << ":" << L << ":" << LEMA << endl;
//            double ratio = (Li - L) / (LEMA - L);
//            weights_->at(cMax->GetVertex(i)->id()) = newMax + ratio * (secondMaxLen - newMax);
//        }

        maxLen = newMax;

        if (cMax->GetOpposite(EMA->id())->GetEdgeCount() == 1) {
            weights_->at(cMax->GetVertex(cMax->GetSize() - 1)->id()) = cMax->GetVertex(cMax->GetSize() - 1)->pos().weight();
        }

//        cMax->setUserInfo
    } else {
        EMAPoint = EMA->pos();
//        drawCircle(overlay_, EMA->getPos(), Vector(1.0, 0.0, 0.0), 0.03);
    }

    return weights_;
}

template<>
Scalar EDFVisitor<WeightedGraph2D>::GetLongestPath(std::shared_ptr<WeightedGraph2D> graph, WeightedGraph2D::VertexPtr start, WeightedGraph2D::EdgePtr edge, std::shared_ptr<WeightedGraph2D::GraphDataMapT<Scalar> > weights, std::vector<WeightedGraph2D::VertexId> &ids)
{
    std::shared_ptr<WeightedVertex2D> opposite = edge->GetOpposite(start);
    ids.push_back(opposite->id());
    Scalar w = weights->at(edge->id());
    WeightedGraph2D::EdgeIteratorPtr edges = opposite->GetEdges();
    while (edges->HasNext()) {
        WeightedGraph2D::EdgePtr e = edges->Next();
        if (e == edge) {
            continue;
        } else {
            if (weights->at(e->id()) == weights->at(opposite->id())) {
                GetLongestPath(graph, opposite, e, weights, ids);
                return w;
            }
        }
    }

    return w;
}

template<>
Scalar EDFVisitor<WeightedGraph2D>::GetInitWeight(WeightedGraph2D::VertexPtr n)
{
    if (n->GetEdgeCount() == 1) {
        Scalar initValue = n->pos().weight();
        if (container_->useWeightedLengths) {
            initValue *= initValue * M_PI / 2.0;
        }
//            initValue = 0.0;
        return initValue;
    } else {
        return expressive::kScalarMax;
    }
}

template<>
Scalar EDFVisitor<WeightedGraph2D>::GetWeight(WeightedGraph2D::EdgePtr edge, WeightedGraph2D::VertexPtr start, WeightedGraph2D::VertexPtr end)
{
    double L = edge->ComputeCurvilinearLength();
    if (container_->useWeightedLengths) {
        double sw = start->pos().weight();
        double ew = end->pos().weight();
        L *= (sw + ew) / 2.0;
    }
    return L;
}

template<>
WeightedGraph2D::VertexPtr EDFVisitor<WeightedGraph2D>::UpdateWeights(std::shared_ptr<WeightedGraph2D::GraphDataMapT<Scalar> > weights, const std::vector<WeightedGraph2D::VertexId> &ids, Scalar startW, Scalar maxW)
{
    VPtr vs = graph_->Get(ids[0]);
    VPtr ve = nullptr;
    for (unsigned int i = 0; i < ids.size() - 1; ++i) {
        ve = graph_->Get(ids[i+1]);

        EPtr edge = nullptr;
        WeightedGraph2D::EdgeIteratorPtr edges = vs->GetEdges();
        while (edges->HasNext()) {
            edge = edges->Next();
            if (edge->GetOpposite(vs)->id() == ve->id()) {
                break;
            }
        }
        Scalar &sW = weights->at(ids[i]);
//        Scalar &eW = weights->at(ids[i+1]);
        Scalar edgeW = GetWeight(edge, vs, ve);

        sW = startW;
        startW += edgeW;
        if (maxW - (sW + edgeW) < EPSILON) {
            container_->maxLen = maxW;
            weights_->at(ve->id()) = maxW;
            edges = ve->GetEdges();
            while (edges->HasNext()) {
                weights_->at(edges->NextId()) = maxW;
            }
            return ve;
        } else if (sW + edgeW > maxW) {
            // Here, we reached the point where the edge should be split (at EMA !)
            Scalar ratio = (maxW - sW) / edgeW;
            Point2 maxP = vs->pos() + ratio * (ve->pos() - vs->pos());
            Scalar w = vs->pos().weight() + ratio * (ve->pos().weight() - vs->pos().weight());
            WeightedGraph2D::VertexId newId = graph_->SplitEdge(edge->id(), 1, WeightedPoint2D(maxP, w));
            VPtr newV = graph_->Get(newId);
            edges = newV->GetEdges();
            while (edges->HasNext()) {
                weights_->at(edges->NextId()) = maxW;
            }
            weights_->at(newId) = maxW;
            container_->maxLen = maxW;
            return newV;
        }
        weights_->at(edge->id()) = startW;
        vs = ve;
    }

    weights_->at(ve->id()) = maxW;
    return ve;
}

template<>
std::shared_ptr<EDFVisitor<WeightedGraph2D>::WeightMap> EDFVisitor<WeightedGraph2D>::ComputeEDF(bool useWeightedLengths, WeightedGraph2D::BasePoint &EMAPoint, double &maxLen)
{
    container_->useWeightedLengths = useWeightedLengths;

    WeightedGraph2D::VertexColorPropertyMap color_map = graph_->vertex_color_property_map();
    // init all weights at either radius at begin or at kScalarMax.
    WeightedGraph2D::VertexIteratorPtr ni = graph_->GetVertices();
    while (ni->HasNext()) {
        VPtr n = ni->Next();
        (*weights_)[n->id()] = GetInitWeight(n);
        boost::put(color_map, n->id(), boost::white_color);
    }

    graph_->GetExtremities(container_->extremities);

    while (container_->extremities.size() > 0) {
        WeightedGraph2D::VertexId id = (*container_->extremities.begin());
        container_->extremities.erase(id);
        container_->current_extremity = id;
        container_->previous_vertex = id;
        try {
            boost::depth_first_visit(*graph_, id, *this, color_map);
        } catch( int err) {
            if (err != 1) {
                throw (err);// not what we expected. error 1 is sent when reaching a new fork, and then we should check the next extremity.
            }
        }
        if (container_->extremities.size() == 0) {
            container_->extremities = container_->new_extremities;
            container_->new_extremities.clear();
        }
    }

    if (container_->EMA->GetEdgeCount() == 1) {
        // Get the longest path from this extremity.
        std::vector<WeightedGraph::VertexId> ids;
        ids.push_back(container_->EMA->id());
        Scalar w = GetLongestPath(graph_, container_->EMA, container_->EMA->GetEdges()->Next(), weights_, ids);
        Scalar realMax = w / 2.0;
        container_->EMA = UpdateWeights(weights_, ids, GetInitWeight(container_->EMA), realMax);
    } else {
        WeightedGraph2D::EdgeIteratorPtr edges = container_->EMA->GetEdges();
        // Get the 2 highest weights : they are 2 sides of the longest path.
        EPtr eMax = nullptr;
        EPtr eSecondMax = nullptr;
        Scalar wMax = 0;
        Scalar wSecondMax = 0;

        while (edges->HasNext()) {
            EPtr edge = edges->Next();
            Scalar w = weights_->at(edge->id());
            if (w > wMax) {
                wSecondMax = wMax;
                wMax = w;
                eSecondMax = eMax;
                eMax = edge;
            } else if (w > wSecondMax) {
                wSecondMax = w;
                eSecondMax = edge;
            }
        }

        std::vector<WeightedGraph2D::VertexId> maxIds;
        std::vector<WeightedGraph2D::VertexId> secondMaxIds;
        maxIds.push_back(container_->EMA->id());
        Scalar w1 = GetLongestPath(graph_, container_->EMA, eMax, weights_, maxIds);
        Scalar w2 = GetLongestPath(graph_, container_->EMA, eSecondMax, weights_, secondMaxIds);
        Scalar realMax = (w1 + w2) / 2.0;
        edges = container_->EMA->GetEdges();
        while (edges->HasNext()) {
            auto e = edges->Next();
        }
        container_->EMA = UpdateWeights(weights_, maxIds, w2, realMax);
    }
    container_->maxLen = weights_->at(container_->EMA->id());

    if (container_->EMA != nullptr) {
        EMAPoint = container_->EMA->pos();
    }
    maxLen = container_->maxLen;

    return weights_;
}

template<>
void EDFVisitor<WeightedGraph2D>::tree_edge(typename WeightedGraph2D::EdgeId e, const WeightedGraph2D& g)
{
    EPtr edge = g.Get(e);
    VPtr start = g.Get(container_->previous_vertex);
    VPtr end = edge->GetOpposite(start);

    double L = GetWeight(edge, start, end);

    Scalar  sWeight = weights_->at(start->id());
    Scalar &eWeight = weights_->at(end->id());
    Scalar len = L + sWeight;
    if (eWeight == expressive::kScalarMax || len > eWeight) {
        eWeight = len;
        if (len > container_->maxLen) {
            container_->maxLen = len;
            container_->EMA = end;
        }
    }

    weights_->at(e) = len;
    container_->visited_edges.insert(e);
    if(end->GetEdgeCount() > 1) {
        if (end->GetEdgeCountWithout(container_->visited_edges) == 1) {
            container_->new_extremities.insert(end->id());
        }
        throw 1;
    }

}

// explicit instantiation
template class EDFVisitor<WeightedGraph2D>;

}

}

