/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#ifndef EXPRESSIVE_GRAPH_H_
#define EXPRESSIVE_GRAPH_H_


// core dependencies
#include <core/utils/Serializable.h>
#include <core/geometry/WeightedPoint.h>
#include <core/geometry/WeightedSegment.h>
#include <core/utils/ListenerT.h>

#include <core/functor/FunctorFactory.h>
#include <core/geometry/PrimitiveFactory.h>

// std dependencies
#include <vector>
#include <sstream>

#if defined(_MSC_VER)
#pragma warning(push, 0)
//#pragma warning(disable: 4619 4371)
#endif

#include <boost/graph/adjacency_list.hpp>

#if defined(_MSC_VER)
#pragma warning(pop)
#pragma warning(push)
// disable "this" utilisé dans la liste des initialiseurs de membre de base" on graph ctor.
// Todo : How to not use "this" for listeners ?MSVC complains about it everywhere listeners are used.
#pragma warning(disable:4355)

#endif

namespace expressive
{

namespace core
{

// TODO : Add AbstractGraph to make this more readable ? This would allow to outsource GraphListener.
// Issue : How to add the EdgeId/VertexId for GraphListener... ?
// Dirty workaround : adding a non-templated GraphListener based on Graph instead of GraphT.

template<typename VertexType>
class GraphListenerT;

template<typename VertexType>
class VertexT;

template<typename VertexType>
class EdgeT;


/**
  * This class proposes a common structure for geometric graphs used in expressive.
  * It is templated over the type of vertex stored in it.
  * It uses GeometryPrimitives as Edges (hence Geometric graphs).
  * It simplifies traversals, offers a wide variety of predefined visitors, and allows users
  * to define their own quite easily.
  * // TODO : Add AbstractGraph.
  * // Difficulty : How to make an AbstractEdgeId for example ?
  */
template<typename VertexTypeT>
class GraphT :
        public boost::adjacency_list<
            boost::listS,
            boost::listS,
            boost::undirectedS,
            std::shared_ptr<VertexT<VertexTypeT> >,
            std::shared_ptr<EdgeT<VertexTypeT> >
            //boost::property<boost::vertex_index_t, std::size_t,
                //boost::property<boost::vertex_color_t, boost::default_color_type,
                    //std::shared_ptr<VertexT<VertexTypeT> > > >,
            //boost::property<boost::edge_index_t, std::size_t, std::shared_ptr<EdgeT<  VertexTypeT> > > //,
//            boost::property<boost::vertex_color_t, boost::default_color_type>
        >,
        public ListenableT<GraphT<  VertexTypeT>>,
        public Serializable
{
public:
    EXPRESSIVE_MACRO_NAME("Graph")

    /////////
    /// Defines associated to boost::graph.
    ///

    typedef VertexTypeT VertexType; //< internal vertex type : This is the Point primitive used as the graph's base (Usually, either WeightedPoint, PointPrimitive, PointPrimitive2D...).
    typedef GeometryPrimitiveT<VertexType> EdgeType; //< internal GeometryPrimitive typedef, which will use VertexType.

    // Graph Data
    typedef VertexT<VertexType> Vertex; //< External vertex type : This is the boost vertex. It contains a VertexType.
    typedef EdgeT  <VertexType> Edge; //< External vertex type : This is the boost edge. It contains a EdgeType.
    typedef typename VertexType::BasePoint BasePoint; //< This is the very base of our PointPrimitive (differentiates between Point2 or Point3).

    // Graph Pointers
    typedef std::shared_ptr<Vertex> VertexPtr; //< typedef for a shared ptr of vertex.
    typedef std::shared_ptr<Edge>   EdgePtr; //< typedef for a shared ptr of edge.
    typedef std::shared_ptr<GraphT> GraphPtr; //< typedef for a shared ptr of graph.

//    // Graph Property
//    typedef boost::property<boost::vertex_all_t, Vertex> VertexProperty;
//    typedef boost::property<boost::edge_all_t  , Edge>   EdgeProperty;

    // Graph Descriptor
    typedef typename boost::graph_traits<GraphT>::vertex_descriptor VertexId; //< A unique id for each vertex.
    typedef typename boost::graph_traits<GraphT>::edge_descriptor   EdgeId; //< A unique id for each edge.

    // Graph Iterator
    typedef typename boost::graph_traits<GraphT>::edge_iterator      BoostEdgeIterator; //< boost edge iterator, for internal use.
    typedef typename boost::graph_traits<GraphT>::out_edge_iterator  BoostOutEdgeIterator; //< boost out edge iterator, for internal use.
    typedef typename boost::graph_traits<GraphT>::vertex_iterator    BoostVertexIterator; //< boost vertex iterator, for internal use.
    typedef typename boost::graph_traits<GraphT>::adjacency_iterator BoostAdjacentVertexIterator; //< boost adjacent vertex iterator, for internal use.


    // Graph Type
    typedef boost::adjacency_list<
        boost::listS, // < listS is used to be able to remove vertices in our graph. Not possible with default vecS.
        boost::listS, // < listS is used to be able to remove edges in our graph. Not possible with default vecS.
        boost::undirectedS, // < Graphs are undirected, i.e. each edge will only be browsed once in a full traversal.
        std::shared_ptr<VertexT<VertexTypeT> >, // < Creates a default vertex_index_t for our graph that will contain shared_ptr to VertexType.
        std::shared_ptr<EdgeT<VertexTypeT> > //< same for edges. These 2 are mandatory because they are not defined by default with the listS container defined above.
        /*ColorProperty */> GraphType;

    /////////
    /// Boost traversal related data.
    ///
    // Graph Index Maps.
    // We create those to have default vertex_index_map & edge_index even though we use ListS instead of VecS.
    // Helps the user to not have to define them manually.
    typedef typename boost::graph_traits<GraphT>::vertices_size_type InternalVertexId; // < type for unique id associated to our vertices, used as an integral for access in maps/vectors.
    typedef typename boost::graph_traits<GraphT>::edges_size_type    InternalEdgeId;// < type for unique id associated to our edges, used as an integral for access in maps/vectors.
    typedef std::map<VertexId, InternalVertexId>          VertexIndexMap; //< A VertexIndexMap maps a vertex to an index. It's used to link our vertices to boost's internal alorithms stuff.
    typedef std::map<VertexId, boost::default_color_type> VertexColorMap; //< A VertexColorMap maps a vertex to a color. It's used to determine if a vertex was traversed or not by a visitor.
    typedef std::map<EdgeId, InternalEdgeId>              EdgeIndexMap; //< An EdgeIndexMap doest the same as a VertexIndexMap, but for edges.
    typedef boost::associative_property_map<VertexIndexMap> VertexIndexPropertyMap; //< Makes our VertexIndexMap readable from boost algorithms. Required by BGL's internal soup.
    typedef boost::associative_property_map<VertexColorMap> VertexColorPropertyMap; //< Makes our VertexColorMap readable from boost algorithms. Required by BGL's internal soup.
    typedef boost::associative_property_map<EdgeIndexMap>   EdgeIndexPropertyMap; //< Makes our EdgeIndexMap readable from boost algorithms. Required by BGL's internal soup.

    // Graph Listener
    typedef ListenerT     <GraphT<VertexType> > GraphListenerBase; //< A more readable name for Listener.
    typedef ListenableT   <GraphT<VertexType> > Listenable;  //< A more readable name for a Listenable.
    typedef GraphListenerT       <VertexType>   GraphListener;   //< A more readable name for GraphListener.

    /**
     * GraphIterator class
     * Allows to have the same API used for any type of content in graphs (edges, vertices, and inherited versions of them.
     * Also offsers the possibility to browse either through ids only, or through real ptrs to content.
     * Note that the second might be a bit slower, since it requires fetching the actual data.
     *
     * This is the abstract version, usefull to have simple names used in user code.
     */
    template<typename S>
    class GraphIterator {
    public:
        // Constructor Destructor
        GraphIterator(const GraphT *owner) : owner_(owner) {}
        virtual ~GraphIterator() {}

        // Getters
        virtual std::shared_ptr<S> Next   () = 0;
        virtual typename S::ID     NextId () = 0;
        virtual bool               HasNext() const = 0;

        // Data
        const GraphT *owner_;
    };

    /**
     * GraphIteratorT class
     * Same as GraphIterator, but is also templated on iterator, in order to provide various constructors.
     * Usefull when constructing the iterator, but not when using it, hence the presence of the abstract version.
     */
    template<typename S, typename T>
    class GraphIteratorT : public GraphIterator<S> {
    public:
        // Constructor / Destructor
        GraphIteratorT(const GraphT *owner, T begin, T end) :
            GraphIterator<S>(owner), iterator_(begin), end_(end) {}

        GraphIteratorT(const GraphT *owner, const std::pair<T, T> &p) :
            GraphIterator<S>(owner), iterator_(p.first), end_(p.second) {}

        GraphIteratorT(const GraphIteratorT & other) :
            GraphIterator<S>(other.owner_),
            iterator_(other.iterator_),
            end_(other.end_)
        {
        }

        virtual ~GraphIteratorT() {}

        // Getters
        virtual std::shared_ptr<S> Next() {
            T it = iterator_++;
            return GraphIterator<S>::owner_->Get(*it);
        }

        virtual typename S::ID NextId() {
            T it = iterator_++;
            return *it;
        }

        virtual bool HasNext() const { return iterator_ != end_; }

        // Data
        T iterator_;
        T end_;
    };

    /**
     * GraphDataMapT class
     * Offers a way to store arbitrary data to any edge/vertex in our graph inside a map.
     * Uses the standard boost way to store stuff.
     */
    template<typename TYPE>
    struct GraphDataMapT
    {
    protected:
        std::map<typename GraphT::VertexId, TYPE> vertex_data; //< Maps a vertex to a TYPE.
        std::map<typename GraphT::EdgeId, TYPE> edge_data; //< Maps an edge to a TYPE.

    public:
        TYPE &operator[] (typename GraphT::VertexId id) { return vertex_data[id]; } //< accessing operator for vertices.
        TYPE &operator[] (typename GraphT::EdgeId id) { return edge_data[id]; } //< accessing operator for edges.

        TYPE &at(typename GraphT::VertexId id) { return vertex_data[id]; }  //< accessing operator for vertices.
        TYPE &at(typename GraphT::EdgeId id) { return edge_data[id]; } //< accessing operator for edges.

        bool contains(typename GraphT::VertexId id) const { return vertex_data.find(id) != vertex_data.end(); } // returns true if data was set for this vertex.
        bool contains(typename GraphT::EdgeId id) const { return edge_data.find(id) != edge_data.end(); } // returns true if data was set for this edge.
    };

    typedef GraphIterator<Edge>                     EdgeIterator; // < Base type for edge iterator.
    typedef GraphIterator<Vertex>                   VertexIterator; // < Base type for vertex iterator.
    typedef std::shared_ptr<GraphIterator<Edge> >   EdgeIteratorPtr; // < a Ptr to an edge iterator. This is used to offer various type of iterators (sets, vectors, etc) in a transparent way.
    typedef std::shared_ptr<GraphIterator<Vertex> > VertexIteratorPtr; // < a Ptr to a vertex iterator. This is used to offer various type of iterators (sets, vectors, etc) in a transparent way.

    typedef GraphIteratorT<Edge  , BoostEdgeIterator>           BaseEdgeIterator; // < A basic iterator based on boost's edges iterators. only for internal usage.
    typedef GraphIteratorT<Edge  , BoostOutEdgeIterator>        OutEdgeIterator; // < A basic iterator based on boost's out edges iterators. only for internal usage.
    typedef GraphIteratorT<Vertex, BoostVertexIterator>         BaseVertexIterator; // < A basic iterator based on boost's vertex iterators. only for internal usage.
    typedef GraphIteratorT<Vertex, BoostAdjacentVertexIterator> AdjacentVertexIterator; // < A basic iterator based on boost's adjacent vertices iterators. only for internal usage.

    typedef GraphIteratorT<Edge  , typename std::set   <EdgeId  >::iterator> EdgeSetIterator; // An iterator on a set of edges.
    typedef GraphIteratorT<Edge  , typename std::vector<EdgeId  >::iterator> EdgeVectorIterator; // An iterator on a vectorof edges.

    typedef GraphIteratorT<Vertex, typename std::set   <VertexId>::iterator> VertexSetIterator; // An iterator on a set of vertices.
    typedef GraphIteratorT<Vertex, typename std::vector<VertexId>::iterator> VertexVectorIterator; // An iterator on a vector of vertices.

    /**
     * The PrimitiveMap class maps the primitives that were added to the graph to every corresponding Edge in this graph.
     * For example, for a Triangle, it will map 3 edges. For a Segment, 2.
     * Allows a quick access 2-way to primitives & edges from each other.
     */
    class PrimitiveMap : public std::map<std::shared_ptr<EdgeType>, std::vector<EdgeId > >
    {
    public:
        PrimitiveMap(GraphT *owner) : owner_(owner) {}

        /**
         * @brief GetEdge Returns an edge that was added from a given primitive.
         * @param primitive The Primitive to get the edge from.
         * @param index the index of the edge inside primitive.
         */
        std::shared_ptr<Edge> GetEdge(std::shared_ptr<EdgeType> primitive, unsigned int index) const
        {
            if (primitive == nullptr) {
                return nullptr;
            } else {
                auto it = ((PrimitiveMap*)this)->find(primitive);
                if (it == ((PrimitiveMap*)this)->end()) {
                    return nullptr;
                }
                if (it->second.size() <= index) {
                    return nullptr;
                }
                return owner_->Get(it->second[index]);
            }
        }

        /**
         * @brief GetEdges Returns every edge that was added from a given primitive.
         * @param primitive The Primitive to get the edges from.
         */
        EdgeIteratorPtr GetEdges(std::shared_ptr<EdgeType> primitive) const
        {
            if (primitive == nullptr) {
                return nullptr;
            } else {
                auto it = ((PrimitiveMap*)this)->find(primitive);
                if (it == ((PrimitiveMap*)this)->end()) {
                    return nullptr;
                }
                return std::make_shared<EdgeVectorIterator>(owner_, it->second.begin(), it->second.end());
            }
        }
        GraphT *owner_;
    };

    /**
     * The GraphChanges struct contains a list of changes that were applied on a graph.
     * Changes are :
     * - Added edges
     * - updated edges
     * - Removed edges
     * - Added vertices
     * - updated vertices
     * - Removed vertices
     */
    struct GraphChanges{
        GraphChanges(GraphT<VertexType> *owner) : owner_(owner) {}
        std::set<EdgeId>   added_edges_;
        std::set<EdgeId>   updated_edges_;
        std::set<EdgeId>   removed_edges_;
        std::set<VertexId> added_vertices_;
        std::set<VertexId> updated_vertices_;
        std::set<VertexId> removed_vertices_;

        GraphT<VertexType> *owner_;

        /**
         * @brief Insert allows to merge 2 list of changes. If edges/vertices were removed,
         * they won't be stored as "added" or "updated" anymore.
         */
        void Insert(const GraphChanges &next);

        /**
         * Removes every changes from this list.
         */
        void Clear();

        VertexIteratorPtr AddedVertices  () const; //< returns the list of added vertices.
        VertexIteratorPtr UpdatedVertices() const; //< returns the list of updated vertices.
        VertexIteratorPtr RemovedVertices() const; //< returns the list of removed vertices.
        EdgeIteratorPtr   AddedEdges     () const; //< returns the list of added edges.
        EdgeIteratorPtr   UpdatedEdges   () const; //< returns the list of updated edges.
        EdgeIteratorPtr   RemovedEdges   () const; //< returns the list of removed edges.
    };

    /**
     * Creates an empty graph.
     * Also initializes a few traversal structures (vertex index map, edge index map, and vertex color map). Those can be used for any visitor.
     */
    GraphT() :
        GraphType                     (),
        Listenable                    (),
        Serializable                  ("Graph"),
        changes_(GraphChanges         (this)),
        edges_(PrimitiveMap(this)),
        internal_vertex_id_(0),
        internal_edge_id_(0)
    {
        // Register IndexMap
        vertex_index_property_map_ = VertexIndexPropertyMap(vertex_index_map_);
        vertex_color_property_map_ = VertexColorPropertyMap(vertex_color_map_);
        edge_index_property_map_   = EdgeIndexPropertyMap  (edge_index_map_);
    }

    virtual ~GraphT() {}

    inline VertexIndexMap &vertex_index_map(); //< returns the VertexIndexMap.
    inline VertexColorMap &vertex_color_map(); //< returns the VertexColorMap.
    inline EdgeIndexMap   &edge_index_map(); //< returns the EdgeIndexMap.
    inline VertexIndexPropertyMap &vertex_index_property_map(); //< returns the VertexIndexMap reader.
    inline EdgeIndexPropertyMap   &edge_index_property_map(); //< returns the VertexColorMap reader.
    inline VertexColorPropertyMap &vertex_color_property_map(); //< returns the EdgeIndexMap reader.

    inline const GraphChanges &changes() const { return changes_; } //< returns the current list of changes since last "flush".

    std::string to_string(bool details = false) const; //< returns a serialized version of this graph, as a string. Should only be used for debugging purposes.

    bool Has(const VertexId &id); //< returns true if this graph has a given vertex.
    bool Has(const EdgeId &id); //< returns true if this graph has a given edge.

    /**
      * Adds a given edge to the graph.
      * If edge contains multiple points, a new edge will be created for each
      * and only the last one will be returned.
      * If addToChanges is left to true, it will also be added to the next update's list of changes.
      * Set it to false when loading stuff for example.
      */
//    std::shared_ptr<Edge> Add(const EdgeType &edge);
    EdgeIteratorPtr Add(const EdgeType &edge, bool addToChanges = true);
    EdgeIteratorPtr Add(const EdgeType &edge, VertexId start, VertexId end, bool addToChanges = true);
    EdgeIteratorPtr Add(std::shared_ptr<EdgeType> shared_edge, VertexId start, VertexId end, bool addToChanges = true);
    EdgeIteratorPtr Add(std::shared_ptr<EdgeType> shared_edge, bool addToChanges = true);
    EdgeIteratorPtr Add(std::shared_ptr<EdgeType> shared_edge, std::map<std::string, std::shared_ptr<Vertex> > &vertices, bool addToChanges = true);
    /**
     * @brief Add Adds a given primitive to the graph. Specified vertices will be used
     * This is the only version that should be overloaded when inheriting graphs.
     * @param shared_edge
     * @param vertices
     * @param addToChanges
     * @return
     */
    virtual EdgeIteratorPtr Add(std::shared_ptr<EdgeType> shared_edge, std::vector<std::shared_ptr<Vertex> > &vertices, bool addToChanges = true);

    EdgePtr AddSegment(const VertexType &start, const VertexType &end, bool addToChanges = true);
    EdgePtr AddSegment(VertexPtr start, const VertexType &end, bool addToChanges = true);
    EdgePtr AddSegment(VertexId start, const VertexType &end, bool addToChanges = true);
    EdgePtr AddSegment(VertexId start, VertexId end, bool addToChanges = true);
    EdgePtr AddSegment(VertexPtr start, VertexPtr end, bool addToChanges = true);

    /**
      * Adds the given vertex to the graph.
      */
    std::shared_ptr<Vertex> Add(const VertexType &vertex, bool addToChanges = true);
    virtual std::shared_ptr<Vertex> Add(std::shared_ptr<VertexType> vertex, bool addToChanges = true);

    /**
     * Removes an Edge Primitive from the graph, and all its corresponding edges.
     */
    virtual void Remove(std::shared_ptr<EdgeType> edge);

    /**
     * @brief Remove Removes a given edge from the graph.
     */
    virtual void Remove(const EdgeId &id);

    /**
     * @brief Remove Removes a given vertex from the graph.
     */
    virtual void Remove(const VertexId &id);

    /**
      * Returns the number of vertices in the graph.
      */
    inline unsigned int GetVertexCount() const;

    /**
      * Returns the number of edges in the graph.
      */
    inline unsigned int GetEdgeCount() const;
    inline unsigned int GetEdgeCount(const VertexId &id) const;

    /**
      * Returns an iterator on this graph's edges. Also returns the end
      * pointer for that iterator
      */
    inline EdgeIteratorPtr GetEdges() const;
    inline EdgeIteratorPtr GetEdges(std::shared_ptr<EdgeType> primitive);
    inline EdgePtr GetEdge(std::shared_ptr<EdgeType> primitive, unsigned int index);
    inline const PrimitiveMap &GetPrimitives() const { return edges_; }

    /**
      * Returns an iterator on this graph's vertices. Also returns the end
      * pointer for that iterator.
      */
    inline VertexIteratorPtr GetVertices             () const;
    inline VertexIteratorPtr GetSelectedVertices     () const;
    inline unsigned int      GetSelectedVerticesCount() const;

    /**
     * Returns the ids of the currently selected vertices.
     * @param[out] ids  the ids of the currently selected vertices.
     * @param getEdgeVertices if true, ids will also contains the vertices from the selected edges.
     */
    void GetSelectedVerticesIds(std::set<VertexId> &ids, bool getEdgeVertices = false) const;

    inline const std::set<VertexId> &GetSelectedVerticesIds() const; //< same as GetSelectedvertices(ids, false);

    /**
     * Returns the ids of the selected edges.
     */
    inline const std::set<EdgeId> &GetSelectedEdgesIds() const;

    /**
     * Returns an iterator on the selected edges.
     */
    inline EdgeIteratorPtr GetSelectedEdges() const;

    /**
     * Returns an iterator on the selected edges.
     */
    inline unsigned int GetSelectedEdgesCount() const;

    /**
     * Returns the list of extremities : Every vertex that has only 1 neighbor.
     * @param[out] extremities a list of vertex ids.
     */
    void GetExtremities(std::set<VertexId> &extremities) const;

    /**
      * Returns the actual vertex associated to v in this graph.
      * @param v a vertex index created by this graph.
      */
    inline std::shared_ptr<Vertex> Get(VertexId v) const {
        return (*this)[v];
    }

    /**
      * Returns the actual edge associated to e in this graph.
      * @param e an edge index created by this graph.
      */
    inline std::shared_ptr<Edge> Get(EdgeId e) const {
        return (*this)[e];
    }

    /**
     * The function that should be called to notify the listeners of this graph that a change occured.
     * This also "flushes " the list of changes in #changes_.
     */
    virtual void NotifyUpdate();

    /**
     * When changing a vertex, it is not guaranteed that every primitive that uses it will be notified that it changed.
     * This method is here to take care of it : it makes sure everything remains coherent by updating the neighbors.
     * @param id the id of the vertex that was updated.
     * @param addToChanges if true, the neighbors will be added to #changes_ and then updated at next #notifyUpdate call.
     */
    void Update1RingNeighborhood(VertexId id, bool addToChanges = true);

    /**
     * @brief MovePoint Moves a point to a given position.
     * @param id the id of the vertex that should be moved.
     * @param point the new position of the vertex.
     * @param addToChanges if true, id will be added to the list of changes to notify.
     */
    void MovePoint(VertexId id, const typename VertexType::BasePoint & point, bool addToChanges = true);

    ///////////////////
    // Xml functions //
    ///////////////////
    /// \brief ToXml Exprorts this graph as an XML element. This version also creates a map that maps the vertices to an index, which
    /// allows to properly link to those later, when saving other stuff that uses the graph.
    /// \param ids After the call, this will map every vertex & edge to a unique index.
    /// \param tagName the name of the graph in the xml element.
    /// \param embed_vertices_in_edges if true, vertices will be saved in the edges. otherwise, they will be saved on their own, making them available
    ///                                 for every xml element that need to use them.
    /// \return an xml element.s
    ///
    virtual TiXmlElement *ToXml(std::map<const void*, unsigned int> &ids, const char *tagName = nullptr, bool embed_vertices_in_edges = false) const;

    virtual TiXmlElement *ToXml(const char *tagName = nullptr, bool embed_vertices_in_edges = false) const;

    virtual TiXmlElement *ToXml(const char *name) const
    {
        return ToXml(name, true);
    }

    /**
     * Returns the bounding box of this graph.
     */
    virtual AABBoxT<typename VertexType::BasePoint> axis_bounding_box() const;

    virtual void set_selected(const VertexId &id, bool selected = true);//< Toggles the selected state of a given vertex.

    virtual void set_selected(const EdgeId &id, bool selected = true); //< Toggles the selected state of a given edge.

    /**
     * Splits a given edge at a given index, and forces the split to be at a given position.
     * @param id the id of the edge to split.
     * @param index where to split the edge.
     * @param point the position of the split.
     * @return the id of the newly created vertex.s
     */
    virtual VertexId SplitEdge(const EdgeId &id, unsigned int index, const VertexType& point);

    /**
     * Applies a deformation to the whole Graph.
     * @param mat the transformation to apply.
     */
    virtual void ApplyDeformation(const Matrix4 &mat, bool addToChanges = true);

    /**
     * @brief MergeVertices Merges the given vertices together, and makes sure that connections with neighbors are maintained correctly.
     * @param vertices the ids of the vertices that needs to be merged.
     * @param moveToBarycenter if true, final point's coordinates will be the barycenter of the merged points. Otherwise, it will be the first in the list of points.
     */
    VertexId MergeVertices(const std::set<VertexId> &vertices, VertexId targetVertex = boost::graph_traits<GraphT>::null_vertex());

    /**
     * @brief MergeCloseVertices Simplifies a graph by merging close vertices together and thus removing unnecessary edges.
     * @param epsilon distance at which we want vertices to be considered the same, and merge them.
     * @param merge_unlinked_vertices whether or not we want to merge unlinked vertices.
     */
    void MergeCloseVertices(Scalar epsilon, bool merge_unlinked_vertices = false);

    /**
     * Creates a new segment based on this graph's type. Important for factory based stuff (Weighted graph...).
     * @param start a VertexPtr
     * @param end a VertexPtr
     * @return a pointer to a new segment from start to end.
     */
    std::shared_ptr<SegmentT<VertexTypeT> > CreateSegment(VertexPtr start, VertexPtr end);

protected:

    virtual void Init() {}

    virtual void swap(ork::ptr<GraphT> t) {
        GraphT *t2 = t.get();
        GraphType::swap(*t2);
        Listenable::swap(*t2);
        std::swap(edges_, t->edges_);
//        std::swap(listeners_, t->listeners_);
    }

    VertexIndexMap vertex_index_map_; //< used for BGL algorithms.
    VertexColorMap vertex_color_map_; //< used for BGL algorithms.
    EdgeIndexMap   edge_index_map_; //< used for BGL algorithms.
    VertexIndexPropertyMap vertex_index_property_map_;// < Creates a default vertex_index_t for our graph
    VertexColorPropertyMap vertex_color_property_map_;// < Creates a default vertex_index_t for our graph
    EdgeIndexPropertyMap   edge_index_property_map_;// < Creates a default edge_index_t for our graph

    GraphChanges changes_; //< List of changes since the last NotifyUpdate(). Used to determine what needs to be refreshed.

    // contains, for each EdgeType, the corresponding EdgePtrs. Mostly useful for primitives that may create multiple Edges.
    PrimitiveMap edges_; //< the list of edges in this graph.

    InternalVertexId internal_vertex_id_; //< a counter to ensure unique internal ids on vertices.
    InternalEdgeId internal_edge_id_; //< a counter to ensure unique internal ids on edges.

    // Selected Data
    std::set<VertexId> selected_vertices_; //< the lsit of selected vertices.
    std::set<EdgeId>   selected_edges_; //< the lsit of selected edges.

//    std::set<GraphListener *> listeners_;

};

/**
 * VertexT is the type that holds vertices information in GraphT.
 * It mostly holds a pointer to a given position, and other informations.
 * It's also aware of it's neighbors and it's parent.
 * Vertices can also be stored with their ID, and they can then be fetched from the graph through their id.
 */
template<typename VertexType>
class VertexT { // node
public:
    typedef GraphT<VertexType> Graph;
    typedef typename GraphT<VertexType>::VertexId ID;

    VertexT();

    VertexT(Graph *owner, typename Graph::InternalVertexId internal_id, std::shared_ptr<VertexType> pos);

    VertexT(Graph *owner, typename Graph::InternalVertexId internal_id, const VertexType &pos);

    virtual ~VertexT();

    bool operator==(const VertexT &other ) const;

    std::string to_string() const; //< serializatiion for debugging purpose.

    const VertexType &pos() const; //< returns the position of this vertex.

    VertexType &nonconst_pos() const; //< returns the position of this vertex.

    inline typename Graph::VertexId id() const; //< returns this vertex's id.

    inline typename Graph::InternalVertexId internal_id() const; //< Returns this vertex's internal id (BGL related).

    typename Graph::EdgeIteratorPtr  GetEdges() const; //< Returns an iterator over the edges around this vertex.

    std::pair<typename Graph::BoostAdjacentVertexIterator, typename Graph::BoostAdjacentVertexIterator> GetAdjacentVerticesId() const; //< Returns an iterator over the neighbors of this vertex.

    typename Graph::VertexIteratorPtr GetAdjacentVertices() const; //< Returns an iterator over the neighbors of this vertex.

    unsigned int GetEdgeCount() const; //< returns the number of neighbors around this vertex.

    unsigned int GetEdgeCountWithout(std::set<typename Graph::EdgeId> excluded) const; //< Returns the number of neighbors around this vertex, without considering the ones in a given list.

    virtual void updated(); //< this should be run when the Vertex was updated.

    virtual Graph *owner() const; // returns the graph that contains this vertex.

    inline const std::shared_ptr<VertexType> posptr() const; //< returns a pointer to the PointPrimitive stored in this vertex.

    inline std::shared_ptr<VertexType> nonconst_posptr(); //< returns a pointer to the PointPrimitive stored in this vertex.

protected:
    void set_pos(const VertexType &pos); //< Update the position of this vertex. Only graph & edges are allowed to do that.

    Graph *owner_; //< The graph that contains this vertex.

    typename Graph::InternalVertexId internal_id_; //< This vertex's internal id.

    std::shared_ptr<VertexType> pos_; //< This Vertex embeded PointPrimitive.

    typename Graph::VertexId id_; //< This vertex's id.

    friend class GraphT<VertexType>;

    friend class EdgeT<VertexType>;
};

/**
 * EdgeT is the type that holds edge information in GraphT.
 * It links 2 or more VertexT, and can store many intermediate points.
 * It's also aware of it's neighbors and it's parent.
 * EdgeTs can also be stored with their ID, and they can then be fetched from the graph through their id.
 */
template<typename VertexTypeT>
class EdgeT { // curve
public:
    typedef VertexTypeT VertexType;
    typedef GraphT<VertexType> Graph;
    typedef typename GraphT<VertexType>::EdgeId ID;
    typedef typename GraphT<VertexType>::EdgeType EdgeType;

    enum EdgeFlag
    {
        E_BASIC = 0,
        E_VIRTUAL = 1//,
        // E_MULTIPART = 2,
        // E_SOMETHINGELSE = 4...
    };

    EdgeT(); //<ctor.

    /**
     * @brief EdgeT ctor
     * @param owner the graph that owns this.
     * @param edge The GeometryPrimitive embedded in this edge.
     * @param internal_id The internal id of this edge.
     * @param index the index of this edge amongst it's GeometryPrimitive (can be a triangle for example, with 3 edges).
     * @param start The VertexId of the Vertex used as the start position.
     * @param end The VertexId of the Vertex used as the end position.
     */
    EdgeT(Graph *owner, const std::shared_ptr<EdgeType> edge, typename Graph::InternalEdgeId internal_id, unsigned int index, typename Graph::VertexId start, typename Graph::VertexId end);

    virtual ~EdgeT(); //< deletor.

    bool operator==(const EdgeT &other ) const;

    std::string to_string() const; //< serialization method, for debugging purpose.

    inline Graph *owner() const; //< this edge's graph.

    inline typename Graph::EdgeId id() const; //< Returns this edge's id.

    inline typename Graph::InternalEdgeId internal_id() const; //< returns this edge's internal id.

    inline const EdgeType &edge() const; //< Returns this edge's embedded GeometryPrimitive.

    inline EdgeType &nonconst_edge() const ; //< Returns this edge's embedded GeometryPrimitive.

    inline const std::shared_ptr<EdgeType> edgeptr() const;//< Returns this edge's embedded GeometryPrimitive.

    inline std::shared_ptr<EdgeType> nonconst_edgeptr() const;//< Returns this edge's embedded GeometryPrimitive.

    inline unsigned int index() const; //< Returns the index of this edge amongst it's GeometryPrimitive.

    inline EdgeFlag type() const; //< Returns this edge's type.

    inline void set_type(const EdgeFlag &type); //< Sets this edge's type.

    unsigned int GetPrimitiveIndex(unsigned int index = 0) const; //< Same as index(), but relative to the index given in parameter.

    unsigned int GetPrimitiveIndex(typename Graph::VertexId start, unsigned int index = 0) const; //< Same as before, but relative to a starting point too.

    std::shared_ptr<typename Graph::Vertex> GetStart() const; //< Returns the starting point of this edge.

    std::shared_ptr<typename Graph::Vertex> GetEnd() const; //< Returns the end point of this edge.

    typename Graph::VertexId GetStartId() const;  //< Returns the starting point of this edge.

    typename Graph::VertexId GetEndId() const; //< Returns the end point of this edge.

    typename Graph::VertexId GetOppositeId(typename Graph::VertexId id) const; //< Returns the opposite point of the given one.

    std::shared_ptr<typename Graph::Vertex> GetOpposite(typename Graph::VertexId id) const; //< Returns the opposite point of the given one.

    std::shared_ptr<typename Graph::Vertex> GetOpposite(std::shared_ptr<typename Graph::Vertex> n) const; //< Returns the opposite point of the given one.

    unsigned int GetSize() const; //< Returns the size of the embedded GeometryPrimitive.

    VertexType &GetPoint(unsigned int index) const; //< Returns the point at given position on this edge.

    VertexType &GetPoint(typename Graph::VertexId start, unsigned int index) const; //< Returns the point at given position on this edge, starting at given extremity.

    typename VertexType::BasePoint GetInterpolatedPoint(Scalar ratio) const; //< Gives an interpolated point, between start (0.0) and end (1.0).

    std::shared_ptr<typename Graph::Vertex> GetVertex(unsigned int index) const; //< Returns the vertex at given index.

    std::shared_ptr<typename Graph::Vertex> GetVertex(typename Graph::VertexId start, unsigned int index) const; //< Returns the vertex at given index.

    void AddPoint(VertexType v); //< Adds a Vertex at the end of this Edge.

    void AddPoint(VertexType v, unsigned int index); //< Adds a vertex.

    std::shared_ptr<EdgeT> RemovePoint(unsigned int index); //< Removes a point.

    std::shared_ptr<EdgeT> RemovePoint(typename Graph::VertexId start, unsigned int index); //< Removes a point.

    Scalar GetCurvilinearLength(Scalar s, VertexType &p, VertexType &n) const; //< Returns the curvilinear length at a given S (between 0 and n_points). Also returns the normal at that point.

    Scalar ComputeCurvilinearLength() const; //< Computes the curvilinear length of the edge.

    Scalar GetCurvilinearCoordinate(Scalar l, VertexType *p) const; //< Returns the curvilinear coordinate (between 0 & n_points) of a given curvilinear length.

    Scalar GetL(unsigned int index = 0) const; //< Returns the curvilinear length at a given index.

    Scalar GetL(typename Graph::VertexId start, unsigned int index = 0) const; //< Returns the curvilinear length at a given index.

//    std::shared_ptr<EdgeT> Inverted();

    void Copy(std::shared_ptr<EdgeT> other, bool reversed); //< Copies the vertices from a given Edge. possibly reversed if required.

    virtual void updated(); //< Should be called when updating the edge.

protected:
    unsigned int GetIndex(typename Graph::VertexId start, unsigned int index) const; //< Computes the internal index of a point of this edge inside the GeometryPrimitive's space.

    Graph *owner_;

    std::shared_ptr<EdgeType> edge_;

    typename Graph::InternalEdgeId internal_id_;

    unsigned int index_;

    typename Graph::EdgeId id_;

    mutable std::vector<Scalar> curvilinear_length_;

    typename Graph::VertexId start_;

    typename Graph::VertexId end_;

    EdgeFlag type_; //< This flag can be used for whatever you want : It's just a placeholder.

    friend class GraphT<VertexType>;

    friend class VertexT<VertexType>;
};

/**
 * A standard GraphListener, when user needs to watch when a graph is updated.
 * 4 methods are provided, for adding and removal of Edges & Vertices.
 * When an edge or a vertex is updated, it's first considered removed and then added back.
 * VertexUpdated/EdgeUpdated methods may be added later.
 */
template<typename VertexType>
class GraphListenerT : public ListenerT<GraphT<VertexType> >
{
public:
    typedef GraphT<VertexType> Graph;
    typedef ListenerT<Graph> Base;

    GraphListenerT(Graph* g) : Base(g) { }

    virtual ~GraphListenerT() { }

//    virtual void EdgeAdded(typename Graph::EdgeId) = 0;
//    virtual void EdgeRemoved(typename Graph::EdgeId) = 0;
//    virtual void VertexAdded(typename Graph::VertexId) = 0;
//    virtual void VertexRemoved(typename Graph::VertexId) = 0;

    virtual void Notify(const typename Graph::GraphChanges &changes) = 0;
    virtual void updated(Graph* t)
    {
        return Notify(t->changes());
    }

    virtual Graph* graph() const { return Base::target_; }

protected:
    GraphListenerT() : Base() {}
    virtual void Init(Graph* graph) { Base::Init(graph); }
};


// It is Also possible de define graphs for other applications than geometry.
typedef GraphT<PointPrimitive> Graph;
typedef EdgeT<PointPrimitive> Edge;
typedef VertexT<PointPrimitive> Vertex;
typedef std::shared_ptr<Graph> GraphPtr;
typedef std::shared_ptr<Edge> EdgePtr;
typedef std::shared_ptr<Vertex> VertexPtr;
typedef Graph::EdgeId EdgeId;
typedef Graph::VertexId VertexId;
typedef GraphListenerT<PointPrimitive> GraphListener;

typedef GraphT<PointPrimitiveT<Point2> > Graph2D;
typedef EdgeT<PointPrimitiveT<Point2> > Edge2D;
typedef VertexT<PointPrimitiveT<Point2> > Vertex2D;

typedef GraphT<WeightedPoint> WeightedGraph;
typedef EdgeT<WeightedPoint> WeightedEdge;
typedef VertexT<WeightedPoint> WeightedVertex;
typedef std::shared_ptr<WeightedGraph> WeightedGraphPtr;
typedef std::shared_ptr<WeightedEdge> WeightedEdgePtr;
typedef std::shared_ptr<WeightedVertex> WeightedVertexPtr;
typedef WeightedGraph::EdgeId WeightedEdgeId;
typedef WeightedGraph::VertexId WeightedVertexId;
typedef GraphListenerT<WeightedPoint> WeightedGraphListener;

typedef GraphT<WeightedPointT<Point2> > WeightedGraph2D;
typedef EdgeT<WeightedPointT<Point2> > WeightedEdge2D;
typedef VertexT<WeightedPointT<Point2> > WeightedVertex2D;

} // namespace core
} // namespace expressive


#include <core/graph/Graph.hpp>
#include <core/graph/Edge.hpp>
#include <core/graph/Vertex.hpp>

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_GRAPH

extern template class expressive::core::GraphT<expressive::core::PointPrimitive>;
extern template class expressive::core::GraphT<expressive::core::PointPrimitiveT<expressive::Point2> > ;
extern template class expressive::core::GraphT<expressive::core::WeightedPoint>;
extern template class expressive::core::GraphT<expressive::core::WeightedPointT<expressive::Point2> >;

//extern template class expressive::core::VertexT<expressive::core::PointPrimitiveT<Point> >;
//extern template class expressive::core::VertexT<expressive::core::PointPrimitiveT<Point2> > ;
//extern template class expressive::core::VertexT<expressive::core::WeightedPoint>;
//extern template class expressive::core::VertexT<expressive::core::WeightedPointT<Point2> >;

//extern template class expressive::core::EdgeT<expressive::core::PointPrimitive>;
//extern template class Eexpressive::core::dgeT<expressive::core::PointPrimitiveT<Point2> > ;
//extern template class expressive::core::EdgeT<expressive::core::WeightedPoint>;
//extern template class expressive::core::EdgeT<expressive::core::WeightedPointT<Point2> >;

extern template class expressive::core::GraphListenerT<expressive::core::PointPrimitive>;
extern template class expressive::core::GraphListenerT<expressive::core::WeightedPoint>;
#endif
#endif

#if defined(_MSC_VER)
#pragma warning(pop)
#endif
#endif // EXPRESSIVE_GRAPH_H_
