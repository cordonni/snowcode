/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

//#ifndef ARAPDEFORMER_H
//#define ARAPDEFORMER_H

//#include <core/graph/Graph.h>

//#include <memory>
//#include <boost/graph/depth_first_search.hpp>
//#include <Eigen/Dense>

//using namespace std;

//namespace expressive
//{

//namespace core
//{

//const string ARAPConstrainedAttributeName = "ARAPConstrained";
//const string ARAPWeightAttributeName = "ARAPWeight";

//class ARAPDeformer
//{
//public:

//    struct SolveData
//    {
//        Eigen::Matrix<float, Eigen::Dynamic, 1>& p;
//        const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>& LWithConstraint;
//        const Eigen::Matrix<float, Eigen::Dynamic, 1>& BWithConstraint;
//    };

//    /**
//     * \brief Construct an ARAPDeformer object.
//     *
//     * When constructing this object, you allow this class to modify your graph when calling the method deformMesh().
//     * When constructor is called, 3 matrices are computed. It takes vertex times edges operations to compute.
//     */
//    ARAPDeformer(std::shared_ptr<Graph> graph);

//    virtual ~ARAPDeformer();

//    /**
//     * \brief Change the graph to deform.
//     */
//    void NewGraph(std::shared_ptr<Graph> graph);

//    /**
//     * \brief This method deforms the graph following the As Rigid As Possible principle [Sorkine - As rigid as possible surface modeling 2009].
//     *
//     * This deformation conserves as rigidly as possible the origin mesh. It consists in two steps.
//     * First a computation of local rotations on the vertices.
//     * Second an update of the positions following the constraints defined by user.
//     * For best performance, compile your project with the intel compiler. It will optimize the Eigen code.
//     */
//    std::shared_ptr<Graph> DeformGraph(unsigned int iterations = );

//    static void ComputeBasicWeights(std::shared_ptr<Graph> graph);

//    static void ComputeCotWeights(std::shared_ptr<Graph> graph);

//    static Point ComputeBi(std::shared_ptr<Graph> graph,
//                           std::shared_ptr<Vertex> vert);

//    static void ComputeB(std::shared_ptr<Graph> graph,
//                         Eigen::Matrix<float, Eigen::Dynamic, 1> Bx,
//                         Eigen::Matrix<float, Eigen::Dynamic, 1> By,
//                         Eigen::Matrix<float, Eigen::Dynamic, 1> Bz );

//    static void ComputeL(std::shared_ptr<Graph> graph,
//                         Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>& L);

//    static void ComputeRotation(std::shared_ptr<Graph> graph);

//    static void SolveFunction(void *data);

//    static void ComputeNewPositions(std::shared_ptr<Graph> graph,
//                                    const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>& precomputedL,
//                                    const Eigen::Matrix<float, Eigen::Dynamic, 1>& precomputedBx,
//                                    const Eigen::Matrix<float, Eigen::Dynamic, 1>& precomputedBy,
//                                    const Eigen::Matrix<float, Eigen::Dynamic, 1>& precomputedBz);

//protected:
//    std::shared_ptr<Graph> graph_;

//    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> L_;
//    Eigen::Matrix<float, Eigen::Dynamic, 1> Bx_;
//    Eigen::Matrix<float, Eigen::Dynamic, 1> By_;
//    Eigen::Matrix<float, Eigen::Dynamic, 1> Bz_;
//};

//class MinOneConstraintDFS : public boost::default_dfs_visitor
//{
//    MinOneConstraintDFS(std::shared_ptr<Graph> graph,
//            Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>& L,
//            Eigen::Matrix<float, Eigen::Dynamic, 1>& Bx,
//            Eigen::Matrix<float, Eigen::Dynamic, 1>& By,
//            Eigen::Matrix<float, Eigen::Dynamic, 1>& Bz) :
//                graph_(graph), L_(L), Bx_(Bx), By_(By), Bz_(Bz), is_constrained_(false)
//    {
//    }

//    // inherited from default_dfs_visitor
//    template<typename VertexT, typename GraphT>
//    void discover_vertex(VertexT u, GraphT &/*g*/)
//    {
//        VertexPtr v = graph_->Get(u);
//        if (v->GetAttribute(ARAPConstrainedAttributeName))
//        {
//            is_constrained_ = true;
//            L_(u, u) += 1.f;
//            Bx_(u) += v->pos()[0];
//            By_(u) += v->pos()[1];
//            Bz_(u) += v->pos()[2];
//        }
//    }

//    template<typename VertexT, typename GraphT>
//    void start_vertex(VertexT u, GraphT &/*g*/)
//    {
//        is_constrained_ = false;
//    }

//    template<typename VertexT, typename GraphT>
//    void finish_vertex(VertexT u, GraphT &/*g*/)
//    {
//        if (!is_constrained_)
//        {
//            VertexPtr v = graph_->Get(u);
//            L_(u, u) += 1;
//            Bx_(u) += v->pos()[0];
//            By_(u) += v->pos()[1];
//            Bz_(u) += v->pos()[2];
//        }
//    }
//protected:
//    std::shared_ptr<Graph> graph_;
//    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>& L_;
//    Eigen::Matrix<float, Eigen::Dynamic, 1>& Bx_;
//    Eigen::Matrix<float, Eigen::Dynamic, 1>& By_;
//    Eigen::Matrix<float, Eigen::Dynamic, 1>& Bz_;
//    bool is_constrained_;
//};

//} // namespace core

//} // namespace expressive

//#endif // ARAPDEFORMER_H
