#ifndef GRAPH_HPP
#define GRAPH_HPP

#include "core/graph/Graph.h"
#include <cstdint>

namespace expressive {
namespace core {
/////////////////////////
/// GraphT
///


template<typename VertexType>
void GraphT<VertexType>::GraphChanges::Insert(const GraphChanges &c)
{
    for (auto id : c.removed_edges_) {
        added_edges_.erase(id);
        updated_edges_.erase(id);
        removed_edges_.insert(id);
    }

    for (auto id : c.removed_vertices_) {
        added_vertices_.erase(id);
        updated_vertices_.erase(id);
        removed_vertices_.insert(id);
    }

    added_vertices_.insert(c.added_vertices_.begin(), c.added_vertices_.end());
    added_edges_.insert(c.added_edges_.begin(), c.added_edges_.end());
    updated_vertices_.insert(c.updated_vertices_.begin(), c.updated_vertices_.end());
    updated_edges_.insert(c.updated_edges_.begin(), c.updated_edges_.end());
}

template<typename VertexType>
void GraphT<VertexType>::GraphChanges::Clear()
{
    added_edges_.clear();
    removed_edges_.clear();
    added_vertices_.clear();
    removed_vertices_.clear();
    updated_edges_.clear();
    updated_vertices_.clear();
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::GraphChanges::AddedEdges() const
{
    return std::make_shared<EdgeSetIterator>(owner_, added_edges_.begin(), added_edges_.end());
}
template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::GraphChanges::UpdatedEdges() const
{
    return std::make_shared<EdgeSetIterator>(owner_, updated_edges_.begin(), updated_edges_.end());
}
template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::GraphChanges::RemovedEdges() const
{
    return std::make_shared<EdgeSetIterator>(owner_, removed_edges_.begin(), removed_edges_.end());
}

template<typename VertexType>
typename GraphT<VertexType>::VertexIteratorPtr GraphT<VertexType>::GraphChanges::AddedVertices() const
{
    return std::make_shared<VertexSetIterator>(owner_, added_vertices_.begin(), added_vertices_.end());
}

template<typename VertexType>
typename GraphT<VertexType>::VertexIteratorPtr GraphT<VertexType>::GraphChanges::UpdatedVertices() const
{
    return std::make_shared<VertexSetIterator>(owner_, updated_vertices_.begin(), updated_vertices_.end());
}

template<typename VertexType>
typename GraphT<VertexType>::VertexIteratorPtr GraphT<VertexType>::GraphChanges::RemovedVertices() const
{
    return std::make_shared<VertexSetIterator>(owner_, removed_vertices_.begin(), removed_vertices_.end());
}

template<typename VertexType>
typename GraphT<VertexType>::VertexIndexMap &GraphT<VertexType>::vertex_index_map()
{
    return vertex_index_map_;
}

template<typename VertexType>
typename GraphT<VertexType>::VertexColorMap &GraphT<VertexType>::vertex_color_map()
{
    return vertex_color_map_;
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIndexMap &GraphT<VertexType>::edge_index_map()
{
    return edge_index_map_;
}

template<typename VertexType>
typename GraphT<VertexType>::VertexIndexPropertyMap &GraphT<VertexType>::vertex_index_property_map()
{
    return vertex_index_property_map_;
}

template<typename VertexType>
typename GraphT<VertexType>::VertexColorPropertyMap &GraphT<VertexType>::vertex_color_property_map()
{
    return vertex_color_property_map_;
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIndexPropertyMap &GraphT<VertexType>::edge_index_property_map()
{
    return edge_index_property_map_;
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::GetEdges() const
{
    return std::make_shared<BaseEdgeIterator>(this, boost::edges(*this));
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::GetEdges(std::shared_ptr<EdgeType> primitive)
{
    return edges_.GetEdges(primitive);
}

template<typename VertexType>
typename GraphT<VertexType>::EdgePtr GraphT<VertexType>::GetEdge(std::shared_ptr<EdgeType> primitive, unsigned int index)
{
    return edges_.GetEdge(primitive, index);
}

template<typename VertexType>
typename GraphT<VertexType>::VertexIteratorPtr GraphT<VertexType>::GetVertices() const {
    return std::make_shared<BaseVertexIterator>(this, boost::vertices(*this));
}

template<typename VertexType>
typename GraphT<VertexType>::VertexIteratorPtr GraphT<VertexType>::GetSelectedVertices() const {
    return std::make_shared<VertexSetIterator>(this, selected_vertices_.begin(), selected_vertices_.end());
}

template<typename VertexType>
void GraphT<VertexType>::GetSelectedVerticesIds(std::set<VertexId> &ids, bool getEdgeVertices) const {
    ids = selected_vertices_;

    if (getEdgeVertices) {
        auto it = GetSelectedEdges();
        while (it->HasNext()) {
            EdgePtr e = it->Next();
            ids.insert(e->start_);
            ids.insert(e->end_);
        }
    }
}

template<typename VertexType>
const std::set<typename GraphT<VertexType>::VertexId> &GraphT<VertexType>::GetSelectedVerticesIds() const {
    return selected_vertices_;
}

template<typename VertexType>
const std::set<typename GraphT<VertexType>::EdgeId> &GraphT<VertexType>::GetSelectedEdgesIds() const {
    return selected_edges_;
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::GetSelectedEdges() const {
//    std::pair<typename GraphT<VertexType>::VertexIterator, typename GraphT<VertexType>::VertexIterator> GraphT<VertexType>::GetVertices() const {
    return std::make_shared<EdgeSetIterator>(this, selected_edges_.begin(), selected_edges_.end());
}

template<typename VertexType>
unsigned int GraphT<VertexType>::GetSelectedEdgesCount() const {
    return (uint) selected_edges_.size();
}

template<typename VertexType>
unsigned int GraphT<VertexType>::GetSelectedVerticesCount() const {
    return (uint) selected_vertices_.size();
}

template<typename VertexType>
bool GraphT<VertexType>::Has(const VertexId &id)
{
    return vertex_index_map_.find(id) != vertex_index_map_.end();
}

template<typename VertexType>
bool GraphT<VertexType>::Has(const EdgeId &id)
{
    return edge_index_map_.find(id) != edge_index_map_.end();
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::Add(const EdgeType &edge, typename GraphT<VertexType>::VertexId start, typename GraphT<VertexType>::VertexId end, bool addToChanges)
{
    assert(edge.GetSize() == 2);
    std::shared_ptr<EdgeType> shared_edge = edge.clone();
    std::vector<std::shared_ptr<Vertex> > vertices = {Get(start), Get(end)};
    return Add(shared_edge, vertices, addToChanges);
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::Add(std::shared_ptr<EdgeType> shared_edge, typename GraphT<VertexType>::VertexId start, typename GraphT<VertexType>::VertexId end, bool addToChanges)
{
    assert(shared_edge->GetSize() == 2);
    std::vector<std::shared_ptr<Vertex> > vertices = {Get(start), Get(end)};
    return Add(shared_edge, vertices, addToChanges);
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::Add(const EdgeType &edge, bool addToChanges)
{
    std::shared_ptr<EdgeType> shared_edge = edge.clone();
    return Add(shared_edge, addToChanges);
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::Add(std::shared_ptr<EdgeType> shared_edge, bool addToChanges)
{
    std::vector<std::shared_ptr<Vertex> > vertices;
    std::shared_ptr<Vertex> v = Add(shared_edge->GetPoint(0));
    vertices.push_back(v);

    for (unsigned int i = 1; i < shared_edge->GetSize(); ++i) {
        v = Add(shared_edge->GetPoint(i));
        vertices.push_back(v);
    }

    if (shared_edge->isLoop()) {
        vertices.push_back(vertices[0]);
    }

    return Add(shared_edge, vertices, addToChanges);
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::Add(std::shared_ptr<EdgeType> shared_edge, std::map<std::string, std::shared_ptr<Vertex> > &vertex_map, bool addToChanges)
{
    std::vector<std::shared_ptr<Vertex> > vertices;


    std::shared_ptr<Vertex> v;
//    auto vertex_iterator = vertex_map.find(shared_edge->GetPoint(0).ToString());
//    if (vertex_iterator != vertex_map.end()) {
//        v = vertex_iterator->second;
//    } else {
//        v = Add(shared_edge->GetPoint(0));
//        vertex_map[shared_edge->GetPoint(0).ToString()] = v;
//    }

//    vertices.push_back(v);


    for (unsigned int i = 0; i < shared_edge->GetSize(); ++i) {
        auto vertex_iterator = vertex_map.find(shared_edge->GetPoint(i).ToString());
        if (vertex_iterator != vertex_map.end()) {
            v = vertex_iterator->second;
        } else {
            v = Add(shared_edge->GetPoint(i));
            vertex_map[shared_edge->GetPoint(i).ToString()] = v;
        }
        vertices.push_back(v);
    }

    if (shared_edge->isLoop()) {
        vertices.push_back(vertices[0]);
    }

    return Add(shared_edge, vertices, addToChanges);
}

template<typename VertexType>
typename GraphT<VertexType>::EdgeIteratorPtr GraphT<VertexType>::Add(std::shared_ptr<EdgeType> shared_edge, std::vector<std::shared_ptr<Vertex> > &vertices, bool addToChanges)
{
    assert((vertices.size() == shared_edge->GetSize() && shared_edge->isLoop() == false) || (shared_edge->isLoop() && vertices.size() == shared_edge->GetSize() + 1));

    std::shared_ptr<Vertex> prev = vertices[0];

    bool error = false;

    uint size = (unsigned int) edges_[shared_edge].size();
    if (size != 0) {
        ork::Logger::ERROR_LOGGER->log("GRAPH", "Primitive is already in graph. Just returning existing edges.");
    } else {
        for (unsigned int i = 1; i < vertices.size()/*shared_edge->GetSize()*/; ++i) {
            std::shared_ptr<Vertex> cur = vertices[i];
            bool ok;
            std::shared_ptr<Edge> e = std::make_shared<Edge>(this, shared_edge, internal_edge_id_++, i - 1, prev->id(), cur->id());
            // if edge was already inside the graph in the other direction,
            // it won't be added, but immediatly returned in ed.
            std::tie(e->id_, ok) = boost::add_edge(prev->id(), cur->id(), e, *this);
            boost::put(edge_index_property_map_, e->id_, e->internal_id_);

            error |= ok == false;
            if (addToChanges) {
                changes_.added_edges_.insert(e->id());
            }
            edges_[shared_edge].push_back(e->id());

            prev = cur;
        }
//        internal_edge_id_++;
    }

    auto &it = edges_[shared_edge];
    return std::make_shared<EdgeVectorIterator>(this, it.begin(),it.end());
}

template<typename VertexType>
typename GraphT<VertexType>::EdgePtr GraphT<VertexType>::AddSegment(const VertexType &start, const VertexType &end, bool addToChanges)
{
    VertexPtr v0 = Add(start, addToChanges);
    VertexPtr v1 = Add(end, addToChanges);
    return AddSegment(v0, v1, addToChanges);
}

template<typename VertexType>
typename GraphT<VertexType>::EdgePtr GraphT<VertexType>::AddSegment(typename GraphT<VertexType>::VertexPtr start, const VertexType &end, bool addToChanges)
{
    VertexPtr v = Add(end, addToChanges);
    return AddSegment(start, v, addToChanges);
}

template<typename VertexType>
typename GraphT<VertexType>::EdgePtr GraphT<VertexType>::AddSegment(typename GraphT<VertexType>::VertexId start, const VertexType &end, bool addToChanges)
{
    VertexPtr v0 = Get(start);
    VertexPtr v1 = Add(end, addToChanges);
    return AddSegment(v0, v1, addToChanges);
}

template<typename VertexType>
typename GraphT<VertexType>::EdgePtr GraphT<VertexType>::AddSegment(typename GraphT<VertexType>::VertexId start, typename GraphT<VertexType>::VertexId end, bool addToChanges)
{
    VertexPtr v0 = Get(start);
    VertexPtr v1 = Get(end);
    return AddSegment(v0, v1, addToChanges);
}

template<typename VertexType>
typename GraphT<VertexType>::EdgePtr GraphT<VertexType>::AddSegment(typename GraphT<VertexType>::VertexPtr start, typename GraphT<VertexType>::VertexPtr end, bool addToChanges)
{
    EdgeIteratorPtr res = Add(CreateSegment(start, end), start->id(), end->id(), addToChanges);
    if (res->HasNext()) {
        return res->Next();
    }
    return nullptr;
}

template<typename VertexType>
std::shared_ptr<typename GraphT<VertexType>::Vertex> GraphT<VertexType>::Add(const VertexType &vertex, bool addToChanges)
{
    return Add(std::dynamic_pointer_cast<VertexType>(vertex.clone()), addToChanges);
}

template<typename VertexType>
std::shared_ptr<typename GraphT<VertexType>::Vertex> GraphT<VertexType>::Add(std::shared_ptr<VertexType> vertex, bool addToChanges)
{
    std::shared_ptr<Vertex> v = std::make_shared<Vertex>(this, internal_vertex_id_++, vertex);
    v->id_ = boost::add_vertex(v, *this);
    boost::put(vertex_index_property_map_, v->id_, v->internal_id_);

    if (addToChanges) {
        changes_.added_vertices_.insert(v->id_);
    }
    return v;
}

template<typename VertexType>
void GraphT<VertexType>::Remove(const VertexId &id)
{
    if (vertex_index_map_.find(id) == vertex_index_map_.end()) {
        return;
    }
    std::shared_ptr<VertexT<VertexType> > v = Get(id);

    typename GraphT::EdgeIteratorPtr edges = v->GetEdges();
    bool has_edge = false;
    while (edges->HasNext()) {
        Remove(edges->Next()->id());
        has_edge = true;
    }

    if (!has_edge) {
        boost::remove_vertex(id, *this);
    }

    selected_vertices_.erase(id);

    changes_.removed_vertices_.insert(id);
    vertex_index_map_.erase(id);
}

template<typename VertexType>
void GraphT<VertexType>::Remove(const EdgeId &id)
{
    auto e = Get(id);

    std::set<VertexId> removedVertices;
    if(e->GetStart()->GetEdgeCount() > 1) {
        changes_.updated_vertices_.insert(e->GetStartId());
    } else {
        removedVertices.insert(e->GetStartId());
    }
    if(e->GetEnd()->GetEdgeCount() > 1) {
        changes_.updated_vertices_.insert(e->GetEndId());
    } else {
        removedVertices.insert(e->GetEndId());
    }

    if (e->edgeptr()->GetSize() == 2) {
        edges_.erase(e->edgeptr());
    } else {
        unsigned int index = e->index_;
        e->edgeptr()->RemovePoint(index);
        EdgeIteratorPtr edges = GetEdges(e->edgeptr());
        while (edges->HasNext()) {
            EdgePtr e2 = edges->Next();
            if (e2->index_ > index) {
                e2->index_--;
            }
        }
        edges_[e->edgeptr()].erase(edges_[e->edgeptr()].begin() + index);
    }
    e = nullptr;

    boost::remove_edge(id, *this);

    selected_edges_.erase(id);

    changes_.removed_edges_.insert(id);
    for (VertexId vid : removedVertices) {
        Remove(vid);
    }
    edge_index_map_.erase(id);
}

template<typename VertexType>
void GraphT<VertexType>::Remove(std::shared_ptr<EdgeType> edge)
{
    std::set<VertexId> removedVertices;

    auto it = edges_.find(edge); // get the list of vertices to remove.
    for (unsigned int i = 0; i < it->second.size(); ++i) {
        GraphT<VertexType>::EdgePtr e = Get(it->second[i]);
        unsigned int startCountLimit = 2;
        unsigned int endCountLimit = 2;
        if (i > 0) {
        } else {
            startCountLimit = 1;
        }
        if (i < it->second.size() - 1) {
        } else {
            endCountLimit = 1;
        }

        if (e->GetStart()->GetEdgeCount() > startCountLimit) {
            changes_.updated_vertices_.insert(e->GetStartId());
        } else {
            removedVertices.insert(e->GetStartId());
        }

        if (e->GetEnd()->GetEdgeCount() > endCountLimit) {
            changes_.updated_vertices_.insert(e->GetEndId());
        } else {
            removedVertices.insert(e->GetEndId());
        }
        edge_index_map_.erase(e->id());
    }
    for (unsigned int i = 0; i < it->second.size(); ++i) {
        boost::remove_edge(it->second[i], *this);
        selected_edges_.erase(it->second[i]);
        changes_.removed_edges_.insert(it->second[i]);
    }
    edges_.erase(it);
    for (VertexId vid : removedVertices) {
        Remove(vid);
    }
    edge = nullptr;
}

template<typename VertexType>
unsigned int GraphT<VertexType>::GetEdgeCount() const
{
    return (uint) boost::num_edges(*this);
}

template<typename VertexType>
unsigned int GraphT<VertexType>::GetEdgeCount(const VertexId &id) const
{
    return (uint) boost::out_degree(id, *this);
}

template<typename VertexType>
unsigned int GraphT<VertexType>::GetVertexCount() const
{
    return (uint) boost::num_vertices(*this);
}

//template<typename VertexType>
//void GraphT<VertexType>::AddListener(GraphListenerT<VertexType, EdgeType> *listener)
//{
//    listeners_.insert(listener);
//}

//template<typename VertexType>
//void GraphT<VertexType>::RemoveListener(GraphListenerT<VertexType, EdgeType> *listener)
//{
//    listeners_.erase(listener);
//}

//template<typename VertexType>
//void GraphT<VertexType>::EdgeAdded(EdgeId id)
//{
//    for (GraphListenerBase *listener : Listenable::listeners_) {
//        dynamic_cast<GraphListener*>(listener)->EdgeAdded(id);
//    }
//}

//template<typename VertexType>
//void GraphT<VertexType>::EdgeRemoved(EdgeId id)
//{
//    for (GraphListenerBase *listener : Listenable::listeners_) {
//        dynamic_cast<GraphListener*>(listener)->EdgeRemoved(id);
//    }
//}

//template<typename VertexType>
//void GraphT<VertexType>::VertexAdded(VertexId id)
//{
//    for (GraphListenerBase *listener : Listenable::listeners_) {
//        dynamic_cast<GraphListener*>(listener)->VertexAdded(id);
//    }
//}

//template<typename VertexType>
//void GraphT<VertexType>::VertexRemoved(VertexId id)
//{
//    for (GraphListenerBase *listener : Listenable::listeners_) {
//        dynamic_cast<GraphListener*>(listener)->VertexRemoved(id);
//    }
//}

template<typename VertexType>
void GraphT<VertexType>::NotifyUpdate()
{
    VertexIteratorPtr vit = changes_.RemovedVertices();
    while (vit->HasNext()) {
        VertexId v = vit->NextId();
        changes_.added_vertices_.erase(v);
        changes_.updated_vertices_.erase(v);
    }

    EdgeIteratorPtr eit = changes_.RemovedEdges();
    while (eit->HasNext()) {
        EdgeId e = eit->NextId();
        changes_.added_edges_.erase(e);
        changes_.updated_edges_.erase(e);
    }

    vit = changes_.AddedVertices();
    while (vit->HasNext()) {
        std::shared_ptr<Vertex> v = vit->Next();
        v->updated();
    }

    eit = changes_.AddedEdges();
    while (eit->HasNext()) {
        std::shared_ptr<Edge> e = eit->Next();
        e->updated();
    }
    vit = changes_.UpdatedVertices();
    while (vit->HasNext()) {
        //ici : seg fault possible
        std::shared_ptr<Vertex> v = vit->Next();
        //ici : seg fault possible
        v->updated();
    }
    eit = changes_.UpdatedEdges();
    while (eit->HasNext()) {
        std::shared_ptr<Edge> e = eit->Next();
        e->updated();
    }

    //Parcourir la liste des graph déformer et les mettre a jour.
    //TODO
    //et mettre a jour le contenu de changes_

    for (GraphListenerBase* listener : Listenable::listeners_) {
        dynamic_cast<GraphListener*>(listener)->Notify(changes_);
    }
    changes_.Clear();
}

template<typename VertexType>
void GraphT<VertexType>::GetExtremities(std::set<VertexId> &extremities) const
{
    GraphT::VertexIteratorPtr vertices = GetVertices();
    while (vertices->HasNext()) {
        std::shared_ptr<Vertex> v = vertices->Next();
        if (v->GetEdgeCount() == 1) {
            extremities.insert(v->id());
        }
    }
}

template<typename VertexType>
void GraphT<VertexType>::Update1RingNeighborhood(VertexId id, bool addToChanges)
{
    std::shared_ptr<Vertex> v = Get(id);
    if (v == nullptr) {
        assert(v != nullptr && "cannot find vertex");
        return;
    }

    EdgeIteratorPtr edges = v->GetEdges();
    std::map<std::shared_ptr<EdgeType> , std::shared_ptr<typename GraphT::Edge> > primitives;

    // getting each primitive independently and getting 1 edge for each individual primitive.
    while (edges->HasNext()) {
        std::shared_ptr<Edge> e = edges->Next();
        primitives[e->edgeptr()] = e;
        if (addToChanges) {
            changes_.updated_edges_.insert(e->id());
        }
    }

    for (auto it = primitives.begin(); it != primitives.end(); ++it) {
        it->first->SetPoint(it->second->GetPrimitiveIndex(id), v->pos());
    }

    if (addToChanges) {
        changes_.updated_vertices_.insert(id);
    }
}

template<typename VertexType>
void GraphT<VertexType>::MovePoint(VertexId id, const typename VertexType::BasePoint &point, bool addToChanges)
{
    std::shared_ptr<Vertex> v = Get(id);
    if (v == nullptr) {
        assert(v != nullptr && "cannot find moving vertex");
        return;
    }

    v->nonconst_pos().array() = point.array();
    Update1RingNeighborhood(id, addToChanges);

////    vertices_.erase(old_pos);
////    vertices_[point] = v;
//    EdgeIteratorPtr edges = v->GetEdges();
//    std::map<std::shared_ptr<EdgeType> , std::shared_ptr<typename GraphT::Edge> > primitives;

//    // getting each primitive independently and getting 1 edge for each individual primitive.
//    while (edges->HasNext()) {
//        std::shared_ptr<Edge> e = edges->Next();
//        primitives[e->edgeptr()] = e;
//        if (addToChanges) {
//            changes_.updated_edges_.insert(e->id());
//        }
//    }

//    for (auto it = primitives.begin(); it != primitives.end(); ++it) {
////        it->first->SetPoint(it->second->GetPrimitiveIndex(id), point);
//        it->first->GetPoint(it->second->GetPrimitiveIndex(id)).array() = point.array();
//    }

//    // position is actually modified after the rest because v->pos can be #point.
//    v->set_pos(point);

//    if (addToChanges) {
//        changes_.updated_vertices_.insert(id);
//    }
}

template<typename VertexType>
std::string GraphT<VertexType>::to_string(bool details) const
{
    std::ostringstream oss;
    oss << "Graph : " << GetVertexCount() << " vertices && " << GetEdgeCount() << " edges." << std::endl;
    if (details) {
        auto verts = GetVertices();
        oss << "Vertices : " << std::endl;
        while (verts->HasNext()) {
            std::shared_ptr<Vertex> v = verts->Next();
            oss << "\t"<< v->to_string() << std::endl;
        }
        auto edges = GetEdges();
        oss << "Edges: " << std::endl;
        while (edges->HasNext()) {
            std::shared_ptr<Edge> e = edges->Next();
            oss << "\t"<< e->to_string() << std::endl;
        }
    }

    return oss.str();
}

template<typename VertexType>
TiXmlElement *GraphT<VertexType>::ToXml(std::map<const void*, unsigned int> &ids, const char *tagName, bool embed_vertices_in_edges) const
{
    TiXmlElement *element_base = new TiXmlElement( tagName == nullptr ? Name() : tagName);
    TiXmlElement *element_vertices = new TiXmlElement("Vertices");
    TiXmlElement *element_edges = new TiXmlElement("Edges");

    VertexIteratorPtr vertices = GetVertices();
    while (vertices->HasNext()) {
        VertexPtr v = vertices->Next();
        TiXmlElement *element_vertex = v->posptr()->ToXml();
        element_vertex->SetAttribute("id", (int) v->internal_id_);
        element_vertices->LinkEndChild(element_vertex);

        //TODO:@todo : this is required for NodePointSField
        ids[v->posptr().get()] = (uint) v->internal_id_;
    }

    std::map<std::shared_ptr<EdgeType>, unsigned int> edges;
    EdgeIteratorPtr edgesit = GetEdges();
    while (edgesit->HasNext()) {
        EdgePtr e = edgesit->Next();
        edges[e->edge_] = (uint) e->internal_id_;
    }

    for (auto it = edges.begin(); it != edges.end(); ++it ) {
        std::shared_ptr<EdgeType> e = it->first;
        unsigned int id = it->second;
        ids[e.get()] = id;

        TiXmlElement *element_edge = e->ToXml(embed_vertices_in_edges);
        auto mit = edges_.find(e); // iterator on graph::edges_.

        assert(mit!=edges_.end());

        element_edge->SetAttribute("id", id);
        std::shared_ptr<Edge> ei = nullptr;
        unsigned int i;
        char n[15];
        for (i = 0; i < mit->second.size(); ++i) {
            sprintf(n, "id_vh%d", i);
            ei = Get(mit->second[i]);
            element_edge->SetAttribute(n, (int) ei->GetStart()->internal_id_);
        }
        sprintf(n, "id_vh%d", i);
        element_edge->SetAttribute(n, (int) ei->GetEnd()->internal_id_);
        element_edges->LinkEndChild(element_edge);
    }

    element_base->LinkEndChild(element_vertices);
    element_base->LinkEndChild(element_edges);

    return element_base;
}

template<typename VertexType>
TiXmlElement *GraphT<VertexType>::ToXml(const char *tagName, bool embed_vertices_in_edges) const
{
    std::map<const void*, unsigned int> ids;
    return ToXml(ids, tagName, embed_vertices_in_edges);
}

template<typename VertexTypeT>
AABBoxT<typename GraphT<VertexTypeT>::VertexType::BasePoint> GraphT<VertexTypeT>::axis_bounding_box() const
{
    AABBoxT<typename GraphT<VertexTypeT>::VertexType::BasePoint> b;
    auto verts = GetVertices();
    while (verts->HasNext()) {
        std::shared_ptr<Vertex> v = verts->Next();
        b.Enlarge(v->pos());
    }

    return b;
}

template<typename VertexType>
void GraphT<VertexType>::set_selected(const VertexId &id, bool selected)
{
    if (selected) {
        selected_vertices_.insert(id);
    } else {
        selected_vertices_.erase(id);
    }
}

template<typename VertexType>
void GraphT<VertexType>::set_selected(const EdgeId &id, bool selected)
{
    if (selected) {
        selected_edges_.insert(id);
    } else {
        selected_edges_.erase(id);
    }
//    EdgePtr edge = Get(id);
//    for (unsigned int i = 0; i < edge->GetSize(); ++i) {
//        std::shared_ptr<Vertex> v = edge->GetVertex(i);
//        set_selected(v->id(), selected);
//    }
}

template<typename VertexType>
typename GraphT<VertexType>::VertexId GraphT<VertexType>::SplitEdge(const EdgeId &id, unsigned int index, const VertexType& point)
{
    // string were used to find pos in the map (which can be of different types, but this type is not usable as such in maps : i.e. compare a weightedPoint & a Point won't ield the good results, unless we use a pointer).
    // how to make this cleaner ?
    std::map<std::string, std::shared_ptr<Vertex> > vertices;
    std::set<std::shared_ptr<EdgeType> > new_edges;
    std::shared_ptr<Edge> prevEdge = Get(id);

    // storing vertices so we can actually use those to find the new primitives later.
    EdgeIteratorPtr itEdges = GetEdges(prevEdge->edgeptr());
    while (itEdges->HasNext()) {
        auto p = itEdges->Next();
        for (unsigned int i = 0; i < p->GetSize(); ++i) {
            vertices[p->GetVertex(i)->pos().ToString()] = p->GetVertex(i);
        }
    }

    std::shared_ptr<EdgeType> replacementEdge = prevEdge->edge().clone();
    /*unsigned int i = */replacementEdge->Split(index, point, new_edges);

    auto edgevector = Add(replacementEdge, vertices);

    // getting the last point from replacementEdge.
//    VertexId res = VertexId();
//    while (edgevector->HasNext()) {
//        EdgePtr ept = edgevector->Next();
//        res = ept->GetEnd()->id();
//    }
    auto end_edge = GetEdge(replacementEdge, index);
    if (end_edge == nullptr) {
        return VertexId();
    }
    VertexId res = end_edge->GetEndId();

    for (std::shared_ptr<EdgeType> e : new_edges) {
        Add(e, vertices);
    }

    Remove(prevEdge->edge_);

//    auto it = edges_.find(prevEdge->edge_);
//    for (int i = (int) it->second.size(); i >= 0; --i) {
//        Remove(it->second[i]);
//    }
//    for (auto eid : it->second) {
//        Remove(eid);
//    }

    return res;
}

template<typename VertexType>
void GraphT<VertexType>::ApplyDeformation(const Matrix4 &mat, bool addToChanges)
{
    Point4 p = Point4::Ones();

    auto vertices = GetVertices();
    while (vertices->HasNext()) {
        auto v = vertices->Next();
        VertexType old_pos = v->pos();
        unsigned int i = 0;
        for (i = 0; i < VertexType::RowsAtCompileTime; ++i) {
            p[i] = v->pos()[i];
        }
        Point4 tmp = (mat * p);//.block<VertexType::BasePoint::RowsAtCompileTime, 1>(0, 0);
        for (unsigned int i = 0; i < VertexType::BasePoint::RowsAtCompileTime; ++i) {
            old_pos[i] = tmp[i];
        }
        MovePoint(v->id(), old_pos, addToChanges);
    }
}

template<typename VertexType>
typename GraphT<VertexType>::VertexId GraphT<VertexType>::MergeVertices(const std::set<VertexId> &vertices, VertexId targetVertex)
{
    assert(vertices.size() > 1);

    std::set<VertexId > neighbor_vertices;
    std::shared_ptr<Vertex> finalVertex = nullptr;

    if (targetVertex != boost::graph_traits<GraphT>::null_vertex()) {
        finalVertex = Get(targetVertex);
    } else {
        finalVertex = Get(*(vertices.begin()));
    }
    std::shared_ptr<VertexType> newPoint = std::dynamic_pointer_cast<VertexType> (finalVertex->pos().clone());
    assert(newPoint != nullptr);

    std::set<VertexType> points;
    std::shared_ptr<EdgeType> randomEdge = nullptr; // this will contain a random edge form the neighbors.
    // We try to get one that is of size 2, to avoid issues with edges like triangles, etc...

    VertexSetIterator it(this, vertices.begin(), vertices.end());
    // browsing every node
    while (it.HasNext()) {
        // get the barycenter of the selected nodes
        finalVertex = it.Next();
        points.insert(finalVertex->pos());

        auto neighbors = finalVertex->GetAdjacentVerticesId();
        // and gather the adjacent vertices that are not going to be deleted.
        while (neighbors.first != neighbors.second) {
            VertexId v2 = *(neighbors.first);
            if (vertices.find(v2) == vertices.end()) {
                neighbor_vertices.insert(v2);
            }

            neighbors.first++;
        }

        if (randomEdge == nullptr) {
            EdgeIteratorPtr edges = finalVertex->GetEdges();

            while (edges->HasNext()) {
                std::shared_ptr<Edge> edge = edges->Next();
                if (edge->edgeptr()->GetSize() == 2) {
                    randomEdge = edge->edgeptr();
                    break;
                }
            }
        }
    }

    if (targetVertex == boost::graph_traits<GraphT>::null_vertex()) {
        newPoint->MoveToBarycenter(points);
    }
    std::shared_ptr<Vertex> newVertex = this->Add(newPoint);

    VertexSetIterator nit = VertexSetIterator(this, neighbor_vertices.begin(), neighbor_vertices.end());
    // Create a new segment between the new barycenter and every neighbor.
    while (nit.HasNext()) {
        std::shared_ptr<Vertex> v = nit.Next();
        std::vector<std::shared_ptr<Vertex> > vlist = {newVertex, v};
        std::shared_ptr<EdgeType> wseg = nullptr;
        if (randomEdge != nullptr) {
            wseg = randomEdge->clone();
        } else {
            std::shared_ptr<SegmentT<VertexType> > w = this->CreateSegment(newVertex, v);
            std::shared_ptr<Functor> func = FunctorFactory::GetInstance().CreateDefaultFunctor();
            wseg = std::dynamic_pointer_cast<EdgeType>(PrimitiveFactory::GetInstance().CreateNewPrimitive(w, *func));
        }

        wseg->RemovePoints();
        wseg->AddPoint(newVertex->pos(), 0);
        wseg->AddPoint(v->pos(), 1);

        this->Add(wseg, vlist);
    }

    // and remove every merged vertex. This shall remove unnecessary segments too.
    it = VertexSetIterator(this, vertices.begin(), vertices.end());
    while (it.HasNext()) {
        VertexId v = it.NextId();
        this->Remove(v);
    }

    return newVertex->id();
}

template<typename VertexType>
void GraphT<VertexType>::MergeCloseVertices(Scalar epsilon, bool merge_unlinked_vertices)
{
    bool finished = false;
    Scalar epsilonSq = epsilon * epsilon;

    std::set<VertexId> merged;

    while (!finished) {
        finished = true;
        auto vertices = GetVertices();
        while (vertices->HasNext()) {
            std::shared_ptr<Vertex> v = vertices->Next();
            if (merge_unlinked_vertices) {
                VertexIteratorPtr neighbors = GetVertices();
                while (neighbors->HasNext()) {
                    std::shared_ptr<Vertex> n = neighbors->Next();
                    if (n != v && (((n->pos()) - v->pos()).squaredNorm() < epsilonSq)) {
                        merged.insert(n->id());
                    }
                }
            } else {
                VertexIteratorPtr neighbors = v->GetAdjacentVertices();
                while (neighbors->HasNext()) {
                    std::shared_ptr<Vertex> n = neighbors->Next();
                    if (((n->pos()) - v->pos()).squaredNorm() < epsilonSq) {
                        merged.insert(n->id());
                    }
                }
            }
            if (merged.size() > 0) {
                merged.insert(v->id());
                VertexId res = MergeVertices(merged);
                auto r = Get(res);
                merged.clear();
                finished = false;
                break;
            }
        }
    }
}

template<>
void CORE_API WeightedGraph2D::MergeCloseVertices(Scalar epsilon, bool merge_unlinked_vertices);

template<typename VertexType>
std::shared_ptr<SegmentT<VertexType> > GraphT<VertexType>::CreateSegment(typename GraphT<VertexType>::VertexPtr start, typename GraphT<VertexType>::VertexPtr end)
{
    return std::make_shared<SegmentT<VertexType> >(start->pos(), end->pos());
}

template<>
std::shared_ptr<SegmentT<WeightedPoint> > CORE_API WeightedGraph::CreateSegment(WeightedVertexPtr start, WeightedVertexPtr end);

} // namespace core

} // namespace expressive

#endif // GRAPH_HPP
