/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GraphCommand_H
#define GraphCommand_H

#include <core/graph/Graph.h>
#include <core/commands/CommandManager.h>
#include <core/geometry/PrimitiveFactory.h>
#include <core/functor/FunctorFactory.h>

namespace expressive
{

namespace core
{

/**
 * This file contains a list of undoable graph-related commands.
 */
// TODO :
// Commands that check dependencies through the CommandManager : if a command depends on the creation of a previous one, it should be updated
// depending on the results on that previous one. For example, when undoing/redoing the construction of a vertex, it's id and pointers will change, and so the movepoint that follows must change too.
/**
 * A base GraphCommand. Does nothing, but stores the graph that will be used for every next command defined here.
 */
template<typename VertexType>
class GraphCommand : public Command
{
public:
    typedef GraphT<VertexType> GraphType;

    GraphCommand(GraphType *graph);

    virtual ~GraphCommand();

protected:
    inline std::shared_ptr<core::GeometryPrimitiveT<VertexType>> CreateSegment(const VertexType &start, const VertexType &end);

    GraphType *graph_;
};

/**
 * Moves a point, or a list of points.
 * Each Vertex is stored with a new and an old position.
 * In order to avoid hundreds of commands for a simple point when moving it, and to avoid merges,
 * there is the possibility to update the position of a point with the #SetFinalPos method.
 */
template<typename VertexType>
class MovePointCommand : public GraphCommand<VertexType>
{
public:
    typedef GraphT<VertexType> GraphType;

    MovePointCommand(GraphType *graph);

    MovePointCommand(GraphType * graph, const typename GraphType::VertexId &id, const typename VertexType::BasePoint &newPos);

    MovePointCommand(GraphType * graph, const typename GraphType::VertexId &id, const typename VertexType::BasePoint &newPos, const typename VertexType::BasePoint &oldPos);

    virtual ~MovePointCommand();

    virtual bool execute();

    virtual bool undo();

    void MovePoint(const std::shared_ptr<typename GraphType::Vertex> v, const typename VertexType::BasePoint &newPos);

    void AddPoint(const typename GraphType::VertexId &id);

    void AddPoint(const std::shared_ptr<typename GraphType::Vertex> point);

    void SetFinalPos(const typename GraphType::VertexId &id, typename VertexType::BasePoint &newPos);

    void SetFinalPos(const std::shared_ptr<typename GraphType::Vertex> point, typename VertexType::BasePoint &newPos);

protected:
    std::map<typename GraphType::VertexId, typename VertexType::BasePoint > old_positions_;

    std::map<typename GraphType::VertexId, typename VertexType::BasePoint > new_positions_;
};

/**
 * Creates a new edge.
 * Can also just add a point, and is able to handle weight changes too.
 */
template<typename VertexType>
class AddEdgeCommand : public GraphCommand<VertexType>
{
public:
    typedef GraphT<VertexType> GraphType;

    AddEdgeCommand(GraphType *graph, const typename GraphType::VertexId &start, const typename GraphType::VertexId &end, Scalar weight = 0);
//    AddEdgeCommand(Graph *graph, std::shared_ptr<Vertex> start, std::shared_ptr<Vertex> end = nullptr);

    virtual ~AddEdgeCommand();

    virtual bool execute();

    virtual bool undo();

    typename GraphType::VertexId start_;

    typename GraphType::VertexId end_;

    bool single_point_;

    typename GraphType::EdgeId edge_;

    Scalar weight_;
};

/**
 * Merges multiple vertices into one.
 * the old vertices are saved so they can be re-added later.
 */
template<typename VertexType>
class MergeVerticesCommand : public GraphCommand<VertexType>
{
public:
    typedef GraphT<VertexType> GraphType;

    MergeVerticesCommand(GraphType *graph, const std::set<typename GraphType::VertexId> &vertices);

    virtual ~MergeVerticesCommand();

    virtual bool execute();

    virtual bool undo();

    std::set<typename GraphType::VertexId> vertices_;

    typename GraphType::VertexId point_id_;
};

/**
 * Changes the weights of a list of vertices. Multiplies them by a given factor.
 */
template<typename VertexType>
class ChangeVerticesWeightCommand : public GraphCommand<VertexType>
{
public:
    typedef GraphT<VertexType> GraphType;

    ChangeVerticesWeightCommand(GraphType *graph, std::set<typename GraphType::VertexId> &vertices, Scalar factor);

    virtual ~ChangeVerticesWeightCommand();

    virtual bool execute();

    virtual bool undo();

    void set_factor(Scalar f);

    std::set<typename GraphType::VertexId> vertices_;

    Scalar factor_;

};

/**
 * Splits an edge at a given coordinate.
 */
template<typename VertexType>
class SubdivideEdgeCommand : public GraphCommand<VertexType>
{
public:
    typedef GraphT<VertexType> GraphType;

    SubdivideEdgeCommand(GraphType *graph, typename GraphType::EdgeId edge, unsigned int index, const VertexType& pos);

    virtual ~SubdivideEdgeCommand();

    virtual bool execute();

    virtual bool undo();

    typename GraphType::EdgeId edge_;

    unsigned int index_;

    VertexType pos_;

    typename GraphType::VertexId vertex_id_;
};

/**
 * Deletes a whole primitive, or a list of primitives.
 */
template<typename VertexType>
class DeletePrimitiveCommand : public GraphCommand<VertexType>
{
public:
    typedef GraphT<VertexType> GraphType;

    DeletePrimitiveCommand(GraphType *graph, typename GraphType::EdgeId edge);
    DeletePrimitiveCommand(GraphType *graph, typename GraphType::VertexId vertex);
    DeletePrimitiveCommand(GraphType *graph, const std::set<typename GraphType::EdgeId> &edges);
    DeletePrimitiveCommand(GraphType *graph, const std::set<typename GraphType::VertexId> &vertices);
    DeletePrimitiveCommand(GraphType *graph, const std::set<typename GraphType::EdgeId> &edges, const std::set<typename GraphType::VertexId> &vertices);

    virtual ~DeletePrimitiveCommand();

    virtual bool execute();

    virtual bool undo();

protected:
    std::set<typename GraphType::EdgeId> edges_;

    std::set<typename GraphType::VertexId> vertices_;

};

} // namespace core

} // namespace expressive

#include <core/graph/GraphCommands.hpp>

#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_GRAPHCOMMANDS
extern template class expressive::core::AddEdgeCommand<expressive::core::WeightedPoint>;
extern template class expressive::core::AddEdgeCommand<expressive::core::WeightedPoint2D>;
extern template class expressive::core::AddEdgeCommand<expressive::core::PointPrimitive>;
extern template class expressive::core::AddEdgeCommand<expressive::core::PointPrimitive2D>;

extern template class expressive::core::ChangeVerticesWeightCommand<expressive::core::WeightedPoint>;
extern template class expressive::core::ChangeVerticesWeightCommand<expressive::core::WeightedPoint2D>;
extern template class expressive::core::ChangeVerticesWeightCommand<expressive::core::PointPrimitive>;
extern template class expressive::core::ChangeVerticesWeightCommand<expressive::core::PointPrimitive2D>;

extern template class expressive::core::DeletePrimitiveCommand<expressive::core::WeightedPoint>;
extern template class expressive::core::DeletePrimitiveCommand<expressive::core::WeightedPoint2D>;
extern template class expressive::core::DeletePrimitiveCommand<expressive::core::PointPrimitive>;
extern template class expressive::core::DeletePrimitiveCommand<expressive::core::PointPrimitive2D>;

extern template class expressive::core::GraphCommand<expressive::core::WeightedPoint>;
extern template class expressive::core::GraphCommand<expressive::core::WeightedPoint2D>;
extern template class expressive::core::GraphCommand<expressive::core::PointPrimitive>;
extern template class expressive::core::GraphCommand<expressive::core::PointPrimitive2D>;

extern template class expressive::core::MergeVerticesCommand<expressive::core::WeightedPoint>;
extern template class expressive::core::MergeVerticesCommand<expressive::core::WeightedPoint2D>;
extern template class expressive::core::MergeVerticesCommand<expressive::core::PointPrimitive>;
extern template class expressive::core::MergeVerticesCommand<expressive::core::PointPrimitive2D>;

extern template class expressive::core::MovePointCommand<expressive::core::WeightedPoint>;
extern template class expressive::core::MovePointCommand<expressive::core::WeightedPoint2D>;
extern template class expressive::core::MovePointCommand<expressive::core::PointPrimitive>;
extern template class expressive::core::MovePointCommand<expressive::core::PointPrimitive2D>;

extern template class expressive::core::SubdivideEdgeCommand<expressive::core::WeightedPoint>;
extern template class expressive::core::SubdivideEdgeCommand<expressive::core::WeightedPoint2D>;
extern template class expressive::core::SubdivideEdgeCommand<expressive::core::PointPrimitive>;
extern template class expressive::core::SubdivideEdgeCommand<expressive::core::PointPrimitive2D>;

#endif
#endif

#endif // GraphCommand_H
