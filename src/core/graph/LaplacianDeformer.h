/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef LAPLACIANDEFORMER_H
#define LAPLACIANDEFORMER_H

#include <core/graph/Graph.h>

#include <memory>
#include <boost/graph/depth_first_search.hpp>

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 175 )
#endif

#include <Eigen/Dense>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

using namespace std;

namespace expressive
{

namespace core
{

// NOT FINISHED YET.
class CORE_API LaplacianDeformer
{
public:
    const static std::string LaplacianConstrainedAttributeName;
    const static std::string LaplacianWeightAttributeName;

    typedef Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> MatXXX;
    typedef Eigen::Matrix<Scalar, Eigen::Dynamic, 1> MatXX1;

    LaplacianDeformer(std::shared_ptr<Graph> graph);
    virtual ~LaplacianDeformer();

    virtual void NewGraph(std::shared_ptr<Graph> graph);

    virtual void DeformGraph(unsigned int nIterations = 5);

//    static void ComputeWeights(std::shared_ptr<Graph> graph, MatXX1 &Bx, MatXX1 &By, MatXX1 &Bz);

//    static void ComputeB(std::shared_ptr<Graph> graph, MatXX1 &Bx, MatXX1 &By, MatXX1 &Bz);

//    static void ComputeL(std::shared_ptr<Graph> graph, MatXXX &l);

    static void ComputeA(std::shared_ptr<Vertex> v, MatXXX &a);

    static void ComputeB(std::shared_ptr<Vertex> v, MatXX1 &b);

    static void ComputeNewPositions(std::shared_ptr<Graph> graph, MatXXX &l, MatXX1 &Bx, MatXX1 &By, MatXX1 &Bz );

protected:
    std::shared_ptr<Graph> graph_;

    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> L_;
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> Bx_;
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> By_;
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> Bz_;
};

} // namespace core

} // namespace expressive

#endif // LAPLACIANDEFORMER_H
