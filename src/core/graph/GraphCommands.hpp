#ifndef GRAPH_COMMANDS_HPP_
#define GRAPH_COMMANDS_HPP_

#include "core/graph/GraphCommands.h"

namespace expressive
{

namespace core
{

template<typename VertexType>
GraphCommand<VertexType>::GraphCommand(GraphType *graph) :
    graph_(graph)
{
}

template<typename VertexType>
GraphCommand<VertexType>::~GraphCommand()
{
}

template<typename VertexType>
std::shared_ptr<core::GeometryPrimitiveT<VertexType>> GraphCommand<VertexType>::CreateSegment(const VertexType &start, const VertexType &end)
{
    return std::make_shared<SegmentT<VertexType>>(start, end);
}

template<>
std::shared_ptr<core::GeometryPrimitiveT<core::WeightedPoint>> CORE_API GraphCommand<core::WeightedPoint>::CreateSegment(const WeightedPoint &start, const WeightedPoint &end);

template<typename VertexType>
MovePointCommand<VertexType>::MovePointCommand(GraphType *graph) :
    GraphCommand<VertexType>(graph)
{
}

template<typename VertexType>
MovePointCommand<VertexType>::MovePointCommand(GraphType *graph, const typename GraphType::VertexId &id, const typename VertexType::BasePoint &newPos) :
    GraphCommand<VertexType>(graph)
{
    old_positions_[id].array() = graph->Get(id)->pos().array();
    new_positions_[id].array() = newPos.array();
}

template<typename VertexType>
MovePointCommand<VertexType>::MovePointCommand(GraphType *graph, const typename GraphType::VertexId &id, const typename VertexType::BasePoint &newPos, const typename VertexType::BasePoint &oldPos) :
    GraphCommand<VertexType>(graph)
{
    old_positions_[id].array() = oldPos.array();
    new_positions_[id].array() = newPos.array();
}

template<typename VertexType>
MovePointCommand<VertexType>::~MovePointCommand()
{
    old_positions_.clear();
    new_positions_.clear();
}

template<typename VertexType>
void MovePointCommand<VertexType>::MovePoint(const std::shared_ptr<typename GraphType::Vertex> v, const typename VertexType::BasePoint &newPos)
{
    auto iter = old_positions_.find(v->id());
    if(iter == old_positions_.end())
        old_positions_[v->id()].array() = v->pos().array();

    new_positions_[v->id()].array() = newPos.array();
}

template<typename VertexType>
void MovePointCommand<VertexType>::AddPoint(const typename GraphType::VertexId &id)
{
    //TODO:@todo : check if still correct
    return AddPoint(this->graph_->Get(id));
}

template<typename VertexType>
void MovePointCommand<VertexType>::AddPoint(const std::shared_ptr<typename GraphType::Vertex> point)
{
    //TODO:@todo : check if still correct
    if (point != nullptr) {
        old_positions_[point->id()].array() = point->pos().array();
    }
}

template<typename VertexType>
void MovePointCommand<VertexType>::SetFinalPos(const typename GraphType::VertexId &id, typename VertexType::BasePoint &newPos)
{
    return SetFinalPos(this->graph_->Get(id), newPos); //TODO:@todo : check if still correct
}

template<typename VertexType>
void MovePointCommand<VertexType>::SetFinalPos(const std::shared_ptr<typename GraphType::Vertex> point, typename VertexType::BasePoint &newPos)
{
    if (point != nullptr) {
        new_positions_[point->id()].array() = newPos.array(); //TODO:@todo : check if still correct
    }
}

template<typename VertexType>
bool MovePointCommand<VertexType>::execute()
{
    for (auto it = new_positions_.begin(); it != new_positions_.end(); ++it) {
        this->graph_->MovePoint(it->first, it->second);
    }
    this->graph_->NotifyUpdate();
    return true;
}

template<typename VertexType>
bool MovePointCommand<VertexType>::undo()
{
    for (auto it = old_positions_.begin(); it != old_positions_.end(); ++it) {
        this->graph_->MovePoint(it->first, it->second);
    }
    this->graph_->NotifyUpdate();
    return true;
}

template<typename VertexType>
AddEdgeCommand<VertexType>::AddEdgeCommand(GraphType *graph, const typename GraphType::VertexId &start, const typename GraphType::VertexId &end, Scalar weight) :
    GraphCommand<VertexType>(graph),
    start_(start),
    end_(end),
    single_point_(start == end),
    weight_(weight)
{
}

template<typename VertexType>
AddEdgeCommand<VertexType>::~AddEdgeCommand()
{
}

template<typename VertexType>
bool AddEdgeCommand<VertexType>::execute()
{
    std::shared_ptr<typename GraphType::Vertex> start = this->graph_->Get(start_);
    std::shared_ptr<typename GraphType::Vertex> end = nullptr;
    if (single_point_) {
        end = this->graph_->Add(start->pos());
        end_ = end->id();
    } else {
        end = this->graph_->Get(end_);
    }

    std::shared_ptr<GeometryPrimitiveT<VertexType>> newG  = nullptr;

    bool found = false;
    if (start->GetEdgeCount() > 0) {
        typename GraphType::EdgeIteratorPtr edges = start->GetEdges();
        while (edges->HasNext()) {

            std::shared_ptr<typename GraphType::Edge> e = edges->Next();
            if (e->edgeptr()->GetSize() == 2) {
                newG = e->edgeptr()->clone();
                found = true;
                break;
            }
        }
    }
    if (!found) {
        std::shared_ptr<GeometryPrimitiveT<VertexType>> wseg = this->CreateSegment(start->pos(), end->pos());
        std::shared_ptr<Functor> func = FunctorFactory::GetInstance().CreateDefaultFunctor();
        newG = std::dynamic_pointer_cast<GeometryPrimitiveT<VertexType>>(PrimitiveFactory::GetInstance().CreateNewPrimitive(wseg, *func));
    }

    newG->RemovePoints();
    newG->AddPoint(start->pos(), 0);
    newG->AddPoint(end->pos(), 1);

    std::vector<std::shared_ptr<typename GraphType::Vertex> > vertices = {start, end};
    typename GraphType::EdgeIteratorPtr newEdges = this->graph_->Add(newG, vertices);

    if (newEdges->HasNext()) {
        edge_ = newEdges->Next()->id();
    }
    this->graph_->NotifyUpdate();
    return true;
}

template<>
bool CORE_API AddEdgeCommand<core::WeightedPoint>::execute();

template<typename VertexType>
bool AddEdgeCommand<VertexType>::undo()
{
    this->graph_->Remove(edge_);
    if (single_point_) {
        this->graph_->Remove(end_);
        end_ = start_;
    }
    this->graph_->NotifyUpdate();
    return true;
}

template<typename VertexType>
MergeVerticesCommand<VertexType>::MergeVerticesCommand(GraphType *graph, const std::set<typename GraphType::VertexId> &vertices) :
    GraphCommand<VertexType>(graph), vertices_(vertices)
{
    assert(vertices.size() > 1);
}

template<typename VertexType>
MergeVerticesCommand<VertexType>::~MergeVerticesCommand()
{
}

template<typename VertexType>
bool MergeVerticesCommand<VertexType>::execute()
{
//    set<VertexId > neighbor_vertices;
//    typename VertexType::BasePoint pos(typename VertexType::BasePoint::Zero());
//    Scalar totalWeights = 0.0;
//    std::shared_ptr<Vertex> finalVertex = nullptr;

//    Graph::VertexSetIterator it(this->graph_, vertices_.begin(), vertices_.end());
//    // browsing every node
//    while (it.HasNext()) {
//        // get the barycenter of the selected nodes
//        finalVertex = it.Next();
//        pos += finalVertex->pos() * finalVertex->pos().weight();
//        totalWeights += finalVertex->pos().weight();
//        auto neighbors = finalVertex->GetAdjacentVerticesId();
//        // and gather the adjacent vertices that are not going to be deleted.
//        while (neighbors.first != neighbors.second) {
//            VertexId v2 = *(neighbors.first);
//            if (vertices_.find(v2) == vertices_.end()) {
//                neighbor_vertices.insert(v2);
//            }

//            neighbors.first++;
//        }
//    }

//    pos /= totalWeights;

//    std::shared_ptr<WeightedPoint> newPoint = dynamic_pointer_cast<WeightedPoint>(finalVertex->pos().clone());
//    newPoint->set_pos(pos);
//    newPoint->set_weight(totalWeights / (Scalar)(vertices_.size()));
//    std::shared_ptr<Vertex> newVertex = this->graph_->Add(newPoint);

//    Graph::VertexSetIterator nit = Graph::VertexSetIterator(this->graph_, neighbor_vertices.begin(), neighbor_vertices.end());
//    // Create a new segment between the new barycenter and every neighbor.
//    while (nit.HasNext()) {
//        std::shared_ptr<Vertex> v = nit.Next();
//        vector<std::shared_ptr<Vertex> > vlist = {newVertex, v};
//        std::shared_ptr<WeightedSegment> wseg = make_shared<WeightedSegment>(newVertex->pos(), v->pos());

//        std::shared_ptr<Functor> func = FunctorFactory::GetInstance().CreateDefaultFunctor();
//        std::shared_ptr<WeightedGeometryPrimitive> newG = dynamic_pointer_cast<WeightedGeometryPrimitive>(PrimitiveFactory::GetInstance().CreateNewPrimitive(wseg, *func));
//        this->graph_->Add(newG, vlist);
//    }

//    // and remove every merged vertex. This shall remove unnecessary segments too.
//    it = Graph::VertexSetIterator(this->graph_, vertices_.begin(), vertices_.end());
//    while (it.HasNext()) {
//        std::shared_ptr<Vertex> v = it.Next();
//        this->graph_->Remove(v->id());
//    }

//    // storing new vertex id for future retrieval (changing the seelection, etc...)
//    point_id_ = newVertex->id();

    point_id_ = this->graph_->MergeVertices(vertices_);

    this->graph_->NotifyUpdate();
    return true;
}

template<typename VertexType>
bool MergeVerticesCommand<VertexType>::undo()
{
    ork::Logger::ERROR_LOGGER->log("GRAPH", "Undo method for \"MergeVerticesCommand\" not implemented yet.");
    assert(false && "TODO");
    return true;
}

template<typename VertexType>
ChangeVerticesWeightCommand<VertexType>::ChangeVerticesWeightCommand(GraphType *graph, std::set<typename GraphType::VertexId> &vertices, Scalar factor) :
    GraphCommand<VertexType>(graph), vertices_(vertices), factor_(factor)
{
}

template<typename VertexType>
ChangeVerticesWeightCommand<VertexType>::~ChangeVerticesWeightCommand()
{
}

template<typename VertexType>
bool ChangeVerticesWeightCommand<VertexType>::execute()
{
    typename GraphType::VertexSetIterator it(this->graph_, vertices_.begin(), vertices_.end());
    while (it.HasNext()) {
        std::shared_ptr<typename GraphType::Vertex> v = it.Next();
        v->nonconst_pos().set_weight(v->pos().weight() * factor_);
        this->graph_->Update1RingNeighborhood(v->id());
//        WeightedPoint &pos = v->nonconst_pos();
//        pos.set_weight(pos.weight() * factor_);
//        this->graph_->MovePoint(v->id(), pos);
    }
    this->graph_->NotifyUpdate();
    return true;
}

template<typename VertexType>
bool ChangeVerticesWeightCommand<VertexType>::undo()
{
    typename GraphType::VertexSetIterator it(this->graph_, vertices_.begin(), vertices_.end());
    while (it.HasNext()) {
        std::shared_ptr<typename GraphType::Vertex> v = it.Next();
        v->nonconst_pos().set_weight(v->pos().weight() / factor_);
        this->graph_->Update1RingNeighborhood(v->id());
    }
    this->graph_->NotifyUpdate();
    return true;
}

template<typename VertexType>
void ChangeVerticesWeightCommand<VertexType>::set_factor(Scalar f) { factor_ = f; }

template<typename VertexType>
SubdivideEdgeCommand<VertexType>::SubdivideEdgeCommand(GraphType *graph, typename GraphType::EdgeId edge, unsigned int index, const VertexType& pos) :
    GraphCommand<VertexType>(graph),
    edge_(edge),
    index_(index),
    pos_(pos),
    vertex_id_()
{
}

template<typename VertexType>
SubdivideEdgeCommand<VertexType>::~SubdivideEdgeCommand()
{
}

template<typename VertexType>
bool SubdivideEdgeCommand<VertexType>::execute()
{
    vertex_id_ = this->graph_->SplitEdge(edge_, index_, pos_);

    this->graph_->NotifyUpdate();
    return true;
}

template<typename VertexType>
bool SubdivideEdgeCommand<VertexType>::undo()
{
    ork::Logger::ERROR_LOGGER->log("GRAPH", "Undo method for \"SubdivideEdgeCommand\" not implemented yet.");
    return true;
}

template<typename VertexType>
DeletePrimitiveCommand<VertexType>::DeletePrimitiveCommand(GraphType *graph, typename GraphType::EdgeId edge) :
    GraphCommand<VertexType>(graph)
{
    edges_.insert(edge);
}

template<typename VertexType>
DeletePrimitiveCommand<VertexType>::DeletePrimitiveCommand(GraphType *graph, typename GraphType::VertexId vertex) :
    GraphCommand<VertexType>(graph)
{
    vertices_.insert(vertex);
}

template<typename VertexType>
DeletePrimitiveCommand<VertexType>::DeletePrimitiveCommand(GraphType *graph, const std::set<typename GraphType::EdgeId> &edges) :
    GraphCommand<VertexType>(graph), edges_(edges)
{
}

template<typename VertexType>
DeletePrimitiveCommand<VertexType>::DeletePrimitiveCommand(GraphType *graph, const std::set<typename GraphType::VertexId> &vertices) :
    GraphCommand<VertexType>(graph), vertices_(vertices)
{
}

template<typename VertexType>
DeletePrimitiveCommand<VertexType>::DeletePrimitiveCommand(GraphType *graph, const std::set<typename GraphType::EdgeId> &edges, const std::set<typename GraphType::VertexId> &vertices) :
    GraphCommand<VertexType>(graph), edges_(edges), vertices_(vertices)
{
    for (auto id : edges_) {
        typename GraphType::EdgePtr e = this->graph_->Get(id);
        vertices_.erase(e->GetStartId());
        vertices_.erase(e->GetEndId());
    }
}

template<typename VertexType>
DeletePrimitiveCommand<VertexType>::~DeletePrimitiveCommand()
{
}

template<typename VertexType>
bool DeletePrimitiveCommand<VertexType>::execute()
{
    for (auto id : edges_) {
        this->graph_->Remove(id);
    }
    for (auto id : vertices_) {
        this->graph_->Remove(id);
    }

    //Seg fault possible ici
    this->graph_->NotifyUpdate();
    return true;
}

template<typename VertexType>
bool DeletePrimitiveCommand<VertexType>::undo()
{
    ork::Logger::ERROR_LOGGER->log("GRAPH", "Undo method for \"DeletePrimitiveCommand\" not implemented yet.");

    this->graph_->NotifyUpdate();
    return true;
}

}

}

#endif // GRAPH_COMMANDS_HPP_
