#include <core/graph/Voronoi.h>
#include <ork/core/Logger.h>

namespace expressive
{

namespace core
{

VoronoiDiagram::VoronoiDiagram()
{
}

VoronoiDiagram::~VoronoiDiagram()
{
    vd_.clear();
    points_.clear();
    segments_.clear();
}

void VoronoiDiagram::addPoint(const core::PointPrimitive2D & p) {
    points_.push_back(p);
}

void VoronoiDiagram::addSegment(const core::PointPrimitive2D& p1, const core::PointPrimitive2D& p2) {
    segments_.push_back(Segment2D(p1, p2));
}

void VoronoiDiagram::construct(std::vector<Segment2D> &res)
{
    boost::polygon::construct_voronoi(points_.begin(), points_.end(), segments_.begin(), segments_.end(), &vd_);
//    int nbInclus = 0, nbTot = 0;

    for (boost_vd::const_edge_iterator it = vd_.edges().begin(); it != vd_.edges().end(); ++it) {
        if (it->is_primary() && it->is_finite()) {
            if (it->cell()->source_index() <
                               it->twin()->cell()->source_index()) { // make sure we don't add edge twice.
//                nbTot++;
                const boost_vd::vertex_type *v0 = it->vertex0();
                const boost_vd::vertex_type *v1 = it->vertex1();
    //            if (v0 != nullptr && v1 != nullptr) {
                res.push_back(Segment2D(Point2(v0->x(), v0->y()), Point2(v1->x(), v1->y())));
    //            }
            }
        }
    }
//    return result;

//    for(boost_vd::const_vertex_iterator it = vd_.vertices().begin(); it != vd_.vertices().end(); ++it) {
//        const boost_vd::vertex_type &vertex =*it;
//        const boost_vd::edge_type *edge =vertex.incident_edge();
//        do {
//            if (edge->is_primary()) {
//                ////
//                nbTot ++;
//                if (edge->vertex0() != NULL && edge->vertex1() != NULL) {
//                    nbInclus++;
//                    const boost_vd::vertex_type *v0 = edge->vertex0();
//                    const boost_vd::vertex_type *v1 = edge->vertex1();
//                    res.push_back(Segment2D(Point2(v0->x(), v0->y()), Point2(v1->x(), v1->y())));
//                }
//            }
//            edge = edge->rot_next();
//        } while (edge != vertex.incident_edge());
//    }
//    ork::Logger::DEBUG_LOGGER->logf("SKELETONIZATION", "Nb res : %d / %d", nbInclus, nbTot);
}

void VoronoiDiagram::triangulate(std::vector<Triangle2D> &res)
{
    boost::polygon::construct_voronoi(points_.begin(), points_.end(), segments_.begin(), segments_.end(), &vd_);

    for (boost_vd::const_edge_iterator it = vd_.edges().begin(); it != vd_.edges().end(); ++it) {
        const Edge e = *it;
        if (it->is_primary()) {
            const Edge *twin = e.twin();
            if (e.cell()->source_index() < twin->cell()->source_index()) { // make sure we don't add edge twice.
                const Edge *nextedge = e.next();
                if (it->is_infinite() && nextedge->is_infinite()) { // if this edge & the next one are infinite, just pass.
                    continue;
                }
                Point2 p0 = getCellSourcePos(e.cell());
                Point2 p1 = getCellSourcePos(twin->cell());
                Point2 p2 = getCellSourcePos(nextedge->twin()->cell());
                res.push_back(Triangle2D(p0, p1, p2));
            }
        }
    }

}

expressive::core::PointPrimitive2D VoronoiDiagram::getCellSourcePos(const Cell *cell)
{
    switch(cell->source_category()) {
    case boost::polygon::SOURCE_CATEGORY_SINGLE_POINT:
        return points_[cell->source_index()];
    case boost::polygon::SOURCE_CATEGORY_SEGMENT_START_POINT:
        return segments_[cell->source_index()].p1();
    case boost::polygon::SOURCE_CATEGORY_SEGMENT_END_POINT:
//    case boost::polygon::SOURCE_CATEGORY_REVERSE_SEGMENT:
        return segments_[cell->source_index()].p2();

    default:
        return segments_[cell->source_index()].p1();
    }
}
 // TODO ASSOCIATE DATA TO VORONOI INPUT PRIMITIVES !

boost_vd &VoronoiDiagram::result()
{
    return vd_;
}

} // namespace core

} // namespace expressive
