#define TEMPLATE_INSTANTIATION_GRAPH
#include "core/graph/Graph.h"

#include "ork/resource/ResourceTemplate.h"

//#include <core/utils/AbstractIterator.h>

using namespace ork;
using namespace std;

namespace expressive
{

namespace core
{

// Forces graph classes to be instantiated here.
// To improve compilation speed with graphs, you should use the following before using Graph :
// extern template class Graph;
// extern template class Edge;
// ...

template class GraphT<PointPrimitive>;
template class GraphT<PointPrimitiveT<Point2> > ;
template class GraphT<WeightedPoint>;
template class GraphT<WeightedPointT<Point2> >;

//template class VertexT<PointPrimitiveT<Point> >;
//template class VertexT<PointPrimitiveT<Point2> > ;
//template class VertexT<WeightedPoint>;
//template class VertexT<WeightedPointT<Point2> >;

//template class EdgeT<PointPrimitive>;
//template class EdgeT<PointPrimitiveT<Point2> > ;
//template class EdgeT<WeightedPoint>;
//template class EdgeT<WeightedPointT<Point2> >;

template class GraphListenerT<PointPrimitive>;
template class GraphListenerT<WeightedPoint>;

template<>
std::shared_ptr<SegmentT<GraphT<WeightedPoint>::VertexType> > GraphT<WeightedPoint>::CreateSegment(GraphT<WeightedPoint>::VertexPtr start, GraphT<WeightedPoint>::VertexPtr end)
{
    return std::make_shared<WeightedSegment>(start->pos(), end->pos());
}

template<>
void WeightedGraph2D::MergeCloseVertices(Scalar epsilon, bool merge_unlinked_vertices)
{
    bool finished = false;
//    Scalar epsilonSq = epsilon * epsilon;
    epsilon = 1.20;

    std::set<VertexId> merged;

    while (!finished) {
        finished = true;
        auto vertices = GetVertices();
        while (vertices->HasNext()) {
            std::shared_ptr<Vertex> v = vertices->Next();
//            WeightedPoint2D p = v->pos();
            VertexIteratorPtr neighbors;
            if (merge_unlinked_vertices) {
                neighbors = GetVertices();
            } else {
                neighbors = v->GetAdjacentVertices();
            }
            while (neighbors->HasNext()) {
                std::shared_ptr<Vertex> n = neighbors->Next();
                Scalar weight_diff = (v->pos().weight() - n->pos().weight())* epsilon;
                weight_diff *= weight_diff;

                if (n != v && n->pos().weight() <= v->pos().weight() && (((n->pos()) - v->pos()).squaredNorm() < (weight_diff))) {
                    merged.insert(n->id());
                }
            }
            if (merged.size() > 0) {
                merged.insert(v->id());
                /*VertexId res = */MergeVertices(merged, v->id());
                merged.clear();
                finished = false;
                break;
            }
        }
    }
}

/// @cond RESOURCES

class GraphResource : public ResourceTemplate<20, Graph>
{
public:
    GraphResource(ptr<ResourceManager> manager, const string &name, ptr<ResourceDescriptor> desc, const TiXmlElement *e = NULL) :
        ResourceTemplate<20, Graph>(manager, name, desc)
    {
        e = e == NULL ? desc->descriptor : e;
        checkParameters(desc, e, "name,");
        Init();
    }
};

extern const char graph[] = "graph";

static ResourceFactory::Type<graph, GraphResource> GraphType;

/// @endcond

} // namespace core

} // namespace expressive

