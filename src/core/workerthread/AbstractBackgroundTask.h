/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: AbstractBackgroundWorker.h

 Language: C++

 License: expressive license

 \author: Amaury Jung
 E-Mail: amaury.jung@inria.fr

 Description: Abstract class designed to handle long computation done in an other
 thread
 Platform Dependencies: None
 */

#pragma once
#ifndef EXPRESSIVE_ABSTRACT_BACKGROUND_TASK_H
#define EXPRESSIVE_ABSTRACT_BACKGROUND_TASK_H

#include <core/CoreRequired.h>

// std dependencies
#include <mutex>
#include <condition_variable>
#include <memory>

namespace expressive {

namespace core {

class CORE_API AbstractBackgroundTask
        : public std::enable_shared_from_this<AbstractBackgroundTask>
{
public:
    // Parent must be a part of the GUI thread
    AbstractBackgroundTask() {}
    virtual ~AbstractBackgroundTask() {}

    /**
     * It returns a task doing the same think as if "pendingTask" and "this"
     * were called serialy.
     * When a task is discarded, memeroy leak and data corruption are avoided
     * thanks to shared_ptr.
     *
     * Note : "pendingTask" may be a null pointer
     */
    virtual std::shared_ptr<AbstractBackgroundTask> Merge(std::shared_ptr<AbstractBackgroundTask> pendingTask) = 0;

    /**
     * This method is called ON THE WORKER THREAD
     * It contains heavy calculus
     *
     * @warning Every data read or written by this method MUST NOT BE READ
     * NOR WRITTEN by the GUI THREAD
     */
    virtual void DoIt() = 0;
};

} // end of namespace core

} // end of namespace expressive

#endif //EXPRESSIVE_ABSTRACT_BACKGROUND_TASK_H
