/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: WorkerThread.h

 Language: C++

 License: expressive license

 \author: Amaury Jung
 E-Mail: amaury.jung@inria.fr

 Description: Represents a thread executed in parallel of the GUI thread.
 It should be used for long computation methods done in parallel of the main
 application. Tasks are encapsulated in class which inherit the
 AbstractBackgroundWorker class.
 This tasks are requested to the WorkerThread. If the worker thread is not
 computing, the tasks is started as soon as it is requested,
 otherwise it will wait until the current task is done.
 When a task is waiting, it may be overided by another requested task is
 requested, in other word, only the last requested task is done every other
 one are discarded.

 Platform Dependencies: None
 */

#pragma once
#ifndef EXPRESSIVE_WORKER_THREAD_H
#define EXPRESSIVE_WORKER_THREAD_H

#include <core/CoreRequired.h>
#include <core/utils/ListenerT.h>
#include "ork/core/Object.h"

// std dependencies
#include <thread>
#include <mutex>
#include <condition_variable>

namespace expressive {

namespace core {

class AbstractBackgroundTask;

class CORE_API WorkerThread
        : public ork::Object,
        public  ListenableT<WorkerThread >
{
public:
    WorkerThread(std::mutex *mutex_data_retrieval, std::condition_variable *cond_data_retrieval);

    ~WorkerThread();

    void requestNewTask(std::shared_ptr<AbstractBackgroundTask> task);

    void run();
//signals: // where used only to display the "work in progress" icon
//    void taskStarted();
//    void taskFinished();

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_data_retrieval_protection(std::mutex *mutex_data_retrieval, std::condition_variable *cond_data_retrieval)
    {
        mutex_data_retrieval_ = mutex_data_retrieval;
        cond_data_retrieval_ = cond_data_retrieval;
    }

    /////////////////
    /// Attributs ///
    /////////////////

    void interrupt()
    {
        quit_ = true;
        cond_.notify_one();
    }

    void WaitForWorkDone()
    {
        if (thread_.joinable()) {
            thread_.join();
        }
    }

private:

    std::thread thread_;
    std::shared_ptr<AbstractBackgroundTask> pending_task_;

    std::mutex              mutex_;
    std::condition_variable cond_;
    volatile bool quit_;

    // synchronization between auto update and the data (currently the trimesh) retriever
    std::mutex              *mutex_data_retrieval_;
    std::condition_variable *cond_data_retrieval_;

//    bool deleteAfterRun_;

};

} // end of namespace core

} // end of namespace expressive

#endif //EXPRESSIVE_WORKER_THREAD_H
