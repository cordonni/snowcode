#include <core/workerthread/WorkerThread.h>

#include <core/workerthread/AbstractBackgroundTask.h>

#include <functional>

namespace expressive {

namespace core {

WorkerThread::WorkerThread(std::mutex *mutex_data_retrieval, std::condition_variable *cond_data_retrieval)
    : Object("WorkerThread"),
      ListenableT<WorkerThread>(),
      thread_(),
      pending_task_(nullptr),
      quit_(false),
      mutex_data_retrieval_(mutex_data_retrieval),
      cond_data_retrieval_(cond_data_retrieval)
{
}

WorkerThread::~WorkerThread()
{
    {
        std::lock_guard<std::mutex> mlock(mutex_);
        quit_ = true;
    }
    cond_.notify_one();

    WaitForWorkDone();
}

void WorkerThread::requestNewTask(std::shared_ptr<AbstractBackgroundTask> task)
{
    {
        std::lock_guard<std::mutex> mlock(mutex_);
        // merge new tasks while the current task is performed
        pending_task_ = task->Merge(pending_task_);
    }

    if(thread_.joinable())
        cond_.notify_one(); // awaken the main loop
    else
        thread_ = std::thread( std::bind(std::mem_fn(&WorkerThread::run), this) );
}

void WorkerThread::run()
{
    std::shared_ptr<AbstractBackgroundTask> task = nullptr;

    while (!quit_)
    {
        {
            std::unique_lock<std::mutex> mlock(mutex_);

            if (pending_task_ == nullptr && !quit_)
                cond_.wait(mlock); // wait until new task arrive (via a requestNewTask)

            task = pending_task_;
            pending_task_ = nullptr;
        }

        if (task != nullptr)
        {
//            emit taskStarted();
            task->DoIt();
            task = nullptr; // the task will be destroy thanks to the shared_ptr mechanism

            if (!quit_) {
                std::unique_lock<std::mutex> mlock(*mutex_data_retrieval_);
                NotifyUpdate();
                cond_data_retrieval_->wait(mlock); // wait signal informing that data as been retrieved
            }
        }
    }
}

} // end namespace core

} // end namespace expressive

