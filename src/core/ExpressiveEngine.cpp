#include <core/ExpressiveEngine.h>
#include <core/utils/ConfigLoader.h>

#include "ork/resource/ResourceManager.h"
#include "ork/resource/XMLResourceLoader.h"
#include "ork/taskgraph/MultithreadScheduler.h"

#include <clocale>

using namespace ork;
using namespace std;

namespace expressive
{

namespace core
{

//ExpressiveEngine* ExpressiveEngine::current_engine_(nullptr);

ExpressiveEngine::ExpressiveEngine(const string &config_file/*, std::shared_ptr<SceneManager> scene_manager*/) :
    mouse_screen_pos_(Point2::Zero())
{
    Init(config_file);
}

ExpressiveEngine::ExpressiveEngine() :
    mouse_screen_pos_(Point2::Zero())
{
}

ExpressiveEngine::~ExpressiveEngine()
{
    // clear static content
    CommandManager::SetSingleton(nullptr, false);
    ScriptPlayer::SetSingleton(nullptr, false);
    ExpressiveEngine::SetSingleton(nullptr, false);
//    CommandManager::GetSingleton()->clear();
//    ScriptPlayer::GetSingleton()->clear();

    scene_manager_->set_current_camera(nullptr);
    camera_manager_ = nullptr;
    scene_manager_->clear(true);

    scene_manager_->set_scheduler(nullptr);
    scene_manager_->set_resource_manager(nullptr);
    scheduler_ = nullptr;
    script_player_ = nullptr;
    command_manager_ = nullptr;
    scene_manager_ = nullptr;
    resource_loader_ = nullptr;

//    ork::Object::exit();
    resource_manager_ = nullptr;
}

void ExpressiveEngine::Init(const string &config_file/*, std::shared_ptr<SceneManager> scene_manager*/)
{
    // IMPORTANT !!! otherwise, dots are not read correctly on some systems, where it expect commas (due to a bug in QT's QLocale)
    std::setlocale(LC_ALL, "en_US.UTF-8");


    command_manager_ = make_shared<CommandManager>(expressive::Config::getIntParameter("expressive_max_undo_steps"));
    scene_manager_ = make_shared<SceneManager>(command_manager_);
    resource_loader_ = new XMLResourceLoader();
    resource_manager_ = new ResourceManager (resource_loader_);
    script_player_ = std::make_shared<ScriptPlayer>();

    scheduler_ = new MultithreadScheduler();

    scene_manager_->set_resource_manager(resource_manager_);
    scene_manager_->set_scheduler(scheduler_);

    resource_loader_->addPath(".");
    resource_loader_->addPath("../../");
    resource_loader_->addPath("../../examples/");
    resource_loader_->addPath("../../resources/");
    resource_loader_->addPath("../../resources/configs/");
    resource_loader_->addPath("../../resources/graphProfiles/");
    resource_loader_->addPath("../../resources/graphs/");
    resource_loader_->addPath("../../resources/gui/");
    resource_loader_->addPath("../../resources/meshes/");
    resource_loader_->addPath("../../resources/methods/");
    resource_loader_->addPath("../../resources/shaders/");
    resource_loader_->addPath("../../resources/shaders/commons/");
    resource_loader_->addPath("../../resources/shaders/sketching/");
    resource_loader_->addPath("../../resources/textures/");


    // This line loads a configuration from a given file.
    std::shared_ptr<Object> r = resource_manager_->loadResource(config_file);
    if (resource_loader_->hasResource("user" + config_file)) {
        resource_manager_->loadResource("user" + config_file);
    }
    r = nullptr;

    camera_manager_ = nullptr;
}

std::shared_ptr<Camera> ExpressiveEngine::camera_manager() const
{
    return camera_manager_;
}

std::shared_ptr<CommandManager> ExpressiveEngine::command_manager() const
{
    return command_manager_;
}

std::shared_ptr<SceneManager> ExpressiveEngine::scene_manager() const
{
    return scene_manager_;
}

ork::ptr<ork::ResourceManager> ExpressiveEngine::resource_manager() const
{
    return resource_manager_;
}

ork::ptr<ork::XMLResourceLoader> ExpressiveEngine::resource_loader() const
{
    return resource_loader_;
}

ork::ptr<ork::MultithreadScheduler> ExpressiveEngine::scheduler() const
{
    return scheduler_;
}

std::shared_ptr<ScriptPlayer> ExpressiveEngine::script_player() const
{
    return script_player_;
}

void ExpressiveEngine::set_scene_manager(std::shared_ptr<SceneManager> scene_manager)
{
    scene_manager_ = scene_manager;
}

void ExpressiveEngine::set_mouse_screen_position(const Point2 &pos)
{
    mouse_screen_pos_ = pos;
}

Point2 ExpressiveEngine::mouse_screen_position() const
{
    return mouse_screen_pos_;
}

} // namespace core

} // namespace expressive
