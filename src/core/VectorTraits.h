/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_VECTOR_TRAITS_H_
#define EXPRESSIVE_VECTOR_TRAITS_H_

#include "CoreRequired.h"
//#include "EigenVectorTraits.h"
#include "ork/OrkVectorTraits.h"

//...
#undef Point

// This big macro maps types from a given Trait to the one used in expressive.
// avoids a lot of recopy.
#define EXPRESSIVE_MAKE_TYPEDEFS(Trait, TypeSuffix, Size)   \
typedef Trait::Matrix##Size##TypeSuffix Matrix##Size##TypeSuffix; \
typedef Trait::Vector##Size##TypeSuffix Vector##Size##TypeSuffix; \
typedef Trait::Vector##Size##TypeSuffix Normal##Size##TypeSuffix; \
typedef Trait::Vector##Size##TypeSuffix Point##Size##TypeSuffix;
//typedef Trait::RowVector##Size##TypeSuffix RowVector##Size##TypeSuffix;

#define EXPRESSIVE_MAKE_ASYMETRIC_TYPEDEFS(Trait, TypeSuffix, Size, Size2)    \
typedef Trait::Matrix##Size##X##Size2##TypeSuffix Matrix##Size##X##Size2##TypeSuffix;

#define EXPRESSIVE_MAKE_TYPEDEFS_ALL_SIZES(Trait, TypeSuffix) \
EXPRESSIVE_MAKE_TYPEDEFS(Trait, TypeSuffix, 2) \
EXPRESSIVE_MAKE_TYPEDEFS(Trait, TypeSuffix, 3) \
EXPRESSIVE_MAKE_TYPEDEFS(Trait, TypeSuffix, 4)
//EXPRESSIVE_MAKE_ASYMETRIC_TYPEDEFS(Trait, TypeSuffix, 2, 3)
//EXPRESSIVE_MAKE_ASYMETRIC_TYPEDEFS(Trait, TypeSuffix, 2, 4)
//EXPRESSIVE_MAKE_ASYMETRIC_TYPEDEFS(Trait, TypeSuffix, 3, 2)
//EXPRESSIVE_MAKE_ASYMETRIC_TYPEDEFS(Trait, TypeSuffix, 3, 4)
//EXPRESSIVE_MAKE_ASYMETRIC_TYPEDEFS(Trait, TypeSuffix, 4, 2)
//EXPRESSIVE_MAKE_ASYMETRIC_TYPEDEFS(Trait, TypeSuffix, 4, 3)


namespace expressive
{
//TODO:@todo : add comment ... why this is done ... and add the other typdef when needed
typedef OrkVectorTraits VectorTraits;
//typedef EigenVectorTraits VectorTraits;

//    typedef EigenVectorTraits::Scalar Scalar;

    // This creates typedefs for Vector & Matrix types for every type defined here, in the form
    // Vector2d, Vector2f, Vector2i, Vector2ui, Vector2b, Vector3d, etc...
    // creates, for each type :
    // - Vectors of size 2/3/4 (vertical and horizontal)
    // - Matrices of size 2x2, 2x3, 2x4, 3x2, 3x3, 3x4, 4x2, 4x3, 4x4
    EXPRESSIVE_MAKE_TYPEDEFS_ALL_SIZES(VectorTraits, b)
    EXPRESSIVE_MAKE_TYPEDEFS_ALL_SIZES(VectorTraits, d)
    EXPRESSIVE_MAKE_TYPEDEFS_ALL_SIZES(VectorTraits, f)
    EXPRESSIVE_MAKE_TYPEDEFS_ALL_SIZES(VectorTraits, i)
    EXPRESSIVE_MAKE_TYPEDEFS_ALL_SIZES(VectorTraits, s)
    EXPRESSIVE_MAKE_TYPEDEFS_ALL_SIZES(VectorTraits, ui)
    EXPRESSIVE_MAKE_TYPEDEFS_ALL_SIZES(VectorTraits, uc)

    template<typename T>
    using Vector2T = VectorTraits::Vector2T<T>;
    template<typename T>
    using Vector3T = VectorTraits::Vector3T<T>;
    template<typename T>
    using Vector4T = VectorTraits::Vector4T<T>;
    template<typename T>
    using Matrix2T = VectorTraits::Matrix2T<T>;
    template<typename T>
    using Matrix3T = VectorTraits::Matrix3T<T>;
    template<typename T>
    using Matrix4T = VectorTraits::Matrix4T<T>;

    typedef Vector3T<Scalar> Point;
    typedef Vector3T<Scalar> Normal;
    typedef Vector3T<Scalar> Vector;
    typedef Vector2T<Scalar> Point2;
    typedef Vector3T<Scalar> Point3;
    typedef Vector4T<Scalar> Point4;
    typedef Vector2T<Scalar> Vector2;
    typedef Vector3T<Scalar> Vector3;
    typedef Vector4T<Scalar> Vector4;
    typedef Matrix3T<Scalar> Matrix;
    typedef Matrix2T<Scalar> Matrix2;
    typedef Matrix3T<Scalar> Matrix3;
    typedef Matrix4T<Scalar> Matrix4;

//    typedef Point3f Point;
//    typedef Point2f Point2;
//    typedef Point3f Point3;
//    typedef Point4f Point4;
//    typedef Normal3f Normal;
//    typedef Vector3f Vector;
//    typedef Matrix3f Matrix;
//    typedef Matrix4f Matrix4;
    typedef VectorTraits::Quaternion Quaternion;
    typedef VectorTraits::Transform  Transform;
    typedef VectorTraits::ProjectiveTransform  ProjectiveTransform;
//    typedef VectorTraits::AngleAxis AngleAxis;

    typedef Point3f HSV;

    /**
     * This class contains 4 unsigned int as RGBA values.
     * This class MUST contain either floats or ints, as it is used in texture, and opengl does not
     * support GL_DOUBLE textures on most computers. So, in order to still be able to compare colors easily,
     * we chose to store them as unsigned ints.
     */
    class Color : public VectorTraits::Color
    {
    public:
        CORE_API Color(unsigned int r = 128, unsigned int g = 128, unsigned int b = 128, unsigned int a = 255);
        CORE_API Color(double r, double g, double b, double a = 1.0, double normalizer = 1.0);
        CORE_API Color(float r, float g, float b, float a = 1.0f, float normalizer = 1.0f);
        CORE_API Color(int r, int g, int b, int a = 255);
        CORE_API Color(const Point4f& p);

        CORE_API Color(const std::string hexstr);
        static CORE_API Color Constant(const int x);
        static CORE_API Color Constant(const unsigned int x);
        static CORE_API Color Constant(const double x);
        static CORE_API Color Constant(const float x);
        static CORE_API Color Zero();
        static CORE_API Color One();


        // Basic colors
        static CORE_API Color White        ();
        static CORE_API Color Black        ();

        // Primary colors // unsigned int version
        static CORE_API Color Gray         (unsigned int x = 128);
        static CORE_API Color Red          (unsigned int x = 255);
        static CORE_API Color Green        (unsigned int x = 255);
        static CORE_API Color Blue         (unsigned int x = 255);

        // Secondary colors
        static CORE_API Color Yellow       (unsigned int x = 255);
        static CORE_API Color Cyan         (unsigned int x = 255);
        static CORE_API Color Magenta      (unsigned int x = 255);

        // Tertiary colors
        static CORE_API Color Orange       (unsigned int x = 255);
        static CORE_API Color Chartreuse   (unsigned int x = 255);
        static CORE_API Color Spring       (unsigned int x = 255);
        static CORE_API Color Azure        (unsigned int x = 255);
        static CORE_API Color Violet       (unsigned int x = 255);
        static CORE_API Color Rose         (unsigned int x = 255);


        // Primary colors - float version
        static CORE_API Color Gray         (float x);
        static CORE_API Color Red          (float x);
        static CORE_API Color Green        (float x);
        static CORE_API Color Blue         (float x);

        // Secondary colors
        static CORE_API Color Yellow       (float x);
        static CORE_API Color Cyan         (float x);
        static CORE_API Color Magenta      (float x);

        // Tertiary colors
        static CORE_API Color Orange       (float x);
        static CORE_API Color Chartreuse   (float x);
        static CORE_API Color Spring       (float x);
        static CORE_API Color Azure        (float x);
        static CORE_API Color Violet       (float x);
        static CORE_API Color Rose         (float x);

        // Primary colors - double version
        static CORE_API Color Gray         (double x);
        static CORE_API Color Red          (double x);
        static CORE_API Color Green        (double x);
        static CORE_API Color Blue         (double x);

        // Secondary colors
        static CORE_API Color Yellow       (double x);
        static CORE_API Color Cyan         (double x);
        static CORE_API Color Magenta      (double x);

        // Tertiary colors
        static CORE_API Color Orange       (double x);
        static CORE_API Color Chartreuse   (double x);
        static CORE_API Color Spring       (double x);
        static CORE_API Color Azure        (double x);
        static CORE_API Color Violet       (double x);
        static CORE_API Color Rose         (double x);

        // Primary colors // int version
        static CORE_API Color Gray         (int x);
        static CORE_API Color Red          (int x);
        static CORE_API Color Green        (int x);
        static CORE_API Color Blue         (int x);

        // Secondary colors
        static CORE_API Color Yellow       (int x);
        static CORE_API Color Cyan         (int x);
        static CORE_API Color Magenta      (int x);

        // Tertiary colors
        static CORE_API Color Orange       (int x);
        static CORE_API Color Chartreuse   (int x);
        static CORE_API Color Spring       (int x);
        static CORE_API Color Azure        (int x);
        static CORE_API Color Violet       (int x);
        static CORE_API Color Rose         (int x);

        static CORE_API Color Random();

        // Accessors
        CORE_API unsigned int Ri() const;
        CORE_API unsigned int Gi() const;
        CORE_API unsigned int Bi() const;
        CORE_API unsigned int Ai() const;
        CORE_API float R() const;
        CORE_API float G() const;
        CORE_API float B() const;
        CORE_API float A() const;

//        unsigned int& R();
//        unsigned int& G();
//        unsigned int& B();
//        unsigned int& A();
        CORE_API float& R();
        CORE_API float& G();
        CORE_API float& B();
        CORE_API float& A();

        CORE_API void saturate();

        static CORE_API Color interpolate(const Color& c0, const Color c1, float t = 0.5);

        // Export
        CORE_API std::string Hex() const;

        friend CORE_API bool operator==(Color &c1, Color &c2);
        friend CORE_API bool operator!=(Color &c1, Color &c2);
    };

    CORE_API bool operator==(Color &c1, Color &c2);
    CORE_API bool operator!=(Color &c1, Color &c2);


//    /**
//     * Returns the product of a matrix and of a given vector. The given
//     * vector w coordinate is set to 1, and the 4 vector result is converted
//     * to a 3 vector by dividing its xyz components by its w component.
//     */
//    template<typename T = Scalar>
//    inline Vector3T<T> operator*(const Matrix4T<T> &m, const Vector3T<T> &v)
//    {
//        Vector3T<T> r;

//        T fInvW = T(1.0) / (m(3,0) * v[0] + m(3,1) * v[1] + m(3,2) * v[2] + m(3,3));

//        r[0] = (m(0, 0) * v[0] + m(0, 1) * v[1] + m(0, 2) * v[2] + m(0, 3)) * fInvW;
//        r[1] = (m(1, 0) * v[0] + m(1, 1) * v[1] + m(1, 2) * v[2] + m(1, 3)) * fInvW;
//        r[2] = (m(2, 0) * v[0] + m(2, 1) * v[1] + m(2, 2) * v[2] + m(2, 3)) * fInvW;

//        return r;
//    }
} // close namespace expressive

#undef EXPRESSIVE_MAKE_TYPEDEFS_ALL_SIZES
#undef EXPRESSIVE_MAKE_TYPEDEFS
#undef EXPRESSIVE_MAKE_ASYMETRIC_TYPEDEFS

#endif // EXPRESSIVE_VECTOR_TRAITS_H_
