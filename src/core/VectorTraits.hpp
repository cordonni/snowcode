#ifndef VECTOR_TRAITS_HPP_
#define VECTOR_TRAITS_HPP_

#include <iomanip>

#include "core/VectorTraits.h"

//// explicit instantiation
//template class Eigen::Matrix<int, 1, 2>;
//template class Eigen::Matrix<int, 1, 3>;
//template class Eigen::Matrix<int, 1, 4>;
//template class Eigen::Matrix<int, 2, 1>;
//template class Eigen::Matrix<int, 2, 2>;
//template class Eigen::Matrix<int, 2, 3>;
//template class Eigen::Matrix<int, 2, 4>;
//template class Eigen::Matrix<int, 3, 1>;
//template class Eigen::Matrix<int, 3, 2>;
//template class Eigen::Matrix<int, 3, 3>;
//template class Eigen::Matrix<int, 3, 4>;
//template class Eigen::Matrix<int, 4, 1>;
//template class Eigen::Matrix<int, 4, 2>;
//template class Eigen::Matrix<int, 4, 3>;
//template class Eigen::Matrix<int, 4, 4>;

//template class Eigen::Matrix<bool, 1, 2>;
//template class Eigen::Matrix<bool, 1, 3>;
//template class Eigen::Matrix<bool, 1, 4>;
//template class Eigen::Matrix<bool, 2, 1>;
//template class Eigen::Matrix<bool, 2, 2>;
//template class Eigen::Matrix<bool, 2, 3>;
//template class Eigen::Matrix<bool, 2, 4>;
//template class Eigen::Matrix<bool, 3, 1>;
//template class Eigen::Matrix<bool, 3, 2>;
//template class Eigen::Matrix<bool, 3, 3>;
//template class Eigen::Matrix<bool, 3, 4>;
//template class Eigen::Matrix<bool, 4, 1>;
//template class Eigen::Matrix<bool, 4, 2>;
//template class Eigen::Matrix<bool, 4, 3>;
//template class Eigen::Matrix<bool, 4, 4>;

//template class Eigen::Matrix<short, 1, 2>;
//template class Eigen::Matrix<short, 1, 3>;
//template class Eigen::Matrix<short, 1, 4>;
//template class Eigen::Matrix<short, 2, 1>;
//template class Eigen::Matrix<short, 2, 2>;
//template class Eigen::Matrix<short, 2, 3>;
//template class Eigen::Matrix<short, 2, 4>;
//template class Eigen::Matrix<short, 3, 1>;
//template class Eigen::Matrix<short, 3, 2>;
//template class Eigen::Matrix<short, 3, 3>;
//template class Eigen::Matrix<short, 3, 4>;
//template class Eigen::Matrix<short, 4, 1>;
//template class Eigen::Matrix<short, 4, 2>;
//template class Eigen::Matrix<short, 4, 3>;
//template class Eigen::Matrix<short, 4, 4>;

//template class Eigen::Matrix<float, 1, 2>;
//template class Eigen::Matrix<float, 1, 3>;
//template class Eigen::Matrix<float, 1, 4>;
//template class Eigen::Matrix<float, 2, 1>;
//template class Eigen::Matrix<float, 2, 2>;
//template class Eigen::Matrix<float, 2, 3>;
//template class Eigen::Matrix<float, 2, 4>;
//template class Eigen::Matrix<float, 3, 1>;
//template class Eigen::Matrix<float, 3, 2>;
//template class Eigen::Matrix<float, 3, 3>;
//template class Eigen::Matrix<float, 3, 4>;
//template class Eigen::Matrix<float, 4, 1>;
//template class Eigen::Matrix<float, 4, 2>;
//template class Eigen::Matrix<float, 4, 3>;
//template class Eigen::Matrix<float, 4, 4>;

//template class Eigen::Matrix<double, 1, 2>;
//template class Eigen::Matrix<double, 1, 3>;
//template class Eigen::Matrix<double, 1, 4>;
//template class Eigen::Matrix<double, 2, 1>;
//template class Eigen::Matrix<double, 2, 2>;
//template class Eigen::Matrix<double, 2, 3>;
//template class Eigen::Matrix<double, 2, 4>;
//template class Eigen::Matrix<double, 3, 1>;
//template class Eigen::Matrix<double, 3, 2>;
//template class Eigen::Matrix<double, 3, 3>;
//template class Eigen::Matrix<double, 3, 4>;
//template class Eigen::Matrix<double, 4, 1>;
//template class Eigen::Matrix<double, 4, 2>;
//template class Eigen::Matrix<double, 4, 3>;
//template class Eigen::Matrix<double, 4, 4>;

//template class Eigen::Matrix<unsigned int , 1, 2>;
//template class Eigen::Matrix<unsigned int , 1, 3>;
//template class Eigen::Matrix<unsigned int , 1, 4>;
//template class Eigen::Matrix<unsigned int , 2, 1>;
//template class Eigen::Matrix<unsigned int , 2, 2>;
//template class Eigen::Matrix<unsigned int , 2, 3>;
//template class Eigen::Matrix<unsigned int , 2, 4>;
//template class Eigen::Matrix<unsigned int , 3, 1>;
//template class Eigen::Matrix<unsigned int , 3, 2>;
//template class Eigen::Matrix<unsigned int , 3, 3>;
//template class Eigen::Matrix<unsigned int , 3, 4>;
//template class Eigen::Matrix<unsigned int , 4, 1>;
//template class Eigen::Matrix<unsigned int , 4, 2>;
//template class Eigen::Matrix<unsigned int , 4, 3>;
//template class Eigen::Matrix<unsigned int , 4, 4>;

//template class Eigen::Matrix<unsigned char, 1, 2>;
//template class Eigen::Matrix<unsigned char, 1, 3>;
//template class Eigen::Matrix<unsigned char, 1, 4>;
//template class Eigen::Matrix<unsigned char, 2, 1>;
//template class Eigen::Matrix<unsigned char, 2, 2>;
//template class Eigen::Matrix<unsigned char, 2, 3>;
//template class Eigen::Matrix<unsigned char, 2, 4>;
//template class Eigen::Matrix<unsigned char, 3, 1>;
//template class Eigen::Matrix<unsigned char, 3, 2>;
//template class Eigen::Matrix<unsigned char, 3, 3>;
//template class Eigen::Matrix<unsigned char, 3, 4>;
//template class Eigen::Matrix<unsigned char, 4, 1>;
//template class Eigen::Matrix<unsigned char, 4, 2>;
//template class Eigen::Matrix<unsigned char, 4, 3>;
//template class Eigen::Matrix<unsigned char, 4, 4>;


namespace expressive
{

inline Color::Color(float r, float g, float b, float a, float normalizer) :
    VectorTraits::Color(r / normalizer, g / normalizer, b / normalizer, a / normalizer)
{
}

inline Color::Color(double r, double g, double b, double a, double normalizer) :
    VectorTraits::Color((float)(r / normalizer), (float)(g / normalizer), (float)(b / normalizer), (float)(a / normalizer))
{
}

inline Color::Color(unsigned int r, unsigned int g, unsigned int b, unsigned int a) :
    VectorTraits::Color(r / 255.f, g / 255.f, b / 255.f, a / 255.f)
{
}

inline Color::Color(int r, int g, int b, int a) :
    VectorTraits::Color(r / 255.f, g / 255.f, b / 255.f, a / 255.f)
{
}

inline Color::Color(const Point4f& p) :
    VectorTraits::Color(p[0], p[1], p[2], p[3])
{
}

inline Color::Color(const std::string hexstr)
{
   unsigned int r,g,b,a(255), sum(0);
   std::stringstream ss;
   ss << std::hex << hexstr; ss >> sum;

   if(hexstr.length() < 8)
   {
       b = sum % 0x100;
       g = sum / 0x100 % 0x100;
       r = sum / 0x10000;
   }
   else
   {
       a = sum % 0x100;
       b = sum / 0x100 % 0x100;
       g = sum / 0x10000 % 0x100;
       r = sum / 0x1000000;
   }

   *this = Color(float(r) / 255.f, float(g) / 255.f, float(b) / 255.f, float(a) / 255.f);
}

inline Color Color::Constant(const int x) { return Color(x,x,x,x); }
inline Color Color::Constant(const unsigned int x) { return Color(x,x,x,x); }
inline Color Color::Constant(const float x) { return Color(x,x,x,x); }
inline Color Color::Constant(const double x) { return Color(x,x,x,x); }

inline Color Color::Zero()
{
    return Constant(0.f);
}

inline Color Color::One()
{
    return Constant(1.0f);
}

// Basic colors
inline Color Color::White        ()
{
    return Color(1.f, 1.f, 1.f, 1.f);
}

inline Color Color::Black        ()
{
    return Color(0.f, 0.f, 0.f, 1.f);
}

inline Color Color::Gray(int x) { return Color(x, x, x); }
inline Color Color::Gray(unsigned int x) { return Color(x, x, x); }
inline Color Color::Gray(float x) { return Color(x, x, x); }
inline Color Color::Gray(double x) { return Color(x, x, x); }

// FLOATS
// Primary colors
inline Color Color::Red          (float x) { return Color(x, 0.f, 0.f); }
inline Color Color::Green        (float x) { return Color(0.f, x, 0.f); }
inline Color Color::Blue         (float x) { return Color(0.f, 0.f, x); }

// Secondary colors
inline Color Color::Yellow       (float x) { return Color(x, x, 0.f); }
inline Color Color::Cyan         (float x) { return Color(0.f, x, x); }
inline Color Color::Magenta      (float x) { return Color(x, 0.f, x); }

// Tertiary colors
inline Color Color::Orange       (float x) { return Color(x, 0.5f*x, 0.f); }
inline Color Color::Chartreuse   (float x) { return Color(0.5f*x, x, 0.f); }
inline Color Color::Spring       (float x) { return Color(0.f, x, 0.5f*x); }
inline Color Color::Azure        (float x) { return Color(0.f, 0.5f*x, x); }
inline Color Color::Violet       (float x) { return Color(0.5f*x, 0.f, x); }
inline Color Color::Rose         (float x) { return Color(x, 0.f, 0.5f*x); }


// DOUBLES
// Primary colors
inline Color Color::Red          (double x) { return Color(x, 0.0, 0.0); }
inline Color Color::Green        (double x) { return Color(0.0, x, 0.0); }
inline Color Color::Blue         (double x) { return Color(0.0, 0.0, x); }

// Secondary colors
inline Color Color::Yellow       (double x) { return Color(x, x, 0.0); }
inline Color Color::Cyan         (double x) { return Color(0.0, x, x); }
inline Color Color::Magenta      (double x) { return Color(x, 0.0, x); }

// Tertiary colors
inline Color Color::Orange       (double x) { return Color(x, 0.5f*x, 0.0); }
inline Color Color::Chartreuse   (double x) { return Color(0.5f*x, x, 0.0); }
inline Color Color::Spring       (double x) { return Color(0.0, x, 0.5f*x); }
inline Color Color::Azure        (double x) { return Color(0.0, 0.5f*x, x); }
inline Color Color::Violet       (double x) { return Color(0.5f*x, 0.0, x); }
inline Color Color::Rose         (double x) { return Color(x, 0.0, 0.5f*x); }

// INTS
// Primary colors
inline Color Color::Red          (int x) { return Color(x, 0, 0); }
inline Color Color::Green        (int x) { return Color(0, x, 0); }
inline Color Color::Blue         (int x) { return Color(0, 0, x); }

// Secondary colors
inline Color Color::Yellow       (int x) { return Color(x, x, 0); }
inline Color Color::Cyan         (int x) { return Color(0, x, x); }
inline Color Color::Magenta      (int x) { return Color(x, 0, x); }

// Tertiary colors
inline Color Color::Orange       (int x) { return Color(x, (int)(0.5*x), 0); }
inline Color Color::Chartreuse   (int x) { return Color((int)(0.5*x), x, 0); }
inline Color Color::Spring       (int x) { return Color(0, x, (int)(0.5*x)); }
inline Color Color::Azure        (int x) { return Color(0, (int)(0.5*x), x); }
inline Color Color::Violet       (int x) { return Color((int)(0.5*x), 0, x); }
inline Color Color::Rose         (int x) { return Color(x, 0, (int)(0.5*x)); }

// UNSIGNED INTS
// Primary colors
inline Color Color::Red          (unsigned int x) { return Color(x, (uint)0, (uint)0); }
inline Color Color::Green        (unsigned int x) { return Color((uint)0, x, (uint)0); }
inline Color Color::Blue         (unsigned int x) { return Color((uint)0, (uint)0, x); }

// Secondary colors
inline Color Color::Yellow       (unsigned int x) { return Color(x, x, (uint)0); }
inline Color Color::Cyan         (unsigned int x) { return Color((uint)0, x, x); }
inline Color Color::Magenta      (unsigned int x) { return Color(x, (uint)0, x); }

// Tertiary colors
inline Color Color::Orange       (unsigned int x) { return Color(x, (unsigned int)(0.5*x), (uint)0); }
inline Color Color::Chartreuse   (unsigned int x) { return Color((unsigned int)(0.5*x), x, (uint)0); }
inline Color Color::Spring       (unsigned int x) { return Color((uint)0, x, (unsigned int)(0.5*x)); }
inline Color Color::Azure        (unsigned int x) { return Color((uint)0, (unsigned int)(0.5*x), x); }
inline Color Color::Violet       (unsigned int x) { return Color((unsigned int)(0.5*x), (uint)0, x); }
inline Color Color::Rose         (unsigned int x) { return Color(x, (uint)0, (unsigned int)(0.5*x)); }

inline Color Color::Random()
{
    return Color(float(rand()%255) / 255.f, float(rand()%255) / 255.f, float(rand()%255) / 255.f, 1.0f);
}

// Accessors
inline unsigned int Color::Ri() const
{
    return (unsigned int) operator[](0) * 255;
}

inline unsigned int Color::Gi() const
{
    return (unsigned int) operator[](1) * 255;
}

inline unsigned int Color::Bi() const
{
    return (unsigned int) operator[](2) * 255;
}

inline unsigned int Color::Ai() const
{
    return (unsigned int) operator[](3) * 255;
}

//unsigned int& Color::R()
//{
//    return operator[](0);
//}

//unsigned int& Color::G()
//{
//    return operator[](1);
//}

//unsigned int& Color::B()
//{
//    return operator[](2);
//}

//unsigned int& Color::A()
//{
//    return operator[](3);
//}

inline float Color::R() const
{
    return operator[](0);
}

inline float Color::G() const
{
    return operator[](1);
}

inline float Color::B() const
{
    return operator[](2);
}

inline float Color::A() const
{
    return operator[](3);
}

inline float& Color::R()
{
    return operator[](0);
}

inline float& Color::G()
{
    return operator[](1);
}

inline float& Color::B()
{
    return operator[](2);
}

inline float& Color::A()
{
    return operator[](3);
}

inline void Color::saturate() { R() = std::min(R(), 1.0f);
                  G() = std::min(G(), 1.0f);
                  B() = std::min(B(), 1.0f);
                  A() = std::min(A(), 1.0f);}

inline Color Color::interpolate(const Color& c0, const Color c1, float t)
{
    return Color(c0.R() * (1.0 - t) + c1.R() * t,
                 c0.G() * (1.0 - t) + c1.G() * t,
                 c0.B() * (1.0 - t) + c1.B() * t,
                 c0.A() * (1.0 - t) + c1.A() * t);
}

// Export
inline std::string Color::Hex() const
{
    std::stringstream ss;
      ss << "0x"
         << std::setfill('0') << std::setw(2)  << std::hex << R() * 255
         << std::setfill('0') << std::setw(2)  << std::hex << G() * 255
         << std::setfill('0') << std::setw(2)  << std::hex << B() * 255
         << std::setfill('0') << std::setw(2)  << std::hex << A() * 255;
      return ss.str();
}

inline bool operator==(Color &c1, Color &c2)
{
    for (unsigned int i = 0; i < Color::RowsAtCompileTime; ++i) {
        if (((int) (c1[i] * 255)) != ((int)(c2[i] * 255))) {
            return false;
        }
    }
    return true;
}

inline bool operator!=(Color &c1, Color &c2)
{
    for (unsigned int i = 0; i < Color::RowsAtCompileTime; ++i) {
        if (((int) (c1[i] * 255)) != ((int)(c2[i] * 255))) {
            return true;
        }
    }
    return false;
}

} // Close namespace expressive

#endif //VECTOR_TRAITS_HPP_
