/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


/*!
   \file: FunctorTypeMapT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inria.fr

   Description: Header file for a mapper from a typetree to something (template).
                This is basically a map containing a map containing a map etc etc...
                We store elements according to their type in order.

                TODO : try to make this container std compliant (this would have important impact but that would be far nicer...)

   Platform Dependencies: None
*/


#pragma once
#ifndef CONVOL_FUNCTOR_TYPE_MAP_H_
#define CONVOL_FUNCTOR_TYPE_MAP_H_

// core dependencies
#include<core/CoreRequired.h>
    #include<core/functor/EnumFunctorType.h>

// std dependencies
#include<map>
#include<assert.h>

namespace expressive {

namespace core {

// This class is only necessary to wrap the std map with a template... 
template< typename value_type >
class FunctorTypeMapT
{

public:
    // Structure to store the mapping.
    // For any given functor type, this lead to the builder function.
    struct MapTree
    {
        MapTree(){}
        MapTree(value_type value_stored): value_stored_(value_stored) {}

        value_type value_stored_;
        std::map<std::string, MapTree> submap_;
    };

    typedef typename std::map<std::string, MapTree> MapperType;
    typedef typename MapperType::iterator MapperTypeIterator;

    FunctorTypeMapT(){}
    ~FunctorTypeMapT(){}

    // bool return true if found, false otherwise
    // val_res contain the value found if found, not changed otherwise
    bool find(const FunctorTypeTree& type_tree, value_type& val_res)
    {
        MapperTypeIterator it;
        if(recursive_map_finder(mapper_, type_tree, it)){
            val_res = it->second.value_stored_;
            return true;
        }else{
            return false;
        }
    }
    // insert a new entry if there is one, otherwise do nothing.
    bool insert(const FunctorTypeTree& type_tree, value_type val)
    {
        MapperTypeIterator it_res;
        bool built = recursive_map_builder(mapper_, type_tree, it_res);

        if(built){
            it_res->second.value_stored_ = val;
            return true;
        }else{
            return false;
        }
    }

    const MapperType& mapper() const { return mapper_; }

private:


    // Actual tree that can be parsed to get to the builder function.
    MapperType mapper_;

    /** \brief Find the leaf corresponding to the given FunctorTypeTree in the given MapperType.
    *
    *   \return true if the type_tree lead to a leaf of mapper_. False otherwise.
    *  
    *   \param pmap A MapperType where we want to find the leaf
    *   \param type_tree The FnuctorTypeTree defining the path to the leaf
    *   \param it_res A resulting iterator to a MapperType corresponding to the FunctorTypeTree given as parameter. pmap.end() if not found.
    */
    static bool recursive_map_finder(MapperType& pmap, const FunctorTypeTree& type_tree, MapperTypeIterator& it_res)
    {
        MapperTypeIterator it = pmap.find(type_tree.type_);
        if(it != pmap.end())
        {
            for(unsigned int i = 0; i<type_tree.subtypes_.size(); ++i)
            {
                MapperType& submap = it->second.submap_;
                if(!recursive_map_finder(submap, type_tree.subtypes_[i], it))
                {
                    it_res = pmap.end();
                    return false;
                }
            }
            it_res = it;
            return true;
        }
        else
        {
            assert(false); // The kernel wanted is not registered.
            it_res = pmap.end();
            return false;
        }
    }

    /** \brief Build the path in the mapper_ down to the MapperType node that will contain
    *          the primitive builder function for the given FunctorTypeTree.
    *
    *   \param pmap A MapperType where we want to insert the MapTree corresponding to type_tree
    *   \param type_tree The FnuctorTypeTree we want to build a path for.
    *   \param it_res An iterator to the MapperType corresponding to the FunctorTypeTree given as parameter.
    *
    *   \return true if it was built, false if it does exist already.
    */
    static bool recursive_map_builder(MapperType& pmap, const FunctorTypeTree& type_tree, MapperTypeIterator& it_res)
    {
        const MapTree init_fmt = MapTree();
        std::pair<MapperTypeIterator, bool> it_pair = pmap.insert(std::pair<std::string, MapTree>(type_tree.type_, init_fmt));

        MapperTypeIterator it = it_pair.first;
        bool a_cell_has_been_created = it_pair.second;
        for(unsigned int i = 0; i<type_tree.subtypes_.size(); ++i)
        {
            a_cell_has_been_created = recursive_map_builder(it->second.submap_, type_tree.subtypes_[i], it);
        }

        it_res = it;

        return a_cell_has_been_created;
    }

}; // // End class FunctorTypeMap declaration
    
} // Close namespace core

} // Close namespace expressive

#endif // CONVOL_FUNCTOR_TYPE_MAP_H_
