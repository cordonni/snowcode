/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: EnumFunctorType.h

   Language: C++

   License: Convol Licence

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for enum type ofr functors.
                Here are types to identify a functor. They will be used by
                function Type() defined in all functors.

                Do not forget to add one when you add a functor ! It is especially 
                important when you add a kernel functor.

                Naming rule for types :
                 - Same name as the corresponding type with E
                 Ex : Cauchy -> E_Cauchy

   Platform Dependencies: None
*/

#pragma once
#ifndef CORE_ENUM_FUNCTOR_TYPE_H_
#define CORE_ENUM_FUNCTOR_TYPE_H_

#include "core/CoreRequired.h"

// std dependencies
#include<vector>
#include<string>

namespace expressive {

namespace core {

    /*! \brief This class will help to return complex functor types.
     *         Indeed, some functors like ProfilDistance require to be specialized
     *         with other functors.
     *         Therefore, to return a complete type we can return a tree of types.
     */
    struct CORE_API FunctorTypeTree
    {
        FunctorTypeTree();
        explicit FunctorTypeTree(std::string type);

        FunctorTypeTree(std::string type, const std::vector<FunctorTypeTree>& subtypes);

        bool operator==(const FunctorTypeTree& other) const;

        bool operator != (const FunctorTypeTree& other) const;

        std::string type_;
        std::vector<FunctorTypeTree> subtypes_;
    };

/*
  // DEPRECATED : out-of-date ...
    enum EFunctorType {
        // Kernels
        E_Cauchy,
            E_Cauchy1,
            E_Cauchy2,
            E_Cauchy3,
            E_Cauchy4,
            E_Cauchy5,
            E_Cauchy6,
            E_Cauchy7,
            E_Cauchy8,
            E_Cauchy9,
            E_Cauchy10,
        E_Inverse,
            E_Inverse1,
            E_Inverse2,
            E_Inverse3,
            E_Inverse4,
            E_Inverse5,
            E_Inverse6,
            E_Inverse7,
            E_Inverse8,
            E_Inverse9,
            E_Inverse10,
        E_CompactPolynomial,
            E_CompactPolynomial1,
            E_CompactPolynomial2,
            E_CompactPolynomial3,
            E_CompactPolynomial4,
            E_CompactPolynomial5,
            E_CompactPolynomial6,
            E_CompactPolynomial7,
            E_CompactPolynomial8,
            E_CompactPolynomial9,
            E_CompactPolynomial10,
        E_AnalyticalConvol,
        E_BBCWLocalProfil,
        E_DamienT,
        E_Gaussian,
        E_Metaballs,
        E_MinusX,
        E_NumericalConvol,
        E_ProfilDistance,
        E_QuarticPolynomial,
        E_SemiNumericalHomotheticConvol,

        // Distances
        E_EuclideanDistance,

        // Operators
            // BlendOperators
            E_BlendBarthe,
            E_BlendBartheDifference,
            E_BlendBartheIntersection,
            E_BlendCleanUnion,
            E_BlendDiff,
            E_BlendMax,
            E_BlendMin,
            E_BlendRicci,
            E_BlendSum,
            E_BlendGradient,
            E_BlendGradientN,
            E_BlendGradientConvolN,
        E_Minus,

        // Noises
        E_NoiseGaborSurface,

        // Unclassified
        E_InverseFieldDivide,
        E_PolynomialDeformationMap
    }; // End enum EFunctorType declaration
*/

} // Close namespace core

} // Close namespace expressive

#endif // CORE_ENUM_FUNCTOR_TYPE_H_
