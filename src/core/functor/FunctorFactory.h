/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: FunctorFactoryT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for the FunctorFactory.
                This class can build a new functor from the given type.
                The functor type must have been registered at compile time.

                Can also check equality between 2 functors.

                // TODO : refactor the map structure using MapFunctorTypeTreeT.h
                

   Platform Dependencies: None
*/

#pragma once
#ifndef CORE_FUNCTOR_FACTORY_T_H_
#define CORE_FUNCTOR_FACTORY_T_H_

// core dependencies
#include <core/CoreRequired.h>
#include <ork/core/Logger.h>
    // functor
    #include <core/functor/Functor.h>
    #include <core/functor/FunctorTypeMapT.h>

#include <tinyxml/tinyxml.h>

namespace expressive {

namespace core {

/*! \brief Factory class for Functors.
 *  
 *  This class can build a new functor from the given type.
 *  The functor type must have been registered at compile time.
 *
 */
class CORE_API FunctorFactory
{
public:
    typedef std::shared_ptr<Functor> (*FunctorBuilderFunction)();

    typedef core::FunctorTypeMapT<FunctorBuilderFunction> MapperType;

    static FunctorFactory& GetInstance();

    std::shared_ptr<Functor> CreateDefaultFunctor();

    /** \brief Create a new Functor with the given kernel
     *
     *    \param ker_type A reference to a type tree representing the type of which we want the returned object to be
     *
     *  \return Return a default instance of the given type functor (or throw an exception if it has not been registered).
     *          This mean that the functor parameter should be initialized afterward.
     */
    std::shared_ptr<Functor> CreateFunctor(const FunctorTypeTree& functor_type);

    /** \brief Create a new Functor from a TiXmlElement.
     *         Note that this function parse two time the TiXmlElement, the first one to create
     *         a FunctorTypeTree and the second time (after the kernel creation using CreateFunctor)
     *         to initialize all its parameters.
     * \param const TiXmlElement*
     * \return
     */
    std::shared_ptr<Functor> CreateFunctorFromXml(const TiXmlElement* functor_xml);

    /** \brief Generic function with the same signature for all functors.
    *          This wrapping is necessary to be stored in the mapper.
    *
    *   \return An allocated instance of a functor of type TFunctor (template argument).
    */
    template<typename TFunctor>
    void RegisterFunctor(FunctorBuilderFunction f);

    void SetDefaultFunctor(FunctorTypeTree default_functor_name);

public :
    template <class TFunctor>
    class Type
    {
    public:
        static std::shared_ptr<Functor> ctor ()
        {
            return std::make_shared<TFunctor>();
        }

        Type()
        {
            FunctorFactory::GetInstance().RegisterFunctor<TFunctor>(ctor);
        }
    };

private:
    MapperType types;

    std::map<std::string, std::vector<std::string> > functor_name_to_subfuncotr_;

    FunctorTypeTree default_functor_name_;

    /** \brief Create a FunctorTypeTree from a TiXmlElement. */
    void RecursiveBuildTypeTree(const TiXmlElement* functor_xml, FunctorTypeTree& functor_type);
};

template<typename TFunctor>
void FunctorFactory::RegisterFunctor(FunctorBuilderFunction f)
{
    const FunctorTypeTree& type_tree = TFunctor::StaticType();

    if(types.insert(type_tree, f))
    {
        functor_name_to_subfuncotr_[TFunctor::StaticName()] = TFunctor::StaticSubFunctorXmlName();
    }
    else
    {
        ork::Logger::ERROR_LOGGER->logf("CORE", "FunctorFactory: Kernel already registered : %s", type_tree.type_.c_str());
        throw std::exception();
    }
}


} // Close namespace core

} // Close namespace expressive

#endif // CORE_FUNCTOR_FACTORY_T_H_
