#include <core/functor/FunctorFactory.h>

#include <ork/core/Logger.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

using namespace ork;

namespace expressive {

namespace core {

FunctorFactory &FunctorFactory::GetInstance()
{
    static FunctorFactory INSTANCE = FunctorFactory();
    return INSTANCE;
}

void FunctorFactory::SetDefaultFunctor(FunctorTypeTree default_functor_name)
{
    default_functor_name_ = default_functor_name;
}

std::shared_ptr<Functor> FunctorFactory::CreateDefaultFunctor()
{
    return CreateFunctor(default_functor_name_);
}

std::shared_ptr<Functor> FunctorFactory::CreateFunctor(const FunctorTypeTree& functor_type)
{
    FunctorBuilderFunction builder = nullptr;
    if(types.find(functor_type,builder))
    {
        return builder();
    }
    else
    {
        Logger::ERROR_LOGGER->log("RESOURCE", "FunctorFactory : functor not registered");
        throw std::exception();
//        return nullptr; // unreachable
    }
}


std::shared_ptr<Functor> FunctorFactory::CreateFunctorFromXml(const TiXmlElement* functor_xml)
{
    // Create the FunctorTypeTree : first parsing of xml tree
    FunctorTypeTree functor_type;
    RecursiveBuildTypeTree(functor_xml, functor_type);

    // Create the kernel
    std::shared_ptr<Functor> functor = CreateFunctor(functor_type);

    // Initialize it : second parsing of xml tree
    functor->InitFromXml(functor_xml);

    return functor;
}

void FunctorFactory::RecursiveBuildTypeTree(const TiXmlElement* functor_xml, FunctorTypeTree& functor_type)
{
    functor_type.type_ = functor_xml->Value();

    auto iter = functor_name_to_subfuncotr_.find(functor_type.type_);
    if(iter != functor_name_to_subfuncotr_.end())
    {
        const TiXmlElement* subtype_xml;
        for(auto &str : iter->second)
        {
            subtype_xml = TraitsXmlQuery::QueryFirstChildElement("FunctorFactory", functor_xml, str.c_str());
            subtype_xml =  TraitsXmlQuery::QueryFirstChildElement("FunctorFactory", subtype_xml);

            functor_type.subtypes_.push_back(FunctorTypeTree()); //avoid useless copy ...
            RecursiveBuildTypeTree(subtype_xml, functor_type.subtypes_.back());
        }
    }
    else
    {
        ork::Logger::ERROR_LOGGER->logf("CORE", "FunctorFactory: Couldn't find functor for %s", functor_type.type_.c_str());
        throw std::exception();
    }
}


//FunctorTypeTree FunctorFactory::GetAssociatedStandardKernel(const FunctorTypeTree &kernel_type)
//{
//    FunctorTypeTree standard_type;
//    if(homothetic_kernel_to_standard_.find(kernel_type,standard_type))
//    {
//        return standard_type;
//    }
//    else
//    {
//        Logger::ERROR_LOGGER->log("RESOURCE", "FunctorFactory : kernel mapping to standard kernel not registered");
//        throw std::exception();
//        return nullptr;
//    }
//}


} // Close namespace core

} // Close namespace expressive

