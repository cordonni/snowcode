#include "core/functor/EnumFunctorType.h"

namespace expressive {

namespace core {

FunctorTypeTree::FunctorTypeTree() {}
FunctorTypeTree::FunctorTypeTree(std::string type) // explicit is here to prevent automatic conversion
    : type_(type)
{

}

FunctorTypeTree::FunctorTypeTree(std::string type, const std::vector<FunctorTypeTree>& subtypes)
    : type_(type),
      subtypes_(subtypes)
{

}

bool FunctorTypeTree::operator==(const FunctorTypeTree& other) const
{
    if(type_ == other.type_ && subtypes_.size() == other.subtypes_.size())
    {
        bool res = true;
        for(unsigned int i = 0; i<subtypes_.size(); ++i)
        {
            res &= (subtypes_[i] == other.subtypes_[i]);
        }
        return res;
    }
    else
    {
        return false;
    }
}

bool FunctorTypeTree::operator != (const FunctorTypeTree& other) const
{
    return !((*this) == other);
}

}

}
