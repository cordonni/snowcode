#include "core/functor/Functor.h"


namespace expressive {

namespace core {

Functor::~Functor()
{
}

void Functor::InitFromXml(const TiXmlElement* /*element*/)
{
}

const std::vector<std::string>& Functor::StaticSubFunctorXmlName()
{
#ifdef _MSC_VER
    static std::vector<std::string> subfunctor_xml_names;
#else
    static std::vector<std::string> subfunctor_xml_names = {};
#endif
    return subfunctor_xml_names;//std::vector<std::string>();
}

TiXmlElement * Functor::ToXml() const
{
    TiXmlElement *element_base = new TiXmlElement( Name() );

    AddXmlAttributes(element_base);

    return element_base;
}

void Functor::AddXmlAttributes(TiXmlElement * /*element*/) const
{
}

}

}
