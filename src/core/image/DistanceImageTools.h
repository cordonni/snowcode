/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: DistanceImageTools.h

   Language: C++

   License: Convol license

   \author: Probably Adrien Bernhardt and/or Adeline Pihuit
   \author: Maxime Quiblier (Adapted and enriched for Convol, 2011)
   E-Mail:  maxime.quiblier@inrialpes.fr

   Description: Header for a the namespace DistanceImageTools.
                This namespace defines functions related to computing
                and processing Distance Images.
                A "Distance Image" is an image built from a bicolor image.
                Each pixel inside the painted region stores the distance to the border of the painted region.

   Platform Dependencies: None
*/

// Convol-Tools dependencies
    // Image
    #include <core/image/ImageT.h>

#pragma once
#ifndef CONVOL_DISTANCE_IMAGE_TOOLS_H_
#define CONVOL_DISTANCE_IMAGE_TOOLS_H_

namespace expressive {

namespace core {

class DistanceImageTools
{
public:
/*! \brief This function builds a "distance image".
*
*          Ref : Matisse : Painting 2D Regions for Modeling Free-Form Shapes - A.Bernhardt, A.Pihuit, M.P.Cani, L.Barthe
*                Eurographics Workshop on Sketch-Based Interfaces and Modeling (2008)
*                paragraph 2.2, see Weighted Distance Transform (WDT)
*
*          Basically apply the filter :  c2  c1  c2
*                                        c1  0   c1
*                                        c2  c1  c2
*
*  \param c1 Coefficient for the metric mask (in Ref, 3)
*  \param c2 Coefficient for the metric mask (in Ref, 4)
*  \param source The source image (should be bicolored).
*  \param res The resulting "distance image".
*
* \tparam res_value_type Value type for the source image. Will be considered bicolored : res_value_type(0) or anything else.
* \tparam src_value_type Value to store the distance (in Ref, int is used)
*
*/
template<typename res_value_type, typename src_value_type>
static void BuildDistanceImage(res_value_type c1, res_value_type c2,
                               const ImageT<src_value_type>& source,
                               src_value_type uncolored,    // color considered as "uncolored", ie the pixel is not filled
                               ImageT<res_value_type>& res)
{
    assert(source.width() == res.width() && source.height() == res.height());

    unsigned int width = source.width();
    unsigned int height = source.height();

    // SuperNew Code
    for (unsigned long i = 0; i<width; ++i)
    {
        res(i,0) = static_cast<res_value_type>(0);
    }
    for (unsigned long j = 1; j<height-1; ++j)
    {
        res(0,j) = static_cast<res_value_type>(0);
        for (unsigned long i = 1; i<width-1; ++i)
        {
            res(i,j) = static_cast<res_value_type>(0);
            res_value_type dist = static_cast<res_value_type>(0);
            if (source(i,j) != uncolored)
            {
                dist = std::min(res(i-1,j-1) + c2, res(i,j-1) + c1);
                dist = std::min(dist, res(i-1,j) + c1);
                dist = std::min(dist, res(i+1,j-1) + c2);
                res(i,j) = dist;
            }
        }
        res(width-1,j) = static_cast<res_value_type>(0);
    }
    for (unsigned long i = 0; i<width; ++i)
    {
        res(i,height-1) = static_cast<res_value_type>(0);
    }


    for (unsigned long j = height-2; j>0; --j)
    {
        for (unsigned long i = width-2; i>0; --i)
        {
            res_value_type dist = res(i,j);
            if (source(i,j) != uncolored)
            {
                dist = std::min(dist, res(i+1,j+1) + c2);
                dist = std::min(dist, res(i,j+1) + c1);
                dist = std::min(dist, res(i-1,j+1) + c2);
                res(i,j) = std::min(dist, res(i+1,j) + c1);
            }
        }
    }

    /* // Old Code : It seemed to me it was unsafe
    unsigned int size = width*height;

    const src_value_type* ptrPix = source.tab();
    res_value_type* ptrRes = res.nonconst_tab();

    for (unsigned long i = 0; i<size; ++i)
    {
        *ptrRes = static_cast<res_value_type>(0);
        if (*ptrPix != uncolored)
        {
            res_value_type* ptrResLocal = ptrRes-width;
            res_value_type dist = static_cast<res_value_type>(0);

            dist = std::min(*(ptrResLocal-1) + c2, *(ptrResLocal) + c1);
            dist = std::min(dist, *(ptrRes-1) + c1);
            *ptrRes = std::min(dist, *(ptrResLocal+1) + c2);
        }
        ++ptrPix;
        ++ptrRes;
    }
    ptrPix = source.tab() + size;
    ptrRes = res.nonconst_tab() + size;

    for (unsigned long i = 0; i<size; ++i)
    {
        --ptrPix;
        --ptrRes;
        if (*ptrPix != uncolored)
        {
            res_value_type* ptrResLocal = ptrRes+width;
            res_value_type dist = *ptrRes;
            dist = std::min(dist, *(ptrResLocal+1) + c2);
            dist = std::min(dist, *ptrResLocal + c1);
            dist = std::min(dist, *(ptrResLocal-1) + c2);
            *ptrRes = std::min(dist, *(ptrRes+1) + c1);
        }
    }
    */
}
/*! \brief This function builds a "distance image".
*
*          Ref : Adeline Pihuit filter, see her thesis.
*
*          Basically apply the filter :
*                       c3      c3
*                   c3  c2  c1  c2  c3
*                       c1  0   c1
*                   c3  c2  c1  c2  c3
*                       c3      c3
*
*  \param c1 Coefficient for the metric mask (in Ref, 5)
*  \param c2 Coefficient for the metric mask (in Ref, 7)
*  \param c3 Coefficient for the metric mask (in Ref, 11)
*  \param source The source image (should be bicolored).
*  \param res The resulting "distance image".
*
* \tparam res_value_type Value type for the source image. Will be considered bicolored : res_value_type(0) or anything else.
* \tparam src_value_type Value to store the distance (in Ref, int is used)
*
*/
template<typename res_value_type, typename src_value_type>
static void BuildDistanceImage(res_value_type c1, res_value_type c2, res_value_type c3,
                               const ImageT<src_value_type>& source,
                               src_value_type uncolored,    // color considered as "uncolored", ie the pixel is not filled
                               ImageT<res_value_type>& res)
{
    assert(source.width() == res.width() && source.height() == res.height());

    unsigned int width = source.width();
    unsigned int height = source.height();

    // SuperNew Code
    for (unsigned long i = 0; i<width; ++i)
    {
        res(i,0) = static_cast<res_value_type>(0);
        res(i,1) = static_cast<res_value_type>(0);
    }

    for (unsigned long j = 2; j<height-2; ++j)
    {
        res(0,j) = static_cast<res_value_type>(0);
        res(1,j) = static_cast<res_value_type>(0);
        for (unsigned long i = 2; i<width-2; ++i)
        {
            res(i,j) = static_cast<res_value_type>(0);
            res_value_type dist = static_cast<res_value_type>(0);
            if (source(i,j) != uncolored)
            {
                dist = std::min(res(i-1,j-1) + c2, res(i,j-1) + c1);
                dist = std::min(dist, res(i-1,j) + c1);
                dist = std::min(dist, res(i+1,j-1) + c2);
                dist = std::min(dist, res(i+2,j-1) + c3);
                dist = std::min(dist, res(i+1,j-2) + c3);
                dist = std::min(dist, res(i-2,j-1) + c3);
                dist = std::min(dist, res(i-1,j-2) + c3);
                res(i,j) = dist;
            }
        }
        res(width-2,j) = static_cast<res_value_type>(0);
        res(width-1,j) = static_cast<res_value_type>(0);
    }
    for (unsigned long i = 0; i<width; ++i)
    {
        res(i,height-2) = static_cast<res_value_type>(0);
        res(i,height-1) = static_cast<res_value_type>(0);
    }

    for (unsigned long j = height-3; j>1; --j)
    {
        for (unsigned long i = width-3; i>1; --i)
        {
            res_value_type dist = res(i,j);
            if (source(i,j) != uncolored)
            {
                dist = std::min(dist, res(i+1,j+1) + c2);
                dist = std::min(dist, res(i,j+1) + c1);
                dist = std::min(dist, res(i-1,j+1) + c2);
                dist = std::min(dist, res(i+1,j) + c1);
                dist = std::min(dist, res(i+2,j+1) + c3);
                dist = std::min(dist, res(i+1,j+2) + c3);
                dist = std::min(dist, res(i-2,j+1) + c3);
                dist = std::min(dist, res(i-1,j+2) + c3);
                res(i,j) = dist;
            }
        }
    }
}

}; // End of class DistanceImageTools Declaration

} // Close namespace core

} // Close namespace expressive

#endif // CONVOL_DISTANCE_IMAGE_TOOLS_H_
