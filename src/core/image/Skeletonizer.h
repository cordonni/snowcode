/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SKELETONIZER_H
#define SKELETONIZER_H

#include <core/geometry/Segment.h>
#include <core/graph/Graph.h>
#include <core/graph/Voronoi.h>
#include <core/image/ImageT.h>

namespace expressive
{

namespace core
{

/**
 * @brief The Skeletonizer class creates a skeleton (a graph) from a 2D image.
 */
class CORE_API Skeletonizer
{
public:
    typedef uint8 SkeletonPixel;
    typedef ImageT<SkeletonPixel> SkeletonImage;

    enum ESkeletonizationType {
        E_SkeletonImageTypeUndefined,
        E_SkeletonImageTypeBasic1,
        E_SkeletonImageTypeHall89,
        E_SkeletonImageTypeEckhardtMaderlechner93,
        E_SkeletonImageTypeVoronoi
    };

    struct PixelInfo
    {
        PixelInfo(): pos(Point2::Zero()), ptr(nullptr) {}
        // Position of the pixel in the picture. Since pixels are squares, the center of a pixel
        // which is what we call "position" here, is not a couple of integer.
        // Example : pixel in picture are numeroted (x,y) integers with 0<x<width and 0<y<height
        //           The corresponding position pos in PixelInfo will be : (x+0.5,y+0.5)
        Point2 pos;
        SkeletonPixel* ptr; // This is very important to have access to neighborhood and to have a unique identifier for a given skel_imgeton point
    };

    typedef WeightedPointT<Point2> WeightedPoint2D;
    typedef SegmentT<WeightedPoint2D> WeightedSegment2D;

    /** Retrieves the skeleton of a given image, using provided method.
     *  Default method is voronoi technique, the one that should give us the most interesting results.
     **/
    template<typename value_type>
    static void ExtractSkeleton(ImageT<value_type> &img, ESkeletonizationType method = E_SkeletonImageTypeVoronoi)
    {
//        Color bg = Color(0.0, 0.0, 0.0, 0.0);
//        ImageT<Color> contours = img->Contours(bg, bg);


//        std::vector<std::list<PixelInfo> > branches;

    }

    /**
      * Skeleton extraction based on Voronoi technique.
      * First, get contours, then retrieve them as a list of pixels, them use them as voronoi input.
      * The internal skeleton (not connected to external lines) should be the final skeleton.
      * @param the simplified contours as a list of list of points.
      */
    static std::shared_ptr<Graph2D> ExtractSkeletonVoronoi(std::vector<std::vector<Point2> > branches, std::vector<core::Segment2D> &res);

    // Build branches as a vector of pixels list.
    // Set the first bit of all processed pixels (all pixels actually) to 0.
    // Set the second bit of extreme pixels (begin and end of each branch) to 1.
    // Note : We don't want to keep 0-length branches, ie branches with only 1 pixel.
    //        branches WILL NOT be erased. Branches found will be added to the existing vector.
    template<typename value_type>
    static void ExtractLines(std::vector<std::list<PixelInfo> *> &res, ImageT<value_type> &img)
    {
        SkeletonImage branch_mask(img.width(), img.height());
        img.ExtractMask(branch_mask, Color(0, 0, 0, 0));
        branch_mask.EraseBorders(1, 1, 0);

        CreateLines(res, branch_mask);
    }

    static void CreateLines(std::vector<std::list<PixelInfo> *> &res_branches, SkeletonImage &skel_img);

    // Recursive function that walks through branches, create them when necessary and fill them.
    static void Walk(PixelInfo pixel, std::vector<std::list<PixelInfo> *>& branches, unsigned int index_branch,
        SkeletonImage& skel_img);

    /**
     * Encode the vertex neighborhood of the pixel.
     * Each bit corresponds to whether or not a pixel has been marked as a vertex.
     * This information is stored in the second bit of SkeletonPixel
     * The following table describes the place of each bit in the neighborhood
     *     ---w--->
     *  |  |0|1|2|
     *  h  |7| |3|
     *  |  |6|5|4|
     *  V
     *
     *  WARNING : do not call this function on border pixels.
     *
     * @param ptr_pix a pointer towards the neighborhood wanted pixel
     * @return The encoded neighborhood
     */
    static uint8 GetVertexNeighborhood(const SkeletonImage& skel_img, SkeletonPixel const* ptr_pix);

    /**
     * Check surrounding pixels of pixel to see if they are marked as vertices and if yes return it.
     *
     * @param pixel the pixel to check. I guess in any use case of this function pixel will be in branch already.
     * @param pixel_previous_ptr The vertex pixels will be compared to this one since we don't want to link to this one.
     * @param skel_img : the corresponding SkeletonImage
     *
     * @return if a vertex has been found that satisfies the conditions return it. Otherwise return pixel.
     */
    static PixelInfo CheckVertexNeighborhoodAndLink( // list<PixelInfo>* branch, // now useless I guess.
                                                            SkeletonImage& skel_img,
                                                            PixelInfo& pixel,
                                                            SkeletonPixel const* pixel_previous_ptr);

    /** Find the first pixel with a neighborhood non equal zero.
     *
     * It also sets other pixels to 0.
     *
     * @param result a reference to the pixel found
     * @param offset gives the pixel at which we begin the search in the image.
     *               This can be useful when you search for a pixel, then do something,
     *               then want to look for the "next" pixel you can found without reprocessing the beginning.
     *               If it's unclear, you can always use 0 and it will be fine.
     * @return true if pixel found, false otherwise
     */
    static bool FindPixelWithNeighbors(unsigned long& offset, PixelInfo& result, SkeletonImage& skel_img);

    /**
     * Encode the neighborhood of the pixel.
     * Each bit corresponds to whether or not a pixel contains informations in
     * a determined place.
     * The following table describes the place of each bit in the neighborhood
     *     ---w--->
     *  |  |0|1|2|
     *  h  |7| |3|
     *  |  |6|5|4|
     *  V
     * @param ptrPix a pointer towards the neighborhood wanted pixel
     * @return The encoded neighborhood
     */
    static uint8 GetCurrentNeighborhood(SkeletonPixel* ptrPix, SkeletonImage &img);

    /**
     * ???
     *
     * @author Adeline_21/01/09
     *
     * @param ???
     * @return ???
     */
    static uint8 GetNbNeighbours(const uint8 neighbours);

    /**
     * @brief FilterSkeleton filters segments resulting from a skeleton search : Removes every
     *                       segment that appears to be outside the given mask. Mostly useful
     *                       voronoi filtering.
     * @param mask
     * @param segments
     */
    static void FilterSkeleton(SkeletonImage &mask, std::vector<core::Segment2D> &segments);
    static std::shared_ptr<WeightedGraph2D> CreateFilteredGraph(ImageT<float> distances, std::vector<core::Segment2D> &segments);

};

}

}

#endif // SKELETONIZER_H
