/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ImageT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail:  maxime.quiblier@inrialpes.fr

   Description: Header for a the scalar field super-class.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_IMAGE_T_H_
#define CONVOL_IMAGE_T_H_

// Convol dependencies
    // Tools
    #include<core/utils/Array2DT.h>
//    #include<Convol/include/tools/Interpolation.h>

#include<cstring>
#include<vector>
#include<list>
#include<ork/core/Object.h>

namespace expressive
{

namespace core
{

/*! \brief Image class that can be specialized by anything.
*
*   
*/
template< typename value_type>
class ImageT : public ork::Object, public Array2DT<value_type>
{
public:
    friend class ImageTools;

    typedef Array2DT<value_type> Base;

    ImageT(unsigned int width, unsigned int height)
        : ork::Object("Image"), Base(width,height)
    {
    }


    void EraseBorders(unsigned int nx, unsigned int ny,value_type val)
    {
        assert(ny < this->height_/2 && nx < this->width_/2);
        // Top rows
        value_type* ptrPix = this->tab_;
        for (unsigned int i = 0; i<nx; ++i)
        {
            for ( uint j = 0; j < this->width_; ++j)
            {
                *(ptrPix++) = val;
            }
        }
        // Bottom rows
        ptrPix = this->tab_ + this->width_-1 + this->width_*(this->height_-1);
        for (unsigned int i = 0; i<nx; ++i)
        {
            for (uint j = 0; j < this->width_; ++j)
            {
                *(ptrPix--) = val;
            }
        }
        // Left columns
        for (unsigned int  i = ny; i < this->height_-ny; ++i)
        {
            ptrPix = this->tab_+ i*this->width_;
            for (unsigned int  j=0;  j<nx; ++j)
            {
                *(ptrPix++) = val;
            }
        }
        // Right columns
        for (unsigned int  i = ny; i < this->height_-ny; ++i)
        {
            ptrPix = this->tab_ + this->width_-1 + i*this->width_;
            for (unsigned int  j=0; j<nx; ++j)
            {
                *(ptrPix--) = val;
            }
        }
    }

    ImageT& CopyDataFrom(const ImageT& other)
    {
        std::memcpy(this->tab_, other.tab_, this->width_*this->height_*sizeof(value_type));
        return *this;
    }

    template<typename Scalar>
    value_type FloorInterp(Scalar x, Scalar y) const
    {
        return (*this)((unsigned int) x, (unsigned int) y);
    }

    template<typename Scalar>
    value_type BilinearInterp(Scalar x, Scalar y) const
    {
        //TODO : @todo : to be implemented
        UNUSED(x); UNUSED(y);
        assert(false); // not yet implemented !
    }
    
    template<typename Scalar>
    value_type BicubicInterp(Scalar x, Scalar y) const
    {
        //TODO : @todo : to be implemented
        UNUSED(x); UNUSED(y);
        assert(false); // not yet implemented !
    }

    ImageT Contours(value_type uncolored, value_type bg_color)
    {
        ImageT<value_type> image_aux(*this);

        image_aux.EraseBorders(1,1,bg_color);

        value_type* ptrPix = Base::tab_+(Base::width_-2);
        value_type* ptrRes = image_aux.tab_+(Base::width_-2);

        for (unsigned int i = 1; i < Base::height_-1; ++i)
        {
            ptrPix+=2;
            ptrRes+=2;
            for (uint j = 1; j < Base::width_ -1; ++j)
            {
                ++ptrPix;
                ++ptrRes;

                value_type* ptrPixLocal = ptrPix;
                if ( *ptrPixLocal == uncolored) {
                    *ptrRes = bg_color;
                    continue;
                }
                if (*(ptrPixLocal+1) == uncolored || *(ptrPixLocal-1) == uncolored)
                {
                    *ptrRes = *ptrPix;
                    continue;
                }
                ptrPixLocal= ptrPix+Base::width_;
                if ( *(ptrPixLocal+1) == uncolored || *ptrPixLocal == uncolored || *(ptrPixLocal-1) == uncolored)
                {
                    *ptrRes = *ptrPix;
                    continue;
                }
                ptrPixLocal= ptrPix-Base::width_;
                if ( *(ptrPixLocal+1) == uncolored || *ptrPixLocal == uncolored || *(ptrPixLocal-1) == uncolored )
                {
                    *ptrRes = *ptrPix;
                    continue;
                }
                if ((i == 1 || i == Base::height_-2 || j == 1 || j == Base::width_-2) && (*ptrPixLocal != uncolored))
                {
                    *ptrRes = *ptrPix;
                    continue;
                }

                *ptrRes = bg_color;
            }
        }

        return image_aux;
    }

    void ExtractMask(ImageT<uint8> &res, value_type uncolored)
    {
        for (uint i = 0; i < Base::width(); ++i) {
            for (uint j = 0; j < Base::height(); ++j) {
                if ((*this)(i, j) != uncolored) {
                    res(i, j) = 1;
                } else {
                    res(i, j) = 0;
                }
            }
        }
    }

protected:
    ImageT() : Object("Image"){}
    void swap(ork::ptr<ImageT<value_type>> oth)
    {
        std::swap(Base::width_, oth->width_);
        std::swap(Base::height_, oth->height_);
        std::swap(Base::tab_, oth->tab_);
    }

}; // End class ImageT declaration

// typedef for usual images
using ImageRGBA = ImageT<Vector4uc>;

} // namespace core

} // namespace expressive

#endif // CONVOL_IMAGE_T_H_
