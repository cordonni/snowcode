
#include <core/image/Skeletonizer.h>

using namespace std;

namespace expressive
{

namespace core
{

std::shared_ptr<Graph2D> Skeletonizer::ExtractSkeletonVoronoi(std::vector<std::vector<Point2> > branches, std::vector<core::Segment2D> &res)
{
    VoronoiDiagram vd;
    for (unsigned int i = 0; i < branches.size(); ++i) {
//        cout << "Extract branch ::: " << endl;
//        cout << branches[i][0][0] << "::::" << branches[i][0][1] << endl;
//        cout << branches[i][branches[i].size() - 1][0] << "::::" << branches[i][branches[i].size() - 1][1] << endl;
        for (unsigned int j = 1; j < branches[i].size(); ++j) {
            vd.addSegment(PointPrimitive2D(branches[i][j - 1]), PointPrimitive2D(branches[i][j]));
        }
//        for (unsigned int j = 0; j < branches[i].size(); ++j) {
//            vd.addPoint(PointPrimitive2D(branches[i][j]));
//        }
    }

    vd.construct(res);

    return nullptr;
}

void Skeletonizer::CreateLines(std::vector<std::list<PixelInfo> *> &res_branches, SkeletonImage &skel_img)
{
    // A brand new vector to put raw branches.
    // This vector will be post-processed before filling branches.
    std::vector<std::list<PixelInfo> *> raw_branches;

    unsigned long offset = 0;
    PixelInfo first_pix;
    while(FindPixelWithNeighbors(offset, first_pix, skel_img))
    {
        // Create the new branch
        raw_branches.push_back(new std::list<PixelInfo>());
        // Set the pixel to be a handle
        *(first_pix.ptr) |= 2;
        Walk(first_pix, raw_branches, (unsigned int) raw_branches.size()-1, skel_img);
    }

    // Delete branches with less than 1 pixel and transfer other branches into res_branches.
    for(unsigned int i = 0; i<raw_branches.size(); ++i)
    {
        if(raw_branches[i]->size()>2)
        {
            res_branches.push_back(raw_branches[i]);
        }
    }
}

// Recursive function that walks through branches, create them when necessary and fill them.
void Skeletonizer::Walk(PixelInfo pixel, std::vector<std::list<PixelInfo> *>& branches, unsigned int index_branch,
    SkeletonImage& skel_img)
{
    branches[index_branch]->push_back(pixel);

    unsigned int width = skel_img.width();

    // erase current pixel
    *(pixel.ptr) &= 254;
    uint8* ptr_pix = pixel.ptr;

    uint8 neighbours = GetCurrentNeighborhood(ptr_pix, skel_img);
    unsigned int n_neighbors = GetNbNeighbours(neighbours);
    if (n_neighbors != 0)
    {
        // Find the colored neighbors and walk along through it.
        uint8* ptr_neighbour;
        PixelInfo pI;
        std::list<PixelInfo> pixel_for_new_branches;
        // for the neighbours of the 4-neighborhood
        // See getCurrentNeighborhood description for more details on bit meaning
        if ((neighbours & 2) )
        {
            ptr_neighbour = ptr_pix-width;
            pI.pos = Point2(pixel.pos[0], pixel.pos[1]-1.0);
            pI.ptr = ptr_neighbour;
            pixel_for_new_branches.push_back(pI);
        }
        if ((neighbours & 8) )
        {
            ptr_neighbour = ptr_pix+1;
            pI.pos = Point2(pixel.pos[0]+1.0, pixel.pos[1]);
            pI.ptr = ptr_neighbour;
            pixel_for_new_branches.push_back(pI);
        }
        if ((neighbours & 32) )
        {
            ptr_neighbour = ptr_pix+width;
            pI.pos = Point2(pixel.pos[0], pixel.pos[1]+1.0);
            pI.ptr = ptr_neighbour;
            pixel_for_new_branches.push_back(pI);
        }
        if ((neighbours & 128) )
        {
            ptr_neighbour = ptr_pix-1;
            pI.pos = Point2(pixel.pos[0]-1.0, pixel.pos[1]);
            pI.ptr = ptr_neighbour;
            pixel_for_new_branches.push_back(pI);
        }
        // for the neighbours of the 8-neighborhood
        // Since there are other pixels colored, we must check adjacent pixels
        // before creating a branch. Indeed, we create a branch only if the diagonal pixel is "alone"
        // See getCurrentNeighborhood description for more details on bit meaning
        if ((neighbours & 1) && !(((neighbours>>7) & 1)^((neighbours>>1) & 1)) )
        {
            ptr_neighbour = ptr_pix-(width+1);
            pI.pos = Point2(pixel.pos[0]-1.0, pixel.pos[1]-1.0);
            pI.ptr = ptr_neighbour;
            pixel_for_new_branches.push_back(pI);
        }
        if ((neighbours & 4) && !(((neighbours>>1) & 1)^((neighbours>>3) & 1)) )
        {
            ptr_neighbour = ptr_pix-(width-1);
            pI.pos = Point2(pixel.pos[0]+1.0, pixel.pos[1]-1.0);
            pI.ptr = ptr_neighbour;
            pixel_for_new_branches.push_back(pI);
        }
        if ((neighbours & 16) && !(((neighbours>>3) & 1)^((neighbours>>5) & 1)) )
        {
            ptr_neighbour = ptr_pix+(width+1);
            pI.pos = Point2(pixel.pos[0]+1.0, pixel.pos[1]+1.0);
            pI.ptr = ptr_neighbour;
            pixel_for_new_branches.push_back(pI);
        }
        if ((neighbours & 64) && !(((neighbours>>5) & 1)^((neighbours>>7) & 1)) )
        {
            ptr_neighbour = ptr_pix+(width-1);
            pI.pos = Point2(pixel.pos[0]-1.0, pixel.pos[1]+1.0);
            pI.ptr = ptr_neighbour;
            pixel_for_new_branches.push_back(pI);
        }

        if(pixel_for_new_branches.size() > 1)
        {
            //// DEPRECATED by Maxime Quiblier
            //   Why? This solves problems but creates others. I thought problems solved could be more easily solved later than
            //        problems created.
            // First we erase all pixels. This is done to ensure that the new starting branches will not try to start a branch at some of the pixels
            // we already have selected to start new branches.
            //
            //for(list<PixelInfo>::iterator it = pixel_for_new_branches.begin(); it != pixel_for_new_branches.end(); ++it)
            //{
            //    *((*it).ptr) &= 254;
            //}
            // set the pixel to be a handle
            *(pixel.ptr) |= 2;
            // Create all new branch beginning with pixels stored in
            for(std::list<PixelInfo>::iterator it = pixel_for_new_branches.begin(); it != pixel_for_new_branches.end(); ++it)
            {
                if( (*(it->ptr)) & 1)   // check that the pixel has not been shut down by another branch or a loop.
                {
                    // push the pixel in the new branch
                    branches.push_back(new std::list<PixelInfo>());
                    branches.back()->push_front(pixel);
                    Walk(*it, branches, (unsigned int) branches.size()-1, skel_img);
                }
            }
        } else {
            Walk(pI, branches, index_branch, skel_img);
        }
    }
    else
    {
        // We are at the "end" of a branch. We must check vertices around in case we should "close" a loop
        std::list<PixelInfo>* branch =  branches[index_branch];
        if(branch->size() != 1)
        {
            PixelInfo checked_pi = CheckVertexNeighborhoodAndLink( //branch ,
                                                                  skel_img, pixel, (--(--(branch->end())))->ptr );

            if(checked_pi.ptr == pixel.ptr)
            {
                // set the pixel to be a handle
                // Indeed it is the end of a branch
                *(pixel.ptr) |= 2;
            }
            else
            {
                branch->push_back(checked_pi);
                PixelInfo b = *(branch->begin());
                if (checked_pi.ptr != b.ptr && (checked_pi.pos - b.pos).squaredNorm() < 2) {
                    branch->push_back(b);
                }
            }
        }
    }
}

/**
 * Encode the vertex neighborhood of the pixel.
 * Each bit corresponds to whether or not a pixel has been marked as a vertex.
 * This information is stored in the second bit of SkeletonPixel
 * The following table describes the place of each bit in the neighborhood
 *     ---w--->
 *  |  |0|1|2|
 *  h  |7| |3|
 *  |  |6|5|4|
 *  V
 *
 *  WARNING : do not call this function on border pixels.
 *
 * @param ptr_pix a pointer towards the neighborhood wanted pixel
 * @return The encoded neighborhood
 */
uint8 Skeletonizer::GetVertexNeighborhood(const SkeletonImage& skel_img, SkeletonPixel const* ptr_pix)
{
    // @todo put an assert loop to check that ptr_pix is not a border pixel
    assert(ptr_pix>=skel_img.tab() && ptr_pix<skel_img.tab()+skel_img.width()*skel_img.height());
    // encode the vertex handle neighborhood of the pixel
    return (  ((*(ptr_pix - 1) & 2)                  << 7) | ((*(ptr_pix + 1) & 2)                  << 3)
            | ((*(ptr_pix + skel_img.width()) & 2)       << 5) | ((*(ptr_pix - skel_img.width()) & 2)       << 1)
            |  (*(ptr_pix - (skel_img.width() + 1)) & 2)       | ((*(ptr_pix - (skel_img.width() - 1)) & 2) << 2)
            | ((*(ptr_pix + (skel_img.width() - 1)) & 2) << 6) | ((*(ptr_pix + (skel_img.width() + 1)) & 2) << 4) );
}

/**
 * Check surrounding pixels of pixel to see if they are marked as vertices and if yes return it.
 *
 * @param pixel the pixel to check. I guess in any use case of this function pixel will be in branch already.
 * @param pixel_previous_ptr The vertex pixels will be compared to this one since we don't want to link to this one.
 * @param skel_img : the corresponding SkeletonImage
 *
 * @return if a vertex has been found that satisfies the conditions return it. Otherwise return pixel.
 */
Skeletonizer::PixelInfo Skeletonizer::CheckVertexNeighborhoodAndLink( // list<PixelInfo>* branch, // now useless I guess.
                                                        SkeletonImage& skel_img,
                                                        PixelInfo& pixel,
                                                        SkeletonPixel const* pixel_previous_ptr)
{
    unsigned int width = skel_img.width();

    PixelInfo res = pixel;
    SkeletonPixel* ptr_pix = pixel.ptr;

    uint8 vertex_neighbors = GetVertexNeighborhood(skel_img, ptr_pix);
    // We check that the neighbour found as a vertex is not the previous in the branch
    if(vertex_neighbors != 0)
    {
        SkeletonPixel* ptr_neighbour;
        // Note : OK I know the code is ugly... Think of it as a elsif ok? Have fun it's short anyway ;-)

        ptr_neighbour = ptr_pix-width-1;
        if (((*ptr_neighbour) & 2) && ptr_neighbour!=pixel_previous_ptr)
        {
            res.pos = Point2(pixel.pos[0]-1.0, pixel.pos[1]-1.0);
            res.ptr = ptr_neighbour;
        }
        // kind of special else if
        else {
        ptr_neighbour = ptr_pix-width;
        if (((*ptr_neighbour) & 2) && ptr_neighbour!=pixel_previous_ptr)
        {
            res.pos = Point2(pixel.pos[0], pixel.pos[1]-1.0);
            res.ptr = ptr_neighbour;
        }
        // kind of special else if
        else {
        ptr_neighbour = ptr_pix-width+1;
        if (((*ptr_neighbour) & 2) && ptr_neighbour!=pixel_previous_ptr)
        {
            res.pos = Point2(pixel.pos[0]+1.0, pixel.pos[1]-1.0);
            res.ptr = ptr_neighbour;
        }
        // kind of special else if
        else {
        ptr_neighbour = ptr_pix+1;
        if (((*ptr_neighbour) & 2) && ptr_neighbour!=pixel_previous_ptr)
        {
            res.pos = Point2(pixel.pos[0]+1.0, pixel.pos[1]);
            res.ptr = ptr_neighbour;
        }
        // kind of special else if
        else {
        ptr_neighbour = ptr_pix+(width+1);
        if (((*ptr_neighbour) & 2) && ptr_neighbour!=pixel_previous_ptr)
        {
            res.pos = Point2(pixel.pos[0]+1.0, pixel.pos[1]+1.0);
            res.ptr = ptr_neighbour;
        }
        // kind of special else if
        else {
        ptr_neighbour = ptr_pix+width;
        if (((*ptr_neighbour) & 2) && ptr_neighbour!=pixel_previous_ptr)
        {
            res.pos = Point2(pixel.pos[0], pixel.pos[1]+1.0);
            res.ptr = ptr_neighbour;
        }
        // kind of special else if
        else {
        ptr_neighbour = ptr_pix+width-1;
        if (((*ptr_neighbour) & 2) && ptr_neighbour!=pixel_previous_ptr)
        {
            res.pos = Point2(pixel.pos[0]-1.0, pixel.pos[1]+1.0);
            res.ptr = ptr_neighbour;
        }
        // kind of special else if
        else {
        ptr_neighbour = ptr_pix-1;
        if (((*ptr_neighbour) & 2) && ptr_neighbour!=pixel_previous_ptr)
        {
            res.pos = Point2(pixel.pos[0]-1.0, pixel.pos[1]);
            res.ptr = ptr_neighbour;
        }
        }}}}}}} // close all previous elsif
    }

    return res;
}

bool Skeletonizer::FindPixelWithNeighbors(unsigned long& offset, PixelInfo& result, SkeletonImage& skel_img)
{
    const unsigned long size = skel_img.width()*skel_img.height();
    const uint width = skel_img.width();

    // find a point on the skel_imgeton
    SkeletonPixel* ptrSkel = skel_img.nonconst_tab() + offset;
    uint n = 0;
    for( ;offset < size; ++ptrSkel, ++offset)
    {
        if ( *ptrSkel & 1 )
        {
            // we assume that the border pixels are uncolored so we don't need to check
            // if we are on the border or not
            if ((n = GetCurrentNeighborhood(ptrSkel, skel_img))==0)
                *ptrSkel = 0;
            else
                break;
        }
    }

    if(offset<size)
    {
        result.ptr = ptrSkel;
        unsigned long y = offset / width;
        unsigned long x = offset - y*width;
        result.pos = Point2( Scalar(x) + Scalar(0.5), Scalar(y) + Scalar(0.5));
        return true;
    }
    else
    {
        result.ptr = NULL;
        result.pos = Point2(0.0,0.0);
        return false;
    }
}

uint8 Skeletonizer::GetCurrentNeighborhood(SkeletonPixel* ptrPix, SkeletonImage &img)
{
    // encode the neighborhood of the pixel
    return (
           ((*(ptrPix - 1) & 1)                     << 7) |
           ((*(ptrPix + 1) & 1)                     << 3) |
           ((*(ptrPix + img.width()) & 1)            << 5) |
           ((*(ptrPix - img.width()) & 1)            << 1) |
           ( *(ptrPix - (img.width() + 1)) & 1)            |
           ((*(ptrPix - (img.width() - 1)) & 1)      << 2) |
           ((*(ptrPix + (img.width() - 1)) & 1)      << 6) |
           ((*(ptrPix + (img.width() + 1)) & 1)      << 4));
}

uint8 Skeletonizer::GetNbNeighbours(const uint8 neighbours)
{
    return (neighbours & 1) + ((neighbours >> 1) & 1) + ((neighbours >> 2)
            & 1) + ((neighbours >> 3) & 1) + ((neighbours >> 4) & 1)
            + ((neighbours >> 5) & 1) + ((neighbours >> 6) & 1)
            + ((neighbours >> 7) & 1);
}

void Skeletonizer::FilterSkeleton(SkeletonImage &mask, std::vector<core::Segment2D> &segments)
{
    for (int i = (int) segments.size() - 1; i>= 0; --i)
    {
        Point2 p1 = segments[i].p1();
        Point2 p2 = segments[i].p2();
        bool remove = false;
        if (p1[0] < 0.0 || p1[0] >= mask.width() || p1[1] < 0.0 || p1[1] >= mask.height() ||
                p2[0] < 0.0 || p2[0] >= mask.width() || p2[1] < 0.0 || p2[1] >= mask.height()) {
            remove = true;
        } else {
            if (!(mask((unsigned int) p1[0], (unsigned int) p1[1]) & 1) || !(mask((unsigned int) p2[0], (unsigned int) p2[1]) & 1)) {
                remove = true;
            }
        }
        if (remove) {
            segments.erase(segments.begin() + i);
        }
    }
}

std::shared_ptr<WeightedGraph2D> Skeletonizer::CreateFilteredGraph(ImageT<float> distances, std::vector<core::Segment2D> &segments)
{
    std::map<WeightedPoint2D, VertexId> ids;
    std::shared_ptr<WeightedGraph2D> res = make_shared<WeightedGraph2D>();
    for (int i = (int) segments.size() - 1; i >= 0; --i) {
        Point2 p1 = segments[i].p1();
        Point2 p2 = segments[i].p2();
        float d1 = 1.f;
        float d2 = 1.f;
        bool remove = false;
        if (p1[0] < 0.0 || p1[0] >= distances.width() || p1[1] < 0.0 || p1[1] >= distances.height() ||
                p2[0] < 0.0 || p2[0] >= distances.width() || p2[1] < 0.0 || p2[1] >= distances.height()) {
            remove = true;
        } else {
            d1 = distances(p1);
            d2 = distances(p2);
            if (d1 == 0 || d2 == 0) {
                remove = true;
            }
//            Scalar d = segments[i].length();

//            if ((d2 > d && d1 == 1.0) || (d2 == 1.0 && d1 > d)) {
//                remove = true;
//            }
//            if (d1 == 1.0) {
//                d1 = 0.0000000000000001;
//                // Todo : how can we keep spiked edges ?
//                remove = true;
//            }
//            if (d2 == 1.0) {
//                d2 = 0.0000000000000001;
//                remove = true;
//            }
        }

        if (remove) {
            segments.erase(segments.begin() + i);
            continue;
        }

        WeightedPoint2D wp1 = WeightedPoint2D(p1, d1);
        WeightedPoint2D wp2 = WeightedPoint2D(p2, d2);
        auto it1 = ids.find(wp1);
        auto it2 = ids.find(wp2);
        VertexId v1;
        VertexId v2;

        if (it1 != ids.end()) {
            v1 = it1->second;
        } else {
            v1 = res->Add(wp1)->id();
            ids[wp1] = v1;
        }
        if (it2 != ids.end()) {
            v2 = it2->second;
        } else {
            v2 = res->Add(wp2)->id();
            ids[wp2] = v2;
        }
        res->Add(WeightedSegment2D(wp1, wp2), v1, v2);
    }

    return res;
}

} // namespace core

} // namespace expressive
