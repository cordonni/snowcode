#include "core/image/ImageT.h"
#include "ork/resource/ResourceTemplate.h"

namespace expressive
{

namespace core
{

class ImageResource : public ork::ResourceTemplate<0, ImageRGBA>
{
public:
    ImageResource(ork::ptr<ork::ResourceManager> manager, const std::string &name,
                  ork::ptr<ork::ResourceDescriptor> desc, const TiXmlElement *e = NULL) :
        ResourceTemplate<0, ImageRGBA>(manager, name, desc)
    {
        e = e == NULL ? desc->descriptor : e;

        int w, h;
        try {
            checkParameters(desc, e, "name,source,width,height,format,type");
            getIntParameter(desc, e, "width", &w);
            getIntParameter(desc, e, "height", &h);
            width_ = w; height_ = h;
            if(getParameter(desc, e, "type") != "UNSIGNED_BYTE")
            {
                if (ork::Logger::ERROR_LOGGER != NULL) {
                    ork::Logger::ERROR_LOGGER->log("RESOURCE", "Image ressources should be unsigned byte");
                }
                throw std::exception();
            }

            tab_ = new Vector4uc[width_*height_];
            unsigned char* data = desc->getData();

            std::string format = getParameter(desc, e, "format");
            if (format == "RED")
                for(uint i = 0; i<width_*height_; ++i, ++data)
                    tab_[i] = Vector4uc(*data, 0,0,0);
            else if (format == "RG")
                for(uint i = 0; i<width_*height_; ++i, data+=2)
                    tab_[i] = Vector4uc(*data, *(data+1),0,0);
            else if (format == "RGB")
                for(uint i = 0; i<width_*height_; ++i, data+=3)
                    tab_[i] = Vector4uc(*data, *(data+1),*(data+2),0);
            else if (format == "RGBA")
                std::memcpy(tab_, data, 4*width_*height_);
            else
            {
                assert(false); // shouldn't happen, format is already tested in ressource loader
            }

            desc->clearData();
        } catch (...) {
            desc->clearData();
            throw std::exception();
        }
    }
};

extern const char ImageRessourceName[] = "image";

static ork::ResourceFactory::Type<ImageRessourceName, ImageResource> ImageType;

}
}
