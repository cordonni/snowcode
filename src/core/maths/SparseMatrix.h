/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#pragma once
#ifndef SPARSE_MATRIX_H
#define SPARSE_MATRIX_H

// Dependencies: core
#include <core/CoreRequired.h>

// Dependencies: Eigen
#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 175 )
#endif

#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>
#include <Eigen/IterativeLinearSolvers>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {
namespace core {

/*! \brief Class to manipulate SparseMatrix from Eigen.
 *
 *         Basically, this class is created in order to resolve Ax = b problem,
 *         using pre-constructed solver (SparseCholesky, CG, ...).
 *
 *         Conventions:
 *          Without additional name, functions manipulate the Sparse Matrix A
 *          With Vector, functions manipulate the known vector b
 *          With ResultVector, functions manipulate the unknown vector x
 */
class CORE_API SparseMatrix {
public:
    // Typedef
//    typedef Eigen::VectorXd             SpVector;
    typedef Eigen::Matrix<Scalar, Eigen::Dynamic, 1> SpVector;
    typedef Eigen::SparseMatrix<Scalar> SpMatrix;

    typedef Eigen::SimplicialLDLT   <SpMatrix>               SpCholeskySolver;
    typedef Eigen::ConjugateGradient<SpMatrix, Eigen::Upper> ConjugateGradientGSolver;

    ////////////////////////////////////////
    //// Constructor / Destructor
    SparseMatrix(unsigned n);
    SparseMatrix(unsigned n, unsigned m);

    ////////////////////////////////////////
    //// Getters

    // Getters: A
    inline unsigned nb_rows() { return A.rows(); }
    inline unsigned nb_cols() { return A.cols(); }

    inline SpMatrix& get()                       { return A; }
    inline Scalar&   get(unsigned i, unsigned j) { return A.coeffRef(i, j); }

    // Getters: b
    inline SpVector& getVector()           { return b; }
    inline Scalar    getVector(unsigned i) { return b[i]; }

    // Getters x
    inline SpVector& getResultVector()           { return x; }
    inline Scalar    getResultVector(unsigned i) { return x[i]; }

    ////////////////////////////////////////
    //// Setters

    inline void clearAll() { clear(); clearVector(); clearResultVector(); }

    // Setters: A
    inline void clear() { A.setZero(); }

    inline void resize(unsigned n)             { A.resize(n, n); }
    inline void resize(unsigned n, unsigned m) { A.resize(n, m); }

    // add: Caution: To use if data location (i,j) not created yet!
    //      If unknown, use set(i, j, val)!
    inline void add(unsigned i, unsigned j, Scalar value) { A.insert  (i, j)  = value; }
    inline void set(unsigned i, unsigned j, Scalar value) { A.coeffRef(i, j)  = value; }
    inline void inc(unsigned i, unsigned j, Scalar value) { A.coeffRef(i, j) += value; }

    void symmetrizeUpToDown();
    void symmetrizeDownToUp();

    // Setters: b
    inline void clearVector() { b = SpVector::Zero(nb_rows()); }

    inline void setVector(unsigned i, Scalar value) { b[i]  = value; }
    inline void incVector(unsigned i, Scalar value) { b[i] += value; }

    // Setters: x
    inline void clearResultVector() { x = SpVector::Zero(nb_cols()); }

    inline void setResultVector(unsigned i, Scalar value) { x[i]  = value; }
    inline void incResultVector(unsigned i, Scalar value) { x[i] += value; }

    ////////////////////////////////////////
    //// Solver

    /** \brief Solve the equation Ax = b, using SparseCholesky methods.
     *         The matrix should not be too complex
     *
     *  \return true if SparseCholesky resolved the equation. (use getResultVector() to obtain the result)
     */
    bool SparseCholeskySolve   ();

    /** \brief Solve the equation Ax = b, using CG (Conjugate Gradient) methods.
     *         Only the upper part of the matrix should be filled. (As CG required a symetric Matrix)
     *
     *  \return true if CG resolved the equation. (use getResultVector() to obtain the result)
     */
    bool ConjugateGragientSolve();

protected:
    // Solver Data: Ax = b
    SpMatrix A;

    SpVector b;
    SpVector x;
};

} // Close namespace core
} // Close namespace expressive

#endif // SPARSE_MATRIX_H
