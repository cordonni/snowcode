#include <core/maths/SparseMatrix.h>

#include <iostream>

using namespace expressive;
using namespace core;

////////////////////////////////////////////////////////////////////
//// Constructor / Destructor

SparseMatrix::SparseMatrix(unsigned n)
    : A(SpMatrix(n, n)), b(SpVector::Zero(n)), x(SpVector::Zero(n)) {}
SparseMatrix::SparseMatrix(unsigned n, unsigned m)
    : A(SpMatrix(n, m)), b(SpVector::Zero(n)), x(SpVector::Zero(m)) {}

////////////////////////////////////////////////////////////////////
//// Setters

//void SparseMatrix::symmetrizeUpToDown() { A.selfadjointView<Eigen::Lower>() = A.selfadjointView<Eigen::Upper>(); }
//void SparseMatrix::symmetrizeDownToUp() { A.selfadjointView<Eigen::Upper>() = A.selfadjointView<Eigen::Lower>(); }

////////////////////////////////////////////////////////////////////
//// Solver

bool SparseMatrix::SparseCholeskySolve() {
    // Init Solver
    SpCholeskySolver solver;
    solver.compute(A);

    if (solver.info()!=Eigen::Success)
        { std::cout << "error Sparse Matrix Init" << std::endl; return false; }

    // Solve
    x = solver.solve(b);

    if (solver.info()!=Eigen::Success)
        { std::cout << "error Sparse Matrix Resolution" << std::endl; return false; }

    // Solve done!
    return true;
}

bool SparseMatrix::ConjugateGragientSolve() {
    // Init Solver
    ConjugateGradientGSolver solver;
    solver.compute(A);

    if (solver.info()!=Eigen::Success)
        { std::cout << "error Sparse Matrix Init" << std::endl; return false; }

    // Solve
    x = solver.solveWithGuess(b, x);

    if (solver.info()!=Eigen::Success)
        { std::cout << "error Sparse Matrix Resolution" << std::endl; return false; }

    // Solve done!
    return true;
}
