/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef _EXPRESSIVE_CORE_COMMANDMANAGER_H
#define _EXPRESSIVE_CORE_COMMANDMANAGER_H

#include <ork/core/Logger.h>
#include <core/utils/Serializable.h>
#include "core/utils/Singleton.h"
#include "core/utils/ListenerT.h"

#include <vector>
#include <cassert>
#include <memory>

namespace expressive
{

namespace core
{

/**
 * @brief The Command class
 * Base class for undo/redo pattern.
 * For more informations on this pattern, check
 * http://www.codeproject.com/Articles/33384/Multilevel-Undo-and-Redo-Implementation-in-Cshar
 */
class CORE_API Command : public core::ListenableT<Command>, public std::enable_shared_from_this<Command>, public Serializable
{
public:
    Command(const char *name = "Command", Command *parent = nullptr) : Serializable(name), m_parent(parent), m_undoable(true) {}

    /**
     * @brief ~Command destructor
     */
    virtual ~Command() {}

    /**
     * @brief execute the do/redo function. Must be implemented for each command class.
     * @return true if command has executed as requested, false otherwise (i.e. there has
     *  been a problem that should probably be solved before running next command).
     */
    virtual bool execute() = 0;

    /**
     * @brief undo the undo function. Muse be implemented for each command class.
     * @return true if command was undone as requested, false otherwise (i.e. there has
     *  been a problem that should probably be solved before undoing next command).
     */
    virtual bool undo() = 0;

    /**
     * Returns a new command that achieves a subpart of this command.
     * %begin and %end are percentage of the total task to be returned.
     * Default only applies the whole command  at the begining (when begin = 0), and null otherwise.
     * (For example, user can ask the first half of the command to be run via
     * a call to #getPart(0, 50); Or even create a script with a smoothed
     * movement between 2 positions (like linear or hermite interpolation).
     * Subcommands should NEVER be added to the CommandManager, but rather be used
     * with core/command/ScriptPlayer.
     */
    virtual std::shared_ptr<Command> getPart(double begin = 0, double end = 100);

    /**
     * Returns this Command's parent. Only valid if this command was created by getPart. Null otherwise.
     */
    Command *parent() const;

    /**
     * @brief undoable Returns true if this command is undoable. Otherwise, it won't be added to the list of commands.
     */
    bool undoable() const;

    /**
     * @brief GetPreferedLength Returns the prefereed duration for a given command.
     * Most of them will only require 1 frame, but for some, it might take longer than that.
     */
    virtual Scalar GetPreferedDuration() const;

protected:
    /**
     * This Command's parent. Only valid if this command was created by getPart. Null otherwise.
     */
    Command *m_parent;

    /**
     * @brief m_undoable true if this command is undoable. Otherwise, it won't be added to the list of commands.
     */
    bool m_undoable;
};

/**
 * @brief The ComposedCommand class
 * A command that contains multiple commands. Allows to play multiple and / or complex commands.
 */
class CORE_API ComposedCommand : public Command
{
public:
    ComposedCommand();

    ComposedCommand(const std::vector<std::shared_ptr<Command> > &commands);

    virtual ~ComposedCommand();

    virtual bool execute();

    virtual bool undo();

    /**
     * @brief AddCommand Adds a new command to the list of played commands.
     * @param command the new command to add.
     */
    void AddCommand(std::shared_ptr<Command> command);

protected:
    std::vector<std::shared_ptr<Command>> commands_;
};

/**
 * @brief The ExampleCommand class just an example for how to use Command
 */
class CORE_API ExampleCommand : public Command
{
public:
    ExampleCommand(int id);

    virtual bool execute();

    virtual bool undo();

protected:
    int id;
};

/**
 * @brief The CommandManager class This class manages commands and stores
 *  a list of executed commands that should be undone, as well as tasks to be
 *  played next.
 */
class CORE_API CommandManager : public Serializable, public Singleton<CommandManager>
{
public:
    EXPRESSIVE_MACRO_NAME("CommandManager")

    /**
     * @brief CommandManager ctor
     * To reduce memory footprint, an optional parameter history_size can be provided.
     * The CommandManager will only remember the last x commands (infinite if 0).
     */
    CommandManager(unsigned int history_size = 0);

    /**
     * @brief ~CommandManager dtor
     */
    virtual ~CommandManager();

    /**
     * @brief run adds a given command to the list of played Commands.
     * @param command The command to be played.
     * @param run false if the command has already been played and just
     *  needs to be added to #previous_commands_ (like a movePoint).
     *
     * the bool run parameter as been removed to force the use of the mutex !
     */
    void run(std::shared_ptr<Command> command /*, bool run = true*/);

    /**
     * @brief update updates the last executed command.
     * @param command The command to be updated and played, it should correspond to the
     * the last executed command.
     */
    void update(std::shared_ptr<Command> command);

    /**
     * @brief run see CommandManager#run(). Applied on #GetSingleton()
     * @param command The command to be played.
     * @param run false if the command has already been played and just
     *  needs to be added to #previous_commands_ (like a movePoint)..
     */
    static void runInSingleton(std::shared_ptr<Command> command /*, bool run = true*/);

    /**
     * @brief hasPreviousCommand
     * @return true if there are still commands to be undone.
     */
    bool hasPreviousCommand();

    /**
     * Returns the last run command.
     */
    std::shared_ptr<Command> last_command();

    /**
     * @brief hasNextCommand
     * @return true if there are still commands to be done.
     */
    bool hasNextCommand();

    /**
     * @brief loadList Loads from a given file the list of commands.
     *  TODO : load this from XML.
     */
    void loadList(const char* /*filename*/);

    /**
     * @brief clear Clears and deletes all commands.
     */
    void  clear();

    /**
     * @brief clearnext_commands_ Clears only undone commands.
     *  Used when the user did a new command for example.
     */
    void clear_next_commands_();

    /**
     * @brief clearnext_commands_ Clears only done commands.
     */
    void clear_previous_commands_();

    /**
     * @brief undo Undoes the last applied command, if any. Adds it to the list of redoable Commands.
     * @return true if all went accordingly to the plan. false otherwise.
     */
    bool undo();

    /**
     * @brief redo Redoes the last undone command.
     * @return true if all went accordingly to the plan. false otherwise.
     */
    bool redo();

    virtual TiXmlElement *ToXml(const char *) const;


    std::mutex& action_mutex();

protected:
    /**
     * @brief add Adds a command to the list of commands.
     * @param command The given command to be added.
     * @param isNextCommand if true, command will be added to #next_commands_;
     *  Otherwise, it's added to #previous_commands_ (Mostly used for loading purpose or specific commands).
     */
    void add(std::shared_ptr<Command> command, bool isNextCommand = false);

    /**
     * @brief previous_commands_ Previous commands. They're awaiting to be undone.
     */
    std::vector<std::shared_ptr<Command> > previous_commands_;

    /**
     * @brief next_commands_ Future commands. They're awaiting to be done.
     */
    std::vector<std::shared_ptr<Command> > next_commands_;

    /**
     * @brief history_size_ The maximum amount of commands to be remembered by this CommandManager.
     * Can be tweaked to reduce memory footprint.
     */
    unsigned int history_size_;

    /**
     * @brief action_mutex_ locked when a command is executed, use to prevent interaction with data under modification
     */
    std::mutex action_mutex_;
};

} // namespace core

} // namespace expressive

#endif // _EXPRESSIVE_CORE_COMMANDMANAGER_H
