/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef _CONVOL_CORE_SCRIPTPLAYER_H_
#define _CONVOL_CORE_SCRIPTPLAYER_H_

#include <core/commands/CommandManager.h>
#include "core/utils/ListenerT.h"

#include <map>
#include <set>
#include <vector>

namespace expressive
{

namespace core
{

class ScriptPlayer;

/**
 * @brief The ScriptListener class
 * This class just offers a basic listener method for classes which need to be updated
 * once a Script has been played (Refresh, etc...).
 */
class CORE_API ScriptListener : public ListenerT<ScriptPlayer>
{
public:
    virtual void frameDoneCallback(int frame) = 0;
};

/**
 * @brief The ScriptElement struct
 * Bascialy, it's just a Command, it's delay and it's length.
 */
struct CORE_API ScriptElement : public core::ListenerT<Command>, public core::ListenableT<ScriptElement>
{
    ScriptElement(const ScriptElement &other);

    ScriptElement(std::shared_ptr<Command> command, unsigned int frameBegin, unsigned int frameEnd);

    /**
     * Updates this ScriptElement when the provided command gets updated.
     * This will also transfer the updated signal to the ScriptPlayer.
     * @param command the command that got updated.
     */
    void updated(Command *command);

    bool operator < (const ScriptElement& other) const;

    unsigned int frame_begin() const;
    unsigned int frame_end() const;

    std::shared_ptr<Command> command;

protected:
    unsigned int m_frame_begin;
    unsigned int m_frame_end;
};

/**
  * A Script Player
  * Each ScriptElement runs a specific command (which will be split in parts by the Player)
  * The Command to be run must have a valid #Command::getPart() method.
  * Speed can be adjusted in the #play method.
  */

class CORE_API ScriptPlayer : public ListenableT<ScriptPlayer>, public Singleton<ScriptPlayer>, public ListenerT<ScriptElement>
{
public:
//    typedef void (*CallbackFunc) ();


    ScriptPlayer(std::shared_ptr<CommandManager> command_manager = nullptr);

    virtual ~ScriptPlayer();

    /**
     * Returns the current number of frames.
     */
    unsigned int num_frames() const;

    /**
     * Returns the index of the current frame.
     */
    unsigned int current_frame() const;

    /**
     * @brief setNumFrame Sets the maximum number of frames that will be played by this ScriptPlayer.
     */
    void set_num_frames(unsigned int nbFrames);

    /**
     * @brief jumpToFrame Directly jump to a given frame, to do some fast-forward playing.
     * @param frame
     */
    void set_current_frame(unsigned int frame);

    /**
     * @brief play Just plays a given amount of frame(s).
     * @param nbFrames
     * @return  true if everything went well. false otherwise.
     */
    virtual bool play(unsigned int nbFrames = 1);

    /**
     * @brief playAll Plays every script parts, at a given rate. At each frame, notify listeners.
     * @param speed Number of script commands to be played at each frame.
     * @param listener the calling class that wants to listen to this. If none specified, Just
     *  notify the ScriptPlayer's own listeners.
     * @param overrideListeners if true, only supplied listener will be notified, and none others.
     */
    void playAll(unsigned int speed, ScriptListener *listener = nullptr, bool overrideListeners = false);

    /**
     * @brief rewind See #play, in reverse order.
     * @param nbFrames
     * @return  true if everything went well. false otherwise.
     */
    virtual bool rewind(unsigned int nbFrames = 1);

    /**
     * @brief rewindAll See #playAll, in reverse order.
     * @param speed Number of script commands to be played at each frame.
     * @param listener the calling class that wants to listen to this. If none specified, Just
     *  notify the ScriptPlayer's own listeners.
     * @param overrideListeners if true, only supplied listener will be notified, and none others.
     */
    void rewindAll(unsigned int speed, ScriptListener *listener = nullptr, bool overrideListeners = false);

    /**
     * @brief addScriptElement Adds a given ScriptElement to this script.
     * @param element
     */
    void addScriptElement(const ScriptElement &element);

    /**
     * @brief interruptAtFrame Forces the script to stop at a given frame.
     * @param frame
     */
    void interruptAtFrame(unsigned int frame);

    /**
     * Remove every scriptElements from this ScriptPlayer.
     */
    void clear();


    /**
     * Gets called when a child scriptElement was updated.
     * @param scriptElement the updated child.
     */
    void updated(ScriptElement *scriptElement);


protected:
    /**
     * @brief scriptElements The list of commands to be run.
     */
    std::set<ScriptElement> scriptElements;

//    std::set<ScriptListener*> listeners;

    // quick typedefs to ease writing.
    typedef std::multimap<unsigned int, std::shared_ptr<Command>> M;
    typedef std::pair<M::iterator, M::iterator> range;

    /**
     * @brief commandManager The CommandManager that handles the script actions.
     *  Mostly used to allow easy rewind for scripts.
     * To avoid conflicts with scene or GUI, this class uses its own CommandManager by default.
     */
    std::shared_ptr<CommandManager> commandManager;

    /**
     * @brief commands contains for each frame which command should be played.
     *  This map is built from #scriptElements, and must be updated each time something is added or removed.
     */
    M commands;

    /**
     * @brief numFrames The number of frames in this ScriptPlayer.
     */
    unsigned int num_frames_;

    /**
     * @brief currentFrame Current frame if ScriptPlayer is already started.
     */
    unsigned int current_frame_;

    /**
     * @brief maxFrame Frame at which the script should stop.
     */
    unsigned int interrupt_frame_;

};

} // namespace core

} // namespace expressive

#endif // _CONVOL_CORE_SCRIPTPLAYER_H_
