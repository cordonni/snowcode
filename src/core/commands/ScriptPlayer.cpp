#include <core/commands/ScriptPlayer.h>

#include <algorithm>

using namespace std;

namespace expressive
{

namespace core
{

ScriptElement::ScriptElement(const ScriptElement &other) :
    ListenerT<Command>(other.command.get()),
    ListenableT<ScriptElement>(other),
    command(other.command),
    m_frame_begin(other.m_frame_begin),
    m_frame_end(other.m_frame_end)
{
}

ScriptElement::ScriptElement(std::shared_ptr<Command> command, unsigned int frameBegin, unsigned int frameEnd) :
    ListenerT<Command>(command.get()),
    command(command),
    m_frame_begin(frameBegin),
    m_frame_end(frameEnd)
{
}


unsigned int ScriptElement::frame_begin() const
{
    return m_frame_begin;
}

unsigned int ScriptElement::frame_end() const
{
    if (m_frame_end != (unsigned int) -1) {
        return m_frame_end;
    }
    return m_frame_begin + command->GetPreferedDuration();
}

void ScriptElement::updated(Command * /*command*/)
{

    NotifyUpdate();
}

bool ScriptElement::operator <(const ScriptElement &other) const
{
    if (other.command == this->command) {
        return false;
    }

    return m_frame_begin < other.m_frame_begin;
}

ScriptPlayer::ScriptPlayer(std::shared_ptr<CommandManager> command_manager) :
    ListenerT<ScriptElement>(nullptr),
    commandManager(command_manager == nullptr ? make_shared<CommandManager>() : command_manager),
    num_frames_(0),
    current_frame_(0),
    interrupt_frame_(0)
{
}

ScriptPlayer::~ScriptPlayer()
{
    clear();
}

unsigned int ScriptPlayer::num_frames() const
{
    return num_frames_;
}

unsigned int ScriptPlayer::current_frame() const
{
    return current_frame_;
}

void ScriptPlayer::set_current_frame(unsigned int frame)
{
    bool reverse = frame < current_frame_;
    while (current_frame_ != frame) {
        if (reverse) {
            rewind();
        } else {
            play();
        }
    }
}

void ScriptPlayer::set_num_frames(unsigned int nbFrames)
{
//    if (interrupt_frame_ == num_frames_) {
    interrupt_frame_ = nbFrames - 1;
//    }
    num_frames_ = nbFrames;
}

void ScriptPlayer::rewindAll(unsigned int speed, ScriptListener *listener, bool overrideListeners)
{
//    for (int i = ((int) currentFrame) - 1; i >= 0; --i) {
    while (current_frame_ > 0) {
        int i = current_frame_;
        if (rewind(speed)) {
            if (!overrideListeners) {
                for (ListenerT<ScriptPlayer> *l : listeners_) {
                    dynamic_cast<ScriptListener*>(l)->frameDoneCallback(i);
                }
            }

            if (listener != nullptr) {
                listener->frameDoneCallback(i);
            }
        }
    }
}

void ScriptPlayer::playAll(unsigned int speed, ScriptListener *listener, bool overrideListeners)
{
//    for (unsigned int i = currentFrame; i < maxFrame; ++i) {
    unsigned int nextStop = current_frame_ < interrupt_frame_ ? interrupt_frame_ : num_frames_ - 1;
    while (current_frame_< nextStop) {
        unsigned int i = current_frame_;
        if (play(speed)) {

            if (!overrideListeners) {
                for (ListenerT<ScriptPlayer> *l : listeners_) {
                    dynamic_cast<ScriptListener*>(l)->frameDoneCallback(i);
                }
            }

            if (listener != nullptr) {
                listener->frameDoneCallback(i);
            }
        }
    }
}

bool ScriptPlayer::rewind(unsigned int nbFrames)
{
    int cpt = 0;
    for (uint i = 0; i < nbFrames; ++i) {
        if (current_frame_ > 0) {
            range r = commands.equal_range(current_frame_);
            for (M::iterator it  = r.first; it != r.second; ++it){
//                if (it->second != nullptr) {
//                    it->second->undo();
//                }
                commandManager->undo();
            }
            ++cpt;
            current_frame_ --;
        }
    }

    return cpt != 0;
}

bool ScriptPlayer::play(unsigned int nbFrames)
{
    int cpt = 0;
    unsigned int nextStop = current_frame_ < interrupt_frame_ ? interrupt_frame_ : num_frames_;
    int lastFrame = std::min(current_frame_+ nbFrames, nextStop);
    for (unsigned int i = current_frame_; i < (uint) lastFrame; ++i) {
        range r = commands.equal_range(i);
        for (M::iterator it = r.first; it != r.second; ++it) {
//            if (it->second != nullptr) {
//                it->second->execute();
//            }
            commandManager->run(it->second);
            cpt++;
        }
    }
    current_frame_ = lastFrame;
    return cpt != 0;
}

void ScriptPlayer::addScriptElement(const ScriptElement &element)
{
    // Copy element in order to be able to add a listener to it, without having to find the listener from the std::set.
    ScriptElement tmpElt = element;
    tmpElt.AddListener(this);
    scriptElements.insert(tmpElt);

    unsigned int nbFrames = element.frame_end()- element.frame_begin();
    double p = 100.0 / nbFrames;

    for (unsigned int i = 0; i < nbFrames; ++i) {
        std::shared_ptr<Command> c = element.command->getPart(i * p, (i + 1) * p );
        if (c != nullptr) {
            commands.insert(make_pair(element.frame_begin() + i, c));
        }
    }

    set_num_frames(std::max(num_frames_, element.frame_end()));
}

void ScriptPlayer::interruptAtFrame(unsigned int frame)
{
    interrupt_frame_ = frame;
}

void ScriptPlayer::clear()
{
//    for (unsigned int i = current_frame_; i < num_frames_; ++i) { // delete the frames that won't be deleted by the command manager
//        range r = commands.equal_range(i);
//        for (M::iterator it = r.first; it != r.second; ++it) {
//            delete it->second;
//        }
//    }
    current_frame_ = 0;
    num_frames_ = 0;
    interrupt_frame_ = 0;
//    for (unsigned int i = 0; i < scriptElements.size(); ++i) {
//        delete scriptElements[i].command;
//    }
    scriptElements.clear();
    commands.clear();
}

void ScriptPlayer::updated(ScriptElement * scriptElement)
{
    // First remove every related commands from the commands list (iterators get invalidated, we have to to this).
    for (M::iterator iter = commands.begin(); iter != commands.end();) {
        M::iterator erase_iter = iter++;

        if (scriptElement->command.get() == erase_iter->second->parent()) {
            commands.erase(erase_iter);
        }
    }

    this->addScriptElement(*scriptElement);

    // update num frames (Could be lower than what it was before if we updated the scriptElement's frame end for example.
    set_num_frames(commands.lower_bound(scriptElement->frame_end())->first);
}

} // namespace core

} // namespace expressive
