#include <core/commands/CommandManager.h>

#include <ork/core/Logger.h>

using namespace ork;

namespace expressive
{

namespace core
{

std::shared_ptr<Command> Command::getPart(double begin, double /*end*/)
{
    return begin == 0 ? shared_from_this() : nullptr;
}

Command *Command::parent() const
{
    return m_parent;
}

bool Command::undoable() const
{
    return m_undoable;
}

Scalar Command::GetPreferedDuration() const
{
    return 1;
}

ExampleCommand::ExampleCommand(int id) {
    this->id = id;
}

bool ExampleCommand::execute()
{
    ork::Logger::DEBUG_LOGGER->logf("DEBUG", "Executing command with id %d...", id);
    return false;
}

bool ExampleCommand::undo()
{
    ork::Logger::DEBUG_LOGGER->logf("DEBUG", "Undoing command with id %d...", id);
    return false;
}

CommandManager::CommandManager(unsigned int history_size) : Serializable(StaticName()), history_size_(history_size)
{
}

CommandManager::~CommandManager()
{
    clear();
}

bool CommandManager::hasPreviousCommand()
{
    return previous_commands_.begin() != previous_commands_.end();
}

std::shared_ptr<Command> CommandManager::last_command()
{
    if (previous_commands_.size() > 0) {
        return previous_commands_[previous_commands_.size() - 1];
    }
    return nullptr;
}

bool CommandManager::hasNextCommand()
{
    return next_commands_.begin() != next_commands_.end();
}

void CommandManager::loadList(const char* /*filename*/)
{
    assert(false);
}

void  CommandManager::clear() {
    clear_next_commands_();
    clear_previous_commands_();
}

void CommandManager::clear_next_commands_()
{
//    for (unsigned int i = 0; i < next_commands_.size(); ++i) {
//        delete next_commands_[i];
//    }
    next_commands_.clear();
}

void CommandManager::clear_previous_commands_()
{
//    for (unsigned int i = 0; i < previous_commands_.size(); ++i) {
//        delete previous_commands_[i];
//    }
    previous_commands_.clear();
}

TiXmlElement *CommandManager::ToXml(const char *) const
{
    assert (false && "This should be coded");
    return nullptr;
}

std::mutex& CommandManager::action_mutex()
{
    return action_mutex_;
}


ComposedCommand::ComposedCommand() :
    Command("ComposedCommand")
{
}

ComposedCommand::ComposedCommand(const std::vector<std::shared_ptr<Command>> &commands) :
    Command("ComposedCommand"),
    commands_(commands)
{
}

ComposedCommand::~ComposedCommand()
{
//    for (unsigned int i = 0; i < commands_.size(); ++i) {
//        delete commands_[i];
//    }
    commands_.clear();
}

bool ComposedCommand::execute()
{
    bool res = true;
    for (unsigned int i = 0; i < commands_.size() && res == true; ++i) {
        res &= commands_[i]->execute();
    }
    return res;
}

bool ComposedCommand::undo()
{
    bool res = true;
    for (unsigned int i = 0; i < commands_.size() && res == true; ++i) {
        res &= commands_[i]->undo();
    }
    return res;
}

void ComposedCommand::AddCommand(std::shared_ptr<Command> command)
{
    commands_.push_back(command);
}

void CommandManager::runInSingleton(std::shared_ptr<Command> command)
{
    GetSingleton()->run(command);
}

void CommandManager::run(std::shared_ptr<Command> command)
{
    if (command->undoable()) {
        add(command);
    }

    std::unique_lock<std::mutex> mlock(action_mutex_);
    command->execute();
}

void CommandManager::update(std::shared_ptr<Command>command)
{
    if(previous_commands_.back() != command)
        Logger::ERROR_LOGGER->log("COMMANDS", "inconsistency between last played command and command to be updated");

    std::unique_lock<std::mutex> mlock(action_mutex_);
    command->execute();
}


void CommandManager::add(std::shared_ptr<Command> command, bool isNextCommand)
{
    if (!isNextCommand) {
        previous_commands_.push_back(command);
        if (hasNextCommand()) {
            clear_next_commands_();
        }
    } else {
        next_commands_.insert(next_commands_.begin(), command);
    }
    // Now, if history is bigger than required, we reduce it's size until it has correct size.
    // It should never have to remove more than 1 command at each time, but let's be careful anyway.
    if (history_size_ > 0) {
        while ((previous_commands_.size() + next_commands_.size() ) > history_size_)
        {
            if (previous_commands_.size() == 0) {
                break;
            }
//            delete (*previous_commands_.begin());
            previous_commands_.erase(previous_commands_.begin());
        }
    }
}


bool CommandManager::undo()
{
    if (hasPreviousCommand()) {
        std::unique_lock<std::mutex> mlock(action_mutex_);

        std::shared_ptr<Command> c = previous_commands_.back();
        bool res = c->undo();

        previous_commands_.pop_back();
        next_commands_.push_back(c);

        return res;
    }
    return false;
}

bool CommandManager::redo()
{
    if (hasNextCommand()) {
        std::unique_lock<std::mutex> mlock(action_mutex_);

        std::shared_ptr<Command> c = next_commands_.back();
        bool res = c->execute();

        next_commands_.pop_back();
        previous_commands_.push_back(c);
        return res;
    }
    return false;
}

} // namespace core

} //namespace expressive
