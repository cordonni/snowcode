/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_MESH_TRAITS_H_
#define EXPRESSIVE_MESH_TRAITS_H_

//////////////////////////////////////////////////////////////////////////////
//   Choose which geometrical structures to use.                            //
//   Include all needed files for those structures.                         //
//   OpenMesh is included since it is used for default geometrical types    //
//   NB : If changed, traits must respect the OpenMesh API                  //
//////////////////////////////////////////////////////////////////////////////

#include "ExpressiveRequired.h"

// OpenMesh dependencies
#ifdef _MSC_VER
    #pragma warning(push, 0)
#endif

//// specifying missing standard functions from MSVC
//#ifdef _MSC_VER
//#include <float.h>
//#define isfinite _finite
//#endif


#include <OpenMesh/Core/Mesh/Traits.hh>
//#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#ifdef _MSC_VER
    #pragma warning(pop)
#endif


namespace expressive 
{

    // For detail documentation about Traits structure in Expressive, see documentation page Traits pattern in Expressive
    // Traits Structures are split in sub structures since we need the parent to use it in the mesh typedef.

    /*! \brief Default structure for OpenMesh Traits used in \link Expressive::ExpressiveTraits3d ExpressiveTraits3d \endlink.
    */
    struct MeshTraits: public OpenMesh::DefaultTraits
    {
        typedef expressive::Scalar Scalar;

        VertexAttributes(OpenMesh::Attributes::Status);
        FaceAttributes(OpenMesh::Attributes::Status);
        EdgeAttributes(OpenMesh::Attributes::Status);
        HalfedgeAttributes(OpenMesh::Attributes::Status);

        typedef OpenMesh::TriMesh_ArrayKernelT<MeshTraits> Mesh;
        typedef OpenMesh::TriMesh_ArrayKernelT<MeshTraits> TriMesh;

        // Maybe in the future with new C++ standard we will use a template alias. //TODO:@todo : take comment into account ...
        // Since it's not currently possible, we use this wrapper.
        // The use of such types is different from OpenMesh use :
        // First choose the type of property, then the geometry to which it is applied.
        template<typename type>
        struct MeshPropHandleT
        {
            typedef OpenMesh::EPropHandleT<type> Edge ;
            typedef OpenMesh::FPropHandleT<type> Face ;
            typedef OpenMesh::VPropHandleT<type> Vertex ;
        };

    };

}

namespace expressive {
}
#endif // EXPRESSIVE_MESH_TRAITS_H_
