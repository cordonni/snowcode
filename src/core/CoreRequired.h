/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: CoreRequired.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: General definition and inclusion for Convol.
                This file should be included in any file of
                the Convol library.

    @todo clean it

   Platform Dependencies: None
*/

#pragma once
#ifndef CORE_REQUIRED_H_
#define CORE_REQUIRED_H_

#include "ExpressiveRequired.h"

#if !defined(BUILD_STATIC) && (defined ( _WIN32 ) || defined( _WIN64 ))
#   if defined( BUILD_CORE )
#        define CORE_API __declspec( dllexport )
#    else
#        define CORE_API __declspec( dllimport )
#    endif
#else //Probably Linux
#   ifdef __APPLE__
#       define CORE_API
#   else // Linux probably
#        define CORE_API
#   endif
#endif

#include <core/utils/AssertionHandler.h>

//////////////////////
// Macro definition //
//////////////////////
// define our own assert expression to handle them.
#ifndef NDEBUG // ifndef NDEBUG & GCC
#ifdef __ASSERT_VOID_CAST
#define expressive_assert(expr) \
((expr) \
? __ASSERT_VOID_CAST (0) \
: ((expressive::AssertionHandler::handle_assert(__STRING(expr), __FILE__, __LINE__, __ASSERT_FUNCTION)) \
? __ASSERT_VOID_CAST(0) \
: __assert_fail(__STRING(expr), __FILE__, __LINE__, __ASSERT_FUNCTION)))
#elif defined _CRT_WIDE //windows
#ifdef _MSC_VER //msvc
#define expressive_assert(e)  \
    ((void) ((e) ? (void)0 : _ASSERT (#e)))
#else // mingw
#define expressive_assert(e)  \
    ((void) ((e) ? (void)0 : _assert (#e, __FILE__, __LINE__)))
#endif
#else // mac
    #define expressive_assert(e)  \
        ((void) ((e) ? (void)0 : __assert (#e, __FILE__, __LINE__)))
#endif
#else
    #define expressive_assert(e) ((void)0)
#endif


#endif // CORE_REQUIRED_H_
