#include "ExpressiveRequired.h"

// std dependencies
#include<float.h> // Limits are used to define the max and min scalar values in defaults Expressive Traits.

namespace expressive
{

const Scalar kScalarMax = DBL_MAX;
const Scalar kScalarMin = -DBL_MAX;

}
