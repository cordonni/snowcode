/*!
   \file: CoreRequired.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: General definition and inclusion for Convol.
                This file should be included in any file of
                the Convol library.

    @todo clean it

   Platform Dependencies: None
*/

#pragma once
#ifndef EXPRESSIVE_REQUIRED_H_
#define EXPRESSIVE_REQUIRED_H_

#include "commons/CommonsRequired.h"


#ifdef _MSC_VER
// Remove stupid inline warnings.
#pragma warning( push )
//#pragma warning( disable : 4005)
#endif
#include <string>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

//// --------------- Determine Operating System Platform ---------------
//#if defined ( _WIN32 ) || defined( _WIN64 )
//#    define EXPRESSIVE_WIN_PLATFORM
//#    if defined( _WIN64 )// Windows
//#        define EXPRESSIVE_WIN64_PLATFORM
//#    else
//#        define EXPRESSIVE_WIN32_PLATFORM
//#    endif
//#endif

//////////////////////
// Macro definition //
//////////////////////
#ifndef UNUSED
#define UNUSED(x) ( (void)(x) ) // Especially to prevent the unused variable warning with GCC
#endif


// This macro helps to have a consistent naming all along our application.
// Can be used for debugging, logging, factories, and serializing.
#define EXPRESSIVE_MACRO_NAME(str_name)                             \
    static const std::string &StaticName()                          \
    {                                                               \
        const static std::string name_(str_name);                   \
        return name_;                                               \
    }                                                               \
    virtual std::string Name() const                                \
    {                                                               \
        return StaticName();                                        \
    }

/////////////////////
// Type definition //
/////////////////////

// Use constant defines in math.h
#define _USE_MATH_DEFINES


#ifdef EXPRESSIVE_WIN_PLATFORM
    typedef unsigned int uint;
    typedef unsigned __int8 uint8;
    typedef unsigned __int16 uint16;
    typedef unsigned __int32 uint32;
    typedef unsigned __int64 uint64;
#else
#include <stdint.h>
#   ifdef EXPRESSIVE_MACOS_PLATFORM
    typedef unsigned int uint;
    typedef uint8_t uint8;
    typedef uint16_t uint16;
    typedef uint32_t uint32;
    typedef uint64_t uint64;
#   else // Must be Linux
    typedef unsigned int uint;
    typedef uint8_t uint8;
    typedef uint16_t uint16;
    typedef uint32_t uint32;
    typedef uint64_t uint64;
#   endif
#endif

namespace expressive {
    typedef double Scalar;

    // Minimum an maximum Scalar values, See .cpp for initialization
    extern const Scalar COMMONS_API kScalarMax;
    extern const Scalar COMMONS_API kScalarMin;
}

#endif // CORE_REQUIRED_H_
