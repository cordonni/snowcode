#ifndef EXPRESSIVE_ORK_TRAITS_DEF_H_
#define EXPRESSIVE_ORK_TRAITS_DEF_H_

#include "ExpressiveRequired.h"

#include "ork/math/vec2.h"
#include "ork/math/vec3.h"
#include "ork/math/vec4.h"
#include "ork/math/mat2.h"
#include "ork/math/mat3.h"
#include "ork/math/mat4.h"
#include "ork/math/quat.h"

namespace expressive
{
    struct OrkVectorTraits
    {
        typedef ork::vec2<int> Vector2i;
        typedef ork::vec2<bool> Vector2b;
        typedef ork::vec2<float> Vector2f;
        typedef ork::vec2<short> Vector2s;
        typedef ork::vec2<double> Vector2d;
        typedef ork::vec2<unsigned int> Vector2ui;
        typedef ork::vec2<unsigned int> Vector2uc;
        typedef ork::vec3<int> Vector3i;
        typedef ork::vec3<bool> Vector3b;
        typedef ork::vec3<float> Vector3f;
        typedef ork::vec3<short> Vector3s;
        typedef ork::vec3<double> Vector3d;
        typedef ork::vec3<unsigned int> Vector3ui;
        typedef ork::vec3<unsigned int> Vector3uc;
        typedef ork::vec4<int> Vector4i;
        typedef ork::vec4<bool> Vector4b;
        typedef ork::vec4<float> Vector4f;
        typedef ork::vec4<short> Vector4s;
        typedef ork::vec4<double> Vector4d;
        typedef ork::vec4<unsigned int> Vector4ui;
        typedef ork::vec4<unsigned int> Vector4uc;

        typedef ork::mat2<int> Matrix2i;
        typedef ork::mat2<bool> Matrix2b;
        typedef ork::mat2<float> Matrix2f;
        typedef ork::mat2<short> Matrix2s;
        typedef ork::mat2<double> Matrix2d;
        typedef ork::mat2<unsigned int> Matrix2ui;
        typedef ork::mat2<unsigned int> Matrix2uc;
        typedef ork::mat3<int> Matrix3i;
        typedef ork::mat3<bool> Matrix3b;
        typedef ork::mat3<float> Matrix3f;
        typedef ork::mat3<short> Matrix3s;
        typedef ork::mat3<double> Matrix3d;
        typedef ork::mat3<unsigned int> Matrix3ui;
        typedef ork::mat3<unsigned int> Matrix3uc;
        typedef ork::mat4<int> Matrix4i;
        typedef ork::mat4<bool> Matrix4b;
        typedef ork::mat4<float> Matrix4f;
        typedef ork::mat4<short> Matrix4s;
        typedef ork::mat4<double> Matrix4d;
        typedef ork::mat4<unsigned int> Matrix4ui;
        typedef ork::mat4<unsigned int> Matrix4uc;

        template<typename T>
        using Vector2T = ork::vec2<T>;
        template<typename T>
        using Vector3T = ork::vec3<T>;
        template<typename T>
        using Vector4T = ork::vec4<T>;
        template<typename T>
        using Matrix2T = ork::mat2<T>;
        template<typename T>
        using Matrix3T = ork::mat3<T>;
        template<typename T>
        using Matrix4T = ork::mat4<T>;

        typedef Vector4T<float> Color;
        typedef Vector3T<Scalar> Vector;
        typedef Vector3T<Scalar> Normal;
        typedef Vector3T<Scalar> Point;

        // transformations & other
        typedef ork::quat<Scalar> Quaternion;
        typedef Matrix4T<Scalar> Transform;
        typedef Matrix4T<Scalar> ProjectiveTransform;

    };
}

#endif // EXPRESSIVE_ORK_TRAITS_DEF_H_
