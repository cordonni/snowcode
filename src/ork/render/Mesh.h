/*
 * Ork: a small object-oriented OpenGL Rendering Kernel.
 * Website : http://ork.gforge.inria.fr/
 * Copyright (c) 2008-2015 INRIA - LJK (CNRS - Grenoble University)
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors 
 * may be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*
 * Ork is distributed under the BSD3 Licence. 
 * For any assistance, feedback and remarks, you can check out the 
 * mailing list on the project page : 
 * http://ork.gforge.inria.fr/
 */
/*
 * Main authors: Eric Bruneton, Antoine Begault, Guillaume Piolat.
 */

#ifndef _ORK_MESH_H_
#define _ORK_MESH_H_

#include <cstring> // for memcpy
#include <memory>

#include "ork/render/CPUBuffer.h"
#include "ork/render/GPUBuffer.h"
#include "ork/render/MeshBuffers.h"

namespace ork
{

class FrameBuffer;

/**
 * A MeshBuffers wrapper that provides a convenient API to define the mesh content.
 * @ingroup render
 *
 * @tparam vertex the type of the vertices of this mesh.
 * @tparam index the type of the indices of this mesh.
 */
template <class vertex, class index> class Mesh : public Object
{
public:
    /**
     * Creates a new mesh.
     *
     * @param m how the list of vertices of this mesh must be interpreted.
     * @param usage how the data should be handled.
     * @param vertexCount the initial capacity of the vertex array.
     * @param indiceCount the initial capacity of the indice array.
     */
    Mesh(MeshMode m, MeshUsage usage, int vertexCount = 4, int indiceCount = 4);

    /**
     * Creates a new mesh.
     *
     * @param target the mesh buffers wrapped by this mesh.
     * @param m how the list of vertices of this mesh must be interpreted.
     * @param usage how the data should be handled.
     * @param vertexCount the initial capacity of the vertex array.
     * @param indiceCount the initial capacity of the indice array.
     */
    Mesh(ptr<MeshBuffers> target, MeshMode m, MeshUsage usage, int vertexCount = 4, int indiceCount = 4);

    /**
     * Deletes this mesh.
     */
    virtual ~Mesh();

    /**
     * Returns a new Mesh, copy of this one.
     */
    std::shared_ptr<Mesh<vertex, index> > clone() const;

    /**
     * Makes a deep copy of the data from a given Mesh to this one.
     * @param mesh a Mesh to copy data from.
     */
    void copyDataFrom(const Mesh<vertex, index> &mesh);

    /**
     * Makes a deep copy of the vertices from a given Mesh to this one.
     * @param mesh a Mesh to copy vertices from.
     */

    void copyVerticesFrom(const Mesh<vertex, index> &mesh );

    /**
     * Makes a deep copy of the indices from a given Mesh to this one.
     * @param mesh a Mesh to copy indices from.
     */
    void copyIndicesFrom(const Mesh<vertex, index> &mesh );

    /**
     * Returns the interpretation mode of the vertices of this mesh.
     */
    inline MeshMode getMode() const;

    /**
     * Returns the usage mode of this mesh.
     */
    inline MeshUsage getUsage() const;

    /**
     * Returns the number of vertices in this mesh.
     */
    inline int getVertexCount() const;

    /**
     * Returns a vertex of this mesh.
     *
     * @param i a vertex index.
     */
    inline const vertex& getVertex(int i) const;
    inline vertex& getVertex(int i);

    /**
     * Returns the number of indices of this mesh.
     */
    inline int getIndiceCount() const;

    /**
     * Returns an indice of this mesh.
     *
     * @param i an indice index.
     */
    inline index getIndice(int i) const;

    /**
     * Returns the vertex index used for primitive restart. -1 means no restart.
     */
    inline GLint getPrimitiveRestart() const;

    /**
     * Returns the number of vertices per patch in this mesh, if #getMode() is PATCHES.
     */
    inline GLint getPatchVertices() const;

    /**
     * Returns the MeshBuffers wrapped by this Mesh instance.
     */
    inline ptr<MeshBuffers> getBuffers() const;

    /**
     * Returns the mutex that protects the meshbuffers wrapped in this mesh.
     */
    inline std::mutex& getMutex() const;

    /**
     * Declares an attribute of the vertices of this mesh.
     *
     * @param id a vertex attribute index.
     * @param size the number of components in attributes of this kind.
     * @param type the type of each component in attributes of this kind.
     * @param norm if the attribute components must be normalized to 0..1.
     */
    inline void addAttributeType(int id, int size, AttributeType type, bool norm);

    /**
     * Sets the capacity of the vertex and indice array of this mesh. Does
     * nothing if the provided sizes are smaller than the current ones.
     *
     * @param vertexCount the new vertex array capacity.
     * @param indiceCount the new indice array capacity.
     */
    inline void setCapacity(int vertexCount, int indiceCount);

    /**
     * Adds a vertex to this mesh.
     *
     * @param v a vertex.
     */
    inline void addVertex(const vertex &v);

    /**
     * Adds vertices this mesh.
     *
     * @param v a pointer to a vertex array.
     * @param count number of vertices
     */
    inline void addVertices(const vertex *v, int count);

    /**
     * Adds an indice to this mesh.
     *
     * @param i a vertex index.
     */
    inline void addIndice(index i);

    /**
     * Sets the interpretation mode of the vertices of this mesh.
     */
    inline void setMode(MeshMode mode);

    /**
     * Changes a vertex of this mesh.
     */
    inline void setVertex(int i, const vertex &v);

    /**
     * Changes an indice of this mesh.
     */
    inline void setIndice(int i, index ind);

    /**
     * Sets the vertex index used for primitive restart. -1 means no restart.
     */
    inline void setPrimitiveRestart(GLint restart);

    /**
     * Sets the number of vertices per patch in this mesh, if #getMode() is PATCHES.
     */
    inline void setPatchVertices(GLint vertices);

    /**
     * Removes all the vertices and indices of this mesh.
     */
    inline void clear();

    /**
     * Removes all indices of this mesh.
     */
    inline void clearIndices();

    /**
     * Sets the new number of used vertices. Should only be used for algorithms that _remove_ vertices from this Mesh. Make sure index are still consistent.
     * @param count the new number of vertices that should be used.
     */
    inline void setVertexCount(unsigned int count);

    /**
     * Sets the new number of used indices.
     * @param count the new number of indices that should be used.
     */
    inline void setIndexCount(unsigned int count);

    /**
     * Forces the GPUBuffers to be updated.
     */
    void InvalidateData();

private:
    /**
     * The usage of this mesh.
     */
    MeshUsage usage;

    /**
     * The Buffer containing the vertices data.
     */
    mutable ptr<Buffer> vertexBuffer;

    /**
     * The Buffer containing the indices data.
     */
    mutable ptr<Buffer> indexBuffer;

    /**
     * True if the vertex data has changed since last call to #uploadVertexDataToGPU.
     */
    mutable bool vertexDataHasChanged;

    /**
     * True if the index data has changed since last call to #uploadIndexDataToGPU.
     */
    mutable bool indexDataHasChanged;

    /**
     * True if the CPU or GPU mesh buffers vertices have been created.
     */
    mutable bool verticesCreated;

    /**
     * True if the CPU or GPU mesh buffers indices have been created.
     */
    mutable bool indicesCreated;

    /**
     * How the list of vertices of this mesh must be interpreted.
     */
    MeshMode m;

    /**
     * The vertices of this mesh.
     */
    vertex *vertices;

    /**
     * The capacity of the vertex array.
     */
    int verticesLength;

    /**
     * The actual number of vertices.
     */
    int verticesCount;

    /**
     * The indices of this mesh.
     */
    index *indices;

    /**
     * The capacity of the indice array.
     */
    int indicesLength;

    /**
     * The actual number of indices.
     */
    int indicesCount;

    /**
     * The vertex index used for primitive restart. -1 means no restart.
     */
    GLint primitiveRestart;

    /**
     * The number of vertices per patch in this mesh, if #getMode() is PATCHES.
     */
    GLint patchVertices;

    /**
     * The MeshBuffers wrapped by this Mesh.
     */
    mutable ptr<MeshBuffers> buffers;

    /**
     * Resizes the vertex array to expand its capacity.
     */
    void resizeVertices(int newSize);

    /**
     * Resizes the indice array to expand its capacity.
     */
    void resizeIndices(int newSize);

    /**
     * Creates the CPU of GPU buffers based on the current content of the
     * vertex and indice arrays.
     */
    void createBuffers() const;

    /**
     * Send the vertices to the GPU.
     */
    void uploadVertexDataToGPU(BufferUsage u) const;

    /**
     * Send the indices to the GPU.
     */
    void uploadIndexDataToGPU(BufferUsage u) const;

    friend class FrameBuffer;
};

template<class vertex, class index>
Mesh<vertex, index>::Mesh(MeshMode m, MeshUsage usage, int vertexCount, int indiceCount) :
    Object("Mesh"), usage(usage), vertexBuffer(NULL), indexBuffer(NULL), verticesCreated(false), indicesCreated(false), m(m), buffers(new MeshBuffers())
{
    vertices = new vertex[vertexCount];
    verticesLength = vertexCount;
    verticesCount = 0;
    indices = new index[indiceCount];
    indicesLength = indiceCount;
    indicesCount = 0;
    primitiveRestart = -1;
    patchVertices = 0;
    vertexDataHasChanged = true;
    indexDataHasChanged = true;
}

template<class vertex, class index>
Mesh<vertex, index>::Mesh(ptr<MeshBuffers> target, MeshMode m, MeshUsage usage, int vertexCount, int indiceCount) :
    Object("Mesh"), usage(usage), verticesCreated(false), indicesCreated(false), m(m), buffers(target)
{
    vertices = new vertex[vertexCount];
    verticesLength = vertexCount;
    verticesCount = 0;
    indices = new index[indiceCount];
    indicesLength = indiceCount;
    indicesCount = 0;
    primitiveRestart = -1;
    patchVertices = 0;
    vertexDataHasChanged = true;
    indexDataHasChanged = true;
}

template<class vertex, class index>
Mesh<vertex, index>::~Mesh()
{
    delete[] vertices;
    delete[] indices;
}

template<class vertex, class index>
std::shared_ptr<Mesh<vertex, index> > Mesh<vertex, index>::clone() const
{
    std::shared_ptr<Mesh<vertex, index>  > res = std::make_shared<Mesh<vertex, index> >(m, usage, verticesLength, indicesLength);
    res->copyDataFrom(*this);
    return res;
}



template<class vertex, class index>
void Mesh<vertex, index>::copyDataFrom(const Mesh<vertex, index> &mesh )
{
    copyVerticesFrom(mesh);
    copyIndicesFrom(mesh);

    primitiveRestart = mesh.primitiveRestart;
    patchVertices = mesh.patchVertices;
}

template<class vertex, class index>
void Mesh<vertex, index>::copyVerticesFrom(const Mesh<vertex, index> &mesh )
{
    if(mesh.verticesLength > verticesLength)
    {
        delete[] vertices;
        vertices = new vertex[mesh.verticesLength];
    }

    memcpy(vertices, mesh.vertices, mesh.verticesCount * sizeof(vertex));

    verticesLength = mesh.verticesLength;
    verticesCount = mesh.verticesCount;

    vertexDataHasChanged = true;
    verticesCreated = false;
}

template<class vertex, class index>
void Mesh<vertex, index>::copyIndicesFrom(const Mesh<vertex, index> &mesh )
{
    if(mesh.indicesLength > indicesLength)
    {
        delete[] indices;
        indices = new index[mesh.indicesLength];
    }

    memcpy(indices, mesh.indices, mesh.indicesCount * sizeof(index));

    indicesLength = mesh.indicesLength;
    indicesCount = mesh.indicesCount;

    indexDataHasChanged = true;
    indicesCreated = false;
}

template<class vertex, class index>
MeshMode Mesh<vertex, index>::getMode() const
{
    return m;
}

template<class vertex, class index>
MeshUsage Mesh<vertex, index>::getUsage() const
{
    return usage;
}

template<class vertex, class index>
int Mesh<vertex, index>::getVertexCount() const
{
    return verticesCount;
}

template<class vertex, class index>
const vertex& Mesh<vertex, index>::getVertex(int i) const
{
    return vertices[i];
}

template<class vertex, class index>
vertex &Mesh<vertex, index>::getVertex(int i)
{
    return vertices[i];
}

template<class vertex, class index>
int Mesh<vertex, index>::getIndiceCount() const
{
    return indicesCount;
}

template<class vertex, class index>
index Mesh<vertex, index>::getIndice(int i) const
{
    return indices[i];
}

template<class vertex, class index>
GLint Mesh<vertex, index>::getPrimitiveRestart() const
{
    return primitiveRestart;
}

template<class vertex, class index>
GLint Mesh<vertex, index>::getPatchVertices() const
{
    return patchVertices;
}

template<class vertex, class index>
std::mutex& Mesh<vertex, index>::getMutex() const
{
    return buffers->mutex_;
}

template<class vertex, class index>
void Mesh<vertex, index>::uploadVertexDataToGPU(BufferUsage u) const
{
    ptr<GPUBuffer> vb = vertexBuffer.cast<GPUBuffer>();
    assert(vb != NULL); // check it's a GPU mesh
    vb->setData(verticesCount * sizeof(vertex), vertices, u);
    buffers->nvertices = verticesCount;
    vertexDataHasChanged = false;
}

template<class vertex, class index>
void Mesh<vertex, index>::uploadIndexDataToGPU(BufferUsage u) const
{
    ptr<GPUBuffer> ib = indexBuffer.cast<GPUBuffer>();
    assert(ib != NULL);
    ib->setData(indicesCount * sizeof(index), indices, u);
    buffers->nindices = indicesCount;
    indexDataHasChanged = false;
}

template<class vertex, class index>
ptr<MeshBuffers> Mesh<vertex, index>::getBuffers() const
{
    // (re)create the buffers only if needed
    createBuffers();


    if ((usage == GPU_DYNAMIC) || (usage == GPU_STREAM)) { // upload data to GPU if needed
        BufferUsage u = usage == GPU_DYNAMIC ? DYNAMIC_DRAW : STREAM_DRAW;
        if (vertexDataHasChanged) {
            uploadVertexDataToGPU(u);
        }
        if ((indicesCount != 0) && indexDataHasChanged) {
            uploadIndexDataToGPU(u);
        }
    }

    buffers->primitiveRestart = primitiveRestart;
    buffers->patchVertices = patchVertices;

    return buffers;
}

template<class vertex, class index>
void Mesh<vertex, index>::addAttributeType(int id, int size, AttributeType type, bool norm)
{
    buffers->addAttributeBuffer(id, size, sizeof(vertex), type, norm);
}

template<class vertex, class index>
void Mesh<vertex, index>::setCapacity(int vertexCount, int indiceCount)
{
    resizeVertices(vertexCount);
    resizeIndices(indiceCount);
}

template<class vertex, class index>
void Mesh<vertex, index>::addVertex(const vertex &v)
{
    if (verticesCount == verticesLength) {
        resizeVertices(2 * verticesLength);
    }
    vertices[verticesCount++] = v;
    vertexDataHasChanged = true;
}

template<class vertex, class index>
void Mesh<vertex, index>::addVertices(const vertex *v, int count)
{
    for (int i = 0; i < count; ++i) {
        addVertex(v[i]);
    }
}

template<class vertex, class index>
void Mesh<vertex, index>::addIndice(index i)
{
    if (indicesCount == indicesLength) {
        resizeIndices(2 * indicesLength);
    }
    indices[indicesCount++] = i;
    indexDataHasChanged = true;
}

template<class vertex, class index>
void Mesh<vertex, index>::setMode(MeshMode mode)
{
    m = mode;
}

template<class vertex, class index>
void Mesh<vertex, index>::setVertex(int i, const vertex &v)
{
    vertices[i] = v;
    vertexDataHasChanged = true;
}

template<class vertex, class index>
void Mesh<vertex, index>::setIndice(int i, index ind)
{
    indices[i] = ind;
    indexDataHasChanged = true;
}

template<class vertex, class index>
void Mesh<vertex, index>::setPrimitiveRestart(GLint restart)
{
    primitiveRestart = restart;
}

template<class vertex, class index>
void Mesh<vertex, index>::setPatchVertices(GLint vertices)
{
    patchVertices = vertices;
}

template<class vertex, class index>
void Mesh<vertex, index>::resizeVertices(int newSize)
{

    if(newSize < verticesLength)
    {
        if(newSize < verticesCount)
        {
            verticesCount = newSize;
            vertexDataHasChanged = true;
        }
        return;
    }

    vertex *newVertices = new vertex[newSize];
    memcpy(newVertices, vertices, verticesLength * sizeof(vertex));
    delete[] vertices;
    vertices = newVertices;
    verticesLength = newSize;
    verticesCreated = false;
}

template<class vertex, class index>
void Mesh<vertex, index>::resizeIndices(int newSize)
{
    if(newSize < indicesLength)
    {
        if(newSize < indicesCount)
        {
            indicesCount = newSize;
            indexDataHasChanged = true;
        }
        return;
    }
    index *newIndices = new index[newSize];
    memcpy(newIndices, indices, indicesLength * sizeof(index));
    delete[] indices;
    indices = newIndices;
    indicesLength = newSize;
    indicesCreated = false;
}

template<class vertex, class index>
void Mesh<vertex, index>::clear()
{
    verticesCount = 0;
    indicesCount = 0;
    vertexDataHasChanged = true;
    indexDataHasChanged = true;

    buffers->setIndicesBuffer(NULL);
    verticesCreated = false;
    indicesCreated = false;
}


template<class vertex, class index>
void Mesh<vertex, index>::clearIndices()
{
    indicesCount = 0;
    indexDataHasChanged = true;
    indicesCreated = false;
    buffers->setIndicesBuffer(NULL);
}

template<class vertex, class index>
void Mesh<vertex, index>::setVertexCount(unsigned int count)
{
    resizeVertices(count);
    verticesCount = count;
    verticesCreated = false;
}

template<class vertex, class index>
void Mesh<vertex, index>::setIndexCount(unsigned int count)
{
    resizeIndices(count);
    indicesCount = count;
    indicesCreated = false;

}


template<class vertex, class index>
void Mesh<vertex, index>::InvalidateData()
{
    vertexDataHasChanged = true;
    indexDataHasChanged = true;
}

template<class vertex, class index>
void Mesh<vertex, index>::createBuffers() const
{
    buffers->reset();

    if(!verticesCreated)
    {
        if (usage == GPU_STATIC || usage == GPU_DYNAMIC || usage ==  GPU_STREAM) {
            GPUBuffer *gpub = new GPUBuffer();
            vertexBuffer = ptr<Buffer>(gpub);
            if (usage == GPU_STATIC) {
                uploadVertexDataToGPU(STATIC_DRAW);
            }

        } else if (usage == CPU) {
            CPUBuffer *cpub = new CPUBuffer(vertices);
            vertexBuffer = ptr<Buffer>((Buffer*)cpub);
        }

        assert(buffers->getAttributeCount() > 0);
        for (int i = 0; i < buffers->getAttributeCount(); ++i) {
            buffers->getAttributeBuffer(i)->setBuffer(vertexBuffer);
        }
        verticesCreated = true;
    }

    if (indicesCount != 0 && !indicesCreated) {
        if (usage == GPU_STATIC || usage == GPU_DYNAMIC || usage == GPU_STREAM) {
            GPUBuffer *gpub = new GPUBuffer();
            indexBuffer = ptr<Buffer>(gpub);
            if (usage == GPU_STATIC) {
                uploadIndexDataToGPU(STATIC_DRAW);
            }
        } else if (usage == CPU) {
            CPUBuffer *cpub = new CPUBuffer(indices);
            indexBuffer = ptr<Buffer>(cpub);
        }

        AttributeType type;
        switch (sizeof(index)) {
        case 1:
            type = A8UI;
            break;
        case 2:
            type = A16UI;
            break;
        default:
            type = A32UI;
            break;
        }
        buffers->setIndicesBuffer(new AttributeBuffer(0, 1, type, false, indexBuffer));

        indicesCreated = true;
    }
    buffers->mode = m;
    buffers->nvertices = verticesCount;
    buffers->nindices = indicesCount;
}

}

#endif
