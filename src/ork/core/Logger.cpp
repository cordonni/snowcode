/*
 * Ork: a small object-oriented OpenGL Rendering Kernel.
 * Website : http://ork.gforge.inria.fr/
 * Copyright (c) 2008-2015 INRIA - LJK (CNRS - Grenoble University)
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors 
 * may be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*
 * Ork is distributed under the BSD3 Licence. 
 * For any assistance, feedback and remarks, you can check out the 
 * mailing list on the project page : 
 * http://ork.gforge.inria.fr/
 */
/*
 * Main authors: Eric Bruneton, Antoine Begault, Guillaume Piolat.
 */

#include "ExpressiveRequired.h"
#include "ork/core/Logger.h"

#include <iostream>

#include <cstdarg>
#include <cstdio>

using namespace std;

//#define ENABLE_LOGGERS_AT_STARTUP

namespace ork
{

static_ptr<Logger> Logger::DEBUG_LOGGER(new Logger("DEBUG"));

static_ptr<Logger> Logger::INFO_LOGGER(new Logger("INFO"));

static_ptr<Logger> Logger::WARNING_LOGGER(new Logger("WARNING"));

static_ptr<Logger> Logger::ERROR_LOGGER(new Logger("ERROR"));

#ifdef ENABLE_LOGGERS_AT_STARTUP
#define DEFAULT_LOGGER_STATE true
#else
#define DEFAULT_LOGGER_STATE false
#endif

Logger::Logger(const string &type, FILE *stream) : Object("Logger"), stream_(stream), type(type), next(nullptr), enabled(DEFAULT_LOGGER_STATE)
{
}

Logger::~Logger()
{
    if (Logger::DEBUG_LOGGER.get() == this) {
        Logger::DEBUG_LOGGER = nullptr;
    }
    if (Logger::INFO_LOGGER.get() == this) {
        Logger::INFO_LOGGER = nullptr;
    }
    if (Logger::WARNING_LOGGER.get() == this) {
        Logger::WARNING_LOGGER = nullptr;
    }
    if (Logger::ERROR_LOGGER.get() == this) {
        Logger::ERROR_LOGGER = nullptr;
    }
}

const std::set<std::string> & Logger::get_topics() const
{
    return topics;
}

const std::set<std::string> & Logger::get_muted_topics() const
{
    return muted_topics;
}

void Logger::addTopic(const string &topic)
{
    if (!enabled) {
        enable(true);
    }
    topics.insert(topic);
    if (next != nullptr) {
        next->addTopic(topic);
    }
}

bool Logger::hasTopic(const string &topic)
{
//    return topics.find(topic) != topics.end();
    return enabled && (!isMuted(topic)) && (topics.size() == 0 || topics.find(topic) != topics.end());
}

void Logger::removeTopic(const std::string &topic)
{
    topics.erase(topic);
    if (next != nullptr) {
        next->removeTopic(topic);
    }
}

void Logger::muteTopic(const std::string &topic)
{
    muted_topics.insert(topic);

    if (next != nullptr) {
        next->muteTopic(topic);
    }
}

void Logger::unmuteTopic(const std::string &topic)
{
    muted_topics.erase(topic);
    if (next != nullptr) {
        next->unmuteTopic(topic);
    }
}

bool Logger::isMuted(const std::string &topic)
{

    return muted_topics.find(topic) != muted_topics.end();
}

void Logger::LOG_DEBUG(const std::string &topic, const std::string &message)
{
#ifndef NDEBUG
    DEBUG_LOGGER->log(topic, message);
#else
    UNUSED(topic);
    UNUSED(message);
#endif // NDEBUG
}

void Logger::LOG_DEBUG(const std::string &topic, const char *msg, ...)
{
#ifndef NDEBUG
    static const int MAX_LOG_SIZE = 512;
    char buf[MAX_LOG_SIZE];

    va_list vl;
    va_start(vl, msg);
    vsnprintf(buf, MAX_LOG_SIZE, msg, vl);
    va_end(vl);
    //std::string msgStr = buf;
    DEBUG_LOGGER->log(topic, buf);
#else
    UNUSED(topic);
    UNUSED(msg);
#endif // NDEBUG
}

void Logger::log(const string &topic, const string &msg)
{
    if (hasTopic(topic)) {
        std::lock_guard<std::mutex> mlock(mutex);
//        cerr << type << " [" << topic << "] " << msg << endl;
        if (stream_ != NULL) {
            FILE *s = stream_;
            fprintf(s, "-[%s][%s] %s\n", type.c_str(), topic.c_str(), msg.c_str());
            fflush(s);
        }
        printf("+[%s][%s] %s\n", type.c_str(), topic.c_str(), msg.c_str());
    }
}

void Logger::logf(const char * topic, const char *fmt, ...)
{
    static const int MAX_LOG_SIZE = 512;
    char buf[MAX_LOG_SIZE];

    va_list vl;
    va_start(vl, fmt);
    vsnprintf(buf, MAX_LOG_SIZE, fmt, vl);
    va_end(vl);
    std::string topicStr = topic;
    std::string msgStr = buf;
    log(topicStr, msgStr);
}

void Logger::flush()
{
    fflush(stream_);
}

void Logger::enable(bool enabled)
{
    this->enabled = enabled;
    if (next != nullptr) {
        next->enable(enabled);
    }
}

}
