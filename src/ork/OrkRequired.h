#ifndef ORKREQUIRED_H
#define ORKREQUIRED_H

#if !defined(BUILD_STATIC) && (defined ( _WIN32 ) || defined( _WIN64 ))
#   if defined( BUILD_ORK )
#        define ORK_API __declspec( dllexport )
#        define ORK_TEMPLATE_IMPORT
#    else
#        define ORK_API __declspec( dllimport )
#        define ORK_TEMPLATE_IMPORT extern
#    endif
#else //Probably Linux
#   ifdef __APPLE__
#       define ORK_API
#        define ORK_TEMPLATE_IMPORT
#   else // Linux probably
#        define ORK_API
#        define ORK_TEMPLATE_IMPORT
#   endif
#endif


//////////////////////
// Macro definition //
//////////////////////
#ifndef UNUSED
#define UNUSED(x) ( (void)(x) ) // Especially to prevent the unused variable warning with GCC
#endif

#endif // ORKREQUIRED_H
