
#include <expressiveLab/main.h>
#include <expressiveLab/QmlView.h>

#include "ork/resource/XMLResourceLoader.h"

#include "core/scenegraph/TerrainNode.h"

#include "landscape/Grid/TerrainGrid.h"
#include "landscape/Grid/TerrainOperations.h"
#include "landscape/Grid/ExpressiveSimulationManager.h"

#include "landscape/Grid/CUDA/StochasticSimulationCUDA.h"

#include "landscape/Grid/Snow/SnowDataIO.h"
#include "landscape/Grid/Snow/RegisterSnow.h"

#include "landscape/User/LandscapeCameraManipulator.h"
#include "landscape/User/LandscapeCameraTask.h"
#include "landscape/Utils/misc.h"

#include "pluginTools/gui/qml/QmlTypeRegisterer.h"


#include <fstream>

using namespace ork;
using namespace expressive;
using namespace expressive::core;
using namespace expressive::plugintools;

using namespace std;

void snowScene(const ExpressiveApplication & /*mainApp*/, std::shared_ptr<GLExpressiveEngine> engine)
{
    Logger::WARNING_LOGGER->muteTopic("OPENGL");


    engine->resource_loader()->addPath("../../resources/shaders/snow/");
    engine->resource_loader()->addPath("../../resources/textures/snow/");
    engine->resource_loader()->addPath("../../resources/methods/snow/");
    engine->resource_loader()->addPath("../../resources/meshes/snow/");
 engine->resource_loader()->addArchive("../../resources/textures/snow/snow.xml");

    ork::ptr<ork::Module> module_background = engine->resource_manager()->loadResource("background/background").cast<ork::Module>();
    ork::ptr<ork::Module> module_terrain = engine->resource_manager()->loadResource("terrainGrid/terrainGrid").cast<ork::Module>();
    ork::ptr<ork::Module> module_fog = engine->resource_manager()->loadResource("deferredAO/deferredFog").cast<ork::Module>();
    ork::ptr<ork::Module> module_ao = engine->resource_manager()->loadResource("deferredAO/deferredAO").cast<ork::Module>();

    // methods
    ork::ptr<ork::Method> method_camera  = new ork::Method(engine->resource_manager()->loadResource("cameraMethodAO").cast<ork::TaskFactory>());
    ork::ptr<ork::Method> method_terrain = new ork::Method(engine->resource_manager()->loadResource("terrainGridMethod").cast<ork::TaskFactory>());

    std::shared_ptr<landscape::LandscapeCameraManipulator> camera_manip = std::make_shared<landscape::LandscapeCameraManipulator>(engine->camera_manager().get());
    camera_manip->resetViewNear();
    engine->scene_manipulator()->set_camera_manipulator(camera_manip);

    ork::ptr<landscape::LandscapeCameraTask> cameraSimuTask = new landscape::LandscapeCameraTask(engine.get());
    camera_manip->setScreenBufferHandler(cameraSimuTask.get());

    engine->camera_manager()->internal_node()->addMethod("draw", method_camera);
    engine->camera_manager()->internal_node()->addMethod("simu", new ork::Method(cameraSimuTask));
    engine->camera_manager()->internal_node()->addModule("fog", module_fog);
    engine->camera_manager()->internal_node()->addModule("ao", module_ao);
    engine->camera_manager()->internal_node()->addFlag("simu");

    // remove scene elements
    engine->scene_manager()->RemoveOverlay(engine->scene_manager()->get_overlay("virtualplane")->id());

    // background
    std::shared_ptr<core::SceneNode> background = engine->scene_manager()->get_overlay("backgroundOverlay");
    background->SetModule(module_background);

    /// TERRAIN

    // terrain scene node
    std::shared_ptr<landscape::TerrainGrid> terrain = std::make_shared<landscape::TerrainGrid>(engine->scene_manager().get());

    // make sure the terrain has no manipulator
    terrain->manipulator() = std::shared_ptr<core::SceneNodeManipulator>();

    // change standart data
    terrain->internal_node()->addMethod("draw", method_terrain);
    terrain->internal_node()->addModule("material", module_terrain);

    terrain->internal_node()->removeValue("hovered");
    terrain->internal_node()->removeValue("selected");

    landscape::GridDEM dem  = landscape::TerrainOperations::loadTER("../../resources/heightfields/alpine-256.ter");

    // add terrain to scene
    engine->scene_manager()->AddNode(terrain);

    //////////  Simulation setup //////////

    // set simulation method
    terrain->internal_node()->addFlag("simu");

    std::shared_ptr<landscape::StochasticSimulationCUDA> simu_cuda = std::make_shared<landscape::StochasticSimulationCUDA>();
    simu_cuda->setTempBufferCount(4);

    ork::ptr<landscape::ExpressiveSimulationManager> simuMgr =
            new landscape::ExpressiveSimulationManager(engine.get(), terrain.get(), dem, 10.0f,
                                                     std::make_shared<landscape::SimulationData<landscape::SnowData, landscape::SnowDataIO>>(),
                                                     simu_cuda);
    simuMgr->setMaxFrame(100);


    landscape::registerSnowEvents();
    landscape::registerSnowLayers(simuMgr.get());
    landscape::registerSnowParameters(simuMgr.get());
    engine->qml_context()->setContextProperty("simulationManager", simuMgr.get());

    terrain->internal_node()->addMethod("simu", new ork::Method(simuMgr));

}

int snowMain(int argc, char *argv[])
{
    ExpressiveApplication app(argc, argv);

    QmlView view(app, snowScene, "../../resources/gui/QmlMainWindow.qml");
    view.resize(900, 700);
    view.raise();
    view.show();

    return app.exec();
}

static MainFunction snowDefine("snow", snowMain);
static MainFunction snowDefine2("expressiveLab", snowMain);

