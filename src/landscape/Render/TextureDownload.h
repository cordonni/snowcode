/**
Copyright (c) 2017, INPG
Main authors : Guillaume Cordonnier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef TEXTUREDOWNLOAD_H
#define TEXTUREDOWNLOAD_H

#include "landscape/LandscapeRequired.h"

#include "core/VectorTraits.h"

#include "ork/core/Object.h"
#include "ork/render/Types.h"

#include <GL/glew.h>

#include <vector>

namespace ork
{
class Texture;
class FrameBuffer;

}

namespace expressive {

namespace landscape {

class TextureDownload
{
public:
    TextureDownload(ork::ptr<ork::Texture>, GLsizeiptr, bool fb=false);
    ~TextureDownload();

    GLubyte* operator () (ork::TextureFormat format, ork::PixelType pixel);
    GLubyte* readPixels(int x, int y, int w, int h, ork::TextureFormat f, ork::PixelType t, bool clamp=false);

    void setFrameBufferViewport(Vector4i);

private:
    ork::ptr<ork::Texture> tex_;
    ork::ptr<ork::FrameBuffer> fb_;

    GLuint buffers_id_[2];

    GLubyte* data_;
    GLsizeiptr data_sz_;
};

}
}

#endif // TEXTUREDOWNLOAD_H
