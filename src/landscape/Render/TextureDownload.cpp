#include "TextureDownload.h"

#include "ork/render/Texture.h"
#include "ork/render/Texture2D.h"
#include "ork/render/FrameBuffer.h"
#include "ork/render/CPUBuffer.h"

#include "landscape/Utils/Profile.h"

#include <iostream>

namespace expressive {
namespace landscape {

// a buffer that dooes nothing to be use with framebuffer readPixels
class TDBuffer : public ork::Buffer
{
public:
    TDBuffer() {}
    ~TDBuffer() {}

    void bind(int) const override final {}
    void *data(int) const override final  {return 0;}
    void unbind(int) const override final  {}

    void dirty() const override final  {}
};


TextureDownload::TextureDownload(ork::ptr<ork::Texture> tex, GLsizeiptr data_sz, bool fb)
    : tex_{tex}, data_sz_{data_sz}
{
    // init buffers
    glGenBuffers(2, buffers_id_);

    glBindBuffer(GL_PIXEL_PACK_BUFFER, buffers_id_[0]);
    glBufferData(GL_PIXEL_PACK_BUFFER, data_sz, 0, GL_DYNAMIC_READ);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, buffers_id_[1]);
    glBufferData(GL_PIXEL_PACK_BUFFER, data_sz, 0, GL_DYNAMIC_READ);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

    assert(!ork::FrameBuffer::getError());

    // allocate the CPU data on our side

    data_ = new GLubyte[data_sz];

    if(fb)
    {
        fb_ = new ork::FrameBuffer();
        fb_->setTextureBuffer(ork::COLOR0, tex_.cast<ork::Texture2D>(), 0);
        fb_->setReadBuffer(ork::COLOR0);
    }
}

TextureDownload::~TextureDownload()
{
    delete [] data_;
    glDeleteBuffers(2, buffers_id_);
}

GLubyte* TextureDownload::operator () (ork::TextureFormat format, ork::PixelType pixel)
{
    // request download of texture in buffer 0
    glBindBuffer(GL_PIXEL_PACK_BUFFER, buffers_id_[0]);
    tex_->getImage(0, format, pixel, 0);


    // bind buffer 1 to get its data
    glBindBuffer(GL_PIXEL_PACK_BUFFER, buffers_id_[1]);

    GLubyte* cur = (GLubyte*)glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);

    if(cur)
    {
        memcpy(data_, cur, data_sz_);
        glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
    }
    else
        std::cerr << "Error in texture download: buffer map failed\n";

    glBindBuffer(GL_PIXEL_PACK_BUFFER,0);

    //swap buffers
    std::swap(buffers_id_[0], buffers_id_[1]);

    assert(!ork::FrameBuffer::getError());

    return data_;
}

void TextureDownload::setFrameBufferViewport(Vector4i v)
{
    assert(fb_);
    fb_->setViewport(v);
}

GLubyte* TextureDownload::readPixels(int x, int y, int w, int h, ork::TextureFormat f, ork::PixelType t, bool clamp)
{
    assert(fb_);


    // request download of texture in buffer 0
    glBindBuffer(GL_PIXEL_PACK_BUFFER, buffers_id_[0]);

    fb_->readPixels(x, y, w, h, f, t, ork::Buffer::Parameters(), TDBuffer(), clamp);

    // bind buffer 1 to get its data
    glBindBuffer(GL_PIXEL_PACK_BUFFER, buffers_id_[1]);

    GLubyte* cur = (GLubyte*)glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);

    if(cur)
    {
        memcpy(data_, cur, data_sz_);
        glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
    }
    else
        std::cerr << "Error in texture download: buffer map failed\n";

    glBindBuffer(GL_PIXEL_PACK_BUFFER,0);

    //swap buffers
    std::swap(buffers_id_[0], buffers_id_[1]);

    assert(!ork::FrameBuffer::getError());

    return data_;
}


}
}
