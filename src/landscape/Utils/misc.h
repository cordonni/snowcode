/**
Copyright (c) 2017, INPG
Main authors : Guillaume Cordonnier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef MISC_H
#define MISC_H

#include <type_traits>
#include <iostream>
#include "core/VectorTraits.h"

namespace expressive {
namespace landscape {

// misc helpers

#define REPEAT(n) \
    for(unsigned int csosqjdknqkdqzku = 0; csosqjdknqkdqzku<(unsigned int)(n); ++csosqjdknqkdqzku)

#define CLAMP(V, MN, MX) std::min(std::max((V), (MN)), (MX))

template<class A, class F>
    A mix(const A& a, const A& b, const F& f)
    {
        return a * (F(1) - f) + b*f;
    }

inline void debug_print()
{
    std::cerr << std::endl;
}

template<typename T, typename... Targs>
void debug_print(T value, Targs... Fargs)
{
    std::cerr << value << ' ';
    debug_print(Fargs...);
}


template <class T>
void debug_print_vec(const T& vec)
{
    for(auto elem : vec)
        std::cerr << elem << ' ';
    std::cerr << std::endl;
}

template <class U, class T>
struct VecPrint_t
{
public:
    VecPrint_t(const U& t) : obj(t) {}
    const U& get() {return obj;}
private:
    const U& obj;
};


namespace ns_dbg_helper
{

class no_type;

template <class>
static constexpr auto get_type_value (long) -> no_type;

template <class T>
static constexpr auto get_type_value (int) -> typename T::value_type;

template <class T, class F>
struct Has_value_type : std::true_type {};

template <class T>
struct Has_value_type<T, no_type> :  std::false_type {};


template <class T>
constexpr bool has_value_type () {return  Has_value_type<T, decltype(get_type_value<T> (0))>::value;}

template<bool b, class C>
using Enable_if = typename std::enable_if<b,C>::type;
}

template <class U, class T>
ns_dbg_helper::Enable_if<!ns_dbg_helper::has_value_type<U>(), std::ostream&> operator << (std::ostream& out, VecPrint_t<U, T> u)
{
    constexpr unsigned int inter = sizeof(U) / sizeof(T);

    out << '(';

    const T* ptr = reinterpret_cast<const T*>(&u.get());
    for(unsigned int i = 0; i<inter;)
    {
        out << *(ptr+i);
        out << ((++i==inter)?')' : ',');
    }

    return out;
}

template <class U>
std::ostream& operator << (std::ostream& out, VecPrint_t<U, typename U::value_type> u)
{
    out << '(';

    for(auto it = u.get().begin(); it != u.get().end();)
    {
        out << *it;
        out << ((++it == u.get().end())?')' : ';');
    }

    return out;
}

template <class U, class T>
VecPrint_t<U, T> VecPrint(const U& u){return VecPrint_t<U, T>(u);}

template <class U>
VecPrint_t<U,typename U::value_type> VecPrint(const U& u){return VecPrint_t<U, typename U::value_type>(u);}

template <class U>
VecPrint_t<U,typename U::Scalar> VecPrint(const U& u){return VecPrint_t<U, typename U::Scalar>(u);}


struct PairHash {
public:
  template <typename T, typename U>
  std::size_t operator()(const std::pair<T, U> &x) const
  {
    return std::hash<T>()(x.first) ^ std::hash<U>()(x.second);
  }
};

inline Color invGamma(Color c)
{
    return Color(std::pow(c[0], 2.2), std::pow(c[1], 2.2), std::pow(c[2], 2.2));
}

}
}
#endif // MISC_H
