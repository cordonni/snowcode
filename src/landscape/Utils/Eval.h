#ifndef EVAL_H
#define EVAL_H

#include <string>
#include <iostream>

#define EVAL(type_name, var) class type_name; Eval<type_name, decltype(var)> eval_scope_var_ ## var ## _sjflfoe (#var, var)
#define EVAL_COUNT(type_name, var, count) class type_name; Eval<type_name, decltype(var), count> eval_scope_var_ ## var ## count ## _sjflfoe (#var, var)
#define EVAL_DBG0(var, count) EVAL_COUNT(Tag_DBG0_EVAL, var, count)
#define EVAL_DBG1(var, count) EVAL_COUNT(Tag_DBG1_EVAL, var, count)
#define EVAL_DBG2(var, count) EVAL_COUNT(Tag_DBG2_EVAL, var, count)
#define EVAL_DBG3(var, count) EVAL_COUNT(Tag_DBG3_EVAL, var, count)
class Tag_DBG0_EVAL;
class Tag_DBG1_EVAL;
class Tag_DBG2_EVAL;
class Tag_DBG3_EVAL;


template<typename id, class Var, int count = 1>
class Eval
{
public:
    Eval(std::string name, Var v) :
        name_ {name}
    {
        min_ = cur_count ? std::min(min_, v) : v;
        max_ = cur_count ? std::max(max_, v) : v;
        sum_ = cur_count ? sum_ + v : v;

        if(++cur_count == count)
        {
            std::cerr << "Eval " << name_
                      << ": min = " << min_
                      << ", max = " << max_
                      << ", avg = " << sum_ / double(count)
                      << std::endl;
            cur_count = 0;
        }
    }

private:
    std::string name_;

    static int cur_count;
    static Var min_, max_ , sum_;

};

template<typename id, class Var, int count>
int Eval<id, Var, count>::cur_count = 0;

template<typename id, class Var, int count>
Var Eval<id, Var, count>::min_ =  Var();

template<typename id, class Var, int count>
Var Eval<id, Var, count>::max_ =  Var();

template<typename id, class Var, int count>
Var Eval<id, Var, count>::sum_ =  Var();

#endif // EVAL_H
