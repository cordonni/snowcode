/**
Copyright (c) 2017, INPG
Main authors : Guillaume Cordonnier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GLPROFILE_H
#define GLPROFILE_H

#include "Profile.h"

#include <GL/glew.h>

// profiler for GPU commands
// see Profile.h for exemples
// warning: the result make take some time to travel back to the GPU.
// although the profiling is accurate, it can cause an overall performance issue

#define PROFILE_GL(type_name, name) class type_name; ProfileGL<type_name> profile_GPU_scope_var_ ## type_name ## _sjflfo (name)
#define PROFILE_GL_COUNT(type_name, name, count) class type_name; ProfileGL<type_name, count> profile_GPU_scope_var_ ## type_name ## count ## _sjflfo (name)

namespace ns_profile_types {

class TimerOpenGL
{
public:
    TimerOpenGL(){}

    void start()
    {
        glGenQueries(1, &querry_id_);
        glBeginQuery(GL_TIME_ELAPSED, querry_id_);
    }
    double get_ms()
    {
        GLuint ns_elapsed;
        glEndQuery(GL_TIME_ELAPSED);
        glGetQueryObjectuiv(querry_id_, GL_QUERY_RESULT, &ns_elapsed);

        return (double)ns_elapsed / 1000000.0;
    }

private:
    GLuint querry_id_;
};

}

template <typename id, int count = 10>
using ProfileGL = Profile_t<id, ns_profile_types::TimerOpenGL, count>;

#endif // GLPROFILE_H
