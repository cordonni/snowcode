#ifndef LANDSCAPECAMERATASK_H
#define LANDSCAPECAMERATASK_H

#include "landscape/LandscapeRequired.h"
#include "ork/scenegraph/AbstractTask.h"

#include "core/ExpressiveEngine.h"

namespace expressive {
namespace landscape {

class TextureDownload;


class LANDSCAPE_API LandscapeCameraTask : public ork::AbstractTask
{
public:
    LandscapeCameraTask(core::ExpressiveEngine*);

    virtual ork::ptr<ork::Task> getTask(ork::ptr<ork::Object> context);

    Vector screenToWorld(Point2i);

protected:

    class Impl : public ork::Task
    {
    public:
        Impl(LandscapeCameraTask *owner);

        virtual ~Impl() {}

        virtual bool run();

        LandscapeCameraTask* owner_;
    };
    void helper();

    core::ExpressiveEngine* engine;
    Vector4f* screen_buffer_;
    std::shared_ptr<TextureDownload> posBufferDownload_;
    Vector4i lastViewPort;
};

}
}

#endif // LANDSCAPECAMERATASK_H
