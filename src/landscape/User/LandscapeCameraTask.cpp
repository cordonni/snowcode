#include "LandscapeCameraTask.h"

#include "landscape/Render/TextureDownload.h"
#include "ork/render/Texture2D.h"
#include "ork/render/FrameBuffer.h"

namespace expressive {
namespace landscape {

LandscapeCameraTask::LandscapeCameraTask(core::ExpressiveEngine* e):
    ork::AbstractTask("GeologyCameraTask"),
    engine(e),
    screen_buffer_(nullptr),
    lastViewPort{Vector4i::Zero()}
{

}

Vector LandscapeCameraTask::screenToWorld(Point2i p)
{

    if(!screen_buffer_ || p[0] < lastViewPort[0] || p[0] >= lastViewPort[2] ||
            p[1] < lastViewPort[1] || p[1] >= lastViewPort[3])
        return Vector();
    Vector4f wpos = screen_buffer_[p[0] + (lastViewPort[3]-1-p[1]) * (lastViewPort[2]-lastViewPort[0])];
    return wpos.cast<double>().xyz();
}

ork::ptr<ork::Task> LandscapeCameraTask::getTask(ork::ptr<ork::Object> /*context*/)
{
    return new Impl(this);
}

LandscapeCameraTask::Impl::Impl(LandscapeCameraTask *owner) :
    ork::Task("GeologyCamera", true, 0),
    owner_(owner)
{
}

bool LandscapeCameraTask::Impl::run()
{
    owner_->helper();
    return true;
}

void LandscapeCameraTask::helper()
{

    static int frame_c = 0;
    if(frame_c++ <2)
        return;
    frame_c = 0;

    Vector4i vp = engine->camera_manager()->framebuffer()->getViewport();

    if(!posBufferDownload_ || lastViewPort != vp)
    {
        lastViewPort = vp;
        posBufferDownload_ = std::make_shared<TextureDownload>(engine->resource_manager()->loadResource("offscreenPos").cast<ork::Texture2D>(),
                                                               4*sizeof(float) * (vp[2]-vp[0])*(vp[3]-vp[1]));

    }

    screen_buffer_ = reinterpret_cast<Vector4f*>((*posBufferDownload_)(ork::RGBA, ork::FLOAT));

}


}
}
