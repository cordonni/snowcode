#include "LandscapeCameraManipulator.h"
#include "LandscapeCameraTask.h"

#include "landscape/Render/TextureDownload.h"

#include <pluginTools/inputcontroller/MouseInputController.h>
#include <pluginTools/inputcontroller/KeyboardInputController.h>

#include <ork/render/FrameBuffer.h>

#include "ork/scenegraph/ShowInfoTask.h"
#include "core/utils/BasicsToXml.h"
#include "core/utils/XmlToBasics.h"

#include <QCursor>

namespace expressive {
namespace landscape {

LandscapeCameraManipulator::LandscapeCameraManipulator(core::Camera* camera_manager) :
    plugintools::CameraManipulator(camera_manager),
    geo_camera_mode_(E_ManipulationMode),
    buffer_handler_{nullptr},
    mousePos_ {Point2::Zero()},
    manipCenter_{Vector::Zero()},
    leftMouseCliked_(false),
    file_path_("../../resources/xmlExports/landscape/camera.xml")
{
    default_speed_ = 50000.0;


    camera_manager_->set_far_clip_distance(500000.0);
    camera_manager_->set_near_distance(100.0);

    resetView();
}

void LandscapeCameraManipulator::resetView()
{
    camera_manager_->SetPos(0.0, 50000.0, 100000.0);
    camera_manager_->LookAt(Point(0, 0, 0));

    set_camera_speed(default_speed_);
}

void LandscapeCameraManipulator::setScreenBufferHandler(LandscapeCameraTask* bh)
{
    buffer_handler_ = bh;
}

Vector LandscapeCameraManipulator::getPointed()
{
    Vector v = buffer_handler_->screenToWorld(mousePos_.cast<int>());
    // ensure that we are not pointing toward the sky
    if(std::abs(v.norm()-1) > 0.1)
    {
        return v;
    }

    return Vector();
}

Vector LandscapeCameraManipulator::getLastPointed()
{
    return manipCenter_;
}

bool LandscapeCameraManipulator::isLeftMouseClicked()
{
    return leftMouseCliked_;
}

void LandscapeCameraManipulator::resetViewNear()
{
    camera_manager_->SetPos(0.0, 5000.0, 10000.0);
    camera_manager_->LookAt(Point(0, 0, 0));

    set_camera_speed(default_speed_);
}

void LandscapeCameraManipulator::updateInfo()
{
    std::stringstream ss;
    switch (geo_camera_mode_) {
    case E_ManipulationMode: ss << "Manip cam"; break;
    case E_FlyMode: ss << "Fly cam"; break;
    case E_FpsMode: ss << "FPS cam"; break;
    default: ss << "ERR cam"; break;
    }
    ss << " (" << move_speed_ << ")";

    ork::ShowInfoTask::setInfo("camera", ss.str());
}

void LandscapeCameraManipulator::terrainSizeChanged(double newSize)
{
    camera_manager_->internal_node()->addValue(new ork::Value1f("ao_scale", (float)newSize));
    camera_manager_->internal_node()->addValue(new ork::Value1f("fog_scale", (float)newSize));
}

void LandscapeCameraManipulator::setGeoCameraMode(ELandscapeCameraMode mode)
{
    geo_camera_mode_ = mode;

    // specific actions
    switch (mode)
    {
    case E_ManipulationMode:
        setGroundBehavior(EGroundMode::None);
        break;
    case E_FlyMode:
        setGroundBehavior(EGroundMode::Limit);
        break;
    case E_FpsMode:
        setGroundBehavior(EGroundMode::Glue);
        break;
    default:
        break;
    }
}


bool LandscapeCameraManipulator::redisplay(double t, double dt)
{
    bool res = CameraManipulator::redisplay(t, dt);
    updateInfo();

    // manipulation mode: recompute target
    if(geo_camera_mode_ == E_ManipulationMode && !moving_)
    {

        Vector v = buffer_handler_->screenToWorld(mousePos_.cast<int>());
        // ensure that we are not pointing toward the sky
        if(std::abs(v.norm()-1.0f) > 0.1)
        {
            manipCenter_ =  v;
        }
    }

    return res;
}



bool LandscapeCameraManipulator::wheelEvent(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    // Check MouseIC and SceneNode
    std::shared_ptr<plugintools::MouseIC> e = std::static_pointer_cast<plugintools::MouseIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    Vector dir = (camera_manager_->GetPointInWorld()-manipCenter_);

    Scalar speed = std::min(0.9, e->wheel_delta() / 10.0);

    if (slow_mode_) {
        speed /= 10.f;
    }

    camera_manager_->Translate(speed * dir, core::SceneNode::TS_WORLD);

    return true;
}

bool LandscapeCameraManipulator::mousePressed(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n) {
    // Check MouseIC and SceneNode
    std::shared_ptr<plugintools::MouseIC> e = std::static_pointer_cast<plugintools::MouseIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    // Register 3D Points
    e->allInput2DTo3D(camera_manager_, camera_manager_->target(), true);
    // Begin Camera Motion
    if(e->button() == Qt::LeftButton)
        leftMouseCliked_ = true;
    else
    {
        moving_ = true;
    }
    return true;
}


bool LandscapeCameraManipulator::mouseMoved(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    // Check MouseIC and SceneNode
    std::shared_ptr<plugintools::MouseIC> e = std::static_pointer_cast<plugintools::MouseIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    // Register 3D Points
    e->allInput2DTo3D(camera_manager_, manipCenter_, true);
    mousePos_ = e->get2DInput();

    auto p = e->get2DDelta();

    if(!moving_)
        return true;

    switch (geo_camera_mode_)
    {
    case E_ManipulationMode:

        if(e->button() == Qt::RightButton)
        {
            if(abs(p[0]) > abs(p[1]))
                camera_manager_->Rotate(Vector::UnitY(), manipCenter_, -p[0] * 0.01, core::SceneNode::TS_WORLD);
            else
                camera_manager_->Rotate(camera_manager_->GetRealRight(), manipCenter_, -p[1] * 0.01, core::SceneNode::TS_WORLD);
        }
        else if(e->button() == Qt::MiddleButton)
        {
            camera_manager_->Translate(-e->get3DDelta(), core::SceneNode::TS_WORLD);
        }
        break;

    case E_FlyMode:
    case E_FpsMode:

        if(e->button() == Qt::RightButton)
        {
            if (cam_rot_mode_ == ECamRotationMode::E_AxisLockedRotationMode)
                p[abs(p[0]) > abs(p[1])?1:0] = 0.0;
            p*=0.01;

            camera_manager_->Yaw  (-p[0], core::SceneNode::TS_WORLD);
            camera_manager_->Pitch( -p[1]);
        }

        break;

    default:
        break;
    }
    return true;
}

bool LandscapeCameraManipulator::mouseReleased(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n) {
    // Check MouseIC and SceneNode
    std::shared_ptr<plugintools::MouseIC> e = std::static_pointer_cast<plugintools::MouseIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    // Stop Camera Motion
    if(e->button() == Qt::LeftButton)
        leftMouseCliked_ = false;
    else
        moving_ = false;

    return true;
}

// Keyboard


bool LandscapeCameraManipulator::keyPressed(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    // Check KeyboardIC and SceneNode
    std::shared_ptr<plugintools::KeyboardIC> e = std::static_pointer_cast<plugintools::KeyboardIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    switch (e->key()) {

    case Qt::Key_Comma:
        resetView();
        break;
    case Qt::Key_Enter:
        setGeoCameraMode(E_ManipulationMode);
        break;
    case Qt::Key_Home:
        setGeoCameraMode(E_FlyMode);
        break;
    case Qt::Key_End:
        setGeoCameraMode(E_FpsMode);
        break;
    case Qt::Key_Asterisk:
        move_speed_ *=1.6;
        break;
    case Qt::Key_Slash:
        move_speed_ *=0.625;
        break;
    case Qt::Key_0:
        move_speed_ = default_speed_;
        break;
    case Qt::Key_1:
        save();
        break;
    case Qt::Key_2:
        load();
        break;

    case Qt::Key_Control:
        slow_mode_ = true;
        return false;
    case Qt::Key_Alt:
        world_translation_mode_ = true;
        return false;
    case Qt::Key_Up:
        //case Qt::Key_Z:
        key_direction_ = (EMovingDirection) (key_direction_ | E_FORWARD);
        return true;
    case Qt::Key_Down:
        //case Qt::Key_S:
        key_direction_ = (EMovingDirection) (key_direction_ | E_BACKWARD);
        return true;
    case Qt::Key_Left:
        //case Qt::Key_Q:
        key_direction_ = (EMovingDirection) (key_direction_ | E_LEFT);
        return true;
    case Qt::Key_Right:
        //case Qt::Key_D:
        key_direction_ = (EMovingDirection) (key_direction_ | E_RIGHT);
        return true;
    case Qt::Key_PageUp:
        key_direction_ = (EMovingDirection) (key_direction_ | E_UP);
        return true;
    case Qt::Key_PageDown:
        key_direction_ = (EMovingDirection) (key_direction_ | E_DOWN);
        return true;
    default:
        break;
    }

    return true;
}

bool LandscapeCameraManipulator::keyReleased(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    std::shared_ptr<plugintools::KeyboardIC> e = std::static_pointer_cast<plugintools::KeyboardIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    switch(e->key()) {
    case Qt::Key_Control:
        slow_mode_ = false;
        return true;
    case Qt::Key_Alt:
        world_translation_mode_ = false;
        return true;
    case Qt::Key_Up:
    case Qt::Key_Z:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_FORWARD);
        return true;
    case Qt::Key_Down:
    case Qt::Key_S:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_BACKWARD);
        return true;
    case Qt::Key_Left:
    case Qt::Key_Q:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_LEFT);
        return true;
    case Qt::Key_Right:
    case Qt::Key_D:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_RIGHT);
        return true;
    case Qt::Key_PageUp:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_UP);
        return true;
    case Qt::Key_PageDown:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_DOWN);
        return true;
    }

    return false;
}

//// Save:
///
// default_speed_ = 50000.0;
// camera_manager_->set_far_clip_distance(500000.0);
// camera_manager_->set_near_distance(1.0);
// fov

// pos
// look_front
// look_up
// speed
// mode

void LandscapeCameraManipulator::save(std::string fname)
{
    if(fname.empty())
        fname = file_path_;

    TiXmlDocument doc;
    doc.LinkEndChild(new TiXmlDeclaration( "1.0", "", "" ));

    TiXmlElement * cam = new TiXmlElement("camera");
    cam->SetDoubleAttribute("fov",camera_manager_->fov());
    cam->SetDoubleAttribute("near",camera_manager_->near_distance());
    cam->SetDoubleAttribute("far",camera_manager_->far_clip_distance());
    doc.LinkEndChild(cam);

    TiXmlElement * manip = new TiXmlElement("manip");
    manip->SetDoubleAttribute("default_speed",default_speed_);
    manip->SetDoubleAttribute("speed",move_speed_);
    manip->SetAttribute("mode",(int)geo_camera_mode_);
    doc.LinkEndChild(manip);

    Vector4 pos = camera_manager_->GetLocalToWorld() * Vector4(0.0,0.0,0.0, 1.0);
    Vector4 front = camera_manager_->GetLocalToWorld() * Vector4(0.0,0.0,-1.0, 0.0);
    Vector4 up = camera_manager_->GetLocalToWorld() * Vector4(0.0,1.0,0.0, 0.0);

    doc.LinkEndChild(core::PointToXml(Vector(pos[0], pos[1], pos[2]), "pos"));
    doc.LinkEndChild(core::PointToXml(Vector(front[0], front[1], front[2]), "front"));
    doc.LinkEndChild(core::PointToXml(Vector(up[0], up[1], up[2]), "up"));

    doc.SaveFile(fname);
}

void LandscapeCameraManipulator::load(std::string fname)
{
    if(fname.empty())
        fname = file_path_;

    TiXmlDocument doc;
    if(!doc.LoadFile(fname))
        return;

    {
        double fov, near, far;

        TiXmlElement * cam = doc.FirstChildElement("camera");
        cam->QueryDoubleAttribute("fov", &fov);
        cam->QueryDoubleAttribute("near", &near);
        cam->QueryDoubleAttribute("far", &far);
        camera_manager_->set_fov(fov);
        camera_manager_->set_near_distance(near);
        camera_manager_->set_far_clip_distance(far);
    }
    {
        int i_geoCam;
        TiXmlElement * manip = doc.FirstChildElement("manip");
        manip->QueryDoubleAttribute("default_speed", &default_speed_);
        manip->QueryDoubleAttribute("speed", &move_speed_);
        manip->QueryIntAttribute("mode", &i_geoCam);
        setGeoCameraMode(ELandscapeCameraMode(i_geoCam));
    }
    {
        Vector pos, front, up;
        core::LoadPoint(doc.FirstChildElement("pos"), pos);
        core::LoadPoint(doc.FirstChildElement("front"), front);
        core::LoadPoint(doc.FirstChildElement("up"), up);

        camera_manager_->SetPos(pos);

        ((core::SceneNode*)(camera_manager_))->LookAt(front);
    }


}

}
}
