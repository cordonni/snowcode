/**
Copyright (c) 2017, INPG
Main authors : Guillaume Cordonnier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef LANDSCAPECAMERAMANIPULATOR_H
#define LANDSCAPECAMERAMANIPULATOR_H

#include "landscape/LandscapeRequired.h"
#include "pluginTools/scenemanipulator/CameraManipulator.h"

#include<memory>
#include<vector>

namespace expressive {
namespace landscape {

class LandscapeCameraTask;

class LANDSCAPE_API LandscapeCameraManipulator : public plugintools::CameraManipulator, public std::enable_shared_from_this<LandscapeCameraManipulator>
{
public:

    // Definitions
    typedef core::SceneNodeManipulator Base;

    enum ELandscapeCameraMode {
        E_ManipulationMode,
        E_FlyMode,
        E_FpsMode
    };

    // Constructor/Destructor
    LandscapeCameraManipulator(core::Camera*);
    void resetView();
    void resetViewNear();

    void setScreenBufferHandler(LandscapeCameraTask*);
    Vector getPointed();
    Vector getLastPointed();
    bool isLeftMouseClicked();

    /////////////////////////
    // Getters & setters   //
    /////////////////////////

    inline ELandscapeCameraMode geoCameraMode() const { return geo_camera_mode_; }
    inline void setGeoCameraMode(ELandscapeCameraMode mode);

    // terrain size event
    void terrainSizeChanged(double newSize);

    // Redisplay event
    virtual bool redisplay(double t, double dt);

    // Mouse Input Controller
    virtual bool wheelEvent   (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool mousePressed (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool mouseMoved   (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool mouseReleased(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);

    // Keyboard Input Controller
    virtual bool keyPressed(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool keyReleased(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);

    // load / save
    void setPath(std::string s) {file_path_ = s;}
    std::string getSavePath() const {return file_path_; }
    void load(std::string = std::string());
    void save(std::string = std::string());

    core::Camera* getCamera() {return camera_manager_;}

protected:

    void updateInfo();

    ELandscapeCameraMode geo_camera_mode_;

    LandscapeCameraTask* buffer_handler_;

    Point2 mousePos_;
    Vector manipCenter_;

    double default_speed_;
    bool leftMouseCliked_;

    std::string file_path_;

};


}
}

#endif // LANDSCAPECAMERAMANIPULATOR_H
