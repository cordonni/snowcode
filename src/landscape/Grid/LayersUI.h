#ifndef LAYERSUI_H
#define LAYERSUI_H

#include "landscape/LandscapeRequired.h"

#include <QQuickItem>
#include <memory>

namespace expressive {
namespace landscape {

class ExpressiveSimulationManager;

class LANDSCAPE_API LayerItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(bool edited READ edited WRITE setEdited NOTIFY editedChanged)
    Q_PROPERTY(bool shown READ shown WRITE setShown NOTIFY shownChanged)

public:
    LayerItem(){}
    LayerItem(ExpressiveSimulationManager*, QString s, int i);

public slots:
    QString name() const;
    bool edited() const;
    bool shown() const;

    void setEdited(bool);
    void setShown(bool);

signals:
    void editedChanged();
    void shownChanged();

private:
    ExpressiveSimulationManager* interface_;
    QString name_;
    int number_;
};

class LANDSCAPE_API LayersUI : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<expressive::landscape::LayerItem> layers READ getLayers CONSTANT)
public:
    LayersUI(){}
    LayersUI(ExpressiveSimulationManager*);
    void addLayer(QString, int);

public slots:
    QQmlListProperty<expressive::landscape::LayerItem> getLayers();

private:
    ExpressiveSimulationManager* interface_;
    std::vector<std::shared_ptr<LayerItem>> layers_;
};


}
}

#endif // LAYERSUI_H
