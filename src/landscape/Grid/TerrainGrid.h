#ifndef TERRAINGRID_H
#define TERRAINGRID_H

#include "core/scenegraph/TerrainNode.h"
#include "GridCommon.h"

namespace expressive {
namespace landscape {

class LANDSCAPE_API TerrainGrid : public core::TerrainNode
{
public:

    TerrainGrid(core::SceneManager *owner, const std::string &name = "Terrain", const std::string &type = "Terrain", const Vector &scale = Vector::Ones()) :
        core::TerrainNode(owner, name, type, scale)
    {initMesh(129); setSize(1024,1024,10.0);}

    std::pair<bool, double> height(Vector2){return {false, 0};}

    void setSize(uint dem_x, uint dem_y, float cell_size);
    void updateBoundingBox(float, float);

    virtual core::AABBox bounding_box() const;

protected:
    void initMesh(uint);

    core::AABBox bbox;
    float bbox_hsize;
};

}
}

#endif // TERRAINGRID_H
