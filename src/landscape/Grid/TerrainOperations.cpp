#include "TerrainOperations.h"
#include "./SunExposure/sunexposure.h"

#include <fstream>
#include <QImage>
#include <QColor>

#include "libtiff/tiffio.h"
#include "landscape/Utils/misc.h"

namespace expressive {
namespace landscape {

namespace grid_TER_heper
{
template<class T>
T read(std::ifstream& stream)
{
    T to_read;
    stream.read(reinterpret_cast<char*>(&to_read), sizeof(T));
    return to_read;
}
template<class T>
void write(std::ofstream& stream, T data)
{
    stream.write(reinterpret_cast<const char*>(&data), sizeof(T));
}
}

GridDEM TerrainOperations::loadTER(std::string fname)
{
    std::ifstream stream (fname, std::ios::binary);

    char buf[1024];
    stream.read(buf, 8); // TERRAGEN
    stream.read(buf, 8); // TERRAIN
    uint width = 0;
    uint height = 0;

    float scalex = 30.0, scaley  = 30.0, scalez  = 30.0;

    while(true)
    {
        buf[4] = 0;
        stream.read(buf, 4);
        if(std::string(buf) == std::string("SIZE"))
        {
            width = grid_TER_heper::read<int16_t>(stream)+1;
            height = width;
            grid_TER_heper::read<int16_t>(stream);
        }
        else if(std::string("XPTS") == std::string(buf))
        {
            width = grid_TER_heper::read<int16_t>(stream);
            grid_TER_heper::read<int16_t>(stream);
        }
        else if(std::string("YPTS") == std::string(buf))
        {
            height = grid_TER_heper::read<int16_t>(stream);
            grid_TER_heper::read<int16_t>(stream);
        }
        else if(std::string("SCAL") == std::string(buf))
        {
            scalex = grid_TER_heper::read<float>(stream);
            scaley = grid_TER_heper::read<float>(stream);
            scalez = grid_TER_heper::read<float>(stream);
        }
        else if(std::string("CRAD") == std::string(buf))
        {
            // planet radius
            grid_TER_heper::read<float>(stream);
        }
        else if(std::string("CRVM") == std::string(buf))
        {
            // wrap mode
            grid_TER_heper::read<uint>(stream); // uint ??
        }else if(std::string("ALTW") == std::string(buf))
            break;
        else
        {
            debug_print("Unknown string", buf);
        }

        if(!stream || std::string("EOF ") == std::string(buf))
        {
            std::cerr << "Error while loading "<< fname<<": EOF before altitude values\n";
            return GridDEM();
        }
    }

    GridDEM data(width, height);

    int heighScale = grid_TER_heper::read<int16_t>(stream);
    int baseHeight = grid_TER_heper::read<int16_t>(stream);

    float rescale = (scalex+scalez) * 0.5 / scaley;

    for(size_t i = 0; i<width*height; ++i)
    {
        int x = i%width;
        int y = (int)(i/width);
        y = width - y -1;
        int16_t v = grid_TER_heper::read<int16_t>(stream);
        data[y*height + x] = rescale * ((float)baseHeight + (float)v * (float)heighScale / 65536.0);

    }

    return data;

}

void TerrainOperations::saveTER(std::string fname, const GridDEM& data, float scale)
{
    std::ofstream file (fname, std::ios::binary);

    file << "TERRAGENTERRAIN SIZE";
    grid_TER_heper::write (file, int16_t(std::min(data.width(), data.height()) - 1));

    if (data.width() != data.height())
    {
        file.write("\0\0XPTS", 6);
        grid_TER_heper::write (file, int16_t(data.width()));
        file.write("\0\0YPTS", 6);
        grid_TER_heper::write (file, int16_t(data.height()));
    }

    file.write("\0\0SCAL", 6);
    grid_TER_heper::write (file, scale);
    grid_TER_heper::write (file, scale);
    grid_TER_heper::write (file, scale);

    file << "ALTW";

    float mn, mx; std::tie(mn, mx) = data.minmax();

    grid_TER_heper::write (file, int16_t((mx-mn)*2));
    grid_TER_heper::write (file, int16_t(mn));

    for(uint i = 0; i<data.size(); i++)
    {
        int x, y; std::tie(x, y) = data.id_to_xy(i);
        y = (int)(data.width() - y -1);
        float h = (data[data.xy_to_id(x, y)] - mn) / (mx-mn);
        grid_TER_heper::write (file, int16_t(h*32767.0f));
    }

    file << "EOF ";

}

void TerrainOperations::computeNormals(const GridDEM& data, GridNormal& normals)
{
    normals.resize(data.width(), data.height());

    for(size_t id = 0; id<data.size(); ++id)
    {
        float x, z, c, y = 2.0f;

        std::tie(x, z, c) = data.gradSlope(id);
        float l = std::sqrt(x*x+y*y+z*z);

        normals[id] = Vector4f(x/l, y/l, z/l, 4.0f*c);
    }
}

void TerrainOperations::saveImg(std::string name, const GridDEM& dem, float clamp_min, float clamp_max)
{
    QImage im((int)dem.width(), (int)dem.height(), QImage::Format_ARGB32);

    for(uint i = 0; i<dem.size(); ++i)
    {
        float v = 255.0f * CLAMP((dem[i]-clamp_min) / (clamp_max-clamp_min), 0.0f, 1.0f);
        // im.setPixelColor(dem.id_to_xy(i).first, dem.id_to_xy(i).second, QColor((int) v, (int) v, (int) v));
        im.setPixel((int) dem.id_to_xy(i).first, (int) dem.id_to_xy(i).second, qRgb((int) v, (int) v, (int) v));
    }

    im.mirrored().save(name.c_str());

}

void TerrainOperations::saveTIFF16(std::string name, const GridDEM& dem, float clamp_min, float clamp_max)
{
    std::vector<uint16_t> data(dem.size());

    for(uint i = 0; i<dem.size(); ++i)
    {
        int x, y; std::tie(x, y) = dem.id_to_xy(i);
        y = (int)(dem.width() - y -1);
        data[i] = uint16_t(65535.0f * CLAMP((dem[dem.xy_to_id(x, y)]-clamp_min) / (clamp_max-clamp_min), 0.0f, 1.0f));
    }

    TIFF* img = TIFFOpen(name.c_str(), "w");
    TIFFSetField(img, TIFFTAG_IMAGEWIDTH, dem.width());
    TIFFSetField(img, TIFFTAG_IMAGELENGTH, dem.height());
    TIFFSetField(img, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(img, TIFFTAG_BITSPERSAMPLE, 16);
    TIFFSetField(img, TIFFTAG_ROWSPERSTRIP, dem.height());

    TIFFWriteEncodedStrip(img, 0, data.data(), (int)dem.size()*2);

    TIFFClose(img);
}

void TerrainOperations::computeExposure(const GridDEM & terrain, float latitude, float compass, GridDEM* exposure) // latitude and compass in degrees
{
    SunExposure sun;
    GridNormal normals;

    computeNormals(terrain, normals);

    sun.calcMonthlySunExposure(terrain, normals, (int) latitude, compass*M_PI/180.0, exposure);
    // values in exposure are equivalent to hours of sunlight
}

}
}
