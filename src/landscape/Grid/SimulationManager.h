#ifndef SIMULATIONMANAGER_H
#define SIMULATIONMANAGER_H

#include "SimulationData.h"

#include "StochasticSimulationBase.h"
#include <unordered_map>
#include <map>

namespace expressive {
namespace landscape {

// the simulation manager handles the simulation itself and its playback

class LANDSCAPE_API SimulationManager
{

public:
    SimulationManager() {}
    SimulationManager(std::shared_ptr<SimulationDataBase>, std::shared_ptr<StochasticSimulationBase>);
    virtual ~SimulationManager(){}

    void goToFrame(int);
    void setMaxFrame(int);

    void setSimulationInput(const GridDEM& dem, float cell_size);
    void setSimulationInput(std::string dirname, float cell_size);
    void startSimulation();

    bool updateSimu(bool record);

    void load(std::string);
    void save(std::string);

    double getLastSimuLength();

    // Parameter functions
    float getParameter(int) const;
    bool  getParameterLock(int) const;
    float  getEventTimeStep(int) const;
    bool  isEventLocked(int)  const;

    void setParameter(int, float);
    void lockParameter(int, bool);
    void setEventTimeStep(int, float);
    void setEventLocked(int, bool);

    void defaultParameters();

    int layerEdited() const;
    int layerShown() const;
    void setLayerEdited(int);
    void setLayerShown(int);

    virtual void parameterChanged();

    void performEdit(Vector2f, float, float, float amount);
    void releaseEdit();

    void setRenderTriggerParams(std::vector<int>);
    void forceUpdateChangedParams();

protected:

    void updateParams(int frame, int &frame_param, bool simulatedData=false, bool force=false);

    std::shared_ptr<StochasticSimulationBase> simulator;

    int frame_cur, frame_number, frame_computed; // frame computed is the last computed frame
    int sample_per_frame;

    bool initialized;

    // simu batch

    std::shared_ptr<SimulationDataBase> simuData_;

//    std::vector<SimuData> data_;
//    SimuData simulatedData_;
    float cell_size_;

    int frame_cur_param, frame_simu_param;

//    Params initial_params, cur_frame_params, computed_frame_params;
    std::map<int, std::unordered_map<int, float>> locked_params;

//    std::vector<bool> initial_events, cur_frame_events, computed_frame_events;
    std::map<int, std::unordered_map<int, float>> locked_events;
//    std::map<int, GeoArray<Cell>> delta_paint_;

    std::vector<std::pair<int, float>> params_trigger_render;

    int layerEdited_;
    int layerShown_;
    GridDEM painted;



};

}
}

#endif // SIMULATIONMANAGER_H
