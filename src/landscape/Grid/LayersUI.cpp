#include "LayersUI.h"

#include "ExpressiveSimulationManager.h"
#include <pluginTools/gui/qml/QmlTypeRegisterer.h>


namespace expressive {
namespace landscape {

LayerItem::LayerItem(ExpressiveSimulationManager* scm, QString s, int i):
    QQuickItem(nullptr),
    interface_ (scm),
    name_ (s),
    number_(i)
{
}

QString LayerItem::name() const
{
    return name_;
}

bool LayerItem::edited() const
{
    return interface_->layerEdited() == number_;
}

bool LayerItem::shown() const
{
    return interface_->layerShown() == number_;
}

void LayerItem::setEdited(bool b)
{
    if(b)
        interface_->setLayerEdited(number_);
    else
        interface_->setLayerEdited(-1);
}

void LayerItem::setShown(bool b)
{
    if(b)
        interface_->setLayerShown(number_);
    else
        interface_->setLayerShown(-1);
}


LayersUI::LayersUI(ExpressiveSimulationManager* esm)
    : interface_(esm)
{

}

void LayersUI::addLayer(QString s, int i)
{
    layers_.push_back(std::make_shared<LayerItem>(interface_, s, i));
}

QQmlListProperty<expressive::landscape::LayerItem> LayersUI::getLayers()
{
    return QQmlListProperty<LayerItem>(this, &layers_,
                                           plugintools::qmlContainerCount<std::vector<std::shared_ptr<LayerItem>>>,
                                           plugintools::qmlSharedPtrContainerAt<std::vector<std::shared_ptr<LayerItem>>>);

}

void LayerUIRegisterer()
{
    qmlRegisterType<LayerItem>("Expressive", 1, 0, "LayerItem");
    qmlRegisterType<LayersUI>("Expressive", 1, 0, "LayersUI");
}

QML_TYPE_REGISTERER_CALLBACK(LayerUIRegisterer)

}
}
