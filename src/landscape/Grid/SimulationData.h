#ifndef SIMULATIONDATA_H
#define SIMULATIONDATA_H

#include "GridCommon.h"
#include "SimulationDataBase.h"

#include <map>
#include <fstream>

namespace expressive {
namespace landscape {

template<class SimuData, class SimuDataIO>
class SimulationData : public SimulationDataBase
{
public:
    SimulationData():
        ski_tracks(8192, 8192)
    {
        ski_tracks.fill(0);
    }
    virtual ~SimulationData(){}

    virtual void setFrameCount(int i) override {    data_.resize(i); data_time_.resize(i);}
    virtual void setSimulated(int i) override
    {
        simulatedData_.cells = data_[i].cells;
        simulatedData_.render_elevations = data_[i].render_elevations;
        simulatedData_.render_normals = data_[i].render_normals;
        simulatedData_.render_colors = data_[i].render_colors;
    }
    virtual void saveSimulated(int i) override
    {
        data_[i].cells = simulatedData_.cells;
        //data_[i].params = simulatedData_.params;
        //data_[i].events = simulatedData_.events;
        data_[i].render_elevations = simulatedData_.render_elevations;
        data_[i].render_normals = simulatedData_.render_normals;
        data_[i].render_colors = simulatedData_.render_colors;
    }
    virtual void setSimulationInput(const GridDEM& dem, float cell_size) override;
    virtual void importLayers(std::string, float) override;

    virtual bool empty() override {return data_.empty();}

    virtual void defaultParameters(const std::vector<float>& tsv)
    {
        SimuDataIO::defaultParameters(this, tsv);

        data_time_[0] = 0.0f;

        simulatedData_.params = data_[0].params;
        simulatedData_.events = data_[0].events;
        simulated_time_ = 0.0f;
    }

    virtual void saveFirstFrameParams()
    {
        initial_params = data_[0].params;
        initial_events = data_[0].events;
    }


    virtual void restoreFirstFrameParams(int f=0)
    {
        data_[f].params = initial_params;
        data_[f].events = initial_events;
    }

    virtual void* getSimulatedCellsPtr() {return simulatedData_.cells.data();}
    virtual void* getSimulatedParamsPtr() {return &simulatedData_.params;}

    virtual void* getCellsPtr(int frame) {return data_[frame].cells.data();}
    virtual void* getParamsPtr(int frame) {return &data_[frame].params;}



    virtual float getParam(int frame, int i) const override { return ((float*) (&data_[frame].params))[i]; }
    virtual void  setParam(int frame, int i, float p) override { ((float*) (&data_[frame].params))[i] = p;}
    virtual float getSimulatedParam(int i) const override { return ((float*) (&simulatedData_.params))[i]; }
    virtual void  setSimulatedParam(int i, float p) override { ((float*) (&simulatedData_.params))[i] = p; }

    virtual float getEvent(int frame, int i) const override {return data_[frame].events[i]; }
    virtual void  setEvent(int frame, int i, float e) override { data_[frame].events[i] = e; }

    virtual float getSimulatedEvent(int i) const {return simulatedData_.events[i];}
    virtual void setSimulatedEvent(int i, float e) override {simulatedData_.events[i] = e;}

    virtual void copySimulatedParams(int from) override
    {
        simulatedData_.params = data_[from].params;
        simulatedData_.events = data_[from].events;
    }

    virtual void copyParams(int from, int to) override
    {
        data_[to].params = data_[from].params;
        data_[to].events = data_[from].events;
    }

    virtual size_t getEventsCount() override {return data_[0].events.size();}

    virtual size_t cells_width() override {return (int)data_[0].cells.width();}
    virtual size_t cells_height() override {return (int)data_[0].cells.height();}
    virtual float& cell(int frame, int x, int y, int layer) override {return *((float*)&data_[frame].cells(x, y) + layer);}
    virtual float& delta_cell(int x, int y, int layer) override {return *((float*)&(*delta_paint_selected)(x, y) + layer);}


    virtual void selectDelta(int frame) override
    {
        delta_paint_selected = &delta_paint_[frame];
        if(!delta_paint_selected->size())
            delta_paint_selected->resize(cells_width(), cells_height());
    }

    virtual void applyDelta(int frame, bool discard= false) override
    {
        auto dp = delta_paint_.find(frame);
        if(dp == delta_paint_.end())
            return;
        GeoArray<typename SimuData::Cell>& delta = dp->second;

        for(size_t id=0; id<delta.size(); ++id)
            for(size_t i =0; i<sizeof(typename SimuData::Cell) / sizeof(float); ++i)
            {
                float* p_p = (float*)&simulatedData_.cells[id] + i;
                float* p_d = (float*)&delta[id] + i;
                *p_p = std::max(*(p_p) + *(p_d), 0.0f);

            }
        if(discard)
            delta_paint_.erase(dp);
    }

    virtual std::vector<int> deltaPaintKeys() override
    {
        std::vector<int> ret;
        for(auto it : delta_paint_)
            ret.push_back(it.first);
        return ret;
    }

    virtual double get_refresh_period() {return getSimulatedParam(SimuDataIO::getRefreshParamId());}

    virtual size_t sizeof_cell() override {return sizeof(typename SimuData::Cell);}
    virtual size_t sizeof_params() override {return sizeof(typename SimuData::Params);}
    virtual void load_params(std::ifstream& file) override {file.read((char*)&data_[0].params, sizeof(typename SimuData::Params));}
    virtual void load_events(std::ifstream&, int load_count, int target_count) override;
    virtual void load_cells(std::ifstream&, int frame_number, int frame_computed) override;

    virtual void save_params(std::ofstream& file) override { file.write((char*)&data_[0].params, sizeof(typename SimuData::Params)); }
    virtual void save_events(std::ofstream& file) override;
    virtual void save_cells(std::ofstream& file, int frame_computed) override;

    virtual void exportMaps(std::string dirname, int frame) override {SimuDataIO::exportMaps(this, dirname, frame);}
    virtual void exportFrames(std::string dirname, int start, int end, int step) override {SimuDataIO::exportFrames(this, dirname, start, end, step);}

    std::vector<SimuData>& data() {return data_;}
    SimuData& computedFrame() {return simulatedData_;}

    virtual GridDEM& renderElevations(int frame) override {return data_[frame].render_elevations;}
    virtual GridNormal& renderNormals(int frame) override {return data_[frame].render_normals;}
    virtual GridColor& renderColors(int frame) override {return data_[frame].render_colors;}
    virtual GridDEM& renderSkiTracks(int frame) override {return ski_tracks;}
    virtual GridDEM& renderElevationsSimulated() override {return simulatedData_.render_elevations;}
    virtual GridNormal& renderNormalsSimulated()  override {return simulatedData_.render_normals;}
    virtual GridColor& renderColorsSimulated()  override {return simulatedData_.render_colors;}
    virtual GridDEM& renderSkiTracksSimulated() override {return ski_tracks;}

    virtual float getTotalTime() const {return simulated_time_;}
    virtual float incrTotalTime(float dt) { simulated_time_ += dt; return simulated_time_;}
    virtual void setSimulatedTime(int from_frame) {simulated_time_ = data_time_[from_frame];}
    virtual void storeSimulatedTime(int storage_frame) {data_time_[storage_frame] = simulated_time_;}


protected:

    std::vector<SimuData> data_;
    SimuData simulatedData_;
    std::map<int, GeoArray<typename SimuData::Cell>> delta_paint_;
    GeoArray<typename SimuData::Cell>* delta_paint_selected;

    std::vector<float> data_time_;
    float simulated_time_;
    GridDEM ski_tracks;

    // first frame
    typename SimuData::Params initial_params;
    std::vector<float>        initial_events;


    friend SimuDataIO;
};

//// impl ///

template<class SimuData, class SimuDataIO>
void SimulationData<SimuData, SimuDataIO>::setSimulationInput(const GridDEM& dem, float cell_size)
{    
    SimuDataIO::import(this, dem, cell_size);
}

template<class SimuData, class SimuDataIO>
void SimulationData<SimuData, SimuDataIO>::importLayers(std::string dirname, float cell_size)
{
     SimuDataIO::importLayers(this, dirname, cell_size);
}

template<class SimuData, class SimuDataIO>
void SimulationData<SimuData, SimuDataIO>::load_events(std::ifstream& file, int load_count, int target_count)
{
    data_[0].events.resize(target_count, true);

    for(int i = 0; i<load_count; ++i)
    {
        float f;
        file.read((char*)&f, sizeof(float));
        if(i < target_count)
            data_[0].events[i] = f;
    }
}

template<class SimuData, class SimuDataIO>
void SimulationData<SimuData, SimuDataIO>::load_cells(std::ifstream& file, int frame_number, int frame_computed)
{
    size_t w, h, in_size_t;
    file.read((char*) &w, sizeof(size_t));
    file.read((char*) &h, sizeof(size_t));
    data_.resize(frame_number);

    for(int frame = 0; frame < frame_computed; ++frame)
    {
        SimuData& cur = data_[frame];
        cur.resize((int)w, (int)h);
        file.read((char*)cur.cells.data(), cur.cells.size() * sizeof(typename SimuData::Cell));
        file.read((char*)cur.render_elevations.data(), cur.cells.size() * sizeof(float));
        file.read((char*)cur.render_normals.data(), cur.cells.size() * sizeof(float) * 4);
        file.read((char*)cur.render_colors.data(), cur.cells.size() * sizeof(float) * 4);
    }

    file.read((char*) &in_size_t, sizeof(size_t));
    for(size_t i = 0; i<in_size_t; ++i)
    {   int frame;
        file.read((char*) &frame, sizeof(int));
        auto& delta = delta_paint_[frame];
        delta.resize((int)w, (int)h);
        file.read((char*)delta.data(), delta.size() * sizeof(typename SimuData::Cell));
    }
}

template<class SimuData, class SimuDataIO>
void SimulationData<SimuData, SimuDataIO>::save_events(std::ofstream& file)
{
    size_t size_t_io = data_[0].events.size();
    file.write((char*) &size_t_io, sizeof(size_t));
    for(float f : data_[0].events)
        file.write((char*)&f, sizeof(float));
}

template<class SimuData, class SimuDataIO>
void SimulationData<SimuData, SimuDataIO>::save_cells(std::ofstream& file, int frame_computed)
{
    size_t size_t_io =  data_.front().cells.width();
    file.write((char*) &size_t_io, sizeof(size_t));
    size_t_io =  data_.front().cells.height();
    file.write((char*) &size_t_io, sizeof(size_t));

    for(int frame = 0; frame < frame_computed; ++frame)
    {
        SimuData& cur = data_[frame];
        file.write((char*)cur.cells.data(), cur.cells.size() * sizeof(typename SimuData::Cell));
        file.write((char*)cur.render_elevations.data(), cur.cells.size() * sizeof(float));
        file.write((char*)cur.render_normals.data(), cur.cells.size() * sizeof(float) * 4);
        file.write((char*)cur.render_colors.data(), cur.cells.size() * sizeof(float) * 4);
    }

    size_t_io = delta_paint_.size();
    file.write((char*) &size_t_io, sizeof(size_t));
    for(auto it : delta_paint_)
    {
        file.write((char*) &it.first, sizeof(int)); // frame
        file.write((char*)it.second.data(), it.second.size() * sizeof(typename SimuData::Cell));
    }
}


}
}

#endif // SIMULATIONDATA_H


