#include "RegisterSnow.h"

#include "../ParameterUI.h"

#include "../ExpressiveSimulationManager.h"
#include "../CUDA/StochasticSimulationCUDA.h"

#include "SnowData.h"


namespace expressive {
namespace landscape {

// macro to access member P of class type of L_tmp variable
#define L_init(T) T L_tmp
#define L(P) (int)((float*)(&L_tmp.P) - (float*)&L_tmp)


void SNOW_API registerSnowLayers(ExpressiveSimulationManager* esm)
{
    LayersUI* layers = esm->getLayersUI();

    L_init(SnowCell);

    layers->addLayer("Illumination", L(illumination));
    layers->addLayer("Bedrock", L(bedrock));
    layers->addLayer("Snow", L(snow));
    layers->addLayer("Unstable Snow", L(unstable_layer));
    layers->addLayer("Compaction", L(compacted));
    layers->addLayer("Powdery Snow", L(powdery));
    layers->addLayer("Wind x", L(wind_speed_x));
    layers->addLayer("Wind y", L(wind_speed_y));
    layers->addLayer("Pressure", L(wind_pressure));
    layers->addLayer("Wind visu", L(wind_visu));
    layers->addLayer("Wind on terrain", L(wind_terrain));
    layers->addLayer("Wind transport", L(wind_transport));
    layers->addLayer("Avalanche trigger", L(avalanche_trigger));
    layers->addLayer("Inertia x", L(flow[0]));
    layers->addLayer("Inertia y", L(flow[1]));
    layers->addLayer("Moving", L(moving));
    layers->addLayer("Number of skiers", L(nb_skiers));
    layers->addLayer("Distance to minima", L(dist_to_exit));
    layers->addLayer("Debug", L(debug));

}

void SNOW_API registerSnowParameters (ExpressiveSimulationManager* esm)
{
    ParameterUI* params = esm->getParameterUI();

    L_init(SnowParams);

    params->addGlobal("Update period (days)", L(update_time));

    params->addGlobal("Latitude (°)", L(latitude));
    params->addGlobal("Compass (°)", L(compass));
    params->addGlobal("Month (1=jan)", L(month));
    params->addGlobal("Temparature base (°C)", L(temperature_base));
    params->addGlobal("Temparature offset (°C/m)", L(temperature_offset));
    params->addGlobal("Temperature from sun (°C)", L(temperature_illum));
    params->addGlobal("GI radius (m)", L(illum_radius));
    params->addGlobal("Illum Sun factor", L(illum_sun));
    params->addGlobal("Illum AO  factor", L(illum_ao));
    params->addGlobal("Illum GI  factor", L(illum_factor));

    // render
    params->addGlobal("Show Wind", L(show_wind));
    params->addGlobal("Show Wind Cap", L(show_wind_cap));
    params->addGlobal("Show Stability", L(show_unstable));
    params->addGlobal("Show Debug", L(show_debug));

    // force render update at render params
    esm->setRenderTriggerParams({L(show_wind), L(show_wind_cap), L(show_unstable), L(show_debug)});



    params->addEvent("Precipiation Start", SnowEventPrecipStartID);
    params->addEvent("Precipiation Stop", SnowEventPrecipStopID);
    int snowFallEventParamId = params->addEvent("Precipiation", SnowEventPrecipID);
    params->addEventParam("Max temperature (°C)", snowFallEventParamId, L(precip_max_temp));
    params->addEventParam("Temp. offset (m/°C)", snowFallEventParamId, L(precip_temp_offset));
    params->addEventParam("Max precip", snowFallEventParamId, L(precip_max));
    params->addEventParam("Stability min slope", snowFallEventParamId, L(stability_min_slope));
    params->addEventParam("Stability slope decrease", snowFallEventParamId, L(unstable_factor));
    params->addEventParam("Moving fix temperature", snowFallEventParamId, L(moving_max_temp));
    params->addEventParam("Moving fix temperature offset", snowFallEventParamId, L(moving_temp_offset));
    params->addEventParam("Moving min slope", snowFallEventParamId, L(moving_min_slope));
    params->addEventParam("Moving slope decrease", snowFallEventParamId, L(moving_propotion));

    int snowMeltEventParamId = params->addEvent("Melt", SnowEventMeltID);
    params->addEventParam("Max temperature (°C)", snowMeltEventParamId, L(melt_max_temp));
    params->addEventParam("Temp. offset (m/°C)", snowMeltEventParamId, L(melt_temp_offset));
    params->addEventParam("Unstable Temp", snowMeltEventParamId, L(melt_unstable_temp));
    params->addEventParam("Stable Temp", snowMeltEventParamId, L(melt_stable_temp));
    params->addEventParam("Freeze Temp", snowMeltEventParamId, L(melt_freeze_temp));
    params->addEventParam("Unstable rate", snowMeltEventParamId, L(melt_hot_stability));
    params->addEventParam("Stable rate", snowMeltEventParamId, L(melt_medium_stability));
    params->addEventParam("Freeze rate", snowMeltEventParamId, L(melt_freeze_stability));
    params->addEventParam("Compaction dampening", snowMeltEventParamId, L(melt_compaction_effect));
    params->addEventParam("Compaction pressure", snowMeltEventParamId, L(melt_compaction_pressure));

    int snowDiffusionEventParamId = params->addEvent("Snow Diffusion", SnowEventDiffusionID);
    params->addEventParam("Slope factor", snowDiffusionEventParamId, L(diffusion_slope_factor));
    params->addEventParam("Rest angle", snowDiffusionEventParamId, L(diffusion_rest_angle));

    int snowWindEventParamId = params->addEvent("Wind", SnowEventWindID);
    params->addEventParam("Compass (°C)", snowWindEventParamId, L(wind_compass));
    params->addEventParam("Strength (m/s)", snowWindEventParamId, L(wind_strength));
    params->addEventParam("Altitude offset(1/s)", snowWindEventParamId, L(wind_altitude_offset));
    params->addEventParam("Terrain effect (0-1)", snowWindEventParamId, L(wind_terrain_force));
    params->addEventParam("Snow curve", snowWindEventParamId, L(wind_snow_curve));
    params->addEventParam("Snow on terrain", snowWindEventParamId, L(wind_snow_terrain));
    params->addEventParam("Snow capacity", snowWindEventParamId, L(wind_snow_capacity));
    params->addEventParam("Fall offset", snowWindEventParamId, L(wind_fall_offset));
    params->addEventParam("Smooth", snowWindEventParamId, L(wind_smooth_ground));
    params->addEventParam("Wind unstability", snowWindEventParamId, L(wind_plates));

    params->addEvent("Avalanche propagation", SnowEventAvPropagID);

    int snowAvFallEventFluidParamId = params->addEvent("Avalanche fluid", SnowEventAvFallFluidID);
    params->addEventParam("Time step", snowAvFallEventFluidParamId, L(av_dt));
    params->addEventParam("Viscosity", snowAvFallEventFluidParamId, L(viscosity));
    params->addEventParam("Max moving amount (m)", snowAvFallEventFluidParamId, L(av_max_simulated));
    params->addEventParam("Rest angle (°C)", snowAvFallEventFluidParamId, L(av_rest_angle));


    int skierSpawnEventParamId = params->addEvent("Skier spawn", SnowEventSpawnSkierID);
    params->addEventParam("Spawn rate", skierSpawnEventParamId, L(skier_spawn_rate));
    params->addEventParam("Min snow needed", skierSpawnEventParamId, L(skier_min_snow));
    params->addEventParam("Snow impact on tracks", skierSpawnEventParamId, L(skier_snow_impact_on_track));
    params->addEventParam("Avalanche trigger chance", skierSpawnEventParamId, L(skier_avalanche_chance));
    int skierMoveEventParamId = params->addEvent("Skier movement", SnowEventMoveSkierID);
    params->addEventParam("Target slope", skierMoveEventParamId, L(skier_target_slope));
    params->addEventParam("Target amplitude", skierMoveEventParamId, L(skier_target_amplitude));
    params->addEventParam("Target curve period", skierMoveEventParamId, L(skier_target_period));
    params->addEventParam("Speed", skierMoveEventParamId, L(skier_speed));
    params->addEventParam("Lookahead", skierMoveEventParamId, L(skier_lookahead));
    params->addEventParam("Rotation step", skierMoveEventParamId, L(skier_angle_step));
}

int SnowEventPrecipStartID = -1;
int SnowEventPrecipStopID = -1;
int SnowEventPrecipID = -1;
int SnowEventMeltID = -1;
int SnowEventDiffusionID = -1;
int SnowEventWindID = -1;
int SnowEventAvPropagID = -1;
int SnowEventAvFallFluidID = -1;
int SnowEventSpawnSkierID = -1;
int SnowEventMoveSkierID = -1;
int SnowEventHideTracksID = -1;

void SNOW_API registerSnowEvents()
{
    StochasticSimulationCUDA::render_func = SnowRenderFunc;

    L_init(SnowParams);

    StochasticSimulationCUDA::RegisterInitialEvent(SnowEventIllumination, {L(latitude), L(compass), L(month),
                                                                           L(illum_factor), L(illum_radius),
                                                                           L(illum_sun), L(illum_ao)});
    StochasticSimulationCUDA::RegisterInitialEvent(SnowEventWindProc, {L(wind_compass), L(wind_strength),
                                                                       L(wind_altitude_offset),  L(wind_terrain_force), L(wind_terrain_force),
                                                                       L(wind_fall_offset)});
    StochasticSimulationCUDA::RegisterInitialEvent(SnowEventPaths, {L(latitude)});

    SnowEventPrecipStartID =  StochasticSimulationCUDA::RegisterEvent(SnowEventPrecipStart, 3.0).ID();
    SnowEventPrecipStopID  =  StochasticSimulationCUDA::RegisterEvent(SnowEventPrecipStop, 1e10f).ID();
    SnowEventPrecipID      =  StochasticSimulationCUDA::RegisterEvent(SnowEventPrecip, 1e10f).ID();
    SnowEventMeltID        =  StochasticSimulationCUDA::RegisterEvent(SnowEventMelt, 0.5f).ID();
    SnowEventDiffusionID   =  StochasticSimulationCUDA::RegisterEvent(SnowEventDiffusion, 0.5f ).ID();
    SnowEventWindID        =  StochasticSimulationCUDA::RegisterEvent(SnowEventWind, 0.5f).ID();
    SnowEventAvPropagID    =  StochasticSimulationCUDA::RegisterEvent(SnowEventAvPropag, 1.0f).ID();
    SnowEventAvFallFluidID =  StochasticSimulationCUDA::RegisterEvent(SnowEventAvFallFluid, 1.0f).ID();
    SnowEventSpawnSkierID  =  StochasticSimulationCUDA::RegisterEvent(SnowEventSpawnSkier, 1.0f).ID();
    SnowEventMoveSkierID   =  StochasticSimulationCUDA::RegisterEvent(SnowEventMoveSkier, 0.5f).ID();
    SnowEventHideTracksID  =  StochasticSimulationCUDA::RegisterEvent(SnowEventHideTracks, 2.0f).ID();
}


}
}
