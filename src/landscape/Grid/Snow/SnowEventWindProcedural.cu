#include <cuda.h>
#include <builtin_types.h>
#include <curand_kernel.h>
#include <math_constants.h>
#include <vector_functions.h>
#include <helper_math.h>

#include "../CUDA/CudaGridUtils.h"
#include "../CUDA/StochasticSimulationCUDA.h"

#include "SnowData.h"

#include "landscape/Utils/Profile.h"

#include "landscape/Utils/Profile.h"


namespace expressive {
namespace landscape {

extern "C"
__global__ void SnowEventWindProcCUDA(SnowCell* __restrict__ cells,
                                      const SnowParams* __restrict__ params,
                                      //                                      curandState* __restrict__ rand_state,
                                      int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    float cur_h = cells[id].bedrock;


    float stength = params->wind_strength + params->wind_altitude_offset * (cur_h - params->altitude_base);

    float2 global_wind = stength * make_float2(
                cos(params->wind_compass * CUDART_PI_F / 180.0),
                sin(params->wind_compass * CUDART_PI_F / 180.0));


    // compute slope gradient

    int nb[5];
    nb[0] = !(id%w)     ? id : id-1;
    nb[1] = !((id+1)%w) ? id : id+1;
    nb[2] = !(id/w)     ? id : id-w;
    nb[3] = id/w+1==h  ? id : id+w;

    float nbh[4];
    for(int i = 0; i<4; ++i)
        nbh[i] = cells[nb[i]].bedrock;

    float4 tn = normalize(make_float4(nbh[0] - nbh[1], 2.0f, nbh[2] - nbh[3], 0.0f));

    float2 tn_orth = make_float2(tn.y, -tn.x);
    float tn_norm = length(tn_orth);

    if(dot(tn_orth, global_wind)< 0 )tn_orth*=-1.0;
    else if (dot(tn_orth, global_wind)< 0 ) tn_orth *= 0.0;

    //float terrain_resistance =  -dot(global_wind, make_float2(tn.x, tn.z));
    //float2 wind_terrain = make_float2(tn.x, tn.z) * abs(terrain_resistance);
    float wind_nm = length(global_wind);
    float2 wind_nmd = global_wind;
    if(wind_nm > 0.0001)
        wind_nmd /= wind_nm;

    float k_terrain = params->wind_terrain_force;

    float2 wind = global_wind * (1.0f-k_terrain*tn_norm) + wind_nm * k_terrain*tn_orth;
    float wind_n = length(wind);
    if (wind_n > 0.0001)
        wind *= wind_nm / wind_n;

    //wind_terrain -= wind_nmd * dot(wind_nmd, wind_terrain);

    //float2 wind =  params->wind_global_force * global_wind +
    //        params->wind_terrain_force * wind_terrain;


    cells[id].wind_speed_x = wind.x;
    cells[id].wind_speed_y = wind.y;

    cells[id].wind_h  = cur_h;
}

__device__ float rand(float2 p)
{
    return fracf(sinf(p.x*12.9898  + p.y*78.233) * 43758.5453);
}

__device__ float mixCUDA(float x, float y, float a)
{
    return x*(1.0f-a) + y*a;
}

__device__ float noise(float2 n) {
    float2 b = floorf(n), f = smoothstep(make_float2(0.0), make_float2(1.0), fracf(n));
    return mixCUDA(
                mixCUDA(
                    rand(b),
                    rand(b + make_float2(1.0f, 0.0f)),
                    f.x),
                mixCUDA(
                    rand(b + make_float2(0.0f, 1.0f)),
                    rand(b + make_float2(1.0f, 1.0f)),
                    f.x),
                f.y);
}

__global__ void SnowEventWindVisuCUDA(SnowCell* __restrict__ cells,
                                      const SnowParams* __restrict__ params,
                                      //                                      curandState* __restrict__ rand_state,
                                      int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    float2 dir = make_float2(cells[id].wind_speed_x, cells[id].wind_speed_y);
    float2 uv = make_float2(float (x)/float(w), float (y)/float(h));

    float LICSize = float(min(w, h));
    float numSamples = 10;
    float value = noise(uv * LICSize);
    dir /=  LICSize; // divided by the size of the texture.
    for (float i = 0.0f; i < numSamples; ++i) {
        value += noise((uv + i * dir) * LICSize);
    }
    for (float i = 0.0f; i < numSamples; ++i) {
        value += noise((uv - i * dir) * LICSize);
    }
    value /= (numSamples * 2.0f + 1.f);
    value = clamp(value*3.0f-1.0f, 0.0f, 1.0f);
    cells[id].wind_visu = value ;
}

extern "C"
__global__ void SnowEventWindSmoothGroundCUDA(SnowCell* __restrict__ cells,
                                              const SnowParams* __restrict__ params,
                                              float* __restrict__ temp,
                                              int w, int h)
{
    // wind ground speed depends on a smoothed bedrock value
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    int wind_dh_offset = w*h;


    float inv_s = params->wind_smooth_ground;
    inv_s = 0.5f / inv_s / inv_s;

    if(x >= w || y >= h)
        return;

    float norm = 0.0f;
    float r = 0.0f;
    for(int i_y  = y-15; i_y<= y+15; ++i_y)
        for(int i_x  = x-15; i_x<= x+15; ++i_x)
        {
            if(i_x>=0 && i_y>=0 && i_x < w && i_y < h )
            {
                float dx = float(i_x-x) * params->cell_size;
                float dy = float(i_y-y) * params->cell_size;

                float v = cells[i_x+w*i_y].bedrock;
                float w = expf(-(dx*dx+dy*dy)*inv_s);
                norm +=w;
                r +=v*w;
            }
        }

    temp[x + y*w] = r/norm;
    temp[wind_dh_offset + x + y*w] = 0.0;

}


extern "C"
__global__ void SnowEventWindHeightCUDA(SnowCell* __restrict__ cells,
                                        const SnowParams* __restrict__ params,
                                        float* __restrict__ temp,
                                        int w, int h, int offset)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    int wind_dh_offset = w*h;

    x = 2*x + ((y+offset)%2);

    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    float wx = cells[id].wind_speed_x;
    float wy = cells[id].wind_speed_y;

    int nb[4];
    nb[0] = !(id%w)     ? id : id-1;
    nb[1] = !((id+1)%w) ? id : id+1;
    nb[2] = !(id/w)     ? id : id-w;
    nb[3] = id/w+1==h  ? id : id+w;

    // chose direction that minimize the 2D wind speed
    int idx = nb[wx<0.0 ? 1: 0];
    int idy = nb[wy<0.0 ? 3: 2];

    wx = abs(wx);
    wy = abs(wy);

    float dhx = temp[wind_dh_offset + idx]; // wind speed
    float dhy = temp[wind_dh_offset + idy];

    float hx = cells[idx].wind_h;
    float hy = cells[idy].wind_h;

    // then we compute the new delta h
    float ndh = (wx*dhx+wy*dhy - params->cell_size * params->wind_fall_offset);
    if(wx+wy)
        ndh /= (wx+wy);
    float nh = (wx*hx+wy*hy + params->cell_size * ndh);
    if(wx+wy)
        nh /= (wx+wy);
    else
        nh = -100000.0;

    float bedrock = cells[id].bedrock;

    if( nh <= bedrock)
    {
        // compute terrain gradient
        float g =  temp[id];
        float gx = temp[idx];
        float gy = temp[idy];

        ndh = (wx * (g - gx) + wy * (g - gy)) / params->cell_size;
        nh = bedrock;
    }

    temp[wind_dh_offset + id] = ndh;
    cells[id].wind_h  = nh;
}

extern "C"
__global__ void SnowEventWindTerrainCUDA(SnowCell* __restrict__ cells,
                                         const SnowParams* __restrict__ params,
                                         float* __restrict__ temp,
                                         int w, int h)
{
    // wind impact on terrain depends on wind dh gradient

    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    int wind_dh_offset = w*h;

    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    float wx = cells[id].wind_speed_x;
    float wy = cells[id].wind_speed_y;

    int nb[4];
    nb[0] = !(id%w)     ? id : id-1;
    nb[1] = !((id+1)%w) ? id : id+1;
    nb[2] = !(id/w)     ? id : id-w;
    nb[3] = id/w+1==h  ? id : id+w;

    // chose direction that minimize the 2D wind speed
    int idx = nb[wx<0.0 ? 1: 0];
    int idy = nb[wy<0.0 ? 3: 2];

    wx = abs(wx);
    wy = abs(wy);

    float dhx = temp[wind_dh_offset + idx]; // wind speed
    float dhy = temp[wind_dh_offset + idy];
    float dh  = temp[wind_dh_offset + id];

    cells[id].wind_terrain = (wx * (dh - dhx) + wy * (dh - dhy)) / params->cell_size;

}

void SnowEventWindProc(StochasticSimulationCUDA::EventArg & args)
{
#ifdef DO_PROFILE
    cudaDeviceSynchronize();
    PROFILE_COUNT(SnowEventWindProcP, "SnowEventWindProc", 1);
#endif
    dim3 threads (32,32);
    dim3 blocks(ceil_div(args.w, threads.x), ceil_div(args.h, threads.y));

    SnowEventWindProcCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                               (SnowParams*)args.cuda_params,
                                               //                                           (curandState*)args.cuda_rnd_state,
                                               args.w,
                                               args.h);



    SnowEventWindVisuCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                               (SnowParams*)args.cuda_params,
                                               //                                           (curandState*)args.cuda_rnd_state,
                                               args.w,
                                               args.h);


    SnowEventWindSmoothGroundCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                       (SnowParams*)args.cuda_params,
                                                       (float*)args.cuda_temp,
                                                       args.w,
                                                       args.h);

    blocks = dim3(ceil_div(args.w/2, threads.x), ceil_div(args.h, threads.y));



    for(int i = 0; i< 500; ++i)
    {
        SnowEventWindHeightCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                     (SnowParams*)args.cuda_params,
                                                     (float*)args.cuda_temp,
                                                     args.w,
                                                     args.h, 0);
        SnowEventWindHeightCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                     (SnowParams*)args.cuda_params,
                                                     (float*)args.cuda_temp,
                                                     args.w,
                                                     args.h, 1);
    }

    blocks = dim3(ceil_div(args.w, threads.x), ceil_div(args.h, threads.y));

    SnowEventWindTerrainCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                  (SnowParams*)args.cuda_params,
                                                  (float*)args.cuda_temp,
                                                  args.w,
                                                  args.h);

#ifdef DO_PROFILE
    cudaDeviceSynchronize();
#endif

}

}
}
