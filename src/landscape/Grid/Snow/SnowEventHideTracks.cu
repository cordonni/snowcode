#include <cuda.h>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <builtin_types.h>
#include <curand_kernel.h>
#include <random>
#include <helper_math.h>

#include "SnowData.h"
#include "../CUDA/CudaGridUtils.h"
#include "../CUDA/StochasticSimulationCUDA.h"

namespace expressive {
namespace landscape {

extern "C"
__global__ void SnowEventHideTracksCUDA(SnowCell* cells,  const SnowParams* params, float *tracks, curandState* rand_state, int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x < 0 || x >= w || y < 0 || y >= h)
        return;

    int id = x + y*w;

    float impact = 1.0f - clamp(cells[id].snow_mvmt, 0.0f, 1.0f) * params->skier_snow_impact_on_track;
    cells[id].snow_mvmt = 0.0f;
    // "Erode" global tracks
    cells[id].ski_tracks *= impact;

#ifdef DETAILED_TRACKS
    int startx = (x * 8192.0 / w);
    int endx = ((x+1) * 8192.0 / w);
    int starty = (y * 8192.0 / h);
    int endy = ((y+1) * 8192.0 / h);

    // "Erode" detailed tracks
    for (int ly = starty; ly < endy; ly++) {
        for (int lx = startx; lx < endx; lx++) {
            tracks[ly * 8192 + lx] *= impact;
        }
    }
#endif
}

void SnowEventHideTracks(StochasticSimulationCUDA::EventArg &args)
{
    dim3 threads (32,32);
    dim3 blocks(ceil_div(args.w, threads.x), ceil_div(args.h, threads.y));

    SnowEventHideTracksCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                 (SnowParams*)args.cuda_params,
                                                 (float *)args.cuda_tracks,
                                                 (curandState*)args.cuda_rnd_state,
                                                 args.w, args.h);
}

}
}
