#include <cuda.h>
#include <builtin_types.h>
#include <curand_kernel.h>
#include <random>
#include <helper_math.h>

#include "SnowData.h"
#include "../CUDA/CudaGridUtils.h"
#include "../CUDA/StochasticSimulationCUDA.h"

namespace expressive {
namespace landscape {

extern "C"
__global__ void SnowEventPathsCUDA(SnowCell* cells, const SnowParams* params, short *buff1, short *buff2, int buffid, int w, int h)
{
    int x = (blockDim.x * blockIdx.x + threadIdx.x);
    int y = (blockDim.y * blockIdx.y + threadIdx.y);

    if(x >= w || y >= h)
        return;

    short *buffs[2];
    buffs[0] = buff1;
    buffs[1] = buff2;
    int id = y * w + x;
    int nb[4];
    nb[0] = !(id%w)     ? id : id-1;
    nb[1] = !((id+1)%w) ? id : id+1;
    nb[2] = !(id/w)     ? id : id-w;
    nb[3] = id/w+1==h  ? id : id+w;

    int nb_lower = 0;
    int highest = -1;
    for (int i = 0; i < 4; i++) {
        if (cells[nb[i]].bedrock < cells[id].bedrock) {
            nb_lower += 1;
            highest = max(buffs[!buffid][nb[i]], highest);
        }
    }
    if (nb_lower == 0)
        buffs[buffid][id] = 0;
    else if (highest > -1)
        buffs[buffid][id] = highest + 1;
}

extern "C"
__global__ void SnowEventPathsExport(SnowCell* cells, short *buffer, int w, int h)
{
    int x = (blockDim.x * blockIdx.x + threadIdx.x);
    int y = (blockDim.y * blockIdx.y + threadIdx.y);

    if(x >= w || y >= h)
        return;
    int id = y * w + x;
    cells[id].dist_to_exit = buffer[id] / 256.0;
}

extern "C"
__global__ void SnowEventPathsInit(short *buffer, int w, int h)
{
    int x = (blockDim.x * blockIdx.x + threadIdx.x);
    int y = (blockDim.y * blockIdx.y + threadIdx.y);

    if(x >= w || y >= h)
        return;
    int id = y * w + x;
    buffer[id] = -1;
}

void SnowEventPaths(StochasticSimulationCUDA::EventArg &args)
{
    dim3 threads (32,32);
    dim3 blocks(ceil_div(args.w, threads.x), ceil_div(args.h, threads.y));

    short *buff_ptrs[2];
    for (int i = 0; i < 2; i++) {
        cudaMalloc(buff_ptrs + i, args.w * args.h * sizeof(short));
        SnowEventPathsInit<<<blocks, threads>>>((short *) buff_ptrs[i], args.w, args.h);
    }

    for (int i = 0; i < 256; i++) {
        SnowEventPathsCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                (SnowParams*)args.cuda_params,
                                                buff_ptrs[0], buff_ptrs[1],
                                                i % 2, args.w, args.h);
    }

    SnowEventPathsExport<<<blocks, threads>>>((SnowCell*)args.cuda_cells, buff_ptrs[1], args.w, args.h);

    for (int i = 0; i < 2; i++) {
        cudaFree(buff_ptrs[i]);
    }
}

}
}
