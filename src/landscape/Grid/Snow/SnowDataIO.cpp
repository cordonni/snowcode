#include "SnowDataIO.h"

#include "../TerrainOperations.h"

namespace expressive {
namespace landscape {

void SnowDataIO::import(SnowSimuData* data, const GridDEM& dem, float cell_size)
{
    assert(!data->data_.empty());
    data->data_[0].params.cell_size = cell_size;
    data->data_[0].params.altitude_base = dem.minmax().first*cell_size;

    data->data_[0].resize((int)dem.width(), (int)dem.height());

    for(uint id = 0; id<dem.size(); ++id)
    {
        SnowCell c;
        c.bedrock = dem[id]*cell_size;
        c.wind_visu = (float)rand() / (float)RAND_MAX;

        data->data_[0].cells[id] = c;
    }
}

void SnowDataIO::importLayers(SnowSimuData* data, std::string dirname, float cell_size)
{
    GridDEM bedrock = TerrainOperations::loadTER(dirname + "/bedrock.ter");
    GridDEM snow = TerrainOperations::loadTER(dirname + "/snow.ter");

    data->data_[0].params.cell_size = cell_size;
    data->data_[0].params.altitude_base = bedrock.minmax().first*cell_size;

    data->data_[0].resize((int)bedrock.width(), (int)bedrock.height());

    for(uint id = 0; id<data->data_[0].size(); ++id)
    {
        SnowCell c;
        c.bedrock = bedrock[id]*cell_size;
        c.snow    = snow[id]   *cell_size;
        data->data_[0].cells[id] = c;
    }
}


void SnowDataIO::defaultParameters(SnowSimuData* data, const std::vector<float>& event_time)
{
    float cell_size = data->data_[0].params.cell_size;
    float altitude_base = data->data_[0].params.altitude_base;

    data->data_[0].events = event_time;
    data->data_[0].params = SnowParams();

    data->data_[0].params.cell_size = cell_size;
    data->data_[0].params.altitude_base = altitude_base;


}


void SnowDataIO::exportMaps(SnowSimuData* simu_data, std::string dir, int frame)
{
    SnowData& data = simu_data->data_[frame];
    float cell_size_ = data.params.cell_size;

    SnowCell tmp;
#define L(P) (int)((float*)(&tmp.P) - (float*)&tmp)

    GridDEM bedrock = data.getSeparateData(L(bedrock));
    GridDEM snow    = data.getSeparateData(L(snow));
    GridDEM ice     = snow;
    GridDEM illum   = data.getSeparateData(L(illumination));
    GridDEM &tracks = simu_data->renderSkiTracksSimulated();

    for (size_t i = 0; i<bedrock.size(); ++i)
    {
        bedrock[i] /= cell_size_;
        snow[i]    /= cell_size_;
        ice[i] = (float)(snow.id_to_xy(i).first < snow.width()/2);
    }

    TerrainOperations::saveTER(dir + "/bedrock.ter", bedrock, cell_size_);
    TerrainOperations::saveTER(dir + "/snow.ter",    snow,    cell_size_);
    TerrainOperations::saveImg(dir + "/illum.png",   illum, 0.0f, 0.305422);
    TerrainOperations::saveImg(dir + "/ski_tracks.png",  tracks);

}

void SnowDataIO::exportFrames(SnowSimuData* simu_data, std::string dir, int start_frame, int last_frame, int step_frame)
{
    for(int frame = start_frame; frame<last_frame; frame += step_frame)
        exportMaps(simu_data, dir, frame);
}

int SnowDataIO::getRefreshParamId()
{
    SnowParams tmp;
    return (int)((float*)(&tmp.update_time) - (float*)&tmp);
}



}
}
