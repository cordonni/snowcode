#include <cuda.h>
#include <builtin_types.h>
#include <curand_kernel.h>
#include <random>
#include <helper_math.h>

#include "SnowData.h"
#include "../CUDA/CudaGridUtils.h"
#include "../CUDA/StochasticSimulationCUDA.h"
#include "landscape/Utils/Profile.h"

namespace expressive {
namespace landscape {

#define MAX_SKIER_MVMT  2

/*
 * Convert global simulation coordinates into the detailed map system (8192x8192)
 */
extern "C"
__device__ int GlobalToLocal(float x, int global_size) {
    return min(max((x + 0.5) / global_size, 0.0), 1.0) * 8192;
}

extern "C"
__device__ void DrawDot(float *tracks, int x, int y, int r, float val) {
    for (int dy = max(y-r, 0); dy <= min(y+r, 8191); dy++) {
        for (int dx = max(x-r, 0); dx <= min(x+r, 8191); dx++) {
            tracks[dy*8192 + dx] = val;
        }
    }
}

extern "C"
__device__ float GetNextOrientation(SnowCell *cells, const SnowParams *params, curandState *state, int skier_id, int x, int y, int w, int h)
{
    int id = y * w + x;
    int rot_range = 2;
    float proba[5] = {0};
    float cur_elev = cells[id].bedrock + cells[id].snow;
    for (int ia = -rot_range; ia <= rot_range; ia++) {
        float a = cells[id].skier_rot[skier_id] + ia * params->skier_angle_step;
        int ax = clamp((int)lroundf(x + cells[id].skier_x[skier_id] + cos(a) * params->skier_lookahead), 0, w - 1);
        int ay = clamp((int)lroundf(y + cells[id].skier_y[skier_id] + sin(a) * params->skier_lookahead), 0, h - 1);
        int aid = ay * w + ax;

        float elev = cells[aid].bedrock + cells[aid].snow;
        float slope = atan2(cur_elev - elev, params->skier_lookahead * params->cell_size);
        float diff_slope = fabs(params->skier_target_slope - slope);
        float dweight = cells[aid].dist_to_exit;
        float weight = 1.0 / diff_slope;
        weight *= (cells[aid].snow > params->skier_min_snow); // Only keep directions with enough snow
        weight *= (elev < cur_elev);                          // Only keep directions going down
        weight *= dweight * (dweight > 0);
        proba[ia + rot_range] = proba[max(ia, 0)] + weight;
    }
    float choice = (1.0 - curand_uniform(state)) * proba[2 * rot_range];

    int iid = 2 * rot_range + 1;
    for (int i = 0; i < 2 * rot_range + 1; ++i)
        if (iid == 2 * rot_range + 1 && choice < proba[i])
            iid = i;

    if (iid != 2 * rot_range + 1)
        return cells[id].skier_rot[skier_id] + (iid - 1) * params->skier_angle_step;
    return cells[id].skier_rot[skier_id];
}

extern "C"
__device__ float GetSlope(SnowCell *cells, const SnowParams *params, float rot, int x, int y, int w, int h)
{
    int id = y * w + x;
    float curr_elev = cells[id].bedrock + cells[id].snow;
    float sx = curr_elev - (cells[id - (!!x)].bedrock + cells[id - (!!x)].snow);
    float sy = curr_elev - (cells[id - w * (!!y)].bedrock + cells[id - w * (!!y)].snow);
    return atan(abs(cos(rot) * sx) + abs(sin(rot) * sy) / params->cell_size);
}

extern "C"
__global__ void SnowEventRotateSkierCUDA(SnowCell* cells,  const SnowParams* params,
                                         curandState* rand_state, int offset_x, int offset_y, int w, int h)
{
    int x = (blockDim.x * blockIdx.x + threadIdx.x);
    int y = (blockDim.y * blockIdx.y + threadIdx.y);

    x = (2*y+1)%5-1 + 5*x + offset_x; // cross tiling pattern
    y += offset_y;

    if(x <0 || y <0 || x >= w || y >= h)
        return;

    int id = x + y*w;

    curandState state = rand_state[id];
    for (int skier_id = cells[id].nb_skiers - 1; skier_id >= 0; skier_id--) {
        cells[id].skier_rot[skier_id] = GetNextOrientation(cells, params, &state, skier_id, x, y, w, h);
    }
    rand_state[id] = state;
}

extern "C"
__global__ void SnowEventMoveSkierCUDA(SnowCell* cells,  const SnowParams* params, float *tracks,
                                       curandState* rand_state, int offset_x, int offset_y, int w, int h)
{
    int x = (blockDim.x * blockIdx.x + threadIdx.x);
    int y = (blockDim.y * blockIdx.y + threadIdx.y);

    x = (2*y+1)%5-1 + 5*x + offset_x; // cross tiling pattern
    y += offset_y;

    if(x <0 || y <0 || x >= w || y >= h)
        return;

    int id = x + y*w;

    curandState state = rand_state[id];
    for (int skier_id = cells[id].nb_skiers - 1; skier_id >= 0; skier_id--) {
        float next_rot = cells[id].skier_rot[skier_id];
        float next_x = x + cells[id].skier_x[skier_id] + cos(next_rot) * params->skier_speed;
        float next_y = y + cells[id].skier_y[skier_id] + sin(next_rot) * params->skier_speed;
        int round_x = lroundf(next_x);
        int round_y = lroundf(next_y);
        float cur_elev = cells[id].bedrock + cells[id].snow;

        // Variables for future drawing
#ifdef DETAILED_TRACKS
        float old_tx = GlobalToLocal(x + cells[id].skier_x[skier_id], w);
        float old_ty = GlobalToLocal(y + cells[id].skier_y[skier_id], h);
        float old_phase = cells[id].skier_phase[skier_id];
        float new_tx = GlobalToLocal(next_x, w);
        float new_ty = GlobalToLocal(next_y, h);
        float vx = new_tx - old_tx, vy = new_ty - old_ty;
        float step_max = max(abs(vx), abs(vy));
        float slope = GetSlope(cells, params, next_rot, x, y, w, h);
        float tgt_freq = 1.0 / params->skier_target_period;
        float tgt_amp = params->skier_target_amplitude;
        float amplitude = tgt_amp;
        float sin_tgt_angle = sin(params->skier_target_slope) /
                                  (2 * tgt_freq * sqrt(1.0 / (4 * tgt_freq * tgt_freq) + (4 * tgt_amp * tgt_amp)));
        float res_freq = sqrt(max(pow(sin(slope), 2) - pow(sin_tgt_angle, 2), 0.0f)) /
                          (4.0 * amplitude * sin_tgt_angle);
        res_freq = max(res_freq, tgt_freq);
        float phase_coef = res_freq * 2 * M_PI;
#endif

        // Move skier to next cell
        float cvt_ratio = (w * params->cell_size) / 8192.0f;
        bool moved = false;
        if (round_x != x || round_y != y) {
            int next_id = round_x + round_y * w;
            if (round_x >= 0 && round_x < w && round_y >= 0 && round_y < h &&
                    cells[next_id].nb_skiers < MAX_SKIERS_PER_CELL &&
                    cells[next_id].bedrock + cells[next_id].snow < cur_elev &&
                    cells[next_id].snow > params->skier_min_snow) {
                int next_nb = cells[next_id].nb_skiers;
                cells[next_id].skier_x[next_nb] = next_x - round_x;
                cells[next_id].skier_y[next_nb] = next_y - round_y;
                cells[next_id].skier_rot[next_nb] = next_rot;
#ifdef DETAILED_TRACKS
                cells[next_id].skier_phase[next_nb] = cells[id].skier_phase[skier_id] + (cvt_ratio * step_max * phase_coef);
#endif
                cells[next_id].ski_tracks = 1.0f;
                cells[next_id].avalanche_trigger = (curand_uniform(&state) < params->skier_avalanche_chance);
                cells[next_id].nb_skiers++;
                moved = true;
            }
            for (int i = skier_id + 1; i < cells[id].nb_skiers; i++) {
                cells[id].skier_x[i - 1] = cells[id].skier_x[i];
                cells[id].skier_y[i - 1] = cells[id].skier_y[i];
                cells[id].skier_rot[i - 1] = cells[id].skier_rot[i];
#ifdef DETAILED_TRACKS
                cells[id].skier_phase[i - 1] = cells[id].skier_phase[i];
#endif
            }
            cells[id].nb_skiers--;
        } else {
            cells[id].skier_x[skier_id] = next_x - round_x;
            cells[id].skier_y[skier_id] = next_y - round_y;
            cells[id].skier_rot[skier_id] = next_rot;
#ifdef DETAILED_TRACKS
            cells[id].skier_phase[skier_id] += cvt_ratio * step_max * phase_coef;
#endif
            cells[id].ski_tracks = 1.0f;
            moved = true;
        }

        // Draw refined tracks
#ifdef DETAILED_TRACKS
        if (moved) {
            float step_x = vx / step_max, step_y = vy / step_max;
            float tx = old_tx, ty = old_ty;
            float vd = sqrt(vx * vx + vy * vy);
            vx /= vd; vy /= vd;
            for (float step = 0; step < step_max; step += 0.5) {
                float final_y = sin(old_phase + (cvt_ratio * step * phase_coef)) * amplitude / cvt_ratio;
                tx = old_tx + step_x * step + vy * final_y;
                ty = old_ty + step_y * step - vx * final_y;
                DrawDot(tracks, tx, ty, 0, 1.0);
            }
        }
#endif
    }
    rand_state[id] = state;
}

void SnowEventMoveSkier(StochasticSimulationCUDA::EventArg &args)
{
#ifdef DO_PROFILE
    cudaDeviceSynchronize();
    PROFILE_COUNT(SnowEventMoveSkierP, "SnowEventMoveSkier", 10);
#endif

    dim3 threads (16,16);
    dim3 blocks(ceil_div(args.w, threads.x * 5), ceil_div(args.h, threads.y));

    if(blocks.x == 1)
        threads.x = ceil_div(args.w, 5);

    std::vector<int> dx = {0, -1, 0, 1, 0};
    std::vector<int> dy = {-1, 0, 0, 0, 1};

    while(dx.size())
    {
        int i = std::uniform_int_distribution<int>(0, dx.size()-1)(*args.rnd);
        std::swap(dx[i], dx.back());
        std::swap(dy[i], dy.back());
        SnowEventRotateSkierCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                      (SnowParams*)args.cuda_params,
                                                      (curandState*)args.cuda_rnd_state,
                                                      dx.back(), dy.back(),
                                                      args.w, args.h);
        SnowEventMoveSkierCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                    (SnowParams*)args.cuda_params,
                                                    (float *)args.cuda_tracks,
                                                    (curandState*)args.cuda_rnd_state,
                                                    dx.back(), dy.back(),
                                                    args.w, args.h);
        dx.pop_back();
        dy.pop_back();
    }

#ifdef DO_PROFILE
    cudaDeviceSynchronize();
#endif
}

}
}
