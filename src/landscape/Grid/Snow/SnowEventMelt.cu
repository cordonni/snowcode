#include <cuda.h>
#include <builtin_types.h>

#include "SnowData.h"
#include "../CUDA/CudaGridUtils.h"
#include <helper_math.h>

#include "landscape/Utils/Profile.h"



namespace expressive {
namespace landscape {

extern "C"
__global__ void SnowEventMeltCUDA(SnowCell* cells,  const SnowParams* params,  int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    float elevation = cells[id].bedrock;

    float temperature = params->temperature_base + params->temperature_offset*(elevation - params->altitude_base);
    temperature += cells[id].illumination * params->temperature_illum;
    float melt_amount =  -min(0.0f, (params->melt_max_temp - temperature) * params->melt_temp_offset);
    float snow = cells[id].snow;



    // stability:
    int nb[5];
    nb[0] = !(id%w)     ? id : id-1;
    nb[1] = !((id+1)%w) ? id : id+1;
    nb[2] = !(id/w)     ? id : id-w;
    nb[3] = id/w+1==h  ? id : id+w;

    float nbh[4];
    for(int i = 0; i<4; ++i)
        nbh[i] = cells[nb[i]].snow + cells[nb[i]].bedrock;

    float ch = snow + cells[id].bedrock;
    float slope = max(
                max(ch - nbh[0],ch - nbh[1]),
            /**/max(ch - nbh[2],ch - nbh[3]))/ params->cell_size;

    slope = max(0.0f, slope - params->stability_min_slope) * params->unstable_factor;

    // stability function
    float stability;

    if( temperature > params->melt_stable_temp)
    {
        float x = clamp((temperature - params->melt_unstable_temp) / (params->melt_stable_temp - params->melt_unstable_temp), 0.0f, 1.0f);
        stability = (1.0f - x) * params->melt_hot_stability + x* params->melt_medium_stability;
    }
    else
    {
        float x = clamp((temperature - params->melt_stable_temp) / (params->melt_freeze_temp - params->melt_stable_temp), 0.0f, 1.0f);
        stability = (1.0f - x) * params->melt_medium_stability + x* params->melt_freeze_stability;
    }

    if(stability < 0.0f)
    {
        stability *= slope;
    }
    else if(slope > 1.0f)
        stability *= slope;

    // first update layers
    // powder melt first, then unstable, then snow
    float powdery = cells[id].powdery;
    float unstable = cells[id].unstable_layer;
    float compacted = cells[id].compacted;
    float stable = snow - powdery - unstable;
    float pressure = snow - compacted;

    float melt = min(powdery, melt_amount);
    powdery -= melt;
    snow    -= melt;
    melt_amount -= melt;

    melt =  min(unstable, melt_amount);
    unstable -= melt;
    snow    -= melt;
    melt_amount -= melt;

    float compaction = cells[id].compacted / stable;
    if (stable < 0.001)
        compaction = 0.0;

    melt_amount *= params->melt_compaction_effect * (1.0f - compaction);

    snow = max(0.0f, snow - melt_amount);

    // available snow to be turned into unstabke
    float available = max(0.0f, snow - powdery - unstable);
    stability = clamp(stability, -available, unstable);

    cells[id].snow = snow;
    unstable = max(0.0f,unstable - stability);
    cells[id].unstable_layer = unstable;
    cells[id].powdery = powdery;
    cells[id].compacted = max(0.0f, min(snow - powdery - unstable, compacted + pressure * params->melt_compaction_pressure));

}

void SnowEventMelt(StochasticSimulationCUDA::EventArg & args)
{
#ifdef DO_PROFILE
    cudaDeviceSynchronize();
    PROFILE_COUNT(SnowEventMeltP, "SnowEventMelt", 100);
#endif
    dim3 threads (32,32);
    dim3 blocks(ceil_div(args.w, threads.x), ceil_div(args.h, threads.y));

    SnowEventMeltCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                            (SnowParams*)args.cuda_params,
                                            args.w,
                                            args.h);

#ifdef DO_PROFILE
    cudaDeviceSynchronize();
#endif
}

}
}
