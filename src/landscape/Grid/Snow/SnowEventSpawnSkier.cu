#include <cuda.h>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <builtin_types.h>
#include <curand_kernel.h>
#include <random>
#include <helper_math.h>

#include "SnowData.h"
#include "../CUDA/CudaGridUtils.h"
#include "../CUDA/StochasticSimulationCUDA.h"

namespace expressive {
namespace landscape {

extern "C"
__global__ void SnowEventSpawnSkierCUDA(SnowCell* cells,  const SnowParams* params, float *tracks, curandState* rand_state, int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x < 1 || x >= w - 1 || y < 1 || y >= h - 1)
        return;

    int id = x + y*w;

    curandState state = rand_state[id];
    if (cells[id].snow > params->skier_min_snow &&
            cells[id].nb_skiers < MAX_SKIERS_PER_CELL &&
            cells[id].dist_to_exit > 0.4 &&
            curand_uniform(&state) < params->skier_spawn_rate) {
        int oldnb = cells[id].nb_skiers;
        float offset_x = (curand_uniform(&state) * 2.0 - 1.0) * 0.2;
        float offset_y = (curand_uniform(&state) * 2.0 - 1.0) * 0.2;
        float elev[4] = {cells[id - 1].bedrock + cells[id - 1].snow,
                         cells[id + 1].bedrock + cells[id + 1].snow,
                         cells[id - w].bedrock + cells[id - w].snow,
                         cells[id + w].bedrock + cells[id + w].snow};
        cells[id].skier_x[oldnb] = offset_x;
        cells[id].skier_y[oldnb] = offset_y;
        cells[id].skier_rot[oldnb] = atan2(elev[2] - elev[3], elev[0] - elev[1]);
#ifdef DETAILED_TRACKS
        cells[id].skier_phase[oldnb] = curand_uniform(&state) * 10.0f;
#endif
        cells[id].ski_tracks = 1.0f;
        cells[id].nb_skiers++;
    }
    rand_state[id] = state;
}

void SnowEventSpawnSkier(StochasticSimulationCUDA::EventArg &args)
{
    dim3 threads (32,32);
    dim3 blocks(ceil_div(args.w, threads.x), ceil_div(args.h, threads.y));

    SnowEventSpawnSkierCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                            (SnowParams*)args.cuda_params,
                                            (float *)args.cuda_tracks,
                                            (curandState*)args.cuda_rnd_state,
                                            args.w,
                                            args.h);

}

}
}
