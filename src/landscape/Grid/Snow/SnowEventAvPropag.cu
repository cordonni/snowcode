#include <cuda.h>
#include <builtin_types.h>

#include "SnowData.h"
#include "../CUDA/CudaGridUtils.h"
#include <vector_functions.h>
#include <helper_math.h>

#include "landscape/Utils/Profile.h"


namespace expressive {
namespace landscape {

extern "C"
__global__ void SnowEventAvPropagInitCUDA(SnowCell* __restrict__ cells, float* __restrict__ temp, int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    float trigger = cells[id].avalanche_trigger;
    temp[id] = __int_as_float(trigger>0.5 ? id : -1);
}



extern "C"
__global__ void SnowEventAvPropagXCUDA(SnowCell* __restrict__ cells, float* __restrict__ temp, int w, int h, int offset)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    int near_id[3];
    near_id[0] = __float_as_int(temp[id]);


    near_id[1] = __float_as_int(temp[(id%w)<offset     ? id : id-offset  ]);
    near_id[2] = __float_as_int(temp[(id%w)>=w-offset  ? id : id+offset  ]);

    int nearest = -1;
    float dist = (float)(2*(w+h));
    for(int i = 0; i<3; ++i)
        if(near_id[i] != -1)
        {
            float value = cells[near_id[i]].avalanche_trigger;

            int xx = near_id[i] % w;
            int yy = near_id[i] / w;
            float d =  length(make_float2(
                                  (float)(x - xx),
                                  (float)(y - yy))) - value;
            if(d < dist)
            {
                nearest = near_id[i];
                dist =d;
            }
        }

    temp[w*h+id] = __int_as_float(nearest);
}

extern "C"
__global__ void SnowEventAvPropagYCUDA(SnowCell* __restrict__ cells, float* __restrict__ temp, int w, int h, int offset)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    int near_id[3];
    near_id[0] = __float_as_int(temp[w*h+id]);

    near_id[1] = __float_as_int(temp[w*h+((id/w)<offset     ? id : id-offset*w)]);
    near_id[2] = __float_as_int(temp[w*h+((id/w)>=h-offset  ? id : id+offset*w)]);

    int nearest = -1;
    float dist = (float)(2*(w+h));
    for(int i = 0; i<3; ++i)
        if(near_id[i] != -1)
        {
            float value = cells[near_id[i]].avalanche_trigger;

            int xx = near_id[i] % w;
            int yy = near_id[i] / w;
            float d =  length(make_float2(
                                  (float)(x - xx),
                                  (float)(y - yy))) - value;
            if(d < dist)
            {
                nearest = near_id[i];
                dist =d;
            }
        }


    temp[id] = __int_as_float(nearest);
}

extern "C"
__global__ void SnowEventAvPropagEndCUDA(SnowCell* __restrict__ cells, float* __restrict__ temp, int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    int id = x + y*w;
    int nearest = __float_as_int(temp[id]);

    if(nearest == -1)
        return; // shouldn't happen

    float target_dist = cells[nearest].avalanche_trigger;
    int xx = nearest % w;
    int yy = nearest / w;
    float dist = length(make_float2(
                            (float)(x - xx),
                            (float)(y - yy)));


    // Each cell with sufficient slope turns into unstable snow
    if(target_dist > dist)
        cells[id].moving = 1.0;
}

extern "C"
__global__ void SnowEventAvPropagCloseCUDA(SnowCell* __restrict__ cells, int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    cells[id].avalanche_trigger = 0.0;
}

void SnowEventAvPropag(StochasticSimulationCUDA::EventArg & args)
{
#ifdef DO_PROFILE
    cudaDeviceSynchronize();
    PROFILE_COUNT(SnowEventAvPropagP, "SnowEventAvPropag", 100);
#endif
    dim3 threads (32,32);
    dim3 blocks(ceil_div(args.w, threads.x), ceil_div(args.h, threads.y));

    // number of iterations is ceil of log_2(max(w, h))
    int num_iter = max(args.w, args.h);
    double lg = std::log2((double)num_iter);
    num_iter = (int)std::ceil(lg);

    //ensure number of iteration is even, to make sure result is stored into the trigger layer
    if((num_iter%2))
        ++num_iter;

    int offset = 1;
    for(int i= 0; i<num_iter; ++i)
        offset*=2;

    SnowEventAvPropagInitCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                   (float*)args.cuda_temp,
                                                   args.w,
                                                   args.h);


    for(int i = 0; i< num_iter; ++i)
    {
        offset/=2;
        SnowEventAvPropagXCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                    (float*)args.cuda_temp,
                                                    args.w,
                                                    args.h, offset);
        SnowEventAvPropagYCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                    (float*)args.cuda_temp,
                                                    args.w,
                                                    args.h, offset);
    }
    SnowEventAvPropagEndCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                  (float*)args.cuda_temp,
                                                  args.w,
                                                  args.h);
    SnowEventAvPropagCloseCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                    args.w,
                                                    args.h);

#ifdef DO_PROFILE
    cudaDeviceSynchronize();
#endif
}

}
}
