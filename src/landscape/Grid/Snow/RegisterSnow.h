#ifndef REGISTERSNOWUI_H
#define REGISTERSNOWUI_H

#include "landscape/Grid/Snow/SnowRequired.h"

namespace expressive {
namespace landscape {
class ExpressiveSimulationManager;

void SNOW_API registerSnowLayers(ExpressiveSimulationManager* esm);

void SNOW_API registerSnowParameters (ExpressiveSimulationManager* esm);

void SNOW_API registerSnowEvents();

}
}

#endif // REGISTERSNOWUI_H
