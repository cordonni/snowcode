#include <cuda.h>
#include <builtin_types.h>
#include <curand_kernel.h>
#include <math_constants.h>
#include <vector_functions.h>
#include <helper_math.h>

#include "../CUDA/CudaGridUtils.h"
#include "../CUDA/StochasticSimulationCUDA.h"

#include "SnowData.h"

#include "landscape/Utils/Profile.h"



namespace expressive {
namespace landscape {

extern "C"
__global__ void SnowEventWindCUDA(SnowCell* __restrict__ cells,
                                  const SnowParams* __restrict__ params,
                                  curandState* __restrict__ rand_state,
                                  int w,
                                  int h,
                                  int offset_x,
                                  int offset_y)
{
    int x = (blockDim.x * blockIdx.x + threadIdx.x);
    int y = (blockDim.y * blockIdx.y + threadIdx.y);

    x = (2*y+1)%5-1 + 5*x + offset_x; // cross tiling pattern
    y += offset_y;

    if(x <0 || y <0 || x >= w || y >= h)
        return;

    int id = x+y*w;

    // wind impact is global (f(wind_terrain, wind_h, bedrock))
    // and local f(curvature)

    float wx = cells[id].wind_speed_x;
    float wy = cells[id].wind_speed_y;
    float wh = cells[id].wind_h;
    float wt = cells[id].wind_terrain;
    float bedrock = cells[id].bedrock;
    float snow = cells[id].snow;

    // neighbors
    int nb[4];
    nb[0] = !(id%w)     ? id : id-1;
    nb[1] = !((id+1)%w) ? id : id+1;
    nb[2] = !(id/w)     ? id : id-w;
    nb[3] = id/w+1==h  ? id : id+w;

    // neighbors height
    float nbh[4];
    for(int i = 0; i<4; ++i)
        nbh[i] = cells[nb[i]].bedrock + cells[nb[i]].snow;

    // local height
    float local_h = bedrock + snow;

    // chose direction that maximize the 2D wind speed
    int iix = wx>0.0 ? 1: 0;
    int iiy = wy>0.0 ? 3: 2;
    int idx = nb[iix];
    int idy = nb[iiy];
    wx = abs(wx);
    wy = abs(wy);

    float wn = wx+wy;

    // wind strength
    float wind_terrain =  params->wind_snow_terrain * wt;
    float wind_curve = params->wind_snow_curve * (0.667 * (wx * (local_h - 0.5f * (nbh[0]+nbh[1])) + wy * (local_h - 0.5f * (nbh[2] + nbh[3]))));

    // amount removed from snow layer depends on curve, wind_terrain
    float transport = (wind_curve) / params->cell_size;
    transport = clamp(transport, 0.0f, min(snow, params->wind_snow_capacity));
    transport = min(transport, max(0.0f, local_h - wh));
    snow -= transport;
    cells[id].debug = transport;

    // wind terrain affect stability
    float powdery = min(snow, cells[id].powdery);
    float unstable = min(cells[id].unstable_layer, snow-powdery);
    float available = max(0.0f, snow - unstable - cells[id].powdery);

    float slope = max(
                max(
                    (local_h - nbh[0]) / params->cell_size,
                /**/(local_h - nbh[1]) / params->cell_size),
            /**/max(
                (local_h - nbh[2]) / params->cell_size,
            /**/(local_h - nbh[3]) / params->cell_size));
    float unstable_proportion = max(0.0f, slope - params->stability_min_slope) * params->unstable_factor;

    float unstability = min(available, unstable_proportion * max(0.0f, wind_terrain)*params->wind_plates);

    cells[id].unstable_layer = unstable + unstability;
    cells[id].powdery = powdery;
    cells[id].snow = snow;



    if(!wn)
    {
        //cells[id].snow += transport;
    }
    else if(idx != id && idy != id)
    {
        cells[idx].snow += transport * wx/wn;
        cells[idy].snow += transport * wy/wn;
    }


}

void SnowEventWind(StochasticSimulationCUDA::EventArg & args)
{
#ifdef DO_PROFILE
    cudaDeviceSynchronize();
    PROFILE_COUNT(SnowEventWindP, "SnowEventWind", 100);
#endif

    dim3 threads (32,32);
    dim3 blocks(ceil_div(args.w, threads.x * 5), ceil_div(args.h, threads.y));

    if(blocks.x == 1)
        threads.x = ceil_div(args.w, 5);

    std::vector<int> dx = {0, -1, 0, 1, 0};
    std::vector<int> dy = {-1, 0, 0, 0, 1};

    while(dx.size())
    {
        int i = std::uniform_int_distribution<int>(0, dx.size()-1)(*args.rnd);
        std::swap(dx[i], dx.back());
        std::swap(dy[i], dy.back());
        SnowEventWindCUDA<<<blocks, threads>>>(
                                                 (SnowCell*)args.cuda_cells,
                                                 (SnowParams*)args.cuda_params,
                                                 (curandState*)args.cuda_rnd_state,
                                                 args.w,
                                                 args.h,
                                                 dx.back(),
                                                 dy.back());

        dx.pop_back();
        dy.pop_back();
    }

#ifdef DO_PROFILE
    cudaDeviceSynchronize();
#endif

}

}
}
