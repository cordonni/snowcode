#include <cuda.h>
#include <builtin_types.h>
#include <curand_kernel.h>
#include <helper_math.h>

#include "../CUDA/CudaGridUtils.h"
#include "../CUDA/StochasticSimulationCUDA.h"

#include "SnowData.h"

#include "landscape/Utils/Profile.h"


namespace expressive {
namespace landscape {

extern "C"
__global__ void SnowEventDiffusionCUDA(SnowCell* cells,  const SnowParams* params, int offset_x, int offset_y, int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    x = (2*y+1)%5-1 + 5*x + offset_x; // cross tiling pattern
    y += offset_y;

    if(x <0 || y <0 || x >= w || y >= h)
        return;

    int id = x+y*w;

    // transport powdery snow, depending on terrain slopes, if slope is low enough

    int nb[5];
    nb[0] = !(id%w)     ? id : id-1;
    nb[1] = !((id+1)%w) ? id : id+1;
    nb[2] = !(id/w)     ? id : id-w;
    nb[3] = id/w+1==h  ? id : id+w;
    nb[4] = id;

    float elev[5];
    for(int i = 0; i<5; ++i)
        elev[i] = cells[nb[i]].bedrock + cells[nb[i]].snow;

    float amount[4];

    float incoming = 0.0f;
    float outgoing = 0.0f;
    float max_slope = 0.0f;
    for(int i = 0; i<4; ++i)
    {
        amount[i] = (elev[4] - elev[i]) / params->cell_size;
        max_slope = max(max_slope, amount[i]);
        if(amount[i] > 0.0f)
        {
            amount[i] = max(0.0f, amount[i] - params->diffusion_rest_angle) * params->diffusion_slope_factor;
            outgoing += amount[i];
        }
        else
        {
            amount[i] = clamp((amount[i] + params->diffusion_rest_angle) * params->diffusion_slope_factor,
                              - cells[nb[i]].powdery, 0.0f);
            incoming -= amount[i];
        }
    }

    float powder = cells[id].powdery;
    float snow = cells[id].snow;

    float available = min(outgoing, powder + incoming);

    for(int i = 0; i<4; ++i)
    {
        float factor = (amount[i] > 0.0f) ? available / outgoing : 1.0f;
        float displaced = factor * amount[i];
        cells[nb[i]].powdery += displaced;
        cells[nb[i]].snow += displaced;
        cells[nb[i]].snow_mvmt += abs(displaced);
        powder-=displaced;
        snow-=displaced;

    }

    powder = max(0.0f, powder);
     snow = max(0.0f, snow);

    //store some of the powder as snow
    //slope of powder base

    cells[id].snow = snow;
    cells[id].powdery = powder;
    cells[id].snow_mvmt += abs(cells[id].snow - snow);
}

void SnowEventDiffusion(StochasticSimulationCUDA::EventArg & args)
{
#ifdef DO_PROFILE
    cudaDeviceSynchronize();
    PROFILE_COUNT(SnowEventDiffusionP, "SnowEventDiffusion", 100);
#endif
    //    std::cerr << "Cells " << cells << " , params " << params << ", rnd " << rnd_state
    //              << ", w " << w << ", h " << h << std::endl;

    dim3 threads (32,32);
    dim3 blocks(ceil_div(args.w, threads.x * 5), ceil_div(args.h, threads.y));

    if(blocks.x == 1)
        threads.x = ceil_div(args.w, 5);

    std::vector<int> dx = {0, -1, 0, 1, 0};
    std::vector<int> dy = {-1, 0, 0, 0, 1};

    while(dx.size())
    {
        int i = std::uniform_int_distribution<int>(0, dx.size()-1)(*args.rnd);
        std::swap(dx[i], dx.back());
        std::swap(dy[i], dy.back());
        SnowEventDiffusionCUDA<<<blocks, threads>>>(
                                                      (SnowCell*)args.cuda_cells,
                                                      (SnowParams*)args.cuda_params,
                                                      dx.back(),
                                                      dy.back(),
                                                      args.w,
                                                      args.h);
        dx.pop_back();
        dy.pop_back();
    }

#ifdef DO_PROFILE
    cudaDeviceSynchronize();
#endif
}

}
}
