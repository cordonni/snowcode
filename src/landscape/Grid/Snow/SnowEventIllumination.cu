#include <cuda.h>

#include <builtin_types.h>
#include <vector_functions.h>
#include <helper_math.h>

#include <curand_kernel.h>
#include <math_constants.h>

#include "../CUDA/CudaGridUtils.h"
#include "../CUDA/StochasticSimulationCUDA.h"

#include "../SunExposure/sunexposure.h"

#include "SnowData.h"

#include "landscape/Utils/Profile.h"


namespace expressive {
namespace landscape {

__global__ void SnowEventIlluminationCUDAinit(SnowCell* cells,
                                              float* __restrict__ temp,
                                              int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    temp[x + y*w] = 0.0f;
}

__global__ void SnowEventIlluminationCUDA(SnowCell* __restrict__ cells, const SnowParams* params,
                                          float* __restrict__ temp,
                                          const float4 sundir, int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    float cell_size = params->cell_size;

    float dt = cell_size;
    float t = 0.0f;
    float4 ray_start = make_float4(float(x)*cell_size, cells[id].bedrock, float(y)*cell_size, 0.0);

    float illum = 1.0f;


    for(int i = 0; i<2048; ++i)
    {
        t+= dt;
        float4 ray_cur = ray_start + t * sundir;

        x = floor(ray_cur.x/cell_size);
        y = floor(ray_cur.z/cell_size);

        if( x>=w || y >=h || x<0 || y<0)
            break;

        float4 nearest_terrain = ray_cur;
        nearest_terrain.y = cells[x+y*w].bedrock;

        float4 terrain_dir = nearest_terrain - ray_start;
        terrain_dir = normalize(terrain_dir);

        float sun_amount = clamp(acos(dot(terrain_dir, sundir)) / 0.1309f, 0.0f, 1.0f)*0.5f;

        if(nearest_terrain.y > ray_cur.y)
            sun_amount = 0.5 - sun_amount;
        else
            sun_amount = 0.5 + sun_amount;

        illum = min(illum, sun_amount);

        if(illum < 0.00001)
            break;
    }

    float xm = cells[!(id%w)     ? id : id-1].bedrock;
    float xp = cells[!((id+1)%w) ? id : id+1].bedrock;
    float ym = cells[!(id/w)     ? id : id-w].bedrock;
    float yp = cells[id/w+1==h  ? id : id+w].bedrock;

    float gx = xm-xp;
    float gy = ym-yp;
    float gh = 2.0f*params->cell_size;

    float l = sqrt(gx*gx + gh*gh + gy*gy);

    float dp = (gx * sundir.x + gh * sundir.y + gy * sundir.z ) / l;

    temp[id] += illum * max(0.0f, dp) / 24.0f;

}

__global__ void SnowEventGICUDA(SnowCell* __restrict__ cells, const SnowParams* params,
                                curandState* __restrict__ rand_state,
                                float* __restrict__ temp,
                                int w, int h)
{

    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    float result = 0.0f;

    // terrain normal
    float xm = cells[!(id%w)     ? id : id-1].bedrock;
    float xp = cells[!((id+1)%w) ? id : id+1].bedrock;
    float ym = cells[!(id/w)     ? id : id-w].bedrock;
    float yp = cells[id/w+1==h   ? id : id+w].bedrock;

    float normal_x = xm-xp;
    float normal_y = ym-yp;
    float normal_h = 2.0f * params->cell_size;

    float l = sqrt(normal_x*normal_x + normal_h*normal_h + normal_y*normal_y);

    normal_x /= l;
    normal_y /= l;
    normal_h /= l;

    float height = cells[id].bedrock;

    curandState local_state = rand_state[x + y*w];

    const int num_theta = 40;
    const int num_r = 40;
    float num_r_p = powf(float(num_r), 1.5f);
    float ao = 0.0f;

    for(int step_theta = 0; step_theta<num_theta; ++step_theta)
    {
        float rnd_th = //curand_uniform(&local_state)
                (float)step_theta / (float)(num_theta)
                * CUDART_PI_F*2.0f;

        float cos_th = cosf(rnd_th);
        float sin_th = sinf(rnd_th);

        float shadow_height = -10000.0f;
        float shadow_dx = 0.0f;
        float shadow_dy = 0.0f;


        for(int step_r = 1; step_r<num_r+1; ++step_r)
        {

            float prev_r = powf((float(step_r)-0.5f), 1.5f) / num_r_p * params->illum_radius;
            float next_r = powf((float(step_r)+0.5f), 1.5f) / num_r_p * params->illum_radius;

            float factor = next_r*next_r - prev_r*prev_r;

            float rnd_r = powf((float)step_r, 1.5f) / num_r_p * params->illum_radius / params->cell_size;

            float dx = rnd_r * cos_th;
            float dy = rnd_r * sin_th;

            int xx = x + (int)(dx > 0.0 ? ceil(dx) : floor(dx));
            int yy = y + (int)(dy > 0.0 ? ceil(dy) : floor(dy));

            if(xx < 0 ||  xx >=w ||yy <0 || yy>=h)
                continue;


            int hit_id = xx + yy*w;

            float hit_h = cells[hit_id].bedrock;

            // ray
            float dh = (hit_h - height) / params->cell_size;
            float lr = sqrtf(dx*dx+dh*dh+dy*dy);
            dx /= lr;
            dh /= lr;
            dy /= lr;

            if(dh < shadow_height-0.1f)
                continue;

            shadow_height = dh;
            shadow_dx = dx;
            shadow_dy = dy;

            factor *= clamp(smoothstep(-0.1f, 0.00f, dh - shadow_height), 0.0f, 1.0f);

            // normal at hit position

            xm = cells[!(hit_id%w)     ? hit_id : hit_id-1].bedrock;
            xp = cells[!((hit_id+1)%w) ? hit_id : hit_id+1].bedrock;
            ym = cells[!(hit_id/w)     ? hit_id : hit_id-w].bedrock;
            yp = cells[hit_id/w+1==h  ? hit_id : hit_id+w].bedrock;

            float hit_norm_x = xm-xp;
            float hit_norm_y = ym-yp;
            float hit_norm_h = 2.0f * params->cell_size;

            l = sqrtf(hit_norm_x*hit_norm_x + hit_norm_h*hit_norm_h + hit_norm_y*hit_norm_y);
            hit_norm_x /= l;
            hit_norm_y /= l;
            hit_norm_h /= l;


            float illum = temp[hit_id];
            // light leave at full strength at hit normal
            illum *= -min(0.0f, dx * hit_norm_x + dh * hit_norm_h + dy * hit_norm_y);
            // light arrives at full strength at terrain normal
            illum *= max(0.0f, dx * normal_x + dh * normal_h + dy * normal_y);
            // influence decrease with square distance
            illum /= lr*lr*params->cell_size*params->cell_size;

            result += illum*factor;
        }

        // compute ao

        float cos_theta = max(0.0f, shadow_dx * normal_x + shadow_height * normal_h + shadow_dy * normal_y);
        float sin_theta = sqrtf(1.0f - cos_theta*cos_theta);
        //ao += acosf(cos_theta)/CUDART_PI_F*0.5f;
        //ao += 1.0f - cos_theta*cos_theta;
        ao += pow(max(0.0f,sin_theta-0.8f) / 0.2f, 10.0f);
    }
   // shouldn't be needed
    result /= float(num_theta);
    ao     /= float(num_theta);

    cells[id].illumination =
            temp[id] * params->illum_sun
            + result * params->illum_factor
            + ao * params->illum_ao;
    rand_state[x + y*w] = local_state;


}

void SnowEventIllumination(StochasticSimulationCUDA::EventArg & args)
{
#ifdef DO_PROFILE
    cudaDeviceSynchronize();
    PROFILE_COUNT(SnowEventIlluminationP, "SnowEventIllumination", 1);
#endif
    dim3 threads (32,32);
    dim3 blocks(ceil_div(args.w, threads.x), ceil_div (args.h, threads.y));

    SnowEventIlluminationCUDAinit<<<blocks, threads>>> (
                                                         (SnowCell*)args.cuda_cells,
                                                         (float*)args.cuda_temp,
                                                         args.w, args.h
                                                         );

    SunExposure sun;
    sun.setCenter(Vector3());
    sun.setLatitude(((SnowParams*)args.cpu_params)->latitude);
    sun.setMonth(((SnowParams*)args.cpu_params)->month);
    sun.setNorthOrientation(0.0, 0.0, -1.0);
    sun.rotateNorth(((SnowParams*)args.cpu_params)->compass);


    // averge illumination over 24 hours.

    for(double minutes = 0.0; minutes < 1440.0; minutes +=60.0)
    {
        sun.setTime(minutes);


        if(sun.position().y < 0.0)
            continue;

        Vector3 sundir = sun.position().normalized();


        float4 sun_cuda = make_float4((float)sundir.x, (float)sundir.y, (float)sundir.z, 0.0f);


        SnowEventIlluminationCUDA<<<blocks, threads>>> (
                                                         (SnowCell*)args.cuda_cells,
                                                         (SnowParams*)args.cuda_params,
                                                         (float*)args.cuda_temp,
                                                         sun_cuda,
                                                         args.w, args.h
                                                         );

    }

    // global illumination
    SnowEventGICUDA<<<blocks, threads>>> (
                                           (SnowCell*)args.cuda_cells,
                                           (SnowParams*)args.cuda_params,
                                           (curandState*)args.cuda_rnd_state,
                                           (float*)args.cuda_temp,
                                           args.w, args.h
                                           );


#ifdef DO_PROFILE
    cudaDeviceSynchronize();
#endif

}

}
}
