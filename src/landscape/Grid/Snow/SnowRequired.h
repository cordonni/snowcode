#ifndef SNOWREQUIRED_H
#define SNOWREQUIRED_H

#if !defined(BUILD_STATIC) && (defined ( _WIN32 ) || defined( _WIN64 ))
#   if defined( BUILD_SNOW)
#        define SNOW_API __declspec( dllexport )
#    else
#        define SNOW_API __declspec( dllimport )
#    endif
#else //Probably Linux
#   ifdef __APPLE__
#       define SNOW_API
#   else // Linux probably
#        define SNOW_API
#   endif
#endif


#endif // SNOWREQUIRED_H
