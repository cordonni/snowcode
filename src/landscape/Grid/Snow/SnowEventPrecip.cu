#include <cuda.h>
#include <builtin_types.h>

#include "SnowData.h"
#include "../CUDA/CudaGridUtils.h"
#include <helper_math.h>

#include "landscape/Utils/Profile.h"



namespace expressive {
namespace landscape {

extern "C"
__global__ void SnowEventPrecipCUDA(SnowCell* cells,  const SnowParams* params,  int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    float elevation = cells[id].bedrock;



    float temperature = params->temperature_base + params->temperature_offset*(elevation - params->altitude_base);
    float snow_precip =  clamp((params->precip_max_temp - temperature) * params->precip_temp_offset, 0.0f, params->precip_max);

    // snow precipitation only depends on altitude, not on illumination
    temperature += cells[id].illumination * params->temperature_illum;

    float snow = cells[id].snow  + snow_precip;

    // stability:
    // all stable above the rest angle, then stability decrease linearly
    // proportion of falling snow is unstable (= f(slope)
    // propotion is already moving

    int nb[5];
    nb[0] = !(id%w)     ? id : id-1;
    nb[1] = !((id+1)%w) ? id : id+1;
    nb[2] = !(id/w)     ? id : id-w;
    nb[3] = id/w+1==h  ? id : id+w;

    float nbh[4];
    for(int i = 0; i<4; ++i)
        nbh[i] = cells[nb[i]].snow + cells[nb[i]].bedrock;


    float ch = cells[id].snow + cells[id].bedrock;
    float slope = max(
                max(
                    (ch - nbh[0]) / params->cell_size,
                /**/(ch - nbh[1]) / params->cell_size),
            /**/max(
                (ch - nbh[2]) / params->cell_size,
            /**/(ch - nbh[3]) / params->cell_size));


    float unstable_proportion = max(0.0f, slope - params->stability_min_slope) * params->unstable_factor;
    float moving_temperature =  max(0.0, params->moving_max_temp - temperature) * params->moving_temp_offset;
    float moving_proportion =  max(0.0f, slope - params->moving_min_slope - moving_temperature) * params->moving_propotion;

    moving_proportion = min(moving_proportion, 1.0f);
    unstable_proportion = min(unstable_proportion, 1.0f - moving_proportion);

    float powdery = cells[id].powdery;
    float unstable = cells[id].unstable_layer;

    powdery = clamp(powdery + snow_precip * moving_proportion, 0.0f, max(0.0f, snow - powdery - unstable));
    unstable = clamp(unstable + snow_precip * unstable_proportion, 0.0f, max(0.0f, snow - powdery - unstable));

    cells[id].snow = snow;
    cells[id].unstable_layer = unstable;
    cells[id].powdery        = powdery;
    cells[id].snow_mvmt += snow_precip;

}

void SnowEventPrecipStart(StochasticSimulationCUDA::EventArg & args)
{
    args.event_freq[SnowEventPrecipID]     = 0.2f;
    args.event_freq[SnowEventPrecipStopID] = 3.0f;
}

void SnowEventPrecipStop(StochasticSimulationCUDA::EventArg & args)
{
    args.event_freq[SnowEventPrecipID]     = 1e10f;
    args.event_freq[SnowEventPrecipStopID] = 1e10f;
}


void SnowEventPrecip(StochasticSimulationCUDA::EventArg & args)
{
#ifdef DO_PROFILE
    cudaDeviceSynchronize();
    PROFILE_COUNT(SnowEventPrecipP, "SnowEventPrecip", 100);
#endif
    dim3 threads (32,32);
    dim3 blocks(ceil_div(args.w, threads.x), ceil_div(args.h, threads.y));

    SnowEventPrecipCUDA<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                             (SnowParams*)args.cuda_params,
                                             args.w,
                                             args.h);

#ifdef DO_PROFILE
    cudaDeviceSynchronize();
#endif
}

}
}
