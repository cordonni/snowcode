#include "SnowData.h"

namespace expressive {
namespace landscape {

SnowCell::SnowCell() :
    illumination {0.0f},
    bedrock {0.0f},
    snow {0.0f},
    powdery{0.0f},
    wind_speed_x{0.f},
    wind_speed_y{0.f},
    wind_pressure{0.f},
    wind_visu{0.f},
    wind_h{0.0f},
    wind_terrain{0.0f},
    wind_transport{0.f},
    unstable_layer{0.0f},
    compacted{0.0f},
    avalanche_trigger{0.0f},
    moving{0.0f},
    flow{0.0f, 0.0f, 0.0f, 0.0f},
    ski_tracks{0.0f},
    nb_skiers{0},
    snow_mvmt{0},
    dist_to_exit{0},
    debug{0.0f}
{

}

SnowParams::SnowParams() :
    // simulation parameters. Set by import
    cell_size {0.0f},
    altitude_base {0.0f},

    //temporal zoom
    update_time{1.0f}, //  days

    // global parameters
    latitude{45.0f},
    compass{0.0f},
    month{12.0f},
    temperature_base{1.0f},
    temperature_offset{-0.01f},
    temperature_illum{20.0f},
    gravity{9.8f},
    illum_factor{0.9f},
    illum_radius{1000.0f},
    illum_sun{0.9f},
    illum_ao{0.1f},

    // render
    show_wind{0.0f},
    show_wind_cap{0.0f},
    show_unstable{0.0f},
    show_debug{0.0f},

    // precipitation parameter
    precip_max_temp{0.0f},
    precip_temp_offset{0.001f},
    precip_max{1000.0f},
    stability_min_slope{0.3f}, //0.57f
    unstable_factor{1.0f},//0.1f
    moving_max_temp{-10.0f},
    moving_temp_offset{0.001f},
    moving_min_slope{0.5f}, //0.5f
    moving_propotion{5.0f},

    // melt parameters
    melt_max_temp{0.0f},
    melt_temp_offset{0.001f},
    melt_unstable_temp{5.0f},
    melt_stable_temp{-5.0f},
    melt_freeze_temp{-20.0f},
    melt_hot_stability{1.0e-4f},//-0.1
    melt_medium_stability{1.0e-4f},//0.1f
    melt_freeze_stability{0.0f},
    melt_compaction_effect{1e-4f},
    melt_compaction_pressure{1e-3f},

    // diffusion
    diffusion_slope_factor{0.1f},
    diffusion_rest_angle{0.5f}, //0.57f

    //wind
    wind_compass{0.0f},
    wind_strength{10.0f},
    wind_altitude_offset{0.001f},
    wind_terrain_force{0.5f},
    wind_fall_offset{0.7f},
    wind_smooth_ground{10.0f},
    wind_snow_curve{0.01f},
    wind_snow_terrain{0.1f},
    wind_snow_capacity{1.f},
    wind_plates{0.1f},

    //avalanche
    av_rest_angle{30.0f},
    av_max_simulated{0.05f},
    av_dt{0.1f},
    viscosity{0.0f},

    // skiers
    skier_target_slope{0.577f},
    skier_target_amplitude{5.0f},
    skier_target_period{90.0f},
    skier_min_snow{1.0f},
    skier_spawn_rate{0.0001f},
    skier_speed{0.3f},
    skier_lookahead{2.0f},
    skier_angle_step{0.2545f},
    skier_avalanche_chance{0.005},
    skier_snow_impact_on_track{1.0f}
{

}

void SnowData::resize(int w, int h)
{
    cells.resize(w, h);
    render_elevations.resize(w, h);
    render_normals.resize(w, h);
    render_colors.resize(w,h);
}

GridDEM SnowData::getSeparateData(int i)
{
    GridDEM out(cells.width(), cells.height());
    for(size_t id = 0; id<out.size(); ++id)
        out[id] = *((float*)&cells[id]+i);
    return out;
}

}
}
