#include <cuda.h>
#include <builtin_types.h>
#include <vector_functions.h>
#include <helper_math.h>

#include "../CUDA/CudaGridUtils.h"

#include "SnowData.h"

namespace expressive {
namespace landscape {

extern "C"
__global__ void SnowRenderFuncCUDA(const SnowCell* cells,  const SnowParams* params,
                                   float* elevation, float4* color,
                                   int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    float4 bedrock_color = make_float4(0.30f,   0.25f,   0.2f,   1.0f);
    float4 snow_color    = make_float4(1.0f, 1.0f, 1.0f, 1.0);

    elevation[id] = (cells[id].bedrock + cells[id].snow) / params->cell_size;
    float snow_transition = min(1.0f, cells[id].snow);

    color[id] = snow_color * snow_transition + bedrock_color * (1.0f - snow_transition);
#ifndef DETAILED_TRACKS
    float track_alpha = cells[id].ski_tracks;
    color[id] = color[id] * (1.0f - track_alpha) + make_float4(0.2f, 0.2f, 0.2f, 1.0f) * (track_alpha);
#endif

    float wvx = cells[id].wind_speed_x;
    float wvy = cells[id].wind_speed_y;
    float wvn = sqrt(wvx*wvx+wvy*wvy);

    float4 red = make_float4(1.00f,   0.0f,   0.0f,   1.0f);
    float4 blue    = make_float4(0.0f, 0.0f, 1.0f, 1.0);
    float4 wind_visu = cells[id].wind_visu * (wvn * red + (1.0-wvn)*blue);


    if(params->show_wind_cap)
    {
        float cap = cells[id].wind_h / params->cell_size;
        float4 wind_c = make_float4(0.2f, 0.2f, 0.7f, 1.0f);
        float w_t = clamp((cap-elevation[id]), 0.0f, 1.0f);
        color[id] = color[id]*(1.0f-w_t) + wind_c*w_t;
        elevation[id] = cap;
    }

    if(params->show_wind)
        color[id] = color[id] *0.1 + 0.9*wind_visu;

    if(params->show_unstable)
    {
        float value = clamp(cells[id].unstable_layer, 0.0f, 1.0f);
        float4 cc =  snow_transition * (value * red + (1.0-value)*blue);
        color[id] = color[id] *0.1 + 0.9*cc;
    }

    if(params->show_debug)
    {
        float value = clamp(logf(cells[id].debug), -1.0f, 1.0f);
        float4 color_hot = make_float4(1.0f, 0.0f, 0.0f, 1.0f);
        float4 color_cold = make_float4(0.0f, 0.0f, 1.0f, 1.0f);
        float4 color_freeze = make_float4(1.0f, 1.0f, 1.0f, 1.0f);
        float4 dbg_color;
        if(value < 0.0f)
        {
            dbg_color = -value * color_hot + (1.0f+value) * color_cold;
        }
        else
        {
            dbg_color = value * color_freeze + (1.0f-value) * color_cold;
        }
        color[id] = /*color[id] *0.1 + 0.9**/dbg_color;

    }


}

__global__ void computeNormalsCUDA(const float* elevation, float4* normal, int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    float xm = elevation[!(id%w)     ? id : id-1];
    float xp = elevation[!((id+1)%w) ? id : id+1];
    float ym = elevation[!(id/w)     ? id : id-w];
    float yp = elevation[id/w+1==h  ? id : id+w];
    float cur = elevation[id];

    float gx = xm-xp;
    float gy = ym-yp;
    float c = cur - 0.25f*(xm + xp + ym + yp);
    float l = sqrt(gx*gx + 4.0f + gy*gy);

    normal[id] = make_float4(gx / l, 2.0f / l, gy / l, c*4.0f);
}

void SnowRenderFunc(void* cells, void* params, void* elevation, void* color, void* normal, int w, int h)
{
    dim3 threads (32,32);
    dim3 blocks(w / threads.x, h / threads.y);
    SnowRenderFuncCUDA<<<blocks, threads>>>((SnowCell*)cells,
                                            (SnowParams*)params,
                                            (float*)elevation,
                                            (float4*)color,
                                            w,
                                            h);


    computeNormalsCUDA<<<blocks, threads>>>((float*)elevation,
                                            (float4*)normal,
                                            w,
                                            h);

}

}
}
