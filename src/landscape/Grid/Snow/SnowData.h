#ifndef SNOWDATA_H
#define SNOWDATA_H

#include "SnowRequired.h"
#include "../GridCommon.h"

#include "../CUDA/StochasticSimulationCUDA.h"

#define MAX_SKIERS_PER_CELL 8
//#define DETAILED_TRACKS
//#define DO_PROFILE

namespace expressive {
namespace landscape {

struct SNOW_API SnowCell
{
    SnowCell();
    float illumination;
    float bedrock;
    float snow; // = compacted + stable + unstable + powdery
    float powdery;

    float wind_speed_x;
    float wind_speed_y;
    float wind_pressure;
    float wind_visu;
    float wind_h;
    float wind_terrain;
    float wind_transport;

    float avalanche_trigger;
    float unstable_layer;
    float compacted;

    float moving;
    float flow[4];

    float ski_tracks;
    float nb_skiers;
    float skier_x[MAX_SKIERS_PER_CELL];
    float skier_y[MAX_SKIERS_PER_CELL];
    float skier_rot[MAX_SKIERS_PER_CELL];
#ifdef DETAILED_TRACKS
    float skier_phase[MAX_SKIERS_PER_CELL];
#endif
    float snow_mvmt;
    float dist_to_exit;

    float debug;
};

struct SNOW_API SnowParams
{
    SnowParams();

    // simulation parameters. Should not be changed by user.
    float cell_size;
    float altitude_base;

    float update_time;

    // global parameters
    float latitude;
    float compass;
    float month;
    float temperature_base;
    float temperature_offset;
    float temperature_illum;
    float gravity;
    float illum_factor;
    float illum_radius;
    float illum_sun;
    float illum_ao;


    // render
    float show_wind;
    float show_wind_cap;
    float show_unstable;
    float show_debug;

    // precipitation parameter
    float precip_max_temp;    // °C
    float precip_temp_offset; // m / °C
    float precip_max;
    float stability_min_slope;
    float unstable_factor;
    float moving_max_temp;
    float moving_temp_offset;
    float moving_min_slope;
    float moving_propotion;

    // melt parameters
    float melt_max_temp;
    float melt_temp_offset;
    float melt_unstable_temp;
    float melt_stable_temp;
    float melt_freeze_temp;
    float melt_hot_stability;
    float melt_medium_stability;
    float melt_freeze_stability;
    float melt_compaction_effect;
    float melt_compaction_pressure;

    // diffusion
    float diffusion_slope_factor;
    float diffusion_rest_angle;

    // wind
    float wind_compass;
    float wind_strength;
    float wind_altitude_offset;
    float wind_terrain_force;
    float wind_fall_offset;
    float wind_smooth_ground;
    float wind_snow_curve;  // impact of the curvature in the snow
    float wind_snow_terrain;
    float wind_snow_capacity; // transport capacity of winf field
    float wind_plates;

    //avalanches
    float av_rest_angle;
    float av_max_simulated;
    float av_dt;
    float viscosity;

    // skiers
    float skier_target_slope;
    float skier_target_amplitude;
    float skier_target_period;
    float skier_min_snow;
    float skier_spawn_rate;
    float skier_speed;
    float skier_lookahead;
    float skier_angle_step;
    float skier_avalanche_chance;
    float skier_snow_impact_on_track;
};

struct SNOW_API SnowData
{
    using Cell = SnowCell;
    using Params = SnowParams;

    SnowData () {}
    void resize (int w, int h);
    size_t size() {return cells.size();}
    GridDEM getSeparateData(int);


    GeoArray<SnowCell> cells;

    GridDEM    render_elevations;
    GridNormal render_normals;
    GridColor  render_colors;

    SnowParams params;
    std::vector<float> events;
};

// render function
void SnowRenderFunc(void* cells, void* params, void* elevation, void* color, void* normal, int w, int h);

// initial events
void SnowEventIllumination(StochasticSimulationCUDA::EventArg&);
void SnowEventWindProc(StochasticSimulationCUDA::EventArg&);

// events
void SnowEventPrecipStart(StochasticSimulationCUDA::EventArg&);
void SnowEventPrecipStop(StochasticSimulationCUDA::EventArg&);
void SnowEventPrecip(StochasticSimulationCUDA::EventArg&);
void SnowEventMelt(StochasticSimulationCUDA::EventArg&);
void SnowEventDiffusion(StochasticSimulationCUDA::EventArg&);
void SnowEventWind(StochasticSimulationCUDA::EventArg&);
void SnowEventAvPropag(StochasticSimulationCUDA::EventArg&);
void SnowEventAvFall(StochasticSimulationCUDA::EventArg&);
void SnowEventAvFallFluid(StochasticSimulationCUDA::EventArg&);
void SnowEventSpawnSkier(StochasticSimulationCUDA::EventArg&);
void SnowEventMoveSkier(StochasticSimulationCUDA::EventArg&);
void SnowEventPaths(StochasticSimulationCUDA::EventArg&);
void SnowEventHideTracks(StochasticSimulationCUDA::EventArg&);

// defined in RegisterSnow.cpp
extern int SnowEventPrecipStartID;
extern int SnowEventPrecipStopID;
extern int SnowEventPrecipID;
extern int SnowEventMeltID;
extern int SnowEventDiffusionID;
extern int SnowEventWindID;
extern int SnowEventAvPropagID;
extern int SnowEventAvFallFluidID;
extern int SnowEventSpawnSkierID;
extern int SnowEventMoveSkierID;
extern int SnowEventHideTracksID;

}
}


#endif // SNOWDATA_H
