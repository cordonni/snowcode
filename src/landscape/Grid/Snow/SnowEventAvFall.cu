#include <cuda.h>
#include <builtin_types.h>
#include <curand_kernel.h>
#include <math_constants.h>
#include <vector_functions.h>
#include <helper_math.h>

#include "../CUDA/CudaGridUtils.h"
#include "../CUDA/StochasticSimulationCUDA.h"

#include "SnowData.h"

#include "landscape/Utils/Profile.h"


namespace expressive {
namespace landscape {

#define EPSILON 1.0e-6f


__global__ void SnowEventAvPropagAvFallFlowCuda(SnowCell* __restrict__ cells,
                                                const SnowParams* __restrict__ params,
                                                float* __restrict__ temp,
                                                int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;


    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    // compute flow between cell i and cell i+1
    // nb are
    // 4 5 6
    // 7 8 0
    // 1 2 3


    bool edge_xp = !((id+1)%w);
    bool edge_yp = id/w+1==h;
    bool edge_xm = !(id%w);
    bool edge_ym = !(id/w);

    int nb[8];
    nb[0] = edge_xp ? id : id+1;
    nb[1] = edge_xm || edge_yp ? id : id-1+w;
    nb[2] = edge_yp  ? id : id+w;
    nb[3] = edge_xp || edge_yp  ? id : id+1+w;
    nb[4] = edge_xm ? id : id-1;
    nb[7] = edge_xm || edge_ym  ? id : id-w-1;
    nb[6] = edge_ym  ? id : id-w;
    nb[5] = edge_ym || edge_xp ? id : id+1-w;

    // the cell starts moving
    // if some neighboor have an incoming inflow
    bool moving = cells[id].moving > 0.5;

    for(int i = 0; i<4; ++i)
        moving = moving || (cells[nb[i+4]].flow[i] > EPSILON);

    float k = params->av_dt * params->gravity * params->cell_size* params->cell_size; // dt*g*C

    float friction = k * tanf(params->av_rest_angle*CUDART_PI_F/180.0f);

    float cur_h = cells[id].bedrock + cells[id].snow;

    //curve
    float elev[8];
    for(int i = 0; i<8; ++i)
        elev[i] = cells[nb[i]].bedrock + cells[nb[i]].snow;

    float curv = cur_h -0.25f*(elev[0] + elev[2] + elev[5]+elev[7]);
    curv*=params->av_max_simulated/params->cell_size;
    friction = max(0.0, friction - k*curv);

    float flow[4];
    for(int i = 0; i<4; ++i)
    {
        float dist = params->cell_size;
        if(i==1 || i==3)
            dist *= 1.41421356f;

        float fl = cells[id].flow[i];
        fl += k*(cur_h -elev[i] )/dist;

        float fr = fl > 0.0 ? -friction : friction;
        flow[i] = (abs(fr) > abs(fl)) ? 0.00f : fr + fl;

        float visc = -fl*params->viscosity*k;
        flow[i] = (abs(visc) >abs(flow[i])) ? 0.0f : flow[i] + visc;

        // if flow is inward, movement and flow will depend on the neighbor
        if(flow[i] <-EPSILON)
        {
            if(cells[nb[i]].moving)
                moving = true;
            else
                flow[i] = 0.0;
        }

    }
    // no flow if the cell is not moving
    for(int i = 0; i<4; ++i)
        temp[id*4+i] = moving ? flow[i] : 0.0f;

}

__global__ void SnowEventAvPropagAvFallFlowScaleCuda(SnowCell* __restrict__ cells,
                                                     const SnowParams* __restrict__ params,
                                                     const float* __restrict__ temp,
                                                     int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    bool edge_xm = !(id%w);
    bool edge_ym = !(id/w);
    bool edge_xp = !((id+1)%w);

    int nb[4]; // we look for flow in reverse neighbors
    nb[0] = edge_xm ? id : id-1;
    nb[3] = edge_xm || edge_ym  ? id : id-w-1;
    nb[2] = edge_ym  ? id : id-w;
    nb[1] = edge_ym || edge_xp ? id : id+1-w;

    float cur_f[4];
    float nb_f[4];

    float outflow = 0.0f;

    for(int i = 0; i<4; ++i)
    {
        cur_f[i] = temp[id*4+i];
        nb_f[i] = -temp[nb[i]*4+i]; // input flow from neighbor is output from this

        outflow += max(0.0,  cur_f[i]) + max(0.0,  nb_f[i]);
    }

    outflow *=params->av_dt;




    float available = params->cell_size*params->cell_size*cells[id].unstable_layer;

    float scale = 1.0f;

    if(outflow > available) //  >=0
    {
        scale =available / outflow;
    }

    for(int i = 0; i<4; ++i)
    {

        if(cur_f[i] >= 0.0)
            cells[id].flow[i] = scale * cur_f[i];
        if(nb_f[i] > 0.0) // strict is important here !
            cells[nb[i]].flow[i] = - scale * nb_f[i];
    }


}

__global__ void SnowEventAvPropagAvFallThickCuda(SnowCell* __restrict__ cells,  const SnowParams* __restrict__ params, int w, int h)
{
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x >= w || y >= h)
        return;

    int id = x + y*w;

    bool edge_xm = !(id%w);
    bool edge_ym = !(id/w);
    bool edge_xp = !((id+1)%w);

    int nb[4]; // we look for flow in reverse neighbors
    nb[0] = edge_xm ? id : id-1;
    nb[3] = edge_xm || edge_ym  ? id : id-w-1;
    nb[2] = edge_ym  ? id : id-w;
    nb[1] = edge_ym || edge_xp ? id : id+1-w;

    // compute inflow

    float inflow = 0.0;
    bool moving = false;
    for(int i = 0; i<4; ++i)
    {
        float df = -cells[id].flow[i] + cells[nb[i]].flow[i];
        inflow += df;
        moving = moving || (abs(df)>EPSILON);
    }

    float delta = params->av_dt/params->cell_size/params->cell_size * inflow;
    if(-delta > cells[id].unstable_layer)
    {
        delta = -cells[id].unstable_layer;
        moving = false;
    }

    cells[id].unstable_layer += delta;
    cells[id].snow           += delta;
    cells[id].snow = max(0.0f, cells[id].snow);
    cells[id].moving = (float)moving;
    cells[id].snow_mvmt += abs(delta);
}


void SnowEventAvFallFluid(StochasticSimulationCUDA::EventArg & args)
{
#ifdef DO_PROFILE
    cudaDeviceSynchronize();
    PROFILE_COUNT(SnowEventAvFall2P, "SnowEventAvFall2", 100);
#endif
    dim3 threads (32,32);
    dim3 blocks(ceil_div(args.w, threads.x), ceil_div(args.h, threads.y));

    SnowEventAvPropagAvFallFlowCuda<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                         (SnowParams*)args.cuda_params,
                                                         (float*)args.cuda_temp,
                                                         args.w,
                                                         args.h);

    SnowEventAvPropagAvFallFlowScaleCuda<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                              (SnowParams*)args.cuda_params,
                                                              (float*)args.cuda_temp,
                                                              args.w,
                                                              args.h);

    SnowEventAvPropagAvFallThickCuda<<<blocks, threads>>>((SnowCell*)args.cuda_cells,
                                                          (SnowParams*)args.cuda_params,
                                                          args.w,
                                                          args.h);


#ifdef DO_PROFILE
    cudaDeviceSynchronize();
#endif

}

}
}
