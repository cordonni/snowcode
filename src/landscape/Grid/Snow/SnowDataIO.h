#ifndef SNOWDATAIO_H
#define SNOWDATAIO_H

#include "SnowData.h"
#include "../SimulationData.h"

namespace expressive {
namespace landscape {

class SNOW_API SnowDataIO
{
public:

    using SnowSimuData = SimulationData<SnowData, SnowDataIO>;

    SnowDataIO() = delete;

    static void import(SnowSimuData*, const GridDEM& dem, float cell_size);
    static void importLayers(SnowSimuData*, std::string, float);
    static void defaultParameters(SnowSimuData*, const std::vector<float>&);
    static void exportMaps(SnowSimuData*, std::string dir, int frame);
    static void exportFrames(SnowSimuData*, std::string dir, int start, int end, int step);

    static int getRefreshParamId();

};
}
}

#endif // SNOWDATAIO_H
