#ifndef STOCHASTICSIMULATIONBASE_H
#define STOCHASTICSIMULATIONBASE_H

#include "landscape/LandscapeRequired.h"
#include "SimulationDataBase.h"


namespace expressive {
namespace landscape {


class LANDSCAPE_API StochasticSimulationBase
{
public:
    StochasticSimulationBase(){}
    virtual ~StochasticSimulationBase(){}

    virtual void initialize() = 0;

    virtual void setSimuData(std::shared_ptr<SimulationDataBase>) = 0;

    virtual size_t eventCount() = 0;

    virtual void start(int samples, bool onlyTerrain) = 0;
    virtual void pause() = 0;
    virtual void resume() = 0;
    virtual bool finished() = 0;

    virtual double getLastSimuLength() = 0;

    virtual bool isDataInit() = 0;
    virtual void initSimuData() = 0;

    virtual void renderUpdate(int frame) = 0; // frame = -1 for "simulated" ?

};

}
}


#endif // STOCHASTICSIMULATIONBASE_H
