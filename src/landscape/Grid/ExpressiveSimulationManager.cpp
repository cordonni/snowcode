#include "ExpressiveSimulationManager.h"
#include "landscape/Grid/Snow/SnowData.h"

#include "QGuiApplication"
#include "QClipboard"

#include "TerrainGrid.h"
#include "TerrainOperations.h"
#include "landscape/User/LandscapeCameraManipulator.h"

#include "ork/render/Texture2D.h"
#include "ork/render/CPUBuffer.h"

#include "pluginTools/gui/qml/QmlTypeRegisterer.h"
#include "core/geometry/VectorTools.h"

#include <QFileDialog>
#include <QInputDialog>

#include <unordered_set>

namespace expressive {
namespace landscape {

ExpressiveSimulationManager::ExpressiveSimulationManager(plugintools::GLExpressiveEngine *engine,
                                                         TerrainGrid* terrain , const GridDEM & dem, float cell_size,
                                                         std::shared_ptr<SimulationDataBase> sdb,
                                                         std::shared_ptr<StochasticSimulationBase> ssb)
    : QmlHandlerBase(engine),
      parameterUI_(0), // order is very important here, as paramterChanged is called in SimulationManager constructor
      layersUI_(0),
      SimulationManager(sdb, ssb),
      AbstractTask("ExpressiveSimulationManagerTask"),
      m_terrain(terrain),
      time_since_last_frame(0.0f), // ms
      dt_per_frame(40.0f), // ms
      prev_frame_(-100),
      prev_frame_max_(-100),
      prev_frame_compute_(-100),
      running (true),
      fps_count_frame_(0),
      fps_(0),
      time_since_fps_update_(0.0f),
      parameter_changed_(false),
      prev_layer_show_(-10),
      edit_0(30.0f), // warning: if changing this value here, chnage it also in PaintTile.qml
      edit_1(50.0f), // idem
      edit_amount(1.0f), // idem
      do_record(true),
      latency(1)
{
    chrono_start_ = std::chrono::high_resolution_clock::now();
    tex_render_height_ = engine->resource_manager()->loadResource("terrain_height").cast<ork::Texture2D>();
    tex_render_normal_ = engine->resource_manager()->loadResource("terrain_normal").cast<ork::Texture2D>();
    tex_render_color_  = engine->resource_manager()->loadResource("terrain_color").cast<ork::Texture2D>();
    tex_render_skitracks_  = engine->resource_manager()->loadResource("ski_tracks").cast<ork::Texture2D>();

    layersUI_ = std::make_shared<LayersUI>(this);
    parameterUI_ = std::make_shared<ParameterUI>(this);

    engine->qml_context()->setContextProperty("simulationManager", this);
    camera = static_cast<LandscapeCameraManipulator*> (engine->camera_manager()->manipulator().get());

    setTerrain(dem, cell_size);
}


ork::ptr<ork::Task> ExpressiveSimulationManager::getTask(ork::ptr<ork::Object> /*context*/)
{
    return new Impl(this);
}

ExpressiveSimulationManager::Impl::Impl(ExpressiveSimulationManager *owner) :
    ork::Task("ExpressiveSimulationManager", true, 0),
    owner_(owner)
{
}

bool ExpressiveSimulationManager::Impl::run()
{
    std::chrono::time_point<std::chrono::high_resolution_clock> chrono_now = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed_seconds = chrono_now-owner_->chrono_start_;
    owner_->chrono_start_ = chrono_now;

    if(owner_->do_record || owner_->latency)
    {
        if(owner_->updateSimu(owner_->do_record) && !owner_->do_record)
            --owner_->latency;
    }

    owner_->updateInterface(elapsed_seconds.count() * 1000.0);

    return true;
}

void ExpressiveSimulationManager::updateInterface(float dt)
{

    bool terrain_need_redraw = false;

    // update edit view
    if(layerEdited_ != -1)
    {
        Vector p = camera->getPointed();
        m_terrain->internal_node()->addValue(new ork::Value2f("uEditRadii",
                                                              Vector2f(std::min(edit_0, edit_1),
                                                                       std::max(edit_0, edit_1))));
        m_terrain->internal_node()->addValue(new ork::Value2f("uMousePoint", Vector2f(p[0], p[2])));
    }
    m_terrain->internal_node()->addValue(new ork::Value1f("uEditLayer", bool(layerEdited_+1)));

    if(camera->isLeftMouseClicked())
    {
        Vector p = camera->getPointed();
        performEdit(Vector2f(p[0], p[2]) ,edit_0, edit_1, edit_amount);
        terrain_need_redraw = true;
    }
    else
        releaseEdit();

    time_since_fps_update_+= dt;
    fps_count_frame_++;
    if(time_since_fps_update_ > 500.0f)
    {
        fps_ = int(double (fps_count_frame_) * 1000.0 / time_since_fps_update_);
        time_since_fps_update_ = 0.0f;
        fps_count_frame_ = 0;
        emit updateFPS();
    }

    if(frame_cur == frame_number-1)
    {
        if(running)
        {
            running = false;
            emit updateInterfaceRuning();
        }
    }

    if(running)
        time_since_last_frame += dt;
    else
        time_since_last_frame = 0.0f;

    if((!do_record ||frame_cur < frame_computed-1) && time_since_last_frame > dt_per_frame)
    {
        if(do_record)
            goToFrame(frame_cur+1);
        else
        {
            ++latency;
            terrain_need_redraw = true;
        }
        time_since_last_frame = 0.0f;
    }

    if(prev_frame_ != frame_cur)
    {
        prev_frame_ = frame_cur;
        if(frame_computed>0) // terrain is initialized
            terrain_need_redraw = true;
        emit updateInterfaceCurFrame();
    }
    if(prev_frame_max_ != frame_number)
    {
        prev_frame_max_ = frame_number;
        emit updateInterfaceMaxFrame();
    }
    if(prev_frame_compute_ != frame_computed)
    {
        if(frame_computed>=0 && (frame_computed==redraw_at_frame_compute_ || frame_cur >= frame_computed ))
            terrain_need_redraw = true;

        prev_frame_compute_ = frame_computed;
        if(frame_computed == redraw_at_frame_compute_)
            redraw_at_frame_compute_ = -1;
        emit updateInterfaceComputeFrame();
    }

    if(prev_layer_show_ != layerShown_)
    {
        prev_layer_show_ = layerShown_;
        if(frame_computed>0)
            terrain_need_redraw = true;
    }

    if(parameter_changed_)
    {
        parameter_changed_ = false;
        parameterUI_->requestAllParamUpdate();

        keyframes_.clear();
        for(auto it : locked_params)
            keyframes_.push_back(it.first);
        for(auto it : locked_events)
            keyframes_.push_back(it.first);
        for(int key : simuData_->deltaPaintKeys())
            keyframes_.push_back(key);

        emit framesChanged();

        terrain_need_redraw = true;
    }

    if(terrain_need_redraw)
        drawTerrain();

}

void ExpressiveSimulationManager::drawTerrain()
{
    GridDEM& render_elevations = simuData_->renderElevations(frame_cur);
    GridNormal& render_normals = simuData_->renderNormals(frame_cur);
    GridColor& render_colors = simuData_->renderColors(frame_cur);
    GridDEM& render_skitracks = simuData_->renderSkiTracks(frame_cur);

    static bool update_tracks = true;

    tex_render_height_->setImage((int)render_elevations.width(), (int)render_elevations.height(), ork::RED,  ork::FLOAT, ork::CPUBuffer(render_elevations.data()));
    tex_render_normal_->setImage((int)render_normals.width(), (int)render_normals.height(), ork::RGBA, ork::FLOAT, ork::CPUBuffer(render_normals.data()));
    if (update_tracks) {
        tex_render_skitracks_ ->setImage((int)render_skitracks.width(), (int)render_skitracks.height(), ork::RED, ork::FLOAT, ork::CPUBuffer(render_skitracks.data()));
#ifndef DETAILED_TRACKS
        update_tracks = false;
#endif
    }

    if(layerShown_>=0) // layer in alpha chanel
    {
        for(size_t y = 0; y<simuData_->cells_height(); ++y)
            for(size_t x = 0; x<simuData_->cells_width(); ++x)
                render_colors[x + simuData_->cells_width() * y][3] = simuData_->cell(frame_cur, (int)x, (int)y, layerShown_);
    }
    m_terrain->internal_node()->addValue(new ork::Value1f("uShowLayer", bool(layerShown_+1)));

    tex_render_color_ ->setImage((int)render_colors.width(), (int)render_colors.height(), ork::RGBA, ork::FLOAT, ork::CPUBuffer(render_colors.data()));


    std::tie(terrain_min_h, terrain_max_h) = render_elevations.minmax();
    terrain_min_h *= cell_size_;
    terrain_max_h *= cell_size_;
    m_terrain->updateBoundingBox(terrain_min_h, terrain_max_h);
    emit terranRangeChanged();
}

void ExpressiveSimulationManager::setTerrain(const GridDEM& dem, float cell_size)
{
    setSimulationInput(dem, cell_size);
    m_terrain->setSize((uint)dem.width(), (uint)dem.height(), cell_size);
    redraw_at_frame_compute_=1;
    emit resolutionChanged();
}

void ExpressiveSimulationManager::setLayers(std::string dirname, float cell_size)
{
    setSimulationInput(dirname, cell_size);
    m_terrain->setSize((int)simuData_->cells_width(), (int)simuData_->cells_width(), cell_size);
    redraw_at_frame_compute_=1;
    emit resolutionChanged();
}

void ExpressiveSimulationManager::parameterChanged()
{
    SimulationManager::parameterChanged();
    parameter_changed_ = true;
}

void ExpressiveSimulationManager::user_importTER()
{
    QString directory = "../../resources/heightfields/";
    QString loaded_file = QFileDialog::getOpenFileName(nullptr, tr("Import terrain"), directory, tr("Terragen (*.ter)"));

    if (loaded_file.isEmpty()) {
        return;
    }

    bool ok;
    double cell_size = QInputDialog::getDouble(nullptr, tr("Cell size"), tr("Cell size"), 10.0, 0.0001, 100000.0, 2, &ok);

    GridDEM dem = TerrainOperations::loadTER(loaded_file.toStdString());

    setTerrain(dem, cell_size);

    time_since_last_frame = 0.0f;

}

void ExpressiveSimulationManager::user_importLayers()
{
    QString directory = "../../resources/heightfields/";
    std::string saved_dir = QFileDialog::getExistingDirectory(nullptr, tr("Open Directory"),
                                                              directory,
                                                              QFileDialog::ShowDirsOnly).toStdString();

    bool ok;

    double cell_size = QInputDialog::getDouble(nullptr, tr("Cell size"), tr("Cell size"), 10.0, 0.0001, 100000.0, 2, &ok);
    setLayers(saved_dir, cell_size);

    time_since_last_frame = 0.0f;


}



void ExpressiveSimulationManager::user_exportTER()
{
    QString directory = "../../examples/snow";
    std::string saved_dir = QFileDialog::getExistingDirectory(nullptr, tr("Open Directory"),
                                                              directory,
                                                              QFileDialog::ShowDirsOnly).toStdString();

    simuData_->exportMaps(saved_dir, frame_cur);
}



void ExpressiveSimulationManager::user_exportAnim()
{
    QString directory = "../../examples/snow";
    std::string saved_dir = QFileDialog::getExistingDirectory(nullptr, tr("Open Directory"),
                                                              directory,
                                                              QFileDialog::ShowDirsOnly).toStdString();
    simuData_->exportFrames(saved_dir, 0, 200, 1);
}

void ExpressiveSimulationManager::user_save()
{
    QString directory = "../../examples/snow";
    QString saved_file = QFileDialog::getSaveFileName(nullptr, tr("Save animation"), directory, tr("Eroveg anim (*.eva)"));

    if (saved_file.isEmpty()) {
        return;
    }

    save(saved_file.toStdString());
}

void ExpressiveSimulationManager::user_load()
{
    QString directory = "../../examples/snow";
    QString loaded_file = QFileDialog::getOpenFileName(nullptr, tr("Load animation"), directory, tr("Eroveg anim (*.eva)"));

    if (loaded_file.isEmpty()) {
        return;
    }

    load(loaded_file.toStdString());
    prev_frame_ = -5;
    time_since_last_frame = 0.0f;
    running = false;
    m_terrain->setSize( (uint)simuData_->cells_width(), (uint)simuData_->cells_height(), cell_size_);
    emit updateInterfaceRuning();
    emit resolutionChanged();
}

void ExpressiveSimulationManager::user_togglePlay()
{
    running = !running;
    emit updateInterfaceRuning();
}

void ExpressiveSimulationManager::user_record(bool record)
{
    do_record = record;
    latency = 1;
    if(!record)
        user_restart();
}

void ExpressiveSimulationManager::user_restart()
{
    startSimulation();
    prev_frame_compute_ = -1;
    redraw_at_frame_compute_ = frame_computed+2;
}

void ExpressiveSimulationManager::user_set_frame(int f)
{
    goToFrame(f);
}

void ExpressiveSimulationManager::user_set_max_frame(int f)
{
    setMaxFrame(f);
}

void ExpressiveSimulationManager::user_set_speed(int s)
{
    dt_per_frame = std::max(1.0f, float(s));
    emit updateReviewSpeed();
}

void ExpressiveSimulationManager::user_set_edit_radius_0(float r0)
{
    edit_0 = r0;
}

void ExpressiveSimulationManager::user_set_edit_radius_1(float r1)
{
    edit_1 = r1;
}

void ExpressiveSimulationManager::user_set_edit_amount(float amount)
{
    edit_amount = amount;
}

ParameterUI* ExpressiveSimulationManager::getParameterUI()
{
    return parameterUI_.get();
}

LayersUI* ExpressiveSimulationManager::getLayersUI()
{
    return layersUI_.get();
}


void ExpressiveSimulationManager::tgd_cam()
{
    QClipboard *clipboard = QGuiApplication::clipboard();

    core::Camera* cam = camera->getCamera();

    Point pos = cam->GetPointInWorld();

    Matrix4 R = cam->GetLocalToWorld()*VectorTools::TranslationMatrix(-pos);

    // get yaw pitch roll
    float sy = sqrt(R[0][0] * R[0][0] +  R[1][0] * R[1][0] );

    bool singular = sy < 1e-6; // If

    float x,y,z;
    if (!singular)
    {
        x = atan2(R[2][1], R[2][2]);
        y = atan2(-R[2][0], sy);
        z = atan2(R[1][0], R[0][0]);
    }
    else
    {
        x = atan2(-R[1][2], R[1][1]);
        y = atan2(-R[2][0], sy);
        z = 0;
    }

    x*= 180.0f / (float)M_PI;
    y*= 180.0f / (float)M_PI;
    z*= 180.0f / (float)M_PI;

    if(abs(z)>90.0f)
    {
        x-=180.0f;
        y-=180.0f;
        z-=180.0f;

    }

    std::stringstream cam_string;
    cam_string << "\
                  <terragen_clip>\n\
                  <camera\n\
                  name = \"Exported Camera\"\n\
            gui_use_node_pos = \"1\"\n\
            gui_node_pos = \"640 0 0\"\n\
            gui_group = \"Cameras\"\n\
            show_camera_body_in_preview = \"1\"\n\
            position = \"" << pos.x << ' ' << pos.y << ' ' << -pos.z << "\"\n\
            rotation = \"" << x << ' ' << -y << ' ' << z << "\"\n\
            light_exposure = \"1\"\n\
            perspective = \"1\"\n\
            fisheye = \"0\"\n\
            use_horizontal_fov = \"1\"\n\
            horizontal_fov = \""<<cam->fov()<<"\"\n\
            use_vertical_fov = \"0\"\n\
            vertical_fov = \"42.10344887\"\n\
            focal_length_in_mm = \"31.17691454\"\n\
            film_aperture_in_mm = \"36 24\"\n\
            orthographic = \"0\"\n\
            use_ortho_width = \"1\"\n\
            ortho_width = \"1000\"\n\
            use_ortho_height = \"0\"\n\
            ortho_height = \"1000\"\n\
            spherical = \"0\"\n\
            motion_blur_position = \"1\"\n\
            motion_blur_length = \"0.5\"\n\
            shutter_offset = \"-0.25\"\n\
            subject_distance = \"100\"\n\
            aperture_diameter_in_mm = \"4.999999998\"\n\
            import_position = \"1\"\n\
            import_rotation = \"1\"\n\
            import_fov_general = \"1\"\n\
            import_Z_up = \"0\"\n\
            import_rotation_order = \"4\"\n\
            import_vertical_FOV = \"1\"\n\
            import_focal_length = \"0\"\n\
            import_focal_length_to_FOV = \"0\"\n\
            do_not_import_FOV = \"0\"\n\
            m_fbx_convert_to_metres = \"1\"\n\
            import_offset = \"0 0 0\"\n\
            import_scale = \"1\"\n\
            import_filename = \"\"\n\
            export_filename = \"\"\n\
            stereo = \"0\"\n\
            stereo_left = \"1\"\n\
            stereo_centre = \"0\"\n\
            stereo_right = \"0\"\n\
            stereo_mode = \"0\"\n\
            inter-axial_separation_in_mm = \"63.5\"\n\
            zero_parallax_distance = \"2.54\"\n\
            >\n\
            </camera>\n\
            </terragen_clip>";

            clipboard->setText(QString(cam_string.str().c_str()));


}


                              QML_TYPE_REGISTERER(ExpressiveSimulationManager, SimulationManager, "Expressive", 1, 0)


}
}
