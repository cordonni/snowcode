#ifndef PARAMETERUI_H
#define PARAMETERUI_H

#include "landscape/LandscapeRequired.h"
#include <QQuickItem>

#include <memory>

namespace expressive {
namespace landscape {

class ExpressiveSimulationManager;

class LANDSCAPE_API ParameterItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(float value READ value WRITE set NOTIFY valueChanged)
    Q_PROPERTY(bool locked READ locked WRITE lock NOTIFY lockChanged)

public:
    ParameterItem(){}
    ParameterItem(ExpressiveSimulationManager*, QString, int);

    void requestAllParamUpdate();

public slots:
    QString name() const;
    float value() const;
    bool locked() const;

    void set(float);
    void lock(bool);

signals:
    void valueChanged();
    void lockChanged();

private:
    ExpressiveSimulationManager* interface_;
    QString name_;
    int number_;
};

class LANDSCAPE_API ParameterEventItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(float timestep READ timestep WRITE settimestep NOTIFY timestepChanged)
    Q_PROPERTY(bool locked READ locked WRITE lock NOTIFY lockChanged)
    Q_PROPERTY(QQmlListProperty<expressive::landscape::ParameterItem> parameters READ getParameters CONSTANT)

public:
    ParameterEventItem(){}
    ParameterEventItem(ExpressiveSimulationManager*, QString, int);

    void requestAllParamUpdate();

public slots:
    QString name() const;
    float timestep() const;
    bool locked() const;
    QQmlListProperty<expressive::landscape::ParameterItem> getParameters();

    void settimestep(float);
    void lock(bool);

signals:
    void timestepChanged();
    void lockChanged();

private:
    ExpressiveSimulationManager* interface_;
    QString name_;
    int number_;
    std::vector<std::shared_ptr<ParameterItem>> parameters_;

    friend class ParameterUI;
};


class LANDSCAPE_API ParameterUI : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<expressive::landscape::ParameterItem> globals READ getGlobals CONSTANT)
    Q_PROPERTY(QQmlListProperty<expressive::landscape::ParameterEventItem> events READ getEvents CONSTANT)

public:
    ParameterUI(){}
    ParameterUI(ExpressiveSimulationManager*);

    int addEvent(QString, int);
    void addGlobal(QString, int);
    void addEventParam(QString, int, int);

    void requestAllParamUpdate();


public slots:
    QQmlListProperty<ParameterItem> getGlobals();
    QQmlListProperty<ParameterEventItem> getEvents();

private:
    std::vector<std::shared_ptr<ParameterItem>> globals_;
    std::vector<std::shared_ptr<ParameterEventItem>> events_;
    ExpressiveSimulationManager* interface_;

};


}
}

#endif // PARAMETERUI_H
