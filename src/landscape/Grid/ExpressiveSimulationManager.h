#ifndef EXPRESSIVESIMULATIONMANAGER_H
#define EXPRESSIVESIMULATIONMANAGER_H

#include "SimulationManager.h"
#include "ParameterUI.h"
#include "LayersUI.h"

#include "pluginTools/gui/qml/QmlHandlerBase.h"
#include "ork/scenegraph/AbstractTask.h"

#include <chrono>

namespace ork {
class Texture2D;
}


namespace expressive {
namespace landscape {
class TerrainGrid;
class LandscapeCameraManipulator;

class LANDSCAPE_API ExpressiveSimulationManager : public plugintools::QmlHandlerBase, public SimulationManager, public ork::AbstractTask
{
    Q_OBJECT
    Q_PROPERTY(expressive::landscape::ParameterUI* parameters READ getParameterUI CONSTANT)
    Q_PROPERTY(expressive::landscape::LayersUI* layers READ getLayersUI CONSTANT)

public:
    ExpressiveSimulationManager() : ork::AbstractTask("Empty ExpressiveSimulationManagerTask"){}
    ExpressiveSimulationManager(plugintools::GLExpressiveEngine *engine, TerrainGrid* terrain, const GridDEM&, float,
                                std::shared_ptr<SimulationDataBase>, std::shared_ptr<StochasticSimulationBase>);
    virtual ork::ptr<ork::Task> getTask(ork::ptr<ork::Object> context);

    virtual void drawTerrain();

    void setTerrain(const GridDEM&, float);
    void setLayers(std::string, float);

    Q_INVOKABLE int currentFrame() {return frame_cur;}
    Q_INVOKABLE int maxFrame(){return frame_number;}
    Q_INVOKABLE int computedFrames(){return frame_computed;}
    Q_INVOKABLE bool isRunning() {return running;}
    Q_INVOKABLE int reviewSpeed() {return (int)dt_per_frame;}
    Q_INVOKABLE int fps() {return (int)fps_;}
    Q_INVOKABLE int simu_time() {return (int)getLastSimuLength();}
    Q_INVOKABLE QVector<int> keyframes() {return keyframes_;}
    Q_INVOKABLE int terrain_width() {return (int)simuData_->cells_width();}
    Q_INVOKABLE int terrain_height() {return (int)simuData_->cells_height();}
    Q_INVOKABLE float terrain_min_alt() {return terrain_min_h;}
    Q_INVOKABLE float terrain_max_alt() {return terrain_max_h;}

    virtual void parameterChanged();

public slots:
    void user_importTER();
    void user_importLayers();
    void user_exportTER();
    void user_exportAnim();
    void user_save();
    void user_load();
    void user_togglePlay();
    void user_restart();
    void user_record(bool);
    void user_set_frame(int);
    void user_set_max_frame(int);
    void user_set_speed(int);
    void user_set_edit_radius_0(float);
    void user_set_edit_radius_1(float);
    void user_set_edit_amount(float);
    void tgd_cam();

    expressive::landscape::ParameterUI* getParameterUI();
    expressive::landscape::LayersUI* getLayersUI();

signals:
    void updateInterfaceCurFrame();
    void updateInterfaceMaxFrame();
    void updateInterfaceComputeFrame();
    void updateInterfaceRuning();
    void updateReviewSpeed();
    void updateFPS();
    void framesChanged();
    void resolutionChanged();
    void terranRangeChanged();


protected:
    class Impl : public ork::Task
    {
    public:
        Impl(ExpressiveSimulationManager *owner);

        virtual ~Impl() {}

        virtual bool run();

        ExpressiveSimulationManager* owner_;
    };

    void updateInterface(float dt);

    std::chrono::time_point<std::chrono::high_resolution_clock> chrono_start_;
    ork::ptr<ork::Texture2D>  tex_render_height_, tex_render_normal_, tex_render_color_, tex_render_skitracks_;

    TerrainGrid* m_terrain;

    float time_since_last_frame, dt_per_frame;

    int prev_frame_;
    int prev_frame_max_;
    int prev_frame_compute_;
    int redraw_at_frame_compute_;
    bool running;

    int fps_count_frame_, fps_;
    double time_since_fps_update_;

    std::shared_ptr<ParameterUI> parameterUI_;
    std::shared_ptr<LayersUI> layersUI_;

    QVector<int> keyframes_;

    bool parameter_changed_;

    int prev_layer_show_;

    float edit_0, edit_1, edit_amount;

    LandscapeCameraManipulator* camera;
    float terrain_min_h, terrain_max_h;

    bool do_record;
    int latency;

};

}
}

#endif // EXPRESSIVESIMULATIONMANAGER_H
