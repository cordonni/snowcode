#ifndef ACCELERATION_STRUCTURE_H
#define ACCELERATION_STRUCTURE_H

#include "landscape/LandscapeRequired.h"
#include <vector>
#include "../GridCommon.h"

namespace expressive {
namespace landscape {


/***********************
 * ACCELERATION SPHERE *
 ***********************/
class AccelerationSphere {
public:
    AccelerationSphere(Vector3 center, int m_radius);
    ~AccelerationSphere();

    bool intersects(const Vector3 & start, const Vector3 & direction) const;

    const Vector3 m_center;
    const int m_radius;
};

/*******************************
 * BASE ACCELERATION STRUCTURE *
 *******************************/
class LANDSCAPE_API Base
{
public:
    Base();
    virtual ~Base();
    std::vector<AccelerationSphere> get_intersecting_spheres(const Vector3 & start, const Vector3 & direction);
    void build(const GridDEM & terrain);

    inline void clear() { m_spheres.clear(); }

    const std::vector<AccelerationSphere> & getSpheres() const;

private:
    const int step_size;
    const int sphere_radius;
    int n_spheres_x;
    int n_spheres_z;

    std::vector<AccelerationSphere> m_spheres;
};

/***************************************
 * HIERARCHICAL ACCELERATION STRUCTURE *
 ***************************************/
class AccelerationTree {
public:
    AccelerationTree(AccelerationSphere node);
    ~AccelerationTree();

    void addChild(AccelerationTree child);
    bool isLeafNode();
    std::vector<AccelerationTree> & getChildren();
    void clear();
    const AccelerationSphere m_node;
private:
    std::vector<AccelerationTree> m_children;
};

class Hierarchical
{
public:
    Hierarchical();
    virtual ~Hierarchical();
    std::vector<AccelerationSphere> get_intersecting_spheres(const Vector3 & start, const Vector3 & direction);
    void build(const GridDEM & terrain);

    void clear();

    const std::vector<AccelerationSphere> & getSpheres() const;

private:
    const int min_sphere_radius;
    void split(AccelerationTree & tree, const GridDEM & terrain, int tree_depth = 1);
    AccelerationSphere get_sphere(const GridDEM & terrain, Vector2 & center, int base_radius);
    std::vector<AccelerationSphere> get_intersecting_spheres(AccelerationTree & parent, const Vector3 & start, const Vector3 & direction);

    AccelerationTree * m_acceleration_tree;
    std::vector<AccelerationSphere> m_spheres;
};

//----------------------------------------------------------------------

template <class T> class SphericalAccelerationStructure : public T
{
public:
    SphericalAccelerationStructure() : T() {}
    ~SphericalAccelerationStructure() {}
    void build(const GridDEM & terrain) { T::build(terrain); }
    void clear() { T::clear(); }
    std::vector<AccelerationSphere> get_intersecting_spheres(const Vector3 & start, const Vector3 & direction) {
        return T::get_intersecting_spheres(start,direction);
    }
    const std::vector<AccelerationSphere> & getSpheres() const { return T::getSpheres(); }

private:
};

}
}

#endif // ACCELERATION_STRUCTURE_H
