#include <algorithm>

#include "geom.h"
#include <iostream>
#include <math.h>
#include "core/geometry/VectorTools.h"
#include "sunexposure.h"

#include <limits>
#include <stack>


using namespace std;

namespace expressive {
namespace landscape {

#ifndef FLT_MAX
#define FLT_MAX std::numeric_limits<float>::max()
#endif

#define SUNLIGHT_BASE_POSITION Vector3(500.0f, 500.0f, -500.0f)
#define AXIS_TILT 0.408407f
const double SunExposure::_axis_tilt = AXIS_TILT;
const double SunExposure::_monthly_axis_tilt = (AXIS_TILT)/3.0;
const double SunExposure::_half_day_in_minutes = 720.0;
const double SunExposure::_quarter_day_in_minutes = SunExposure::_half_day_in_minutes/2.0;
const double SunExposure::_3_quarters_day_in_minutes = SunExposure::_half_day_in_minutes + SunExposure::_quarter_day_in_minutes;

SunExposure::SunExposure() :
    m_terrain_depth(0), m_terrain_width(0), m_month(1), m_time_of_day(0),
    m_latitude(0)
{
    setNorthOrientation(0, 0, -1);
    refresh_position();
}

SunExposure::~SunExposure()
{

}

void SunExposure::setLatitude(double latitude)
{
    m_latitude = latitude;
    refresh_position();
}

void SunExposure::setMonth(double month)
{
    m_month = month;
    refresh_position();
}

void SunExposure::setTime(double minutes)
{
    m_time_of_day = minutes;
    refresh_position();
}

double SunExposure::latitude() const
{
    return m_latitude;
}

double SunExposure::month() const
{
    return m_month;
}

double SunExposure::time() const
{
    return m_time_of_day;
}

void SunExposure::setTerrainDimensions(int width, int depth)
{
    m_terrain_width = width;
    m_terrain_depth = depth;

    m_center_x = m_terrain_width/2.0;
    m_center_y = 0;
    m_center_z = m_terrain_depth/2.0;

    refresh_position();
}

void SunExposure::refresh_position()
{
    // First calculate some orientations we need
    Vector3 east_orientation = VectorTools::Rotate(m_north_orientation, Vector3(0.0,1.0,0.0), (double)M_PI_2);
    Vector3 true_north_orientation = VectorTools::Rotate(m_north_orientation, east_orientation, latitude_to_angle(m_latitude));

    double sun_trajectory_radius(500000);
    double max_axis_tilt(get_axis_tilt_angle(m_month));
    double day_angle(minutes_to_angle(m_time_of_day));
    Vector3 cp_tn_and_east = true_north_orientation.cross(east_orientation).normalized();

    // First calculate the sun position at midday during the equinox
    Vector3 sun_position ( (sun_trajectory_radius) * cp_tn_and_east );

    // Now take into consideration axis tilt based on the month
    sun_position = VectorTools::Rotate( sun_position, east_orientation, -max_axis_tilt ); // PITCH

    // Now rotate around true north for the day
    sun_position = VectorTools::Rotate(sun_position, true_north_orientation, day_angle);

    // Now align to the center of the terrain (i.e the center of the terrain is at the latitude specified)
    sun_position += Vector3(m_center_x, m_center_y, m_center_z);

    // Done!
    setPosition(sun_position);
}

void SunExposure::setNorthOrientation(double north_x, double north_y, double north_z)
{
    m_north_orientation = Vector3(north_x, north_y, north_z).normalized();

    refresh_position();
}

void SunExposure::setNorthOrientation(Vector3 north)
{
    m_north_orientation = north;

    refresh_position();
}

// Static methods
// Returns in radians, the rotation relative to noon
double SunExposure::minutes_to_angle(double minutes)
{
    return (( _half_day_in_minutes - minutes) / _half_day_in_minutes) * M_PI;
}

double SunExposure::get_axis_tilt_angle(double month)
{
    return -_axis_tilt + (std::abs(6.0 - month) * _monthly_axis_tilt);
}

double SunExposure::latitude_to_angle(double latitude)
{
    return -latitude / (180.0/M_PI);
}

void SunExposure::split_tilt(double time_of_day, double & pitch, double & roll)
{
    double f_time_of_day ( time_of_day);
    // Pitch
    {
        if(time_of_day <= _half_day_in_minutes) // Before midday
            pitch = 1.0 - ((f_time_of_day / _half_day_in_minutes) * 2.0);
        else // After midday
            pitch = -1 + (((f_time_of_day-_half_day_in_minutes)/_half_day_in_minutes) * 2.0);
    }

    // Roll
    {
        if(time_of_day < (_quarter_day_in_minutes))
            roll = (f_time_of_day/_quarter_day_in_minutes) * 1.0;
        else if(f_time_of_day >= _quarter_day_in_minutes && f_time_of_day <= _3_quarters_day_in_minutes)
            roll = 1.0 - (((f_time_of_day-_quarter_day_in_minutes)/_half_day_in_minutes)*2.0);
        else // 6 pm -> midnight
            roll = -1.0 + ((f_time_of_day - _3_quarters_day_in_minutes) / _quarter_day_in_minutes);
    }
}

void SunExposure::setPosition(Vector3 position)
{
    m_position = position;
}

Vector3 SunExposure::position() const
{
    return m_position;
}

void SunExposure::setCenter(Vector3 center)
{
    m_center_x = center.x;
    m_center_y = center.y;
    m_center_z = center.z;

    refresh_position();
}

void SunExposure::rotateNorth(double compass)
{
    m_north_orientation = VectorTools::Rotate(m_north_orientation, Vector(0.0,1.0,0.0), compass / (180.0/M_PI));
}

Vector3 SunExposure::north() const
{
    return m_north_orientation;
}


bool SunExposure::traceRay(const GridDEM & terrain, const Vector3 & start, const Vector3 & direction, Vector3 & intersection_point, bool search_closest)
{
    // bounding sphere accel structure
    double scaler(.0), distance(.0);
    double tolerance (1.0);
    bool found(false);

    double min_distance(FLT_MAX);

    // Get all possible spheres
    std::vector<AccelerationSphere> intersecting_spheres(m_sphere_acceleration_structure.get_intersecting_spheres(start, direction));

    for(AccelerationSphere intersecting_sphere : intersecting_spheres)
    {
        int x_min = std::max(0, (int)(intersecting_sphere.m_center[0] - intersecting_sphere.m_radius));
        int x_max(std::min((int) terrain.width()-1, ((int) intersecting_sphere.m_center[0] + intersecting_sphere.m_radius)));

        int z_min(std::max(0, (int)(intersecting_sphere.m_center[2] - intersecting_sphere.m_radius)));
        int z_max(std::min((int) terrain.height()-1, ((int) intersecting_sphere.m_center[2] + intersecting_sphere.m_radius)));

        for(int point_in_sphere_x = x_min; point_in_sphere_x < x_max; point_in_sphere_x++)
        {
            for(int point_in_sphere_z = z_min; point_in_sphere_z < z_max; point_in_sphere_z++)
            {
                Vector3 point_in_sphere(point_in_sphere_x, terrain[terrain.xy_to_id(point_in_sphere_x,point_in_sphere_z)],
                        point_in_sphere_z);
                if(Geom::length(Geom::diff(start, point_in_sphere)) > 5.0) // Make sure the point is further than 5 unit distance from the start point
                {
                    Geom::rayPointDist(start, direction, point_in_sphere, scaler, distance);
                    if(distance < tolerance && distance < min_distance)
                    {
                        found = true;
                        min_distance = distance;
                        intersection_point = point_in_sphere;
                        if(!search_closest)
                        {
                            return true;
                        }
                    }
                }
            }
        }
    }
    return found;
}

bool * SunExposure::get_shade(const GridDEM & terrain, const GridNormal & normals, const Vector3 & sun_position)
{
    int terrain_width = (int)terrain.width();
    int terrain_depth = (int)terrain.height();

    bool * shade_data = NULL;

    // Generate the data
    shade_data = new bool[terrain_width * terrain_depth];

    // find minimum elevation over terrain
    double minhght = std::numeric_limits<double>::max();
    for(int i = 0; i < (int) terrain.size(); i++)
    {
        if(terrain[i] < minhght)
            minhght = terrain[i];
    }

    if(sun_position[1] <= minhght) // Optimization - the sun is set. There will be no light
    {
        memset(shade_data, true, terrain_depth*terrain_width);
    }
    else
    {
        Vector3 dummy_intersection_point;
        int i(0);
        for(int z = 0 ; z < terrain_depth; z++, i+=terrain_width)
        {
#pragma omp parallel for
            for(int x = 0; x < terrain_width; x++)
            {
                Vector3 point_on_terrain(Vector3(x, terrain[terrain.xy_to_id(x,z)], z));
                Vector3 sun_direction((sun_position - point_on_terrain).normalized());

                // First check if the normal is within sun angle
                int index(z*terrain_depth+x);

                // extract components
                // TO DO - what is the fourth component?
                Vector4f norm = normals[normals.xy_to_id(x,z)];
                Vector3 ter_normal = Vector3(norm[0], norm[1], norm[2]);

                if(ter_normal.dot(sun_direction) < 0.0 ||
                        traceRay(terrain, point_on_terrain, sun_direction, dummy_intersection_point, false)) // Not within direction
                {
                    shade_data[index] = true;
                }
                else
                {
                    shade_data[index] = false;
                }
            }
        }
    }
    return shade_data;
}

// TO DO
// test results

void SunExposure::calcDailySunExposure(const GridDEM & terrain, const GridNormal & normals, double latitude, double compass, GridDEM & exposure)
{
    int terrain_width = (int)terrain.width();
    int terrain_depth = (int)terrain.height();
    int n_elements(terrain_width*terrain_depth);

    setTerrainDimensions(terrain_width, terrain_depth);
    setLatitude(latitude);

    // use compass value to derive new north orientation
    Vector3 save_north = m_north_orientation;
    m_north_orientation = VectorTools::Rotate(m_north_orientation, Vector(0.0,1.0,0.0), compass);

    refresh_position();
    // double n_iterations = 24 * 12;
    int iteration = 0;

    m_sphere_acceleration_structure.build(terrain);

    cerr << "Calculating Daily Sun Exposure" << endl;

    double * illumination_data = new double[n_elements];
    memset(illumination_data, 0.0, n_elements*sizeof(double));

    for(int month = 1; month < 13; month++)
    {

        cerr << "month = " << month << endl;
        setMonth(month);

        // cerr << "hour = ";
        for(int hour = 0; hour < 24; hour++, iteration++)
        {
            cerr << hour << " ";
            setTime(hour * 60);
            bool * shade_data = get_shade(terrain, normals, position());

#pragma omp parallel for
            for(int z = 0 ; z < terrain_depth; z++)
            {
                for(int x = 0; x < terrain_width; x++)
                {
                    int index(z*terrain_width+x);
                    if(!shade_data[index])
                        illumination_data[index] += 1.0;
                    // illumination_data[index] += (shade_data[index] > 0 ? 0 : 1);
                }
            }
            delete shade_data;
        }
        // cerr << endl;
    }

    // average over months of the year
    for(int z = 0 ; z < terrain_depth; z++)
        for(int x = 0; x < terrain_width; x++)
        {
            int index(z*terrain_width+x);
            illumination_data[index] /= 12.0;
            exposure[exposure.xy_to_id(x, z)] = illumination_data[index]; // and copy across to exposure
        }

    m_north_orientation = save_north; // restore standard north
}

static float getOcclusion(std::stack<std::pair<float /*x*/, float /*alt*/>>& slopes, float fx, float alt)
{
    float cur_slope = -1.0;
    std::pair<float, float> ls;
    while(!slopes.empty())
    {
        std::pair<float, float> cs = slopes.top();
        float cmpslope = std::max(0.0f, (cs.second - alt) / (fx - cs.first));
        if(cmpslope < cur_slope)
            break;

        cur_slope = cmpslope;
        slopes.pop();
        ls = cs;
    }

    // restore the selected max
    slopes.push(ls);
    slopes.push({fx, alt});
    return cur_slope;
}


GridDEM SunExposure::getExposureAngles(const GridDEM & terrain, const Vector3 & sun_position)
{
    GridDEM angles(terrain.width(), terrain.height());
    GeoArray<int> parse_prev(terrain.width(), terrain.height(), -1);
    std::vector<int> border_cells;
    int max_t_size = (int)std::max(terrain.width(), terrain.height());

    Vector2 sun_dir = Vector2(sun_position[0]-m_center_x, sun_position[2]-m_center_z).normalized();

    // draw a line on the D4 of the terrain
    Vector2 l_start = (Vector2(m_center_x, m_center_z) - sun_dir * float(max_t_size));
    l_start[0] = std::max(std::min((double)terrain.width()-1.0f, std::floor(l_start[0])), 0.0)+0.5;
    l_start[1] = std::max(std::min((double)terrain.height()-1.0f, std::floor(l_start[1])), 0.0)+0.5;

    std::vector<Vector2i> line_dir;

    Vector2 cur_pos = l_start;

    Vector2 delta(sun_dir[0] / std::abs(sun_dir[1]), sun_dir[1] / std::abs(sun_dir[0]));
    if(std::abs(sun_dir[0])>std::abs(sun_dir[1]))
        delta[0] = (sun_dir[0]>0.0f)*2.0f-1.0f;
    else
        delta[1] = (sun_dir[1]>0.0f)*2.0f-1.0f;

    while(true)
    {
        Vector2 next = cur_pos;
        next += delta;

        if(!terrain.isInTerrain(std::floor(next[0]), std::floor(next[1])))
            break;

        line_dir.push_back(Vector2i(std::floor(next[0]) - std::floor(cur_pos[0]), std::floor(next[1])-std::floor(cur_pos[1])));

        cur_pos = next;
    }
    //std::cerr << "Line dir "<<line_dir.size() <<std::endl;;

    // fill a table of 'prev' cell id for parsing, along parallel lines
    Vector2i orthi = Vector2i::ZERO;
    if(std::abs(sun_dir[0])>std::abs(sun_dir[1]))
        orthi[1] = 1;
    else
        orthi[0] = 1;

    for (int i = -max_t_size; i<max_t_size; ++i)
    {
        Vector2i pos(std::floor(l_start[0]), std::floor(l_start[1]));
        pos += i * orthi;

        int l_id = parse_prev.isInTerrain(pos[0], pos[1])? (int)parse_prev.xy_to_id(pos[0], pos[1]) : -1;

        for(Vector2i dir : line_dir)
        {
            pos+=dir;

            if(parse_prev.isInTerrain(pos[0], pos[1]))
            {
                int cid = (int)parse_prev.xy_to_id(pos[0], pos[1]);
                parse_prev[cid] = l_id;
                l_id = cid;
            }
        }
        if(l_id!=-1)
            border_cells.push_back(l_id);
    }

    //std::cerr << "Border "<<border_cells.size() << std::endl;;

    // for each border cell, we pase the terrain in the direction of ray sun to compute exposure slope
    for (int id : border_cells)
    {
        std::stack<std::pair<float /*x*/, float /*alt*/>> slopes;
        slopes.push({0.0f, terrain[id]});
        angles[id] = 0.0;
        int x, y; std::tie(x, y) = terrain.id_to_xy(id);
        Vector2 s(double(x)+0.5, double (y)+0.5);
        id=parse_prev[id];


        while (id >=0)
        {
            std::tie(x, y) = terrain.id_to_xy(id);
            Vector2 c(double(x)+0.5, double (y)+0.5);

            angles[id] = getOcclusion(slopes, (c-s).norm(), terrain[id]);
            id = parse_prev[id];
        }
    }

    return angles;

}

void SunExposure::calcMonthlySunExposure(const GridDEM & terrain, const GridNormal & normals, double latitude, double compass, GridDEM* exposure)
{
    int terrain_width = (int)terrain.width();
    int terrain_depth = (int)terrain.height();

    setTerrainDimensions(terrain_width, terrain_depth);
    setLatitude(latitude);

    // use compass value to derive new north orientation
    Vector3 save_north = m_north_orientation;
    m_north_orientation = VectorTools::Rotate(m_north_orientation, Vector(0.0,1.0,0.0), compass);

    refresh_position();

    // clear exposure
    for(int i = 0; i<12; ++i)
        exposure[i].fill(0.0f);

    for(int month = 1; month < 13; month++)
    {
        setMonth(month);
        for(int hour = 0; hour < 24*6; hour++)
        {
            setTime(hour * 10);

            GridDEM terrainAngles = getExposureAngles(terrain, position());

            Vector3 sun_direction((position() - Vector3(m_center_x, m_center_y, m_center_z)).normalized());

            float sun_angle = sun_direction[1] / std::hypot(sun_direction[0], sun_direction[2]);

            float mn , mx;std::tie(mn, mx) = terrainAngles.minmax();

            for(uint id = 0; id<terrainAngles.size(); ++id)
                exposure[month-1][id] += float(sun_angle > terrainAngles[id]) *
                        (sun_direction.cast<float>().dot(normals[id].xyz()) > 0.0f ? 1.0f : 0.0f) / 24.0/6.0;
        }

    }
    m_north_orientation = save_north; // restore standard north

}

}
}
