#ifndef GEOM_H
#define GEOM_H

#include "landscape/LandscapeRequired.h"
#include <math.h>
#include <vector>
// #include <gl.h>
#include <QString>
#include <QDebug>
#include "core/VectorTraits.h"

namespace expressive {
namespace Geom{
    inline double squaredLength(const Vector3 & vector)
    {
        double len;

        len = vector[0] * vector[0]; //i
        len += vector[1] * vector[1]; //j
        len +=  vector[2] * vector[2]; //j

        return len;
    }

    inline double squaredLength(const Vector2 & vector)
    {
        double len;

        len = vector[0] * vector[0]; //i
        len += vector[1] * vector[1]; //j

        return len;
    }

    inline Vector3 affinecombine(const double & c1, Vector3& p1, const double c2, Vector3& p2)
    {
        double x, y, z;
        x = c1 * p1[0] + c2 * p2[0]; //x
        y = c1 * p1[1]+ c2 * p2[1]; //y
        z = c1 * p1[2] + c2 * p2[2]; //z

        return Vector3(x,y,z);
    }

    inline Vector2 affinecombine(const double & c1, Vector2& p1, const double c2, Vector2& p2)
    {
        float x, y;
        x = c1 * p1[0] + c2 * p2[0]; //x
        y = c1 * p1[1] + c2 * p2[1]; //y

        return Vector2(x,y);
    }

    // returns vector p2 to p1
    inline Vector3 diff(const Vector3 & p1, const Vector3 & p2)
    {
        float i, j, k;

        i = (p1[0] - p2[0]);
        j = (p1[1] - p2[1]);
        k = (p1[2] - p2[2]);

        return Vector3(i, j, k);
    }

    inline Vector3 scale(const Vector3 & v, const double & c)
    {
        double i, j, k;

        i = v[0] * c;
        j = v[1] * c;
        k = v[2] * c;

        return Vector3(i, j, k);
    }

    inline Vector2 scale(const Vector2 & v, const double & c)
    {
        float i, j;

        i = v[0] * c;
        j = v[1] * c;

        return Vector2(i, j);
    }

    inline double length(const Vector3 & v)
    {
        float length ((v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]));
        return std::sqrt(length);
    }

    inline void normalizeDegrees(double& p_angle)
    {
        if(p_angle < 0)
            p_angle += 360;

        while(p_angle > 360.f)
            p_angle -= 360;            
    }

    inline void normalizeRadians(double& p_angle)
    {
        double tmp(p_angle);

        while(p_angle < 0)
            p_angle += 2*M_PI;

        while(p_angle > 2*M_PI)
            p_angle -= 2*M_PI;

        if(tmp != p_angle)
        {

            qCritical() << "Unnormalized: " << tmp;
            qCritical() << "Normalized: " << p_angle;
        }
    }

    inline double toRadians(const double & degrees_angle)
    {
        return degrees_angle / (180/M_PI);
    }

    inline double toDegrees(const double & radians_angle)
    {
        return radians_angle * (180/M_PI);
    }

    QString toString(const Matrix4 & mat);
    void rayPointDist(const Vector3 & start, const Vector3 & direction, const Vector3 & query_point, double &scaler, double &dist);
    bool rayPlaneIntersection(const double & plane_height, const Vector3 & ray_start, const Vector3 & ray_direction, Vector3 & intersection);
}
}

#endif // GEOM_H
