#ifndef SUN_EXPOSURE_H
#define SUN_EXPOSURE_H

#include "landscape/LandscapeRequired.h"

#include "acceleration_structure.h"
#include "../TerrainOperations.h"

namespace expressive {
namespace landscape {

class LANDSCAPE_API SunExposure {

public:
    SunExposure();
    ~SunExposure();

    void calcDailySunExposure(const GridDEM & terrain, const GridNormal & normals, double latitiude, double compass, GridDEM & exposure);
    void calcMonthlySunExposure(const GridDEM & terrain, const GridNormal & normals, double latitude, double compass, GridDEM* exposure);

    void setNorthOrientation(double north_x, double north_y, double north_z);
    void setNorthOrientation(Vector3 north);

    void setMonth(double month);
    void setTime(double minutes);
    void setLatitude(double latitude);
    void setTerrainDimensions(int width, int depth);
    void setPosition(Vector3 position);
    void setCenter(Vector3 center);
    void rotateNorth(double compass);

    double month() const;
    double time() const;
    double latitude() const;
    Vector3 position() const;
    Vector3 north() const;

private:

    bool traceRay(const GridDEM & terrain, const Vector3 & start_point, const Vector3 & direction, Vector3 & intersection_point, bool search_closest = true);

    bool * get_shade(const GridDEM & terrain, const GridNormal & normals, const Vector3 & sun_position);
    GridDEM getExposureAngles(const GridDEM & terrain, const Vector3 & sun_position);


    void refresh_position();

    static void split_tilt(double time_of_day, double & pitch, double & roll);



    static double minutes_to_angle(double minutes);
    static double get_axis_tilt_angle(double month);
    static double latitude_to_angle(double latitude);



    double m_month;
    double m_time_of_day;
    double m_latitude;
    Vector3 m_position;

    int m_terrain_width, m_terrain_depth;
    double m_center_x, m_center_y, m_center_z;

    SphericalAccelerationStructure<Hierarchical> m_sphere_acceleration_structure;

    Vector3 m_north_orientation;

    static const double _axis_tilt;
    static const double _monthly_axis_tilt;
    static const double _half_day_in_minutes;
    static const double _quarter_day_in_minutes;
    static const double _3_quarters_day_in_minutes;
};

}
}

#endif // SUN_EXPOSURE_H
