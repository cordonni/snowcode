#ifndef GEOARRAY_H
#define GEOARRAY_H

#include <vector>
#include <tuple>
#include <algorithm>

using std::size_t;

template<class T>
class GeoArray
{
public:
    GeoArray();
    GeoArray(size_t, size_t);
    GeoArray(size_t, size_t, T);

    void resize(size_t, size_t);
    void resize(size_t, size_t, T);

    void fill(T);

    size_t height() const {return w_;}
    size_t width()  const {return h_;}
    size_t size()   const {return w_*h_;}

    std::pair<T, T> minmax() const;

    size_t xy_to_id(int x, int y) const {return x+y*w_;}
    size_t xy_to_id_clamp(int x, int y) const {return std::min((int)w_-1, std::max(0, x)) + std::min((int)h_-1, std::max(0, y)) * w_;}
    std::pair<int, int> id_to_xy(size_t id) const  {return {(int)(id%w_), (int)(id/w_)};}

    T& operator[](size_t id){return data_[id];}
    const T&  operator[](size_t id) const {return data_[id];}
    T& operator() (int x, int y) {return data_[xy_to_id(x, y)];}
    const T& operator() (int x, int y) const {return data_[xy_to_id(x, y)];}
    T* data() {return data_.data(); }
    const T* data() const {return data_.data(); }

    T fetchNearest(float, float);
    T fetchBilinear(float, float);
    T fetchBicubic(float, float);

    // fallowing 4 function only return valid ids
    std::vector<size_t> nb_d4(size_t);
    std::vector<size_t> nb_d8(size_t);

    int nb_d4_ptr(size_t, size_t*);
    int nb_d8_ptr(size_t, size_t*);

    // following 2 function return xm, xp, ym, yp for d4 replaced by cur if invalid
    // for d8: 00 10 20 01 21 02 20 22
    void nb_d4_or_cur(size_t, size_t[]);
    void nb_d8_or_cur(size_t, size_t[]);

    // return the 2 neighbors of direction in d4/d8 of id
    void nb_dir_d4(size_t, size_t, int[]);
    void nb_dir_d8(size_t, size_t, int[]);

    bool isNbDiag(size_t, size_t);

    std::tuple<T, T, T> gradSlope(size_t) const;

    // bottom is 0, top is height !
    bool isLeftBorder  (size_t id) const {return !(id%w_);}
    bool isRightBorder (size_t id) const {return !((id+1)%w_);}
    bool isBttmBorder  (size_t id) const {return !(id/w_);}
    bool isTopBorder   (size_t id) const {return id/w_+1==h_;}
    bool isInTerrain   (int x, int y) const {return x>=0 && x<w_ && y>=0 && y<h_;}

    // downsample and upsample by a factor of 2
    // the array is seen as a lattice:
    // downsampling will result in n/2+1 nodes
    // upsampling in n*2-1 nodes.
    // it is advisable to have a 2^n+1 sized array for multiple down sampling
    GeoArray downSample();
    GeoArray upSampleNearest();
    GeoArray upSampleBilinear();
    GeoArray upSampleBicubic();

    GeoArray upSampleNearestJitter(float);
    GeoArray upSampleBilinearJitter(float);
    GeoArray upSampleBicubicJitter(float);

private:
    size_t w_, h_;
    std::vector<T> data_;
};

#include "GeoArray.hpp"

#endif // GEOARRAY_H
