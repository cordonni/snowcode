#include "GeoArray.h"

#include <random>
#include <tuple>
#include <assert.h>

template<class T>
GeoArray<T>::GeoArray() :
    w_(0), h_(0)
{

}

template<class T>
GeoArray<T>::GeoArray(size_t w, size_t h)
{
    resize(w, h);
}

template<class T>
GeoArray<T>::GeoArray(size_t w, size_t h, T t)
{
    resize(w, h, t);
}

template<class T>
void GeoArray<T>::resize(size_t w, size_t h)
{
    w_ = w; h_ = h;
    data_.resize(w*h);
}

template<class T>
void GeoArray<T>::resize(size_t w, size_t h, T t)
{
    w_ = w; h_ = h;
    data_.resize(w*h, t);
}
template<class T>
void GeoArray<T>::fill(T t)
{
    for(T& val : data_)
        val = t;
}

template<class T>
std::pair<T, T> GeoArray<T>::minmax() const
{
    T mn = data_[0];
    T mx = data_[0];
    for(const T& val : data_)
    {
        mn = std::min(mn, val);
        mx = std::max(mx, val);
    }
    return {mn, mx};
}

// arrays itmems are points in a lattice
template<class T>
T GeoArray<T>::fetchNearest(float x, float y)
{
    int x_i = std::round(x * float(w_-1));
    int y_i = std::round(y * float(h_-1));

    return data_[xy_to_id_clamp(x_i, y_i)];
}

template<class T>
T GeoArray<T>::fetchBilinear(float x, float y)
{
    float x_f = x * float(w_-1);
    float y_f = y * float(h_-1);

    int x_i = int(x_f);
    int y_i = int(y_f);

    float dx = x_f - float(x_i);
    float dy = y_f - float(y_i);

    const T& v00 = data_[xy_to_id_clamp(x_i + 0, y_i + 0)];
    const T& v10 = data_[xy_to_id_clamp(x_i + 1, y_i + 0)];
    const T& v01 = data_[xy_to_id_clamp(x_i + 0, y_i + 1)];
    const T& v11 = data_[xy_to_id_clamp(x_i + 1, y_i + 1)];

    T interp_x0 = v00 * (1.0-dx) + v10 * dx;
    T interp_x1 = v01 * (1.0-dx) + v11 * dx;

    return interp_x0 * (1.0-dy) + interp_x1 * dy;
}

// http://blog.demofox.org/2015/08/15/resizing-images-with-bicubic-interpolation/
// t is a value that goes from 0 to 1 to interpolate in a C1 continuous way across uniformly sampled data points.
// when t is 0, this will return B.  When t is 1, this will return C.  Inbetween values will return an interpolation
// between B and C.  A and B are used to calculate slopes at the edges.
template<class T>
static float CubicHermite (const T& A, const T& B, const T& C, const T& D, float t)
{
    T a = -0.5f * A + 1.5f * B - 1.5f * C + 0.5f * D;
    T b = A - 2.5f*B + 2.0f*C - 0.5f * D;
    T c = -0.5f * A+ 0.5f * C;
    float d = B;

    return a*t*t*t + b*t*t + c*t + d;
}

template<class T>
T GeoArray<T>::fetchBicubic(float x, float y)
{
    float x_f = x * float(w_-1);
    float y_f = y * float(h_-1);

    int x_i = int(x_f);
    int y_i = int(y_f);

    float dx = x_f - float(x_i);
    float dy = y_f - float(y_i);

    const T& v00 = data_[xy_to_id_clamp(x_i - 1, y_i - 1)];
    const T& v10 = data_[xy_to_id_clamp(x_i + 0, y_i - 1)];
    const T& v20 = data_[xy_to_id_clamp(x_i + 1, y_i - 1)];
    const T& v30 = data_[xy_to_id_clamp(x_i + 2, y_i - 1)];

    const T& v01 = data_[xy_to_id_clamp(x_i - 1, y_i + 0)];
    const T& v11 = data_[xy_to_id_clamp(x_i + 0, y_i + 0)];
    const T& v21 = data_[xy_to_id_clamp(x_i + 1, y_i + 0)];
    const T& v31 = data_[xy_to_id_clamp(x_i + 2, y_i + 0)];

    const T& v02 = data_[xy_to_id_clamp(x_i - 1, y_i + 1)];
    const T& v12 = data_[xy_to_id_clamp(x_i + 0, y_i + 1)];
    const T& v22 = data_[xy_to_id_clamp(x_i + 1, y_i + 1)];
    const T& v32 = data_[xy_to_id_clamp(x_i + 2, y_i + 1)];

    const T& v03 = data_[xy_to_id_clamp(x_i - 1, y_i + 2)];
    const T& v13 = data_[xy_to_id_clamp(x_i + 0, y_i + 2)];
    const T& v23 = data_[xy_to_id_clamp(x_i + 1, y_i + 2)];
    const T& v33 = data_[xy_to_id_clamp(x_i + 2, y_i + 2)];

    T bc0 = CubicHermite(v00, v10, v20, v30, dx);
    T bc1 = CubicHermite(v01, v11, v21, v31, dx);
    T bc2 = CubicHermite(v02, v12, v22, v32, dx);
    T bc3 = CubicHermite(v03, v13, v23, v33, dx);

    return CubicHermite(bc0, bc1, bc2, bc3, dy);
}

template<class T>
std::vector<size_t> GeoArray<T>::nb_d4(size_t id)
{
    std::vector<size_t> r;
    if (id%w_) r.push_back(id-1);
    if ((id+1)%w_) r.push_back(id+1);
    if (id/w_)r.push_back(id-w_);
    if (id/w_!=h_-1)r.push_back(id+w_);
    return r;
}

template<class T>
std::vector<size_t> GeoArray<T>::nb_d8(size_t id)
{
    int x, y;
    std::tie(x, y) = id_to_xy(id);
    int x0 = std::max(0, x-1);
    int x1 = std::min((int)w_-1, x+1);
    int y0 = std::max(0, y-1);
    int y1 = std::min((int)h_-1, y+1);

    std::vector<size_t> r;
    for(int j = y0; j<=y1; ++j)
        for(int i = x0; i<=x1; ++i)
            if(i!=x || j!=y)
                r.push_back(xy_to_id(i, j));
    return r;
}

template<class T>
int GeoArray<T>::nb_d4_ptr(size_t id, size_t* ptr)
{
    int num = 0;
    if (id%w_) ptr[num++] = id-1;
    if ((id+1)%w_) ptr[num++] = id+1;
    if (id/w_)ptr[num++] = id-w_;
    if (id/w_!=h_-1)ptr[num++] = id+w_;

    return num;
}

template<class T>
int GeoArray<T>::nb_d8_ptr(size_t id, size_t* ptr)
{
    int num = 0;
    int x, y;
    std::tie(x, y) = id_to_xy(id);
    int x0 = std::max(0, x-1);
    int x1 = std::min((int)w_-1, x+1);
    int y0 = std::max(0, y-1);
    int y1 = std::min((int)h_-1, y+1);
    for(int j = y0; j<=y1; ++j)
        for(int i = x0; i<=x1; ++i)
            if(i!=x || j!=y)
                ptr[num++] = xy_to_id(i, j);
    return num;
}

template<class T>
void GeoArray<T>::nb_d4_or_cur(size_t id, size_t ret[])
{
    ret[0] = isLeftBorder(id)  ? id : id-1;
    ret[1] = isRightBorder(id) ? id : id+1;
    ret[2] = isBttmBorder(id)  ? id : id-w_;
    ret[3] = isTopBorder(id)   ? id : id+w_;
}

template<class T>
void GeoArray<T>::nb_d8_or_cur(size_t id, size_t ret [])
{
    int x, y;
    std::tie(x, y) = id_to_xy(id);
    for(int j = y-1; j<=y+1; ++j)
        for(int i = x-1; i<=x+1; ++i)
            if(i!=x || j!=y)
                *(ret++) = (i<0 || i>= w_ || j<0 || j>=h_) ? id : xy_to_id(i, j);
}

template<class T>
void GeoArray<T>::nb_dir_d4(size_t id, size_t dir, int ret[])
{
    if(dir == id-1 || dir == id+1)
    {
        ret[0] = (!(id/w_))?id : id-w_;
        ret[1] = (id/w_ == h_-1)? id : id+w_;
    }
    else if(dir == id-w_ || dir == id+w_)
    {
        ret[0] = (!(id%w_))?id : id-1;
        ret[1] = (id%w_ == w_-1)? id : id+1;
    }
    else
        assert(false && "dir not in d4");
}

template<class T>
void GeoArray<T>::nb_dir_d8(size_t id, size_t dir, int ret[])
{
    if(dir == id-1 || dir == id+1)
    {
        ret[0] = (!(id/w_))?(int)id : (int)dir-(int)w_;
        ret[1] = (id/w_ == h_-1)? (int)id : (int)dir+(int)w_;;
    }
    else if(dir ==  id-w_ || dir ==  id+w_)
    {
        ret[0] = (!(id%w_))? (int)id : (int)dir-1;
        ret[1] = (id%w_ == w_-1)? (int)id : (int)dir+1;
    }
    else
    {
        assert((dir == id-w_-1 || dir == id+w_-1 || dir == id-w_+1 || dir == id+w_+1) && "dir not in d8");
        ret[0] = (dir == id-w_-1 || dir == id+w_-1)? (int)id-1 : (int)id+1;
        ret[1] = (dir == id-w_-1 || dir == id-w_+1)? (int)id-(int)w_ : (int)id+(int)w_;
    }
}

template<class T>
bool GeoArray<T>::isNbDiag(size_t i0, size_t i1)
{
    int diff = std::abs((int)i0-(int)i1);
    return diff != 1 && diff != (int)w_;
}

template<class T>
std::tuple<T, T, T> GeoArray<T>::gradSlope(size_t id) const
{
    float xm = data_[isLeftBorder(id)  ? id : id-1];
    float xp = data_[isRightBorder(id) ? id : id+1];
    float ym = data_[isBttmBorder(id)  ? id : id-w_];
    float yp = data_[isTopBorder(id)   ? id : id+w_];
    float cur = data_[id];

    return std::make_tuple<T, T, T>(xm-xp, ym-yp, cur - T(0.25)*(xm + xp + ym + yp));
}

template<class T>
GeoArray<T> GeoArray<T>::downSample()
{
    GeoArray<T> result(w_/2+1, h_/2+1);
    for(size_t id : result.parser)
    {
        int x, y; std::tie(x, y) = result.id_to_xy(id);

        int g00 = data_[xy_to_id_clamp(2*x-1, 2*y-1)];
        int g10 = data_[xy_to_id_clamp(2*x  , 2*y-1)];
        int g20 = data_[xy_to_id_clamp(2*x+1, 2*y-1)];

        int g01 = data_[xy_to_id_clamp(2*x-1, 2*y  )];
        int g11 = data_[xy_to_id_clamp(2*x  , 2*y  )];
        int g21 = data_[xy_to_id_clamp(2*x+1, 2*y  )];

        int g02 = data_[xy_to_id_clamp(2*x-1, 2*y+1)];
        int g12 = data_[xy_to_id_clamp(2*x  , 2*y+1)];
        int g22 = data_[xy_to_id_clamp(2*x+1, 2*y+1)];

        result[id] = 0.0625 * (g00 + g20 + g02 + g22) + 0.125 * (g10 + g01 + g21 + g12) + 0.25 * g11;
    }

    return result;
}

template<class T>
GeoArray<T> GeoArray<T>::upSampleNearest()
{
    GeoArray<T> result(w_*2-1, h_*2-1);
    for(size_t id = 0; id<size(); ++id)
    {
        const T& cur = data_[id];
        int x, y; std::tie(x, y) = id_to_xy(id);
        result[result.xy_to_id_clamp(2*x,   2*y  )] = cur;
        result[result.xy_to_id_clamp(2*x+1, 2*y  )] = cur;
        result[result.xy_to_id_clamp(2*x,   2*y+1)] = cur;
        result[result.xy_to_id_clamp(2*x+1, 2*y+1)] = cur;
    }

    return result;
}

template<class T>
GeoArray<T> GeoArray<T>::upSampleBilinear()
{
    GeoArray<T> result(w_*2-1, h_*2-1);
    for(size_t id = 0; id<size(); ++id)
    {
        int x, y; std::tie(x, y) = id_to_xy(id);
        const T& cur00 = data_[id];
        const T& cur10 = data_[xy_to_id_clamp(x+1, y  )];
        const T& cur01 = data_[xy_to_id_clamp(x  , y+1)];
        const T& cur11 = data_[xy_to_id_clamp(x+1, y+1)];

        result[result.xy_to_id_clamp(2*x,   2*y  )] = cur00;
        result[result.xy_to_id_clamp(2*x+1, 2*y  )] = 0.5 * (cur00 + cur10);
        result[result.xy_to_id_clamp(2*x,   2*y+1)] = 0.5 * (cur00 + cur01);
        result[result.xy_to_id_clamp(2*x+1, 2*y+1)] = 0.25 * (cur00 + cur01 + cur10 + cur11);
    }

    return result;
}

template<class T>
GeoArray<T> GeoArray<T>::upSampleBicubic()
{
    GeoArray<T> result(w_*2-1, h_*2-1);
    for(size_t id = 0; id<size(); ++id)
    {
        int x, y; std::tie(x, y) = id_to_xy(id);
        const T& v00 = data_[xy_to_id_clamp(x - 1, y - 1)];
        const T& v10 = data_[xy_to_id_clamp(x + 0, y - 1)];
        const T& v20 = data_[xy_to_id_clamp(x + 1, y - 1)];
        const T& v30 = data_[xy_to_id_clamp(x + 2, y - 1)];

        const T& v01 = data_[xy_to_id_clamp(x - 1, y + 0)];
        const T& v11 = data_[xy_to_id_clamp(x + 0, y + 0)];
        const T& v21 = data_[xy_to_id_clamp(x + 1, y + 0)];
        const T& v31 = data_[xy_to_id_clamp(x + 2, y + 0)];

        const T& v02 = data_[xy_to_id_clamp(x - 1, y + 1)];
        const T& v12 = data_[xy_to_id_clamp(x + 0, y + 1)];
        const T& v22 = data_[xy_to_id_clamp(x + 1, y + 1)];
        const T& v32 = data_[xy_to_id_clamp(x + 2, y + 1)];

        const T& v03 = data_[xy_to_id_clamp(x - 1, y + 2)];
        const T& v13 = data_[xy_to_id_clamp(x + 0, y + 2)];
        const T& v23 = data_[xy_to_id_clamp(x + 1, y + 2)];
        const T& v33 = data_[xy_to_id_clamp(x + 2, y + 2)];

        result[result.xy_to_id_clamp(2*x  , 2*y  )] = v11;
        result[result.xy_to_id_clamp(2*x+1, 2*y  )] = -0.0625 *(v01+v31) + 0.5625*(v11+v21);
        result[result.xy_to_id_clamp(2*x  , 2*y+1)] = -0.0625 *(v10+v13) + 0.5625*(v11+v12);
        result[result.xy_to_id_clamp(2*x+1, 2*y+1)] =
                +0.00390625 * (v00+v03+v30+v33)
                -0.03515625 * (v01+v02+v10+v13+v20+v23+v31+v32)
                +0.31640625 * (v11+v12+v21+v22);
    }

    return result;
}

template<class T>
GeoArray<T> GeoArray<T>::upSampleNearestJitter(float amount)
{
    std::minstd_rand r(15623);
    std::uniform_real_distribution<float> distr(-amount, amount);

    GeoArray<T> result(w_*2-1, h_*2-1);
    for(size_t id = 0; id<size(); ++id)
    {
        int x, y; std::tie(x, y) = id_to_xy(id);
        float fx = (float(x) + distr(r)) / float(result.width()-1);
        float fy = (float(y) + distr(r)) / float(result.height()-1);

        result[id] = fetchNearest(fx,fy);
    }
    return result;
}

template<class T>
GeoArray<T> GeoArray<T>::upSampleBilinearJitter(float amount)
{
    std::minstd_rand r(15623);
    std::uniform_real_distribution<float> distr(-amount, amount);

    GeoArray<T> result(w_*2-1, h_*2-1);
    for(size_t id = 0; id<size(); ++id)
    {
        int x, y; std::tie(x, y) = id_to_xy(id);
        float fx = (float(x) + distr(r)) / float(result.width()-1);
        float fy = (float(y) + distr(r)) / float(result.height()-1);

        result[id] = fetchBilinear(fx,fy);
    }
    return result;
}


template<class T>
GeoArray<T> GeoArray<T>::upSampleBicubicJitter(float amount)
{
    std::minstd_rand r(15623);
    std::uniform_real_distribution<float> distr(-amount, amount);

    GeoArray<T> result(w_*2-1, h_*2-1);
    for(size_t id = 0; id<size(); ++id)
    {
        int x, y; std::tie(x, y) = id_to_xy(id);
        float fx = (float(x) + distr(r)) / float(result.width()-1);
        float fy = (float(y) + distr(r)) / float(result.height()-1);

        result[id] = fetchBicubic(fx,fy);
    }
    return result;
}

