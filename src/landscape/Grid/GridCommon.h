#ifndef GRIDCOMMON_H
#define GRIDCOMMON_H

#include "landscape/Grid/GeoArray.h"
#include "landscape/LandscapeRequired.h"

#include "core/VectorTraits.h"

namespace expressive {
namespace landscape {

using GridDEM    = GeoArray<float>;
using GridNormal = GeoArray<Vector4f> ;
using GridColor  = GeoArray<Vector4f> ;


}
}

#endif // GRIDCOMMON_H
