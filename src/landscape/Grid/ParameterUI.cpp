#include "ParameterUI.h"

#include "ExpressiveSimulationManager.h"

#include <pluginTools/gui/qml/QmlTypeRegisterer.h>



namespace expressive {
namespace landscape {

ParameterItem::ParameterItem(ExpressiveSimulationManager* esm, QString name, int pos):
    QQuickItem(nullptr),
    interface_(esm),
    name_(name),
    number_(pos)
{
}

QString ParameterItem::name() const
{
    return name_;
}

float ParameterItem::value() const
{
    return interface_->getParameter(number_);
}

bool ParameterItem::locked() const
{
    return interface_->getParameterLock(number_);
}

void ParameterItem::set(float v)
{
    interface_->setParameter(number_, v);
}

void ParameterItem::lock(bool l)
{
    interface_->lockParameter(number_, l);
}

ParameterEventItem::ParameterEventItem(ExpressiveSimulationManager* esm, QString name, int pos) :
    QQuickItem(nullptr),
    interface_(esm),
    name_(name),
    number_(pos)
{
}

QString ParameterEventItem::name() const
{
    return name_;
}
float ParameterEventItem::timestep() const
{
    return interface_->getEventTimeStep(number_);
}
bool ParameterEventItem::locked() const
{
    return interface_->isEventLocked(number_);
}
QQmlListProperty<ParameterItem> ParameterEventItem::getParameters()
{
    return QQmlListProperty<ParameterItem>(this, &parameters_,
                                           plugintools::qmlContainerCount<std::vector<std::shared_ptr<ParameterItem>>>,
                                           plugintools::qmlSharedPtrContainerAt<std::vector<std::shared_ptr<ParameterItem>>>);
}

void ParameterEventItem::settimestep(float f)
{
    interface_->setEventTimeStep(number_, f);
}

void ParameterEventItem::lock(bool l)
{
    interface_->setEventLocked(number_, l);
}

ParameterUI::ParameterUI(ExpressiveSimulationManager* esm):
    QQuickItem(nullptr),
    interface_(esm)
{

}

int ParameterUI::addEvent(QString name, int p)
{
    events_.push_back(std::make_shared<ParameterEventItem>(interface_, name, p));
    return (int)(events_.size()-1);
}

void ParameterUI::addGlobal(QString name, int p)
{
    globals_.push_back(std::make_shared<ParameterItem>(interface_, name, p));
}

void ParameterUI::addEventParam(QString name, int evid, int p)
{
    assert(evid < (int)events_.size());
    events_[evid]->parameters_.push_back(std::make_shared<ParameterItem>(interface_, name, p));
}

QQmlListProperty<ParameterItem> ParameterUI::getGlobals()
{
    return QQmlListProperty<ParameterItem>(this, &globals_,
                                           plugintools::qmlContainerCount<std::vector<std::shared_ptr<ParameterItem>>>,
                                           plugintools::qmlSharedPtrContainerAt<std::vector<std::shared_ptr<ParameterItem>>>);

}

QQmlListProperty<ParameterEventItem> ParameterUI::getEvents()
{
    return QQmlListProperty<ParameterEventItem>(this, &events_,
                                           plugintools::qmlContainerCount<std::vector<std::shared_ptr<ParameterEventItem>>>,
                                           plugintools::qmlSharedPtrContainerAt<std::vector<std::shared_ptr<ParameterEventItem>>>);


}

void ParameterItem::requestAllParamUpdate()
{
    emit valueChanged();
    emit lockChanged();
}

void ParameterEventItem::requestAllParamUpdate()
{
    emit timestepChanged();
    emit lockChanged();
    for (auto it : parameters_)
        it->requestAllParamUpdate();
}

void ParameterUI::requestAllParamUpdate()
{
    for (auto it : globals_)
        it->requestAllParamUpdate();
    for (auto it : events_)
        it->requestAllParamUpdate();
}

void ParameterUIRegisterer()
{
    qmlRegisterType<ParameterItem>("Expressive", 1, 0, "ParameterItem");
    qmlRegisterType<ParameterEventItem>("Expressive", 1, 0, "ParameterEventItem");
    qmlRegisterType<ParameterUI>("Expressive", 1, 0, "ParameterUI");
}

QML_TYPE_REGISTERER_CALLBACK(ParameterUIRegisterer)


}
}
