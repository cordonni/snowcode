#ifndef TERRAINOPERATIONS_H
#define TERRAINOPERATIONS_H

#include "GridCommon.h"

namespace expressive {
namespace landscape {

class LANDSCAPE_API TerrainOperations
{
public:
    static GridDEM loadTER(std::string);
    static void    saveTER(std::string, const GridDEM&, float=30.0);

    static void saveImg(std::string, const GridDEM&, float clamp_min=0.0f, float clamp_max=1.0f);
    static void saveTIFF16(std::string, const GridDEM&, float clamp_min=0.0f, float clamp_max=1.0f);

    static void computeNormals(const GridDEM&, GridNormal&);

    /**
     * @brief computeExposure Calculate the average sunlight exposure per grid cell over the course of a year
     * @param latitude  latitude in degrees (negative latitudes represent locations south of the equator)
     * @param compass   rotation of the terrain from its initial orientation, clockwise in radians
     * @return floating point grid, where each element holds the average daily hourse of sunlight
     */
    static void computeExposure(const GridDEM&, float latitude, float compass, GridDEM *exposure);

private:
    TerrainOperations() = delete;
};

}
}

#endif // TERRAINOPERATIONS_H
