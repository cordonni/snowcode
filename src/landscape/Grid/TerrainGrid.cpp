#include "TerrainGrid.h"

#include "core/geometry/MeshTools.h"
#include "core/geometry/BasicTriMesh.h"

namespace expressive {
namespace landscape {

void TerrainGrid::setSize(uint dem_x, uint dem_y, float cell_size)
{
    bbox.Enlarge(Point::Zero(), float(std::max(dem_x, dem_y)) * cell_size);

    internal_node()->addValue(new ork::Value2f("uTerrainSize", Vector2f(float(dem_x)*cell_size, float(dem_y)*cell_size)));
    internal_node()->addValue(new ork::Value1f("uVerticalScale", cell_size));
}


core::AABBox TerrainGrid::bounding_box() const
{
    return bbox;
}

void TerrainGrid::updateBoundingBox(float mn, float mx)
{
    bbox.ymax() = mx;
    bbox.ymin() = mn;
    internal_node()->setLocalBounds(ork::box3d(bbox.min(), bbox.max()));
}

void TerrainGrid::initMesh(uint resol)
{
    ork::ptr<ork::Mesh<Vector2f, uint>> mesh = new ork::Mesh<Vector2f, uint>(ork::PATCHES, ork::GPU_STATIC, resol*resol, (resol-1)*(resol-1)*6);
    mesh->addAttributeType(0, 2, ork::A32F, false);
    mesh->setPatchVertices(4);

    for (uint y = 0; y<resol; ++y)
        for(uint x = 0; x<resol; ++x)
            mesh->addVertex(Vector2f(float(x) / float(resol-1), float(y) / float (resol-1)));

    for (uint y = 0; y<resol-1; ++y)
        for(uint x = 0; x<resol-1; ++x)
        {
            mesh->addIndice(x+resol*y);
            mesh->addIndice(x+resol*(y+1));
            mesh->addIndice(x+1+resol*(y+1));
            mesh->addIndice(x+1+resol*y);
        }

    internal_node()->addMesh("grid", mesh->getBuffers());
}


}
}
