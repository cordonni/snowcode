#include "StochasticSimulationCUDA.h"
#include "landscape/Grid/Snow/SnowData.h"

#include <cuda.h>
#include <builtin_types.h>

#include <QDebug>
#include <iostream>

namespace expressive {
namespace landscape {

std::vector<std::pair<StochasticSimulationCUDA::EventFuncPtr, float>> StochasticSimulationCUDA::events;
std::vector<StochasticSimulationCUDA::InitialEventStatus> StochasticSimulationCUDA::intial_events;
const float StochasticSimulationCUDA::RegisterInitialEvent::uninit_param_ = -53696512.03256963f;


StochasticSimulationCUDA::RenderFuncPtr StochasticSimulationCUDA::render_func = nullptr;

StochasticSimulationCUDA::StochasticSimulationCUDA() :
    simulation_time_(0.0),
    simu_finished(true),
    simu_request_close(false),
    render_frame(-1),
    temporary_buffers(0)
{

    fast_rnd_ = std::ranlux24_base(std::random_device()());

    simu_thread = std::thread(&StochasticSimulationCUDA::simuThreadFunction, this);

}

StochasticSimulationCUDA::~StochasticSimulationCUDA()
{
    simu_request_close = true;
    simu_thread.join();
}

void StochasticSimulationCUDA::initialize()
{
    for(InitialEventStatus& status : intial_events )
        for(auto& dep : status.second)
            dep.second = RegisterInitialEvent::uninit_param_;
}

void StochasticSimulationCUDA::simuThreadFunction()
{
    bool tracks_loaded = false;
    int deviceCount = 0;
    int cudaDevice = 0;
    char cudaDeviceName [100];

    cuInit(0);
    cuDeviceGetCount(&deviceCount);
    cuDeviceGet(&cudaDevice, 0);
    cuDeviceGetName(cudaDeviceName, 100, cudaDevice);
    cuda_mem_skitracks.checkAlloc(8192 * 8192 * sizeof(float));

    std::cout << "Number of devices: " << deviceCount << std::endl;
    std::cout << "Device name:" << cudaDeviceName << std::endl;

    std::vector<int> init_seeds(16);
    for(int& s : init_seeds)
        s = std::uniform_int_distribution<int>(0, std::numeric_limits<int>::max())(fast_rnd_);

    EventArg event_args;


    for(;;)
    {
        // wait for start
        while(simu_finished)
        {
            if(simu_request_close)
                break;
        }

        if(simu_request_close)
            break;

        if(render_frame != -1 && only_terrain_)
        {
            simuRenderUpdate(render_frame);
            simu_finished = true;
            continue;
        }

        std::chrono::time_point<std::chrono::high_resolution_clock> chrono_start = std::chrono::high_resolution_clock::now();

        // upload computed content to cuda memory
        if (!tracks_loaded) {
            tracks_loaded = true;
            cuda_mem_skitracks.hostToDevice(simu_data->renderSkiTracksSimulated().data());
        }
        cuda_mem_cells.checkAlloc(simu_data->cells_width() * simu_data->cells_height() * simu_data->sizeof_cell());
        cuda_mem_params.checkAlloc(simu_data->sizeof_params());

        cuda_mem_cells.hostToDevice(simu_data->getSimulatedCellsPtr());
        cuda_mem_params.hostToDevice(simu_data->getSimulatedParamsPtr());

        cuda_mem_temp.checkAlloc(simu_data->cells_width() * simu_data->cells_height() * temporary_buffers * sizeof(float));

        cuda_random.checkInit(init_seeds, (int)(simu_data->cells_width() * simu_data->cells_height()));

        event_args.cuda_cells = cuda_mem_cells.get();
        event_args.cuda_tracks = cuda_mem_skitracks.get();
        event_args.cuda_params = cuda_mem_params.get();
        event_args.cuda_temp = cuda_mem_temp.get();
        event_args.cpu_params = simu_data->getSimulatedParamsPtr();
        event_args.cuda_rnd_state = cuda_random.getState();
        event_args.w = (int)simu_data->cells_width();
        event_args.h = (int)simu_data->cells_height();
        event_args.rnd = &fast_rnd_;

        event_args.event_freq.clear();
        for (int i = 0; i<(int)simu_data->getEventsCount(); ++i)
            event_args.event_freq.push_back(simu_data->getSimulatedEvent(i));

        // check for initial event
        handleInitialEvents(event_args);

        if(!only_terrain_)
            handleEvents(event_args);

        simuRenderUpdate(render_frame);


        // load back in computed memory
        cuda_mem_cells.deviceToHost(simu_data->getSimulatedCellsPtr());
#ifdef DETAILED_TRACKS
        cuda_mem_skitracks.deviceToHost(simu_data->renderSkiTracksSimulated().data());
#endif

        std::chrono::time_point<std::chrono::high_resolution_clock> chrono_stop = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed_seconds = chrono_stop-chrono_start;
        simulation_time_ = elapsed_seconds.count() * 1000.0;
        simu_finished = true;

    }

}

void StochasticSimulationCUDA::handleInitialEvents(EventArg& event_args)
{
    // check if a dependance is broken
    for(InitialEventStatus& status : intial_events )
    {
        std::vector<std::pair<int, float>>& depend = status.second;
        bool needed = false;
        for(auto& dep : depend)
        {
            if(simu_data->getSimulatedParam(dep.first) != dep.second)
            {
                dep.second = simu_data->getSimulatedParam(dep.first);
                needed = true;
            }
        }

        // if needed, lauch the event
        if(needed)
            status.first(event_args);

    }
}

void StochasticSimulationCUDA::handleEvents(EventArg &event_args)
{
    // recomputed event probability
    std::vector<double> event_proba(simu_data->getEventsCount());
    float prev_time = simu_data->getTotalTime();

    for(;;)
    {

        double proba_sum = 0.0f;
        for(size_t i = 0; i< event_proba.size(); ++i)
        {
            double ev_time = (double)(simu_data->getSimulatedEvent((int)i));
            if(!ev_time) ev_time = 0.01;
            event_proba[i] = 1.0 / ev_time;
            proba_sum += event_proba[i];

            event_distrib = std::discrete_distribution<>(event_proba.begin(), event_proba.end());
        }


        float dt = 1.0f / (float)proba_sum;

        for(;;) {

            // get event number
            int ev_id = event_distrib(fast_rnd_);

            // launch event
            events[ev_id].first (event_args);

            bool change = false;
            for(size_t i = 0; i< event_proba.size(); ++i)
                if(event_args.event_freq[i] != simu_data->getSimulatedEvent((int)i))
                {
                    simu_data->setSimulatedEvent((int)i, event_args.event_freq[i]);
                    change = true;
                }

            if (simu_data->incrTotalTime(dt) >= prev_time + simu_data->get_refresh_period())
                return;

            if (change)
                break;
        }
    }
}



void StochasticSimulationCUDA::start(int /*samples*/, bool onlyTerrain)
{
    assert(simu_finished);
    only_terrain_ = onlyTerrain;
    simu_finished = false;
}

void StochasticSimulationCUDA::pause()
{
    while(!simu_finished);
}

void StochasticSimulationCUDA::simuRenderUpdate(int frame)
{
    if(frame != -1)
    {
        cuda_mem_cells.checkAlloc(simu_data->cells_width() * simu_data->cells_height() * simu_data->sizeof_cell());
        cuda_mem_params.checkAlloc(simu_data->sizeof_params());

        cuda_mem_cells.hostToDevice(simu_data->getCellsPtr(frame));
        cuda_mem_params.hostToDevice(simu_data->getParamsPtr(frame));
    }

    cuda_mem_elev.checkAlloc(simu_data->cells_width() * simu_data->cells_height() * sizeof(float));
    cuda_mem_color.checkAlloc(simu_data->cells_width() * simu_data->cells_height() * sizeof(Vector4f));
    cuda_mem_normal.checkAlloc(simu_data->cells_width() * simu_data->cells_height() * sizeof(Vector4f));

    // launch update kernel
    assert(render_func);
    render_func(cuda_mem_cells.get(), cuda_mem_params.get(),
                cuda_mem_elev.get(), cuda_mem_color.get(), cuda_mem_normal.get(),
                (int)simu_data->cells_width(), (int)simu_data->cells_height());

    if(frame != -1)
    {
        cuda_mem_elev.deviceToHost(simu_data->renderElevations(frame).data());
        cuda_mem_color.deviceToHost(simu_data->renderColors(frame).data());
        cuda_mem_normal.deviceToHost(simu_data->renderNormals(frame).data());
    }
    else
    {
        cuda_mem_elev.deviceToHost(simu_data->renderElevationsSimulated().data());
        cuda_mem_color.deviceToHost(simu_data->renderColorsSimulated().data());
        cuda_mem_normal.deviceToHost(simu_data->renderNormalsSimulated().data());
    }

}

void StochasticSimulationCUDA::renderUpdate(int frame)
{
    // wait for finished
    while(!finished());
    render_frame = frame;
    start(0, true);
    while(!finished());
    render_frame = -1;
}


}
}
