#include "CudaRand.h"
#include "CudaGridUtils.h"

#include <cuda.h>
#include <builtin_types.h>

#include <curand_kernel.h>
#include <assert.h>
#include <algorithm>

namespace expressive {
namespace landscape {

extern "C"
__global__ void initRandCUDA(curandState *state, int seed, int offset, int size, int offset_id)
{
    int id = blockDim.x * blockIdx.x + threadIdx.x;
    if(id+offset_id < size)
        curand_init(seed, id, offset, &state[id+offset_id]);
}

void CudaRand::checkInit(int seed, int size, int offset)
{
    if(!state.checkAlloc(size * sizeof(curandState)))
    {

        int thread_num = size < 1024 ? size : 1024;
        int block_num = size/thread_num + std::min(1, size%thread_num);
        assert(thread_num*block_num >= size);

        initRandCUDA<<<block_num, thread_num>>>(
                                                  (curandState *)state.get(),
                                                  seed,
                                                  offset,
                                                  size,
                                                  0);

        cudaAssertKernel();
    }
}

void CudaRand::checkInit(std::vector<int> seeds, int size)
{
    if(!state.checkAlloc(size * sizeof(curandState)))
    {
        int thread_num = size < 1024 ? ceil_div(size, (int)seeds.size()) : 1024;
        int block_num = ceil_div(size, thread_num * (int)seeds.size());

        for(int i = 0; i< seeds.size(); ++i)
            initRandCUDA<<<block_num, thread_num>>>(
                                                      (curandState *)state.get(),
                                                      seeds[i],
                                                      0,
                                                      size,
                                                      block_num*thread_num*i);

    }
}



}
}
