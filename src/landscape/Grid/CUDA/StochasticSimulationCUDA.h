#ifndef STOCHASTICSIMULATIONCUDA_H
#define STOCHASTICSIMULATIONCUDA_H

#include "landscape/Grid/Snow/SnowRequired.h"
#include "../StochasticSimulationBase.h"

#include "CudaMemory.h"
#include "CudaRand.h"

#include <random>
#include <thread>
#include <atomic>

namespace expressive {
namespace landscape {

class SNOW_API StochasticSimulationCUDA : public StochasticSimulationBase
{
public:
    StochasticSimulationCUDA();
    virtual ~StochasticSimulationCUDA();

    void initialize();

    virtual void setSimuData(std::shared_ptr<SimulationDataBase> sdb) {simu_data = sdb;}
    virtual size_t eventCount() {return events.size();}

    virtual void start(int, bool onlyTerrain);
    virtual void pause();
    virtual void resume(){assert(false);}
    virtual bool finished() {return simu_finished;}

    virtual double getLastSimuLength() {return simulation_time_;}

    virtual bool isDataInit() {return simu_data->getEventsCount() == events.size();}

    void initSimuData()
    {
        std::vector<float> initial_events (events.size());
        for(size_t i =0; i<initial_events.size(); ++i)
            initial_events[i] = StochasticSimulationCUDA::events[i].second;

        simu_data->defaultParameters(initial_events);
    }

    virtual void renderUpdate(int frame);

    void setTempBufferCount(int i){temporary_buffers = i;}

    struct EventArg
    {
        void* cuda_cells;
        void* cuda_tracks;
        void* cuda_params;
        void* cuda_temp;
        void* cuda_rnd_state;
        void* cpu_params;
        std::vector<float> event_freq;
        int w;
        int h;
        std::ranlux24_base* rnd;
    };

    // Events
    using EventFuncPtr = void (*) (EventArg&);

    static std::vector<std::pair<EventFuncPtr, float>> events;

    class RegisterEvent
    {
    public:
        RegisterEvent(EventFuncPtr ev, float timestep) {
            count = StochasticSimulationCUDA::events.size();
            StochasticSimulationCUDA::events.push_back({ev, timestep});
        }
        size_t ID() {return count;}
    private:
        size_t count;
    };

    using InitialEventStatus = std::pair<EventFuncPtr, std::vector<std::pair<int, float>>>;

    static std::vector<InitialEventStatus> intial_events;

    class RegisterInitialEvent
    {
    public:
        RegisterInitialEvent(EventFuncPtr ev, std::vector<int> depend_params)
        {
            std::vector<std::pair<int, float>> depend_status;
            for( int dp : depend_params)
                depend_status.push_back({dp, uninit_param_});
            StochasticSimulationCUDA::intial_events.push_back({ev, depend_status});
        }
    private:
        static const float uninit_param_;
        friend class StochasticSimulationCUDA;
    };

    // render function
    using RenderFuncPtr = void (*) (void* cells, void* params, void* elevation, void* color, void* normal, int w, int h);

    static RenderFuncPtr render_func;

protected:
    void simuThreadFunction();
    void simuRenderUpdate(int frame);

    void handleInitialEvents(EventArg &);
    void handleEvents(EventArg&);

protected:

    std::shared_ptr<SimulationDataBase> simu_data;
    double simulation_time_;

    CudaMemory cuda_mem_cells, cuda_mem_params, cuda_mem_elev, cuda_mem_color, cuda_mem_normal, cuda_mem_temp, cuda_mem_skitracks;
    CudaRand cuda_random;

    std::ranlux24_base fast_rnd_;
    std::discrete_distribution<> event_distrib;

    std::thread simu_thread;
    std::atomic<bool> simu_finished;
    std::atomic<bool> simu_request_close;
    bool only_terrain_;
    int render_frame;

    int temporary_buffers;
};

// 3 ev:
//  global rare (illum,  vent)
//  large occasionel (avalanches)
//  local frequent (precip, diffusion, changement d'état, transport du vent)

// avalanche:
// 1 (ou +) tirage de position.
// determine zone alétoire autour (perlin ?)
// compare ce masque avec la pente, transforme en neige tombante

}
}

#endif // STOCHASTICSIMULATIONCUDA_H
