#ifndef CUDAGRIDUTILS_H
#define CUDAGRIDUTILS_H

#include <cuda.h>
#include <builtin_types.h>

#include <iostream>

namespace expressive {
namespace landscape {

#ifndef GRIDUTILS_HOST

#define cudaCheck(ans) { cudaAssertFunc((ans), __FILE__, __LINE__, false); }
#define cudaAssert(ans)     { cudaAssertFunc((ans), __FILE__, __LINE__, true); }
inline void cudaAssertFunc(cudaError_t code, const char *file, int line, bool abort)
{
   if (code != cudaSuccess)
   {
      std::cerr << "GPUassert: " << cudaGetErrorString(code) << " " << file << " " << line << std::endl;
      if (abort) std::exit(code);
   }
}

#define cudaCheckKernel() \
    cudaCheck(cudaPeekAtLastError()); \
    cudaCheck(cudaDeviceSynchronize())

#define cudaAssertKernel() \
//    cudaAssert(cudaPeekAtLastError()); \
//    cudaAssert(cudaDeviceSynchronize())

#endif

void requestSync();

inline int ceil_div(int a, int b) {return a/b + ((a%b)?1:0);}
__device__ inline float sigmoid(float x)
{
    return 1.0f / (1.0f + exp(-x));
}

}
}
#endif // CUDAGRIDUTILS_H
