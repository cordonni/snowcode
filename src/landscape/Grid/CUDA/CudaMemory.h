#ifndef CUDAMEMORY_H
#define CUDAMEMORY_H

#include <cstddef>
#include "../Snow/SnowRequired.h"

namespace expressive {
namespace landscape {

class CudaMemory
{
public:
    CudaMemory(size_t=0);
    ~CudaMemory();

    // return true if memory is already allocated,
    // else allocate it and return false
    bool checkAlloc(size_t);

    void hostToDevice(void*);
    void deviceToHost(void*);

    void* get() {return cuda_ptr_;}
    int size() {return size_;}

private:
    int size_;
    void* cuda_ptr_;
};

}
}

#endif // CUDAMEMORY_H
