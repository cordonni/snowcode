#ifndef CUDARAND_H
#define CUDARAND_H

#include "../Snow/SnowRequired.h"
#include "CudaMemory.h"

#include <vector>

namespace expressive {
namespace landscape {


class CudaRand
{
public:
    CudaRand(){}

    // check if the state is initialised for that sequence
    // otherwise allocate and intialize it
    void checkInit(int seed, int size, int offset=0);
    void checkInit(std::vector<int> seeds, int size);

    // get state ptr
    void* getState() {return state.get();}

private:
    CudaMemory state;

};

}
}


#endif // CUDARAND_H
