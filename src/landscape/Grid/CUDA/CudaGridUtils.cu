#ifndef CUDAGRIDUTILS_H
#define CUDAGRIDUTILS_H

#include <cuda.h>

namespace expressive {
namespace landscape {

void requestSync()
{
    cudaDeviceSynchronize();
}


}
}

#endif // CUDAGRIDUTILS_H
