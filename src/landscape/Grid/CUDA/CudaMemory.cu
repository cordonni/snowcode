#include "CudaMemory.h"

#include <cuda.h>
#include <builtin_types.h>

#include "CudaGridUtils.h"

namespace expressive {
namespace landscape {

CudaMemory::CudaMemory(size_t s) :
    size_(s),
    cuda_ptr_(nullptr)
{
    if(s)
        cudaCheck(cudaMalloc((void**)&cuda_ptr_, size_));
}

CudaMemory::~CudaMemory()
{
    if(size_)
        cudaCheck(cudaFree(cuda_ptr_));
}

bool CudaMemory::checkAlloc(size_t new_s)
{
    if(new_s == size_)
        return true;

    if(size_)
        cudaCheck(cudaFree(cuda_ptr_));
    size_ = new_s;

    cudaCheck(cudaMalloc((void**)&cuda_ptr_, size_));

    return false;
}

void CudaMemory::hostToDevice(void* host_ptr)
{
    cudaCheck(cudaMemcpy(cuda_ptr_, host_ptr, size_, cudaMemcpyHostToDevice));
}

void CudaMemory::deviceToHost(void* host_ptr)
{
    cudaCheck(cudaMemcpy(host_ptr, cuda_ptr_, size_, cudaMemcpyDeviceToHost));

}

}
}
