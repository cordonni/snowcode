#ifndef SIMULATIONDATABASE_H
#define SIMULATIONDATABASE_H

#include "GridCommon.h"

#include <map>
#include <fstream>

namespace expressive {
namespace landscape {


class LANDSCAPE_API SimulationDataBase
{
public:
    SimulationDataBase(){}
    virtual ~SimulationDataBase(){}

    virtual void setFrameCount(int) = 0;
    virtual void setSimulated(int) = 0;
    virtual void saveSimulated(int) = 0;
    virtual void importLayers(std::string, float)=0;
    virtual void setSimulationInput(const GridDEM& dem, float cell_size) =0;
    virtual bool empty() = 0;

    virtual void defaultParameters(const std::vector<float>&) = 0;
    virtual void saveFirstFrameParams() = 0;
    virtual void restoreFirstFrameParams(int=0) = 0;

    virtual void* getSimulatedCellsPtr() = 0;
    virtual void* getSimulatedParamsPtr() = 0;
    virtual void* getCellsPtr(int frame) = 0;
    virtual void* getParamsPtr(int frame) = 0;

    virtual float getParam(int frame, int i) const = 0;
    virtual void  setParam(int frame, int i, float param) = 0;
    virtual float getSimulatedParam(int i) const = 0;
    virtual void  setSimulatedParam(int i, float param) = 0;

    virtual float getEvent(int frame, int i) const = 0;
    virtual void  setEvent(int frame, int i, float e) = 0;

    virtual float getSimulatedEvent(int i) const = 0;
    virtual void  setSimulatedEvent(int i, float e) = 0;

    virtual void copySimulatedParams(int from) = 0;
    virtual void copyParams(int from, int to) =0 ;

    virtual size_t getEventsCount() = 0;

    virtual size_t cells_width() = 0;
    virtual size_t cells_height() = 0;
    virtual float& cell(int frame, int x, int y, int layer)= 0;
    virtual float& delta_cell(int x, int y, int layer) = 0;
    virtual void selectDelta(int frame) = 0;
    virtual void applyDelta(int frame, bool=false) = 0;

    virtual std::vector<int> deltaPaintKeys() = 0;

    virtual size_t sizeof_cell()   = 0;
    virtual size_t sizeof_params() = 0;

    virtual double get_refresh_period() =0;

    virtual void load_params(std::ifstream&) = 0;
    virtual void load_events(std::ifstream&, int load_count, int target_count) = 0;
    virtual void load_cells(std::ifstream&, int frame_number, int frame_computed) = 0;

    virtual void save_params(std::ofstream&) = 0;
    virtual void save_events(std::ofstream&) = 0;
    virtual void save_cells(std::ofstream&, int frame_computed) = 0;

    virtual void exportMaps(std::string, int frame) = 0;
    virtual void exportFrames(std::string, int start, int end, int step) = 0;

    virtual GridDEM& renderElevations(int frame) = 0;
    virtual GridDEM& renderSkiTracks(int frame) = 0;
    virtual GridNormal& renderNormals(int frame) = 0;
    virtual GridColor& renderColors(int frame) = 0;
    virtual GridDEM& renderElevationsSimulated() = 0;
    virtual GridNormal& renderNormalsSimulated() = 0;
    virtual GridColor& renderColorsSimulated() = 0;
    virtual GridDEM& renderSkiTracksSimulated() = 0;

    virtual float getTotalTime() const = 0;
    virtual float incrTotalTime(float) = 0;

    virtual void setSimulatedTime(int from_frame) = 0;
    virtual void storeSimulatedTime(int storage_frame) = 0;

};



}
}

#endif // SIMULATIONDATABASE_H


