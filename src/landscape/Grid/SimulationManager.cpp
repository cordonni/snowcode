#include "SimulationManager.h"

#include "landscape/Utils/misc.h"

#include <fstream>

namespace expressive {
namespace landscape {


SimulationManager::SimulationManager(std::shared_ptr<SimulationDataBase> sdb,
                                     std::shared_ptr<StochasticSimulationBase> ssb) :
    initialized(false),
    frame_computed(0),
    layerShown_(-1),
    layerEdited_(-1)
{
    simuData_ = sdb;
    simulator = ssb;
    simulator->setSimuData(sdb);

    setMaxFrame(1000);
    goToFrame(0);
    startSimulation();
    frame_cur_param = -2;
    frame_simu_param = -2;

}


void SimulationManager::goToFrame(int i)
{
    frame_cur = std::max(0, std::min(frame_number-1, i));
    forceUpdateChangedParams(); // check if render params where already updated
    updateParams(frame_cur, frame_cur_param);
    parameterChanged(); // check for change from this frame
}

void SimulationManager::setMaxFrame(int i)
{
    frame_number = std::max(0, i);
    simuData_->setFrameCount(frame_number);
    if(frame_computed > i)
    {
        simuData_->setSimulated(i-1);
        frame_computed = i;
    }
    goToFrame(frame_cur);
}

void SimulationManager::startSimulation()
{
    // Stop the simulation if it is runing
    simulator->pause();
    frame_computed = frame_cur-1;

    simuData_->setSimulated(std::max(0,frame_computed));
    simuData_->setSimulatedTime(std::max(0,frame_computed));
}

void SimulationManager::setSimulationInput(const GridDEM& dem, float cell_size)
{
    simulator->pause();
    simulator->initialize();

    // update first frame
    if(simuData_->empty())
        setMaxFrame(1);

    simuData_->restoreFirstFrameParams();
    simuData_->setSimulationInput(dem, cell_size);
    simuData_->saveFirstFrameParams();

    // update cell size
    cell_size_ = cell_size;
    frame_cur_param = -2;

    sample_per_frame = (int)dem.size();

    // restart simulation
    goToFrame(0);
    startSimulation();
    initialized = true;

}

void SimulationManager::setSimulationInput(std::string dirname, float cell_size)
{
    simulator->pause();
    simulator->initialize();

    // update first frame
    if(simuData_->empty())
        setMaxFrame(1);

    simuData_->restoreFirstFrameParams();
    simuData_->importLayers(dirname, cell_size);
    simuData_->saveFirstFrameParams();

    // update cell size
    cell_size_ = cell_size;
    frame_cur_param = -2;

    sample_per_frame = (int)(simuData_->cells_width() * simuData_->cells_width());

    // restart simulation
    goToFrame(0);
    startSimulation();
    initialized = true;
}

bool SimulationManager::updateSimu(bool record)
{
    if(!initialized)
        return false;

    // simulation side
    if(simulator->finished() && frame_computed < frame_number)
    {
        if(record)
        {
            if(++frame_computed) // initialy -1
            {
                // apply user mask after simu
                simuData_->applyDelta(frame_computed-1);

                simuData_->saveSimulated(frame_computed-1);
                simuData_->storeSimulatedTime(frame_computed-1);
            }
            updateParams(frame_computed, frame_simu_param, true);
        }
        else{
            simuData_->applyDelta(frame_cur, true);
            simuData_->saveSimulated(frame_cur);
            updateParams(frame_cur, frame_simu_param, true, true);
        }


        simulator->start(sample_per_frame, !frame_computed);

        return true;
    }
    return false;

}

double SimulationManager::getLastSimuLength()
{
    return simulator->getLastSimuLength();
}


void SimulationManager::updateParams(int frame, int& frame_param, bool simulatedData, bool force)
{
    if(!simulator->isDataInit())
    {
        defaultParameters();
        return;
    }

    if(frame_param == frame && !force)
        return;

    if(frame_param == frame-1 || (force && frame_param == frame))
    {
        if(simulatedData){}
        //          simuData_->copySimulatedParams(frame_param);
        else
            simuData_->copyParams(frame_param, frame);

        auto to_change = locked_params.find(frame);
        if(to_change != locked_params.end())
        {
            for(auto to_change_pair : to_change->second)
                if(simulatedData)
                    simuData_->setSimulatedParam(to_change_pair.first, to_change_pair.second);
                else
                    simuData_->setParam(frame, to_change_pair.first, to_change_pair.second);
        }

        auto to_change_event = locked_events.find(frame);
        if(to_change_event != locked_events.end())
        {
            for(auto to_change_pair : to_change_event->second)
                if(simulatedData)
                    simuData_->setSimulatedEvent(to_change_pair.first, to_change_pair.second);
                else
                    simuData_->setEvent(frame, to_change_pair.first, to_change_pair.second);
        }
    }
    else
    {
        // parse params lock in frame order, changing each parameters
        if(simulatedData)
            simuData_->copySimulatedParams(0);
        else
            simuData_->restoreFirstFrameParams(frame);

        for(auto it : locked_params)
        {
            if(it.first > frame)
                break;
            for(auto to_change_pair : it.second)
                if(simulatedData)
                    simuData_->setSimulatedParam(to_change_pair.first, to_change_pair.second);
                else
                    simuData_->setParam(frame, to_change_pair.first, to_change_pair.second);
        }

        for(auto it : locked_events)
        {
            if(it.first > frame)
                break;
            for(auto to_change_pair : it.second)
                if(simulatedData)
                    simuData_->setSimulatedEvent(to_change_pair.first, to_change_pair.second);
                else
                    simuData_->setEvent(frame, to_change_pair.first, to_change_pair.second);
        }

    }
    frame_param = frame;
}


void SimulationManager::defaultParameters()
{
    frame_cur_param = 0;
    frame_simu_param = 0;

    simulator->initSimuData();
    simuData_->saveFirstFrameParams();

    locked_events.clear();
}

float SimulationManager::getParameter(int i) const
{
    return simuData_->getParam(frame_cur, i);
}

bool SimulationManager::getParameterLock(int i) const
{
    auto it = locked_params.find(frame_cur);
    if(it == locked_params.end())
        return false;

    return it->second.find(i) != it->second.end();
}

float  SimulationManager::getEventTimeStep(int i) const
{
    if (i>=(int)simuData_->getEventsCount())
        return 0.0f;
    else
        return simuData_->getEvent(frame_cur, i);
}
bool  SimulationManager::isEventLocked(int i)  const
{
    auto it = locked_events.find(frame_cur);
    if(it == locked_events.end())
        return false;

    return it->second.find(i) != it->second.end();
}

void SimulationManager::setParameter(int i, float v)
{
    simuData_->setParam(frame_cur, i, v);
    lockParameter(i, true);
}

void SimulationManager::lockParameter(int i, bool b)
{
    if(b)
        locked_params[frame_cur][i] = simuData_->getParam(frame_cur, i);
    else
    {
        locked_params[frame_cur].erase(i);
        if(locked_params[frame_cur].empty())
            locked_params.erase(frame_cur);

        frame_simu_param = -2;
        frame_cur_param  =-2;
        updateParams(frame_cur, frame_cur_param);

    }
    parameterChanged();
}

void SimulationManager::setEventTimeStep(int i, float e)
{
    simuData_->setEvent(frame_cur, i, e);
    setEventLocked(i, true);
}

void SimulationManager::setEventLocked(int i, bool b)
{
    if(b)
        locked_events[frame_cur][i] = simuData_->getEvent(frame_cur, i);
    else
    {
        locked_events[frame_cur].erase(i);
        if(locked_events[frame_cur].empty())
            locked_events.erase(frame_cur);

        frame_simu_param = -2;
        frame_cur_param  =-2;
        updateParams(frame_cur, frame_cur_param);
    }
    parameterChanged();
}

int SimulationManager::layerEdited() const
{
    return layerEdited_;
}
int SimulationManager::layerShown() const
{
    return layerShown_;
}
void SimulationManager::setLayerEdited(int i)
{
    layerEdited_ = i;
}

void SimulationManager::setLayerShown(int i)
{
    layerShown_ = i;
}

void SimulationManager::forceUpdateChangedParams()
{
    // set a undef value to render triger params to make sure they will updated later
    for(auto & p : params_trigger_render)
    {
        float param = simuData_->getParam(frame_cur, p.first);
        if(param != p.second)
            p.second = std::numeric_limits<float>::max();
    }

}

void SimulationManager::parameterChanged()
{
    bool update = false;
    for(auto & p : params_trigger_render)
    {
        float param = simuData_->getParam(frame_cur, p.first);
        if(param != p.second)
        {
            update = true;
            p.second = param;
        }
    }
    if(update)
        simulator->renderUpdate(frame_cur);
}

void SimulationManager::performEdit(Vector2f pos, float r0, float r1, float amount)
{

    if(layerEdited_ == -1)
        return;
    if(r0>r1)
        std::swap(r0, r1);

    simuData_->selectDelta(frame_cur);

    if(painted.width() != simuData_->cells_width() || painted.height() != simuData_->cells_height())
        painted.resize(simuData_->cells_width(), simuData_->cells_height(), 0.0f);

    int x_pos = int(pos[0]/cell_size_ + 0.5*(float)simuData_->cells_width());
    int y_pos = int(pos[1]/cell_size_ + 0.5*(float)simuData_->cells_height());
    int rs = int(r1 / cell_size_)+1;

    for(int y = std::max(0,y_pos - rs); y<=std::min((int)simuData_->cells_height()-1,y_pos+rs); ++y)
        for(int x = std::max(0,x_pos - rs); x<=std::min((int)simuData_->cells_width()-1,x_pos+rs); ++x)
        {
            float dx = cell_size_*(float(x)-0.5f*(float)simuData_->cells_width())-pos[0];
            float dy = cell_size_*(float(y)-0.5f*(float)simuData_->cells_height())-pos[1];
            float d = std::hypot(dx, dy);
            d = CLAMP((d-r1) / (r0-r1), 0.0f, 1.0f);
            float fact = d*d*(3.0f-2.0f*d);
            if(r0==r1) fact = float(d<r0);
            fact = std::max(fact - painted(x, y), 0.0f);
            painted(x, y)+=fact;

            // paint here ..
            float& p_p = simuData_->cell(frame_cur, x, y, layerEdited_);
            float& p_d = simuData_->delta_cell(x, y, layerEdited_);
            p_p = std::max(p_p + fact * amount, 0.0f);
            p_d += fact * amount;
            if(p_p == 0.0f && fact * amount < 0.0f)
                p_d = -100000.0f; // wanted behaviour is "erase all" here

        }
    simulator->renderUpdate(frame_cur);

}

void SimulationManager::releaseEdit()
{
    painted.fill(0.0f);
}

void SimulationManager::setRenderTriggerParams(std::vector<int> rtp )
{
    params_trigger_render.resize(rtp.size());
    for(int i = 0; i< rtp.size(); ++i)
        params_trigger_render[i] = {rtp[i], std::numeric_limits<float>::min()};
}


void SimulationManager::load(std::string fname)
{
    simulator->pause();

    std::ifstream file (fname, std::ios::binary);

    size_t in_size_t;
    file.read((char*) &in_size_t, sizeof(size_t));

    if(in_size_t != simuData_->sizeof_cell())
    {
        std::cout << "Error while loading " << fname << " : Cell type has evolved\n";
        return;
    }

    file.read((char*) &frame_cur, sizeof(int));
    file.read((char*) &frame_number, sizeof(int));
    file.read((char*) &frame_computed, sizeof(int));

    file.read((char*) &sample_per_frame, sizeof(float));
    file.read((char*) &cell_size_, sizeof(float));

    file.read((char*) &in_size_t, sizeof(size_t));

    bool do_load_params = true;
    if(in_size_t != simuData_->sizeof_params())
    {

        std::cout << "Warning: loading " << fname << " : Parameters have evolved, simulation might me unstable\n";
        file.seekg((long int)file.tellg() + (long int)in_size_t);
        frame_number = frame_computed;
        do_load_params = false;
    }
    else
        simuData_->load_params(file);

    size_t num_locked;
    file.read((char*) &num_locked, sizeof(size_t));
    for(size_t i =0; i< num_locked; ++i)
    {
        int frame;
        size_t num_lock_in_frame;
        file.read((char*) &frame, sizeof(int));
        file.read((char*) &num_lock_in_frame, sizeof(size_t));
        for(size_t j = 0; j<num_lock_in_frame; ++j)
        {
            int n;
            float v;
            file.read((char*) &n, sizeof(int)); // param number
            file.read((char*) &v, sizeof(float)); // param value
            if(do_load_params)
                locked_params[frame][n] = v;
        }
    }

    bool do_load_events = true;
    file.read((char*) &in_size_t, sizeof(size_t));
    if(in_size_t != simulator->eventCount())
    {
        std::cout << "Warning: loading " << fname << " : Events have evolved, simulation might me unstable\n";
        do_load_events = false;
    }

    simuData_->load_events(file, (int) in_size_t, (int)simulator->eventCount());

    file.read((char*) &in_size_t, sizeof(size_t));
    for(size_t i =0; i<in_size_t; ++i)
    {
        int frame; size_t frame_size;
        file.read((char*) &frame, sizeof(int)); // frame
        file.read((char*) &frame_size, sizeof(size_t));
        for(size_t j = 0; j<frame_size; ++j)
        {
            int ev; float f;
            file.read((char*) &ev, sizeof(int)); // event number
            file.read((char*)&f, sizeof(float)); // event activated
            if(do_load_events)
                locked_events[frame][ev] = f;
        }
    }

    simuData_->load_cells(file, frame_number, frame_computed);

    if(frame_computed != frame_number)
        --frame_computed;

    simuData_->setSimulated(std::max(0,frame_cur));

    initialized = true;

    // force recomputation of parameters
    frame_cur_param = -2;
    frame_simu_param = -2;

    updateParams(frame_cur, frame_cur_param);

    parameterChanged();
}

void SimulationManager::save(std::string fname)
{
    assert(frame_computed > 0);

    std::ofstream file (fname, std::ios::binary);

    size_t size_t_io = simuData_->sizeof_cell();

    file.write((char*) &size_t_io, sizeof(size_t));

    file.write((char*) &frame_cur, sizeof(int));
    file.write((char*) &frame_number, sizeof(int));
    file.write((char*) &frame_computed, sizeof(int));

    file.write((char*) &sample_per_frame, sizeof(float));
    file.write((char*) &cell_size_, sizeof(float));

    size_t_io =  simuData_->sizeof_params();
    file.write((char*) &size_t_io, sizeof(size_t));

    // save params
    simuData_->save_params(file);

    size_t_io = locked_params.size();
    file.write((char*) &size_t_io, sizeof(size_t));
    for(auto it : locked_params)
    {
        file.write((char*) &it.first, sizeof(int)); // frame
        size_t_io = it.second.size();
        file.write((char*) &size_t_io, sizeof(size_t));
        for(auto it2 : it.second)
        {
            file.write((char*) &it2.first, sizeof(int)); // param number
            file.write((char*) &it2.second, sizeof(float)); // param value
        }
    }

    simuData_->save_events(file);

    size_t_io = locked_events.size();
    file.write((char*) &size_t_io, sizeof(size_t));
    for(auto it : locked_events)
    {
        file.write((char*) &it.first, sizeof(int)); // frame
        size_t_io = it.second.size();
        file.write((char*) &size_t_io, sizeof(size_t));
        for(auto it2 : it.second)
        {
            file.write((char*) &it2.first, sizeof(int)); // event number
            float f = it2.second;
            file.write((char*)&f, sizeof(float)); // event activated
        }
    }

    simuData_->save_cells(file, frame_computed);

}


}
}
