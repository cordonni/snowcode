/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVEAPPLICATION_H
#define EXPRESSIVEAPPLICATION_H

#include <expressiveLab/main.h>

#include <core/utils/LeakDetector.h>
#include <pluginTools/GLExpressiveEngine.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QApplication>
//#include <QQuickView>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

/**
 * ExpressiveApplication allows to initialize every application-related stuff (loggers, locale, assertion handler. 
 * Also contains the default scene initialization stuff. This also stores main arguments passed to the application.
 * If given --example argument, it will try to load a given .xml scene / object and add it to the scene. It will first 
 * try to load the file as such, and if not found, it will try to find it in the examples directory.
 */
class ExpressiveApplication : public QApplication
{
    Q_OBJECT
public:
    ExpressiveApplication(int& argc, char *argv[]);

    virtual ~ExpressiveApplication();

	/**
	 * static function to try if file exists.
	 */
    static bool FileExists(std::string filename);

	/**
	 * Loads an object or scene passed in arguments through the "--example=..." style.
	 */
    static std::shared_ptr<core::SceneNode> InitializeDefaultSceneNode(std::string filename, ork::ptr<ork::ResourceManager> resource_manager, std::shared_ptr<core::SceneManager> scene_manager);

    int argc;
    char **argv;
//    QQuickView window;

};

} // namespace expressive

#endif // EXPRESSIVEAPPLICATION_H
