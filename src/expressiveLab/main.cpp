/*
 * Ork: a small object-oriented OpenGL Rendering Kernel.
 * Copyright (c) 2008-2010 INRIA
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.
 */

/*
 * Authors: Eric Bruneton, Antoine Begault, Guillaume Piolat.
 */

#include <vector>
#include <assert.h>
#include <cstdio>
#include <iostream>
#include <cstring>
#include <cstdlib>

//#include <boost/program_options.hpp>

#include <expressiveLab/main.h>

#include <QApplication>

using namespace std;

std::string parseCommandArguments(const std::string &arg_name, int argc, char* argv[])
{
    if (argc > 1) {
        for (int i = 1; i < argc; ++i) {
            std::string s = argv[i];
            std::string::size_type index = s.find("-" + arg_name + "=");
            if (index != std::string::npos) {
                return s.substr(index + 2 + arg_name.size(), s.size());
            }
        }
    }
    return "";
}

class MainFunctions
{
public:
    vector<const char*> names;

    vector<MainFunction::mainFunction> functions;
};

static MainFunctions *mainFunctions = NULL;



MainFunction::MainFunction(const char* name, mainFunction f)
{
    if (mainFunctions == NULL) {
        mainFunctions = new MainFunctions();
    }
    mainFunctions->names.push_back(name);
    mainFunctions->functions.push_back(f);
}


int mainFunction(int argc, char* argv[])
{
    assert(mainFunctions != NULL);
    char funcName[50];
#ifdef EXPRESSIVE_MAIN
    sprintf(funcName, "%s", EXPRESSIVE_MAIN);
#else
    sprintf(funcName, "expressiveLab");
#endif

    std::string command_arg = parseCommandArguments("main", argc, argv);
    if (command_arg.length() > 0) {
        sprintf(funcName, "%s", command_arg.c_str());
    }

    for (unsigned int i = 0; i < mainFunctions->names.size(); ++i) {
        if (strcmp(funcName, mainFunctions->names[i]) == 0) {

            printf("Executing '%s'\n", funcName);
            return mainFunctions->functions[i](argc, argv);
        }
    }
    printf("Unknown command line argument '%s'\n", funcName);
    printf("Must be one of:\n");
    for (unsigned int i = 0; i < mainFunctions->names.size(); ++i) {
        printf("%s\n", mainFunctions->names[i]);
    }
    fflush(0);
    return 0;
}


Q_DECL_EXPORT int main(int argc, char *argv[])
{
    std::setbuf(stdout, NULL);
    return mainFunction(argc, argv);
}


