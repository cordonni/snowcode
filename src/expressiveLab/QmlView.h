/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QMLVIEW_H
#define QMLVIEW_H

// plugintools dependencies
#include <pluginTools/sketching/SketchingModeler.h>
//#include <pluginTools/inputcontroller/InputControllerViewer.h>
#include <pluginTools/GLExpressiveEngine.h>

// expressiveLab dependencies
#include <expressiveLab/ExpressiveApplication.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
// qt dependencies
#include <QQuickView>
#ifdef _MSC_VER
#pragma warning( pop )
#endif


class QOpenGLDebugLogger;
class QOpenGLDebugMessage;

namespace expressive
{

namespace plugintools {
    class TouchOverlay;
}

/**
 * QmlView helps to initialize the scene.
 * It is generally necessary when using QML gui's because qt has to be initialized before OpenGL, which in turn
 * has to be initialized before we add anything to the scene.
 *
 * A callback function can be given in argument to replace the default behavior. This allows you to create your own
 * customized scene.
 * A Qml file can also be provided, with the same intent.
 *
 * By default, the scene will try to load the file given with the "--example="...." argument, if any. If it can't, it will create a
 * cube and add it to the scene.
 * The default QML file is a basic scene, with the minimalistic menus of our application. It also offers a sketching interface
 * (available either with the "draw" button or with the 'S' key).
 */
class QmlView: public QQuickView
{
public:
	/**
	 * Typedef for callbacks used to initialize the scene. 
	 */
    typedef void (*callbackFunc)(const ExpressiveApplication &app, std::shared_ptr<plugintools::GLExpressiveEngine> engine);

	/**
	 * QmlView constructor.
	 * @param mainApp The ExpressiveApplication which stores the application arguments. 
	 * @param scene_initialization_callback an optional argument for a custom scene initializer.
	 * @param qml_source an optional custom qml file. Default is QmlMainWindow-GL.qml.
	 * @param parent an optional parent window in which to embed this qml view.
     */
    QmlView(const ExpressiveApplication &mainApp, callbackFunc scene_initialization_callback = nullptr, const char *qml_source = nullptr, QWindow * parent = nullptr);

    virtual ~QmlView();

public slots:
    void InitWindow(); //< initializes Window related stuff in qml.
    void handleLoggedMessage(const QOpenGLDebugMessage& debugMessage);
    virtual void InitScene(); //< Initializes the scene.

protected:
    const ExpressiveApplication &mainApp;

    std::shared_ptr<plugintools::GLExpressiveEngine> engine_;
    bool initialized_;
    bool scene_filled_;

    callbackFunc scene_initialization_callback_; //< callback function called for initializing the scene. Default scene uses the --example argument or adds a cube if the scene is empty.
//    std::shared_ptr<InputControllerViewer> ic_viewer_;
    std::shared_ptr<plugintools::SketchingModeler> sketcher_; // < Default sketching modeler. Currently not used, but activable.
    std::shared_ptr<plugintools::TouchOverlay> touch_overlay_; // < Default touch overlay. Displays the multitouch events on screen.

    std::shared_ptr<QOpenGLDebugLogger> logger_;
};

}

#endif // QMLVIEW_H
