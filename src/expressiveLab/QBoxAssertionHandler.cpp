#include <expressiveLab/QBoxAssertionHandler.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QMessageBox>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <iostream>

using namespace std;

namespace expressive 
{

bool QBoxAssertionHandler::check_assert(const char *expr, const char *file, int line, const char *func_name)
{
    QMessageBox msgbox;
    msgbox.setText("Assertion raised");
    msgbox.setInformativeText("Ignore assertion ?");
    QString err = QString((string(func_name) + ":::" + string(file) + ":" + to_string(line) + "\n" + string(expr) + "\n").c_str());
    std::cout << err.toStdString() << endl;
    if (is_ignored(err)) {
        return true;
    }
    msgbox.setDetailedText(err);
    msgbox.setStandardButtons(QMessageBox::No | QMessageBox::Yes | QMessageBox::YesToAll);
    msgbox.setDefaultButton(QMessageBox::No);
    int ret = msgbox.exec();
    if (ret == QMessageBox::YesToAll) {
        ignore(err);
    }
    return ret != QMessageBox::No;
}

void QBoxAssertionHandler::ignore(const QString &err)
{
    ignored_messages_.insert(err);
}

bool QBoxAssertionHandler::is_ignored(const QString &err)
{
    return ignored_messages_.find(err) != ignored_messages_.end();
}

}
