#include "expressiveLab/ExpressiveApplication.h"

//#include <pluginTools/convol/ImplicitSurfaceSceneNode.h>
#include <expressiveLab/QBoxAssertionHandler.h>

#include "ork/core/Object.h"
#include "ork/core/FileLogger.h"
#include "ork/scenegraph/ShowLogTask.h"

#include <cstdio>

using namespace std;
using namespace expressive::plugintools;
using namespace expressive::core;

namespace expressive
{

ExpressiveApplication::ExpressiveApplication(int & argc, char *argv[]) :
    QApplication(argc, argv),
    argc(argc),
    argv(argv)
{
    // This line avoids issues with qml's handling of threads : items are not created in the same thread as the one where they are manipulated.
    // So until we have a command queue that allows us to delay interactions, use this line that makes our program "monothreaded".
    putenv((char*)"QSG_RENDER_LOOP=basic");

    AssertionHandler::DEFAULT_HANDLER = std::make_shared<QBoxAssertionHandler>();

    QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));
    QCoreApplication::setOrganizationName("Imagine");
    QCoreApplication::setApplicationName("ExpressiveLab");

    // Make the log actually log inside files too. This will enable us to draw the logs on screen too.
    ork::FileLogger::File *out = new ork::FileLogger::File("log.html");
//    Logger::DEBUG_LOGGER = new Logger("DEBUG");

    QSurfaceFormat surfaceFormat;
    surfaceFormat.setProfile(QSurfaceFormat::CoreProfile);
//    surfaceFormat.setVersion(3, 3);
    surfaceFormat.setOption(QSurfaceFormat::DebugContext);
    QSurfaceFormat::setDefaultFormat(surfaceFormat);

    ork::Logger::DEBUG_LOGGER = new ork::FileLogger("DEBUG", out, ork::Logger::DEBUG_LOGGER);
    ork::Logger::INFO_LOGGER = new ork::FileLogger("INFO", out, ork::Logger::INFO_LOGGER);
    ork::Logger::WARNING_LOGGER = new ork::FileLogger("WARNING", out, ork::Logger::WARNING_LOGGER);
    ork::Logger::ERROR_LOGGER = new ork::FileLogger("ERROR", out, ork::Logger::ERROR_LOGGER);

    ork::Logger::DEBUG_LOGGER->enable();
    ork::Logger::INFO_LOGGER->enable();
    ork::Logger::WARNING_LOGGER->enable();
    ork::Logger::ERROR_LOGGER->enable();

    ork::Logger::DEBUG_LOGGER->muteTopic("CORE");
    ork::Logger::DEBUG_LOGGER->muteTopic("MESHDEBUG");
    ork::Logger::DEBUG_LOGGER->muteTopic("POLYGONIZER");
    ork::Logger::DEBUG_LOGGER->muteTopic("RENDER");
    ork::Logger::DEBUG_LOGGER->muteTopic("RESOURCE");
    ork::Logger::DEBUG_LOGGER->muteTopic("SCENEGRAPH");
    ork::Logger::DEBUG_LOGGER->muteTopic("SCHEDULER");

    ork::Logger::INFO_LOGGER->muteTopic("POLYGONIZER");
    ork::Logger::INFO_LOGGER->muteTopic("RESOURCE");

    ork::Logger::ERROR_LOGGER->muteTopic("MESHDEBUG");
    ork::Logger::ERROR_LOGGER->muteTopic("MESHDEBUGADVANCED");

    ork::ShowLogTask::enabled = false;
}

ExpressiveApplication::~ExpressiveApplication()
{
#ifdef CHECK_EXPRESSIVE_LEAKS
    expressive::core::LeakDetector::exit();
#endif
    ork::Object::exit();
}

bool ExpressiveApplication::FileExists(std::string filename)
{
    FILE *f = fopen(filename.c_str(), "r");
    if (f != nullptr) {
        fclose(f);
        return true;
    }
    return false;
}

std::shared_ptr<core::SceneNode> ExpressiveApplication::InitializeDefaultSceneNode(std::string filename, ork::ptr<ork::ResourceManager> resource_manager, std::shared_ptr<SceneManager> scene_manager)
{
//    std::string filename = parseCommandArguments("example", argc, argv);
    if (filename.length() != 0) {
        if (!ExpressiveApplication::FileExists(filename)) {
            filename = "../../examples/" + filename;
            if (!ExpressiveApplication::FileExists(filename)) {
                ork::Logger::ERROR_LOGGER->logf("RESOURCE", "Cannot find file %s", filename.c_str());
                filename = "";
            }
        }
    }

    if (filename.length() != 0) {
        std::shared_ptr<Serializable> obj = resource_manager->loadFromFile(filename).cast<Serializable>();
        if (obj != nullptr) {
            return scene_manager->AddNodeFromSerializable(obj, true);
        }
    }

    return nullptr;
}

} // namespace expressive
