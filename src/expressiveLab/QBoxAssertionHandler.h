/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QBOXASSERTIONHANDLER_H
#define QBOXASSERTIONHANDLER_H

#include <core/utils/AssertionHandler.h>

#include <qstring.h>

#include <set>

namespace expressive
{

// TODO add a always ignore button.
/**
 * The QBoxAssertionHandler class offers a new way to handle exceptions : It offers a way
 * to ignore assertions when debugging requires it. A popup will occur when assertion is raised,
 * and one can then ignore a single assertion, every identical assertions, or close the program.
 * To use them, use expressive_assert instead of classical assert.
 */
class QBoxAssertionHandler : public AssertionHandler
{
public:
    /**
     * QBoxAssertionHandler constructor
     */
    QBoxAssertionHandler() {}

    virtual ~QBoxAssertionHandler() {}

    /**
     * Checks if a given assertion is ignored.
     * @param expr The assertion text.
     * @param file The source file of the assertion
     * @param line the source line of the assertion
     * @param func_name source function of the assertion
     * @return true if the assertion was ignored.
     */
    virtual bool check_assert(const char *expr, const char *file, int line, const char *func_name);

    /**
     * Adds a given assertion to the ignore list.
     */
    void ignore(const QString &err);

    /**
     * Returns true if the assertion is ignored by this handler.
     */
    bool is_ignored(const QString &err);

protected:
    std::set<QString> ignored_messages_;
};

} // namespace expressive

#endif // QBOXASSERTIONHANDLER_H
