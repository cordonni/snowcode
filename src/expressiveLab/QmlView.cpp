#include "expressiveLab/QmlView.h"

#include "core/geometry/MeshTools.h"

#include "pluginTools/inputcontroller/SelectionOverlay.h"
#include "pluginTools/inputcontroller/TouchOverlay.h"
#include "pluginTools/scenemanipulator/SceneManipulator.h"

#include "pluginTools/convol/ImplicitSurfaceSceneNode.h"

#include <QOpenGLDebugLogger>
//#include <QOpenGLDebugMessage>

#include <QTimer>

using namespace ork;
using namespace expressive;
using namespace expressive::core;
using namespace expressive::plugintools;
using namespace std;

namespace expressive
{


/*void QmlView::handleLoggedMessage(const QOpenGLDebugMessage& debugMessage)
{
        printf("%d %d %d %d %s\n",debugMessage.source,debugMessage.type,debugMessage.id,debugMessage.severity,debugMessage.message.toStdString().c_str());
}*/

QmlView::QmlView(const ExpressiveApplication &mainApp, QmlView::callbackFunc scene_initialization_callback, const char *qml_source, QWindow * parent) :
    QQuickView(parent),
    mainApp(mainApp),
    scene_initialization_callback_(scene_initialization_callback)
{
    initialized_ = false;
    scene_filled_ = false;

//    QOpenGLContext* ctx = QOpenGLContext::currentContext();
//    logger_ = std::make_shared<QOpenGLDebugLogger>(this);
//    logger_->initialize();
//    connect(logger_, &QOpenGLDebugLogger::messageLogged, this,&QmlView::handleLoggedMessage);
    engine_ = std::make_shared<GLExpressiveEngine>(this);
    setSource(QUrl(qml_source != nullptr ? qml_source : Config::getStringParameter("default_gui_file").c_str()));

    connect(engine_.get(), &GLExpressiveEngine::glInitialized, this, &QmlView::InitWindow);
    connect(this, &QQuickView::beforeRendering, this, &QmlView::InitScene, Qt::DirectConnection);

    connect((QObject*)this->engine(), SIGNAL(quit()), &mainApp, SLOT(quit()));

}

QmlView::~QmlView()
{
    sketcher_ = nullptr;
    touch_overlay_ = nullptr;
//    ic_viewer_ = nullptr;
    engine_ = nullptr;
}


void QmlView::InitWindow() {
    disconnect(engine_.get(), &GLExpressiveEngine::glInitialized, this, &QmlView::InitWindow);

    // defines GUI file !
    setResizeMode(QQuickView::SizeRootObjectToView);
    initialized_ = true;

}

void QmlView::InitScene()
{
    if (initialized_) {

        disconnect(this, &QQuickView::beforeRendering, this, &QmlView::InitScene);

        std::shared_ptr<SceneManipulator> scene_manipulator = make_shared<SceneManipulator>(engine_.get());
        engine_->set_scene_manipulator(scene_manipulator);
        if (scene_initialization_callback_ != nullptr) {
            scene_initialization_callback_(mainApp, engine_);
        } else {
            std::string filename = parseCommandArguments("example", mainApp.argc, mainApp.argv);
            auto node = ExpressiveApplication::InitializeDefaultSceneNode(filename, engine_->resource_manager(), engine_->scene_manager());

            if (engine_->scene_manipulator() != nullptr) {
    //            ic_viewer_ = make_shared<InputControllerViewer>(engine->scene_manipulator());

                touch_overlay_ = make_shared<TouchOverlay>(engine_->scene_manipulator().get());
                engine_->scene_manipulator()->AddListener(touch_overlay_);
            }

            if (node == nullptr) {
//            if (true) {

                std::shared_ptr<core::SceneNode> n = std::make_shared<core::SceneNode>(engine_->scene_manager().get(), nullptr, "cube");
                std::shared_ptr<AbstractTriMesh> mesh = MeshTools::CreateCube();/*std::make_shared<BasicTriMesh>();*/
                mesh->FillColor(Color(1.0, 0.0, 0.0, 1.0));
                mesh->EdgeSplit(0, 1);
                mesh->garbage_collection();
                n->set_tri_mesh(mesh);
                engine_->scene_manager()->AddNode(n);
            }

        }

        engine_->NotifySceneInitialized();
    }
}

}
