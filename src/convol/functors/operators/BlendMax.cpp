#include <convol/functors/operators/BlendMax.h>

#include <core/functor/FunctorFactory.h>

namespace expressive
{

namespace convol
{

core::FunctorTypeTree BlendMax::BuildStaticType()
{
    std::vector<core::FunctorTypeTree> vec;
    return core::FunctorTypeTree("BlendMax", vec);
}

bool operator==(const BlendMax& /*a*/, const BlendMax& /*b*/)
{
    return true;
}

static core::FunctorFactory::Type<BlendMax> BlendMaxType;

} // namespace convol

} // namespace expressive
