/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BlendOperatorT.h

   Language: C++

   License: Convol Licence

   \author: Adrien Bernhardt
   \author: Maxime Quiblier (Adapted for Convol, 2011)
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for BlendOperator super class.
                This class defines all functions common to all BlendOperators.


   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_BLEND_OPERATOR_H
#define CONVOL_BLEND_OPERATOR_H

// core dependencies
#include<core/CoreRequired.h>
    // functor
    #include<core/functor/Functor.h>
    // geometry
    #include<core/geometry/AABBox.h>

// convol dependencies
    // ScalarFields
    #include<convol/blobtreenode/BlobtreeNode.h>
    // tools
//    #include<Convol/include/tools/ConvergenceTools.h>
//    #include<Convol/include/tools/PointCloudT.h>

// std dependencies
#include<vector>
//#include<string>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/xml/tinyxml/tinyxml.h>



namespace expressive {

namespace convol {

class CONVOL_API BlendOperator : public core::Functor
{
public:
//    virtual const FunctorTypeTree& Type() const = 0;
    
//    virtual std::string Name() const = 0;
    
//    virtual AABBox GetAxisBoundingBox(const std::vector<BlobtreeNode*>& blobtree_node_vector, Scalar value) const = 0;

    virtual core::AABBox GetAxisBoundingBox(const std::vector<std::shared_ptr<BlobtreeNode> >& blobtree_node_vector, Scalar value) const = 0;

};

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_BLEND_OPERATOR_T_H
