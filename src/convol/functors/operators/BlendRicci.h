/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BlendSumT.h

   Language: C++

   License: Convol Licence

   \author: Adrien Bernhardt
   \author: Maxime Quiblier (Adapted for Convol, 2011)
   \author: Galel Koraa
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for union using the Ricci function.
                Ricci function does the following :
                Ricci(e1,e2...en) = sqrt( e1² + e2² + ... + en²)

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_BLEND_OPERATOR_RICCI_T_H_
#define CONVOL_BLEND_OPERATOR_RICCI_T_H_

// core dependencies
#include <core/CoreRequired.h>
    #include<core/functor/MACRO_FUNCTOR_TYPE.h>

// convol dependencies
//    // Macro
//    #include<Convol/include/Macro/CONVOL_MACRO_NAME.h>
    // ScalarFields
    #include<convol/blobtreenode/BlobtreeNode.h>
    // Functors
    #include<convol/functors/operators/BlendOperator.h>

// std dependencies
#include <vector>

namespace expressive {

namespace convol {

class CONVOL_API BlendRicci :
    public BlendOperator
{
public:
    EXPRESSIVE_MACRO_NAME("BlendRicci")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ////////////////////
    // Type functions //
    ////////////////////
    static core::FunctorTypeTree BuildStaticType()
    {
        std::vector<core::FunctorTypeTree> vec;
        return core::FunctorTypeTree("BlendRicci", vec);
    }
    //-------------------------------------------------------------------------
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defined functions :
    //-------------------------------------------------------------------------
    // static const FunctorTypeTree& StaticType();
    //-------------------------------------------------------------------------
    // virtual const FunctorTypeTree& Type() const;

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        core::Functor::AddXmlAttributes(element);
        element->SetDoubleAttribute("exposant",exposant_);
    }

    void InitFromXml(const TiXmlElement* element);

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    inline Scalar Eval(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2, const Point& point) const {
        bool test1 = s_field_1.axis_bounding_box().Contains(point);
        bool test2 = s_field_2.axis_bounding_box().Contains(point);

        if (test1) {
            if (test2) { return pow(pow(s_field_1.Eval(point), exposant_) + pow(s_field_2.Eval(point), exposant_), 1./exposant_); }
            else { return s_field_1.Eval(point); }
        } else if (test2) { return s_field_2.Eval(point); }

        return 0.;
        // return pow(pow(e1,exposant_) + pow(e2,exposant_), 1.0/exposant_);
    }
    inline Scalar operator() (const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2, const Point& point) const
        { return Eval(s_field_1,s_field_2,point); }

    inline Vector EvalGrad(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2, const Point& point, Scalar epsilon_grad) {
        Scalar val_res;
        Vector grad_res;
        EvalValueAndGrad(s_field_1, s_field_2, point, epsilon_grad, val_res, grad_res);
        return grad_res;
    }

    inline void EvalValueAndGrad(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2, const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const {
        bool test1 = s_field_1.axis_bounding_box().Contains(point);
        bool test2 = s_field_2.axis_bounding_box().Contains(point);

        Scalar e1 = 0.f, e2 = 0.f;
        Vector e1_grad, e2_grad;

        if(test1) {
            s_field_1.EvalValueAndGrad(point, epsilon_grad, e1, e1_grad);
            if(!test2) {
                value_res = e1;
                grad_res = e1_grad;
                return;
            }
        }

        if(test2) {
            s_field_2.EvalValueAndGrad(point, epsilon_grad, e2, e2_grad);
            if(!test1) {
                value_res = e2;
                grad_res = e2_grad;
                return;
            }
        }

        Scalar e1_pow = pow(e1,exposant_-1.0);
        Scalar e2_pow = pow(e2,exposant_-1.0);

        Scalar sum_pow = e1*e1_pow + e2*e2_pow;
        value_res = pow(sum_pow, 1.0/exposant_);

        if(value_res != 0.0)
        {
            grad_res = (value_res/sum_pow)*(e1_pow*e1_grad + e2_pow*e2_grad);
        }
        else
        {
            grad_res = Vector::Zero();
        }
    }
    virtual void EvalValueAndGradAndProperties(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,
                                                const Point& point, const Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids, 
                                                const std::vector<ESFVectorProperties>& vect_prop_ids, 
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  
                                                std::vector<Vector>& vect_prop_res   
                                                ) const
    {
        bool test1 = s_field_1.axis_bounding_box().Contains(point);
        bool test2 = s_field_2.axis_bounding_box().Contains(point);

        Scalar e1 = 0.f, e2 = 0.f;
        Vector e1_grad, e2_grad;
        std::vector<Scalar> scal_prop_1, scal_prop_2;
        scal_prop_1.resize(scal_prop_ids.size());
        scal_prop_2.resize(scal_prop_ids.size());
        std::vector<Vector> vect_prop_1, vect_prop_2;
        vect_prop_1.resize(vect_prop_ids.size());
        vect_prop_2.resize(vect_prop_ids.size());

        if(test1) {
            s_field_1.EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, e1, e1_grad, scal_prop_1, vect_prop_1);
            if(!test2) {
                value_res = e1;
                grad_res = e1_grad;
                for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
                    scal_prop_res[i] = scal_prop_1[i];
                for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
                    vect_prop_res[i] = vect_prop_1[i];
                return;
            }
        }

        if(test2) {
            s_field_2.EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, e2, e2_grad, scal_prop_2, vect_prop_2);
            if(!test1) {
                value_res = e2;
                grad_res = e2_grad;
                for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
                    scal_prop_res[i] = scal_prop_2[i];
                for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
                    vect_prop_res[i] = vect_prop_2[i];
                return;
            }
        }

        // Properties res
        Scalar e1_pow = pow(e1,exposant_-1);
        Scalar e2_pow = pow(e2,exposant_-1);

        Scalar sum_pow = e1*e1_pow + e2*e2_pow;
        value_res = pow(sum_pow, 1.0/exposant_);

        if(value_res != 0.0)
        {
            grad_res = (value_res/sum_pow)*(e1_pow*e1_grad + e2_pow*e2_grad);    // done here to avoid 0 div by 0

            Scalar sum_field = e1+e2;
            for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
            {
                scal_prop_res[i] = (e1*scal_prop_1[i] + e2*scal_prop_2[i] )/sum_field;
            }
            for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
            {
                vect_prop_res[i] = (e1*vect_prop_1[i] + e2*vect_prop_2[i] )/sum_field;
            }
        }
    }

    template<typename stdContainer_BlobtreeNodePointer_>
    inline Scalar Eval(const stdContainer_BlobtreeNodePointer_& s_field_container,const Point& point) const
    {
        Scalar res = 0.0;
        for(auto &bt_node : s_field_container)
        {
            if(bt_node->axis_bounding_box().Contains(point))
            {
                Scalar e = bt_node->Eval(point);
                res += pow(e,exposant_);
            }
        }
        return pow(res,1.0/exposant_);
    }
    template<typename stdContainer_BlobtreeNodePointer_>
    inline Scalar operator() (const stdContainer_BlobtreeNodePointer_& s_field_container,const Point& point) const
    {
        return Eval(s_field_container, point);
    }
    template<typename stdContainer_BlobtreeNodePointer_>
    inline Vector EvalGrad(const stdContainer_BlobtreeNodePointer_& s_field_container, const Point& point, 
        Scalar epsilon_grad) const
    {
        Scalar val_res;
        Vector grad_res;
        EvalValueAndGrad(s_field_container, point, epsilon_grad, val_res, grad_res);
        return grad_res;
    }
    template<typename stdContainer_BlobtreeNodePointer_>
    inline void EvalValueAndGrad(const stdContainer_BlobtreeNodePointer_& s_field_container, const Point& point, 
                                 Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        Scalar res = 0.0;
        grad_res = Vector::Zero();
        for(auto &bt_node : s_field_container)
        {
            if(bt_node->axis_bounding_box().Contains(point))
            {
                Scalar e;
                Vector grad_e;
                bt_node->EvalValueAndGrad(point, epsilon_grad, e, grad_e);
                Scalar e_pow = pow(e,exposant_-1.0);
                res += e*e_pow;
                grad_res += e_pow*grad_e;
            }
        }
        value_res = pow(res,1.0/exposant_);

        if(value_res != 0.0) {
            grad_res = (value_res/res)*grad_res;
        }
    }
    template<typename stdContainer_BlobtreeNodePointer_>
    inline void EvalValueAndGradAndProperties(const stdContainer_BlobtreeNodePointer_& blobtree_node_container,
                                                const Point& point, const Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = 0.0;
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = Vector::Zero();
        }
        Scalar res = 0.0;
        Scalar value_aux;
        Vector grad_aux;
        std::vector<Scalar> scal_prop_aux;
        scal_prop_aux.resize(scal_prop_ids.size());
        std::vector<Vector> vect_prop_aux;
        vect_prop_aux.resize(vect_prop_ids.size());
        Scalar sum_field = 0.0;

        for(auto &bt_node : blobtree_node_container)
        {
            if(bt_node->axis_bounding_box().Contains(point))
            {
                bt_node->EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, value_aux, grad_aux, scal_prop_aux, vect_prop_aux);

                Scalar e_pow = pow(value_aux,exposant_-1.0);
                res += value_aux*e_pow;
                grad_res += e_pow*grad_aux;
                for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
                {
                    scal_prop_res[i] += value_aux*scal_prop_aux[i];
                }
                for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
                {
                    vect_prop_res[i] += value_aux*vect_prop_aux[i];
                }
                sum_field += value_aux;
            }
        }
        value_res = pow(res,1.0/exposant_);

        if(value_res != 0.0)
        {
            grad_res = (value_res/res)*grad_res;    // done here to avoid 0 div by 0

            for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
            {
                scal_prop_res[i] = scal_prop_res[i]/sum_field;
            }
            for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
            {
                vect_prop_res[i] = vect_prop_res[i]/sum_field;
            }
        }
    }

        /** \copydoc Convol::BlobtreeNodeT::PrepareForEval(const Scalar , const Scalar,  std::map<BlobtreeNodeT*, std::pair<AABBox, AABBox> >& )
     *
     *  \param blobtree_node Node to prepare for evaluation. It will be prepared as if the scalar fields of its children are blended using a BlendSumT.
     */
    void PrepareForEval(BlobtreeNode& blobtree_node,
                        const Scalar epsilon_bbox, const Scalar accuracy_needed,
                        std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& updated_areas)
    {
        if(blobtree_node.epsilon_bbox() != epsilon_bbox)
        {
            Scalar epsilon_limit = epsilon_bbox / pow( ((Scalar) blobtree_node.children().size()) , 1.0/exposant_);

            core::AABBox bt_node_abb = core::AABBox(); // this auxilliary is there to avoid calling set_axis_bounding_box which implies calls to node listeners.
            for(typename std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = blobtree_node.nonconst_children().begin(); it != blobtree_node.nonconst_children().end(); ++it)
            {
                assert( (*it)!=NULL );
                (*it)->PrepareForEval(epsilon_limit, accuracy_needed, updated_areas);
                bt_node_abb.Enlarge((*it)->axis_bounding_box());
            }

            blobtree_node.set_axis_bounding_box(bt_node_abb);
            blobtree_node.set_epsilon_bbox(epsilon_bbox);
        }
    }

    ///////////////////////////////
    /// BoundingBox computation ///
    ///////////////////////////////

    virtual core::AABBox GetAxisBoundingBox(const std::vector<std::shared_ptr<BlobtreeNode> >& blobtree_node_vector, Scalar value) const
    {
        Scalar value_limit = value / pow( ((Scalar) blobtree_node_vector.size()) , 1.0/exposant_);

        core::AABBox res = core::AABBox();
        for(typename std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = blobtree_node_vector.begin(); it!=blobtree_node_vector.end(); ++it)
        {
            res.Enlarge((*it)->GetAxisBoundingBox(value_limit));
        }
        return res;
    }


    /////////////////
    /// Modifiers ///
    /////////////////

    virtual void set_exposant(Scalar exposant)
    {
        exposant_ = exposant;
    }

    /////////////////
    /// Accessors ///
    /////////////////

    Scalar exposant() const { return exposant_; }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    Scalar exposant_;
};

//-----------------------------------------------------------------------------
bool operator==(const BlendRicci& a, const BlendRicci& b);

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_BLEND_OPERATOR_RICCI_T_H_
