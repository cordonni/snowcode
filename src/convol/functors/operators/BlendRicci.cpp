#include <convol/functors/operators/BlendRicci.h>

#include <core/functor/FunctorFactory.h>

#include <core/xmlloaders/TraitsXmlQuery.h>

namespace expressive {

namespace convol {

void BlendRicci::InitFromXml(const TiXmlElement* element)
{
    double exposant;
    core::TraitsXmlQuery::QueryDoubleAttribute("BlendRicci::InitFromXml",
                                               element, "exposant", &exposant);
    set_exposant(exposant);
}

bool operator==(const BlendRicci& a, const BlendRicci& b)
{
    return a.exposant() == b.exposant();
}

static core::FunctorFactory::Type<BlendRicci> BlendRicciType;

} // namespace convol

} // namespace expressive
