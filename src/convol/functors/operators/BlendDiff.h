/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BlendSumT.h

   Language: C++

   License: Convol Licence

   \author: Adrien Bernhardt
   \author: Maxime Quiblier (Adapted for Convol, 2011)
   \author: Galel Koraa
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for union using a basic sum of the scalar fields.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_BLEND_OPERATOR_DIFF_H_
#define CONVOL_BLEND_OPERATOR_DIFF_H_

// core dependencies
#include <core/CoreRequired.h>
    #include<core/functor/MACRO_FUNCTOR_TYPE.h>

// convol dependencies
//    // Macro
//    #include<Convol/include/Macro/CONVOL_MACRO_NAME.h>
    // ScalarFields
    #include<convol/blobtreenode/BlobtreeNode.h>
    // Functors
    #include<convol/functors/operators/BlendOperator.h>

// std dependencies
#include <vector>
//#include <string>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/xml/tinyxml/tinyxml.h>


namespace expressive {

namespace convol {

class BlendDiff :
    public BlendOperator
{
public:
    EXPRESSIVE_MACRO_NAME("BlendDiff")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ////////////////////
    // Type functions //
    ////////////////////
    static core::FunctorTypeTree BuildStaticType()
    {
        std::vector<core::FunctorTypeTree> vec;
        return core::FunctorTypeTree("BlendDiff", vec);
    }
    //-------------------------------------------------------------------------
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defined functions :
    //-------------------------------------------------------------------------
    // static const FunctorTypeTree& StaticType();
    //-------------------------------------------------------------------------
    // virtual const FunctorTypeTree& Type() const;

//    ///////////////////
//    // Xml functions //
//    ///////////////////

//    TiXmlElement * GetXml() const
//    {
//        TiXmlElement *element_base = new TiXmlElement(Name());

//        AddXmlSimpleAttribute(element_base);
//        AddXmlComplexAttribute(element_base);

//        return element_base;
//    }
//    inline void AddXmlSimpleAttribute(TiXmlElement * element) const
//    {
//        Base::AddXmlSimpleAttribute(element);
//    }
//    inline void AddXmlComplexAttribute(TiXmlElement * element) const
//    {
//        Base::AddXmlComplexAttribute(element);
//    }

    //////////////////////////
    // Evaluation functions //
    //////////////////////////

    inline Scalar Eval(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,
                       const Point& point) const
    {
        const Scalar f1 = s_field_1.Eval(point);
        const Scalar f2 = s_field_2.Eval(point);

        const Scalar ratio = f1 / (f2+0.001); // 0.001 is here to have numerical stablity when f2->0.0
        if(ratio>2.0)
        {
            return f1 - f2;
        }
        else
        {
            const Scalar p = 0.5*ratio + 1.0;
            const Scalar factor = p*p * (-0.5*p + 1.5) - 1.0 ; // cubic poynomial factor \in [0;1]
            return f1 - factor * f2;
        }
    }

    inline Scalar operator() (const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,
                              const Point& point) const
    {
        //TODO:@todo : rajouter test de bounding box ...
        return Eval(s_field_1,s_field_2,point);
    }
    inline Vector EvalGrad(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2, const Point& point,
                         Scalar epsilon_grad)
    {
        // TODO:@todo : to be implemented
        Scalar f;
        Vector grad;
        EvalValueAndGrad(s_field_1, s_field_2, point, epsilon_grad, f, grad);
        return grad;
    }
    inline void EvalValueAndGrad(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2, const Point& point,
                                 Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        Scalar f1, f2;
        Vector f1_grad, f2_grad;
        s_field_1.EvalValueAndGrad(point, epsilon_grad, f1, f1_grad);
        s_field_2.EvalValueAndGrad(point, epsilon_grad, f2, f2_grad);
//        const Scalar f1 = s_field_1.Eval(point);
//        const Scalar f2 = s_field_2.Eval(point);

        const Scalar inv_f2_eps = 1.0 / (f2+0.001);
        const Scalar ratio = f1 * inv_f2_eps; // 0.001 is here to have numerical stablity when f2->0.0
        if(ratio>2.0)
        {
            value_res = f1 - f2;
            grad_res = f1_grad - f2_grad;
        }
        else
        {
            const Scalar p = 0.5*ratio + 1.0;
            const Scalar p_sqr = p*p;
            const Scalar factor = p_sqr * (-0.5*p + 1.5) - 1.0 ; // cubic poynomial factor \in [0;1]
            const Scalar d_factor = - 0.75 * p_sqr + 1.5 * p ; // derivative of poynomial factor

            value_res = f1 - factor * f2;

            const Scalar var = f2 * d_factor * inv_f2_eps;
            grad_res = (1.0-var)*f1_grad  + (var*ratio - factor)*f2_grad;

//            //TODO:@todo : compute this in an analytical way in previous if
//            Vector test = GradientEvaluationT<Traits>::NumericalGradient6points(*this, s_field_1, s_field_2, point, epsilon_grad);
//            std::cout<< (grad_res-test).norm() / test.norm() << std::endl;
        }
    }
    virtual void EvalValueAndGradAndProperties(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,
                                                const Point& point, const Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
    {
        Scalar f1, f2;
        Vector f1_grad, f2_grad;
        std::vector<Scalar> scal_prop_1, scal_prop_2;
        scal_prop_1.resize(scal_prop_ids.size());
        scal_prop_2.resize(scal_prop_ids.size());
        std::vector<Vector> vect_prop_1, vect_prop_2;
        vect_prop_1.resize(vect_prop_ids.size());
        vect_prop_2.resize(vect_prop_ids.size());

        s_field_1.EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, f1, f1_grad, scal_prop_1, vect_prop_1);
        s_field_2.EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, f2, f2_grad, scal_prop_2, vect_prop_2);

        const Scalar inv_f2_eps = 1.0 / (f2+0.001);
        const Scalar ratio = f1 * inv_f2_eps; // 0.001 is here to have numerical stablity when f2->0.0
        if(ratio>2.0)
        {
            value_res = f1 - f2;
            grad_res = f1_grad - f2_grad;
        }
        else
        {
            const Scalar p = 0.5*ratio + 1.0;
            const Scalar p_sqr = p*p;
            const Scalar factor = p_sqr * (-0.5*p + 1.5) - 1.0 ; // cubic poynomial factor \in [0;1]
            const Scalar d_factor = - 0.75 * p_sqr + 1.5 * p ; // derivative of poynomial factor

            value_res = f1 - factor * f2;

            //TODO:@odo : seems to be wrong :
            //"relative error" (norm of the diff divided by norm of num_grad) in between 0.01 and 0.4 last time I check...
            const Scalar var = f2 * d_factor * inv_f2_eps;
            grad_res = (1.0-var)*f1_grad  + (var*ratio - factor)*f2_grad;
//            Vector test = GradientEvaluationT<Traits>::NumericalGradient6points(*this, s_field_1, s_field_2, point, epsilon_grad*0.001);
//            std::cout<< "////begin/////"<<std::endl;
//            std::cout<< "diff grad : " << (grad_res-test).norm() << std::endl;
//            std::cout<< "associated num grad : "<< (test).norm() << std::endl;
//            std::cout<< "associated grad : "<< (grad_res).norm() << std::endl;
        }

        if(value_res > 0.0) {
            // Properties res
            Scalar sum_f1_f2 = f1 + f2;
            for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
            {
                scal_prop_res[i] = (f1*scal_prop_1[i] + f2*scal_prop_2[i] )/sum_f1_f2;
            }
            for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
            {
                vect_prop_res[i] = (f1*vect_prop_1[i] + f2*vect_prop_2[i] )/sum_f1_f2;
            }
        }
        else
        {
            // Properties res
            for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
            {
                scal_prop_res[i] = ExpressiveTraits::kSFScalarPropertiesDefault[scal_prop_ids[i]];
            }
            for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
            {
                vect_prop_res[i] = ExpressiveTraits::kSFVectorPropertiesDefault[vect_prop_ids[i]];
            }
        }
    }

        /** \copydoc Convol::BlobtreeNodeT::PrepareForEval(const Scalar , const Scalar,  std::map<BlobtreeNodeT*, std::pair<core::AABBox, core::AABBox> >& )
     *
     *  \param blobtree_node Node to prepare for evaluation. It will be prepared as if the scalar fields of its children are blended using a BlendSumT.
     */
    void PrepareForEval(BlobtreeNode& blobtree_node,
                        const Scalar epsilon_bbox, const Scalar accuracy_needed,
                        std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& updated_areas)
    {
        if(blobtree_node.epsilon_bbox() != epsilon_bbox)
        {
            Scalar scal_n = (Scalar) blobtree_node.children().size();
            core::AABBox bt_node_abb = core::AABBox(); // this auxilliary is there to avoid calling set_axis_bounding_box which implies calls to node listeners.
            for(typename std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = blobtree_node.nonconst_children().begin(); it != blobtree_node.nonconst_children().end(); ++it)
            {
                assert( (*it)!=NULL );
                (*it)->PrepareForEval(epsilon_bbox/scal_n, accuracy_needed, updated_areas);
                bt_node_abb = core::AABBox::Union(bt_node_abb, (*it)->axis_bounding_box());
            }

            blobtree_node.set_axis_bounding_box(bt_node_abb);
            blobtree_node.set_epsilon_bbox(epsilon_bbox);
        }
    }

    /////////////////////////////
    // BoundingBox computation //
    /////////////////////////////


//    virtual core::AABBox GetAxisBoundingBox(const std::vector<BlobtreeNode*>& blobtree_node_vector, const Scalar value) const
    virtual core::AABBox GetAxisBoundingBox(const std::vector<std::shared_ptr<BlobtreeNode> >& blobtree_node_vector, Scalar value) const
    {
        core::AABBox res = core::AABBox();
        for(typename std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = blobtree_node_vector.begin(); it!=blobtree_node_vector.end(); ++it)
        {
            res = core::AABBox::Union(res,(*it)->GetAxisBoundingBox(value/((Scalar) blobtree_node_vector.size())));
        }
        return res;
    }

};

//-----------------------------------------------------------------------------
bool CONVOL_API operator==(const BlendDiff& /*a*/, const BlendDiff& /*b*/);

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_BLEND_OPERATOR_DIFF_H_
