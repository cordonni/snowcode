#include <convol/functors/operators/BlendDiff.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

bool operator==(const BlendDiff& /*a*/, const BlendDiff& /*b*/)
{
    return true;
}

static core::FunctorFactory::Type<BlendDiff> BlendDiffType;

} // namespace convol

} // namespace expressive
