/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: DistanceIntegralSurface.h

   Language: C++

   License: Expressive Licence

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Header file for distance-like integral surfaces.
                It automatically use Inverse kernel as the base kernel for computation.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_DISTANCE_INTEGRAL_SURFACE_H_
#define CONVOL_DISTANCE_INTEGRAL_SURFACE_H_

// core dependencies
#include <core/CoreRequired.h>
    #include <core/functor/MACRO_FUNCTOR_TYPE.h>

// convol dependencies
#include <convol/blobtreenode/BlobtreeNode.h>
#include <convol/functors/operators/BlendOperator.h>

#include <convol/functors/kernels/cauchyinverse/HomotheticInverse.h>
#include <convol/functors/kernels/cauchyinverse/HornusInverse.h>

//TODO:@todo : temporary !
#include <convol/tools/GradientEvaluationTools.h>

// std dependencies
#include <vector>

namespace expressive {

namespace convol {

class CONVOL_API DistanceIntegralSurface :
    public BlendOperator
{
public:
    EXPRESSIVE_MACRO_NAME("DistanceIntegralSurface")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ////////////////////
    // Type functions //
    ////////////////////
    static core::FunctorTypeTree BuildStaticType()
    {
        std::vector<core::FunctorTypeTree> vec;
        return core::FunctorTypeTree("DistanceIntegralSurface", vec);
    }
    //-------------------------------------------------------------------------
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defined functions :
    //-------------------------------------------------------------------------
    // static const FunctorTypeTree& StaticType();
    //-------------------------------------------------------------------------
    // virtual const FunctorTypeTree& Type() const;

    //////////////////////////
    // Evaluation functions //
    //////////////////////////

    Scalar Eval(const std::vector<std::shared_ptr<BlobtreeNode> >& s_field_container,const Point& point) const;
    inline Scalar operator() (const std::vector<std::shared_ptr<BlobtreeNode> >& s_field_container,const Point& point) const
    {
        return Eval(s_field_container, point);
    }
    Vector EvalGrad(const std::vector<std::shared_ptr<BlobtreeNode> >& s_field_container, const Point& point,
                    Scalar epsilon_grad) const;
    void EvalValueAndGrad(const std::vector<std::shared_ptr<BlobtreeNode> >& s_field_container, const Point& point,
                          Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;
    void EvalValueAndGradAndProperties(const std::vector<std::shared_ptr<BlobtreeNode> >& blobtree_node_container,
                                       const Point& point, const Scalar epsilon_grad,
                                       const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                       const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                       Scalar& value_res, Vector& grad_res,
                                       std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                       std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                       ) const;

    /** \copydoc Convol::BlobtreeNodeT::PrepareForEval(const Scalar , const Scalar,  std::map<BlobtreeNodeT*, std::pair<AABBox, AABBox> >& )
     *
     *  \param blobtree_node Node to prepare for evaluation. It will be prepared as if the scalar fields of its children are blended using a DistanceIntegralSurfaceT.
     */
    void PrepareForEval(BlobtreeNode& blobtree_node,
                        const Scalar epsilon_bbox, const Scalar accuracy_needed,
                        std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& updated_areas);

    ///////////////////////////////
    /// BoundingBox computation ///
    ///////////////////////////////

    core::AABBox GetAxisBoundingBox(const std::vector<std::shared_ptr<BlobtreeNode> >& blobtree_node_vector, Scalar value) const;

protected :
    HomotheticInverse4 homothetic_kernel;
    HornusInverse4 hornus_kernel;
};

//-----------------------------------------------------------------------------
bool operator==(const DistanceIntegralSurface& /*a*/, const DistanceIntegralSurface& /*b*/);

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_BLEND_OPERATOR_SUM_H_
