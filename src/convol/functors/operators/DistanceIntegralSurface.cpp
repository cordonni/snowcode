#include <convol/functors/operators/DistanceIntegralSurface.h>

#include <convol/primitives/NodeSegmentSFieldT.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

using namespace core;

namespace convol {

Scalar DistanceIntegralSurface::Eval(const std::vector<std::shared_ptr<BlobtreeNode> >& s_field_container,
                                     const Point& point) const
{
    Scalar homothetic_field = 0.0;
    Scalar hornus_field = 0.0;
    for(auto &bt_node : s_field_container)
    {
        if(bt_node->axis_bounding_box().Contains(point))
        {
            auto node_segment = std::static_pointer_cast<AbstractNodeSegmentSField>(bt_node);
            homothetic_field += homothetic_kernel.Eval(node_segment->opt_w_seg(),point);
            hornus_field += hornus_kernel.Eval(node_segment->opt_w_seg(),point);
        }
    }

    Scalar local_radius = hornus_field / homothetic_field;
    return local_radius * (pow(homothetic_field, -1.0/(4.0-1.0)) - 1.0);

//    return homothetic_field;
}

Vector DistanceIntegralSurface::EvalGrad(const std::vector<std::shared_ptr<BlobtreeNode> >& s_field_container, const Point& point,
                                         Scalar epsilon_grad) const
{
    //TODO:@todo : derive new gradient later ...
    return GradientEvaluation::NumericalGradient6points(*this, s_field_container, point, epsilon_grad);
}
void DistanceIntegralSurface::EvalValueAndGrad(const std::vector<std::shared_ptr<BlobtreeNode> >& s_field_container, const Point& point,
                                               Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
    //TODO:@todo : derive new gradient later ...
    value_res = Eval(s_field_container, point);
    grad_res = GradientEvaluation::NumericalGradient6points(*this, s_field_container, point, epsilon_grad);
}
void DistanceIntegralSurface::EvalValueAndGradAndProperties(
        const std::vector<std::shared_ptr<BlobtreeNode> >& blobtree_node_container,
        const Point& point, const Scalar epsilon_grad,
        const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
        const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
        Scalar& value_res, Vector& grad_res,
        std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
        std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
        ) const
{
    //TODO:@todo : derive new gradient later ...
    value_res = Eval(blobtree_node_container, point);
    grad_res = GradientEvaluation::NumericalGradient6points(*this, blobtree_node_container, point, epsilon_grad);
    // and just use constant properties for now ...
    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
    {
        scal_prop_res[i] = 0.0;//0.5;
    }
    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
    {
        vect_prop_res[i] = Vector::Zero();
    }
}


void DistanceIntegralSurface::PrepareForEval(BlobtreeNode& blobtree_node,
                                             const Scalar epsilon_bbox, const Scalar accuracy_needed,
                                             std::map<BlobtreeNode*, std::pair<AABBox, AABBox> >& updated_areas)
{
    if(blobtree_node.epsilon_bbox() != epsilon_bbox)
    {
        Scalar scal_n = (Scalar) blobtree_node.children().size();
        AABBox bt_node_abb = AABBox(); // this auxilliary is there to avoid calling set_axis_bounding_box which implies calls to node listeners.
        for(std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = blobtree_node.nonconst_children().begin(); it != blobtree_node.nonconst_children().end(); ++it)
        {
            assert( (*it)!=NULL );
            (*it)->PrepareForEval(epsilon_bbox/scal_n, accuracy_needed, updated_areas);
            bt_node_abb.Enlarge((*it)->axis_bounding_box());
        }

        blobtree_node.set_axis_bounding_box(bt_node_abb);
        blobtree_node.set_epsilon_bbox(epsilon_bbox);
    }
}
    
AABBox DistanceIntegralSurface::GetAxisBoundingBox(const std::vector<std::shared_ptr<BlobtreeNode> >& blobtree_node_vector, const Scalar value) const
{
    AABBox res = AABBox();
    for(auto &bt_node : blobtree_node_vector) {
        //TODO:@todo : for now do a simple some, will have to do a cast to get the segment geometry...
        res.Enlarge(bt_node->GetAxisBoundingBox(value/((Scalar) blobtree_node_vector.size())));
    }
    return res;
}
        
    
bool operator==(const DistanceIntegralSurface& /*a*/, const DistanceIntegralSurface& /*b*/)
{
    return true;
}

static core::FunctorFactory::Type<DistanceIntegralSurface> DistanceIntegralSurfaceType;

} // namespace convol

} // namespace expressive
