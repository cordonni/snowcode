/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BlendSumT.h

   Language: C++

   License: Convol Licence

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inrialpes.fr

   Description: Header file for blend using a sum of the scalar fields.
                It use a basic (& naive) spatial optimization structure to avoid multiple BBox query on all children

                We should use a smarter method ...BVH ???

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_BLEND_OPERATOR_SUM_OPTIMIZED_H_
#define CONVOL_BLEND_OPERATOR_SUM_OPTIMIZED_H_

// core dependencies
#include <core/CoreRequired.h>

// convol dependencies
    // ScalarFields
    #include<convol/blobtreenode/BlobtreeNode.h>
    // Functors
    #include<convol/functors/operators/BlendOperator.h>
    // tools
    #include<convol/tools/Array3DT.h> //TODO:@todo : to be changed with something smarter ...
//    #include<Convol/include/tools/ConvergenceTools.h>
//    #include<Convol/include/tools/PointCloudT.h>
//    // Probablement provisoir

//// std dependencies
//#include <string>
//#include <vector>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/Xml/tinyxml/tinyxml.h>

namespace expressive {

namespace convol {

class CONVOL_API BlendSumOptimized :
    public BlendOperator
{
public:
    EXPRESSIVE_MACRO_NAME("BlendSumOptimized")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    typedef Array3DT< typename std::vector<BlobtreeNode*> > Array3DVecNode;

    BlendSumOptimized() : grid_vect_node_(NULL)
    {
        res_ = 0.5;
    }

//    ////////////////////
//    // Type functions //
//    ////////////////////
//    static FunctorTypeTree BuildStaticType()
//    {
//        std::vector<FunctorTypeTree> vec;
//        return FunctorTypeTree("BlendSumOptimized", vec);
//    }
//    //-------------------------------------------------------------------------
//    CONVOL_MACRO_FUNCTOR_TYPE // This macro defined functions :
//    //-------------------------------------------------------------------------
//    // static const FunctorTypeTree& StaticType();
//    //-------------------------------------------------------------------------
//    // virtual const FunctorTypeTree& Type() const;

//    ///////////////////
//    // Xml functions //
//    ///////////////////

//    TiXmlElement * GetXml() const
//    {
//        TiXmlElement *element_base = new TiXmlElement(StaticName());

//        AddXmlSimpleAttribute(element_base);
//        AddXmlComplexAttribute(element_base);

//        return element_base;
//    }
//    inline void AddXmlSimpleAttribute(TiXmlElement * element) const
//    {
//        Base::AddXmlSimpleAttribute(element);
//    }
//    inline void AddXmlComplexAttribute(TiXmlElement * element) const
//    {
//        Base::AddXmlComplexAttribute(element);
//    }

    //////////////////////////
    // Evaluation functions //
    //////////////////////////

    inline Scalar Eval(const Point& point) const
    {
        // Compute the cell of the grid to take :
        Point relative_point = point - corner_000_;
        int index_0 = (int)(relative_point[0]/res_);
        int index_1 = (int)(relative_point[1]/res_);
        int index_2 = (int)(relative_point[2]/res_);

        Scalar res = 0.0;
        // avoid signed/unsigned warning
        unsigned int ui_index_0 = (unsigned int) index_0;
        unsigned int ui_index_1 = (unsigned int) index_1;
        unsigned int ui_index_2 = (unsigned int) index_2;
        if( index_0>=0 && ui_index_0<size_[0] && index_1>=0 && ui_index_1<size_[1] && index_2>=0 && ui_index_2<size_[2] )
        {
            const std::vector<BlobtreeNode*>& vec_node = (*grid_vect_node_)(ui_index_0, ui_index_1, ui_index_2);
            auto it(vec_node.begin()), end(vec_node.end());
            for( ; it != end; ++it)
            {
                if((*it)->axis_bounding_box().Contains(point))
                {
                    res += (*it)->Eval(point);
                }
            }
        }
        return res;
    }
    inline Scalar operator() (const Point& point) const
    {
        return Eval(point);
    }
    inline Vector EvalGrad(const Point& point, Scalar epsilon_grad) const
    {
        Scalar value_res; Vector grad_res;
        EvalValueAndGrad(point, epsilon_grad, value_res, grad_res);
        return grad_res;
    }
    inline void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        // Compute the cell of the grid to take :
        Point relative_point = point - corner_000_;
        int index_0 = (int)(relative_point[0]/res_);
        int index_1 = (int)(relative_point[1]/res_);
        int index_2 = (int)(relative_point[2]/res_);

        value_res = 0.0;
        grad_res.vectorize(0.0);
        // avoid signed/unsigned warning
        unsigned int ui_index_0 = (unsigned int) index_0;
        unsigned int ui_index_1 = (unsigned int) index_1;
        unsigned int ui_index_2 = (unsigned int) index_2;
        if( index_0>=0 && ui_index_0<size_[0] && index_1>=0 && ui_index_1<size_[1] && index_2>=0 && ui_index_2<size_[2] )
        {
            Scalar value_aux;
            Vector grad_aux;
            const std::vector<BlobtreeNode*>& vec_node = (*grid_vect_node_)(ui_index_0, ui_index_1, ui_index_2);
            auto it(vec_node.begin()), end(vec_node.end());
            for( ; it != end; ++it)
            {
                if((*it)->axis_bounding_box().Contains(point))
                {
                    (*it)->EvalValueAndGrad(point, epsilon_grad, value_aux, grad_aux);
                    value_res += value_aux;
                    grad_res += grad_aux;
                }
            }
        }
    }
    inline void EvalValueAndGradAndProperties(  const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
    {
        // Compute the cell of the grid to take :
        Point relative_point = point - corner_000_;
        int index_0 = (int)(relative_point[0]/res_);
        int index_1 = (int)(relative_point[1]/res_);
        int index_2 = (int)(relative_point[2]/res_);
        // avoid signed/unsigned warning
        unsigned int ui_index_0 = (unsigned int) index_0;
        unsigned int ui_index_1 = (unsigned int) index_1;
        unsigned int ui_index_2 = (unsigned int) index_2;

        value_res = 0.0;
        grad_res.vectorize(0.0);
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = 0.0;
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = Vector::vectorized(0.0);
        }
        if( index_0>=0 && ui_index_0<size_[0] && index_1>=0 && ui_index_1<size_[1] && index_2>=0 && ui_index_2<size_[2] )
        {
            Scalar value_aux;
            Vector grad_aux;
            std::vector<Scalar> scal_prop_aux;
            scal_prop_aux.resize(scal_prop_ids.size());
            std::vector<Vector> vect_prop_aux;
            vect_prop_aux.resize(vect_prop_ids.size());
            const std::vector<BlobtreeNode*>& vec_node = (*grid_vect_node_)(ui_index_0, ui_index_1, ui_index_2);
            auto it(vec_node.begin()), end(vec_node.end());
            for( ; it != end; ++it)
            {
                if((*it)->axis_bounding_box().Contains(point))
                {
                    (*it)->EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, value_aux, grad_aux, scal_prop_aux, vect_prop_aux);
                    value_res += value_aux;
                    grad_res += grad_aux;

                    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
                    {
                        scal_prop_res[i] += value_aux*scal_prop_aux[i];
                    }
                    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
                    {
                        vect_prop_res[i] += value_aux*vect_prop_aux[i];
                    }
                }
            }
        }
        if(value_res != 0.0)
        {
            for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
            {
                scal_prop_res[i] = scal_prop_res[i]/value_res;
            }
            for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
            {
                vect_prop_res[i] = vect_prop_res[i]/value_res;
            }
        }
//        else
//        {
//            for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
//            {
//                scal_prop_res[i] = Traits::kSFScalarPropertiesDefault[scal_prop_ids[i]];
//            }
//            for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
//            {
//                vect_prop_res[i] = Traits::kSFVectorPropertiesDefault[vect_prop_ids[i]];
//            }
//        }
    }

    /** \copydoc Convol::BlobtreeNodeT::PrepareForEval(Scalar , Scalar,  std::map<BlobtreeNodeT*, std::pair<AABBox, AABBox> >& )
     *
     *  \param blobtree_node Node to prepare for evaluation. It will be prepared as if the scalar fields of its children are blended using a BlendSumT.
     */

//const stdContainer_BlobtreeNodePointer_& blobtree_node_container,
    template<typename stdContainer_BlobtreeNodePointer_>
    core::AABBox PrepareForEval(const stdContainer_BlobtreeNodePointer_& blobtree_node_container,
                        Scalar epsilon_bbox, Scalar accuracy_needed,
                        std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& updated_areas)
    {
        Scalar scal_n = (Scalar) blobtree_node_container.size();
        core::AABBox bt_node_abb = core::AABBox(); // this auxilliary is there to avoid calling set_axis_bounding_box which implies calls to node listeners.
        for(auto it = blobtree_node_container.begin(); it != blobtree_node_container.end(); ++it)
        {
            assert( (*it)!=NULL );
            (*it)->PrepareForEval(epsilon_bbox/scal_n, accuracy_needed, updated_areas);
            bt_node_abb = core::AABBox::Union(bt_node_abb, (*it)->axis_bounding_box());
        }

        // Effective update of the grid
        PrepareGrid(bt_node_abb, blobtree_node_container);
//        ////////////////////////////////////////////////////
//        // Update of the grid
//        corner_000_ = bt_node_abb.min_;
//        Scalar size_0 = bt_node_abb.max_[0] - corner_000_[0];
//        Scalar size_1 = bt_node_abb.max_[1] - corner_000_[1];
//        Scalar size_2 = bt_node_abb.max_[2] - corner_000_[2];
//        size_.resize(3);
//        size_[0] = (unsigned int)(size_0/res_) + 2;
//        size_[1] = (unsigned int)(size_1/res_) + 2;
//        size_[2] = (unsigned int)(size_2/res_) + 2;

//        if(grid_vect_node_ != NULL)
//        {
//            delete grid_vect_node_;
//        }
//        grid_vect_node_ = new Array3DVecNode(size_);

//        for(auto it = blobtree_node_container.begin(); it != blobtree_node_container.end(); ++it)
//        {
//            // get min and max index
//            Vector upper_bound = (*it)->axis_bounding_box().max_ - corner_000_;
//            Vector lower_bound = (*it)->axis_bounding_box().min_ - corner_000_;
//            int x_min = (int)(lower_bound[0]/res_);
//            int y_min = (int)(lower_bound[1]/res_);
//            int z_min = (int)(lower_bound[2]/res_);

//            int x_max = (int)(upper_bound[0]/res_)+1;
//            int y_max = (int)(upper_bound[1]/res_)+1;
//            int z_max = (int)(upper_bound[2]/res_)+1;

//            for(int i = x_min; i<=x_max; ++i)
//            {
//                for(int j = y_min; j<=y_max; ++j)
//                {
//                    for(int k = z_min; k<=z_max; ++k)
//                    {
//                        (*grid_vect_node_)(i,j,k).push_back(*it);
//                    }
//                }
//            }
//        }
//        ////////////////////////////////////////////////////

        return bt_node_abb;
    }

    /** Update the grid in function of a vector of NodeScalarField and a bounding box
     *  This function is called either in PrepareForEval and clone function
     *  \param aabb bounding node which combined with the grid resolution give the size of the grid
     *  \param blobtree_node_container node that will be inserted in all cell of the grid where they have some influence (depending on there bounding box)
     */
    template<typename stdContainer_BlobtreeNodePointer_>
    void PrepareGrid(const core::AABBox& aabb, const stdContainer_BlobtreeNodePointer_& blobtree_node_container)
    {
        corner_000_ = aabb.min_;
        Scalar size_0 = aabb.max_[0] - corner_000_[0];
        Scalar size_1 = aabb.max_[1] - corner_000_[1];
        Scalar size_2 = aabb.max_[2] - corner_000_[2];
        size_.resize(3);
        size_[0] = (unsigned int)(size_0/res_) + 2;
        size_[1] = (unsigned int)(size_1/res_) + 2;
        size_[2] = (unsigned int)(size_2/res_) + 2;

        if(grid_vect_node_) delete grid_vect_node_;

        grid_vect_node_ = new Array3DVecNode(size_);

        for(auto it = blobtree_node_container.begin(); it != blobtree_node_container.end(); ++it)
        {
            // get min and max index
            Vector upper_bound = (*it)->axis_bounding_box().max_ - corner_000_;
            Vector lower_bound = (*it)->axis_bounding_box().min_ - corner_000_;
            int x_min = (int)(lower_bound[0]/res_);
            int y_min = (int)(lower_bound[1]/res_);
            int z_min = (int)(lower_bound[2]/res_);

            int x_max = (int)(upper_bound[0]/res_)+1;
            int y_max = (int)(upper_bound[1]/res_)+1;
            int z_max = (int)(upper_bound[2]/res_)+1;

            for(int i = x_min; i<=x_max; ++i)
            {
                for(int j = y_min; j<=y_max; ++j)
                {
                    for(int k = z_min; k<=z_max; ++k)
                    {
                        (*grid_vect_node_)(i,j,k).push_back(*it);
                    }
                }
            }
        }
    }

    /////////////////////////////
    // BoundingBox computation //
    /////////////////////////////

    virtual core::AABBox GetAxisBoundingBox(const std::vector<BlobtreeNode*>& blobtree_node_vector, Scalar value) const
    {
        core::AABBox res = core::AABBox();
        for(typename std::vector<BlobtreeNode*>::const_iterator it = blobtree_node_vector.begin(); it!=blobtree_node_vector.end(); ++it)
        {
            res = core::AABBox::Union(res,(*it)->GetAxisBoundingBox(value/((Scalar) blobtree_node_vector.size())));
        }
        return res;
    }
    
private :    

    // Attributs
    Scalar res_;

    Array3DVecNode *grid_vect_node_;
    typename std::vector<unsigned int> size_;
    Point corner_000_;

};

//-----------------------------------------------------------------------------
bool operator==(const BlendSumOptimized& a, const BlendSumOptimized& b)
{
    UNUSED(a); UNUSED(b);
    return true;
}

} // Close namespace convol

} // Close namespace expressive






//template<typename stdContainer_BlobtreeNodePointer_>
//inline Scalar Eval(const stdContainer_BlobtreeNodePointer_& s_field_container,const Point& point) const
//{
//    // Compute the cell of the grid to take :
//    Point relative_point = point - corner_000_;
//    int index_0 = (int)(relative_point[0]/res_);
//    int index_1 = (int)(relative_point[1]/res_);
//    int index_2 = (int)(relative_point[2]/res_);

//    Scalar res = 0.0;
//    if( index_0>=0 && index_0<size_[0] && index_1>=0 && index_1<size_[1] && index_2>=0 && index_2<size_[2] )
//    {
//        const std::vector<BlobtreeNode*>& vec_node = (*grid_vect_node_)((unsigned int)(index_0), (unsigned int)(index_1), (unsigned int)(index_2));
//        for(auto it = vec_node.begin(); it != vec_node.end(); ++it)
//        {
//            res += (*it)->Eval(point);
//        }
//    }
//    return res;
//}
//template<typename stdContainer_BlobtreeNodePointer_>
//inline Scalar operator() (const stdContainer_BlobtreeNodePointer_& s_field_container,const Point& point) const
//{
//    return Eval(s_field_container, point);
//}
//template<typename stdContainer_BlobtreeNodePointer_>
//inline Vector EvalGrad(const stdContainer_BlobtreeNodePointer_& s_field_container, const Point& point,
//    Scalar epsilon_grad) const
//{
//    Vector grad_res = Vector::vectorized(0.0);
//    for(typename stdContainer_BlobtreeNodePointer_::const_iterator it = s_field_container.begin(); it != s_field_container.end(); it++)
//    {
//        grad_res += (*it)->EvalGrad(point, epsilon_grad);
//    }
//    return grad_res;
//}
//template<typename stdContainer_BlobtreeNodePointer_>
//inline void EvalValueAndGrad(const stdContainer_BlobtreeNodePointer_& s_field_container, const Point& point,
//                             Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
//{
//    // Compute the cell of the grid to take :
//    Point relative_point = point - corner_000_;
//    int index_0 = (int)(relative_point[0]/res_);
//    int index_1 = (int)(relative_point[1]/res_);
//    int index_2 = (int)(relative_point[2]/res_);

//    value_res = 0.0;
//    grad_res.vectorize(0.0);
//    if( index_0>=0 && index_0<size_[0] && index_1>=0 && index_1<size_[1] && index_2>=0 && index_2<size_[2] )
//    {
//        Scalar value_aux;
//        Vector grad_aux;
//        const std::vector<BlobtreeNode*>& vec_node = (*grid_vect_node_)(index_0, index_1, index_2);
//        for(auto it = vec_node.begin(); it != vec_node.end(); ++it)
//        {
//            (*it)->EvalValueAndGrad(point, epsilon_grad, value_aux, grad_aux);
//            value_res += value_aux;
//            grad_res += grad_aux;
//        }
//    }
//}
//template<typename stdContainer_BlobtreeNodePointer_>
//inline void EvalValueAndGradAndProperties(const stdContainer_BlobtreeNodePointer_& blobtree_node_container,
//                                            const Point& point, Scalar epsilon_grad,
//                                            const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
//                                            const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
//                                            Scalar& value_res, Vector& grad_res,
//                                            std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
//                                            std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
//                                            ) const
//{
//    // Compute the cell of the grid to take :
//    Point relative_point = point - corner_000_;
//    int index_0 = (int)(relative_point[0]/res_);
//    int index_1 = (int)(relative_point[1]/res_);
//    int index_2 = (int)(relative_point[2]/res_);

//    value_res = 0.0;
//    grad_res.vectorize(0.0);
//    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
//    {
//        scal_prop_res[i] = 0.0;
//    }
//    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
//    {
//        vect_prop_res[i] = Vector::vectorized(0.0);
//    }
//    if( index_0>=0 && index_0<size_[0] && index_1>=0 && index_1<size_[1] && index_2>=0 && index_2<size_[2] )
//    {
//        Scalar value_aux;
//        Vector grad_aux;
//        std::vector<Scalar> scal_prop_aux;
//        scal_prop_aux.resize(scal_prop_ids.size());
//        std::vector<Vector> vect_prop_aux;
//        vect_prop_aux.resize(vect_prop_ids.size());
//        const std::vector<BlobtreeNode*>& vec_node = (*grid_vect_node_)(index_0, index_1, index_2);
//        for(auto it = vec_node.begin(); it != vec_node.end(); ++it)
//        {
//            (*it)->EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, value_aux, grad_aux, scal_prop_aux, vect_prop_aux);
//            value_res += value_aux;
//            grad_res += grad_aux;

//            for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
//            {
//                scal_prop_res[i] += value_aux*scal_prop_aux[i];
//            }
//            for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
//            {
//                vect_prop_res[i] += value_aux*vect_prop_aux[i];
//            }
//        }
//    }
//    if(value_res != 0.0)
//    {
//        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
//        {
//            scal_prop_res[i] = scal_prop_res[i]/value_res;
//        }
//        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
//        {
//            vect_prop_res[i] = vect_prop_res[i]/value_res;
//        }
//    }
//    else
//    {
//        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
//        {
//            scal_prop_res[i] = Traits::kSFScalarPropertiesDefault[scal_prop_ids[i]];
//        }
//        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
//        {
//            vect_prop_res[i] = Traits::kSFVectorPropertiesDefault[vect_prop_ids[i]];
//        }
//    }
//}

///** \copydoc Convol::BlobtreeNodeT::PrepareForEval(Scalar , Scalar,  std::map<BlobtreeNodeT*, std::pair<AABBox, AABBox> >& )
// *
// *  \param blobtree_node Node to prepare for evaluation. It will be prepared as if the scalar fields of its children are blended using a BlendSumT.
// */
//void PrepareForEval(BlobtreeNode& blobtree_node,
//                    Scalar epsilon_bbox, Scalar accuracy_needed,
//                    std::map<BlobtreeNode*, std::pair<AABBox, AABBox> >& updated_areas)
//{
//    if(blobtree_node.epsilon_bbox() != epsilon_bbox)
//    {
//        Scalar scal_n = (Scalar) blobtree_node.children().size();
//        AABBox bt_node_abb = AABBox(); // this auxilliary is there to avoid calling set_axis_bounding_box which implies calls to node listeners.
//        for(typename std::vector<BlobtreeNode*>::const_iterator it = blobtree_node.nonconst_children().begin(); it != blobtree_node.nonconst_children().end(); ++it)
//        {
//            assert( (*it)!=NULL );
//            (*it)->PrepareForEval(epsilon_bbox/scal_n, accuracy_needed, updated_areas);
//            bt_node_abb = AABBox::Union(bt_node_abb, (*it)->axis_bounding_box());
//        }

//        blobtree_node.set_axis_bounding_box(bt_node_abb);
//        blobtree_node.set_epsilon_bbox(epsilon_bbox);

//        ////////////////////////////////////////////////////
//        // Update of the grid
//        corner_000_ = blobtree_node.axis_bounding_box().min_;
//        Scalar size_0 = blobtree_node.axis_bounding_box().max_[0] - corner_000_[0];
//        Scalar size_1 = blobtree_node.axis_bounding_box().max_[1] - corner_000_[1];
//        Scalar size_2 = blobtree_node.axis_bounding_box().max_[2] - corner_000_[2];
//        size_.resize(3);
//        size_[0] = (unsigned int)(size_0/res_) + 2;
//        size_[1] = (unsigned int)(size_1/res_) + 2;
//        size_[2] = (unsigned int)(size_2/res_) + 2;

//        if(grid_vect_node_ != NULL)
//        {
//            delete grid_vect_node_;
//        }
//        grid_vect_node_ = new Array3DVecNode(size_);

//        for(typename std::vector<BlobtreeNode*>::const_iterator it = blobtree_node.nonconst_children().begin(); it != blobtree_node.nonconst_children().end(); ++it)
//        {
//            // get min and max index
//            Vector upper_bound = (*it)->axis_bounding_box().max_ - corner_000_;
//            Vector lower_bound = (*it)->axis_bounding_box().min_ - corner_000_;
//            int x_min = (int)(lower_bound[0]/res_);
//            int y_min = (int)(lower_bound[1]/res_);
//            int z_min = (int)(lower_bound[2]/res_);

//            int x_max = (int)(upper_bound[0]/res_)+1;
//            int y_max = (int)(upper_bound[1]/res_)+1;
//            int z_max = (int)(upper_bound[2]/res_)+1;

//            for(int i = x_min; i<=x_max; ++i)
//            {
//                for(int j = y_min; j<=y_max; ++j)
//                {
//                    for(int k = z_min; k<=z_max; ++k)
//                    {
//                        (*grid_vect_node_)(i,j,k).push_back(*it);
//                    }
//                }
//            }
//        }
//        ////////////////////////////////////////////////////
//    }
//}

#endif // CONVOL_BLEND_OPERATOR_SUM_OPTIMIZED_H_
