/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#ifndef CONVOL_BLEND_OPERATOR_BULGE_H_
#define CONVOL_BLEND_OPERATOR_BULGE_H_

// core dependencies
#include <core/CoreRequired.h>
    #include<core/functor/MACRO_FUNCTOR_TYPE.h>

// convol dependencies
//    // Macro
//    #include<Convol/include/Macro/CONVOL_MACRO_NAME.h>
    // ScalarFields
    #include<convol/blobtreenode/BlobtreeNode.h>
    // Functors
    #include<convol/functors/operators/BlendOperator.h>

// std dependencies
#include <vector>
//#include <string>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/xml/tinyxml/tinyxml.h>


namespace expressive {

namespace convol {

class CONVOL_API BlendBulge:
    public BlendOperator
{
public:
    EXPRESSIVE_MACRO_NAME("BlendBulge")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ////////////////////
    // Type functions //
    ////////////////////
    static core::FunctorTypeTree BuildStaticType();

    //-------------------------------------------------------------------------
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defined functions :
    //-------------------------------------------------------------------------
    // static const FunctorTypeTree& StaticType();
    //-------------------------------------------------------------------------
    // virtual const FunctorTypeTree& Type() const;

//    ///////////////////
//    // Xml functions //
//    ///////////////////

//    TiXmlElement * GetXml() const
//    {
//        TiXmlElement *element_base = new TiXmlElement(Name());

//        AddXmlSimpleAttribute(element_base);
//        AddXmlComplexAttribute(element_base);

//        return element_base;
//    }
//    inline void AddXmlSimpleAttribute(TiXmlElement * element) const
//    {
//        Base::AddXmlSimpleAttribute(element);
//    }
//    inline void AddXmlComplexAttribute(TiXmlElement * element) const
//    {
//        Base::AddXmlComplexAttribute(element);
//    }

    //////////////////////////
    // Evaluation functions //
    //////////////////////////
    /// \brief Eval
    /// \param s_field_1
    /// \param s_field_2
    /// \param point
    /// \return
    inline Scalar Eval(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,
                       const Point& point) const;

    Scalar operator() (const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,
                              const Point& point) const;

    Vector EvalGrad(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2, const Point& point,
                         Scalar epsilon_grad);

    void EvalValueAndGrad(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2, const Point& point,
                                 Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;

    virtual void EvalValueAndGradAndProperties(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,
                                                const Point& point, const Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const;

    /** \copydoc Convol::BlobtreeNodeT::PrepareForEval(const Scalar , const Scalar,  std::map<BlobtreeNodeT*, std::pair<core::AABBox, core::AABBox> >& )
     *
     *  \param blobtree_node Node to prepare for evaluation. It will be prepared as if the scalar fields of its children are blended using a BlendSumT.
     */
    void PrepareForEval(BlobtreeNode& blobtree_node,
                        const Scalar epsilon_bbox, const Scalar accuracy_needed,
                        std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& updated_areas);

    /////////////////////////////
    // BoundingBox computation //
    /////////////////////////////


//    virtual core::AABBox GetAxisBoundingBox(const std::vector<BlobtreeNode*>& blobtree_node_vector, const Scalar value) const
    virtual core::AABBox GetAxisBoundingBox(const std::vector<std::shared_ptr<BlobtreeNode> >& blobtree_node_vector, Scalar value) const;
};

//-----------------------------------------------------------------------------
bool operator==(const BlendBulge& /*a*/, const BlendBulge& /*b*/);

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_BLEND_OPERATOR_SUM_H_
