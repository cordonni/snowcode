#include <convol/functors/operators/BlendSum.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

bool operator==(const BlendSum& /*a*/, const BlendSum& /*b*/)
{
    return true;
}

static core::FunctorFactory::Type<BlendSum> BlendSumType;

} // namespace convol

} // namespace expressive
