/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BlendMaxT.h

   Language: C++

   License: Convol Licence

   \author: Adrien Bernhardt
   \author: Maxime Quiblier (Adapted for Convol, 2011)
   \author: Galel Koraa
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for union using the max of the scalar fields.
                This functor is made to be used in Blobtree blending nodes.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_BLEND_OPERATOR_MAX_T_H_
#define CONVOL_BLEND_OPERATOR_MAX_T_H_


// core dependencies
#include <core/CoreRequired.h>

// convol dependencies
//    // Macro
//    #include<Convol/include/Macro/CONVOL_MACRO_NAME.h>
    #include<core/functor/MACRO_FUNCTOR_TYPE.h>
    // ScalarFields
    #include<convol/blobtreenode/BlobtreeNode.h>
    // Functors
    #include<convol/functors/operators/BlendOperator.h>

// std dependencies
#include <vector>
//#include <string>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/xml/tinyxml/tinyxml.h>


namespace expressive {

namespace convol {

class CONVOL_API BlendMax :
    public BlendOperator
{
public:
    EXPRESSIVE_MACRO_NAME("BlendMax")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    BlendMax() {}
    ~BlendMax() {}

    //////////////////////
    /// Type functions ///
    //////////////////////

    static core::FunctorTypeTree BuildStaticType();

    //-------------------------------------------------------------------------
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defined functions :
    //-------------------------------------------------------------------------
    // static const FunctorTypeTree& StaticType();
    //-------------------------------------------------------------------------
    // virtual const FunctorTypeTree& Type() const;


//    ///////////////////
//    // Xml functions //
//    ///////////////////


    //////////////////////////
    // Evaluation functions //
    //////////////////////////
    
    inline Scalar Eval(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,const Point& point) const
    {
        //TODO:@todo : rajouter test de bounding box ...
        return std::max(s_field_1.Eval(point), s_field_2.Eval(point));
    }
    inline Scalar operator() (const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,const Point& point) const
    {
        //TODO:@todo : rajouter test de bounding box ...
        return Eval(s_field_1, s_field_2, point);
    }
    // todo review
    inline void EvalValueAndGrad(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,const Point& point,
                                 Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        //TODO:@todo : rajouter test de bounding box ...
        Scalar e1, e2;
        Vector grad_e1, grad_e2;
        s_field_1.EvalValueAndGrad(point, epsilon_grad, e1, grad_e1);
        s_field_2.EvalValueAndGrad(point, epsilon_grad, e2, grad_e2);
        if(e1 > e2)
        {
            value_res = e1;
            grad_res = grad_e1;
        }
        else
        {   
            value_res = e2;
            grad_res = grad_e2;
        }
    }
    inline void EvalValueAndGradAndProperties(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,
                                                const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
    {
        //TODO:@todo : rajouter test de bounding box ...
        Scalar e1, e2;
        Vector e1_grad, e2_grad;
        std::vector<Scalar> scal_prop_1, scal_prop_2;
        scal_prop_1.resize(scal_prop_ids.size());
        scal_prop_2.resize(scal_prop_ids.size());
        std::vector<Vector> vect_prop_1, vect_prop_2;
        vect_prop_1.resize(vect_prop_ids.size());
        vect_prop_2.resize(vect_prop_ids.size());

        s_field_1.EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, e1, e1_grad, scal_prop_1, vect_prop_1);
        s_field_2.EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, e2, e2_grad, scal_prop_2, vect_prop_2);

        if(e1 > e2)
        {
            value_res = e1;
            grad_res = e1_grad;
            scal_prop_res = scal_prop_1;
            vect_prop_res = vect_prop_1;
        }
        else
        {
            value_res = e2;
            grad_res = e2_grad;
            scal_prop_res = scal_prop_2;
            vect_prop_res = vect_prop_2;
        }
    }

    template<typename stdContainer_BlobtreeNodePointer_>
    inline Scalar Eval(const stdContainer_BlobtreeNodePointer_& blobtree_node_container,const Point& point) const
    {
        Scalar res = expressive::kScalarMin; //0.0; //TODO:@todo : good question ...
        for(auto &bt_node : blobtree_node_container)
        {
            if(bt_node->axis_bounding_box().Contains(point))
            {
                res = std::max(res, bt_node->Eval(point));
            }
        }
        return res;
    }
    template<typename stdContainer_BlobtreeNodePointer_>
    inline Scalar operator() (const stdContainer_BlobtreeNodePointer_& blobtree_node_container,const Point& point) const
    {
        return Eval(blobtree_node_container, point);
    }
    template<typename stdContainer_BlobtreeNodePointer_>
    inline Vector EvalGrad(const stdContainer_BlobtreeNodePointer_& blobtree_node_container, const Point& point, 
                         const Scalar epsilon_grad) const
    {
        Scalar val_res;
        Vector grad_res;
        EvalValueAndGrad(blobtree_node_container, point, epsilon_grad, val_res, grad_res);
        return grad_res;
    }
    template<typename stdContainer_BlobtreeNodePointer_>
    inline void EvalValueAndGrad(const stdContainer_BlobtreeNodePointer_& blobtree_node_container, const Point& point, 
                                 Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        value_res = 0.0;
        grad_res = Vector::Zero();

        Scalar value_aux;
        Vector grad_aux;
        for(auto &bt_node : blobtree_node_container)
        {
            if(bt_node->axis_bounding_box().Contains(point))
            {
                bt_node->EvalValueAndGrad(point, epsilon_grad, value_aux, grad_aux);
                if(value_res <  value_aux)
                {
                    value_res = value_aux;
                    grad_res = grad_aux;
                }
            }
        }
    }
    template<typename stdContainer_BlobtreeNodePointer_>
    inline void EvalValueAndGradAndProperties(const stdContainer_BlobtreeNodePointer_& blobtree_node_container,const Point& point, const Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = 0.0;
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = Vector::Zero();
        }

        Scalar value_aux;
        Vector grad_aux;
        std::vector<Scalar> scal_prop_aux;
        scal_prop_aux.resize(scal_prop_ids.size());
        std::vector<Vector> vect_prop_aux;
        vect_prop_aux.resize(vect_prop_ids.size());

        for(auto &bt_node : blobtree_node_container)
        {
            if(bt_node->axis_bounding_box().Contains(point))
            {
                bt_node->EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, value_aux, grad_aux, scal_prop_aux, vect_prop_aux);
                if(value_res < value_aux)
                {
                    value_res = value_aux;
                    grad_res = grad_aux;
                    scal_prop_res = scal_prop_aux;
                    vect_prop_res = vect_prop_aux;
                }
            }
        }
    }

    /** \copydoc Convol::BlobtreeNodeT::PrepareForEval(const Scalar , const Scalar,  std::map<BlobtreeNodeT*, std::pair<AABBox, AABBox> >& )
     *
     *  \param blobtree_node Node to prepare for evaluation. It will be prepared as if the scalar fields of its children are blended using a BlendMaxT.
     */
    void PrepareForEval(BlobtreeNode& blobtree_node,
                        Scalar epsilon_bbox, Scalar accuracy_needed,
                        std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& updated_areas)
    {
        if(blobtree_node.epsilon_bbox() != epsilon_bbox)
        {
            core::AABBox bt_node_abb = core::AABBox(); // this auxilliary is there to avoid calling set_axis_bounding_box which implies calls to node listeners.
            for(typename std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = blobtree_node.nonconst_children().begin(); it != blobtree_node.nonconst_children().end(); ++it)
            {
                assert( (*it)!=NULL );
                (*it)->PrepareForEval(epsilon_bbox, accuracy_needed, updated_areas);
                bt_node_abb = core::AABBox::Union(bt_node_abb, (*it)->axis_bounding_box());
            }

            blobtree_node.set_axis_bounding_box(bt_node_abb);
            blobtree_node.set_epsilon_bbox(epsilon_bbox);
        }
    }

    /////////////////////////////
    // BoundingBox computation //
    /////////////////////////////

//    virtual AABBox GetAxisBoundingBox(const std::vector<BlobtreeNode*>& blobtree_node_vector, const Scalar value) const
    virtual core::AABBox GetAxisBoundingBox(const std::vector<std::shared_ptr<BlobtreeNode> >& blobtree_node_vector, Scalar value) const
    {
        core::AABBox res = core::AABBox();

        for(std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = blobtree_node_vector.begin(); it!=blobtree_node_vector.end(); ++it)
        {
            res = core::AABBox::Union(res,(*it)->GetAxisBoundingBox(value));
        }
        return res;
    }
};

//-----------------------------------------------------------------------------
bool operator==(const BlendMax& /*a*/, const BlendMax& /*b*/);

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_BLEND_OPERATOR_MAX_T_H_
