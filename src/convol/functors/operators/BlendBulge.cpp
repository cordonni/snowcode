#include <convol/functors/operators/BlendBulge.h>

#include <core/functor/FunctorFactory.h>


using namespace std;

namespace expressive {

using namespace core;

namespace convol {

////////////////////
// Type functions //
////////////////////
core::FunctorTypeTree BlendBulge::BuildStaticType()
{
    std::vector<core::FunctorTypeTree> vec;
    return core::FunctorTypeTree("BlendBulge", vec);
}

inline Scalar F(Scalar f, Scalar iso, Scalar m, Scalar cj, Scalar h)
{
    if (f < cj) {
        return 0.0;
    }
    if (f >= cj && f <= m) {
        Scalar x = (f - cj) / (m - cj);
        return (3.0 - 2.0 * x) * pow(x, 2) * h;
    }
    if (f >= m && f <= iso) {
        Scalar x = (f - m) / (iso - m);
        return h + ((1.0 - 3.0 * h) + (2.0 * h - 1.0) * x) * pow(x, 2);
    }
    return 0.0;
}

inline Scalar F2(Scalar f, Scalar iso, Scalar m, Scalar cj, Scalar h, Scalar n)
{
    if (f < cj) {
        return 0.0;
    }
    if (f >= cj && f <= m) {
        Scalar x = (f - cj) / (m - cj);
        Scalar y = (cos(((2.0 * n + 1) * x + 1.0) * M_PI) + 1.0) / 2.0;
        return (3.0 - 2.0 * x) * pow(x, 2) * h * y;
    }
    if (f >= m && f <= iso) {
        Scalar x = (f - m) / (iso - m);
        return h + ((1.0 - 3.0 * h) + (2.0 * h - 1.0) * x) * pow(x, 2);
    }
    return 0.0;
}

//inline Scalar F2(Scalar v, Scalar m, Scalar ci, Scalar h, Scalar n) {
//    Scalar iso = 1.0;
//    Scalar distIso = 0.5;
//    if (v < distIso) {
//        return 0.0;
//    }
//    if (v > iso) {
//        return 1.0;
//    }
//    return pow(pow(1.0 - (v - iso) / (distIso), 2), 2);
//}

Scalar EvalBulge(Scalar val1, Scalar val2)
{
//    Scalar res = F(val2, 1.0, 0.5, 0.1, 0.3);
    Scalar res = F2(val2, 1.0, 1.2, 0.01, 0.4, 3);
    return val1 + res;
}

Scalar BlendBulge::Eval(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,
                   const Point& point) const
{
    //TODO:@todo : rajouter test de bounding box ...
    Scalar val1, val2;
    val1 = s_field_1.Eval(point);
    val2 = s_field_2.Eval(point);

    Scalar res = EvalBulge(val1, val2);

    return res;
}

Scalar BlendBulge::operator() (const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,
                          const Point& point) const
{
    //TODO:@todo : rajouter test de bounding box ...
    return Eval(s_field_1,s_field_2,point);
}

Vector BlendBulge::EvalGrad(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2, const Point& point,
                     Scalar epsilon_grad)
{
    //TODO:@todo : rajouter test de bounding box ...
    return s_field_1.EvalGrad(point, epsilon_grad) + s_field_2.EvalGrad(point, epsilon_grad);
}

void BlendBulge::EvalValueAndGrad(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2, const Point& point,
                             Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
    // Since this code is used as an example, here is some details about what is done :
    // This operator can compute the gradient given the node gradient. So it does it.
    // We could also have used a numerical computation calling Eval.
    // To do so, we would have need :
    // #include<Convol/include/tools/GradientEvaluationToolsT.h>
    // and a call to
    // GradientEvaluationT<Traits>::NumericalGradient6points(*this, point, epsilon);

    //TODO:@todo : rajouter test de bounding box ...
    Scalar value_1, value_2;
    Vector grad_1, grad_2;
    s_field_1.EvalValueAndGrad(point, epsilon_grad, value_1, grad_1);
    s_field_2.EvalValueAndGrad(point, epsilon_grad, value_2, grad_2);

//        value_res = value_1 + value_2;
    value_res = EvalBulge(value_1, value_2);
    grad_res = grad_1 + grad_2;
}

void BlendBulge::EvalValueAndGradAndProperties(const BlobtreeNode& s_field_1,const BlobtreeNode& s_field_2,
                                            const Point& point, const Scalar epsilon_grad,
                                            const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                            const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                            Scalar& value_res, Vector& grad_res,
                                            std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                            std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                            ) const
{
    //TODO:@todo : rajouter test de bounding box ...
    Scalar e1, e2;
    Vector e1_grad, e2_grad;
    std::vector<Scalar> scal_prop_1, scal_prop_2;
    scal_prop_1.resize(scal_prop_ids.size());
    scal_prop_2.resize(scal_prop_ids.size());
    std::vector<Vector> vect_prop_1, vect_prop_2;
    vect_prop_1.resize(vect_prop_ids.size());
    vect_prop_2.resize(vect_prop_ids.size());

    s_field_1.EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, e1, e1_grad, scal_prop_1, vect_prop_1);
    s_field_2.EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, e2, e2_grad, scal_prop_2, vect_prop_2);

    // Properties res
//    value_res = e1 + e2;
    value_res = EvalBulge(e1, e2);
    grad_res = e1_grad + e2_grad;

    if(value_res != 0.0)
    {
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = (e1*scal_prop_1[i] + e2*scal_prop_2[i] )/value_res;
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = (e1*vect_prop_1[i] + e2*vect_prop_2[i] )/value_res;
        }
    }
    else
    {
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = ExpressiveTraits::kSFScalarPropertiesDefault[scal_prop_ids[i]];
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = ExpressiveTraits::kSFVectorPropertiesDefault[vect_prop_ids[i]];
        }
    }
}

/** \copydoc Convol::BlobtreeNodeT::PrepareForEval(const Scalar , const Scalar,  std::map<BlobtreeNodeT*, std::pair<AABBox, AABBox> >& )
 *
 *  \param blobtree_node Node to prepare for evaluation. It will be prepared as if the scalar fields of its children are blended using a BlendSumT.
 */
void BlendBulge::PrepareForEval(BlobtreeNode& blobtree_node,
                    const Scalar epsilon_bbox, const Scalar accuracy_needed,
                    std::map<BlobtreeNode*, std::pair<AABBox, AABBox> >& updated_areas)
{
    if(blobtree_node.epsilon_bbox() != epsilon_bbox)
    {
        Scalar scal_n = (Scalar) blobtree_node.children().size();
        AABBox bt_node_abb = AABBox(); // this auxilliary is there to avoid calling set_axis_bounding_box which implies calls to node listeners.
        for(typename std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = blobtree_node.nonconst_children().begin(); it != blobtree_node.nonconst_children().end(); ++it)
        {
            assert( (*it)!=NULL );
            (*it)->PrepareForEval(epsilon_bbox/scal_n, accuracy_needed, updated_areas);
            bt_node_abb = AABBox::Union(bt_node_abb, (*it)->axis_bounding_box());
        }

        blobtree_node.set_axis_bounding_box(bt_node_abb);
        blobtree_node.set_epsilon_bbox(epsilon_bbox);
    }
}

/////////////////////////////
// BoundingBox computation //
/////////////////////////////


//    virtual AABBox GetAxisBoundingBox(const std::vector<BlobtreeNode*>& blobtree_node_vector, const Scalar value) const
AABBox BlendBulge::GetAxisBoundingBox(const std::vector<std::shared_ptr<BlobtreeNode> >& blobtree_node_vector, Scalar value) const
{
    AABBox res = AABBox();
    for(typename std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = blobtree_node_vector.begin(); it!=blobtree_node_vector.end(); ++it)
    {
        res = AABBox::Union(res,(*it)->GetAxisBoundingBox(value/((Scalar) blobtree_node_vector.size())));
    }
    return res;
}

bool operator==(const BlendBulge& /*a*/, const BlendBulge& /*b*/)
{
    return true;
}

static core::FunctorFactory::Type<BlendBulge> BlendBulgeType;

} // namespace convol

} // namespace expressive
