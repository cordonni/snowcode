#include <convol/functors/noises/ControllableGaborSurfaceNoise.h>

#include <core/utils/BasicsToXml.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

////////////////////////////////////////////////
/// ControllableGaborSurfaceNoise Parameters ///
////////////////////////////////////////////////

void ControllableGaborSurfaceNoiseParameters::AddXmlAttributes(TiXmlElement * element) const
{
    core::Functor::AddXmlAttributes(element);

    element->SetDoubleAttribute("bandwith", bandwith_wrt_Fs_*F_scale_);
    element->SetDoubleAttribute("F_min", F_scale_ * (1.0 - F_half_width_wrt_Fs_));
    element->SetDoubleAttribute("F_max", F_scale_ * (1.0 + F_half_width_wrt_Fs_));

    element->SetDoubleAttribute("w0_mean",get_w0_mean());
//    TiXmlElement * element_wmd = core::PointToXml(w0_main_dir_, "w0_main_dir");
//    element->LinkEndChild(element_wmd);
    element->SetDoubleAttribute("w0_half_opening",w0_half_opening_);
}

void ControllableGaborSurfaceNoiseParameters::InitFromXml(const TiXmlElement* element)
{
    double bandwith;
    core::TraitsXmlQuery::QueryDoubleAttribute("ControllableGaborSurfaceNoiseParameters::InitFromXml",
                                               element, "bandwith", &bandwith);

    double F_min, F_max;
    core::TraitsXmlQuery::QueryDoubleAttribute("ControllableGaborSurfaceNoiseParameters::InitFromXml",
                                               element, "F_min", &F_min);
    core::TraitsXmlQuery::QueryDoubleAttribute("ControllableGaborSurfaceNoiseParameters::InitFromXml",
                                               element, "F_max", &F_max);

    set_F_scale(0.5*(F_min+F_max));
    set_bandwith_wrt_Fs(bandwith/F_scale_);
    set_F_half_width_wrt_Fs( 0.5*(F_max-F_min) / F_scale_ );

//    Point2 w0_main_dir = Point2::UnitX();
//    core::TraitsXmlQuery::QueryPointAttribute("ControllableGaborSurfaceNoise::InitFromXml",
//                                              element, "w0_main_dir", w0_main_dir);
    double w0_mean;
    core::TraitsXmlQuery::QueryDoubleAttribute("ControllableGaborSurfaceNoise::InitFromXml",
                                               element, "w0_mean", &w0_mean);
    double w0_half_opening;
    core::TraitsXmlQuery::QueryDoubleAttribute("ControllableGaborSurfaceNoise::InitFromXml",
                                               element, "w0_half_opening", &w0_half_opening);
    set_w0(w0_mean, w0_half_opening);
}

/////////////////////////////////////
/// ControllableGaborSurfaceNoise ///
/////////////////////////////////////

void ControllableGaborSurfaceNoise::AddXmlAttributes(TiXmlElement * element) const
{
    core::Functor::AddXmlAttributes(element);

    element->SetDoubleAttribute("impulse",impulse_);
    element->SetDoubleAttribute("error",error_);
    element->SetAttribute("seed",seed_);
}

void ControllableGaborSurfaceNoise::InitFromXml(const TiXmlElement* element)
{
    double impulse;
    core::TraitsXmlQuery::QueryDoubleAttribute("ControllableGaborSurfaceNoise::InitFromXml",
                                               element, "impulse", &impulse);
    set_impulse(impulse);

    double error;
    core::TraitsXmlQuery::QueryDoubleAttribute("ControllableGaborSurfaceNoise::InitFromXml",
                                               element, "error", &error);
    set_error(error);

    int seed;
    core::TraitsXmlQuery::QueryIntAttribute("ControllableGaborSurfaceNoise::InitFromXml",
                                            element, "seed", &seed);
    set_seed(seed);
}


// Register functor in factory
static core::FunctorFactory::Type<ControllableGaborSurfaceNoiseParameters> ControllableGaborSurfaceNoiseParametersType;
static core::FunctorFactory::Type<ControllableGaborSurfaceNoise> ControllableGaborSurfaceNoiseType;

} // Close namespace convol

} // Close namespace expressive


