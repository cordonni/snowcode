#include <convol/functors/noises/PolynomialDeformationMap.h>

#include <core/xmlloaders/TraitsXmlQuery.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

void PolynomialDeformationMap::InitFromXml(const TiXmlElement* element)
{
    double deform_0_0, deriv_0_0;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "deform_0_0", &deform_0_0);
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "deriv_0_0", &deriv_0_0);
    set_constraint_0_0(deform_0_0, deriv_0_0);

    double deform_0_25, deriv_0_25;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "deform_0_25", &deform_0_25);
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "deriv_0_25", &deriv_0_25);
    set_constraint_0_25(deform_0_25, deriv_0_25);

    double deform_0_5, deriv_0_5;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "deform_0_5", &deform_0_5);
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "deriv_0_5", &deriv_0_5);
    set_constraint_0_5(deform_0_5, deriv_0_5);

    double deform_0_75, deriv_0_75;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "deform_0_75", &deform_0_75);
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "deriv_0_75", &deriv_0_75);
    set_constraint_0_75(deform_0_75, deriv_0_75);

    double deform_1_0, deriv_1_0;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "deform_1_0", &deform_1_0);
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "deriv_1_0", &deriv_1_0);
    set_constraint_1_0(deform_1_0, deriv_1_0);
}

bool operator==(const PolynomialDeformationMap& a, const PolynomialDeformationMap& b)
{
    return  (a.deform_0_0()  == b.deform_0_0())   &&
            (a.deriv_0_0()   == b.deriv_0_0())    &&
            (a.deform_0_25() == b.deform_0_25())  &&
            (a.deriv_0_25()  == b.deriv_0_25())   &&
            (a.deform_0_5()  == b.deform_0_5())   &&
            (a.deriv_0_5()   == b.deriv_0_5())    &&
            (a.deform_0_75() == b.deform_0_75())  &&
            (a.deriv_0_75()  == b.deriv_0_75())   &&
            (a.deform_1_0()  == b.deform_1_0())   &&
            (a.deriv_1_0()   == b.deriv_1_0());
}

// Register functor in factory
static core::FunctorFactory::Type<PolynomialDeformationMap> PolynomialDeformationMapType;

} // Close namespace convol

} // Close namespace expressive


