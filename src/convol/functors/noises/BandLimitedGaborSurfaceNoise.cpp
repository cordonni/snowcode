#include <convol/functors/noises/BandLimitedGaborSurfaceNoise.h>

#include <core/utils/BasicsToXml.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {


void BandLimitedGaborSurfaceNoise::AddXmlAttributes(TiXmlElement * element) const
{
    core::Functor::AddXmlAttributes(element);

    element->SetDoubleAttribute("a",a_);
    element->SetDoubleAttribute("F0_min",F0_min_);
    element->SetDoubleAttribute("F0_max",F0_max_);

    element->SetDoubleAttribute("w0_mean",get_w0_mean());
//    TiXmlElement * element_wmd = core::PointToXml(w0_main_dir_, "w0_main_dir");
//    element->LinkEndChild(element_wmd);

    element->SetDoubleAttribute("w0_half_opening",w0_half_opening_);

    element->SetDoubleAttribute("lambda",lambda_);
    element->SetDoubleAttribute("r",r_);

    element->SetAttribute("seed",seed_);
}

void BandLimitedGaborSurfaceNoise::InitFromXml(const TiXmlElement* element)
{
    double lambda;
    core::TraitsXmlQuery::QueryDoubleAttribute("BandLimitedGaborSurfaceNoise::InitFromXml",
                                               element, "lambda", &lambda);
    set_lambda(lambda);

    double r;
    core::TraitsXmlQuery::QueryDoubleAttribute("BandLimitedGaborSurfaceNoise::InitFromXml",
                                               element, "r", &r);
    set_r(r);

    double a;
    core::TraitsXmlQuery::QueryDoubleAttribute("BandLimitedGaborSurfaceNoise::InitFromXml",
                                               element, "a", &a);
    set_a(a);

    double F0_min, F0_max;
    core::TraitsXmlQuery::QueryDoubleAttribute("BandLimitedGaborSurfaceNoise::InitFromXml",
                                               element, "F0_min", &F0_min);
    core::TraitsXmlQuery::QueryDoubleAttribute("BandLimitedGaborSurfaceNoise::InitFromXml",
                                               element, "F0_max", &F0_max);
    set_F0(F0_min, F0_max);

//    Point2 w0_main_dir = Point2::UnitX();
//    core::TraitsXmlQuery::QueryPointAttribute("BandLimitedGaborSurfaceNoise::InitFromXml",
//                                              element, "w0_main_dir", w0_main_dir);
    double w0_mean;
    core::TraitsXmlQuery::QueryDoubleAttribute("BandLimitedGaborSurfaceNoise::InitFromXml",
                                               element, "w0_mean", &w0_mean);
    double w0_half_opening;
    core::TraitsXmlQuery::QueryDoubleAttribute("BandLimitedGaborSurfaceNoise::InitFromXml",
                                               element, "w0_half_opening", &w0_half_opening);
    set_w0(w0_mean, w0_half_opening);

    int seed;
    core::TraitsXmlQuery::QueryIntAttribute("BandLimitedGaborSurfaceNoise::InitFromXml",
                                            element, "seed", &seed);
    set_seed(seed);
}


// Register functor in factory
static core::FunctorFactory::Type<BandLimitedGaborSurfaceNoise> BandLimitedGaborSurfaceNoiseType;

} // Close namespace convol

} // Close namespace expressive


