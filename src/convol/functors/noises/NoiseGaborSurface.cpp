#include <convol/functors/noises/NoiseGaborSurface.h>

#include <core/xmlloaders/TraitsXmlQuery.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {


void NoiseGaborSurface::InitFromXml(const TiXmlElement* element)
{
    double lambda;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "lambda", &lambda);
    set_lambda(lambda);

    double r;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "r", &r);
    set_r(r);

    double a;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "a", &a);
    set_a(a);

    double F0, w0;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "F0", &F0);
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "w0", &w0);
    set_F0_w0(F0, w0);

    int seed;
    core::TraitsXmlQuery::QueryIntAttribute("NodeProceduralDetailT::InitFromXml",
                                            element, "seed", &seed);
    set_seed(seed);

    int isotropic;
    core::TraitsXmlQuery::QueryIntAttribute("NodeProceduralDetailT::InitFromXml",
                                            element, "isotropic", &isotropic);
    set_isotropic((isotropic!=0)); // assume that if different of zero correspond to true
}


// Register functor in factory
static core::FunctorFactory::Type<NoiseGaborSurface> NoiseGaborSurfaceType;

} // Close namespace convol

} // Close namespace expressive


