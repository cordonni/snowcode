/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: ControllableGaborSurfaceNoise.h

 Language: C++

 License: expressive license and "SEE BELOW"

 \author: Cédric Zanni
 E-Mail:  cedric.zanni@inria.fr

 Description:

 IMPORTANT NOTICE : this code as been writen from the sample example code
 associtated to the paper :
     Procedural Noise using Sparse Gabor Convolution
     Ares Lagae, Sylvain Lefebvre, George Drettakis and Philip Dutre
     SIGGRAPH 2009
 The associated license is given below.

 The random phase present in the provided code as been removed, because it break
 the "symmetry" of anisotropoy.
 */


/* Copyright (c) 2009 by Ares Lagae, Sylvain Lefebvre,
 * George Drettakis and Philip Dutre
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef CONTROLLABLE_GABOR_SURFACE_NOISE_H
#define CONTROLLABLE_GABOR_SURFACE_NOISE_H

// core dependencies
#include <core/CoreRequired.h>
#include<core/functor/Functor.h>
#include<core/functor/MACRO_FUNCTOR_TYPE.h>

// Convol dependencies
#include <convol/ConvolRequired.h>
#include <convol/functors/noises/PseudoRandomNumberGenerator.h>

#include "core/VectorTraits.h"
#include "commons/EigenVectorTraits.h"

#define SMART_ANGLE_INTERPOLATION
#define SPATIAL_INTERPOLATION

#define SMALL_GRID

namespace expressive {

namespace convol {

/**
 * @brief The ControllableGaborSurfaceNoiseParameters class regroup the parameter
 * of Gabor Noise with controllable band limit that can vary in space (opposed
 * to constant parameter such as impulse density)
 */
class CONVOL_API ControllableGaborSurfaceNoiseParameters : public core::Functor
{
public:
    EXPRESSIVE_MACRO_NAME("ControllableGaborSurfaceNoiseParameters")

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    ControllableGaborSurfaceNoiseParameters(Scalar F_scale = 0.1,
                                            Scalar F_half_width_wrt_Fs = 0.1,
                                            Scalar bandwith_wrt_Fs = 0.1,
                                            Point2 w0_main_dir = Point2::UnitX(), Scalar w0_half_opening = 0.1)
        : F_scale_(F_scale),
          F_half_width_wrt_Fs_(F_half_width_wrt_Fs), bandwith_wrt_Fs_(bandwith_wrt_Fs),
          w0_main_dir_(w0_main_dir), w0_half_opening_(w0_half_opening)
    {}

    virtual ~ControllableGaborSurfaceNoiseParameters(){}

    //////////////////////
    /// Type functions ///
    //////////////////////

    static core::FunctorTypeTree BuildStaticType()
    {
        std::vector<core::FunctorTypeTree> vec;
        return core::FunctorTypeTree(StaticName(), vec);
    }
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defines functions

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const;

    virtual void InitFromXml(const TiXmlElement* element);

    /////////////////

    //! return the bandwith
    inline Scalar get_bandwith() const
    {
        return bandwith_wrt_Fs_*F_scale_;
    }
    inline Scalar get_F_min() const
    {
        return F_scale_ * (1.0 - F_half_width_wrt_Fs_);
    }
    inline Scalar get_F_max() const
    {
        return F_scale_ * (1.0 + F_half_width_wrt_Fs_);
    }

    inline Scalar get_w0_mean() const
    {
        Scalar cos_m = w0_main_dir_.dot(Point2::UnitX());
        Scalar m = std::acos(std::max(std::min(cos_m, 1.0), -1.0)); // only work between 0 and pi
        if(w0_main_dir_.dot(Point2::UnitY()) >= 0.0)
            return m;
        else
            return -m;
    }

    /////////////////
    /// Accessors ///
    /////////////////

    inline Scalar F_scale() const { return F_scale_; }

    inline Scalar F_half_width_wrt_Fs() const { return F_half_width_wrt_Fs_; }
    inline Scalar bandwith_wrt_Fs() const { return bandwith_wrt_Fs_; }

    inline const Point2& w0_main_dir() const { return w0_main_dir_; }
    inline Scalar w0_half_opening() const { return w0_half_opening_; }

    /////////////////
    /// Modifiers ///
    /////////////////

    inline void set_F_scale(Scalar F_scale) { F_scale_ = F_scale; }
    inline void set_F_half_width_wrt_Fs(Scalar F_half_width_wrt_Fs) { F_half_width_wrt_Fs_ = F_half_width_wrt_Fs; }
    inline void set_bandwith_wrt_Fs(Scalar bandwith_wrt_Fs) { bandwith_wrt_Fs_ = bandwith_wrt_Fs; }

    inline void set_w0(const Point2& w0_main_dir, Scalar w0_half_opening)
    {
        w0_main_dir_ = w0_main_dir;
        w0_half_opening_ = w0_half_opening;
    }
    inline void set_w0(Scalar w0_mean, Scalar w0_half_opening)
    {
        set_w0(Point2(cos(w0_mean), sin(w0_mean)), w0_half_opening);
    }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    Scalar F_scale_;  //! main frequency of the noise, changing this parameter scale inversely the noise pattern
    // this is also equal to 0.5*(F_max+F_min)

    Scalar F_half_width_wrt_Fs_; //! we have 0.5*(F_max-F_min) = F_half_width_wrt_Fs_ * F_scale_
    Scalar bandwith_wrt_Fs_; //! we have bandwith = bandwith_wrt_Fs_ * F_scale_

    Point2 w0_main_dir_; //! unit vector corresponding to the main direction of anisotropy (w0_mean)
    Scalar w0_half_opening_;   //! variance of direction of anisotropy,can vary between epsilon and pi
};

/**
 * @brief The ControllableGaborSurfaceNoise class is a functor that can compute Gabor noise from
 * arbitrary noise parameter with fixed impulse density and precision.
 */
class CONVOL_API ControllableGaborSurfaceNoise : public core::Functor
{
public:
    EXPRESSIVE_MACRO_NAME("ControllableGaborSurfaceNoise")

    typedef ControllableGaborSurfaceNoiseParameters NoiseParameters;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    ControllableGaborSurfaceNoise(Scalar impulse = 20.0, Scalar error = 0.05, unsigned int seed = 1234)
        : impulse_(impulse), error_(error), seed_(seed)
    {}

    virtual ~ControllableGaborSurfaceNoise() {}

    //////////////////////
    /// Type functions ///
    //////////////////////

    static core::FunctorTypeTree BuildStaticType()
    {
        std::vector<core::FunctorTypeTree> vec;
        return core::FunctorTypeTree(StaticName(), vec);
    }
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defines functions

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const;

    virtual void InitFromXml(const TiXmlElement* element);

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    Scalar operator()(const Point& p, const Vector& n, const Vector& t, const Vector& b,
                      const ControllableGaborSurfaceNoiseParameters& param)
            const
    {
        Scalar noise;
        Vector grad;
        EvalValueAndGrad(p,n,t,b,
                         param.F_scale(), param.F_half_width_wrt_Fs(), param.bandwith_wrt_Fs(),
                         param.w0_main_dir(), param.w0_half_opening(),
                         noise, grad);
        return noise;
    }


    void EvalValueAndGrad(const Point& p, const Vector& n, const Vector& t, const Vector& b,
                          Scalar F_scale, Scalar F_half_width_wrt_Fs, Scalar bandwith_wrt_Fs,
                          const Point2& w0_main_dir, Scalar w0_half_opening,
                          Scalar& noise_value, Vector& noise_grad) const
    {
        Scalar F_min = F_scale * (1.0 - F_half_width_wrt_Fs);
        Scalar F_max = F_scale * (1.0 + F_half_width_wrt_Fs);
        Scalar bandwith = bandwith_wrt_Fs*F_scale;

        Scalar r = sqrt(-log(error_)/M_PI)  / bandwith; // optimal grid size in function of bandwith
        Scalar r_i = exp2(floor(log2(r)));
        Scalar r_m = r_i * sqrt(2.0); // mult by sqrt(2) come from "cylindrical shape of kernel"
#ifndef SMALL_GRID
        Scalar r_0 = r_m;
#else
        Scalar r_0 = 0.5 * r_m; // change grid size to avoid useless eval
#endif
        Scalar r_1 = 2.0*r_0;
        Scalar u = (r/r_i) - 1.0;
        Scalar w0 = (1.0-u) / pow(1.0+u,2.0);
        Scalar w1 = (4.0*u) / pow(1.0+u,2.0);
        Point p_g_0 = (1.0/r_0) * p;
        Point p_g_1 = (1.0/r_1) * p;

        Scalar noise0, noise1;
        Vector grad0, grad1;
#ifndef SMALL_GRID
        Grid<2>(r, r_0, p_g_0, n, t, b,
                F_min, F_max, bandwith,
                w0_main_dir, w0_half_opening,
                noise0, grad0);
        Grid<1>(r, r_1, p_g_1, n, t, b,
                F_min, F_max, bandwith,
                w0_main_dir, w0_half_opening,
                noise1, grad1);
#else
        Grid<4>(r, r_0, p_g_0, n, t, b,
                F_min, F_max, bandwith,
                w0_main_dir, w0_half_opening,
                noise0, grad0);
        Grid<2>(r, r_1, p_g_1, n, t, b,
                F_min, F_max, bandwith,
                w0_main_dir, w0_half_opening,
                noise1, grad1);
#endif
        Scalar noise = w0 * noise0 + w1 * noise1;
        Vector grad = w0 * grad0 + w1 * grad1;

        //r_i ou r_m dans le lambda ???
        Scalar lambda = impulse_ / (M_PI*(r_i*r_i));
//        Scalar lambda = impulse_ / (M_PI*(r_m*r_m));
        Scalar variance = ((w0*w0)+(w1*w1)/4.0) * lambda * (1.0/3.0)  / (4.0*bandwith*bandwith); //TODO:@todo : to be confirmed...

        Scalar scale = 0.5/(/*3.0*/ 2.0*sqrt(variance)); // not sure where 3.0 come from, nore the new 2.0 ...
        noise_value = scale*noise + 0.5; // not sure where 3.0 come from, nore the new 2.0 ...
        noise_grad = scale*grad; //TODO:@todo : perhaps have to do a change of frame... grad along normal 0.0 because undefined out of surface...
    }

    /*!
     * \brief ComputeInterpolatedNoise this function compute an noise value corresponding to an interpolation
     * of a set of noise parameters that are stored inside vec_param and are accessed via the function
     * or lambda function access.
     *
     * Accessor can be either a function pointer with the signature :
     *      const ControllableGaborSurfaceNoiseParameters& (*accessor) (const TVectorParameter&, int)
     * or a lambda function :
     *      [](const TVectorParameter&, int i) -> const ControllableGaborSurfaceNoiseParameters&
     *
     * TODO:@todo : find a mecanism to choose which kind of noise evaluation should be done
     */
    template <typename TVectorParameter, typename Accessor>
    void ComputeInterpolatedNoiseAndGrad(const Point& p, const Vector& n, const Vector& t, const Vector& b,
                                         const std::vector<Scalar>& vec_field_value, Scalar global_field,
                                         const TVectorParameter& vec_param,
                                         Accessor access,
                                         Scalar& noise_value, Vector& noise_grad
                                         ) const
    {
        Scalar local_F_scale = 0.0;
        Scalar local_F_half_width_wrt_Fs = 0.0;
        Scalar local_bandwith_wrt_Fs = 0.0;
        Scalar local_w0_half_opening = 0.0;

#ifdef SMART_ANGLE_INTERPOLATION
        Eigen::Matrix2d W = Eigen::Matrix2d::Zero();
#else
        Scalar local_w0_mean = 0.0;
#endif
Scalar local_scale = 0.0;
        int index = 0;
        for(Scalar field_value : vec_field_value) {
            const auto& param = access(vec_param, index);

local_scale += field_value / param.F_scale();
            local_F_scale += field_value * param.F_scale();
            local_F_half_width_wrt_Fs += field_value * param.F_half_width_wrt_Fs();
            local_bandwith_wrt_Fs += field_value * param.bandwith_wrt_Fs();

            local_w0_half_opening += field_value * param.w0_half_opening();
    #ifdef SMART_ANGLE_INTERPOLATION
    // the sqrt factor enable a smoother transition between node parameter
            EigenVectorTraits::Vector2d w0_main_dir_eigen(param.w0_main_dir()[0], param.w0_main_dir()[1]);
            W.noalias() += sqrt(field_value) * w0_main_dir_eigen * w0_main_dir_eigen.transpose();
    #else
            local_w0_mean += field_value * param.get_w0_mean();
    #endif

            ++index;
        }


        local_F_scale /= global_field;  // div by \sum_{i} vec_field_value[i]
#ifdef SPATIAL_INTERPOLATION
        local_scale /= global_field;
        local_F_scale = 1.0/ local_scale;
//        local_F_scale = 0.5*local_F_scale + 0.5/local_scale;
#endif
        local_F_half_width_wrt_Fs /= global_field;
        local_bandwith_wrt_Fs /= global_field;
        local_w0_half_opening /= global_field;
    #ifdef SMART_ANGLE_INTERPOLATION
        // NB : W should be symetric
        W *= 1.0/global_field;

        Scalar diff_diag = W(0,0)-W(1,1);
        Scalar delta = diff_diag*diff_diag + 4*W(0,1)*W(1,0); // discriminant of the ??? polynomial

        Scalar sqrt_delta = sqrt(delta); // this is the distance between the two eigenvalues

        Vector2 local_w0_dir(0.5*(diff_diag + sqrt_delta), W(0,1)); // correspond to main eigenvector
        local_w0_dir.normalize(); // Should check if it is possible that the norm before normalization tend toward zero...

        const Scalar limit = 0.7;
        if(sqrt_delta < limit) {
            assert(local_w0_dir.dot(Vector2::UnitX())>=0.0); //TODO:@todo : vector should not be in opposite direction otherwise : BOOM !
            Scalar x = sqrt_delta/limit;
            //Scalar b = x; // linear interpolation
            Scalar b = x*x*(3.0-2.0*x); // cubic interpolation

            local_w0_half_opening = (1.0-b) * (0.5*M_PI) + b * local_w0_half_opening;

            //TODO:@todo : it should be better to introduce an additionnal constraint in the system rather than using a fixed direction and interpolate.
            //TODO:@todo : try to find something better than UnitX ...
    //        local_w0_dir = (1.0-b) * Vector2::UnitX() + b * local_w0_dir;
    //        local_w0_dir.normalize();

            // this should be sufficient to guarantee a new direction
            W.noalias() += (1.0-b) * EigenVectorTraits::Vector2d::UnitX() * EigenVectorTraits::Vector2d::UnitX().transpose();
            Scalar diff_diag = W(0,0)-W(1,1);
            Scalar delta = diff_diag*diff_diag + 4*W(0,1)*W(1,0); // discriminant of the ??? polynomial
            Scalar sqrt_delta = sqrt(delta); // this is the distance between the two eigenvalues
            Vector2 local_w0_dir(0.5*(diff_diag + sqrt_delta), W(0,1)); // correspond to main eigenvector
            local_w0_dir.normalize(); // Should check if it is possible that the norm before normalization tend toward zero...
        }
    #else
        local_w0_mean /= global_field;
        //TODO:@todo : should deduce w0_dir ...
    #endif

        EvalValueAndGrad(p, n, t, b,
                         local_F_scale, local_F_half_width_wrt_Fs, local_bandwith_wrt_Fs,
                         local_w0_dir, local_w0_half_opening,
                         noise_value, noise_grad);
    }

private:
    inline void Sample(PseudoRandomNumberGenerator& prng,
                       Scalar F_min, Scalar F_max,
                       Scalar w0_half_opening,
                       Scalar& Fs_r, Scalar& w0_r) const
    {
        Fs_r = prng.uniform(F_min, F_max);
        w0_r = prng.uniform(-w0_half_opening, w0_half_opening);
        //phi_r = prng.uniform(0, 2.0*M_PI);
        ////WARNING : I do not use random phase anymore because it break the "symmetry" of the anisotropy
        ////WARNING : if phi is used, then the random part of w_i is useless (does not change the probability)
    }

    void Cell(Scalar kernel_radius, Scalar r,
                const Vector3i& c, const Point& p_c, const Vector& n, const Vector& t, const Vector& b,
                Scalar F_min, Scalar F_max, Scalar bandwith,
                const Point2& w0_main_dir, Scalar w0_half_opening,
                Scalar& noise, Vector2& grad
                ) const
    {
        // Init random number generator
        const unsigned p = 256u;
        unsigned s = (((((unsigned(c[2]) % p) * p) + (unsigned(c[1]) % p)) * p) + (unsigned(c[0]) % p)) + seed_;
        if (s == 0) s = 1;
        PseudoRandomNumberGenerator prng(s);

        Scalar sqr_bandwith = bandwith*bandwith;

        noise = 0.0;
        grad = Vector2::Zero();
#ifndef SMALL_GRID
//        unsigned n_ = prng.poisson( impulse_ * (r/kernel_radius) / (2.0 * M_PI) ); //this is the theorical one but is problematic !
        unsigned n_ = prng.poisson( sqrt(2.0) * impulse_ / (2.0 * M_PI) );
//        unsigned n_ = prng.poisson( impulse_ / (2.0 * M_PI) );
#else
        unsigned n_ = prng.poisson( sqrt(2.0) * impulse_ / (16.0 * M_PI) );
#endif
        for (unsigned i = 0u; i < n_; ++i) {
            Point p_i_c(prng.uniform_0_1(), prng.uniform_0_1(), prng.uniform_0_1());
            Point tmp = r*(p_c-p_i_c);
            Point x_k_i( t.dot(tmp), b.dot(tmp), n.dot(tmp) ); //TODO:@todo : the change of frame should be computed before as a matrix, it would avoid the tmp vector
            // NOTE: assumes t, b and n are orthogonal

            Scalar Fs_r, w0_r;
            Sample(prng, F_min, F_max, w0_half_opening, Fs_r, w0_r);

            Scalar norm_d2 = ((x_k_i[0] * x_k_i[0]) + (x_k_i[1] * x_k_i[1]));
            Scalar abs_dist_p = 1.0-std::abs(x_k_i[2] / kernel_radius);
            Scalar w_i = (prng.uniform_bool() ? -1.0 : +1.0) * abs_dist_p; // cf comment in sample function

            if (( norm_d2 < (kernel_radius * kernel_radius)) && abs_dist_p > 0.0)
            {
//                Scalar w_i = abs_dist_p; // if phi is used (cf comment in sample function)

                // Gabor kernel evaluation

                //TODO:@todo : this small trick on the cos ans sin could be removed by pre-aligning the frame to the main direction of
                // anisotropy (this can be done efficiently without any trigonometric function)
                Scalar cos_w = std::cos(w0_r);
                Scalar sin_w = std::sin(w0_r);
                Scalar cos = cos_w * w0_main_dir[0] - sin_w * w0_main_dir[1];
                Scalar sin = sin_w * w0_main_dir[0] + cos_w * w0_main_dir[1];

                Scalar g = std::exp(-M_PI * sqr_bandwith * norm_d2); //TODO:@todo : replace by a compact polynomial kernel
                Scalar phase = (2.0 * M_PI * Fs_r * ((cos * x_k_i[0]) + (sin * x_k_i[1]))) /*+ phi_r*/;
                Scalar h = std::cos(phase); // the inside cos-sin is an alignement to a direction, their should be way to avoid it
                Scalar h_p = std::sin(phase);
                noise += w_i * g * h;
                grad += (-2.0 * M_PI * w_i * g) * ((sqr_bandwith*h)*Vector2(x_k_i[0],x_k_i[1]) + (Fs_r*h_p) * Vector2(cos,sin));
                //TODO:@todo : compute the gradient : check the formula again !!!!
            }
        }
    }


    template<int I>
    void Grid(Scalar kernel_radius, Scalar r, const Point& p_g, const Vector& n, const Vector& t, const Vector& b,
              Scalar F_min, Scalar F_max, Scalar bandwith,
              const Point2& w0_main_dir, Scalar w0_half_opening,
              Scalar& noise, Vector& grad
              ) const
    {
        Point int_p_g = Point(floor(p_g[0]),floor(p_g[1]),floor(p_g[2]));//p_g_e.unaryExpr(std::ptr_fun<Scalar, Scalar>(floor)); // real index of cell
        Point p_c = p_g-int_p_g; // local coordinate in cell (in [0;1[ )
        Vector3i c = int_p_g.cast<int>(); // integer index of cell

#ifndef NO_CLIPPING
        Scalar ratio = kernel_radius/r;
        // cylinder clipping
        Scalar dx = ratio * (fabs(n[0]) + sqrt(1.0 - n[0]*n[0])) ;
        Scalar dy = ratio * (fabs(n[1]) + sqrt(1.0 - n[1]*n[1])) ;
        Scalar dz = ratio * (fabs(n[2]) + sqrt(1.0 - n[2]*n[2])) ;
//        // sphere clipping
//        Scalar dx = sqrt(2.0)*ratio;
//        Scalar dy = sqrt(2.0)*ratio;
//        Scalar dz = sqrt(2.0)*ratio;
        int ix_m = ((int)(p_c[0]-dx)) - 1;
        int iy_m = ((int)(p_c[1]-dy)) - 1;
        int iz_m = ((int)(p_c[2]-dz)) - 1;
        int ix_M = (int)(p_c[0]+dx);
        int iy_M = (int)(p_c[1]+dy);
        int iz_M = (int)(p_c[2]+dz);
#else
        int ix_m = -I, iy_m = -I, iz_m = -I;
        int ix_M = I, iy_M = I, iz_M = I;
#endif
//        std::cout<< "I="<<I<< "pourcent :  " <<((Scalar)(ix_M-ix_m+1)*(iy_M-iy_m+1)*(iz_M-iz_m+1))/((Scalar)((2*I+1)*(2*I+1)*(2*I+1))) << " of " << ((2*I+1)*(2*I+1)*(2*I+1)) << std::endl;

        Scalar val_tmp; Vector2 grad_tmp;
        noise = 0.0;
        Vector2 grad2D = Vector2::Zero();
        Vector3i i;
        for (i[2] = iz_m; i[2] <= iz_M; ++i[2]) {
            for (i[1] = iy_m; i[1] <= iy_M; ++i[1]) {
                for (i[0] = ix_m; i[0] <= ix_M; ++i[0]) {
                    //TODO:@todo : test intersection individual cell with cylinder could save a bit more time
                    Vector3i c_i = c + i;
                    Point p_c_i = p_c - i.cast<Scalar>(); // coordinate of point in new cell
                    Cell(kernel_radius, r,
                         c_i, p_c_i, n, t, b,
                         F_min, F_max, bandwith,
                         w0_main_dir, w0_half_opening,
                         val_tmp, grad_tmp);
                    noise += val_tmp;
                    grad2D += grad_tmp;
                }
            }
        }
        grad = grad2D[0]*t + grad2D[1]*b;
        //TODO:@todo : currently assume that "details" are small enough in comparison to surface curvature
    }

public:

    /////////////////
    /// Accessors ///
    /////////////////

    inline Scalar impulse() const { return impulse_; }
    inline Scalar error() const { return error_; }
    inline unsigned int seed() const { return seed_; }

    /////////////////
    /// Modifiers ///
    /////////////////

    inline void set_impulse(Scalar impulse){ impulse_ = impulse; }
    inline void set_error(Scalar error){ error_ = error; }
    inline void set_seed(unsigned int seed){ seed_ = seed; }

    /////////////////
    /// Attributs ///
    /////////////////

private :
    Scalar impulse_;    //! number of impulse per kernel area
    Scalar error_;      //! error on the gaussian
    unsigned seed_;     //! offset to initialize the seed of the prng
};

} // Close namespace convol

} // Close namespace expressive

#endif // GABOR_SURFACE_H
