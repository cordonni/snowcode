/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#pragma once
#ifndef CONVOL_FUNCTOR_POLYNOMIAL_DEFORMATION_MAP_H_
#define CONVOL_FUNCTOR_POLYNOMIAL_DEFORMATION_MAP_H_

// core dependencies
#include <core/CoreRequired.h>
#include<core/functor/Functor.h>
#include<core/functor/MACRO_FUNCTOR_TYPE.h>

// Convol dependencies
#include <convol/ConvolRequired.h>

#include <algorithm>

namespace expressive {

namespace convol {

class PolynomialDeformationMap : public core::Functor
{
public:
    EXPRESSIVE_MACRO_NAME("PolynomialDeformationMap")

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    PolynomialDeformationMap()
        : deform_0_0_(-1.0), deriv_0_0_(2.0),
          deform_0_25_(-0.5), deriv_0_25_(2.0),
          deform_0_5_(0.0), deriv_0_5_(2.0),
          deform_0_75_(0.5), deriv_0_75_(2.0),
          deform_1_0_(1.0), deriv_1_0_(2.0)
    {
        UpdateLimits();
    }

    PolynomialDeformationMap(Scalar deform_0_0, Scalar deriv_0_0, Scalar deform_0_25, Scalar deriv_0_25,
                             Scalar deform_0_5, Scalar deriv_0_5, Scalar deform_0_75, Scalar deriv_0_75,
                             Scalar deform_1_0, Scalar deriv_1_0)
        : deform_0_0_(deform_0_0), deriv_0_0_(deriv_0_0),
          deform_0_25_(deform_0_25), deriv_0_25_(deriv_0_25),
          deform_0_5_(deform_0_5), deriv_0_5_(deriv_0_5),
          deform_0_75_(deform_0_75), deriv_0_75_(deriv_0_75),
          deform_1_0_(deform_1_0), deriv_1_0_(deriv_1_0)
    {
        UpdateLimits();
    }

    PolynomialDeformationMap(const PolynomialDeformationMap& rhs)
        : deform_0_0_(rhs.deform_0_0_), deriv_0_0_(rhs.deriv_0_0_),
          deform_0_25_(rhs.deform_0_25_), deriv_0_25_(rhs.deriv_0_25_),
          deform_0_5_(rhs.deform_0_5_), deriv_0_5_(rhs.deriv_0_5_),
          deform_0_75_(rhs.deform_0_75_), deriv_0_75_(rhs.deriv_0_75_),
          deform_1_0_(rhs.deform_1_0_), deriv_1_0_(rhs.deriv_1_0_),
          d_min_(rhs.d_min_), d_max_(rhs.d_max_) ,
          l_min_(rhs.l_min_), l_max_(rhs.l_max_) {}

    //////////////////////
    /// Type functions ///
    //////////////////////

    static core::FunctorTypeTree BuildStaticType()
    {
        std::vector<core::FunctorTypeTree> vec;
        return core::FunctorTypeTree(StaticName(), vec);
    }
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defines functions

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        core::Functor::AddXmlAttributes(element);

        element->SetDoubleAttribute("deform_0_0",deform_0_0_);
        element->SetDoubleAttribute("deriv_0_0",deriv_0_0_);

        element->SetDoubleAttribute("deform_0_25",deform_0_25_);
        element->SetDoubleAttribute("deriv_0_25",deriv_0_25_);

        element->SetDoubleAttribute("deform_0_5",deform_0_5_);
        element->SetDoubleAttribute("deriv_0_5",deriv_0_5_);

        element->SetDoubleAttribute("deform_0_75",deform_0_75_);
        element->SetDoubleAttribute("deriv_0_75",deriv_0_75_);

        element->SetDoubleAttribute("deform_1_0",deform_1_0_);
        element->SetDoubleAttribute("deriv_1_0",deriv_1_0_);
    }

    virtual void InitFromXml(const TiXmlElement* element);


    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    inline Scalar operator() (Scalar value) const
    {
        return ComputeDeformation(value);
    }

    inline Scalar ComputeDeformation(Scalar value) const
    {
        if(value < 0.5)
        {
            return CubicInterpolation( 2.0*value,
                                       deform_0_0_, 0.5*deriv_0_0_,
                                       deform_0_5_, 0.5*deriv_0_5_);
        }
        else
        {
            return CubicInterpolation( 2.0*(value - 0.5),
                                       deform_0_5_, 0.5*deriv_0_5_,
                                       deform_1_0_, 0.5*deriv_1_0_);
        }

        /*
            if(value < 0.5)
            {
                if(value < 0.25)
                {
                    return CubicInterpolation( 4.0*value,
                                               deform_0_0_, 0.25*deriv_0_0_,
                                               deform_0_25_, 0.25*deriv_0_25_);
                }
                else
                {
                    return CubicInterpolation( 4.0*(value - 0.25),
                                               deform_0_25_, 0.25*deriv_0_25_,
                                               deform_0_5_, 0.25*deriv_0_5_);
                }
            }
            else
            {
                if(value > 0.75)
                {
                    return CubicInterpolation( 4.0*(value - 0.75),
                                               deform_0_75_, 0.25*deriv_0_75_,
                                               deform_1_0_, 0.25*deriv_1_0_);
                }
                else
                {
                    return CubicInterpolation( 4.0*(value - 0.5),
                                               deform_0_5_, 0.25*deriv_0_5_,
                                               deform_0_75_, 0.25*deriv_0_75_);
                }
            }
            */
    }


    /*!
     * \brief ComputeInterpolatedDeformation this function compute a deformation value corresponding
     * to an interpolation of a set of deformation function that are stored inside vec_param
     * and are accessed via the function or lambda function access.
     *
     * Accessor can be either a function pointer with the signature :
     *      const PolynomialDeformationMap& (*accessor) (const TVectorParameter&, int)
     * or a lambda function :
     *      [](const TVectorParameter&, int i) -> const PolynomialDeformationMap&
     *
     * TODO:@todo : find a way to choose the kind of evaluation to be done !
     * This is the same problematic as in ComputeInterpolatedNoiseAndGrad
     */
    template <typename TVectorParameter, typename Accessor>
    void ComputeInterpolatedDeformation(Scalar value,
                                        const std::vector<Scalar>& vec_field_value, Scalar global_field,
                                        const TVectorParameter& vec_param, Accessor access,
                                        Scalar& deformation_amplitude, Scalar& deriv_amplitude,
                                        Scalar& l_min, Scalar& l_max
                                        ) const
    {
        ///TODO:@todo : for now only use 3 of the constraint
        Scalar deform_s1, deriv_s1, deform_s2, deriv_s2;
        Scalar new_value;
        if(value < 0.5) {
            InterpolateParameter(vec_field_value, global_field, vec_param, access,
                                 [](const PolynomialDeformationMap& pdm) -> Scalar { return pdm.deform_0_0_; },
                                 [](const PolynomialDeformationMap& pdm) -> Scalar { return pdm.deriv_0_0_; },
                                 [](const PolynomialDeformationMap& pdm) -> Scalar { return pdm.deform_0_5_; },
                                 [](const PolynomialDeformationMap& pdm) -> Scalar { return pdm.deriv_0_5_; },
                                 deform_s1, deriv_s1, deform_s2, deriv_s2
                                 );
            new_value = 2.0*value;
        } else {
            InterpolateParameter(vec_field_value, global_field, vec_param, access,
                                 [](const PolynomialDeformationMap& pdm) -> Scalar { return pdm.deform_0_5_; },
                                 [](const PolynomialDeformationMap& pdm) -> Scalar { return pdm.deriv_0_5_; },
                                 [](const PolynomialDeformationMap& pdm) -> Scalar { return pdm.deform_1_0_; },
                                 [](const PolynomialDeformationMap& pdm) -> Scalar { return pdm.deriv_1_0_; },
                                 deform_s1, deriv_s1, deform_s2, deriv_s2);
            new_value = 2.0*(value - 0.5);
        }

        CubicInterpolation( new_value,
                            deform_s1, 0.5*deriv_s1,
                            deform_s2, 0.5*deriv_s2,
                            deformation_amplitude, deriv_amplitude);

        l_min = 0.0;
        l_max = 0.0;
        int index = 0;
        for(Scalar field_value : vec_field_value) {
            const auto& param = access(vec_param, index);

            l_min += field_value * param.l_min_;
            l_max += field_value * param.l_max_;
        }
        l_min /= global_field;  // div by \sum_{i} vec_field_value[i]
        l_max /= global_field;
    }

    // to avoid the 4 template parameter ValAccessorN,
    // we could just use  a function pointer as parameter
    // I do not know if there is any difference when it comes to performance
    //      Scalar (*a_deform_s1) (const PolynomialDeformationMap&)
    template<typename TVectorParameter, typename Accessor,
             typename ValAccessor1, typename ValAccessor2,
             typename ValAccessor3, typename ValAccessor4>
    void InterpolateParameter(
            const std::vector<Scalar>& vec_field_value, Scalar global_field,
            const TVectorParameter& vec_param, Accessor access,
            ValAccessor1 a_deform_s1, ValAccessor2 a_deriv_s1,
            ValAccessor3 a_deform_s2, ValAccessor4 a_deriv_s2,
            Scalar& deform_s1, Scalar& deriv_s1,
            Scalar& deform_s2, Scalar& deriv_s2) const
    {
        deform_s1 = 0.0; deriv_s1 = 0.0;
        deform_s2 = 0.0; deriv_s2 = 0.0;

        int index = 0;
        for(Scalar field_value : vec_field_value) {
            const auto& param = access(vec_param, index);

            deform_s1 += field_value * a_deform_s1(param);
            deriv_s1 += field_value * a_deriv_s1(param);
            deform_s2 += field_value * a_deform_s2(param);
            deriv_s2 += field_value * a_deriv_s2(param);
        }

        deform_s1 /= global_field;  // div by \sum_{i} vec_field_value[i]
        deriv_s1 /= global_field;
        deform_s2 /= global_field;
        deriv_s2 /= global_field;

        ++index;
    }


    // Cubic interpolation between 0 and 1
    inline Scalar CubicInterpolation( Scalar abs, Scalar deform0, Scalar deriv0, Scalar deform1, Scalar deriv1) const
    {
        Scalar a0 = deform0;
        Scalar a1 = deriv0;
        Scalar a2 = -2.0*deriv0 + 3.0*(deform1 - deform0) - deriv1 ;
        Scalar a3 = 2.0*(deform0 - deform1) + deriv0 + deriv1;

        return (((a3*abs)+a2)*abs+a1)*abs+a0;
    }
    inline void CubicInterpolation( Scalar abs, Scalar deform0, Scalar deriv0, Scalar deform1, Scalar deriv1,
                                    Scalar& res, Scalar& deriv) const
    {
        Scalar a0 = deform0;
        Scalar a1 = deriv0;
        Scalar a2 = -2.0*deriv0 + 3.0*(deform1 - deform0) - deriv1 ;
        Scalar a3 = 2.0*(deform0 - deform1) + deriv0 + deriv1;

        res = (((a3*abs)+a2)*abs+a1)*abs+a0;
        deriv = 2.0*(((3.0*a3*abs)+2.0*a2)*abs+a1); //TODO:@todo : WARNING : the 2.0*... come from the scaling of the abs !!!
    }

    /////////////////
    /// Accessors ///
    /////////////////

    inline Scalar deform_0_0() const { return deform_0_0_; }
    inline Scalar deriv_0_0() const { return deriv_0_0_; }
    inline Scalar deform_0_25() const { return deform_0_25_; }
    inline Scalar deriv_0_25() const { return deriv_0_25_; }
    inline Scalar deform_0_5() const { return deform_0_5_; }
    inline Scalar deriv_0_5() const { return deriv_0_5_; }
    inline Scalar deform_0_75() const { return deform_0_75_; }
    inline Scalar deriv_0_75() const { return deriv_0_75_; }
    inline Scalar deform_1_0() const { return deform_1_0_; }
    inline Scalar deriv_1_0() const { return deriv_1_0_; }

    inline Scalar d_min() const { return d_min_; }
    inline Scalar d_max() const { return d_max_; }
    inline Scalar l_min() const { return l_min_; }
    inline Scalar l_max() const { return l_max_; }


    /////////////////
    /// Modifiers ///
    /////////////////

    void set_constraint_0_0(Scalar defrom, Scalar deriv)
    {
        deform_0_0_ = defrom;
        deriv_0_0_ = deriv;

        UpdateLimits();
    }
    void set_constraint_0_25(Scalar defrom, Scalar deriv)
    {
        deform_0_25_ = defrom;
        deriv_0_25_ = deriv;

        UpdateLimits();
    }
    void set_constraint_0_5(Scalar defrom, Scalar deriv)
    {
        deform_0_5_ = defrom;
        deriv_0_5_ = deriv;

        UpdateLimits();
    }
    void set_constraint_0_75(Scalar defrom, Scalar deriv)
    {
        deform_0_75_ = defrom;
        deriv_0_75_ = deriv;

        UpdateLimits();
    }
    void set_constraint_1_0(Scalar defrom, Scalar deriv)
    {
        deform_1_0_ = defrom;
        deriv_1_0_ = deriv;

        UpdateLimits();
    }

    void UpdateLimits()
    {
        //TODO:@todo : for now, only take into account 3 of the constraint...
        //TODO:@todo : for first test, assum that extrema are localized on one of the constraint !!!
        // which is obvisoulsy not the case ...
        d_min_ = std::min( {deform_0_0_, deform_0_5_, deform_1_0_} );
        d_max_ = std::max( {deform_0_0_, deform_0_5_, deform_1_0_} );

        Scalar scale = 1.5;
        l_min_ = std::min(scale*d_min_,-d_max_);
        l_max_ = std::max(scale*d_max_,-d_min_);
    }

    /////////////////
    /// Attributs ///
    /////////////////

private:
    Scalar deform_0_0_;
    Scalar deriv_0_0_;

    Scalar deform_0_25_;
    Scalar deriv_0_25_;

    Scalar deform_0_5_;
    Scalar deriv_0_5_;

    Scalar deform_0_75_;
    Scalar deriv_0_75_;

    Scalar deform_1_0_;
    Scalar deriv_1_0_;

    Scalar d_min_, d_max_; //! extrema of deformation
    Scalar l_min_, l_max_; //! limit of field warping to ensure good field quality given deformation extrema
};

//-----------------------------------------------------------------------------
bool operator==(const PolynomialDeformationMap& a, const PolynomialDeformationMap& b);

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_FUNCTOR_POLYNOMIAL_DEFORMATION_MAP_H_
