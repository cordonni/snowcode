/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: BandLimitedGaborSurfaceNoise.h

 Language: C++

 License: expressive license and "SEE BELOW"

 \author: Cédric Zanni
 E-Mail:  cedric.zanni@inria.fr

 Description:

 IMPORTANT NOTICE : this code as been writen from the sample example code
 associtated to the paper :
     Procedural Noise using Sparse Gabor Convolution
     Ares Lagae, Sylvain Lefebvre, George Drettakis and Philip Dutre
     SIGGRAPH 2009
 The associated license is given below.

 The random phase present in the provided code as been removed, because it break
 the "symmetry" of anisotropoy.
 */


/* Copyright (c) 2009 by Ares Lagae, Sylvain Lefebvre,
 * George Drettakis and Philip Dutre
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef BAND_LIMITED_GABOR_SURFACE_NOISE_H
#define BAND_LIMITED_GABOR_SURFACE_NOISE_H

// core dependencies
#include <core/CoreRequired.h>
#include<core/functor/Functor.h>
#include<core/functor/MACRO_FUNCTOR_TYPE.h>

// Convol dependencies
#include <convol/ConvolRequired.h>
#include <convol/functors/noises/PseudoRandomNumberGenerator.h>

#include "core/VectorTraits.h"

namespace expressive {

namespace convol {


class CONVOL_API BandLimitedGaborSurfaceNoise : public core::Functor
{
public:
    EXPRESSIVE_MACRO_NAME("BandLimitedGaborSurfaceNoise")

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    BandLimitedGaborSurfaceNoise(Scalar lambda = 60.0, Scalar r = 0.6, unsigned int seed = 1234,
                                 Scalar F0_min = 3.9, Scalar F0_max = 4.1 ,
                                 Point2 w0_main_dir = Point2::UnitX(), Scalar w0_half_opening = 0.1,
                                 Scalar a = 1.0)
        : a_(a), F0_min_(F0_min), F0_max_(F0_max), w0_main_dir_(w0_main_dir), w0_half_opening_(w0_half_opening),
          lambda_(lambda), r_(r), seed_(seed) {}

    //////////////////////
    /// Type functions ///
    //////////////////////

    static core::FunctorTypeTree BuildStaticType()
    {
        std::vector<core::FunctorTypeTree> vec;
        return core::FunctorTypeTree(StaticName(), vec);
    }
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defines functions

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const;

    virtual void InitFromXml(const TiXmlElement* element);

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    inline void Sample(PseudoRandomNumberGenerator& prng, Scalar& F0_r, Scalar& w0_r) const
    {
        F0_r = prng.uniform(F0_min_, F0_max_);
        w0_r = prng.uniform(-w0_half_opening_, w0_half_opening_);
        //phi_r = prng.uniform(0, 2.0*M_PI);
        ////WARNING : I do not use random phase anymore because it break the "symmetry" of the anisotropy
        ////WARNING : if phi is used, then the random part of w_i is useless (does not change the probability)
    }

    Scalar Cell(const int c[3], const Point& p_c, const Vector& n, const Vector& t, const Vector& b) const
    {
        // Init random number generator
        const unsigned p = 256u;
        unsigned s = (((((unsigned(c[2]) % p) * p) + (unsigned(c[1]) % p)) * p) + (unsigned(c[0]) % p)) + seed_;
        if (s == 0) s = 1;
        PseudoRandomNumberGenerator prng(s);

        Scalar sqr_a = a_*a_;
        Scalar r_g = std::sqrt(2.0f) * r_;

        Scalar sum = 0.0;
        unsigned n_ = prng.poisson((lambda_ / (2.0 * r_)) * (r_g * r_g * r_g));
        for (unsigned i = 0u; i < n_; ++i) {
            Point p_i_c(prng.uniform_0_1(), prng.uniform_0_1(), prng.uniform_0_1());
            Point tmp = r_g*(p_c-p_i_c);

            Point x_k_i( t.dot(tmp), b.dot(tmp), n.dot(tmp) );
            // NOTE: assumes t, b and n are orthogonal

            Scalar F0_r, w0_r;
            Sample(prng, F0_r, w0_r);

            Scalar norm_d2 = ((x_k_i[0] * x_k_i[0]) + (x_k_i[1] * x_k_i[1]));
            Scalar abs_dist_p = 1.0-std::abs(x_k_i[2] / r_);
            Scalar w_i = (prng.uniform_bool() ? -1.0 : +1.0) * abs_dist_p; // cf comment in sample function

            if (( norm_d2 < (r_ * r_)) && abs_dist_p > 0.0)
            {
//                Scalar w_i = abs_dist_p; // if phi is used (cf comment in sample function)

                // Gabor kernel evaluation

                //TODO:@todo : this small trick on the cos ans sin could be removed by pre-aligning the frame to the main direction of
                // anisotropy (this can be done efficiently without any trigonometric function)
                Scalar cos_w = std::cos(w0_r);
                Scalar sin_w = std::sin(w0_r);
                Scalar cos = cos_w * w0_main_dir_[0] - sin_w * w0_main_dir_[1];
                Scalar sin = sin_w * w0_main_dir_[0] + cos_w * w0_main_dir_[1];

                Scalar g = std::exp(-M_PI * sqr_a * norm_d2); //TODO:@todo : replace by a compact polynomial kernel
                Scalar h = std::cos((2.0 * M_PI * F0_r * ((cos * x_k_i[0]) + (sin * x_k_i[1]))) /*+ phi_r*/);// the inside cos-sin is an alignement to a direction, their should be way to avoid it
                sum += w_i * g * h;
            }
        }
        return sum;
    }


    Scalar operator()(const Point& p, const Vector& n, const Vector& t, const Vector& b)
            const
    {
///TODO:@todo : alginment of the frame (n,t,b) such that t is orthogonal to the direction of anisotropy
/// at this point could save a little bit of computational time...

        Scalar r_g = std::sqrt(2.0f) * r_;
        Point p_g = (1.0/r_g) * p; //TODO:@todo cell-size behing of factor two in the multi-frequency, we could simplify things ...

        Point int_p_g = Point(floor(p_g[0]), floor(p_g[1]), floor(p_g[2]));//.unaryExpr(std::ptr_fun<Scalar, Scalar>(floor)); // real index of cell
        int c[3] = { int(int_p_g[0]), int(int_p_g[1]), int(int_p_g[2]) }; // integer index of cell
        Point p_c = p_g-int_p_g; // local coordinate in cell (in [0;1[ )

        Scalar sum = 0.0;
        int i[3];
        for (i[2] = -1; i[2] <= +1; ++i[2]) {
            for (i[1] = -1; i[1] <= +1; ++i[1]) {
                for (i[0] = -1; i[0] <= +1; ++i[0]) {
                    int c_i[3] = { c[0] + i[0], c[1] + i[1], c[2] + i[2] };
                    Point p_c_i ( p_c[0] - i[0], p_c[1] - i[1], p_c[2] - i[2] ); // coordinate of point in new cell
                    sum += Cell(c_i, p_c_i, n, t, b);
                }
            }
        }
//        return sum * 0.5 / (3.0 * std::sqrt(Variance()) * std::sqrt(lambda_)) + 0.5;
        return sum * a_/ std::sqrt(3.0*lambda_) + 0.5;
    }

    ///TODO:@todo : the variance in the new sample code is completely different from this one ...
    Scalar Variance() const
    {
        return (1.0 / 3.0) * (1.0 / (4.0 * (a_ * a_)));
    }

    inline Scalar get_w0_mean() const
    {
        Scalar cos_m = w0_main_dir_.dot(Point2::UnitX());
        Scalar m = std::acos(std::max(std::min(cos_m, (Scalar)1.0), (Scalar) -1.0)); // only work between 0 and pi
        if(w0_main_dir_.dot(Point2::UnitY()) >= 0.0)
            return m;
        else
            return -m;
    }

    /////////////////
    /// Accessors ///
    /////////////////

    inline Scalar a() const { return a_; }

    inline Scalar F0_min() const { return F0_min_; }
    inline Scalar F0_max() const { return F0_max_; }

    inline const Point2& w0_main_dir() const { return w0_main_dir_; }
    inline Scalar w0_half_opening() const { return w0_half_opening_; }

    inline Scalar lambda() const { return lambda_; }
    inline Scalar r() const { return r_; }

    inline unsigned int seed() const { return seed_; }

    /////////////////
    /// Modifiers ///
    /////////////////

    inline void set_F0(Scalar F0_min, Scalar F0_max)
    {
        F0_min_ = F0_min;
        F0_max_ = F0_max;
    }
    inline void set_w0(const Point2& w0_main_dir, Scalar w0_half_opening)
    {
        w0_main_dir_ = w0_main_dir;
        w0_half_opening_ = w0_half_opening;
    }
    inline void set_w0(Scalar w0_mean, Scalar w0_half_opening)
    {
        set_w0(Point2(cos(w0_mean), sin(w0_mean)), w0_half_opening);
    }

    inline void set_lambda(Scalar lambda){ lambda_ = lambda; }
    inline void set_r(Scalar r){ r_ = r; }
    inline void set_seed(unsigned int seed){ seed_ = seed; }
    inline void set_a(Scalar a){ a_ = a; }

    /////////////////
    /// Attributs ///
    /////////////////

private :

    Scalar a_;          //! bandwith

    // frequencies
    Scalar F0_min_;
    Scalar F0_max_;

    Point2 w0_main_dir_; //! unit vector corresponding to the main direction of anisotropy (w0_mean)
    Scalar w0_half_opening_;   //! variance of direction of anisotropy,can vary between epsilon and pi

    // later automatically chosen from other parameters
    Scalar lambda_;     //! impulse density
    Scalar r_;          //! grid cell size

    unsigned seed_;     //! offset to initialize the seed of the prng

};

} // Close namespace convol

} // Close namespace expressive


#endif // GABOR_SURFACE_H
