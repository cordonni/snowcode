/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/* Copyright (c) 2009 by Ares Lagae, Sylvain Lefebvre,
 * George Drettakis and Philip Dutre
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef PSEUDO_RANDOM_NUMBER_GENERATOR_H
#define PSEUDO_RANDOM_NUMBER_GENERATOR_H

// core dependencies
#include <core/CoreRequired.h>
//#include<core/functor/Functor.h>
//#include<core/functor/MACRO_FUNCTOR_TYPE.h>

// Convol dependencies
#include <convol/ConvolRequired.h>

#include <cmath>

namespace expressive {

namespace convol {

class PseudoRandomNumberGenerator
{
public:
      /*!
       * \brief PseudoRandomNumberGenerator
       * \param s seed for the pseudo random number
       */
      PseudoRandomNumberGenerator(unsigned s) : x_(s) {}

      void srand(unsigned s) { x_ = s; }

      unsigned rand() { x_ *= 3039177861u; return x_; }
      Scalar uniform_0_1() { return Scalar(rand()) / Scalar(4294967295u); }
      Scalar uniform(Scalar min, Scalar max)
      {
          return min + (uniform_0_1() * (max - min));
      }
      unsigned poisson(Scalar mean)
      {
        Scalar g_ = std::exp(-mean);
        unsigned em = 0;
        Scalar t = uniform_0_1();
        while (t > g_) {
          ++em;
          t *= uniform_0_1();
        }
        return em;
      }
      bool uniform_bool() { return rand() < (4294967295u / 2u); }

private:
      unsigned x_;
};

} // Close namespace convol

} // Close namespace expressive


#endif // PSEUDO_RANDOM_NUMBER_GENERATOR_H
