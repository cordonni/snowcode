/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: NodeGaborT.h

 Language: C++

 License: expressive license and "SEE BELOW"

 \author: Cédric Zanni
 E-Mail:  cedric.zanni@inria.fr

 Description:

 IMPORTANT NOTICE : this code as been writen from the sample example code
 associtated to the paper :
     Procedural Noise using Sparse Gabor Convolution
     Ares Lagae, Sylvain Lefebvre, George Drettakis and Philip Dutre
     SIGGRAPH 2009
 The associated license is given below.
 */


/* Copyright (c) 2009 by Ares Lagae, Sylvain Lefebvre,
 * George Drettakis and Philip Dutre
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef GABOR_SURFACE_H
#define GABOR_SURFACE_H

// core dependencies
#include <core/CoreRequired.h>
#include <core/functor/Functor.h>
#include <core/functor/MACRO_FUNCTOR_TYPE.h>

// Convol dependencies
#include <convol/ConvolRequired.h>
#include <convol/functors/noises/PseudoRandomNumberGenerator.h>

#include "core/VectorTraits.h"

namespace expressive {

namespace convol {


class CONVOL_API NoiseGaborSurface : public core::Functor
{
public:
    EXPRESSIVE_MACRO_NAME("NoiseGaborSurface")

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    NoiseGaborSurface()
        : lambda_(60.0), r_(0.6), seed_(1234), a_(2.0), isotropic_(false), F0_(4.0), w0_(0.0)
    {
        set_F0_w0(F0_,w0_);
    }

    NoiseGaborSurface(Scalar lambda, Scalar r, unsigned int seed, Scalar F0 = 1.0, Scalar w0 = 0.0, Scalar a = 1.0, bool isotropic = false)
    {
      lambda_ = (Scalar) lambda;
      r_ = (Scalar) r;
      seed_ = (unsigned) seed;
      a_ = (Scalar) a;
      isotropic_ = isotropic;

      F0_ = F0;
      w0_ = w0;

      f_[0] = F0 * cos(w0);
      f_[1] = F0 * sin(w0);
    }

    //////////////////////
    /// Type functions ///
    //////////////////////

    static core::FunctorTypeTree BuildStaticType()
    {
        std::vector<core::FunctorTypeTree> vec;
        return core::FunctorTypeTree(StaticName(), vec);
    }
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defines functions

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        core::Functor::AddXmlAttributes(element);

        element->SetDoubleAttribute("lambda",lambda_);
        element->SetDoubleAttribute("r",r_);
        element->SetDoubleAttribute("a",a_);
        element->SetDoubleAttribute("F0",F0_);
        element->SetDoubleAttribute("w0",w0_);

        element->SetAttribute("seed",seed_);

        //TODO : @todo : use an int until we use tinyxml-2
        if(isotropic_)
            element->SetAttribute("isotropic",1);
        else
            element->SetAttribute("isotropic",0);
    }

    virtual void InitFromXml(const TiXmlElement* element);


    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    //TODO:@todo : to be inlined : avoid some redundant computation : x^2 + y^2 already computed
    Scalar Kernel(Scalar w, const Scalar f[2], Scalar phi, Scalar a, const Point& x) const
    {
      Scalar g = std::exp(-M_PI * (a * a) * ((x[0] * x[0]) + (x[1] * x[1])));
      Scalar h = std::cos((2.0 * M_PI * ((f[0] * x[0]) + (f[1] * x[1]))) + phi);
      return w * g * h;
    }

    void Sample(PseudoRandomNumberGenerator& prng, Scalar f[2], Scalar& phi) const
    {
        if (!isotropic_)
        {
            f[0] = f_[0], f[1] = f_[1];
        }
        else
        {
            Scalar f_t = prng.uniform(0.0, 2.0 * M_PI);
            Scalar f_r = std::sqrt((f_[0] * f_[0]) + (f_[1] * f_[1]));
            f[0] = f_r * std::cos(f_t), f[1] = f_r * std::sin(f_t);
        }
        phi = prng.uniform(0.0, 2.0 * M_PI);
    }

    Scalar Cell(const int c[3], const Point& x_c, const Vector& n, const Vector& t, const Vector& b) const
    {
        const unsigned p = 256u;
        unsigned s = (((((unsigned(c[2]) % p) * p) + (unsigned(c[1]) % p)) * p) + (unsigned(c[0]) % p)) + seed_;
        if (s == 0) s = 1;
        PseudoRandomNumberGenerator prng(s);

        Scalar sqr_a = a_*a_;
        Scalar r_g = std::sqrt(2.0f) * r_;
        unsigned n_ = prng.poisson((lambda_ / (2.0 * r_)) * (r_g * r_g * r_g));
        Scalar sum = 0.0;
        for (unsigned i = 0u; i < n_; ++i) {
            Point x_i_c(prng.uniform_0_1(), prng.uniform_0_1(), prng.uniform_0_1());
            Point tmp = x_c-x_i_c;

            Point x_k_i( r_g * t.dot(tmp), r_g * b.dot(tmp), r_g * n.dot(tmp) );
            // NOTE: assumes t, b and n are orthogonal

            Scalar f_i[2], phi_i;
            Sample(prng, f_i, phi_i);

            Scalar norm_d2 = ((x_k_i[0] * x_k_i[0]) + (x_k_i[1] * x_k_i[1]));
            Scalar abs_dist_p = 1.0 - std::abs(x_k_i[2])/r_; // positif inside the scope of following if
            Scalar w_i = (prng.uniform_bool() ? -1.0 : +1.0) * abs_dist_p;
            if ((norm_d2 < (r_ * r_)) && (abs_dist_p > 0.0)) {
                // Gabor kernel evaluation
                Scalar g = std::exp(-M_PI * sqr_a * norm_d2);
                Scalar h = std::cos((2.0 * M_PI * ((f_i[0] * x_k_i[0]) + (f_i[1] * x_k_i[1]))) + phi_i);
                sum += w_i * g * h;
            }
        }
        return sum;
    }

    Scalar Grid(const Point& x_g, const Point& n, const Point& t, const Point& b) const
    {
        Point int_x_g = Point(floor(x_g[0]), floor(x_g[1]), floor(x_g[2]));//x_g.unaryExpr(std::ptr_fun<Scalar, Scalar>(floor));
        int c[3] = { int(int_x_g[0]), int(int_x_g[1]), int(int_x_g[2]) };
        Point x_c = x_g-int_x_g;

        Scalar sum = 0.0;
        int i[3];
        for (i[2] = -1; i[2] <= +1; ++i[2]) {
            for (i[1] = -1; i[1] <= +1; ++i[1]) {
                for (i[0] = -1; i[0] <= +1; ++i[0]) {
                    int c_i[3] = { c[0] + i[0], c[1] + i[1], c[2] + i[2] };
                    Point x_c_i ( x_c[0] - i[0], x_c[1] - i[1], x_c[2] - i[2] );
                    sum += Cell(c_i, x_c_i, n, t, b);
                }
            }
        }
        return sum / std::sqrt(lambda_);
    }

    Scalar Noise(const Vector& point, const Vector& normal, const Vector& tangent, const Vector& ortho_dir) const
    {
        Scalar r_g = std::sqrt(2.0f) * r_;
        Point x_g = (1.0/r_g) * point; //TODO:@todo cell-size behing of factor two in the multi-frequency, we could simplify things ...
        return Grid(x_g,normal,tangent,ortho_dir);
    }

    Scalar operator()(const Vector& point, const Vector& normal, const Vector& tangent, const Vector& ortho_dir)
    const
    {
        //        return Noise(point, normal, tangent, ortho_dir) * 0.5 / (3.0 * std::sqrt(Variance()))+0.5;
        return Noise(point, normal, tangent, ortho_dir) * a_ / std::sqrt(3.0) +0.5;
    }

    ///TODO:@todo : the variance in the new sample code is completely different from this one ...
    Scalar Variance() const
    {
        return (1.0 / 3.0) * (1.0 / (4.0 * (a_ * a_)));
    }


    /////////////////
    /// Accessors ///
    /////////////////

    inline Scalar lambda() const { return lambda_; }
    inline Scalar r() const { return r_; }
    inline unsigned int seed() const { return seed_; }
    inline Scalar a() const { return a_; }
    inline bool isotropic() const { return isotropic_; }

    inline Scalar F0() const { return F0_; }
    inline Scalar w0() const { return w0_; }

    /////////////////
    /// Modifiers ///
    /////////////////

    inline void set_F0_w0(Scalar F0, Scalar w0)
    {
        F0_ = F0;
        w0_ = w0;

        f_[0] = F0 * cos(w0);
        f_[1] = F0 * sin(w0);
    }

    inline void set_lambda(Scalar lambda){ lambda_ = lambda; }
    inline void set_r(Scalar r){ r_ = r; }
    inline void set_seed(unsigned int seed){ seed_ = seed; }
    inline void set_a(Scalar a){ a_ = a; }
    inline void set_isotropic(bool isotropic){ isotropic_ = isotropic; }

    /////////////////
    /// Attributs ///
    /////////////////

private :
    Scalar lambda_;
    Scalar r_;
    unsigned seed_;
    Scalar a_;
    bool isotropic_;
    Scalar F0_;
    Scalar w0_;
    Scalar f_[2];
};


/*
    Scalar Cell(const int c[3], const Scalar x_c[3], const Scalar n[3], const Scalar t[3], const Scalar b[3]) const
    {
        const unsigned p = 256u;
        unsigned s = (((((unsigned(c[2]) % p) * p) + (unsigned(c[1]) % p)) * p) + (unsigned(c[0]) % p)) + seed_;
        if (s == 0) s = 1;
        PseudoRandomNumberGenerator prng;
        prng.srand(s);
        Scalar r_g = std::sqrt(2.0f) * r_;
        unsigned n_ = prng.poisson((lambda_ / (2.0 * r_)) * (r_g * r_g * r_g));
        Scalar sum = 0.0;
        for (unsigned i = 0u; i < n_; ++i) {
            Scalar x_i_c[3] = { prng.uniform_0_1(), prng.uniform_0_1(), prng.uniform_0_1() };
            Scalar x_k_i[3] = {
                (t[0] * (r_g * (x_c[0] - x_i_c[0]))) + (t[1] * (r_g * (x_c[1] - x_i_c[1]))) + (t[2] * (r_g * (x_c[2] - x_i_c[2]))),
                (b[0] * (r_g * (x_c[0] - x_i_c[0]))) + (b[1] * (r_g * (x_c[1] - x_i_c[1]))) + (b[2] * (r_g * (x_c[2] - x_i_c[2]))),
                (n[0] * (r_g * (x_c[0] - x_i_c[0]))) + (n[1] * (r_g * (x_c[1] - x_i_c[1]))) + (n[2] * (r_g * (x_c[2] - x_i_c[2])))
            }; // NOTE: assumes t, b and n are orthogonal
            Scalar w_i, f_i[2], phi_i, a_i;
            Sample(prng, w_i, f_i, phi_i, a_i);
            w_i = (prng.uniform_bool() ? -1.0 : +1.0) * std::max(0.0, (1.0 - std::abs(x_k_i[2] / r_)));
            if ((((x_k_i[0] * x_k_i[0]) + (x_k_i[1] * x_k_i[1])) < (r_ * r_)) && (std::abs(x_k_i[2]) < r_)) //testing the square of last number avoid indirection...
            {
                sum += Kernel(w_i, f_i, phi_i, a_i, x_k_i);
            }
        }
        return sum;
    }

    Scalar Grid(const Scalar x_g[3], const Scalar n[3], const Scalar t[3], const Scalar b[3]) const
    {
        Scalar int_x_g[3] = { std::floor(x_g[0]), std::floor(x_g[1]), std::floor(x_g[2]) };
        int c[3] = { int(int_x_g[0]), int(int_x_g[1]), int(int_x_g[2]) };
        Scalar x_c[3] = { x_g[0] - int_x_g[0], x_g[1] - int_x_g[1], x_g[2] - int_x_g[2] };
        Scalar sum = 0.0;
        int i[3];
        for (i[2] = -1; i[2] <= +1; ++i[2]) {
            for (i[1] = -1; i[1] <= +1; ++i[1]) {
                for (i[0] = -1; i[0] <= +1; ++i[0]) {
                    int c_i[3] = { c[0] + i[0], c[1] + i[1], c[2] + i[2] };
                    Scalar x_c_i[3] = { x_c[0] - i[0], x_c[1] - i[1], x_c[2] - i[2] };
                    sum += Cell(c_i, x_c_i, n, t, b);
                }
            }
        }
        return sum / std::sqrt(lambda_);
    }
*/


} // Close namespace convol

} // Close namespace expressive


#endif // GABOR_SURFACE_H
