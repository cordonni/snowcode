#include <convol/functors/kernels/Kernel.h>

#include <core/xmlloaders/TraitsXmlQuery.h>

namespace expressive {

namespace convol {

void Kernel::InitFromXml(const TiXmlElement* element)
{
    double scale;
    core::TraitsXmlQuery::QueryDoubleAttribute("Kernel::InitFromXml",
                                               element, "scale", &scale);
    set_scale(scale);
}


/////////////////////////////////////////////
/// Help function template specialization ///
/////////////////////////////////////////////

namespace kernelHelpFunction {

template<>
Scalar IntPow<1>(Scalar aux) { return aux; }
template<>
Scalar IntPow<2>(Scalar aux) { return aux*aux; }
template<>
Scalar IntPow<3>(Scalar aux) { return aux*aux*aux; }

template<>
Scalar IntPow<4>(Scalar aux) {
    Scalar tmp_aux = aux*aux;
    return tmp_aux*tmp_aux;
}

template<>
Scalar IntPow<5>(Scalar aux) {
    Scalar tmp_aux = aux*aux;
    return aux*tmp_aux*tmp_aux;
}

template<>
Scalar IntPow<6>(Scalar aux) {
    Scalar tmp_aux = aux*aux*aux;
    return tmp_aux*tmp_aux;
}

template<>
Scalar IntPow<7>(Scalar aux) {
    Scalar tmp_aux = aux*aux*aux;
    return aux*tmp_aux*tmp_aux;
}

template<>
void ExpandWeightPolynome<3>(Scalar (&special_coeff)[3] , Scalar w_min, Scalar delta_w)
{
    special_coeff[0] = w_min * w_min;
    special_coeff[1] = 2.0 * w_min * delta_w;
    special_coeff[2] = delta_w * delta_w;
}

template<>
void ExpandWeightPolynome<4>(Scalar (&special_coeff)[4] , Scalar w_min, Scalar delta_w)
{
    const Scalar t1 = delta_w * delta_w;
    const Scalar t2 = w_min * w_min;
    special_coeff[0] = w_min * t2;
    special_coeff[1] = 3.0 * t2 * delta_w;
    special_coeff[2] = 3.0 * w_min * t1;
    special_coeff[3] = delta_w * t1;
}
template<>
void ExpandWeightPolynome<5>(Scalar (&special_coeff)[5] , Scalar w_min, Scalar delta_w)
{
    const Scalar t1 = 4.0 * w_min * delta_w;
    const Scalar t2 = delta_w * delta_w;
    const Scalar t3 = w_min * w_min;
    special_coeff[0] = t3 * t3;
    special_coeff[1] = t3 * t1;
    special_coeff[2] = 6.0 * t3 * t2;
    special_coeff[3] = t2 * t1;
    special_coeff[4] = t2 * t2;
}
template<>
void ExpandWeightPolynome<6>(Scalar (&special_coeff)[6] , Scalar w_min, Scalar delta_w)
{
    const Scalar t1 = w_min * w_min;
    const Scalar t2 = delta_w * delta_w;
    const Scalar t3 = 10.0 * t1 * t2;
    const Scalar t4 = t2 * t2;
    const Scalar t5 = t1 * t1;
    special_coeff[0] = w_min * t5;
    special_coeff[1] = 5.0 * t5 * delta_w;
    special_coeff[2] = w_min * t3;
    special_coeff[3] = delta_w * t3;
    special_coeff[4] = 5.0 * w_min * t4;
    special_coeff[5] = delta_w * t4;
}
template<>
void ExpandWeightPolynome<7>(Scalar (&special_coeff)[7] , Scalar w_min, Scalar delta_w)
{
    const Scalar t1 = 6.0 * w_min * delta_w;
    const Scalar t2 = delta_w * delta_w;
    const Scalar t3 = t2 * t2;
    const Scalar t4 = delta_w * t2;
    const Scalar t5 = w_min * w_min;
    const Scalar t6 = t5 * t5;
    const Scalar t7 = w_min * t5;
    special_coeff[0] = t7 * t7;
    special_coeff[1] = t6 * t1;
    special_coeff[2] = 15.0 * t6 * t2;
    special_coeff[3] = 20.0 * t7 * t4;
    special_coeff[4] = 15.0 * t5 * t3;
    special_coeff[5] = t3 * t1;
    special_coeff[6] = t4 * t4;
}

}

} // Close namespace convol

} // Close namespace expressive
