#include<convol/functors/kernels/compactpolynomial/HornusCompactPolynomialOptimizedFunctions.h>

namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////



//-------------------------------------------------------------------------
// Point evaluation -------------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar HornusCompactPolynomial<DEGREE>::Eval(const WeightedPoint& p_source, const Point& point) const
{
    const Scalar dist2 = (point-p_source).squaredNorm();

    const Scalar aux = 1.0-this->inv_scale_2_*dist2 / (p_source.weight()*p_source.weight());
    if(aux > 0.0)
    {
        Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2>(aux);
        return this->normalization_factor_0D_*int_power_aux;
    }
    else
    {
        return 0.0;
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector HornusCompactPolynomial<DEGREE>::EvalGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon*/) const
{
    Vector direction = point - p_source;
    const Scalar dist2 = direction.squaredNorm() / (p_source.weight()*p_source.weight());

    const Scalar aux = 1.0-this->inv_scale_2_*dist2;
    if(aux > 0.0)
    {
        Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2-1>(aux);
        return (-this->normalization_factor_0D_*this->inv_scale_2_*this->degree_r()*int_power_aux)*direction;
    }
    else
    {
        return Vector::Zero();
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
void HornusCompactPolynomial<DEGREE>::EvalValueAndGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon_grad*/,
                                                  Scalar& value_res, Vector& grad_res) const
{
    Vector direction = point - p_source;
    const Scalar dist2 = direction.squaredNorm() / (p_source.weight()*p_source.weight());

    const Scalar aux = 1.0-this->inv_scale_2_*dist2;
    if(aux > 0.0)
    {
        Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2-1>(aux);

        value_res = this->normalization_factor_0D_*aux*int_power_aux;
        grad_res = (-this->normalization_factor_0D_*this->inv_scale_2_*this->degree_r()*int_power_aux)*direction;
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
// Segment evaluation -----------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar HornusCompactPolynomial<DEGREE>::Eval(const OptWeightedSegment& seg, const Point& point) const
{
    const Vector p_min_to_point( point-seg.p_min() );
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();

    Scalar special_coeff[3] = { seg.weight_min()*seg.weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.unit_delta_weight()*seg.weight_min()     - this->inv_scale_2_ * uv ,
                               seg.unit_delta_weight()*seg.unit_delta_weight() - this->inv_scale_2_ };

    Scalar clipped_l1, clipped_l2;
    if(seg.HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.weight_min() + clipped_l1 * seg.unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        if(seg.unit_delta_weight() >= 0.045) {
            //TODO:@todo : should do the same re-writing of the formula to avoid this test !
            return   this->normalization_factor_1D_
                    * HornusCompactPolynomialOptim::HornusCompactPolynomial_segment_F<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                               seg.unit_delta_weight(),special_coeff);
        }else{
            return  this->normalization_factor_1D_
                   * HornusCompactPolynomialOptim::HornusCompactPolynomial_approx_segment_F<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                     seg.unit_delta_weight(), inv_local_min_weight, special_coeff);
        }
    }
    else
    {
        return 0.0;
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector HornusCompactPolynomial<DEGREE>::EvalGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const
{
    const Vector p_min_to_point( point-seg.p_min() );
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();

    Scalar special_coeff[3] = { seg.weight_min()*seg.weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.unit_delta_weight()*seg.weight_min()     - this->inv_scale_2_ * uv ,
                               seg.unit_delta_weight()*seg.unit_delta_weight() - this->inv_scale_2_ };

    Scalar clipped_l1, clipped_l2;
    if(seg.HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.weight_min() + clipped_l1 * seg.unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        Scalar F0F1[2];
        if(seg.unit_delta_weight() >= 0.045) {
            //TODO:@todo : should do the same re-writing of the formula to avoid this test !
            HornusCompactPolynomialOptim::HornusCompactPolynomial_segment_GradF<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                      seg.unit_delta_weight(),
                                                                                      special_coeff, F0F1);
        }else{
            HornusCompactPolynomialOptim::HornusCompactPolynomial_approx_segment_GradF<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                             seg.unit_delta_weight(), inv_local_min_weight,
                                                                                             special_coeff, F0F1);
        }
        F0F1[0] *= inv_local_min_weight;
        return (this->normalization_factor_1D_*this->degree_r()*this->inv_scale_2_*inv_local_min_weight)
                * (   (F0F1[1] + clipped_l1 * F0F1[0]) * seg.increase_unit_dir()
                    -  F0F1[0] * p_min_to_point );
    }
    else
    {
        return Vector::Zero();
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
void HornusCompactPolynomial<DEGREE>::EvalValueAndGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                                                  Scalar& value_res, Vector& grad_res) const
{
    const Vector p_min_to_point( point-seg.p_min() );
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();

    Scalar special_coeff[3] = { seg.weight_min()*seg.weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.unit_delta_weight()*seg.weight_min()     - this->inv_scale_2_ * uv ,
                               seg.unit_delta_weight()*seg.unit_delta_weight() - this->inv_scale_2_ };

    Scalar clipped_l1, clipped_l2;
    if(seg.HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.weight_min() + clipped_l1 * seg.unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        Scalar F0F1F2[3];
        if(seg.unit_delta_weight() >= 0.045) { // ensure a maximum relative error of ??? (for degree i up to 8)
            //TODO:@todo : should do the same re-writing of the formula to avoid this test !
            HornusCompactPolynomialOptim::HornusCompactPolynomial_segment_FGradF<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                       seg.unit_delta_weight(),
                                                                                       special_coeff, F0F1F2);
        }else{
            HornusCompactPolynomialOptim::HornusCompactPolynomial_approx_segment_FGradF<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                              seg.unit_delta_weight(), seg.inv_weight_min(),
                                                                                              special_coeff, F0F1F2);
        }
        value_res = this->normalization_factor_1D_ * F0F1F2[0];
        F0F1F2[1] *= inv_local_min_weight;
        grad_res = (this->normalization_factor_1D_*this->degree_r()*this->inv_scale_2_*inv_local_min_weight)
                   * (  (F0F1F2[2] + clipped_l1 * F0F1F2[1]) * seg.increase_unit_dir()
                      -  F0F1F2[1] * p_min_to_point );
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}
//-------------------------------------------------------------------------



//////////////////////////
/// Bounding functions ///
//////////////////////////

template <int DEGREE>
Scalar HornusCompactPolynomial<DEGREE>::GetIsoValueForThickness(Scalar length, Scalar thickness) const
{
        if(this->scale_ > thickness)
        {
            const Scalar sqr_thickness = thickness*thickness;
            const Scalar d = sqrt(this->scale_*this->scale_ - sqr_thickness);
            const Scalar l_clipped = (d < 0.5*length) ? d : 0.5*length;

            Scalar special_coeff[3] = {1.0 - this->inv_scale_2_* sqr_thickness,
                                       0.0,
                                       - this->inv_scale_2_ * l_clipped*l_clipped };

            return 2.0 * this->normalization_factor_1D_ * l_clipped * HornusCompactPolynomialOptim::HornusCompactPolynomial_approx_segment_F<DEGREE>(0.0,1.0,0.0,1.0,special_coeff);
        }
        else
        {
            return 0.0;
        }
/*
        const Scalar l2 = length*length;
        Scalar special_coeff[3] = {1.0 - this->inv_scale_2_* (thickness*thickness + 0.25*l2),
                                   - 0.5 * this->inv_scale_2_ * l2 ,
                                   - this->inv_scale_2_ * l2 };

        Scalar clipped_l1, clipped_l2;
        PolyWeightedSegment seg(Point(0.0,0.0,0.0), Point(1.0,0.0,0.0), 1.0, 1.0); // point information are useless !
        if(seg.HomotheticClipping(special_coeff, clipped_l1, clipped_l2))
        {
            return length * HornusCompactPolynomialOptim::HornusCompactPolynomial_approx_segment_F_i$i$(clipped_l1,clipped_l2,0.0,1.0,special_coeff);
        }
        else
        {
            return 0.0;
        }
*/
}

} // Close namespace convol

} // Close namespace expressive
