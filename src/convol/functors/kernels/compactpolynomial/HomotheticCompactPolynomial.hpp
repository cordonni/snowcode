#include <convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomialOptimizedFunctions.h>

namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////

//-------------------------------------------------------------------------
// Point evaluation -------------------------------------------------------
//-------------------------------------------------------------------------
template <int DEGREE>
Scalar HomotheticCompactPolynomial<DEGREE>::Eval(const WeightedPoint& p_source, const Point& point) const
{
    const Scalar dist2 = (point-p_source).squaredNorm();
    
    const Scalar aux = 1.0-this->inv_scale_2_*dist2 / (p_source.weight()*p_source.weight());
    if(aux > 0.0)
    {
        Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2>(aux);

        return this->normalization_factor_0D_*int_power_aux;
    }
    else
    {
        return 0.0;
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
Vector HomotheticCompactPolynomial<DEGREE>::EvalGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon*/) const
{
    Vector direction = point - p_source;
    const Scalar dist2 = direction.squaredNorm() / (p_source.weight()*p_source.weight());
    
    const Scalar aux = 1.0-this->inv_scale_2_*dist2;
    if(aux > 0.0)
    {
        Scalar int_power_aux =  kernelHelpFunction::IntPow<DEGREE/2-1>(aux);

        return -this->normalization_factor_0D_*this->inv_scale_2_*this->degree_r()*(int_power_aux)*direction;
    }
    else
    {
        return Vector::Zero();
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticCompactPolynomial<DEGREE>::EvalValueAndGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon_grad*/,
                                                    Scalar& value_res, Vector& grad_res) const
{
    Vector direction = point - p_source;
    const Scalar dist2 = direction.squaredNorm() / (p_source.weight()*p_source.weight());
    
    const Scalar aux = 1.0-this->inv_scale_2_*dist2;
    if(aux > 0.0)
    {
        Scalar int_power_aux =  kernelHelpFunction::IntPow<DEGREE/2-1>(aux);

        value_res = this->normalization_factor_0D_*aux*int_power_aux;
        grad_res = -this->normalization_factor_0D_*this->inv_scale_2_*this->degree_r()*(int_power_aux)*direction;
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticCompactPolynomial<DEGREE>::EvalHomotheticValues(const WeightedPoint& p_source, const Point& point,
                                                        Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
    // TODO:@todo : should use degree i+1
    assert(false);
    UNUSED(point); UNUSED(p_source);
    homothetic_value_res = 0.0;
    homothetic_grad_res = Vector::Zero();
}
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
// Segment evaluation -----------------------------------------------------
//-------------------------------------------------------------------------
template <int DEGREE>
Scalar HomotheticCompactPolynomial<DEGREE>::Eval(const OptWeightedSegment& seg, const Point& point) const
{
    const Vector p_min_to_point( point-seg.p_min() );
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();
    
    Scalar special_coeff[3] = { seg.weight_min()*seg.weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.unit_delta_weight()*seg.weight_min()     - this->inv_scale_2_ * uv ,
                                seg.unit_delta_weight()*seg.unit_delta_weight() - this->inv_scale_2_ };
    
    Scalar clipped_l1, clipped_l2;
    if(seg.HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.weight_min() + clipped_l1 * seg.unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        return   this->normalization_factor_1D_
                * HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_segment_F<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                   seg.unit_delta_weight(),special_coeff);
    }
    else
    {
        return 0.0;
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
Vector HomotheticCompactPolynomial<DEGREE>::EvalGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const
{
    const Vector p_min_to_point( point-seg.p_min() );
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();
    
    Scalar special_coeff[3] = { seg.weight_min()*seg.weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.unit_delta_weight()*seg.weight_min()     - this->inv_scale_2_ * uv ,
                                seg.unit_delta_weight()*seg.unit_delta_weight() - this->inv_scale_2_ };
    
    Scalar clipped_l1, clipped_l2;
    if(seg.HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.weight_min() + clipped_l1 * seg.unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        Scalar F0F1[2];
        HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_segment_GradF<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                             seg.unit_delta_weight(),
                                                                                             special_coeff, F0F1);
        F0F1[0] *= inv_local_min_weight;
        return (this->normalization_factor_1D_*this->degree_r()*this->inv_scale_2_*inv_local_min_weight)
                * (   (F0F1[1] + clipped_l1 * F0F1[0]) * seg.increase_unit_dir()
                -  F0F1[0] * p_min_to_point );
    }
    else
    {
        return Vector::Zero();
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticCompactPolynomial<DEGREE>::EvalValueAndGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                                                    Scalar& value_res, Vector& grad_res) const
{
    const Vector p_min_to_point( point-seg.p_min() );
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();
    
    Scalar special_coeff[3] = { seg.weight_min()*seg.weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.unit_delta_weight()*seg.weight_min()     - this->inv_scale_2_ * uv ,
                                seg.unit_delta_weight()*seg.unit_delta_weight() - this->inv_scale_2_ };
    
    Scalar clipped_l1, clipped_l2;
    if(seg.HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.weight_min() + clipped_l1 * seg.unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        Scalar F0F1F2[3];
        HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_segment_FGradF<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                              seg.unit_delta_weight(),
                                                                                              special_coeff, F0F1F2);
        value_res = this->normalization_factor_1D_ * F0F1F2[0];
        F0F1F2[1] *= inv_local_min_weight;
        grad_res = (this->normalization_factor_1D_*this->degree_r()*this->inv_scale_2_*inv_local_min_weight)
                * (  (F0F1F2[2] + clipped_l1 * F0F1F2[1]) * seg.increase_unit_dir()
                -  F0F1F2[1] * p_min_to_point );
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}

//-------------------------------------------------------------------------

// Two following functions compute integral for constant weight along a segment represented
// by some base parameters (can be useful when "segments" are created on the fly such as in SemiNumericalKernel)
template <int DEGREE>
Scalar HomotheticCompactPolynomial<DEGREE>::Eval( const Point& p_1, Scalar w_1,
                                           const Vector& unit_dir, Scalar length,
                                           const Point& point) const
{
    const Vector p_min_to_point( point - p_1 );
    const Scalar uv = unit_dir.dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();
    
    Scalar special_coeff[3] = { w_1*w_1  - this->inv_scale_2_ * d2 ,
                                - this->inv_scale_2_ * uv ,
                                - this->inv_scale_2_ };
    
    Scalar clipped_l1, clipped_l2;
    if(OptWeightedSegment::HomotheticClippingSpecial(special_coeff, length, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / w_1;
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - this->inv_scale_2_*(uv-clipped_l1) * inv_local_min_weight;

        return HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_segment_F_cste<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                     special_coeff );
    }
    else
    {
        return 0.0;
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticCompactPolynomial<DEGREE>::EvalValueAndGrad( const Point& p_1, Scalar w_1,
                                                     const Vector& unit_dir, Scalar length,
                                                     const Point& point, Scalar /*epsilon_grad*/,
                                                     Scalar& value_res, Vector& grad_res) const
{
    const Vector p_min_to_point( point - p_1 );
    const Scalar uv = unit_dir.dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();
    
    Scalar special_coeff[3] = { w_1*w_1  - this->inv_scale_2_ * d2 ,
                                - this->inv_scale_2_ * uv ,
                                - this->inv_scale_2_ };
    
    Scalar clipped_l1, clipped_l2;
    if(OptWeightedSegment::HomotheticClippingSpecial(special_coeff, length, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / w_1;
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - this->inv_scale_2_*(uv-clipped_l1) * inv_local_min_weight;

        Scalar F0F1F2[3];
        HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_segment_FGradF_cste<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                   special_coeff, F0F1F2);
        value_res = F0F1F2[0];
        F0F1F2[1] *= inv_local_min_weight;
        grad_res = (this->degree_r()*this->inv_scale_2_*inv_local_min_weight)
                * (  (F0F1F2[2] + clipped_l1 * F0F1F2[1]) * unit_dir
                -  F0F1F2[1] * p_min_to_point );
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
// Segment with Density evaluation ----------------------------------------
//-------------------------------------------------------------------------
template <int DEGREE>
Scalar HomotheticCompactPolynomial<DEGREE>::Eval(const WeightedSegmentWithDensity& seg, const Point& point) const
{
    const Vector p_min_to_point( point-seg.opt_w_seg().p_min() );
    const Scalar uv = seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();
    
    Scalar special_coeff[3] = { seg.opt_w_seg().weight_min()*seg.opt_w_seg().weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().weight_min()     - this->inv_scale_2_ * uv ,
                                seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_ };
    
    Scalar clipped_l1, clipped_l2;
    if(seg.opt_w_seg().HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.opt_w_seg().weight_min() + clipped_l1 * seg.opt_w_seg().unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        return  this->normalization_factor_1D_
                * HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_density_segment_F<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                           seg.opt_w_seg().unit_delta_weight(),
                                                                                                           special_coeff,seg.density_special_coeff());
    }
    else
    {
        return 0.0;
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
Vector HomotheticCompactPolynomial<DEGREE>::EvalGrad(const WeightedSegmentWithDensity& seg, const Point& point, Scalar /*epsilon*/) const
{
    const Vector p_min_to_point( point-seg.opt_w_seg().p_min() );
    const Scalar uv = seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();
    
    Scalar special_coeff[3] = { seg.opt_w_seg().weight_min()*seg.opt_w_seg().weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().weight_min()     - this->inv_scale_2_ * uv ,
                                seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_ };
    
    Scalar clipped_l1, clipped_l2;
    if(seg.opt_w_seg().HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.opt_w_seg().weight_min() + clipped_l1 * seg.opt_w_seg().unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        Scalar F0F1[2];
        HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_density_segment_GradF<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                     seg.opt_w_seg().unit_delta_weight(),
                                                                                                     special_coeff,seg.density_special_coeff(), F0F1);

        F0F1[0] *= inv_local_min_weight;
        return (this->normalization_factor_1D_*this->degree_r()*this->inv_scale_2_*inv_local_min_weight)
                * (   (F0F1[1] + clipped_l1 * F0F1[0]) * seg.opt_w_seg().increase_unit_dir()
                -  F0F1[0] * p_min_to_point );
    }
    else
    {
        return Vector::Zero();
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticCompactPolynomial<DEGREE>::EvalValueAndGrad(const WeightedSegmentWithDensity& seg, const Point& point, Scalar /*epsilon_grad*/,
                                                    Scalar& value_res, Vector& grad_res) const
{
    const Vector p_min_to_point( point-seg.opt_w_seg().p_min() );
    const Scalar uv = seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();
    
    Scalar special_coeff[3] = { seg.opt_w_seg().weight_min()*seg.opt_w_seg().weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().weight_min()     - this->inv_scale_2_ * uv ,
                                seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_ };
    
    Scalar clipped_l1, clipped_l2;
    if(seg.opt_w_seg().HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.opt_w_seg().weight_min() + clipped_l1 * seg.opt_w_seg().unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        Scalar F0F1F2[3];
        HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_density_segment_FGradF<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                      seg.opt_w_seg().unit_delta_weight(),
                                                                                                      special_coeff,seg.density_special_coeff(), F0F1F2);

        value_res = this->normalization_factor_1D_ * F0F1F2[0];
        F0F1F2[1] *= inv_local_min_weight;
        grad_res = (this->normalization_factor_1D_*this->degree_r()*this->inv_scale_2_*inv_local_min_weight)
                * (  (F0F1F2[2] + clipped_l1 * F0F1F2[1]) * seg.opt_w_seg().increase_unit_dir()
                -  F0F1F2[1] * p_min_to_point );
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
// Clipped Segment evaluation ( for Witnessed Operator) -------------------
//-------------------------------------------------------------------------
template <int DEGREE>
Scalar HomotheticCompactPolynomial<DEGREE>::EvalClipped(const WeightedSegmentWithDensity& seg, const Sphere& clipping_sphere) const
{
    const Vector p_min_to_point( clipping_sphere.center_-seg.opt_w_seg().p_min() );
    const Scalar uv = seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();
    
    const Scalar radius_sqr = this->inv_scale_2_ * clipping_sphere.radius_ * clipping_sphere.radius_;
    Scalar special_coeff[3] = { radius_sqr*seg.opt_w_seg().weight_min()*seg.opt_w_seg().weight_min()               - this->inv_scale_2_*d2,
                                -radius_sqr*seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().weight_min()       - this->inv_scale_2_*uv,
                                radius_sqr*seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_};

    Scalar clipped_l1, clipped_l2;
    if(seg.opt_w_seg().HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.opt_w_seg().weight_min() + clipped_l1 * seg.opt_w_seg().unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;
        special_coeff[2] = seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_ ;

        return HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_segment_F<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                seg.opt_w_seg().unit_delta_weight(),special_coeff);
    }
    else
    {
        return 0.0;
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticCompactPolynomial<DEGREE>::EvalValueAndGradClipped(const WeightedSegmentWithDensity& seg, const Sphere& clipping_sphere, Scalar /*epsilon_grad*/,
                                                           Scalar& value_res, Vector& grad_res) const
{
    const Vector p_min_to_point( clipping_sphere.center_-seg.opt_w_seg().p_min() );
    const Scalar uv = seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();
    
    const Scalar radius_sqr = this->inv_scale_2_ * clipping_sphere.radius_ * clipping_sphere.radius_;
    Scalar special_coeff[3] = { radius_sqr*seg.opt_w_seg().weight_min()*seg.opt_w_seg().weight_min()               - this->inv_scale_2_*d2,
                                -radius_sqr*seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().weight_min()       - this->inv_scale_2_*uv,
                                radius_sqr*seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_};

    Scalar clipped_l1, clipped_l2;
    if(seg.opt_w_seg().HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.opt_w_seg().weight_min() + clipped_l1 * seg.opt_w_seg().unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;
        special_coeff[2] = seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().unit_delta_weight() - this->inv_scale_2_ ;

        Scalar F0F1F2[3];
        HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_segment_FGradF<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                              seg.opt_w_seg().unit_delta_weight(),
                                                                                              special_coeff, F0F1F2);
        value_res = F0F1F2[0];
        F0F1F2[1] *= inv_local_min_weight;
        grad_res = (this->degree_r()*this->inv_scale_2_*inv_local_min_weight)
                * (  (F0F1F2[2] + clipped_l1 * F0F1F2[1]) * seg.opt_w_seg().increase_unit_dir()
                -  F0F1F2[1] * p_min_to_point );
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
// Special Segment evaluation ( for Gradient-based Integral Surfaces) -----
// WARNING : Homothetic field value is not normalized ---------------------
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticCompactPolynomial<DEGREE>::EvalHomotheticValues(const OptWeightedSegment& seg, const Point& point,
                                                        Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
    const Vector p_min_to_point( point-seg.p_min() );
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();
    
    Scalar special_coeff[3] = { seg.weight_min()*seg.weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.unit_delta_weight()*seg.weight_min()     - this->inv_scale_2_ * uv ,
                                seg.unit_delta_weight()*seg.unit_delta_weight() - this->inv_scale_2_ };
    
    Scalar clipped_l1, clipped_l2;
    if(seg.HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.weight_min() + clipped_l1 * seg.unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        Scalar F0F1F2[3];
        HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_segment_ScalisF_HornusGradF<DEGREE>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                           seg.unit_delta_weight(),
                                                                                                           special_coeff, F0F1F2);

        homothetic_value_res = F0F1F2[0];
        F0F1F2[1] *= inv_local_min_weight;
        homothetic_grad_res = (this->normalization_factor_1D_*this->degree_r()*this->inv_scale_2_*inv_local_min_weight)
                * (   (F0F1F2[2] + clipped_l1 * F0F1F2[1]) * seg.increase_unit_dir()
                -  F0F1F2[1] * p_min_to_point ); //TODO:@todo : formula to be checked
    }
    else
    {
        homothetic_value_res = 0.0;
        homothetic_grad_res = Vector::Zero();
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticCompactPolynomial<DEGREE>::EvalHomotheticValues( const Point& p_1, Scalar w_1,
                                                                const Vector& unit_dir, Scalar length,
                                                                const Point& point,
                                                                Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
    //TODO:@todo : to be implemented : should use degree i-1
    assert(false);
    UNUSED(p_1); UNUSED(w_1); UNUSED(unit_dir); UNUSED(length); UNUSED(point);
    homothetic_value_res = 0.0;
    homothetic_grad_res = Vector::Zero();
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
template <int DEGREE>
Scalar HomotheticCompactPolynomial<DEGREE>::Eval(const CsteWeightedSegmentWithDensity& seg, const Point& point) const
{
    return seg.density() * this->normalization_factor_1D_
           * Eval(seg.p1(), seg.weight(), seg.unit_dir(), seg.length(), point);
}
//-------------------------------------------------------------------------
template <int DEGREE>
Vector HomotheticCompactPolynomial<DEGREE>::EvalGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar epsilon) const
{
    Scalar f_tmp; Vector grad_res;
    EvalValueAndGrad(seg.p1(), seg.weight(),
                     seg.unit_dir(), seg.length(),
                     point, epsilon, f_tmp, grad_res);
    return (seg.density() * this->normalization_factor_1D_)*grad_res;
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticCompactPolynomial<DEGREE>::EvalValueAndGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar epsilon_grad,
                      Scalar& value_res, Vector& grad_res) const
{
    EvalValueAndGrad(seg.p1(), seg.weight(),
                     seg.unit_dir(), seg.length(),
                     point, epsilon_grad, value_res, grad_res);
    Scalar factor = seg.density() * this->normalization_factor_1D_;
    value_res *= factor;
    grad_res *= factor;
}
//-------------------------------------------------------------------------



//////////////////////////
/// Bounding functions ///
//////////////////////////

template <int DEGREE>
Scalar HomotheticCompactPolynomial<DEGREE>::GetIsoValueForThickness(Scalar length, Scalar thickness) const
{
    if(this->scale_ > thickness)
    {
        const Scalar sqr_thickness = thickness*thickness;
        const Scalar d = sqrt(this->scale_*this->scale_ - sqr_thickness);
        const Scalar l_clipped = (d < 0.5*length) ? d : 0.5*length;

        Scalar special_coeff[3] = {1.0 - this->inv_scale_2_* sqr_thickness,
                                   0.0,
                                   - this->inv_scale_2_ * l_clipped*l_clipped };

        return 2.0 * this->normalization_factor_1D_ * l_clipped * HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_approx_segment_F<6>(1.0,0.0,l_clipped,special_coeff);
    }
    else
    {
        return 0.0;
    }
}

} // Close namespace convol

} // Close namespace expressive

