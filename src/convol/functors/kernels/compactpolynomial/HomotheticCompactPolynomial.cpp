#include <convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

static core::FunctorFactory::Type<HomotheticCompactPolynomial<4> > HomotheticCompactPolynomial4Type;
static core::FunctorFactory::Type<HomotheticCompactPolynomial<6> > HomotheticCompactPolynomial6Type;
static core::FunctorFactory::Type<HomotheticCompactPolynomial<8> > HomotheticCompactPolynomial8Type;

///////////////////////////////////////////////////////////////////////
/// TODO:@todo : temporary implementation waiting for rearrangement ///
///////////////////////////////////////////////////////////////////////

template <>
Scalar HomotheticCompactPolynomial<8>::Eval(const OptWeightedSegment& seg, const Point& point) const
{
    const Vector p_min_to_point( point-seg.p_min() );
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();

    Scalar special_coeff[3] = { seg.weight_min()*seg.weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.unit_delta_weight()*seg.weight_min()     - this->inv_scale_2_ * uv ,
                                seg.unit_delta_weight()*seg.unit_delta_weight() - this->inv_scale_2_ };

    Scalar clipped_l1, clipped_l2;
    if(seg.HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.weight_min() + clipped_l1 * seg.unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        if(seg.unit_delta_weight() >= 0.045) { // ensure a maximum relative error of ??? (for degree i up to 8)
            return   normalization_factor_1D_
                    * HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_segment_F<8>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                     seg.unit_delta_weight(),special_coeff);
        }else{
            return  normalization_factor_1D_
                   * HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_approx_segment_F<8>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                           seg.unit_delta_weight(), inv_local_min_weight, special_coeff);
        }
    }
    else
    {
        return 0.0;
    }
}
//-------------------------------------------------------------------------
template <>
Vector HomotheticCompactPolynomial<8>::EvalGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const
{
    const Vector p_min_to_point( point-seg.p_min() );
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();

    Scalar special_coeff[3] = { seg.weight_min()*seg.weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.unit_delta_weight()*seg.weight_min()     - this->inv_scale_2_ * uv ,
                                seg.unit_delta_weight()*seg.unit_delta_weight() - this->inv_scale_2_ };

    Scalar clipped_l1, clipped_l2;
    if(seg.HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.weight_min() + clipped_l1 * seg.unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        Scalar F0F1[2];
        if(seg.unit_delta_weight() >= 0.045) { // ensure a maximum relative error of ??? (for degree i up to 8)
            HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_segment_GradF<8>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                              seg.unit_delta_weight(),
                                                                                              special_coeff, F0F1);
        }else{
            HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_approx_segment_GradF<8>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                     seg.unit_delta_weight(), inv_local_min_weight,
                                                                                                     special_coeff, F0F1);
        }
        F0F1[0] *= inv_local_min_weight;
        return (this->normalization_factor_1D_*this->degree_r()*this->inv_scale_2_*inv_local_min_weight)
                * (   (F0F1[1] + clipped_l1 * F0F1[0]) * seg.increase_unit_dir()
                -  F0F1[0] * p_min_to_point );
    }
    else
    {
        return Vector::Zero();
    }
}
//-------------------------------------------------------------------------
template <>
void HomotheticCompactPolynomial<8>::EvalValueAndGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                                                    Scalar& value_res, Vector& grad_res) const
{
    const Vector p_min_to_point( point-seg.p_min() );
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();

    Scalar special_coeff[3] = { seg.weight_min()*seg.weight_min()              - this->inv_scale_2_ * d2 ,
                                - seg.unit_delta_weight()*seg.weight_min()     - this->inv_scale_2_ * uv ,
                                seg.unit_delta_weight()*seg.unit_delta_weight() - this->inv_scale_2_ };

    Scalar clipped_l1, clipped_l2;
    if(seg.HomotheticClippingSpecial(special_coeff, clipped_l1, clipped_l2))
    {
        Scalar inv_local_min_weight = 1.0 / (seg.weight_min() + clipped_l1 * seg.unit_delta_weight());
        special_coeff[0] = 1.0 - this->inv_scale_2_ * ( clipped_l1*(clipped_l1-2.0*uv) + d2 ) * inv_local_min_weight*inv_local_min_weight;
        special_coeff[1] = - seg.unit_delta_weight() - this->inv_scale_2_*(uv-clipped_l1)     * inv_local_min_weight;

        Scalar F0F1F2[3];
        if(seg.unit_delta_weight() >= 0.045) { // ensure a maximum relative error of ??? (for degree i up to 8)
            HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_segment_FGradF<8>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                               seg.unit_delta_weight(),
                                                                                               special_coeff, F0F1F2);
        }else{
            HomotheticCompactPolynomialOptim::HomotheticCompactPolynomial_approx_segment_FGradF<8>( (clipped_l2-clipped_l1) * inv_local_min_weight,
                                                                                                      seg.unit_delta_weight(), seg.inv_weight_min(),
                                                                                                      special_coeff, F0F1F2);
        }
        value_res = this->normalization_factor_1D_ * F0F1F2[0];
        F0F1F2[1] *= inv_local_min_weight;
        grad_res = (this->normalization_factor_1D_*this->degree_r()*this->inv_scale_2_*inv_local_min_weight)
                * (  (F0F1F2[2] + clipped_l1 * F0F1F2[1]) * seg.increase_unit_dir()
                -  F0F1F2[1] * p_min_to_point );
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}





} // Close namespace convol

} // Close namespace expressive


