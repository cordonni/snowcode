
namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////

template<int DEGREE>
Scalar CompactPolynomial<DEGREE>::Eval(Scalar r) const
{
    const Scalar aux = 1.0-inv_scale_2_*r*r;

    if(aux > 0.0)
    {
        return pow(aux, 0.5*degree_r());
    }
    else
    {
        return 0.0;
    }
}
template<int DEGREE>
Scalar CompactPolynomial<DEGREE>::EvalGrad(Scalar r, Scalar /*epsilon*/) const
{
    const Scalar aux = 1.0-inv_scale_2_*r*r;

    if(aux > 0.0)
    {
        return -inv_scale_2_*r*degree_r()*pow(aux, 0.5*degree_r()-1.0);
    }
    else
    {
        return 0.0;
    }
}
template<int DEGREE>
Scalar CompactPolynomial<DEGREE>::InverseFct(Scalar y) const
{
    if(y>0.0)
    {
        return scale_ * sqrt(1.0-pow(y,2.0/degree_r()));
    }
    else
    {
        return expressive::kScalarMax;
    }
}

//-----------------------------------------------------------------------------
template<int DEGREE>
bool operator==(const CompactPolynomial<DEGREE>& a, const CompactPolynomial<DEGREE>& b)
{
    return ( a.scale() == b.scale() );
}

} // Close namespace convol

} // Close namespace expressive
