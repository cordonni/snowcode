#include <convol/functors/kernels/compactpolynomial/StandardCompactPolynomial.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

static core::FunctorFactory::Type<StandardCompactPolynomial<4> > StandardCompactPolynomial4Type;
static core::FunctorFactory::Type<StandardCompactPolynomial<6> > StandardCompactPolynomial6Type;
static core::FunctorFactory::Type<StandardCompactPolynomial<8> > StandardCompactPolynomial8Type;

} // Close namespace convol

} // Close namespace expressive
