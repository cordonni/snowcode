/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: HomotheticCompactPolynomialOptimizedFunctions.h
  
   Language: C++
  
   License: Convol Licence
  
   \author: Cedric Zanni 
   E-Mail: cedric.zanni@inria.fr
  
   Description: Header files defining optimized function for Homothetic CompactPolynomialX kernels.
 
   Platform Dependencies: None 
*/  

#pragma once
#ifndef CONVOL_HOMOTHETIC_COMPACT_POLYNOMIAL_OPTIMIZED_FUNCTIONS_H_
#define CONVOL_HOMOTHETIC_COMPACT_POLYNOMIAL_OPTIMIZED_FUNCTIONS_H_

// Convol dependencies
#include<core/CoreRequired.h>

// std dependencies
#include<vector>
#include<array>

#include<math.h>

namespace expressive {

namespace convol {

namespace HomotheticCompactPolynomialOptim {

template<int I>
inline Scalar HomotheticCompactPolynomial_segment_F (Scalar l, Scalar d, const Scalar w[3]);
template<int I>
inline void HomotheticCompactPolynomial_segment_GradF (Scalar l, Scalar d, const Scalar w[3], Scalar res[2]);
template<int I>
inline void HomotheticCompactPolynomial_segment_FGradF (Scalar l, Scalar d, const Scalar w[3], Scalar res[3]);


template<int I>
inline Scalar HomotheticCompactPolynomial_approx_segment_F (Scalar l, Scalar d, Scalar q, const Scalar w[3]);
template<int I>
inline void HomotheticCompactPolynomial_approx_segment_GradF (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[2]);
template<int I>
inline void HomotheticCompactPolynomial_approx_segment_FGradF (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3]);

template<int I>
inline Scalar HomotheticCompactPolynomial_segment_F_cste (Scalar l, const Scalar w[3]);
template<int I>
inline void HomotheticCompactPolynomial_segment_FGradF_cste (Scalar l, const Scalar w[3], Scalar res[3]);

template<int I>
inline Scalar HomotheticCompactPolynomial_density_segment_F (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g);
template<int I>
inline void HomotheticCompactPolynomial_density_segment_GradF (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[2]);
template<int I>
inline void HomotheticCompactPolynomial_density_segment_FGradF (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[3]);

template<int I>
inline Scalar HomotheticCompactPolynomial_density_approx_segment_F (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g);
template<int I>
inline void HomotheticCompactPolynomial_density_approx_segment_GradF (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[2]);
template<int I>
inline void HomotheticCompactPolynomial_density_approx_segment_FGradF (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[3]);

template<int I>
inline void HomotheticCompactPolynomial_segment_ScalisF_HornusGradF (Scalar l, Scalar d, const Scalar w[3], Scalar res[3]);
template<int I>
inline void HomotheticCompactPolynomial_approx_segment_ScalisF_HornusGradF (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3]);





    /////////////////////////////////
    // Code for kernel of degree 4 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HomotheticCompactPolynomial_segment_F<4> (Scalar l, Scalar d, const Scalar w[3])
    {
        const Scalar t914 = d * l + 0.1e1;
        const Scalar t910 = 0.1e1 / t914;
        const Scalar t923 = t914 * t914;
        const Scalar t911 = 0.1e1 / t923;
        const Scalar t920 = 0.1e1 / d;
        const Scalar t919 = l * l;
        const Scalar t918 = l * t919;
        const Scalar t917 = w[0];
        const Scalar t916 = w[1];
        const Scalar t915 = w[2];
        const Scalar t2 = t923 * t923;
        const Scalar t913 = 0.1e1 / t2;
        const Scalar t912 = t910 * t911;
        const Scalar t3 = t917 * t917;
        const Scalar t10 = t916 * t916;
        const Scalar t23 = t915 * t915;
        const Scalar t24 = log(t914);
        const Scalar t38 = t919 * t919;
        return  -t3 * (t913 - 0.1e1) * t920 / 0.4e1 + ((0.2e1 * t915 * t917 + 0.4e1 * t10) * (0.2e1 / 0.3e1 * (-(t911 - 0.1e1) * t920 / 0.2e1 - l * t912) * t920 - t919 * t913) + t23 * (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * (0.2e1 * (t24 * t920 - l * t910) * t920 - t919 * t911) * t920 - t918 * t912) * t920 - t38 * t913)) * t920 / 0.4e1 + (-t917 * (-(t912 - 0.1e1) * t920 / 0.3e1 - l * t913) - t915 * (((-(t910 - 0.1e1) * t920 - l * t911) * t920 - t919 * t912) * t920 - t918 * t913)) * t916 * t920;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_segment_GradF<4> (Scalar l, Scalar d, const Scalar w[3], Scalar res[2])
    {
        const Scalar t933 = d * l + 0.1e1;
        const Scalar t946 = 0.1e1 / t933;
        const Scalar t940 = t933 * t933;
        const Scalar t930 = 0.1e1 / t940;
        const Scalar t938 = 0.1e1 / d;
        const Scalar t945 = -t938 / 0.2e1;
        const Scalar t3 = t940 * t940;
        const Scalar t932 = 0.1e1 / t3;
        const Scalar t937 = l * l;
        const Scalar t943 = t937 * t932;
        const Scalar t936 = w[0];
        const Scalar t935 = w[1];
        const Scalar t934 = w[2];
        const Scalar t931 = t946 * t930;
        const Scalar t929 = -(t931 - 0.1e1) * t938 / 0.3e1 - l * t932;
        const Scalar t928 = 0.2e1 / 0.3e1 * ((t930 - 0.1e1) * t945 - l * t931) * t938 - t943;
        res[0] = (-t936 * (t932 - 0.1e1) / 0.4e1 - t935 * t929 / 0.2e1 + t934 * t928 / 0.4e1) * t938;
        res[1] = t935 * t928 * t945 + (t936 * t929 + t934 * (((-(t946 - 0.1e1) * t938 - l * t930) * t938 - t937 * t931) * t938 - l * t943)) * t938 / 0.4e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_segment_FGradF<4> (Scalar l, Scalar d, const Scalar w[3], Scalar res[3])
    {
        const Scalar t955 = d * l + 0.1e1;
        const Scalar t951 = 0.1e1 / t955;
        const Scalar t964 = t955 * t955;
        const Scalar t952 = 0.1e1 / t964;
        const Scalar t2 = t964 * t964;
        const Scalar t954 = 0.1e1 / t2;
        const Scalar t970 = -t954 / 0.4e1 + 0.1e1 / 0.4e1;
        const Scalar t956 = w[2];
        const Scalar t969 = t956 / 0.4e1;
        const Scalar t953 = t951 * t952;
        const Scalar t960 = l * l;
        const Scalar t961 = 0.1e1 / d;
        const Scalar t948 = 0.2e1 / 0.3e1 * (-(t952 - 0.1e1) * t961 / 0.2e1 - l * t953) * t961 - t960 * t954;
        const Scalar t957 = w[1];
        const Scalar t968 = t957 * t948;
        const Scalar t949 = -(t953 - 0.1e1) * t961 / 0.3e1 - l * t954;
        const Scalar t958 = w[0];
        const Scalar t967 = t958 * t949;
        const Scalar t959 = l * t960;
        const Scalar t947 = ((-(t951 - 0.1e1) * t961 - l * t952) * t961 - t960 * t953) * t961 - t959 * t954;
        const Scalar t25 = t958 * t958;
        const Scalar t36 = log(t955);
        const Scalar t50 = t960 * t960;
        res[0] = (t25 * t970 + (-t967 + t968) * t957) * t961 + (t958 * t948 * t961 / 0.2e1 - t957 * t947 * t961 + (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * (0.2e1 * (t36 * t961 - l * t951) * t961 - t960 * t952) * t961 - t959 * t953) * t961 - t50 * t954) * t961 * t969) * t956;
        res[1] = (t958 * t970 - t957 * t949 / 0.2e1 + t948 * t969) * t961;
        res[2] = (t967 / 0.4e1 - t968 / 0.2e1 + t947 * t969) * t961;

    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_approx_segment_F<4> (Scalar l, Scalar d, Scalar q, const Scalar w[3])
    {
        const Scalar t999 = q * d;
        const Scalar t980 = t999 + 0.1e1;
        const Scalar t1000 = 0.1e1 / t980;
        const Scalar t1 = t980 * t980;
        const Scalar t2 = t1 * t1;
        const Scalar t978 = t1000 / t2;
        const Scalar t982 = w[2];
        const Scalar t983 = w[1];
        const Scalar t984 = w[0];
        const Scalar t985 = l * l;
        const Scalar t975 = t982 * t985 - 0.2e1 * t983 * l + t984;
        const Scalar t8 = t983 * t983;
        const Scalar t976 = t982 * t984 - t8;
        const Scalar t977 = t982 * l - t983;
        const Scalar t981 = 0.1e1 / t982;
        const Scalar t988 = t975 * t975;
        const Scalar t994 = t984 * t984;
        const Scalar t998 = 0.4e1 / 0.15e2 * t976 * (0.2e1 * t976 * l + t977 * t975 + t983 * t984) * t981 + t977 * t988 / 0.5e1 + t983 * t994 / 0.5e1;
        const Scalar t996 = t981 * t998;
        const Scalar t979 = t1000 * t978;
        const Scalar t974 = t975 * t988;
        const Scalar t972 = t983 * t996 + t974 / 0.6e1 - t984 * t994 / 0.6e1;
        const Scalar t971 = -l * t974 + (-0.8e1 * t983 * t972 + t984 * t998) * t981;
        const Scalar t62 = q * q;
        return  t996 - 0.5e1 * d * t972 * t981 + (-0.1428571429e0 * (0.3e1 * t978 - 0.300e1 + 0.5e1 * (0.2e1 + t979) * t999) * t971 - 0.1250000000e0 * (0.2e1 - 0.200e1 * t978 - 0.5e1 * (0.1e1 + t979) * t999) / q * (-0.1e1 * t985 * t974 + (0.1428571429e1 * t983 * t971 + 0.2e1 * t984 * t972) * t981)) * t981 / t62;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_approx_segment_GradF<4> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[2])
    {
        const Scalar t1031 = q * d;
        const Scalar t1011 = t1031 + 0.1e1;
        const Scalar t1032 = 0.1e1 / t1011;
        const Scalar t1 = q * q;
        const Scalar t1016 = 0.1e1 / t1;
        const Scalar t2 = t1011 * t1011;
        const Scalar t3 = t2 * t2;
        const Scalar t1009 = t1032 / t3;
        const Scalar t1013 = w[2];
        const Scalar t1014 = w[1];
        const Scalar t1015 = w[0];
        const Scalar t1018 = l * l;
        const Scalar t1008 = t1013 * t1018 - 0.2e1 * t1014 * l + t1015;
        const Scalar t1007 = t1008 * t1008;
        const Scalar t1012 = 0.1e1 / t1013;
        const Scalar t9 = t1014 * t1014;
        const Scalar t1029 = 0.2e1 / 0.3e1 * (t1013 * t1015 - t9) * l + (t1013 * l - t1014) * t1008 / 0.3e1 + t1014 * t1015 / 0.3e1;
        const Scalar t22 = t1015 * t1015;
        const Scalar t1003 = t1014 * t1012 * t1029 + t1007 / 0.4e1 - t22 / 0.4e1;
        const Scalar t1002 = -l * t1007 + (-0.6e1 * t1014 * t1003 + t1015 * t1029) * t1012;
        const Scalar t1026 = t1018 * t1007;
        const Scalar t1001 = -t1026 + (0.8e1 / 0.5e1 * t1014 * t1002 + 0.2e1 * t1015 * t1003) * t1012;
        const Scalar t1030 = -t1001 / 0.6e1;
        const Scalar t1010 = t1032 * t1009;
        const Scalar t1028 = (0.3e1 * t1009 - 0.300e1 + 0.5e1 * (0.2e1 + t1010) * t1031) * t1016;
        const Scalar t1027 = (0.2e1 - 0.200e1 * t1009 - 0.5e1 * (0.1e1 + t1010) * t1031) / q * t1016;
        res[0] = (t1029 - 0.5e1 * d * t1003 - t1002 * t1028 / 0.5e1 + t1027 * t1030) * t1012;
        res[1] = (t1003 + d * t1002 + t1028 * t1030 + (l * t1026 / 0.7e1 - (0.5e1 / 0.3e1 * t1014 * t1001 - 0.3e1 / 0.5e1 * t1015 * t1002) * t1012 / 0.7e1) * t1027) * t1012;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_approx_segment_FGradF<4> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3])
    {
        const Scalar t1079 = q * d;
        const Scalar t1049 = t1079 + 0.1e1;
        const Scalar t1080 = 0.1e1 / t1049;
        const Scalar t1 = q * q;
        const Scalar t1055 = 0.1e1 / t1;
        const Scalar t2 = t1049 * t1049;
        const Scalar t3 = t2 * t2;
        const Scalar t1047 = t1080 / t3;
        const Scalar t1054 = w[0];
        const Scalar t1078 = 0.2e1 * t1054;
        const Scalar t1052 = w[2];
        const Scalar t1050 = 0.1e1 / t1052;
        const Scalar t1077 = d * t1050;
        const Scalar t1053 = w[1];
        const Scalar t1057 = l * l;
        const Scalar t1044 = t1052 * t1057 - 0.2e1 * t1053 * l + t1054;
        const Scalar t1043 = t1044 * t1044;
        const Scalar t1051 = t1054 * t1054;
        const Scalar t9 = t1053 * t1053;
        const Scalar t1045 = t1052 * t1054 - t9;
        const Scalar t1046 = t1052 * l - t1053;
        const Scalar t1039 = 0.2e1 * t1045 * l + t1046 * t1044 + t1053 * t1054;
        const Scalar t1073 = t1039 * t1050;
        const Scalar t1038 = t1053 * t1073 / 0.3e1 + t1043 / 0.4e1 - t1051 / 0.4e1;
        const Scalar t1074 = t1039 / 0.3e1;
        const Scalar t1035 = -l * t1043 + (-0.6e1 * t1053 * t1038 + t1054 * t1074) * t1050;
        const Scalar t1070 = t1057 * t1043;
        const Scalar t1033 = -t1070 + (0.8e1 / 0.5e1 * t1053 * t1035 + t1038 * t1078) * t1050;
        const Scalar t1076 = -t1033 / 0.6e1;
        const Scalar t1075 = 0.4e1 / 0.15e2 * t1045 * t1073 + t1046 * t1043 / 0.5e1 + t1053 * t1051 / 0.5e1;
        const Scalar t1048 = t1080 * t1047;
        const Scalar t1072 = (0.3e1 * t1047 - 0.300e1 + 0.5e1 * (0.2e1 + t1048) * t1079) * t1055;
        const Scalar t1071 = (0.2e1 - 0.200e1 * t1047 - 0.5e1 * (0.1e1 + t1048) * t1079) / q * t1055;
        const Scalar t1069 = t1050 * t1072;
        const Scalar t1068 = t1050 * t1071;
        const Scalar t1067 = t1050 * t1075;
        const Scalar t1042 = t1044 * t1043;
        const Scalar t1036 = t1053 * t1067 + t1042 / 0.6e1 - t1054 * t1051 / 0.6e1;
        const Scalar t1034 = -l * t1042 + (-0.8e1 * t1053 * t1036 + t1054 * t1075) * t1050;
        res[0] = t1067 - 0.5e1 * t1036 * t1077 - t1034 * t1069 / 0.7e1 - (-t1057 * t1042 + (0.10e2 / 0.7e1 * t1053 * t1034 + t1036 * t1078) * t1050) * t1068 / 0.8e1;
        res[1] = (t1074 - 0.5e1 * d * t1038 - t1035 * t1072 / 0.5e1 + t1071 * t1076) * t1050;
        res[2] = t1038 * t1050 + t1035 * t1077 + t1069 * t1076 - (-l * t1070 + (0.5e1 / 0.3e1 * t1053 * t1033 - 0.3e1 / 0.5e1 * t1054 * t1035) * t1050) * t1068 / 0.7e1;

    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_segment_F_cste<4> (Scalar l, const Scalar w[3])
    {
        const Scalar t1085 = w[2];
        const Scalar t1088 = t1085 * l;
        const Scalar t1087 = w[0];
        const Scalar t1086 = w[1];
        const Scalar t1084 = 0.1e1 / t1085;
        const Scalar t1083 = t1088 - t1086;
        const Scalar t2 = t1086 * t1086;
        const Scalar t1082 = t1085 * t1087 - t2;
        const Scalar t1081 = t1087 + (-0.2e1 * t1086 + t1088) * l;
        const Scalar t14 = t1081 * t1081;
        const Scalar t16 = t1087 * t1087;
        return  (0.4e1 / 0.3e1 * t1082 * (0.2e1 * t1082 * l + t1083 * t1081 + t1086 * t1087) * t1084 + t1083 * t14 + t1086 * t16) * t1084 / 0.5e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_segment_FGradF_cste<4> (Scalar l, const Scalar w[3], Scalar res[3])
    {
        const Scalar t1096 = w[2];
        const Scalar t1101 = t1096 * l;
        const Scalar t1097 = w[1];
        const Scalar t1098 = w[0];
        const Scalar t1091 = t1098 + (-0.2e1 * t1097 + t1101) * l;
        const Scalar t5 = t1097 * t1097;
        const Scalar t1092 = t1096 * t1098 - t5;
        const Scalar t1093 = t1101 - t1097;
        const Scalar t1094 = 0.1e1 / t1096;
        const Scalar t1100 = (0.2e1 * t1092 * l + t1093 * t1091 + t1097 * t1098) * t1094;
        const Scalar t1099 = t1100 / 0.3e1;
        const Scalar t1095 = t1098 * t1098;
        const Scalar t1090 = t1091 * t1091;
        res[0] = (0.4e1 / 0.3e1 * t1092 * t1100 + t1093 * t1090 + t1097 * t1095) * t1094 / 0.5e1;
        res[1] = t1099;
        res[2] = (t1097 * t1099 + t1090 / 0.4e1 - t1095 / 0.4e1) * t1094;

    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_density_segment_F<4> (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g)
    {
        const Scalar t1107 = d * l + 0.1e1;
        const Scalar t1103 = 0.1e1 / t1107;
        const Scalar t1123 = t1107 * t1107;
        const Scalar t1104 = 0.1e1 / t1123;
        const Scalar t1112 = w[1];
        const Scalar t2 = t1112 * t1112;
        const Scalar t1133 = 0.4e1 * t2;
        const Scalar t1132 = -0.4e1 * t1112;
        const Scalar t1119 = 0.1e1 / d;
        const Scalar t1131 = 0.2e1 * t1119;
        const Scalar t4 = log(t1107);
        const Scalar t1129 = t4 * t1119;
        const Scalar t5 = t1123 * t1123;
        const Scalar t1106 = 0.1e1 / t5;
        const Scalar t1118 = l * l;
        const Scalar t1121 = t1118 * t1118;
        const Scalar t1128 = t1121 * t1106;
        const Scalar t1114 = g[1];
        const Scalar t1127 = t1114 * t1132;
        const Scalar t1115 = g[0];
        const Scalar t1126 = t1115 * t1132;
        const Scalar t1117 = l * t1118;
        const Scalar t1113 = w[0];
        const Scalar t1111 = w[2];
        const Scalar t1110 = t1113 * t1113;
        const Scalar t1108 = t1111 * t1111;
        const Scalar t1105 = t1103 * t1104;
        return  -t1115 * t1110 * (t1106 - 0.1e1) * t1119 / 0.4e1 + ((t1114 * t1110 + t1113 * t1126) * (-(t1105 - 0.1e1) * t1119 / 0.3e1 - l * t1106) + (t1115 * t1133 + (t1127 + 0.2e1 * t1115 * t1111) * t1113) * (0.2e1 / 0.3e1 * (-(t1104 - 0.1e1) * t1119 / 0.2e1 - l * t1105) * t1119 - t1118 * t1106) + (t1114 * t1133 + (0.2e1 * t1114 * t1113 + t1126) * t1111) * (((-(t1103 - 0.1e1) * t1119 - l * t1104) * t1119 - t1118 * t1105) * t1119 - t1117 * t1106) + (t1111 * t1127 + t1115 * t1108) * (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * ((t1129 - l * t1103) * t1131 - t1118 * t1104) * t1119 - t1117 * t1105) * t1119 - t1128) + t1114 * t1108 * (0.5e1 / 0.3e1 * ((0.3e1 * ((l - t1129) * t1131 - t1118 * t1103) * t1119 - t1117 * t1104) * t1131 - t1121 * t1105) * t1119 - l * t1128)) * t1119 / 0.4e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_segment_GradF<4> (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[2])
    {
        const Scalar t1143 = d * l + 0.1e1;
        const Scalar t1139 = 0.1e1 / t1143;
        const Scalar t1154 = t1143 * t1143;
        const Scalar t1140 = 0.1e1 / t1154;
        const Scalar t1160 = -0.2e1 * w[1];
        const Scalar t1146 = w[0];
        const Scalar t1148 = g[0];
        const Scalar t1158 = t1146 * t1148;
        const Scalar t1144 = w[2];
        const Scalar t1147 = g[1];
        const Scalar t1157 = t1147 * t1144 / 0.4e1;
        const Scalar t1151 = 0.1e1 / d;
        const Scalar t1150 = l * l;
        const Scalar t1149 = l * t1150;
        const Scalar t5 = t1154 * t1154;
        const Scalar t1142 = 0.1e1 / t5;
        const Scalar t1141 = t1139 * t1140;
        const Scalar t1138 = t1148 * t1144 + t1147 * t1160;
        const Scalar t1137 = t1147 * t1146 + t1148 * t1160;
        const Scalar t1136 = -(t1141 - 0.1e1) * t1151 / 0.3e1 - l * t1142;
        const Scalar t1135 = 0.2e1 / 0.3e1 * (-(t1140 - 0.1e1) * t1151 / 0.2e1 - l * t1141) * t1151 - t1150 * t1142;
        const Scalar t1134 = ((-(t1139 - 0.1e1) * t1151 - l * t1140) * t1151 - t1150 * t1141) * t1151 - t1149 * t1142;
        res[0] = (-(t1142 - 0.1e1) * t1158 / 0.4e1 + t1137 * t1136 / 0.4e1 + t1138 * t1135 / 0.4e1 + t1134 * t1157) * t1151;
        const Scalar t40 = log(t1143);
        const Scalar t54 = t1150 * t1150;
        res[1] = (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * (0.2e1 * (t40 * t1151 - l * t1139) * t1151 - t1150 * t1140) * t1151 - t1149 * t1141) * t1151 - t54 * t1142) * t1151 * t1157 + (t1136 * t1158 + t1137 * t1135 + t1138 * t1134) * t1151 / 0.4e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_segment_FGradF<4> (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[3])
    {
        const Scalar t1173 = d * l + 0.1e1;
        const Scalar t1169 = 0.1e1 / t1173;
        const Scalar t1189 = t1173 * t1173;
        const Scalar t1170 = 0.1e1 / t1189;
        const Scalar t1178 = w[1];
        const Scalar t2 = t1178 * t1178;
        const Scalar t1206 = 0.4e1 * t2;
        const Scalar t1185 = 0.1e1 / d;
        const Scalar t1205 = 0.2e1 * t1185;
        const Scalar t1181 = g[0];
        const Scalar t1197 = t1181 * t1178;
        const Scalar t1179 = w[0];
        const Scalar t1180 = g[1];
        const Scalar t1199 = t1180 * t1179;
        const Scalar t1204 = t1199 / 0.4e1 - t1197 / 0.2e1;
        const Scalar t1177 = w[2];
        const Scalar t1198 = t1181 * t1177;
        const Scalar t1200 = t1180 * t1178;
        const Scalar t1203 = t1198 / 0.4e1 - t1200 / 0.2e1;
        const Scalar t7 = log(t1173);
        const Scalar t1201 = t7 * t1185;
        const Scalar t8 = t1189 * t1189;
        const Scalar t1172 = 0.1e1 / t8;
        const Scalar t1184 = l * l;
        const Scalar t1187 = t1184 * t1184;
        const Scalar t1196 = t1187 * t1172;
        const Scalar t1195 = -0.4e1 * t1200;
        const Scalar t1194 = -0.4e1 * t1197;
        const Scalar t1193 = -(t1172 - 0.1e1) * t1181 / 0.4e1;
        const Scalar t1192 = t1180 * t1177 / 0.4e1;
        const Scalar t1183 = l * t1184;
        const Scalar t1176 = t1179 * t1179;
        const Scalar t1174 = t1177 * t1177;
        const Scalar t1171 = t1169 * t1170;
        const Scalar t1164 = -(t1171 - 0.1e1) * t1185 / 0.3e1 - l * t1172;
        const Scalar t1163 = 0.2e1 / 0.3e1 * (-(t1170 - 0.1e1) * t1185 / 0.2e1 - l * t1171) * t1185 - t1184 * t1172;
        const Scalar t1162 = ((-(t1169 - 0.1e1) * t1185 - l * t1170) * t1185 - t1184 * t1171) * t1185 - t1183 * t1172;
        const Scalar t1161 = 0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * ((t1201 - l * t1169) * t1205 - t1184 * t1170) * t1185 - t1183 * t1171) * t1185 - t1196;
        res[0] = t1176 * t1185 * t1193 + ((t1180 * t1176 + t1179 * t1194) * t1164 + (t1181 * t1206 + (t1195 + 0.2e1 * t1198) * t1179) * t1163 + (t1180 * t1206 + (0.2e1 * t1199 + t1194) * t1177) * t1162 + (t1177 * t1195 + t1181 * t1174) * t1161 + t1180 * t1174 * (0.5e1 / 0.3e1 * ((0.3e1 * ((l - t1201) * t1205 - t1184 * t1169) * t1185 - t1183 * t1170) * t1205 - t1187 * t1171) * t1185 - l * t1196)) * t1185 / 0.4e1;
        res[1] = (t1179 * t1193 + t1164 * t1204 + t1163 * t1203 + t1162 * t1192) * t1185;
        res[2] = (t1181 * t1179 * t1164 / 0.4e1 + t1163 * t1204 + t1162 * t1203 + t1161 * t1192) * t1185;

    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_density_approx_segment_F<4> (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g)
    {
        const Scalar t1245 = q * d;
        const Scalar t1219 = t1245 + 0.1e1;
        const Scalar t1247 = 0.1e1 / t1219;
        const Scalar t1246 = -0.5e1 * d;
        const Scalar t2 = q * q;
        const Scalar t1226 = 0.1e1 / t2;
        const Scalar t3 = t1219 * t1219;
        const Scalar t4 = t3 * t3;
        const Scalar t1217 = t1247 / t4;
        const Scalar t1221 = w[2];
        const Scalar t1222 = w[1];
        const Scalar t1223 = w[0];
        const Scalar t1228 = l * l;
        const Scalar t1214 = t1221 * t1228 - 0.2e1 * t1222 * l + t1223;
        const Scalar t10 = t1222 * t1222;
        const Scalar t1215 = t1221 * t1223 - t10;
        const Scalar t1216 = t1221 * l - t1222;
        const Scalar t1220 = 0.1e1 / t1221;
        const Scalar t1232 = t1214 * t1214;
        const Scalar t1238 = t1223 * t1223;
        const Scalar t1244 = 0.4e1 / 0.15e2 * t1215 * (0.2e1 * t1215 * l + t1216 * t1214 + t1222 * t1223) * t1220 + t1216 * t1232 / 0.5e1 + t1222 * t1238 / 0.5e1;
        const Scalar t1218 = t1247 * t1217;
        const Scalar t1243 = (0.2e1 - 0.200e1 * t1217 - 0.5e1 * (0.1e1 + t1218) * t1245) / q * t1226;
        const Scalar t1242 = (0.3e1 * t1217 - 0.300e1 + 0.5e1 * (0.2e1 + t1218) * t1245) * t1226;
        const Scalar t1213 = t1214 * t1232;
        const Scalar t1241 = t1228 * t1213;
        const Scalar t1240 = t1220 * t1244;
        const Scalar t1225 = g[0];
        const Scalar t1224 = g[1];
        const Scalar t1209 = t1222 * t1240 + t1213 / 0.6e1 - t1223 * t1238 / 0.6e1;
        const Scalar t1208 = -l * t1213 + (-0.8e1 * t1222 * t1209 + t1223 * t1244) * t1220;
        const Scalar t1207 = -t1241 + (0.10e2 / 0.7e1 * t1222 * t1208 + 0.2e1 * t1223 * t1209) * t1220;
        return  t1225 * t1240 + (t1225 * t1246 + t1224) * t1209 * t1220 - (t1225 * t1242 + t1224 * t1246) * t1208 * t1220 / 0.7e1 - (t1225 * t1243 + t1224 * t1242) * t1207 * t1220 / 0.8e1 - t1224 * (-l * t1241 + (0.3e1 / 0.2e1 * t1222 * t1207 - 0.3e1 / 0.7e1 * t1223 * t1208) * t1220) * t1220 * t1243 / 0.9e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_approx_segment_GradF<4> (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[2])
    {
        const Scalar t1288 = q * d;
        const Scalar t1262 = t1288 + 0.1e1;
        const Scalar t1290 = 0.1e1 / t1262;
        const Scalar t1289 = -0.5e1 * d;
        const Scalar t2 = q * q;
        const Scalar t1269 = 0.1e1 / t2;
        const Scalar t3 = t1262 * t1262;
        const Scalar t4 = t3 * t3;
        const Scalar t1260 = t1290 / t4;
        const Scalar t1264 = w[2];
        const Scalar t1265 = w[1];
        const Scalar t1266 = w[0];
        const Scalar t1271 = l * l;
        const Scalar t1258 = t1264 * t1271 - 0.2e1 * t1265 * l + t1266;
        const Scalar t1257 = t1258 * t1258;
        const Scalar t1263 = 0.1e1 / t1264;
        const Scalar t10 = t1265 * t1265;
        const Scalar t1284 = 0.2e1 / 0.3e1 * (t1264 * t1266 - t10) * l + (t1264 * l - t1265) * t1258 / 0.3e1 + t1265 * t1266 / 0.3e1;
        const Scalar t23 = t1266 * t1266;
        const Scalar t1251 = t1265 * t1263 * t1284 + t1257 / 0.4e1 - t23 / 0.4e1;
        const Scalar t1250 = -l * t1257 + (-0.6e1 * t1265 * t1251 + t1266 * t1284) * t1263;
        const Scalar t1281 = t1271 * t1257;
        const Scalar t1249 = -t1281 + (0.8e1 / 0.5e1 * t1265 * t1250 + 0.2e1 * t1266 * t1251) * t1263;
        const Scalar t1248 = -l * t1281 + (0.5e1 / 0.3e1 * t1265 * t1249 - 0.3e1 / 0.5e1 * t1266 * t1250) * t1263;
        const Scalar t1287 = -t1248 / 0.7e1;
        const Scalar t1286 = -t1249 / 0.6e1;
        const Scalar t1285 = -t1250 / 0.5e1;
        const Scalar t1261 = t1290 * t1260;
        const Scalar t1283 = (0.2e1 - 0.200e1 * t1260 - 0.5e1 * (0.1e1 + t1261) * t1288) / q * t1269;
        const Scalar t1282 = (0.3e1 * t1260 - 0.300e1 + 0.5e1 * (0.2e1 + t1261) * t1288) * t1269;
        const Scalar t1267 = g[1];
        const Scalar t1280 = t1267 * t1283;
        const Scalar t1268 = g[0];
        const Scalar t1259 = t1268 * t1289 + t1267;
        const Scalar t1254 = t1268 * t1282 + t1267 * t1289;
        const Scalar t1252 = t1268 * t1283 + t1267 * t1282;
        res[0] = (t1268 * t1284 + t1259 * t1251 + t1254 * t1285 + t1252 * t1286 + t1280 * t1287) * t1263;
        const Scalar t74 = t1271 * t1271;
        res[1] = (t1268 * t1251 + t1259 * t1285 + t1254 * t1286 + t1252 * t1287 + (t74 * t1257 / 0.8e1 - (0.12e2 / 0.7e1 * t1265 * t1248 - 0.2e1 / 0.3e1 * t1266 * t1249) * t1263 / 0.8e1) * t1280) * t1263;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_approx_segment_FGradF<4> (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[3])
    {
        const Scalar t1349 = q * d;
        const Scalar t1312 = t1349 + 0.1e1;
        const Scalar t1351 = 0.1e1 / t1312;
        const Scalar t1350 = -0.5e1 * d;
        const Scalar t2 = q * q;
        const Scalar t1320 = 0.1e1 / t2;
        const Scalar t3 = t1312 * t1312;
        const Scalar t4 = t3 * t3;
        const Scalar t1310 = t1351 / t4;
        const Scalar t1317 = w[0];
        const Scalar t1348 = 0.2e1 * t1317;
        const Scalar t1315 = w[2];
        const Scalar t1316 = w[1];
        const Scalar t1323 = l * l;
        const Scalar t1306 = t1315 * t1323 - 0.2e1 * t1316 * l + t1317;
        const Scalar t1305 = t1306 * t1306;
        const Scalar t1314 = t1317 * t1317;
        const Scalar t10 = t1316 * t1316;
        const Scalar t1307 = t1315 * t1317 - t10;
        const Scalar t1309 = t1315 * l - t1316;
        const Scalar t1300 = 0.2e1 * t1307 * l + t1309 * t1306 + t1316 * t1317;
        const Scalar t1313 = 0.1e1 / t1315;
        const Scalar t1341 = t1300 * t1313;
        const Scalar t1298 = t1316 * t1341 / 0.3e1 + t1305 / 0.4e1 - t1314 / 0.4e1;
        const Scalar t1343 = t1300 / 0.3e1;
        const Scalar t1295 = -l * t1305 + (-0.6e1 * t1316 * t1298 + t1317 * t1343) * t1313;
        const Scalar t1293 = -t1323 * t1305 + (0.8e1 / 0.5e1 * t1316 * t1295 + t1298 * t1348) * t1313;
        const Scalar t1322 = l * t1323;
        const Scalar t1291 = -t1322 * t1305 + (0.5e1 / 0.3e1 * t1316 * t1293 - 0.3e1 / 0.5e1 * t1317 * t1295) * t1313;
        const Scalar t1347 = -t1291 / 0.7e1;
        const Scalar t1346 = -t1293 / 0.6e1;
        const Scalar t1345 = -t1295 / 0.5e1;
        const Scalar t1344 = 0.4e1 / 0.15e2 * t1307 * t1341 + t1309 * t1305 / 0.5e1 + t1316 * t1314 / 0.5e1;
        const Scalar t1318 = g[1];
        const Scalar t1319 = g[0];
        const Scalar t1311 = t1351 * t1310;
        const Scalar t1338 = (0.3e1 * t1310 - 0.300e1 + 0.5e1 * (0.2e1 + t1311) * t1349) * t1320;
        const Scalar t1339 = (0.2e1 - 0.200e1 * t1310 - 0.5e1 * (0.1e1 + t1311) * t1349) / q * t1320;
        const Scalar t1299 = t1319 * t1339 + t1318 * t1338;
        const Scalar t1342 = t1299 * t1313;
        const Scalar t1301 = t1319 * t1338 + t1318 * t1350;
        const Scalar t1340 = t1301 * t1313;
        const Scalar t1308 = t1319 * t1350 + t1318;
        const Scalar t1337 = t1308 * t1313;
        const Scalar t1336 = t1318 * t1339;
        const Scalar t1335 = t1313 * t1344;
        const Scalar t1334 = t1313 * t1336;
        const Scalar t1304 = t1306 * t1305;
        const Scalar t1296 = t1316 * t1335 + t1304 / 0.6e1 - t1317 * t1314 / 0.6e1;
        const Scalar t1294 = -l * t1304 + (-0.8e1 * t1316 * t1296 + t1317 * t1344) * t1313;
        const Scalar t1292 = -t1323 * t1304 + (0.10e2 / 0.7e1 * t1316 * t1294 + t1296 * t1348) * t1313;
        res[0] = t1319 * t1335 + t1296 * t1337 - t1294 * t1340 / 0.7e1 - t1292 * t1342 / 0.8e1 - (-t1322 * t1304 + (0.3e1 / 0.2e1 * t1316 * t1292 - 0.3e1 / 0.7e1 * t1317 * t1294) * t1313) * t1334 / 0.9e1;
        res[1] = (t1319 * t1343 + t1308 * t1298 + t1301 * t1345 + t1299 * t1346 + t1336 * t1347) * t1313;
        const Scalar t108 = t1323 * t1323;
        res[2] = t1319 * t1298 * t1313 + t1337 * t1345 + t1340 * t1346 + t1342 * t1347 - (-t108 * t1305 + (0.12e2 / 0.7e1 * t1316 * t1291 - 0.2e1 / 0.3e1 * t1317 * t1293) * t1313) * t1334 / 0.8e1;

    }
    
    template<>
    inline void HomotheticCompactPolynomial_segment_ScalisF_HornusGradF<4> (Scalar l, Scalar d, const Scalar w[3], Scalar res[3])
    {
        const Scalar t1360 = d * l + 0.1e1;
        const Scalar t1356 = 0.1e1 / t1360;
        const Scalar t1369 = t1360 * t1360;
        const Scalar t1357 = 0.1e1 / t1369;
        const Scalar t1358 = t1356 * t1357;
        const Scalar t1376 = -t1358 / 0.3e1 + 0.1e1 / 0.3e1;
        const Scalar t1361 = w[2];
        const Scalar t1375 = t1361 / 0.3e1;
        const Scalar t1362 = w[1];
        const Scalar t1374 = -0.2e1 / 0.3e1 * t1362;
        const Scalar t1366 = 0.1e1 / d;
        const Scalar t1365 = l * l;
        const Scalar t1364 = l * t1365;
        const Scalar t1363 = w[0];
        const Scalar t4 = t1369 * t1369;
        const Scalar t1359 = 0.1e1 / t4;
        const Scalar t1354 = -(t1357 - 0.1e1) * t1366 / 0.2e1 - l * t1358;
        const Scalar t1353 = (-(t1356 - 0.1e1) * t1366 - l * t1357) * t1366 - t1365 * t1358;
        const Scalar t15 = log(t1360);
        const Scalar t1352 = 0.3e1 / 0.2e1 * (0.2e1 * (t15 * t1366 - l * t1356) * t1366 - t1365 * t1357) * t1366 - t1364 * t1358;
        const Scalar t26 = t1363 * t1363;
        const Scalar t33 = t1362 * t1362;
        const Scalar t41 = t1361 * t1361;
        const Scalar t44 = t1365 * t1365;
        res[0] = -t26 * (t1359 - 0.1e1) * t1366 / 0.4e1 + ((0.2e1 * t1361 * t1363 + 0.4e1 * t33) * (0.2e1 / 0.3e1 * t1354 * t1366 - t1365 * t1359) + t41 * (0.4e1 / 0.3e1 * t1352 * t1366 - t44 * t1359)) * t1366 / 0.4e1 + (-t1363 * (t1366 * t1376 - l * t1359) - t1361 * (t1353 * t1366 - t1364 * t1359)) * t1362 * t1366;
        res[1] = (t1363 * t1376 + t1354 * t1374 + t1353 * t1375) * t1366;
        res[2] = (t1363 * t1354 / 0.3e1 + t1353 * t1374 + t1352 * t1375) * t1366;

    }
    
    template<>
    inline void HomotheticCompactPolynomial_approx_segment_ScalisF_HornusGradF<4> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3])
    {
        const Scalar t1423 = q * d;
        const Scalar t1393 = t1423 + 0.1e1;
        const Scalar t1424 = 0.1e1 / t1393;
        const Scalar t1 = q * q;
        const Scalar t1399 = 0.1e1 / t1;
        const Scalar t2 = t1393 * t1393;
        const Scalar t3 = t2 * t2;
        const Scalar t1391 = t1424 / t3;
        const Scalar t1398 = w[0];
        const Scalar t1422 = 0.2e1 * t1398;
        const Scalar t1396 = w[2];
        const Scalar t1394 = 0.1e1 / t1396;
        const Scalar t1421 = d * t1394;
        const Scalar t1397 = w[1];
        const Scalar t1401 = l * l;
        const Scalar t1388 = t1396 * t1401 - 0.2e1 * t1397 * l + t1398;
        const Scalar t1387 = t1388 * t1388;
        const Scalar t1395 = t1398 * t1398;
        const Scalar t9 = t1397 * t1397;
        const Scalar t1389 = t1396 * t1398 - t9;
        const Scalar t1390 = t1396 * l - t1397;
        const Scalar t1383 = 0.2e1 * t1389 * l + t1390 * t1388 + t1397 * t1398;
        const Scalar t1417 = t1383 * t1394;
        const Scalar t1382 = t1397 * t1417 / 0.3e1 + t1387 / 0.4e1 - t1395 / 0.4e1;
        const Scalar t1418 = t1383 / 0.3e1;
        const Scalar t1379 = -l * t1387 + (-0.6e1 * t1397 * t1382 + t1398 * t1418) * t1394;
        const Scalar t1414 = t1401 * t1387;
        const Scalar t1377 = -t1414 + (0.8e1 / 0.5e1 * t1397 * t1379 + t1382 * t1422) * t1394;
        const Scalar t1420 = -t1377 / 0.6e1;
        const Scalar t1419 = 0.4e1 / 0.15e2 * t1389 * t1417 + t1390 * t1387 / 0.5e1 + t1397 * t1395 / 0.5e1;
        const Scalar t1392 = t1424 * t1391;
        const Scalar t1416 = (0.3e1 * t1391 - 0.300e1 + 0.5e1 * (0.2e1 + t1392) * t1423) * t1399;
        const Scalar t1415 = (0.2e1 - 0.200e1 * t1391 - 0.5e1 * (0.1e1 + t1392) * t1423) / q * t1399;
        const Scalar t1413 = t1394 * t1416;
        const Scalar t1412 = t1394 * t1415;
        const Scalar t1411 = t1394 * t1419;
        const Scalar t1386 = t1388 * t1387;
        const Scalar t1380 = t1397 * t1411 + t1386 / 0.6e1 - t1398 * t1395 / 0.6e1;
        const Scalar t1378 = -l * t1386 + (-0.8e1 * t1397 * t1380 + t1398 * t1419) * t1394;
        res[0] = t1411 - 0.5e1 * t1380 * t1421 - t1378 * t1413 / 0.7e1 - (-t1401 * t1386 + (0.10e2 / 0.7e1 * t1397 * t1378 + t1380 * t1422) * t1394) * t1412 / 0.8e1;
        res[1] = (t1418 - 0.5e1 * d * t1382 - t1379 * t1416 / 0.5e1 + t1415 * t1420) * t1394;
        res[2] = t1382 * t1394 + t1379 * t1421 + t1413 * t1420 - (-l * t1414 + (0.5e1 / 0.3e1 * t1397 * t1377 - 0.3e1 / 0.5e1 * t1398 * t1379) * t1394) * t1412 / 0.7e1;

    }
    
    
    

    /////////////////////////////////
    // Code for kernel of degree 6 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HomotheticCompactPolynomial_segment_F<6> (Scalar l, Scalar d, const Scalar w[3])
    {
        const Scalar dl = d*l;

        const Scalar inv_dlp1 = 1.0 / (dl + 1.0);
        const Scalar l_div_dlp1 = l * inv_dlp1;

        const Scalar l_div_dlp2 = l / (dl + 2.0);
        const Scalar l_div_dlp2_2 = l_div_dlp2*l_div_dlp2;

        //        SUM [k=0:infty] 1/(2*k+7) ((d*l)/(d*l+2))^2k
        const Scalar sqr_dl_divdlp2 = d*d*l_div_dlp2_2 ;
        const Scalar serie_approx = (1.0/7.0) + sqr_dl_divdlp2*(
                                       (1.0/9.0) + sqr_dl_divdlp2*(
                                            (1.0/11.0) + sqr_dl_divdlp2*(
                                                (1.0/13.0) + sqr_dl_divdlp2*(
                                                    (1.0/15.0) + sqr_dl_divdlp2*(
                                                        (1.0/17.0) + sqr_dl_divdlp2*(
                                                            (1.0/19.0) ))))));
//                                                            (1.0/19.0) + sqr_dl_divdlp2*(1.0/21.0)))))));

        const Scalar inv_dlp1_2 = inv_dlp1 * inv_dlp1;
        const Scalar inv_dlp1_4 = inv_dlp1_2 * inv_dlp1_2;

        const Scalar w0 = w[0];
        const Scalar sqrw0 = 0.5 * w0 * w0;
        const Scalar w1 = 2.0*w[1];
        const Scalar sqrw1 = w1 * w1;
        const Scalar w2 = w[2];
        const Scalar sqrw2 = 0.5 * w2 * w2;

        const Scalar w0w2 = w0 * w2;
        const Scalar spe_w = (w0w2 + sqrw1) * 0.5;

        const Scalar spe_l_div_dlp2_5  = w2 * sqrw2 * l_div_dlp2_2*l_div_dlp2_2*l_div_dlp2;

        return
                l_div_dlp1 * (
                    sqrw0 * w0 * (
                                    1.0 + inv_dlp1 * (1.0 + inv_dlp1 * (1.0 + inv_dlp1 * (1.0 + inv_dlp1 * (1.0 + inv_dlp1))))
                                )  / 3.0
                    + l_div_dlp1 * (
                        - sqrw0 * w1 *(
                                    (1.0/5.0) + inv_dlp1 *( (2.0/5.0) + inv_dlp1 * ( (3.0/5.0) + inv_dlp1 * ((4.0/5.0) + inv_dlp1)))
                                       )
                        + spe_l_div_dlp2_5 * ( 27.0 + dl * (63.0 + dl * ( 56.5 + dl * ( 23.2 + dl * 3.7 ))) ) * inv_dlp1_4 / 3.0
                        + l_div_dlp1 * (
                             w0 * spe_w * (
                                    1.0/10.0 + inv_dlp1 * ( (3.0/10.0) + inv_dlp1 * ((3.0/5.0) + inv_dlp1) )
                                )
                            + l_div_dlp1 * (
                                - w1 * (w0w2 + (1.0/6.0) * sqrw1) * (
                                                                     1.0/10.0 + inv_dlp1 * ( (2.0/5.0) + inv_dlp1)
                                                                   )
                                + l_div_dlp1 * (
                                    + w2 * spe_w * (  1.0/5.0 + inv_dlp1  )
                                    - sqrw2 * w1 * l_div_dlp1
                                                )
                                            )
                                        )
                                    )
                             )
                + 4.0 * spe_l_div_dlp2_5 * l_div_dlp2_2 * serie_approx ;
/*
        // Code generated by maple before "hand stabilisation"
        const Scalar t6457 = d * l + 0.1e1;
        const Scalar t6451 = 0.1e1 / t6457;
        const Scalar t6473 = t6457 * t6457;
        const Scalar t2 = t6473 * t6473;
        const Scalar t6454 = 0.1e1 / t2;
        const Scalar t6462 = w[1];
        const Scalar t6459 = t6462 * t6462;
        const Scalar t6483 = 0.12e2 * t6459;
        const Scalar t6468 = 0.1e1 / d;
        const Scalar t6481 = t6462 * t6468;
        const Scalar t6474 = t6457 * t6473;
        const Scalar t6467 = l * l;
        const Scalar t6470 = t6467 * t6467;
        const Scalar t6469 = l * t6467;
        const Scalar t6464 = l * t6470;
        const Scalar t6463 = w[0];
        const Scalar t6461 = w[2];
        const Scalar t6460 = t6463 * t6463;
        const Scalar t6458 = t6461 * t6461;
        const Scalar t3 = t6474 * t6474;
        const Scalar t6456 = 0.1e1 / t3;
        const Scalar t6455 = t6451 * t6454;
        const Scalar t6453 = 0.1e1 / t6474;
        const Scalar t6452 = 0.1e1 / t6473;
        const Scalar t71 = log(t6457);
        const Scalar t93 = t6469 * t6469;
        return  -t6458 * (((((-(t6451 - 0.1e1) * t6468 - l * t6452) * t6468 - t6467 * t6453) * t6468 - t6469 * t6454) * t6468 - t6470 * t6455) * t6468 - t6464 * t6456) * t6481 + (-t6463 * (t6456 - 0.1e1) * t6468 / 0.6e1 - (-(t6455 - 0.1e1) * t6468 / 0.5e1 - l * t6456) * t6481) * t6460 + ((t6463 * t6483 + 0.3e1 * t6461 * t6460) * (0.2e1 / 0.5e1 * (-(t6454 - 0.1e1) * t6468 / 0.4e1 - l * t6455) * t6468 - t6467 * t6456) + (0.3e1 * t6458 * t6463 + t6461 * t6483) * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * (0.2e1 / 0.3e1 * (-(t6452 - 0.1e1) * t6468 / 0.2e1 - l * t6453) * t6468 - t6467 * t6454) * t6468 - t6469 * t6455) * t6468 - t6470 * t6456) + t6461 * t6458 * (0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * (0.2e1 * (t71 * t6468 - l * t6451) * t6468 - t6467 * t6452) * t6468 - t6469 * t6453) * t6468 - t6470 * t6454) * t6468 - t6464 * t6455) * t6468 - t93 * t6456) + (-0.12e2 * t6461 * t6463 - 0.8e1 * t6459) * (0.3e1 / 0.5e1 * ((-(t6453 - 0.1e1) * t6468 / 0.3e1 - l * t6454) * t6468 / 0.2e1 - t6467 * t6455) * t6468 - t6469 * t6456) * t6462) * t6468 / 0.6e1;

*/
    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_segment_GradF<6> (Scalar l, Scalar d, const Scalar w[3], Scalar res[2])
    {
        const Scalar dl = d*l;

        const Scalar inv_dlp1 = 1.0 / (dl + 1.0);
        const Scalar l_div_dlp1 = l * inv_dlp1;

        const Scalar w0 = w[0];
        const Scalar sqrw0 = w0 * w0;
        const Scalar w1 = w[1];
        const Scalar sqrw1 = 2.0*w1 * w1;
        const Scalar w2 = w[2];
        const Scalar sqrw2 = w2 * w2;


        const Scalar t6518 = 1.0/5.0 + inv_dlp1;
        const Scalar t6519 = 1.0/10.0 + inv_dlp1 * (2.0/5.0 + inv_dlp1 );
        const Scalar t6520 = 1.0/10.0 + inv_dlp1 * (3.0/10.0 + inv_dlp1 * (6.0/10.0 + inv_dlp1 ));
        const Scalar t6521 = 1.0/5.0 + inv_dlp1 * (2.0/5.0 + inv_dlp1 * (3.0/5.0 + inv_dlp1 * (4.0/5.0 + inv_dlp1)));
        const Scalar t6554 = ( 1.0 + inv_dlp1 * (1.0 + inv_dlp1 * (1.0 + inv_dlp1 * (1.0 + inv_dlp1 * (1.0 + inv_dlp1))))) / 6.0;

        const Scalar t6560 = (1.0/3.0) * (sqrw1 + w0 * w2);
        const Scalar t6558 = (-2.0/3.0)*w1 * w2;
        const Scalar w01_a = (-2.0/3.0)*w1 * w0;

        res[0] = l_div_dlp1 * ( sqrw0 * t6554
                                + l_div_dlp1 * ( w01_a * t6521
                                                 + l_div_dlp1 * ( t6520 * t6560
                                                                  + l_div_dlp1 * ( t6558 * t6519
                                                                                   + l_div_dlp1 * sqrw2 / 6.0 * t6518 )
                                                                  )
                                                 )
                                );
        res[1] = l_div_dlp1*l_div_dlp1 * (sqrw0 * t6521 / 6.0
                                 + l_div_dlp1 * ( w01_a * t6520
                                                  + l_div_dlp1 * ( t6519 * t6560
                                                                   + l_div_dlp1 * ( t6558 * t6518
                                                                                    + l_div_dlp1 * sqrw2 / 6.0)
                                                                   )
                                                  )
                                 );

        /*
        const Scalar t6494 = d * l + 0.1e1;
        const Scalar t6516 = 0.1e1 / t6494;
        const Scalar t6507 = t6494 * t6494;
        const Scalar t2 = t6507 * t6507;
        const Scalar t6491 = 0.1e1 / t2;
        const Scalar t6497 = w[2];
        const Scalar t6515 = -0.2e1 / 0.3e1 * t6497;
        const Scalar t6499 = w[0];
        const Scalar t6514 = -0.2e1 / 0.3e1 * t6499;
        const Scalar t6508 = t6494 * t6507;
        const Scalar t5 = t6508 * t6508;
        const Scalar t6493 = 0.1e1 / t5;
        const Scalar t6502 = l * l;
        const Scalar t6505 = t6502 * t6502;
        const Scalar t6512 = t6505 * t6493;
        const Scalar t6503 = 0.1e1 / d;
        const Scalar t6501 = l * t6502;
        const Scalar t6498 = w[1];
        const Scalar t6496 = t6499 * t6499;
        const Scalar t6495 = t6497 * t6497;
        const Scalar t6492 = t6516 * t6491;
        const Scalar t6490 = 0.1e1 / t6508;
        const Scalar t6489 = 0.1e1 / t6507;
        const Scalar t8 = t6498 * t6498;
        const Scalar t6488 = 0.2e1 * t6497 * t6499 + 0.4e1 * t8;
        const Scalar t6487 = -(t6492 - 0.1e1) * t6503 / 0.5e1 - l * t6493;
        const Scalar t6486 = 0.2e1 / 0.5e1 * (-(t6491 - 0.1e1) * t6503 / 0.4e1 - l * t6492) * t6503 - t6502 * t6493;
        const Scalar t6485 = 0.3e1 / 0.5e1 * ((-(t6490 - 0.1e1) * t6503 / 0.3e1 - l * t6491) * t6503 / 0.2e1 - t6502 * t6492) * t6503 - t6501 * t6493;
        const Scalar t6484 = 0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * (0.2e1 / 0.3e1 * (-(t6489 - 0.1e1) * t6503 / 0.2e1 - l * t6490) * t6503 - t6502 * t6491) * t6503 - t6501 * t6492) * t6503 - t6512;
        res[0] = (-t6496 * (t6493 - 0.1e1) / 0.6e1 + t6488 * t6486 / 0.6e1 + t6495 * t6484 / 0.6e1 + (t6487 * t6514 + t6485 * t6515) * t6498) * t6503;
        res[1] = (t6486 * t6514 + t6484 * t6515) * t6503 * t6498 + (t6496 * t6487 + t6488 * t6485 + t6495 * (((((-(t6516 - 0.1e1) * t6503 - l * t6489) * t6503 - t6502 * t6490) * t6503 - t6501 * t6491) * t6503 - t6505 * t6492) * t6503 - l * t6512)) * t6503 / 0.6e1;

*/
    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_segment_FGradF<6> (Scalar l, Scalar d, const Scalar w[3], Scalar res[3])
    {
        const Scalar dl = d*l;

        const Scalar inv_dlp1 = 1.0 / (dl + 1.0);
        const Scalar inv_dlp1_2 = inv_dlp1 * inv_dlp1;
        const Scalar inv_dlp1_4 = inv_dlp1_2 * inv_dlp1_2;
        const Scalar l_div_dlp1 = l * inv_dlp1;

        const Scalar l_div_dlp2 = l / (dl + 2.0);
        const Scalar l_div_dlp2_2 = l_div_dlp2*l_div_dlp2;

        //        SUM [k=0:infty] 1/(2*k+7) ((d*l)/(d*l+2))^2k
        const Scalar sqr_dl_divdlp2 = d*d*l_div_dlp2_2;
        const Scalar serie_approx = (1.0/7.0) + sqr_dl_divdlp2*(
                                       (1.0/9.0) + sqr_dl_divdlp2*(
                                            (1.0/11.0) + sqr_dl_divdlp2*(
                                                (1.0/13.0) + sqr_dl_divdlp2*(
                                                    (1.0/15.0) + sqr_dl_divdlp2*(
                                                        (1.0/17.0) + sqr_dl_divdlp2*(
                                                            (1.0/19.0) + sqr_dl_divdlp2*(1.0/21.0)))))));

        const Scalar w0 = w[0];
        const Scalar sqrw0 = w0 * w0;
        const Scalar w1 = w[1];
        const Scalar sqrw1 = 2.0*w1 * w1;
        const Scalar w2 = w[2];
        const Scalar sqrw2 = w2 * w2;

        const Scalar w0w2 = w0 * w2;
        const Scalar spe_w = sqrw1 + 0.5*w0w2;

        const Scalar spe_l_div_dlp2_5  = w2 * sqrw2 * l_div_dlp2_2*l_div_dlp2_2*l_div_dlp2;

        const Scalar t6518 = 1.0/5.0 + inv_dlp1;
        const Scalar t6519 = 1.0/10.0 + inv_dlp1 * (2.0/5.0 + inv_dlp1 );
        const Scalar t6520 = 1.0/10.0 + inv_dlp1 * (3.0/10.0 + inv_dlp1 * (6.0/10.0 + inv_dlp1 ));
        const Scalar t6521 = 1.0/5.0 + inv_dlp1 * (2.0/5.0 + inv_dlp1 * (3.0/5.0 + inv_dlp1 * (4.0/5.0 + inv_dlp1)));
        const Scalar t6554 = ( 1.0 + inv_dlp1 * (1.0 + inv_dlp1 * (1.0 + inv_dlp1 * (1.0 + inv_dlp1 * (1.0 + inv_dlp1))))) / 6.0;

        res[0] =  l_div_dlp1 * (
            sqrw0 * w0 * t6554
            + l_div_dlp1 * (
                - w1 * sqrw0 * t6521
                + spe_l_div_dlp2_5 * ( 27.0 + dl * (63.0 + dl * ( 56.5 + dl * ( 23.2 + dl * 3.7 ))) ) * inv_dlp1_4 / 6.0
                + l_div_dlp1 * (
                    w0 * spe_w * t6520
                    + l_div_dlp1 * (
                        w1 * ( (-2.0/3.0) * sqrw1 - 2.0 * w0w2) * t6519
                        + l_div_dlp1 * (
                            w2 * spe_w * t6518
                            - sqrw2 * w1 * l_div_dlp1
                                )
                            )
                        )
                    )
                )
        + spe_l_div_dlp2_5 * 2.0*l_div_dlp2_2*serie_approx ;

        const Scalar t6560 = (1.0/3.0) * (sqrw1 + w0w2);
        const Scalar t6558 = (-2.0/3.0)*w1 * w2;
        const Scalar w01_a = (-2.0/3.0)*w1 * w0;

        res[1] = l_div_dlp1 * ( sqrw0 * t6554
                                + l_div_dlp1 * ( w01_a * t6521
                                                 + l_div_dlp1 * ( t6520 * t6560
                                                                  + l_div_dlp1 * ( t6558 * t6519
                                                                                   + l_div_dlp1 * sqrw2 / 6.0 * t6518 )
                                                                  )
                                                 )
                                );
        res[2] = l_div_dlp1*l_div_dlp1 * (sqrw0 * t6521 / 6.0
                                 + l_div_dlp1 * ( w01_a * t6520
                                                  + l_div_dlp1 * ( t6519 * t6560
                                                                   + l_div_dlp1 * ( t6558 * t6518
                                                                                    + l_div_dlp1 * sqrw2 / 6.0)
                                                                   )
                                                  )
                                 );
  /*
        const Scalar t6530 = d * l + 0.1e1;
        const Scalar t6524 = 0.1e1 / t6530;
        const Scalar t6546 = t6530 * t6530;
        const Scalar t2 = t6546 * t6546;
        const Scalar t6527 = 0.1e1 / t2;
        const Scalar t6535 = w[1];
        const Scalar t6532 = t6535 * t6535;
        const Scalar t6561 = 0.2e1 * t6532;
        const Scalar t6534 = w[2];
        const Scalar t6536 = w[0];
        const Scalar t6560 = t6534 * t6536 / 0.3e1 + 0.2e1 / 0.3e1 * t6532;
        const Scalar t6531 = t6534 * t6534;
        const Scalar t6559 = t6531 / 0.6e1;
        const Scalar t6558 = -0.2e1 / 0.3e1 * t6534;
        const Scalar t6547 = t6530 * t6546;
        const Scalar t6526 = 0.1e1 / t6547;
        const Scalar t6528 = t6524 * t6527;
        const Scalar t7 = t6547 * t6547;
        const Scalar t6529 = 0.1e1 / t7;
        const Scalar t6540 = l * l;
        const Scalar t6541 = 0.1e1 / d;
        const Scalar t6542 = l * t6540;
        const Scalar t6519 = 0.3e1 / 0.5e1 * ((-(t6526 - 0.1e1) * t6541 / 0.3e1 - l * t6527) * t6541 / 0.2e1 - t6540 * t6528) * t6541 - t6542 * t6529;
        const Scalar t6557 = t6519 * t6535;
        const Scalar t6521 = -(t6528 - 0.1e1) * t6541 / 0.5e1 - l * t6529;
        const Scalar t6533 = t6536 * t6536;
        const Scalar t6556 = t6533 * t6521;
        const Scalar t6520 = 0.2e1 / 0.5e1 * (-(t6527 - 0.1e1) * t6541 / 0.4e1 - l * t6528) * t6541 - t6540 * t6529;
        const Scalar t6555 = t6536 * t6520;
        const Scalar t6554 = -t6533 * (t6529 - 0.1e1) / 0.6e1;
        const Scalar t6543 = t6540 * t6540;
        const Scalar t6537 = l * t6543;
        const Scalar t6525 = 0.1e1 / t6546;
        const Scalar t6518 = 0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * (0.2e1 / 0.3e1 * (-(t6525 - 0.1e1) * t6541 / 0.2e1 - l * t6526) * t6541 - t6540 * t6527) * t6541 - t6542 * t6528) * t6541 - t6543 * t6529;
        const Scalar t6517 = ((((-(t6524 - 0.1e1) * t6541 - l * t6525) * t6541 - t6540 * t6526) * t6541 - t6542 * t6527) * t6541 - t6543 * t6528) * t6541 - t6537 * t6529;
        const Scalar t81 = t6542 * t6542;
        const Scalar t92 = log(t6530);
        res[0] = (t6536 * t6554 - t6535 * t6556 + t6555 * t6561 - 0.4e1 / 0.3e1 * t6532 * t6557 + (t6533 * t6520 / 0.2e1 + t6518 * t6561 - 0.2e1 * t6536 * t6557) * t6534 + (t6536 * t6518 / 0.2e1 - t6535 * t6517 + (-t81 * t6529 / 0.6e1 + (-t6537 * t6528 / 0.5e1 + (-t6543 * t6527 / 0.4e1 + (-t6542 * t6526 / 0.3e1 + (-t6540 * t6525 / 0.2e1 + (t92 * t6541 - l * t6524) * t6541) * t6541) * t6541) * t6541) * t6541) * t6534) * t6531) * t6541;
        res[1] = (t6554 + t6520 * t6560 + t6518 * t6559 + (-0.2e1 / 0.3e1 * t6536 * t6521 + t6519 * t6558) * t6535) * t6541;
        res[2] = (t6556 / 0.6e1 + t6519 * t6560 + t6517 * t6559 + (-0.2e1 / 0.3e1 * t6555 + t6518 * t6558) * t6535) * t6541;

*/
    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_approx_segment_F<6> (Scalar l, Scalar d, Scalar q, const Scalar w[3])
    {
        const Scalar t1570 = q * d;
        const Scalar t1545 = t1570 + 0.1e1;
        const Scalar t1571 = 0.1e1 / t1545;
        const Scalar t1 = t1545 * t1545;
        const Scalar t2 = t1 * t1;
        const Scalar t1543 = t1571 / t2 / t1;
        const Scalar t1547 = w[2];
        const Scalar t1548 = w[1];
        const Scalar t1549 = w[0];
        const Scalar t1550 = l * l;
        const Scalar t1540 = t1547 * t1550 - 0.2e1 * t1548 * l + t1549;
        const Scalar t9 = t1548 * t1548;
        const Scalar t1541 = t1547 * t1549 - t9;
        const Scalar t1542 = t1547 * l - t1548;
        const Scalar t1561 = t1549 * t1549;
        const Scalar t1565 = t1548 * t1561;
        const Scalar t1553 = t1540 * t1540;
        const Scalar t1567 = t1542 * t1553;
        const Scalar t1546 = 0.1e1 / t1547;
        const Scalar t1568 = t1541 * t1546;
        const Scalar t1569 = 0.6e1 / 0.35e2 * (0.4e1 / 0.3e1 * (0.2e1 * t1541 * l + t1542 * t1540 + t1548 * t1549) * t1568 + t1567 + t1565) * t1568 + t1540 * t1567 / 0.7e1 + t1549 * t1565 / 0.7e1;
        const Scalar t1564 = t1546 * t1569;
        const Scalar t1544 = t1571 * t1543;
        const Scalar t1539 = t1553 * t1553;
        const Scalar t27 = t1561 * t1561;
        const Scalar t1537 = t1548 * t1564 + t1539 / 0.8e1 - t27 / 0.8e1;
        const Scalar t1536 = -l * t1539 + (-0.10e2 * t1548 * t1537 + t1549 * t1569) * t1546;
        const Scalar t65 = q * q;
        return  t1564 - 0.7e1 * d * t1537 * t1546 + (-0.1111111111e0 * (0.3e1 * t1543 - 0.300e1 + 0.7e1 * (0.2e1 + t1544) * t1570) * t1536 - 0.1000000000e0 * (0.2e1 - 0.200e1 * t1543 - 0.7e1 * (0.1e1 + t1544) * t1570) / q * (-0.1e1 * t1550 * t1539 + (0.1333333333e1 * t1548 * t1536 + 0.2e1 * t1549 * t1537) * t1546)) * t1546 / t65;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_approx_segment_GradF<6> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[2])
    {
        const Scalar t1609 = q * d;
        const Scalar t1584 = t1609 + 0.1e1;
        const Scalar t1610 = 0.1e1 / t1584;
        const Scalar t1 = q * q;
        const Scalar t1589 = 0.1e1 / t1;
        const Scalar t2 = t1584 * t1584;
        const Scalar t3 = t2 * t2;
        const Scalar t1582 = t1610 / t3 / t2;
        const Scalar t1586 = w[2];
        const Scalar t1587 = w[1];
        const Scalar t1588 = w[0];
        const Scalar t1591 = l * l;
        const Scalar t1579 = t1586 * t1591 - 0.2e1 * t1587 * l + t1588;
        const Scalar t1595 = t1579 * t1579;
        const Scalar t1578 = t1579 * t1595;
        const Scalar t1585 = 0.1e1 / t1586;
        const Scalar t1602 = t1588 * t1588;
        const Scalar t10 = t1587 * t1587;
        const Scalar t1580 = t1586 * t1588 - t10;
        const Scalar t1581 = t1586 * l - t1587;
        const Scalar t1607 = 0.4e1 / 0.15e2 * t1580 * (0.2e1 * t1580 * l + t1581 * t1579 + t1587 * t1588) * t1585 + t1581 * t1595 / 0.5e1 + t1587 * t1602 / 0.5e1;
        const Scalar t1574 = t1587 * t1585 * t1607 + t1578 / 0.6e1 - t1588 * t1602 / 0.6e1;
        const Scalar t1573 = -l * t1578 + (-0.8e1 * t1587 * t1574 + t1588 * t1607) * t1585;
        const Scalar t1604 = t1591 * t1578;
        const Scalar t1572 = -t1604 + (0.10e2 / 0.7e1 * t1587 * t1573 + 0.2e1 * t1588 * t1574) * t1585;
        const Scalar t1608 = -t1572 / 0.8e1;
        const Scalar t1583 = t1610 * t1582;
        const Scalar t1606 = (0.3e1 * t1582 - 0.300e1 + 0.7e1 * (0.2e1 + t1583) * t1609) * t1589;
        const Scalar t1605 = (0.2e1 - 0.200e1 * t1582 - 0.7e1 * (0.1e1 + t1583) * t1609) / q * t1589;
        res[0] = (t1607 - 0.7e1 * d * t1574 - t1573 * t1606 / 0.7e1 + t1605 * t1608) * t1585;
        res[1] = (t1574 + d * t1573 + t1606 * t1608 + (l * t1604 / 0.9e1 - (0.3e1 / 0.2e1 * t1587 * t1572 - 0.3e1 / 0.7e1 * t1588 * t1573) * t1585 / 0.9e1) * t1605) * t1585;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_approx_segment_FGradF<6> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3])
    {
        const Scalar t1662 = q * d;
        const Scalar t1627 = t1662 + 0.1e1;
        const Scalar t1663 = 0.1e1 / t1627;
        const Scalar t1 = q * q;
        const Scalar t1633 = 0.1e1 / t1;
        const Scalar t2 = t1627 * t1627;
        const Scalar t3 = t2 * t2;
        const Scalar t1625 = t1663 / t3 / t2;
        const Scalar t1632 = w[0];
        const Scalar t1661 = 0.2e1 * t1632;
        const Scalar t1630 = w[2];
        const Scalar t1628 = 0.1e1 / t1630;
        const Scalar t1660 = d * t1628;
        const Scalar t1631 = w[1];
        const Scalar t1635 = l * l;
        const Scalar t1622 = t1630 * t1635 - 0.2e1 * t1631 * l + t1632;
        const Scalar t1639 = t1622 * t1622;
        const Scalar t1621 = t1622 * t1639;
        const Scalar t1647 = t1632 * t1632;
        const Scalar t1629 = t1632 * t1647;
        const Scalar t10 = t1631 * t1631;
        const Scalar t1623 = t1630 * t1632 - t10;
        const Scalar t1624 = t1630 * l - t1631;
        const Scalar t1654 = t1623 * t1628;
        const Scalar t1617 = 0.4e1 / 0.3e1 * (0.2e1 * t1623 * l + t1624 * t1622 + t1631 * t1632) * t1654 + t1624 * t1639 + t1631 * t1647;
        const Scalar t1657 = t1617 / 0.5e1;
        const Scalar t1616 = t1631 * t1628 * t1657 + t1621 / 0.6e1 - t1629 / 0.6e1;
        const Scalar t1613 = -l * t1621 + (-0.8e1 * t1631 * t1616 + t1632 * t1657) * t1628;
        const Scalar t1653 = t1635 * t1621;
        const Scalar t1611 = -t1653 + (0.10e2 / 0.7e1 * t1631 * t1613 + t1616 * t1661) * t1628;
        const Scalar t1659 = -t1611 / 0.8e1;
        const Scalar t1658 = 0.6e1 / 0.35e2 * t1617 * t1654 + t1624 * t1621 / 0.7e1 + t1631 * t1629 / 0.7e1;
        const Scalar t1626 = t1663 * t1625;
        const Scalar t1656 = (0.3e1 * t1625 - 0.300e1 + 0.7e1 * (0.2e1 + t1626) * t1662) * t1633;
        const Scalar t1655 = (0.2e1 - 0.200e1 * t1625 - 0.7e1 * (0.1e1 + t1626) * t1662) / q * t1633;
        const Scalar t1652 = t1628 * t1656;
        const Scalar t1651 = t1628 * t1655;
        const Scalar t1650 = t1628 * t1658;
        const Scalar t1620 = t1639 * t1639;
        const Scalar t57 = t1647 * t1647;
        const Scalar t1614 = t1631 * t1650 + t1620 / 0.8e1 - t57 / 0.8e1;
        const Scalar t1612 = -l * t1620 + (-0.10e2 * t1631 * t1614 + t1632 * t1658) * t1628;
        res[0] = t1650 - 0.7e1 * t1614 * t1660 - t1612 * t1652 / 0.9e1 - (-t1635 * t1620 + (0.4e1 / 0.3e1 * t1631 * t1612 + t1614 * t1661) * t1628) * t1651 / 0.10e2;
        res[1] = (t1657 - 0.7e1 * d * t1616 - t1613 * t1656 / 0.7e1 + t1655 * t1659) * t1628;
        res[2] = t1616 * t1628 + t1613 * t1660 + t1652 * t1659 - (-l * t1653 + (0.3e1 / 0.2e1 * t1631 * t1611 - 0.3e1 / 0.7e1 * t1632 * t1613) * t1628) * t1651 / 0.9e1;

    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_segment_F_cste<6> (Scalar l, const Scalar w[3])
    {
        const Scalar t1668 = w[2];
        const Scalar t1678 = t1668 * l;
        const Scalar t1669 = w[1];
        const Scalar t1670 = w[0];
        const Scalar t2 = t1669 * t1669;
        const Scalar t1665 = t1668 * t1670 - t2;
        const Scalar t1667 = 0.1e1 / t1668;
        const Scalar t1677 = t1665 * t1667;
        const Scalar t1664 = t1670 + (-0.2e1 * t1669 + t1678) * l;
        const Scalar t1666 = t1678 - t1669;
        const Scalar t6 = t1664 * t1664;
        const Scalar t1676 = t1666 * t6;
        const Scalar t7 = t1670 * t1670;
        const Scalar t1675 = t1669 * t7;
        return  (0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t1665 * l + t1666 * t1664 + t1669 * t1670) * t1677 + t1676 + t1675) * t1677 + t1664 * t1676 + t1670 * t1675) * t1667 / 0.7e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_segment_FGradF_cste<6> (Scalar l, const Scalar w[3], Scalar res[3])
    {
        const Scalar t1686 = w[2];
        const Scalar t1695 = t1686 * l;
        const Scalar t1687 = w[1];
        const Scalar t1688 = w[0];
        const Scalar t2 = t1687 * t1687;
        const Scalar t1682 = t1686 * t1688 - t2;
        const Scalar t1684 = 0.1e1 / t1686;
        const Scalar t1694 = t1682 * t1684;
        const Scalar t1681 = t1688 + (-0.2e1 * t1687 + t1695) * l;
        const Scalar t1683 = t1695 - t1687;
        const Scalar t1689 = t1681 * t1681;
        const Scalar t1691 = t1688 * t1688;
        const Scalar t1679 = 0.4e1 / 0.3e1 * (0.2e1 * t1682 * l + t1683 * t1681 + t1687 * t1688) * t1694 + t1683 * t1689 + t1687 * t1691;
        const Scalar t1693 = t1679 * t1684 / 0.5e1;
        const Scalar t1685 = t1688 * t1691;
        const Scalar t1680 = t1681 * t1689;
        res[0] = (0.6e1 / 0.5e1 * t1679 * t1694 + t1683 * t1680 + t1687 * t1685) * t1684 / 0.7e1;
        res[1] = t1693;
        res[2] = (t1687 * t1693 + t1680 / 0.6e1 - t1685 / 0.6e1) * t1684;

    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_density_segment_F<6> (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g)
    {
        const Scalar t1703 = d * l + 0.1e1;
        const Scalar t1697 = 0.1e1 / t1703;
        const Scalar t1726 = t1703 * t1703;
        const Scalar t2 = t1726 * t1726;
        const Scalar t1700 = 0.1e1 / t2;
        const Scalar t1711 = w[1];
        const Scalar t1707 = t1711 * t1711;
        const Scalar t1746 = -0.8e1 * t1711 * t1707;
        const Scalar t1745 = -0.6e1 * t1711;
        const Scalar t1720 = 0.1e1 / d;
        const Scalar t1744 = 0.2e1 * t1720;
        const Scalar t1743 = 0.12e2 * t1707;
        const Scalar t1741 = 0.3e1 / 0.2e1 * t1720;
        const Scalar t6 = log(t1703);
        const Scalar t1740 = t6 * t1720;
        const Scalar t1712 = w[0];
        const Scalar t1713 = g[1];
        const Scalar t1739 = t1713 * t1712;
        const Scalar t1714 = g[0];
        const Scalar t1738 = t1714 * t1712;
        const Scalar t1727 = t1703 * t1726;
        const Scalar t7 = t1727 * t1727;
        const Scalar t1702 = 0.1e1 / t7;
        const Scalar t1719 = l * l;
        const Scalar t1721 = l * t1719;
        const Scalar t1724 = t1721 * t1721;
        const Scalar t1737 = t1724 * t1702;
        const Scalar t1736 = t1713 * t1745;
        const Scalar t1735 = t1714 * t1745;
        const Scalar t1734 = t1713 * t1743;
        const Scalar t1722 = t1719 * t1719;
        const Scalar t1716 = l * t1722;
        const Scalar t1710 = w[2];
        const Scalar t1709 = t1712 * t1712;
        const Scalar t1708 = t1712 * t1709;
        const Scalar t1705 = t1710 * t1710;
        const Scalar t1704 = t1710 * t1705;
        const Scalar t1701 = t1697 * t1700;
        const Scalar t1699 = 0.1e1 / t1727;
        const Scalar t1698 = 0.1e1 / t1726;
        return  -t1714 * t1708 * (t1702 - 0.1e1) * t1720 / 0.6e1 + ((t1713 * t1708 + t1709 * t1735) * (-(t1701 - 0.1e1) * t1720 / 0.5e1 - l * t1702) + (t1738 * t1743 + (0.3e1 * t1714 * t1710 + t1736) * t1709) * (0.2e1 / 0.5e1 * (-(t1700 - 0.1e1) * t1720 / 0.4e1 - l * t1701) * t1720 - t1719 * t1702) + (t1714 * t1746 + t1712 * t1734 + (-0.12e2 * t1711 * t1738 + 0.3e1 * t1713 * t1709) * t1710) * (0.3e1 / 0.5e1 * ((-(t1699 - 0.1e1) * t1720 / 0.3e1 - l * t1700) * t1720 / 0.2e1 - t1719 * t1701) * t1720 - t1721 * t1702) + (t1713 * t1746 + 0.3e1 * t1705 * t1738 + 0.12e2 * (t1714 * t1707 - t1711 * t1739) * t1710) * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * (0.2e1 / 0.3e1 * (-(t1698 - 0.1e1) * t1720 / 0.2e1 - l * t1699) * t1720 - t1719 * t1700) * t1720 - t1721 * t1701) * t1720 - t1722 * t1702) + (t1710 * t1734 + (t1735 + 0.3e1 * t1739) * t1705) * (((((-(t1697 - 0.1e1) * t1720 - l * t1698) * t1720 - t1719 * t1699) * t1720 - t1721 * t1700) * t1720 - t1722 * t1701) * t1720 - t1716 * t1702) + (t1714 * t1704 + t1705 * t1736) * (0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (((t1740 - l * t1697) * t1744 - t1719 * t1698) * t1741 - t1721 * t1699) * t1720 - t1722 * t1700) * t1720 - t1716 * t1701) * t1720 - t1737) + t1713 * t1704 * (0.7e1 / 0.5e1 * ((0.5e1 / 0.3e1 * ((0.3e1 * ((l - t1740) * t1744 - t1719 * t1697) * t1720 - t1721 * t1698) * t1744 - t1722 * t1699) * t1720 - t1716 * t1700) * t1741 - t1724 * t1701) * t1720 - l * t1737)) * t1720 / 0.6e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_segment_GradF<6> (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[2])
    {
        const Scalar t1762 = d * l + 0.1e1;
        const Scalar t1756 = 0.1e1 / t1762;
        const Scalar t1780 = t1762 * t1762;
        const Scalar t2 = t1780 * t1780;
        const Scalar t1759 = 0.1e1 / t2;
        const Scalar t1767 = w[1];
        const Scalar t3 = t1767 * t1767;
        const Scalar t1791 = 0.4e1 * t3;
        const Scalar t1790 = -0.4e1 * t1767;
        const Scalar t1768 = w[0];
        const Scalar t1765 = t1768 * t1768;
        const Scalar t1770 = g[0];
        const Scalar t1788 = t1765 * t1770;
        const Scalar t1769 = g[1];
        const Scalar t1787 = t1769 * t1790;
        const Scalar t1786 = t1770 * t1790;
        const Scalar t1766 = w[2];
        const Scalar t1763 = t1766 * t1766;
        const Scalar t1785 = t1769 * t1763 / 0.6e1;
        const Scalar t1781 = t1762 * t1780;
        const Scalar t1774 = l * l;
        const Scalar t1777 = t1774 * t1774;
        const Scalar t1776 = l * t1774;
        const Scalar t1775 = 0.1e1 / d;
        const Scalar t1771 = l * t1777;
        const Scalar t6 = t1781 * t1781;
        const Scalar t1761 = 0.1e1 / t6;
        const Scalar t1760 = t1756 * t1759;
        const Scalar t1758 = 0.1e1 / t1781;
        const Scalar t1757 = 0.1e1 / t1780;
        const Scalar t1755 = t1769 * t1765 + t1768 * t1786;
        const Scalar t1754 = t1766 * t1787 + t1770 * t1763;
        const Scalar t1753 = -(t1760 - 0.1e1) * t1775 / 0.5e1 - l * t1761;
        const Scalar t1752 = t1770 * t1791 + (t1787 + 0.2e1 * t1770 * t1766) * t1768;
        const Scalar t1751 = t1769 * t1791 + (0.2e1 * t1769 * t1768 + t1786) * t1766;
        const Scalar t1750 = 0.2e1 / 0.5e1 * (-(t1759 - 0.1e1) * t1775 / 0.4e1 - l * t1760) * t1775 - t1774 * t1761;
        const Scalar t1749 = 0.3e1 / 0.5e1 * ((-(t1758 - 0.1e1) * t1775 / 0.3e1 - l * t1759) * t1775 / 0.2e1 - t1774 * t1760) * t1775 - t1776 * t1761;
        const Scalar t1748 = 0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * (0.2e1 / 0.3e1 * (-(t1757 - 0.1e1) * t1775 / 0.2e1 - l * t1758) * t1775 - t1774 * t1759) * t1775 - t1776 * t1760) * t1775 - t1777 * t1761;
        const Scalar t1747 = ((((-(t1756 - 0.1e1) * t1775 - l * t1757) * t1775 - t1774 * t1758) * t1775 - t1776 * t1759) * t1775 - t1777 * t1760) * t1775 - t1771 * t1761;
        res[0] = (-(t1761 - 0.1e1) * t1788 / 0.6e1 + t1755 * t1753 / 0.6e1 + t1752 * t1750 / 0.6e1 + t1751 * t1749 / 0.6e1 + t1754 * t1748 / 0.6e1 + t1747 * t1785) * t1775;
        const Scalar t89 = log(t1762);
        const Scalar t111 = t1776 * t1776;
        res[1] = (0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * (0.2e1 * (t89 * t1775 - l * t1756) * t1775 - t1774 * t1757) * t1775 - t1776 * t1758) * t1775 - t1777 * t1759) * t1775 - t1771 * t1760) * t1775 - t111 * t1761) * t1775 * t1785 + (t1753 * t1788 + t1752 * t1749 + t1751 * t1748 + t1754 * t1747 + t1755 * t1750) * t1775 / 0.6e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_segment_FGradF<6> (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[3])
    {
        const Scalar t1817 = w[2];
        const Scalar t1867 = t1817 / 0.6e1;
        const Scalar t1821 = g[0];
        const Scalar t1866 = t1821 / 0.6e1;
        const Scalar t1810 = d * l + 0.1e1;
        const Scalar t1804 = 0.1e1 / t1810;
        const Scalar t1833 = t1810 * t1810;
        const Scalar t2 = t1833 * t1833;
        const Scalar t1807 = 0.1e1 / t2;
        const Scalar t1818 = w[1];
        const Scalar t1814 = t1818 * t1818;
        const Scalar t1865 = -0.8e1 * t1818 * t1814;
        const Scalar t1827 = 0.1e1 / d;
        const Scalar t1864 = 0.2e1 * t1827;
        const Scalar t1850 = t1821 * t1818;
        const Scalar t1819 = w[0];
        const Scalar t1820 = g[1];
        const Scalar t1853 = t1820 * t1819;
        const Scalar t1856 = t1820 * t1814;
        const Scalar t1863 = 0.2e1 / 0.3e1 * t1856 + (0.2e1 * t1853 - 0.4e1 * t1850) * t1867;
        const Scalar t1854 = t1820 * t1818;
        const Scalar t1846 = -0.4e1 * t1854;
        const Scalar t1851 = t1821 * t1817;
        const Scalar t1852 = t1821 * t1814;
        const Scalar t1862 = 0.2e1 / 0.3e1 * t1852 + (t1846 + 0.2e1 * t1851) * t1819 / 0.6e1;
        const Scalar t1812 = t1817 * t1817;
        const Scalar t1861 = t1846 * t1867 + t1812 * t1866;
        const Scalar t1849 = t1821 * t1819;
        const Scalar t1843 = t1818 * t1849;
        const Scalar t1816 = t1819 * t1819;
        const Scalar t1855 = t1820 * t1816;
        const Scalar t1860 = t1855 / 0.6e1 - 0.2e1 / 0.3e1 * t1843;
        const Scalar t1858 = 0.3e1 / 0.2e1 * t1827;
        const Scalar t20 = log(t1810);
        const Scalar t1857 = t20 * t1827;
        const Scalar t1834 = t1810 * t1833;
        const Scalar t21 = t1834 * t1834;
        const Scalar t1809 = 0.1e1 / t21;
        const Scalar t1826 = l * l;
        const Scalar t1828 = l * t1826;
        const Scalar t1831 = t1828 * t1828;
        const Scalar t1848 = t1831 * t1809;
        const Scalar t1847 = -0.6e1 * t1854;
        const Scalar t1845 = -0.6e1 * t1850;
        const Scalar t1844 = 0.12e2 * t1856;
        const Scalar t1842 = -(t1809 - 0.1e1) * t1821 / 0.6e1;
        const Scalar t1841 = t1820 * t1812 / 0.6e1;
        const Scalar t1829 = t1826 * t1826;
        const Scalar t1823 = l * t1829;
        const Scalar t1815 = t1819 * t1816;
        const Scalar t1811 = t1817 * t1812;
        const Scalar t1808 = t1804 * t1807;
        const Scalar t1806 = 0.1e1 / t1834;
        const Scalar t1805 = 0.1e1 / t1833;
        const Scalar t1799 = -(t1808 - 0.1e1) * t1827 / 0.5e1 - l * t1809;
        const Scalar t1796 = 0.2e1 / 0.5e1 * (-(t1807 - 0.1e1) * t1827 / 0.4e1 - l * t1808) * t1827 - t1826 * t1809;
        const Scalar t1795 = 0.3e1 / 0.5e1 * ((-(t1806 - 0.1e1) * t1827 / 0.3e1 - l * t1807) * t1827 / 0.2e1 - t1826 * t1808) * t1827 - t1828 * t1809;
        const Scalar t1794 = 0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * (0.2e1 / 0.3e1 * (-(t1805 - 0.1e1) * t1827 / 0.2e1 - l * t1806) * t1827 - t1826 * t1807) * t1827 - t1828 * t1808) * t1827 - t1829 * t1809;
        const Scalar t1793 = ((((-(t1804 - 0.1e1) * t1827 - l * t1805) * t1827 - t1826 * t1806) * t1827 - t1828 * t1807) * t1827 - t1829 * t1808) * t1827 - t1823 * t1809;
        const Scalar t1792 = 0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (((t1857 - l * t1804) * t1864 - t1826 * t1805) * t1858 - t1828 * t1806) * t1827 - t1829 * t1807) * t1827 - t1823 * t1808) * t1827 - t1848;
        res[0] = t1815 * t1827 * t1842 + ((t1820 * t1815 + t1816 * t1845) * t1799 + (0.12e2 * t1814 * t1849 + (0.3e1 * t1851 + t1847) * t1816) * t1796 + (t1821 * t1865 + t1819 * t1844 + (-0.12e2 * t1843 + 0.3e1 * t1855) * t1817) * t1795 + (t1820 * t1865 + 0.3e1 * t1812 * t1849 + 0.12e2 * (t1852 - t1818 * t1853) * t1817) * t1794 + (t1817 * t1844 + (t1845 + 0.3e1 * t1853) * t1812) * t1793 + (t1821 * t1811 + t1812 * t1847) * t1792 + t1820 * t1811 * (0.7e1 / 0.5e1 * ((0.5e1 / 0.3e1 * ((0.3e1 * ((l - t1857) * t1864 - t1826 * t1804) * t1827 - t1828 * t1805) * t1864 - t1829 * t1806) * t1827 - t1823 * t1807) * t1858 - t1831 * t1808) * t1827 - l * t1848)) * t1827 / 0.6e1;
        res[1] = (t1816 * t1842 + t1799 * t1860 + t1796 * t1862 + t1795 * t1863 + t1794 * t1861 + t1793 * t1841) * t1827;
        res[2] = (t1816 * t1799 * t1866 + t1796 * t1860 + t1795 * t1862 + t1794 * t1863 + t1793 * t1861 + t1792 * t1841) * t1827;

    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_density_approx_segment_F<6> (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g)
    {
        const Scalar t1912 = q * d;
        const Scalar t1880 = t1912 + 0.1e1;
        const Scalar t1914 = 0.1e1 / t1880;
        const Scalar t1913 = -0.7e1 * d;
        const Scalar t2 = q * q;
        const Scalar t1887 = 0.1e1 / t2;
        const Scalar t3 = t1880 * t1880;
        const Scalar t4 = t3 * t3;
        const Scalar t1878 = t1914 / t4 / t3;
        const Scalar t1882 = w[2];
        const Scalar t1883 = w[1];
        const Scalar t1884 = w[0];
        const Scalar t1889 = l * l;
        const Scalar t1875 = t1882 * t1889 - 0.2e1 * t1883 * l + t1884;
        const Scalar t11 = t1883 * t1883;
        const Scalar t1876 = t1882 * t1884 - t11;
        const Scalar t1877 = t1882 * l - t1883;
        const Scalar t1901 = t1884 * t1884;
        const Scalar t1906 = t1883 * t1901;
        const Scalar t1893 = t1875 * t1875;
        const Scalar t1907 = t1877 * t1893;
        const Scalar t1881 = 0.1e1 / t1882;
        const Scalar t1908 = t1876 * t1881;
        const Scalar t1911 = 0.6e1 / 0.35e2 * (0.4e1 / 0.3e1 * (0.2e1 * t1876 * l + t1877 * t1875 + t1883 * t1884) * t1908 + t1907 + t1906) * t1908 + t1875 * t1907 / 0.7e1 + t1884 * t1906 / 0.7e1;
        const Scalar t1879 = t1914 * t1878;
        const Scalar t1910 = (0.2e1 - 0.200e1 * t1878 - 0.7e1 * (0.1e1 + t1879) * t1912) / q * t1887;
        const Scalar t1909 = (0.3e1 * t1878 - 0.300e1 + 0.7e1 * (0.2e1 + t1879) * t1912) * t1887;
        const Scalar t1874 = t1893 * t1893;
        const Scalar t1905 = t1889 * t1874;
        const Scalar t1904 = t1881 * t1911;
        const Scalar t1886 = g[0];
        const Scalar t1885 = g[1];
        const Scalar t41 = t1901 * t1901;
        const Scalar t1870 = t1883 * t1904 + t1874 / 0.8e1 - t41 / 0.8e1;
        const Scalar t1869 = -l * t1874 + (-0.10e2 * t1883 * t1870 + t1884 * t1911) * t1881;
        const Scalar t1868 = -t1905 + (0.4e1 / 0.3e1 * t1883 * t1869 + 0.2e1 * t1884 * t1870) * t1881;
        return  t1886 * t1904 + (t1886 * t1913 + t1885) * t1870 * t1881 - (t1886 * t1909 + t1885 * t1913) * t1869 * t1881 / 0.9e1 - (t1886 * t1910 + t1885 * t1909) * t1868 * t1881 / 0.10e2 - t1885 * (-l * t1905 + (0.7e1 / 0.5e1 * t1883 * t1868 - t1884 * t1869 / 0.3e1) * t1881) * t1881 * t1910 / 0.11e2;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_approx_segment_GradF<6> (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[2])
    {
        const Scalar t1962 = q * d;
        const Scalar t1931 = t1962 + 0.1e1;
        const Scalar t1964 = 0.1e1 / t1931;
        const Scalar t1963 = -0.7e1 * d;
        const Scalar t2 = q * q;
        const Scalar t1938 = 0.1e1 / t2;
        const Scalar t3 = t1931 * t1931;
        const Scalar t4 = t3 * t3;
        const Scalar t1929 = t1964 / t4 / t3;
        const Scalar t1933 = w[2];
        const Scalar t1934 = w[1];
        const Scalar t1935 = w[0];
        const Scalar t1940 = l * l;
        const Scalar t1925 = t1933 * t1940 - 0.2e1 * t1934 * l + t1935;
        const Scalar t1945 = t1925 * t1925;
        const Scalar t1924 = t1925 * t1945;
        const Scalar t1932 = 0.1e1 / t1933;
        const Scalar t1952 = t1935 * t1935;
        const Scalar t11 = t1934 * t1934;
        const Scalar t1926 = t1933 * t1935 - t11;
        const Scalar t1928 = t1933 * l - t1934;
        const Scalar t1958 = 0.4e1 / 0.15e2 * t1926 * (0.2e1 * t1926 * l + t1928 * t1925 + t1934 * t1935) * t1932 + t1928 * t1945 / 0.5e1 + t1934 * t1952 / 0.5e1;
        const Scalar t1918 = t1934 * t1932 * t1958 + t1924 / 0.6e1 - t1935 * t1952 / 0.6e1;
        const Scalar t1917 = -l * t1924 + (-0.8e1 * t1934 * t1918 + t1935 * t1958) * t1932;
        const Scalar t1955 = t1940 * t1924;
        const Scalar t1916 = -t1955 + (0.10e2 / 0.7e1 * t1934 * t1917 + 0.2e1 * t1935 * t1918) * t1932;
        const Scalar t1915 = -l * t1955 + (0.3e1 / 0.2e1 * t1934 * t1916 - 0.3e1 / 0.7e1 * t1935 * t1917) * t1932;
        const Scalar t1961 = -t1915 / 0.9e1;
        const Scalar t1960 = -t1916 / 0.8e1;
        const Scalar t1959 = -t1917 / 0.7e1;
        const Scalar t1930 = t1964 * t1929;
        const Scalar t1957 = (0.2e1 - 0.200e1 * t1929 - 0.7e1 * (0.1e1 + t1930) * t1962) / q * t1938;
        const Scalar t1956 = (0.3e1 * t1929 - 0.300e1 + 0.7e1 * (0.2e1 + t1930) * t1962) * t1938;
        const Scalar t1936 = g[1];
        const Scalar t1954 = t1936 * t1957;
        const Scalar t1937 = g[0];
        const Scalar t1927 = t1937 * t1963 + t1936;
        const Scalar t1921 = t1937 * t1956 + t1936 * t1963;
        const Scalar t1920 = t1937 * t1957 + t1936 * t1956;
        res[0] = (t1937 * t1958 + t1927 * t1918 + t1921 * t1959 + t1920 * t1960 + t1954 * t1961) * t1932;
        const Scalar t79 = t1940 * t1940;
        res[1] = (t1937 * t1918 + t1927 * t1959 + t1921 * t1960 + t1920 * t1961 + (t79 * t1924 - (0.14e2 / 0.9e1 * t1934 * t1915 - t1935 * t1916 / 0.2e1) * t1932) * t1954 / 0.10e2) * t1932;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_approx_segment_FGradF<6> (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[3])
    {
        const Scalar t2028 = q * d;
        const Scalar t1986 = t2028 + 0.1e1;
        const Scalar t2030 = 0.1e1 / t1986;
        const Scalar t2029 = -0.7e1 * d;
        const Scalar t2 = q * q;
        const Scalar t1994 = 0.1e1 / t2;
        const Scalar t3 = t1986 * t1986;
        const Scalar t4 = t3 * t3;
        const Scalar t1984 = t2030 / t4 / t3;
        const Scalar t1991 = w[0];
        const Scalar t2027 = 0.2e1 * t1991;
        const Scalar t1989 = w[2];
        const Scalar t1990 = w[1];
        const Scalar t1997 = l * l;
        const Scalar t1980 = t1989 * t1997 - 0.2e1 * t1990 * l + t1991;
        const Scalar t2002 = t1980 * t1980;
        const Scalar t1979 = t1980 * t2002;
        const Scalar t1987 = 0.1e1 / t1989;
        const Scalar t2010 = t1991 * t1991;
        const Scalar t1988 = t1991 * t2010;
        const Scalar t11 = t1990 * t1990;
        const Scalar t1981 = t1989 * t1991 - t11;
        const Scalar t1983 = t1989 * l - t1990;
        const Scalar t2017 = t1981 * t1987;
        const Scalar t1973 = 0.4e1 / 0.3e1 * (0.2e1 * t1981 * l + t1983 * t1980 + t1990 * t1991) * t2017 + t1983 * t2002 + t1990 * t2010;
        const Scalar t2022 = t1973 / 0.5e1;
        const Scalar t1972 = t1990 * t1987 * t2022 + t1979 / 0.6e1 - t1988 / 0.6e1;
        const Scalar t1969 = -l * t1979 + (-0.8e1 * t1990 * t1972 + t1991 * t2022) * t1987;
        const Scalar t1967 = -t1997 * t1979 + (0.10e2 / 0.7e1 * t1990 * t1969 + t1972 * t2027) * t1987;
        const Scalar t1996 = l * t1997;
        const Scalar t1965 = -t1996 * t1979 + (0.3e1 / 0.2e1 * t1990 * t1967 - 0.3e1 / 0.7e1 * t1991 * t1969) * t1987;
        const Scalar t2026 = -t1965 / 0.9e1;
        const Scalar t2025 = -t1967 / 0.8e1;
        const Scalar t2024 = -t1969 / 0.7e1;
        const Scalar t2023 = 0.6e1 / 0.35e2 * t1973 * t2017 + t1983 * t1979 / 0.7e1 + t1990 * t1988 / 0.7e1;
        const Scalar t1992 = g[1];
        const Scalar t1993 = g[0];
        const Scalar t1985 = t2030 * t1984;
        const Scalar t2018 = (0.3e1 * t1984 - 0.300e1 + 0.7e1 * (0.2e1 + t1985) * t2028) * t1994;
        const Scalar t2019 = (0.2e1 - 0.200e1 * t1984 - 0.7e1 * (0.1e1 + t1985) * t2028) / q * t1994;
        const Scalar t1974 = t1993 * t2019 + t1992 * t2018;
        const Scalar t2021 = t1974 * t1987;
        const Scalar t1975 = t1993 * t2018 + t1992 * t2029;
        const Scalar t2020 = t1975 * t1987;
        const Scalar t1982 = t1993 * t2029 + t1992;
        const Scalar t2016 = t1982 * t1987;
        const Scalar t2015 = t1992 * t2019;
        const Scalar t2014 = t1987 * t2023;
        const Scalar t2013 = t1987 * t2015;
        const Scalar t1978 = t2002 * t2002;
        const Scalar t73 = t2010 * t2010;
        const Scalar t1970 = t1990 * t2014 + t1978 / 0.8e1 - t73 / 0.8e1;
        const Scalar t1968 = -l * t1978 + (-0.10e2 * t1990 * t1970 + t1991 * t2023) * t1987;
        const Scalar t1966 = -t1997 * t1978 + (0.4e1 / 0.3e1 * t1990 * t1968 + t1970 * t2027) * t1987;
        res[0] = t1993 * t2014 + t1970 * t2016 - t1968 * t2020 / 0.9e1 - t1966 * t2021 / 0.10e2 - (-t1996 * t1978 + (0.7e1 / 0.5e1 * t1990 * t1966 - t1991 * t1968 / 0.3e1) * t1987) * t2013 / 0.11e2;
        res[1] = (t1993 * t2022 + t1982 * t1972 + t1975 * t2024 + t1974 * t2025 + t2015 * t2026) * t1987;
        const Scalar t114 = t1997 * t1997;
        res[2] = t1993 * t1972 * t1987 + t2016 * t2024 + t2020 * t2025 + t2021 * t2026 - (-t114 * t1979 + (0.14e2 / 0.9e1 * t1990 * t1965 - t1991 * t1967 / 0.2e1) * t1987) * t2013 / 0.10e2;

    }
    
    template<>
    inline void HomotheticCompactPolynomial_segment_ScalisF_HornusGradF<6> (Scalar l, Scalar d, const Scalar w[3], Scalar res[3])
    {
        const Scalar t2044 = d * l + 0.1e1;
        const Scalar t2038 = 0.1e1 / t2044;
        const Scalar t2060 = t2044 * t2044;
        const Scalar t2 = t2060 * t2060;
        const Scalar t2041 = 0.1e1 / t2;
        const Scalar t2049 = w[1];
        const Scalar t2046 = t2049 * t2049;
        const Scalar t2076 = 0.12e2 * t2046;
        const Scalar t2048 = w[2];
        const Scalar t2050 = w[0];
        const Scalar t2069 = t2048 * t2050;
        const Scalar t2075 = 0.2e1 / 0.5e1 * t2069 + 0.4e1 / 0.5e1 * t2046;
        const Scalar t2042 = t2038 * t2041;
        const Scalar t2074 = -t2042 / 0.5e1 + 0.1e1 / 0.5e1;
        const Scalar t2045 = t2048 * t2048;
        const Scalar t2073 = t2045 / 0.5e1;
        const Scalar t2072 = -0.4e1 / 0.5e1 * t2048;
        const Scalar t2071 = -0.4e1 / 0.5e1 * t2050;
        const Scalar t2055 = 0.1e1 / d;
        const Scalar t2068 = t2049 * t2055;
        const Scalar t2061 = t2044 * t2060;
        const Scalar t2054 = l * l;
        const Scalar t2057 = t2054 * t2054;
        const Scalar t2056 = l * t2054;
        const Scalar t2051 = l * t2057;
        const Scalar t2047 = t2050 * t2050;
        const Scalar t8 = t2061 * t2061;
        const Scalar t2043 = 0.1e1 / t8;
        const Scalar t2040 = 0.1e1 / t2061;
        const Scalar t2039 = 0.1e1 / t2060;
        const Scalar t2035 = -(t2041 - 0.1e1) * t2055 / 0.4e1 - l * t2042;
        const Scalar t2034 = (-(t2040 - 0.1e1) * t2055 / 0.3e1 - l * t2041) * t2055 / 0.2e1 - t2054 * t2042;
        const Scalar t2033 = 0.3e1 / 0.4e1 * (0.2e1 / 0.3e1 * (-(t2039 - 0.1e1) * t2055 / 0.2e1 - l * t2040) * t2055 - t2054 * t2041) * t2055 - t2056 * t2042;
        const Scalar t2032 = (((-(t2038 - 0.1e1) * t2055 - l * t2039) * t2055 - t2054 * t2040) * t2055 - t2056 * t2041) * t2055 - t2057 * t2042;
        const Scalar t45 = log(t2044);
        const Scalar t2031 = 0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * (0.2e1 * (t45 * t2055 - l * t2038) * t2055 - t2054 * t2039) * t2055 - t2056 * t2040) * t2055 - t2057 * t2041) * t2055 - t2051 * t2042;
        const Scalar t100 = t2056 * t2056;
        res[0] = -t2045 * (t2032 * t2055 - t2051 * t2043) * t2068 + (-t2050 * (t2043 - 0.1e1) * t2055 / 0.6e1 - (t2055 * t2074 - l * t2043) * t2068) * t2047 + ((t2050 * t2076 + 0.3e1 * t2048 * t2047) * (0.2e1 / 0.5e1 * t2035 * t2055 - t2054 * t2043) + (0.3e1 * t2045 * t2050 + t2048 * t2076) * (0.4e1 / 0.5e1 * t2033 * t2055 - t2057 * t2043) + t2048 * t2045 * (0.6e1 / 0.5e1 * t2031 * t2055 - t100 * t2043) + (-0.12e2 * t2069 - 0.8e1 * t2046) * (0.3e1 / 0.5e1 * t2034 * t2055 - t2056 * t2043) * t2049) * t2055 / 0.6e1;
        res[1] = (t2047 * t2074 + t2034 * t2075 + t2032 * t2073 + (t2035 * t2071 + t2033 * t2072) * t2049) * t2055;
        res[2] = (t2047 * t2035 / 0.5e1 + t2033 * t2075 + t2031 * t2073 + (t2034 * t2071 + t2032 * t2072) * t2049) * t2055;

    }
    
    template<>
    inline void HomotheticCompactPolynomial_approx_segment_ScalisF_HornusGradF<6> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3])
    {
        const Scalar t2128 = q * d;
        const Scalar t2093 = t2128 + 0.1e1;
        const Scalar t2129 = 0.1e1 / t2093;
        const Scalar t1 = q * q;
        const Scalar t2099 = 0.1e1 / t1;
        const Scalar t2 = t2093 * t2093;
        const Scalar t3 = t2 * t2;
        const Scalar t2091 = t2129 / t3 / t2;
        const Scalar t2098 = w[0];
        const Scalar t2127 = 0.2e1 * t2098;
        const Scalar t2096 = w[2];
        const Scalar t2094 = 0.1e1 / t2096;
        const Scalar t2126 = d * t2094;
        const Scalar t2097 = w[1];
        const Scalar t2101 = l * l;
        const Scalar t2088 = t2096 * t2101 - 0.2e1 * t2097 * l + t2098;
        const Scalar t2105 = t2088 * t2088;
        const Scalar t2087 = t2088 * t2105;
        const Scalar t2113 = t2098 * t2098;
        const Scalar t2095 = t2098 * t2113;
        const Scalar t10 = t2097 * t2097;
        const Scalar t2089 = t2096 * t2098 - t10;
        const Scalar t2090 = t2096 * l - t2097;
        const Scalar t2120 = t2089 * t2094;
        const Scalar t2083 = 0.4e1 / 0.3e1 * (0.2e1 * t2089 * l + t2090 * t2088 + t2097 * t2098) * t2120 + t2090 * t2105 + t2097 * t2113;
        const Scalar t2123 = t2083 / 0.5e1;
        const Scalar t2082 = t2097 * t2094 * t2123 + t2087 / 0.6e1 - t2095 / 0.6e1;
        const Scalar t2079 = -l * t2087 + (-0.8e1 * t2097 * t2082 + t2098 * t2123) * t2094;
        const Scalar t2119 = t2101 * t2087;
        const Scalar t2077 = -t2119 + (0.10e2 / 0.7e1 * t2097 * t2079 + t2082 * t2127) * t2094;
        const Scalar t2125 = -t2077 / 0.8e1;
        const Scalar t2124 = 0.6e1 / 0.35e2 * t2083 * t2120 + t2090 * t2087 / 0.7e1 + t2097 * t2095 / 0.7e1;
        const Scalar t2092 = t2129 * t2091;
        const Scalar t2122 = (0.3e1 * t2091 - 0.300e1 + 0.7e1 * (0.2e1 + t2092) * t2128) * t2099;
        const Scalar t2121 = (0.2e1 - 0.200e1 * t2091 - 0.7e1 * (0.1e1 + t2092) * t2128) / q * t2099;
        const Scalar t2118 = t2094 * t2122;
        const Scalar t2117 = t2094 * t2121;
        const Scalar t2116 = t2094 * t2124;
        const Scalar t2086 = t2105 * t2105;
        const Scalar t57 = t2113 * t2113;
        const Scalar t2080 = t2097 * t2116 + t2086 / 0.8e1 - t57 / 0.8e1;
        const Scalar t2078 = -l * t2086 + (-0.10e2 * t2097 * t2080 + t2098 * t2124) * t2094;
        res[0] = t2116 - 0.7e1 * t2080 * t2126 - t2078 * t2118 / 0.9e1 - (-t2101 * t2086 + (0.4e1 / 0.3e1 * t2097 * t2078 + t2080 * t2127) * t2094) * t2117 / 0.10e2;
        res[1] = (t2123 - 0.7e1 * d * t2082 - t2079 * t2122 / 0.7e1 + t2121 * t2125) * t2094;
        res[2] = t2082 * t2094 + t2079 * t2126 + t2118 * t2125 - (-l * t2119 + (0.3e1 / 0.2e1 * t2097 * t2077 - 0.3e1 / 0.7e1 * t2098 * t2079) * t2094) * t2117 / 0.9e1;

    }
    
    
    

    /////////////////////////////////
    // Code for kernel of degree 8 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HomotheticCompactPolynomial_segment_F<8> (Scalar l, Scalar d, const Scalar w[3])
    {
        const Scalar t2138 = d * l + 0.1e1;
        const Scalar t2130 = 0.1e1 / t2138;
        const Scalar t2161 = t2138 * t2138;
        const Scalar t2163 = t2161 * t2161;
        const Scalar t2133 = 0.1e1 / t2163;
        const Scalar t2162 = t2138 * t2161;
        const Scalar t2 = t2162 * t2162;
        const Scalar t2135 = 0.1e1 / t2;
        const Scalar t2146 = w[1];
        const Scalar t2142 = t2146 * t2146;
        const Scalar t2180 = -0.32e2 * t2146 * t2142;
        const Scalar t2179 = 0.24e2 * t2142;
        const Scalar t2178 = -0.24e2 * t2146;
        const Scalar t2154 = 0.1e1 / d;
        const Scalar t2177 = t2154 / 0.2e1;
        const Scalar t2175 = 0.2e1 / 0.3e1 * t2154;
        const Scalar t2153 = l * l;
        const Scalar t2155 = l * t2153;
        const Scalar t2158 = t2155 * t2155;
        const Scalar t2156 = t2153 * t2153;
        const Scalar t2150 = l * t2156;
        const Scalar t2148 = l * t2158;
        const Scalar t2147 = w[0];
        const Scalar t2145 = w[2];
        const Scalar t2144 = t2147 * t2147;
        const Scalar t2143 = t2147 * t2144;
        const Scalar t2140 = t2145 * t2145;
        const Scalar t2139 = t2145 * t2140;
        const Scalar t6 = t2163 * t2163;
        const Scalar t2137 = 0.1e1 / t6;
        const Scalar t2136 = t2130 * t2135;
        const Scalar t2134 = t2130 * t2133;
        const Scalar t2132 = 0.1e1 / t2162;
        const Scalar t2131 = 0.1e1 / t2161;
        const Scalar t7 = t2144 * t2144;
        const Scalar t154 = t2140 * t2140;
        const Scalar t155 = log(t2138);
        const Scalar t185 = t2156 * t2156;
        return  -t7 * (t2137 - 0.1e1) * t2154 / 0.8e1 + (-t2143 * (-(t2136 - 0.1e1) * t2154 / 0.7e1 - l * t2137) - t2139 * (((((((-(t2130 - 0.1e1) * t2154 - l * t2131) * t2154 - t2153 * t2132) * t2154 - t2155 * t2133) * t2154 - t2156 * t2134) * t2154 - t2150 * t2135) * t2154 - t2158 * t2136) * t2154 - t2148 * t2137)) * t2146 * t2154 + ((t2144 * t2179 + 0.4e1 * t2145 * t2143) * (0.2e1 / 0.7e1 * (-(t2135 - 0.1e1) * t2154 / 0.6e1 - l * t2136) * t2154 - t2153 * t2137) + (0.6e1 * t2140 * t2144 + (0.48e2 * t2145 * t2147 + 0.16e2 * t2142) * t2142) * (0.4e1 / 0.7e1 * ((0.2e1 / 0.5e1 * (-(t2133 - 0.1e1) * t2154 / 0.4e1 - l * t2134) * t2154 - t2153 * t2135) * t2177 - t2155 * t2136) * t2154 - t2156 * t2137) + (t2140 * t2147 * t2178 + t2145 * t2180) * (0.5e1 / 0.7e1 * ((0.3e1 / 0.5e1 * ((-(t2132 - 0.1e1) * t2154 / 0.3e1 - l * t2133) * t2177 - t2153 * t2134) * t2154 - t2155 * t2135) * t2175 - t2156 * t2136) * t2154 - t2150 * t2137) + (t2140 * t2179 + 0.4e1 * t2139 * t2147) * (0.6e1 / 0.7e1 * (0.5e1 / 0.6e1 * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * ((-(t2131 - 0.1e1) * t2154 / 0.2e1 - l * t2132) * t2175 - t2153 * t2133) * t2154 - t2155 * t2134) * t2154 - t2156 * t2135) * t2154 - t2150 * t2136) * t2154 - t2158 * t2137) + (t2145 * t2144 * t2178 + t2147 * t2180) * (0.3e1 / 0.7e1 * ((-(t2134 - 0.1e1) * t2154 / 0.5e1 - l * t2135) * t2154 / 0.3e1 - t2153 * t2136) * t2154 - t2155 * t2137) + t154 * (0.8e1 / 0.7e1 * (0.7e1 / 0.6e1 * (0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * (0.2e1 * (t155 * t2154 - l * t2130) * t2154 - t2153 * t2131) * t2154 - t2155 * t2132) * t2154 - t2156 * t2133) * t2154 - t2150 * t2134) * t2154 - t2158 * t2135) * t2154 - t2148 * t2136) * t2154 - t185 * t2137)) * t2154 / 0.8e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_segment_GradF<8> (Scalar l, Scalar d, const Scalar w[3], Scalar res[2])
    {
        const Scalar t2197 = d * l + 0.1e1;
        const Scalar t2234 = 0.1e1 / t2197;
        const Scalar t2217 = t2197 * t2197;
        const Scalar t2219 = t2217 * t2217;
        const Scalar t2192 = 0.1e1 / t2219;
        const Scalar t2218 = t2197 * t2217;
        const Scalar t2 = t2218 * t2218;
        const Scalar t2194 = 0.1e1 / t2;
        const Scalar t2204 = w[1];
        const Scalar t2200 = t2204 * t2204;
        const Scalar t2233 = 0.12e2 * t2200;
        const Scalar t2203 = w[2];
        const Scalar t2199 = t2203 * t2203;
        const Scalar t2232 = -0.3e1 / 0.4e1 * t2199;
        const Scalar t2205 = w[0];
        const Scalar t2202 = t2205 * t2205;
        const Scalar t2231 = -0.3e1 / 0.4e1 * t2202;
        const Scalar t2211 = 0.1e1 / d;
        const Scalar t2230 = t2211 / 0.2e1;
        const Scalar t2228 = 0.2e1 / 0.3e1 * t2211;
        const Scalar t5 = t2219 * t2219;
        const Scalar t2196 = 0.1e1 / t5;
        const Scalar t2210 = l * l;
        const Scalar t2212 = l * t2210;
        const Scalar t2215 = t2212 * t2212;
        const Scalar t2227 = t2215 * t2196;
        const Scalar t2213 = t2210 * t2210;
        const Scalar t2207 = l * t2213;
        const Scalar t2201 = t2205 * t2202;
        const Scalar t2198 = t2203 * t2199;
        const Scalar t2195 = t2234 * t2194;
        const Scalar t2193 = t2234 * t2192;
        const Scalar t2191 = 0.1e1 / t2218;
        const Scalar t2190 = 0.1e1 / t2217;
        const Scalar t2189 = t2205 * t2233 + 0.3e1 * t2203 * t2202;
        const Scalar t2188 = 0.3e1 * t2199 * t2205 + t2203 * t2233;
        const Scalar t2187 = (-0.12e2 * t2203 * t2205 - 0.8e1 * t2200) * t2204;
        const Scalar t2186 = -(t2195 - 0.1e1) * t2211 / 0.7e1 - l * t2196;
        const Scalar t2185 = 0.2e1 / 0.7e1 * (-(t2194 - 0.1e1) * t2211 / 0.6e1 - l * t2195) * t2211 - t2210 * t2196;
        const Scalar t2184 = 0.3e1 / 0.7e1 * ((-(t2193 - 0.1e1) * t2211 / 0.5e1 - l * t2194) * t2211 / 0.3e1 - t2210 * t2195) * t2211 - t2212 * t2196;
        const Scalar t2183 = 0.4e1 / 0.7e1 * ((0.2e1 / 0.5e1 * (-(t2192 - 0.1e1) * t2211 / 0.4e1 - l * t2193) * t2211 - t2210 * t2194) * t2230 - t2212 * t2195) * t2211 - t2213 * t2196;
        const Scalar t2182 = 0.5e1 / 0.7e1 * ((0.3e1 / 0.5e1 * ((-(t2191 - 0.1e1) * t2211 / 0.3e1 - l * t2192) * t2230 - t2210 * t2193) * t2211 - t2212 * t2194) * t2228 - t2213 * t2195) * t2211 - t2207 * t2196;
        const Scalar t2181 = 0.6e1 / 0.7e1 * (0.5e1 / 0.6e1 * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * ((-(t2190 - 0.1e1) * t2211 / 0.2e1 - l * t2191) * t2228 - t2210 * t2192) * t2211 - t2212 * t2193) * t2211 - t2213 * t2194) * t2211 - t2207 * t2195) * t2211 - t2227;
        res[0] = (-t2201 * (t2196 - 0.1e1) / 0.8e1 + t2189 * t2185 / 0.8e1 + t2187 * t2184 / 0.8e1 + t2188 * t2183 / 0.8e1 + t2198 * t2181 / 0.8e1 + (t2186 * t2231 + t2182 * t2232) * t2204) * t2211;
        res[1] = (t2185 * t2231 + t2181 * t2232) * t2211 * t2204 + (t2201 * t2186 + t2187 * t2183 + t2188 * t2182 + t2198 * (((((((-(t2234 - 0.1e1) * t2211 - l * t2190) * t2211 - t2210 * t2191) * t2211 - t2212 * t2192) * t2211 - t2213 * t2193) * t2211 - t2207 * t2194) * t2211 - t2215 * t2195) * t2211 - l * t2227) + t2189 * t2184) * t2211 / 0.8e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_segment_FGradF<8> (Scalar l, Scalar d, const Scalar w[3], Scalar res[3])
    {
        const Scalar t2254 = d * l + 0.1e1;
        const Scalar t2277 = t2254 * t2254;
        const Scalar t2279 = t2277 * t2277;
        const Scalar t2 = t2279 * t2279;
        const Scalar t2253 = 0.1e1 / t2;
        const Scalar t2306 = -t2253 / 0.8e1;
        const Scalar t2262 = w[1];
        const Scalar t2258 = t2262 * t2262;
        const Scalar t2305 = 0.3e1 / 0.2e1 * t2258;
        const Scalar t2246 = 0.1e1 / t2254;
        const Scalar t2249 = 0.1e1 / t2279;
        const Scalar t2278 = t2254 * t2277;
        const Scalar t4 = t2278 * t2278;
        const Scalar t2251 = 0.1e1 / t4;
        const Scalar t2257 = t2262 * t2258;
        const Scalar t2261 = w[2];
        const Scalar t2263 = w[0];
        const Scalar t2290 = t2261 * t2263;
        const Scalar t2303 = -0.3e1 / 0.2e1 * t2262 * t2290 - t2257;
        const Scalar t2256 = t2261 * t2261;
        const Scalar t2294 = t2256 * t2263;
        const Scalar t2302 = 0.3e1 / 0.8e1 * t2294 + t2261 * t2305;
        const Scalar t2260 = t2263 * t2263;
        const Scalar t2291 = t2261 * t2260;
        const Scalar t2301 = t2263 * t2305 + 0.3e1 / 0.8e1 * t2291;
        const Scalar t2300 = t2306 + 0.1e1 / 0.8e1;
        const Scalar t2270 = 0.1e1 / d;
        const Scalar t2299 = t2270 / 0.2e1;
        const Scalar t2298 = 0.2e1 / 0.3e1 * t2270;
        const Scalar t2247 = 0.1e1 / t2277;
        const Scalar t2248 = 0.1e1 / t2278;
        const Scalar t2250 = t2246 * t2249;
        const Scalar t2252 = t2246 * t2251;
        const Scalar t2255 = t2261 * t2256;
        const Scalar t2269 = l * l;
        const Scalar t2271 = l * t2269;
        const Scalar t2274 = t2271 * t2271;
        const Scalar t2264 = l * t2274;
        const Scalar t2272 = t2269 * t2269;
        const Scalar t2266 = l * t2272;
        const Scalar t2297 = t2255 * (((((((-(t2246 - 0.1e1) * t2270 - l * t2247) * t2270 - t2269 * t2248) * t2270 - t2271 * t2249) * t2270 - t2272 * t2250) * t2270 - t2266 * t2251) * t2270 - t2274 * t2252) * t2270 - t2264 * t2253);
        const Scalar t2236 = 0.6e1 / 0.7e1 * (0.5e1 / 0.6e1 * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * ((-(t2247 - 0.1e1) * t2270 / 0.2e1 - l * t2248) * t2298 - t2269 * t2249) * t2270 - t2271 * t2250) * t2270 - t2272 * t2251) * t2270 - t2266 * t2252) * t2270 - t2274 * t2253;
        const Scalar t2296 = t2255 * t2236;
        const Scalar t2295 = t2256 * t2236;
        const Scalar t2241 = -(t2252 - 0.1e1) * t2270 / 0.7e1 - l * t2253;
        const Scalar t2259 = t2263 * t2260;
        const Scalar t2293 = t2259 * t2241;
        const Scalar t2240 = 0.2e1 / 0.7e1 * (-(t2251 - 0.1e1) * t2270 / 0.6e1 - l * t2252) * t2270 - t2269 * t2253;
        const Scalar t2292 = t2260 * t2240;
        const Scalar t2239 = 0.3e1 / 0.7e1 * ((-(t2250 - 0.1e1) * t2270 / 0.5e1 - l * t2251) * t2270 / 0.3e1 - t2269 * t2252) * t2270 - t2271 * t2253;
        const Scalar t2238 = 0.4e1 / 0.7e1 * ((0.2e1 / 0.5e1 * (-(t2249 - 0.1e1) * t2270 / 0.4e1 - l * t2250) * t2270 - t2269 * t2251) * t2299 - t2271 * t2252) * t2270 - t2272 * t2253;
        const Scalar t2237 = 0.5e1 / 0.7e1 * ((0.3e1 / 0.5e1 * ((-(t2248 - 0.1e1) * t2270 / 0.3e1 - l * t2249) * t2299 - t2269 * t2250) * t2270 - t2271 * t2251) * t2298 - t2272 * t2252) * t2270 - t2266 * t2253;
        const Scalar t113 = t2260 * t2260;
        const Scalar t141 = t2272 * t2272;
        const Scalar t155 = log(t2254);
        res[0] = (t113 * t2300 + 0.4e1 * (-t2263 * t2239 - t2261 * t2237) * t2257 + t2261 * t2259 * t2240 / 0.2e1 + t2263 * t2296 / 0.2e1 + (-t2293 - 0.3e1 * t2239 * t2291 - t2297 - 0.3e1 * t2237 * t2294) * t2262 + (0.3e1 * t2295 + 0.3e1 * t2292 + (0.6e1 * t2290 + 0.2e1 * t2258) * t2238) * t2258 + (0.3e1 / 0.4e1 * t2260 * t2238 + (t141 * t2306 + (-t2264 * t2252 / 0.7e1 + (-t2274 * t2251 / 0.6e1 + (-t2266 * t2250 / 0.5e1 + (-t2272 * t2249 / 0.4e1 + (-t2271 * t2248 / 0.3e1 + (-t2269 * t2247 / 0.2e1 + (t155 * t2270 - l * t2246) * t2270) * t2270) * t2270) * t2270) * t2270) * t2270) * t2270) * t2256) * t2256) * t2270;
        res[1] = (t2259 * t2300 + t2240 * t2301 + t2239 * t2303 + t2238 * t2302 + t2296 / 0.8e1 + (-0.3e1 / 0.4e1 * t2260 * t2241 - 0.3e1 / 0.4e1 * t2256 * t2237) * t2262) * t2270;
        res[2] = (t2293 / 0.8e1 + t2239 * t2301 + t2238 * t2303 + t2237 * t2302 + t2297 / 0.8e1 + (-0.3e1 / 0.4e1 * t2292 - 0.3e1 / 0.4e1 * t2295) * t2262) * t2270;

    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_approx_segment_F<8> (Scalar l, Scalar d, Scalar q, const Scalar w[3])
    {
        const Scalar t2343 = q * d;
        const Scalar t2316 = t2343 + 0.1e1;
        const Scalar t2344 = 0.1e1 / t2316;
        const Scalar t1 = t2316 * t2316;
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t2315 = t2344 / t3;
        const Scalar t2318 = w[2];
        const Scalar t2319 = w[1];
        const Scalar t2320 = w[0];
        const Scalar t2321 = l * l;
        const Scalar t2311 = t2318 * t2321 - 0.2e1 * t2319 * l + t2320;
        const Scalar t9 = t2319 * t2319;
        const Scalar t2312 = t2318 * t2320 - t9;
        const Scalar t2313 = t2318 * l - t2319;
        const Scalar t2324 = t2311 * t2311;
        const Scalar t2326 = t2324 * t2324;
        const Scalar t2333 = t2320 * t2320;
        const Scalar t2335 = t2333 * t2333;
        const Scalar t2338 = t2319 * t2333;
        const Scalar t2340 = t2313 * t2324;
        const Scalar t2317 = 0.1e1 / t2318;
        const Scalar t2341 = t2312 * t2317;
        const Scalar t2342 = 0.8e1 / 0.63e2 * (0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t2312 * l + t2313 * t2311 + t2319 * t2320) * t2341 + t2340 + t2338) * t2341 + t2311 * t2340 + t2320 * t2338) * t2341 + t2313 * t2326 / 0.9e1 + t2319 * t2335 / 0.9e1;
        const Scalar t2337 = t2317 * t2342;
        const Scalar t2314 = t2344 * t2315;
        const Scalar t2310 = t2311 * t2326;
        const Scalar t2308 = t2319 * t2337 + t2310 / 0.10e2 - t2320 * t2335 / 0.10e2;
        const Scalar t2307 = -l * t2310 + (-0.12e2 * t2319 * t2308 + t2320 * t2342) * t2317;
        const Scalar t70 = q * q;
        return  t2337 - 0.9e1 * d * t2308 * t2317 + (-0.9090909091e-1 * (0.3e1 * t2315 - 0.300e1 + 0.9e1 * (0.2e1 + t2314) * t2343) * t2307 - 0.8333333333e-1 * (0.2e1 - 0.200e1 * t2315 - 0.9e1 * (0.1e1 + t2314) * t2343) / q * (-0.1e1 * t2321 * t2310 + (0.1272727273e1 * t2319 * t2307 + 0.2e1 * t2320 * t2308) * t2317)) * t2317 / t70;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_approx_segment_GradF<8> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[2])
    {
        const Scalar t2387 = q * d;
        const Scalar t2357 = t2387 + 0.1e1;
        const Scalar t2388 = 0.1e1 / t2357;
        const Scalar t1 = q * q;
        const Scalar t2362 = 0.1e1 / t1;
        const Scalar t2 = t2357 * t2357;
        const Scalar t3 = t2 * t2;
        const Scalar t4 = t3 * t3;
        const Scalar t2356 = t2388 / t4;
        const Scalar t2359 = w[2];
        const Scalar t2360 = w[1];
        const Scalar t2361 = w[0];
        const Scalar t2364 = l * l;
        const Scalar t2352 = t2359 * t2364 - 0.2e1 * t2360 * l + t2361;
        const Scalar t10 = t2360 * t2360;
        const Scalar t2353 = t2359 * t2361 - t10;
        const Scalar t2354 = t2359 * l - t2360;
        const Scalar t2376 = t2361 * t2361;
        const Scalar t2381 = t2360 * t2376;
        const Scalar t2368 = t2352 * t2352;
        const Scalar t2382 = t2354 * t2368;
        const Scalar t2358 = 0.1e1 / t2359;
        const Scalar t2383 = t2353 * t2358;
        const Scalar t2386 = 0.6e1 / 0.35e2 * (0.4e1 / 0.3e1 * (0.2e1 * t2353 * l + t2354 * t2352 + t2360 * t2361) * t2383 + t2382 + t2381) * t2383 + t2352 * t2382 / 0.7e1 + t2361 * t2381 / 0.7e1;
        const Scalar t2355 = t2388 * t2356;
        const Scalar t2385 = (0.3e1 * t2356 - 0.300e1 + 0.9e1 * (0.2e1 + t2355) * t2387) * t2362;
        const Scalar t2384 = (0.2e1 - 0.200e1 * t2356 - 0.9e1 * (0.1e1 + t2355) * t2387) / q * t2362;
        const Scalar t2351 = t2368 * t2368;
        const Scalar t2380 = t2364 * t2351;
        const Scalar t41 = t2376 * t2376;
        const Scalar t2347 = t2360 * t2358 * t2386 + t2351 / 0.8e1 - t41 / 0.8e1;
        const Scalar t2346 = -l * t2351 + (-0.10e2 * t2360 * t2347 + t2361 * t2386) * t2358;
        const Scalar t2345 = -t2380 + (0.4e1 / 0.3e1 * t2360 * t2346 + 0.2e1 * t2361 * t2347) * t2358;
        const Scalar t2379 = t2345 / 0.10e2;
        res[0] = (t2386 - 0.9e1 * d * t2347 - t2346 * t2385 / 0.9e1 - t2379 * t2384) * t2358;
        res[1] = (t2347 + d * t2346 - t2379 * t2385 + (l * t2380 - (0.7e1 / 0.5e1 * t2360 * t2345 - t2361 * t2346 / 0.3e1) * t2358) * t2384 / 0.11e2) * t2358;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_approx_segment_FGradF<8> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3])
    {
        const Scalar t2444 = q * d;
        const Scalar t2405 = t2444 + 0.1e1;
        const Scalar t2445 = 0.1e1 / t2405;
        const Scalar t1 = q * q;
        const Scalar t2411 = 0.1e1 / t1;
        const Scalar t2 = t2405 * t2405;
        const Scalar t3 = t2 * t2;
        const Scalar t4 = t3 * t3;
        const Scalar t2404 = t2445 / t4;
        const Scalar t2410 = w[0];
        const Scalar t2443 = 0.2e1 * t2410;
        const Scalar t2408 = w[2];
        const Scalar t2406 = 0.1e1 / t2408;
        const Scalar t2442 = d * t2406;
        const Scalar t2409 = w[1];
        const Scalar t2413 = l * l;
        const Scalar t2400 = t2408 * t2413 - 0.2e1 * t2409 * l + t2410;
        const Scalar t10 = t2409 * t2409;
        const Scalar t2401 = t2408 * t2410 - t10;
        const Scalar t2402 = t2408 * l - t2409;
        const Scalar t2426 = t2410 * t2410;
        const Scalar t2435 = t2409 * t2426;
        const Scalar t2417 = t2400 * t2400;
        const Scalar t2436 = t2402 * t2417;
        const Scalar t2437 = t2401 * t2406;
        const Scalar t2395 = 0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t2401 * l + t2402 * t2400 + t2409 * t2410) * t2437 + t2436 + t2435) * t2437 + t2400 * t2436 + t2410 * t2435;
        const Scalar t2419 = t2417 * t2417;
        const Scalar t2428 = t2426 * t2426;
        const Scalar t2441 = 0.8e1 / 0.63e2 * t2395 * t2437 + t2402 * t2419 / 0.9e1 + t2409 * t2428 / 0.9e1;
        const Scalar t2440 = t2395 / 0.7e1;
        const Scalar t2403 = t2445 * t2404;
        const Scalar t2439 = (0.3e1 * t2404 - 0.300e1 + 0.9e1 * (0.2e1 + t2403) * t2444) * t2411;
        const Scalar t2438 = (0.2e1 - 0.200e1 * t2404 - 0.9e1 * (0.1e1 + t2403) * t2444) / q * t2411;
        const Scalar t2434 = t2413 * t2419;
        const Scalar t2394 = t2409 * t2406 * t2440 + t2419 / 0.8e1 - t2428 / 0.8e1;
        const Scalar t2391 = -l * t2419 + (-0.10e2 * t2409 * t2394 + t2410 * t2440) * t2406;
        const Scalar t2389 = -t2434 + (0.4e1 / 0.3e1 * t2409 * t2391 + t2394 * t2443) * t2406;
        const Scalar t2433 = t2389 / 0.10e2;
        const Scalar t2432 = t2406 * t2439;
        const Scalar t2431 = t2406 * t2438;
        const Scalar t2430 = t2406 * t2441;
        const Scalar t2398 = t2400 * t2419;
        const Scalar t2392 = t2409 * t2430 + t2398 / 0.10e2 - t2410 * t2428 / 0.10e2;
        const Scalar t2390 = -l * t2398 + (-0.12e2 * t2409 * t2392 + t2410 * t2441) * t2406;
        res[0] = t2430 - 0.9e1 * t2392 * t2442 - t2390 * t2432 / 0.11e2 - (-t2413 * t2398 + (0.14e2 / 0.11e2 * t2409 * t2390 + t2392 * t2443) * t2406) * t2431 / 0.12e2;
        res[1] = (t2440 - 0.9e1 * d * t2394 - t2391 * t2439 / 0.9e1 - t2433 * t2438) * t2406;
        res[2] = t2394 * t2406 + t2391 * t2442 - t2432 * t2433 - (-l * t2434 + (0.7e1 / 0.5e1 * t2409 * t2389 - t2410 * t2391 / 0.3e1) * t2406) * t2431 / 0.11e2;

    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_segment_F_cste<8> (Scalar l, const Scalar w[3])
    {
        const Scalar t2450 = w[2];
        const Scalar t2462 = t2450 * l;
        const Scalar t2451 = w[1];
        const Scalar t2452 = w[0];
        const Scalar t2 = t2451 * t2451;
        const Scalar t2447 = t2450 * t2452 - t2;
        const Scalar t2449 = 0.1e1 / t2450;
        const Scalar t2461 = t2447 * t2449;
        const Scalar t2448 = t2462 - t2451;
        const Scalar t2446 = t2452 + (-0.2e1 * t2451 + t2462) * l;
        const Scalar t2453 = t2446 * t2446;
        const Scalar t2460 = t2448 * t2453;
        const Scalar t2456 = t2452 * t2452;
        const Scalar t2459 = t2451 * t2456;
        const Scalar t21 = t2453 * t2453;
        const Scalar t23 = t2456 * t2456;
        return  (0.8e1 / 0.7e1 * (0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t2447 * l + t2448 * t2446 + t2451 * t2452) * t2461 + t2460 + t2459) * t2461 + t2446 * t2460 + t2452 * t2459) * t2461 + t2448 * t21 + t2451 * t23) * t2449 / 0.9e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_segment_FGradF_cste<8> (Scalar l, const Scalar w[3], Scalar res[3])
    {
        const Scalar t2470 = w[2];
        const Scalar t2483 = t2470 * l;
        const Scalar t2471 = w[1];
        const Scalar t2472 = w[0];
        const Scalar t2 = t2471 * t2471;
        const Scalar t2466 = t2470 * t2472 - t2;
        const Scalar t2468 = 0.1e1 / t2470;
        const Scalar t2482 = t2466 * t2468;
        const Scalar t2467 = t2483 - t2471;
        const Scalar t2465 = t2472 + (-0.2e1 * t2471 + t2483) * l;
        const Scalar t2473 = t2465 * t2465;
        const Scalar t2481 = t2467 * t2473;
        const Scalar t2476 = t2472 * t2472;
        const Scalar t2480 = t2471 * t2476;
        const Scalar t2463 = 0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t2466 * l + t2467 * t2465 + t2471 * t2472) * t2482 + t2481 + t2480) * t2482 + t2465 * t2481 + t2472 * t2480;
        const Scalar t2479 = t2463 * t2468 / 0.7e1;
        const Scalar t2469 = t2476 * t2476;
        const Scalar t2464 = t2473 * t2473;
        res[0] = (0.8e1 / 0.7e1 * t2463 * t2482 + t2467 * t2464 + t2471 * t2469) * t2468 / 0.9e1;
        res[1] = t2479;
        res[2] = (t2471 * t2479 + t2464 / 0.8e1 - t2469 / 0.8e1) * t2468;

    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_density_segment_F<8> (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g)
    {
        const Scalar t2503 = w[2];
        const Scalar t2496 = t2503 * t2503;
        const Scalar t2557 = 0.24e2 * t2496;
        const Scalar t2505 = w[0];
        const Scalar t2502 = t2505 * t2505;
        const Scalar t2556 = 0.24e2 * t2502;
        const Scalar t2493 = d * l + 0.1e1;
        const Scalar t2485 = 0.1e1 / t2493;
        const Scalar t2523 = t2493 * t2493;
        const Scalar t2525 = t2523 * t2523;
        const Scalar t2488 = 0.1e1 / t2525;
        const Scalar t2524 = t2493 * t2523;
        const Scalar t2 = t2524 * t2524;
        const Scalar t2490 = 0.1e1 / t2;
        const Scalar t2507 = g[0];
        const Scalar t2555 = 0.4e1 * t2507;
        const Scalar t2515 = 0.1e1 / d;
        const Scalar t2554 = 0.2e1 * t2515;
        const Scalar t2504 = w[1];
        const Scalar t2499 = t2504 * t2504;
        const Scalar t2553 = -0.32e2 * t2504 * t2499;
        const Scalar t2552 = -0.24e2 * t2504;
        const Scalar t2551 = t2515 / 0.2e1;
        const Scalar t2549 = 0.2e1 / 0.3e1 * t2515;
        const Scalar t2548 = 0.3e1 / 0.2e1 * t2515;
        const Scalar t2547 = 0.4e1 / 0.3e1 * t2515;
        const Scalar t6 = log(t2493);
        const Scalar t2546 = t6 * t2515;
        const Scalar t2506 = g[1];
        const Scalar t2545 = t2506 * t2499;
        const Scalar t2544 = t2506 * t2505;
        const Scalar t2543 = t2507 * t2499;
        const Scalar t2542 = t2507 * t2504;
        const Scalar t7 = t2525 * t2525;
        const Scalar t2492 = 0.1e1 / t7;
        const Scalar t2514 = l * l;
        const Scalar t2517 = t2514 * t2514;
        const Scalar t2521 = t2517 * t2517;
        const Scalar t2541 = t2521 * t2492;
        const Scalar t2540 = -0.8e1 * t2506 * t2504;
        const Scalar t2539 = -0.8e1 * t2542;
        const Scalar t2538 = t2505 * t2553;
        const Scalar t2537 = t2503 * t2553;
        const Scalar t2536 = 0.6e1 * t2496 * t2502 + (0.48e2 * t2503 * t2505 + 0.16e2 * t2499) * t2499;
        const Scalar t2516 = l * t2514;
        const Scalar t2519 = t2516 * t2516;
        const Scalar t2511 = l * t2517;
        const Scalar t2509 = l * t2519;
        const Scalar t2501 = t2505 * t2502;
        const Scalar t2500 = t2502 * t2502;
        const Scalar t2495 = t2503 * t2496;
        const Scalar t2494 = t2496 * t2496;
        const Scalar t2491 = t2485 * t2490;
        const Scalar t2489 = t2485 * t2488;
        const Scalar t2487 = 0.1e1 / t2524;
        const Scalar t2486 = 0.1e1 / t2523;
        return  -t2507 * t2500 * (t2492 - 0.1e1) * t2515 / 0.8e1 + ((t2506 * t2500 + t2501 * t2539) * (-(t2491 - 0.1e1) * t2515 / 0.7e1 - l * t2492) + (t2543 * t2556 + (t2540 + t2503 * t2555) * t2501) * (0.2e1 / 0.7e1 * (-(t2490 - 0.1e1) * t2515 / 0.6e1 - l * t2491) * t2515 - t2514 * t2492) + (0.4e1 * t2506 * t2503 * t2501 + t2507 * t2538 + (-t2503 * t2542 + t2545) * t2556) * (0.3e1 / 0.7e1 * ((-(t2489 - 0.1e1) * t2515 / 0.5e1 - l * t2490) * t2515 / 0.3e1 - t2514 * t2491) * t2515 - t2516 * t2492) + ((t2538 + t2503 * t2502 * t2552) * t2506 + t2536 * t2507) * (0.4e1 / 0.7e1 * ((0.2e1 / 0.5e1 * (-(t2488 - 0.1e1) * t2515 / 0.4e1 - l * t2489) * t2515 - t2514 * t2490) * t2551 - t2516 * t2491) * t2515 - t2517 * t2492) + ((t2496 * t2505 * t2552 + t2537) * t2507 + t2536 * t2506) * (0.5e1 / 0.7e1 * ((0.3e1 / 0.5e1 * ((-(t2487 - 0.1e1) * t2515 / 0.3e1 - l * t2488) * t2551 - t2514 * t2489) * t2515 - t2516 * t2490) * t2549 - t2517 * t2491) * t2515 - t2511 * t2492) + (t2506 * t2537 + t2495 * t2505 * t2555 + (-t2504 * t2544 + t2543) * t2557) * (0.6e1 / 0.7e1 * (0.5e1 / 0.6e1 * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * ((-(t2486 - 0.1e1) * t2515 / 0.2e1 - l * t2487) * t2549 - t2514 * t2488) * t2515 - t2516 * t2489) * t2515 - t2517 * t2490) * t2515 - t2511 * t2491) * t2515 - t2519 * t2492) + (t2545 * t2557 + (0.4e1 * t2544 + t2539) * t2495) * (((((((-(t2485 - 0.1e1) * t2515 - l * t2486) * t2515 - t2514 * t2487) * t2515 - t2516 * t2488) * t2515 - t2517 * t2489) * t2515 - t2511 * t2490) * t2515 - t2519 * t2491) * t2515 - t2509 * t2492) + (t2495 * t2540 + t2507 * t2494) * (0.8e1 / 0.7e1 * (0.7e1 / 0.6e1 * (0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * ((((t2546 - l * t2485) * t2554 - t2514 * t2486) * t2548 - t2516 * t2487) * t2547 - t2517 * t2488) * t2515 - t2511 * t2489) * t2515 - t2519 * t2490) * t2515 - t2509 * t2491) * t2515 - t2541) + t2506 * t2494 * (0.9e1 / 0.7e1 * ((0.7e1 / 0.5e1 * ((0.5e1 / 0.3e1 * ((0.3e1 * ((l - t2546) * t2554 - t2514 * t2485) * t2515 - t2516 * t2486) * t2554 - t2517 * t2487) * t2515 - t2511 * t2488) * t2548 - t2519 * t2489) * t2515 - t2509 * t2490) * t2547 - t2521 * t2491) * t2515 - l * t2541)) * t2515 / 0.8e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_segment_GradF<8> (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[2])
    {
        const Scalar t2579 = d * l + 0.1e1;
        const Scalar t2571 = 0.1e1 / t2579;
        const Scalar t2604 = t2579 * t2579;
        const Scalar t2606 = t2604 * t2604;
        const Scalar t2574 = 0.1e1 / t2606;
        const Scalar t2605 = t2579 * t2604;
        const Scalar t2 = t2605 * t2605;
        const Scalar t2576 = 0.1e1 / t2;
        const Scalar t2587 = w[1];
        const Scalar t2583 = t2587 * t2587;
        const Scalar t2626 = -0.8e1 * t2587 * t2583;
        const Scalar t2590 = g[0];
        const Scalar t2625 = 0.3e1 * t2590;
        const Scalar t2597 = 0.1e1 / d;
        const Scalar t2624 = t2597 / 0.2e1;
        const Scalar t2622 = 0.2e1 / 0.3e1 * t2597;
        const Scalar t2588 = w[0];
        const Scalar t2585 = t2588 * t2588;
        const Scalar t2584 = t2588 * t2585;
        const Scalar t2621 = t2584 * t2590;
        const Scalar t2589 = g[1];
        const Scalar t2620 = t2589 * t2588;
        const Scalar t2619 = t2590 * t2583;
        const Scalar t2618 = t2590 * t2587;
        const Scalar t2617 = -0.6e1 * t2589 * t2587;
        const Scalar t2616 = -0.6e1 * t2618;
        const Scalar t2615 = 0.12e2 * t2589 * t2583;
        const Scalar t2586 = w[2];
        const Scalar t2581 = t2586 * t2586;
        const Scalar t2580 = t2586 * t2581;
        const Scalar t2614 = t2589 * t2580 / 0.8e1;
        const Scalar t2596 = l * l;
        const Scalar t2598 = l * t2596;
        const Scalar t2601 = t2598 * t2598;
        const Scalar t2599 = t2596 * t2596;
        const Scalar t2593 = l * t2599;
        const Scalar t2591 = l * t2601;
        const Scalar t10 = t2606 * t2606;
        const Scalar t2578 = 0.1e1 / t10;
        const Scalar t2577 = t2571 * t2576;
        const Scalar t2575 = t2571 * t2574;
        const Scalar t2573 = 0.1e1 / t2605;
        const Scalar t2572 = 0.1e1 / t2604;
        const Scalar t2570 = t2589 * t2584 + t2585 * t2616;
        const Scalar t2569 = t2581 * t2617 + t2590 * t2580;
        const Scalar t2568 = -(t2577 - 0.1e1) * t2597 / 0.7e1 - l * t2578;
        const Scalar t2567 = 0.12e2 * t2588 * t2619 + (t2586 * t2625 + t2617) * t2585;
        const Scalar t2566 = t2586 * t2615 + (t2616 + 0.3e1 * t2620) * t2581;
        const Scalar t2565 = 0.2e1 / 0.7e1 * (-(t2576 - 0.1e1) * t2597 / 0.6e1 - l * t2577) * t2597 - t2596 * t2578;
        const Scalar t2564 = t2589 * t2626 + t2581 * t2588 * t2625 + 0.12e2 * (t2619 - t2587 * t2620) * t2586;
        const Scalar t2563 = t2590 * t2626 + t2588 * t2615 + (0.3e1 * t2589 * t2585 - 0.12e2 * t2588 * t2618) * t2586;
        const Scalar t2562 = 0.3e1 / 0.7e1 * ((-(t2575 - 0.1e1) * t2597 / 0.5e1 - l * t2576) * t2597 / 0.3e1 - t2596 * t2577) * t2597 - t2598 * t2578;
        const Scalar t2561 = 0.4e1 / 0.7e1 * ((0.2e1 / 0.5e1 * (-(t2574 - 0.1e1) * t2597 / 0.4e1 - l * t2575) * t2597 - t2596 * t2576) * t2624 - t2598 * t2577) * t2597 - t2599 * t2578;
        const Scalar t2560 = 0.5e1 / 0.7e1 * ((0.3e1 / 0.5e1 * ((-(t2573 - 0.1e1) * t2597 / 0.3e1 - l * t2574) * t2624 - t2596 * t2575) * t2597 - t2598 * t2576) * t2622 - t2599 * t2577) * t2597 - t2593 * t2578;
        const Scalar t2559 = 0.6e1 / 0.7e1 * (0.5e1 / 0.6e1 * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * ((-(t2572 - 0.1e1) * t2597 / 0.2e1 - l * t2573) * t2622 - t2596 * t2574) * t2597 - t2598 * t2575) * t2597 - t2599 * t2576) * t2597 - t2593 * t2577) * t2597 - t2601 * t2578;
        const Scalar t2558 = ((((((-(t2571 - 0.1e1) * t2597 - l * t2572) * t2597 - t2596 * t2573) * t2597 - t2598 * t2574) * t2597 - t2599 * t2575) * t2597 - t2593 * t2576) * t2597 - t2601 * t2577) * t2597 - t2591 * t2578;
        res[0] = (-(t2578 - 0.1e1) * t2621 / 0.8e1 + t2570 * t2568 / 0.8e1 + t2567 * t2565 / 0.8e1 + t2563 * t2562 / 0.8e1 + t2564 * t2561 / 0.8e1 + t2566 * t2560 / 0.8e1 + t2569 * t2559 / 0.8e1 + t2558 * t2614) * t2597;
        const Scalar t157 = log(t2579);
        const Scalar t187 = t2599 * t2599;
        res[1] = (0.8e1 / 0.7e1 * (0.7e1 / 0.6e1 * (0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * (0.2e1 * (t157 * t2597 - l * t2571) * t2597 - t2596 * t2572) * t2597 - t2598 * t2573) * t2597 - t2599 * t2574) * t2597 - t2593 * t2575) * t2597 - t2601 * t2576) * t2597 - t2591 * t2577) * t2597 - t187 * t2578) * t2597 * t2614 + (t2568 * t2621 + t2570 * t2565 + t2567 * t2562 + t2564 * t2560 + t2566 * t2559 + t2569 * t2558 + t2563 * t2561) * t2597 / 0.8e1;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_segment_FGradF<8> (Scalar l, Scalar d, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[3])
    {
        const Scalar t2661 = w[2];
        const Scalar t2654 = t2661 * t2661;
        const Scalar t2736 = 0.24e2 * t2654;
        const Scalar t2663 = w[0];
        const Scalar t2660 = t2663 * t2663;
        const Scalar t2735 = 0.24e2 * t2660;
        const Scalar t2734 = t2654 / 0.8e1;
        const Scalar t2733 = t2660 / 0.8e1;
        const Scalar t2665 = g[0];
        const Scalar t2732 = t2665 / 0.8e1;
        const Scalar t2662 = w[1];
        const Scalar t2657 = t2662 * t2662;
        const Scalar t2664 = g[1];
        const Scalar t2713 = t2664 * t2657;
        const Scalar t2731 = 0.3e1 / 0.2e1 * t2713;
        const Scalar t2651 = d * l + 0.1e1;
        const Scalar t2643 = 0.1e1 / t2651;
        const Scalar t2681 = t2651 * t2651;
        const Scalar t2683 = t2681 * t2681;
        const Scalar t2646 = 0.1e1 / t2683;
        const Scalar t2682 = t2651 * t2681;
        const Scalar t2 = t2682 * t2682;
        const Scalar t2648 = 0.1e1 / t2;
        const Scalar t2673 = 0.1e1 / d;
        const Scalar t2728 = 0.2e1 * t2673;
        const Scalar t2656 = t2662 * t2657;
        const Scalar t2727 = -0.32e2 * t2656;
        const Scalar t2726 = -0.24e2 * t2662;
        const Scalar t2706 = t2665 * t2663;
        const Scalar t2725 = -t2665 * t2656 + t2663 * t2731 + (0.3e1 * t2664 * t2660 - 0.12e2 * t2662 * t2706) * t2661 / 0.8e1;
        const Scalar t2709 = t2665 * t2657;
        const Scalar t2710 = t2664 * t2663;
        const Scalar t2695 = t2709 - t2662 * t2710;
        const Scalar t2724 = -t2664 * t2656 + 0.3e1 / 0.8e1 * t2654 * t2706 + 0.3e1 / 0.2e1 * t2695 * t2661;
        const Scalar t2707 = t2665 * t2662;
        const Scalar t2701 = -0.6e1 * t2707;
        const Scalar t2723 = t2661 * t2731 + (t2701 + 0.3e1 * t2710) * t2734;
        const Scalar t2711 = t2664 * t2662;
        const Scalar t2703 = -0.6e1 * t2711;
        const Scalar t2708 = t2665 * t2661;
        const Scalar t2722 = 0.3e1 / 0.2e1 * t2657 * t2706 + (0.3e1 * t2708 + t2703) * t2733;
        const Scalar t2653 = t2661 * t2654;
        const Scalar t2721 = t2703 * t2734 + t2653 * t2732;
        const Scalar t2659 = t2663 * t2660;
        const Scalar t2712 = t2664 * t2659;
        const Scalar t2720 = t2712 / 0.8e1 + t2701 * t2733;
        const Scalar t2719 = t2673 / 0.2e1;
        const Scalar t2717 = 0.2e1 / 0.3e1 * t2673;
        const Scalar t2716 = 0.3e1 / 0.2e1 * t2673;
        const Scalar t2715 = 0.4e1 / 0.3e1 * t2673;
        const Scalar t35 = log(t2651);
        const Scalar t2714 = t35 * t2673;
        const Scalar t36 = t2683 * t2683;
        const Scalar t2650 = 0.1e1 / t36;
        const Scalar t2672 = l * l;
        const Scalar t2675 = t2672 * t2672;
        const Scalar t2679 = t2675 * t2675;
        const Scalar t2705 = t2679 * t2650;
        const Scalar t2704 = -0.8e1 * t2711;
        const Scalar t2702 = -0.8e1 * t2707;
        const Scalar t2700 = t2663 * t2727;
        const Scalar t2699 = t2661 * t2727;
        const Scalar t2697 = -(t2650 - 0.1e1) * t2665 / 0.8e1;
        const Scalar t2696 = t2664 * t2653 / 0.8e1;
        const Scalar t2694 = 0.6e1 * t2654 * t2660 + (0.48e2 * t2661 * t2663 + 0.16e2 * t2657) * t2657;
        const Scalar t2674 = l * t2672;
        const Scalar t2677 = t2674 * t2674;
        const Scalar t2669 = l * t2675;
        const Scalar t2667 = l * t2677;
        const Scalar t2658 = t2660 * t2660;
        const Scalar t2652 = t2654 * t2654;
        const Scalar t2649 = t2643 * t2648;
        const Scalar t2647 = t2643 * t2646;
        const Scalar t2645 = 0.1e1 / t2682;
        const Scalar t2644 = 0.1e1 / t2681;
        const Scalar t2638 = -(t2649 - 0.1e1) * t2673 / 0.7e1 - l * t2650;
        const Scalar t2635 = 0.2e1 / 0.7e1 * (-(t2648 - 0.1e1) * t2673 / 0.6e1 - l * t2649) * t2673 - t2672 * t2650;
        const Scalar t2632 = 0.3e1 / 0.7e1 * ((-(t2647 - 0.1e1) * t2673 / 0.5e1 - l * t2648) * t2673 / 0.3e1 - t2672 * t2649) * t2673 - t2674 * t2650;
        const Scalar t2631 = 0.4e1 / 0.7e1 * ((0.2e1 / 0.5e1 * (-(t2646 - 0.1e1) * t2673 / 0.4e1 - l * t2647) * t2673 - t2672 * t2648) * t2719 - t2674 * t2649) * t2673 - t2675 * t2650;
        const Scalar t2630 = 0.5e1 / 0.7e1 * ((0.3e1 / 0.5e1 * ((-(t2645 - 0.1e1) * t2673 / 0.3e1 - l * t2646) * t2719 - t2672 * t2647) * t2673 - t2674 * t2648) * t2717 - t2675 * t2649) * t2673 - t2669 * t2650;
        const Scalar t2629 = 0.6e1 / 0.7e1 * (0.5e1 / 0.6e1 * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * ((-(t2644 - 0.1e1) * t2673 / 0.2e1 - l * t2645) * t2717 - t2672 * t2646) * t2673 - t2674 * t2647) * t2673 - t2675 * t2648) * t2673 - t2669 * t2649) * t2673 - t2677 * t2650;
        const Scalar t2628 = ((((((-(t2643 - 0.1e1) * t2673 - l * t2644) * t2673 - t2672 * t2645) * t2673 - t2674 * t2646) * t2673 - t2675 * t2647) * t2673 - t2669 * t2648) * t2673 - t2677 * t2649) * t2673 - t2667 * t2650;
        const Scalar t2627 = 0.8e1 / 0.7e1 * (0.7e1 / 0.6e1 * (0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * ((((t2714 - l * t2643) * t2728 - t2672 * t2644) * t2716 - t2674 * t2645) * t2715 - t2675 * t2646) * t2673 - t2669 * t2647) * t2673 - t2677 * t2648) * t2673 - t2667 * t2649) * t2673 - t2705;
        res[0] = t2658 * t2673 * t2697 + ((t2664 * t2658 + t2659 * t2702) * t2638 + (t2709 * t2735 + (t2704 + 0.4e1 * t2708) * t2659) * t2635 + (0.4e1 * t2661 * t2712 + t2665 * t2700 + (-t2661 * t2707 + t2713) * t2735) * t2632 + ((t2700 + t2661 * t2660 * t2726) * t2664 + t2694 * t2665) * t2631 + ((t2654 * t2663 * t2726 + t2699) * t2665 + t2694 * t2664) * t2630 + (t2664 * t2699 + 0.4e1 * t2653 * t2706 + t2695 * t2736) * t2629 + (t2713 * t2736 + (0.4e1 * t2710 + t2702) * t2653) * t2628 + (t2653 * t2704 + t2665 * t2652) * t2627 + t2664 * t2652 * (0.9e1 / 0.7e1 * ((0.7e1 / 0.5e1 * ((0.5e1 / 0.3e1 * ((0.3e1 * ((l - t2714) * t2728 - t2672 * t2643) * t2673 - t2674 * t2644) * t2728 - t2675 * t2645) * t2673 - t2669 * t2646) * t2716 - t2677 * t2647) * t2673 - t2667 * t2648) * t2715 - t2679 * t2649) * t2673 - l * t2705)) * t2673 / 0.8e1;
        res[1] = (t2659 * t2697 + t2638 * t2720 + t2635 * t2722 + t2632 * t2725 + t2631 * t2724 + t2630 * t2723 + t2629 * t2721 + t2628 * t2696) * t2673;
        res[2] = (t2659 * t2638 * t2732 + t2635 * t2720 + t2632 * t2722 + t2631 * t2725 + t2630 * t2724 + t2629 * t2723 + t2628 * t2721 + t2627 * t2696) * t2673;

    }
    
    template<>
    inline Scalar HomotheticCompactPolynomial_density_approx_segment_F<8> (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g)
    {
        const Scalar t2783 = q * d;
        const Scalar t2749 = t2783 + 0.1e1;
        const Scalar t2785 = 0.1e1 / t2749;
        const Scalar t2784 = -0.9e1 * d;
        const Scalar t2 = q * q;
        const Scalar t2756 = 0.1e1 / t2;
        const Scalar t3 = t2749 * t2749;
        const Scalar t4 = t3 * t3;
        const Scalar t5 = t4 * t4;
        const Scalar t2748 = t2785 / t5;
        const Scalar t2751 = w[2];
        const Scalar t2752 = w[1];
        const Scalar t2753 = w[0];
        const Scalar t2758 = l * l;
        const Scalar t2744 = t2751 * t2758 - 0.2e1 * t2752 * l + t2753;
        const Scalar t11 = t2752 * t2752;
        const Scalar t2745 = t2751 * t2753 - t11;
        const Scalar t2746 = t2751 * l - t2752;
        const Scalar t2762 = t2744 * t2744;
        const Scalar t2764 = t2762 * t2762;
        const Scalar t2771 = t2753 * t2753;
        const Scalar t2773 = t2771 * t2771;
        const Scalar t2777 = t2752 * t2771;
        const Scalar t2778 = t2746 * t2762;
        const Scalar t2750 = 0.1e1 / t2751;
        const Scalar t2779 = t2745 * t2750;
        const Scalar t2782 = 0.8e1 / 0.63e2 * (0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t2745 * l + t2746 * t2744 + t2752 * t2753) * t2779 + t2778 + t2777) * t2779 + t2744 * t2778 + t2753 * t2777) * t2779 + t2746 * t2764 / 0.9e1 + t2752 * t2773 / 0.9e1;
        const Scalar t2747 = t2785 * t2748;
        const Scalar t2781 = (0.2e1 - 0.200e1 * t2748 - 0.9e1 * (0.1e1 + t2747) * t2783) / q * t2756;
        const Scalar t2780 = (0.3e1 * t2748 - 0.300e1 + 0.9e1 * (0.2e1 + t2747) * t2783) * t2756;
        const Scalar t2743 = t2744 * t2764;
        const Scalar t2776 = t2758 * t2743;
        const Scalar t2775 = t2750 * t2782;
        const Scalar t2755 = g[0];
        const Scalar t2754 = g[1];
        const Scalar t2739 = t2752 * t2775 + t2743 / 0.10e2 - t2753 * t2773 / 0.10e2;
        const Scalar t2738 = -l * t2743 + (-0.12e2 * t2752 * t2739 + t2753 * t2782) * t2750;
        const Scalar t2737 = -t2776 + (0.14e2 / 0.11e2 * t2752 * t2738 + 0.2e1 * t2753 * t2739) * t2750;
        return  t2755 * t2775 + (t2755 * t2784 + t2754) * t2739 * t2750 - (t2755 * t2780 + t2754 * t2784) * t2738 * t2750 / 0.11e2 - (t2755 * t2781 + t2754 * t2780) * t2737 * t2750 / 0.12e2 - t2754 * (-l * t2776 + (0.4e1 / 0.3e1 * t2752 * t2737 - 0.3e1 / 0.11e2 * t2753 * t2738) * t2750) * t2750 * t2781 / 0.13e2;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_approx_segment_GradF<8> (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[2])
    {
        const Scalar t2838 = q * d;
        const Scalar t2802 = t2838 + 0.1e1;
        const Scalar t2840 = 0.1e1 / t2802;
        const Scalar t2839 = -0.9e1 * d;
        const Scalar t2 = q * q;
        const Scalar t2809 = 0.1e1 / t2;
        const Scalar t3 = t2802 * t2802;
        const Scalar t4 = t3 * t3;
        const Scalar t5 = t4 * t4;
        const Scalar t2801 = t2840 / t5;
        const Scalar t2804 = w[2];
        const Scalar t2805 = w[1];
        const Scalar t2806 = w[0];
        const Scalar t2811 = l * l;
        const Scalar t2796 = t2804 * t2811 - 0.2e1 * t2805 * l + t2806;
        const Scalar t2816 = t2796 * t2796;
        const Scalar t2795 = t2816 * t2816;
        const Scalar t2803 = 0.1e1 / t2804;
        const Scalar t2824 = t2806 * t2806;
        const Scalar t11 = t2805 * t2805;
        const Scalar t2797 = t2804 * t2806 - t11;
        const Scalar t2799 = t2804 * l - t2805;
        const Scalar t2831 = t2805 * t2824;
        const Scalar t2832 = t2799 * t2816;
        const Scalar t2833 = t2797 * t2803;
        const Scalar t2836 = 0.6e1 / 0.35e2 * (0.4e1 / 0.3e1 * (0.2e1 * t2797 * l + t2799 * t2796 + t2805 * t2806) * t2833 + t2832 + t2831) * t2833 + t2796 * t2832 / 0.7e1 + t2806 * t2831 / 0.7e1;
        const Scalar t30 = t2824 * t2824;
        const Scalar t2789 = t2805 * t2803 * t2836 + t2795 / 0.8e1 - t30 / 0.8e1;
        const Scalar t2788 = -l * t2795 + (-0.10e2 * t2805 * t2789 + t2806 * t2836) * t2803;
        const Scalar t2837 = -t2788 / 0.9e1;
        const Scalar t2800 = t2840 * t2801;
        const Scalar t2835 = (0.2e1 - 0.200e1 * t2801 - 0.9e1 * (0.1e1 + t2800) * t2838) / q * t2809;
        const Scalar t2834 = (0.3e1 * t2801 - 0.300e1 + 0.9e1 * (0.2e1 + t2800) * t2838) * t2809;
        const Scalar t2830 = t2811 * t2795;
        const Scalar t2787 = -t2830 + (0.4e1 / 0.3e1 * t2805 * t2788 + 0.2e1 * t2806 * t2789) * t2803;
        const Scalar t2786 = -l * t2830 + (0.7e1 / 0.5e1 * t2805 * t2787 - t2806 * t2788 / 0.3e1) * t2803;
        const Scalar t2829 = t2786 / 0.11e2;
        const Scalar t2828 = t2787 / 0.10e2;
        const Scalar t2807 = g[1];
        const Scalar t2827 = t2807 * t2835;
        const Scalar t2808 = g[0];
        const Scalar t2798 = t2808 * t2839 + t2807;
        const Scalar t2792 = t2808 * t2834 + t2807 * t2839;
        const Scalar t2791 = t2808 * t2835 + t2807 * t2834;
        res[0] = (t2808 * t2836 + t2798 * t2789 + t2792 * t2837 - t2791 * t2828 - t2827 * t2829) * t2803;
        const Scalar t79 = t2811 * t2811;
        res[1] = (t2808 * t2789 + t2798 * t2837 - t2792 * t2828 - t2791 * t2829 + (t79 * t2795 - (0.16e2 / 0.11e2 * t2805 * t2786 - 0.2e1 / 0.5e1 * t2806 * t2787) * t2803) * t2827 / 0.12e2) * t2803;

    }
    
    
    template<>
    inline void HomotheticCompactPolynomial_density_approx_segment_FGradF<8> (Scalar l, Scalar d, Scalar q, const Scalar w[3], const std::array<Scalar,2>& g, Scalar res[3])
    {
        const Scalar t2907 = q * d;
        const Scalar t2862 = t2907 + 0.1e1;
        const Scalar t2909 = 0.1e1 / t2862;
        const Scalar t2908 = -0.9e1 * d;
        const Scalar t2 = q * q;
        const Scalar t2870 = 0.1e1 / t2;
        const Scalar t3 = t2862 * t2862;
        const Scalar t4 = t3 * t3;
        const Scalar t5 = t4 * t4;
        const Scalar t2861 = t2909 / t5;
        const Scalar t2867 = w[0];
        const Scalar t2906 = 0.2e1 * t2867;
        const Scalar t2865 = w[2];
        const Scalar t2863 = 0.1e1 / t2865;
        const Scalar t2866 = w[1];
        const Scalar t2873 = l * l;
        const Scalar t2856 = t2865 * t2873 - 0.2e1 * t2866 * l + t2867;
        const Scalar t2878 = t2856 * t2856;
        const Scalar t2880 = t2878 * t2878;
        const Scalar t2887 = t2867 * t2867;
        const Scalar t2889 = t2887 * t2887;
        const Scalar t11 = t2866 * t2866;
        const Scalar t2857 = t2865 * t2867 - t11;
        const Scalar t2859 = t2865 * l - t2866;
        const Scalar t2896 = t2866 * t2887;
        const Scalar t2897 = t2859 * t2878;
        const Scalar t2899 = t2857 * t2863;
        const Scalar t2849 = 0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t2857 * l + t2859 * t2856 + t2866 * t2867) * t2899 + t2897 + t2896) * t2899 + t2856 * t2897 + t2867 * t2896;
        const Scalar t2903 = t2849 / 0.7e1;
        const Scalar t2848 = t2866 * t2863 * t2903 + t2880 / 0.8e1 - t2889 / 0.8e1;
        const Scalar t2845 = -l * t2880 + (-0.10e2 * t2866 * t2848 + t2867 * t2903) * t2863;
        const Scalar t2905 = -t2845 / 0.9e1;
        const Scalar t2904 = 0.8e1 / 0.63e2 * t2849 * t2899 + t2859 * t2880 / 0.9e1 + t2866 * t2889 / 0.9e1;
        const Scalar t2902 = 0.4e1 / 0.3e1 * t2866;
        const Scalar t2860 = t2909 * t2861;
        const Scalar t2901 = (0.2e1 - 0.200e1 * t2861 - 0.9e1 * (0.1e1 + t2860) * t2907) / q * t2870;
        const Scalar t2900 = (0.3e1 * t2861 - 0.300e1 + 0.9e1 * (0.2e1 + t2860) * t2907) * t2870;
        const Scalar t2868 = g[1];
        const Scalar t2869 = g[0];
        const Scalar t2858 = t2869 * t2908 + t2868;
        const Scalar t2898 = t2858 * t2863;
        const Scalar t2843 = -t2873 * t2880 + (t2845 * t2902 + t2848 * t2906) * t2863;
        const Scalar t2895 = t2843 / 0.10e2;
        const Scalar t2894 = t2863 / 0.11e2;
        const Scalar t2893 = t2868 * t2901;
        const Scalar t2892 = t2863 * t2904;
        const Scalar t2891 = t2863 * t2893;
        const Scalar t2872 = l * t2873;
        const Scalar t2854 = t2856 * t2880;
        const Scalar t2851 = t2869 * t2900 + t2868 * t2908;
        const Scalar t2850 = t2869 * t2901 + t2868 * t2900;
        const Scalar t2846 = t2866 * t2892 + t2854 / 0.10e2 - t2867 * t2889 / 0.10e2;
        const Scalar t2844 = -l * t2854 + (-0.12e2 * t2866 * t2846 + t2867 * t2904) * t2863;
        const Scalar t2842 = -t2873 * t2854 + (0.14e2 / 0.11e2 * t2866 * t2844 + t2846 * t2906) * t2863;
        const Scalar t2841 = -t2872 * t2880 + (0.7e1 / 0.5e1 * t2866 * t2843 - t2867 * t2845 / 0.3e1) * t2863;
        res[0] = t2869 * t2892 + t2846 * t2898 - t2851 * t2844 * t2894 - t2850 * t2842 * t2863 / 0.12e2 - (-t2872 * t2854 + (t2842 * t2902 - 0.3e1 / 0.11e2 * t2867 * t2844) * t2863) * t2891 / 0.13e2;
        res[1] = (t2869 * t2903 + t2858 * t2848 + t2851 * t2905 - t2850 * t2895 - t2841 * t2893 / 0.11e2) * t2863;
        const Scalar t117 = t2873 * t2873;
        res[2] = t2869 * t2848 * t2863 + t2898 * t2905 - t2851 * t2863 * t2895 - t2850 * t2841 * t2894 - (-t117 * t2880 + (0.16e2 / 0.11e2 * t2866 * t2841 - 0.2e1 / 0.5e1 * t2867 * t2843) * t2863) * t2891 / 0.12e2;

    }
    
    template<>
    inline void HomotheticCompactPolynomial_segment_ScalisF_HornusGradF<8> (Scalar l, Scalar d, const Scalar w[3], Scalar res[3])
    {
        const Scalar t2929 = d * l + 0.1e1;
        const Scalar t2921 = 0.1e1 / t2929;
        const Scalar t2952 = t2929 * t2929;
        const Scalar t2954 = t2952 * t2952;
        const Scalar t2924 = 0.1e1 / t2954;
        const Scalar t2953 = t2929 * t2952;
        const Scalar t2 = t2953 * t2953;
        const Scalar t2926 = 0.1e1 / t2;
        const Scalar t2937 = w[1];
        const Scalar t2933 = t2937 * t2937;
        const Scalar t2932 = t2937 * t2933;
        const Scalar t2980 = -0.32e2 * t2932;
        const Scalar t2979 = 0.24e2 * t2933;
        const Scalar t2936 = w[2];
        const Scalar t2931 = t2936 * t2936;
        const Scalar t2938 = w[0];
        const Scalar t2968 = t2936 * t2933;
        const Scalar t2978 = 0.3e1 / 0.7e1 * t2931 * t2938 + 0.12e2 / 0.7e1 * t2968;
        const Scalar t2935 = t2938 * t2938;
        const Scalar t2967 = t2936 * t2935;
        const Scalar t2977 = 0.12e2 / 0.7e1 * t2933 * t2938 + 0.3e1 / 0.7e1 * t2967;
        const Scalar t2966 = t2937 * t2938;
        const Scalar t2976 = -0.8e1 / 0.7e1 * t2932 - 0.12e2 / 0.7e1 * t2936 * t2966;
        const Scalar t2927 = t2921 * t2926;
        const Scalar t2975 = -t2927 / 0.7e1 + 0.1e1 / 0.7e1;
        const Scalar t2930 = t2936 * t2931;
        const Scalar t2974 = t2930 / 0.7e1;
        const Scalar t2973 = -0.6e1 / 0.7e1 * t2931;
        const Scalar t2972 = -0.6e1 / 0.7e1 * t2935;
        const Scalar t2945 = 0.1e1 / d;
        const Scalar t2971 = t2945 / 0.2e1;
        const Scalar t2969 = 0.2e1 / 0.3e1 * t2945;
        const Scalar t2944 = l * l;
        const Scalar t2946 = l * t2944;
        const Scalar t2949 = t2946 * t2946;
        const Scalar t2947 = t2944 * t2944;
        const Scalar t2941 = l * t2947;
        const Scalar t2939 = l * t2949;
        const Scalar t2934 = t2938 * t2935;
        const Scalar t16 = t2954 * t2954;
        const Scalar t2928 = 0.1e1 / t16;
        const Scalar t2925 = t2921 * t2924;
        const Scalar t2923 = 0.1e1 / t2953;
        const Scalar t2922 = 0.1e1 / t2952;
        const Scalar t2916 = -(t2926 - 0.1e1) * t2945 / 0.6e1 - l * t2927;
        const Scalar t2915 = (-(t2925 - 0.1e1) * t2945 / 0.5e1 - l * t2926) * t2945 / 0.3e1 - t2944 * t2927;
        const Scalar t2914 = (0.2e1 / 0.5e1 * (-(t2924 - 0.1e1) * t2945 / 0.4e1 - l * t2925) * t2945 - t2944 * t2926) * t2971 - t2946 * t2927;
        const Scalar t2913 = (0.3e1 / 0.5e1 * ((-(t2923 - 0.1e1) * t2945 / 0.3e1 - l * t2924) * t2971 - t2944 * t2925) * t2945 - t2946 * t2926) * t2969 - t2947 * t2927;
        const Scalar t2912 = 0.5e1 / 0.6e1 * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * ((-(t2922 - 0.1e1) * t2945 / 0.2e1 - l * t2923) * t2969 - t2944 * t2924) * t2945 - t2946 * t2925) * t2945 - t2947 * t2926) * t2945 - t2941 * t2927;
        const Scalar t2911 = (((((-(t2921 - 0.1e1) * t2945 - l * t2922) * t2945 - t2944 * t2923) * t2945 - t2946 * t2924) * t2945 - t2947 * t2925) * t2945 - t2941 * t2926) * t2945 - t2949 * t2927;
        const Scalar t91 = log(t2929);
        const Scalar t2910 = 0.7e1 / 0.6e1 * (0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * (0.2e1 * (t91 * t2945 - l * t2921) * t2945 - t2944 * t2922) * t2945 - t2946 * t2923) * t2945 - t2947 * t2924) * t2945 - t2941 * t2925) * t2945 - t2949 * t2926) * t2945 - t2939 * t2927;
        const Scalar t118 = t2935 * t2935;
        const Scalar t143 = t2933 * t2933;
        const Scalar t182 = t2931 * t2931;
        const Scalar t185 = t2947 * t2947;
        res[0] = -t118 * (t2928 - 0.1e1) * t2945 / 0.8e1 + (-t2934 * (t2945 * t2975 - l * t2928) - t2930 * (t2911 * t2945 - t2939 * t2928)) * t2937 * t2945 + ((t2935 * t2979 + 0.4e1 * t2936 * t2934) * (0.2e1 / 0.7e1 * t2916 * t2945 - t2944 * t2928) + (0.16e2 * t143 + 0.6e1 * t2931 * t2935 + 0.48e2 * t2938 * t2968) * (0.4e1 / 0.7e1 * t2914 * t2945 - t2947 * t2928) + (t2936 * t2980 - 0.24e2 * t2931 * t2966) * (0.5e1 / 0.7e1 * t2913 * t2945 - t2941 * t2928) + (0.4e1 * t2930 * t2938 + t2931 * t2979) * (0.6e1 / 0.7e1 * t2912 * t2945 - t2949 * t2928) + (-0.24e2 * t2937 * t2967 + t2938 * t2980) * (0.3e1 / 0.7e1 * t2915 * t2945 - t2946 * t2928) + t182 * (0.8e1 / 0.7e1 * t2910 * t2945 - t185 * t2928)) * t2945 / 0.8e1;
        res[1] = (t2934 * t2975 + t2915 * t2977 + t2914 * t2976 + t2913 * t2978 + t2911 * t2974 + (t2916 * t2972 + t2912 * t2973) * t2937) * t2945;
        res[2] = (t2934 * t2916 / 0.7e1 + t2914 * t2977 + t2913 * t2976 + t2912 * t2978 + t2910 * t2974 + (t2915 * t2972 + t2911 * t2973) * t2937) * t2945;

    }
    
    template<>
    inline void HomotheticCompactPolynomial_approx_segment_ScalisF_HornusGradF<8> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3])
    {
        const Scalar t3036 = q * d;
        const Scalar t2997 = t3036 + 0.1e1;
        const Scalar t3037 = 0.1e1 / t2997;
        const Scalar t1 = q * q;
        const Scalar t3003 = 0.1e1 / t1;
        const Scalar t2 = t2997 * t2997;
        const Scalar t3 = t2 * t2;
        const Scalar t4 = t3 * t3;
        const Scalar t2996 = t3037 / t4;
        const Scalar t3002 = w[0];
        const Scalar t3035 = 0.2e1 * t3002;
        const Scalar t3000 = w[2];
        const Scalar t2998 = 0.1e1 / t3000;
        const Scalar t3034 = d * t2998;
        const Scalar t3001 = w[1];
        const Scalar t3005 = l * l;
        const Scalar t2992 = t3000 * t3005 - 0.2e1 * t3001 * l + t3002;
        const Scalar t10 = t3001 * t3001;
        const Scalar t2993 = t3000 * t3002 - t10;
        const Scalar t2994 = t3000 * l - t3001;
        const Scalar t3018 = t3002 * t3002;
        const Scalar t3027 = t3001 * t3018;
        const Scalar t3009 = t2992 * t2992;
        const Scalar t3028 = t2994 * t3009;
        const Scalar t3029 = t2993 * t2998;
        const Scalar t2987 = 0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t2993 * l + t2994 * t2992 + t3001 * t3002) * t3029 + t3028 + t3027) * t3029 + t2992 * t3028 + t3002 * t3027;
        const Scalar t3011 = t3009 * t3009;
        const Scalar t3020 = t3018 * t3018;
        const Scalar t3033 = 0.8e1 / 0.63e2 * t2987 * t3029 + t2994 * t3011 / 0.9e1 + t3001 * t3020 / 0.9e1;
        const Scalar t3032 = t2987 / 0.7e1;
        const Scalar t2995 = t3037 * t2996;
        const Scalar t3031 = (0.3e1 * t2996 - 0.300e1 + 0.9e1 * (0.2e1 + t2995) * t3036) * t3003;
        const Scalar t3030 = (0.2e1 - 0.200e1 * t2996 - 0.9e1 * (0.1e1 + t2995) * t3036) / q * t3003;
        const Scalar t3026 = t3005 * t3011;
        const Scalar t2986 = t3001 * t2998 * t3032 + t3011 / 0.8e1 - t3020 / 0.8e1;
        const Scalar t2983 = -l * t3011 + (-0.10e2 * t3001 * t2986 + t3002 * t3032) * t2998;
        const Scalar t2981 = -t3026 + (0.4e1 / 0.3e1 * t3001 * t2983 + t2986 * t3035) * t2998;
        const Scalar t3025 = t2981 / 0.10e2;
        const Scalar t3024 = t2998 * t3031;
        const Scalar t3023 = t2998 * t3030;
        const Scalar t3022 = t2998 * t3033;
        const Scalar t2990 = t2992 * t3011;
        const Scalar t2984 = t3001 * t3022 + t2990 / 0.10e2 - t3002 * t3020 / 0.10e2;
        const Scalar t2982 = -l * t2990 + (-0.12e2 * t3001 * t2984 + t3002 * t3033) * t2998;
        res[0] = t3022 - 0.9e1 * t2984 * t3034 - t2982 * t3024 / 0.11e2 - (-t3005 * t2990 + (0.14e2 / 0.11e2 * t3001 * t2982 + t2984 * t3035) * t2998) * t3023 / 0.12e2;
        res[1] = (t3032 - 0.9e1 * d * t2986 - t2983 * t3031 / 0.9e1 - t3025 * t3030) * t2998;
        res[2] = t2986 * t2998 + t2983 * t3034 - t3024 * t3025 - (-l * t3026 + (0.7e1 / 0.5e1 * t3001 * t2981 - t3002 * t2983 / 0.3e1) * t2998) * t3023 / 0.11e2;

    }
    
    
    



} // Close namespace HomotheticCompactPolynomialOptim
} // Close namespace convol
} // Close namespace expressive

#endif // CONVOL_HOMOTHETIC_COMPACT_POLYNOMIAL_OPTIMIZED_FUNCTIONS_H_

