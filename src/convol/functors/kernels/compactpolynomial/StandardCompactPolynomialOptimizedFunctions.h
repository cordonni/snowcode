/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: StandardCompactPolynomialOptimizedFunctions.h
  
   Language: C++
  
   License: Convol Licence
  
   \author: Cedric Zanni 
   E-Mail: cedric.zanni@inria.fr
  
   Description: Header files defining optimized function for Standard CompactPolynomialX kernels.
 
   Platform Dependencies: None 
*/  

#pragma once
#ifndef CONVOL_STANDARD_COMPACT_POLYNOMIAL_OPTIMIZED_FUNCTIONS_H_
#define CONVOL_STANDARD_COMPACT_POLYNOMIAL_OPTIMIZED_FUNCTIONS_H_

// core dependencies
#include <core/CoreRequired.h>

// std dependencies
#include<vector>
#include<array>

#include<math.h>

namespace expressive {

namespace convol {

namespace StandardCompactPolynomialOptim {

////////////////////////////
/// Function declaration ///
////////////////////////////

template<int I, int K>
inline Scalar StandardCompactPolynomial_segment_F (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w);
template<int I, int K>
inline void StandardCompactPolynomial_segment_GradF (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2]);
template<int I, int K>
inline void StandardCompactPolynomial_segment_FGradF (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3]);

///////////////////////////////
/// Template specialization ///
///////////////////////////////





    /////////////////////////////////
    // Code for kernel of degree 4 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCompactPolynomial_segment_F<4,1> (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t4998 = -0.2e1 * b;
        const Scalar t4997 = a * l1;
        const Scalar t4996 = a * l2;
        const Scalar t4985 = c + (t4998 + t4996) * l2;
        const Scalar t4986 = c + (t4998 + t4997) * l1;
        const Scalar t7 = b * b;
        const Scalar t4987 = a * c - t7;
        const Scalar t4988 = t4996 - b;
        const Scalar t4989 = t4997 - b;
        const Scalar t4990 = 0.1e1 / a;
        const Scalar t4991 = t4985 * t4985;
        const Scalar t4993 = t4986 * t4986;
        const Scalar t4995 = (0.4e1 / 0.3e1 * t4987 * (0.2e1 * t4987 * (l2 - l1) + t4988 * t4985 - t4989 * t4986) * t4990 + t4988 * t4991 - t4989 * t4993) * t4990 / 0.5e1;
        return  w[0] * t4995 + w[1] * (b * t4995 + t4985 * t4991 / 0.6e1 - t4986 * t4993 / 0.6e1) * t4990;

    }
    
    
    template<>
    inline void StandardCompactPolynomial_segment_GradF<4,1> (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t5012 = -0.2e1 * b;
        const Scalar t5011 = a * l1;
        const Scalar t5010 = a * l2;
        const Scalar t5003 = c + (t5012 + t5010) * l2;
        const Scalar t5004 = c + (t5012 + t5011) * l1;
        const Scalar t7 = b * b;
        const Scalar t5009 = 0.2e1 / 0.3e1 * (a * c - t7) * (l2 - l1) + (t5010 - b) * t5003 / 0.3e1 - (t5011 - b) * t5004 / 0.3e1;
        const Scalar t5007 = 0.1e1 / a;
        const Scalar t5006 = w[0];
        const Scalar t5005 = w[1];
        const Scalar t5002 = t5004 * t5004;
        const Scalar t5001 = t5003 * t5003;
        const Scalar t4999 = b * t5007 * t5009 + t5001 / 0.4e1 - t5002 / 0.4e1;
        res[0] = (t5006 * t5009 + t5005 * t4999) * t5007;
        res[1] = (t5006 * t4999 - (-l2 * t5001 + l1 * t5002 + (-0.6e1 * b * t4999 + c * t5009) * t5007) * t5005 / 0.5e1) * t5007;

    }
    
    
    template<>
    inline void StandardCompactPolynomial_segment_FGradF<4,1> (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t5033 = -0.2e1 * b;
        const Scalar t5032 = a * l1;
        const Scalar t5031 = a * l2;
        const Scalar t5018 = c + (t5033 + t5031) * l2;
        const Scalar t5019 = c + (t5033 + t5032) * l1;
        const Scalar t7 = b * b;
        const Scalar t5020 = a * c - t7;
        const Scalar t5021 = t5031 - b;
        const Scalar t5022 = t5032 - b;
        const Scalar t5015 = 0.2e1 * t5020 * (l2 - l1) + t5021 * t5018 - t5022 * t5019;
        const Scalar t5030 = t5015 / 0.3e1;
        const Scalar t5025 = 0.1e1 / a;
        const Scalar t5029 = t5015 * t5025;
        const Scalar t5023 = w[1];
        const Scalar t5028 = t5023 * t5025;
        const Scalar t5024 = w[0];
        const Scalar t5017 = t5019 * t5019;
        const Scalar t5016 = t5018 * t5018;
        const Scalar t5014 = b * t5029 / 0.3e1 + t5016 / 0.4e1 - t5017 / 0.4e1;
        res[0] = (t5023 * (t5018 * t5016 / 0.6e1 - t5019 * t5017 / 0.6e1) + (t5024 / 0.5e1 + b * t5028 / 0.5e1) * (0.4e1 / 0.3e1 * t5020 * t5029 + t5021 * t5016 - t5022 * t5017)) * t5025;
        res[1] = (t5024 * t5030 + t5023 * t5014) * t5025;
        res[2] = (t5024 * t5014 - t5023 * (-l2 * t5016 + l1 * t5017) / 0.5e1 - (-0.6e1 * b * t5014 + c * t5030) * t5028 / 0.5e1) * t5025;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 6 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCompactPolynomial_segment_F<6,1> (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t5053 = -0.2e1 * b;
        const Scalar t5052 = a * l1;
        const Scalar t5051 = a * l2;
        const Scalar t3 = b * b;
        const Scalar t5037 = a * c - t3;
        const Scalar t5040 = 0.1e1 / a;
        const Scalar t5050 = t5037 * t5040;
        const Scalar t5038 = t5051 - b;
        const Scalar t5035 = c + (t5053 + t5051) * l2;
        const Scalar t5041 = t5035 * t5035;
        const Scalar t5049 = t5038 * t5041;
        const Scalar t5039 = t5052 - b;
        const Scalar t5036 = c + (t5053 + t5052) * l1;
        const Scalar t5044 = t5036 * t5036;
        const Scalar t5048 = t5039 * t5044;
        const Scalar t5047 = (0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t5037 * (l2 - l1) + t5038 * t5035 - t5039 * t5036) * t5050 + t5049 - t5048) * t5050 + t5035 * t5049 - t5036 * t5048) * t5040 / 0.7e1;
        const Scalar t27 = t5041 * t5041;
        const Scalar t29 = t5044 * t5044;
        return  w[0] * t5047 + w[1] * (b * t5047 + t27 / 0.8e1 - t29 / 0.8e1) * t5040;

    }
    
    
    template<>
    inline void StandardCompactPolynomial_segment_GradF<6,1> (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t5074 = -0.2e1 * b;
        const Scalar t5073 = a * l1;
        const Scalar t5072 = a * l2;
        const Scalar t5058 = c + (t5074 + t5072) * l2;
        const Scalar t5059 = c + (t5074 + t5073) * l1;
        const Scalar t7 = b * b;
        const Scalar t5060 = a * c - t7;
        const Scalar t5061 = t5072 - b;
        const Scalar t5062 = t5073 - b;
        const Scalar t5065 = 0.1e1 / a;
        const Scalar t5066 = t5058 * t5058;
        const Scalar t5068 = t5059 * t5059;
        const Scalar t5071 = 0.4e1 / 0.15e2 * t5060 * (0.2e1 * t5060 * (l2 - l1) + t5061 * t5058 - t5062 * t5059) * t5065 + t5061 * t5066 / 0.5e1 - t5062 * t5068 / 0.5e1;
        const Scalar t5064 = w[0];
        const Scalar t5063 = w[1];
        const Scalar t5057 = t5059 * t5068;
        const Scalar t5056 = t5058 * t5066;
        const Scalar t5054 = b * t5065 * t5071 + t5056 / 0.6e1 - t5057 / 0.6e1;
        res[0] = (t5064 * t5071 + t5063 * t5054) * t5065;
        res[1] = (t5064 * t5054 - (-l2 * t5056 + l1 * t5057 + (-0.8e1 * b * t5054 + c * t5071) * t5065) * t5063 / 0.7e1) * t5065;

    }
    
    
    template<>
    inline void StandardCompactPolynomial_segment_FGradF<6,1> (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t5099 = -0.2e1 * b;
        const Scalar t5098 = a * l1;
        const Scalar t5097 = a * l2;
        const Scalar t5080 = c + (t5099 + t5097) * l2;
        const Scalar t5081 = c + (t5099 + t5098) * l1;
        const Scalar t7 = b * b;
        const Scalar t5082 = a * c - t7;
        const Scalar t5083 = t5097 - b;
        const Scalar t5084 = t5098 - b;
        const Scalar t5088 = t5080 * t5080;
        const Scalar t5091 = t5081 * t5081;
        const Scalar t5087 = 0.1e1 / a;
        const Scalar t5095 = t5082 * t5087;
        const Scalar t5077 = 0.4e1 / 0.3e1 * (0.2e1 * t5082 * (l2 - l1) + t5083 * t5080 - t5084 * t5081) * t5095 + t5083 * t5088 - t5084 * t5091;
        const Scalar t5096 = t5077 / 0.5e1;
        const Scalar t5085 = w[1];
        const Scalar t5094 = t5085 * t5087;
        const Scalar t5086 = w[0];
        const Scalar t5079 = t5081 * t5091;
        const Scalar t5078 = t5080 * t5088;
        const Scalar t5076 = b * t5087 * t5096 + t5078 / 0.6e1 - t5079 / 0.6e1;
        const Scalar t22 = t5088 * t5088;
        const Scalar t23 = t5091 * t5091;
        res[0] = (t5085 * (t22 / 0.8e1 - t23 / 0.8e1) + (t5086 / 0.7e1 + b * t5094 / 0.7e1) * (0.6e1 / 0.5e1 * t5077 * t5095 + t5083 * t5078 - t5084 * t5079)) * t5087;
        res[1] = (t5086 * t5096 + t5085 * t5076) * t5087;
        res[2] = (t5086 * t5076 - t5085 * (-l2 * t5078 + l1 * t5079) / 0.7e1 - (-0.8e1 * b * t5076 + c * t5096) * t5094 / 0.7e1) * t5087;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 8 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCompactPolynomial_segment_F<8,1> (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t5121 = -0.2e1 * b;
        const Scalar t5120 = a * l1;
        const Scalar t5119 = a * l2;
        const Scalar t3 = b * b;
        const Scalar t5103 = a * c - t3;
        const Scalar t5106 = 0.1e1 / a;
        const Scalar t5118 = t5103 * t5106;
        const Scalar t5104 = t5119 - b;
        const Scalar t5101 = c + (t5121 + t5119) * l2;
        const Scalar t5107 = t5101 * t5101;
        const Scalar t5117 = t5104 * t5107;
        const Scalar t5105 = t5120 - b;
        const Scalar t5102 = c + (t5121 + t5120) * l1;
        const Scalar t5111 = t5102 * t5102;
        const Scalar t5116 = t5105 * t5111;
        const Scalar t5109 = t5107 * t5107;
        const Scalar t5113 = t5111 * t5111;
        const Scalar t5115 = (0.8e1 / 0.7e1 * (0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t5103 * (l2 - l1) + t5104 * t5101 - t5105 * t5102) * t5118 + t5117 - t5116) * t5118 + t5101 * t5117 - t5102 * t5116) * t5118 + t5104 * t5109 - t5105 * t5113) * t5106 / 0.9e1;
        return  w[0] * t5115 + w[1] * (b * t5115 + t5101 * t5109 / 0.10e2 - t5102 * t5113 / 0.10e2) * t5106;

    }
    
    
    template<>
    inline void StandardCompactPolynomial_segment_GradF<8,1> (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t5147 = -0.2e1 * b;
        const Scalar t5146 = a * l1;
        const Scalar t5145 = a * l2;
        const Scalar t5126 = c + (t5147 + t5145) * l2;
        const Scalar t5127 = c + (t5147 + t5146) * l1;
        const Scalar t7 = b * b;
        const Scalar t5128 = a * c - t7;
        const Scalar t5129 = t5145 - b;
        const Scalar t5130 = t5146 - b;
        const Scalar t5137 = t5127 * t5127;
        const Scalar t5140 = t5130 * t5137;
        const Scalar t5134 = t5126 * t5126;
        const Scalar t5141 = t5129 * t5134;
        const Scalar t5133 = 0.1e1 / a;
        const Scalar t5142 = t5128 * t5133;
        const Scalar t5144 = 0.6e1 / 0.35e2 * (0.4e1 / 0.3e1 * (0.2e1 * t5128 * (l2 - l1) + t5129 * t5126 - t5130 * t5127) * t5142 + t5141 - t5140) * t5142 + t5126 * t5141 / 0.7e1 - t5127 * t5140 / 0.7e1;
        const Scalar t5132 = w[0];
        const Scalar t5131 = w[1];
        const Scalar t5125 = t5137 * t5137;
        const Scalar t5124 = t5134 * t5134;
        const Scalar t5122 = b * t5133 * t5144 + t5124 / 0.8e1 - t5125 / 0.8e1;
        res[0] = (t5132 * t5144 + t5131 * t5122) * t5133;
        res[1] = (t5132 * t5122 - (-l2 * t5124 + l1 * t5125 + (-0.10e2 * b * t5122 + c * t5144) * t5133) * t5131 / 0.9e1) * t5133;

    }
    
    
    template<>
    inline void StandardCompactPolynomial_segment_FGradF<8,1> (const Scalar l1, const Scalar l2, const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t5176 = -0.2e1 * b;
        const Scalar t5175 = a * l1;
        const Scalar t5174 = a * l2;
        const Scalar t5153 = c + (t5176 + t5174) * l2;
        const Scalar t5154 = c + (t5176 + t5175) * l1;
        const Scalar t7 = b * b;
        const Scalar t5155 = a * c - t7;
        const Scalar t5156 = t5174 - b;
        const Scalar t5157 = t5175 - b;
        const Scalar t5165 = t5154 * t5154;
        const Scalar t5170 = t5157 * t5165;
        const Scalar t5161 = t5153 * t5153;
        const Scalar t5171 = t5156 * t5161;
        const Scalar t5160 = 0.1e1 / a;
        const Scalar t5172 = t5155 * t5160;
        const Scalar t5150 = 0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t5155 * (l2 - l1) + t5156 * t5153 - t5157 * t5154) * t5172 + t5171 - t5170) * t5172 + t5153 * t5171 - t5154 * t5170;
        const Scalar t5173 = t5150 / 0.7e1;
        const Scalar t5158 = w[1];
        const Scalar t5169 = t5158 * t5160;
        const Scalar t5167 = t5165 * t5165;
        const Scalar t5163 = t5161 * t5161;
        const Scalar t5159 = w[0];
        const Scalar t5149 = b * t5160 * t5173 + t5163 / 0.8e1 - t5167 / 0.8e1;
        res[0] = ((t5159 / 0.9e1 + b * t5169 / 0.9e1) * (0.8e1 / 0.7e1 * t5150 * t5172 + t5156 * t5163 - t5157 * t5167) + t5158 * (t5153 * t5163 - t5154 * t5167) / 0.10e2) * t5160;
        res[1] = (t5159 * t5173 + t5158 * t5149) * t5160;
        res[2] = (t5159 * t5149 - t5158 * (-l2 * t5163 + l1 * t5167) / 0.9e1 - (-0.10e2 * b * t5149 + c * t5173) * t5169 / 0.9e1) * t5160;

    }
    
    



} // Close namespace StandardCompactPolynomialOptim
} // Close namespace convol
} // Close namespace expressive

#endif // CONVOL_STANDARD_COMPACT_POLYNOMIAL_OPTIMIZED_FUNCTIONS_H_

