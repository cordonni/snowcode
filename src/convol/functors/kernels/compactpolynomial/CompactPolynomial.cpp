#include <convol/functors/kernels/compactpolynomial/CompactPolynomial.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

static core::FunctorFactory::Type<CompactPolynomial<4> > CompactPolynomial4Type;
static core::FunctorFactory::Type<CompactPolynomial<6> > CompactPolynomial6Type;
static core::FunctorFactory::Type<CompactPolynomial<8> > CompactPolynomial8Type;

} // Close namespace convol

} // Close namespace expressive
