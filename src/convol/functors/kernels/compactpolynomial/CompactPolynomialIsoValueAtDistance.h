/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: CompactPolynomialIsoValueForDistance.h
  
   Language: C++
  
   License: Convol Licence
  
   \author: Cedric Zanni 
   E-Mail: cedric.zanni@inria.fr
  
   Description: Header files defining ...
                //TODO : @todo : a completer
                   
   Platform Dependencies: None 
*/  

#pragma once
#ifndef COMPACT_POLYNOMIAL_ISO_VALUE_AT_DISTANCE_H_
#define COMPACT_POLYNOMIAL_ISO_VALUE_AT_DISTANCE_H_

// Convol dependencies
#include<core/CoreRequired.h>

#include<math.h>

namespace expressive {

namespace convol {

namespace CompactPolynomialIsoValueAtDistance
{
    inline Scalar GetIsoValueAtDistanceGeom0D (int degree, Scalar scale, Scalar dist)
    {
        assert(degree%2==0);
        
        if(dist < scale)
        {
            Scalar func_dist_scale = 1.0 - (dist*dist) / (scale*scale);
            return pow(func_dist_scale, (Scalar)(degree)/2.0);
        }
        else
        {
            return 0.0;
        }
    }
    
    inline Scalar GetIsoValueAtDistanceGeom1D (int degree, Scalar scale, Scalar dist)
    {
        assert(degree%2==0);
        
        if(dist < scale)
        {
            Scalar func_dist_scale = 1.0 - (dist*dist) / (scale*scale);
            Scalar iso_for_dist = 2.0 * scale * sqrt(func_dist_scale);
            int k = 0;
            while(k!=degree)
            {
                k += 2;
                iso_for_dist *= ((Scalar)(k)) / (1.0 + (Scalar)(k)) * func_dist_scale;
            }
            return iso_for_dist;
        }
        else
        {
            return 0.0;
        }
    }

    inline Scalar GetIsoValueAtDistanceGeom2D (int degree, Scalar scale, Scalar dist)
    {
        if(dist < scale)
        {
            Scalar i_p_2 = ((Scalar) (degree+2));
            Scalar func_dist_scale = 1.0 - (dist*dist) / (scale*scale);
            return (2.0 * M_PI / i_p_2) * scale*scale * pow(func_dist_scale, i_p_2 * 0.5);
        }
        else
        {
            return 0.0;
        }
    }

} // Close namespace CompactPolynomialIsoValueAtDistance

} // Close namespace convol

} // Close namespace expressive


#endif // COMPACT_POLYNOMIAL_ISO_VALUE_AT_DISTANCE_H_
