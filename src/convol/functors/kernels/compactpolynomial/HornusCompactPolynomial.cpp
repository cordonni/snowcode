#include <convol/functors/kernels/compactpolynomial/HornusCompactPolynomial.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

static core::FunctorFactory::Type<HornusCompactPolynomial<4> > HornusCompactPolynomial4Type;
static core::FunctorFactory::Type<HornusCompactPolynomial<6> > HornusCompactPolynomial6Type;
static core::FunctorFactory::Type<HornusCompactPolynomial<8> > HornusCompactPolynomial8Type;

} // Close namespace convol

} // Close namespace expressive
