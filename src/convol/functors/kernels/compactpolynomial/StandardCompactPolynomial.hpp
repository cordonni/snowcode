#include <convol/functors/kernels/compactpolynomial/StandardCompactPolynomialOptimizedFunctions.h>

#include <core/geometry/GeometryTools.h>

namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////


//-------------------------------------------------------------------------
// Point evaluation -------------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar StandardCompactPolynomial<DEGREE>::Eval(const WeightedPoint& p_source, const Point& point) const
{
    const Scalar dist2 = (point-p_source).squaredNorm();

    const Scalar aux = 1.0-this->inv_scale_2_*dist2;
    if(aux > 0.0)
    {
        Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2>(aux);

        return this->normalization_factor_0D_ * p_source.weight()*int_power_aux;
    }
    else
    {
        return 0.0;
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector StandardCompactPolynomial<DEGREE>::EvalGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon*/) const
{
    Vector direction = point - p_source;
    const Scalar dist2 = direction.squaredNorm();

    const Scalar aux = 1.0-this->inv_scale_2_*dist2;
    if(aux > 0.0)
    {
        Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2-1>(aux);

        return -this->normalization_factor_0D_*p_source.weight()*this->inv_scale_2_*this->degree_r()*(int_power_aux)*direction;
    }
    else
    {
        return Vector::Zero();
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
void StandardCompactPolynomial<DEGREE>::EvalValueAndGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon_grad*/,
                                                         Scalar& value_res, Vector& grad_res) const
{
    Vector direction = point - p_source;
    const Scalar dist2 = direction.squaredNorm();

    const Scalar aux = 1.0-this->inv_scale_2_*dist2;
    if(aux > 0.0)
    {
        Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2-1>(aux);

        Scalar val = this->normalization_factor_0D_ * p_source.weight() * int_power_aux;
        value_res = val*aux;
        grad_res = (-val*this->inv_scale_2_*this->degree_r())*direction;
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
// Segment evaluation -----------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar StandardCompactPolynomial<DEGREE>::Eval(const OptWeightedSegment& seg, const Point& point) const
{
//TODO:@todo : some data structure have changed and some part of the following code should be improved

    // Computation of the intersection of the segment with the sphere of radius scale_
    Scalar l1, l2;
    int n_inter = core::Geometry::Intersection(Sphere(this->scale_, point),
                                               seg.p_min(),  seg.length()*seg.increase_unit_dir(), l1, l2);

    if(n_inter == 2)
    {
        if(l1>1.0) { l1 = 1.0; }
        if(l1<0.0) { l1 = 0.0; }
        if(l2>1.0) { l2 = 1.0; }
        if(l2<0.0) { l2 = 0.0; }

        const Vector p_min_to_point( point-seg.p_min() );
        const Scalar a = - this->inv_scale_2_*seg.length()*seg.length();
        const Scalar b = - this->inv_scale_2_*seg.length()*seg.increase_unit_dir().dot(p_min_to_point);
        const Scalar c = - this->inv_scale_2_*p_min_to_point.squaredNorm() + 1.0;

        std::vector<Scalar> weight_coeff; //TODO:@todo : beurk
        weight_coeff.push_back(seg.weight_min());
        weight_coeff.push_back(seg.unit_delta_weight()*seg.length());

        return this->normalization_factor_1D_ * seg.length()*StandardCompactPolynomialOptim::StandardCompactPolynomial_segment_F<DEGREE,1>(l1,l2,a,b,c,weight_coeff);

//        Scalar delta_w = seg.length()*seg.unit_delta_weight();
//        return this->normalization_factor_1D_ * seg.length()*StandardCompactPolynomialOptim::StandardCompactPolynomial_segment_F<DEGREE,1>(l1,l2,a,b,c,seg.weight_min(),delta_w);
    }
    else
    {
        return 0.0;
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector StandardCompactPolynomial<DEGREE>::EvalGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const
{
//TODO:@todo : some data structure have changed and some part of the following code should be improved

    // Computation of the intersection of the segment with the sphere of radius scale_
    Scalar l1, l2;
    int n_inter = core::Geometry::Intersection(Sphere(this->scale_, point),
                                               seg.p_min(), seg.length()*seg.increase_unit_dir(), l1, l2);

    if(n_inter == 2)
    {
        if(l1>1.0) { l1 = 1.0; }
        if(l1<0.0) { l1 = 0.0; }
        if(l2>1.0) { l2 = 1.0; }
        if(l2<0.0) { l2 = 0.0; }

        const Vector p_min_to_point( point-seg.p_min() );
        const Scalar a = - this->inv_scale_2_*seg.length()*seg.length();
        const Scalar b = - this->inv_scale_2_*seg.length()*seg.increase_unit_dir().dot(p_min_to_point);
        const Scalar c = - this->inv_scale_2_*p_min_to_point.squaredNorm() + 1.0;

        std::vector<Scalar> weight_coeff; //TODO:@todo : beurk
        weight_coeff.push_back(seg.weight_min());
        weight_coeff.push_back(seg.unit_delta_weight()*seg.length());

        Scalar F0F1[2];
        StandardCompactPolynomialOptim::StandardCompactPolynomial_segment_GradF<DEGREE,1>(l1,l2,a,b,c,weight_coeff, F0F1);

//    Scalar delta_w = seg.length()*seg.unit_delta_weight();

//    Scalar F0F1[2];
//    StandardCompactPolynomialOptim::StandardCompactPolynomial_segment_GradF<DEGREE,1>(l1,l2,a,b,c,seg.weight_min(),delta_w, F0F1);

    const Scalar factor = this->normalization_factor_1D_ * seg.length()*this->degree_r()*this->inv_scale_2_;
    return (factor*F0F1[1]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point;


    }
    else
    {
        return Vector::Zero();
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
void StandardCompactPolynomial<DEGREE>::EvalValueAndGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                                                         Scalar& value_res, Vector& grad_res) const
{
//TODO:@todo : some data structure have changed and some part of the following code should be improved

    // Computation of the intersection of the segment with the sphere of radius scale_
    Scalar l1, l2;
    int n_inter = core::Geometry::Intersection(Sphere(this->scale_, point),
                                               seg.p_min(),  seg.length()*seg.increase_unit_dir(), l1, l2);

    if(n_inter == 2)
    {
        if(l1>1.0) { l1 = 1.0; }
        if(l1<0.0) { l1 = 0.0; }
        if(l2>1.0) { l2 = 1.0; }
        if(l2<0.0) { l2 = 0.0; }

        const Vector p_min_to_point( point-seg.p_min() );
        const Scalar a = - this->inv_scale_2_*seg.length()*seg.length();
        const Scalar b = - this->inv_scale_2_*seg.length()*seg.increase_unit_dir().dot(p_min_to_point);
        const Scalar c = - this->inv_scale_2_*p_min_to_point.squaredNorm() + 1.0;

        std::vector<Scalar> weight_coeff; //TODO:@todo : beurk
        weight_coeff.push_back(seg.weight_min());
        weight_coeff.push_back(seg.unit_delta_weight()*seg.length());

        Scalar F0F1F2[3];
        StandardCompactPolynomialOptim::StandardCompactPolynomial_segment_FGradF<DEGREE,1>(l1,l2,a,b,c,weight_coeff, F0F1F2);

//        Scalar delta_w = seg.length()*seg.unit_delta_weight();

//        Scalar F0F1F2[3];
//        StandardCompactPolynomialOptim::StandardCompactPolynomial_segment_FGradF<DEGREE,1>(l1,l2,a,b,c,seg.weight_min(),delta_w, F0F1F2);

        value_res = this->normalization_factor_1D_ * seg.length() * F0F1F2[0];
        const Scalar factor = this->normalization_factor_1D_ * seg.length()*this->degree_r()*this->inv_scale_2_;
        grad_res = (factor*F0F1F2[2]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point;
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}
//-------------------------------------------------------------------------



//////////////////////////
/// Bounding functions ///
//////////////////////////

template <int DEGREE>
Scalar StandardCompactPolynomial<DEGREE>::GetIsoValueForThickness(Scalar length, Scalar thickness) const
{
    std::vector<Scalar> weight;
    weight.push_back(1.0);
    weight.push_back(0.0);

    Scalar l1, l2;
    int n_inter = core::Geometry::Intersection(core::Sphere(this->scale_, Point(0.0,0.0,thickness)),
                                               Point(-length*0.5,0.0,0.0), Vector(length,0.0,0.0), l1, l2);
    if(n_inter == 2) {
        if(l1>1.0) { l1 = 1.0; }
        if(l1<0.0) { l1 = 0.0; }
        if(l2>1.0) { l2 = 1.0; }
        if(l2<0.0) { l2 = 0.0; }
        const Scalar a = - this->inv_scale_2_*length*length;
        const Scalar b = a*0.5;
        const Scalar c = 1.0 - this->inv_scale_2_*thickness*thickness + a*0.25;

        return this->normalization_factor_1D_*length*StandardCompactPolynomialOptim::StandardCompactPolynomial_segment_F<DEGREE,1>(l1,l2,a,b,c,weight);
    } else {
        return 0.0;
    }
}

} // Close namespace convol

} // Close namespace expressive
