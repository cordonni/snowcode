/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: HornusConvolCompactPolynomialOptimizedFunctions.h
  
   Language: C++
  
   License: Convol Licence
  
   \author: Cedric Zanni 
   E-Mail: cedric.zanni@inria.fr
  
   Description: Header files defining optimized function for Hornus CompactPolynomialX kernels.
 
   Platform Dependencies: None 
*/  

#pragma once
#ifndef CONVOL_HORNUS_COMPACT_POLYNOMIAL_OPTIMIZED_FUNCTIONS_H_
#define CONVOL_HORNUS_COMPACT_POLYNOMIAL_OPTIMIZED_FUNCTIONS_H_

// core dependencies
#include <core/CoreRequired.h>

// std dependencies
#include<vector>
#include<array>

#include<math.h>

namespace expressive {

namespace convol {

namespace HornusCompactPolynomialOptim {

////////////////////////////
/// Function declaration ///
////////////////////////////

template<int I>
inline Scalar HornusCompactPolynomial_segment_F (Scalar l, Scalar d, const Scalar w[3]);
template<int I>
inline void HornusCompactPolynomial_segment_GradF (Scalar l, Scalar d, const Scalar w[3], Scalar res[2]);
template<int I>
inline void HornusCompactPolynomial_segment_FGradF (Scalar l, Scalar d, const Scalar w[3], Scalar res[3]);

template<int I>
inline Scalar HornusCompactPolynomial_approx_segment_F (Scalar l, Scalar d, Scalar q, const Scalar w[3]);
template<int I>
inline void HornusCompactPolynomial_approx_segment_GradF (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[2]);
template<int I>
inline void HornusCompactPolynomial_approx_segment_FGradF (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3]);

///////////////////////////////
/// Template specialization ///
///////////////////////////////





    /////////////////////////////////
    // Code for kernel of degree 4 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HornusCompactPolynomial_segment_F<4> (Scalar l, Scalar d, const Scalar w[3])
    {
        const Scalar t5181 = d * l + 0.1e1;
        const Scalar t5178 = 0.1e1 / t5181;
        const Scalar t2 = t5181 * t5181;
        const Scalar t5179 = 0.1e1 / t2;
        const Scalar t5187 = 0.1e1 / d;
        const Scalar t5195 = 0.2e1 * t5187;
        const Scalar t3 = log(t5181);
        const Scalar t5193 = t3 * t5187;
        const Scalar t5186 = l * l;
        const Scalar t5185 = l * t5186;
        const Scalar t5184 = w[0];
        const Scalar t5183 = w[1];
        const Scalar t5182 = w[2];
        const Scalar t5180 = t5178 * t5179;
        const Scalar t4 = t5184 * t5184;
        const Scalar t11 = t5183 * t5183;
        const Scalar t22 = t5182 * t5182;
        const Scalar t32 = t5186 * t5186;
        return  -t4 * (t5180 - 0.1e1) * t5187 / 0.3e1 + ((0.2e1 * t5182 * t5184 + 0.4e1 * t11) * ((-(t5178 - 0.1e1) * t5187 - l * t5179) * t5187 - t5186 * t5180) + t22 * ((0.3e1 * ((l - t5193) * t5195 - t5186 * t5178) * t5187 - t5185 * t5179) * t5195 - t32 * t5180)) * t5187 / 0.3e1 - 0.4e1 / 0.3e1 * (t5184 * (-(t5179 - 0.1e1) * t5187 / 0.2e1 - l * t5180) + t5182 * (0.3e1 / 0.2e1 * ((t5193 - l * t5178) * t5195 - t5186 * t5179) * t5187 - t5185 * t5180)) * t5183 * t5187;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_segment_GradF<4> (Scalar l, Scalar d, const Scalar w[3], Scalar res[2])
    {
        const Scalar t5201 = d * l + 0.1e1;
        const Scalar t5198 = 0.1e1 / t5201;
        const Scalar t2 = t5201 * t5201;
        const Scalar t5199 = 0.1e1 / t2;
        const Scalar t5211 = -0.2e1 / 0.3e1 * w[1];
        const Scalar t5200 = t5198 * t5199;
        const Scalar t5205 = l * l;
        const Scalar t5210 = t5205 * t5200;
        const Scalar t5206 = 0.1e1 / d;
        const Scalar t5204 = w[0];
        const Scalar t5202 = w[2];
        const Scalar t5197 = -(t5199 - 0.1e1) * t5206 / 0.2e1 - l * t5200;
        const Scalar t5196 = (-(t5198 - 0.1e1) * t5206 - l * t5199) * t5206 - t5210;
        res[0] = (-t5204 * (t5200 - 0.1e1) / 0.3e1 + t5197 * t5211 + t5202 * t5196 / 0.3e1) * t5206;
        const Scalar t28 = log(t5201);
        res[1] = (t5204 * t5197 / 0.3e1 + t5196 * t5211 + (-l * t5210 / 0.3e1 + (-t5205 * t5199 / 0.2e1 + (t28 * t5206 - l * t5198) * t5206) * t5206) * t5202) * t5206;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_segment_FGradF<4> (Scalar l, Scalar d, const Scalar w[3], Scalar res[3])
    {
        const Scalar t5220 = d * l + 0.1e1;
        const Scalar t5217 = 0.1e1 / t5220;
        const Scalar t2 = t5220 * t5220;
        const Scalar t5218 = 0.1e1 / t2;
        const Scalar t5226 = 0.1e1 / d;
        const Scalar t5236 = 0.2e1 * t5226;
        const Scalar t5219 = t5217 * t5218;
        const Scalar t5235 = -t5219 / 0.3e1 + 0.1e1 / 0.3e1;
        const Scalar t5221 = w[2];
        const Scalar t5234 = t5221 / 0.3e1;
        const Scalar t4 = log(t5220);
        const Scalar t5233 = t4 * t5226;
        const Scalar t5225 = l * l;
        const Scalar t5213 = (-(t5217 - 0.1e1) * t5226 - l * t5218) * t5226 - t5225 * t5219;
        const Scalar t5222 = w[1];
        const Scalar t5232 = t5222 * t5213;
        const Scalar t5214 = -(t5218 - 0.1e1) * t5226 / 0.2e1 - l * t5219;
        const Scalar t5223 = w[0];
        const Scalar t5231 = t5223 * t5214;
        const Scalar t5224 = l * t5225;
        const Scalar t5212 = 0.3e1 / 0.2e1 * ((t5233 - l * t5217) * t5236 - t5225 * t5218) * t5226 - t5224 * t5219;
        const Scalar t23 = t5223 * t5223;
        const Scalar t45 = t5225 * t5225;
        res[0] = (t23 * t5235 + (-0.4e1 / 0.3e1 * t5231 + 0.4e1 / 0.3e1 * t5232) * t5222) * t5226 + (0.2e1 / 0.3e1 * t5223 * t5213 * t5226 - 0.4e1 / 0.3e1 * t5222 * t5212 * t5226 + ((0.3e1 * ((l - t5233) * t5236 - t5225 * t5217) * t5226 - t5224 * t5218) * t5236 - t45 * t5219) * t5226 * t5234) * t5221;
        res[1] = (t5223 * t5235 - 0.2e1 / 0.3e1 * t5222 * t5214 + t5213 * t5234) * t5226;
        res[2] = (t5231 / 0.3e1 - 0.2e1 / 0.3e1 * t5232 + t5212 * t5234) * t5226;

    }
    
    template<>
    inline Scalar HornusCompactPolynomial_approx_segment_F<4> (Scalar l, Scalar d, Scalar q, const Scalar w[3])
    {
        const Scalar t5264 = q * d;
        const Scalar t5246 = t5264 + 0.1e1;
        const Scalar t1 = t5246 * t5246;
        const Scalar t2 = t1 * t1;
        const Scalar t5244 = 0.1e1 / t2;
        const Scalar t5248 = w[2];
        const Scalar t5249 = w[1];
        const Scalar t5250 = w[0];
        const Scalar t5251 = l * l;
        const Scalar t5241 = t5248 * t5251 - 0.2e1 * t5249 * l + t5250;
        const Scalar t7 = t5249 * t5249;
        const Scalar t5242 = t5248 * t5250 - t7;
        const Scalar t5243 = t5248 * l - t5249;
        const Scalar t5247 = 0.1e1 / t5248;
        const Scalar t5254 = t5241 * t5241;
        const Scalar t5259 = t5250 * t5250;
        const Scalar t5263 = 0.4e1 / 0.15e2 * t5242 * (0.2e1 * t5242 * l + t5243 * t5241 + t5249 * t5250) * t5247 + t5243 * t5254 / 0.5e1 + t5249 * t5259 / 0.5e1;
        const Scalar t5261 = t5247 * t5263;
        const Scalar t5245 = 0.1e1 / t5246 * t5244;
        const Scalar t5240 = t5241 * t5254;
        const Scalar t5238 = t5249 * t5261 + t5240 / 0.6e1 - t5250 * t5259 / 0.6e1;
        const Scalar t5237 = -l * t5240 + (-0.8e1 * t5249 * t5238 + t5250 * t5263) * t5247;
        const Scalar t62 = q * q;
        return  t5261 - 0.4e1 * d * t5238 * t5247 + (-0.1428571429e0 * (0.3e1 * t5244 - 0.300e1 + 0.4e1 * (0.2e1 + t5245) * t5264) * t5237 - 0.1250000000e0 * (0.2e1 - 0.200e1 * t5244 - 0.5e1 * (0.1e1 + t5245) * t5264) / q * (-0.1e1 * t5251 * t5240 + (0.1428571429e1 * t5249 * t5237 + 0.2e1 * t5250 * t5238) * t5247)) * t5247 / t62;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_approx_segment_GradF<4> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[2])
    {
        const Scalar t1 = q * q;
        const Scalar t5280 = 0.1e1 / t1;
        const Scalar t5294 = q * d;
        const Scalar t5275 = t5294 + 0.1e1;
        const Scalar t2 = t5275 * t5275;
        const Scalar t3 = t2 * t2;
        const Scalar t5273 = 0.1e1 / t3;
        const Scalar t5277 = w[2];
        const Scalar t5278 = w[1];
        const Scalar t5279 = w[0];
        const Scalar t5282 = l * l;
        const Scalar t5272 = t5277 * t5282 - 0.2e1 * t5278 * l + t5279;
        const Scalar t5271 = t5272 * t5272;
        const Scalar t5276 = 0.1e1 / t5277;
        const Scalar t8 = t5278 * t5278;
        const Scalar t5292 = 0.2e1 / 0.3e1 * (t5277 * t5279 - t8) * l + (t5277 * l - t5278) * t5272 / 0.3e1 + t5278 * t5279 / 0.3e1;
        const Scalar t21 = t5279 * t5279;
        const Scalar t5267 = t5278 * t5276 * t5292 + t5271 / 0.4e1 - t21 / 0.4e1;
        const Scalar t5266 = -l * t5271 + (-0.6e1 * t5278 * t5267 + t5279 * t5292) * t5276;
        const Scalar t5289 = t5282 * t5271;
        const Scalar t5265 = -t5289 + (0.8e1 / 0.5e1 * t5278 * t5266 + 0.2e1 * t5279 * t5267) * t5276;
        const Scalar t5293 = -t5265 / 0.6e1;
        const Scalar t5274 = 0.1e1 / t5275 * t5273;
        const Scalar t5291 = (0.3e1 * t5273 - 0.300e1 + 0.4e1 * (0.2e1 + t5274) * t5294) * t5280;
        const Scalar t5290 = (0.2e1 - 0.200e1 * t5273 - 0.4e1 * (0.1e1 + t5274) * t5294) / q * t5280;
        res[0] = (t5292 - 0.4e1 * d * t5267 - t5266 * t5291 / 0.5e1 + t5290 * t5293) * t5276;
        res[1] = (t5267 + 0.4e1 / 0.5e1 * d * t5266 + t5291 * t5293 + (l * t5289 / 0.7e1 - (0.5e1 / 0.3e1 * t5278 * t5265 - 0.3e1 / 0.5e1 * t5279 * t5266) * t5276 / 0.7e1) * t5290) * t5276;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_approx_segment_FGradF<4> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3])
    {
        const Scalar t1 = q * q;
        const Scalar t5317 = 0.1e1 / t1;
        const Scalar t5340 = q * d;
        const Scalar t5311 = t5340 + 0.1e1;
        const Scalar t2 = t5311 * t5311;
        const Scalar t3 = t2 * t2;
        const Scalar t5309 = 0.1e1 / t3;
        const Scalar t5316 = w[0];
        const Scalar t5339 = 0.2e1 * t5316;
        const Scalar t5314 = w[2];
        const Scalar t5312 = 0.1e1 / t5314;
        const Scalar t5338 = d * t5312;
        const Scalar t5315 = w[1];
        const Scalar t5319 = l * l;
        const Scalar t5306 = t5314 * t5319 - 0.2e1 * t5315 * l + t5316;
        const Scalar t5305 = t5306 * t5306;
        const Scalar t5313 = t5316 * t5316;
        const Scalar t8 = t5315 * t5315;
        const Scalar t5307 = t5314 * t5316 - t8;
        const Scalar t5308 = t5314 * l - t5315;
        const Scalar t5301 = 0.2e1 * t5307 * l + t5308 * t5306 + t5315 * t5316;
        const Scalar t5334 = t5301 * t5312;
        const Scalar t5300 = t5315 * t5334 / 0.3e1 + t5305 / 0.4e1 - t5313 / 0.4e1;
        const Scalar t5335 = t5301 / 0.3e1;
        const Scalar t5297 = -l * t5305 + (-0.6e1 * t5315 * t5300 + t5316 * t5335) * t5312;
        const Scalar t5331 = t5319 * t5305;
        const Scalar t5295 = -t5331 + (0.8e1 / 0.5e1 * t5315 * t5297 + t5300 * t5339) * t5312;
        const Scalar t5337 = -t5295 / 0.6e1;
        const Scalar t5336 = 0.4e1 / 0.15e2 * t5307 * t5334 + t5308 * t5305 / 0.5e1 + t5315 * t5313 / 0.5e1;
        const Scalar t5310 = 0.1e1 / t5311 * t5309;
        const Scalar t5333 = (0.3e1 * t5309 - 0.300e1 + 0.4e1 * (0.2e1 + t5310) * t5340) * t5317;
        const Scalar t5332 = (0.2e1 - 0.200e1 * t5309 - 0.4e1 * (0.1e1 + t5310) * t5340) / q * t5317;
        const Scalar t5330 = t5312 * t5333;
        const Scalar t5329 = t5312 * t5332;
        const Scalar t5328 = t5312 * t5336;
        const Scalar t5304 = t5306 * t5305;
        const Scalar t5298 = t5315 * t5328 + t5304 / 0.6e1 - t5316 * t5313 / 0.6e1;
        const Scalar t5296 = -l * t5304 + (-0.8e1 * t5315 * t5298 + t5316 * t5336) * t5312;
        res[0] = t5328 - 0.4e1 * t5298 * t5338 - t5296 * t5330 / 0.7e1 - (-t5319 * t5304 + (0.10e2 / 0.7e1 * t5315 * t5296 + t5298 * t5339) * t5312) * t5329 / 0.8e1;
        res[1] = (t5335 - 0.4e1 * d * t5300 - t5297 * t5333 / 0.5e1 + t5332 * t5337) * t5312;
        res[2] = t5300 * t5312 + 0.4e1 / 0.5e1 * t5297 * t5338 + t5330 * t5337 - (-l * t5331 + (0.5e1 / 0.3e1 * t5315 * t5295 - 0.3e1 / 0.5e1 * t5316 * t5297) * t5312) * t5329 / 0.7e1;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 6 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HornusCompactPolynomial_segment_F<6> (Scalar l, Scalar d, const Scalar w[3])
    {
        const Scalar t5347 = d * l + 0.1e1;
        const Scalar t5342 = 0.1e1 / t5347;
        const Scalar t5363 = t5347 * t5347;
        const Scalar t5343 = 0.1e1 / t5363;
        const Scalar t2 = t5363 * t5363;
        const Scalar t5345 = 0.1e1 / t2;
        const Scalar t5358 = 0.1e1 / d;
        const Scalar t5375 = 0.2e1 * t5358;
        const Scalar t5352 = w[1];
        const Scalar t5349 = t5352 * t5352;
        const Scalar t5374 = 0.12e2 * t5349;
        const Scalar t5372 = 0.3e1 / 0.2e1 * t5358;
        const Scalar t3 = log(t5347);
        const Scalar t5371 = t3 * t5358;
        const Scalar t5370 = -0.6e1 / 0.5e1 * t5352 * t5358;
        const Scalar t5357 = l * l;
        const Scalar t5360 = t5357 * t5357;
        const Scalar t5359 = l * t5357;
        const Scalar t5354 = l * t5360;
        const Scalar t5353 = w[0];
        const Scalar t5351 = w[2];
        const Scalar t5350 = t5353 * t5353;
        const Scalar t5348 = t5351 * t5351;
        const Scalar t5346 = t5342 * t5345;
        const Scalar t5344 = t5342 * t5343;
        const Scalar t85 = t5359 * t5359;
        return  t5348 * (0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (((t5371 - l * t5342) * t5375 - t5357 * t5343) * t5372 - t5359 * t5344) * t5358 - t5360 * t5345) * t5358 - t5354 * t5346) * t5370 + (-t5353 * (t5346 - 0.1e1) * t5358 / 0.5e1 + (-(t5345 - 0.1e1) * t5358 / 0.4e1 - l * t5346) * t5370) * t5350 + ((t5353 * t5374 + 0.3e1 * t5351 * t5350) * ((-(t5344 - 0.1e1) * t5358 / 0.3e1 - l * t5345) * t5358 / 0.2e1 - t5357 * t5346) + (0.3e1 * t5348 * t5353 + t5351 * t5374) * ((((-(t5342 - 0.1e1) * t5358 - l * t5343) * t5358 - t5357 * t5344) * t5358 - t5359 * t5345) * t5358 - t5360 * t5346) + t5351 * t5348 * ((0.5e1 / 0.3e1 * ((0.3e1 * ((l - t5371) * t5375 - t5357 * t5342) * t5358 - t5359 * t5343) * t5375 - t5360 * t5344) * t5358 - t5354 * t5345) * t5372 - t85 * t5346) + (-0.8e1 * t5349 - 0.12e2 * t5351 * t5353) * (0.3e1 / 0.4e1 * (0.2e1 / 0.3e1 * (-(t5343 - 0.1e1) * t5358 / 0.2e1 - l * t5344) * t5358 - t5357 * t5345) * t5358 - t5359 * t5346) * t5352) * t5358 / 0.5e1;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_segment_GradF<6> (Scalar l, Scalar d, const Scalar w[3], Scalar res[2])
    {
        const Scalar t5386 = d * l + 0.1e1;
        const Scalar t5381 = 0.1e1 / t5386;
        const Scalar t5399 = t5386 * t5386;
        const Scalar t5382 = 0.1e1 / t5399;
        const Scalar t2 = t5399 * t5399;
        const Scalar t5384 = 0.1e1 / t2;
        const Scalar t5389 = w[2];
        const Scalar t5406 = -0.4e1 / 0.5e1 * t5389;
        const Scalar t5391 = w[0];
        const Scalar t5405 = -0.4e1 / 0.5e1 * t5391;
        const Scalar t5385 = t5381 * t5384;
        const Scalar t5394 = l * l;
        const Scalar t5397 = t5394 * t5394;
        const Scalar t5403 = t5397 * t5385;
        const Scalar t5395 = 0.1e1 / d;
        const Scalar t5393 = l * t5394;
        const Scalar t5390 = w[1];
        const Scalar t5388 = t5391 * t5391;
        const Scalar t5387 = t5389 * t5389;
        const Scalar t5383 = t5381 * t5382;
        const Scalar t7 = t5390 * t5390;
        const Scalar t5380 = 0.2e1 * t5389 * t5391 + 0.4e1 * t7;
        const Scalar t5379 = -(t5384 - 0.1e1) * t5395 / 0.4e1 - l * t5385;
        const Scalar t5378 = (-(t5383 - 0.1e1) * t5395 / 0.3e1 - l * t5384) * t5395 / 0.2e1 - t5394 * t5385;
        const Scalar t5377 = 0.3e1 / 0.4e1 * (0.2e1 / 0.3e1 * (-(t5382 - 0.1e1) * t5395 / 0.2e1 - l * t5383) * t5395 - t5394 * t5384) * t5395 - t5393 * t5385;
        const Scalar t5376 = (((-(t5381 - 0.1e1) * t5395 - l * t5382) * t5395 - t5394 * t5383) * t5395 - t5393 * t5384) * t5395 - t5403;
        res[0] = (-t5388 * (t5385 - 0.1e1) / 0.5e1 + t5380 * t5378 / 0.5e1 + t5387 * t5376 / 0.5e1 + (t5379 * t5405 + t5377 * t5406) * t5390) * t5395;
        const Scalar t63 = log(t5386);
        res[1] = (t5378 * t5405 + t5376 * t5406) * t5395 * t5390 + (t5388 * t5379 + t5380 * t5377 + t5387 * (0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * (0.2e1 * (t63 * t5395 - l * t5381) * t5395 - t5394 * t5382) * t5395 - t5393 * t5383) * t5395 - t5397 * t5384) * t5395 - l * t5403)) * t5395 / 0.5e1;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_segment_FGradF<6> (Scalar l, Scalar d, const Scalar w[3], Scalar res[3])
    {
        const Scalar t5420 = d * l + 0.1e1;
        const Scalar t5415 = 0.1e1 / t5420;
        const Scalar t5436 = t5420 * t5420;
        const Scalar t5416 = 0.1e1 / t5436;
        const Scalar t2 = t5436 * t5436;
        const Scalar t5418 = 0.1e1 / t2;
        const Scalar t5431 = 0.1e1 / d;
        const Scalar t5455 = 0.2e1 * t5431;
        const Scalar t5425 = w[1];
        const Scalar t5422 = t5425 * t5425;
        const Scalar t5454 = 0.12e2 * t5422;
        const Scalar t5424 = w[2];
        const Scalar t5426 = w[0];
        const Scalar t5445 = t5424 * t5426;
        const Scalar t5453 = 0.2e1 / 0.5e1 * t5445 + 0.4e1 / 0.5e1 * t5422;
        const Scalar t5452 = -0.4e1 / 0.5e1 * t5424;
        const Scalar t5451 = -0.4e1 / 0.5e1 * t5426;
        const Scalar t5449 = 0.3e1 / 0.2e1 * t5431;
        const Scalar t7 = log(t5420);
        const Scalar t5448 = t7 * t5431;
        const Scalar t5417 = t5415 * t5416;
        const Scalar t5419 = t5415 * t5418;
        const Scalar t5421 = t5424 * t5424;
        const Scalar t5430 = l * l;
        const Scalar t5433 = t5430 * t5430;
        const Scalar t5427 = l * t5433;
        const Scalar t5432 = l * t5430;
        const Scalar t5447 = t5421 * (0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (((t5448 - l * t5415) * t5455 - t5430 * t5416) * t5449 - t5432 * t5417) * t5431 - t5433 * t5418) * t5431 - t5427 * t5419);
        const Scalar t5411 = -(t5418 - 0.1e1) * t5431 / 0.4e1 - l * t5419;
        const Scalar t5423 = t5426 * t5426;
        const Scalar t5446 = t5423 * t5411;
        const Scalar t5443 = -t5423 * (t5419 - 0.1e1) / 0.5e1;
        const Scalar t5410 = (-(t5417 - 0.1e1) * t5431 / 0.3e1 - l * t5418) * t5431 / 0.2e1 - t5430 * t5419;
        const Scalar t5409 = 0.3e1 / 0.4e1 * (0.2e1 / 0.3e1 * (-(t5416 - 0.1e1) * t5431 / 0.2e1 - l * t5417) * t5431 - t5430 * t5418) * t5431 - t5432 * t5419;
        const Scalar t5408 = (((-(t5415 - 0.1e1) * t5431 - l * t5416) * t5431 - t5430 * t5417) * t5431 - t5432 * t5418) * t5431 - t5433 * t5419;
        const Scalar t96 = t5432 * t5432;
        res[0] = t5426 * t5431 * t5443 + (-0.6e1 / 0.5e1 * t5446 - 0.6e1 / 0.5e1 * t5447 + (-0.8e1 / 0.5e1 * t5422 - 0.12e2 / 0.5e1 * t5445) * t5409) * t5425 * t5431 + ((t5426 * t5454 + 0.3e1 * t5424 * t5423) * t5410 + t5424 * t5421 * ((0.5e1 / 0.3e1 * ((0.3e1 * ((l - t5448) * t5455 - t5430 * t5415) * t5431 - t5432 * t5416) * t5455 - t5433 * t5417) * t5431 - t5427 * t5418) * t5449 - t96 * t5419) + (0.3e1 * t5421 * t5426 + t5424 * t5454) * t5408) * t5431 / 0.5e1;
        res[1] = (t5443 + t5410 * t5453 + t5421 * t5408 / 0.5e1 + (t5411 * t5451 + t5409 * t5452) * t5425) * t5431;
        res[2] = (t5446 / 0.5e1 + t5409 * t5453 + t5447 / 0.5e1 + (t5410 * t5451 + t5408 * t5452) * t5425) * t5431;

    }
    
    template<>
    inline Scalar HornusCompactPolynomial_approx_segment_F<6> (Scalar l, Scalar d, Scalar q, const Scalar w[3])
    {
        const Scalar t5489 = q * d;
        const Scalar t5465 = t5489 + 0.1e1;
        const Scalar t1 = t5465 * t5465;
        const Scalar t2 = t1 * t1;
        const Scalar t5463 = 0.1e1 / t2 / t1;
        const Scalar t5467 = w[2];
        const Scalar t5468 = w[1];
        const Scalar t5469 = w[0];
        const Scalar t5470 = l * l;
        const Scalar t5460 = t5467 * t5470 - 0.2e1 * t5468 * l + t5469;
        const Scalar t8 = t5468 * t5468;
        const Scalar t5461 = t5467 * t5469 - t8;
        const Scalar t5462 = t5467 * l - t5468;
        const Scalar t5480 = t5469 * t5469;
        const Scalar t5484 = t5468 * t5480;
        const Scalar t5473 = t5460 * t5460;
        const Scalar t5486 = t5462 * t5473;
        const Scalar t5466 = 0.1e1 / t5467;
        const Scalar t5487 = t5461 * t5466;
        const Scalar t5488 = 0.6e1 / 0.35e2 * (0.4e1 / 0.3e1 * (0.2e1 * t5461 * l + t5462 * t5460 + t5468 * t5469) * t5487 + t5486 + t5484) * t5487 + t5460 * t5486 / 0.7e1 + t5469 * t5484 / 0.7e1;
        const Scalar t5483 = t5466 * t5488;
        const Scalar t5464 = 0.1e1 / t5465 * t5463;
        const Scalar t5459 = t5473 * t5473;
        const Scalar t27 = t5480 * t5480;
        const Scalar t5457 = t5468 * t5483 + t5459 / 0.8e1 - t27 / 0.8e1;
        const Scalar t5456 = -l * t5459 + (-0.10e2 * t5468 * t5457 + t5469 * t5488) * t5466;
        const Scalar t65 = q * q;
        return  t5483 - 0.6e1 * d * t5457 * t5466 + (-0.1111111111e0 * (0.3e1 * t5463 - 0.300e1 + 0.6e1 * (0.2e1 + t5464) * t5489) * t5456 - 0.1000000000e0 * (0.2e1 - 0.200e1 * t5463 - 0.7e1 * (0.1e1 + t5464) * t5489) / q * (-0.1e1 * t5470 * t5459 + (0.1333333333e1 * t5468 * t5456 + 0.2e1 * t5469 * t5457) * t5466)) * t5466 / t65;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_approx_segment_GradF<6> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[2])
    {
        const Scalar t1 = q * q;
        const Scalar t5507 = 0.1e1 / t1;
        const Scalar t5526 = q * d;
        const Scalar t5502 = t5526 + 0.1e1;
        const Scalar t2 = t5502 * t5502;
        const Scalar t3 = t2 * t2;
        const Scalar t5500 = 0.1e1 / t3 / t2;
        const Scalar t5504 = w[2];
        const Scalar t5505 = w[1];
        const Scalar t5506 = w[0];
        const Scalar t5509 = l * l;
        const Scalar t5497 = t5504 * t5509 - 0.2e1 * t5505 * l + t5506;
        const Scalar t5513 = t5497 * t5497;
        const Scalar t5496 = t5497 * t5513;
        const Scalar t5503 = 0.1e1 / t5504;
        const Scalar t5519 = t5506 * t5506;
        const Scalar t9 = t5505 * t5505;
        const Scalar t5498 = t5504 * t5506 - t9;
        const Scalar t5499 = t5504 * l - t5505;
        const Scalar t5524 = 0.4e1 / 0.15e2 * t5498 * (0.2e1 * t5498 * l + t5499 * t5497 + t5505 * t5506) * t5503 + t5499 * t5513 / 0.5e1 + t5505 * t5519 / 0.5e1;
        const Scalar t5492 = t5505 * t5503 * t5524 + t5496 / 0.6e1 - t5506 * t5519 / 0.6e1;
        const Scalar t5491 = -l * t5496 + (-0.8e1 * t5505 * t5492 + t5506 * t5524) * t5503;
        const Scalar t5521 = t5509 * t5496;
        const Scalar t5490 = -t5521 + (0.10e2 / 0.7e1 * t5505 * t5491 + 0.2e1 * t5506 * t5492) * t5503;
        const Scalar t5525 = -t5490 / 0.8e1;
        const Scalar t5501 = 0.1e1 / t5502 * t5500;
        const Scalar t5523 = (0.3e1 * t5500 - 0.300e1 + 0.6e1 * (0.2e1 + t5501) * t5526) * t5507;
        const Scalar t5522 = (0.2e1 - 0.200e1 * t5500 - 0.6e1 * (0.1e1 + t5501) * t5526) / q * t5507;
        res[0] = (t5524 - 0.6e1 * d * t5492 - t5491 * t5523 / 0.7e1 + t5522 * t5525) * t5503;
        res[1] = (t5492 + 0.6e1 / 0.7e1 * d * t5491 + t5523 * t5525 + (l * t5521 / 0.9e1 - (0.3e1 / 0.2e1 * t5505 * t5490 - 0.3e1 / 0.7e1 * t5506 * t5491) * t5503 / 0.9e1) * t5522) * t5503;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_approx_segment_FGradF<6> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3])
    {
        const Scalar t1 = q * q;
        const Scalar t5549 = 0.1e1 / t1;
        const Scalar t5577 = q * d;
        const Scalar t5543 = t5577 + 0.1e1;
        const Scalar t2 = t5543 * t5543;
        const Scalar t3 = t2 * t2;
        const Scalar t5541 = 0.1e1 / t3 / t2;
        const Scalar t5548 = w[0];
        const Scalar t5576 = 0.2e1 * t5548;
        const Scalar t5546 = w[2];
        const Scalar t5544 = 0.1e1 / t5546;
        const Scalar t5575 = d * t5544;
        const Scalar t5547 = w[1];
        const Scalar t5551 = l * l;
        const Scalar t5538 = t5546 * t5551 - 0.2e1 * t5547 * l + t5548;
        const Scalar t5555 = t5538 * t5538;
        const Scalar t5537 = t5538 * t5555;
        const Scalar t5562 = t5548 * t5548;
        const Scalar t5545 = t5548 * t5562;
        const Scalar t9 = t5547 * t5547;
        const Scalar t5539 = t5546 * t5548 - t9;
        const Scalar t5540 = t5546 * l - t5547;
        const Scalar t5569 = t5539 * t5544;
        const Scalar t5533 = 0.4e1 / 0.3e1 * (0.2e1 * t5539 * l + t5540 * t5538 + t5547 * t5548) * t5569 + t5540 * t5555 + t5547 * t5562;
        const Scalar t5572 = t5533 / 0.5e1;
        const Scalar t5532 = t5547 * t5544 * t5572 + t5537 / 0.6e1 - t5545 / 0.6e1;
        const Scalar t5529 = -l * t5537 + (-0.8e1 * t5547 * t5532 + t5548 * t5572) * t5544;
        const Scalar t5568 = t5551 * t5537;
        const Scalar t5527 = -t5568 + (0.10e2 / 0.7e1 * t5547 * t5529 + t5532 * t5576) * t5544;
        const Scalar t5574 = -t5527 / 0.8e1;
        const Scalar t5573 = 0.6e1 / 0.35e2 * t5533 * t5569 + t5540 * t5537 / 0.7e1 + t5547 * t5545 / 0.7e1;
        const Scalar t5542 = 0.1e1 / t5543 * t5541;
        const Scalar t5571 = (0.3e1 * t5541 - 0.300e1 + 0.6e1 * (0.2e1 + t5542) * t5577) * t5549;
        const Scalar t5570 = (0.2e1 - 0.200e1 * t5541 - 0.6e1 * (0.1e1 + t5542) * t5577) / q * t5549;
        const Scalar t5567 = t5544 * t5571;
        const Scalar t5566 = t5544 * t5570;
        const Scalar t5565 = t5544 * t5573;
        const Scalar t5536 = t5555 * t5555;
        const Scalar t57 = t5562 * t5562;
        const Scalar t5530 = t5547 * t5565 + t5536 / 0.8e1 - t57 / 0.8e1;
        const Scalar t5528 = -l * t5536 + (-0.10e2 * t5547 * t5530 + t5548 * t5573) * t5544;
        res[0] = t5565 - 0.6e1 * t5530 * t5575 - t5528 * t5567 / 0.9e1 - (-t5551 * t5536 + (0.4e1 / 0.3e1 * t5547 * t5528 + t5530 * t5576) * t5544) * t5566 / 0.10e2;
        res[1] = (t5572 - 0.6e1 * d * t5532 - t5529 * t5571 / 0.7e1 + t5570 * t5574) * t5544;
        res[2] = t5532 * t5544 + 0.6e1 / 0.7e1 * t5529 * t5575 + t5567 * t5574 - (-l * t5568 + (0.3e1 / 0.2e1 * t5547 * t5527 - 0.3e1 / 0.7e1 * t5548 * t5529) * t5544) * t5566 / 0.9e1;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 8 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HornusCompactPolynomial_segment_F<8> (Scalar l, Scalar d, const Scalar w[3])
    {
        const Scalar t5586 = d * l + 0.1e1;
        const Scalar t5579 = 0.1e1 / t5586;
        const Scalar t5609 = t5586 * t5586;
        const Scalar t2 = t5609 * t5609;
        const Scalar t5582 = 0.1e1 / t2;
        const Scalar t5610 = t5586 * t5609;
        const Scalar t3 = t5610 * t5610;
        const Scalar t5584 = 0.1e1 / t3;
        const Scalar t5602 = 0.1e1 / d;
        const Scalar t5631 = 0.2e1 * t5602;
        const Scalar t5594 = w[1];
        const Scalar t5590 = t5594 * t5594;
        const Scalar t5630 = -0.32e2 * t5594 * t5590;
        const Scalar t5629 = 0.24e2 * t5590;
        const Scalar t5628 = -0.24e2 * t5594;
        const Scalar t5627 = t5602 / 0.2e1;
        const Scalar t5625 = 0.2e1 / 0.3e1 * t5602;
        const Scalar t5624 = 0.3e1 / 0.2e1 * t5602;
        const Scalar t5623 = 0.4e1 / 0.3e1 * t5602;
        const Scalar t7 = log(t5586);
        const Scalar t5622 = t7 * t5602;
        const Scalar t5601 = l * l;
        const Scalar t5603 = l * t5601;
        const Scalar t5606 = t5603 * t5603;
        const Scalar t5604 = t5601 * t5601;
        const Scalar t5598 = l * t5604;
        const Scalar t5596 = l * t5606;
        const Scalar t5595 = w[0];
        const Scalar t5593 = w[2];
        const Scalar t5592 = t5595 * t5595;
        const Scalar t5591 = t5595 * t5592;
        const Scalar t5588 = t5593 * t5593;
        const Scalar t5587 = t5593 * t5588;
        const Scalar t5585 = t5579 * t5584;
        const Scalar t5583 = t5579 * t5582;
        const Scalar t5581 = 0.1e1 / t5610;
        const Scalar t5580 = 0.1e1 / t5609;
        const Scalar t8 = t5592 * t5592;
        const Scalar t151 = t5588 * t5588;
        const Scalar t175 = t5604 * t5604;
        return  -t8 * (t5585 - 0.1e1) * t5602 / 0.7e1 - 0.8e1 / 0.7e1 * (t5591 * (-(t5584 - 0.1e1) * t5602 / 0.6e1 - l * t5585) + t5587 * (0.7e1 / 0.6e1 * (0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * ((((t5622 - l * t5579) * t5631 - t5601 * t5580) * t5624 - t5603 * t5581) * t5623 - t5604 * t5582) * t5602 - t5598 * t5583) * t5602 - t5606 * t5584) * t5602 - t5596 * t5585)) * t5594 * t5602 + ((t5592 * t5629 + 0.4e1 * t5593 * t5591) * ((-(t5583 - 0.1e1) * t5602 / 0.5e1 - l * t5584) * t5602 / 0.3e1 - t5601 * t5585) + (0.6e1 * t5588 * t5592 + (0.48e2 * t5593 * t5595 + 0.16e2 * t5590) * t5590) * ((0.3e1 / 0.5e1 * ((-(t5581 - 0.1e1) * t5602 / 0.3e1 - l * t5582) * t5627 - t5601 * t5583) * t5602 - t5603 * t5584) * t5625 - t5604 * t5585) + (t5588 * t5595 * t5628 + t5593 * t5630) * (0.5e1 / 0.6e1 * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * ((-(t5580 - 0.1e1) * t5602 / 0.2e1 - l * t5581) * t5625 - t5601 * t5582) * t5602 - t5603 * t5583) * t5602 - t5604 * t5584) * t5602 - t5598 * t5585) + (0.4e1 * t5587 * t5595 + t5588 * t5629) * ((((((-(t5579 - 0.1e1) * t5602 - l * t5580) * t5602 - t5601 * t5581) * t5602 - t5603 * t5582) * t5602 - t5604 * t5583) * t5602 - t5598 * t5584) * t5602 - t5606 * t5585) + (t5595 * t5630 + t5593 * t5592 * t5628) * ((0.2e1 / 0.5e1 * (-(t5582 - 0.1e1) * t5602 / 0.4e1 - l * t5583) * t5602 - t5601 * t5584) * t5627 - t5603 * t5585) + t151 * ((0.7e1 / 0.5e1 * ((0.5e1 / 0.3e1 * ((0.3e1 * ((l - t5622) * t5631 - t5601 * t5579) * t5602 - t5603 * t5580) * t5631 - t5604 * t5581) * t5602 - t5598 * t5582) * t5624 - t5606 * t5583) * t5602 - t5596 * t5584) * t5623 - t175 * t5585)) * t5602 / 0.7e1;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_segment_GradF<8> (Scalar l, Scalar d, const Scalar w[3], Scalar res[2])
    {
        const Scalar t5648 = d * l + 0.1e1;
        const Scalar t5641 = 0.1e1 / t5648;
        const Scalar t5668 = t5648 * t5648;
        const Scalar t2 = t5668 * t5668;
        const Scalar t5644 = 0.1e1 / t2;
        const Scalar t5669 = t5648 * t5668;
        const Scalar t3 = t5669 * t5669;
        const Scalar t5646 = 0.1e1 / t3;
        const Scalar t5655 = w[1];
        const Scalar t5651 = t5655 * t5655;
        const Scalar t5683 = 0.12e2 * t5651;
        const Scalar t5654 = w[2];
        const Scalar t5650 = t5654 * t5654;
        const Scalar t5682 = -0.6e1 / 0.7e1 * t5650;
        const Scalar t5656 = w[0];
        const Scalar t5653 = t5656 * t5656;
        const Scalar t5681 = -0.6e1 / 0.7e1 * t5653;
        const Scalar t5662 = 0.1e1 / d;
        const Scalar t5680 = t5662 / 0.2e1;
        const Scalar t5678 = 0.2e1 / 0.3e1 * t5662;
        const Scalar t5647 = t5641 * t5646;
        const Scalar t5661 = l * l;
        const Scalar t5663 = l * t5661;
        const Scalar t5666 = t5663 * t5663;
        const Scalar t5677 = t5666 * t5647;
        const Scalar t5664 = t5661 * t5661;
        const Scalar t5658 = l * t5664;
        const Scalar t5652 = t5656 * t5653;
        const Scalar t5649 = t5654 * t5650;
        const Scalar t5645 = t5641 * t5644;
        const Scalar t5643 = 0.1e1 / t5669;
        const Scalar t5642 = 0.1e1 / t5668;
        const Scalar t5640 = 0.3e1 * t5654 * t5653 + t5656 * t5683;
        const Scalar t5639 = 0.3e1 * t5650 * t5656 + t5654 * t5683;
        const Scalar t5638 = (-0.12e2 * t5654 * t5656 - 0.8e1 * t5651) * t5655;
        const Scalar t5637 = -(t5646 - 0.1e1) * t5662 / 0.6e1 - l * t5647;
        const Scalar t5636 = (-(t5645 - 0.1e1) * t5662 / 0.5e1 - l * t5646) * t5662 / 0.3e1 - t5661 * t5647;
        const Scalar t5635 = (0.2e1 / 0.5e1 * (-(t5644 - 0.1e1) * t5662 / 0.4e1 - l * t5645) * t5662 - t5661 * t5646) * t5680 - t5663 * t5647;
        const Scalar t5634 = (0.3e1 / 0.5e1 * ((-(t5643 - 0.1e1) * t5662 / 0.3e1 - l * t5644) * t5680 - t5661 * t5645) * t5662 - t5663 * t5646) * t5678 - t5664 * t5647;
        const Scalar t5633 = 0.5e1 / 0.6e1 * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * ((-(t5642 - 0.1e1) * t5662 / 0.2e1 - l * t5643) * t5678 - t5661 * t5644) * t5662 - t5663 * t5645) * t5662 - t5664 * t5646) * t5662 - t5658 * t5647;
        const Scalar t5632 = (((((-(t5641 - 0.1e1) * t5662 - l * t5642) * t5662 - t5661 * t5643) * t5662 - t5663 * t5644) * t5662 - t5664 * t5645) * t5662 - t5658 * t5646) * t5662 - t5677;
        res[0] = (-t5652 * (t5647 - 0.1e1) / 0.7e1 + t5640 * t5636 / 0.7e1 + t5638 * t5635 / 0.7e1 + t5639 * t5634 / 0.7e1 + t5649 * t5632 / 0.7e1 + (t5637 * t5681 + t5633 * t5682) * t5655) * t5662;
        const Scalar t113 = log(t5648);
        res[1] = (t5636 * t5681 + t5632 * t5682) * t5662 * t5655 + (t5652 * t5637 + t5638 * t5634 + t5639 * t5633 + t5649 * (0.7e1 / 0.6e1 * (0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * (0.4e1 / 0.3e1 * (0.3e1 / 0.2e1 * (0.2e1 * (t113 * t5662 - l * t5641) * t5662 - t5661 * t5642) * t5662 - t5663 * t5643) * t5662 - t5664 * t5644) * t5662 - t5658 * t5645) * t5662 - t5666 * t5646) * t5662 - l * t5677) + t5640 * t5635) * t5662 / 0.7e1;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_segment_FGradF<8> (Scalar l, Scalar d, const Scalar w[3], Scalar res[3])
    {
        const Scalar t5711 = w[1];
        const Scalar t5707 = t5711 * t5711;
        const Scalar t5758 = 0.12e2 / 0.7e1 * t5707;
        const Scalar t5703 = d * l + 0.1e1;
        const Scalar t5696 = 0.1e1 / t5703;
        const Scalar t5726 = t5703 * t5703;
        const Scalar t2 = t5726 * t5726;
        const Scalar t5699 = 0.1e1 / t2;
        const Scalar t5727 = t5703 * t5726;
        const Scalar t3 = t5727 * t5727;
        const Scalar t5701 = 0.1e1 / t3;
        const Scalar t5719 = 0.1e1 / d;
        const Scalar t5757 = 0.2e1 * t5719;
        const Scalar t5755 = 0.24e2 * t5707;
        const Scalar t5706 = t5711 * t5707;
        const Scalar t5710 = w[2];
        const Scalar t5712 = w[0];
        const Scalar t5738 = t5710 * t5712;
        const Scalar t5754 = -0.12e2 / 0.7e1 * t5711 * t5738 - 0.8e1 / 0.7e1 * t5706;
        const Scalar t5705 = t5710 * t5710;
        const Scalar t5741 = t5705 * t5712;
        const Scalar t5753 = 0.3e1 / 0.7e1 * t5741 + t5710 * t5758;
        const Scalar t5709 = t5712 * t5712;
        const Scalar t5739 = t5710 * t5709;
        const Scalar t5752 = 0.3e1 / 0.7e1 * t5739 + t5712 * t5758;
        const Scalar t5702 = t5696 * t5701;
        const Scalar t5751 = -t5702 / 0.7e1 + 0.1e1 / 0.7e1;
        const Scalar t5750 = -0.6e1 / 0.7e1 * t5705;
        const Scalar t5749 = -0.6e1 / 0.7e1 * t5709;
        const Scalar t5748 = t5719 / 0.2e1;
        const Scalar t5746 = 0.2e1 / 0.3e1 * t5719;
        const Scalar t5745 = 0.3e1 / 0.2e1 * t5719;
        const Scalar t5744 = 0.4e1 / 0.3e1 * t5719;
        const Scalar t14 = log(t5703);
        const Scalar t5743 = t14 * t5719;
        const Scalar t5697 = 0.1e1 / t5726;
        const Scalar t5698 = 0.1e1 / t5727;
        const Scalar t5700 = t5696 * t5699;
        const Scalar t5704 = t5710 * t5705;
        const Scalar t5718 = l * l;
        const Scalar t5720 = l * t5718;
        const Scalar t5723 = t5720 * t5720;
        const Scalar t5713 = l * t5723;
        const Scalar t5721 = t5718 * t5718;
        const Scalar t5715 = l * t5721;
        const Scalar t5742 = t5704 * (0.7e1 / 0.6e1 * (0.6e1 / 0.5e1 * (0.5e1 / 0.4e1 * ((((t5743 - l * t5696) * t5757 - t5718 * t5697) * t5745 - t5720 * t5698) * t5744 - t5721 * t5699) * t5719 - t5715 * t5700) * t5719 - t5723 * t5701) * t5719 - t5713 * t5702);
        const Scalar t5690 = -(t5701 - 0.1e1) * t5719 / 0.6e1 - l * t5702;
        const Scalar t5708 = t5712 * t5709;
        const Scalar t5740 = t5708 * t5690;
        const Scalar t5689 = (-(t5700 - 0.1e1) * t5719 / 0.5e1 - l * t5701) * t5719 / 0.3e1 - t5718 * t5702;
        const Scalar t5688 = (0.2e1 / 0.5e1 * (-(t5699 - 0.1e1) * t5719 / 0.4e1 - l * t5700) * t5719 - t5718 * t5701) * t5748 - t5720 * t5702;
        const Scalar t5687 = (0.3e1 / 0.5e1 * ((-(t5698 - 0.1e1) * t5719 / 0.3e1 - l * t5699) * t5748 - t5718 * t5700) * t5719 - t5720 * t5701) * t5746 - t5721 * t5702;
        const Scalar t5686 = 0.5e1 / 0.6e1 * (0.4e1 / 0.5e1 * (0.3e1 / 0.4e1 * ((-(t5697 - 0.1e1) * t5719 / 0.2e1 - l * t5698) * t5746 - t5718 * t5699) * t5719 - t5720 * t5700) * t5719 - t5721 * t5701) * t5719 - t5715 * t5702;
        const Scalar t5685 = (((((-(t5696 - 0.1e1) * t5719 - l * t5697) * t5719 - t5718 * t5698) * t5719 - t5720 * t5699) * t5719 - t5721 * t5700) * t5719 - t5715 * t5701) * t5719 - t5723 * t5702;
        const Scalar t112 = t5709 * t5709;
        const Scalar t134 = t5705 * t5705;
        const Scalar t158 = t5721 * t5721;
        res[0] = (t112 * t5751 + 0.32e2 / 0.7e1 * (-t5710 * t5686 - t5712 * t5688) * t5706 + (-0.8e1 / 0.7e1 * t5740 - 0.8e1 / 0.7e1 * t5742 - 0.24e2 / 0.7e1 * t5686 * t5741 - 0.24e2 / 0.7e1 * t5688 * t5739) * t5711) * t5719 + ((0.4e1 * t5704 * t5712 + t5705 * t5755) * t5685 + t134 * ((0.7e1 / 0.5e1 * ((0.5e1 / 0.3e1 * ((0.3e1 * ((l - t5743) * t5757 - t5718 * t5696) * t5719 - t5720 * t5697) * t5757 - t5721 * t5698) * t5719 - t5715 * t5699) * t5745 - t5723 * t5700) * t5719 - t5713 * t5701) * t5744 - t158 * t5702) + (0.6e1 * t5705 * t5709 + (0.48e2 * t5738 + 0.16e2 * t5707) * t5707) * t5687 + (t5709 * t5755 + 0.4e1 * t5710 * t5708) * t5689) * t5719 / 0.7e1;
        res[1] = (t5708 * t5751 + t5689 * t5752 + t5688 * t5754 + t5687 * t5753 + t5704 * t5685 / 0.7e1 + (t5690 * t5749 + t5686 * t5750) * t5711) * t5719;
        res[2] = (t5740 / 0.7e1 + t5688 * t5752 + t5687 * t5754 + t5686 * t5753 + t5742 / 0.7e1 + (t5689 * t5749 + t5685 * t5750) * t5711) * t5719;

    }
    
    template<>
    inline Scalar HornusCompactPolynomial_approx_segment_F<8> (Scalar l, Scalar d, Scalar q, const Scalar w[3])
    {
        const Scalar t5794 = q * d;
        const Scalar t5768 = t5794 + 0.1e1;
        const Scalar t1 = t5768 * t5768;
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t5766 = 0.1e1 / t3;
        const Scalar t5770 = w[2];
        const Scalar t5771 = w[1];
        const Scalar t5772 = w[0];
        const Scalar t5773 = l * l;
        const Scalar t5763 = t5770 * t5773 - 0.2e1 * t5771 * l + t5772;
        const Scalar t8 = t5771 * t5771;
        const Scalar t5764 = t5770 * t5772 - t8;
        const Scalar t5765 = t5770 * l - t5771;
        const Scalar t5776 = t5763 * t5763;
        const Scalar t5778 = t5776 * t5776;
        const Scalar t5784 = t5772 * t5772;
        const Scalar t5786 = t5784 * t5784;
        const Scalar t5789 = t5771 * t5784;
        const Scalar t5791 = t5765 * t5776;
        const Scalar t5769 = 0.1e1 / t5770;
        const Scalar t5792 = t5764 * t5769;
        const Scalar t5793 = 0.8e1 / 0.63e2 * (0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t5764 * l + t5765 * t5763 + t5771 * t5772) * t5792 + t5791 + t5789) * t5792 + t5763 * t5791 + t5772 * t5789) * t5792 + t5765 * t5778 / 0.9e1 + t5771 * t5786 / 0.9e1;
        const Scalar t5788 = t5769 * t5793;
        const Scalar t5767 = 0.1e1 / t5768 * t5766;
        const Scalar t5762 = t5763 * t5778;
        const Scalar t5760 = t5771 * t5788 + t5762 / 0.10e2 - t5772 * t5786 / 0.10e2;
        const Scalar t5759 = -l * t5762 + (-0.12e2 * t5771 * t5760 + t5772 * t5793) * t5769;
        const Scalar t70 = q * q;
        return  t5788 - 0.8e1 * d * t5760 * t5769 + (-0.9090909091e-1 * (0.3e1 * t5766 - 0.300e1 + 0.8e1 * (0.2e1 + t5767) * t5794) * t5759 - 0.8333333333e-1 * (0.2e1 - 0.200e1 * t5766 - 0.9e1 * (0.1e1 + t5767) * t5794) / q * (-0.1e1 * t5773 * t5762 + (0.1272727273e1 * t5771 * t5759 + 0.2e1 * t5772 * t5760) * t5769)) * t5769 / t70;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_approx_segment_GradF<8> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[2])
    {
        const Scalar t1 = q * q;
        const Scalar t5812 = 0.1e1 / t1;
        const Scalar t5836 = q * d;
        const Scalar t5807 = t5836 + 0.1e1;
        const Scalar t2 = t5807 * t5807;
        const Scalar t3 = t2 * t2;
        const Scalar t4 = t3 * t3;
        const Scalar t5805 = 0.1e1 / t4;
        const Scalar t5809 = w[2];
        const Scalar t5810 = w[1];
        const Scalar t5811 = w[0];
        const Scalar t5814 = l * l;
        const Scalar t5802 = t5809 * t5814 - 0.2e1 * t5810 * l + t5811;
        const Scalar t9 = t5810 * t5810;
        const Scalar t5803 = t5809 * t5811 - t9;
        const Scalar t5804 = t5809 * l - t5810;
        const Scalar t5825 = t5811 * t5811;
        const Scalar t5830 = t5810 * t5825;
        const Scalar t5818 = t5802 * t5802;
        const Scalar t5831 = t5804 * t5818;
        const Scalar t5808 = 0.1e1 / t5809;
        const Scalar t5832 = t5803 * t5808;
        const Scalar t5835 = 0.6e1 / 0.35e2 * (0.4e1 / 0.3e1 * (0.2e1 * t5803 * l + t5804 * t5802 + t5810 * t5811) * t5832 + t5831 + t5830) * t5832 + t5802 * t5831 / 0.7e1 + t5811 * t5830 / 0.7e1;
        const Scalar t5806 = 0.1e1 / t5807 * t5805;
        const Scalar t5834 = (0.3e1 * t5805 - 0.300e1 + 0.8e1 * (0.2e1 + t5806) * t5836) * t5812;
        const Scalar t5833 = (0.2e1 - 0.200e1 * t5805 - 0.8e1 * (0.1e1 + t5806) * t5836) / q * t5812;
        const Scalar t5801 = t5818 * t5818;
        const Scalar t5829 = t5814 * t5801;
        const Scalar t41 = t5825 * t5825;
        const Scalar t5797 = t5810 * t5808 * t5835 + t5801 / 0.8e1 - t41 / 0.8e1;
        const Scalar t5796 = -l * t5801 + (-0.10e2 * t5810 * t5797 + t5811 * t5835) * t5808;
        const Scalar t5795 = -t5829 + (0.4e1 / 0.3e1 * t5810 * t5796 + 0.2e1 * t5811 * t5797) * t5808;
        const Scalar t5828 = t5795 / 0.10e2;
        res[0] = (t5835 - 0.8e1 * d * t5797 - t5796 * t5834 / 0.9e1 - t5828 * t5833) * t5808;
        res[1] = (t5797 + 0.8e1 / 0.9e1 * d * t5796 - t5828 * t5834 + (l * t5829 - (0.7e1 / 0.5e1 * t5810 * t5795 - t5811 * t5796 / 0.3e1) * t5808) * t5833 / 0.11e2) * t5808;

    }
    
    
    template<>
    inline void HornusCompactPolynomial_approx_segment_FGradF<8> (Scalar l, Scalar d, Scalar q, const Scalar w[3], Scalar res[3])
    {
        const Scalar t1 = q * q;
        const Scalar t5859 = 0.1e1 / t1;
        const Scalar t5891 = q * d;
        const Scalar t5853 = t5891 + 0.1e1;
        const Scalar t2 = t5853 * t5853;
        const Scalar t3 = t2 * t2;
        const Scalar t4 = t3 * t3;
        const Scalar t5851 = 0.1e1 / t4;
        const Scalar t5858 = w[0];
        const Scalar t5890 = 0.2e1 * t5858;
        const Scalar t5856 = w[2];
        const Scalar t5854 = 0.1e1 / t5856;
        const Scalar t5889 = d * t5854;
        const Scalar t5857 = w[1];
        const Scalar t5861 = l * l;
        const Scalar t5848 = t5856 * t5861 - 0.2e1 * t5857 * l + t5858;
        const Scalar t9 = t5857 * t5857;
        const Scalar t5849 = t5856 * t5858 - t9;
        const Scalar t5850 = t5856 * l - t5857;
        const Scalar t5873 = t5858 * t5858;
        const Scalar t5882 = t5857 * t5873;
        const Scalar t5865 = t5848 * t5848;
        const Scalar t5883 = t5850 * t5865;
        const Scalar t5884 = t5849 * t5854;
        const Scalar t5843 = 0.6e1 / 0.5e1 * (0.4e1 / 0.3e1 * (0.2e1 * t5849 * l + t5850 * t5848 + t5857 * t5858) * t5884 + t5883 + t5882) * t5884 + t5848 * t5883 + t5858 * t5882;
        const Scalar t5867 = t5865 * t5865;
        const Scalar t5875 = t5873 * t5873;
        const Scalar t5888 = 0.8e1 / 0.63e2 * t5843 * t5884 + t5850 * t5867 / 0.9e1 + t5857 * t5875 / 0.9e1;
        const Scalar t5887 = t5843 / 0.7e1;
        const Scalar t5852 = 0.1e1 / t5853 * t5851;
        const Scalar t5886 = (0.3e1 * t5851 - 0.300e1 + 0.8e1 * (0.2e1 + t5852) * t5891) * t5859;
        const Scalar t5885 = (0.2e1 - 0.200e1 * t5851 - 0.8e1 * (0.1e1 + t5852) * t5891) / q * t5859;
        const Scalar t5881 = t5861 * t5867;
        const Scalar t5842 = t5857 * t5854 * t5887 + t5867 / 0.8e1 - t5875 / 0.8e1;
        const Scalar t5839 = -l * t5867 + (-0.10e2 * t5857 * t5842 + t5858 * t5887) * t5854;
        const Scalar t5837 = -t5881 + (0.4e1 / 0.3e1 * t5857 * t5839 + t5842 * t5890) * t5854;
        const Scalar t5880 = t5837 / 0.10e2;
        const Scalar t5879 = t5854 * t5886;
        const Scalar t5878 = t5854 * t5885;
        const Scalar t5877 = t5854 * t5888;
        const Scalar t5846 = t5848 * t5867;
        const Scalar t5840 = t5857 * t5877 + t5846 / 0.10e2 - t5858 * t5875 / 0.10e2;
        const Scalar t5838 = -l * t5846 + (-0.12e2 * t5857 * t5840 + t5858 * t5888) * t5854;
        res[0] = t5877 - 0.8e1 * t5840 * t5889 - t5838 * t5879 / 0.11e2 - (-t5861 * t5846 + (0.14e2 / 0.11e2 * t5857 * t5838 + t5840 * t5890) * t5854) * t5878 / 0.12e2;
        res[1] = (t5887 - 0.8e1 * d * t5842 - t5839 * t5886 / 0.9e1 - t5880 * t5885) * t5854;
        res[2] = t5842 * t5854 + 0.8e1 / 0.9e1 * t5839 * t5889 - t5879 * t5880 - (-l * t5881 + (0.7e1 / 0.5e1 * t5857 * t5837 - t5858 * t5839 / 0.3e1) * t5854) * t5878 / 0.11e2;

    }
    
    



} // Close namespace HornusCompactPolynomialOptim
} // Close namespace convol
} // Close namespace expressive

#endif // CONVOL_HORNUS_COMPACT_POLYNOMIAL_OPTIMIZED_FUNCTIONS_H_

