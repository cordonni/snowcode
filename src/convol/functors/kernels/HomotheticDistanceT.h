/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: HomotheticDistance.h

   Language: C++

   License: Convol Licence

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for distance functor.
                ie this functor used with a scalar field primitive
                creates a field by using the distance to the primitive
                (Can be a special distance) and a Real function applied
                to this distance.
                This correspond to the classical distance fields.

   Platform Dependencies: None
*/


#pragma once
#ifndef CONVOL_HOMOTHETIC_DISTANCE_H_
#define CONVOL_HOMOTHETIC_DISTANCE_H_

#include<core/functor/MACRO_FUNCTOR_TYPE.h>
#include<core/geometry/OptWeightedSegment.h>

#include <convol/ConvolRequired.h>
#include <convol/functors/kernels/Kernel.h>
    //TODO:@todo : if extern template obliged to include this in .h I would prefer avoid it ...
    #include <convol/functors/kernels/compactpolynomial/CompactPolynomial.h>

namespace expressive {

namespace convol {

class CONVOL_API AbstractHomotheticDistance : public Kernel
{
public:
    EXPRESSIVE_MACRO_NAME("HomotheticDistance")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractHomotheticDistance() {}
    AbstractHomotheticDistance(const AbstractHomotheticDistance& rhs) : Kernel(rhs) {}
    virtual ~AbstractHomotheticDistance(){}

    //////////////////////
    /// Type functions ///
    //////////////////////

    virtual const core::FunctorTypeTree& Type() const = 0;

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool isCompact() const = 0;

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    //-------------------------------------------------------------------------
    void EvalHomotheticValues(const core::OptWeightedSegment& /*seg*/, const Point& /*point*/,
                              Scalar& /*homothetic_value_res*/, Vector& /*homothetic_grad_res*/) const
    {
        assert(false &&  "Function not implemented !");
    }
    //-------------------------------------------------------------------------

    /////////////////
    /// Modifiers ///
    /////////////////

//    virtual void set_functor_profil(const Functor& profil) = 0;

    /////////////////
    /// Accessors ///
    /////////////////

    virtual const Functor& functor_profil() const = 0;

    void AddXmlAttributes(TiXmlElement *element) const;
};

template< typename TFunctorKernel>
class HomotheticDistanceT :
    public AbstractHomotheticDistance
{
public:

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////
    HomotheticDistanceT() : AbstractHomotheticDistance(), profil_() {}
    HomotheticDistanceT(const TFunctorKernel& profil)
        : AbstractHomotheticDistance(), profil_(profil) {}
    HomotheticDistanceT(const HomotheticDistanceT& rhs)
        : AbstractHomotheticDistance(rhs), profil_(rhs.profil_) {}
    virtual ~HomotheticDistanceT(){}

    //////////////////////
    /// Type functions ///
    //////////////////////

    static core::FunctorTypeTree BuildStaticType();

    //-------------------------------------------------------------------------
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defined functions :
    //-------------------------------------------------------------------------
    // static const FunctorTypeTree& StaticType();
    //-------------------------------------------------------------------------
    // virtual const FunctorTypeTree& Type() const;

    static const std::vector<std::string>& StaticSubFunctorXmlName();

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    //-------------------------------------------------------------------------
    // Segment evaluation -----------------------------------------------------
    //-------------------------------------------------------------------------
    Scalar Eval(const core::OptWeightedSegment& seg, const Point& point) const;
    //-------------------------------------------------------------------------
    inline Scalar operator() (const core::OptWeightedSegment& seg_source, const Point& point) const
    {
        return Eval(seg_source, point);
    }
    //-------------------------------------------------------------------------
    Vector EvalGrad(const core::OptWeightedSegment& seg, const Point& point, Scalar epsilon) const;
    //-------------------------------------------------------------------------
    void EvalValueAndGrad(const core::OptWeightedSegment& seg, const Point& point, Scalar epsilon_grad,
                          Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------


    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool isCompact() const { return profil_.isCompact(); }

    //-------------------------------------------------------------------------
    inline Scalar GetEuclidDistBound(const core::OptWeightedSegment& seg, Scalar value) const
    {
        return profil_.InverseFct(value*profil_.Eval(1.0))*seg.WeightMax();
    }
    inline Scalar GetEuclidDistBound1D(Scalar /*weight*/, Scalar /*value*/) const
    {
        assert(false && "Function to be implemented !");
        return 1.0;
    }

    Scalar GetIsoValueForThickness(const Scalar /*length*/, const Scalar /*thickness*/) const
    {
        assert(false && "Function to be implemented !");
        return 1.0;
    }

    /////////////////
    /// Accessors ///
    /////////////////

    virtual const Functor& functor_profil() const { return profil_; }
    const TFunctorKernel& profil() const  { return profil_; }
//    TFunctorKernel& profil() { return profil_; }

private:
    TFunctorKernel profil_;
};


template< typename TFunctorKernel>
bool operator==(const HomotheticDistanceT<TFunctorKernel>& a, const HomotheticDistanceT<TFunctorKernel>& b);

//TODO:@todo : we can typedef the most useful one


} // Close namespace convol
} // Close namespace expressive


#ifndef _WIN32
#ifndef TEMPLATE_INSTANTIATION_HOMOTHETIC_DISTANCE
    extern template class expressive::convol::HomotheticDistanceT<expressive::convol::CompactPolynomial<6> >;
#endif
#endif

#endif // CONVOL_HOMOTHETIC_DISTANCE_H_

/*
    //////////////////////////
    // Evaluation functions //
    //////////////////////////

    //-------------------------------------------------------------------------
    // Scalar evaluation : none since it does not mean anything
    //-------------------------------------------------------------------------

    // Point evaluation -------------------------------------------------------
    //-------------------------------------------------------------------------
    inline Scalar Eval(const WeightedPoint& p_source, const Point& point) const
    {
        Scalar dist = (p_source.position() - point).norm();
        return profil_(dist/p_source.weight())/profil_.Eval(1.0);
    }
    //-------------------------------------------------------------------------
    inline Scalar operator() (const WeightedPoint& p_source, const Point& point) const
    {
        return Eval(p_source, point);
    }
    //-------------------------------------------------------------------------
    inline Vector EvalGrad(const WeightedPoint& p_source, const Point& point, const Scalar epsilon) const
    {
        Vector grad_res;
        const Point& pos = p_source.position();
        Scalar dist = (pos - point).norm();
        for(unsigned int i=0; i<Vector::size_; ++i){
            grad_res[i] = pos[i]-point[i];
        }
        return (profil_.EvalGrad(dist/p_source.weight(), epsilon)/ (p_source.weight()*dist))*grad_res /profil_.Eval(1.0);
    }
    //-------------------------------------------------------------------------
    inline void EvalValueAndGrad(const WeightedPoint& p_source, const Point& point, const Scalar epsilon_grad,
        Scalar& value_res, Vector& grad_res) const
    { 
        const Point& pos = p_source.position();
        Scalar dist = (pos - point).norm();
        for(unsigned int i=0; i<Vector::size_; ++i){
            grad_res[i] = pos[i]-point[i];
        }
        Scalar d_over_w = dist/p_source.weight();
        Scalar prof_grad_res = profil_.EvalGrad(d_over_w, epsilon_grad);
        value_res = profil_.Eval(d_over_w)/profil_.Eval(1.0);
        grad_res = (prof_grad_res/ (p_source.weight()*dist))*grad_res/profil_.Eval(1.0);
    }
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    void EvalHomotheticValues(const WeightedPoint& p_source, const Point& point,
                              Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
    {
        assert(false);
        UNUSED(point); UNUSED(p_source);
        homothetic_value_res = 0.0;
        homothetic_grad_res.vectorize(0.0);
    }
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    // Triangle evaluation ----------------------------------------------------
    //-------------------------------------------------------------------------
    inline Scalar operator() (const WeightedTriangle& triangle_source,const Point& point) const
    {
        return Eval(triangle_source, point);
    }
    //-------------------------------------------------------------------------
    inline Scalar Eval(const WeightedTriangle& triangle_source,const Point& point) const
    {
        // First compute the distance to the triangle and find the nearest point

        // Code taken from EuclideanDistance functor, can be optimized.
        Point nearest_point;
        Vector p1_to_p = point - triangle_source.p1();
        //Scalar p1_to_p_dot_tri_n = (p1_to_p | triangle_source.unit_normal());
        //Point proj = point - p1_to_p_dot_tri_n*triangle_source.unit_normal();

        /////////////////////////////////////////////////////////
        // non Orthogonal projection to handle non uniform weights //
        // Note : does not work well, see the new code below
//        Scalar sign_fact = p1_to_p_dot_tri_n > 0.0 ? 1.0 : -1.0;
//        Point p1_weight_plane = triangle_source.p1() + sign_fact*triangle_source.weight_p1()*triangle_source.unit_normal();
//        Point p2_weight_plane = triangle_source.p2() + sign_fact*triangle_source.weight_p2()*triangle_source.unit_normal();
//        Point p3_weight_plane = triangle_source.p3() + sign_fact*triangle_source.weight_p3()*triangle_source.unit_normal();
//        Vector weight_plane_normal = (p2_weight_plane -p1_weight_plane) % (p3_weight_plane - p1_weight_plane);
//        Point non_ortho_proj = point -((p1_to_p|triangle_source.unit_normal())/(weight_plane_normal|triangle_source.unit_normal()))*weight_plane_normal;
        //////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////
        // We must generalize the principle used for the segment

        // So we first find the direction of maximum weight variation.
        // SHOULD BE PRECOMPUTED !!!
        Scalar weight_1, weight_2, weight_max;
        Point point_1, point_2, point_max;
        if (triangle_source.weight_p1() > triangle_source.weight_p2())
        {
            if (triangle_source.weight_p1() > triangle_source.weight_p3())
            {
                point_max = triangle_source.p1();
                weight_max = triangle_source.weight_p1();

                point_1 = triangle_source.p2();
                weight_1 = triangle_source.weight_p2();

                point_2 = triangle_source.p3();
                weight_2 = triangle_source.weight_p3();
            }
            else
            {
                point_max = triangle_source.p3();
                weight_max = triangle_source.weight_p3();

                point_1 = triangle_source.p1();
                weight_1 = triangle_source.weight_p1();

                point_2 = triangle_source.p2();
                weight_2 = triangle_source.weight_p2();
            }
        }
        else
        {
            if (triangle_source.weight_p2() > triangle_source.weight_p3())
            {
                point_max = triangle_source.p2();
                weight_max = triangle_source.weight_p2();

                point_1 = triangle_source.p3();
                weight_1 = triangle_source.weight_p3();

                point_2 = triangle_source.p1();
                weight_2 = triangle_source.weight_p1();
            }
            else
            {
                point_max = triangle_source.p3();
                weight_max = triangle_source.weight_p3();

                point_1 = triangle_source.p1();
                weight_1 = triangle_source.weight_p1();

                point_2 = triangle_source.p2();
                weight_2 = triangle_source.weight_p2();
            }
        }

        const Vector dir_1 = point_1 - point_max;
        const Vector dir_2 = point_2 - point_max;

        const Scalar delta_1 = weight_1 - weight_max;
        const Scalar delta_2 = weight_2 - weight_max;

        Vector ortho_dir, main_dir, proj_dir;
        Point point_iso_zero;                   // will be a point at 0 weight to find Z later.
        const Scalar numerical_eps = 0.000001;  // threshold under which we consider weights to be equals, this is to avoid numerical unstability (at the cost of accuracy)
        bool equal_weights = false;             // Use later to skip computations if we are in this specific case
        if(abs(delta_1) <= numerical_eps)
        {
            if(abs(delta_2) <= numerical_eps){  // The 3 weights are close to each other, we project orthogonally
                proj_dir = triangle_source.unit_normal();
                equal_weights = true;
            }else{

                // we need a point at 0 weight to find Z later  (see above)
                Scalar coord_iso_zero_dir = - weight_max / delta_2;
                point_iso_zero = point_max + coord_iso_zero_dir*dir_2;

                ortho_dir = dir_1;
                ortho_dir.normalize();
            }
        }
        else if(abs(delta_2) <= numerical_eps)
        {
            // we need a point at 0 weight to find Z later  (see above)
            Scalar coord_iso_zero_dir = - weight_max / delta_1;
            point_iso_zero = point_max + coord_iso_zero_dir*dir_1;

            ortho_dir = dir_2;
            ortho_dir.normalize();  // should be useless
        }
        else
        {
            // find the point were weight equal zero along the two edges that leave from point_max
            Scalar coord_iso_zero_dir1 = - weight_max / delta_1;
            point_iso_zero = point_max + coord_iso_zero_dir1*dir_1;
            Scalar coord_iso_zero_dir2 = - weight_max / delta_2;
            Point point_iso_zero_bis = point_max + coord_iso_zero_dir2*dir_2;

            // along ortho_dir the weights are const
            ortho_dir = point_iso_zero_bis - point_iso_zero;
            ortho_dir.normalize();  // should be useless
        }

        if(!equal_weights){
            // direction of fastest variation of weight
            main_dir = ortho_dir %  triangle_source.unit_normal();
            main_dir.normalize(); // should be useless

            // Now look for the point equivalent to the Z point for the segment.
            // This point Z is the intersection of 3 orthogonal planes :
            //      plane 1 : triangle plane
            //      plane 2 : n = ortho_dir, passing through point
            //      plane 3 : n = main_dir, passing through point_iso_zero_dir1 and point_iso_zero_dir2
            // Formula for a unique intersection of 3 planes : http://geomalgorithms.com/a05-_intersect-1.html
            //  Plane equation from a normal n and a point p0 : <n.(x,y,z)> - <n.p0> = 0
            //
            // this formula can probably be optimized :
            //        - some elements can be stored
            //        - some assertion are verified and may help to simplify the computation, for example : n3 = n2%n1 
            const Vector& n1 = triangle_source.unit_normal();
            const Vector& n2 = ortho_dir;
            const Vector& n3 = main_dir;
            const Scalar d1 = -(triangle_source.p1() | n1);
            const Scalar d2 = -(point                | n2);
            const Scalar d3 = -(point_iso_zero  | n3);
            Point Z = (-d1*(n2%n3)-d2*(n3%n1)-d3*(n1%n2))/(n1|(n2%n3));

            // Now we want to project in the direction orthogonal to (pZ) and ortho_dir
            Vector pz = Z-point;
            
            // set proj_dir
            proj_dir = pz%ortho_dir;
            proj_dir.normalize(); // should be useless
        }
        
        // Project along the given direction
        Point non_ortho_proj = point -((p1_to_p|triangle_source.unit_normal())/(proj_dir|triangle_source.unit_normal()))*proj_dir;

        Scalar res;
        if( ((triangle_source.unit_p1_to_p2() % (non_ortho_proj - triangle_source.p1())) | triangle_source.unit_normal()) > 0.0
            && ((triangle_source.unit_p2_to_p3() % (non_ortho_proj - triangle_source.p2())) | triangle_source.unit_normal()) > 0.0
            && ((triangle_source.unit_p3_to_p1() % (non_ortho_proj - triangle_source.p3())) | triangle_source.unit_normal()) > 0.0)
        {
            res = (point - non_ortho_proj).sqrnorm();
            nearest_point = non_ortho_proj;

            // get barycentric coordinates of nearest_point (which is necessarily in the triangle
            Vector n = (triangle_source.p2() - triangle_source.p1()) % (triangle_source.p3() - triangle_source.p1());
            Vector n1 = (triangle_source.p3() - triangle_source.p2()) % (non_ortho_proj - triangle_source.p2());
            Vector n2 = (triangle_source.p1() - triangle_source.p3()) % (non_ortho_proj - triangle_source.p3());
            Vector n3 = (triangle_source.p2() - triangle_source.p1()) % (non_ortho_proj - triangle_source.p1());

            Scalar nsq = n.sqrnorm();
            Scalar a1 = (n|n1)/nsq;
            Scalar a2 = (n|n2)/nsq;
            Scalar a3 = (n|n3)/nsq;

            Scalar inter_weight = a1*triangle_source.weight_p1()+a2*triangle_source.weight_p2()+a3*triangle_source.weight_p3();

            return profil_(sqrt(res)/inter_weight)/profil_.Eval(1.0);
        }
        else
        {
            // do the same as for a segment on all triangle sides
            Point tmp_proj_to_point, proj_to_point;
            Scalar tmp_weight_proj, weight_proj;
            Scalar tmp_sqrdist, sqrdist;
            Scalar tmp_ratio, ratio;

            GenericSegmentComputation(  point,
                                        triangle_source.p1(), 
                                        triangle_source.norm_p1_to_p2()*triangle_source.unit_p1_to_p2(), 
                                        triangle_source.norm_p1_to_p2(), 
                                        triangle_source.norm_p1_to_p2()*triangle_source.norm_p1_to_p2(),
                                        triangle_source.weight_p1(), triangle_source.weight_p2()-triangle_source.weight_p1(),
                                        proj_to_point,
                                        weight_proj
                                        );

            sqrdist = proj_to_point.sqrnorm();
            ratio = sqrdist/(weight_proj*weight_proj);

            GenericSegmentComputation(  point,
                                        triangle_source.p2(), 
                                        triangle_source.norm_p2_to_p3()*triangle_source.unit_p2_to_p3(), 
                                        triangle_source.norm_p2_to_p3(), 
                                        triangle_source.norm_p2_to_p3()*triangle_source.norm_p2_to_p3(),
                                        triangle_source.weight_p2(), triangle_source.weight_p3()-triangle_source.weight_p2(),
                                        tmp_proj_to_point,
                                        tmp_weight_proj
                                        );
            tmp_sqrdist = tmp_proj_to_point.sqrnorm();
            tmp_ratio = tmp_sqrdist/(tmp_weight_proj*tmp_weight_proj);
            if(ratio>tmp_ratio){
                sqrdist       = tmp_sqrdist;
                proj_to_point = tmp_proj_to_point;
                weight_proj   = tmp_weight_proj;
                ratio = tmp_ratio;
            }

            GenericSegmentComputation(  point,
                                        triangle_source.p3(), 
                                        triangle_source.norm_p3_to_p1()*triangle_source.unit_p3_to_p1(), 
                                        triangle_source.norm_p3_to_p1(), 
                                        triangle_source.norm_p3_to_p1()*triangle_source.norm_p3_to_p1(),
                                        triangle_source.weight_p3(), triangle_source.weight_p1()-triangle_source.weight_p3(),
                                        tmp_proj_to_point,
                                        tmp_weight_proj
                                        );
            tmp_sqrdist = tmp_proj_to_point.sqrnorm();
            tmp_ratio = tmp_sqrdist/(tmp_weight_proj*tmp_weight_proj);
            if(ratio>tmp_ratio){
                sqrdist       = tmp_sqrdist;
                proj_to_point = tmp_proj_to_point;
                weight_proj   = tmp_weight_proj;
                ratio = tmp_ratio;
            }

            return profil_.Eval(sqrt(sqrdist)/weight_proj)/profil_.Eval(1.0);
            
        }
    }
    // Used in triangle evaluation 
    inline void GenericSegmentComputation(const Point& point,
                                     const Point& p1, 
                                     const Vector& p1p2, 
                                     const Scalar& length, 
                                     const Scalar& sqr_length,
                                     const Scalar& weight_1,
                                     const Scalar& delta_weight, // = weight_2-weight_1
                                     Point& res_proj_to_point,
                                     Scalar& res_weight_proj
                                     ) const
    {
        UNUSED(length);
        const Vector origin_to_p = point - p1;

        const Scalar orig_p_scal_dir = (origin_to_p | p1p2);
        const Scalar orig_p_sqr = origin_to_p.sqrnorm();

        Scalar denum = sqr_length * weight_1 + orig_p_scal_dir * delta_weight;
        Scalar t = (delta_weight<0.0) ? 0.0 : 1.0;
        if(denum > 0.0)
        {
            t = (orig_p_scal_dir * weight_1 + orig_p_sqr * delta_weight) /denum;
            t = (t<0.0) ? 0.0 : ((t>1.0) ? 1.0 : t) ; // clipping (nearest point on segment not line)
        }

        res_proj_to_point = t*p1p2 - origin_to_p;
        res_weight_proj = weight_1 + t*delta_weight;
    }


    //-------------------------------------------------------------------------
    inline Vector EvalGrad(const WeightedTriangle& triangle_source, const Point& point, const Scalar epsilon) const
    {
        return GradientEvaluationT<Traits>::NumericalGradient6points(*this, triangle_source, point, epsilon);
    }
    //-------------------------------------------------------------------------
    inline void EvalValueAndGrad(const WeightedTriangle& triangle_source, const Point& point, const Scalar epsilon_grad,
                                    Scalar& value_res, Vector& grad_res) const
    {
        grad_res = EvalGrad(triangle_source, point, epsilon_grad)/profil_.Eval(1.0);
        value_res = Eval(triangle_source, point)/profil_.Eval(1.0);
    }
    //-------------------------------------------------------------------------
    inline void EvalHomotheticValues(const WeightedTriangle& triangle_source, const Point& point,
                                     Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
    {
        assert(false);
        UNUSED(point); UNUSED(triangle_source);
        homothetic_value_res = 0.0;
        homothetic_grad_res.vectorize(0.0);
    }

    ////////////////////////
    // Bounding functions //
    ////////////////////////

    // Check the formula
    Scalar GetEuclidDistBound (const WeightedPoint& p, const Scalar value) const
    {
        UNUSED(p);
        return profil_.Inverse(value*profil_.Eval(1.0))*p.weight();
    }

    //-------------------------------------------------------------------------
    Scalar GetEuclidDistBound (const WeightedTriangle& triangle, const Scalar value) const
    {
        UNUSED(triangle);
        Scalar w_max = triangle.weight_p1() > triangle.weight_p2() ? triangle.weight_p1() : triangle.weight_p2();
        if(w_max<triangle.weight_p3()){
            w_max = triangle.weight_p3();
        }
        return profil_.Inverse(value*profil_.Eval(1.0))*w_max;
    }

    ///////////////
    // Modifiers //
    ///////////////
    void set_profil (const TFunctorProfilR1toR1& profil)
    {
        profil_ = profil;
    }
    // Same as above but virtual
    virtual void set_functor_profil(const Functor& profil)
    { 
        const TFunctorProfilR1toR1* spec_profil = dynamic_cast<const TFunctorProfilR1toR1*>(&profil);
        if(spec_profil != NULL){// The functor given must have the good class.
            profil_ = *spec_profil;
        }else{
            assert(false);
        }
    }
*/



#include <convol/functors/kernels/HomotheticDistanceT.hpp>
