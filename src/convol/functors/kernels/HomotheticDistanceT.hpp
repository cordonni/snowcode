
#pragma once
#ifndef CONVOL_HOMOTHETIC_DISTANCE_HPP_
#define CONVOL_HOMOTHETIC_DISTANCE_HPP_

#include <convol/functors/kernels/HomotheticDistanceT.h>

namespace expressive {

namespace convol {

template<typename TFunctorKernel>
core::FunctorTypeTree HomotheticDistanceT<TFunctorKernel>::BuildStaticType()
{
    std::vector<core::FunctorTypeTree> vec(1,TFunctorKernel::BuildStaticType());
    return core::FunctorTypeTree("HomotheticDistance", vec);
}

template<typename TFunctorKernel>
const std::vector<std::string>& HomotheticDistanceT<TFunctorKernel>::StaticSubFunctorXmlName()
{
    static std::vector<std::string> subfunctor_xml_names = { std::string("TFunctorKernel") };
    return subfunctor_xml_names;
}

//-------------------------------------------------------------------------
// Segment evaluation -----------------------------------------------------
//-------------------------------------------------------------------------
template<typename TFunctorKernel>
Scalar HomotheticDistanceT<TFunctorKernel>::Eval(const core::OptWeightedSegment& seg, const Point& point) const
{
    //TODO:@todo : could be optimized
    return profil_.Eval(seg.HomotheticDistance(point)) / profil_.Eval(1.0);
}
//-------------------------------------------------------------------------
template<typename TFunctorKernel>
Vector HomotheticDistanceT<TFunctorKernel>::EvalGrad(const core::OptWeightedSegment& seg, const Point& point, Scalar epsilon) const
{
    const Vector p_min_to_point = point-seg.p_min();
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();

    Scalar h_dist = 0; Vector h_grad = Vector::Zero();
    seg.HomotheticDistanceAndGrad(p_min_to_point, uv, d2, h_dist, h_grad);

    //TODO:@todo : could be optimized
    return (profil_.EvalGrad(h_dist, epsilon)/profil_.Eval(1.0)) * h_grad;
}
//-------------------------------------------------------------------------
template<typename TFunctorKernel>
void HomotheticDistanceT<TFunctorKernel>::EvalValueAndGrad(const core::OptWeightedSegment& seg, const Point& point, Scalar epsilon_grad,
                      Scalar& value_res, Vector& grad_res) const
{
    const Vector p_min_to_point = point-seg.p_min();
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();

    Scalar h_dist = 0; Vector h_grad = Vector::Zero();
    seg.HomotheticDistanceAndGrad(p_min_to_point, uv, d2, h_dist, h_grad);

    //TODO:@todo : could be optimized
    Scalar norma = 1.0/profil_.Eval(1.0);
    value_res = norma * profil_.Eval(h_dist);
    grad_res = (norma * profil_.EvalGrad(h_dist, epsilon_grad)) * h_grad;
}
//-------------------------------------------------------------------------

template< typename TFunctorKernel>
bool operator==(const HomotheticDistanceT<TFunctorKernel>& a, const HomotheticDistanceT<TFunctorKernel>& b)
{
    return a.profil() == b.profil();
}

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_HOMOTHETIC_DISTANCE_HPP_


