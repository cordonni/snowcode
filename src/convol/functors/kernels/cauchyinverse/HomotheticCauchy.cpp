#include <convol/functors/kernels/cauchyinverse/HomotheticCauchy.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

static core::FunctorFactory::Type<HomotheticCauchy<3> > HomotheticCauchy3Type;
static core::FunctorFactory::Type<HomotheticCauchy<4> > HomotheticCauchy4Type;
static core::FunctorFactory::Type<HomotheticCauchy<5> > HomotheticCauchy5Type;
static core::FunctorFactory::Type<HomotheticCauchy<6> > HomotheticCauchy6Type;

} // Close namespace convol

} // Close namespace expressive
