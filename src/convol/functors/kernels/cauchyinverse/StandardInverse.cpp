#include <convol/functors/kernels/cauchyinverse/StandardInverse.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

static core::FunctorFactory::Type<StandardInverse<3> > StandardInverse3Type;
static core::FunctorFactory::Type<StandardInverse<4> > StandardInverse4Type;
static core::FunctorFactory::Type<StandardInverse<5> > StandardInverse5Type;
static core::FunctorFactory::Type<StandardInverse<6> > StandardInverse6Type;

} // Close namespace convol

} // Close namespace expressive
