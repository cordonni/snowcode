#include<convol/functors/kernels/cauchyinverse/HomotheticCauchyAndInverseOptimizedFunctions.h>
#include<convol/functors/kernels/cauchyinverse/StandardCauchyAndInverseOptimizedFunctions.h>

namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////

//-------------------------------------------------------------------------
// Point evaluation -------------------------------------------------------
//-------------------------------------------------------------------------
template <int DEGREE>
Scalar HomotheticInverse<DEGREE>::Eval(const WeightedPoint& p_source, const Point& point) const
{
    const Scalar aux = this->scale_*p_source.weight() / ((point-p_source).norm());
    Scalar int_power_aux  = kernelHelpFunction::IntPow<DEGREE>(aux);

    return this->normalization_factor_0D_*int_power_aux;
}
//-------------------------------------------------------------------------
template <int DEGREE>
Vector HomotheticInverse<DEGREE>::EvalGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon*/) const
{
    const Vector direction = point - p_source;
    const Scalar dist = direction.norm();

    const Scalar aux = this->scale_*p_source.weight() / dist;
    Scalar int_power_aux  = kernelHelpFunction::IntPow<DEGREE+1>(aux);

    return (-this->degree_r()*this->normalization_factor_0D_*int_power_aux/(this->scale_*p_source.weight())) * direction;
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticInverse<DEGREE>::EvalValueAndGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon_grad*/,
                                            Scalar& value_res, Vector& grad_res) const
{
    const Vector direction = point - p_source;
    const Scalar dist = direction.norm();

    const Scalar aux = this->scale_*p_source.weight() / dist;
    Scalar int_power_aux  = kernelHelpFunction::IntPow<DEGREE>(aux);

    value_res = p_source.weight()*int_power_aux;
    grad_res = (-this->degree_r()*this->normalization_factor_0D_*int_power_aux*aux/(this->scale_*p_source.weight())) * direction;
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticInverse<DEGREE>::EvalHomotheticValues(const WeightedPoint& p_source, const Point& point,
                                                Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
    // WARNING : this is in fact Inverse kernel of degree i-1

    Vector direction = point - p_source;
    const Scalar dist = direction.norm();

    const Scalar aux = p_source.weight() / dist;
    Scalar int_power_aux  = kernelHelpFunction::IntPow<DEGREE-1>(aux);

    homothetic_value_res = int_power_aux;
    homothetic_grad_res  = (-(this->degree_r()-1.0)*int_power_aux*aux/dist ) * direction;
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
// Segment evaluation -----------------------------------------------------
//-------------------------------------------------------------------------
template <int DEGREE>
Scalar HomotheticInverse<DEGREE>::Eval(const OptWeightedSegment& seg, const Point& point) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = this->inv_scale_2_*seg.length()*seg.length();
    const Scalar b = this->inv_scale_2_*seg.length()*(seg.increase_unit_dir().dot(p_min_to_point));
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE>(special_coeff, seg.weight_min(), delta_w);

    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 ) {
        return  this->normalization_factor_1D_ * seg.length() * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F<DEGREE>(a,b,c,special_coeff);
    } else {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0) {
            return this->normalization_factor_1D_ * seg.length()*HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_on_line<DEGREE>(a,b,special_coeff);
        } else {
            return expressive::kScalarMax;
        }
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
Vector HomotheticInverse<DEGREE>::EvalGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = this->inv_scale_2_*seg.length()*seg.length();
    const Scalar b = this->inv_scale_2_*seg.length()*(seg.increase_unit_dir().dot(p_min_to_point));
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE>(special_coeff, seg.weight_min(), delta_w);


    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 ) {
        Scalar F0F1[2];
        HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_GradF<DEGREE>(a,b,c,special_coeff, F0F1);

        const Scalar factor = seg.length() * this->degree_r() * this->inv_scale_2_;
        return this->normalization_factor_1D_ * ((factor*F0F1[1]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point);
    } else {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0) {
            Scalar F0F1[2];
            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_GradF_on_line<DEGREE>(a,b,special_coeff, F0F1);

            const Scalar factor = seg.length() * this->degree_r() * this->inv_scale_2_;
            return this->normalization_factor_1D_ * ((factor*F0F1[1]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point);
        } else {
            return Vector::Zero(); //the gradient is undefined
        }
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticInverse<DEGREE>::EvalValueAndGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                                            Scalar& value_res, Vector& grad_res) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = this->inv_scale_2_*seg.length()*seg.length();
    const Scalar b = this->inv_scale_2_*seg.length()*(seg.increase_unit_dir().dot(p_min_to_point));
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE>(special_coeff, seg.weight_min(), delta_w);

    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 ) {
        Scalar F0F1F2[3];
        HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF<DEGREE>(a,b,c,special_coeff, F0F1F2);

        value_res = this->normalization_factor_1D_* seg.length() * F0F1F2[0];
        const Scalar factor = this->normalization_factor_1D_ * seg.length() * this->degree_r() * this->inv_scale_2_;
        grad_res = ((factor*F0F1F2[2]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);
    } else {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0) {
            Scalar F0F1F2[3];
            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF_on_line<DEGREE>(a,b,special_coeff, F0F1F2);

            value_res = this->normalization_factor_1D_ * seg.length() * F0F1F2[0];
            const Scalar factor = seg.length() * this->degree_r() * this->inv_scale_2_;
            grad_res = this->normalization_factor_1D_ * ((factor*F0F1F2[2]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);
        } else {
            value_res = expressive::kScalarMax;
            grad_res = Vector::Zero(); //the gradient is undefined
        }
    }
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

// Two following functions compute integral for constant weight along a segment represented
// by some base parameters (can be useful when "segments" are created on the fly such as in SemiNumericalKernel)
template <int DEGREE>
Scalar HomotheticInverse<DEGREE>::Eval( const Point& p_1, Scalar w_1,
                                   const Vector& unit_dir, Scalar length,
                                   const Point& point) const
{
    const Vector p1_to_point = point-p_1;
    const Scalar a = this->inv_scale_2_;
    const Scalar b = this->inv_scale_2_ * unit_dir.dot(p1_to_point);
    const Scalar c = this->inv_scale_2_ * p1_to_point.squaredNorm();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar pow_weight = w_1;
    Scalar int_power_pow_weight = kernelHelpFunction::IntPow<DEGREE-1>(pow_weight);

    // Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        return int_power_pow_weight * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_cste<DEGREE>(a,b,c, length);
    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            return int_power_pow_weight * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_on_line_cste<DEGREE>(a,b, length);
        }
        else
        {
            return expressive::kScalarMax;
        }
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticInverse<DEGREE>::EvalValueAndGrad( const Point& p_1, Scalar w_1,
                                             const Vector& unit_dir, Scalar length,
                                             const Point& point, Scalar /*epsilon_grad*/,
                                             Scalar& value_res, Vector& grad_res) const
{
    const Vector p1_to_point = point-p_1;
    const Scalar a = this->inv_scale_2_;
    const Scalar b = this->inv_scale_2_ * unit_dir.dot(p1_to_point);
    const Scalar c = this->inv_scale_2_ * p1_to_point.squaredNorm();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar pow_weight = w_1;
    Scalar int_power_pow_weight = kernelHelpFunction::IntPow<3>(pow_weight);

    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        Scalar F0F1F2[3];
        HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF_cste<DEGREE>(a,b,c, length, F0F1F2);

        value_res = int_power_pow_weight * F0F1F2[0];
        const Scalar factor = int_power_pow_weight * this->degree_r() * this->inv_scale_2_;
        grad_res = ((factor*F0F1F2[2]) * unit_dir - (factor*F0F1F2[1]) * p1_to_point);
    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            Scalar F0F1F2[3];
            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF_on_line_cste<DEGREE>(a,b, length, F0F1F2);

            value_res = int_power_pow_weight * F0F1F2[0];
            const Scalar factor = int_power_pow_weight * this->degree_r() * this->inv_scale_2_;
            grad_res = ((factor*F0F1F2[2]) * unit_dir - (factor*F0F1F2[1]) * p1_to_point);
        }
        else
        {
            value_res = expressive::kScalarMax;
            grad_res = Vector::Zero(); //the gradient is undefined
        }
    }
}

//-------------------------------------------------------------------------
// Segment with Density evaluation ------------------------------------------
//-------------------------------------------------------------------------
//    Scalar HomotheticInverse4::Eval(const WeightedSegmentWithDensity& seg, const Point& point) const
//    {
//        const Vector p_min_to_point = (point-seg.opt_w_seg().p_min());
//        const Scalar a = this->inv_scale_2_*seg.opt_w_seg().length()*seg.opt_w_seg().length();
//        const Scalar b = this->inv_scale_2_*seg.opt_w_seg().length()*(seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point));
//        const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm();

//        // Compute the new polynomial weight (see formulas in the ref)
//        Scalar delta_w = seg.opt_w_seg().unit_delta_weight() * seg.opt_w_seg().length();
//        Scalar special_coeff[5];
//        const Scalar t8421 = delta_w * delta_w;
//        const Scalar t8427 = t8421 * seg.opt_w_seg().weight_min();
//        const Scalar t8423 = seg.opt_w_seg().weight_min() * seg.opt_w_seg().weight_min();
//        const Scalar t8426 = t8423 * delta_w;
//        const Scalar t8422 = seg.opt_w_seg().weight_min() * t8423;
//        const Scalar t8420 = seg.density_coeff(0);
//        const Scalar t8419 = seg.density_coeff(1);
//        special_coeff[0] = t8420 * t8422;
//        special_coeff[1] = t8419 * t8422 + 0.3e1 * t8420 * t8426;
//        special_coeff[2] = 0.3e1 * t8419 * t8426 + 0.3e1 * t8420 * t8427;
//        special_coeff[3] = 0.3e1 * t8419 * t8427 + t8420 * delta_w * t8421;


//        //  Manage cases when point is on the line defined by seg.opt_w_seg().
//        const Scalar delta = a*c - b*b;
//        if( delta > 0.0 )
//        {
//            return  normalization_factor_1D_ * seg.length() * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_i4(a,b,c,special_coeff);


//        }
//        else
//        {
//            // Check whether the evaluation point is on the line segment or not
//            const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
//            if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
//            {
//            return normalization_factor_1D_ * seg.length()*HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_on_line_i4(a,b,special_coeff);


//            }
//            else
//            {
//                return expressive::kScalarMax;
//            }
//        }
//    }
//    //-------------------------------------------------------------------------
//    Vector HomotheticInverse4::EvalGrad(const WeightedSegmentWithDensity& seg, const Point& point, Scalar /*epsilon*/) const
//    {
//        const Vector p_min_to_point = (point-seg.opt_w_seg().p_min());
//        const Scalar a = this->inv_scale_2_*seg.opt_w_seg().length()*seg.opt_w_seg().length();
//        const Scalar b = this->inv_scale_2_*seg.opt_w_seg().length()*(seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point));
//        const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm();

//        // Compute the new polynomial weight (see formulas in the ref)
//        Scalar delta_w = seg.opt_w_seg().unit_delta_weight() * seg.opt_w_seg().length();
//        Scalar special_coeff[5];
//        const Scalar t8430 = delta_w * delta_w;
//        const Scalar t8436 = t8430 * seg.opt_w_seg().weight_min();
//        const Scalar t8432 = seg.opt_w_seg().weight_min() * seg.opt_w_seg().weight_min();
//        const Scalar t8435 = t8432 * delta_w;
//        const Scalar t8431 = seg.opt_w_seg().weight_min() * t8432;
//        const Scalar t8429 = seg.density_coeff(0);
//        const Scalar t8428 = seg.density_coeff(1);
//        special_coeff[0] = t8429 * t8431;
//        special_coeff[1] = t8428 * t8431 + 0.3e1 * t8429 * t8435;
//        special_coeff[2] = 0.3e1 * t8428 * t8435 + 0.3e1 * t8429 * t8436;
//        special_coeff[3] = 0.3e1 * t8428 * t8436 + t8429 * delta_w * t8430;


//        //  Manage cases when point is on the line defined by seg.opt_w_seg().
//        const Scalar delta = a*c - b*b;
//        if( delta > 0.0 )
//        {
//            Scalar F0F1[2];
//            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_GradF_i4(a,b,c,special_coeff, F0F1);

//            const Scalar factor = seg.length()*4.0*this->inv_scale_2_;
//            return normalization_factor_1D_ * ((factor*F0F1[1]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point);


//        }
//        else
//        {
//            // Check whether the evaluation point is on the line segment or not
//            const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
//            if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
//            {
//            Scalar F0F1[2];
//            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_GradF_on_line_i4(a,b,special_coeff, F0F1);

//            const Scalar factor = seg.length()*4.0*this->inv_scale_2_;
//            return normalization_factor_1D_ * ((factor*F0F1[1]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point);


//            }
//            else
//            {
//                return Vector(0.0);
//            }
//        }
//    }
//    //-------------------------------------------------------------------------
//    void HomotheticInverse4::EvalValueAndGrad(const WeightedSegmentWithDensity& seg, const Point& point, Scalar /*epsilon_grad*/,
//                                                Scalar& value_res, Vector& grad_res) const
//    {
//        const Vector p_min_to_point = (point-seg.opt_w_seg().p_min());
//        const Scalar a = this->inv_scale_2_*seg.opt_w_seg().length()*seg.opt_w_seg().length();
//        const Scalar b = this->inv_scale_2_*seg.opt_w_seg().length()*(seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point));
//        const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm();

//        // Compute the new polynomial weight (see formulas in the ref)
//        Scalar delta_w = seg.opt_w_seg().unit_delta_weight() * seg.opt_w_seg().length();
//        Scalar special_coeff[5];
//        const Scalar t8439 = delta_w * delta_w;
//        const Scalar t8445 = t8439 * seg.opt_w_seg().weight_min();
//        const Scalar t8441 = seg.opt_w_seg().weight_min() * seg.opt_w_seg().weight_min();
//        const Scalar t8444 = t8441 * delta_w;
//        const Scalar t8440 = seg.opt_w_seg().weight_min() * t8441;
//        const Scalar t8438 = seg.density_coeff(0);
//        const Scalar t8437 = seg.density_coeff(1);
//        special_coeff[0] = t8438 * t8440;
//        special_coeff[1] = t8437 * t8440 + 0.3e1 * t8438 * t8444;
//        special_coeff[2] = 0.3e1 * t8437 * t8444 + 0.3e1 * t8438 * t8445;
//        special_coeff[3] = 0.3e1 * t8437 * t8445 + t8438 * delta_w * t8439;


//        //  Manage cases when point is on the line defined by seg.opt_w_seg().
//        const Scalar delta = a*c - b*b;
//        if( delta > 0.0 )
//        {
//            Scalar F0F1F2[3];
//            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF_i4(a,b,c,special_coeff, F0F1F2);

//            value_res = normalization_factor_1D_* seg.length() * F0F1F2[0];
//            const Scalar factor = normalization_factor_1D_ * seg.length() * 4.0*this->inv_scale_2_;
//            grad_res = ((factor*F0F1F2[2]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);


//        }
//        else
//        {
//            // Check whether the evaluation point is on the line segment or not
//            const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
//            if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
//            {
//            Scalar F0F1F2[3];
//            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF_on_line_i4(a,b,special_coeff, F0F1F2);

//            value_res = normalization_factor_1D_ * seg.length() * F0F1F2[0];
//            const Scalar factor = seg.length()*4.0*this->inv_scale_2_;
//            grad_res = normalization_factor_1D_ * ((factor*F0F1F2[2]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);


//            }
//            else
//            {
//                value_res = expressive::kScalarMax;
//                grad_res = Vector::Zero(); //TODO : @todo ?
//            }
//        }
//    }
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
// Clipped Segment evaluation ( for Witnessed Operator) -------------------
//-------------------------------------------------------------------------
//    Scalar HomotheticInverse4::EvalClipped(const WeightedSegmentWithDensity& seg, const Sphere& clipping_sphere) const
//    {
//        const Vector p_min_to_point = (clipping_sphere.center_-seg.opt_w_seg().p_min());
//        Scalar special_coeff[3] = { - this->inv_scale_2_ * p_min_to_point.squaredNorm() ,
//                                    - this->inv_scale_2_ * seg.opt_w_seg().length()*(seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point)) ,
//                                    - this->inv_scale_2_ * seg.opt_w_seg().length()*seg.opt_w_seg().length() };

//        Scalar delta_w = seg.opt_w_seg().unit_delta_weight() * seg.opt_w_seg().length();

//        const Scalar radius_sqr = this->inv_scale_2_ * clipping_sphere.radius_ * clipping_sphere.radius_;
//        const Scalar w0_sqr = seg.opt_w_seg().weight_min()*seg.opt_w_seg().weight_min();
//        const Scalar w0_w1 = -seg.opt_w_seg().weight_min()*delta_w;
//        const Scalar w1_sqr = delta_w*delta_w;
//        Scalar clipping_coeff[3] = {radius_sqr * w0_sqr + special_coeff[0] ,
//                                    radius_sqr * w0_w1 + special_coeff[1] ,
//                                    radius_sqr * w1_sqr + special_coeff[2] };

//        Scalar clipped_l1, clipped_l2;
//        if(seg.opt_w_seg().HomotheticClipping(clipping_coeff, clipped_l1, clipped_l2))
//        {
//            const Scalar a = - special_coeff[2];
//            const Scalar b = - special_coeff[1];
//            const Scalar c = - special_coeff[0];

//            // Compute the new polynomial weight (see formulas in the ref) : this could be precomputed to improve efficiency
//            Scalar numerator_coeff[4];
//            const Scalar t8448 = delta_w * delta_w;
//            const Scalar t8446 = seg.opt_w_seg().weight_min() * seg.opt_w_seg().weight_min();
//            numerator_coeff[0] = seg.opt_w_seg().weight_min() * t8446;
//            numerator_coeff[1] = 0.3e1 * t8446 * delta_w;
//            numerator_coeff[2] = 0.3e1 * seg.opt_w_seg().weight_min() * t8448;
//            numerator_coeff[3] = delta_w * t8448;


//            const Scalar delta = a*c - b*b;
//            if( delta > 0.0 )
//            {
//                return  seg.length() * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_i4(clipped_l1,clipped_l2,a,b,c,numerator_coeff);


//            }
//            else
//            {
//                // Check whether the evaluation point is on the line segment or not
//                const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
//                if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
//                {
//                        return seg.length()*HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_on_line_i4(clipped_l1,clipped_l2,a,b,numerator_coeff);


//                }
//                else
//                {
//                    return expressive::kScalarMax;
//                }
//            }
//        }
//        else
//        {
//           return 0.0;
//        }
//    }
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticInverse<DEGREE>::EvalValueAndGradClipped(const WeightedSegmentWithDensity& seg, const Sphere& clipping_sphere, Scalar /*epsilon_grad*/,
                                                   Scalar& value_res, Vector& grad_res) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();
    assert(false);
    UNUSED(seg); UNUSED(clipping_sphere);
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
// Special Segment evaluation ( for Gradient-based Integral Surfaces) -----
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticInverse<DEGREE>::EvalHomotheticValues(const OptWeightedSegment& seg, const Point& point,
                                                Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = this->inv_scale_2_*seg.length()*seg.length();
    const Scalar b = this->inv_scale_2_*seg.length()*(seg.increase_unit_dir().dot(p_min_to_point));
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE>(special_coeff, seg.weight_min(), delta_w);

    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        Scalar F0F1F2[3];
        HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_ScalisF_HornusGradF<DEGREE>(a,b,c,
                                                                                                delta_w, seg.weight_min(),
                                                                                                special_coeff, F0F1F2);

        homothetic_value_res   = this->normalization_factor_1D_ * seg.length() * F0F1F2[0];

        const Scalar factor = this->normalization_factor_1D_ * seg.length() * this->degree_r() *this->inv_scale_2_;
        homothetic_grad_res = (((factor*F0F1F2[2]) * seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);
    //TODO:@todo : see if it is possible to apply the method directly on squared value (avoid a sqrt)
    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            Scalar F0F1F2[3];
            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line<DEGREE>(a, b,
                                                                                                            delta_w, seg.weight_min(),
                                                                                                            special_coeff, F0F1F2);

            homothetic_value_res   = this->normalization_factor_1D_ * seg.length() * F0F1F2[0];

            const Scalar factor = this->normalization_factor_1D_ * seg.length() * this->degree_r() * this->inv_scale_2_;
            homothetic_grad_res = (((factor*F0F1F2[2]) * seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);
        }
        else
        {
            homothetic_value_res = expressive::kScalarMax;
            homothetic_grad_res = Vector::Zero(); //the gradient is undefined
        }
    }
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticInverse<DEGREE>::EvalHomotheticValues( const Point& p_1, Scalar w_1,
                                                 const Vector& unit_dir, Scalar length,
                                                 const Point& point,
                                                 Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
    // WARNING : since this function is used during triangles computation only,
    // the optimized function use in fact a degree i+1 instead of i
    // TODO:@todo : this isn't a clean way to proceed ... !

    const Vector p1_to_point = point-p_1;
    const Scalar a = this->inv_scale_2_;
    const Scalar b = this->inv_scale_2_ * unit_dir.dot(p1_to_point);
    const Scalar c = this->inv_scale_2_ * p1_to_point.squaredNorm();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar pow_weight = w_1;
    Scalar int_power_pow_weight = kernelHelpFunction::IntPow<DEGREE>(pow_weight);

    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        Scalar F0F1F2[3];
        HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_ScalisF_HornusGradF_cste<DEGREE>(a,b,c, length, F0F1F2);

        homothetic_value_res = int_power_pow_weight * F0F1F2[0];
        const Scalar factor = w_1 * int_power_pow_weight * this->degree_r() * this->inv_scale_2_;
        homothetic_grad_res = ((factor*F0F1F2[2]) * unit_dir - (factor*F0F1F2[1]) * p1_to_point);
    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            Scalar F0F1F2[3];
            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line_cste<DEGREE>(a, b, length, F0F1F2);

            homothetic_value_res = int_power_pow_weight * F0F1F2[0];
            const Scalar factor = w_1 * int_power_pow_weight * this->degree_r() * this->inv_scale_2_;
            homothetic_grad_res = ((factor*F0F1F2[2]) * unit_dir - (factor*F0F1F2[1]) * p1_to_point);
        }
        else
        {
            homothetic_value_res = expressive::kScalarMax;
            homothetic_grad_res = Vector::Zero(); //the gradient is undefined
        }
    }
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
template <int DEGREE>
Scalar HomotheticInverse<DEGREE>::Eval(const CsteWeightedSegmentWithDensity& seg, const Point& point) const
{
    return seg.density() * this->normalization_factor_1D_
           * Eval(seg.p1(), seg.weight(), seg.unit_dir(), seg.length(), point);
}
//-------------------------------------------------------------------------
template <int DEGREE>
Vector HomotheticInverse<DEGREE>::EvalGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar epsilon) const
{
    Scalar f_tmp; Vector grad_res;
    EvalValueAndGrad(seg.p1(), seg.weight(),
                     seg.unit_dir(), seg.length(),
                     point, epsilon, f_tmp, grad_res);
    return (seg.density() * this->normalization_factor_1D_)*grad_res;
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticInverse<DEGREE>::EvalValueAndGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar epsilon_grad,
                      Scalar& value_res, Vector& grad_res) const
{
    EvalValueAndGrad(seg.p1(), seg.weight(),
                     seg.unit_dir(), seg.length(),
                     point, epsilon_grad, value_res, grad_res);
    Scalar factor = seg.density() * this->normalization_factor_1D_;
    value_res *= factor;
    grad_res *= factor;
}
//-------------------------------------------------------------------------


//////////////////////////
/// Bounding functions ///
//////////////////////////

template <int DEGREE>
Scalar HomotheticInverse<DEGREE>::GetIsoValueForThickness(Scalar length, Scalar thickness) const
{
    std::vector<Scalar> weight;
    weight.push_back(1.0);

    const Scalar a = this->inv_scale_2_*length*length;
    const Scalar b = a*0.5;
    const Scalar c = this->inv_scale_2_*thickness*thickness + a*0.25;

    return this->normalization_factor_1D_*length*StandardCauchyAndInverseOptim::StandardCauchyInverse_segment_F<DEGREE,0>(a,b,c,weight);
}


} // Close namespace convol

} // Close namespace expressive
