/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: HornusCauchyAndInverseOptimizedFunctions.h
  
   Language: C++
  
   License: Convol Licence
  
   \author: Cedric Zanni 
   E-Mail: cedric.zanni@inria.fr
  
   Description: Header files defining optimized function for Hornus CauchyX ans InverseX kernels.
 
   Platform Dependencies: None 
*/  

#pragma once
#ifndef CONVOL_HORNUS_CAUCHY_AND_INVERSE_OPTIMIZED_FUNCTIONS_H_
#define CONVOL_HORNUS_CAUCHY_AND_INVERSE_OPTIMIZED_FUNCTIONS_H_

// core dependencies
#include <core/CoreRequired.h>

// std dependencies
#include<vector>
#include<array>

#include<math.h>

namespace expressive {

namespace convol {

namespace HornusCauchyAndInverseOptim {

////////////////////////////
/// Function declaration ///
////////////////////////////

template<int I>
inline Scalar HornusCauchyInverse_segment_F (Scalar a, Scalar b, Scalar c, const Scalar (&w)[I+1]);
template<int I>
inline void HornusCauchyInverse_segment_GradF (Scalar a, Scalar b, Scalar c, const Scalar (&w)[I+1], Scalar (&res)[2]);
template<int I>
inline void HornusCauchyInverse_segment_FGradF (Scalar a, Scalar b, Scalar c, const Scalar (&w)[I+1], Scalar (&res)[3]);

template<int I>
inline Scalar HornusCauchyInverse_segment_F_on_line (Scalar a, Scalar b, const Scalar (&w)[I+1]);
template<int I>
inline void HornusCauchyInverse_segment_GradF_on_line (Scalar a, Scalar b, const Scalar (&w)[I+1], Scalar (&res)[2]);
template<int I>
inline void HornusCauchyInverse_segment_FGradF_on_line (Scalar a, Scalar b, const Scalar (&w)[I+1], Scalar (&res)[3]);

///////////////////////////////
/// Template specialization ///
///////////////////////////////





    /////////////////////////////////
    // Code for kernel of degree 3 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HornusCauchyInverse_segment_F<3> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[4])
    {
        const Scalar t2083 = a - b;
        const Scalar t2077 = a - 0.2e1 * b + c;
        const Scalar t2076 = sqrt(0.1e1 / t2077);
        const Scalar t2078 = sqrt(0.1e1 / c);
        const Scalar t2079 = a * c;
        const Scalar t7 = b * b;
        const Scalar t2081 = (t2083 * t2076 + b * t2078) / (t2079 - t7);
        const Scalar t2073 = b * t2081 - t2076 + t2078;
        const Scalar t2080 = 0.1e1 / a;
        const Scalar t2082 = t2073 * t2080;
        const Scalar t13 = sqrt(a * t2077);
        const Scalar t15 = sqrt(t2079);
        const Scalar t19 = log((t13 + t2083) / (-b + t15));
        const Scalar t20 = sqrt(a);
        const Scalar t2072 = b * t2082 + t19 / t20 - t2076;
        return  w[0] * t2081 + w[1] * t2082 + w[2] * t2072 * t2080 - w[3] * (-t2076 + (-0.3e1 * b * t2072 + 0.2e1 * c * t2073) * t2080) * t2080;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_GradF<3> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[4], Scalar (&res)[2])
    {
        const Scalar t2097 = a - b;
        const Scalar t2094 = a - 0.2e1 * b + c;
        const Scalar t2093 = 0.1e1 / t2094;
        const Scalar t2105 = sqrt(t2093);
        const Scalar t2091 = t2105 * t2093;
        const Scalar t2098 = a * c;
        const Scalar t2 = b * b;
        const Scalar t2090 = 0.1e1 / (t2098 - t2);
        const Scalar t2103 = 0.1e1 / c;
        const Scalar t2106 = sqrt(t2103);
        const Scalar t2095 = t2106 * t2103;
        const Scalar t2110 = (t2097 * t2105 + b * t2106) * t2090;
        const Scalar t2109 = (0.2e1 * a * t2110 + t2097 * t2091 + b * t2095) * t2090 / 0.3e1;
        const Scalar t2089 = -t2091 / 0.3e1;
        const Scalar t2086 = b * t2109 + t2089 + t2095 / 0.3e1;
        const Scalar t2104 = 0.1e1 / a;
        const Scalar t2111 = t2086 * t2104;
        const Scalar t2085 = b * t2111 + c * t2109 - t2091;
        const Scalar t2112 = t2085 / 0.2e1;
        const Scalar t2102 = w[0];
        const Scalar t2101 = w[1];
        const Scalar t2100 = w[2];
        const Scalar t2099 = w[3];
        const Scalar t2084 = -t2091 + (-b * t2085 / 0.2e1 + 0.2e1 * c * t2086) * t2104;
        res[0] = t2102 * t2109 + (t2101 * t2086 + t2100 * t2112 + t2099 * t2084) * t2104;
        const Scalar t37 = sqrt(a * t2094);
        const Scalar t39 = sqrt(t2098);
        const Scalar t43 = log((t37 + t2097) / (-b + t39));
        const Scalar t44 = sqrt(a);
        res[1] = t2102 * t2111 + t2101 * t2104 * t2112 + t2100 * t2084 * t2104 + t2099 * (t2089 + (b * t2084 + t43 / t44 - t2105 + b * (b * t2110 - t2105 + t2106) * t2104) * t2104) * t2104;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_FGradF<3> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[4], Scalar (&res)[3])
    {
        const Scalar t2147 = 0.2e1 * c;
        const Scalar t2128 = a - b;
        const Scalar t2125 = a - 0.2e1 * b + c;
        const Scalar t2124 = 0.1e1 / t2125;
        const Scalar t2136 = sqrt(t2124);
        const Scalar t2122 = t2136 * t2124;
        const Scalar t2129 = a * c;
        const Scalar t2 = b * b;
        const Scalar t2121 = 0.1e1 / (t2129 - t2);
        const Scalar t2134 = 0.1e1 / c;
        const Scalar t2137 = sqrt(t2134);
        const Scalar t2126 = t2137 * t2134;
        const Scalar t2144 = (t2128 * t2136 + b * t2137) * t2121;
        const Scalar t2140 = (0.2e1 * a * t2144 + t2128 * t2122 + b * t2126) * t2121 / 0.3e1;
        const Scalar t2120 = -t2122 / 0.3e1;
        const Scalar t2116 = b * t2140 + t2120 + t2126 / 0.3e1;
        const Scalar t2135 = 0.1e1 / a;
        const Scalar t2145 = t2116 * t2135;
        const Scalar t2114 = b * t2145 + c * t2140 - t2122;
        const Scalar t2146 = t2114 / 0.2e1;
        const Scalar t2130 = w[3];
        const Scalar t2143 = t2130 * t2135;
        const Scalar t2131 = w[2];
        const Scalar t2142 = t2131 * t2135;
        const Scalar t2132 = w[1];
        const Scalar t2141 = t2132 * t2135;
        const Scalar t2133 = w[0];
        const Scalar t2118 = b * t2144 - t2136 + t2137;
        const Scalar t22 = sqrt(a * t2125);
        const Scalar t24 = sqrt(t2129);
        const Scalar t28 = log((t22 + t2128) / (-b + t24));
        const Scalar t29 = sqrt(a);
        const Scalar t2115 = b * t2118 * t2135 + t28 / t29 - t2136;
        const Scalar t2113 = -t2122 + (-b * t2114 / 0.2e1 + t2116 * t2147) * t2135;
        res[0] = t2133 * t2144 + t2118 * t2141 + t2115 * t2142 - (-t2136 + (-0.3e1 * b * t2115 + t2118 * t2147) * t2135) * t2143;
        res[1] = t2133 * t2140 + (t2132 * t2116 + t2131 * t2146 + t2130 * t2113) * t2135;
        res[2] = t2133 * t2145 + t2141 * t2146 + t2113 * t2142 + (t2120 + (b * t2113 + t2115) * t2135) * t2143;

    }
    
    template<>
    inline Scalar HornusCauchyInverse_segment_F_on_line<3> (Scalar a, Scalar b, const Scalar (&w)[4])
    {
        const Scalar t2157 = 0.1e1 / a;
        const Scalar t2167 = b * t2157;
        const Scalar t2154 = a - b;
        const Scalar t1 = fabs(t2154);
        const Scalar t2 = t1 * t1;
        const Scalar t2151 = 0.1e1 / t2 / t1;
        const Scalar t4 = fabs(b);
        const Scalar t5 = t4 * t4;
        const Scalar t2152 = 0.1e1 / t5 / t4;
        const Scalar t10 = fabs(t2154 * t2151 + b * t2152);
        const Scalar t2166 = t10 / 0.2e1;
        const Scalar t11 = t2154 * t2154;
        const Scalar t2164 = t11 * t2151;
        const Scalar t2158 = sqrt(a);
        const Scalar t2163 = t2158 * t2164;
        const Scalar t2156 = b * b;
        const Scalar t2149 = (b * t2166 - t2164 + t2156 * t2152) * t2158;
        const Scalar t18 = log(-t2154 / b);
        const Scalar t19 = fabs(t18);
        const Scalar t2148 = t2149 * t2167 + t19 / t2158 - t2163;
        const Scalar t30 = a * a;
        return  w[0] * t2158 * t2166 + (w[1] * t2149 + w[2] * t2148 + (-0.2e1 * t2156 * t2149 / t30 + t2163 + 0.3e1 * t2148 * t2167) * w[3]) * t2157;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_GradF_on_line<3> (Scalar a, Scalar b, const Scalar (&w)[4], Scalar (&res)[2])
    {
        const Scalar t2180 = a - b;
        const Scalar t2175 = fabs(t2180);
        const Scalar t1 = t2175 * t2175;
        const Scalar t2206 = 0.1e1 / t1;
        const Scalar t2179 = fabs(b);
        const Scalar t2 = t2179 * t2179;
        const Scalar t2205 = 0.1e1 / t2;
        const Scalar t2173 = 0.1e1 / t2175 * t2206;
        const Scalar t2176 = 0.1e1 / t2179 * t2205;
        const Scalar t2174 = t2206 * t2173;
        const Scalar t2177 = t2205 * t2176;
        const Scalar t8 = fabs(t2180 * t2174 + b * t2177);
        const Scalar t2204 = t8 / 0.4e1;
        const Scalar t2189 = sqrt(a);
        const Scalar t2181 = t2189 * a;
        const Scalar t2187 = b * b;
        const Scalar t2178 = t2180 * t2180;
        const Scalar t2199 = t2178 * t2174;
        const Scalar t2197 = -t2199 / 0.3e1;
        const Scalar t2171 = (b * t2204 + t2197 + t2187 * t2177 / 0.3e1) * t2181;
        const Scalar t2198 = t2181 * t2199;
        const Scalar t2188 = 0.1e1 / a;
        const Scalar t2201 = t2171 * t2188;
        const Scalar t2170 = b * t2201 + t2187 * t2189 * t2204 - t2198;
        const Scalar t2202 = t2170 * t2188;
        const Scalar t20 = a * a;
        const Scalar t2169 = -b * t2202 / 0.2e1 + 0.2e1 * t2187 * t2171 / t20 - t2198;
        const Scalar t2203 = t2169 * t2188;
        const Scalar t2200 = t2178 * t2173;
        const Scalar t2186 = w[0];
        const Scalar t2185 = w[1];
        const Scalar t2184 = w[2];
        const Scalar t2183 = w[3];
        res[0] = t2186 * t2181 * t2204 + (t2185 * t2171 + t2184 * t2170 / 0.2e1 + t2183 * t2169) * t2188;
        const Scalar t40 = fabs(t2180 * t2173 + b * t2176);
        const Scalar t50 = log(-t2180 / b);
        const Scalar t51 = fabs(t50);
        res[1] = t2186 * t2201 + t2185 * t2202 / 0.2e1 + t2184 * t2203 + t2183 * (b * t2203 + (b * (b * t40 / 0.2e1 - t2200 + t2187 * t2176) * t2189 * t2188 + t51 / t2189 - t2189 * t2200) * t2188 + t2181 * t2197) * t2188;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_FGradF_on_line<3> (Scalar a, Scalar b, const Scalar (&w)[4], Scalar (&res)[3])
    {
        const Scalar t2221 = a - b;
        const Scalar t2216 = fabs(t2221);
        const Scalar t1 = t2216 * t2216;
        const Scalar t2251 = 0.1e1 / t1;
        const Scalar t2220 = fabs(b);
        const Scalar t2 = t2220 * t2220;
        const Scalar t2250 = 0.1e1 / t2;
        const Scalar t2214 = 0.1e1 / t2216 * t2251;
        const Scalar t2217 = 0.1e1 / t2220 * t2250;
        const Scalar t8 = fabs(t2221 * t2214 + b * t2217);
        const Scalar t2249 = t8 / 0.2e1;
        const Scalar t2215 = t2251 * t2214;
        const Scalar t2218 = t2250 * t2217;
        const Scalar t12 = fabs(t2221 * t2215 + b * t2218);
        const Scalar t2248 = t12 / 0.4e1;
        const Scalar t2228 = b * b;
        const Scalar t2231 = sqrt(a);
        const Scalar t2222 = t2231 * a;
        const Scalar t2219 = t2221 * t2221;
        const Scalar t2244 = t2219 * t2215;
        const Scalar t2241 = t2222 * t2244;
        const Scalar t2239 = -t2244 / 0.3e1;
        const Scalar t2210 = (b * t2248 + t2239 + t2228 * t2218 / 0.3e1) * t2222;
        const Scalar t2229 = 0.1e1 / a;
        const Scalar t2246 = t2210 * t2229;
        const Scalar t2208 = b * t2246 + t2228 * t2231 * t2248 - t2241;
        const Scalar t2247 = t2208 * t2229;
        const Scalar t2245 = t2219 * t2214;
        const Scalar t2224 = w[3];
        const Scalar t2243 = t2224 * t2229;
        const Scalar t21 = a * a;
        const Scalar t2242 = 0.2e1 * t2228 / t21;
        const Scalar t2240 = t2231 * t2245;
        const Scalar t2227 = w[0];
        const Scalar t2226 = w[1];
        const Scalar t2225 = w[2];
        const Scalar t2211 = (b * t2249 - t2245 + t2228 * t2217) * t2231;
        const Scalar t31 = log(-t2221 / b);
        const Scalar t32 = fabs(t31);
        const Scalar t2209 = b * t2211 * t2229 + t32 / t2231 - t2240;
        const Scalar t2207 = -b * t2247 / 0.2e1 + t2210 * t2242 - t2241;
        res[0] = t2227 * t2231 * t2249 + (t2226 * t2211 - t2224 * (t2211 * t2242 - t2240) + (t2225 + 0.3e1 * b * t2243) * t2209) * t2229;
        res[1] = t2227 * t2222 * t2248 + (t2226 * t2210 + t2225 * t2208 / 0.2e1 + t2224 * t2207) * t2229;
        res[2] = t2227 * t2246 + t2226 * t2247 / 0.2e1 + t2225 * t2207 * t2229 + (t2222 * t2239 + (b * t2207 + t2209) * t2229) * t2243;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 4 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HornusCauchyInverse_segment_F<4> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[5])
    {
        const Scalar t2 = b * b;
        const Scalar t2260 = a * c - t2;
        const Scalar t2257 = sqrt(t2260);
        const Scalar t2259 = 0.1e1 / t2260;
        const Scalar t2263 = a - b;
        const Scalar t3 = sqrt(t2259);
        const Scalar t4 = atan2(t2263, t2257);
        const Scalar t5 = atan2(-b, t2257);
        const Scalar t2267 = t3 * (t4 - t5);
        const Scalar t2262 = a - 0.2e1 * b + c;
        const Scalar t2261 = 0.1e1 / t2262;
        const Scalar t2264 = 0.1e1 / c;
        const Scalar t2266 = (a * t2267 + t2263 * t2261 + b * t2264) * t2259 / 0.2e1;
        const Scalar t2265 = 0.1e1 / a;
        const Scalar t2258 = -t2261 / 0.2e1;
        const Scalar t2253 = c * t2266 - t2261;
        const Scalar t16 = log(t2262 * t2264);
        const Scalar t2252 = t2258 + (t16 / 0.2e1 + (t2253 + t2267) * b) * t2265;
        return  w[0] * t2266 + (w[1] * (b * t2266 + t2258 + t2264 / 0.2e1) + w[2] * t2253 + w[3] * t2252 + (t2261 - (-0.4e1 * b * t2252 + 0.3e1 * c * t2253) * t2265) * w[4]) * t2265;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_GradF<4> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[5], Scalar (&res)[2])
    {
        const Scalar t2282 = a - 0.2e1 * b + c;
        const Scalar t2 = t2282 * t2282;
        const Scalar t2281 = 0.1e1 / t2;
        const Scalar t4 = b * b;
        const Scalar t2279 = a * c - t4;
        const Scalar t2278 = 0.1e1 / t2279;
        const Scalar t2283 = a - b;
        const Scalar t5 = c * c;
        const Scalar t2290 = 0.1e1 / t5;
        const Scalar t2280 = 0.1e1 / t2282;
        const Scalar t2289 = 0.1e1 / c;
        const Scalar t2276 = sqrt(t2279);
        const Scalar t6 = sqrt(t2278);
        const Scalar t7 = atan2(t2283, t2276);
        const Scalar t8 = atan2(-b, t2276);
        const Scalar t2294 = t6 * (t7 - t8);
        const Scalar t2295 = (a * t2294 + t2283 * t2280 + b * t2289) * t2278;
        const Scalar t2292 = (0.3e1 / 0.2e1 * a * t2295 + t2283 * t2281 + b * t2290) * t2278 / 0.4e1;
        const Scalar t2277 = -t2281 / 0.4e1;
        const Scalar t2271 = b * t2292 + t2277 + t2290 / 0.4e1;
        const Scalar t2291 = 0.1e1 / a;
        const Scalar t2296 = t2271 * t2291;
        const Scalar t2293 = 0.2e1 * t2296;
        const Scalar t2269 = b * t2293 + c * t2292 - t2281;
        const Scalar t2298 = t2269 / 0.3e1;
        const Scalar t2297 = t2291 / 0.2e1;
        const Scalar t2288 = w[0];
        const Scalar t2287 = w[1];
        const Scalar t2286 = w[2];
        const Scalar t2285 = w[3];
        const Scalar t2284 = w[4];
        const Scalar t2270 = c * t2293 - t2281;
        const Scalar t2268 = -t2281 + (-b * t2270 + c * t2269) * t2291;
        res[0] = t2288 * t2292 + (t2287 * t2271 + t2286 * t2298 + t2285 * t2270 / 0.2e1 + t2284 * t2268) * t2291;
        const Scalar t46 = log(t2282 * t2289);
        res[1] = t2288 * t2296 + t2287 * t2291 * t2298 + t2286 * t2270 * t2297 + t2285 * t2268 * t2291 + t2284 * (t2277 + (t46 * t2297 - t2280 / 0.2e1 + (t2268 + (c * t2295 / 0.2e1 - t2280 + t2294) * t2291) * b) * t2291) * t2291;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_FGradF<4> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[5], Scalar (&res)[3])
    {
        const Scalar t2316 = a - 0.2e1 * b + c;
        const Scalar t2 = t2316 * t2316;
        const Scalar t2315 = 0.1e1 / t2;
        const Scalar t4 = b * b;
        const Scalar t2313 = a * c - t4;
        const Scalar t2312 = 0.1e1 / t2313;
        const Scalar t2317 = a - b;
        const Scalar t5 = c * c;
        const Scalar t2324 = 0.1e1 / t5;
        const Scalar t2314 = 0.1e1 / t2316;
        const Scalar t2323 = 0.1e1 / c;
        const Scalar t2309 = sqrt(t2313);
        const Scalar t6 = sqrt(t2312);
        const Scalar t7 = atan2(t2317, t2309);
        const Scalar t8 = atan2(-b, t2309);
        const Scalar t2333 = t6 * (t7 - t8);
        const Scalar t2334 = (a * t2333 + t2317 * t2314 + b * t2323) * t2312;
        const Scalar t2327 = (0.3e1 / 0.2e1 * a * t2334 + t2317 * t2315 + b * t2324) * t2312 / 0.4e1;
        const Scalar t2311 = -t2315 / 0.4e1;
        const Scalar t2303 = b * t2327 + t2311 + t2324 / 0.4e1;
        const Scalar t2325 = 0.1e1 / a;
        const Scalar t2335 = t2303 * t2325;
        const Scalar t2328 = 0.2e1 * t2335;
        const Scalar t2300 = b * t2328 + c * t2327 - t2315;
        const Scalar t2337 = t2300 / 0.3e1;
        const Scalar t2302 = c * t2328 - t2315;
        const Scalar t2336 = t2302 / 0.2e1;
        const Scalar t2318 = w[4];
        const Scalar t2332 = t2318 * t2325;
        const Scalar t2319 = w[3];
        const Scalar t2331 = t2319 * t2325;
        const Scalar t2320 = w[2];
        const Scalar t2330 = t2320 * t2325;
        const Scalar t2321 = w[1];
        const Scalar t2329 = t2321 * t2325;
        const Scalar t2326 = t2334 / 0.2e1;
        const Scalar t2322 = w[0];
        const Scalar t2310 = -t2314 / 0.2e1;
        const Scalar t2305 = c * t2326 - t2314;
        const Scalar t29 = log(t2316 * t2323);
        const Scalar t2301 = t2310 + (t29 / 0.2e1 + (t2305 + t2333) * b) * t2325;
        const Scalar t2299 = -t2315 + (-b * t2302 + c * t2300) * t2325;
        res[0] = t2322 * t2326 + (b * t2326 + t2310 + t2323 / 0.2e1) * t2329 + t2305 * t2330 + t2301 * t2331 - (-t2314 + (-0.4e1 * b * t2301 + 0.3e1 * c * t2305) * t2325) * t2332;
        res[1] = t2322 * t2327 + (t2321 * t2303 + t2320 * t2337 + t2319 * t2336 + t2318 * t2299) * t2325;
        res[2] = t2322 * t2335 + t2329 * t2337 + t2330 * t2336 + t2299 * t2331 + (t2311 + (b * t2299 + t2301) * t2325) * t2332;

    }
    
    template<>
    inline Scalar HornusCauchyInverse_segment_F_on_line<4> (Scalar a, Scalar b, const Scalar (&w)[5])
    {
        const Scalar t2346 = a - b;
        const Scalar t1 = fabs(t2346);
        const Scalar t2349 = t1 * t1;
        const Scalar t2 = t2349 * t2349;
        const Scalar t2341 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t2351 = t3 * t3;
        const Scalar t4 = t2351 * t2351;
        const Scalar t2343 = 0.1e1 / t4;
        const Scalar t8 = fabs(t2346 * t2341 + b * t2343);
        const Scalar t2357 = t8 / 0.3e1;
        const Scalar t2347 = b * b;
        const Scalar t9 = t2346 * t2346;
        const Scalar t2355 = t9 * t2341;
        const Scalar t2354 = a * t2355;
        const Scalar t2339 = t2347 * t2357 - t2354;
        const Scalar t2348 = 0.1e1 / a;
        const Scalar t2353 = -t2355 / 0.2e1;
        const Scalar t19 = fabs(t2346 / t2349 + b / t2351);
        const Scalar t23 = log(-t2346 / b);
        const Scalar t2356 = (a * t2353 + (b * t2339 + b * t19 + t23) * t2348) * t2348;
        const Scalar t45 = a * a;
        return  w[0] * a * t2357 + w[3] * t2356 + (w[1] * (b * t2357 + t2353 + t2347 * t2343 / 0.2e1) * a + w[2] * t2339 - w[4] * (-0.4e1 * b * t2356 + 0.3e1 * t2347 * t2339 / t45 - t2354)) * t2348;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_GradF_on_line<4> (Scalar a, Scalar b, const Scalar (&w)[5], Scalar (&res)[2])
    {
        const Scalar t2369 = a - b;
        const Scalar t1 = fabs(t2369);
        const Scalar t2379 = t1 * t1;
        const Scalar t2396 = 0.1e1 / t2379;
        const Scalar t2 = t2379 * t2379;
        const Scalar t2363 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t2382 = t3 * t3;
        const Scalar t2395 = 0.1e1 / t2382;
        const Scalar t4 = t2382 * t2382;
        const Scalar t2394 = 0.1e1 / t4;
        const Scalar t2375 = b * b;
        const Scalar t2376 = a * a;
        const Scalar t2364 = t2396 * t2363;
        const Scalar t2367 = t2369 * t2369;
        const Scalar t2389 = t2367 * t2364;
        const Scalar t2386 = t2376 * t2389;
        const Scalar t2366 = t2395 * t2394;
        const Scalar t2385 = -t2389 / 0.4e1;
        const Scalar t9 = fabs(t2369 * t2364 + b * t2366);
        const Scalar t2392 = t9 / 0.5e1;
        const Scalar t2361 = (b * t2392 + t2385 + t2375 * t2366 / 0.4e1) * t2376;
        const Scalar t2377 = 0.1e1 / a;
        const Scalar t2390 = t2361 * t2377;
        const Scalar t2359 = 0.2e1 * b * t2390 + t2375 * a * t2392 - t2386;
        const Scalar t2393 = t2359 / 0.3e1;
        const Scalar t2388 = t2375 / t2376;
        const Scalar t2360 = 0.2e1 * t2361 * t2388 - t2386;
        const Scalar t2391 = t2360 * t2377;
        const Scalar t2387 = a * t2367 * t2363;
        const Scalar t2374 = w[0];
        const Scalar t2373 = w[1];
        const Scalar t2372 = w[2];
        const Scalar t2371 = w[3];
        const Scalar t2370 = w[4];
        const Scalar t2358 = -b * t2391 + t2359 * t2388 - t2386;
        res[0] = t2374 * t2376 * t2392 + (t2373 * t2361 + t2372 * t2393 + t2371 * t2360 / 0.2e1 + t2370 * t2358) * t2377;
        const Scalar t46 = fabs(t2369 * t2363 + b * t2394);
        const Scalar t54 = fabs(t2369 * t2396 + b * t2395);
        const Scalar t58 = log(-t2369 / b);
        res[1] = t2374 * t2390 + t2373 * t2377 * t2393 + t2372 * t2391 / 0.2e1 + t2371 * t2358 * t2377 + t2370 * (t2376 * t2385 + (b * t2358 - t2387 / 0.2e1 + (b * (t2375 * t46 / 0.3e1 - t2387) + b * t54 + t58) * t2377) * t2377) * t2377;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_FGradF_on_line<4> (Scalar a, Scalar b, const Scalar (&w)[5], Scalar (&res)[3])
    {
        const Scalar t2412 = a - b;
        const Scalar t1 = fabs(t2412);
        const Scalar t2422 = t1 * t1;
        const Scalar t2446 = 0.1e1 / t2422;
        const Scalar t2 = t2422 * t2422;
        const Scalar t2405 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t2425 = t3 * t3;
        const Scalar t2445 = 0.1e1 / t2425;
        const Scalar t4 = t2425 * t2425;
        const Scalar t2408 = 0.1e1 / t4;
        const Scalar t2420 = 0.1e1 / a;
        const Scalar t2444 = b * t2420;
        const Scalar t2418 = b * b;
        const Scalar t2419 = a * a;
        const Scalar t2406 = t2446 * t2405;
        const Scalar t2410 = t2412 * t2412;
        const Scalar t2437 = t2410 * t2406;
        const Scalar t2430 = t2419 * t2437;
        const Scalar t2409 = t2445 * t2408;
        const Scalar t2428 = -t2437 / 0.4e1;
        const Scalar t9 = fabs(t2412 * t2406 + b * t2409);
        const Scalar t2440 = t9 / 0.5e1;
        const Scalar t2401 = (b * t2440 + t2428 + t2418 * t2409 / 0.4e1) * t2419;
        const Scalar t2439 = t2401 * t2420;
        const Scalar t2399 = 0.2e1 * b * t2439 + t2418 * a * t2440 - t2430;
        const Scalar t2443 = t2399 / 0.3e1;
        const Scalar t2432 = t2418 / t2419;
        const Scalar t2400 = 0.2e1 * t2401 * t2432 - t2430;
        const Scalar t2442 = t2400 / 0.2e1;
        const Scalar t24 = fabs(t2412 * t2405 + b * t2408);
        const Scalar t2441 = t24 / 0.3e1;
        const Scalar t2438 = t2410 * t2405;
        const Scalar t2413 = w[4];
        const Scalar t2436 = t2413 * t2420;
        const Scalar t2414 = w[3];
        const Scalar t2435 = t2414 * t2420;
        const Scalar t2415 = w[2];
        const Scalar t2434 = t2415 * t2420;
        const Scalar t2416 = w[1];
        const Scalar t2433 = t2416 * t2420;
        const Scalar t2431 = a * t2438;
        const Scalar t2429 = -t2438 / 0.2e1;
        const Scalar t2417 = w[0];
        const Scalar t2402 = t2418 * t2441 - t2431;
        const Scalar t32 = fabs(t2412 * t2446 + b * t2445);
        const Scalar t36 = log(-t2412 / b);
        const Scalar t2398 = a * t2429 + (b * t2402 + b * t32 + t36) * t2420;
        const Scalar t2397 = -t2400 * t2444 + t2399 * t2432 - t2430;
        res[0] = -(-0.4e1 * t2398 * t2444 + 0.3e1 * t2402 * t2432 - t2431) * t2436 + t2402 * t2434 + t2398 * t2435 + ((b * t2441 + t2429 + t2418 * t2408 / 0.2e1) * t2433 + t2417 * t2441) * a;
        res[1] = t2417 * t2419 * t2440 + (t2416 * t2401 + t2415 * t2443 + t2414 * t2442 + t2413 * t2397) * t2420;
        res[2] = t2417 * t2439 + t2433 * t2443 + t2434 * t2442 + t2397 * t2435 + (t2419 * t2428 + (b * t2397 + t2398) * t2420) * t2436;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 5 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HornusCauchyInverse_segment_F<5> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[6])
    {
        const Scalar t2461 = a - b;
        const Scalar t2458 = a - 0.2e1 * b + c;
        const Scalar t2457 = 0.1e1 / t2458;
        const Scalar t2465 = sqrt(t2457);
        const Scalar t2455 = t2465 * t2457;
        const Scalar t2453 = -t2455 / 0.3e1;
        const Scalar t2463 = 0.1e1 / c;
        const Scalar t2466 = sqrt(t2463);
        const Scalar t2459 = t2466 * t2463;
        const Scalar t2462 = a * c;
        const Scalar t3 = b * b;
        const Scalar t2454 = 0.1e1 / (t2462 - t3);
        const Scalar t2470 = (t2461 * t2465 + b * t2466) * t2454;
        const Scalar t2469 = (0.2e1 * a * t2470 + t2461 * t2455 + b * t2459) * t2454 / 0.3e1;
        const Scalar t2450 = b * t2469 + t2453 + t2459 / 0.3e1;
        const Scalar t2464 = 0.1e1 / a;
        const Scalar t2471 = t2450 * t2464;
        const Scalar t2449 = b * t2471 + c * t2469 - t2455;
        const Scalar t2448 = -t2455 + (-b * t2449 / 0.2e1 + 0.2e1 * c * t2450) * t2464;
        const Scalar t26 = sqrt(a * t2458);
        const Scalar t28 = sqrt(t2462);
        const Scalar t32 = log((t26 + t2461) / (-b + t28));
        const Scalar t33 = sqrt(a);
        const Scalar t2447 = t2453 + (b * t2448 + t32 / t33 - t2465 + b * (b * t2470 - t2465 + t2466) * t2464) * t2464;
        return  w[0] * t2469 + w[1] * t2471 + (w[2] * t2449 / 0.2e1 + w[3] * t2448 + w[4] * t2447 + (t2455 - (-0.5e1 * b * t2447 + 0.4e1 * c * t2448) * t2464) * w[5]) * t2464;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_GradF<5> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[6], Scalar (&res)[2])
    {
        const Scalar t2501 = 0.1e1 / a;
        const Scalar t2518 = b * t2501;
        const Scalar t2516 = b * b;
        const Scalar t2515 = 0.2e1 * c;
        const Scalar t2492 = a - b;
        const Scalar t2488 = a - 0.2e1 * b + c;
        const Scalar t2487 = 0.1e1 / t2488;
        const Scalar t2502 = sqrt(t2487);
        const Scalar t2504 = t2502 * t2487;
        const Scalar t2484 = t2487 * t2504;
        const Scalar t2481 = -t2484 / 0.5e1;
        const Scalar t2500 = 0.1e1 / c;
        const Scalar t2503 = sqrt(t2500);
        const Scalar t2506 = t2503 * t2500;
        const Scalar t2489 = t2500 * t2506;
        const Scalar t2493 = a * c;
        const Scalar t2483 = 0.1e1 / (t2493 - t2516);
        const Scalar t2510 = (t2492 * t2502 + b * t2503) * t2483;
        const Scalar t2511 = (0.2e1 * a * t2510 + t2492 * t2504 + b * t2506) * t2483;
        const Scalar t2509 = (0.4e1 / 0.3e1 * a * t2511 + t2492 * t2484 + b * t2489) * t2483 / 0.5e1;
        const Scalar t2476 = b * t2509 + t2481 + t2489 / 0.5e1;
        const Scalar t2475 = 0.3e1 * t2476 * t2518 + c * t2509 - t2484;
        const Scalar t2512 = t2475 / 0.4e1;
        const Scalar t2474 = -t2484 + (b * t2512 + t2476 * t2515) * t2501;
        const Scalar t2473 = -t2484 + (-b * t2474 / 0.3e1 + 0.3e1 / 0.4e1 * c * t2475) * t2501;
        const Scalar t2514 = t2473 / 0.2e1;
        const Scalar t2513 = t2474 / 0.3e1;
        const Scalar t2508 = t2511 / 0.3e1;
        const Scalar t2499 = w[0];
        const Scalar t2498 = w[1];
        const Scalar t2497 = w[2];
        const Scalar t2496 = w[3];
        const Scalar t2495 = w[4];
        const Scalar t2494 = w[5];
        const Scalar t2482 = -t2504 / 0.3e1;
        const Scalar t2478 = b * t2508 + t2482 + t2506 / 0.3e1;
        const Scalar t2472 = -t2484 + (-0.3e1 / 0.2e1 * b * t2473 + 0.4e1 / 0.3e1 * c * t2474) * t2501;
        res[0] = t2499 * t2509 + (t2498 * t2476 + t2497 * t2512 + t2496 * t2513 + t2495 * t2514 + t2494 * t2472) * t2501;
        const Scalar t58 = sqrt(a * t2488);
        const Scalar t60 = sqrt(t2493);
        const Scalar t64 = log((t58 + t2492) / (-b + t60));
        const Scalar t65 = sqrt(a);
        res[1] = (t2499 * t2476 + t2498 * t2512 + t2497 * t2513 + t2496 * t2514 + t2495 * t2472 + (t2481 + (b * t2472 + t2482 + (-b * t2504 + t64 / t65 - t2502 + (-t2516 * t2478 * t2518 / 0.2e1 + (t2478 * t2515 - t2502 + t2503 + (t2510 - c * t2508 / 0.2e1 + t2504 / 0.2e1) * b) * b) * t2501) * t2501) * t2501) * t2494) * t2501;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_FGradF<5> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[6], Scalar (&res)[3])
    {
        const Scalar t2572 = 0.2e1 * c;
        const Scalar t2542 = a - b;
        const Scalar t2551 = 0.1e1 / a;
        const Scalar t2571 = b * t2551;
        const Scalar t2538 = a - 0.2e1 * b + c;
        const Scalar t2537 = 0.1e1 / t2538;
        const Scalar t2552 = sqrt(t2537);
        const Scalar t2554 = t2552 * t2537;
        const Scalar t2534 = t2537 * t2554;
        const Scalar t2531 = -t2534 / 0.5e1;
        const Scalar t2550 = 0.1e1 / c;
        const Scalar t2553 = sqrt(t2550);
        const Scalar t2556 = t2553 * t2550;
        const Scalar t2539 = t2550 * t2556;
        const Scalar t2543 = a * c;
        const Scalar t3 = b * b;
        const Scalar t2533 = 0.1e1 / (t2543 - t3);
        const Scalar t2565 = (t2542 * t2552 + b * t2553) * t2533;
        const Scalar t2566 = (0.2e1 * a * t2565 + t2542 * t2554 + b * t2556) * t2533;
        const Scalar t2559 = (0.4e1 / 0.3e1 * a * t2566 + t2542 * t2534 + b * t2539) * t2533 / 0.5e1;
        const Scalar t2526 = b * t2559 + t2531 + t2539 / 0.5e1;
        const Scalar t2567 = t2526 * t2551;
        const Scalar t2524 = 0.3e1 * b * t2567 + c * t2559 - t2534;
        const Scalar t2568 = t2524 / 0.4e1;
        const Scalar t2522 = -t2534 + (b * t2568 + t2526 * t2572) * t2551;
        const Scalar t2520 = -t2534 + (-b * t2522 / 0.3e1 + 0.3e1 / 0.4e1 * c * t2524) * t2551;
        const Scalar t2570 = t2520 / 0.2e1;
        const Scalar t2569 = t2522 / 0.3e1;
        const Scalar t2544 = w[5];
        const Scalar t2564 = t2544 * t2551;
        const Scalar t2545 = w[4];
        const Scalar t2563 = t2545 * t2551;
        const Scalar t2546 = w[3];
        const Scalar t2562 = t2546 * t2551;
        const Scalar t2547 = w[2];
        const Scalar t2561 = t2547 * t2551;
        const Scalar t2548 = w[1];
        const Scalar t2560 = t2548 * t2551;
        const Scalar t2558 = t2566 / 0.3e1;
        const Scalar t2549 = w[0];
        const Scalar t2532 = -t2554 / 0.3e1;
        const Scalar t2528 = b * t2558 + t2532 + t2556 / 0.3e1;
        const Scalar t2525 = t2528 * t2571 + c * t2558 - t2554;
        const Scalar t2523 = -t2554 + (-b * t2525 / 0.2e1 + t2528 * t2572) * t2551;
        const Scalar t46 = sqrt(a * t2538);
        const Scalar t48 = sqrt(t2543);
        const Scalar t52 = log((t46 + t2542) / (-b + t48));
        const Scalar t53 = sqrt(a);
        const Scalar t2521 = t2532 + (b * t2523 + t52 / t53 - t2552 + (b * t2565 - t2552 + t2553) * t2571) * t2551;
        const Scalar t2519 = -t2534 + (-0.3e1 / 0.2e1 * b * t2520 + 0.4e1 / 0.3e1 * c * t2522) * t2551;
        res[0] = t2549 * t2558 + t2528 * t2560 + t2525 * t2561 / 0.2e1 + t2523 * t2562 + t2521 * t2563 - (-t2554 + (-0.5e1 * b * t2521 + 0.4e1 * c * t2523) * t2551) * t2564;
        res[1] = t2549 * t2559 + (t2548 * t2526 + t2547 * t2568 + t2546 * t2569 + t2545 * t2570 + t2544 * t2519) * t2551;
        res[2] = t2549 * t2567 + t2560 * t2568 + t2561 * t2569 + t2562 * t2570 + t2519 * t2563 + (t2531 + (b * t2519 + t2521) * t2551) * t2564;

    }
    
    template<>
    inline Scalar HornusCauchyInverse_segment_F_on_line<5> (Scalar a, Scalar b, const Scalar (&w)[6])
    {
        const Scalar t2585 = a - b;
        const Scalar t2580 = fabs(t2585);
        const Scalar t1 = t2580 * t2580;
        const Scalar t2607 = 0.1e1 / t1;
        const Scalar t2584 = fabs(b);
        const Scalar t2 = t2584 * t2584;
        const Scalar t2606 = 0.1e1 / t2;
        const Scalar t2578 = 0.1e1 / t2580 * t2607;
        const Scalar t2581 = 0.1e1 / t2584 * t2606;
        const Scalar t2589 = 0.1e1 / a;
        const Scalar t2605 = b * t2589;
        const Scalar t2579 = t2607 * t2578;
        const Scalar t2582 = t2606 * t2581;
        const Scalar t8 = fabs(t2585 * t2579 + b * t2582);
        const Scalar t2604 = t8 / 0.4e1;
        const Scalar t2583 = t2585 * t2585;
        const Scalar t2603 = t2583 * t2578;
        const Scalar t2602 = t2583 * t2579;
        const Scalar t2588 = b * b;
        const Scalar t9 = a * a;
        const Scalar t2601 = t2588 / t9;
        const Scalar t2591 = sqrt(a);
        const Scalar t2586 = t2591 * a;
        const Scalar t2600 = t2586 * t2602;
        const Scalar t2599 = -t2602 / 0.3e1;
        const Scalar t2576 = (b * t2604 + t2599 + t2588 * t2582 / 0.3e1) * t2586;
        const Scalar t2575 = t2576 * t2605 + t2588 * t2591 * t2604 - t2600;
        const Scalar t2574 = -t2575 * t2605 / 0.2e1 + 0.2e1 * t2576 * t2601 - t2600;
        const Scalar t27 = fabs(t2585 * t2578 + b * t2581);
        const Scalar t36 = log(-t2585 / b);
        const Scalar t37 = fabs(t36);
        const Scalar t2573 = t2574 * t2605 + ((b * t27 / 0.2e1 - t2603 + t2588 * t2581) * t2591 * t2605 + t37 / t2591 - t2591 * t2603) * t2589 + t2586 * t2599;
        return  w[0] * t2586 * t2604 + (w[1] * t2576 + w[2] * t2575 / 0.2e1 + w[3] * t2574 + w[4] * t2573 + (-0.4e1 * t2574 * t2601 + t2600 + 0.5e1 * t2573 * t2605) * w[5]) * t2589;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_GradF_on_line<5> (Scalar a, Scalar b, const Scalar (&w)[6], Scalar (&res)[2])
    {
        const Scalar t2625 = a - b;
        const Scalar t2619 = fabs(t2625);
        const Scalar t1 = t2619 * t2619;
        const Scalar t2668 = 0.1e1 / t1;
        const Scalar t2624 = fabs(b);
        const Scalar t2 = t2624 * t2624;
        const Scalar t2667 = 0.1e1 / t2;
        const Scalar t2616 = 0.1e1 / t2619 * t2668;
        const Scalar t2620 = 0.1e1 / t2624 * t2667;
        const Scalar t2617 = t2668 * t2616;
        const Scalar t2621 = t2667 * t2620;
        const Scalar t2636 = 0.1e1 / a;
        const Scalar t2666 = b * t2636;
        const Scalar t2635 = b * b;
        const Scalar t2638 = sqrt(a);
        const Scalar t2647 = t2638 * a;
        const Scalar t2626 = a * t2647;
        const Scalar t2618 = t2668 * t2617;
        const Scalar t2623 = t2625 * t2625;
        const Scalar t2656 = t2623 * t2618;
        const Scalar t2653 = t2626 * t2656;
        const Scalar t2622 = t2667 * t2621;
        const Scalar t2649 = -t2656 / 0.5e1;
        const Scalar t9 = fabs(t2625 * t2618 + b * t2622);
        const Scalar t2663 = t9 / 0.6e1;
        const Scalar t2612 = (b * t2663 + t2649 + t2635 * t2622 / 0.5e1) * t2626;
        const Scalar t2659 = t2612 * t2636;
        const Scalar t2611 = 0.3e1 * b * t2659 + t2635 * t2647 * t2663 - t2653;
        const Scalar t2665 = t2611 / 0.4e1;
        const Scalar t21 = fabs(t2625 * t2617 + b * t2621);
        const Scalar t2664 = t21 / 0.4e1;
        const Scalar t2651 = t2636 * t2665;
        const Scalar t22 = a * a;
        const Scalar t2655 = t2635 / t22;
        const Scalar t2654 = 0.2e1 * t2655;
        const Scalar t2610 = b * t2651 + t2612 * t2654 - t2653;
        const Scalar t2660 = t2610 * t2636;
        const Scalar t2609 = -b * t2660 / 0.3e1 + 0.3e1 / 0.4e1 * t2611 * t2655 - t2653;
        const Scalar t2661 = t2609 * t2636;
        const Scalar t2608 = -0.3e1 / 0.2e1 * b * t2661 + 0.4e1 / 0.3e1 * t2610 * t2655 - t2653;
        const Scalar t2662 = t2608 * t2636;
        const Scalar t2658 = t2623 * t2616;
        const Scalar t2657 = t2623 * t2617;
        const Scalar t2652 = t2647 * t2657;
        const Scalar t2650 = -t2657 / 0.3e1;
        const Scalar t2634 = w[0];
        const Scalar t2633 = w[1];
        const Scalar t2632 = w[2];
        const Scalar t2631 = w[3];
        const Scalar t2630 = w[4];
        const Scalar t2629 = w[5];
        const Scalar t2613 = (b * t2664 + t2650 + t2635 * t2621 / 0.3e1) * t2647;
        res[0] = t2634 * t2626 * t2663 + (t2633 * t2612 + t2632 * t2665 + t2631 * t2610 / 0.3e1 + t2630 * t2609 / 0.2e1 + t2629 * t2608) * t2636;
        const Scalar t70 = fabs(t2625 * t2616 + b * t2620);
        const Scalar t79 = log(-t2625 / b);
        const Scalar t80 = fabs(t79);
        res[1] = t2634 * t2659 + t2633 * t2651 + t2632 * t2660 / 0.3e1 + t2631 * t2661 / 0.2e1 + t2630 * t2662 + t2629 * (b * t2662 + ((-(t2613 * t2666 + t2635 * t2638 * t2664 - t2652) * t2666 / 0.2e1 + t2613 * t2654 - t2652) * t2666 + ((b * t70 / 0.2e1 - t2658 + t2635 * t2620) * t2638 * t2666 + t80 / t2638 - t2638 * t2658) * t2636 + t2647 * t2650) * t2636 + t2626 * t2649) * t2636;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_FGradF_on_line<5> (Scalar a, Scalar b, const Scalar (&w)[6], Scalar (&res)[3])
    {
        const Scalar t2689 = a - b;
        const Scalar t2683 = fabs(t2689);
        const Scalar t1 = t2683 * t2683;
        const Scalar t2732 = 0.1e1 / t1;
        const Scalar t2688 = fabs(b);
        const Scalar t2 = t2688 * t2688;
        const Scalar t2731 = 0.1e1 / t2;
        const Scalar t2680 = 0.1e1 / t2683 * t2732;
        const Scalar t2684 = 0.1e1 / t2688 * t2731;
        const Scalar t2681 = t2732 * t2680;
        const Scalar t2685 = t2731 * t2684;
        const Scalar t2700 = 0.1e1 / a;
        const Scalar t2730 = b * t2700;
        const Scalar t2699 = b * b;
        const Scalar t2702 = sqrt(a);
        const Scalar t2711 = t2702 * a;
        const Scalar t2690 = a * t2711;
        const Scalar t2682 = t2732 * t2681;
        const Scalar t2687 = t2689 * t2689;
        const Scalar t2721 = t2687 * t2682;
        const Scalar t2717 = t2690 * t2721;
        const Scalar t2686 = t2731 * t2685;
        const Scalar t2713 = -t2721 / 0.5e1;
        const Scalar t9 = fabs(t2689 * t2682 + b * t2686);
        const Scalar t2727 = t9 / 0.6e1;
        const Scalar t2676 = (b * t2727 + t2713 + t2699 * t2686 / 0.5e1) * t2690;
        const Scalar t2724 = t2676 * t2700;
        const Scalar t2674 = 0.3e1 * b * t2724 + t2699 * t2711 * t2727 - t2717;
        const Scalar t2729 = t2674 / 0.4e1;
        const Scalar t21 = fabs(t2689 * t2681 + b * t2685);
        const Scalar t2728 = t21 / 0.4e1;
        const Scalar t22 = a * a;
        const Scalar t2719 = t2699 / t22;
        const Scalar t2715 = t2700 * t2729;
        const Scalar t2718 = 0.2e1 * t2719;
        const Scalar t2672 = b * t2715 + t2676 * t2718 - t2717;
        const Scalar t2725 = t2672 * t2700;
        const Scalar t2670 = -b * t2725 / 0.3e1 + 0.3e1 / 0.4e1 * t2674 * t2719 - t2717;
        const Scalar t2726 = t2670 * t2700;
        const Scalar t2723 = t2687 * t2680;
        const Scalar t2722 = t2687 * t2681;
        const Scalar t2693 = w[5];
        const Scalar t2720 = t2693 * t2700;
        const Scalar t2716 = t2711 * t2722;
        const Scalar t2714 = -t2722 / 0.3e1;
        const Scalar t2698 = w[0];
        const Scalar t2697 = w[1];
        const Scalar t2696 = w[2];
        const Scalar t2695 = w[3];
        const Scalar t2694 = w[4];
        const Scalar t2677 = (b * t2728 + t2714 + t2699 * t2685 / 0.3e1) * t2711;
        const Scalar t2675 = t2677 * t2730 + t2699 * t2702 * t2728 - t2716;
        const Scalar t2673 = -t2675 * t2730 / 0.2e1 + t2677 * t2718 - t2716;
        const Scalar t45 = fabs(t2689 * t2680 + b * t2684);
        const Scalar t54 = log(-t2689 / b);
        const Scalar t55 = fabs(t54);
        const Scalar t2671 = t2673 * t2730 + ((b * t45 / 0.2e1 - t2723 + t2699 * t2684) * t2702 * t2730 + t55 / t2702 - t2702 * t2723) * t2700 + t2711 * t2714;
        const Scalar t2669 = -0.3e1 / 0.2e1 * b * t2726 + 0.4e1 / 0.3e1 * t2672 * t2719 - t2717;
        res[0] = t2698 * t2711 * t2728 + (t2697 * t2677 + t2696 * t2675 / 0.2e1 + t2695 * t2673 - t2693 * (0.4e1 * t2673 * t2719 - t2716) + (t2694 + 0.5e1 * b * t2720) * t2671) * t2700;
        res[1] = t2698 * t2690 * t2727 + (t2697 * t2676 + t2696 * t2729 + t2695 * t2672 / 0.3e1 + t2694 * t2670 / 0.2e1 + t2693 * t2669) * t2700;
        res[2] = t2698 * t2724 + t2697 * t2715 + t2696 * t2725 / 0.3e1 + t2695 * t2726 / 0.2e1 + t2694 * t2669 * t2700 + (t2690 * t2713 + (b * t2669 + t2671) * t2700) * t2720;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 6 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HornusCauchyInverse_segment_F<6> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[7])
    {
        const Scalar t2752 = 0.1e1 / a;
        const Scalar t2758 = t2752 / 0.2e1;
        const Scalar t2748 = a - 0.2e1 * b + c;
        const Scalar t2 = t2748 * t2748;
        const Scalar t2747 = 0.1e1 / t2;
        const Scalar t2743 = -t2747 / 0.4e1;
        const Scalar t4 = c * c;
        const Scalar t2751 = 0.1e1 / t4;
        const Scalar t6 = b * b;
        const Scalar t2745 = a * c - t6;
        const Scalar t2744 = 0.1e1 / t2745;
        const Scalar t2749 = a - b;
        const Scalar t2746 = 0.1e1 / t2748;
        const Scalar t2750 = 0.1e1 / c;
        const Scalar t2742 = sqrt(t2745);
        const Scalar t7 = sqrt(t2744);
        const Scalar t8 = atan2(t2749, t2742);
        const Scalar t9 = atan2(-b, t2742);
        const Scalar t2755 = t7 * (t8 - t9);
        const Scalar t2756 = (a * t2755 + t2749 * t2746 + b * t2750) * t2744;
        const Scalar t2753 = (0.3e1 / 0.2e1 * a * t2756 + t2749 * t2747 + b * t2751) * t2744 / 0.4e1;
        const Scalar t2757 = (b * t2753 + t2743 + t2751 / 0.4e1) * t2752;
        const Scalar t2754 = 0.2e1 * t2757;
        const Scalar t2736 = c * t2754 - t2747;
        const Scalar t2735 = b * t2754 + c * t2753 - t2747;
        const Scalar t2734 = -t2747 + (-b * t2736 + c * t2735) * t2752;
        const Scalar t32 = log(t2748 * t2750);
        const Scalar t2733 = t2743 + (t32 * t2758 - t2746 / 0.2e1 + (t2734 + (c * t2756 / 0.2e1 - t2746 + t2755) * t2752) * b) * t2752;
        return  w[0] * t2753 + w[1] * t2757 + w[3] * t2736 * t2758 + (w[2] * t2735 / 0.3e1 + w[4] * t2734 + w[5] * t2733 + (t2747 - (-0.6e1 * b * t2733 + 0.5e1 * c * t2734) * t2752) * w[6]) * t2752;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_GradF<6> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[7], Scalar (&res)[2])
    {
        const Scalar t2788 = 0.1e1 / c;
        const Scalar t2807 = -0.2e1 * b;
        const Scalar t2779 = a + t2807 + c;
        const Scalar t2776 = 0.1e1 / t2779;
        const Scalar t2 = c * c;
        const Scalar t2806 = 0.1e1 / t2;
        const Scalar t3 = t2779 * t2779;
        const Scalar t2777 = 0.1e1 / t3;
        const Scalar t2805 = b * t2777;
        const Scalar t2778 = t2776 * t2777;
        const Scalar t5 = b * b;
        const Scalar t2775 = a * c - t5;
        const Scalar t2774 = 0.1e1 / t2775;
        const Scalar t2780 = a - b;
        const Scalar t2790 = t2788 * t2806;
        const Scalar t2771 = sqrt(t2775);
        const Scalar t6 = sqrt(t2774);
        const Scalar t7 = atan2(t2780, t2771);
        const Scalar t8 = atan2(-b, t2771);
        const Scalar t2797 = t6 * (t7 - t8);
        const Scalar t2798 = (a * t2797 + t2780 * t2776 + b * t2788) * t2774;
        const Scalar t2799 = (0.3e1 / 0.2e1 * a * t2798 + t2780 * t2777 + b * t2806) * t2774;
        const Scalar t2796 = (0.5e1 / 0.4e1 * a * t2799 + t2780 * t2778 + b * t2790) * t2774 / 0.6e1;
        const Scalar t2773 = -t2778 / 0.6e1;
        const Scalar t2764 = b * t2796 + t2773 + t2790 / 0.6e1;
        const Scalar t2791 = 0.1e1 / a;
        const Scalar t2800 = t2764 * t2791;
        const Scalar t2763 = 0.4e1 * b * t2800 + c * t2796 - t2778;
        const Scalar t2761 = -t2778 + (0.2e1 / 0.5e1 * b * t2763 + 0.2e1 * c * t2764) * t2791;
        const Scalar t2801 = t2763 * t2791;
        const Scalar t2762 = 0.3e1 / 0.5e1 * c * t2801 - t2778;
        const Scalar t2760 = -t2778 + (-0.2e1 / 0.3e1 * b * t2762 + c * t2761) * t2791;
        const Scalar t2804 = t2760 / 0.2e1;
        const Scalar t2803 = t2761 / 0.4e1;
        const Scalar t2802 = t2762 / 0.3e1;
        const Scalar t2787 = w[0];
        const Scalar t2786 = w[1];
        const Scalar t2785 = w[2];
        const Scalar t2784 = w[3];
        const Scalar t2783 = w[4];
        const Scalar t2782 = w[5];
        const Scalar t2781 = w[6];
        const Scalar t2759 = -t2778 + (t2760 * t2807 + 0.5e1 / 0.3e1 * c * t2762) * t2791;
        res[0] = t2787 * t2796 + (t2786 * t2764 + t2785 * t2763 / 0.5e1 + t2784 * t2803 + t2783 * t2802 + t2782 * t2804 + t2781 * t2759) * t2791;
        const Scalar t70 = log(t2779 * t2788);
        res[1] = t2787 * t2800 + t2786 * t2801 / 0.5e1 + (t2785 * t2803 + t2784 * t2802 + t2783 * t2804 + t2782 * t2759 + (t2773 + (-t2777 / 0.4e1 + b * t2759 + (-t2776 / 0.2e1 - t2805 + (t70 / 0.2e1 + (-t2776 + t2797 + t2805 + (t2798 / 0.2e1 - t2777 + c * t2799 / 0.4e1) * c) * b) * t2791) * t2791) * t2791) * t2781) * t2791;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_FGradF<6> (Scalar a, Scalar b, Scalar c, const Scalar (&w)[7], Scalar (&res)[3])
    {
        const Scalar t2841 = 0.1e1 / c;
        const Scalar t2866 = -0.2e1 * b;
        const Scalar t2832 = a + t2866 + c;
        const Scalar t2829 = 0.1e1 / t2832;
        const Scalar t2 = c * c;
        const Scalar t2842 = 0.1e1 / t2;
        const Scalar t3 = t2832 * t2832;
        const Scalar t2830 = 0.1e1 / t3;
        const Scalar t2831 = t2829 * t2830;
        const Scalar t5 = b * b;
        const Scalar t2828 = a * c - t5;
        const Scalar t2827 = 0.1e1 / t2828;
        const Scalar t2833 = a - b;
        const Scalar t2843 = t2841 * t2842;
        const Scalar t2824 = sqrt(t2828);
        const Scalar t6 = sqrt(t2827);
        const Scalar t7 = atan2(t2833, t2824);
        const Scalar t8 = atan2(-b, t2824);
        const Scalar t2858 = t6 * (t7 - t8);
        const Scalar t2859 = (a * t2858 + t2833 * t2829 + b * t2841) * t2827;
        const Scalar t2860 = (0.3e1 / 0.2e1 * a * t2859 + t2833 * t2830 + b * t2842) * t2827;
        const Scalar t2850 = (0.5e1 / 0.4e1 * a * t2860 + t2833 * t2831 + b * t2843) * t2827 / 0.6e1;
        const Scalar t2826 = -t2831 / 0.6e1;
        const Scalar t2816 = b * t2850 + t2826 + t2843 / 0.6e1;
        const Scalar t2844 = 0.1e1 / a;
        const Scalar t2861 = t2816 * t2844;
        const Scalar t2814 = 0.4e1 * b * t2861 + c * t2850 - t2831;
        const Scalar t2811 = -t2831 + (0.2e1 / 0.5e1 * b * t2814 + 0.2e1 * c * t2816) * t2844;
        const Scalar t2813 = 0.3e1 / 0.5e1 * c * t2814 * t2844 - t2831;
        const Scalar t2809 = -t2831 + (-0.2e1 / 0.3e1 * b * t2813 + c * t2811) * t2844;
        const Scalar t2865 = t2809 / 0.2e1;
        const Scalar t2864 = t2811 / 0.4e1;
        const Scalar t2863 = t2813 / 0.3e1;
        const Scalar t2862 = t2814 / 0.5e1;
        const Scalar t2834 = w[6];
        const Scalar t2857 = t2834 * t2844;
        const Scalar t2835 = w[5];
        const Scalar t2856 = t2835 * t2844;
        const Scalar t2836 = w[4];
        const Scalar t2855 = t2836 * t2844;
        const Scalar t2837 = w[3];
        const Scalar t2854 = t2837 * t2844;
        const Scalar t2838 = w[2];
        const Scalar t2853 = t2838 * t2844;
        const Scalar t2839 = w[1];
        const Scalar t2852 = t2839 * t2844;
        const Scalar t2825 = -t2830 / 0.4e1;
        const Scalar t2849 = t2860 / 0.4e1;
        const Scalar t2819 = b * t2849 + t2825 + t2842 / 0.4e1;
        const Scalar t2851 = 0.2e1 * t2819 * t2844;
        const Scalar t2840 = w[0];
        const Scalar t2817 = c * t2851 - t2830;
        const Scalar t2815 = b * t2851 + c * t2849 - t2830;
        const Scalar t2812 = -t2830 + (-b * t2817 + c * t2815) * t2844;
        const Scalar t57 = log(t2832 * t2841);
        const Scalar t2810 = t2825 + (t57 * t2844 / 0.2e1 - t2829 / 0.2e1 + (t2812 + (c * t2859 / 0.2e1 - t2829 + t2858) * t2844) * b) * t2844;
        const Scalar t2808 = -t2831 + (t2809 * t2866 + 0.5e1 / 0.3e1 * c * t2813) * t2844;
        res[0] = t2840 * t2849 + t2819 * t2852 + t2815 * t2853 / 0.3e1 + t2817 * t2854 / 0.2e1 + t2812 * t2855 + t2810 * t2856 - (-t2830 + (-0.6e1 * b * t2810 + 0.5e1 * c * t2812) * t2844) * t2857;
        res[1] = t2840 * t2850 + (t2839 * t2816 + t2838 * t2862 + t2837 * t2864 + t2836 * t2863 + t2835 * t2865 + t2834 * t2808) * t2844;
        res[2] = t2840 * t2861 + t2852 * t2862 + t2853 * t2864 + t2854 * t2863 + t2855 * t2865 + t2808 * t2856 + (t2826 + (b * t2808 + t2810) * t2844) * t2857;

    }
    
    template<>
    inline Scalar HornusCauchyInverse_segment_F_on_line<6> (Scalar a, Scalar b, const Scalar (&w)[7])
    {
        const Scalar t2879 = a - b;
        const Scalar t1 = fabs(t2879);
        const Scalar t2884 = t1 * t1;
        const Scalar t2900 = 0.1e1 / t2884;
        const Scalar t2 = t2884 * t2884;
        const Scalar t2873 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t2887 = t3 * t3;
        const Scalar t2899 = 0.1e1 / t2887;
        const Scalar t4 = t2887 * t2887;
        const Scalar t2898 = 0.1e1 / t4;
        const Scalar t2876 = t2899 * t2898;
        const Scalar t2880 = b * b;
        const Scalar t2881 = a * a;
        const Scalar t2874 = t2900 * t2873;
        const Scalar t2877 = t2879 * t2879;
        const Scalar t2894 = t2877 * t2874;
        const Scalar t2890 = -t2894 / 0.4e1;
        const Scalar t9 = fabs(t2879 * t2874 + b * t2876);
        const Scalar t2895 = t9 / 0.5e1;
        const Scalar t2871 = (b * t2895 + t2890 + t2880 * t2876 / 0.4e1) * t2881;
        const Scalar t2897 = 0.2e1 * t2871;
        const Scalar t2882 = 0.1e1 / a;
        const Scalar t2896 = b * t2882;
        const Scalar t2893 = t2880 / t2881;
        const Scalar t2892 = a * t2877 * t2873;
        const Scalar t2891 = t2881 * t2894;
        const Scalar t2870 = t2893 * t2897 - t2891;
        const Scalar t2869 = t2896 * t2897 + t2880 * a * t2895 - t2891;
        const Scalar t2868 = -t2870 * t2896 + t2869 * t2893 - t2891;
        const Scalar t28 = fabs(t2879 * t2873 + b * t2898);
        const Scalar t36 = fabs(t2879 * t2900 + b * t2899);
        const Scalar t40 = log(-t2879 / b);
        const Scalar t2867 = t2881 * t2890 + (b * t2868 - t2892 / 0.2e1 + (b * (t2880 * t28 / 0.3e1 - t2892) + b * t36 + t40) * t2882) * t2882;
        return  w[0] * t2881 * t2895 + (w[1] * t2871 + w[2] * t2869 / 0.3e1 + w[3] * t2870 / 0.2e1 + w[4] * t2868 + w[5] * t2867 + (-0.5e1 * t2868 * t2893 + t2891 + 0.6e1 * t2867 * t2896) * w[6]) * t2882;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_GradF_on_line<6> (Scalar a, Scalar b, const Scalar (&w)[7], Scalar (&res)[2])
    {
        const Scalar t2926 = b * b;
        const Scalar t2918 = a - b;
        const Scalar t1 = fabs(t2918);
        const Scalar t2932 = t1 * t1;
        const Scalar t2954 = 0.1e1 / t2932;
        const Scalar t2933 = t2932 * t2932;
        const Scalar t2910 = 0.1e1 / t2933;
        const Scalar t2 = fabs(b);
        const Scalar t2936 = t2 * t2;
        const Scalar t2953 = 0.1e1 / t2936;
        const Scalar t2937 = t2936 * t2936;
        const Scalar t2952 = b / t2937;
        const Scalar t4 = t2937 * t2937;
        const Scalar t2915 = 0.1e1 / t4;
        const Scalar t2928 = a * a;
        const Scalar t2927 = a * t2928;
        const Scalar t5 = t2933 * t2933;
        const Scalar t2912 = 0.1e1 / t5;
        const Scalar t2916 = t2918 * t2918;
        const Scalar t2945 = t2916 * t2912;
        const Scalar t2940 = -t2945 / 0.6e1;
        const Scalar t10 = fabs(t2918 * t2912 + b * t2915);
        const Scalar t2950 = t10 / 0.7e1;
        const Scalar t2906 = (b * t2950 + t2940 + t2926 * t2915 / 0.6e1) * t2927;
        const Scalar t2942 = t2927 * t2945;
        const Scalar t2944 = t2926 / t2928;
        const Scalar t2929 = 0.1e1 / a;
        const Scalar t2946 = t2906 * t2929;
        const Scalar t2905 = 0.4e1 * b * t2946 + t2926 * t2928 * t2950 - t2942;
        const Scalar t2947 = t2905 * t2929;
        const Scalar t2903 = 0.2e1 / 0.5e1 * b * t2947 + 0.2e1 * t2906 * t2944 - t2942;
        const Scalar t2951 = t2903 / 0.4e1;
        const Scalar t2904 = 0.3e1 / 0.5e1 * t2905 * t2944 - t2942;
        const Scalar t2948 = t2904 * t2929;
        const Scalar t2902 = -0.2e1 / 0.3e1 * b * t2948 + t2903 * t2944 - t2942;
        const Scalar t2949 = t2902 * t2929;
        const Scalar t2943 = a * t2916 * t2910;
        const Scalar t2911 = t2954 * t2910;
        const Scalar t2941 = t2928 * t2916 * t2911;
        const Scalar t2925 = w[0];
        const Scalar t2924 = w[1];
        const Scalar t2923 = w[2];
        const Scalar t2922 = w[3];
        const Scalar t2921 = w[4];
        const Scalar t2920 = w[5];
        const Scalar t2919 = w[6];
        const Scalar t2901 = -0.2e1 * b * t2949 + 0.5e1 / 0.3e1 * t2904 * t2944 - t2942;
        res[0] = t2925 * t2927 * t2950 + (t2924 * t2906 + t2923 * t2905 / 0.5e1 + t2922 * t2951 + t2921 * t2904 / 0.3e1 + t2920 * t2902 / 0.2e1 + t2919 * t2901) * t2929;
        const Scalar t67 = fabs(t2918 * t2911 + t2953 * t2952);
        const Scalar t77 = fabs(t2918 * t2910 + t2952);
        const Scalar t85 = fabs(t2918 * t2954 + b * t2953);
        const Scalar t89 = log(-t2918 / b);
        res[1] = t2925 * t2946 + t2924 * t2947 / 0.5e1 + t2923 * t2929 * t2951 + t2922 * t2948 / 0.3e1 + t2921 * t2949 / 0.2e1 + t2920 * t2901 * t2929 + t2919 * (t2927 * t2940 + (-t2941 / 0.4e1 + b * t2901 + (-t2943 / 0.2e1 + b * ((t2926 * a * t67 / 0.5e1 - t2941) * t2944 - t2941) + (t2926 * t2941 + b * (t2926 * t77 / 0.3e1 - t2943) + b * t85 + t89) * t2929) * t2929) * t2929) * t2929;

    }
    
    
    template<>
    inline void HornusCauchyInverse_segment_FGradF_on_line<6> (Scalar a, Scalar b, const Scalar (&w)[7], Scalar (&res)[3])
    {
        const Scalar t2976 = a - b;
        const Scalar t1 = fabs(t2976);
        const Scalar t2990 = t1 * t1;
        const Scalar t3018 = 0.1e1 / t2990;
        const Scalar t2991 = t2990 * t2990;
        const Scalar t2968 = 0.1e1 / t2991;
        const Scalar t2 = fabs(b);
        const Scalar t2994 = t2 * t2;
        const Scalar t3017 = 0.1e1 / t2994;
        const Scalar t2995 = t2994 * t2994;
        const Scalar t3016 = 0.1e1 / t2995;
        const Scalar t2987 = 0.1e1 / a;
        const Scalar t3015 = b * t2987;
        const Scalar t3 = t2995 * t2995;
        const Scalar t2973 = 0.1e1 / t3;
        const Scalar t2984 = b * b;
        const Scalar t2986 = a * a;
        const Scalar t2985 = a * t2986;
        const Scalar t4 = t2991 * t2991;
        const Scalar t2970 = 0.1e1 / t4;
        const Scalar t2974 = t2976 * t2976;
        const Scalar t3006 = t2974 * t2970;
        const Scalar t2998 = -t3006 / 0.6e1;
        const Scalar t9 = fabs(t2976 * t2970 + b * t2973);
        const Scalar t3012 = t9 / 0.7e1;
        const Scalar t2964 = (b * t3012 + t2998 + t2984 * t2973 / 0.6e1) * t2985;
        const Scalar t3001 = t2985 * t3006;
        const Scalar t3004 = t2984 / t2986;
        const Scalar t3002 = 0.2e1 * t3004;
        const Scalar t3008 = t2964 * t2987;
        const Scalar t2961 = 0.4e1 * b * t3008 + t2984 * t2986 * t3012 - t3001;
        const Scalar t3009 = t2961 * t2987;
        const Scalar t2959 = 0.2e1 / 0.5e1 * b * t3009 + t2964 * t3002 - t3001;
        const Scalar t3014 = t2959 / 0.4e1;
        const Scalar t2969 = t3018 * t2968;
        const Scalar t2972 = t3017 * t3016;
        const Scalar t25 = fabs(t2976 * t2969 + b * t2972);
        const Scalar t3013 = t25 / 0.5e1;
        const Scalar t2960 = 0.3e1 / 0.5e1 * t2961 * t3004 - t3001;
        const Scalar t3010 = t2960 * t2987;
        const Scalar t2956 = -0.2e1 / 0.3e1 * b * t3010 + t2959 * t3004 - t3001;
        const Scalar t3011 = t2956 * t2987;
        const Scalar t3007 = t2974 * t2969;
        const Scalar t2977 = w[6];
        const Scalar t3005 = t2977 * t2987;
        const Scalar t3003 = a * t2974 * t2968;
        const Scalar t3000 = t2986 * t3007;
        const Scalar t2999 = -t3007 / 0.4e1;
        const Scalar t2983 = w[0];
        const Scalar t2982 = w[1];
        const Scalar t2981 = w[2];
        const Scalar t2980 = w[3];
        const Scalar t2979 = w[4];
        const Scalar t2978 = w[5];
        const Scalar t2965 = (b * t3013 + t2999 + t2984 * t2972 / 0.4e1) * t2986;
        const Scalar t2963 = t2965 * t3002 - t3000;
        const Scalar t2962 = 0.2e1 * t2965 * t3015 + t2984 * a * t3013 - t3000;
        const Scalar t2958 = -t2963 * t3015 + t2962 * t3004 - t3000;
        const Scalar t50 = fabs(t2976 * t2968 + b * t3016);
        const Scalar t58 = fabs(t2976 * t3018 + b * t3017);
        const Scalar t62 = log(-t2976 / b);
        const Scalar t2957 = t2986 * t2999 + (b * t2958 - t3003 / 0.2e1 + (b * (t2984 * t50 / 0.3e1 - t3003) + b * t58 + t62) * t2987) * t2987;
        const Scalar t2955 = -0.2e1 * b * t3011 + 0.5e1 / 0.3e1 * t2960 * t3004 - t3001;
        res[0] = t2983 * t2986 * t3013 + (t2982 * t2965 + t2981 * t2962 / 0.3e1 + t2980 * t2963 / 0.2e1 + t2979 * t2958 - t2977 * (0.5e1 * t2958 * t3004 - t3000) + (t2978 + 0.6e1 * b * t3005) * t2957) * t2987;
        res[1] = t2983 * t2985 * t3012 + (t2982 * t2964 + t2981 * t2961 / 0.5e1 + t2980 * t3014 + t2979 * t2960 / 0.3e1 + t2978 * t2956 / 0.2e1 + t2977 * t2955) * t2987;
        res[2] = t2983 * t3008 + t2982 * t3009 / 0.5e1 + t2985 * t2998 * t3005 + t2980 * t3010 / 0.3e1 + t2979 * t3011 / 0.2e1 + (t2978 * t2955 + t2981 * t3014 + (b * t2955 + t2957) * t3005) * t2987;

    }
    
    



} // Close namespace HornusCauchyAndInverseOptim
} // Close namespace convol
} // Close namespace expressive

#endif // CONVOL_HORNUS_CAUCHY_AND_INVERSE_OPTIMIZED_FUNCTIONS_H_

