/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: StandardCauchyAndInverseOptimizedFunctions.h
  
   Language: C++
  
   License: Convol Licence
  
   \author: Cedric Zanni 
   E-Mail: cedric.zanni@inria.fr
  
   Description: Header files defining optimized function for Standard CauchyX ans InverseX kernels.
 
   Platform Dependencies: None 
*/  

#pragma once
#ifndef CONVOL_STANDARD_CAUCHY_AND_INVERSE_OPTIMIZED_FUNCTIONS_H_
#define CONVOL_STANDARD_CAUCHY_AND_INVERSE_OPTIMIZED_FUNCTIONS_H_

// core dependencies
#include <core/CoreRequired.h>

// std dependencies
#include<vector>
#include<array>

#include<math.h>

namespace expressive {

namespace convol {

namespace StandardCauchyAndInverseOptim {

////////////////////////////
/// Function declaration ///
////////////////////////////

template<int I, int K>
inline Scalar StandardCauchyInverse_segment_F (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w);
template<int I, int K>
inline void StandardCauchyInverse_segment_GradF (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2]);
template<int I, int K>
inline void StandardCauchyInverse_segment_FGradF (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3]);

template<int I, int K>
inline Scalar StandardCauchyInverse_segment_F_on_line (const Scalar a, const Scalar b, const std::vector<Scalar>& w);
template<int I, int K>
inline void StandardCauchyInverse_segment_GradF_on_line (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2]);
template<int I, int K>
inline void StandardCauchyInverse_segment_FGradF_on_line (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3]);

///////////////////////////////
/// Template specialization ///
///////////////////////////////





    /////////////////////////////////
    // Code for kernel of degree 3 //
    // and weight of degree 0      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<3,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t6 = sqrt(0.1e1 / (a - 0.2e1 * b + c));
        const Scalar t9 = sqrt(0.1e1 / c);
        const Scalar t14 = b * b;
        return  w[0] * ((a - b) * t6 + b * t9) / (a * c - t14);

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<3,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t8 = 0.1e1 / c;
        const Scalar t10 = sqrt(t8);
        const Scalar t11 = b * b;
        const Scalar t2 = 0.1e1 / (a * c - t11);
        const Scalar t4 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t9 = sqrt(t4);
        const Scalar t3 = t9 * t4;
        const Scalar t5 = t10 * t8;
        const Scalar t6 = a - b;
        const Scalar t13 = (0.2e1 * a * (t6 * t9 + b * t10) * t2 + t6 * t3 + b * t5) * t2 / 0.3e1;
        const Scalar t7 = w[0];
        res[0] = t7 * t13;
        res[1] = t7 * (b * t13 - t3 / 0.3e1 + t5 / 0.3e1) / a;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<3,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t2 = b * b;
        const Scalar t16 = 0.1e1 / (a * c - t2);
        const Scalar t20 = a - b;
        const Scalar t18 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t23 = sqrt(t18);
        const Scalar t22 = 0.1e1 / c;
        const Scalar t24 = sqrt(t22);
        const Scalar t28 = (t20 * t23 + b * t24) * t16;
        const Scalar t17 = t23 * t18;
        const Scalar t19 = t24 * t22;
        const Scalar t27 = (0.2e1 * a * t28 + t20 * t17 + b * t19) * t16 / 0.3e1;
        const Scalar t21 = w[0];
        res[0] = t21 * t28;
        res[1] = t21 * t27;
        res[2] = t21 * (b * t27 - t17 / 0.3e1 + t19 / 0.3e1) / a;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<3,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t29 = a - b;
        const Scalar t2 = sqrt(a);
        const Scalar t4 = fabs(t29);
        const Scalar t5 = t4 * t4;
        const Scalar t9 = fabs(b);
        const Scalar t10 = t9 * t9;
        const Scalar t15 = fabs(t29 / t5 / t4 + b / t10 / t9);
        return  w[0] * t2 * t15 / 0.2e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<3,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t51 = b / t3 / t1;
        const Scalar t39 = a - b;
        const Scalar t6 = fabs(t39);
        const Scalar t7 = t6 * t6;
        const Scalar t8 = t7 * t7;
        const Scalar t37 = 0.1e1 / t8 / t6;
        const Scalar t12 = fabs(t39 * t37 + t51);
        const Scalar t50 = t12 / 0.4e1;
        const Scalar t13 = sqrt(a);
        const Scalar t48 = t13 * a * w[0];
        res[0] = t48 * t50;
        const Scalar t16 = t39 * t39;
        res[1] = (-t16 * t37 / 0.3e1 + (t50 + t51 / 0.3e1) * b) / a * t48;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<3,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t58 = a - b;
        const Scalar t55 = fabs(t58);
        const Scalar t1 = t55 * t55;
        const Scalar t77 = 0.1e1 / t1;
        const Scalar t57 = fabs(b);
        const Scalar t2 = t57 * t57;
        const Scalar t76 = 0.1e1 / t2;
        const Scalar t75 = b / t57 * t76;
        const Scalar t6 = sqrt(a);
        const Scalar t74 = w[0] * t6;
        const Scalar t73 = 0.1e1 / t55 * t77;
        const Scalar t71 = t76 * t75;
        const Scalar t54 = t77 * t73;
        const Scalar t10 = fabs(t58 * t54 + t71);
        const Scalar t70 = t10 / 0.4e1;
        const Scalar t69 = a * t74;
        const Scalar t13 = fabs(t58 * t73 + t75);
        res[0] = t13 * t74 / 0.2e1;
        res[1] = t69 * t70;
        const Scalar t15 = t58 * t58;
        res[2] = (-t15 * t54 / 0.3e1 + (t70 + t71 / 0.3e1) * b) / a * t69;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 3 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<3,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t80 = sqrt(0.1e1 / (a - 0.2e1 * b + c));
        const Scalar t81 = sqrt(0.1e1 / c);
        const Scalar t10 = b * b;
        const Scalar t82 = ((a - b) * t80 + b * t81) / (a * c - t10);
        return  w[0] * t82 + w[1] * (b * t82 - t80 + t81) / a;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<3,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t87 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t94 = sqrt(t87);
        const Scalar t86 = t94 * t87;
        const Scalar t92 = 0.1e1 / c;
        const Scalar t95 = sqrt(t92);
        const Scalar t88 = t95 * t92;
        const Scalar t4 = b * b;
        const Scalar t85 = 0.1e1 / (a * c - t4);
        const Scalar t89 = a - b;
        const Scalar t98 = (0.2e1 * a * (t89 * t94 + b * t95) * t85 + t89 * t86 + b * t88) * t85 / 0.3e1;
        const Scalar t83 = b * t98 - t86 / 0.3e1 + t88 / 0.3e1;
        const Scalar t93 = 0.1e1 / a;
        const Scalar t100 = t83 * t93;
        const Scalar t99 = w[1] * t93;
        const Scalar t91 = w[0];
        res[0] = t91 * t98 + t83 * t99;
        res[1] = t91 * t100 + (b * t100 + c * t98 - t86) * t99 / 0.2e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<3,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t107 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t115 = sqrt(t107);
        const Scalar t105 = t115 * t107;
        const Scalar t113 = 0.1e1 / c;
        const Scalar t116 = sqrt(t113);
        const Scalar t108 = t116 * t113;
        const Scalar t4 = b * b;
        const Scalar t104 = 0.1e1 / (a * c - t4);
        const Scalar t110 = a - b;
        const Scalar t121 = (t110 * t115 + b * t116) * t104;
        const Scalar t119 = (0.2e1 * a * t121 + t110 * t105 + b * t108) * t104 / 0.3e1;
        const Scalar t101 = b * t119 - t105 / 0.3e1 + t108 / 0.3e1;
        const Scalar t114 = 0.1e1 / a;
        const Scalar t122 = t101 * t114;
        const Scalar t120 = w[1] * t114;
        const Scalar t112 = w[0];
        res[0] = t112 * t121 + (b * t121 - t115 + t116) * t120;
        res[1] = t112 * t119 + t101 * t120;
        res[2] = t112 * t122 + (b * t122 + c * t119 - t105) * t120 / 0.2e1;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<3,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t134 = b / t2 / t1;
        const Scalar t126 = a - b;
        const Scalar t5 = fabs(t126);
        const Scalar t6 = t5 * t5;
        const Scalar t124 = 0.1e1 / t6 / t5;
        const Scalar t10 = fabs(t126 * t124 + t134);
        const Scalar t133 = t10 / 0.2e1;
        const Scalar t14 = t126 * t126;
        const Scalar t23 = sqrt(a);
        return  (w[0] * t133 + w[1] * (-t14 * t124 + (t133 + t134) * b) / a) * t23;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<3,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t141 = a - b;
        const Scalar t1 = fabs(t141);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t138 = 0.1e1 / t3 / t1;
        const Scalar t5 = fabs(b);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t139 = 0.1e1 / t7 / t5;
        const Scalar t12 = fabs(t141 * t138 + b * t139);
        const Scalar t159 = t12 / 0.4e1;
        const Scalar t147 = sqrt(a);
        const Scalar t142 = t147 * a;
        const Scalar t145 = b * b;
        const Scalar t13 = t141 * t141;
        const Scalar t156 = t13 * t138;
        const Scalar t136 = (b * t159 - t156 / 0.3e1 + t145 * t139 / 0.3e1) * t142;
        const Scalar t146 = 0.1e1 / a;
        const Scalar t157 = t136 * t146;
        const Scalar t155 = w[1] * t146;
        const Scalar t144 = w[0];
        res[0] = t144 * t142 * t159 + t136 * t155;
        res[1] = t144 * t157 + (b * t157 + t145 * t147 * t159 - t142 * t156) * t155 / 0.2e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<3,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t171 = a - b;
        const Scalar t166 = fabs(t171);
        const Scalar t1 = t166 * t166;
        const Scalar t192 = 0.1e1 / t1;
        const Scalar t170 = fabs(b);
        const Scalar t2 = t170 * t170;
        const Scalar t191 = 0.1e1 / t2;
        const Scalar t164 = 0.1e1 / t166 * t192;
        const Scalar t167 = 0.1e1 / t170 * t191;
        const Scalar t8 = fabs(t171 * t164 + b * t167);
        const Scalar t190 = t8 / 0.2e1;
        const Scalar t165 = t192 * t164;
        const Scalar t168 = t191 * t167;
        const Scalar t12 = fabs(t171 * t165 + b * t168);
        const Scalar t189 = t12 / 0.4e1;
        const Scalar t178 = sqrt(a);
        const Scalar t172 = t178 * a;
        const Scalar t176 = b * b;
        const Scalar t169 = t171 * t171;
        const Scalar t187 = t169 * t165;
        const Scalar t161 = (b * t189 - t187 / 0.3e1 + t176 * t168 / 0.3e1) * t172;
        const Scalar t177 = 0.1e1 / a;
        const Scalar t188 = t161 * t177;
        const Scalar t186 = w[1] * t177;
        const Scalar t175 = w[0];
        res[0] = (t175 * t190 + (b * t190 - t169 * t164 + t176 * t167) * t186) * t178;
        res[1] = t175 * t172 * t189 + t161 * t186;
        res[2] = t175 * t188 + (b * t188 + t176 * t178 * t189 - t172 * t187) * t186 / 0.2e1;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 3 //
    // and weight of degree 2      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<3,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t203 = a - b;
        const Scalar t197 = a - 0.2e1 * b + c;
        const Scalar t196 = sqrt(0.1e1 / t197);
        const Scalar t198 = sqrt(0.1e1 / c);
        const Scalar t200 = 0.1e1 / a;
        const Scalar t199 = a * c;
        const Scalar t7 = b * b;
        const Scalar t201 = (t203 * t196 + b * t198) / (t199 - t7);
        const Scalar t202 = (b * t201 - t196 + t198) * t200;
        const Scalar t19 = sqrt(a * t197);
        const Scalar t21 = sqrt(t199);
        const Scalar t25 = log((t19 + t203) / (-b + t21));
        const Scalar t26 = sqrt(a);
        return  w[0] * t201 + w[1] * t202 + w[2] * (b * t202 + t25 / t26 - t196) * t200;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<3,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t209 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t217 = sqrt(t209);
        const Scalar t208 = t217 * t209;
        const Scalar t4 = b * b;
        const Scalar t207 = 0.1e1 / (a * c - t4);
        const Scalar t215 = 0.1e1 / c;
        const Scalar t218 = sqrt(t215);
        const Scalar t210 = t218 * t215;
        const Scalar t211 = a - b;
        const Scalar t221 = (0.2e1 * a * (t211 * t217 + b * t218) * t207 + t211 * t208 + b * t210) * t207 / 0.3e1;
        const Scalar t205 = b * t221 - t208 / 0.3e1 + t210 / 0.3e1;
        const Scalar t216 = 0.1e1 / a;
        const Scalar t222 = t205 * t216;
        const Scalar t204 = b * t222 + c * t221 - t208;
        const Scalar t223 = t204 / 0.2e1;
        const Scalar t214 = w[0];
        const Scalar t213 = w[1];
        const Scalar t212 = w[2];
        res[0] = t214 * t221 + (t213 * t205 + t212 * t223) * t216;
        res[1] = t214 * t222 + t213 * t216 * t223 + t212 * (-t208 + (-b * t204 / 0.2e1 + 0.2e1 * c * t205) * t216) * t216;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<3,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t236 = a - b;
        const Scalar t233 = a - 0.2e1 * b + c;
        const Scalar t232 = 0.1e1 / t233;
        const Scalar t243 = sqrt(t232);
        const Scalar t230 = t243 * t232;
        const Scalar t237 = a * c;
        const Scalar t2 = b * b;
        const Scalar t229 = 0.1e1 / (t237 - t2);
        const Scalar t241 = 0.1e1 / c;
        const Scalar t244 = sqrt(t241);
        const Scalar t234 = t244 * t241;
        const Scalar t250 = (t236 * t243 + b * t244) * t229;
        const Scalar t247 = (0.2e1 * a * t250 + t236 * t230 + b * t234) * t229 / 0.3e1;
        const Scalar t225 = b * t247 - t230 / 0.3e1 + t234 / 0.3e1;
        const Scalar t242 = 0.1e1 / a;
        const Scalar t251 = t225 * t242;
        const Scalar t224 = b * t251 + c * t247 - t230;
        const Scalar t252 = t224 / 0.2e1;
        const Scalar t238 = w[2];
        const Scalar t249 = t238 * t242;
        const Scalar t239 = w[1];
        const Scalar t248 = t239 * t242;
        const Scalar t240 = w[0];
        const Scalar t227 = b * t250 - t243 + t244;
        const Scalar t24 = sqrt(a * t233);
        const Scalar t26 = sqrt(t237);
        const Scalar t30 = log((t24 + t236) / (-b + t26));
        const Scalar t31 = sqrt(a);
        res[0] = t240 * t250 + t227 * t248 + (b * t227 * t242 + t30 / t31 - t243) * t249;
        res[1] = t240 * t247 + (t239 * t225 + t238 * t252) * t242;
        res[2] = t240 * t251 + t248 * t252 + (-t230 + (-b * t224 / 0.2e1 + 0.2e1 * c * t225) * t242) * t249;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<3,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t270 = b / t2 / t1;
        const Scalar t258 = a - b;
        const Scalar t5 = fabs(t258);
        const Scalar t6 = t5 * t5;
        const Scalar t255 = 0.1e1 / t6 / t5;
        const Scalar t10 = fabs(t258 * t255 + t270);
        const Scalar t269 = t10 / 0.2e1;
        const Scalar t260 = 0.1e1 / a;
        const Scalar t261 = sqrt(a);
        const Scalar t11 = t258 * t258;
        const Scalar t266 = t11 * t255;
        const Scalar t267 = (-t266 + (t269 + t270) * b) * t261 * t260;
        const Scalar t25 = log(-t258 / b);
        const Scalar t26 = fabs(t25);
        return  w[0] * t261 * t269 + w[1] * t267 + w[2] * (b * t267 + t26 / t261 - t261 * t266) * t260;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<3,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t284 = 0.1e1 / a;
        const Scalar t297 = b * t284;
        const Scalar t278 = a - b;
        const Scalar t1 = fabs(t278);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t275 = 0.1e1 / t3 / t1;
        const Scalar t5 = fabs(b);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t276 = 0.1e1 / t7 / t5;
        const Scalar t12 = fabs(t278 * t275 + b * t276);
        const Scalar t296 = t12 / 0.4e1;
        const Scalar t13 = t278 * t278;
        const Scalar t294 = t13 * t275;
        const Scalar t285 = sqrt(a);
        const Scalar t279 = t285 * a;
        const Scalar t293 = t279 * t294;
        const Scalar t283 = b * b;
        const Scalar t282 = w[0];
        const Scalar t281 = w[1];
        const Scalar t280 = w[2];
        const Scalar t273 = (b * t296 - t294 / 0.3e1 + t283 * t276 / 0.3e1) * t279;
        const Scalar t272 = t273 * t297 + t283 * t285 * t296 - t293;
        res[0] = t282 * t279 * t296 + (t281 * t273 + t280 * t272 / 0.2e1) * t284;
        const Scalar t31 = a * a;
        res[1] = (t282 * t273 + t280 * (0.2e1 * t283 * t273 / t31 - t293) + (t281 / 0.2e1 - t280 * t297 / 0.2e1) * t272) * t284;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<3,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t311 = a - b;
        const Scalar t306 = fabs(t311);
        const Scalar t1 = t306 * t306;
        const Scalar t335 = 0.1e1 / t1;
        const Scalar t310 = fabs(b);
        const Scalar t2 = t310 * t310;
        const Scalar t334 = 0.1e1 / t2;
        const Scalar t304 = 0.1e1 / t306 * t335;
        const Scalar t307 = 0.1e1 / t310 * t334;
        const Scalar t8 = fabs(t311 * t304 + b * t307);
        const Scalar t333 = t8 / 0.2e1;
        const Scalar t305 = t335 * t304;
        const Scalar t308 = t334 * t307;
        const Scalar t12 = fabs(t311 * t305 + b * t308);
        const Scalar t332 = t12 / 0.4e1;
        const Scalar t317 = b * b;
        const Scalar t318 = 0.1e1 / a;
        const Scalar t319 = sqrt(a);
        const Scalar t309 = t311 * t311;
        const Scalar t330 = t309 * t304;
        const Scalar t331 = (b * t333 - t330 + t317 * t307) * t319 * t318;
        const Scalar t329 = t309 * t305;
        const Scalar t314 = w[2];
        const Scalar t328 = t314 * t318;
        const Scalar t312 = t319 * a;
        const Scalar t327 = t312 * t329;
        const Scalar t316 = w[0];
        const Scalar t315 = w[1];
        const Scalar t300 = (b * t332 - t329 / 0.3e1 + t317 * t308 / 0.3e1) * t312;
        const Scalar t299 = b * t300 * t318 + t317 * t319 * t332 - t327;
        const Scalar t32 = log(-t311 / b);
        const Scalar t33 = fabs(t32);
        res[0] = t316 * t319 * t333 + t315 * t331 + (b * t331 + t33 / t319 - t319 * t330) * t328;
        res[1] = t316 * t312 * t332 + (t315 * t300 + t314 * t299 / 0.2e1) * t318;
        const Scalar t48 = a * a;
        res[2] = (t316 * t300 + t314 * (0.2e1 * t317 * t300 / t48 - t327) + (t315 / 0.2e1 - b * t328 / 0.2e1) * t299) * t318;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 3 //
    // and weight of degree 3      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<3,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t347 = a - b;
        const Scalar t341 = a - 0.2e1 * b + c;
        const Scalar t340 = sqrt(0.1e1 / t341);
        const Scalar t342 = sqrt(0.1e1 / c);
        const Scalar t343 = a * c;
        const Scalar t7 = b * b;
        const Scalar t345 = (t347 * t340 + b * t342) / (t343 - t7);
        const Scalar t337 = b * t345 - t340 + t342;
        const Scalar t344 = 0.1e1 / a;
        const Scalar t346 = t337 * t344;
        const Scalar t13 = sqrt(a * t341);
        const Scalar t15 = sqrt(t343);
        const Scalar t19 = log((t13 + t347) / (-b + t15));
        const Scalar t20 = sqrt(a);
        const Scalar t336 = b * t346 + t19 / t20 - t340;
        return  w[0] * t345 + w[1] * t346 + w[2] * t336 * t344 - w[3] * (-t340 + (-0.3e1 * b * t336 + 0.2e1 * c * t337) * t344) * t344;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<3,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t361 = a - b;
        const Scalar t358 = a - 0.2e1 * b + c;
        const Scalar t357 = 0.1e1 / t358;
        const Scalar t369 = sqrt(t357);
        const Scalar t355 = t369 * t357;
        const Scalar t362 = a * c;
        const Scalar t2 = b * b;
        const Scalar t354 = 0.1e1 / (t362 - t2);
        const Scalar t367 = 0.1e1 / c;
        const Scalar t370 = sqrt(t367);
        const Scalar t359 = t370 * t367;
        const Scalar t374 = (t361 * t369 + b * t370) * t354;
        const Scalar t373 = (0.2e1 * a * t374 + t361 * t355 + b * t359) * t354 / 0.3e1;
        const Scalar t353 = -t355 / 0.3e1;
        const Scalar t350 = b * t373 + t353 + t359 / 0.3e1;
        const Scalar t368 = 0.1e1 / a;
        const Scalar t375 = t350 * t368;
        const Scalar t349 = b * t375 + c * t373 - t355;
        const Scalar t376 = t349 / 0.2e1;
        const Scalar t366 = w[0];
        const Scalar t365 = w[1];
        const Scalar t364 = w[2];
        const Scalar t363 = w[3];
        const Scalar t348 = -t355 + (-b * t349 / 0.2e1 + 0.2e1 * c * t350) * t368;
        res[0] = t366 * t373 + (t365 * t350 + t364 * t376 + t363 * t348) * t368;
        const Scalar t37 = sqrt(a * t358);
        const Scalar t39 = sqrt(t362);
        const Scalar t43 = log((t37 + t361) / (-b + t39));
        const Scalar t44 = sqrt(a);
        res[1] = t366 * t375 + t365 * t368 * t376 + t364 * t348 * t368 + t363 * (t353 + (b * t348 + t43 / t44 - t369 + b * (b * t374 - t369 + t370) * t368) * t368) * t368;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<3,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t411 = 0.2e1 * c;
        const Scalar t392 = a - b;
        const Scalar t389 = a - 0.2e1 * b + c;
        const Scalar t388 = 0.1e1 / t389;
        const Scalar t400 = sqrt(t388);
        const Scalar t386 = t400 * t388;
        const Scalar t393 = a * c;
        const Scalar t2 = b * b;
        const Scalar t385 = 0.1e1 / (t393 - t2);
        const Scalar t398 = 0.1e1 / c;
        const Scalar t401 = sqrt(t398);
        const Scalar t390 = t401 * t398;
        const Scalar t408 = (t392 * t400 + b * t401) * t385;
        const Scalar t404 = (0.2e1 * a * t408 + t392 * t386 + b * t390) * t385 / 0.3e1;
        const Scalar t384 = -t386 / 0.3e1;
        const Scalar t380 = b * t404 + t384 + t390 / 0.3e1;
        const Scalar t399 = 0.1e1 / a;
        const Scalar t409 = t380 * t399;
        const Scalar t378 = b * t409 + c * t404 - t386;
        const Scalar t410 = t378 / 0.2e1;
        const Scalar t394 = w[3];
        const Scalar t407 = t394 * t399;
        const Scalar t395 = w[2];
        const Scalar t406 = t395 * t399;
        const Scalar t396 = w[1];
        const Scalar t405 = t396 * t399;
        const Scalar t397 = w[0];
        const Scalar t382 = b * t408 - t400 + t401;
        const Scalar t22 = sqrt(a * t389);
        const Scalar t24 = sqrt(t393);
        const Scalar t28 = log((t22 + t392) / (-b + t24));
        const Scalar t29 = sqrt(a);
        const Scalar t379 = b * t382 * t399 + t28 / t29 - t400;
        const Scalar t377 = -t386 + (-b * t378 / 0.2e1 + t380 * t411) * t399;
        res[0] = t397 * t408 + t382 * t405 + t379 * t406 - (-t400 + (-0.3e1 * b * t379 + t382 * t411) * t399) * t407;
        res[1] = t397 * t404 + (t396 * t380 + t395 * t410 + t394 * t377) * t399;
        res[2] = t397 * t409 + t405 * t410 + t377 * t406 + (t384 + (b * t377 + t379) * t399) * t407;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<3,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t421 = 0.1e1 / a;
        const Scalar t431 = b * t421;
        const Scalar t418 = a - b;
        const Scalar t1 = fabs(t418);
        const Scalar t2 = t1 * t1;
        const Scalar t415 = 0.1e1 / t2 / t1;
        const Scalar t4 = fabs(b);
        const Scalar t5 = t4 * t4;
        const Scalar t416 = 0.1e1 / t5 / t4;
        const Scalar t10 = fabs(t418 * t415 + b * t416);
        const Scalar t430 = t10 / 0.2e1;
        const Scalar t11 = t418 * t418;
        const Scalar t428 = t11 * t415;
        const Scalar t422 = sqrt(a);
        const Scalar t427 = t422 * t428;
        const Scalar t420 = b * b;
        const Scalar t413 = (b * t430 - t428 + t420 * t416) * t422;
        const Scalar t18 = log(-t418 / b);
        const Scalar t19 = fabs(t18);
        const Scalar t412 = t413 * t431 + t19 / t422 - t427;
        const Scalar t30 = a * a;
        return  w[0] * t422 * t430 + (w[1] * t413 + w[2] * t412 + (-0.2e1 * t420 * t413 / t30 + t427 + 0.3e1 * t412 * t431) * w[3]) * t421;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<3,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t444 = a - b;
        const Scalar t439 = fabs(t444);
        const Scalar t1 = t439 * t439;
        const Scalar t470 = 0.1e1 / t1;
        const Scalar t443 = fabs(b);
        const Scalar t2 = t443 * t443;
        const Scalar t469 = 0.1e1 / t2;
        const Scalar t437 = 0.1e1 / t439 * t470;
        const Scalar t440 = 0.1e1 / t443 * t469;
        const Scalar t438 = t470 * t437;
        const Scalar t441 = t469 * t440;
        const Scalar t8 = fabs(t444 * t438 + b * t441);
        const Scalar t468 = t8 / 0.4e1;
        const Scalar t453 = sqrt(a);
        const Scalar t445 = t453 * a;
        const Scalar t451 = b * b;
        const Scalar t442 = t444 * t444;
        const Scalar t463 = t442 * t438;
        const Scalar t461 = -t463 / 0.3e1;
        const Scalar t435 = (b * t468 + t461 + t451 * t441 / 0.3e1) * t445;
        const Scalar t462 = t445 * t463;
        const Scalar t452 = 0.1e1 / a;
        const Scalar t465 = t435 * t452;
        const Scalar t434 = b * t465 + t451 * t453 * t468 - t462;
        const Scalar t466 = t434 * t452;
        const Scalar t20 = a * a;
        const Scalar t433 = -b * t466 / 0.2e1 + 0.2e1 * t451 * t435 / t20 - t462;
        const Scalar t467 = t433 * t452;
        const Scalar t464 = t442 * t437;
        const Scalar t450 = w[0];
        const Scalar t449 = w[1];
        const Scalar t448 = w[2];
        const Scalar t447 = w[3];
        res[0] = t450 * t445 * t468 + (t449 * t435 + t448 * t434 / 0.2e1 + t447 * t433) * t452;
        const Scalar t40 = fabs(t444 * t437 + b * t440);
        const Scalar t50 = log(-t444 / b);
        const Scalar t51 = fabs(t50);
        res[1] = t450 * t465 + t449 * t466 / 0.2e1 + t448 * t467 + t447 * (b * t467 + (b * (b * t40 / 0.2e1 - t464 + t451 * t440) * t453 * t452 + t51 / t453 - t453 * t464) * t452 + t445 * t461) * t452;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<3,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t485 = a - b;
        const Scalar t480 = fabs(t485);
        const Scalar t1 = t480 * t480;
        const Scalar t515 = 0.1e1 / t1;
        const Scalar t484 = fabs(b);
        const Scalar t2 = t484 * t484;
        const Scalar t514 = 0.1e1 / t2;
        const Scalar t478 = 0.1e1 / t480 * t515;
        const Scalar t481 = 0.1e1 / t484 * t514;
        const Scalar t8 = fabs(t485 * t478 + b * t481);
        const Scalar t513 = t8 / 0.2e1;
        const Scalar t479 = t515 * t478;
        const Scalar t482 = t514 * t481;
        const Scalar t12 = fabs(t485 * t479 + b * t482);
        const Scalar t512 = t12 / 0.4e1;
        const Scalar t492 = b * b;
        const Scalar t495 = sqrt(a);
        const Scalar t486 = t495 * a;
        const Scalar t483 = t485 * t485;
        const Scalar t508 = t483 * t479;
        const Scalar t505 = t486 * t508;
        const Scalar t503 = -t508 / 0.3e1;
        const Scalar t474 = (b * t512 + t503 + t492 * t482 / 0.3e1) * t486;
        const Scalar t493 = 0.1e1 / a;
        const Scalar t510 = t474 * t493;
        const Scalar t472 = b * t510 + t492 * t495 * t512 - t505;
        const Scalar t511 = t472 * t493;
        const Scalar t509 = t483 * t478;
        const Scalar t488 = w[3];
        const Scalar t507 = t488 * t493;
        const Scalar t21 = a * a;
        const Scalar t506 = 0.2e1 * t492 / t21;
        const Scalar t504 = t495 * t509;
        const Scalar t491 = w[0];
        const Scalar t490 = w[1];
        const Scalar t489 = w[2];
        const Scalar t475 = (b * t513 - t509 + t492 * t481) * t495;
        const Scalar t31 = log(-t485 / b);
        const Scalar t32 = fabs(t31);
        const Scalar t473 = b * t475 * t493 + t32 / t495 - t504;
        const Scalar t471 = -b * t511 / 0.2e1 + t474 * t506 - t505;
        res[0] = t491 * t495 * t513 + (t490 * t475 - t488 * (t475 * t506 - t504) + (t489 + 0.3e1 * b * t507) * t473) * t493;
        res[1] = t491 * t486 * t512 + (t490 * t474 + t489 * t472 / 0.2e1 + t488 * t471) * t493;
        res[2] = t491 * t510 + t490 * t511 / 0.2e1 + t489 * t471 * t493 + (t486 * t503 + (b * t471 + t473) * t493) * t507;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 4 //
    // and weight of degree 0      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<4,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t519 = a - b;
        const Scalar t2 = b * b;
        const Scalar t518 = a * c - t2;
        const Scalar t517 = 0.1e1 / t518;
        const Scalar t516 = sqrt(t518);
        const Scalar t4 = sqrt(t517);
        const Scalar t6 = atan2(t519, t516);
        const Scalar t7 = atan2(-b, t516);
        return  w[0] * (a * t4 * (t6 - t7) + t519 / (a - 0.2e1 * b + c) + b / c) * t517 / 0.2e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<4,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t2 = b * b;
        const Scalar t523 = a * c - t2;
        const Scalar t521 = sqrt(t523);
        const Scalar t522 = 0.1e1 / t523;
        const Scalar t525 = a - 0.2e1 * b + c;
        const Scalar t4 = t525 * t525;
        const Scalar t524 = 0.1e1 / t4;
        const Scalar t526 = a - b;
        const Scalar t5 = c * c;
        const Scalar t528 = 0.1e1 / t5;
        const Scalar t6 = sqrt(t522);
        const Scalar t8 = atan2(t526, t521);
        const Scalar t9 = atan2(-b, t521);
        const Scalar t529 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t526 / t525 + b / c) * t522 + t526 * t524 + b * t528) * t522 / 0.4e1;
        const Scalar t527 = w[0];
        res[0] = t527 * t529;
        res[1] = t527 * (b * t529 - t524 / 0.4e1 + t528 / 0.4e1) / a;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<4,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t2 = b * b;
        const Scalar t534 = a * c - t2;
        const Scalar t532 = sqrt(t534);
        const Scalar t533 = 0.1e1 / t534;
        const Scalar t536 = a - 0.2e1 * b + c;
        const Scalar t537 = a - b;
        const Scalar t4 = sqrt(t533);
        const Scalar t6 = atan2(t537, t532);
        const Scalar t7 = atan2(-b, t532);
        const Scalar t541 = (a * t4 * (t6 - t7) + t537 / t536 + b / c) * t533;
        const Scalar t15 = t536 * t536;
        const Scalar t535 = 0.1e1 / t15;
        const Scalar t16 = c * c;
        const Scalar t539 = 0.1e1 / t16;
        const Scalar t540 = (0.3e1 / 0.2e1 * a * t541 + t537 * t535 + b * t539) * t533 / 0.4e1;
        const Scalar t538 = w[0];
        res[0] = t538 * t541 / 0.2e1;
        res[1] = t538 * t540;
        res[2] = t538 * (b * t540 - t535 / 0.4e1 + t539 / 0.4e1) / a;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<4,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t542 = a - b;
        const Scalar t3 = fabs(t542);
        const Scalar t4 = t3 * t3;
        const Scalar t5 = t4 * t4;
        const Scalar t8 = fabs(b);
        const Scalar t9 = t8 * t8;
        const Scalar t10 = t9 * t9;
        const Scalar t14 = fabs(t542 / t5 + b / t10);
        return  w[0] * a * t14 / 0.3e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<4,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t564 = b / t3 / t2;
        const Scalar t552 = a - b;
        const Scalar t6 = fabs(t552);
        const Scalar t7 = t6 * t6;
        const Scalar t8 = t7 * t7;
        const Scalar t550 = 0.1e1 / t8 / t7;
        const Scalar t12 = fabs(t552 * t550 + t564);
        const Scalar t563 = t12 / 0.5e1;
        const Scalar t14 = a * a;
        const Scalar t561 = w[0] * t14;
        res[0] = t561 * t563;
        const Scalar t15 = t552 * t552;
        res[1] = (-t15 * t550 / 0.4e1 + (t563 + t564 / 0.4e1) * b) / a * t561;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<4,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1 = fabs(b);
        const Scalar t577 = t1 * t1;
        const Scalar t2 = t577 * t577;
        const Scalar t585 = b / t2;
        const Scalar t571 = a - b;
        const Scalar t4 = fabs(t571);
        const Scalar t574 = t4 * t4;
        const Scalar t5 = t574 * t574;
        const Scalar t584 = 0.1e1 / t5;
        const Scalar t582 = 0.1e1 / t577 * t585;
        const Scalar t567 = 0.1e1 / t574 * t584;
        const Scalar t10 = fabs(t571 * t567 + t582);
        const Scalar t581 = t10 / 0.5e1;
        const Scalar t572 = w[0];
        const Scalar t11 = a * a;
        const Scalar t580 = t572 * t11;
        const Scalar t15 = fabs(t571 * t584 + t585);
        res[0] = t572 * a * t15 / 0.3e1;
        res[1] = t580 * t581;
        const Scalar t17 = t571 * t571;
        res[2] = (-t17 * t567 / 0.4e1 + (t581 + t582 / 0.4e1) * b) / a * t580;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 4 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<4,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t2 = b * b;
        const Scalar t589 = a * c - t2;
        const Scalar t587 = sqrt(t589);
        const Scalar t588 = 0.1e1 / t589;
        const Scalar t590 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t591 = a - b;
        const Scalar t592 = 0.1e1 / c;
        const Scalar t5 = sqrt(t588);
        const Scalar t7 = atan2(t591, t587);
        const Scalar t8 = atan2(-b, t587);
        const Scalar t593 = (a * t5 * (t7 - t8) + t591 * t590 + b * t592) * t588 / 0.2e1;
        return  w[0] * t593 + w[1] * (b * t593 - t590 / 0.2e1 + t592 / 0.2e1) / a;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<4,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t602 = w[1];
        const Scalar t605 = 0.1e1 / a;
        const Scalar t607 = t602 * t605;
        const Scalar t2 = b * b;
        const Scalar t598 = a * c - t2;
        const Scalar t596 = sqrt(t598);
        const Scalar t597 = 0.1e1 / t598;
        const Scalar t600 = a - 0.2e1 * b + c;
        const Scalar t4 = t600 * t600;
        const Scalar t599 = 0.1e1 / t4;
        const Scalar t601 = a - b;
        const Scalar t5 = c * c;
        const Scalar t604 = 0.1e1 / t5;
        const Scalar t6 = sqrt(t597);
        const Scalar t8 = atan2(t601, t596);
        const Scalar t9 = atan2(-b, t596);
        const Scalar t606 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t601 / t600 + b / c) * t597 + t601 * t599 + b * t604) * t597 / 0.4e1;
        const Scalar t603 = w[0];
        const Scalar t594 = b * t606 - t599 / 0.4e1 + t604 / 0.4e1;
        res[0] = t603 * t606 + t594 * t607;
        res[1] = (t602 * (c * t606 - t599) / 0.3e1 + (t603 + 0.2e1 / 0.3e1 * b * t607) * t594) * t605;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<4,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t2 = b * b;
        const Scalar t613 = a * c - t2;
        const Scalar t611 = sqrt(t613);
        const Scalar t612 = 0.1e1 / t613;
        const Scalar t616 = a - 0.2e1 * b + c;
        const Scalar t614 = 0.1e1 / t616;
        const Scalar t617 = a - b;
        const Scalar t620 = 0.1e1 / c;
        const Scalar t4 = sqrt(t612);
        const Scalar t6 = atan2(t617, t611);
        const Scalar t7 = atan2(-b, t611);
        const Scalar t626 = (a * t4 * (t6 - t7) + t617 * t614 + b * t620) * t612;
        const Scalar t618 = w[1];
        const Scalar t622 = 0.1e1 / a;
        const Scalar t625 = t618 * t622;
        const Scalar t13 = t616 * t616;
        const Scalar t615 = 0.1e1 / t13;
        const Scalar t14 = c * c;
        const Scalar t621 = 0.1e1 / t14;
        const Scalar t624 = (0.3e1 / 0.2e1 * a * t626 + t617 * t615 + b * t621) * t612 / 0.4e1;
        const Scalar t623 = t626 / 0.2e1;
        const Scalar t619 = w[0];
        const Scalar t608 = b * t624 - t615 / 0.4e1 + t621 / 0.4e1;
        res[0] = t619 * t623 + (b * t623 - t614 / 0.2e1 + t620 / 0.2e1) * t625;
        res[1] = t619 * t624 + t608 * t625;
        res[2] = (t618 * (c * t624 - t615) / 0.3e1 + (t619 + 0.2e1 / 0.3e1 * b * t625) * t608) * t622;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<4,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t637 = b / t3;
        const Scalar t630 = a - b;
        const Scalar t5 = fabs(t630);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t628 = 0.1e1 / t7;
        const Scalar t10 = fabs(t630 * t628 + t637);
        const Scalar t636 = t10 / 0.3e1;
        const Scalar t15 = t630 * t630;
        return  w[0] * a * t636 + w[1] * (-t15 * t628 / 0.2e1 + (t636 + t637 / 0.2e1) * b);

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<4,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t644 = a - b;
        const Scalar t1 = fabs(t644);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t641 = 0.1e1 / t3 / t2;
        const Scalar t5 = fabs(b);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t642 = 0.1e1 / t7 / t6;
        const Scalar t12 = fabs(t644 * t641 + b * t642);
        const Scalar t659 = t12 / 0.5e1;
        const Scalar t13 = t644 * t644;
        const Scalar t657 = t13 * t641;
        const Scalar t645 = w[1];
        const Scalar t649 = 0.1e1 / a;
        const Scalar t656 = t645 * t649;
        const Scalar t648 = a * a;
        const Scalar t647 = b * b;
        const Scalar t646 = w[0];
        const Scalar t639 = (b * t659 - t657 / 0.4e1 + t647 * t642 / 0.4e1) * t648;
        res[0] = t646 * t648 * t659 + t639 * t656;
        res[1] = (t645 * (t647 * a * t659 - t648 * t657) / 0.3e1 + (t646 + 0.2e1 / 0.3e1 * b * t656) * t639) * t649;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<4,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t671 = a - b;
        const Scalar t1 = fabs(t671);
        const Scalar t677 = t1 * t1;
        const Scalar t2 = t677 * t677;
        const Scalar t664 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t680 = t3 * t3;
        const Scalar t4 = t680 * t680;
        const Scalar t667 = 0.1e1 / t4;
        const Scalar t8 = fabs(t671 * t664 + b * t667);
        const Scalar t686 = t8 / 0.3e1;
        const Scalar t665 = 0.1e1 / t677 * t664;
        const Scalar t668 = 0.1e1 / t680 * t667;
        const Scalar t14 = fabs(t671 * t665 + b * t668);
        const Scalar t685 = t14 / 0.5e1;
        const Scalar t669 = t671 * t671;
        const Scalar t684 = t669 * t665;
        const Scalar t672 = w[1];
        const Scalar t676 = 0.1e1 / a;
        const Scalar t683 = t672 * t676;
        const Scalar t675 = a * a;
        const Scalar t674 = b * b;
        const Scalar t673 = w[0];
        const Scalar t661 = (b * t685 - t684 / 0.4e1 + t674 * t668 / 0.4e1) * t675;
        res[0] = (t673 * t686 + (b * t686 - t669 * t664 / 0.2e1 + t674 * t667 / 0.2e1) * t683) * a;
        res[1] = t673 * t675 * t685 + t661 * t683;
        res[2] = (t672 * (t674 * a * t685 - t675 * t684) / 0.3e1 + (t673 + 0.2e1 / 0.3e1 * b * t683) * t661) * t676;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 4 //
    // and weight of degree 2      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<4,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t2 = b * b;
        const Scalar t690 = a * c - t2;
        const Scalar t688 = sqrt(t690);
        const Scalar t689 = 0.1e1 / t690;
        const Scalar t691 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t692 = a - b;
        const Scalar t693 = 0.1e1 / c;
        const Scalar t5 = sqrt(t689);
        const Scalar t7 = atan2(t692, t688);
        const Scalar t8 = atan2(-b, t688);
        const Scalar t695 = (a * t5 * (t7 - t8) + t692 * t691 + b * t693) * t689 / 0.2e1;
        return  w[0] * t695 + (w[1] * (b * t695 - t691 / 0.2e1 + t693 / 0.2e1) + w[2] * (c * t695 - t691)) / a;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<4,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t703 = a - 0.2e1 * b + c;
        const Scalar t2 = t703 * t703;
        const Scalar t702 = 0.1e1 / t2;
        const Scalar t3 = c * c;
        const Scalar t708 = 0.1e1 / t3;
        const Scalar t5 = b * b;
        const Scalar t701 = a * c - t5;
        const Scalar t699 = sqrt(t701);
        const Scalar t700 = 0.1e1 / t701;
        const Scalar t704 = a - b;
        const Scalar t6 = sqrt(t700);
        const Scalar t8 = atan2(t704, t699);
        const Scalar t9 = atan2(-b, t699);
        const Scalar t710 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t704 / t703 + b / c) * t700 + t704 * t702 + b * t708) * t700 / 0.4e1;
        const Scalar t697 = b * t710 - t702 / 0.4e1 + t708 / 0.4e1;
        const Scalar t709 = 0.1e1 / a;
        const Scalar t711 = 0.2e1 / 0.3e1 * b * t697 * t709 + c * t710 / 0.3e1 - t702 / 0.3e1;
        const Scalar t707 = w[0];
        const Scalar t706 = w[1];
        const Scalar t705 = w[2];
        res[0] = t707 * t710 + (t706 * t697 + t705 * t711) * t709;
        res[1] = (t706 * t711 - t705 * t702 / 0.2e1 + (t707 + t705 * c * t709) * t697) * t709;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<4,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t721 = a - 0.2e1 * b + c;
        const Scalar t2 = t721 * t721;
        const Scalar t720 = 0.1e1 / t2;
        const Scalar t3 = c * c;
        const Scalar t727 = 0.1e1 / t3;
        const Scalar t5 = b * b;
        const Scalar t718 = a * c - t5;
        const Scalar t717 = 0.1e1 / t718;
        const Scalar t722 = a - b;
        const Scalar t716 = sqrt(t718);
        const Scalar t719 = 0.1e1 / t721;
        const Scalar t726 = 0.1e1 / c;
        const Scalar t6 = sqrt(t717);
        const Scalar t8 = atan2(t722, t716);
        const Scalar t9 = atan2(-b, t716);
        const Scalar t731 = (a * t6 * (t8 - t9) + t722 * t719 + b * t726) * t717;
        const Scalar t730 = (0.3e1 / 0.2e1 * a * t731 + t722 * t720 + b * t727) * t717 / 0.4e1;
        const Scalar t713 = b * t730 - t720 / 0.4e1 + t727 / 0.4e1;
        const Scalar t728 = 0.1e1 / a;
        const Scalar t732 = 0.2e1 / 0.3e1 * b * t713 * t728 + c * t730 / 0.3e1 - t720 / 0.3e1;
        const Scalar t729 = t731 / 0.2e1;
        const Scalar t725 = w[0];
        const Scalar t724 = w[1];
        const Scalar t723 = w[2];
        res[0] = t725 * t729 + (t724 * (b * t729 - t719 / 0.2e1 + t726 / 0.2e1) + t723 * (c * t729 - t719)) * t728;
        res[1] = t725 * t730 + (t724 * t713 + t723 * t732) * t728;
        res[2] = (t724 * t732 - t723 * t720 / 0.2e1 + (t725 + t723 * c * t728) * t713) * t728;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<4,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t737 = a - b;
        const Scalar t1 = fabs(t737);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t734 = 0.1e1 / t3;
        const Scalar t4 = fabs(b);
        const Scalar t5 = t4 * t4;
        const Scalar t6 = t5 * t5;
        const Scalar t735 = 0.1e1 / t6;
        const Scalar t10 = fabs(t737 * t734 + b * t735);
        const Scalar t746 = t10 / 0.3e1;
        const Scalar t11 = t737 * t737;
        const Scalar t744 = t11 * t734;
        const Scalar t738 = b * b;
        return  w[0] * a * t746 + (w[1] * (b * t746 - t744 / 0.2e1 + t738 * t735 / 0.2e1) * a + w[2] * (t738 * t746 - a * t744)) / a;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<4,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t752 = 0.1e1 / t3 / t2;
        const Scalar t758 = b * b;
        const Scalar t759 = a * a;
        const Scalar t754 = a - b;
        const Scalar t5 = fabs(t754);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t751 = 0.1e1 / t7 / t6;
        const Scalar t9 = t754 * t754;
        const Scalar t768 = t9 * t751;
        const Scalar t13 = fabs(t754 * t751 + b * t752);
        const Scalar t770 = t13 / 0.5e1;
        const Scalar t749 = (b * t770 - t768 / 0.4e1 + t758 * t752 / 0.4e1) * t759;
        const Scalar t760 = 0.1e1 / a;
        const Scalar t767 = t759 * t768;
        const Scalar t771 = 0.2e1 / 0.3e1 * b * t749 * t760 + t758 * a * t770 / 0.3e1 - t767 / 0.3e1;
        const Scalar t757 = w[0];
        const Scalar t756 = w[1];
        const Scalar t755 = w[2];
        res[0] = t757 * t759 * t770 + (t756 * t749 + t755 * t771) * t760;
        res[1] = (t756 * t771 - t755 * t767 / 0.2e1 + (t757 + t755 * t758 / t759) * t749) * t760;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<4,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t784 = a - b;
        const Scalar t1 = fabs(t784);
        const Scalar t791 = t1 * t1;
        const Scalar t2 = t791 * t791;
        const Scalar t777 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t794 = t3 * t3;
        const Scalar t4 = t794 * t794;
        const Scalar t780 = 0.1e1 / t4;
        const Scalar t781 = 0.1e1 / t794 * t780;
        const Scalar t788 = b * b;
        const Scalar t789 = a * a;
        const Scalar t778 = 0.1e1 / t791 * t777;
        const Scalar t782 = t784 * t784;
        const Scalar t798 = t782 * t778;
        const Scalar t10 = fabs(t784 * t778 + b * t781);
        const Scalar t800 = t10 / 0.5e1;
        const Scalar t774 = (b * t800 - t798 / 0.4e1 + t788 * t781 / 0.4e1) * t789;
        const Scalar t790 = 0.1e1 / a;
        const Scalar t797 = t789 * t798;
        const Scalar t802 = 0.2e1 / 0.3e1 * b * t774 * t790 + t788 * a * t800 / 0.3e1 - t797 / 0.3e1;
        const Scalar t26 = fabs(t784 * t777 + b * t780);
        const Scalar t801 = t26 / 0.3e1;
        const Scalar t799 = t782 * t777;
        const Scalar t787 = w[0];
        const Scalar t786 = w[1];
        const Scalar t785 = w[2];
        res[0] = t787 * a * t801 + (t786 * (b * t801 - t799 / 0.2e1 + t788 * t780 / 0.2e1) * a + t785 * (t788 * t801 - a * t799)) * t790;
        res[1] = t787 * t789 * t800 + (t786 * t774 + t785 * t802) * t790;
        res[2] = (t786 * t802 - t785 * t797 / 0.2e1 + (t787 + t785 * t788 / t789) * t774) * t790;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 4 //
    // and weight of degree 3      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<4,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t2 = b * b;
        const Scalar t810 = a * c - t2;
        const Scalar t807 = sqrt(t810);
        const Scalar t809 = 0.1e1 / t810;
        const Scalar t813 = a - b;
        const Scalar t3 = sqrt(t809);
        const Scalar t4 = atan2(t813, t807);
        const Scalar t5 = atan2(-b, t807);
        const Scalar t817 = t3 * (t4 - t5);
        const Scalar t812 = a - 0.2e1 * b + c;
        const Scalar t811 = 0.1e1 / t812;
        const Scalar t814 = 0.1e1 / c;
        const Scalar t816 = (a * t817 + t813 * t811 + b * t814) * t809 / 0.2e1;
        const Scalar t815 = 0.1e1 / a;
        const Scalar t808 = -t811 / 0.2e1;
        const Scalar t803 = c * t816 - t811;
        const Scalar t25 = log(t812 * t814);
        return  w[0] * t816 + (w[1] * (b * t816 + t808 + t814 / 0.2e1) + w[2] * t803 + (t808 + (t25 / 0.2e1 + (t803 + t817) * b) * t815) * w[3]) * t815;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<4,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t826 = a - 0.2e1 * b + c;
        const Scalar t2 = t826 * t826;
        const Scalar t825 = 0.1e1 / t2;
        const Scalar t4 = b * b;
        const Scalar t824 = a * c - t4;
        const Scalar t822 = sqrt(t824);
        const Scalar t823 = 0.1e1 / t824;
        const Scalar t827 = a - b;
        const Scalar t5 = c * c;
        const Scalar t832 = 0.1e1 / t5;
        const Scalar t6 = sqrt(t823);
        const Scalar t8 = atan2(t827, t822);
        const Scalar t9 = atan2(-b, t822);
        const Scalar t834 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t827 / t826 + b / c) * t823 + t827 * t825 + b * t832) * t823 / 0.4e1;
        const Scalar t820 = b * t834 - t825 / 0.4e1 + t832 / 0.4e1;
        const Scalar t833 = 0.1e1 / a;
        const Scalar t836 = t820 * t833;
        const Scalar t835 = 0.2e1 * t836;
        const Scalar t818 = b * t835 + c * t834 - t825;
        const Scalar t838 = t818 / 0.3e1;
        const Scalar t819 = c * t835 - t825;
        const Scalar t837 = t819 / 0.2e1;
        const Scalar t831 = w[0];
        const Scalar t830 = w[1];
        const Scalar t829 = w[2];
        const Scalar t828 = w[3];
        res[0] = t831 * t834 + (t830 * t820 + t829 * t838 + t828 * t837) * t833;
        res[1] = t831 * t836 + (t830 * t838 + t829 * t837 + (-t825 + (-b * t819 + c * t818) * t833) * t828) * t833;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<4,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t853 = a - 0.2e1 * b + c;
        const Scalar t2 = t853 * t853;
        const Scalar t852 = 0.1e1 / t2;
        const Scalar t4 = b * b;
        const Scalar t850 = a * c - t4;
        const Scalar t849 = 0.1e1 / t850;
        const Scalar t854 = a - b;
        const Scalar t5 = c * c;
        const Scalar t860 = 0.1e1 / t5;
        const Scalar t851 = 0.1e1 / t853;
        const Scalar t859 = 0.1e1 / c;
        const Scalar t847 = sqrt(t850);
        const Scalar t6 = sqrt(t849);
        const Scalar t7 = atan2(t854, t847);
        const Scalar t8 = atan2(-b, t847);
        const Scalar t868 = t6 * (t7 - t8);
        const Scalar t869 = (a * t868 + t854 * t851 + b * t859) * t849;
        const Scalar t863 = (0.3e1 / 0.2e1 * a * t869 + t854 * t852 + b * t860) * t849 / 0.4e1;
        const Scalar t841 = b * t863 - t852 / 0.4e1 + t860 / 0.4e1;
        const Scalar t861 = 0.1e1 / a;
        const Scalar t870 = t841 * t861;
        const Scalar t864 = 0.2e1 * t870;
        const Scalar t839 = b * t864 + c * t863 - t852;
        const Scalar t872 = t839 / 0.3e1;
        const Scalar t840 = c * t864 - t852;
        const Scalar t871 = t840 / 0.2e1;
        const Scalar t855 = w[3];
        const Scalar t867 = t855 * t861;
        const Scalar t856 = w[2];
        const Scalar t866 = t856 * t861;
        const Scalar t857 = w[1];
        const Scalar t865 = t857 * t861;
        const Scalar t862 = t869 / 0.2e1;
        const Scalar t858 = w[0];
        const Scalar t848 = -t851 / 0.2e1;
        const Scalar t843 = c * t862 - t851;
        const Scalar t35 = log(t853 * t859);
        res[0] = t858 * t862 + (b * t862 + t848 + t859 / 0.2e1) * t865 + t843 * t866 + (t848 + (t35 / 0.2e1 + (t843 + t868) * b) * t861) * t867;
        res[1] = t858 * t863 + (t857 * t841 + t856 * t872 + t855 * t871) * t861;
        res[2] = t858 * t870 + t865 * t872 + t866 * t871 + (-t852 + (-b * t840 + c * t839) * t861) * t867;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<4,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t880 = a - b;
        const Scalar t1 = fabs(t880);
        const Scalar t883 = t1 * t1;
        const Scalar t2 = t883 * t883;
        const Scalar t875 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t885 = t3 * t3;
        const Scalar t4 = t885 * t885;
        const Scalar t877 = 0.1e1 / t4;
        const Scalar t8 = fabs(t880 * t875 + b * t877);
        const Scalar t889 = t8 / 0.3e1;
        const Scalar t9 = t880 * t880;
        const Scalar t888 = t9 * t875;
        const Scalar t887 = -t888 / 0.2e1;
        const Scalar t882 = 0.1e1 / a;
        const Scalar t881 = b * b;
        const Scalar t873 = t881 * t889 - a * t888;
        const Scalar t32 = fabs(t880 / t883 + b / t885);
        const Scalar t36 = log(-t880 / b);
        return  w[0] * a * t889 + (w[1] * (b * t889 + t887 + t881 * t877 / 0.2e1) * a + w[2] * t873 + (a * t887 + (b * t873 + b * t32 + t36) * t882) * w[3]) * t882;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<4,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t895 = 0.1e1 / t3 / t2;
        const Scalar t902 = b * b;
        const Scalar t903 = a * a;
        const Scalar t897 = a - b;
        const Scalar t5 = fabs(t897);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t894 = 0.1e1 / t7 / t6;
        const Scalar t9 = t897 * t897;
        const Scalar t914 = t9 * t894;
        const Scalar t13 = fabs(t897 * t894 + b * t895);
        const Scalar t916 = t13 / 0.5e1;
        const Scalar t892 = (b * t916 - t914 / 0.4e1 + t902 * t895 / 0.4e1) * t903;
        const Scalar t920 = 0.2e1 * t892;
        const Scalar t904 = 0.1e1 / a;
        const Scalar t918 = b * t904;
        const Scalar t912 = t903 * t914;
        const Scalar t890 = t918 * t920 + t902 * a * t916 - t912;
        const Scalar t917 = t890 / 0.3e1;
        const Scalar t913 = t902 / t903;
        const Scalar t901 = w[0];
        const Scalar t900 = w[1];
        const Scalar t899 = w[2];
        const Scalar t898 = w[3];
        const Scalar t891 = t913 * t920 - t912;
        res[0] = t901 * t903 * t916 + (t900 * t892 + t899 * t917 + t898 * t891 / 0.2e1) * t904;
        res[1] = (t901 * t892 + t900 * t917 + t898 * (t890 * t913 - t912) + (t899 / 0.2e1 - t898 * t918) * t891) * t904;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<4,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t934 = a - b;
        const Scalar t1 = fabs(t934);
        const Scalar t943 = t1 * t1;
        const Scalar t960 = 0.1e1 / t943;
        const Scalar t2 = t943 * t943;
        const Scalar t927 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t946 = t3 * t3;
        const Scalar t959 = 0.1e1 / t946;
        const Scalar t4 = t946 * t946;
        const Scalar t930 = 0.1e1 / t4;
        const Scalar t931 = t959 * t930;
        const Scalar t939 = b * b;
        const Scalar t940 = a * a;
        const Scalar t928 = t960 * t927;
        const Scalar t932 = t934 * t934;
        const Scalar t953 = t932 * t928;
        const Scalar t8 = fabs(t934 * t928 + b * t931);
        const Scalar t955 = t8 / 0.5e1;
        const Scalar t923 = (b * t955 - t953 / 0.4e1 + t939 * t931 / 0.4e1) * t940;
        const Scalar t958 = 0.2e1 * t923;
        const Scalar t941 = 0.1e1 / a;
        const Scalar t950 = t940 * t953;
        const Scalar t921 = b * t941 * t958 + t939 * a * t955 - t950;
        const Scalar t957 = t921 / 0.3e1;
        const Scalar t21 = fabs(t934 * t927 + b * t930);
        const Scalar t956 = t21 / 0.3e1;
        const Scalar t954 = t932 * t927;
        const Scalar t935 = w[3];
        const Scalar t952 = t935 * t941;
        const Scalar t951 = t939 / t940;
        const Scalar t949 = -t954 / 0.2e1;
        const Scalar t938 = w[0];
        const Scalar t937 = w[1];
        const Scalar t936 = w[2];
        const Scalar t924 = t939 * t956 - a * t954;
        const Scalar t922 = t951 * t958 - t950;
        const Scalar t32 = fabs(t934 * t960 + b * t959);
        const Scalar t36 = log(-t934 / b);
        res[0] = (t936 * t924 + (b * t924 + b * t32 + t36) * t952) * t941 + (t938 * t956 + t937 * (b * t956 + t949 + t939 * t930 / 0.2e1) * t941 + t949 * t952) * a;
        res[1] = t938 * t940 * t955 + (t937 * t923 + t936 * t957 + t935 * t922 / 0.2e1) * t941;
        res[2] = (t938 * t923 + t937 * t957 + t935 * (t921 * t951 - t950) + (t936 / 0.2e1 - b * t952) * t922) * t941;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 5 //
    // and weight of degree 0      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<5,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t964 = 0.1e1 / c;
        const Scalar t1 = sqrt(t964);
        const Scalar t970 = b * t1;
        const Scalar t962 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t5 = sqrt(t962);
        const Scalar t969 = (a - b) * t5;
        const Scalar t7 = b * b;
        const Scalar t961 = 0.1e1 / (a * c - t7);
        return  w[0] * (0.2e1 * a * (t969 + t970) * t961 + t962 * t969 + t964 * t970) * t961 / 0.3e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<5,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t2 = b * b;
        const Scalar t972 = 0.1e1 / (a * c - t2);
        const Scalar t986 = a * t972;
        const Scalar t974 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t979 = sqrt(t974);
        const Scalar t981 = t979 * t974;
        const Scalar t973 = t974 * t981;
        const Scalar t978 = 0.1e1 / c;
        const Scalar t980 = sqrt(t978);
        const Scalar t983 = t980 * t978;
        const Scalar t975 = t978 * t983;
        const Scalar t976 = a - b;
        const Scalar t985 = (0.4e1 / 0.3e1 * (0.2e1 * (t976 * t979 + b * t980) * t986 + t976 * t981 + b * t983) * t986 + t976 * t973 + b * t975) * t972 / 0.5e1;
        const Scalar t977 = w[0];
        res[0] = t977 * t985;
        res[1] = t977 * (b * t985 - t973 / 0.5e1 + t975 / 0.5e1) / a;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<5,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t995 = 0.1e1 / c;
        const Scalar t997 = sqrt(t995);
        const Scalar t1000 = t997 * t995;
        const Scalar t2 = b * b;
        const Scalar t989 = 0.1e1 / (a * c - t2);
        const Scalar t993 = a - b;
        const Scalar t991 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t996 = sqrt(t991);
        const Scalar t998 = t996 * t991;
        const Scalar t1003 = (0.2e1 * a * (t993 * t996 + b * t997) * t989 + t993 * t998 + b * t1000) * t989;
        const Scalar t990 = t991 * t998;
        const Scalar t992 = t995 * t1000;
        const Scalar t1002 = (0.4e1 / 0.3e1 * a * t1003 + t993 * t990 + b * t992) * t989 / 0.5e1;
        const Scalar t994 = w[0];
        res[0] = t994 * t1003 / 0.3e1;
        res[1] = t994 * t1002;
        res[2] = t994 * (b * t1002 - t990 / 0.5e1 + t992 / 0.5e1) / a;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<5,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t1004 = a - b;
        const Scalar t2 = sqrt(a);
        const Scalar t5 = fabs(t1004);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t11 = fabs(b);
        const Scalar t12 = t11 * t11;
        const Scalar t13 = t12 * t12;
        const Scalar t18 = fabs(t1004 / t7 / t5 + b / t13 / t11);
        return  w[0] * t2 * a * t18 / 0.4e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<5,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t4 = t2 * t2;
        const Scalar t1030 = b / t4 / t2 / t1;
        const Scalar t1016 = a - b;
        const Scalar t7 = fabs(t1016);
        const Scalar t8 = t7 * t7;
        const Scalar t10 = t8 * t8;
        const Scalar t1014 = 0.1e1 / t10 / t8 / t7;
        const Scalar t14 = fabs(t1016 * t1014 + t1030);
        const Scalar t1029 = t14 / 0.6e1;
        const Scalar t15 = a * a;
        const Scalar t16 = sqrt(a);
        const Scalar t1027 = t16 * t15 * w[0];
        res[0] = t1027 * t1029;
        const Scalar t19 = t1016 * t1016;
        res[1] = (-t19 * t1014 / 0.5e1 + (t1029 + t1030 / 0.5e1) * b) / a * t1027;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<5,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1036 = fabs(b);
        const Scalar t1045 = t1036 * t1036;
        const Scalar t3 = t1045 * t1045;
        const Scalar t1057 = b / t1036 / t3;
        const Scalar t6 = sqrt(a);
        const Scalar t1056 = w[0] * t6 * a;
        const Scalar t1037 = a - b;
        const Scalar t1034 = fabs(t1037);
        const Scalar t1041 = t1034 * t1034;
        const Scalar t9 = t1041 * t1041;
        const Scalar t1055 = 0.1e1 / t1034 / t9;
        const Scalar t1053 = 0.1e1 / t1045 * t1057;
        const Scalar t1033 = 0.1e1 / t1041 * t1055;
        const Scalar t15 = fabs(t1037 * t1033 + t1053);
        const Scalar t1052 = t15 / 0.6e1;
        const Scalar t1051 = a * t1056;
        const Scalar t18 = fabs(t1037 * t1055 + t1057);
        res[0] = t18 * t1056 / 0.4e1;
        res[1] = t1051 * t1052;
        const Scalar t20 = t1037 * t1037;
        res[2] = (-t20 * t1033 / 0.5e1 + (t1052 + t1053 / 0.5e1) * b) / a * t1051;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 5 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<5,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t2 = b * b;
        const Scalar t1059 = 0.1e1 / (a * c - t2);
        const Scalar t1061 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t1065 = sqrt(t1061);
        const Scalar t1060 = t1065 * t1061;
        const Scalar t1064 = 0.1e1 / c;
        const Scalar t1066 = sqrt(t1064);
        const Scalar t1062 = t1066 * t1064;
        const Scalar t1063 = a - b;
        const Scalar t1069 = (0.2e1 * a * (t1063 * t1065 + b * t1066) * t1059 + t1063 * t1060 + b * t1062) * t1059 / 0.3e1;
        return  w[0] * t1069 + w[1] * (b * t1069 - t1060 / 0.3e1 + t1062 / 0.3e1) / a;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<5,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t2 = b * b;
        const Scalar t1072 = 0.1e1 / (a * c - t2);
        const Scalar t1089 = a * t1072;
        const Scalar t1077 = w[1];
        const Scalar t1080 = 0.1e1 / a;
        const Scalar t1088 = t1077 * t1080;
        const Scalar t1074 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t1081 = sqrt(t1074);
        const Scalar t1083 = t1081 * t1074;
        const Scalar t1073 = t1074 * t1083;
        const Scalar t1079 = 0.1e1 / c;
        const Scalar t1082 = sqrt(t1079);
        const Scalar t1085 = t1082 * t1079;
        const Scalar t1075 = t1079 * t1085;
        const Scalar t1076 = a - b;
        const Scalar t1087 = (0.4e1 / 0.3e1 * (0.2e1 * (t1076 * t1081 + b * t1082) * t1089 + t1076 * t1083 + b * t1085) * t1089 + t1076 * t1073 + b * t1075) * t1072 / 0.5e1;
        const Scalar t1078 = w[0];
        const Scalar t1070 = b * t1087 - t1073 / 0.5e1 + t1075 / 0.5e1;
        res[0] = t1078 * t1087 + t1070 * t1088;
        res[1] = (t1077 * (c * t1087 - t1073) / 0.4e1 + (t1078 + 0.3e1 / 0.4e1 * b * t1088) * t1070) * t1080;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<5,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t2 = b * b;
        const Scalar t1093 = 0.1e1 / (a * c - t2);
        const Scalar t1099 = a - b;
        const Scalar t1096 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t1104 = sqrt(t1096);
        const Scalar t1102 = 0.1e1 / c;
        const Scalar t1105 = sqrt(t1102);
        const Scalar t1106 = t1104 * t1096;
        const Scalar t1108 = t1105 * t1102;
        const Scalar t1113 = (0.2e1 * a * (t1099 * t1104 + b * t1105) * t1093 + t1099 * t1106 + b * t1108) * t1093;
        const Scalar t1100 = w[1];
        const Scalar t1103 = 0.1e1 / a;
        const Scalar t1112 = t1100 * t1103;
        const Scalar t1094 = t1096 * t1106;
        const Scalar t1097 = t1102 * t1108;
        const Scalar t1111 = (0.4e1 / 0.3e1 * a * t1113 + t1099 * t1094 + b * t1097) * t1093 / 0.5e1;
        const Scalar t1110 = t1113 / 0.3e1;
        const Scalar t1101 = w[0];
        const Scalar t1090 = b * t1111 - t1094 / 0.5e1 + t1097 / 0.5e1;
        res[0] = t1101 * t1110 + (b * t1110 - t1106 / 0.3e1 + t1108 / 0.3e1) * t1112;
        res[1] = t1101 * t1111 + t1090 * t1112;
        res[2] = (t1100 * (c * t1111 - t1094) / 0.4e1 + (t1101 + 0.3e1 / 0.4e1 * b * t1112) * t1090) * t1103;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<5,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t1127 = b / t3 / t1;
        const Scalar t1117 = a - b;
        const Scalar t6 = fabs(t1117);
        const Scalar t7 = t6 * t6;
        const Scalar t8 = t7 * t7;
        const Scalar t1115 = 0.1e1 / t8 / t6;
        const Scalar t12 = fabs(t1117 * t1115 + t1127);
        const Scalar t1126 = t12 / 0.4e1;
        const Scalar t16 = t1117 * t1117;
        const Scalar t27 = sqrt(a);
        return  (w[0] * t1126 + w[1] * (-t16 * t1115 / 0.3e1 + (t1126 + t1127 / 0.3e1) * b) / a) * t27 * a;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<5,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1134 = a - b;
        const Scalar t1 = fabs(t1134);
        const Scalar t2 = t1 * t1;
        const Scalar t4 = t2 * t2;
        const Scalar t1131 = 0.1e1 / t4 / t2 / t1;
        const Scalar t6 = fabs(b);
        const Scalar t7 = t6 * t6;
        const Scalar t9 = t7 * t7;
        const Scalar t1132 = 0.1e1 / t9 / t7 / t6;
        const Scalar t14 = fabs(t1134 * t1131 + b * t1132);
        const Scalar t1154 = t14 / 0.6e1;
        const Scalar t15 = t1134 * t1134;
        const Scalar t1152 = t15 * t1131;
        const Scalar t1136 = w[1];
        const Scalar t1139 = 0.1e1 / a;
        const Scalar t1151 = t1136 * t1139;
        const Scalar t16 = sqrt(a);
        const Scalar t1141 = t16 * a;
        const Scalar t1138 = b * b;
        const Scalar t1137 = w[0];
        const Scalar t1135 = a * t1141;
        const Scalar t1129 = (b * t1154 - t1152 / 0.5e1 + t1138 * t1132 / 0.5e1) * t1135;
        res[0] = t1137 * t1135 * t1154 + t1129 * t1151;
        res[1] = (t1136 * (t1138 * t1141 * t1154 - t1135 * t1152) / 0.4e1 + (t1137 + 0.3e1 / 0.4e1 * b * t1151) * t1129) * t1139;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<5,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1166 = a - b;
        const Scalar t1161 = fabs(t1166);
        const Scalar t1174 = t1161 * t1161;
        const Scalar t2 = t1174 * t1174;
        const Scalar t1159 = 0.1e1 / t1161 / t2;
        const Scalar t1165 = fabs(b);
        const Scalar t1178 = t1165 * t1165;
        const Scalar t5 = t1178 * t1178;
        const Scalar t1162 = 0.1e1 / t1165 / t5;
        const Scalar t10 = fabs(t1166 * t1159 + b * t1162);
        const Scalar t1187 = t10 / 0.4e1;
        const Scalar t1160 = 0.1e1 / t1174 * t1159;
        const Scalar t1163 = 0.1e1 / t1178 * t1162;
        const Scalar t16 = fabs(t1166 * t1160 + b * t1163);
        const Scalar t1186 = t16 / 0.6e1;
        const Scalar t1164 = t1166 * t1166;
        const Scalar t1185 = t1164 * t1160;
        const Scalar t1169 = w[1];
        const Scalar t1172 = 0.1e1 / a;
        const Scalar t1184 = t1169 * t1172;
        const Scalar t17 = sqrt(a);
        const Scalar t1182 = t17 * a;
        const Scalar t1171 = b * b;
        const Scalar t1170 = w[0];
        const Scalar t1167 = a * t1182;
        const Scalar t1156 = (b * t1186 - t1185 / 0.5e1 + t1171 * t1163 / 0.5e1) * t1167;
        res[0] = (t1170 * t1187 + (b * t1187 - t1164 * t1159 / 0.3e1 + t1171 * t1162 / 0.3e1) * t1184) * t1182;
        res[1] = t1170 * t1167 * t1186 + t1156 * t1184;
        res[2] = (t1169 * (t1171 * t1182 * t1186 - t1167 * t1185) / 0.4e1 + (t1170 + 0.3e1 / 0.4e1 * b * t1184) * t1156) * t1172;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 5 //
    // and weight of degree 2      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<5,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t1192 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t1197 = sqrt(t1192);
        const Scalar t1191 = t1197 * t1192;
        const Scalar t1195 = 0.1e1 / c;
        const Scalar t1198 = sqrt(t1195);
        const Scalar t1193 = t1198 * t1195;
        const Scalar t1196 = 0.1e1 / a;
        const Scalar t4 = b * b;
        const Scalar t1190 = 0.1e1 / (a * c - t4);
        const Scalar t1194 = a - b;
        const Scalar t1201 = (0.2e1 * a * (t1194 * t1197 + b * t1198) * t1190 + t1194 * t1191 + b * t1193) * t1190 / 0.3e1;
        const Scalar t1202 = (b * t1201 - t1191 / 0.3e1 + t1193 / 0.3e1) * t1196;
        return  w[0] * t1201 + w[1] * t1202 + w[2] * (b * t1202 + c * t1201 - t1191) * t1196 / 0.2e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<5,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t2 = b * b;
        const Scalar t1206 = 0.1e1 / (a * c - t2);
        const Scalar t1225 = a * t1206;
        const Scalar t1208 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t1216 = sqrt(t1208);
        const Scalar t1218 = t1216 * t1208;
        const Scalar t1207 = t1208 * t1218;
        const Scalar t1214 = 0.1e1 / c;
        const Scalar t1217 = sqrt(t1214);
        const Scalar t1220 = t1217 * t1214;
        const Scalar t1209 = t1214 * t1220;
        const Scalar t1210 = a - b;
        const Scalar t1222 = (0.4e1 / 0.3e1 * (0.2e1 * (t1210 * t1216 + b * t1217) * t1225 + t1210 * t1218 + b * t1220) * t1225 + t1210 * t1207 + b * t1209) * t1206 / 0.5e1;
        const Scalar t1204 = b * t1222 - t1207 / 0.5e1 + t1209 / 0.5e1;
        const Scalar t1215 = 0.1e1 / a;
        const Scalar t1223 = t1204 * t1215;
        const Scalar t1224 = 0.3e1 / 0.4e1 * b * t1223 + c * t1222 / 0.4e1 - t1207 / 0.4e1;
        const Scalar t1213 = w[0];
        const Scalar t1212 = w[1];
        const Scalar t1211 = w[2];
        res[0] = t1213 * t1222 + (t1212 * t1204 + t1211 * t1224) * t1215;
        res[1] = t1213 * t1223 + t1212 * t1215 * t1224 + t1211 * (-t1207 + (b * t1224 + 0.2e1 * c * t1204) * t1215) * t1215 / 0.3e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<5,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1234 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t1243 = sqrt(t1234);
        const Scalar t1245 = t1243 * t1234;
        const Scalar t1232 = t1234 * t1245;
        const Scalar t4 = b * b;
        const Scalar t1231 = 0.1e1 / (a * c - t4);
        const Scalar t1241 = 0.1e1 / c;
        const Scalar t1244 = sqrt(t1241);
        const Scalar t1247 = t1244 * t1241;
        const Scalar t1235 = t1241 * t1247;
        const Scalar t1237 = a - b;
        const Scalar t1253 = (0.2e1 * a * (t1237 * t1243 + b * t1244) * t1231 + t1237 * t1245 + b * t1247) * t1231;
        const Scalar t1250 = (0.4e1 / 0.3e1 * a * t1253 + t1237 * t1232 + b * t1235) * t1231 / 0.5e1;
        const Scalar t1227 = b * t1250 - t1232 / 0.5e1 + t1235 / 0.5e1;
        const Scalar t1242 = 0.1e1 / a;
        const Scalar t1254 = t1227 * t1242;
        const Scalar t1255 = 0.3e1 / 0.4e1 * b * t1254 + c * t1250 / 0.4e1 - t1232 / 0.4e1;
        const Scalar t1238 = w[2];
        const Scalar t1252 = t1238 * t1242;
        const Scalar t1239 = w[1];
        const Scalar t1251 = t1239 * t1242;
        const Scalar t1249 = t1253 / 0.3e1;
        const Scalar t1240 = w[0];
        const Scalar t1229 = b * t1249 - t1245 / 0.3e1 + t1247 / 0.3e1;
        res[0] = t1240 * t1249 + t1229 * t1251 + (b * t1229 * t1242 + c * t1249 - t1245) * t1252 / 0.2e1;
        res[1] = t1240 * t1250 + (t1239 * t1227 + t1238 * t1255) * t1242;
        res[2] = t1240 * t1254 + t1251 * t1255 + (-t1232 + (b * t1255 + 0.2e1 * c * t1227) * t1242) * t1252 / 0.3e1;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<5,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t1261 = a - b;
        const Scalar t1 = fabs(t1261);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t1258 = 0.1e1 / t3 / t1;
        const Scalar t5 = fabs(b);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t1259 = 0.1e1 / t7 / t5;
        const Scalar t12 = fabs(t1261 * t1258 + b * t1259);
        const Scalar t1276 = t12 / 0.4e1;
        const Scalar t1265 = sqrt(a);
        const Scalar t1262 = t1265 * a;
        const Scalar t1263 = b * b;
        const Scalar t1264 = 0.1e1 / a;
        const Scalar t13 = t1261 * t1261;
        const Scalar t1273 = t13 * t1258;
        const Scalar t1274 = (b * t1276 - t1273 / 0.3e1 + t1263 * t1259 / 0.3e1) * t1262 * t1264;
        return  w[0] * t1262 * t1276 + w[1] * t1274 + w[2] * (b * t1274 + t1263 * t1265 * t1276 - t1262 * t1273) * t1264 / 0.2e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<5,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1290 = 0.1e1 / a;
        const Scalar t1306 = b * t1290;
        const Scalar t1284 = a - b;
        const Scalar t1 = fabs(t1284);
        const Scalar t2 = t1 * t1;
        const Scalar t4 = t2 * t2;
        const Scalar t1281 = 0.1e1 / t4 / t2 / t1;
        const Scalar t6 = fabs(b);
        const Scalar t7 = t6 * t6;
        const Scalar t9 = t7 * t7;
        const Scalar t1282 = 0.1e1 / t9 / t7 / t6;
        const Scalar t14 = fabs(t1284 * t1281 + b * t1282);
        const Scalar t1305 = t14 / 0.6e1;
        const Scalar t15 = t1284 * t1284;
        const Scalar t1303 = t15 * t1281;
        const Scalar t16 = sqrt(a);
        const Scalar t1292 = t16 * a;
        const Scalar t1285 = a * t1292;
        const Scalar t1302 = t1285 * t1303;
        const Scalar t1289 = b * b;
        const Scalar t1288 = w[0];
        const Scalar t1287 = w[1];
        const Scalar t1286 = w[2];
        const Scalar t1279 = (b * t1305 - t1303 / 0.5e1 + t1289 * t1282 / 0.5e1) * t1285;
        const Scalar t1278 = 0.3e1 * t1279 * t1306 + t1289 * t1292 * t1305 - t1302;
        res[0] = t1288 * t1285 * t1305 + (t1287 * t1279 + t1286 * t1278 / 0.4e1) * t1290;
        const Scalar t35 = a * a;
        res[1] = (t1288 * t1279 + t1286 * (0.2e1 * t1289 * t1279 / t35 - t1302) / 0.3e1 + (t1287 / 0.4e1 + t1286 * t1306 / 0.12e2) * t1278) * t1290;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<5,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1320 = a - b;
        const Scalar t1315 = fabs(t1320);
        const Scalar t1329 = t1315 * t1315;
        const Scalar t2 = t1329 * t1329;
        const Scalar t1313 = 0.1e1 / t1315 / t2;
        const Scalar t1319 = fabs(b);
        const Scalar t1333 = t1319 * t1319;
        const Scalar t5 = t1333 * t1333;
        const Scalar t1316 = 0.1e1 / t1319 / t5;
        const Scalar t10 = fabs(t1320 * t1313 + b * t1316);
        const Scalar t1345 = t10 / 0.4e1;
        const Scalar t1314 = 0.1e1 / t1329 * t1313;
        const Scalar t1317 = 0.1e1 / t1333 * t1316;
        const Scalar t16 = fabs(t1320 * t1314 + b * t1317);
        const Scalar t1344 = t16 / 0.6e1;
        const Scalar t1326 = b * b;
        const Scalar t1327 = 0.1e1 / a;
        const Scalar t1328 = sqrt(a);
        const Scalar t1337 = t1328 * a;
        const Scalar t1318 = t1320 * t1320;
        const Scalar t1342 = t1318 * t1313;
        const Scalar t1343 = (b * t1345 - t1342 / 0.3e1 + t1326 * t1316 / 0.3e1) * t1337 * t1327;
        const Scalar t1341 = t1318 * t1314;
        const Scalar t1323 = w[2];
        const Scalar t1340 = t1323 * t1327;
        const Scalar t1321 = a * t1337;
        const Scalar t1339 = t1321 * t1341;
        const Scalar t1325 = w[0];
        const Scalar t1324 = w[1];
        const Scalar t1309 = (b * t1344 - t1341 / 0.5e1 + t1326 * t1317 / 0.5e1) * t1321;
        const Scalar t1308 = 0.3e1 * b * t1309 * t1327 + t1326 * t1337 * t1344 - t1339;
        res[0] = t1325 * t1337 * t1345 + t1324 * t1343 + (b * t1343 + t1326 * t1328 * t1345 - t1337 * t1342) * t1340 / 0.2e1;
        res[1] = t1325 * t1321 * t1344 + (t1324 * t1309 + t1323 * t1308 / 0.4e1) * t1327;
        const Scalar t52 = a * a;
        res[2] = (t1325 * t1309 + t1323 * (0.2e1 * t1326 * t1309 / t52 - t1339) / 0.3e1 + (t1324 / 0.4e1 + b * t1340 / 0.12e2) * t1308) * t1327;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 5 //
    // and weight of degree 3      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<5,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t1351 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t1356 = sqrt(t1351);
        const Scalar t1350 = t1356 * t1351;
        const Scalar t1354 = 0.1e1 / c;
        const Scalar t1357 = sqrt(t1354);
        const Scalar t1352 = t1357 * t1354;
        const Scalar t4 = b * b;
        const Scalar t1349 = 0.1e1 / (a * c - t4);
        const Scalar t1353 = a - b;
        const Scalar t1360 = (0.2e1 * a * (t1353 * t1356 + b * t1357) * t1349 + t1353 * t1350 + b * t1352) * t1349 / 0.3e1;
        const Scalar t1347 = b * t1360 - t1350 / 0.3e1 + t1352 / 0.3e1;
        const Scalar t1355 = 0.1e1 / a;
        const Scalar t1361 = t1347 * t1355;
        const Scalar t1346 = b * t1361 + c * t1360 - t1350;
        return  w[0] * t1360 + w[1] * t1361 + w[2] * t1346 * t1355 / 0.2e1 + w[3] * (-t1350 + (-b * t1346 / 0.2e1 + 0.2e1 * c * t1347) * t1355) * t1355;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<5,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t2 = b * b;
        const Scalar t1366 = 0.1e1 / (a * c - t2);
        const Scalar t1387 = a * t1366;
        const Scalar t1368 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t1377 = sqrt(t1368);
        const Scalar t1379 = t1377 * t1368;
        const Scalar t1367 = t1368 * t1379;
        const Scalar t1375 = 0.1e1 / c;
        const Scalar t1378 = sqrt(t1375);
        const Scalar t1381 = t1378 * t1375;
        const Scalar t1369 = t1375 * t1381;
        const Scalar t1370 = a - b;
        const Scalar t1383 = (0.4e1 / 0.3e1 * (0.2e1 * (t1370 * t1377 + b * t1378) * t1387 + t1370 * t1379 + b * t1381) * t1387 + t1370 * t1367 + b * t1369) * t1366 / 0.5e1;
        const Scalar t1364 = b * t1383 - t1367 / 0.5e1 + t1369 / 0.5e1;
        const Scalar t1376 = 0.1e1 / a;
        const Scalar t1384 = t1364 * t1376;
        const Scalar t1363 = 0.3e1 * b * t1384 + c * t1383 - t1367;
        const Scalar t1385 = t1363 / 0.4e1;
        const Scalar t1362 = -t1367 + (b * t1385 + 0.2e1 * c * t1364) * t1376;
        const Scalar t1386 = t1362 / 0.3e1;
        const Scalar t1374 = w[0];
        const Scalar t1373 = w[1];
        const Scalar t1372 = w[2];
        const Scalar t1371 = w[3];
        res[0] = t1374 * t1383 + (t1373 * t1364 + t1372 * t1385 + t1371 * t1386) * t1376;
        res[1] = t1374 * t1384 + t1373 * t1376 * t1385 + t1372 * t1376 * t1386 + t1371 * (-t1367 + (-b * t1362 / 0.3e1 + 0.3e1 / 0.4e1 * c * t1363) * t1376) * t1376 / 0.2e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<5,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1423 = 0.2e1 * c;
        const Scalar t1398 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t1408 = sqrt(t1398);
        const Scalar t1410 = t1408 * t1398;
        const Scalar t1396 = t1398 * t1410;
        const Scalar t1406 = 0.1e1 / c;
        const Scalar t1409 = sqrt(t1406);
        const Scalar t1412 = t1409 * t1406;
        const Scalar t1399 = t1406 * t1412;
        const Scalar t4 = b * b;
        const Scalar t1395 = 0.1e1 / (a * c - t4);
        const Scalar t1401 = a - b;
        const Scalar t1419 = (0.2e1 * a * (t1401 * t1408 + b * t1409) * t1395 + t1401 * t1410 + b * t1412) * t1395;
        const Scalar t1415 = (0.4e1 / 0.3e1 * a * t1419 + t1401 * t1396 + b * t1399) * t1395 / 0.5e1;
        const Scalar t1391 = b * t1415 - t1396 / 0.5e1 + t1399 / 0.5e1;
        const Scalar t1407 = 0.1e1 / a;
        const Scalar t1420 = t1391 * t1407;
        const Scalar t1389 = 0.3e1 * b * t1420 + c * t1415 - t1396;
        const Scalar t1421 = t1389 / 0.4e1;
        const Scalar t1388 = -t1396 + (b * t1421 + t1391 * t1423) * t1407;
        const Scalar t1422 = t1388 / 0.3e1;
        const Scalar t1402 = w[3];
        const Scalar t1418 = t1402 * t1407;
        const Scalar t1403 = w[2];
        const Scalar t1417 = t1403 * t1407;
        const Scalar t1404 = w[1];
        const Scalar t1416 = t1404 * t1407;
        const Scalar t1414 = t1419 / 0.3e1;
        const Scalar t1405 = w[0];
        const Scalar t1393 = b * t1414 - t1410 / 0.3e1 + t1412 / 0.3e1;
        const Scalar t1390 = b * t1393 * t1407 + c * t1414 - t1410;
        res[0] = t1405 * t1414 + t1393 * t1416 + t1390 * t1417 / 0.2e1 + (-t1410 + (-b * t1390 / 0.2e1 + t1393 * t1423) * t1407) * t1418;
        res[1] = t1405 * t1415 + (t1404 * t1391 + t1403 * t1421 + t1402 * t1422) * t1407;
        res[2] = t1405 * t1420 + t1416 * t1421 + t1417 * t1422 + (-t1396 + (-b * t1388 / 0.3e1 + 0.3e1 / 0.4e1 * c * t1389) * t1407) * t1418 / 0.2e1;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<5,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t1433 = 0.1e1 / a;
        const Scalar t1446 = b * t1433;
        const Scalar t1430 = a - b;
        const Scalar t1 = fabs(t1430);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t1427 = 0.1e1 / t3 / t1;
        const Scalar t5 = fabs(b);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t1428 = 0.1e1 / t7 / t5;
        const Scalar t12 = fabs(t1430 * t1427 + b * t1428);
        const Scalar t1445 = t12 / 0.4e1;
        const Scalar t13 = t1430 * t1430;
        const Scalar t1443 = t13 * t1427;
        const Scalar t1434 = sqrt(a);
        const Scalar t1431 = t1434 * a;
        const Scalar t1442 = t1431 * t1443;
        const Scalar t1432 = b * b;
        const Scalar t1425 = (b * t1445 - t1443 / 0.3e1 + t1432 * t1428 / 0.3e1) * t1431;
        const Scalar t1424 = t1425 * t1446 + t1432 * t1434 * t1445 - t1442;
        const Scalar t33 = a * a;
        return  w[0] * t1431 * t1445 + (w[1] * t1425 + w[2] * t1424 / 0.2e1 + (-t1424 * t1446 / 0.2e1 + 0.2e1 * t1432 * t1425 / t33 - t1442) * w[3]) * t1433;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<5,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1462 = 0.1e1 / a;
        const Scalar t1481 = b * t1462;
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t4 = t2 * t2;
        const Scalar t1453 = 0.1e1 / t4 / t2 / t1;
        const Scalar t6 = sqrt(a);
        const Scalar t1465 = t6 * a;
        const Scalar t1456 = a * t1465;
        const Scalar t1461 = b * b;
        const Scalar t1455 = a - b;
        const Scalar t7 = fabs(t1455);
        const Scalar t8 = t7 * t7;
        const Scalar t10 = t8 * t8;
        const Scalar t1452 = 0.1e1 / t10 / t8 / t7;
        const Scalar t12 = t1455 * t1455;
        const Scalar t1477 = t12 * t1452;
        const Scalar t16 = fabs(t1455 * t1452 + b * t1453);
        const Scalar t1479 = t16 / 0.6e1;
        const Scalar t1450 = (b * t1479 - t1477 / 0.5e1 + t1461 * t1453 / 0.5e1) * t1456;
        const Scalar t1475 = t1456 * t1477;
        const Scalar t1449 = 0.3e1 * t1450 * t1481 + t1461 * t1465 * t1479 - t1475;
        const Scalar t1480 = t1449 / 0.4e1;
        const Scalar t26 = a * a;
        const Scalar t1476 = t1461 / t26;
        const Scalar t1460 = w[0];
        const Scalar t1459 = w[1];
        const Scalar t1458 = w[2];
        const Scalar t1457 = w[3];
        const Scalar t1448 = t1480 * t1481 + 0.2e1 * t1450 * t1476 - t1475;
        res[0] = t1460 * t1456 * t1479 + (t1459 * t1450 + t1458 * t1480 + t1457 * t1448 / 0.3e1) * t1462;
        res[1] = (t1460 * t1450 + t1459 * t1480 + t1457 * (0.3e1 / 0.4e1 * t1449 * t1476 - t1475) / 0.2e1 + (t1458 / 0.3e1 - t1457 * t1481 / 0.6e1) * t1448) * t1462;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<5,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1497 = a - b;
        const Scalar t1492 = fabs(t1497);
        const Scalar t1508 = t1492 * t1492;
        const Scalar t2 = t1508 * t1508;
        const Scalar t1490 = 0.1e1 / t1492 / t2;
        const Scalar t1496 = fabs(b);
        const Scalar t1512 = t1496 * t1496;
        const Scalar t5 = t1512 * t1512;
        const Scalar t1493 = 0.1e1 / t1496 / t5;
        const Scalar t1505 = 0.1e1 / a;
        const Scalar t1528 = b * t1505;
        const Scalar t1494 = 0.1e1 / t1512 * t1493;
        const Scalar t1507 = sqrt(a);
        const Scalar t1516 = t1507 * a;
        const Scalar t1498 = a * t1516;
        const Scalar t1504 = b * b;
        const Scalar t1491 = 0.1e1 / t1508 * t1490;
        const Scalar t1495 = t1497 * t1497;
        const Scalar t1523 = t1495 * t1491;
        const Scalar t12 = fabs(t1497 * t1491 + b * t1494);
        const Scalar t1525 = t12 / 0.6e1;
        const Scalar t1486 = (b * t1525 - t1523 / 0.5e1 + t1504 * t1494 / 0.5e1) * t1498;
        const Scalar t1519 = t1498 * t1523;
        const Scalar t1484 = 0.3e1 * t1486 * t1528 + t1504 * t1516 * t1525 - t1519;
        const Scalar t1527 = t1484 / 0.4e1;
        const Scalar t25 = fabs(t1497 * t1490 + b * t1493);
        const Scalar t1526 = t25 / 0.4e1;
        const Scalar t1524 = t1495 * t1490;
        const Scalar t26 = a * a;
        const Scalar t1522 = t1504 / t26;
        const Scalar t1500 = w[3];
        const Scalar t1521 = t1500 * t1528;
        const Scalar t1520 = 0.2e1 * t1522;
        const Scalar t1518 = t1516 * t1524;
        const Scalar t1503 = w[0];
        const Scalar t1502 = w[1];
        const Scalar t1501 = w[2];
        const Scalar t1487 = (b * t1526 - t1524 / 0.3e1 + t1504 * t1493 / 0.3e1) * t1516;
        const Scalar t1483 = t1527 * t1528 + t1486 * t1520 - t1519;
        res[0] = t1503 * t1516 * t1526 + (t1502 * t1487 + t1500 * (t1487 * t1520 - t1518) + (t1501 / 0.2e1 - t1521 / 0.2e1) * (t1487 * t1528 + t1504 * t1507 * t1526 - t1518)) * t1505;
        res[1] = t1503 * t1498 * t1525 + (t1502 * t1486 + t1501 * t1527 + t1500 * t1483 / 0.3e1) * t1505;
        res[2] = (t1503 * t1486 + t1502 * t1527 + t1500 * (0.3e1 / 0.4e1 * t1484 * t1522 - t1519) / 0.2e1 + (t1501 / 0.3e1 - t1521 / 0.6e1) * t1483) * t1505;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 6 //
    // and weight of degree 0      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<6,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t1533 = a - b;
        const Scalar t1532 = a - 0.2e1 * b + c;
        const Scalar t3 = b * b;
        const Scalar t1531 = a * c - t3;
        const Scalar t1530 = 0.1e1 / t1531;
        const Scalar t1529 = sqrt(t1531);
        const Scalar t5 = sqrt(t1530);
        const Scalar t7 = atan2(t1533, t1529);
        const Scalar t8 = atan2(-b, t1529);
        const Scalar t19 = t1532 * t1532;
        const Scalar t22 = c * c;
        return  w[0] * (0.3e1 / 0.2e1 * a * (a * t5 * (t7 - t8) + t1533 / t1532 + b / c) * t1530 + t1533 / t19 + b / t22) * t1530 / 0.4e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<6,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1552 = 0.1e1 / c;
        const Scalar t1539 = a - 0.2e1 * b + c;
        const Scalar t1551 = 0.1e1 / t1539;
        const Scalar t2 = c * c;
        const Scalar t1550 = 0.1e1 / t2;
        const Scalar t3 = t1539 * t1539;
        const Scalar t1549 = 0.1e1 / t3;
        const Scalar t5 = b * b;
        const Scalar t1537 = a * c - t5;
        const Scalar t1536 = 0.1e1 / t1537;
        const Scalar t1548 = a * t1536;
        const Scalar t1535 = sqrt(t1537);
        const Scalar t1538 = t1551 * t1549;
        const Scalar t1540 = a - b;
        const Scalar t1542 = t1552 * t1550;
        const Scalar t6 = sqrt(t1536);
        const Scalar t8 = atan2(t1540, t1535);
        const Scalar t9 = atan2(-b, t1535);
        const Scalar t1547 = (0.5e1 / 0.4e1 * (0.3e1 / 0.2e1 * (a * t6 * (t8 - t9) + t1540 * t1551 + b * t1552) * t1548 + t1540 * t1549 + b * t1550) * t1548 + t1540 * t1538 + b * t1542) * t1536 / 0.6e1;
        const Scalar t1541 = w[0];
        res[0] = t1541 * t1547;
        res[1] = t1541 * (b * t1547 - t1538 / 0.6e1 + t1542 / 0.6e1) / a;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<6,0> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1572 = 0.1e1 / c;
        const Scalar t1559 = a - 0.2e1 * b + c;
        const Scalar t1571 = 0.1e1 / t1559;
        const Scalar t2 = c * c;
        const Scalar t1570 = 0.1e1 / t2;
        const Scalar t3 = t1559 * t1559;
        const Scalar t1569 = 0.1e1 / t3;
        const Scalar t5 = b * b;
        const Scalar t1557 = a * c - t5;
        const Scalar t1555 = sqrt(t1557);
        const Scalar t1556 = 0.1e1 / t1557;
        const Scalar t1560 = a - b;
        const Scalar t6 = sqrt(t1556);
        const Scalar t8 = atan2(t1560, t1555);
        const Scalar t9 = atan2(-b, t1555);
        const Scalar t1568 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t1560 * t1571 + b * t1572) * t1556 + t1560 * t1569 + b * t1570) * t1556;
        const Scalar t1558 = t1571 * t1569;
        const Scalar t1562 = t1572 * t1570;
        const Scalar t1567 = (0.5e1 / 0.4e1 * a * t1568 + t1560 * t1558 + b * t1562) * t1556 / 0.6e1;
        const Scalar t1561 = w[0];
        res[0] = t1561 * t1568 / 0.4e1;
        res[1] = t1561 * t1567;
        res[2] = t1561 * (b * t1567 - t1558 / 0.6e1 + t1562 / 0.6e1) / a;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<6,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t1573 = a - b;
        const Scalar t2 = a * a;
        const Scalar t4 = fabs(t1573);
        const Scalar t5 = t4 * t4;
        const Scalar t6 = t5 * t5;
        const Scalar t10 = fabs(b);
        const Scalar t11 = t10 * t10;
        const Scalar t12 = t11 * t11;
        const Scalar t17 = fabs(t1573 / t6 / t5 + b / t12 / t11);
        return  w[0] * t2 * t17 / 0.5e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<6,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t4 = t3 * t3;
        const Scalar t1599 = b / t4;
        const Scalar t1585 = a - b;
        const Scalar t6 = fabs(t1585);
        const Scalar t7 = t6 * t6;
        const Scalar t8 = t7 * t7;
        const Scalar t9 = t8 * t8;
        const Scalar t1583 = 0.1e1 / t9;
        const Scalar t12 = fabs(t1585 * t1583 + t1599);
        const Scalar t1598 = t12 / 0.7e1;
        const Scalar t14 = a * a;
        const Scalar t1596 = w[0] * t14 * a;
        res[0] = t1596 * t1598;
        const Scalar t16 = t1585 * t1585;
        res[1] = (-t16 * t1583 / 0.6e1 + (t1598 + t1599 / 0.6e1) * b) / a * t1596;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<6,0> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1605 = fabs(b);
        const Scalar t1615 = t1605 * t1605;
        const Scalar t1 = 0.1e1 / t1615;
        const Scalar t3 = t1615 * t1615;
        const Scalar t1625 = b * t1 / t3;
        const Scalar t6 = a * a;
        const Scalar t1624 = w[0] * t6;
        const Scalar t1606 = a - b;
        const Scalar t1603 = fabs(t1606);
        const Scalar t1611 = t1603 * t1603;
        const Scalar t7 = 0.1e1 / t1611;
        const Scalar t8 = t1611 * t1611;
        const Scalar t1623 = t7 / t8;
        const Scalar t1621 = t1 * t1625;
        const Scalar t1602 = t7 * t1623;
        const Scalar t12 = fabs(t1606 * t1602 + t1621);
        const Scalar t1620 = t12 / 0.7e1;
        const Scalar t1619 = a * t1624;
        const Scalar t15 = fabs(t1606 * t1623 + t1625);
        res[0] = t15 * t1624 / 0.5e1;
        res[1] = t1619 * t1620;
        const Scalar t17 = t1606 * t1606;
        res[2] = (-t17 * t1602 / 0.6e1 + (t1620 + t1621 / 0.6e1) * b) / a * t1619;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 6 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<6,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t2 = b * b;
        const Scalar t1629 = a * c - t2;
        const Scalar t1627 = sqrt(t1629);
        const Scalar t1628 = 0.1e1 / t1629;
        const Scalar t1631 = a - 0.2e1 * b + c;
        const Scalar t4 = t1631 * t1631;
        const Scalar t1630 = 0.1e1 / t4;
        const Scalar t1632 = a - b;
        const Scalar t5 = c * c;
        const Scalar t1633 = 0.1e1 / t5;
        const Scalar t6 = sqrt(t1628);
        const Scalar t8 = atan2(t1632, t1627);
        const Scalar t9 = atan2(-b, t1627);
        const Scalar t1634 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t1632 / t1631 + b / c) * t1628 + t1632 * t1630 + b * t1633) * t1628 / 0.4e1;
        return  w[0] * t1634 + w[1] * (b * t1634 - t1630 / 0.4e1 + t1633 / 0.4e1) / a;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<6,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1657 = 0.1e1 / c;
        const Scalar t1641 = a - 0.2e1 * b + c;
        const Scalar t1656 = 0.1e1 / t1641;
        const Scalar t2 = c * c;
        const Scalar t1655 = 0.1e1 / t2;
        const Scalar t3 = t1641 * t1641;
        const Scalar t1654 = 0.1e1 / t3;
        const Scalar t5 = b * b;
        const Scalar t1639 = a * c - t5;
        const Scalar t1638 = 0.1e1 / t1639;
        const Scalar t1653 = a * t1638;
        const Scalar t1643 = w[1];
        const Scalar t1646 = 0.1e1 / a;
        const Scalar t1652 = t1643 * t1646;
        const Scalar t1637 = sqrt(t1639);
        const Scalar t1640 = t1656 * t1654;
        const Scalar t1642 = a - b;
        const Scalar t1645 = t1657 * t1655;
        const Scalar t6 = sqrt(t1638);
        const Scalar t8 = atan2(t1642, t1637);
        const Scalar t9 = atan2(-b, t1637);
        const Scalar t1651 = (0.5e1 / 0.4e1 * (0.3e1 / 0.2e1 * (a * t6 * (t8 - t9) + t1642 * t1656 + b * t1657) * t1653 + t1642 * t1654 + b * t1655) * t1653 + t1642 * t1640 + b * t1645) * t1638 / 0.6e1;
        const Scalar t1644 = w[0];
        const Scalar t1635 = b * t1651 - t1640 / 0.6e1 + t1645 / 0.6e1;
        res[0] = t1644 * t1651 + t1635 * t1652;
        res[1] = (t1643 * (c * t1651 - t1640) / 0.5e1 + (t1644 + 0.4e1 / 0.5e1 * b * t1652) * t1635) * t1646;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<6,1> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1682 = 0.1e1 / c;
        const Scalar t1666 = a - 0.2e1 * b + c;
        const Scalar t1681 = 0.1e1 / t1666;
        const Scalar t2 = c * c;
        const Scalar t1670 = 0.1e1 / t2;
        const Scalar t3 = t1666 * t1666;
        const Scalar t1664 = 0.1e1 / t3;
        const Scalar t5 = b * b;
        const Scalar t1663 = a * c - t5;
        const Scalar t1661 = sqrt(t1663);
        const Scalar t1662 = 0.1e1 / t1663;
        const Scalar t1667 = a - b;
        const Scalar t6 = sqrt(t1662);
        const Scalar t8 = atan2(t1667, t1661);
        const Scalar t9 = atan2(-b, t1661);
        const Scalar t1680 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t1667 * t1681 + b * t1682) * t1662 + t1667 * t1664 + b * t1670) * t1662;
        const Scalar t1668 = w[1];
        const Scalar t1672 = 0.1e1 / a;
        const Scalar t1679 = t1668 * t1672;
        const Scalar t1665 = t1681 * t1664;
        const Scalar t1671 = t1682 * t1670;
        const Scalar t1678 = (0.5e1 / 0.4e1 * a * t1680 + t1667 * t1665 + b * t1671) * t1662 / 0.6e1;
        const Scalar t1677 = t1680 / 0.4e1;
        const Scalar t1669 = w[0];
        const Scalar t1658 = b * t1678 - t1665 / 0.6e1 + t1671 / 0.6e1;
        res[0] = t1669 * t1677 + (b * t1677 - t1664 / 0.4e1 + t1670 / 0.4e1) * t1679;
        res[1] = t1669 * t1678 + t1658 * t1679;
        res[2] = (t1668 * (c * t1678 - t1665) / 0.5e1 + (t1669 + 0.4e1 / 0.5e1 * b * t1679) * t1658) * t1672;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<6,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t1696 = b / t3 / t2;
        const Scalar t1686 = a - b;
        const Scalar t6 = fabs(t1686);
        const Scalar t7 = t6 * t6;
        const Scalar t8 = t7 * t7;
        const Scalar t1684 = 0.1e1 / t8 / t7;
        const Scalar t12 = fabs(t1686 * t1684 + t1696);
        const Scalar t1695 = t12 / 0.5e1;
        const Scalar t16 = t1686 * t1686;
        const Scalar t27 = a * a;
        return  (w[0] * t1695 + w[1] * (-t16 * t1684 / 0.4e1 + (t1695 + t1696 / 0.4e1) * b) / a) * t27;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<6,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1703 = a - b;
        const Scalar t1 = fabs(t1703);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t4 = t3 * t3;
        const Scalar t1700 = 0.1e1 / t4;
        const Scalar t5 = fabs(b);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t8 = t7 * t7;
        const Scalar t1701 = 0.1e1 / t8;
        const Scalar t12 = fabs(t1703 * t1700 + b * t1701);
        const Scalar t1720 = t12 / 0.7e1;
        const Scalar t13 = t1703 * t1703;
        const Scalar t1718 = t13 * t1700;
        const Scalar t1704 = w[1];
        const Scalar t1708 = 0.1e1 / a;
        const Scalar t1717 = t1704 * t1708;
        const Scalar t1709 = a * a;
        const Scalar t1707 = a * t1709;
        const Scalar t1706 = b * b;
        const Scalar t1705 = w[0];
        const Scalar t1698 = (b * t1720 - t1718 / 0.6e1 + t1706 * t1701 / 0.6e1) * t1707;
        res[0] = t1705 * t1707 * t1720 + t1698 * t1717;
        res[1] = (t1704 * (t1706 * t1709 * t1720 - t1707 * t1718) / 0.5e1 + (t1705 + 0.4e1 / 0.5e1 * b * t1717) * t1698) * t1708;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<6,1> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1732 = a - b;
        const Scalar t1727 = fabs(t1732);
        const Scalar t1740 = t1727 * t1727;
        const Scalar t1 = 0.1e1 / t1740;
        const Scalar t2 = t1740 * t1740;
        const Scalar t1725 = t1 / t2;
        const Scalar t1731 = fabs(b);
        const Scalar t1744 = t1731 * t1731;
        const Scalar t4 = 0.1e1 / t1744;
        const Scalar t5 = t1744 * t1744;
        const Scalar t1728 = t4 / t5;
        const Scalar t10 = fabs(t1732 * t1725 + b * t1728);
        const Scalar t1751 = t10 / 0.5e1;
        const Scalar t1726 = t1 * t1725;
        const Scalar t1729 = t4 * t1728;
        const Scalar t14 = fabs(t1732 * t1726 + b * t1729);
        const Scalar t1750 = t14 / 0.7e1;
        const Scalar t1730 = t1732 * t1732;
        const Scalar t1749 = t1730 * t1726;
        const Scalar t1733 = w[1];
        const Scalar t1738 = 0.1e1 / a;
        const Scalar t1748 = t1733 * t1738;
        const Scalar t1737 = a * a;
        const Scalar t1736 = a * t1737;
        const Scalar t1735 = b * b;
        const Scalar t1734 = w[0];
        const Scalar t1722 = (b * t1750 - t1749 / 0.6e1 + t1735 * t1729 / 0.6e1) * t1736;
        res[0] = (t1734 * t1751 + (b * t1751 - t1730 * t1725 / 0.4e1 + t1735 * t1728 / 0.4e1) * t1748) * t1737;
        res[1] = t1734 * t1736 * t1750 + t1722 * t1748;
        res[2] = (t1733 * (t1735 * t1737 * t1750 - t1736 * t1749) / 0.5e1 + (t1734 + 0.4e1 / 0.5e1 * b * t1748) * t1722) * t1738;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 6 //
    // and weight of degree 2      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<6,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t1758 = a - 0.2e1 * b + c;
        const Scalar t2 = t1758 * t1758;
        const Scalar t1757 = 0.1e1 / t2;
        const Scalar t3 = c * c;
        const Scalar t1760 = 0.1e1 / t3;
        const Scalar t1761 = 0.1e1 / a;
        const Scalar t5 = b * b;
        const Scalar t1756 = a * c - t5;
        const Scalar t1754 = sqrt(t1756);
        const Scalar t1755 = 0.1e1 / t1756;
        const Scalar t1759 = a - b;
        const Scalar t6 = sqrt(t1755);
        const Scalar t8 = atan2(t1759, t1754);
        const Scalar t9 = atan2(-b, t1754);
        const Scalar t1762 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t1759 / t1758 + b / c) * t1755 + t1759 * t1757 + b * t1760) * t1755 / 0.4e1;
        const Scalar t1763 = (b * t1762 - t1757 / 0.4e1 + t1760 / 0.4e1) * t1761;
        return  w[0] * t1762 + w[1] * t1763 + w[2] * (0.2e1 * b * t1763 + c * t1762 - t1757) * t1761 / 0.3e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<6,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1789 = 0.1e1 / c;
        const Scalar t1771 = a - 0.2e1 * b + c;
        const Scalar t1788 = 0.1e1 / t1771;
        const Scalar t2 = c * c;
        const Scalar t1787 = 0.1e1 / t2;
        const Scalar t3 = t1771 * t1771;
        const Scalar t1786 = 0.1e1 / t3;
        const Scalar t5 = b * b;
        const Scalar t1769 = a * c - t5;
        const Scalar t1768 = 0.1e1 / t1769;
        const Scalar t1785 = a * t1768;
        const Scalar t1770 = t1788 * t1786;
        const Scalar t1767 = sqrt(t1769);
        const Scalar t1772 = a - b;
        const Scalar t1776 = t1789 * t1787;
        const Scalar t6 = sqrt(t1768);
        const Scalar t8 = atan2(t1772, t1767);
        const Scalar t9 = atan2(-b, t1767);
        const Scalar t1782 = (0.5e1 / 0.4e1 * (0.3e1 / 0.2e1 * (a * t6 * (t8 - t9) + t1772 * t1788 + b * t1789) * t1785 + t1772 * t1786 + b * t1787) * t1785 + t1772 * t1770 + b * t1776) * t1768 / 0.6e1;
        const Scalar t1765 = b * t1782 - t1770 / 0.6e1 + t1776 / 0.6e1;
        const Scalar t1777 = 0.1e1 / a;
        const Scalar t1783 = t1765 * t1777;
        const Scalar t1764 = 0.4e1 * b * t1783 + c * t1782 - t1770;
        const Scalar t1784 = t1764 / 0.5e1;
        const Scalar t1775 = w[0];
        const Scalar t1774 = w[1];
        const Scalar t1773 = w[2];
        res[0] = t1775 * t1782 + (t1774 * t1765 + t1773 * t1784) * t1777;
        res[1] = t1775 * t1783 + t1774 * t1777 * t1784 + t1773 * (-t1770 + (0.2e1 / 0.5e1 * b * t1764 + 0.2e1 * c * t1765) * t1777) * t1777 / 0.4e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<6,2> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1820 = 0.1e1 / c;
        const Scalar t1800 = a - 0.2e1 * b + c;
        const Scalar t1819 = 0.1e1 / t1800;
        const Scalar t2 = c * c;
        const Scalar t1805 = 0.1e1 / t2;
        const Scalar t3 = t1800 * t1800;
        const Scalar t1798 = 0.1e1 / t3;
        const Scalar t1799 = t1819 * t1798;
        const Scalar t5 = b * b;
        const Scalar t1797 = a * c - t5;
        const Scalar t1796 = 0.1e1 / t1797;
        const Scalar t1801 = a - b;
        const Scalar t1806 = t1820 * t1805;
        const Scalar t1795 = sqrt(t1797);
        const Scalar t6 = sqrt(t1796);
        const Scalar t8 = atan2(t1801, t1795);
        const Scalar t9 = atan2(-b, t1795);
        const Scalar t1816 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t1801 * t1819 + b * t1820) * t1796 + t1801 * t1798 + b * t1805) * t1796;
        const Scalar t1813 = (0.5e1 / 0.4e1 * a * t1816 + t1801 * t1799 + b * t1806) * t1796 / 0.6e1;
        const Scalar t1791 = b * t1813 - t1799 / 0.6e1 + t1806 / 0.6e1;
        const Scalar t1807 = 0.1e1 / a;
        const Scalar t1817 = t1791 * t1807;
        const Scalar t1790 = 0.4e1 * b * t1817 + c * t1813 - t1799;
        const Scalar t1818 = t1790 / 0.5e1;
        const Scalar t1802 = w[2];
        const Scalar t1815 = t1802 * t1807;
        const Scalar t1803 = w[1];
        const Scalar t1814 = t1803 * t1807;
        const Scalar t1812 = t1816 / 0.4e1;
        const Scalar t1804 = w[0];
        const Scalar t1793 = b * t1812 - t1798 / 0.4e1 + t1805 / 0.4e1;
        res[0] = t1804 * t1812 + t1793 * t1814 + (0.2e1 * b * t1793 * t1807 + c * t1812 - t1798) * t1815 / 0.3e1;
        res[1] = t1804 * t1813 + (t1803 * t1791 + t1802 * t1818) * t1807;
        res[2] = t1804 * t1817 + t1814 * t1818 + (-t1799 + (0.2e1 / 0.5e1 * b * t1790 + 0.2e1 * c * t1791) * t1807) * t1815 / 0.4e1;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<6,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t1826 = a - b;
        const Scalar t1 = fabs(t1826);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t1823 = 0.1e1 / t3 / t2;
        const Scalar t5 = fabs(b);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t1824 = 0.1e1 / t7 / t6;
        const Scalar t12 = fabs(t1826 * t1823 + b * t1824);
        const Scalar t1839 = t12 / 0.5e1;
        const Scalar t1827 = b * b;
        const Scalar t1828 = a * a;
        const Scalar t1829 = 0.1e1 / a;
        const Scalar t13 = t1826 * t1826;
        const Scalar t1836 = t13 * t1823;
        const Scalar t1837 = (b * t1839 - t1836 / 0.4e1 + t1827 * t1824 / 0.4e1) * t1828 * t1829;
        return  w[0] * t1828 * t1839 + w[1] * t1837 + w[2] * (0.2e1 * b * t1837 + t1827 * a * t1839 - t1828 * t1836) * t1829 / 0.3e1;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<6,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1853 = 0.1e1 / a;
        const Scalar t1866 = b * t1853;
        const Scalar t1847 = a - b;
        const Scalar t1 = fabs(t1847);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t4 = t3 * t3;
        const Scalar t1844 = 0.1e1 / t4;
        const Scalar t5 = fabs(b);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t8 = t7 * t7;
        const Scalar t1845 = 0.1e1 / t8;
        const Scalar t12 = fabs(t1847 * t1844 + b * t1845);
        const Scalar t1865 = t12 / 0.7e1;
        const Scalar t13 = t1847 * t1847;
        const Scalar t1863 = t13 * t1844;
        const Scalar t1854 = a * a;
        const Scalar t1852 = a * t1854;
        const Scalar t1862 = t1852 * t1863;
        const Scalar t1851 = b * b;
        const Scalar t1850 = w[0];
        const Scalar t1849 = w[1];
        const Scalar t1848 = w[2];
        const Scalar t1842 = (b * t1865 - t1863 / 0.6e1 + t1851 * t1845 / 0.6e1) * t1852;
        const Scalar t1841 = 0.4e1 * t1842 * t1866 + t1851 * t1854 * t1865 - t1862;
        res[0] = t1850 * t1852 * t1865 + (t1849 * t1842 + t1848 * t1841 / 0.5e1) * t1853;
        res[1] = (t1850 * t1842 + t1848 * (0.2e1 * t1851 * t1842 / t1854 - t1862) / 0.4e1 + (t1849 / 0.5e1 + t1848 * t1866 / 0.10e2) * t1841) * t1853;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<6,2> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1880 = a - b;
        const Scalar t1875 = fabs(t1880);
        const Scalar t1889 = t1875 * t1875;
        const Scalar t1 = 0.1e1 / t1889;
        const Scalar t2 = t1889 * t1889;
        const Scalar t1873 = t1 / t2;
        const Scalar t1879 = fabs(b);
        const Scalar t1893 = t1879 * t1879;
        const Scalar t4 = 0.1e1 / t1893;
        const Scalar t5 = t1893 * t1893;
        const Scalar t1876 = t4 / t5;
        const Scalar t10 = fabs(t1880 * t1873 + b * t1876);
        const Scalar t1903 = t10 / 0.5e1;
        const Scalar t1874 = t1 * t1873;
        const Scalar t1877 = t4 * t1876;
        const Scalar t14 = fabs(t1880 * t1874 + b * t1877);
        const Scalar t1902 = t14 / 0.7e1;
        const Scalar t1884 = b * b;
        const Scalar t1886 = a * a;
        const Scalar t1887 = 0.1e1 / a;
        const Scalar t1878 = t1880 * t1880;
        const Scalar t1900 = t1878 * t1873;
        const Scalar t1901 = (b * t1903 - t1900 / 0.4e1 + t1884 * t1876 / 0.4e1) * t1886 * t1887;
        const Scalar t1899 = t1878 * t1874;
        const Scalar t1881 = w[2];
        const Scalar t1898 = t1881 * t1887;
        const Scalar t1885 = a * t1886;
        const Scalar t1897 = t1885 * t1899;
        const Scalar t1883 = w[0];
        const Scalar t1882 = w[1];
        const Scalar t1869 = (b * t1902 - t1899 / 0.6e1 + t1884 * t1877 / 0.6e1) * t1885;
        const Scalar t1868 = 0.4e1 * b * t1869 * t1887 + t1884 * t1886 * t1902 - t1897;
        res[0] = t1883 * t1886 * t1903 + t1882 * t1901 + (0.2e1 * b * t1901 + t1884 * a * t1903 - t1886 * t1900) * t1898 / 0.3e1;
        res[1] = t1883 * t1885 * t1902 + (t1882 * t1869 + t1881 * t1868 / 0.5e1) * t1887;
        res[2] = (t1883 * t1869 + t1881 * (0.2e1 * t1884 * t1869 / t1886 - t1897) / 0.4e1 + (t1882 / 0.5e1 + b * t1898 / 0.10e2) * t1868) * t1887;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 6 //
    // and weight of degree 3      //
    /////////////////////////////////

    template<>
    inline Scalar StandardCauchyInverse_segment_F<6,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w)
    {
        const Scalar t1916 = w[2];
        const Scalar t1915 = w[3];
        const Scalar t2 = b * b;
        const Scalar t1908 = a * c - t2;
        const Scalar t1906 = sqrt(t1908);
        const Scalar t1907 = 0.1e1 / t1908;
        const Scalar t1910 = a - 0.2e1 * b + c;
        const Scalar t4 = t1910 * t1910;
        const Scalar t1909 = 0.1e1 / t4;
        const Scalar t1911 = a - b;
        const Scalar t5 = c * c;
        const Scalar t1912 = 0.1e1 / t5;
        const Scalar t6 = sqrt(t1907);
        const Scalar t8 = atan2(t1911, t1906);
        const Scalar t9 = atan2(-b, t1906);
        const Scalar t1914 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t1911 / t1910 + b / c) * t1907 + t1911 * t1909 + b * t1912) * t1907 / 0.4e1;
        const Scalar t1913 = 0.1e1 / a;
        return  w[0] * t1914 + (t1916 * (c * t1914 - t1909) / 0.3e1 - t1915 * t1909 / 0.2e1 + (w[1] + (0.2e1 / 0.3e1 * t1916 * b + t1915 * c) * t1913) * (b * t1914 - t1909 / 0.4e1 + t1912 / 0.4e1)) * t1913;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF<6,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t1943 = 0.1e1 / c;
        const Scalar t1925 = a - 0.2e1 * b + c;
        const Scalar t1942 = 0.1e1 / t1925;
        const Scalar t2 = c * c;
        const Scalar t1941 = 0.1e1 / t2;
        const Scalar t3 = t1925 * t1925;
        const Scalar t1940 = 0.1e1 / t3;
        const Scalar t5 = b * b;
        const Scalar t1923 = a * c - t5;
        const Scalar t1922 = 0.1e1 / t1923;
        const Scalar t1939 = a * t1922;
        const Scalar t1924 = t1942 * t1940;
        const Scalar t1931 = t1943 * t1941;
        const Scalar t1921 = sqrt(t1923);
        const Scalar t1926 = a - b;
        const Scalar t6 = sqrt(t1922);
        const Scalar t8 = atan2(t1926, t1921);
        const Scalar t9 = atan2(-b, t1921);
        const Scalar t1937 = (0.5e1 / 0.4e1 * (0.3e1 / 0.2e1 * (a * t6 * (t8 - t9) + t1926 * t1942 + b * t1943) * t1939 + t1926 * t1940 + b * t1941) * t1939 + t1926 * t1924 + b * t1931) * t1922 / 0.6e1;
        const Scalar t1919 = b * t1937 - t1924 / 0.6e1 + t1931 / 0.6e1;
        const Scalar t1932 = 0.1e1 / a;
        const Scalar t1918 = 0.4e1 * b * t1919 * t1932 + c * t1937 - t1924;
        const Scalar t1938 = -t1924 / 0.4e1 + (0.2e1 / 0.5e1 * b * t1918 + 0.2e1 * c * t1919) * t1932 / 0.4e1;
        const Scalar t1930 = w[0];
        const Scalar t1929 = w[1];
        const Scalar t1928 = w[2];
        const Scalar t1927 = w[3];
        res[0] = t1930 * t1937 + (t1929 * t1919 + t1928 * t1918 / 0.5e1 + t1927 * t1938) * t1932;
        res[1] = (t1930 * t1919 + t1928 * t1938 - t1927 * t1924 / 0.3e1 + (t1929 / 0.5e1 + t1927 * c * t1932 / 0.5e1) * t1918) * t1932;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF<6,3> (const Scalar a, const Scalar b, const Scalar c, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t1974 = 0.1e1 / c;
        const Scalar t1955 = a - 0.2e1 * b + c;
        const Scalar t1973 = 0.1e1 / t1955;
        const Scalar t2 = c * c;
        const Scalar t1961 = 0.1e1 / t2;
        const Scalar t3 = t1955 * t1955;
        const Scalar t1953 = 0.1e1 / t3;
        const Scalar t1957 = w[3];
        const Scalar t1972 = t1957 * c;
        const Scalar t1954 = t1973 * t1953;
        const Scalar t1962 = t1974 * t1961;
        const Scalar t5 = b * b;
        const Scalar t1952 = a * c - t5;
        const Scalar t1951 = 0.1e1 / t1952;
        const Scalar t1956 = a - b;
        const Scalar t1950 = sqrt(t1952);
        const Scalar t6 = sqrt(t1951);
        const Scalar t8 = atan2(t1956, t1950);
        const Scalar t9 = atan2(-b, t1950);
        const Scalar t1970 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t1956 * t1973 + b * t1974) * t1951 + t1956 * t1953 + b * t1961) * t1951;
        const Scalar t1969 = (0.5e1 / 0.4e1 * a * t1970 + t1956 * t1954 + b * t1962) * t1951 / 0.6e1;
        const Scalar t1946 = b * t1969 - t1954 / 0.6e1 + t1962 / 0.6e1;
        const Scalar t1963 = 0.1e1 / a;
        const Scalar t1945 = 0.4e1 * b * t1946 * t1963 + c * t1969 - t1954;
        const Scalar t1971 = -t1954 / 0.4e1 + (0.2e1 / 0.5e1 * b * t1945 + 0.2e1 * c * t1946) * t1963 / 0.4e1;
        const Scalar t1968 = t1970 / 0.4e1;
        const Scalar t1960 = w[0];
        const Scalar t1959 = w[1];
        const Scalar t1958 = w[2];
        res[0] = t1960 * t1968 + (t1958 * (c * t1968 - t1953) / 0.3e1 - t1957 * t1953 / 0.2e1 + (t1959 + (0.2e1 / 0.3e1 * t1958 * b + t1972) * t1963) * (b * t1968 - t1953 / 0.4e1 + t1961 / 0.4e1)) * t1963;
        res[1] = t1960 * t1969 + (t1959 * t1946 + t1958 * t1945 / 0.5e1 + t1957 * t1971) * t1963;
        res[2] = (t1960 * t1946 + t1958 * t1971 - t1957 * t1954 / 0.3e1 + (t1959 / 0.5e1 + t1963 * t1972 / 0.5e1) * t1945) * t1963;

    }
    
    template<>
    inline Scalar StandardCauchyInverse_segment_F_on_line<6,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w)
    {
        const Scalar t1996 = w[2];
        const Scalar t1995 = w[3];
        const Scalar t1980 = a - b;
        const Scalar t1 = fabs(t1980);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t1977 = 0.1e1 / t3 / t2;
        const Scalar t5 = fabs(b);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t1978 = 0.1e1 / t7 / t6;
        const Scalar t12 = fabs(t1980 * t1977 + b * t1978);
        const Scalar t1993 = t12 / 0.5e1;
        const Scalar t13 = t1980 * t1980;
        const Scalar t1991 = t13 * t1977;
        const Scalar t1982 = a * a;
        const Scalar t1990 = t1982 * t1991;
        const Scalar t1983 = 0.1e1 / a;
        const Scalar t1981 = b * b;
        return  w[0] * t1982 * t1993 + (t1996 * (t1981 * a * t1993 - t1990) / 0.3e1 - t1995 * t1990 / 0.2e1 + (w[1] + t1995 * t1981 / t1982 + 0.2e1 / 0.3e1 * t1996 * b * t1983) * (b * t1993 - t1991 / 0.4e1 + t1981 * t1978 / 0.4e1) * t1982) * t1983;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_GradF_on_line<6,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[2])
    {
        const Scalar t2011 = 0.1e1 / a;
        const Scalar t2028 = b * t2011;
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t4 = t3 * t3;
        const Scalar t2002 = 0.1e1 / t4;
        const Scalar t2009 = b * b;
        const Scalar t2013 = a * a;
        const Scalar t2010 = a * t2013;
        const Scalar t2004 = a - b;
        const Scalar t5 = fabs(t2004);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t8 = t7 * t7;
        const Scalar t2001 = 0.1e1 / t8;
        const Scalar t9 = t2004 * t2004;
        const Scalar t2023 = t9 * t2001;
        const Scalar t13 = fabs(t2004 * t2001 + b * t2002);
        const Scalar t2025 = t13 / 0.7e1;
        const Scalar t1999 = (b * t2025 - t2023 / 0.6e1 + t2009 * t2002 / 0.6e1) * t2010;
        const Scalar t2021 = t2010 * t2023;
        const Scalar t1998 = 0.4e1 * t1999 * t2028 + t2009 * t2013 * t2025 - t2021;
        const Scalar t2022 = t2009 / t2013;
        const Scalar t2027 = t1998 * t2028 / 0.10e2 + t1999 * t2022 / 0.2e1 - t2021 / 0.4e1;
        const Scalar t2026 = t1998 / 0.5e1;
        const Scalar t2008 = w[0];
        const Scalar t2007 = w[1];
        const Scalar t2006 = w[2];
        const Scalar t2005 = w[3];
        res[0] = t2008 * t2010 * t2025 + (t2007 * t1999 + t2006 * t2026 + t2005 * t2027) * t2011;
        res[1] = (t2008 * t1999 + t2007 * t2026 + t2006 * t2027 + t2005 * (0.3e1 / 0.5e1 * t1998 * t2022 - t2021) / 0.3e1) * t2011;

    }
    
    
    template<>
    inline void StandardCauchyInverse_segment_FGradF_on_line<6,3> (const Scalar a, const Scalar b, const std::vector<Scalar>& w, Scalar (&res)[3])
    {
        const Scalar t2043 = a - b;
        const Scalar t2038 = fabs(t2043);
        const Scalar t2054 = t2038 * t2038;
        const Scalar t1 = 0.1e1 / t2054;
        const Scalar t2 = t2054 * t2054;
        const Scalar t2036 = t1 / t2;
        const Scalar t2042 = fabs(b);
        const Scalar t2058 = t2042 * t2042;
        const Scalar t4 = 0.1e1 / t2058;
        const Scalar t5 = t2058 * t2058;
        const Scalar t2039 = t4 / t5;
        const Scalar t2051 = 0.1e1 / a;
        const Scalar t2071 = b * t2051;
        const Scalar t2040 = t4 * t2039;
        const Scalar t2048 = b * b;
        const Scalar t2050 = a * a;
        const Scalar t2049 = a * t2050;
        const Scalar t2037 = t1 * t2036;
        const Scalar t2041 = t2043 * t2043;
        const Scalar t2065 = t2041 * t2037;
        const Scalar t10 = fabs(t2043 * t2037 + b * t2040);
        const Scalar t2067 = t10 / 0.7e1;
        const Scalar t2032 = (b * t2067 - t2065 / 0.6e1 + t2048 * t2040 / 0.6e1) * t2049;
        const Scalar t2063 = t2049 * t2065;
        const Scalar t2031 = 0.4e1 * t2032 * t2071 + t2048 * t2050 * t2067 - t2063;
        const Scalar t2064 = t2048 / t2050;
        const Scalar t2070 = t2031 * t2071 / 0.10e2 + t2032 * t2064 / 0.2e1 - t2063 / 0.4e1;
        const Scalar t2069 = t2031 / 0.5e1;
        const Scalar t29 = fabs(t2043 * t2036 + b * t2039);
        const Scalar t2068 = t29 / 0.5e1;
        const Scalar t2066 = t2041 * t2036;
        const Scalar t2062 = t2050 * t2066;
        const Scalar t2047 = w[0];
        const Scalar t2046 = w[1];
        const Scalar t2045 = w[2];
        const Scalar t2044 = w[3];
        res[0] = t2047 * t2050 * t2068 + (t2045 * (t2048 * a * t2068 - t2062) / 0.3e1 - t2044 * t2062 / 0.2e1 + (t2046 + t2044 * t2064 + 0.2e1 / 0.3e1 * t2045 * t2071) * (b * t2068 - t2066 / 0.4e1 + t2048 * t2039 / 0.4e1) * t2050) * t2051;
        res[1] = t2047 * t2049 * t2067 + (t2046 * t2032 + t2045 * t2069 + t2044 * t2070) * t2051;
        res[2] = (t2047 * t2032 + t2046 * t2069 + t2045 * t2070 + t2044 * (0.3e1 / 0.5e1 * t2031 * t2064 - t2063) / 0.3e1) * t2051;

    }
    
    



} // Close namespace StandardCauchyAndInverseOptim
} // Close namespace convol
} // Close namespace expressive

#endif // CONVOL_STANDARD_CAUCHY_AND_INVERSE_OPTIMIZED_FUNCTIONS_H_

