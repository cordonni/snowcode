/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
  \file: Cauchy.h

  Language: C++

  License: Convol Licence

  \author: Cedric Zanni
  E-Mail: cedric.zanni@inria.fr

  Description: Header file for optimized HomotheticCauchy kernel

  Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_FUNCTOR_KERNEL_HOMOTHETIC_CAUCHY_H_
#define CONVOL_FUNCTOR_KERNEL_HOMOTHETIC_CAUCHY_H_

// core dependencies
#include <core/CoreRequired.h>
    // Geometry
    #include<core/geometry/CsteWeightedSegmentWithDensity.h>
    #include<core/geometry/Sphere.h>
    #include<core/geometry/WeightedPoint.h>
    #include<core/geometry/WeightedPointWithDensity.h>
    #include<core/geometry/OptWeightedSegment.h>
    #include<core/geometry/WeightedSegmentWithDensity.h> //TODO:@todo : should be changed
    #include<core/geometry/WeightedTriangle.h>

// convol dependencies
    // Kernels
    #include<convol/functors/kernels/cauchyinverse/Cauchy.h>
    #include<convol/functors/kernels/cauchyinverse/CauchyIsoValueAtDistance.h>

namespace expressive {

namespace convol {

/*! \Brief Base class for all HomotheticCauchy kernel functor
*
*/
template<int DEGREE>
class HomotheticCauchy : public Cauchy<DEGREE>
{
public:
    typedef core::Sphere Sphere;
    typedef core::CsteWeightedSegmentWithDensity CsteWeightedSegmentWithDensity;
    typedef core::WeightedPoint WeightedPoint;
    typedef core::OptWeightedSegment OptWeightedSegment;
    typedef core::WeightedSegmentWithDensity WeightedSegmentWithDensity;
    typedef core::WeightedTriangle WeightedTriangle;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    HomotheticCauchy(Scalar scale = 0.3) : Cauchy<DEGREE>(scale) { }
    virtual ~HomotheticCauchy(){}

    /////////////////////////////
    /// Name & Type functions ///
    /////////////////////////////

    static const std::string &StaticName()
    {
        const static std::string name_ = "HomotheticCauchy" + std::to_string(DEGREE);
        return name_;
    }
    virtual std::string Name() const
    {
        return StaticName();
    }

    static core::FunctorTypeTree BuildStaticType()
    {
        std::vector<core::FunctorTypeTree> vec;
        return core::FunctorTypeTree(StaticName(), vec);
    }
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defines functions

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    //-------------------------------------------------------------------------
    // Point evaluation -------------------------------------------------------
    //-------------------------------------------------------------------------
    Scalar Eval(const core::WeightedPoint& p_source, const Point& point) const;
    //-------------------------------------------------------------------------
    inline Scalar operator() (const core::WeightedPoint& p_source, const Point& point) const
    {
        return Eval(p_source, point);
    }
    //-------------------------------------------------------------------------
    Vector EvalGrad(const core::WeightedPoint& p_source, const Point& point, Scalar /*epsilon*/) const;
    //-------------------------------------------------------------------------
    void EvalValueAndGrad(const core::WeightedPoint& p_source, const Point& point, Scalar /*epsilon_grad*/,
                          Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------
    void EvalHomotheticValues(const core::WeightedPoint& p_source, const Point& point,
                              Scalar& homothetic_value_res, Vector& homothetic_grad_res) const;
    //-------------------------------------------------------------------------
    // Point with Density evaluation ------------------------------------------
    //-------------------------------------------------------------------------
    Scalar Eval(const core::WeightedPointWithDensity& p_source, const Point& point) const
    {
        return p_source.density() * Eval(((core::WeightedPoint)p_source), point);
    }
    //-------------------------------------------------------------------------
    Vector EvalGrad(const core::WeightedPointWithDensity& p_source, const Point& point, Scalar epsilon) const
    {
        return p_source.density() * EvalGrad(((core::WeightedPoint)p_source), point, epsilon);
    }
    //-------------------------------------------------------------------------
    void EvalValueAndGrad(const core::WeightedPointWithDensity& p_source, const Point& point, Scalar epsilon_grad,
                          Scalar& value_res, Vector& grad_res) const
    {
        EvalValueAndGrad(((core::WeightedPoint)p_source), point, epsilon_grad,
                         value_res, grad_res);
        value_res *= p_source.density();
        grad_res *= p_source.density();
    }
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    // Segment evaluation -----------------------------------------------------
    //-------------------------------------------------------------------------
    Scalar Eval(const core::OptWeightedSegment& seg, const Point& point) const;
    //-------------------------------------------------------------------------
    inline Scalar operator() (const core::OptWeightedSegment& seg_source, const Point& point) const
    {
        return Eval(seg_source, point);
    }
    //-------------------------------------------------------------------------
    Vector EvalGrad(const core::OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const;
    //-------------------------------------------------------------------------
    void EvalValueAndGrad(const core::OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                          Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------
    // Two following functions compute integral for constant weight along a segment represented
    // by some base parameters (can be useful when "segments" are created on the fly such as in SemiNumericalKernel)
    //-------------------------------------------------------------------------
    Scalar Eval( const Point& p_1, Scalar w_1,
                        const Vector& unit_dir, Scalar length,
                        const Point& point) const ;
    //-------------------------------------------------------------------------
    void EvalValueAndGrad( const Point& p_1, Scalar w_1,
                                  const Vector& unit_dir, Scalar length,
                                  const Point& point, Scalar /*epsilon_grad*/,
                                  Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------
    // Segment with Density evaluation ----------------------------------------
    //-------------------------------------------------------------------------
    Scalar Eval(const core::WeightedSegmentWithDensity& seg, const Point& point) const;
    //-------------------------------------------------------------------------
    inline Scalar operator() (const core::WeightedSegmentWithDensity& seg_source, const Point& point) const
    {
        return Eval(seg_source, point);
    }
    //-------------------------------------------------------------------------
    Vector EvalGrad(const core::WeightedSegmentWithDensity& seg, const Point& point, Scalar /*epsilon*/) const;
    //-------------------------------------------------------------------------
    void EvalValueAndGrad(const core::WeightedSegmentWithDensity& seg, const Point& point, Scalar /*epsilon_grad*/,
                                 Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------
    // Clipped Segment evaluation ( for Witnessed Operator) -------------------
    //-------------------------------------------------------------------------
    Scalar EvalClipped(const core::WeightedSegmentWithDensity& seg, const core::Sphere& clipping_sphere) const;
    //-------------------------------------------------------------------------
    void EvalValueAndGradClipped(const core::WeightedSegmentWithDensity& seg, const core::Sphere& clipping_sphere, Scalar /*epsilon_grad*/,
                                        Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------
    // Special Segment evaluation ( for Gradient-based Integral Surfaces) -----
    // WARNING : Homothetic field value is not normalized ---------------------
    //-------------------------------------------------------------------------
    void EvalHomotheticValues(const core::OptWeightedSegment& seg, const Point& point,
                              Scalar& homothetic_value_res, Vector& homothetic_grad_res) const;
    //-------------------------------------------------------------------------
    void EvalHomotheticValues( const Point& p_1, Scalar w_1,
                               const Vector& unit_dir, Scalar length,
                               const Point& point,
                               Scalar& homothetic_value_res, Vector& homothetic_grad_res) const;
    //-------------------------------------------------------------------------
    //-- Segment with constant weight and density used for correctyors --------
    //-------------------------------------------------------------------------
    Scalar Eval(const CsteWeightedSegmentWithDensity& seg, const Point& point) const;
    //-------------------------------------------------------------------------
    Vector EvalGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar /*epsilon*/) const;
    //-------------------------------------------------------------------------
    void EvalValueAndGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar /*epsilon_grad*/,
                          Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------


    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    /*! \Brief Given the thickness we want in the middle of a segment for which the length is known,
     *          this function returns the corresponding iso value such that a convolution along a
     *          segment with constant weights equal to 1.0 and of length will lead to a surface with
     *          a maximum thickness equel to thickness
     *
     *   \param length : the length of the segment to be convolued
     *   \param thickness : the thickness we want for our surface
     *	\return The iso value to set to get this thickness
     */
    Scalar GetIsoValueForThickness(Scalar length, Scalar thickness) const;

    /*! \Brief Gives a boundary for field with respect to value.
     *		More precisely, this function returns a distance B such that any
     *		point p such that:
     *		distance(p,primitive) > B => p is outside the surface
     *   \param primitive Primitive creating the scalar field.
     *   \param value
     *	\return Scalar euclidian distance ensuring that any point farther than this distance is outside the surface.
     */
    inline Scalar GetEuclidDistBound(const core::WeightedPoint& point, Scalar value) const
    { 
        return point.weight() * HomotheticCauchy<DEGREE>::InverseFct(value/this->normalization_factor_0D_);
    }
    
    inline Scalar GetEuclidDistBound(const core::OptWeightedSegment& seg, Scalar value) const
    {
        return GetEuclidDistBound1D(seg.weight_min()+seg.unit_delta_weight()*seg.length(), seg.length(), value);
    }

    inline Scalar GetEuclidDistBound(const core::WeightedSegmentWithDensity& seg, Scalar value) const
    {
        return GetEuclidDistBound1D(seg.opt_w_seg().weight_min()+seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().length(), seg.opt_w_seg().length(), value);
    }

    //TODO:@todo : triangle are not well treated ...
    inline Scalar GetEuclidDistBound(const core::WeightedTriangle& tri, Scalar /*value*/) const
    { 
    	//TODO:@todo : this should be implemented properly ...!
        Scalar weight_max = (tri.weight_p1()>tri.weight_p2()) ? tri.weight_p1() : tri.weight_p2() ;
        if(weight_max>tri.weight_p3())
        {
            return 100.0*weight_max;
        }
        else
        {
            return 100.0*tri.weight_p3();
        }
    }

    /*! \Brief TODO:@todo
     */
    inline Scalar GetEuclidDistBound1D(Scalar weight, Scalar value) const
    {
        //TODO:@todo : a implementer correctement
        return GetEuclidDistBound1D(weight, 10000.0, value);
    }

    inline Scalar GetEuclidDistBound1D(Scalar weight, Scalar length, Scalar value) const
    {
//TODO:@todo : value to be replaced by i_
    Scalar inv_special_value = this->normalization_factor_1D_/(weight*value);

    if(length*inv_special_value<=1.0)
    {
        return 0.0;
    }
    else
    {
       if(2.0*M_PI*M_PI*inv_special_value*inv_special_value >= this->degree_r()*this->inv_scale_2_)
       {
          return weight * (std::min(sqrt( (pow( length*inv_special_value, 2.0/this->degree_r()) - 1.0)/this->inv_scale_2_ ),
                                    sqrt((pow( M_PI*inv_special_value*sqrt(2.0/(this->degree_r()*this->inv_scale_2_)), 2.0/(this->degree_r()-1.0)) - 1.0)/this->inv_scale_2_ )));
       }
       else
       {
          return weight * (sqrt( (pow( length*inv_special_value, 2.0/this->degree_r()) - 1.0)/this->inv_scale_2_ ));
       }
    }
    }
    
}; // End class HomotheticCauchy declaration

/// Typdef for useable degree
typedef HomotheticCauchy<3> HomotheticCauchy3;
typedef HomotheticCauchy<4> HomotheticCauchy4;
typedef HomotheticCauchy<5> HomotheticCauchy5;
typedef HomotheticCauchy<6> HomotheticCauchy6;

} // Close namespace convol

} // Close namespace expressive

// implementation file
#include <convol/functors/kernels/cauchyinverse/HomotheticCauchy.hpp>

#endif // CONVOL_FUNCTOR_KERNEL_HOMOTHETIC_CAUCHY_H_

