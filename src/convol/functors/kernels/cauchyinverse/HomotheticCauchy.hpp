#include<convol/functors/kernels/cauchyinverse/HomotheticCauchyAndInverseOptimizedFunctions.h>
#include<convol/functors/kernels/cauchyinverse/StandardCauchyAndInverseOptimizedFunctions.h>

namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////


//-------------------------------------------------------------------------
// Point evaluation -------------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar HomotheticCauchy<DEGREE>::Eval(const WeightedPoint& p_source, const Point& point) const
{
    const Scalar dist2 = (point-p_source).squaredNorm() / (p_source.weight()*p_source.weight());

    const Scalar aux = 1.0+this->inv_scale_2_*dist2;
    Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2>(aux);
    if(DEGREE%2 == 0) {
        return this->normalization_factor_0D_/(int_power_aux);
    } else {
        return this->normalization_factor_0D_/(int_power_aux*sqrt(aux));
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector HomotheticCauchy<DEGREE>::EvalGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon*/) const
{
    const Vector direction = point - p_source;
    const Scalar dist2 = direction.squaredNorm() / (p_source.weight()*p_source.weight());

    const Scalar aux = 1.0+this->inv_scale_2_*dist2;

    Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2+1>(aux);
    if(DEGREE%2 == 0) {
        return (-this->inv_scale_2_*this->degree_r()*this->normalization_factor_0D_/(int_power_aux)) * direction;
    } else {
        return (-this->inv_scale_2_*this->degree_r()*this->normalization_factor_0D_/(int_power_aux*sqrt(aux))) * direction;
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
void HomotheticCauchy<DEGREE>::EvalValueAndGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon_grad*/,
                                       Scalar& value_res, Vector& grad_res) const
{
    const Vector direction = point - p_source;
    const Scalar dist2 = direction.squaredNorm() / (p_source.weight()*p_source.weight());

    const Scalar aux = 1.0+this->inv_scale_2_*dist2;

    Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2>(aux);
    if(DEGREE%2 == 0) {
        value_res = this->normalization_factor_0D_/(int_power_aux);
        grad_res =  (-this->inv_scale_2_*this->degree_r()*this->normalization_factor_0D_/(int_power_aux*aux)) * direction;
    } else {
        value_res = this->normalization_factor_0D_/(int_power_aux*sqrt(aux));
        grad_res =  (-this->inv_scale_2_*this->degree_r()*this->normalization_factor_0D_/(int_power_aux*aux*sqrt(aux))) * direction;
    }
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
void HomotheticCauchy<DEGREE>::EvalHomotheticValues(const WeightedPoint& p_source, const Point& point,
                                           Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
    assert(false);
    UNUSED(point); UNUSED(p_source);
    homothetic_value_res = 0.0;
    homothetic_grad_res  = Vector::Zero();
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
// Segment evaluation -----------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar HomotheticCauchy<DEGREE>::Eval(const OptWeightedSegment& seg, const Point& point) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = seg.length()*seg.length() * (this->inv_scale_2_ + seg.unit_delta_weight()*seg.unit_delta_weight());
    const Scalar b = seg.length()* (this->inv_scale_2_* seg.increase_unit_dir().dot(p_min_to_point) - seg.unit_delta_weight()*seg.weight_min());
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm() + seg.weight_min()*seg.weight_min();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE>(special_coeff, seg.weight_min(), delta_w);

    return  this->normalization_factor_1D_ * seg.length() * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F<DEGREE>(a,b,c,special_coeff);
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector HomotheticCauchy<DEGREE>::EvalGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = seg.length()*seg.length() * (this->inv_scale_2_ + seg.unit_delta_weight()*seg.unit_delta_weight());
    const Scalar b = seg.length()* (this->inv_scale_2_ * seg.increase_unit_dir().dot(p_min_to_point) - seg.unit_delta_weight()*seg.weight_min());
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm() + seg.weight_min()*seg.weight_min();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE>(special_coeff, seg.weight_min(), delta_w);

    Scalar F0F1[2];
    HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_GradF<DEGREE>(a,b,c,special_coeff, F0F1);

    const Scalar factor = seg.length()*this->degree_r()*this->inv_scale_2_;
    return this->normalization_factor_1D_ * ((factor*F0F1[1]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point);
}
//-------------------------------------------------------------------------
template<int DEGREE>
void HomotheticCauchy<DEGREE>::EvalValueAndGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                                       Scalar& value_res, Vector& grad_res) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = seg.length()*seg.length() * (this->inv_scale_2_ + seg.unit_delta_weight()*seg.unit_delta_weight());
    const Scalar b = seg.length()* (this->inv_scale_2_* seg.increase_unit_dir().dot(p_min_to_point) - seg.unit_delta_weight()*seg.weight_min());
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm() + seg.weight_min()*seg.weight_min();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE>(special_coeff, seg.weight_min(), delta_w);

    Scalar F0F1F2[3];
    HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF<DEGREE>(a,b,c,special_coeff, F0F1F2);

    value_res = this->normalization_factor_1D_* seg.length() * F0F1F2[0];
    const Scalar factor = this->normalization_factor_1D_ * seg.length() * this->degree_r() * this->inv_scale_2_;
    grad_res = ((factor*F0F1F2[2]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

// Two following functions compute integral for constant weight along a segment represented
// by some base parameters (can be useful when "segments" are created on the fly such as in SemiNumericalKernel)

template<int DEGREE>
Scalar HomotheticCauchy<DEGREE>::Eval( const Point& p_1, Scalar w_1,
                                       const Vector& unit_dir, Scalar length,
                                       const Point& point) const
{
    const Vector p1_to_point = (point-p_1);
    const Scalar a = this->inv_scale_2_;
    const Scalar b = this->inv_scale_2_ * unit_dir.dot(p1_to_point);
    const Scalar c = this->inv_scale_2_ * p1_to_point.squaredNorm() + w_1*w_1;

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar pow_weight = w_1;
    Scalar int_power_pow_weight = kernelHelpFunction::IntPow<DEGREE-1>(pow_weight);


    return int_power_pow_weight * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_cste<DEGREE>(a,b,c, length);
}
//-------------------------------------------------------------------------
template<int DEGREE>
void HomotheticCauchy<DEGREE>::EvalValueAndGrad( const Point& p_1, Scalar w_1,
                                        const Vector& unit_dir, Scalar length,
                                        const Point& point, Scalar /*epsilon_grad*/,
                                        Scalar& value_res, Vector& grad_res) const
{
    const Vector p1_to_point = (point-p_1);
    const Scalar a = this->inv_scale_2_;
    const Scalar b = this->inv_scale_2_ * unit_dir.dot(p1_to_point);
    const Scalar c = this->inv_scale_2_ * p1_to_point.squaredNorm() + w_1*w_1;

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar pow_weight = w_1;
    Scalar int_power_pow_weight = kernelHelpFunction::IntPow<DEGREE-1>(pow_weight);

    Scalar F0F1F2[3];
    HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF_cste<DEGREE>(a,b,c, length, F0F1F2);

    value_res = int_power_pow_weight * F0F1F2[0];
    const Scalar factor = int_power_pow_weight * this->degree_r()*this->inv_scale_2_;
    grad_res = ((factor*F0F1F2[2]) * unit_dir - (factor*F0F1F2[1]) * p1_to_point);
}

//-------------------------------------------------------------------------
// Segment with Density evaluation ------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar HomotheticCauchy<DEGREE>::Eval(const WeightedSegmentWithDensity& /*seg*/, const Point& /*point*/) const
{
    assert(false && "implementation to be checked again...");
    return 0.0;
//const Vector p_min_to_point = (point-seg.opt_w_seg().p_min());
//const Scalar a = seg.opt_w_seg().length()*seg.opt_w_seg().length() * (inv_scale_2_ + seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().unit_delta_weight());
//const Scalar b = seg.opt_w_seg().length()* (inv_scale_2_* seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point) - seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().weight_min());
//const Scalar c = inv_scale_2_*p_min_to_point.squaredNorm() + seg.opt_w_seg().weight_min()*seg.opt_w_seg().weight_min();

//// Compute the new polynomial weight (see formulas in the ref)
//Scalar delta_w = seg.opt_w_seg().unit_delta_weight() * seg.opt_w_seg().length();
//Scalar special_coeff[5];
//const Scalar t8081 = delta_w * delta_w;
//const Scalar t8087 = t8081 * seg.opt_w_seg().weight_min();
//const Scalar t8083 = seg.opt_w_seg().weight_min() * seg.opt_w_seg().weight_min();
//const Scalar t8086 = t8083 * delta_w;
//const Scalar t8082 = seg.opt_w_seg().weight_min() * t8083;
//const Scalar t8080 = density_coeff(0);
//const Scalar t8079 = density_coeff(1);
//special_coeff[0] = t8080 * t8082;
//special_coeff[1] = t8079 * t8082 + 0.3e1 * t8080 * t8086;
//special_coeff[2] = 0.3e1 * t8079 * t8086 + 0.3e1 * t8080 * t8087;
//special_coeff[3] = 0.3e1 * t8079 * t8087 + t8080 * delta_w * t8081;


//return  normalization_factor_1D_ * seg.length() * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_i4(a,b,c,special_coeff);


}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector HomotheticCauchy<DEGREE>::EvalGrad(const WeightedSegmentWithDensity& /*seg*/, const Point& /*point*/, Scalar /*epsilon*/) const
{
    assert(false && "implementation to be checked again...");
    return Vector::Zero();

////TODO:@todo: use segment density !
//const Vector p_min_to_point = (point-seg.opt_w_seg().p_min());
//const Scalar a = seg.opt_w_seg().length()*seg.opt_w_seg().length() * (inv_scale_2_ + seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().unit_delta_weight());
//const Scalar b = seg.opt_w_seg().length()* (inv_scale_2_* seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point) - seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().weight_min());
//const Scalar c = inv_scale_2_*p_min_to_point.squaredNorm() + seg.opt_w_seg().weight_min()*seg.opt_w_seg().weight_min();

//// Compute the new polynomial weight (see formulas in the ref)
//Scalar delta_w = seg.opt_w_seg().unit_delta_weight() * seg.opt_w_seg().length();
//Scalar special_coeff[5];
//const Scalar t8090 = delta_w * delta_w;
//const Scalar t8096 = t8090 * seg.opt_w_seg().weight_min();
//const Scalar t8092 = seg.opt_w_seg().weight_min() * seg.opt_w_seg().weight_min();
//const Scalar t8095 = t8092 * delta_w;
//const Scalar t8091 = seg.opt_w_seg().weight_min() * t8092;
//const Scalar t8089 = density_coeff(0);
//const Scalar t8088 = density_coeff(1);
//special_coeff[0] = t8089 * t8091;
//special_coeff[1] = t8088 * t8091 + 0.3e1 * t8089 * t8095;
//special_coeff[2] = 0.3e1 * t8088 * t8095 + 0.3e1 * t8089 * t8096;
//special_coeff[3] = 0.3e1 * t8088 * t8096 + t8089 * delta_w * t8090;


//Scalar F0F1[2];
//HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_GradF_i4(a,b,c,special_coeff, F0F1);

//const Scalar factor = seg.length()*4.0*inv_scale_2_;
//return normalization_factor_1D_ * ((factor*F0F1[1]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point);

}
//-------------------------------------------------------------------------
template<int DEGREE>
void HomotheticCauchy<DEGREE>::EvalValueAndGrad(const WeightedSegmentWithDensity& /*seg*/, const Point& /*point*/, Scalar /*epsilon_grad*/,
                                       Scalar& /*value_res*/, Vector& /*grad_res*/) const
{
        assert(false && "implementation to be checked again...");
////TODO:@todo: use segment density !
//const Vector p_min_to_point = (point-seg.opt_w_seg().p_min());
//const Scalar a = seg.opt_w_seg().length()*seg.opt_w_seg().length() * (inv_scale_2_ + seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().unit_delta_weight());
//const Scalar b = seg.opt_w_seg().length()* (inv_scale_2_* seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point) - seg.opt_w_seg().unit_delta_weight()*seg.opt_w_seg().weight_min());
//const Scalar c = inv_scale_2_*p_min_to_point.squaredNorm() + seg.opt_w_seg().weight_min()*seg.opt_w_seg().weight_min();

//// Compute the new polynomial weight (see formulas in the ref)
//Scalar delta_w = seg.opt_w_seg().unit_delta_weight() * seg.opt_w_seg().length();
//Scalar special_coeff[5];
//const Scalar t8099 = delta_w * delta_w;
//const Scalar t8105 = t8099 * seg.opt_w_seg().weight_min();
//const Scalar t8101 = seg.opt_w_seg().weight_min() * seg.opt_w_seg().weight_min();
//const Scalar t8104 = t8101 * delta_w;
//const Scalar t8100 = seg.opt_w_seg().weight_min() * t8101;
//const Scalar t8098 = density_coeff(0);
//const Scalar t8097 = density_coeff(1);
//special_coeff[0] = t8098 * t8100;
//special_coeff[1] = t8097 * t8100 + 0.3e1 * t8098 * t8104;
//special_coeff[2] = 0.3e1 * t8097 * t8104 + 0.3e1 * t8098 * t8105;
//special_coeff[3] = 0.3e1 * t8097 * t8105 + t8098 * delta_w * t8099;


//Scalar F0F1F2[3];
//HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF_i4(a,b,c,special_coeff, F0F1F2);

//value_res = normalization_factor_1D_* seg.length() * F0F1F2[0];
//const Scalar factor = normalization_factor_1D_ * seg.length() * 4.0*inv_scale_2_;
//grad_res = ((factor*F0F1F2[2]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);


}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
// Clipped Segment evaluation ( for Witnessed Operator) -------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar HomotheticCauchy<DEGREE>::EvalClipped(const WeightedSegmentWithDensity& /*seg*/, const Sphere& /*clipping_sphere*/) const
{
    assert(false && "implementation to be checked again...");
    return 0.0;
//const Vector p1_to_point = (clipping_sphere.center_-seg.opt_w_seg().p_min());
//Scalar special_coeff[3] = { - inv_scale_2_ * p_min_to_point.squaredNorm() ,
//                            - inv_scale_2_ * seg.opt_w_seg().length()*(seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point)) ,
//                            - inv_scale_2_ * seg.opt_w_seg().length()*seg.opt_w_seg().length() };

//Scalar delta_w = seg.opt_w_seg().unit_delta_weight() * seg.opt_w_seg().length();

//const Scalar radius_sqr = inv_scale_2_ * clipping_sphere.radius_ * clipping_sphere.radius_;
//const Scalar w0_sqr = seg.opt_w_seg().weight_min()*seg.opt_w_seg().weight_min();
//const Scalar w0_w1 = -seg.opt_w_seg().weight_min()*delta_w;
//const Scalar w1_sqr = delta_w*delta_w;
//Scalar clipping_coeff[3] = {radius_sqr * w0_sqr + special_coeff[0] ,
//                            radius_sqr * w0_w1 + special_coeff[1] ,
//                            radius_sqr * w1_sqr + special_coeff[2] };

//Scalar clipped_l1, clipped_l2;
//if(seg.opt_w_seg().HomotheticClipping(clipping_coeff, clipped_l1, clipped_l2))
//{
//    const Scalar a = w1_sqr - special_coeff[2];
//    const Scalar b = w0_w1  - special_coeff[1];
//    const Scalar c = w0_sqr - special_coeff[0];

//    // Compute the new polynomial weight (see formulas in the ref) : this could be precomputed to improve efficiency
//    Scalar numerator_coeff[4];
//    const Scalar t8108 = delta_w * delta_w;
//    const Scalar t8106 = seg.opt_w_seg().weight_min() * seg.opt_w_seg().weight_min();
//    numerator_coeff[0] = seg.opt_w_seg().weight_min() * t8106;
//    numerator_coeff[1] = 0.3e1 * t8106 * delta_w;
//    numerator_coeff[2] = 0.3e1 * seg.opt_w_seg().weight_min() * t8108;
//    numerator_coeff[3] = delta_w * t8108;


//    return  seg.length() * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_i4(clipped_l1,clipped_l2,a,b,c,numerator_coeff);


//}
//else
//{
//   return 0.0;
//}
}
template<int DEGREE>
void HomotheticCauchy<DEGREE>::EvalValueAndGradClipped(const WeightedSegmentWithDensity& /*seg*/, const Sphere& /*clipping_sphere*/, Scalar /*epsilon_grad*/,
                                              Scalar& /*value_res*/, Vector& /*grad_res*/) const
{
        assert(false && "implementation to be checked again...");
//const Vector p1_to_point = (clipping_sphere.center_-seg.opt_w_seg().p_min());
//Scalar special_coeff[3] = { - inv_scale_2_ * p_min_to_point.squaredNorm() ,
//                            - inv_scale_2_ * seg.opt_w_seg().length()*(seg.opt_w_seg().increase_unit_dir().dot(p_min_to_point)) ,
//                            - inv_scale_2_ * seg.opt_w_seg().length()*seg.opt_w_seg().length() };

//Scalar delta_w = seg.opt_w_seg().unit_delta_weight() * seg.opt_w_seg().length();

//const Scalar radius_sqr = inv_scale_2_ * clipping_sphere.radius_ * clipping_sphere.radius_;
//const Scalar w0_sqr = seg.opt_w_seg().weight_min()*seg.opt_w_seg().weight_min();
//const Scalar w0_w1 = -seg.opt_w_seg().weight_min()*delta_w;
//const Scalar w1_sqr = delta_w*delta_w;
//Scalar clipping_coeff[3] = {radius_sqr * w0_sqr + special_coeff[0] ,
//                            radius_sqr * w0_w1 + special_coeff[1] ,
//                            radius_sqr * w1_sqr + special_coeff[2] };

//Scalar clipped_l1, clipped_l2;
//if(seg.opt_w_seg().HomotheticClipping(clipping_coeff, clipped_l1, clipped_l2))
//{
//    const Scalar a = w1_sqr - special_coeff[2];
//    const Scalar b = w0_w1  - special_coeff[1];
//    const Scalar c = w0_sqr - special_coeff[0];

//    // Compute the new polynomial weight (see formulas in the ref) : this could be precomputed to improve efficiency
//    Scalar numerator_coeff[4];
//    const Scalar t8112 = delta_w * delta_w;
//    const Scalar t8110 = seg.opt_w_seg().weight_min() * seg.opt_w_seg().weight_min();
//    numerator_coeff[0] = seg.opt_w_seg().weight_min() * t8110;
//    numerator_coeff[1] = 0.3e1 * t8110 * delta_w;
//    numerator_coeff[2] = 0.3e1 * seg.opt_w_seg().weight_min() * t8112;
//    numerator_coeff[3] = delta_w * t8112;


//    Scalar F0F1F2[3];
//    HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FgradF_i4(clipped_l1,clipped_l2,a,b,c,numerator_coeff, F0F1F2);

//    value_res = seg.length() * F0F1F2[0];
//    grad_res = (seg.length() * 4.0*inv_scale_2_)*((F0F1F2[2]*seg.length()) * seg.increase_unit_dir() - (F0F1F2[1]) * p_min_to_point);


//}
//else
//{
//    value_res = 0.0;
//    grad_res = Vector::Zero();
//}
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
void HomotheticCauchy<DEGREE>::EvalHomotheticValues(const OptWeightedSegment& /*seg*/, const Point& /*point*/,
                                           Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
assert(false);
homothetic_value_res = 0.0;
homothetic_grad_res = Vector::Zero();
}
//-------------------------------------------------------------------------
template<int DEGREE>
void HomotheticCauchy<DEGREE>::EvalHomotheticValues( const Point& /*p_1*/, Scalar /*w_1*/,
                                            const Vector& /*unit_dir*/, Scalar /*length*/,
                                            const Point& /*point*/,
                                            Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
assert(false);
homothetic_value_res = 0.0;
homothetic_grad_res = Vector::Zero();
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
template <int DEGREE>
Scalar HomotheticCauchy<DEGREE>::Eval(const CsteWeightedSegmentWithDensity& seg, const Point& point) const
{
    return seg.density() * this->normalization_factor_1D_
           * Eval(seg.p1(), seg.weight(), seg.unit_dir(), seg.length(), point);
}
//-------------------------------------------------------------------------
template <int DEGREE>
Vector HomotheticCauchy<DEGREE>::EvalGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar epsilon) const
{
    Scalar f_tmp; Vector grad_res;
    EvalValueAndGrad(seg.p1(), seg.weight(),
                     seg.unit_dir(), seg.length(),
                     point, epsilon, f_tmp, grad_res);
    return (seg.density() * this->normalization_factor_1D_)*grad_res;
}
//-------------------------------------------------------------------------
template <int DEGREE>
void HomotheticCauchy<DEGREE>::EvalValueAndGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar epsilon_grad,
                      Scalar& value_res, Vector& grad_res) const
{
    EvalValueAndGrad(seg.p1(), seg.weight(),
                     seg.unit_dir(), seg.length(),
                     point, epsilon_grad, value_res, grad_res);
    Scalar factor = seg.density() * this->normalization_factor_1D_;
    value_res *= factor;
    grad_res *= factor;
}
//-------------------------------------------------------------------------


//////////////////////////
/// Bounding functions ///
//////////////////////////

template <int DEGREE>
Scalar HomotheticCauchy<DEGREE>::GetIsoValueForThickness(Scalar length, Scalar thickness) const
{
    std::vector<Scalar> weight;
    weight.push_back(1.0);

    const Scalar a = this->inv_scale_2_*length*length;
    const Scalar b = a*0.5;
    const Scalar c = 1.0 + this->inv_scale_2_*thickness*thickness + a*0.25;

    return this->normalization_factor_1D_*length*StandardCauchyAndInverseOptim::StandardCauchyInverse_segment_F<DEGREE,0>(a,b,c,weight);
}


} // Close namespace convol

} // Close namespace expressive
