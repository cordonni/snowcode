#include <convol/functors/kernels/cauchyinverse/HornusInverse.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

static core::FunctorFactory::Type<HornusInverse<3> > HornusInverse3Type;
static core::FunctorFactory::Type<HornusInverse<4> > HornusInverse4Type;
static core::FunctorFactory::Type<HornusInverse<5> > HornusInverse5Type;
static core::FunctorFactory::Type<HornusInverse<6> > HornusInverse6Type;

} // Close namespace convol

} // Close namespace expressive
