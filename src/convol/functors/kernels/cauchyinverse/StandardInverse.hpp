#include<convol/functors/kernels/cauchyinverse/StandardCauchyAndInverseOptimizedFunctions.h>

namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////



//-------------------------------------------------------------------------
// Point evaluation -------------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar StandardInverse<DEGREE>::Eval(const WeightedPoint& p_source, const Point& point) const
{
    const Scalar aux = this->scale_ / ((point-p_source).norm());
    Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE>(aux);

    return this->normalization_factor_0D_ * p_source.weight()*int_power_aux;
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector StandardInverse<DEGREE>::EvalGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon*/) const
{
    const Vector direction = point - p_source;
    const Scalar dist = direction.norm();

    const Scalar aux = this->scale_ / dist;
    Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE+1>(aux);

    return (-this->normalization_factor_0D_ * this->degree_r()*p_source.weight()*int_power_aux/this->scale_) * direction;
}
//-------------------------------------------------------------------------
template<int DEGREE>
void StandardInverse<DEGREE>::EvalValueAndGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon_grad*/,
                                          Scalar& value_res, Vector& grad_res) const
{
    const Vector direction = point - p_source;
    const Scalar dist = direction.norm();

    const Scalar aux = this->scale_ / dist;
    Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE>(aux);

    value_res = this->normalization_factor_0D_ * p_source.weight()*int_power_aux;
    grad_res = (-value_res*this->degree_r()*aux/this->scale_) * direction;
}
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
// Segment evaluation -----------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar StandardInverse<DEGREE>::Eval(const OptWeightedSegment& seg, const Point& point) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = this->inv_scale_2_*seg.length()*seg.length();
    const Scalar b = this->inv_scale_2_*seg.length()*(seg.increase_unit_dir().dot(p_min_to_point));
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm();

    //TODO:@todo : to be changed, come from the time when we were using PolyWeightedSegment ...
    std::vector<Scalar> weight_coeff;
    weight_coeff.push_back(seg.weight_min());
    weight_coeff.push_back(seg.unit_delta_weight()*seg.length());

    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        return this->normalization_factor_1D_ * seg.length()*StandardCauchyAndInverseOptim::StandardCauchyInverse_segment_F<DEGREE,1>(a,b,c,weight_coeff);
    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            return this->normalization_factor_1D_ * seg.length()*StandardCauchyAndInverseOptim::StandardCauchyInverse_segment_F_on_line<DEGREE,1>(a,b,weight_coeff);
        }
        else
        {
            return expressive::kScalarMax;
        }
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector StandardInverse<DEGREE>::EvalGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = this->inv_scale_2_*seg.length()*seg.length();
    const Scalar b = this->inv_scale_2_*seg.length()*(seg.increase_unit_dir().dot(p_min_to_point));
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm();

    //TODO:@todo : to be changed, come from the time when we were using PolyWeightedSegment ...
    std::vector<Scalar> weight_coeff;
    weight_coeff.push_back(seg.weight_min());
    weight_coeff.push_back(seg.unit_delta_weight()*seg.length());

    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        Scalar F0F1[2];
        StandardCauchyAndInverseOptim::StandardCauchyInverse_segment_GradF<DEGREE,1>(a,b,c,weight_coeff, F0F1);

        const Scalar factor = this->normalization_factor_1D_ * seg.length()*this->degree_r()*this->inv_scale_2_;
        return (seg.length()*factor*F0F1[1]) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point;
    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            //TODO : @todo : the validity of gradient formula in the case where point is on the line must be checked
            Scalar F0F1[2];
            StandardCauchyAndInverseOptim::StandardCauchyInverse_segment_GradF_on_line<DEGREE,1>(a,b,weight_coeff, F0F1);

            const Scalar factor = this->normalization_factor_1D_ * seg.length()*this->degree_r()*this->inv_scale_2_;
            return (seg.length()*factor*F0F1[1]) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point;
        }
        else
        {
            return Vector::Zero();
        }
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
void StandardInverse<DEGREE>::EvalValueAndGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                                          Scalar& value_res, Vector& grad_res) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = this->inv_scale_2_*seg.length()*seg.length();
    const Scalar b = this->inv_scale_2_*seg.length()*(seg.increase_unit_dir().dot(p_min_to_point));
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm();

    //TODO:@todo : to be changed, come from the time when we were using PolyWeightedSegment ...
    std::vector<Scalar> weight_coeff;
    weight_coeff.push_back(seg.weight_min());
    weight_coeff.push_back(seg.unit_delta_weight()*seg.length());

    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        Scalar F0F1F2[3];
        StandardCauchyAndInverseOptim::StandardCauchyInverse_segment_FGradF<DEGREE,1>(a,b,c,weight_coeff, F0F1F2);

        value_res = this->normalization_factor_1D_ * seg.length() * F0F1F2[0];
        const Scalar factor = this->normalization_factor_1D_ * seg.length()* this->degree_r()*this->inv_scale_2_;
        grad_res = (seg.length()*factor*F0F1F2[2]) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point;
    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            //TODO : @todo : the validity of gradient formula in the case where point is on the line must be checked
            Scalar F0F1F2[3];
            StandardCauchyAndInverseOptim::StandardCauchyInverse_segment_FGradF_on_line<DEGREE,1>(a,b,weight_coeff, F0F1F2);

            value_res = this->normalization_factor_1D_ * seg.length() * F0F1F2[0];
            const Scalar factor = this->normalization_factor_1D_ * seg.length()*this->degree_r()*this->inv_scale_2_;
            grad_res = (seg.length()*factor*F0F1F2[2]) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point;
        }
        else
        {
            value_res = expressive::kScalarMax;
            grad_res = Vector::Zero();
        }
    }
}
//-------------------------------------------------------------------------


//////////////////////////
/// Bounding functions ///
//////////////////////////

template <int DEGREE>
Scalar StandardInverse<DEGREE>::GetIsoValueForThickness(Scalar length, Scalar thickness) const
{
    std::vector<Scalar> weight;
    weight.push_back(1.0);

    const Scalar a = this->inv_scale_2_*length*length;
    const Scalar b = a*0.5;
    const Scalar c = this->inv_scale_2_*thickness*thickness + a*0.25;

    return this->normalization_factor_1D_*length*StandardCauchyAndInverseOptim::StandardCauchyInverse_segment_F<DEGREE,0>(a,b,c,weight);
}


} // Close namespace convol

} // Close namespace expressive
