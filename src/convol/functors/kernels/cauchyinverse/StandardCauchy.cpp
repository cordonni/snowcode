#include <convol/functors/kernels/cauchyinverse/StandardCauchy.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

static core::FunctorFactory::Type<StandardCauchy<3> > StandardCauchy3Type;
static core::FunctorFactory::Type<StandardCauchy<4> > StandardCauchy4Type;
static core::FunctorFactory::Type<StandardCauchy<5> > StandardCauchy5Type;
static core::FunctorFactory::Type<StandardCauchy<6> > StandardCauchy6Type;

} // Close namespace convol

} // Close namespace expressive
