#include <convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

static core::FunctorFactory::Type<CompactedHomotheticInverse<3> > CompactedHomotheticInverse3Type;
static core::FunctorFactory::Type<CompactedHomotheticInverse<4> > CompactedHomotheticInverse4Type;
static core::FunctorFactory::Type<CompactedHomotheticInverse<5> > CompactedHomotheticInverse5Type;
static core::FunctorFactory::Type<CompactedHomotheticInverse<6> > CompactedHomotheticInverse6Type;

} // Close namespace convol

} // Close namespace expressive
