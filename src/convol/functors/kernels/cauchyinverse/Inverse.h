/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
  \file: Inverse.h

  Language: C++

  License: Convol Licence

  \author: Cedric Zanni
  E-Mail: cedric.zanni@inria.fr

  Description: Header file for optimized Inverse kernel

  Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_FUNCTOR_KERNEL_INVERSE_H_
#define CONVOL_FUNCTOR_KERNEL_INVERSE_H_

// core dependencies
#include <core/CoreRequired.h>
    #include<core/functor/MACRO_FUNCTOR_TYPE.h>

// convol dependencies
    // Kernels
    #include<convol/functors/kernels/Kernel.h>
    #include<convol/functors/kernels/cauchyinverse/InverseIsoValueAtDistance.h>

// std dependencies
#include<vector>

//// Additional dependency, to remove as soon as GetIsoValueForThicknessXD functions has been rewritten correctly.
//#include<Convol/include/functors/Kernels/GetIsoValueForThicknessHelperT.h>

namespace expressive {

namespace convol {

/*! \Brief Base class for all Inverse kernel functor
*
*/
template<int DEGREE>
class Inverse : public Kernel
{
public:
    typedef core::FunctorTypeTree FunctorTypeTree;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    Inverse(Scalar scale) : Kernel(scale)
    {
        ComputeNormalizationFactors();
    }

    virtual ~Inverse(){}

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    //-------------------------------------------------------------------------
    // Scalar evaluation (useful for numerical computation) -------------------
    //-------------------------------------------------------------------------
    Scalar Eval(Scalar r) const;
    inline Scalar operator() (Scalar r) const
    {
        return Eval(r);
    }
    Scalar EvalGrad(Scalar r, Scalar /*epsilon*/) const;
    Scalar InverseFct(Scalar y) const;

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool isCompact() const { return false; }
    
    /*! \Brief This function returns the iso value for a convolution over an infinite segment (2D geometry) at a given thickness around the geometry.
     *	Weights of the segment are normalized to 1.0 everywhere.
     * 
     *  \param thickness The thickness for which the field value is wanted (ie the distance to the segment at which we want the field value)
     *	\return The iso value to set to get a surface passing through this thickness
     */
    Scalar GetIsoValueForThicknessGeom1D(Scalar thickness) const
    {
        return InverseIsoValueAtDistance::GetIsoValueAtDistanceGeom1D(DEGREE, scale_, thickness);// * normalization_factor_1D_;
	//TODO:@todo : find why their is * normalization_factor_1D_ for compact polynomial and not for Inverse
    }
    
    /*! \Brief This function returns the iso value for a convolution over an infinite plane (2D geometry) at a given thickness around the geometry.
     *	Weight of the plane is normalized to 1.0 everywhere.
     *
     *  \param thickness The thickness for which the field value is wanted (ie the distance to the plane at which we want the field value)
     *	\return The iso value to set to get a surface passing through this thickness
     */
//    Scalar GetIsoValueForThicknessGeom2D(Scalar thickness) const
//    {
//// TODO : @todo : for now uses a large triangle and a value computation see details in the following function definition.
//        return GetIsoValueForThicknessHelperT<HomotheticInverse4T>::Geom2D(*this, thickness);
////        return InverseIsoValueAtDistance::GetIsoValueAtDistanceGeom2D(i_, scale_, thickness);
//    }


    /////////////////
    /// Modifiers ///
    /////////////////

    void set_scale(Scalar scale)
    {
        Kernel::set_scale(scale);
        ComputeNormalizationFactors();
    }

    /////////////////
    /// Accessors ///
    /////////////////

    constexpr inline static int degree() { return DEGREE; }
    constexpr inline static Scalar degree_r() { return ((Scalar)DEGREE); }
    
    Scalar normalization_factor_0D() const { return normalization_factor_0D_; }
    Scalar normalization_factor_1D() const { return normalization_factor_1D_; }
    Scalar normalization_factor_2D() const { return normalization_factor_2D_; }

    // TODO:@todo : this isn't clean ... : should find a better way to proceed
    Scalar normalization_factor_2D_homothetic() const { return normalization_factor_2D_homothetic_; }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    Scalar normalization_factor_0D_;
    Scalar normalization_factor_1D_;
    Scalar normalization_factor_2D_;

    Scalar normalization_factor_2D_homothetic_; //TODO:@todo : not clean ...

    virtual void ComputeNormalizationFactors()
    {
        normalization_factor_0D_ = 1.0 / InverseIsoValueAtDistance::GetIsoValueAtDistanceGeom0D(DEGREE, scale_, 1.0);
        normalization_factor_1D_ = 1.0 / InverseIsoValueAtDistance::GetIsoValueAtDistanceGeom1D(DEGREE, scale_, 1.0);
        normalization_factor_2D_ = 1.0 / InverseIsoValueAtDistance::GetIsoValueAtDistanceGeom2D(DEGREE, scale_, 1.0);

        normalization_factor_2D_homothetic_ = 1.0 / InverseIsoValueAtDistance::GetIsoValueAtDistanceGeom2D(DEGREE+1, scale_, 1.0);
    }

}; // End class (Inverse) declaration

template<int DEGREE>
bool operator==(const Inverse<DEGREE>& a, const Inverse<DEGREE>& b);

} // Close namespace convol

} // Close namespace expressive

// implementation file
#include <convol/functors/kernels/cauchyinverse/Inverse.hpp>

#endif // CONVOL_FUNCTOR_KERNEL_INVERSE_H_

