/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
  \file: CompactedHomotheticInverse.h

  Language: C++

  License: Convol Licence

  \author: Cedric Zanni
  E-Mail: cedric.zanni@inria.fr

  Description: Header file for optimized HomotheticInverse kernel with a fixed
               degree DEGREE (template parameter) that use an integrated smooth
               bounding box to limit its influence in space.

  Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_FUNCTOR_KERNEL_COMPATCED_HOMOTHETIC_INVERSE_H_
#define CONVOL_FUNCTOR_KERNEL_COMPATCED_HOMOTHETIC_INVERSE_H_

// convol dependencies
    // Functors
    #include<convol/functors/kernels/cauchyinverse/HomotheticInverse.h>

// std dependencies
#include<vector>

namespace expressive {

namespace convol {

/*!
 * \brief The CompactedHomotheticInverse class
 *        In this class the "scale_" multiplied by the weight is the kernel support
 */
template<int DEGREE>
class CompactedHomotheticInverse : public HomotheticInverse<DEGREE>
{
public:
    typedef core::Sphere Sphere;
    typedef core::CsteWeightedSegmentWithDensity CsteWeightedSegmentWithDensity;
    typedef core::WeightedPoint WeightedPoint;
    typedef core::OptWeightedSegment OptWeightedSegment;
    typedef core::WeightedSegmentWithDensity WeightedSegmentWithDensity;
    typedef core::WeightedTriangle WeightedTriangle;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    ///TODO:@todo : in this kernel scale*weight correspond to the kernel support
    CompactedHomotheticInverse(Scalar scale = 3.5, Scalar transition = 3.5)
        : HomotheticInverse<DEGREE>(scale), transition_(transition)
    { }
    virtual ~CompactedHomotheticInverse(){}

    /////////////////////////////
    /// Name & Type functions ///
    /////////////////////////////

    static const std::string &StaticName()
    {
        const static std::string name_ = "CompactedHomotheticInverse" + std::to_string(DEGREE);
        return name_;
    }
    virtual std::string Name() const
    {
        return StaticName();
    }

    static core::FunctorTypeTree BuildStaticType()
    {
        std::vector<core::FunctorTypeTree> vec;
        return core::FunctorTypeTree(StaticName(), vec);
    }
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defines functions

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////
    
    //-------------------------------------------------------------------------
    // Point evaluation -------------------------------------------------------
    //-------------------------------------------------------------------------
    Scalar Eval(const core::WeightedPoint& p_source, const Point& point) const;
    //-------------------------------------------------------------------------
    inline Scalar operator() (const core::WeightedPoint& p_source, const Point& point) const
    {
        return Eval(p_source, point);
    }
    //-------------------------------------------------------------------------
    Vector EvalGrad(const core::WeightedPoint& p_source, const Point& point, Scalar /*epsilon*/) const;
    //-------------------------------------------------------------------------
    void EvalValueAndGrad(const core::WeightedPoint& p_source, const Point& point, Scalar /*epsilon_grad*/,
                          Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------
    void EvalHomotheticValues(const core::WeightedPoint& p_source, const Point& point,
                              Scalar& homothetic_value_res, Vector& homothetic_grad_res) const;
    //-------------------------------------------------------------------------
    
    
    //-------------------------------------------------------------------------
    // Segment evaluation -----------------------------------------------------
    //-------------------------------------------------------------------------
    Scalar Eval(const core::OptWeightedSegment& seg, const Point& point) const;
    //-------------------------------------------------------------------------
    inline Scalar operator() (const core::OptWeightedSegment& seg_source, const Point& point) const
    {
        return Eval(seg_source, point);
    }
    //-------------------------------------------------------------------------
    Vector EvalGrad(const core::OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const;
    //-------------------------------------------------------------------------
    void EvalValueAndGrad(const core::OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                          Scalar& value_res, Vector& grad_res) const;
    //--------------------

//-------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------------
    
//    // Two following functions compute integral for constant weight along a segment represented
//    // by some base parameters (can be useful when "segments" are created on the fly such as in SemiNumericalKernel)
    
    Scalar Eval( const Point& p_1, Scalar w_1,
                 const Vector& unit_dir, Scalar length,
                 const Point& point) const ;
//    //-------------------------------------------------------------------------
    void EvalValueAndGrad( const Point& p_1, Scalar w_1,
                                  const Vector& unit_dir, Scalar length,
                                  const Point& point, Scalar /*epsilon_grad*/,
                                  Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------
    
    //-------------------------------------------------------------------------
    // Special Segment evaluation ( for Gradient-based Integral Surfaces) -----
    // WARNING : Homothetic field value is not normalized ---------------------
    //-------------------------------------------------------------------------
    void EvalHomotheticValues(const core::OptWeightedSegment& seg, const Point& point,
                              Scalar& homothetic_value_res, Vector& homothetic_grad_res) const;
    //-------------------------------------------------------------------------
    void EvalHomotheticValues( const Point& p_1, Scalar w_1,
                               const Vector& unit_dir, Scalar length,
                               const Point& point,
                               Scalar& homothetic_value_res, Vector& homothetic_grad_res) const;
    //-------------------------------------------------------------------------
    //-- Segment with constant weight and density used for correctyors --------
    //-------------------------------------------------------------------------
    Scalar Eval(const CsteWeightedSegmentWithDensity& seg, const Point& point) const;
    //-------------------------------------------------------------------------
    Vector EvalGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar /*epsilon*/) const;
    //-------------------------------------------------------------------------
    void EvalValueAndGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar /*epsilon_grad*/,
                          Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------

    ///////////
    /// Xml ///
    ///////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        HomotheticInverse<DEGREE>::AddXmlAttributes(element);
        element->SetDoubleAttribute("transition",transition_);
    }

    virtual void InitFromXml(const TiXmlElement* element);

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool isCompact() const { return true; }

    /*! \Brief Given the thickness we want in the middle of a segment for which the length is known,
     *          this function returns the corresponding iso value such that a convolution along a
     *          segment with constant weights equal to 1.0 and of length will lead to a surface with 
     *          a maximum thickness equel to thickness
     * 
     *   \param length : the length of the segment to be convolued 
     *   \param thickness : the thickness we want for our surface
     *	\return The iso value to set to get this thickness
     */
    Scalar GetIsoValueForThickness(Scalar length, Scalar thickness) const;

    /*! \Brief Gives a boundary for field with respect to value.
     *		More precisely, this function returns a distance B such that any
     *		point p such that:
     *		distance(p,primitive) > B => p is outside the surface
     *   \param primitive Primitive creating the scalar field.
     *   \param value
     *	\return Scalar euclidian distance ensuring that any point farther than this distance is outside the surface.
     */
    inline Scalar GetEuclidDistBound(const core::WeightedPoint& point, Scalar /*value*/) const
    {
        // TODO:@todo : checkthis one too.
        return this->scale_*point.weight();
    }

    //TODO:@todo : triangle are not well treated ...
    virtual  Scalar GetEuclidDistBound(const core::WeightedTriangle& tri, Scalar /*value*/) const
    {  // this is a "zero" bounding box !
        Scalar weight_max = (tri.weight_p1()>tri.weight_p2()) ? tri.weight_p1() : tri.weight_p2() ;
        if(weight_max>tri.weight_p3())
        {
            return this->scale_*weight_max;
        }
        else
        {
            return this->scale_*tri.weight_p3();
        }
    }

    Scalar GetEuclidDistBound(const core::OptWeightedSegment& seg, Scalar value) const
    { // this is a "zero" bounding box !
        return GetEuclidDistBound1D(seg.weight_min()+seg.unit_delta_weight()*seg.length(), value);
    }

    Scalar GetEuclidDistBound1D(Scalar weight, Scalar /*value*/) const
    {
        return this->scale_*weight;
    }

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_transition(Scalar transition) { transition_ = transition; }

    /////////////////
    /// Attributs ///
    /////////////////
protected :
    // bound for the transition (for a wanted radius of 1)
    Scalar transition_;

    virtual void ComputeNormalizationFactors()
    {
        this->normalization_factor_0D_ = 1.0 / InverseIsoValueAtDistance::GetIsoValueAtDistanceGeom0D(DEGREE, 1.0, 1.0);
        this->normalization_factor_1D_ = 1.0 / InverseIsoValueAtDistance::GetIsoValueAtDistanceGeom1D(DEGREE, 1.0, 1.0);
        this->normalization_factor_2D_ = 1.0 / InverseIsoValueAtDistance::GetIsoValueAtDistanceGeom2D(DEGREE, 1.0, 1.0);

        this->normalization_factor_2D_homothetic_ = 1.0 / InverseIsoValueAtDistance::GetIsoValueAtDistanceGeom2D(DEGREE+1, 1.0, 1.0);
    }

}; // End class CompectedHomotheticInverse declaration

/// Typdef for useable degree
typedef CompactedHomotheticInverse<3> CompactedHomotheticInverse3;
typedef CompactedHomotheticInverse<4> CompactedHomotheticInverse4;
typedef CompactedHomotheticInverse<5> CompactedHomotheticInverse5;
typedef CompactedHomotheticInverse<6> CompactedHomotheticInverse6;


} // Close namespace convol

} // Close namespace expressive

// implementation file
#include <convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.hpp>

#endif // CONVOL_FUNCTOR_KERNEL_COMPATCED_HOMOTHETIC_INVERSE_H_

