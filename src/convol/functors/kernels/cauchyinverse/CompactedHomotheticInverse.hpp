
#include<convol/functors/kernels/cauchyinverse/HomotheticCauchyAndInverseOptimizedFunctions.h>
#include<convol/functors/kernels/cauchyinverse/StandardCauchyAndInverseOptimizedFunctions.h>

#include <core/functor/FunctorFactory.h>

#include <core/xmlloaders/TraitsXmlQuery.h>

namespace expressive {

namespace convol {

///////////
/// Xml ///
///////////

template<int DEGREE>
void CompactedHomotheticInverse<DEGREE>::InitFromXml(const TiXmlElement* element)
{
    double scale;
    core::TraitsXmlQuery::QueryDoubleAttribute("CompactedHomotheticInverse::InitFromXml",
                                               element, "scale", &scale);
    Inverse<DEGREE>::set_scale(scale);

    double transition;
    if (element->Attribute("transition") != nullptr) {
        core::TraitsXmlQuery::QueryDoubleAttribute("CompactedHomotheticInverse::InitFromXml",
                                                   element, "transition", &transition);
        set_transition(transition);
    }
}

////////////////////////////
/// Evaluation functions ///
////////////////////////////

//-------------------------------------------------------------------------
// Point evaluation -------------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar CompactedHomotheticInverse<DEGREE>::Eval(const WeightedPoint& p_source, const Point& point) const
{
    const Scalar aux = p_source.weight() / ((point-p_source).norm());

    if(aux*this->scale_>1.0) {
        Scalar int_power_aux  = kernelHelpFunction::IntPow<DEGREE>(aux);
        return int_power_aux;
    } else {
        return 0.0;
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector CompactedHomotheticInverse<DEGREE>::EvalGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon*/) const
{
    const Vector direction = point - p_source;
    const Scalar dist = direction.norm();
    
    const Scalar aux = p_source.weight() / dist;

    if(aux*this->scale_>1.0)
    {
        Scalar int_power_aux  = kernelHelpFunction::IntPow<DEGREE+1>(aux);

        return (-this->degree_r()*int_power_aux/p_source.weight()) * direction;
    } else {
        return Vector::Zero(); // the gradient is undefined
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
void CompactedHomotheticInverse<DEGREE>::EvalValueAndGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon_grad*/,
                                                   Scalar& value_res, Vector& grad_res) const
{
    const Vector direction = point - p_source;
    const Scalar dist = direction.norm();
    
    const Scalar aux = p_source.weight() / dist;
    if(aux*this->scale_>1.0)
    {
        Scalar int_power_aux  = kernelHelpFunction::IntPow<DEGREE>(aux);

        value_res = p_source.weight()*int_power_aux;
        grad_res = (-this->degree_r()*int_power_aux*aux/p_source.weight()) * direction;
    } else {
        value_res = 0.0;
        grad_res  = Vector::Zero(); // the gradient is undefined
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
void CompactedHomotheticInverse<DEGREE>::EvalHomotheticValues(const WeightedPoint& p_source, const Point& point,
                                                       Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
    // WARNING : this is in fact Inverse kernel of degree i-1
    Vector direction = point - p_source;
    const Scalar dist = direction.norm();

    const Scalar aux = p_source.weight() / dist;

    if(aux*this->scale_>1.0)
    {
        Scalar int_power_aux  = kernelHelpFunction::IntPow<DEGREE-1>(aux);

        homothetic_value_res = int_power_aux;
        homothetic_grad_res  = (-(this->degree_r()-1.0)*int_power_aux*aux/dist ) * direction;
    } else {
        homothetic_value_res = 0.0;
        homothetic_grad_res  = Vector::Zero(); // the gradient is undefined
    }
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------


//    //-------------------------------------------------------------------------
//    // Segment evaluation -----------------------------------------------------
//    //-------------------------------------------------------------------------
template<int DEGREE>
Scalar CompactedHomotheticInverse<DEGREE>::Eval(const OptWeightedSegment& seg, const Point& point) const
{
    //TODO:@todo : compact this ...

    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = seg.length()*seg.length();
    const Scalar b = seg.length()*(seg.increase_unit_dir().dot(p_min_to_point));
    const Scalar c = p_min_to_point.squaredNorm();
    
    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE>(special_coeff, seg.weight_min(), delta_w);
    
    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        return  this->normalization_factor_1D_ * seg.length() * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F<4>(a,b,c,special_coeff);


    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            return this->normalization_factor_1D_ * seg.length()*HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_on_line<4>(a,b,special_coeff);
            

        }
        else
        {
            return expressive::kScalarMax;
        }
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector CompactedHomotheticInverse<DEGREE>::EvalGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const
{
    //TODO:@todo : compact this ...

    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = seg.length()*seg.length();
    const Scalar b = seg.length()*(seg.increase_unit_dir().dot(p_min_to_point));
    const Scalar c = p_min_to_point.squaredNorm();
    
    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE>(special_coeff, seg.weight_min(), delta_w);

    
    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        Scalar F0F1[2];
        HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_GradF<4>(a,b,c,special_coeff, F0F1);

        const Scalar factor = seg.length()*4.0;
        return this->normalization_factor_1D_ * ((factor*F0F1[1]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point);


    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            Scalar F0F1[2];
            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_GradF_on_line<4>(a,b,special_coeff, F0F1);
            
            const Scalar factor = seg.length()*4.0;
            return this->normalization_factor_1D_ * ((factor*F0F1[1]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point);
            

        }
        else
        {
            return Vector::Zero(); // the gradient is undefined
        }
    }
}
//    //-------------------------------------------------------------------------
template<int DEGREE>
void CompactedHomotheticInverse<DEGREE>::EvalValueAndGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                                                   Scalar& value_res, Vector& grad_res) const
{
    //TODO:@todo : compact this ...

    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = seg.length()*seg.length();
    const Scalar b = seg.length()*(seg.increase_unit_dir().dot(p_min_to_point));
    const Scalar c = p_min_to_point.squaredNorm();
    
    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE>(special_coeff, seg.weight_min(), delta_w);
    
    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        Scalar F0F1F2[3];
        HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF<4>(a,b,c,special_coeff, F0F1F2);

        value_res = this->normalization_factor_1D_* seg.length() * F0F1F2[0];
        const Scalar factor = this->normalization_factor_1D_ * seg.length() * 4.0;
        grad_res = ((factor*F0F1F2[2]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);
    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            Scalar F0F1F2[3];
            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF_on_line<4>(a,b,special_coeff, F0F1F2);
            
            value_res = this->normalization_factor_1D_ * seg.length() * F0F1F2[0];
            const Scalar factor = seg.length()*4.0;
            grad_res = this->normalization_factor_1D_ * ((factor*F0F1F2[2]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);
            

        }
        else
        {
            value_res = expressive::kScalarMax;
            grad_res = Vector::Zero(); // the gradient is undefined
        }
    }
}
//    //-------------------------------------------------------------------------
//    //-------------------------------------------------------------------------

//    // Two following functions compute integral for constant weight along a segment represented
//    // by some base parameters (can be useful when "segments" are created on the fly such as in SemiNumericalKernel)
template<int DEGREE>
Scalar CompactedHomotheticInverse<DEGREE>::Eval( const Point& p_1, Scalar w_1,
                                          const Vector& unit_dir, Scalar length,
                                          const Point& point) const
{
    //TODO:@todo : compact this ...

    const Vector p1_to_point = point-p_1;
    const Scalar a = 1.0;
    const Scalar b = unit_dir.dot(p1_to_point);
    const Scalar c = p1_to_point.squaredNorm();
    
    // Compute the new polynomial weight (see formulas in the ref)
    Scalar pow_weight = w_1;
    Scalar int_power_pow_weight = kernelHelpFunction::IntPow<DEGREE-1>(pow_weight);
    
    // Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        return int_power_pow_weight * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_cste<4>(a,b,c, length);
    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            return int_power_pow_weight * HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_F_on_line_cste<4>(a,b, length);
        }
        else
        {
            return expressive::kScalarMax;
        }
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
void CompactedHomotheticInverse<DEGREE>::EvalValueAndGrad( const Point& p_1, Scalar w_1,
                                                    const Vector& unit_dir, Scalar length,
                                                    const Point& point, Scalar /*epsilon_grad*/,
                                                    Scalar& value_res, Vector& grad_res) const
{
    //TODO:@todo : compact this ...

    const Vector p1_to_point = point-p_1;
    const Scalar a = 1.0;
    const Scalar b = unit_dir.dot(p1_to_point);
    const Scalar c = p1_to_point.squaredNorm();
    
    // Compute the new polynomial weight (see formulas in the ref)
    Scalar pow_weight = w_1;
    Scalar int_power_pow_weight = kernelHelpFunction::IntPow<DEGREE-1>(pow_weight);
    
    //  Manage cases when point is on the line defined by seg.
    const Scalar delta = a*c - b*b;
    if( delta > 0.0 )
    {
        Scalar F0F1F2[3];
        HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF_cste<4>(a,b,c, length, F0F1F2);

        value_res = int_power_pow_weight * F0F1F2[0];
        const Scalar factor = int_power_pow_weight * 4.0;
        grad_res = ((factor*F0F1F2[2]) * unit_dir - (factor*F0F1F2[1]) * p1_to_point);
    }
    else
    {
        // Check whether the evaluation point is on the line segment or not
        const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
        if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
        {
            Scalar F0F1F2[3];
            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_FGradF_on_line_cste<4>(a,b, length, F0F1F2);

            value_res = int_power_pow_weight * F0F1F2[0];
            const Scalar factor = int_power_pow_weight*4.0;
            grad_res = ((factor*F0F1F2[2]) * unit_dir - (factor*F0F1F2[1]) * p1_to_point);
        }
        else
        {
            value_res = expressive::kScalarMax;
            grad_res = Vector::Zero(); // the gradient is undefined
        }
    }
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------



//-------------------------------------------------------------------------
// Special Segment evaluation ( for Gradient-based Integral Surfaces) -----
//-------------------------------------------------------------------------
template<int DEGREE>
void CompactedHomotheticInverse<DEGREE>::EvalHomotheticValues(const OptWeightedSegment& seg, const Point& point,
                                                       Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
    const Vector p_min_to_point = point-seg.p_min();
    const Scalar uv = seg.increase_unit_dir().dot(p_min_to_point);
    const Scalar d2 = p_min_to_point.squaredNorm();

    //        Scalar h_dist = seg.HomotheticDistance(p_min_to_point, uv, d2);
    Scalar h_dist = 0; Vector h_grad = Vector::Zero();
    seg.HomotheticDistanceAndGrad(p_min_to_point, uv, d2, h_dist, h_grad);
    if(h_dist<this->scale_)
    {
        // Check if inside the transition area and update factor accordingly
        Scalar res_factor = 1.0;
        Scalar grad_res_factor = 0.0;

        if(h_dist>transition_)
        {
            ///TODO:@todo : The smoothing does not work and i don't understand why !
            /// and its "catastrophique" when using the gradient of the smoothing ...
            //                Scalar local_dist = (h_dist-transition_)/(scale_-transition_);
            //                res_factor = (1-local_dist*local_dist);
            //                grad_res_factor = -4.0*local_dist*res_factor / (scale_-transition_);
            //                res_factor *= res_factor;

            Scalar local_dist = (h_dist-transition_)/(this->scale_-transition_);
            Scalar local_dist_sqr = local_dist*local_dist;

            res_factor = (1.0-local_dist_sqr*local_dist_sqr);
            //                grad_res_factor = 2.0 * (-4.0/(scale_-transition_)) * local_dist*local_dist_sqr * res_factor ;
            res_factor *= res_factor;
        }
        h_grad *= grad_res_factor;

        const Scalar a = seg.length()*seg.length();
        const Scalar b = seg.length()*uv;
        const Scalar c = d2;

        // Compute the new polynomial weight (see formulas in the ref)
        Scalar delta_w = seg.unit_delta_weight() * seg.length();
        Scalar special_coeff[DEGREE];
        kernelHelpFunction::ExpandWeightPolynome<DEGREE>(special_coeff, seg.weight_min(), delta_w);

        //  Manage cases when point is on the line defined by seg.
        const Scalar delta = a*c - b*b;
        if( delta > 0.0 )
        {
            Scalar F0F1F2[3];
            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_ScalisF_HornusGradF<4>(a,b,c,
                                                                                                    delta_w, seg.weight_min(),
                                                                                                    special_coeff, F0F1F2);

            homothetic_value_res   = this->normalization_factor_1D_ * seg.length() * F0F1F2[0];

            const Scalar factor = res_factor * this->normalization_factor_1D_ * seg.length() * 4.0;
            homothetic_grad_res = (((factor*F0F1F2[2]) * seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point)
                    + (homothetic_value_res*grad_res_factor)*h_grad;

            homothetic_value_res *= res_factor;

            //                homothetic_value_res   = res_factor * normalization_factor_1D_ * seg.length() * F0F1F2[0];
            //                const Scalar factor = grad_res_factor * normalization_factor_1D_ * seg.length() * 4.0;
            //                homothetic_grad_res = (((factor*F0F1F2[2]) * seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);
            //TODO:@todo : see if it is possible to apply the method directly on squared value (avoid a sqrt)
        }
        else
        {
            // Check whether the evaluation point is on the line segment or not
            const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
            if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
            {
                Scalar F0F1F2[3];
                HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line<4>(a, b,
                                                                                                                delta_w, seg.weight_min(),
                                                                                                                special_coeff, F0F1F2);

                homothetic_value_res   = this->normalization_factor_1D_ * seg.length() * F0F1F2[0];

                const Scalar factor = res_factor * this->normalization_factor_1D_ * seg.length() * 4.0;
                homothetic_grad_res = (((factor*F0F1F2[2]) * seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point)
                        + (homothetic_value_res*grad_res_factor)*h_grad;

                homothetic_value_res *= res_factor;

                //                    homothetic_value_res   = res_factor * normalization_factor_1D_ * seg.length() * F0F1F2[0];
                //                    const Scalar factor = grad_res_factor * normalization_factor_1D_ * seg.length() * 4.0;
                //                    homothetic_grad_res = (((factor*F0F1F2[2]) * seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);
            }
            else
            {
                homothetic_value_res = expressive::kScalarMax;
                homothetic_grad_res = Vector::Constant(expressive::kScalarMax); //TODO : @todo ?
            }
        }
    }
    else
    {
        homothetic_value_res = 0.0;
        homothetic_grad_res = Vector::Zero(); // the gradient is undefined
    }
}
//    //-------------------------------------------------------------------------
template<int DEGREE>
void CompactedHomotheticInverse<DEGREE>::EvalHomotheticValues( const Point& p_1, Scalar w_1,
                                                        const Vector& unit_dir, Scalar length,
                                                        const Point& point,
                                                        Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{

    // WARNING : since this function is used during triangles computation only,
    // the optimized function use in fact a degree i+1 instead of i
    // TODO:@todo : this isn't a clean way to proceed ... !
    
    //TODO:@todo : should test the clipping !!!

    const Vector p1_to_point = point-p_1;
    const Scalar a = 1.0;
    const Scalar b = unit_dir.dot(p1_to_point);
    const Scalar c = p1_to_point.squaredNorm();
    
    // Test the support ...
    Scalar denum = length * w_1;
    Scalar t = length;
    if(denum > 0.0)
    {
        t = b ;
        t = (t<0.0) ? 0.0 : ((t>length) ? length : t) ; // clipping (nearest point on segment not line)
    }
    const Point proj_to_point = p1_to_point - t * unit_dir;
    Scalar dist = proj_to_point.norm();
    //        hgrad = (1.0/(dist*w_1)) * proj_to_point;
    Scalar h_dist = dist / w_1;

    if(h_dist<this->scale_)
    {
        //TODO:@todo : this is not the true smoothed gradient !!!
        Scalar res_factor = 1.0;
        if(h_dist>transition_)
        {
            Scalar local_dist = (h_dist-transition_)/(this->scale_-transition_);
            Scalar local_dist_sqr = local_dist*local_dist;
            res_factor = (1.0-local_dist_sqr*local_dist_sqr);
            res_factor *= res_factor;
        }


        // Compute the new polynomial weight (see formulas in the ref)
        Scalar pow_weight = w_1;
        Scalar int_power_pow_weight = kernelHelpFunction::IntPow<DEGREE>(pow_weight);

        //  Manage cases when point is on the line defined by seg.
        const Scalar delta = a*c - b*b;
        if( delta > 0.0 )
        {
            Scalar F0F1F2[3];
            HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_ScalisF_HornusGradF_cste<4>(a,b,c, length, F0F1F2);

            homothetic_value_res = res_factor*int_power_pow_weight * F0F1F2[0];
            const Scalar factor = res_factor*w_1 * int_power_pow_weight * 4.0;
            homothetic_grad_res = ((factor*F0F1F2[2]) * unit_dir - (factor*F0F1F2[1]) * p1_to_point);
        }
        else
        {
            // Check whether the evaluation point is on the line segment or not
            const Scalar n_coord_along_seg = b / a; // in [0;1] if the point is included in the segment
            if(n_coord_along_seg < 0.0 || n_coord_along_seg > 1.0)
            {
                Scalar F0F1F2[3];
                HomotheticCauchyAndInverseOptim::HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line_cste<4>(a, b, length, F0F1F2);

                homothetic_value_res = res_factor*int_power_pow_weight * F0F1F2[0];
                const Scalar factor = res_factor*w_1 * int_power_pow_weight*4.0;
                homothetic_grad_res = ((factor*F0F1F2[2]) * unit_dir - (factor*F0F1F2[1]) * p1_to_point);
            }
            else
            {
                homothetic_value_res = expressive::kScalarMax;
                homothetic_grad_res  = Vector::Zero(); // the gradient is undefined
            }
        }
    }
    else
    {
        homothetic_value_res = 0.0;
        homothetic_grad_res = Vector::Zero(); // the gradient is undefined
    }
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
template <int DEGREE>
Scalar CompactedHomotheticInverse<DEGREE>::Eval(const CsteWeightedSegmentWithDensity& seg, const Point& point) const
{
    return seg.density() * this->normalization_factor_1D_
           * Eval(seg.p1(), seg.weight(), seg.unit_dir(), seg.length(), point);
}
//-------------------------------------------------------------------------
template <int DEGREE>
Vector CompactedHomotheticInverse<DEGREE>::EvalGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar epsilon) const
{
    Scalar f_tmp; Vector grad_res;
    EvalValueAndGrad(seg.p1(), seg.weight(),
                     seg.unit_dir(), seg.length(),
                     point, epsilon, f_tmp, grad_res);
    return (seg.density() * this->normalization_factor_1D_)*grad_res;
}
//-------------------------------------------------------------------------
template <int DEGREE>
void CompactedHomotheticInverse<DEGREE>::EvalValueAndGrad(const CsteWeightedSegmentWithDensity& seg, const Point& point, Scalar epsilon_grad,
                      Scalar& value_res, Vector& grad_res) const
{
    EvalValueAndGrad(seg.p1(), seg.weight(),
                     seg.unit_dir(), seg.length(),
                     point, epsilon_grad, value_res, grad_res);
    Scalar factor = seg.density() * this->normalization_factor_1D_;
    value_res *= factor;
    grad_res *= factor;
}
//-------------------------------------------------------------------------



    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

template<int DEGREE>
Scalar CompactedHomotheticInverse<DEGREE>::GetIsoValueForThickness(Scalar length, Scalar thickness) const
{
    std::vector<Scalar> weight;
    weight.push_back(1.0);

    const Scalar a = length*length;
    const Scalar b = a*0.5;
    const Scalar c = thickness*thickness + a*0.25;

    return this->normalization_factor_1D_*length*StandardCauchyAndInverseOptim::StandardCauchyInverse_segment_F<4,0>(a,b,c,weight);
}

} // Close namespace convol

} // Close namespace expressive


