
namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////

template<int DEGREE>
Scalar Cauchy<DEGREE>::Eval(Scalar r) const
{
    Scalar aux = r/this->scale_;
    return pow(1.0+aux*aux, -degree_r()/2.0);
}
template<int DEGREE>
Scalar Cauchy<DEGREE>::EvalGrad(Scalar r, Scalar /*epsilon*/) const
{
    Scalar aux = r/scale_;
    return -degree_r() * r * this->inv_scale_2_ * pow(1.0+aux*aux, -degree_r()/2.0-1.0);
}
template<int DEGREE>
Scalar Cauchy<DEGREE>::InverseFct(Scalar y) const
{
    if(y>0.0)
    {
        return scale_ * sqrt( pow(y, -2.0/degree_r()) - 1.0 ) ;
    }
    else
    {
        return expressive::kScalarMax;
    }
}


//-----------------------------------------------------------------------------
template<int DEGREE>
bool operator==(const Cauchy<DEGREE>& a, const Cauchy<DEGREE>& b)
{
    return (a.scale() == b.scale());
}



} // Close namespace convol

} // Close namespace expressive
