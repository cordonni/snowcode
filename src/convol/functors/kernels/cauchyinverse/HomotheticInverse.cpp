#include <convol/functors/kernels/cauchyinverse/HomotheticInverse.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

static core::FunctorFactory::Type<HomotheticInverse<3> > HomotheticInverse3Type;
static core::FunctorFactory::Type<HomotheticInverse<4> > HomotheticInverse4Type;
static core::FunctorFactory::Type<HomotheticInverse<5> > HomotheticInverse5Type;
static core::FunctorFactory::Type<HomotheticInverse<6> > HomotheticInverse6Type;

} // Close namespace convol

} // Close namespace expressive
