/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
  \file: Inverse.h

  Language: C++

  License: Convol Licence

  \author: Cedric Zanni
  E-Mail: cedric.zanni@inria.fr

  Description: Header file for optimized HornusInverse kernel

  Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_FUNCTOR_KERNEL_HORNUS_INVERSE_H_
#define CONVOL_FUNCTOR_KERNEL_HORNUS_INVERSE_H_

// core dependencies
#include <core/CoreRequired.h>
    // Geometry
    #include<core/geometry/Sphere.h>
    #include<core/geometry/WeightedPoint.h>
    #include<core/geometry/OptWeightedSegment.h>
    #include<core/geometry/WeightedSegmentWithDensity.h> //TODO:@todo : should be changed
    #include<core/geometry/WeightedTriangle.h>

// convol dependencies
    // Kernels
    #include<convol/functors/kernels/cauchyinverse/Inverse.h>
    #include<convol/functors/kernels/cauchyinverse/InverseIsoValueAtDistance.h>

namespace expressive {

namespace convol {

/*! \Brief Base class for all HornusInverse kernel functor
*
*/
template<int DEGREE>
class HornusInverse : public Inverse<DEGREE>
{
public:
    typedef core::Sphere Sphere;
    typedef core::WeightedPoint WeightedPoint;
    typedef core::OptWeightedSegment OptWeightedSegment;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    HornusInverse(Scalar scale = 1.0) : Inverse<DEGREE>(scale) { }
    virtual ~HornusInverse(){}

    /////////////////////////////
    /// Name & Type functions ///
    /////////////////////////////

    static const std::string &StaticName()
    {
        const static std::string name_ = "HornusInverse" + std::to_string(DEGREE);
        return name_;
    }
    virtual std::string Name() const
    {
        return StaticName();
    }

    static core::FunctorTypeTree BuildStaticType()
    {
        std::vector<core::FunctorTypeTree> vec;
        return core::FunctorTypeTree(StaticName(), vec);
    }
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defines functions

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    //-------------------------------------------------------------------------
    // Point evaluation -------------------------------------------------------
    //-------------------------------------------------------------------------
    Scalar Eval(const core::WeightedPoint& p_source, const Point& point) const;
    //-------------------------------------------------------------------------
    inline Scalar operator() (const core::WeightedPoint& p_source, const Point& point) const
    {
        return Eval(p_source, point);
    }
    //-------------------------------------------------------------------------
    Vector EvalGrad(const core::WeightedPoint& p_source, const Point& point, Scalar /*epsilon*/) const;
    //-------------------------------------------------------------------------
    void EvalValueAndGrad(const core::WeightedPoint& p_source, const Point& point, Scalar /*epsilon_grad*/,
                          Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------


    //-------------------------------------------------------------------------
    // Segment evaluation -----------------------------------------------------
    //-------------------------------------------------------------------------
    Scalar Eval(const core::OptWeightedSegment& seg, const Point& point) const;
    //-------------------------------------------------------------------------
    inline Scalar operator() (const core::OptWeightedSegment& seg_source, const Point& point) const
    {
        return Eval(seg_source, point);
    }
    //-------------------------------------------------------------------------
    Vector EvalGrad(const core::OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const;
    //-------------------------------------------------------------------------
    void EvalValueAndGrad(const core::OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                          Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------
    // Special Segment evaluation (for Gradient-based Integral Surfaces) ------
    // WARNING : NOT DEFINED FOR HORNUS INTEGRATION ! -------------------------
    //-------------------------------------------------------------------------
    void EvalHomotheticValues(const core::OptWeightedSegment&, const Point&,
                              Scalar&, Vector&) const  { assert(false); }
    //-------------------------------------------------------------------------
    void EvalHomotheticValues( const Point&, Scalar, const Vector&, Scalar, const Point&,
                               Scalar&, Vector&) const  { assert(false); }
    //-------------------------------------------------------------------------

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    /*! \Brief Given the thickness we want in the middle of a segment for which the length is known,
     *          this function returns the corresponding iso value such that a convolution along a
     *          segment with constant weights equal to 1.0 and of length will lead to a surface with
     *          a maximum thickness equel to thickness
     *
     *   \param length : the length of the segment to be convolued
     *   \param thickness : the thickness we want for our surface
     *	\return The iso value to set to get this thickness
     */
    Scalar GetIsoValueForThickness(Scalar length, Scalar thickness) const;

    /*! \Brief Gives a boundary for field with respect to value.
     *		More precisely, this function returns a distance B such that any
     *		point p such that:
     *		distance(p,primitive) > B => p is outside the surface
     *   \param primitive Primitive creating the scalar field.
     *   \param value
     *	\return Scalar euclidian distance ensuring that any point farther than this distance is outside the surface.
     */
    inline Scalar GetEuclidDistBound(const core::WeightedPoint& point, Scalar value) const
    { 
        return point.weight() * HornusInverse<DEGREE>::InverseFct(value/this->normalization_factor_0D_);
    }
    
    inline Scalar GetEuclidDistBound(const core::OptWeightedSegment& seg, Scalar value) const
    {
        return GetEuclidDistBound1D(seg.weight_min()+seg.unit_delta_weight()*seg.length(), seg.length(), value);
    }

    /*! \Brief TODO:@todo
     */
    inline Scalar GetEuclidDistBound1D(Scalar weight, Scalar value) const
    {
        //TODO:@todo : a implementer correctement
        return GetEuclidDistBound1D(weight, 10000.0, value);
    }

    inline Scalar GetEuclidDistBound1D(Scalar weight, Scalar length, Scalar value) const
    {
        Scalar inv_special_value = this->normalization_factor_1D_ * pow(this->scale_,this->degree_r()) / value;
    
        Scalar pow_val = pow(length*inv_special_value,1.0/this->degree_r());
        const Scalar pow_val_sqr = pow_val*pow_val;
        const Scalar sqrlength = length*length;
        if(pow_val_sqr > sqrlength/2.0)	// H1 combine with H2 in Paul bares proof
        {
            return weight * (sqrt( pow_val_sqr + (sqrlength/4.0)) + sqrt( pow_val_sqr - (sqrlength/4.0)))/2.0;
        }
	else
        {
            return weight * pow_val_sqr;
        }
    }
    
}; // End class HornusInverse declaration

/// Typdef for useable degree
typedef HornusInverse<3> HornusInverse3;
typedef HornusInverse<4> HornusInverse4;
typedef HornusInverse<5> HornusInverse5;
typedef HornusInverse<6> HornusInverse6;

} // Close namespace convol

} // Close namespace expressive

// implementation file
#include <convol/functors/kernels/cauchyinverse/HornusInverse.hpp>


#endif // CONVOL_FUNCTOR_KERNEL_HORNUS_INVERSE_H_

