#include <convol/functors/kernels/cauchyinverse/HornusCauchy.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

static core::FunctorFactory::Type<HornusCauchy<3> > HornusCauchy3Type;
static core::FunctorFactory::Type<HornusCauchy<4> > HornusCauchy4Type;
static core::FunctorFactory::Type<HornusCauchy<5> > HornusCauchy5Type;
static core::FunctorFactory::Type<HornusCauchy<6> > HornusCauchy6Type;

} // Close namespace convol

} // Close namespace expressive
