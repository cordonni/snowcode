/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: HomotheticCauchyAndInverseOptimizedFunctions.h
  
   Language: C++
  
   License: Convol Licence
  
   \author: Cedric Zanni 
   E-Mail: cedric.zanni@inria.fr
  
   Description: Header files defining optimized function for Homothetic CauchyX ans InverseX kernels.
 
   Platform Dependencies: None 
*/  

#pragma once
#ifndef CONVOL_HOMOTHETIC_CAUCHY_AND_INVERSE_OPTIMIZED_FUNCTIONS_H_
#define CONVOL_HOMOTHETIC_CAUCHY_AND_INVERSE_OPTIMIZED_FUNCTIONS_H_

// core dependencies
#include <core/CoreRequired.h>

// std dependencies
#include<vector>
#include<array>

#include<math.h>

namespace expressive {

namespace convol {

namespace HomotheticCauchyAndInverseOptim {

////////////////////////////
/// Function declaration ///
////////////////////////////

template<int I>
Scalar HomotheticCauchyInverse_segment_F (Scalar a, Scalar b, Scalar c, const Scalar w[/*I*/]);
template<int I>
void HomotheticCauchyInverse_segment_GradF (Scalar a, Scalar b, Scalar c, Scalar w[/*I*/], Scalar (&res)[2]);
template<int I>
void HomotheticCauchyInverse_segment_FGradF (Scalar a, Scalar b, Scalar c, const Scalar w[/*I*/], Scalar (&res)[3]);

template<int I>
Scalar HomotheticCauchyInverse_segment_F_on_line (Scalar a, Scalar b, const Scalar w[/*I*/]);
template<int I>
void HomotheticCauchyInverse_segment_GradF_on_line (Scalar a, Scalar b, const Scalar w[/*I*/], Scalar (&res)[2]);
template<int I>
void HomotheticCauchyInverse_segment_FGradF_on_line (Scalar a, Scalar b, const Scalar w[/*I*/], Scalar (&res)[3]);

template<int I>
Scalar HomotheticCauchyInverse_segment_F (Scalar l1, Scalar l2, Scalar a, Scalar b, Scalar c, const Scalar w[/*I*/]);
template<int I>
Scalar HomotheticCauchyInverse_segment_F_on_line (Scalar l1, Scalar l2, Scalar a, Scalar b, const Scalar w[/*I*/]);
template<int I>
void HomotheticCauchyInverse_segment_FgradF (Scalar l1, Scalar l2, Scalar a, Scalar b, Scalar c, const Scalar w[/*I*/], Scalar (&res)[3]);

template<int I>
Scalar HomotheticCauchyInverse_segment_F_cste (Scalar a, Scalar b, Scalar c, Scalar l);
template<int I>
void HomotheticCauchyInverse_segment_FGradF_cste (Scalar a, Scalar b, Scalar c, Scalar l, Scalar (&res)[3]);

template<int I>
Scalar HomotheticCauchyInverse_segment_F_on_line_cste (Scalar a, Scalar b, Scalar l);
template<int I>
void HomotheticCauchyInverse_segment_FGradF_on_line_cste (Scalar a, Scalar b, Scalar l, Scalar (&res)[3]);

template<int I>
void HomotheticCauchyInverse_segment_ScalisF_HornusGradF (Scalar a, Scalar b, Scalar c, 
                                                          Scalar d, Scalar q,
                                                          const Scalar w[/*I*/], Scalar (&res)[3]);
template<int I>
void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_cste (Scalar a, Scalar b, Scalar c, Scalar l, Scalar (&res)[3]);
template<int I>
void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line (Scalar a, Scalar b, 
                                                                  Scalar d, Scalar q,
                                                                  const Scalar w[/*I*/], Scalar (&res)[3]);
template<int I>
void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line_cste (Scalar a, Scalar b, Scalar l, Scalar (&res)[3]);


///////////////////////////////
/// Template specialization ///
///////////////////////////////

    /////////////////////////////////
    // Code for kernel of degree 3 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HomotheticCauchyInverse_segment_F<3> (Scalar a, Scalar b, Scalar c, const Scalar w[3])
    {
        const Scalar t3029 = a - b;
        const Scalar t3023 = a - 0.2e1 * b + c;
        const Scalar t3022 = sqrt(0.1e1 / t3023);
        const Scalar t3024 = sqrt(0.1e1 / c);
        const Scalar t3026 = 0.1e1 / a;
        const Scalar t3025 = a * c;
        const Scalar t7 = b * b;
        const Scalar t3027 = (t3029 * t3022 + b * t3024) / (t3025 - t7);
        const Scalar t3028 = (b * t3027 - t3022 + t3024) * t3026;
        const Scalar t19 = sqrt(a * t3023);
        const Scalar t21 = sqrt(t3025);
        const Scalar t25 = log((t19 + t3029) / (-b + t21));
        const Scalar t26 = sqrt(a);
        return  w[0] * t3027 + w[1] * t3028 + w[2] * (b * t3028 + t25 / t26 - t3022) * t3026;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_GradF<3> (Scalar a, Scalar b, Scalar c, Scalar w[3], Scalar (&res)[2])
    {
        const Scalar t3035 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t3043 = sqrt(t3035);
        const Scalar t3034 = t3043 * t3035;
        const Scalar t4 = b * b;
        const Scalar t3033 = 0.1e1 / (a * c - t4);
        const Scalar t3041 = 0.1e1 / c;
        const Scalar t3044 = sqrt(t3041);
        const Scalar t3036 = t3044 * t3041;
        const Scalar t3037 = a - b;
        const Scalar t3047 = (0.2e1 * a * (t3037 * t3043 + b * t3044) * t3033 + t3037 * t3034 + b * t3036) * t3033 / 0.3e1;
        const Scalar t3031 = b * t3047 - t3034 / 0.3e1 + t3036 / 0.3e1;
        const Scalar t3042 = 0.1e1 / a;
        const Scalar t3048 = t3031 * t3042;
        const Scalar t3030 = b * t3048 + c * t3047 - t3034;
        const Scalar t3049 = t3030 / 0.2e1;
        const Scalar t3040 = w[0];
        const Scalar t3039 = w[1];
        const Scalar t3038 = w[2];
        res[0] = t3040 * t3047 + (t3039 * t3031 + t3038 * t3049) * t3042;
        res[1] = t3040 * t3048 + t3039 * t3042 * t3049 + t3038 * (-t3034 + (-b * t3030 / 0.2e1 + 0.2e1 * c * t3031) * t3042) * t3042;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF<3> (Scalar a, Scalar b, Scalar c, const Scalar w[3], Scalar (&res)[3])
    {
        const Scalar t3062 = a - b;
        const Scalar t3059 = a - 0.2e1 * b + c;
        const Scalar t3058 = 0.1e1 / t3059;
        const Scalar t3069 = sqrt(t3058);
        const Scalar t3056 = t3069 * t3058;
        const Scalar t3063 = a * c;
        const Scalar t2 = b * b;
        const Scalar t3055 = 0.1e1 / (t3063 - t2);
        const Scalar t3067 = 0.1e1 / c;
        const Scalar t3070 = sqrt(t3067);
        const Scalar t3060 = t3070 * t3067;
        const Scalar t3076 = (t3062 * t3069 + b * t3070) * t3055;
        const Scalar t3073 = (0.2e1 * a * t3076 + t3062 * t3056 + b * t3060) * t3055 / 0.3e1;
        const Scalar t3051 = b * t3073 - t3056 / 0.3e1 + t3060 / 0.3e1;
        const Scalar t3068 = 0.1e1 / a;
        const Scalar t3077 = t3051 * t3068;
        const Scalar t3050 = b * t3077 + c * t3073 - t3056;
        const Scalar t3078 = t3050 / 0.2e1;
        const Scalar t3064 = w[2];
        const Scalar t3075 = t3064 * t3068;
        const Scalar t3065 = w[1];
        const Scalar t3074 = t3065 * t3068;
        const Scalar t3066 = w[0];
        const Scalar t3053 = b * t3076 - t3069 + t3070;
        const Scalar t24 = sqrt(a * t3059);
        const Scalar t26 = sqrt(t3063);
        const Scalar t30 = log((t24 + t3062) / (-b + t26));
        const Scalar t31 = sqrt(a);
        res[0] = t3066 * t3076 + t3053 * t3074 + (b * t3053 * t3068 + t30 / t31 - t3069) * t3075;
        res[1] = t3066 * t3073 + (t3065 * t3051 + t3064 * t3078) * t3068;
        res[2] = t3066 * t3077 + t3074 * t3078 + (-t3056 + (-b * t3050 / 0.2e1 + 0.2e1 * c * t3051) * t3068) * t3075;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line<3> (Scalar a, Scalar b, const Scalar w[3])
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3096 = b / t2 / t1;
        const Scalar t3084 = a - b;
        const Scalar t5 = fabs(t3084);
        const Scalar t6 = t5 * t5;
        const Scalar t3081 = 0.1e1 / t6 / t5;
        const Scalar t10 = fabs(t3084 * t3081 + t3096);
        const Scalar t3095 = t10 / 0.2e1;
        const Scalar t3086 = 0.1e1 / a;
        const Scalar t3087 = sqrt(a);
        const Scalar t11 = t3084 * t3084;
        const Scalar t3092 = t11 * t3081;
        const Scalar t3093 = (-t3092 + (t3095 + t3096) * b) * t3087 * t3086;
        const Scalar t25 = log(-t3084 / b);
        const Scalar t26 = fabs(t25);
        return  w[0] * t3087 * t3095 + w[1] * t3093 + w[2] * (b * t3093 + t26 / t3087 - t3087 * t3092) * t3086;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_GradF_on_line<3> (Scalar a, Scalar b, const Scalar w[3], Scalar (&res)[2])
    {
        const Scalar t3110 = 0.1e1 / a;
        const Scalar t3123 = b * t3110;
        const Scalar t3104 = a - b;
        const Scalar t1 = fabs(t3104);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t3101 = 0.1e1 / t3 / t1;
        const Scalar t5 = fabs(b);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t3102 = 0.1e1 / t7 / t5;
        const Scalar t12 = fabs(t3104 * t3101 + b * t3102);
        const Scalar t3122 = t12 / 0.4e1;
        const Scalar t13 = t3104 * t3104;
        const Scalar t3120 = t13 * t3101;
        const Scalar t3111 = sqrt(a);
        const Scalar t3105 = t3111 * a;
        const Scalar t3119 = t3105 * t3120;
        const Scalar t3109 = b * b;
        const Scalar t3108 = w[0];
        const Scalar t3107 = w[1];
        const Scalar t3106 = w[2];
        const Scalar t3099 = (b * t3122 - t3120 / 0.3e1 + t3109 * t3102 / 0.3e1) * t3105;
        const Scalar t3098 = t3099 * t3123 + t3109 * t3111 * t3122 - t3119;
        res[0] = t3108 * t3105 * t3122 + (t3107 * t3099 + t3106 * t3098 / 0.2e1) * t3110;
        const Scalar t31 = a * a;
        res[1] = (t3108 * t3099 + t3106 * (0.2e1 * t3109 * t3099 / t31 - t3119) + (t3107 / 0.2e1 - t3106 * t3123 / 0.2e1) * t3098) * t3110;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_on_line<3> (Scalar a, Scalar b, const Scalar w[3], Scalar (&res)[3])
    {
        const Scalar t3137 = a - b;
        const Scalar t3132 = fabs(t3137);
        const Scalar t1 = t3132 * t3132;
        const Scalar t3161 = 0.1e1 / t1;
        const Scalar t3136 = fabs(b);
        const Scalar t2 = t3136 * t3136;
        const Scalar t3160 = 0.1e1 / t2;
        const Scalar t3130 = 0.1e1 / t3132 * t3161;
        const Scalar t3133 = 0.1e1 / t3136 * t3160;
        const Scalar t8 = fabs(t3137 * t3130 + b * t3133);
        const Scalar t3159 = t8 / 0.2e1;
        const Scalar t3131 = t3161 * t3130;
        const Scalar t3134 = t3160 * t3133;
        const Scalar t12 = fabs(t3137 * t3131 + b * t3134);
        const Scalar t3158 = t12 / 0.4e1;
        const Scalar t3143 = b * b;
        const Scalar t3144 = 0.1e1 / a;
        const Scalar t3145 = sqrt(a);
        const Scalar t3135 = t3137 * t3137;
        const Scalar t3156 = t3135 * t3130;
        const Scalar t3157 = (b * t3159 - t3156 + t3143 * t3133) * t3145 * t3144;
        const Scalar t3155 = t3135 * t3131;
        const Scalar t3140 = w[2];
        const Scalar t3154 = t3140 * t3144;
        const Scalar t3138 = t3145 * a;
        const Scalar t3153 = t3138 * t3155;
        const Scalar t3142 = w[0];
        const Scalar t3141 = w[1];
        const Scalar t3126 = (b * t3158 - t3155 / 0.3e1 + t3143 * t3134 / 0.3e1) * t3138;
        const Scalar t3125 = b * t3126 * t3144 + t3143 * t3145 * t3158 - t3153;
        const Scalar t32 = log(-t3137 / b);
        const Scalar t33 = fabs(t32);
        res[0] = t3142 * t3145 * t3159 + t3141 * t3157 + (b * t3157 + t33 / t3145 - t3145 * t3156) * t3154;
        res[1] = t3142 * t3138 * t3158 + (t3141 * t3126 + t3140 * t3125 / 0.2e1) * t3144;
        const Scalar t48 = a * a;
        res[2] = (t3142 * t3126 + t3140 * (0.2e1 * t3143 * t3126 / t48 - t3153) + (t3141 / 0.2e1 - b * t3154 / 0.2e1) * t3125) * t3144;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_cste<3> (Scalar a, Scalar b, Scalar c, Scalar l)
    {
        const Scalar t3162 = a * l;
        const Scalar t7 = sqrt(0.1e1 / (c + (-0.2e1 * b + t3162) * l));
        const Scalar t10 = sqrt(0.1e1 / c);
        const Scalar t14 = b * b;
        return  ((t3162 - b) * t7 + b * t10) / (a * c - t14);

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_cste<3> (Scalar a, Scalar b, Scalar c, Scalar l, Scalar (&res)[3])
    {
        const Scalar t3177 = a * l;
        const Scalar t2 = b * b;
        const Scalar t3167 = 0.1e1 / (a * c - t2);
        const Scalar t3168 = t3177 - b;
        const Scalar t3166 = 0.1e1 / (c + (-0.2e1 * b + t3177) * l);
        const Scalar t3171 = sqrt(t3166);
        const Scalar t3170 = 0.1e1 / c;
        const Scalar t3172 = sqrt(t3170);
        const Scalar t3176 = (t3168 * t3171 + b * t3172) * t3167;
        const Scalar t3165 = t3171 * t3166;
        const Scalar t3169 = t3172 * t3170;
        const Scalar t3175 = (0.2e1 * a * t3176 + t3168 * t3165 + b * t3169) * t3167 / 0.3e1;
        res[0] = t3176;
        res[1] = t3175;
        res[2] = (b * t3175 - t3165 / 0.3e1 + t3169 / 0.3e1) / a;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line_cste<3> (Scalar a, Scalar b, Scalar l)
    {
        const Scalar t3178 = a * l - b;
        const Scalar t2 = sqrt(a);
        const Scalar t3 = fabs(t3178);
        const Scalar t4 = t3 * t3;
        const Scalar t8 = fabs(b);
        const Scalar t9 = t8 * t8;
        const Scalar t14 = fabs(t3178 / t4 / t3 + b / t9 / t8);
        return  t2 * t14 / 0.2e1;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_on_line_cste<3> (Scalar a, Scalar b, Scalar l, Scalar (&res)[3])
    {
        const Scalar t3188 = a * l - b;
        const Scalar t3187 = fabs(t3188);
        const Scalar t2 = t3187 * t3187;
        const Scalar t3206 = 0.1e1 / t2;
        const Scalar t3190 = fabs(b);
        const Scalar t3 = t3190 * t3190;
        const Scalar t3205 = 0.1e1 / t3;
        const Scalar t3204 = b / t3190 * t3205;
        const Scalar t3203 = 0.1e1 / t3187 * t3206;
        const Scalar t3201 = t3205 * t3204;
        const Scalar t3186 = t3206 * t3203;
        const Scalar t9 = fabs(t3188 * t3186 + t3201);
        const Scalar t3200 = t9 / 0.4e1;
        const Scalar t3192 = sqrt(a);
        const Scalar t3191 = t3192 * a;
        const Scalar t12 = fabs(t3188 * t3203 + t3204);
        res[0] = t3192 * t12 / 0.2e1;
        res[1] = t3191 * t3200;
        const Scalar t14 = t3188 * t3188;
        res[2] = (-t14 * t3186 / 0.3e1 + (t3200 + t3201 / 0.3e1) * b) * t3191 / a;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F<3> (Scalar l1, Scalar l2, Scalar a, Scalar b, Scalar c, const Scalar w[3])
    {
        const Scalar t3221 = -0.2e1 * b;
        const Scalar t3215 = a * l1;
        const Scalar t3214 = a * l2;
        const Scalar t3220 = t3214 - b;
        const Scalar t3219 = t3215 - b;
        const Scalar t3211 = c + (t3221 + t3214) * l2;
        const Scalar t3209 = sqrt(0.1e1 / t3211);
        const Scalar t3212 = c + (t3221 + t3215) * l1;
        const Scalar t3210 = sqrt(0.1e1 / t3212);
        const Scalar t3216 = 0.1e1 / a;
        const Scalar t12 = b * b;
        const Scalar t3217 = (t3220 * t3209 - t3219 * t3210) / (a * c - t12);
        const Scalar t3218 = (b * t3217 - t3209 + t3210) * t3216;
        const Scalar t24 = sqrt(a * t3211);
        const Scalar t27 = sqrt(a * t3212);
        const Scalar t31 = log((t24 + t3220) / (t27 + t3219));
        const Scalar t32 = sqrt(a);
        return  w[0] * t3217 + w[1] * t3218 + w[2] * (b * t3218 + t31 / t32 - l2 * t3209 + l1 * t3210) * t3216;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FgradF<3> (Scalar l1, Scalar l2, Scalar a, Scalar b, Scalar c, const Scalar w[3], Scalar (&res)[3])
    {
        const Scalar t3254 = -0.2e1 * b;
        const Scalar t3243 = l2 * l2;
        const Scalar t3233 = a * t3243 + l2 * t3254 + c;
        const Scalar t3231 = 0.1e1 / t3233;
        const Scalar t3246 = sqrt(t3231);
        const Scalar t3227 = t3246 * t3231;
        const Scalar t3244 = l1 * l1;
        const Scalar t3234 = a * t3244 + l1 * t3254 + c;
        const Scalar t3232 = 0.1e1 / t3234;
        const Scalar t3247 = sqrt(t3232);
        const Scalar t3229 = t3247 * t3232;
        const Scalar t7 = b * b;
        const Scalar t3235 = 0.1e1 / (a * c - t7);
        const Scalar t3236 = a * l2 - b;
        const Scalar t3237 = a * l1 - b;
        const Scalar t3251 = (t3236 * t3246 - t3237 * t3247) * t3235;
        const Scalar t3250 = (0.2e1 * a * t3251 + t3236 * t3227 - t3237 * t3229) * t3235 / 0.3e1;
        const Scalar t3223 = b * t3250 - t3227 / 0.3e1 + t3229 / 0.3e1;
        const Scalar t3245 = 0.1e1 / a;
        const Scalar t3222 = b * t3223 * t3245 + c * t3250 - l2 * t3227 + l1 * t3229;
        const Scalar t3253 = t3222 / 0.2e1;
        const Scalar t3252 = (b * t3251 - t3246 + t3247) * t3245;
        const Scalar t3242 = w[0];
        const Scalar t3241 = w[1];
        const Scalar t3240 = w[2];
        const Scalar t34 = sqrt(a * t3233);
        const Scalar t37 = sqrt(a * t3234);
        const Scalar t41 = log((t34 + t3236) / (t37 + t3237));
        const Scalar t42 = sqrt(a);
        res[0] = t3242 * t3251 + t3241 * t3252 + t3240 * (b * t3252 + t41 / t42 - l2 * t3246 + l1 * t3247) * t3245;
        res[1] = t3242 * t3250 + (t3241 * t3223 + t3240 * t3253) * t3245;
        res[2] = (t3242 * t3223 + t3241 * t3253 + (-t3243 * t3227 + t3244 * t3229 + (-b * t3222 / 0.2e1 + 0.2e1 * c * t3223) * t3245) * t3240) * t3245;

    }
    
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line<3> (Scalar l1, Scalar l2, Scalar a, Scalar b, const Scalar w[3])
    {
        const Scalar t3261 = a * l2 - b;
        const Scalar t2 = fabs(t3261);
        const Scalar t3 = t2 * t2;
        const Scalar t3257 = 0.1e1 / t3 / t2;
        const Scalar t3262 = a * l1 - b;
        const Scalar t6 = fabs(t3262);
        const Scalar t7 = t6 * t6;
        const Scalar t3258 = 0.1e1 / t7 / t6;
        const Scalar t12 = fabs(t3261 * t3257 - t3262 * t3258);
        const Scalar t3275 = t12 / 0.2e1;
        const Scalar t3264 = 0.1e1 / a;
        const Scalar t3265 = sqrt(a);
        const Scalar t13 = t3262 * t3262;
        const Scalar t3270 = t13 * t3258;
        const Scalar t14 = t3261 * t3261;
        const Scalar t3271 = t14 * t3257;
        const Scalar t3272 = (b * t3275 - t3271 + t3270) * t3265 * t3264;
        const Scalar t27 = log(t3261 / t3262);
        const Scalar t28 = fabs(t27);
        return  w[0] * t3265 * t3275 + w[1] * t3272 + w[2] * (b * t3272 + t28 / t3265 - t3265 * (l2 * t3271 - l1 * t3270)) * t3264;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF<3> (Scalar a, Scalar b, Scalar c, 
                                                                          Scalar d, Scalar q,
                                                                          const Scalar w[3], Scalar (&res)[3])
    {
        const Scalar t3292 = a - b;
        const Scalar t3298 = 0.1e1 / a;
        const Scalar t3306 = b * t3298;
        const Scalar t3289 = a - 0.2e1 * b + c;
        const Scalar t3288 = 0.1e1 / t3289;
        const Scalar t3299 = sqrt(t3288);
        const Scalar t3286 = t3299 * t3288;
        const Scalar t3284 = -t3286 / 0.3e1;
        const Scalar t3297 = 0.1e1 / c;
        const Scalar t3300 = sqrt(t3297);
        const Scalar t3290 = t3300 * t3297;
        const Scalar t3293 = a * c;
        const Scalar t3 = b * b;
        const Scalar t3285 = 0.1e1 / (t3293 - t3);
        const Scalar t3304 = (t3292 * t3299 + b * t3300) * t3285;
        const Scalar t3303 = (0.2e1 * a * t3304 + t3292 * t3286 + b * t3290) * t3285 / 0.3e1;
        const Scalar t3280 = b * t3303 + t3284 + t3290 / 0.3e1;
        const Scalar t3278 = t3280 * t3306 + c * t3303 - t3286;
        const Scalar t3305 = t3278 / 0.2e1;
        const Scalar t3296 = w[0];
        const Scalar t3295 = w[1];
        const Scalar t3294 = w[2];
        const Scalar t3282 = b * t3304 - t3299 + t3300;
        const Scalar t21 = sqrt(a * t3289);
        const Scalar t23 = sqrt(t3293);
        const Scalar t27 = log((t21 + t3292) / (-b + t23));
        const Scalar t28 = sqrt(a);
        const Scalar t3279 = t3282 * t3306 + t27 / t28 - t3299;
        const Scalar t3277 = -t3286 + (-b * t3278 / 0.2e1 + 0.2e1 * c * t3280) * t3298;
        const Scalar t3276 = (t3296 * t3280 + t3295 * t3305 + t3294 * t3277) * t3298;
        res[0] = t3296 * t3304 + (t3295 * t3282 + t3294 * t3279) * t3298;
        res[1] = q * (t3296 * t3303 + (t3295 * t3280 + t3294 * t3305) * t3298) + d * t3276;
        res[2] = q * t3276 + (t3296 * t3305 + t3295 * t3277 + (t3284 + (b * t3277 + t3279) * t3298) * t3294) * d * t3298;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line<3> (Scalar a, Scalar b, 
                                                                                  Scalar d, Scalar q,
                                                                                  const Scalar w[3], Scalar (&res)[3])
    {
        const Scalar t3322 = a - b;
        const Scalar t3317 = fabs(t3322);
        const Scalar t1 = t3317 * t3317;
        const Scalar t3347 = 0.1e1 / t1;
        const Scalar t3321 = fabs(b);
        const Scalar t2 = t3321 * t3321;
        const Scalar t3346 = 0.1e1 / t2;
        const Scalar t3315 = 0.1e1 / t3317 * t3347;
        const Scalar t3318 = 0.1e1 / t3321 * t3346;
        const Scalar t3329 = 0.1e1 / a;
        const Scalar t3345 = b * t3329;
        const Scalar t3319 = t3346 * t3318;
        const Scalar t3330 = sqrt(a);
        const Scalar t3323 = t3330 * a;
        const Scalar t3328 = b * b;
        const Scalar t3316 = t3347 * t3315;
        const Scalar t3320 = t3322 * t3322;
        const Scalar t3340 = t3320 * t3316;
        const Scalar t3338 = -t3340 / 0.3e1;
        const Scalar t9 = fabs(t3322 * t3316 + b * t3319);
        const Scalar t3343 = t9 / 0.4e1;
        const Scalar t3311 = (b * t3343 + t3338 + t3328 * t3319 / 0.3e1) * t3323;
        const Scalar t3339 = t3323 * t3340;
        const Scalar t3309 = t3311 * t3345 + t3328 * t3330 * t3343 - t3339;
        const Scalar t3344 = t3309 / 0.2e1;
        const Scalar t3327 = w[0];
        const Scalar t3342 = t3327 / 0.2e1;
        const Scalar t3341 = t3320 * t3315;
        const Scalar t3326 = w[1];
        const Scalar t3325 = w[2];
        const Scalar t3313 = fabs(t3322 * t3315 + b * t3318);
        const Scalar t3312 = (b * t3313 / 0.2e1 - t3341 + t3328 * t3318) * t3330;
        const Scalar t27 = log(-t3322 / b);
        const Scalar t28 = fabs(t27);
        const Scalar t3310 = t3312 * t3345 + t28 / t3330 - t3330 * t3341;
        const Scalar t35 = a * a;
        const Scalar t3308 = -t3309 * t3345 / 0.2e1 + 0.2e1 * t3328 * t3311 / t35 - t3339;
        const Scalar t3307 = (t3327 * t3311 + t3326 * t3344 + t3325 * t3308) * t3329;
        res[0] = t3330 * t3313 * t3342 + (t3326 * t3312 + t3325 * t3310) * t3329;
        res[1] = q * (t3327 * t3323 * t3343 + (t3326 * t3311 + t3325 * t3344) * t3329) + d * t3307;
        res[2] = q * t3307 + (t3309 * t3342 + t3326 * t3308 + (t3323 * t3338 + (b * t3308 + t3310) * t3329) * t3325) * d * t3329;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_cste<3> (Scalar a, Scalar b, Scalar c, Scalar l, Scalar (&res)[3])
    {
        const Scalar t3359 = a * l;
        const Scalar t3351 = c + (-0.2e1 * b + t3359) * l;
        const Scalar t5 = b * b;
        const Scalar t3354 = a * c - t5;
        const Scalar t3352 = sqrt(t3354);
        const Scalar t3353 = 0.1e1 / t3354;
        const Scalar t3355 = t3359 - b;
        const Scalar t6 = sqrt(t3353);
        const Scalar t8 = atan2(t3355, t3352);
        const Scalar t9 = atan2(-b, t3352);
        const Scalar t3358 = (a * t6 * (t8 - t9) + t3355 / t3351 + b / c) * t3353;
        const Scalar t17 = t3351 * t3351;
        const Scalar t3350 = 0.1e1 / t17;
        const Scalar t18 = c * c;
        const Scalar t3356 = 0.1e1 / t18;
        const Scalar t3357 = (0.3e1 / 0.2e1 * a * t3358 + t3355 * t3350 + b * t3356) * t3353 / 0.4e1;
        res[0] = t3358 / 0.2e1;
        res[1] = t3357;
        res[2] = (b * t3357 - t3350 / 0.4e1 + t3356 / 0.4e1) / a;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line_cste<3> (Scalar a, Scalar b, Scalar l, Scalar (&res)[3]) 
    {
        const Scalar t1 = fabs(b);
        const Scalar t3370 = t1 * t1;
        const Scalar t2 = t3370 * t3370;
        const Scalar t3377 = b / t2;
        const Scalar t3363 = a * l - b;
        const Scalar t5 = fabs(t3363);
        const Scalar t3367 = t5 * t5;
        const Scalar t6 = t3367 * t3367;
        const Scalar t3376 = 0.1e1 / t6;
        const Scalar t3374 = 0.1e1 / t3370 * t3377;
        const Scalar t3361 = 0.1e1 / t3367 * t3376;
        const Scalar t11 = fabs(t3363 * t3361 + t3374);
        const Scalar t3373 = t11 / 0.5e1;
        const Scalar t3366 = a * a;
        const Scalar t14 = fabs(t3363 * t3376 + t3377);
        res[0] = a * t14 / 0.3e1;
        res[1] = t3366 * t3373;
        const Scalar t16 = t3363 * t3363;
        res[2] = (-t16 * t3361 / 0.4e1 + (t3373 + t3374 / 0.4e1) * b) * t3366 / a;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 4 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HomotheticCauchyInverse_segment_F<4> (Scalar a, Scalar b, Scalar c, const Scalar w[4])
    {
        const Scalar t2 = b * b;
        const Scalar t3385 = a * c - t2;
        const Scalar t3382 = sqrt(t3385);
        const Scalar t3384 = 0.1e1 / t3385;
        const Scalar t3388 = a - b;
        const Scalar t3 = sqrt(t3384);
        const Scalar t4 = atan2(t3388, t3382);
        const Scalar t5 = atan2(-b, t3382);
        const Scalar t3392 = t3 * (t4 - t5);
        const Scalar t3387 = a - 0.2e1 * b + c;
        const Scalar t3386 = 0.1e1 / t3387;
        const Scalar t3389 = 0.1e1 / c;
        const Scalar t3391 = (a * t3392 + t3388 * t3386 + b * t3389) * t3384 / 0.2e1;
        const Scalar t3390 = 0.1e1 / a;
        const Scalar t3383 = -t3386 / 0.2e1;
        const Scalar t3378 = c * t3391 - t3386;
        const Scalar t25 = log(t3387 * t3389);
        return  w[0] * t3391 + (w[1] * (b * t3391 + t3383 + t3389 / 0.2e1) + w[2] * t3378 + (t3383 + (t25 / 0.2e1 + (t3378 + t3392) * b) * t3390) * w[3]) * t3390;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_GradF<4> (Scalar a, Scalar b, Scalar c, Scalar w[4], Scalar (&res)[2])
    {
        const Scalar t3401 = a - 0.2e1 * b + c;
        const Scalar t2 = t3401 * t3401;
        const Scalar t3400 = 0.1e1 / t2;
        const Scalar t4 = b * b;
        const Scalar t3399 = a * c - t4;
        const Scalar t3397 = sqrt(t3399);
        const Scalar t3398 = 0.1e1 / t3399;
        const Scalar t3402 = a - b;
        const Scalar t5 = c * c;
        const Scalar t3407 = 0.1e1 / t5;
        const Scalar t6 = sqrt(t3398);
        const Scalar t8 = atan2(t3402, t3397);
        const Scalar t9 = atan2(-b, t3397);
        const Scalar t3409 = (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t3402 / t3401 + b / c) * t3398 + t3402 * t3400 + b * t3407) * t3398 / 0.4e1;
        const Scalar t3395 = b * t3409 - t3400 / 0.4e1 + t3407 / 0.4e1;
        const Scalar t3408 = 0.1e1 / a;
        const Scalar t3411 = t3395 * t3408;
        const Scalar t3410 = 0.2e1 * t3411;
        const Scalar t3393 = b * t3410 + c * t3409 - t3400;
        const Scalar t3413 = t3393 / 0.3e1;
        const Scalar t3394 = c * t3410 - t3400;
        const Scalar t3412 = t3394 / 0.2e1;
        const Scalar t3406 = w[0];
        const Scalar t3405 = w[1];
        const Scalar t3404 = w[2];
        const Scalar t3403 = w[3];
        res[0] = t3406 * t3409 + (t3405 * t3395 + t3404 * t3413 + t3403 * t3412) * t3408;
        res[1] = t3406 * t3411 + (t3405 * t3413 + t3404 * t3412 + (-t3400 + (-b * t3394 + c * t3393) * t3408) * t3403) * t3408;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF<4> (Scalar a, Scalar b, Scalar c, const Scalar w[4], Scalar (&res)[3])
    {
        const Scalar t3428 = a - 0.2e1 * b + c;
        const Scalar t2 = t3428 * t3428;
        const Scalar t3427 = 0.1e1 / t2;
        const Scalar t4 = b * b;
        const Scalar t3425 = a * c - t4;
        const Scalar t3424 = 0.1e1 / t3425;
        const Scalar t3429 = a - b;
        const Scalar t5 = c * c;
        const Scalar t3435 = 0.1e1 / t5;
        const Scalar t3426 = 0.1e1 / t3428;
        const Scalar t3434 = 0.1e1 / c;
        const Scalar t3422 = sqrt(t3425);
        const Scalar t6 = sqrt(t3424);
        const Scalar t7 = atan2(t3429, t3422);
        const Scalar t8 = atan2(-b, t3422);
        const Scalar t3443 = t6 * (t7 - t8);
        const Scalar t3444 = (a * t3443 + t3429 * t3426 + b * t3434) * t3424;
        const Scalar t3438 = (0.3e1 / 0.2e1 * a * t3444 + t3429 * t3427 + b * t3435) * t3424 / 0.4e1;
        const Scalar t3416 = b * t3438 - t3427 / 0.4e1 + t3435 / 0.4e1;
        const Scalar t3436 = 0.1e1 / a;
        const Scalar t3445 = t3416 * t3436;
        const Scalar t3439 = 0.2e1 * t3445;
        const Scalar t3414 = b * t3439 + c * t3438 - t3427;
        const Scalar t3447 = t3414 / 0.3e1;
        const Scalar t3415 = c * t3439 - t3427;
        const Scalar t3446 = t3415 / 0.2e1;
        const Scalar t3430 = w[3];
        const Scalar t3442 = t3430 * t3436;
        const Scalar t3431 = w[2];
        const Scalar t3441 = t3431 * t3436;
        const Scalar t3432 = w[1];
        const Scalar t3440 = t3432 * t3436;
        const Scalar t3437 = t3444 / 0.2e1;
        const Scalar t3433 = w[0];
        const Scalar t3423 = -t3426 / 0.2e1;
        const Scalar t3418 = c * t3437 - t3426;
        const Scalar t35 = log(t3428 * t3434);
        res[0] = t3433 * t3437 + (b * t3437 + t3423 + t3434 / 0.2e1) * t3440 + t3418 * t3441 + (t3423 + (t35 / 0.2e1 + (t3418 + t3443) * b) * t3436) * t3442;
        res[1] = t3433 * t3438 + (t3432 * t3416 + t3431 * t3447 + t3430 * t3446) * t3436;
        res[2] = t3433 * t3445 + t3440 * t3447 + t3441 * t3446 + (-t3427 + (-b * t3415 + c * t3414) * t3436) * t3442;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line<4> (Scalar a, Scalar b, const Scalar w[4])
    {
        const Scalar t3455 = a - b;
        const Scalar t1 = fabs(t3455);
        const Scalar t3458 = t1 * t1;
        const Scalar t2 = t3458 * t3458;
        const Scalar t3450 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t3460 = t3 * t3;
        const Scalar t4 = t3460 * t3460;
        const Scalar t3452 = 0.1e1 / t4;
        const Scalar t8 = fabs(t3455 * t3450 + b * t3452);
        const Scalar t3464 = t8 / 0.3e1;
        const Scalar t9 = t3455 * t3455;
        const Scalar t3463 = t9 * t3450;
        const Scalar t3462 = -t3463 / 0.2e1;
        const Scalar t3457 = 0.1e1 / a;
        const Scalar t3456 = b * b;
        const Scalar t3448 = t3456 * t3464 - a * t3463;
        const Scalar t32 = fabs(t3455 / t3458 + b / t3460);
        const Scalar t36 = log(-t3455 / b);
        return  w[0] * a * t3464 + (w[1] * (b * t3464 + t3462 + t3456 * t3452 / 0.2e1) * a + w[2] * t3448 + (a * t3462 + (b * t3448 + b * t32 + t36) * t3457) * w[3]) * t3457;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_GradF_on_line<4> (Scalar a, Scalar b, const Scalar w[4], Scalar (&res)[2])
    {
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t3470 = 0.1e1 / t3 / t2;
        const Scalar t3477 = b * b;
        const Scalar t3478 = a * a;
        const Scalar t3472 = a - b;
        const Scalar t5 = fabs(t3472);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t3469 = 0.1e1 / t7 / t6;
        const Scalar t9 = t3472 * t3472;
        const Scalar t3489 = t9 * t3469;
        const Scalar t13 = fabs(t3472 * t3469 + b * t3470);
        const Scalar t3491 = t13 / 0.5e1;
        const Scalar t3467 = (b * t3491 - t3489 / 0.4e1 + t3477 * t3470 / 0.4e1) * t3478;
        const Scalar t3495 = 0.2e1 * t3467;
        const Scalar t3479 = 0.1e1 / a;
        const Scalar t3493 = b * t3479;
        const Scalar t3487 = t3478 * t3489;
        const Scalar t3465 = t3493 * t3495 + t3477 * a * t3491 - t3487;
        const Scalar t3492 = t3465 / 0.3e1;
        const Scalar t3488 = t3477 / t3478;
        const Scalar t3476 = w[0];
        const Scalar t3475 = w[1];
        const Scalar t3474 = w[2];
        const Scalar t3473 = w[3];
        const Scalar t3466 = t3488 * t3495 - t3487;
        res[0] = t3476 * t3478 * t3491 + (t3475 * t3467 + t3474 * t3492 + t3473 * t3466 / 0.2e1) * t3479;
        res[1] = (t3476 * t3467 + t3475 * t3492 + t3473 * (t3465 * t3488 - t3487) + (t3474 / 0.2e1 - t3473 * t3493) * t3466) * t3479;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_on_line<4> (Scalar a, Scalar b, const Scalar w[4], Scalar (&res)[3])
    {
        const Scalar t3509 = a - b;
        const Scalar t1 = fabs(t3509);
        const Scalar t3518 = t1 * t1;
        const Scalar t3535 = 0.1e1 / t3518;
        const Scalar t2 = t3518 * t3518;
        const Scalar t3502 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t3521 = t3 * t3;
        const Scalar t3534 = 0.1e1 / t3521;
        const Scalar t4 = t3521 * t3521;
        const Scalar t3505 = 0.1e1 / t4;
        const Scalar t3506 = t3534 * t3505;
        const Scalar t3514 = b * b;
        const Scalar t3515 = a * a;
        const Scalar t3503 = t3535 * t3502;
        const Scalar t3507 = t3509 * t3509;
        const Scalar t3528 = t3507 * t3503;
        const Scalar t8 = fabs(t3509 * t3503 + b * t3506);
        const Scalar t3530 = t8 / 0.5e1;
        const Scalar t3498 = (b * t3530 - t3528 / 0.4e1 + t3514 * t3506 / 0.4e1) * t3515;
        const Scalar t3533 = 0.2e1 * t3498;
        const Scalar t3516 = 0.1e1 / a;
        const Scalar t3525 = t3515 * t3528;
        const Scalar t3496 = b * t3516 * t3533 + t3514 * a * t3530 - t3525;
        const Scalar t3532 = t3496 / 0.3e1;
        const Scalar t21 = fabs(t3509 * t3502 + b * t3505);
        const Scalar t3531 = t21 / 0.3e1;
        const Scalar t3529 = t3507 * t3502;
        const Scalar t3510 = w[3];
        const Scalar t3527 = t3510 * t3516;
        const Scalar t3526 = t3514 / t3515;
        const Scalar t3524 = -t3529 / 0.2e1;
        const Scalar t3513 = w[0];
        const Scalar t3512 = w[1];
        const Scalar t3511 = w[2];
        const Scalar t3499 = t3514 * t3531 - a * t3529;
        const Scalar t3497 = t3526 * t3533 - t3525;
        const Scalar t32 = fabs(t3509 * t3535 + b * t3534);
        const Scalar t36 = log(-t3509 / b);
        res[0] = (t3511 * t3499 + (b * t3499 + b * t32 + t36) * t3527) * t3516 + (t3513 * t3531 + t3512 * (b * t3531 + t3524 + t3514 * t3505 / 0.2e1) * t3516 + t3524 * t3527) * a;
        res[1] = t3513 * t3515 * t3530 + (t3512 * t3498 + t3511 * t3532 + t3510 * t3497 / 0.2e1) * t3516;
        res[2] = (t3513 * t3498 + t3512 * t3532 + t3510 * (t3496 * t3526 - t3525) + (t3511 / 0.2e1 - b * t3527) * t3497) * t3516;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_cste<4> (Scalar a, Scalar b, Scalar c, Scalar l)
    {
        const Scalar t3540 = a * l;
        const Scalar t3539 = t3540 - b;
        const Scalar t2 = b * b;
        const Scalar t3538 = a * c - t2;
        const Scalar t3537 = 0.1e1 / t3538;
        const Scalar t3536 = sqrt(t3538);
        const Scalar t3 = sqrt(t3537);
        const Scalar t5 = atan2(t3539, t3536);
        const Scalar t6 = atan2(-b, t3536);
        return  (a * t3 * (t5 - t6) + t3539 / (c + (-0.2e1 * b + t3540) * l) + b / c) * t3537 / 0.2e1;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_cste<4> (Scalar a, Scalar b, Scalar c, Scalar l, Scalar (&res)[3])
    {
        const Scalar t3552 = a * l;
        const Scalar t3544 = c + (-0.2e1 * b + t3552) * l;
        const Scalar t5 = b * b;
        const Scalar t3547 = a * c - t5;
        const Scalar t3545 = sqrt(t3547);
        const Scalar t3546 = 0.1e1 / t3547;
        const Scalar t3548 = t3552 - b;
        const Scalar t6 = sqrt(t3546);
        const Scalar t8 = atan2(t3548, t3545);
        const Scalar t9 = atan2(-b, t3545);
        const Scalar t3551 = (a * t6 * (t8 - t9) + t3548 / t3544 + b / c) * t3546;
        const Scalar t17 = t3544 * t3544;
        const Scalar t3543 = 0.1e1 / t17;
        const Scalar t18 = c * c;
        const Scalar t3549 = 0.1e1 / t18;
        const Scalar t3550 = (0.3e1 / 0.2e1 * a * t3551 + t3548 * t3543 + b * t3549) * t3546 / 0.4e1;
        res[0] = t3551 / 0.2e1;
        res[1] = t3550;
        res[2] = (b * t3550 - t3543 / 0.4e1 + t3549 / 0.4e1) / a;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line_cste<4> (Scalar a, Scalar b, Scalar l)
    {
        const Scalar t3553 = a * l - b;
        const Scalar t2 = fabs(t3553);
        const Scalar t3 = t2 * t2;
        const Scalar t4 = t3 * t3;
        const Scalar t7 = fabs(b);
        const Scalar t8 = t7 * t7;
        const Scalar t9 = t8 * t8;
        const Scalar t13 = fabs(t3553 / t4 + b / t9);
        return  a * t13 / 0.3e1;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_on_line_cste<4> (Scalar a, Scalar b, Scalar l, Scalar (&res)[3])
    {
        const Scalar t1 = fabs(b);
        const Scalar t3570 = t1 * t1;
        const Scalar t2 = t3570 * t3570;
        const Scalar t3577 = b / t2;
        const Scalar t3563 = a * l - b;
        const Scalar t5 = fabs(t3563);
        const Scalar t3567 = t5 * t5;
        const Scalar t6 = t3567 * t3567;
        const Scalar t3576 = 0.1e1 / t6;
        const Scalar t3574 = 0.1e1 / t3570 * t3577;
        const Scalar t3561 = 0.1e1 / t3567 * t3576;
        const Scalar t11 = fabs(t3563 * t3561 + t3574);
        const Scalar t3573 = t11 / 0.5e1;
        const Scalar t3566 = a * a;
        const Scalar t14 = fabs(t3563 * t3576 + t3577);
        res[0] = a * t14 / 0.3e1;
        res[1] = t3566 * t3573;
        const Scalar t16 = t3563 * t3563;
        res[2] = (-t16 * t3561 / 0.4e1 + (t3573 + t3574 / 0.4e1) * b) * t3566 / a;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F<4> (Scalar l1, Scalar l2, Scalar a, Scalar b, Scalar c, const Scalar w[4])
    {
        const Scalar t3597 = -0.2e1 * b;
        const Scalar t3590 = l2 * l2;
        const Scalar t3584 = a * t3590 + l2 * t3597 + c;
        const Scalar t3581 = 0.1e1 / t3584;
        const Scalar t3596 = -t3581 / 0.2e1;
        const Scalar t3591 = l1 * l1;
        const Scalar t3582 = 0.1e1 / (a * t3591 + l1 * t3597 + c);
        const Scalar t3595 = t3582 / 0.2e1;
        const Scalar t9 = b * b;
        const Scalar t3587 = a * c - t9;
        const Scalar t3585 = sqrt(t3587);
        const Scalar t3586 = 0.1e1 / t3587;
        const Scalar t3588 = a * l2 - b;
        const Scalar t3589 = a * l1 - b;
        const Scalar t12 = sqrt(t3586);
        const Scalar t13 = atan2(t3588, t3585);
        const Scalar t14 = atan2(t3589, t3585);
        const Scalar t3594 = t12 * (t13 - t14);
        const Scalar t3593 = (a * t3594 + t3588 * t3581 - t3589 * t3582) * t3586 / 0.2e1;
        const Scalar t3592 = 0.1e1 / a;
        const Scalar t3578 = c * t3593 - l2 * t3581 + l1 * t3582;
        const Scalar t35 = log(t3584 * t3582);
        return  w[0] * t3593 + (w[1] * (b * t3593 + t3596 + t3595) + w[2] * t3578 + (t3590 * t3596 + t3591 * t3595 + (t35 / 0.2e1 + (t3578 + t3594) * b) * t3592) * w[3]) * t3592;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FgradF<4> (Scalar l1, Scalar l2, Scalar a, Scalar b, Scalar c, const Scalar w[4], Scalar (&res)[3])
    {
        const Scalar t3641 = -0.2e1 * b;
        const Scalar t3621 = l2 * l2;
        const Scalar t3610 = a * t3621 + l2 * t3641 + c;
        const Scalar t4 = t3610 * t3610;
        const Scalar t3606 = 0.1e1 / t4;
        const Scalar t3622 = l1 * l1;
        const Scalar t3611 = a * t3622 + l1 * t3641 + c;
        const Scalar t7 = t3611 * t3611;
        const Scalar t3608 = 0.1e1 / t7;
        const Scalar t9 = b * b;
        const Scalar t3614 = a * c - t9;
        const Scalar t3613 = 0.1e1 / t3614;
        const Scalar t3615 = a * l2 - b;
        const Scalar t3616 = a * l1 - b;
        const Scalar t3605 = 0.1e1 / t3610;
        const Scalar t3607 = 0.1e1 / t3611;
        const Scalar t3612 = sqrt(t3614);
        const Scalar t12 = sqrt(t3613);
        const Scalar t13 = atan2(t3615, t3612);
        const Scalar t14 = atan2(t3616, t3612);
        const Scalar t3634 = t12 * (t13 - t14);
        const Scalar t3635 = (a * t3634 + t3615 * t3605 - t3616 * t3607) * t3613;
        const Scalar t3627 = (0.3e1 / 0.2e1 * a * t3635 + t3615 * t3606 - t3616 * t3608) * t3613 / 0.4e1;
        const Scalar t3600 = b * t3627 - t3606 / 0.4e1 + t3608 / 0.4e1;
        const Scalar t3623 = 0.1e1 / a;
        const Scalar t3636 = t3600 * t3623;
        const Scalar t3628 = 0.2e1 * t3636;
        const Scalar t3598 = b * t3628 + c * t3627 - l2 * t3606 + l1 * t3608;
        const Scalar t3640 = t3598 / 0.3e1;
        const Scalar t3629 = t3622 * t3608;
        const Scalar t3630 = t3621 * t3606;
        const Scalar t3599 = c * t3628 - t3630 + t3629;
        const Scalar t3639 = t3599 / 0.2e1;
        const Scalar t3638 = -t3605 / 0.2e1;
        const Scalar t3637 = t3607 / 0.2e1;
        const Scalar t3617 = w[3];
        const Scalar t3633 = t3617 * t3623;
        const Scalar t3618 = w[2];
        const Scalar t3632 = t3618 * t3623;
        const Scalar t3619 = w[1];
        const Scalar t3631 = t3619 * t3623;
        const Scalar t3626 = t3635 / 0.2e1;
        const Scalar t3620 = w[0];
        const Scalar t3602 = c * t3626 - l2 * t3605 + l1 * t3607;
        const Scalar t46 = log(t3610 * t3607);
        res[0] = t3620 * t3626 + (b * t3626 + t3638 + t3637) * t3631 + t3602 * t3632 + (t3621 * t3638 + t3622 * t3637 + (t46 / 0.2e1 + (t3602 + t3634) * b) * t3623) * t3633;
        res[1] = t3620 * t3627 + (t3619 * t3600 + t3618 * t3640 + t3617 * t3639) * t3623;
        res[2] = t3620 * t3636 + t3631 * t3640 + t3632 * t3639 + (-l2 * t3630 + l1 * t3629 + (-b * t3599 + c * t3598) * t3623) * t3633;

    }
    
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line<4> (Scalar l1, Scalar l2, Scalar a, Scalar b, const Scalar w[4])
    {
        const Scalar t3650 = a * l2 - b;
        const Scalar t2 = fabs(t3650);
        const Scalar t3653 = t2 * t2;
        const Scalar t3 = t3653 * t3653;
        const Scalar t3644 = 0.1e1 / t3;
        const Scalar t3651 = a * l1 - b;
        const Scalar t5 = fabs(t3651);
        const Scalar t3655 = t5 * t5;
        const Scalar t6 = t3655 * t3655;
        const Scalar t3645 = 0.1e1 / t6;
        const Scalar t10 = fabs(t3650 * t3644 - t3651 * t3645);
        const Scalar t3659 = t10 / 0.3e1;
        const Scalar t11 = t3650 * t3650;
        const Scalar t3658 = t11 * t3644;
        const Scalar t12 = t3651 * t3651;
        const Scalar t3657 = t12 * t3645;
        const Scalar t3652 = 0.1e1 / a;
        const Scalar t13 = b * b;
        const Scalar t3642 = t13 * t3659 - a * (l2 * t3658 - l1 * t3657);
        const Scalar t31 = l2 * l2;
        const Scalar t33 = l1 * l1;
        const Scalar t40 = log(t3650 / t3651);
        const Scalar t46 = fabs(t3650 / t3653 - t3651 / t3655);
        return  w[0] * a * t3659 + (w[1] * (b * t3659 - t3658 / 0.2e1 + t3657 / 0.2e1) * a + w[2] * t3642 + (-a * (t31 * t3658 - t33 * t3657) / 0.2e1 + (t40 + (t3642 + t46) * b) * t3652) * w[3]) * t3652;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF<4> (Scalar a, Scalar b, Scalar c, 
                                                                          Scalar d, Scalar q,
                                                                          const Scalar w[4], Scalar (&res)[3])
    {
        const Scalar t3678 = a - 0.2e1 * b + c;
        const Scalar t2 = t3678 * t3678;
        const Scalar t3677 = 0.1e1 / t2;
        const Scalar t4 = b * b;
        const Scalar t3675 = a * c - t4;
        const Scalar t3674 = 0.1e1 / t3675;
        const Scalar t3679 = a - b;
        const Scalar t5 = c * c;
        const Scalar t3685 = 0.1e1 / t5;
        const Scalar t3676 = 0.1e1 / t3678;
        const Scalar t3684 = 0.1e1 / c;
        const Scalar t3671 = sqrt(t3675);
        const Scalar t6 = sqrt(t3674);
        const Scalar t7 = atan2(t3679, t3671);
        const Scalar t8 = atan2(-b, t3671);
        const Scalar t3690 = t6 * (t7 - t8);
        const Scalar t3691 = (a * t3690 + t3679 * t3676 + b * t3684) * t3674;
        const Scalar t3688 = (0.3e1 / 0.2e1 * a * t3691 + t3679 * t3677 + b * t3685) * t3674 / 0.4e1;
        const Scalar t3673 = -t3677 / 0.4e1;
        const Scalar t3665 = b * t3688 + t3673 + t3685 / 0.4e1;
        const Scalar t3686 = 0.1e1 / a;
        const Scalar t3689 = 0.2e1 * t3665 * t3686;
        const Scalar t3662 = b * t3689 + c * t3688 - t3677;
        const Scalar t3693 = t3662 / 0.3e1;
        const Scalar t3664 = c * t3689 - t3677;
        const Scalar t3692 = t3664 / 0.2e1;
        const Scalar t3687 = t3691 / 0.2e1;
        const Scalar t3683 = w[0];
        const Scalar t3682 = w[1];
        const Scalar t3681 = w[2];
        const Scalar t3680 = w[3];
        const Scalar t3672 = -t3676 / 0.2e1;
        const Scalar t3667 = c * t3687 - t3676;
        const Scalar t30 = log(t3678 * t3684);
        const Scalar t3663 = t3672 + (t30 / 0.2e1 + (t3667 + t3690) * b) * t3686;
        const Scalar t3661 = -t3677 + (-b * t3664 + c * t3662) * t3686;
        const Scalar t3660 = (t3683 * t3665 + t3682 * t3693 + t3681 * t3692 + t3680 * t3661) * t3686;
        res[0] = t3683 * t3687 + (t3682 * (b * t3687 + t3672 + t3684 / 0.2e1) + t3681 * t3667 + t3680 * t3663) * t3686;
        res[1] = d * t3660 + (t3683 * t3688 + (t3682 * t3665 + t3681 * t3693 + t3680 * t3692) * t3686) * q;
        res[2] = q * t3660 + (t3683 * t3693 + t3682 * t3692 + t3681 * t3661 + (t3673 + (b * t3661 + t3663) * t3686) * t3680) * d * t3686;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line<4> (Scalar a, Scalar b, 
                                                                                  Scalar d, Scalar q,
                                                                                  const Scalar w[4], Scalar (&res)[3])
    {
        const Scalar t3710 = a - b;
        const Scalar t1 = fabs(t3710);
        const Scalar t3719 = t1 * t1;
        const Scalar t3739 = 0.1e1 / t3719;
        const Scalar t2 = t3719 * t3719;
        const Scalar t3703 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t3722 = t3 * t3;
        const Scalar t3738 = 0.1e1 / t3722;
        const Scalar t4 = t3722 * t3722;
        const Scalar t3706 = 0.1e1 / t4;
        const Scalar t3707 = t3738 * t3706;
        const Scalar t3715 = b * b;
        const Scalar t3716 = a * a;
        const Scalar t3704 = t3739 * t3703;
        const Scalar t3708 = t3710 * t3710;
        const Scalar t3729 = t3708 * t3704;
        const Scalar t3725 = -t3729 / 0.4e1;
        const Scalar t9 = fabs(t3710 * t3704 + b * t3707);
        const Scalar t3732 = t9 / 0.5e1;
        const Scalar t3699 = (b * t3732 + t3725 + t3715 * t3707 / 0.4e1) * t3716;
        const Scalar t3737 = 0.2e1 * t3699;
        const Scalar t3717 = 0.1e1 / a;
        const Scalar t3736 = b * t3717;
        const Scalar t3727 = t3716 * t3729;
        const Scalar t3697 = t3736 * t3737 + t3715 * a * t3732 - t3727;
        const Scalar t3735 = t3697 / 0.3e1;
        const Scalar t3728 = t3715 / t3716;
        const Scalar t3698 = t3728 * t3737 - t3727;
        const Scalar t3734 = t3698 / 0.2e1;
        const Scalar t3701 = fabs(t3710 * t3703 + b * t3706);
        const Scalar t3733 = t3701 / 0.3e1;
        const Scalar t3714 = w[0];
        const Scalar t3731 = t3714 / 0.3e1;
        const Scalar t3730 = t3708 * t3703;
        const Scalar t3726 = -t3730 / 0.2e1;
        const Scalar t3713 = w[1];
        const Scalar t3712 = w[2];
        const Scalar t3711 = w[3];
        const Scalar t3700 = t3715 * t3733 - a * t3730;
        const Scalar t30 = fabs(t3710 * t3739 + b * t3738);
        const Scalar t34 = log(-t3710 / b);
        const Scalar t3696 = a * t3726 + (b * t3700 + b * t30 + t34) * t3717;
        const Scalar t3695 = -t3698 * t3736 + t3697 * t3728 - t3727;
        const Scalar t3694 = (t3714 * t3699 + t3713 * t3735 + t3712 * t3734 + t3711 * t3695) * t3717;
        res[0] = a * t3701 * t3731 + (t3713 * (b * t3733 + t3726 + t3715 * t3706 / 0.2e1) * a + t3712 * t3700 + t3711 * t3696) * t3717;
        res[1] = d * t3694 + (t3714 * t3716 * t3732 + (t3713 * t3699 + t3712 * t3735 + t3711 * t3734) * t3717) * q;
        res[2] = q * t3694 + (t3697 * t3731 + t3713 * t3734 + t3712 * t3695 + (t3716 * t3725 + (b * t3695 + t3696) * t3717) * t3711) * d * t3717;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_cste<4> (Scalar a, Scalar b, Scalar c, Scalar l, Scalar (&res)[3])
    {
        const Scalar t3756 = a * l;
        const Scalar t2 = b * b;
        const Scalar t3744 = 0.1e1 / (a * c - t2);
        const Scalar t3745 = t3756 - b;
        const Scalar t3743 = 0.1e1 / (c + (-0.2e1 * b + t3756) * l);
        const Scalar t3748 = sqrt(t3743);
        const Scalar t3747 = 0.1e1 / c;
        const Scalar t3749 = sqrt(t3747);
        const Scalar t3750 = t3748 * t3743;
        const Scalar t3752 = t3749 * t3747;
        const Scalar t3755 = (0.2e1 * a * (t3745 * t3748 + b * t3749) * t3744 + t3745 * t3750 + b * t3752) * t3744;
        const Scalar t3742 = t3743 * t3750;
        const Scalar t3746 = t3747 * t3752;
        const Scalar t3754 = (0.4e1 / 0.3e1 * a * t3755 + t3745 * t3742 + b * t3746) * t3744 / 0.5e1;
        res[0] = t3755 / 0.3e1;
        res[1] = t3754;
        res[2] = (b * t3754 - t3742 / 0.5e1 + t3746 / 0.5e1) / a;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line_cste<4> (Scalar a, Scalar b, Scalar l, Scalar (&res)[3]) 
    {
        const Scalar t3762 = fabs(b);
        const Scalar t3769 = t3762 * t3762;
        const Scalar t3 = t3769 * t3769;
        const Scalar t3779 = b / t3762 / t3;
        const Scalar t3760 = a * l - b;
        const Scalar t3759 = fabs(t3760);
        const Scalar t3765 = t3759 * t3759;
        const Scalar t7 = t3765 * t3765;
        const Scalar t3778 = 0.1e1 / t3759 / t7;
        const Scalar t3776 = 0.1e1 / t3769 * t3779;
        const Scalar t3758 = 0.1e1 / t3765 * t3778;
        const Scalar t13 = fabs(t3760 * t3758 + t3776);
        const Scalar t3775 = t13 / 0.6e1;
        const Scalar t14 = sqrt(a);
        const Scalar t3773 = t14 * a;
        const Scalar t3763 = a * t3773;
        const Scalar t17 = fabs(t3760 * t3778 + t3779);
        res[0] = t3773 * t17 / 0.4e1;
        res[1] = t3763 * t3775;
        const Scalar t19 = t3760 * t3760;
        res[2] = (-t19 * t3758 / 0.5e1 + (t3775 + t3776 / 0.5e1) * b) * t3763 / a;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 5 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HomotheticCauchyInverse_segment_F<5> (Scalar a, Scalar b, Scalar c, const Scalar w[5])
    {
        const Scalar t3793 = a - b;
        const Scalar t3790 = a - 0.2e1 * b + c;
        const Scalar t3789 = 0.1e1 / t3790;
        const Scalar t3797 = sqrt(t3789);
        const Scalar t3787 = t3797 * t3789;
        const Scalar t3785 = -t3787 / 0.3e1;
        const Scalar t3795 = 0.1e1 / c;
        const Scalar t3798 = sqrt(t3795);
        const Scalar t3791 = t3798 * t3795;
        const Scalar t3794 = a * c;
        const Scalar t3 = b * b;
        const Scalar t3786 = 0.1e1 / (t3794 - t3);
        const Scalar t3802 = (t3793 * t3797 + b * t3798) * t3786;
        const Scalar t3801 = (0.2e1 * a * t3802 + t3793 * t3787 + b * t3791) * t3786 / 0.3e1;
        const Scalar t3782 = b * t3801 + t3785 + t3791 / 0.3e1;
        const Scalar t3796 = 0.1e1 / a;
        const Scalar t3803 = t3782 * t3796;
        const Scalar t3781 = b * t3803 + c * t3801 - t3787;
        const Scalar t3780 = -t3787 + (-b * t3781 / 0.2e1 + 0.2e1 * c * t3782) * t3796;
        const Scalar t38 = sqrt(a * t3790);
        const Scalar t40 = sqrt(t3794);
        const Scalar t44 = log((t38 + t3793) / (-b + t40));
        const Scalar t45 = sqrt(a);
        return  w[0] * t3801 + w[1] * t3803 + w[2] * t3781 * t3796 / 0.2e1 + w[3] * t3780 * t3796 + w[4] * (t3785 + (b * t3780 + t44 / t45 - t3797 + b * (b * t3802 - t3797 + t3798) * t3796) * t3796) * t3796;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_GradF<5> (Scalar a, Scalar b, Scalar c, Scalar w[5], Scalar (&res)[2])
    {
        const Scalar t2 = b * b;
        const Scalar t3809 = 0.1e1 / (a * c - t2);
        const Scalar t3832 = a * t3809;
        const Scalar t3811 = 0.1e1 / (a - 0.2e1 * b + c);
        const Scalar t3821 = sqrt(t3811);
        const Scalar t3823 = t3821 * t3811;
        const Scalar t3810 = t3811 * t3823;
        const Scalar t3819 = 0.1e1 / c;
        const Scalar t3822 = sqrt(t3819);
        const Scalar t3825 = t3822 * t3819;
        const Scalar t3812 = t3819 * t3825;
        const Scalar t3813 = a - b;
        const Scalar t3827 = (0.4e1 / 0.3e1 * (0.2e1 * (t3813 * t3821 + b * t3822) * t3832 + t3813 * t3823 + b * t3825) * t3832 + t3813 * t3810 + b * t3812) * t3809 / 0.5e1;
        const Scalar t3807 = b * t3827 - t3810 / 0.5e1 + t3812 / 0.5e1;
        const Scalar t3820 = 0.1e1 / a;
        const Scalar t3828 = t3807 * t3820;
        const Scalar t3806 = 0.3e1 * b * t3828 + c * t3827 - t3810;
        const Scalar t3829 = t3806 / 0.4e1;
        const Scalar t3805 = -t3810 + (b * t3829 + 0.2e1 * c * t3807) * t3820;
        const Scalar t3804 = -t3810 + (-b * t3805 / 0.3e1 + 0.3e1 / 0.4e1 * c * t3806) * t3820;
        const Scalar t3831 = t3804 / 0.2e1;
        const Scalar t3830 = t3805 / 0.3e1;
        const Scalar t3818 = w[0];
        const Scalar t3817 = w[1];
        const Scalar t3816 = w[2];
        const Scalar t3815 = w[3];
        const Scalar t3814 = w[4];
        res[0] = t3818 * t3827 + (t3817 * t3807 + t3816 * t3829 + t3815 * t3830 + t3814 * t3831) * t3820;
        res[1] = t3818 * t3828 + (t3817 * t3829 + t3816 * t3830 + t3815 * t3831 + (-t3810 + (-0.3e1 / 0.2e1 * b * t3804 + 0.4e1 / 0.3e1 * c * t3805) * t3820) * t3814) * t3820;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF<5> (Scalar a, Scalar b, Scalar c, const Scalar w[5], Scalar (&res)[3])
    {
        const Scalar t3881 = 0.2e1 * c;
        const Scalar t3853 = a - b;
        const Scalar t3861 = 0.1e1 / a;
        const Scalar t3880 = b * t3861;
        const Scalar t3849 = a - 0.2e1 * b + c;
        const Scalar t3848 = 0.1e1 / t3849;
        const Scalar t3862 = sqrt(t3848);
        const Scalar t3864 = t3862 * t3848;
        const Scalar t3845 = t3848 * t3864;
        const Scalar t3860 = 0.1e1 / c;
        const Scalar t3863 = sqrt(t3860);
        const Scalar t3866 = t3863 * t3860;
        const Scalar t3850 = t3860 * t3866;
        const Scalar t3854 = a * c;
        const Scalar t2 = b * b;
        const Scalar t3844 = 0.1e1 / (t3854 - t2);
        const Scalar t3874 = (t3853 * t3862 + b * t3863) * t3844;
        const Scalar t3875 = (0.2e1 * a * t3874 + t3853 * t3864 + b * t3866) * t3844;
        const Scalar t3869 = (0.4e1 / 0.3e1 * a * t3875 + t3853 * t3845 + b * t3850) * t3844 / 0.5e1;
        const Scalar t3838 = b * t3869 - t3845 / 0.5e1 + t3850 / 0.5e1;
        const Scalar t3876 = t3838 * t3861;
        const Scalar t3836 = 0.3e1 * b * t3876 + c * t3869 - t3845;
        const Scalar t3877 = t3836 / 0.4e1;
        const Scalar t3834 = -t3845 + (b * t3877 + t3838 * t3881) * t3861;
        const Scalar t3833 = -t3845 + (-b * t3834 / 0.3e1 + 0.3e1 / 0.4e1 * c * t3836) * t3861;
        const Scalar t3879 = t3833 / 0.2e1;
        const Scalar t3878 = t3834 / 0.3e1;
        const Scalar t3855 = w[4];
        const Scalar t3873 = t3855 * t3861;
        const Scalar t3856 = w[3];
        const Scalar t3872 = t3856 * t3861;
        const Scalar t3857 = w[2];
        const Scalar t3871 = t3857 * t3861;
        const Scalar t3858 = w[1];
        const Scalar t3870 = t3858 * t3861;
        const Scalar t3868 = t3875 / 0.3e1;
        const Scalar t3859 = w[0];
        const Scalar t3843 = -t3864 / 0.3e1;
        const Scalar t3840 = b * t3868 + t3843 + t3866 / 0.3e1;
        const Scalar t3837 = t3840 * t3880 + c * t3868 - t3864;
        const Scalar t3835 = -t3864 + (-b * t3837 / 0.2e1 + t3840 * t3881) * t3861;
        const Scalar t51 = sqrt(a * t3849);
        const Scalar t53 = sqrt(t3854);
        const Scalar t57 = log((t51 + t3853) / (-b + t53));
        const Scalar t58 = sqrt(a);
        res[0] = t3859 * t3868 + t3840 * t3870 + t3837 * t3871 / 0.2e1 + t3835 * t3872 + (t3843 + (b * t3835 + t57 / t58 - t3862 + (b * t3874 - t3862 + t3863) * t3880) * t3861) * t3873;
        res[1] = t3859 * t3869 + (t3858 * t3838 + t3857 * t3877 + t3856 * t3878 + t3855 * t3879) * t3861;
        res[2] = t3859 * t3876 + t3870 * t3877 + t3871 * t3878 + t3872 * t3879 + (-t3845 + (-0.3e1 / 0.2e1 * b * t3833 + 0.4e1 / 0.3e1 * c * t3834) * t3861) * t3873;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line<5> (Scalar a, Scalar b, const Scalar w[5])
    {
        const Scalar t3893 = a - b;
        const Scalar t3888 = fabs(t3893);
        const Scalar t1 = t3888 * t3888;
        const Scalar t3915 = 0.1e1 / t1;
        const Scalar t3892 = fabs(b);
        const Scalar t2 = t3892 * t3892;
        const Scalar t3914 = 0.1e1 / t2;
        const Scalar t3886 = 0.1e1 / t3888 * t3915;
        const Scalar t3889 = 0.1e1 / t3892 * t3914;
        const Scalar t3887 = t3915 * t3886;
        const Scalar t3890 = t3914 * t3889;
        const Scalar t8 = fabs(t3893 * t3887 + b * t3890);
        const Scalar t3913 = t8 / 0.4e1;
        const Scalar t3898 = sqrt(a);
        const Scalar t3894 = t3898 * a;
        const Scalar t3896 = b * b;
        const Scalar t3891 = t3893 * t3893;
        const Scalar t3908 = t3891 * t3887;
        const Scalar t3906 = -t3908 / 0.3e1;
        const Scalar t3884 = (b * t3913 + t3906 + t3896 * t3890 / 0.3e1) * t3894;
        const Scalar t3897 = 0.1e1 / a;
        const Scalar t3907 = t3894 * t3908;
        const Scalar t3910 = t3884 * t3897;
        const Scalar t3911 = (b * t3910 + t3896 * t3898 * t3913 - t3907) * t3897;
        const Scalar t21 = a * a;
        const Scalar t3912 = (-b * t3911 / 0.2e1 + 0.2e1 * t3896 * t3884 / t21 - t3907) * t3897;
        const Scalar t3909 = t3891 * t3886;
        const Scalar t41 = fabs(t3893 * t3886 + b * t3889);
        const Scalar t51 = log(-t3893 / b);
        const Scalar t52 = fabs(t51);
        return  w[0] * t3894 * t3913 + w[1] * t3910 + w[2] * t3911 / 0.2e1 + w[3] * t3912 + w[4] * (b * t3912 + (b * (b * t41 / 0.2e1 - t3909 + t3896 * t3889) * t3898 * t3897 + t52 / t3898 - t3898 * t3909) * t3897 + t3894 * t3906) * t3897;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_GradF_on_line<5> (Scalar a, Scalar b, const Scalar w[5], Scalar (&res)[2])
    {
        const Scalar t3932 = 0.1e1 / a;
        const Scalar t3952 = b * t3932;
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t4 = t2 * t2;
        const Scalar t3922 = 0.1e1 / t4 / t2 / t1;
        const Scalar t6 = sqrt(a);
        const Scalar t3935 = t6 * a;
        const Scalar t3925 = a * t3935;
        const Scalar t3931 = b * b;
        const Scalar t3924 = a - b;
        const Scalar t7 = fabs(t3924);
        const Scalar t8 = t7 * t7;
        const Scalar t10 = t8 * t8;
        const Scalar t3921 = 0.1e1 / t10 / t8 / t7;
        const Scalar t12 = t3924 * t3924;
        const Scalar t3947 = t12 * t3921;
        const Scalar t16 = fabs(t3924 * t3921 + b * t3922);
        const Scalar t3949 = t16 / 0.6e1;
        const Scalar t3919 = (b * t3949 - t3947 / 0.5e1 + t3931 * t3922 / 0.5e1) * t3925;
        const Scalar t3945 = t3925 * t3947;
        const Scalar t22 = a * a;
        const Scalar t3946 = t3931 / t22;
        const Scalar t3918 = 0.3e1 * t3919 * t3952 + t3931 * t3935 * t3949 - t3945;
        const Scalar t3950 = t3918 / 0.4e1;
        const Scalar t3917 = t3950 * t3952 + 0.2e1 * t3919 * t3946 - t3945;
        const Scalar t3951 = t3917 / 0.3e1;
        const Scalar t3930 = w[0];
        const Scalar t3929 = w[1];
        const Scalar t3928 = w[2];
        const Scalar t3927 = w[3];
        const Scalar t3926 = w[4];
        const Scalar t3916 = -t3917 * t3952 / 0.3e1 + 0.3e1 / 0.4e1 * t3918 * t3946 - t3945;
        res[0] = t3930 * t3925 * t3949 + (t3929 * t3919 + t3928 * t3950 + t3927 * t3951 + t3926 * t3916 / 0.2e1) * t3932;
        res[1] = (t3930 * t3919 + t3929 * t3950 + t3928 * t3951 + t3926 * (0.4e1 / 0.3e1 * t3917 * t3946 - t3945) + (t3927 / 0.2e1 - 0.3e1 / 0.2e1 * t3926 * t3952) * t3916) * t3932;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_on_line<5> (Scalar a, Scalar b, const Scalar w[5], Scalar (&res)[3])
    {
        const Scalar t3972 = a - b;
        const Scalar t3966 = fabs(t3972);
        const Scalar t1 = t3966 * t3966;
        const Scalar t4013 = 0.1e1 / t1;
        const Scalar t3971 = fabs(b);
        const Scalar t2 = t3971 * t3971;
        const Scalar t4012 = 0.1e1 / t2;
        const Scalar t3963 = 0.1e1 / t3966 * t4013;
        const Scalar t3967 = 0.1e1 / t3971 * t4012;
        const Scalar t3964 = t4013 * t3963;
        const Scalar t3968 = t4012 * t3967;
        const Scalar t3982 = 0.1e1 / a;
        const Scalar t4011 = b * t3982;
        const Scalar t3969 = t4012 * t3968;
        const Scalar t3984 = sqrt(a);
        const Scalar t3993 = t3984 * a;
        const Scalar t3973 = a * t3993;
        const Scalar t3981 = b * b;
        const Scalar t3965 = t4013 * t3964;
        const Scalar t3970 = t3972 * t3972;
        const Scalar t4001 = t3970 * t3965;
        const Scalar t8 = fabs(t3972 * t3965 + b * t3969);
        const Scalar t4007 = t8 / 0.6e1;
        const Scalar t3959 = (b * t4007 - t4001 / 0.5e1 + t3981 * t3969 / 0.5e1) * t3973;
        const Scalar t3997 = t3973 * t4001;
        const Scalar t14 = a * a;
        const Scalar t3999 = t3981 / t14;
        const Scalar t3998 = 0.2e1 * t3999;
        const Scalar t3957 = 0.3e1 * t3959 * t4011 + t3981 * t3993 * t4007 - t3997;
        const Scalar t4009 = t3957 / 0.4e1;
        const Scalar t3955 = t4009 * t4011 + t3959 * t3998 - t3997;
        const Scalar t4010 = t3955 / 0.3e1;
        const Scalar t25 = fabs(t3972 * t3964 + b * t3968);
        const Scalar t4008 = t25 / 0.4e1;
        const Scalar t4002 = t3970 * t3964;
        const Scalar t3995 = -t4002 / 0.3e1;
        const Scalar t3960 = (b * t4008 + t3995 + t3981 * t3968 / 0.3e1) * t3993;
        const Scalar t3996 = t3993 * t4002;
        const Scalar t4004 = t3960 * t3982;
        const Scalar t4005 = (b * t4004 + t3981 * t3984 * t4008 - t3996) * t3982;
        const Scalar t4006 = (-b * t4005 / 0.2e1 + t3960 * t3998 - t3996) * t3982;
        const Scalar t4003 = t3970 * t3963;
        const Scalar t3976 = w[4];
        const Scalar t4000 = t3976 * t3982;
        const Scalar t3980 = w[0];
        const Scalar t3979 = w[1];
        const Scalar t3978 = w[2];
        const Scalar t3977 = w[3];
        const Scalar t3954 = -t3955 * t4011 / 0.3e1 + 0.3e1 / 0.4e1 * t3957 * t3999 - t3997;
        const Scalar t53 = fabs(t3972 * t3963 + b * t3967);
        const Scalar t62 = log(-t3972 / b);
        const Scalar t63 = fabs(t62);
        res[0] = t3980 * t3993 * t4008 + t3979 * t4004 + t3978 * t4005 / 0.2e1 + t3977 * t4006 + (b * t4006 + ((b * t53 / 0.2e1 - t4003 + t3981 * t3967) * t3984 * t4011 + t63 / t3984 - t3984 * t4003) * t3982 + t3993 * t3995) * t4000;
        res[1] = t3980 * t3973 * t4007 + (t3979 * t3959 + t3978 * t4009 + t3977 * t4010 + t3976 * t3954 / 0.2e1) * t3982;
        res[2] = (t3980 * t3959 + t3979 * t4009 + t3978 * t4010 + t3976 * (0.4e1 / 0.3e1 * t3955 * t3999 - t3997) + (t3977 / 0.2e1 - 0.3e1 / 0.2e1 * b * t4000) * t3954) * t3982;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_cste<5> (Scalar a, Scalar b, Scalar c, Scalar l)
    {
        const Scalar t4024 = a * l;
        const Scalar t4017 = 0.1e1 / c;
        const Scalar t1 = sqrt(t4017);
        const Scalar t4023 = b * t1;
        const Scalar t4014 = 0.1e1 / (c + (-0.2e1 * b + t4024) * l);
        const Scalar t7 = sqrt(t4014);
        const Scalar t4022 = (t4024 - b) * t7;
        const Scalar t9 = b * b;
        const Scalar t4015 = 0.1e1 / (a * c - t9);
        return  (0.2e1 * a * (t4022 + t4023) * t4015 + t4014 * t4022 + t4017 * t4023) * t4015 / 0.3e1;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_cste<5> (Scalar a, Scalar b, Scalar c, Scalar l, Scalar (&res)[3])
    {
        const Scalar t4041 = a * l;
        const Scalar t2 = b * b;
        const Scalar t4029 = 0.1e1 / (a * c - t2);
        const Scalar t4030 = t4041 - b;
        const Scalar t4028 = 0.1e1 / (c + (-0.2e1 * b + t4041) * l);
        const Scalar t4033 = sqrt(t4028);
        const Scalar t4032 = 0.1e1 / c;
        const Scalar t4034 = sqrt(t4032);
        const Scalar t4035 = t4033 * t4028;
        const Scalar t4037 = t4034 * t4032;
        const Scalar t4040 = (0.2e1 * a * (t4030 * t4033 + b * t4034) * t4029 + t4030 * t4035 + b * t4037) * t4029;
        const Scalar t4027 = t4028 * t4035;
        const Scalar t4031 = t4032 * t4037;
        const Scalar t4039 = (0.4e1 / 0.3e1 * a * t4040 + t4030 * t4027 + b * t4031) * t4029 / 0.5e1;
        res[0] = t4040 / 0.3e1;
        res[1] = t4039;
        res[2] = (b * t4039 - t4027 / 0.5e1 + t4031 / 0.5e1) / a;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line_cste<5> (Scalar a, Scalar b, Scalar l)
    {
        const Scalar t4042 = a * l - b;
        const Scalar t2 = sqrt(a);
        const Scalar t4 = fabs(t4042);
        const Scalar t5 = t4 * t4;
        const Scalar t6 = t5 * t5;
        const Scalar t10 = fabs(b);
        const Scalar t11 = t10 * t10;
        const Scalar t12 = t11 * t11;
        const Scalar t17 = fabs(t4042 / t6 / t4 + b / t12 / t10);
        return  t2 * a * t17 / 0.4e1;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_on_line_cste<5> (Scalar a, Scalar b, Scalar l, Scalar (&res)[3])
    {
        const Scalar t4056 = fabs(b);
        const Scalar t4063 = t4056 * t4056;
        const Scalar t3 = t4063 * t4063;
        const Scalar t4073 = b / t4056 / t3;
        const Scalar t4054 = a * l - b;
        const Scalar t4053 = fabs(t4054);
        const Scalar t4059 = t4053 * t4053;
        const Scalar t7 = t4059 * t4059;
        const Scalar t4072 = 0.1e1 / t4053 / t7;
        const Scalar t4070 = 0.1e1 / t4063 * t4073;
        const Scalar t4052 = 0.1e1 / t4059 * t4072;
        const Scalar t13 = fabs(t4054 * t4052 + t4070);
        const Scalar t4069 = t13 / 0.6e1;
        const Scalar t14 = sqrt(a);
        const Scalar t4067 = t14 * a;
        const Scalar t4057 = a * t4067;
        const Scalar t17 = fabs(t4054 * t4072 + t4073);
        res[0] = t4067 * t17 / 0.4e1;
        res[1] = t4057 * t4069;
        const Scalar t19 = t4054 * t4054;
        res[2] = (-t19 * t4052 / 0.5e1 + (t4069 + t4070 / 0.5e1) * b) * t4057 / a;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F<5> (Scalar l1, Scalar l2, Scalar a, Scalar b, Scalar c, const Scalar w[5])
    {
        const Scalar t4106 = -0.2e1 * b;
        const Scalar t4088 = a * l2 - b;
        const Scalar t4089 = a * l1 - b;
        const Scalar t4092 = l2 * l2;
        const Scalar t4085 = a * t4092 + l2 * t4106 + c;
        const Scalar t4083 = 0.1e1 / t4085;
        const Scalar t4095 = sqrt(t4083);
        const Scalar t4079 = t4095 * t4083;
        const Scalar t4093 = l1 * l1;
        const Scalar t4086 = a * t4093 + l1 * t4106 + c;
        const Scalar t4084 = 0.1e1 / t4086;
        const Scalar t4096 = sqrt(t4084);
        const Scalar t4081 = t4096 * t4084;
        const Scalar t9 = b * b;
        const Scalar t4087 = 0.1e1 / (a * c - t9);
        const Scalar t4104 = (t4088 * t4095 - t4089 * t4096) * t4087;
        const Scalar t4101 = (0.2e1 * a * t4104 + t4088 * t4079 - t4089 * t4081) * t4087 / 0.3e1;
        const Scalar t4076 = b * t4101 - t4079 / 0.3e1 + t4081 / 0.3e1;
        const Scalar t4094 = 0.1e1 / a;
        const Scalar t4105 = t4076 * t4094;
        const Scalar t4103 = t4092 * t4079;
        const Scalar t4102 = t4093 * t4081;
        const Scalar t4075 = b * t4105 + c * t4101 - l2 * t4079 + l1 * t4081;
        const Scalar t4074 = -t4103 + t4102 + (-b * t4075 / 0.2e1 + 0.2e1 * c * t4076) * t4094;
        const Scalar t50 = sqrt(a * t4085);
        const Scalar t53 = sqrt(a * t4086);
        const Scalar t57 = log((t50 + t4088) / (t53 + t4089));
        const Scalar t58 = sqrt(a);
        return  w[0] * t4101 + w[1] * t4105 + w[2] * t4075 * t4094 / 0.2e1 + w[3] * t4074 * t4094 + w[4] * (l1 * t4102 / 0.3e1 - l2 * t4103 / 0.3e1 + (t57 / t58 - l2 * t4095 + l1 * t4096 + b * t4074 + b * (b * t4104 - t4095 + t4096) * t4094) * t4094) * t4094;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FgradF<5> (Scalar l1, Scalar l2, Scalar a, Scalar b, Scalar c, const Scalar w[5], Scalar (&res)[3])
    {
        const Scalar t4168 = -0.2e1 * b;
        const Scalar t4167 = 0.2e1 * c;
        const Scalar t4141 = 0.1e1 / a;
        const Scalar t4166 = b * t4141;
        const Scalar t4138 = l2 * l2;
        const Scalar t4125 = a * t4138 + l2 * t4168 + c;
        const Scalar t4123 = 0.1e1 / t4125;
        const Scalar t4142 = sqrt(t4123);
        const Scalar t4148 = t4142 * t4123;
        const Scalar t4117 = t4123 * t4148;
        const Scalar t4140 = l1 * l1;
        const Scalar t4126 = a * t4140 + l1 * t4168 + c;
        const Scalar t4124 = 0.1e1 / t4126;
        const Scalar t4143 = sqrt(t4124);
        const Scalar t4150 = t4143 * t4124;
        const Scalar t4120 = t4124 * t4150;
        const Scalar t7 = b * b;
        const Scalar t4127 = 0.1e1 / (a * c - t7);
        const Scalar t4128 = a * l2 - b;
        const Scalar t4129 = a * l1 - b;
        const Scalar t4158 = (t4128 * t4142 - t4129 * t4143) * t4127;
        const Scalar t4159 = (0.2e1 * a * t4158 + t4128 * t4148 - t4129 * t4150) * t4127;
        const Scalar t4153 = (0.4e1 / 0.3e1 * a * t4159 + t4128 * t4117 - t4129 * t4120) * t4127 / 0.5e1;
        const Scalar t4112 = b * t4153 - t4117 / 0.5e1 + t4120 / 0.5e1;
        const Scalar t4160 = t4112 * t4141;
        const Scalar t4110 = 0.3e1 * b * t4160 + c * t4153 - l2 * t4117 + l1 * t4120;
        const Scalar t4163 = t4110 / 0.4e1;
        const Scalar t4108 = -t4138 * t4117 + t4140 * t4120 + (b * t4163 + t4112 * t4167) * t4141;
        const Scalar t4137 = l2 * t4138;
        const Scalar t4139 = l1 * t4140;
        const Scalar t4107 = -t4137 * t4117 + t4139 * t4120 + (-b * t4108 / 0.3e1 + 0.3e1 / 0.4e1 * c * t4110) * t4141;
        const Scalar t4165 = t4107 / 0.2e1;
        const Scalar t4164 = t4108 / 0.3e1;
        const Scalar t4162 = -t4148 / 0.3e1;
        const Scalar t4161 = t4150 / 0.3e1;
        const Scalar t4132 = w[4];
        const Scalar t4157 = t4132 * t4141;
        const Scalar t4133 = w[3];
        const Scalar t4156 = t4133 * t4141;
        const Scalar t4134 = w[2];
        const Scalar t4155 = t4134 * t4141;
        const Scalar t4135 = w[1];
        const Scalar t4154 = t4135 * t4141;
        const Scalar t4152 = t4159 / 0.3e1;
        const Scalar t4136 = w[0];
        const Scalar t4114 = b * t4152 + t4162 + t4161;
        const Scalar t4111 = t4114 * t4166 + c * t4152 - l2 * t4148 + l1 * t4150;
        const Scalar t4109 = -t4138 * t4148 + t4140 * t4150 + (-b * t4111 / 0.2e1 + t4114 * t4167) * t4141;
        const Scalar t68 = sqrt(a * t4125);
        const Scalar t71 = sqrt(a * t4126);
        const Scalar t75 = log((t68 + t4128) / (t71 + t4129));
        const Scalar t76 = sqrt(a);
        res[0] = t4136 * t4152 + t4114 * t4154 + t4111 * t4155 / 0.2e1 + t4109 * t4156 + (t4139 * t4161 + t4137 * t4162 + (t75 / t76 - l2 * t4142 + l1 * t4143 + b * t4109 + (b * t4158 - t4142 + t4143) * t4166) * t4141) * t4157;
        res[1] = t4136 * t4153 + (t4135 * t4112 + t4134 * t4163 + t4133 * t4164 + t4132 * t4165) * t4141;
        const Scalar t100 = t4138 * t4138;
        const Scalar t102 = t4140 * t4140;
        res[2] = t4136 * t4160 + t4154 * t4163 + t4155 * t4164 + t4156 * t4165 + (-t100 * t4117 + t102 * t4120 + (-0.3e1 / 0.2e1 * b * t4107 + 0.4e1 / 0.3e1 * c * t4108) * t4141) * t4157;

    }
    
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line<5> (Scalar l1, Scalar l2, Scalar a, Scalar b, const Scalar w[5])
    {
        const Scalar t4181 = a * l2 - b;
        const Scalar t4177 = fabs(t4181);
        const Scalar t2 = t4177 * t4177;
        const Scalar t4210 = 0.1e1 / t2;
        const Scalar t4182 = a * l1 - b;
        const Scalar t4178 = fabs(t4182);
        const Scalar t4 = t4178 * t4178;
        const Scalar t4209 = 0.1e1 / t4;
        const Scalar t4173 = 0.1e1 / t4177 * t4210;
        const Scalar t4175 = 0.1e1 / t4178 * t4209;
        const Scalar t4174 = t4210 * t4173;
        const Scalar t4176 = t4209 * t4175;
        const Scalar t10 = fabs(t4181 * t4174 - t4182 * t4176);
        const Scalar t4208 = t10 / 0.4e1;
        const Scalar t4187 = sqrt(a);
        const Scalar t4183 = t4187 * a;
        const Scalar t4180 = t4182 * t4182;
        const Scalar t4201 = t4180 * t4176;
        const Scalar t4179 = t4181 * t4181;
        const Scalar t4203 = t4179 * t4174;
        const Scalar t4171 = (b * t4208 - t4203 / 0.3e1 + t4201 / 0.3e1) * t4183;
        const Scalar t4185 = b * b;
        const Scalar t4186 = 0.1e1 / a;
        const Scalar t15 = l2 * l2;
        const Scalar t4199 = t15 * t4203;
        const Scalar t16 = l1 * l1;
        const Scalar t4200 = t16 * t4201;
        const Scalar t4205 = t4171 * t4186;
        const Scalar t4206 = (b * t4205 + t4185 * t4187 * t4208 - t4183 * (l2 * t4203 - l1 * t4201)) * t4186;
        const Scalar t28 = a * a;
        const Scalar t4207 = (-b * t4206 / 0.2e1 + 0.2e1 * t4185 * t4171 / t28 - t4183 * (t4199 - t4200)) * t4186;
        const Scalar t4204 = t4179 * t4173;
        const Scalar t4202 = t4180 * t4175;
        const Scalar t50 = fabs(t4181 * t4173 - t4182 * t4175);
        const Scalar t59 = log(t4181 / t4182);
        const Scalar t60 = fabs(t59);
        return  w[0] * t4183 * t4208 + w[1] * t4205 + w[2] * t4206 / 0.2e1 + w[3] * t4207 + w[4] * (b * t4207 + (b * (b * t50 / 0.2e1 - t4204 + t4202) * t4187 * t4186 + t60 / t4187 - t4187 * (l2 * t4204 - l1 * t4202)) * t4186 - t4183 * (l2 * t4199 - l1 * t4200) / 0.3e1) * t4186;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF<5> (Scalar a, Scalar b, Scalar c, 
                                                                          Scalar d, Scalar q,
                                                                          const Scalar w[5], Scalar (&res)[3])
    {
        const Scalar t4259 = 0.2e1 * c;
        const Scalar t4235 = a - b;
        const Scalar t4243 = 0.1e1 / a;
        const Scalar t4258 = b * t4243;
        const Scalar t4231 = a - 0.2e1 * b + c;
        const Scalar t4230 = 0.1e1 / t4231;
        const Scalar t4244 = sqrt(t4230);
        const Scalar t4246 = t4244 * t4230;
        const Scalar t4227 = t4230 * t4246;
        const Scalar t4224 = -t4227 / 0.5e1;
        const Scalar t4242 = 0.1e1 / c;
        const Scalar t4245 = sqrt(t4242);
        const Scalar t4248 = t4245 * t4242;
        const Scalar t4232 = t4242 * t4248;
        const Scalar t4236 = a * c;
        const Scalar t3 = b * b;
        const Scalar t4226 = 0.1e1 / (t4236 - t3);
        const Scalar t4252 = (t4235 * t4244 + b * t4245) * t4226;
        const Scalar t4253 = (0.2e1 * a * t4252 + t4235 * t4246 + b * t4248) * t4226;
        const Scalar t4251 = (0.4e1 / 0.3e1 * a * t4253 + t4235 * t4227 + b * t4232) * t4226 / 0.5e1;
        const Scalar t4219 = b * t4251 + t4224 + t4232 / 0.5e1;
        const Scalar t4217 = 0.3e1 * t4219 * t4258 + c * t4251 - t4227;
        const Scalar t4255 = t4217 / 0.4e1;
        const Scalar t4215 = -t4227 + (b * t4255 + t4219 * t4259) * t4243;
        const Scalar t4213 = -t4227 + (-b * t4215 / 0.3e1 + 0.3e1 / 0.4e1 * c * t4217) * t4243;
        const Scalar t4257 = t4213 / 0.2e1;
        const Scalar t4256 = t4215 / 0.3e1;
        const Scalar t4239 = w[2];
        const Scalar t4254 = t4239 / 0.2e1;
        const Scalar t4250 = t4253 / 0.3e1;
        const Scalar t4241 = w[0];
        const Scalar t4240 = w[1];
        const Scalar t4238 = w[3];
        const Scalar t4237 = w[4];
        const Scalar t4225 = -t4246 / 0.3e1;
        const Scalar t4221 = b * t4250 + t4225 + t4248 / 0.3e1;
        const Scalar t4218 = t4221 * t4258 + c * t4250 - t4246;
        const Scalar t4216 = -t4246 + (-b * t4218 / 0.2e1 + t4221 * t4259) * t4243;
        const Scalar t46 = sqrt(a * t4231);
        const Scalar t48 = sqrt(t4236);
        const Scalar t52 = log((t46 + t4235) / (-b + t48));
        const Scalar t53 = sqrt(a);
        const Scalar t4214 = t4225 + (b * t4216 + t52 / t53 - t4244 + (b * t4252 - t4244 + t4245) * t4258) * t4243;
        const Scalar t4212 = -t4227 + (-0.3e1 / 0.2e1 * b * t4213 + 0.4e1 / 0.3e1 * c * t4215) * t4243;
        const Scalar t4211 = (t4241 * t4219 + t4240 * t4255 + t4239 * t4256 + t4238 * t4257 + t4237 * t4212) * t4243;
        res[0] = t4241 * t4250 + (t4240 * t4221 + t4218 * t4254 + t4238 * t4216 + t4237 * t4214) * t4243;
        res[1] = d * t4211 + (t4241 * t4251 + (t4240 * t4219 + t4239 * t4255 + t4238 * t4256 + t4237 * t4257) * t4243) * q;
        res[2] = q * t4211 + (t4241 * t4255 + t4240 * t4256 + t4213 * t4254 + t4238 * t4212 + (t4224 + (b * t4212 + t4214) * t4243) * t4237) * d * t4243;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line<5> (Scalar a, Scalar b, 
                                                                                  Scalar d, Scalar q,
                                                                                  const Scalar w[5], Scalar (&res)[3])
    {
        const Scalar t4281 = a - b;
        const Scalar t4275 = fabs(t4281);
        const Scalar t1 = t4275 * t4275;
        const Scalar t4322 = 0.1e1 / t1;
        const Scalar t4280 = fabs(b);
        const Scalar t2 = t4280 * t4280;
        const Scalar t4321 = 0.1e1 / t2;
        const Scalar t4272 = 0.1e1 / t4275 * t4322;
        const Scalar t4276 = 0.1e1 / t4280 * t4321;
        const Scalar t4273 = t4322 * t4272;
        const Scalar t4277 = t4321 * t4276;
        const Scalar t4291 = 0.1e1 / a;
        const Scalar t4320 = b * t4291;
        const Scalar t4278 = t4321 * t4277;
        const Scalar t4293 = sqrt(a);
        const Scalar t4302 = t4293 * a;
        const Scalar t4282 = a * t4302;
        const Scalar t4290 = b * b;
        const Scalar t4274 = t4322 * t4273;
        const Scalar t4279 = t4281 * t4281;
        const Scalar t4310 = t4279 * t4274;
        const Scalar t4304 = -t4310 / 0.5e1;
        const Scalar t9 = fabs(t4281 * t4274 + b * t4278);
        const Scalar t4315 = t9 / 0.6e1;
        const Scalar t4268 = (b * t4315 + t4304 + t4290 * t4278 / 0.5e1) * t4282;
        const Scalar t4307 = t4282 * t4310;
        const Scalar t14 = a * a;
        const Scalar t4309 = t4290 / t14;
        const Scalar t4308 = 0.2e1 * t4309;
        const Scalar t4266 = 0.3e1 * t4268 * t4320 + t4290 * t4302 * t4315 - t4307;
        const Scalar t4317 = t4266 / 0.4e1;
        const Scalar t4264 = t4317 * t4320 + t4268 * t4308 - t4307;
        const Scalar t4262 = -t4264 * t4320 / 0.3e1 + 0.3e1 / 0.4e1 * t4266 * t4309 - t4307;
        const Scalar t4319 = t4262 / 0.2e1;
        const Scalar t4318 = t4264 / 0.3e1;
        const Scalar t4270 = fabs(t4281 * t4273 + b * t4277);
        const Scalar t4316 = t4270 / 0.4e1;
        const Scalar t4287 = w[2];
        const Scalar t4314 = t4287 / 0.2e1;
        const Scalar t4289 = w[0];
        const Scalar t4313 = t4289 / 0.4e1;
        const Scalar t4312 = t4279 * t4272;
        const Scalar t4311 = t4279 * t4273;
        const Scalar t4306 = t4302 * t4311;
        const Scalar t4305 = -t4311 / 0.3e1;
        const Scalar t4288 = w[1];
        const Scalar t4286 = w[3];
        const Scalar t4285 = w[4];
        const Scalar t4269 = (b * t4316 + t4305 + t4290 * t4277 / 0.3e1) * t4302;
        const Scalar t4267 = t4269 * t4320 + t4290 * t4293 * t4316 - t4306;
        const Scalar t4265 = -t4267 * t4320 / 0.2e1 + t4269 * t4308 - t4306;
        const Scalar t44 = fabs(t4281 * t4272 + b * t4276);
        const Scalar t53 = log(-t4281 / b);
        const Scalar t54 = fabs(t53);
        const Scalar t4263 = t4265 * t4320 + ((b * t44 / 0.2e1 - t4312 + t4290 * t4276) * t4293 * t4320 + t54 / t4293 - t4293 * t4312) * t4291 + t4302 * t4305;
        const Scalar t4261 = -0.3e1 / 0.2e1 * t4262 * t4320 + 0.4e1 / 0.3e1 * t4264 * t4309 - t4307;
        const Scalar t4260 = (t4289 * t4268 + t4288 * t4317 + t4287 * t4318 + t4286 * t4319 + t4285 * t4261) * t4291;
        res[0] = t4302 * t4270 * t4313 + (t4288 * t4269 + t4267 * t4314 + t4286 * t4265 + t4285 * t4263) * t4291;
        res[1] = d * t4260 + (t4289 * t4282 * t4315 + (t4288 * t4268 + t4287 * t4317 + t4286 * t4318 + t4285 * t4319) * t4291) * q;
        res[2] = q * t4260 + (t4266 * t4313 + t4288 * t4318 + t4262 * t4314 + t4286 * t4261 + (t4282 * t4304 + (b * t4261 + t4263) * t4291) * t4285) * d * t4291;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_cste<5> (Scalar a, Scalar b, Scalar c, Scalar l, Scalar (&res)[3])
    {
        const Scalar t4342 = 0.1e1 / c;
        const Scalar t4338 = a * l;
        const Scalar t4326 = c + (-0.2e1 * b + t4338) * l;
        const Scalar t4341 = 0.1e1 / t4326;
        const Scalar t4 = c * c;
        const Scalar t4340 = 0.1e1 / t4;
        const Scalar t5 = t4326 * t4326;
        const Scalar t4339 = 0.1e1 / t5;
        const Scalar t7 = b * b;
        const Scalar t4329 = a * c - t7;
        const Scalar t4327 = sqrt(t4329);
        const Scalar t4328 = 0.1e1 / t4329;
        const Scalar t4330 = t4338 - b;
        const Scalar t8 = sqrt(t4328);
        const Scalar t10 = atan2(t4330, t4327);
        const Scalar t11 = atan2(-b, t4327);
        const Scalar t4337 = (0.3e1 / 0.2e1 * a * (a * t8 * (t10 - t11) + t4330 * t4341 + b * t4342) * t4328 + t4330 * t4339 + b * t4340) * t4328;
        const Scalar t4325 = t4341 * t4339;
        const Scalar t4331 = t4342 * t4340;
        const Scalar t4336 = (0.5e1 / 0.4e1 * a * t4337 + t4330 * t4325 + b * t4331) * t4328 / 0.6e1;
        res[0] = t4337 / 0.4e1;
        res[1] = t4336;
        res[2] = (b * t4336 - t4325 / 0.6e1 + t4331 / 0.6e1) / a;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line_cste<5> (Scalar a, Scalar b, Scalar l, Scalar (&res)[3]) 
    {
        const Scalar t4348 = fabs(b);
        const Scalar t4356 = t4348 * t4348;
        const Scalar t1 = 0.1e1 / t4356;
        const Scalar t3 = t4356 * t4356;
        const Scalar t4364 = b * t1 / t3;
        const Scalar t4346 = a * l - b;
        const Scalar t4345 = fabs(t4346);
        const Scalar t4352 = t4345 * t4345;
        const Scalar t6 = 0.1e1 / t4352;
        const Scalar t7 = t4352 * t4352;
        const Scalar t4363 = t6 / t7;
        const Scalar t4361 = t1 * t4364;
        const Scalar t4344 = t6 * t4363;
        const Scalar t11 = fabs(t4346 * t4344 + t4361);
        const Scalar t4360 = t11 / 0.7e1;
        const Scalar t4350 = a * a;
        const Scalar t4349 = a * t4350;
        const Scalar t14 = fabs(t4346 * t4363 + t4364);
        res[0] = t4350 * t14 / 0.5e1;
        res[1] = t4349 * t4360;
        const Scalar t16 = t4346 * t4346;
        res[2] = (-t16 * t4344 / 0.6e1 + (t4360 + t4361 / 0.6e1) * b) * t4349 / a;

    }
    
    

    /////////////////////////////////
    // Code for kernel of degree 6 //
    // and weight of degree 1      //
    /////////////////////////////////

    template<>
    inline Scalar HomotheticCauchyInverse_segment_F<6> (Scalar a, Scalar b, Scalar c, const Scalar w[6])
    {
        const Scalar t4383 = 0.1e1 / a;
        const Scalar t4389 = t4383 / 0.2e1;
        const Scalar t4379 = a - 0.2e1 * b + c;
        const Scalar t2 = t4379 * t4379;
        const Scalar t4378 = 0.1e1 / t2;
        const Scalar t4374 = -t4378 / 0.4e1;
        const Scalar t4 = c * c;
        const Scalar t4382 = 0.1e1 / t4;
        const Scalar t6 = b * b;
        const Scalar t4376 = a * c - t6;
        const Scalar t4375 = 0.1e1 / t4376;
        const Scalar t4380 = a - b;
        const Scalar t4377 = 0.1e1 / t4379;
        const Scalar t4381 = 0.1e1 / c;
        const Scalar t4373 = sqrt(t4376);
        const Scalar t7 = sqrt(t4375);
        const Scalar t8 = atan2(t4380, t4373);
        const Scalar t9 = atan2(-b, t4373);
        const Scalar t4386 = t7 * (t8 - t9);
        const Scalar t4387 = (a * t4386 + t4380 * t4377 + b * t4381) * t4375;
        const Scalar t4384 = (0.3e1 / 0.2e1 * a * t4387 + t4380 * t4378 + b * t4382) * t4375 / 0.4e1;
        const Scalar t4388 = (b * t4384 + t4374 + t4382 / 0.4e1) * t4383;
        const Scalar t4385 = 0.2e1 * t4388;
        const Scalar t4367 = c * t4385 - t4378;
        const Scalar t4366 = b * t4385 + c * t4384 - t4378;
        const Scalar t4365 = -t4378 + (-b * t4367 + c * t4366) * t4383;
        const Scalar t47 = log(t4379 * t4381);
        return  w[0] * t4384 + w[1] * t4388 + w[2] * t4366 * t4383 / 0.3e1 + w[3] * t4367 * t4389 + w[4] * t4365 * t4383 + w[5] * (t4374 + (t47 * t4389 - t4377 / 0.2e1 + (t4365 + (c * t4387 / 0.2e1 - t4377 + t4386) * t4383) * b) * t4383) * t4383;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_GradF<6> (Scalar a, Scalar b, Scalar c, Scalar w[6], Scalar (&res)[2])
    {
        const Scalar t4425 = 0.1e1 / c;
        const Scalar t4424 = -0.2e1 * b;
        const Scalar t4400 = a + t4424 + c;
        const Scalar t4423 = 0.1e1 / t4400;
        const Scalar t2 = c * c;
        const Scalar t4422 = 0.1e1 / t2;
        const Scalar t3 = t4400 * t4400;
        const Scalar t4421 = 0.1e1 / t3;
        const Scalar t5 = b * b;
        const Scalar t4398 = a * c - t5;
        const Scalar t4397 = 0.1e1 / t4398;
        const Scalar t4420 = a * t4397;
        const Scalar t4399 = t4423 * t4421;
        const Scalar t4396 = sqrt(t4398);
        const Scalar t4401 = a - b;
        const Scalar t4408 = t4425 * t4422;
        const Scalar t6 = sqrt(t4397);
        const Scalar t8 = atan2(t4401, t4396);
        const Scalar t9 = atan2(-b, t4396);
        const Scalar t4414 = (0.5e1 / 0.4e1 * (0.3e1 / 0.2e1 * (a * t6 * (t8 - t9) + t4401 * t4423 + b * t4425) * t4420 + t4401 * t4421 + b * t4422) * t4420 + t4401 * t4399 + b * t4408) * t4397 / 0.6e1;
        const Scalar t4394 = b * t4414 - t4399 / 0.6e1 + t4408 / 0.6e1;
        const Scalar t4409 = 0.1e1 / a;
        const Scalar t4415 = t4394 * t4409;
        const Scalar t4393 = 0.4e1 * b * t4415 + c * t4414 - t4399;
        const Scalar t4391 = -t4399 + (0.2e1 / 0.5e1 * b * t4393 + 0.2e1 * c * t4394) * t4409;
        const Scalar t4416 = t4393 * t4409;
        const Scalar t4392 = 0.3e1 / 0.5e1 * c * t4416 - t4399;
        const Scalar t4390 = -t4399 + (-0.2e1 / 0.3e1 * b * t4392 + c * t4391) * t4409;
        const Scalar t4419 = t4390 / 0.2e1;
        const Scalar t4418 = t4391 / 0.4e1;
        const Scalar t4417 = t4392 / 0.3e1;
        const Scalar t4407 = w[0];
        const Scalar t4406 = w[1];
        const Scalar t4405 = w[2];
        const Scalar t4404 = w[3];
        const Scalar t4403 = w[4];
        const Scalar t4402 = w[5];
        res[0] = t4407 * t4414 + (t4406 * t4394 + t4405 * t4393 / 0.5e1 + t4404 * t4418 + t4403 * t4417 + t4402 * t4419) * t4409;
        res[1] = t4407 * t4415 + t4406 * t4416 / 0.5e1 + (t4405 * t4418 + t4404 * t4417 + t4403 * t4419 + (-t4399 + (t4390 * t4424 + 0.5e1 / 0.3e1 * c * t4392) * t4409) * t4402) * t4409;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF<6> (Scalar a, Scalar b, Scalar c, const Scalar w[6], Scalar (&res)[3])
    {
        const Scalar t4455 = 0.1e1 / c;
        const Scalar t4479 = -0.2e1 * b;
        const Scalar t4447 = a + t4479 + c;
        const Scalar t4444 = 0.1e1 / t4447;
        const Scalar t2 = c * c;
        const Scalar t4456 = 0.1e1 / t2;
        const Scalar t3 = t4447 * t4447;
        const Scalar t4445 = 0.1e1 / t3;
        const Scalar t4446 = t4444 * t4445;
        const Scalar t5 = b * b;
        const Scalar t4443 = a * c - t5;
        const Scalar t4442 = 0.1e1 / t4443;
        const Scalar t4448 = a - b;
        const Scalar t4457 = t4455 * t4456;
        const Scalar t4440 = sqrt(t4443);
        const Scalar t6 = sqrt(t4442);
        const Scalar t7 = atan2(t4448, t4440);
        const Scalar t8 = atan2(-b, t4440);
        const Scalar t4471 = t6 * (t7 - t8);
        const Scalar t4472 = (a * t4471 + t4448 * t4444 + b * t4455) * t4442;
        const Scalar t4473 = (0.3e1 / 0.2e1 * a * t4472 + t4448 * t4445 + b * t4456) * t4442;
        const Scalar t4464 = (0.5e1 / 0.4e1 * a * t4473 + t4448 * t4446 + b * t4457) * t4442 / 0.6e1;
        const Scalar t4432 = b * t4464 - t4446 / 0.6e1 + t4457 / 0.6e1;
        const Scalar t4458 = 0.1e1 / a;
        const Scalar t4474 = t4432 * t4458;
        const Scalar t4430 = 0.4e1 * b * t4474 + c * t4464 - t4446;
        const Scalar t4427 = -t4446 + (0.2e1 / 0.5e1 * b * t4430 + 0.2e1 * c * t4432) * t4458;
        const Scalar t4429 = 0.3e1 / 0.5e1 * c * t4430 * t4458 - t4446;
        const Scalar t4426 = -t4446 + (-0.2e1 / 0.3e1 * b * t4429 + c * t4427) * t4458;
        const Scalar t4478 = t4426 / 0.2e1;
        const Scalar t4477 = t4427 / 0.4e1;
        const Scalar t4476 = t4429 / 0.3e1;
        const Scalar t4475 = t4430 / 0.5e1;
        const Scalar t4449 = w[5];
        const Scalar t4470 = t4449 * t4458;
        const Scalar t4450 = w[4];
        const Scalar t4469 = t4450 * t4458;
        const Scalar t4451 = w[3];
        const Scalar t4468 = t4451 * t4458;
        const Scalar t4452 = w[2];
        const Scalar t4467 = t4452 * t4458;
        const Scalar t4453 = w[1];
        const Scalar t4466 = t4453 * t4458;
        const Scalar t4441 = -t4445 / 0.4e1;
        const Scalar t4463 = t4473 / 0.4e1;
        const Scalar t4435 = b * t4463 + t4441 + t4456 / 0.4e1;
        const Scalar t4465 = 0.2e1 * t4435 * t4458;
        const Scalar t4454 = w[0];
        const Scalar t4433 = c * t4465 - t4445;
        const Scalar t4431 = b * t4465 + c * t4463 - t4445;
        const Scalar t4428 = -t4445 + (-b * t4433 + c * t4431) * t4458;
        const Scalar t64 = log(t4447 * t4455);
        res[0] = t4454 * t4463 + t4435 * t4466 + t4431 * t4467 / 0.3e1 + t4433 * t4468 / 0.2e1 + t4428 * t4469 + (t4441 + (t64 * t4458 / 0.2e1 - t4444 / 0.2e1 + (t4428 + (c * t4472 / 0.2e1 - t4444 + t4471) * t4458) * b) * t4458) * t4470;
        res[1] = t4454 * t4464 + (t4453 * t4432 + t4452 * t4475 + t4451 * t4477 + t4450 * t4476 + t4449 * t4478) * t4458;
        res[2] = t4454 * t4474 + t4466 * t4475 + t4467 * t4477 + t4468 * t4476 + t4469 * t4478 + (-t4446 + (t4426 * t4479 + 0.5e1 / 0.3e1 * c * t4429) * t4458) * t4470;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line<6> (Scalar a, Scalar b, const Scalar w[6])
    {
        const Scalar t4491 = a - b;
        const Scalar t1 = fabs(t4491);
        const Scalar t4496 = t1 * t1;
        const Scalar t4512 = 0.1e1 / t4496;
        const Scalar t2 = t4496 * t4496;
        const Scalar t4485 = 0.1e1 / t2;
        const Scalar t3 = fabs(b);
        const Scalar t4499 = t3 * t3;
        const Scalar t4511 = 0.1e1 / t4499;
        const Scalar t4 = t4499 * t4499;
        const Scalar t4510 = 0.1e1 / t4;
        const Scalar t4486 = t4512 * t4485;
        const Scalar t4488 = t4511 * t4510;
        const Scalar t8 = fabs(t4491 * t4486 + b * t4488);
        const Scalar t4509 = t8 / 0.5e1;
        const Scalar t4492 = b * b;
        const Scalar t4493 = a * a;
        const Scalar t4489 = t4491 * t4491;
        const Scalar t4506 = t4489 * t4486;
        const Scalar t4502 = -t4506 / 0.4e1;
        const Scalar t4483 = (b * t4509 + t4502 + t4492 * t4488 / 0.4e1) * t4493;
        const Scalar t4494 = 0.1e1 / a;
        const Scalar t4503 = t4493 * t4506;
        const Scalar t4505 = t4492 / t4493;
        const Scalar t4508 = (0.2e1 * t4483 * t4505 - t4503) * t4494;
        const Scalar t4507 = t4483 * t4494;
        const Scalar t4504 = a * t4489 * t4485;
        const Scalar t4481 = 0.2e1 * b * t4507 + t4492 * a * t4509 - t4503;
        const Scalar t4480 = -b * t4508 + t4481 * t4505 - t4503;
        const Scalar t47 = fabs(t4491 * t4485 + b * t4510);
        const Scalar t55 = fabs(t4491 * t4512 + b * t4511);
        const Scalar t59 = log(-t4491 / b);
        return  w[0] * t4493 * t4509 + w[1] * t4507 + w[2] * t4481 * t4494 / 0.3e1 + w[3] * t4508 / 0.2e1 + w[4] * t4480 * t4494 + w[5] * (t4493 * t4502 + (b * t4480 - t4504 / 0.2e1 + (b * (t4492 * t47 / 0.3e1 - t4504) + b * t55 + t59) * t4494) * t4494) * t4494;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_GradF_on_line<6> (Scalar a, Scalar b, const Scalar w[6], Scalar (&res)[2])
    {
        const Scalar t4531 = 0.1e1 / a;
        const Scalar t4549 = b * t4531;
        const Scalar t1 = fabs(b);
        const Scalar t2 = t1 * t1;
        const Scalar t3 = t2 * t2;
        const Scalar t4 = t3 * t3;
        const Scalar t4520 = 0.1e1 / t4;
        const Scalar t4529 = b * b;
        const Scalar t4533 = a * a;
        const Scalar t4530 = a * t4533;
        const Scalar t4522 = a - b;
        const Scalar t5 = fabs(t4522);
        const Scalar t6 = t5 * t5;
        const Scalar t7 = t6 * t6;
        const Scalar t8 = t7 * t7;
        const Scalar t4519 = 0.1e1 / t8;
        const Scalar t9 = t4522 * t4522;
        const Scalar t4543 = t9 * t4519;
        const Scalar t13 = fabs(t4522 * t4519 + b * t4520);
        const Scalar t4545 = t13 / 0.7e1;
        const Scalar t4517 = (b * t4545 - t4543 / 0.6e1 + t4529 * t4520 / 0.6e1) * t4530;
        const Scalar t4541 = t4530 * t4543;
        const Scalar t4516 = 0.4e1 * t4517 * t4549 + t4529 * t4533 * t4545 - t4541;
        const Scalar t4542 = t4529 / t4533;
        const Scalar t4514 = 0.2e1 / 0.5e1 * t4516 * t4549 + 0.2e1 * t4517 * t4542 - t4541;
        const Scalar t4548 = t4514 / 0.4e1;
        const Scalar t4515 = 0.3e1 / 0.5e1 * t4516 * t4542 - t4541;
        const Scalar t4547 = t4515 / 0.3e1;
        const Scalar t4546 = t4516 / 0.5e1;
        const Scalar t4528 = w[0];
        const Scalar t4527 = w[1];
        const Scalar t4526 = w[2];
        const Scalar t4525 = w[3];
        const Scalar t4524 = w[4];
        const Scalar t4523 = w[5];
        const Scalar t4513 = -0.2e1 / 0.3e1 * t4515 * t4549 + t4514 * t4542 - t4541;
        res[0] = t4528 * t4530 * t4545 + (t4527 * t4517 + t4526 * t4546 + t4525 * t4548 + t4524 * t4547 + t4523 * t4513 / 0.2e1) * t4531;
        res[1] = (t4528 * t4517 + t4527 * t4546 + t4526 * t4548 + t4525 * t4547 + t4523 * (0.5e1 / 0.3e1 * t4515 * t4542 - t4541) + (t4524 / 0.2e1 - 0.2e1 * t4523 * t4549) * t4513) * t4531;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_on_line<6> (Scalar a, Scalar b, const Scalar w[6], Scalar (&res)[3])
    {
        const Scalar t4570 = a - b;
        const Scalar t1 = fabs(t4570);
        const Scalar t4583 = t1 * t1;
        const Scalar t4610 = 0.1e1 / t4583;
        const Scalar t4584 = t4583 * t4583;
        const Scalar t4562 = 0.1e1 / t4584;
        const Scalar t2 = fabs(b);
        const Scalar t4587 = t2 * t2;
        const Scalar t4609 = 0.1e1 / t4587;
        const Scalar t4588 = t4587 * t4587;
        const Scalar t4608 = 0.1e1 / t4588;
        const Scalar t4580 = 0.1e1 / a;
        const Scalar t4607 = b * t4580;
        const Scalar t3 = t4588 * t4588;
        const Scalar t4567 = 0.1e1 / t3;
        const Scalar t4577 = b * b;
        const Scalar t4579 = a * a;
        const Scalar t4578 = a * t4579;
        const Scalar t4 = t4584 * t4584;
        const Scalar t4564 = 0.1e1 / t4;
        const Scalar t4568 = t4570 * t4570;
        const Scalar t4598 = t4568 * t4564;
        const Scalar t8 = fabs(t4570 * t4564 + b * t4567);
        const Scalar t4602 = t8 / 0.7e1;
        const Scalar t4558 = (b * t4602 - t4598 / 0.6e1 + t4577 * t4567 / 0.6e1) * t4578;
        const Scalar t4593 = t4578 * t4598;
        const Scalar t4555 = 0.4e1 * t4558 * t4607 + t4577 * t4579 * t4602 - t4593;
        const Scalar t4596 = t4577 / t4579;
        const Scalar t4594 = 0.2e1 * t4596;
        const Scalar t4553 = 0.2e1 / 0.5e1 * t4555 * t4607 + t4558 * t4594 - t4593;
        const Scalar t4606 = t4553 / 0.4e1;
        const Scalar t4554 = 0.3e1 / 0.5e1 * t4555 * t4596 - t4593;
        const Scalar t4605 = t4554 / 0.3e1;
        const Scalar t4604 = t4555 / 0.5e1;
        const Scalar t4563 = t4610 * t4562;
        const Scalar t4566 = t4609 * t4608;
        const Scalar t27 = fabs(t4570 * t4563 + b * t4566);
        const Scalar t4603 = t27 / 0.5e1;
        const Scalar t4599 = t4568 * t4563;
        const Scalar t4591 = -t4599 / 0.4e1;
        const Scalar t4559 = (b * t4603 + t4591 + t4577 * t4566 / 0.4e1) * t4579;
        const Scalar t4592 = t4579 * t4599;
        const Scalar t4601 = (t4559 * t4594 - t4592) * t4580;
        const Scalar t4600 = t4559 * t4580;
        const Scalar t4571 = w[5];
        const Scalar t4597 = t4571 * t4580;
        const Scalar t4595 = a * t4568 * t4562;
        const Scalar t4576 = w[0];
        const Scalar t4575 = w[1];
        const Scalar t4574 = w[2];
        const Scalar t4573 = w[3];
        const Scalar t4572 = w[4];
        const Scalar t4556 = 0.2e1 * b * t4600 + t4577 * a * t4603 - t4592;
        const Scalar t4552 = -b * t4601 + t4556 * t4596 - t4592;
        const Scalar t4551 = -0.2e1 / 0.3e1 * t4554 * t4607 + t4553 * t4596 - t4593;
        const Scalar t61 = fabs(t4570 * t4562 + b * t4608);
        const Scalar t69 = fabs(t4570 * t4610 + b * t4609);
        const Scalar t73 = log(-t4570 / b);
        res[0] = t4576 * t4579 * t4603 + t4575 * t4600 + t4574 * t4556 * t4580 / 0.3e1 + t4573 * t4601 / 0.2e1 + t4572 * t4552 * t4580 + (t4579 * t4591 + (b * t4552 - t4595 / 0.2e1 + (b * (t4577 * t61 / 0.3e1 - t4595) + b * t69 + t73) * t4580) * t4580) * t4597;
        res[1] = t4576 * t4578 * t4602 + (t4575 * t4558 + t4574 * t4604 + t4573 * t4606 + t4572 * t4605 + t4571 * t4551 / 0.2e1) * t4580;
        res[2] = (t4576 * t4558 + t4575 * t4604 + t4574 * t4606 + t4573 * t4605 + t4571 * (0.5e1 / 0.3e1 * t4554 * t4596 - t4593) + (t4572 / 0.2e1 - 0.2e1 * b * t4597) * t4551) * t4580;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_cste<6> (Scalar a, Scalar b, Scalar c, Scalar l)
    {
        const Scalar t4616 = a * l;
        const Scalar t4615 = t4616 - b;
        const Scalar t2 = b * b;
        const Scalar t4614 = a * c - t2;
        const Scalar t4613 = 0.1e1 / t4614;
        const Scalar t4612 = sqrt(t4614);
        const Scalar t4611 = c + (-0.2e1 * b + t4616) * l;
        const Scalar t6 = sqrt(t4613);
        const Scalar t8 = atan2(t4615, t4612);
        const Scalar t9 = atan2(-b, t4612);
        const Scalar t20 = t4611 * t4611;
        const Scalar t23 = c * c;
        return  (0.3e1 / 0.2e1 * a * (a * t6 * (t8 - t9) + t4615 / t4611 + b / c) * t4613 + t4615 / t20 + b / t23) * t4613 / 0.4e1;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_cste<6> (Scalar a, Scalar b, Scalar c, Scalar l, Scalar (&res)[3])
    {
        const Scalar t4636 = 0.1e1 / c;
        const Scalar t4632 = a * l;
        const Scalar t4620 = c + (-0.2e1 * b + t4632) * l;
        const Scalar t4635 = 0.1e1 / t4620;
        const Scalar t4 = c * c;
        const Scalar t4634 = 0.1e1 / t4;
        const Scalar t5 = t4620 * t4620;
        const Scalar t4633 = 0.1e1 / t5;
        const Scalar t7 = b * b;
        const Scalar t4623 = a * c - t7;
        const Scalar t4621 = sqrt(t4623);
        const Scalar t4622 = 0.1e1 / t4623;
        const Scalar t4624 = t4632 - b;
        const Scalar t8 = sqrt(t4622);
        const Scalar t10 = atan2(t4624, t4621);
        const Scalar t11 = atan2(-b, t4621);
        const Scalar t4631 = (0.3e1 / 0.2e1 * a * (a * t8 * (t10 - t11) + t4624 * t4635 + b * t4636) * t4622 + t4624 * t4633 + b * t4634) * t4622;
        const Scalar t4619 = t4635 * t4633;
        const Scalar t4625 = t4636 * t4634;
        const Scalar t4630 = (0.5e1 / 0.4e1 * a * t4631 + t4624 * t4619 + b * t4625) * t4622 / 0.6e1;
        res[0] = t4631 / 0.4e1;
        res[1] = t4630;
        res[2] = (b * t4630 - t4619 / 0.6e1 + t4625 / 0.6e1) / a;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line_cste<6> (Scalar a, Scalar b, Scalar l)
    {
        const Scalar t4637 = a * l - b;
        const Scalar t2 = a * a;
        const Scalar t3 = fabs(t4637);
        const Scalar t4 = t3 * t3;
        const Scalar t5 = t4 * t4;
        const Scalar t9 = fabs(b);
        const Scalar t10 = t9 * t9;
        const Scalar t11 = t10 * t10;
        const Scalar t16 = fabs(t4637 / t5 / t4 + b / t11 / t10);
        return  t2 * t16 / 0.5e1;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FGradF_on_line_cste<6> (Scalar a, Scalar b, Scalar l, Scalar (&res)[3])
    {
        const Scalar t4651 = fabs(b);
        const Scalar t4659 = t4651 * t4651;
        const Scalar t1 = 0.1e1 / t4659;
        const Scalar t3 = t4659 * t4659;
        const Scalar t4667 = b * t1 / t3;
        const Scalar t4649 = a * l - b;
        const Scalar t4648 = fabs(t4649);
        const Scalar t4655 = t4648 * t4648;
        const Scalar t6 = 0.1e1 / t4655;
        const Scalar t7 = t4655 * t4655;
        const Scalar t4666 = t6 / t7;
        const Scalar t4664 = t1 * t4667;
        const Scalar t4647 = t6 * t4666;
        const Scalar t11 = fabs(t4649 * t4647 + t4664);
        const Scalar t4663 = t11 / 0.7e1;
        const Scalar t4653 = a * a;
        const Scalar t4652 = a * t4653;
        const Scalar t14 = fabs(t4649 * t4666 + t4667);
        res[0] = t4653 * t14 / 0.5e1;
        res[1] = t4652 * t4663;
        const Scalar t16 = t4649 * t4649;
        res[2] = (-t16 * t4647 / 0.6e1 + (t4663 + t4664 / 0.6e1) * b) * t4652 / a;

    }
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F<6> (Scalar l1, Scalar l2, Scalar a, Scalar b, Scalar c, const Scalar w[6])
    {
        const Scalar t4704 = -0.2e1 * b;
        const Scalar t4687 = l2 * l2;
        const Scalar t4680 = a * t4687 + l2 * t4704 + c;
        const Scalar t4 = t4680 * t4680;
        const Scalar t4676 = 0.1e1 / t4;
        const Scalar t4703 = -t4676 / 0.4e1;
        const Scalar t4688 = l1 * l1;
        const Scalar t4681 = a * t4688 + l1 * t4704 + c;
        const Scalar t8 = t4681 * t4681;
        const Scalar t4678 = 0.1e1 / t8;
        const Scalar t4702 = t4678 / 0.4e1;
        const Scalar t4689 = 0.1e1 / a;
        const Scalar t4701 = t4689 / 0.2e1;
        const Scalar t10 = b * b;
        const Scalar t4684 = a * c - t10;
        const Scalar t4683 = 0.1e1 / t4684;
        const Scalar t4685 = a * l2 - b;
        const Scalar t4686 = a * l1 - b;
        const Scalar t4675 = 0.1e1 / t4680;
        const Scalar t4677 = 0.1e1 / t4681;
        const Scalar t4682 = sqrt(t4684);
        const Scalar t13 = sqrt(t4683);
        const Scalar t14 = atan2(t4685, t4682);
        const Scalar t15 = atan2(t4686, t4682);
        const Scalar t4698 = t13 * (t14 - t15);
        const Scalar t4699 = (a * t4698 + t4685 * t4675 - t4686 * t4677) * t4683;
        const Scalar t4694 = (0.3e1 / 0.2e1 * a * t4699 + t4685 * t4676 - t4686 * t4678) * t4683 / 0.4e1;
        const Scalar t4700 = (b * t4694 + t4703 + t4702) * t4689;
        const Scalar t4697 = t4687 * t4676;
        const Scalar t4696 = t4688 * t4678;
        const Scalar t4695 = 0.2e1 * t4700;
        const Scalar t4670 = c * t4695 - t4697 + t4696;
        const Scalar t4669 = b * t4695 + c * t4694 - l2 * t4676 + l1 * t4678;
        const Scalar t4668 = -l2 * t4697 + l1 * t4696 + (-b * t4670 + c * t4669) * t4689;
        const Scalar t55 = t4688 * t4688;
        const Scalar t57 = t4687 * t4687;
        const Scalar t62 = log(t4680 * t4677);
        return  w[0] * t4694 + w[1] * t4700 + w[2] * t4669 * t4689 / 0.3e1 + w[3] * t4670 * t4701 + w[4] * t4668 * t4689 + w[5] * (t55 * t4702 + t57 * t4703 + (t4688 * t4677 / 0.2e1 + t62 * t4701 - t4687 * t4675 / 0.2e1 + (t4668 + (c * t4699 / 0.2e1 - l2 * t4675 + l1 * t4677 + t4698) * t4689) * b) * t4689) * t4689;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_FgradF<6> (Scalar l1, Scalar l2, Scalar a, Scalar b, Scalar c, const Scalar w[6], Scalar (&res)[3])
    {
        const Scalar t4775 = -0.2e1 * b;
        const Scalar t4740 = l2 * l2;
        const Scalar t4725 = a * t4740 + l2 * t4775 + c;
        const Scalar t4718 = 0.1e1 / t4725;
        const Scalar t4743 = l1 * l1;
        const Scalar t4726 = a * t4743 + l1 * t4775 + c;
        const Scalar t4721 = 0.1e1 / t4726;
        const Scalar t6 = t4725 * t4725;
        const Scalar t4719 = 0.1e1 / t6;
        const Scalar t7 = t4726 * t4726;
        const Scalar t4722 = 0.1e1 / t7;
        const Scalar t4720 = t4718 * t4719;
        const Scalar t4723 = t4721 * t4722;
        const Scalar t9 = b * b;
        const Scalar t4729 = a * c - t9;
        const Scalar t4728 = 0.1e1 / t4729;
        const Scalar t4730 = a * l2 - b;
        const Scalar t4731 = a * l1 - b;
        const Scalar t4727 = sqrt(t4729);
        const Scalar t12 = sqrt(t4728);
        const Scalar t13 = atan2(t4730, t4727);
        const Scalar t14 = atan2(t4731, t4727);
        const Scalar t4765 = t12 * (t13 - t14);
        const Scalar t4766 = (a * t4765 + t4730 * t4718 - t4731 * t4721) * t4728;
        const Scalar t4767 = (0.3e1 / 0.2e1 * a * t4766 + t4730 * t4719 - t4731 * t4722) * t4728;
        const Scalar t4756 = (0.5e1 / 0.4e1 * a * t4767 + t4730 * t4720 - t4731 * t4723) * t4728 / 0.6e1;
        const Scalar t4711 = b * t4756 - t4720 / 0.6e1 + t4723 / 0.6e1;
        const Scalar t4744 = 0.1e1 / a;
        const Scalar t4768 = t4711 * t4744;
        const Scalar t4709 = 0.4e1 * b * t4768 + c * t4756 - l2 * t4720 + l1 * t4723;
        const Scalar t4706 = -t4740 * t4720 + t4743 * t4723 + (0.2e1 / 0.5e1 * b * t4709 + 0.2e1 * c * t4711) * t4744;
        const Scalar t4739 = l2 * t4740;
        const Scalar t4742 = l1 * t4743;
        const Scalar t4708 = 0.3e1 / 0.5e1 * c * t4709 * t4744 - t4739 * t4720 + t4742 * t4723;
        const Scalar t4749 = t4740 * t4740;
        const Scalar t4758 = t4749 * t4720;
        const Scalar t4746 = t4743 * t4743;
        const Scalar t4759 = t4746 * t4723;
        const Scalar t4705 = -t4758 + t4759 + (-0.2e1 / 0.3e1 * b * t4708 + c * t4706) * t4744;
        const Scalar t4774 = t4705 / 0.2e1;
        const Scalar t4773 = t4706 / 0.4e1;
        const Scalar t4772 = t4708 / 0.3e1;
        const Scalar t4771 = t4709 / 0.5e1;
        const Scalar t4770 = -t4719 / 0.4e1;
        const Scalar t4769 = t4722 / 0.4e1;
        const Scalar t4732 = w[5];
        const Scalar t4764 = t4732 * t4744;
        const Scalar t4733 = w[4];
        const Scalar t4763 = t4733 * t4744;
        const Scalar t4734 = w[3];
        const Scalar t4762 = t4734 * t4744;
        const Scalar t4735 = w[2];
        const Scalar t4761 = t4735 * t4744;
        const Scalar t4736 = w[1];
        const Scalar t4760 = t4736 * t4744;
        const Scalar t4755 = t4767 / 0.4e1;
        const Scalar t4714 = b * t4755 + t4770 + t4769;
        const Scalar t4757 = 0.2e1 * t4714 * t4744;
        const Scalar t4737 = w[0];
        const Scalar t4712 = c * t4757 - t4740 * t4719 + t4743 * t4722;
        const Scalar t4710 = b * t4757 + c * t4755 - l2 * t4719 + l1 * t4722;
        const Scalar t4707 = -t4739 * t4719 + t4742 * t4722 + (-b * t4712 + c * t4710) * t4744;
        const Scalar t85 = log(t4725 * t4721);
        res[0] = t4737 * t4755 + t4714 * t4760 + t4710 * t4761 / 0.3e1 + t4712 * t4762 / 0.2e1 + t4707 * t4763 + (t4746 * t4769 + t4749 * t4770 + (t4743 * t4721 / 0.2e1 + t85 * t4744 / 0.2e1 - t4740 * t4718 / 0.2e1 + (t4707 + (c * t4766 / 0.2e1 - l2 * t4718 + l1 * t4721 + t4765) * t4744) * b) * t4744) * t4764;
        res[1] = t4737 * t4756 + (t4736 * t4711 + t4735 * t4771 + t4734 * t4773 + t4733 * t4772 + t4732 * t4774) * t4744;
        res[2] = t4737 * t4768 + t4760 * t4771 + t4761 * t4773 + t4762 * t4772 + t4763 * t4774 + (-l2 * t4758 + l1 * t4759 + (t4705 * t4775 + 0.5e1 / 0.3e1 * c * t4708) * t4744) * t4764;

    }
    
    
    template<>
    inline Scalar HomotheticCauchyInverse_segment_F_on_line<6> (Scalar l1, Scalar l2, Scalar a, Scalar b, const Scalar w[6])
    {
        const Scalar t4789 = a * l2 - b;
        const Scalar t2 = fabs(t4789);
        const Scalar t4801 = t2 * t2;
        const Scalar t4818 = 0.1e1 / t4801;
        const Scalar t3 = t4801 * t4801;
        const Scalar t4781 = 0.1e1 / t3;
        const Scalar t4790 = a * l1 - b;
        const Scalar t5 = fabs(t4790);
        const Scalar t4804 = t5 * t5;
        const Scalar t4817 = 0.1e1 / t4804;
        const Scalar t6 = t4804 * t4804;
        const Scalar t4783 = 0.1e1 / t6;
        const Scalar t4782 = t4818 * t4781;
        const Scalar t4784 = t4817 * t4783;
        const Scalar t10 = fabs(t4789 * t4782 - t4790 * t4784);
        const Scalar t4816 = t10 / 0.5e1;
        const Scalar t4794 = a * a;
        const Scalar t4788 = t4790 * t4790;
        const Scalar t4811 = t4788 * t4784;
        const Scalar t4787 = t4789 * t4789;
        const Scalar t4813 = t4787 * t4782;
        const Scalar t4779 = (b * t4816 - t4813 / 0.4e1 + t4811 / 0.4e1) * t4794;
        const Scalar t4795 = 0.1e1 / a;
        const Scalar t4792 = l1 * l1;
        const Scalar t4810 = t4788 * t4792;
        const Scalar t4807 = t4784 * t4810;
        const Scalar t4791 = l2 * l2;
        const Scalar t4812 = t4787 * t4791;
        const Scalar t4808 = t4782 * t4812;
        const Scalar t4793 = b * b;
        const Scalar t4809 = t4793 / t4794;
        const Scalar t4815 = (0.2e1 * t4779 * t4809 - t4794 * (t4808 - t4807)) * t4795;
        const Scalar t4814 = t4779 * t4795;
        const Scalar t4777 = 0.2e1 * b * t4814 + t4793 * a * t4816 - t4794 * (l2 * t4813 - l1 * t4811);
        const Scalar t4776 = -b * t4815 + t4777 * t4809 - t4794 * (l2 * t4808 - l1 * t4807);
        const Scalar t51 = t4791 * t4791;
        const Scalar t53 = t4792 * t4792;
        const Scalar t60 = log(t4789 / t4790);
        const Scalar t70 = fabs(t4789 * t4781 - t4790 * t4783);
        const Scalar t82 = fabs(t4789 * t4818 - t4790 * t4817);
        return  w[0] * t4794 * t4816 + w[1] * t4814 + w[2] * t4777 * t4795 / 0.3e1 + w[3] * t4815 / 0.2e1 + w[4] * t4776 * t4795 + w[5] * (-t4794 * (t51 * t4813 - t53 * t4811) / 0.4e1 + (t60 * t4795 - a * (t4781 * t4812 - t4783 * t4810) / 0.2e1 + (t4776 + (t4793 * t70 / 0.3e1 - a * (l2 * t4787 * t4781 - l1 * t4788 * t4783) + t82) * t4795) * b) * t4795) * t4795;

    }
    
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF<6> (Scalar a, Scalar b, Scalar c, 
                                                                          Scalar d, Scalar q,
                                                                          const Scalar w[6], Scalar (&res)[3])
    {
        const Scalar t4852 = 0.1e1 / c;
        const Scalar t4872 = -0.2e1 * b;
        const Scalar t4844 = a + t4872 + c;
        const Scalar t4841 = 0.1e1 / t4844;
        const Scalar t2 = c * c;
        const Scalar t4853 = 0.1e1 / t2;
        const Scalar t3 = t4844 * t4844;
        const Scalar t4842 = 0.1e1 / t3;
        const Scalar t4843 = t4841 * t4842;
        const Scalar t4838 = -t4843 / 0.6e1;
        const Scalar t4854 = t4852 * t4853;
        const Scalar t6 = b * b;
        const Scalar t4840 = a * c - t6;
        const Scalar t4839 = 0.1e1 / t4840;
        const Scalar t4845 = a - b;
        const Scalar t4836 = sqrt(t4840);
        const Scalar t7 = sqrt(t4839);
        const Scalar t8 = atan2(t4845, t4836);
        const Scalar t9 = atan2(-b, t4836);
        const Scalar t4863 = t7 * (t8 - t9);
        const Scalar t4864 = (a * t4863 + t4845 * t4841 + b * t4852) * t4839;
        const Scalar t4865 = (0.3e1 / 0.2e1 * a * t4864 + t4845 * t4842 + b * t4853) * t4839;
        const Scalar t4861 = (0.5e1 / 0.4e1 * a * t4865 + t4845 * t4843 + b * t4854) * t4839 / 0.6e1;
        const Scalar t4828 = b * t4861 + t4838 + t4854 / 0.6e1;
        const Scalar t4855 = 0.1e1 / a;
        const Scalar t4826 = 0.4e1 * b * t4828 * t4855 + c * t4861 - t4843;
        const Scalar t4823 = -t4843 + (0.2e1 / 0.5e1 * b * t4826 + 0.2e1 * c * t4828) * t4855;
        const Scalar t4825 = 0.3e1 / 0.5e1 * c * t4826 * t4855 - t4843;
        const Scalar t4821 = -t4843 + (-0.2e1 / 0.3e1 * b * t4825 + c * t4823) * t4855;
        const Scalar t4871 = t4821 / 0.2e1;
        const Scalar t4870 = t4823 / 0.4e1;
        const Scalar t4869 = t4825 / 0.3e1;
        const Scalar t4868 = t4826 / 0.5e1;
        const Scalar t4848 = w[3];
        const Scalar t4867 = t4848 / 0.2e1;
        const Scalar t4849 = w[2];
        const Scalar t4866 = t4849 / 0.3e1;
        const Scalar t4837 = -t4842 / 0.4e1;
        const Scalar t4860 = t4865 / 0.4e1;
        const Scalar t4831 = b * t4860 + t4837 + t4853 / 0.4e1;
        const Scalar t4862 = 0.2e1 * t4831 * t4855;
        const Scalar t4851 = w[0];
        const Scalar t4850 = w[1];
        const Scalar t4847 = w[4];
        const Scalar t4846 = w[5];
        const Scalar t4829 = c * t4862 - t4842;
        const Scalar t4827 = b * t4862 + c * t4860 - t4842;
        const Scalar t4824 = -t4842 + (-b * t4829 + c * t4827) * t4855;
        const Scalar t58 = log(t4844 * t4852);
        const Scalar t4822 = t4837 + (t58 * t4855 / 0.2e1 - t4841 / 0.2e1 + (t4824 + (c * t4864 / 0.2e1 - t4841 + t4863) * t4855) * b) * t4855;
        const Scalar t4820 = -t4843 + (t4821 * t4872 + 0.5e1 / 0.3e1 * c * t4825) * t4855;
        const Scalar t4819 = (t4851 * t4828 + t4850 * t4868 + t4849 * t4870 + t4848 * t4869 + t4847 * t4871 + t4846 * t4820) * t4855;
        res[0] = t4851 * t4860 + (t4850 * t4831 + t4827 * t4866 + t4829 * t4867 + t4847 * t4824 + t4846 * t4822) * t4855;
        res[1] = d * t4819 + (t4851 * t4861 + (t4850 * t4828 + t4849 * t4868 + t4848 * t4870 + t4847 * t4869 + t4846 * t4871) * t4855) * q;
        res[2] = q * t4819 + (t4851 * t4868 + t4850 * t4870 + t4825 * t4866 + t4821 * t4867 + t4847 * t4820 + (t4838 + (b * t4820 + t4822) * t4855) * t4846) * d * t4855;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line<6> (Scalar a, Scalar b, 
                                                                                  Scalar d, Scalar q,
                                                                                  const Scalar w[6], Scalar (&res)[3])
    {
        const Scalar t4895 = a - b;
        const Scalar t1 = fabs(t4895);
        const Scalar t4908 = t1 * t1;
        const Scalar t4937 = 0.1e1 / t4908;
        const Scalar t4909 = t4908 * t4908;
        const Scalar t4887 = 0.1e1 / t4909;
        const Scalar t2 = fabs(b);
        const Scalar t4912 = t2 * t2;
        const Scalar t4936 = 0.1e1 / t4912;
        const Scalar t4913 = t4912 * t4912;
        const Scalar t4935 = 0.1e1 / t4913;
        const Scalar t4905 = 0.1e1 / a;
        const Scalar t4934 = b * t4905;
        const Scalar t3 = t4913 * t4913;
        const Scalar t4892 = 0.1e1 / t3;
        const Scalar t4902 = b * b;
        const Scalar t4904 = a * a;
        const Scalar t4903 = a * t4904;
        const Scalar t4 = t4909 * t4909;
        const Scalar t4889 = 0.1e1 / t4;
        const Scalar t4893 = t4895 * t4895;
        const Scalar t4923 = t4893 * t4889;
        const Scalar t4916 = -t4923 / 0.6e1;
        const Scalar t9 = fabs(t4895 * t4889 + b * t4892);
        const Scalar t4928 = t9 / 0.7e1;
        const Scalar t4883 = (b * t4928 + t4916 + t4902 * t4892 / 0.6e1) * t4903;
        const Scalar t4919 = t4903 * t4923;
        const Scalar t4880 = 0.4e1 * t4883 * t4934 + t4902 * t4904 * t4928 - t4919;
        const Scalar t4922 = t4902 / t4904;
        const Scalar t4920 = 0.2e1 * t4922;
        const Scalar t4878 = 0.2e1 / 0.5e1 * t4880 * t4934 + t4883 * t4920 - t4919;
        const Scalar t4879 = 0.3e1 / 0.5e1 * t4880 * t4922 - t4919;
        const Scalar t4875 = -0.2e1 / 0.3e1 * t4879 * t4934 + t4878 * t4922 - t4919;
        const Scalar t4933 = t4875 / 0.2e1;
        const Scalar t4932 = t4878 / 0.4e1;
        const Scalar t4931 = t4879 / 0.3e1;
        const Scalar t4930 = t4880 / 0.5e1;
        const Scalar t4888 = t4937 * t4887;
        const Scalar t4891 = t4936 * t4935;
        const Scalar t4885 = fabs(t4895 * t4888 + b * t4891);
        const Scalar t4929 = t4885 / 0.5e1;
        const Scalar t4898 = w[3];
        const Scalar t4927 = t4898 / 0.2e1;
        const Scalar t4899 = w[2];
        const Scalar t4926 = t4899 / 0.3e1;
        const Scalar t4901 = w[0];
        const Scalar t4925 = t4901 / 0.5e1;
        const Scalar t4924 = t4893 * t4888;
        const Scalar t4921 = a * t4893 * t4887;
        const Scalar t4918 = t4904 * t4924;
        const Scalar t4917 = -t4924 / 0.4e1;
        const Scalar t4900 = w[1];
        const Scalar t4897 = w[4];
        const Scalar t4896 = w[5];
        const Scalar t4884 = (b * t4929 + t4917 + t4902 * t4891 / 0.4e1) * t4904;
        const Scalar t4882 = t4884 * t4920 - t4918;
        const Scalar t4881 = 0.2e1 * t4884 * t4934 + t4902 * a * t4929 - t4918;
        const Scalar t4877 = -t4882 * t4934 + t4881 * t4922 - t4918;
        const Scalar t49 = fabs(t4895 * t4887 + b * t4935);
        const Scalar t57 = fabs(t4895 * t4937 + b * t4936);
        const Scalar t61 = log(-t4895 / b);
        const Scalar t4876 = t4904 * t4917 + (b * t4877 - t4921 / 0.2e1 + (b * (t4902 * t49 / 0.3e1 - t4921) + b * t57 + t61) * t4905) * t4905;
        const Scalar t4874 = -0.2e1 * t4875 * t4934 + 0.5e1 / 0.3e1 * t4879 * t4922 - t4919;
        const Scalar t4873 = (t4901 * t4883 + t4900 * t4930 + t4899 * t4932 + t4898 * t4931 + t4897 * t4933 + t4896 * t4874) * t4905;
        res[0] = t4904 * t4885 * t4925 + (t4900 * t4884 + t4881 * t4926 + t4882 * t4927 + t4897 * t4877 + t4896 * t4876) * t4905;
        res[1] = d * t4873 + (t4901 * t4903 * t4928 + (t4900 * t4883 + t4899 * t4930 + t4898 * t4932 + t4897 * t4931 + t4896 * t4933) * t4905) * q;
        res[2] = q * t4873 + (t4880 * t4925 + t4900 * t4932 + t4879 * t4926 + t4875 * t4927 + t4897 * t4874 + (t4903 * t4916 + (b * t4874 + t4876) * t4905) * t4896) * d * t4905;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_cste<6> (Scalar a, Scalar b, Scalar c, Scalar l, Scalar (&res)[3])
    {
        const Scalar t4957 = a * l;
        const Scalar t2 = b * b;
        const Scalar t4942 = 0.1e1 / (a * c - t2);
        const Scalar t4956 = a * t4942;
        const Scalar t4943 = t4957 - b;
        const Scalar t4941 = 0.1e1 / (c + (-0.2e1 * b + t4957) * l);
        const Scalar t4946 = sqrt(t4941);
        const Scalar t4945 = 0.1e1 / c;
        const Scalar t4947 = sqrt(t4945);
        const Scalar t4948 = t4946 * t4941;
        const Scalar t4949 = t4941 * t4948;
        const Scalar t4951 = t4947 * t4945;
        const Scalar t4952 = t4945 * t4951;
        const Scalar t4955 = (0.4e1 / 0.3e1 * (0.2e1 * (t4943 * t4946 + b * t4947) * t4956 + t4943 * t4948 + b * t4951) * t4956 + t4943 * t4949 + b * t4952) * t4942;
        const Scalar t4940 = t4941 * t4949;
        const Scalar t4944 = t4945 * t4952;
        const Scalar t4954 = (0.6e1 / 0.5e1 * a * t4955 + t4943 * t4940 + b * t4944) * t4942 / 0.7e1;
        res[0] = t4955 / 0.5e1;
        res[1] = t4954;
        res[2] = (b * t4954 - t4940 / 0.7e1 + t4944 / 0.7e1) / a;

    }
    
    template<>
    inline void HomotheticCauchyInverse_segment_ScalisF_HornusGradF_on_line_cste<6> (Scalar a, Scalar b, Scalar l, Scalar (&res)[3]) 
    {
        const Scalar t4963 = fabs(b);
        const Scalar t4971 = t4963 * t4963;
        const Scalar t4 = t4971 * t4971;
        const Scalar t4983 = b / t4971 / t4963 / t4;
        const Scalar t4961 = a * l - b;
        const Scalar t4960 = fabs(t4961);
        const Scalar t4966 = t4960 * t4960;
        const Scalar t9 = t4966 * t4966;
        const Scalar t4982 = 0.1e1 / t4966 / t4960 / t9;
        const Scalar t4980 = 0.1e1 / t4971 * t4983;
        const Scalar t4959 = 0.1e1 / t4966 * t4982;
        const Scalar t15 = fabs(t4961 * t4959 + t4980);
        const Scalar t4979 = t15 / 0.8e1;
        const Scalar t16 = sqrt(a);
        const Scalar t17 = a * a;
        const Scalar t4977 = t16 * t17;
        const Scalar t4964 = a * t4977;
        const Scalar t20 = fabs(t4961 * t4982 + t4983);
        res[0] = t4977 * t20 / 0.6e1;
        res[1] = t4964 * t4979;
        const Scalar t22 = t4961 * t4961;
        res[2] = (-t22 * t4959 / 0.7e1 + (t4979 + t4980 / 0.7e1) * b) * t4964 / a;

    }
    
    



} // Close namespace HomotheticCauchyAndInverseOptim
} // Close namespace convol
} // Close namespace expressive

#endif // CONVOL_HOMOTHETIC_CAUCHY_AND_INVERSE_OPTIMIZED_FUNCTIONS_H_

