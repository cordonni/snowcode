
namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////

template<int DEGREE>
Scalar Inverse<DEGREE>::Eval(Scalar r) const
{
    return pow(r/scale_, -degree_r());
}
template<int DEGREE>
Scalar Inverse<DEGREE>::EvalGrad(Scalar r, Scalar /*epsilon*/) const
{
    return -degree_r()*scale_*pow(r/scale_, -(degree_r()+1.0));
}
template<int DEGREE>
Scalar Inverse<DEGREE>::InverseFct(Scalar y) const
{
    if(y>0.0)
    {
        return scale_ * pow(1.0/y,1.0/degree_r());
    }
    else
    {
        return expressive::kScalarMax;
    }
}


//-----------------------------------------------------------------------------
template<int DEGREE>
bool operator==(const Inverse<DEGREE>& a, const Inverse<DEGREE>& b)
{
    return ( a.scale() == b.scale() );
}



} // Close namespace convol

} // Close namespace expressive
