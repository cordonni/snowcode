#include<convol/functors/kernels/cauchyinverse/HornusCauchyAndInverseOptimizedFunctions.h>
#include<convol/functors/kernels/cauchyinverse/StandardCauchyAndInverseOptimizedFunctions.h>

namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////


//-------------------------------------------------------------------------
// Point evaluation -------------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar HornusCauchy<DEGREE>::Eval(const WeightedPoint& p_source, const Point& point) const
{
    const Scalar dist2 = (point-p_source).squaredNorm() / (p_source.weight()*p_source.weight());

    const Scalar aux = 1.0+this->inv_scale_2_*dist2;
    Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2>(aux);
    if(DEGREE%2 == 0) {
        return this->normalization_factor_0D_/(int_power_aux);
    } else {
        return this->normalization_factor_0D_/(int_power_aux*sqrt(aux));
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector HornusCauchy<DEGREE>::EvalGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon*/) const
{
    const Vector direction = point - p_source;
    const Scalar dist2 = direction.squaredNorm() / (p_source.weight()*p_source.weight());

    const Scalar aux = 1.0+this->inv_scale_2_*dist2;

    Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2+1>(aux);
    if(DEGREE%2 == 0) {
        return (-this->inv_scale_2_*this->degree_r()*this->normalization_factor_0D_/(int_power_aux)) * direction;
    } else {
        return (-this->inv_scale_2_*this->degree_r()*this->normalization_factor_0D_/(int_power_aux*sqrt(aux))) * direction;
    }
}
//-------------------------------------------------------------------------
template<int DEGREE>
void HornusCauchy<DEGREE>::EvalValueAndGrad(const WeightedPoint& p_source, const Point& point, Scalar /*epsilon_grad*/,
                                   Scalar& value_res, Vector& grad_res) const
{
    const Vector direction = point - p_source;
    const Scalar dist2 = direction.squaredNorm() / (p_source.weight()*p_source.weight());

    const Scalar aux = 1.0+this->inv_scale_2_*dist2;

    Scalar int_power_aux = kernelHelpFunction::IntPow<DEGREE/2>(aux);
    if(DEGREE%2 == 0) {
        value_res = this->normalization_factor_0D_/(int_power_aux);
        grad_res =  (-this->inv_scale_2_*this->degree_r()*this->normalization_factor_0D_/(int_power_aux*aux)) * direction;
    } else {
        value_res = this->normalization_factor_0D_/(int_power_aux*sqrt(aux));
        grad_res =  (-this->inv_scale_2_*this->degree_r()*this->normalization_factor_0D_/(int_power_aux*aux*sqrt(aux))) * direction;
    }
}
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
// Segment evaluation -----------------------------------------------------
//-------------------------------------------------------------------------
template<int DEGREE>
Scalar HornusCauchy<DEGREE>::Eval(const OptWeightedSegment& seg, const Point& point) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = seg.length()*seg.length() * (this->inv_scale_2_ + seg.unit_delta_weight()*seg.unit_delta_weight());
    const Scalar b = seg.length()* (this->inv_scale_2_* seg.increase_unit_dir().dot(p_min_to_point) - seg.unit_delta_weight()*seg.weight_min());
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm() + seg.weight_min()*seg.weight_min();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE+1];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE+1>(special_coeff, seg.weight_min(), delta_w);

    return  this->normalization_factor_1D_ * seg.length() * HornusCauchyAndInverseOptim::HornusCauchyInverse_segment_F<DEGREE>(a,b,c,special_coeff);
}
//-------------------------------------------------------------------------
template<int DEGREE>
Vector HornusCauchy<DEGREE>::EvalGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon*/) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = seg.length()*seg.length() * (this->inv_scale_2_ + seg.unit_delta_weight()*seg.unit_delta_weight());
    const Scalar b = seg.length()* (this->inv_scale_2_* seg.increase_unit_dir().dot(p_min_to_point) - seg.unit_delta_weight()*seg.weight_min());
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm() + seg.weight_min()*seg.weight_min();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE+1];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE+1>(special_coeff, seg.weight_min(), delta_w);

    Scalar F0F1[2];
    HornusCauchyAndInverseOptim::HornusCauchyInverse_segment_GradF<DEGREE>(a,b,c,special_coeff, F0F1);

    const Scalar factor = seg.length()*this->degree_r()*this->inv_scale_2_;
    return this->normalization_factor_1D_ * ((factor*F0F1[1]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1[0]) * p_min_to_point);
}
//-------------------------------------------------------------------------
template<int DEGREE>
void HornusCauchy<DEGREE>::EvalValueAndGrad(const OptWeightedSegment& seg, const Point& point, Scalar /*epsilon_grad*/,
                                   Scalar& value_res, Vector& grad_res) const
{
    const Vector p_min_to_point = (point-seg.p_min());
    const Scalar a = seg.length()*seg.length() * (this->inv_scale_2_ + seg.unit_delta_weight()*seg.unit_delta_weight());
    const Scalar b = seg.length()* (this->inv_scale_2_* seg.increase_unit_dir().dot(p_min_to_point) - seg.unit_delta_weight()*seg.weight_min());
    const Scalar c = this->inv_scale_2_*p_min_to_point.squaredNorm() + seg.weight_min()*seg.weight_min();

    // Compute the new polynomial weight (see formulas in the ref)
    Scalar delta_w = seg.unit_delta_weight() * seg.length();
    Scalar special_coeff[DEGREE+1];
    kernelHelpFunction::ExpandWeightPolynome<DEGREE+1>(special_coeff, seg.weight_min(), delta_w);

    Scalar F0F1F2[3];
    HornusCauchyAndInverseOptim::HornusCauchyInverse_segment_FGradF<DEGREE>(a,b,c,special_coeff, F0F1F2);

    value_res = this->normalization_factor_1D_* seg.length() * F0F1F2[0];
    const Scalar factor = seg.length() * this->degree_r() *this->inv_scale_2_;
    grad_res = this->normalization_factor_1D_ * ((factor*F0F1F2[2]*seg.length()) * seg.increase_unit_dir() - (factor*F0F1F2[1]) * p_min_to_point);
}
//-------------------------------------------------------------------------



//////////////////////////
/// Bounding functions ///
//////////////////////////

template <int DEGREE>
Scalar HornusCauchy<DEGREE>::GetIsoValueForThickness(Scalar length, Scalar thickness) const
{
    std::vector<Scalar> weight;
    weight.push_back(1.0);

    const Scalar a = this->inv_scale_2_*length*length;
    const Scalar b = a*0.5;
    const Scalar c = 1.0 + this->inv_scale_2_*thickness*thickness + a*0.25;

    return this->normalization_factor_1D_*length*StandardCauchyAndInverseOptim::StandardCauchyInverse_segment_F<DEGREE,0>(a,b,c,weight);
}


} // Close namespace convol

} // Close namespace expressive
