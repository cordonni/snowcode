
#define TEMPLATE_INSTANTIATION_HOMOTHETIC_DISTANCE


#include <convol/functors/kernels/HomotheticDistanceT.h>

#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

#include <convol/functors/kernels/cauchyinverse/Cauchy.h>
#include <convol/functors/kernels/cauchyinverse/Inverse.h>
#include <convol/functors/kernels/compactpolynomial/CompactPolynomial.h>

namespace expressive {

namespace convol {

void AbstractHomotheticDistance::AddXmlAttributes(TiXmlElement *element) const
{
    Kernel::AddXmlAttributes(element);
    TiXmlElement *child = new TiXmlElement("TFunctorKernel");
    TiXmlElement *fxml = functor_profil().ToXml();
    child->LinkEndChild(fxml);
    element->LinkEndChild(child);
}



static core::FunctorFactory::Type<HomotheticDistanceT<CompactPolynomial<6> > > HomotheticDistanceT_CompactPolynomial6_Type;

// explicit instantiation
template class HomotheticDistanceT<CompactPolynomial<6> >;

} // end namespace convol
} // end namespace expressive
