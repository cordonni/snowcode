/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: AnalyticalConvol.h

   Language: C++

   License: Convol Licence

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Header file for a semi Numerical Convolution (only over triangles).
                

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_SEMI_NUMERICAL_HOMOTHETIC_CONVOL_H_
#define CONVOL_SEMI_NUMERICAL_HOMOTHETIC_CONVOL_H_

// core dependencies
#include <core/CoreRequired.h>
    #include<core/functor/MACRO_FUNCTOR_TYPE.h>
    // Geometry
    #include<core/geometry/OptWeightedTriangle.h>

// convol dependencies
    // Functors
    #include <convol/functors/kernels/Kernel.h>

//#include<Convol/include/ConvolDefaultDef.h>
//    #include<Convol/include/functors/EnumFunctorType.h>
//    // Geometry
//    #include<Convol/include/geometry/PolyWeightedSegmentT.h>
//    #include<Convol/include/geometry/WeightedTriangleT.h>
//    // Tools
//    #include<Convol/include/tools/GradientEvaluationToolsT.h>

//// std dependencies
//#include <string>
//#include <sstream>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/Xml/tinyxml/tinyxml.h>

namespace expressive {

namespace convol {

class CONVOL_API AbstractSemiNumericalHomotheticConvol :
    public Kernel
{
public:
    EXPRESSIVE_MACRO_NAME("SemiNumericalHomotheticConvol")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractSemiNumericalHomotheticConvol() : N_(10), scalar_N_(10.0) {}
    //-------------------------------------------------------------------------
    // N must be even, if it is not, N+1 will be used.
    AbstractSemiNumericalHomotheticConvol(unsigned int N)
    { 
        set_N(N);
//        if(N & 1){
//            N_ = N+1;
//            scalar_N_ = (Scalar) (N+1);
//        }else{
//            N_ = N;
//            scalar_N_ = (Scalar) N;
//        }
    }    
    //-------------------------------------------------------------------------
    AbstractSemiNumericalHomotheticConvol(const AbstractSemiNumericalHomotheticConvol& rhs)
        : Kernel(rhs), N_(rhs.N_), scalar_N_(rhs.scalar_N_) {}

    //-------------------------------------------------------------------------
    virtual ~AbstractSemiNumericalHomotheticConvol(){}

//    virtual std::string FunctorName() const = 0;
//    virtual std::string ParameterName() const ;

    //////////////////////
    /// Type functions ///
    //////////////////////

    virtual const core::FunctorTypeTree& Type() const = 0;

//    ///////////////////
//    // Xml functions //
//    ///////////////////

//    virtual TiXmlElement * GetXml() const = 0;
//    inline void AddXmlSimpleAttribute(TiXmlElement * element) const ;
//    inline void AddXmlComplexAttribute(TiXmlElement * element) const ;

    void InitFromXml(const TiXmlElement *element);

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool isCompact() const = 0;

    /////////////////
    /// Modifiers ///
    /////////////////

    inline void set_N (unsigned int N)
    {
        if(N & 1){
            N_ = N+1;
            scalar_N_ = (Scalar) (N+1);
        }else{
            N_ = N;
            scalar_N_ = (Scalar) N;
        }
    }

//    virtual void set_functor_point_contribution(const Functor& functor) = 0;

    /////////////////
    /// Accessors ///
    /////////////////

    inline unsigned int N() const { return N_; }
    virtual const Functor& functor_point_contribution() const = 0;


    virtual void AddXmlAttributes(TiXmlElement *element) const
    {
        TiXmlElement *child = new TiXmlElement("TFunctorPointContribution");
        TiXmlElement *fxml = functor_point_contribution().ToXml();
        child->LinkEndChild(fxml);
        element->LinkEndChild(child);

        element->SetAttribute("N", N_);
    }

protected:
    unsigned int N_;    // N_ is the number of segment to be used in an integral of length 1 and radius 1
    Scalar scalar_N_;   // For optimization purpose : avoid conversion
};


template<typename TFunctorPointContribution>
class SemiNumericalHomotheticConvolT :
    public AbstractSemiNumericalHomotheticConvol
{
public:

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////
    SemiNumericalHomotheticConvolT() : AbstractSemiNumericalHomotheticConvol(10) { }
    //-------------------------------------------------------------------------
    SemiNumericalHomotheticConvolT(unsigned int N) : AbstractSemiNumericalHomotheticConvol(N) { }
    //-------------------------------------------------------------------------
    SemiNumericalHomotheticConvolT(unsigned int N, const TFunctorPointContribution& ker)
        : AbstractSemiNumericalHomotheticConvol(N), point_contribution_(ker)    { }
    //-------------------------------------------------------------------------
    SemiNumericalHomotheticConvolT(const SemiNumericalHomotheticConvolT& rhs)
        : AbstractSemiNumericalHomotheticConvol(rhs), point_contribution_(rhs.point_contribution_) {}

    virtual ~SemiNumericalHomotheticConvolT(){}

//    ////////////////////
//    // Name functions //
//    ////////////////////

//    virtual std::string FunctorName() const ;

//    virtual std::string ParameterName() const;

    //////////////////////
    /// Type functions ///
    //////////////////////

    static core::FunctorTypeTree BuildStaticType();

    //-------------------------------------------------------------------------
    CONVOL_MACRO_FUNCTOR_TYPE // This macro defined functions :
    //-------------------------------------------------------------------------
    // static const FunctorTypeTree& StaticType();
    //-------------------------------------------------------------------------
    // virtual const FunctorTypeTree& Type() const;

    static const std::vector<std::string>& StaticSubFunctorXmlName()
    {
        static std::vector<std::string> subfunctor_xml_names = { std::string("TFunctorPointContribution") };
        return subfunctor_xml_names;
    }

//    ///////////////////
//    // Xml functions //
//    ///////////////////
    
//    TiXmlElement * GetXml() const;

//    void AddXmlFunctor(TiXmlElement * element) const;

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    Scalar Eval(const core::OptWeightedTriangle& tri, const Point& p) const ;

    Scalar operator() (const core::OptWeightedTriangle& tri, const Point& p) const
    {
        return Eval(tri, p);
    }
    Vector EvalGrad(const core::OptWeightedTriangle& tri, const Point& point, Scalar epsilon) const
    {
        Scalar value_res;
        Vector grad_res;
        EvalValueAndGrad(tri, point, epsilon, value_res, grad_res);
        return grad_res;
    }
    inline void EvalValueAndGrad(const core::OptWeightedTriangle& tri, const Point& point, Scalar epsilon_grad,
                                 Scalar& value_res, Vector& grad_res) const ;

    inline void EvalHomotheticValues(const core::OptWeightedTriangle& tri, const Point& point,
                                     Scalar& homothetic_value_res, Vector& homothetic_grad_res) const;


    // Functions used for the numerical integration

    // Function performing a 1D integration along the direction of constant weight of the triangle
    inline Scalar ComputeLineIntegral(const core::OptWeightedTriangle& tri, Scalar t, const Point& p) const;
    inline void ComputeValueAndGradLineIntegral(const core::OptWeightedTriangle& tri, Scalar t, const Point& p, Scalar epsilon, Scalar& field_res, Vector& grad_res) const;
    inline void ComputeHomotheticValueLineIntegral(const core::OptWeightedTriangle& tri, Scalar t, const Point& p, Scalar& field_res, Vector& grad_res) const;

    // Sub function for numerical computation
    inline Scalar WarpAbscissa   (const core::OptWeightedTriangle& tri, Scalar t) const;
    inline Scalar UnwarpAbscissa (const core::OptWeightedTriangle& tri, Scalar t) const;


    inline bool ComputeTParam(const core::OptWeightedTriangle& tri, const Point& point, Scalar& t_low, Scalar& t_high, Scalar& t_closest) const;

////     ecrire fonction de calcul de la distance �  un edge (dans l'espace déformé)
////     et renvoyant aussi la coordonné le long de main axis
////     parametre :
////     Vector orig_to_point
////     unit_dir, longueur
////     poids �  l'origine, variation de poids
//    inline void ComputeWarpedDistAndParam(const Point& point,
//                                          const Point& origin, const Normal& unit_dir,
//                                          const Scalar weight_orig, const Scalar delta_weight, const Scalar length,
//                                          Scalar& sqr_warped_dist, Scalar& t_param) const ;


    ////////////////////////
    // Bounding functions //
    ////////////////////////

    virtual bool isCompact() const { return point_contribution_.isCompact(); }

    Scalar GetEuclidDistBound (const core::WeightedTriangle& tri, Scalar value) const
    {
        return point_contribution_.GetEuclidDistBound(tri, value);
    }
	
    Scalar GetIsoValueForThicknessGeom2D(Scalar thickness) const
    {
        return point_contribution_.GetIsoValueForThicknessGeom2D(thickness);
    }


    ///////////////
    // Modifiers //
    ///////////////

//    virtual void set_functor_point_contribution(const Functor& functor);

    ///////////////
    // Accessors //
    ///////////////

    virtual const Functor& functor_point_contribution() const { return point_contribution_; }
    const TFunctorPointContribution& point_contribution() const  { return point_contribution_; }
    TFunctorPointContribution& point_contribution() { return point_contribution_; }


private:
    TFunctorPointContribution point_contribution_;

};

//-----------------------------------------------------------------------------
template<typename TFunctorPointContribution>
bool operator==(const SemiNumericalHomotheticConvolT<TFunctorPointContribution>& a, const SemiNumericalHomotheticConvolT<TFunctorPointContribution>& b)
{
    return (a.N() == b.N()) && (a.point_contribution() == b.point_contribution());
}

} // Close namespace convol

} // Close namespace expressive

// implementation file
#include <convol/functors/kernels/SemiNumericalHomotheticConvolT.hpp>

#endif // CONVOL_SEMI_NUMERICAL_HOMOTHETIC_CONVOL_H
