
//#include<core/CoreRequired.h>

#include <core/geometry/OptWeightedSegment.h>

// convol dependencies
    // Functors
//    #include<convol/functors/kernels/KernelT.h>
//    #include<convol/functors/EnumFunctorType.h>
    // Geometry
//    #include<Convol/include/geometry/PolyWeightedSegmentT.h>
//    #include<Convol/include/geometry/WeightedTriangleT.h>
    // Tools
//    #include<convol/tools/GradientEvaluationTools.h>

//// std dependencies
//#include <string>
//#include <sstream>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/Xml/tinyxml/tinyxml.h>

namespace expressive {

namespace convol {

////////////////////////////////////////////////////
/// Class AbstractSemiNumericalHomotheticConvolT ///
////////////////////////////////////////////////////

//////////////////////
//// Name functions //
//////////////////////

//template<typename TTraits>
//std::string AbstractSemiNumericalHomotheticConvolT<TTraits>::ParameterName() const
//{
//    std::ostringstream name;
//    name << "[N(" << N_ << ")]";
//    return name.str();
//}

/////////////////////
//// Xml functions //
/////////////////////

//template<typename TTraits>
//inline void AbstractSemiNumericalHomotheticConvolT<TTraits>::AddXmlSimpleAttribute(TiXmlElement * element) const
//{
//    Base::AddXmlSimpleAttribute(element);
//    element->SetAttribute("N",N_);
//}

//template<typename TTraits>
//inline void AbstractSemiNumericalHomotheticConvolT<TTraits>::AddXmlComplexAttribute(TiXmlElement * element) const
//{
//    Base::AddXmlComplexAttribute(element);
//}


////////////////////////////////////////////
/// Class SemiNumericalHomotheticConvolT ///
////////////////////////////////////////////

//////////////////////
//// Name functions //
//////////////////////

//template<typename TFunctorPointContribution>
//std::string SemiNumericalHomotheticConvolT<TFunctorPointContribution>::FunctorName() const
//{
//    std::ostringstream name;
//    name << "<" << point_contribution_.Name() << ">";
//    return name.str();
//}
//template<typename TFunctorPointContribution>
//std::string SemiNumericalHomotheticConvolT<TFunctorPointContribution>::ParameterName() const
//{
//    std::ostringstream name;
//    name << "[N(" << this->N_ << ")]";
//    return name.str();
//}

//////////////////////
/// Type functions ///
//////////////////////

template<typename TFunctorPointContribution>
core::FunctorTypeTree SemiNumericalHomotheticConvolT<TFunctorPointContribution>::BuildStaticType()
{
    std::vector<core::FunctorTypeTree> vec(1,TFunctorPointContribution::BuildStaticType());
    return core::FunctorTypeTree("SemiNumericalHomotheticConvol", vec);
}

/////////////////////
//// Xml functions //
/////////////////////

//template<typename TFunctorPointContribution>
//TiXmlElement * SemiNumericalHomotheticConvolT<TFunctorPointContribution>::GetXml() const
//{
//    TiXmlElement *element_base = new TiXmlElement( this->StaticName() );

//    Base::AddXmlSimpleAttribute(element_base);
//    Base::AddXmlComplexAttribute(element_base);
//    AddXmlFunctor(element_base);

//    return element_base;
//}

//template<typename TFunctorPointContribution>
//void SemiNumericalHomotheticConvolT<TFunctorPointContribution>::AddXmlFunctor(TiXmlElement * element) const
//{
//    TiXmlElement* element_eval_functor, *element_child;

//    element_eval_functor = new TiXmlElement( "TFunctorPointContribution" );
//    element->LinkEndChild(element_eval_functor);

//    element_child = point_contribution_.GetXml();
//    element_eval_functor->LinkEndChild(element_child);
//}

////////////////////////////
/// Evaluation functions ///
////////////////////////////

//TODO:@todo : an idea to improve the quality of the integration while using fewer sample would be
// to place one of them at the projection of the computation point on the triangle in "homothetic" space.

template<typename TFunctorPointContribution>
auto SemiNumericalHomotheticConvolT<TFunctorPointContribution>::WarpAbscissa (const core::OptWeightedTriangle& tri, Scalar t) const -> Scalar
{
    // Compute approx of ln(d*l+1)/d
    const Scalar dt = t * tri.unit_delta_weight();
    const Scalar inv_dtp2 = 1.0 / (dt + 2.0);
    Scalar sqr_dt_divdlp2 = dt * inv_dtp2;
    sqr_dt_divdlp2 *= sqr_dt_divdlp2;
    const Scalar serie_approx = 1.0 + sqr_dt_divdlp2*(
                                   (1.0/3.0) + sqr_dt_divdlp2*(
                                        (1.0/5.0) + sqr_dt_divdlp2*(
                                            (1.0/7.0) + sqr_dt_divdlp2*(
                                                (1.0/9.0) + sqr_dt_divdlp2*(
                                                    (1.0/11.0) + sqr_dt_divdlp2*(1.0/13.0) )))));
    return 2.0 * t * inv_dtp2 * serie_approx;
}
template<typename TFunctorPointContribution>
auto SemiNumericalHomotheticConvolT<TFunctorPointContribution>::UnwarpAbscissa (const core::OptWeightedTriangle& tri, Scalar t) const -> Scalar
{
    // Compute approx of (exp(d*l)-1)/d
    const Scalar dt = t * tri.unit_delta_weight();
    return t * ( 1.0 + dt *( 1.0/2.0 + dt * ( 1.0/6.0 + dt * ( 1.0/24.0 + dt * ( 1.0/120.0 + dt * 1.0/720.0 ))))) ;
}


template<typename TFunctorPointContribution>
auto SemiNumericalHomotheticConvolT<TFunctorPointContribution>::ComputeLineIntegral(const core::OptWeightedTriangle& tri, Scalar t, const Point& p) const -> Scalar
{
    Scalar weight = tri.weight_min() + t * tri.unit_delta_weight();

    Point p_1 = tri.point_min() + t * tri.longest_dir_special();

    Scalar length = (t<tri.coord_middle()) ? (t/tri.coord_middle()) * tri.max_seg_length()
                                           : ((tri.coord_max()-t)/(tri.coord_max() - tri.coord_middle())) * tri.max_seg_length();

    return point_contribution_.Eval( p_1, weight, tri.ortho_dir(), length, p);
}


template<typename TFunctorPointContribution>
void SemiNumericalHomotheticConvolT<TFunctorPointContribution>::ComputeValueAndGradLineIntegral(const core::OptWeightedTriangle& tri, Scalar t, const Point& p, Scalar epsilon, Scalar& field_res, Vector& grad_res) const
{
    Scalar weight = tri.weight_min() + t * tri.unit_delta_weight();

    Point p_1 = tri.point_min() + t * tri.longest_dir_special();

    Scalar length = (t<tri.coord_middle()) ? (t/tri.coord_middle()) * tri.max_seg_length()
                                           : ((tri.coord_max()-t)/(tri.coord_max() - tri.coord_middle())) * tri.max_seg_length();

    point_contribution_.EvalValueAndGrad( p_1, weight, tri.ortho_dir(), length,
                                          p, epsilon, field_res, grad_res) ;
}


template<typename TFunctorPointContribution>
auto SemiNumericalHomotheticConvolT<TFunctorPointContribution>::Eval(const core::OptWeightedTriangle& tri, const Point& p) const -> Scalar
{
    // Compute closest point (t parameter) on the triangle in "warped space" as well as clipping
    Scalar t_low, t_high, t_closest;
    if( ComputeTParam(tri, p, t_low, t_high, t_closest) )
    {
        // Compute local warp coordinates
        Scalar w_local = tri.weight_min() + t_low*tri.unit_delta_weight();
        Scalar local_t_max = WarpAbscissa(tri, (t_high-t_low) / w_local );

        // Compute the required number of sample
        unsigned int nb_samples = 2 * ((unsigned int)(0.5*this->scalar_N_*local_t_max+1.0));
        Scalar d_step_size = local_t_max  / ((Scalar)nb_samples);

        // Perform Simpson scheme

        Scalar t = d_step_size;
        d_step_size *= 2.0;
        Scalar res_odd = 0.0;
        for(unsigned int i = 1; i < nb_samples; i+=2)
        {
            res_odd += ComputeLineIntegral(tri,  UnwarpAbscissa(tri, t) * w_local + t_low, p);
            t += d_step_size ;
        }

        Scalar res_even = 0.0;
        t = 0.0;
        for(unsigned int i = 2; i < nb_samples; i+=2)
        {
            t += d_step_size ;
            res_even += ComputeLineIntegral(tri,  UnwarpAbscissa(tri, t) * w_local + t_low, p);
        }

        Scalar res = ComputeLineIntegral(tri, t_low, p) + 4.0*res_odd + 2.0*res_even + ComputeLineIntegral(tri, t_high, p);
        Scalar factor = ( local_t_max /(3.0*((Scalar)nb_samples)) ) * point_contribution_.normalization_factor_2D();
        return res *factor;

    }
    else
    {
        return 0.0;
    }
}

template<typename TFunctorPointContribution>
void SemiNumericalHomotheticConvolT<TFunctorPointContribution>::EvalValueAndGrad(const core::OptWeightedTriangle& tri, const Point& point, Scalar epsilon_grad,
                             Scalar& value_res, Vector& grad_res) const
{
    // Compute closest point (t parameter) on the triangle in "warped space" as well as clipping
    Scalar t_low, t_high, t_closest;
    if( ComputeTParam(tri, point, t_low, t_high, t_closest) )
    {
        // Compute local warp coordinates
        Scalar w_local = tri.weight_min() + t_low*tri.unit_delta_weight();
        Scalar local_t_max = WarpAbscissa(tri, (t_high-t_low) / w_local );

        // Compute the required number of sample
        unsigned int nb_samples = 2 * ((unsigned int)(0.5*this->scalar_N_*local_t_max+1.0));
        Scalar d_step_size = local_t_max  / ((Scalar)nb_samples);

        // Perform Simpson scheme

        Scalar field_tmp;
        Vector grad_tmp;

        Scalar value_odd = 0.0;
        Vector grad_odd = Vector::Zero();
        Scalar t = d_step_size;
        d_step_size *= 2.0;
        for(unsigned int i = 1; i < nb_samples; i+=2)
        {
            ComputeValueAndGradLineIntegral( tri, UnwarpAbscissa(tri, t) * w_local + t_low,
                                             point, epsilon_grad, field_tmp, grad_tmp);
            value_odd += field_tmp;
            grad_odd += grad_tmp;

            t += d_step_size ;
        }

        Scalar value_even = 0.0;
        Vector grad_even = Vector::Zero();
        t = 0.0;
        for(unsigned int i = 2; i < nb_samples; i+=2)
        {
            t += d_step_size ;

            ComputeValueAndGradLineIntegral( tri, UnwarpAbscissa(tri, t) * w_local + t_low,
                                             point, epsilon_grad, field_tmp, grad_tmp);
            value_even += field_tmp;
            grad_even += grad_tmp;
        }

        Scalar field_low, field_high; Vector grad_low, grad_high;
        ComputeValueAndGradLineIntegral(tri, t_low, point, epsilon_grad, field_low, grad_low);
        ComputeValueAndGradLineIntegral(tri, t_high, point, epsilon_grad, field_high, grad_high);


        value_res = field_low + 4.0*value_odd + 2.0*value_even + field_high;
        grad_res  = grad_low + 4.0*grad_odd + 2.0*grad_even + grad_high;

        Scalar factor = (local_t_max /(3.0*((Scalar)nb_samples)) ) * point_contribution_.normalization_factor_2D();
        value_res *= factor;
        grad_res  *= factor;
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}







template<typename TFunctorPointContribution>
bool  SemiNumericalHomotheticConvolT<TFunctorPointContribution>::ComputeTParam(const core::OptWeightedTriangle& tri,
                                                                               const Point& point, Scalar& t_low, Scalar& t_high, Scalar& /*t_closest*/) const
{
    // Compute projection (in warped space) on the plane containing the triangle
    const Vector pmin_to_p(point - tri.point_min());

    const Scalar coord_main_dir = pmin_to_p.dot(tri.main_dir());
    const Scalar coord_normal = pmin_to_p.dot(tri.unit_normal());
    //        const Scalar coord_ortho_dir = pmax_to_p.dot(ortho_dir_);

    if(point_contribution_.isCompact())
    {   //WARNING : Assume that the compact support is defined in the same way as HomotheticCompactPolynomial kernels
        const Scalar dist_sqr = coord_main_dir*coord_main_dir + coord_normal*coord_normal;

        const Scalar inv_scale_2_ = 1.0/(point_contribution_.scale()*point_contribution_.scale());
        Scalar special_coeff[3] = { tri.weight_min()*tri.weight_min()            - inv_scale_2_ * dist_sqr ,
                                    - tri.unit_delta_weight()*tri.weight_min()    - inv_scale_2_ * coord_main_dir ,
                                    tri.unit_delta_weight()*tri.unit_delta_weight() - inv_scale_2_ };

        Scalar clipped_l1, clipped_l2;
        if( core::OptWeightedSegment::HomotheticClippingSpecial(special_coeff, tri.coord_max(), clipped_l1, clipped_l2) )
        {
            t_low = clipped_l1;
            t_high = clipped_l2;

            return true;
            //TODO:@todo: try to perform a clipping in the orthogonal direction
        }
        else
        {
            return false;
        }

    }
    else
    {
        t_low = 0.0;
        t_high = tri.coord_max();
    }

    return true;
}





template<typename TFunctorPointContribution>
void SemiNumericalHomotheticConvolT<TFunctorPointContribution>::ComputeHomotheticValueLineIntegral(const core::OptWeightedTriangle& tri, Scalar t, const Point& p, Scalar& field_res, Vector& grad_res) const
{
    Scalar weight = tri.weight_min() + t * tri.unit_delta_weight();

    Point p_1 = tri.point_min() + t * tri.longest_dir_special();

    Scalar length = (t<tri.coord_middle()) ? (t/tri.coord_middle()) * tri.max_seg_length()
                                           : ((tri.coord_max()-t)/(tri.coord_max() - tri.coord_middle())) * tri.max_seg_length();

    //TODO:@todo
    point_contribution_.EvalHomotheticValues( p_1, weight, tri.ortho_dir(), length,
                                              p, field_res, grad_res) ;
}
template<typename TFunctorPointContribution>
void SemiNumericalHomotheticConvolT<TFunctorPointContribution>::EvalHomotheticValues(const core::OptWeightedTriangle& tri, const Point& point,
                                 Scalar& homothetic_value_res, Vector& homothetic_grad_res) const
{
    // Compute closest point (t parameter) on the triangle in "warped space" as well as clipping
    Scalar t_low, t_high, t_closest;
    if( ComputeTParam(tri, point, t_low, t_high, t_closest) )
    {
        // Compute local warp coordinates
        Scalar w_local = tri.weight_min() + t_low*tri.unit_delta_weight();
        Scalar local_t_max = WarpAbscissa(tri, (t_high-t_low) / w_local );

        // Compute the required number of sample
        unsigned int nb_samples = 2 * ((unsigned int)(0.5*this->scalar_N_*local_t_max+1.0));
        Scalar d_step_size = local_t_max  / ((Scalar)nb_samples);

        // Perform Simpson scheme

        Scalar field_tmp;
        Vector grad_tmp;

        Scalar value_odd = 0.0;
        Vector grad_odd = Vector::Zero();
        Scalar t = d_step_size;
        d_step_size *= 2.0;
        for(unsigned int i = 1; i < nb_samples; i+=2)
        {
            ComputeHomotheticValueLineIntegral( tri, UnwarpAbscissa(tri, t) * w_local + t_low,
                                                point, field_tmp, grad_tmp);
            value_odd += field_tmp;
            grad_odd += grad_tmp;

            t += d_step_size ;
        }

        Scalar value_even = 0.0;
        Vector grad_even = Vector::Zero();
        t = 0.0;
        for(unsigned int i = 2; i < nb_samples; i+=2)
        {
            t += d_step_size ;

            ComputeHomotheticValueLineIntegral( tri, UnwarpAbscissa(tri, t) * w_local + t_low,
                                             point, field_tmp, grad_tmp);
            value_even += field_tmp;
            grad_even += grad_tmp;
        }

        Scalar field_low, field_high; Vector grad_low, grad_high;
        ComputeHomotheticValueLineIntegral(tri, t_low, point,  field_low, grad_low);
        ComputeHomotheticValueLineIntegral(tri, t_high, point, field_high, grad_high);


        homothetic_value_res = field_low + 4.0*value_odd + 2.0*value_even + field_high;
        homothetic_grad_res  = grad_low + 4.0*grad_odd + 2.0*grad_even + grad_high;

        Scalar factor = (local_t_max /(3.0*((Scalar)nb_samples)) ) * point_contribution_.normalization_factor_2D_homothetic();
        homothetic_value_res *= factor;
        homothetic_grad_res  *= factor;
    }
    else
    {
        homothetic_value_res = 0.0;
        homothetic_grad_res = Vector::Zero();
    }
}


//template<typename TFunctorPointContribution>
//void SemiNumericalHomotheticConvolT<TFunctorPointContribution>::ComputeWarpedDistAndParam(const Point& point,
//                                      const Point& origin, const Normal& unit_dir,
//                                      const Scalar weight_orig, const Scalar delta_weight, const Scalar length,
//                                      Scalar& sqr_warped_dist, Scalar& t_param) const
//{
//    // Compute projection (in warped space) on the segment
//    const Vector origin_to_p = point - origin;

//    const Scalar p_x = (origin_to_p | unit_dir);
//    const Scalar sqr_p_y = origin_to_p.sqrnorm() - p_x*p_x;

//    const Scalar denum = weight_orig * length + delta_weight * p_x;
//    Scalar t = 0.0;
//    if(denum>0.0)
//    {
//        t = p_x + delta_weight * sqr_p_y / denum;
//        t = (t<0.0) ? 0.0 : ((t>length) ? length : t) ; // clipping (nearest point on segment not line)
//    }
//    t_param = t/length;

//    const Point proj_to_point = origin + t*unit_dir - point;
//    const Scalar weight = weight_orig + t_param*delta_weight;
//    sqr_warped_dist = proj_to_point.sqrnorm() / (weight*weight);
//}

///////////////
// Modifiers //
///////////////

//template<typename TFunctorPointContribution>
//void SemiNumericalHomotheticConvolT<TFunctorPointContribution>::set_functor_point_contribution(const Functor& functor)
//{
//    const TFunctorPointContribution* spec_point_contribution = dynamic_cast<const TFunctorPointContribution*>(&functor);
//    if(spec_point_contribution != nullptr){// The functor given must have the good class.
//        point_contribution_ = *spec_point_contribution;
//    }else{
//        assert(false);
//    }
//}

} // Close namespace convol

} // Close namespace expressive

//// CODE DEPRECATED : used to find closest point to computation point : use old parametrization of the triangle, should be updated if needed
//    // Compute projection (in warped space) on the plane containing the triangle
//    const Vector pmax_to_p = point - point_max_;

//    const Scalar coord_main_dir = (pmax_to_p | main_dir_);
//    const Scalar coord_normal = (pmax_to_p | normal_);
////        const Scalar coord_ortho_dir = (pmax_to_p | ortho_dir_);

//    const Scalar sqr_coord_normal = coord_normal*coord_normal;
//    const Scalar denum = weight_max_ * coord_max_ + delta_weight_ * coord_main_dir;
//    Scalar t_param = 0.0;
//    if(denum>0.0)
//    {
//        t_param = coord_main_dir + delta_weight_ * sqr_coord_normal / denum;
//        t_param /= coord_max_;
//    }
//    return (t_param<0.0) ? 0.0 : ( (t_param>1.0) ? 1.0 : t_param );


//    //        // Compute projection (in warped space) on the plane containing the triangle
//    //        const Vector pmax_to_p = point - point_max_;

//    //        const Scalar coord_main_dir = (pmax_to_p | main_dir_);
//    //        const Scalar coord_normal = (pmax_to_p | normal_);
//    //        const Scalar coord_ortho_dir = (pmax_to_p | ortho_dir_);

//    //        const Scalar sqr_coord_normal = coord_normal*coord_normal;
//    //        const Scalar denum = weight_max_ * coord_max_ + delta_weight_ * coord_main_dir;
//    //        Scalar t_param = 0.0;
//    //        if(denum>0.0)
//    //        {
//    //            t_param = coord_main_dir + delta_weight_ * sqr_coord_normal / denum;
//    //            t_param /= coord_max_;
//    //        }

//    //        const Point proj = point_max_ + (t_param*coord_max_)*main_dir_ + coord_ortho_dir*ortho_dir_;

//    //        // check if projected point is inside the triangle ...

//    //        Vector unit_longest_dir = longest_dir_; unit_longest_dir.normalize();
//    //        Vector unit_half_dir_1 = half_dir_1_; unit_half_dir_1.normalize();
//    //        Vector unit_half_dir_2 = half_dir_2_; unit_half_dir_2.normalize();

//    //        // this is done in a ugly way ...
//    //        const Point pmax_to_proj = proj - point_max_;
//    //        const Vector dir_middle = 0.5*(longest_dir_+ half_dir_1_);
//    //        Scalar sign = (((unit_longest_dir % dir_middle) | normal_) < 0.0) ? -1.0 : 1.0;
//    //        if( sign*((unit_longest_dir % pmax_to_proj) | normal_) > 0.0
//    //            && sign*((unit_half_dir_1 % pmax_to_proj) | normal_) < 0.0
//    //            && sign*((unit_half_dir_2 % (proj - point_half_)) | normal_) < 0.0)
//    //        {
//    ////            // Point point is inside the triangle
//    ////            return t_param;

//    //            const Scalar weight = weight_max_ + t_param*delta_weight_;
//    //            return point_contribution_.Eval((proj - point).norm()/weight);
//    //        }
//    //        else
//    //        {
//    //            // if not "compute distance to edges"
//    //            // on ne devrait pas être obligé de calculer chacune des distances, ce qui est fait juste avant doit pouvoir etre réutilisé
//    //            Scalar sqr_w_dist_longest_edge; Scalar t_param_longest;
//    //            ComputeWarpedDistAndParam(point, point_max_, unit_longest_dir,
//    //                                      weight_max_, delta_weight_, longest_dir_.norm(),
//    //                                      sqr_w_dist_longest_edge, t_param_longest);

//    //            Scalar sqr_w_dist_half_edge_1; Scalar t_param_half_1;
//    //            ComputeWarpedDistAndParam(point, point_max_, unit_half_dir_1,
//    //                                      weight_max_, normalized_coord_middle_*delta_weight_, half_dir_1_.norm(),
//    //                                      sqr_w_dist_half_edge_1, t_param_half_1);

//    //            Scalar sqr_w_dist_half_edge_2; Scalar t_param_half_2;
//    //            ComputeWarpedDistAndParam(point, point_half_, unit_half_dir_2,
//    //                                      weight_max_+normalized_coord_middle_*delta_weight_, (1.0-normalized_coord_middle_)*delta_weight_, half_dir_2_.norm(),
//    //                                      sqr_w_dist_half_edge_2, t_param_half_2);

//    //            if(sqr_w_dist_longest_edge<sqr_w_dist_half_edge_1)
//    //            {
//    //                if(sqr_w_dist_longest_edge<sqr_w_dist_half_edge_2)
//    //                {
//    //                    return t_param_longest;
//    ////                    return point_contribution_.Eval(sqrt(sqr_w_dist_longest_edge));
//    //                }
//    //                else
//    //                {
//    //                    return normalized_coord_middle_ + t_param_half_2 * (1.0 - normalized_coord_middle_);
//    ////                    return point_contribution_.Eval(sqrt(sqr_w_dist_half_edge_2));
//    //                }
//    //            }
//    //            else
//    //            {
//    //                if(sqr_w_dist_half_edge_1<sqr_w_dist_half_edge_2)
//    //                {
//    //                    return t_param_half_1 * normalized_coord_middle_;
//    ////                    return point_contribution_.Eval(sqrt(sqr_w_dist_half_edge_1));
//    //                }
//    //                else
//    //                {
//    //                    return normalized_coord_middle_ + t_param_half_2 * (1.0 - normalized_coord_middle_);
//    ////                    return point_contribution_.Eval(sqrt(sqr_w_dist_half_edge_2));
//    //                }
//    //            }
//    //        }
