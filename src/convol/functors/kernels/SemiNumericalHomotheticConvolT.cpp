
#include <convol/functors/kernels/SemiNumericalHomotheticConvolT.h>

#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

// Header file of the kernels that can be used as a functor of this kernel
//  this is required to register these associations inside the FunctorFactory

#include <convol/functors/kernels/cauchyinverse/HomotheticCauchy.h>
#include <convol/functors/kernels/cauchyinverse/HomotheticInverse.h>
#include <convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.h>
#include <convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.h>


namespace expressive {

namespace convol {

void AbstractSemiNumericalHomotheticConvol::InitFromXml(const TiXmlElement *element)
{
    int n = -1;
    core::TraitsXmlQuery::QueryIntAttribute("AbstractSemiNumericalHomotheticConvol::InitFromXml", element, "N", &n);
    set_N(n);
}


//static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<HomotheticCompactPolynomial4> > SemiNumericalHomotheticConvolT_HomotheticCompactPolynomial4_Type;
static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<HomotheticCompactPolynomial6> > SemiNumericalHomotheticConvolT_HomotheticCompactPolynomial6_Type;
//static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<HomotheticCompactPolynomial8> > SemiNumericalHomotheticConvolT_HomotheticCompactPolynomial8_Type;

static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<HomotheticCauchy3> > SemiNumericalHomotheticConvolT_HomotheticCauchy3_Type;
static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<HomotheticCauchy4> > SemiNumericalHomotheticConvolT_HomotheticCauchy4_Type;
static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<HomotheticCauchy5> > SemiNumericalHomotheticConvolT_HomotheticCauchy5_Type;
static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<HomotheticCauchy6> > SemiNumericalHomotheticConvolT_HomotheticCauchy6_Type;

static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<HomotheticInverse3> > SemiNumericalHomotheticConvolT_HomotheticInverse3_Type;
static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<HomotheticInverse4> > SemiNumericalHomotheticConvolT_HomotheticInverse4_Type;
static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<HomotheticInverse5> > SemiNumericalHomotheticConvolT_HomotheticInverse5_Type;
static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<HomotheticInverse6> > SemiNumericalHomotheticConvolT_HomotheticInverse6_Type;
static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<CompactedHomotheticInverse3> > SemiNumericalHomotheticConvolT_CompactedHomotheticInverse3_Type;
static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<CompactedHomotheticInverse4> > SemiNumericalHomotheticConvolT_CompactedHomotheticInverse4_Type;
static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<CompactedHomotheticInverse5> > SemiNumericalHomotheticConvolT_CompactedHomotheticInverse5_Type;
static core::FunctorFactory::Type<SemiNumericalHomotheticConvolT<CompactedHomotheticInverse6> > SemiNumericalHomotheticConvolT_CompactedHomotheticInverse6_Type;

} // end namespace convol
} // end namespace expressive
