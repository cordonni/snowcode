/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: KernelT.h

   Language: C++

   License: Convol Licence

   \author: Amaury Jung
   E-Mail: amaury.jung@inrialpes.fr

   Description: Header file for Kernel super class.
                This class defines all functions common to all Kernels.


   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_KERNEL_T_H
#define CONVOL_KERNEL_T_H


// Convol dependencies
#include <convol/ConvolRequired.h>
    // Functors
    #include<core/functor/Functor.h>

namespace expressive {

namespace convol {

class CONVOL_API Kernel : public core::Functor
{
public:

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    Kernel(Scalar scale = 1.0) : scale_(scale), inv_scale_2_(1.0/(scale*scale)) {}

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        core::Functor::AddXmlAttributes(element);
        element->SetDoubleAttribute("scale",scale_);
    }

    void InitFromXml(const TiXmlElement* element);

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool isCompact() const{ return false; }

    /////////////////
    /// Modifiers ///
    /////////////////

    virtual void set_scale(Scalar scale)
    {
        scale_ = scale;
        inv_scale_2_ = 1.0 / (scale*scale);
    }

    /////////////////
    /// Accessors ///
    /////////////////

    Scalar scale() const { return scale_; }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    Scalar scale_;
    Scalar inv_scale_2_;

};

//////////////////////
/// Help functions ///
//////////////////////

namespace kernelHelpFunction {
    template<int N> Scalar IntPow   (Scalar aux);
    template<>      CONVOL_API Scalar IntPow<1>(Scalar aux);
    template<>      CONVOL_API Scalar IntPow<2>(Scalar aux);
    template<>      CONVOL_API Scalar IntPow<3>(Scalar aux);
    template<>      CONVOL_API Scalar IntPow<4>(Scalar aux);
    template<>      CONVOL_API Scalar IntPow<5>(Scalar aux);
    template<>      CONVOL_API Scalar IntPow<6>(Scalar aux);
    template<>      CONVOL_API Scalar IntPow<7>(Scalar aux);

    template<int N> void ExpandWeightPolynome   (Scalar (&special_coeff)[N] , Scalar w_min, Scalar delta_w);
    template<>      CONVOL_API void ExpandWeightPolynome<3>(Scalar (&special_coeff)[3] , Scalar w_min, Scalar delta_w);
    template<>      CONVOL_API void ExpandWeightPolynome<4>(Scalar (&special_coeff)[4] , Scalar w_min, Scalar delta_w);
    template<>      CONVOL_API void ExpandWeightPolynome<5>(Scalar (&special_coeff)[5] , Scalar w_min, Scalar delta_w);
    template<>      CONVOL_API void ExpandWeightPolynome<6>(Scalar (&special_coeff)[6] , Scalar w_min, Scalar delta_w);
    template<>      CONVOL_API void ExpandWeightPolynome<7>(Scalar (&special_coeff)[7] , Scalar w_min, Scalar delta_w);
}

} // Close namespace convol
} // Close namespace expressive

#endif // CONVOL_KERNEL_T_H
