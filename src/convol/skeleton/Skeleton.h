/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: Skeleton.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for the skeleton. This structure manages all skeleton primitives and their geometrical connections and handles through
                a graph structure.

                Most SkelPrimitiveT manages nodes in the Blobtree, which mean a skeleton cannont exist without a blobtree. 
                A Skeleton is bound to a BlobtreeRootT in which all primitives will be added. 

                Creation and addition functions are placed as static functions in the different primitives, so that this class does not need to be modified
                if a new class of primitive is written.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_SKELETON_H_
#define CONVOL_SKELETON_H_

#include <core/graph/Graph.h>

#include <convol/blobtreenode/BlobtreeRoot.h>
#include <convol/blobtreenode/NodeScalarField.h>

namespace expressive {

namespace convol {

class Skeleton
        : public core::WeightedGraph
{
public:
    EXPRESSIVE_MACRO_NAME("Skeleton")

    typedef core::WeightedPoint VertexType;
    typedef core::WeightedGeometryPrimitive EdgeType;

    using core::WeightedGraph::Add;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    CONVOL_API Skeleton();

    virtual CONVOL_API ~Skeleton();

    /**
     * Creates a Skeleton from a given WeightedGraph.
     * This will also create the proper Primitives & blobtreenodes (should be the same items).
     * @param source An input graph
     * @return graph converted as a skeleton.
     */
    static CONVOL_API std::shared_ptr<Skeleton> CreateFromGraph(std::shared_ptr<core::WeightedGraph> source);

    //TODO:@todo : ajouter une fonction ajoutant une primitive dans le graph tout en la placant dans le blobtree root ?

    /////////////////
    /// Accessors ///
    /////////////////

    CONVOL_API std::shared_ptr<BlobtreeRoot> nonconst_blobtree_root();
    CONVOL_API const BlobtreeRoot& blobtree_root() const;

    /////////////////
    /// Attributs ///
    /////////////////

    /**
      * Adds a given edge to the graph.
      * If edge contains multiple points, a new edge will be created for each
      * and only the last one will be returned.
      * If addToChanges is left to true, it will also be added to the next update's list of changes.
      * Set it to false when loading stuff for example.
      */
    CONVOL_API virtual EdgeIteratorPtr Add(std::shared_ptr<EdgeType> shared_edge, std::vector<std::shared_ptr<Vertex> > &vertices, bool addToChanges = true);

    /**
     * @brief Remove Removes a given edge from the graph.
     * Remove functions have to be redefined because we can't get the NodeScalarField otherwise when using the listeners
     * in ConvolObject. Doh.
     * Also, the "Add" methods are in ConvolObject as listeners method to avoid that loading messes up everything.
     */
    CONVOL_API virtual void Remove(const EdgeId &id);
    CONVOL_API virtual void Remove(std::shared_ptr<EdgeType> edge);

    /**
     * @brief Remove Removes a given vertex from the graph.
     */
    CONVOL_API virtual void Remove(const VertexId &id);

    CONVOL_API void AddToBlobtree(std::shared_ptr<convol::NodeScalarField> node);
    CONVOL_API void RemoveFromBlobtree(std::shared_ptr<convol::NodeScalarField> node);

//    virtual VertexId SplitEdge(const EdgeId &id, unsigned int index, const VertexType& point);

protected :
    // The Blobtree in which all implicit primitive created by this skeleton will be added
    std::shared_ptr<BlobtreeRoot> blobtree_root_;
};

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_SKELETON_H_
