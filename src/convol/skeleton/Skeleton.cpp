#include <convol/skeleton/Skeleton.h>

#include <convol/blobtreenode/NodeScalarField.h>
#include <convol/blobtreenode/NodeOperatorT.h>
#include <convol/functors/operators/BlendSum.h>


using namespace expressive::core;
using namespace std;

namespace expressive {

namespace convol {

Skeleton::Skeleton() : core::WeightedGraph()
{
    blobtree_root_ = std::make_shared<BlobtreeRoot>();
    //TODO:@todo : serializable will have "graph" as a string which is not right !!!
}

Skeleton::~Skeleton()
{
}

std::shared_ptr<BlobtreeRoot> Skeleton::nonconst_blobtree_root()
{
    return blobtree_root_;
}

const BlobtreeRoot& Skeleton::blobtree_root() const
{
    return *blobtree_root_;
}

std::shared_ptr<Skeleton> Skeleton::CreateFromGraph(std::shared_ptr<WeightedGraph> source)
{
    std::shared_ptr<Skeleton> res = std::make_shared<Skeleton>();
    std::map<WeightedGraph::VertexId, WeightedGraph::VertexPtr> vertices_map;

    WeightedGraph::VertexIteratorPtr vertices = source->GetVertices();

    std::shared_ptr<Functor> func = FunctorFactory::GetInstance().CreateDefaultFunctor();

    std::shared_ptr<BlobtreeNode> orig = make_shared<NodeOperatorNArityT<BlendSum> >();
    res->nonconst_blobtree_root()->AddNewNodeToRoot(orig);

    while (vertices->HasNext()) {
        std::shared_ptr<WeightedVertex> vertex = vertices->Next();
        std::shared_ptr<WeightedPoint > newP = std::dynamic_pointer_cast<WeightedPoint >(PrimitiveFactory::GetInstance().CreateNewPrimitive(vertex->posptr(), *func));

        auto p = res->Add(newP, true);
        vertices_map.insert(std::make_pair(vertex->id(), p));
    }

//    std::set<std::shared_ptr<WeightedGeometryPrimitive> > primitives;

//    WeightedGraph::EdgeIteratorPtr edges = source->GetEdges();
//    while (edges->HasNext()) {
//        std::shared_ptr<WeightedEdge> edge = edges->Next();
//        primitives.insert(edge->edgeptr();
//    }
    WeightedGraph::PrimitiveMap primitives = source->GetPrimitives();

    for (PrimitiveMap::iterator it = primitives.begin(); it != primitives.end(); ++it) {
        std::shared_ptr<WeightedGeometryPrimitive> oldG = it->first;
        std::vector<WeightedGraph::VertexPtr> primitive_vertices;
        primitive_vertices.push_back(vertices_map[source->Get(it->second[0])->GetStartId()]);
        for (unsigned int i = 0; i < it->second.size(); ++i) {
            primitive_vertices.push_back(vertices_map[source->Get(it->second[i])->GetEndId()]);
        }
        std::shared_ptr<WeightedGeometryPrimitive> newG = std::dynamic_pointer_cast<WeightedGeometryPrimitive>(PrimitiveFactory::GetInstance().CreateNewPrimitive(oldG, *func));
        res->Add(newG, primitive_vertices, false);
        auto node = dynamic_pointer_cast<NodeScalarField>(newG);
        if (node != nullptr) {
            res->nonconst_blobtree_root()->AddChildToNAryNode(orig, node);
        }
    }

    return res;

}

Skeleton::EdgeIteratorPtr Skeleton::Add(std::shared_ptr<EdgeType> shared_edge, std::vector<std::shared_ptr<WeightedVertex> > &vertices, bool addToChanges)
{
    EdgeIteratorPtr res = WeightedGraph::Add(shared_edge, vertices, addToChanges);

    if (addToChanges) {
        // this flag is only set to false when we are currently loading the blobtree,
        // in which case the blobtree organisation is handled there.

        // Reproduce Blobtree
        auto node = dynamic_pointer_cast<NodeScalarField>(shared_edge);
        if (node != nullptr) { // if added node is a NodeScalarField
            auto parent = node->nonconst_parent();
            if (!parent || parent->arity() != -1) {
                blobtree_root_->AddNewNodeToRoot(node);
            } else { // TODO : case where arity == 1 or arity == 2
                if (!parent->HasChild(node)) {
                    node->set_parent(nullptr); // unset the parent if it was set to avoid conflicts with the add_child method.
                }
                blobtree_root_->AddChildToNAryNode(parent, node);
            }
        }
    }
    return res;
}

void Skeleton::Remove(const EdgeId &id)
{
    RemoveFromBlobtree(dynamic_pointer_cast<NodeScalarField>(Get(id)->edgeptr()));

    WeightedGraph::Remove(id);
}

void Skeleton::Remove(std::shared_ptr<EdgeType> edge)
{
    RemoveFromBlobtree(dynamic_pointer_cast<NodeScalarField>(edge));

    WeightedGraph::Remove(edge);
}

void Skeleton::Remove(const VertexId &id)
{
    if (vertex_index_map_.find(id) == vertex_index_map_.end()) {
        ork::Logger::ERROR_LOGGER->logf("GRAPH", "Vertex is not part of the graph or is already removed [%d]", id);
        return;
    }
    RemoveFromBlobtree(dynamic_pointer_cast<NodeScalarField>(Get(id)->posptr()));

    WeightedGraph::Remove(id);
}

// Todo pass these functions to ConvolObject as VertexAdded & EdgeAdded etc... functions (GraphListener)
void Skeleton::AddToBlobtree(std::shared_ptr<NodeScalarField> node)
{

    if (node == nullptr) { // if the node provided was not actually a NodeScalarField... (i.e. not a blobtreenode at all)
        return;
    }
    NodeOperator *p = dynamic_cast<NodeOperator*>(node->nonconst_parent());
    // If it has a parent, it should already be inserted in it (via clone).
    if (p != nullptr) {
        // TODO : Add this node to the parent if not an Narity parent !
        if (p->arity() == -1) {
            blobtree_root_->AddChildToNAryNode(p, node);
        } else {
            blobtree_root_->AddNewNodeToRoot(node);
        }
    } else if (node->nonconst_parent() == nullptr) {  // else no parent : inserted at blobtreeroot
        blobtree_root_->AddNewNodeToRoot(node);
    }
}

void Skeleton::RemoveFromBlobtree(std::shared_ptr<NodeScalarField> node)
{
    if (node == nullptr) {
        return;
    }

    if (blobtree_root_->CheckParent(node)) {
        blobtree_root_->DeleteNode(node);
    }
}

//Skeleton::VertexId Skeleton::SplitEdge(const EdgeId &id, unsigned int index, const VertexType &point) {
//    // Register Blobtree Parent

//    // Split inside Graph
//    VertexId vid = Graph::SplitEdge(id, index, point);


//    // Reproduce Blobtree
//    auto it = Get(vid)->GetEdges();
//    while(it->HasNext()) {
//        auto node = dynamic_pointer_cast<NodeScalarField>(it->Next()->edgeptr());
//        auto parent = node->nonconst_parent();
//        node->set_parent(nullptr);
//        if (!parent || parent->arity() != -1) {
//            blobtree_root_->AddNewNodeToRoot(node);
//        } else {
//            blobtree_root_->AddChildToNAryNode(parent, node);
//        }
//    }

//    return vid;
//}

} // Close namespace convol
} // Close namespace expressive

