#include <convol/xmlloaders/blobtreenode/NodeIntegralSurfaceOperatorLoader.h>

// core dependencies
#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

// convol dependencies
#include <convol/factories/BlobtreeNodeFactory.h>
#include <convol/xmlloaders/blobtreenode/BlobtreeNodeLoader.h>
#include <convol/blobtreenode/nodeoperator/IntegralSurfaceOperator.h>

// node operator to be registered
#include <convol/blobtreenode/nodeoperator/CorrectedSumIntegralSurfaceT.h>
#include <convol/blobtreenode/nodeoperator/GradientBasedIntegralSurfaceT.h>

namespace expressive {

namespace convol {

auto NodeOperatorIntegralSurfaceLoader::CreateFromXml(  const TiXmlElement *e,
                                                        std::map<unsigned long, std::shared_ptr<BlobtreeNode>>& map_id_node,
                                                        Skeleton* skel)
-> std::shared_ptr<NodeOperator>
{
    // In passing, get the scalar field node id.
    int node_id = -1;
    core::TraitsXmlQuery::QueryIntAttribute("NodeOperatorIntegralSurfaceLoader::Load", e,
                                            "id", &node_id);

    // Check if the node has already been built, if not build it and insert it into the map
    std::shared_ptr<NodeOperator> node_operator = nullptr;
    auto it_node = map_id_node.find(node_id);
    if(it_node == map_id_node.end())
    {
        const TiXmlElement* element_kernel_functor =
                core::TraitsXmlQuery::QueryFirstChildElement("NodeOperatorIntegralSurfaceLoader::Load",
                                                             e, "TFunctorKernel");
        element_kernel_functor = element_kernel_functor->FirstChildElement();

        std::shared_ptr<core::Functor> kernel_functor = core::FunctorFactory::GetInstance().CreateFunctorFromXml(element_kernel_functor);

        node_operator = std::dynamic_pointer_cast<NodeOperator>(BlobtreeNodeFactory::GetInstance().CreateNewBlobtreeNode(e->ValueStr(), *kernel_functor));

        node_operator->InitFromXml(e);

        //TODO:@todo : this is really ugly ...
        std::dynamic_pointer_cast<AbstractIntegralSurfaceOperator>(node_operator)->Init(node_operator.get(), skel);
        //TODO:@todo : this should depend upon the type of object and/or the xml
    }
    else
    {
        node_operator = std::dynamic_pointer_cast<NodeOperator>(it_node->second);
    }

    BlobtreeNodeLoader::LoadChildren(e, node_operator, map_id_node, skel);

    int is_corrector = -1;
    // TODO this check might be necessar
    if (e->Attribute("is_corrector") != nullptr) {
        core::TraitsXmlQuery::QueryIntAttribute("NodeOperatorIntegralSurfaceLoader::Load", e,
                                                "is_corrector", &is_corrector);
    }
    if(is_corrector==1) {
        //this is important to do this after all the children have been added to blobtree node
        std::dynamic_pointer_cast<AbstractIntegralSurfaceOperator>(node_operator)->AddCorrector();
    }

    return node_operator;
}

static BlobtreeNodeLoader::Type<AbstractCorrectedSumIntegralSurface, NodeOperatorIntegralSurfaceLoader> CorrectedSumIntegralSurfaceLoaderType;
static BlobtreeNodeLoader::Type<AbstractGradientBasedIntegralSurface, NodeOperatorIntegralSurfaceLoader> GradientBasedIntegralSurfaceLoaderType;
    
} // Close namespace convol

} // Close namespace expressive
