#include <convol/xmlloaders/blobtreenode/BlobtreeNodeLoader.h>

// core dependencies
#include <ork/core/Logger.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

// std dependencies
#include <exception>

namespace expressive {

namespace convol {


BlobtreeNodeLoader& BlobtreeNodeLoader::GetInstance()
{
    static BlobtreeNodeLoader INSTANCE = BlobtreeNodeLoader();
    return INSTANCE;
}

auto BlobtreeNodeLoader::CreateBlobtreeNode(const TiXmlElement *e,
                                            std::map<unsigned long, std::shared_ptr<BlobtreeNode>>& map_id_node,
                                            Skeleton* skel)
-> std::shared_ptr<BlobtreeNode>
{
    auto it = types.find(e->ValueStr());

    if (it != types.end()) {
        return it->second(e, map_id_node, skel);
    } else {
        ork::Logger::ERROR_LOGGER->log("CONVOL", "Unknown BlobtreeNode xml descriptor '" + e->ValueStr() + "'");
        throw std::exception();
    }
}

/** \brief Load children of the provided blobtreenode.
 *         Throw an exception in case of problem.
 */
void BlobtreeNodeLoader::LoadChildren(  const TiXmlElement* element_blobtree_node, std::shared_ptr<BlobtreeNode> blobtree_node_parent,
                                        std::map<unsigned long, std::shared_ptr<BlobtreeNode> >& map_id_node, Skeleton* skel)
{
    const TiXmlElement* element_children =
            core::TraitsXmlQuery::QueryFirstChildElement("BlobtreeNodeLoader::LoadChildren",
                                                         element_blobtree_node, "children");

    // Get the number of child and do a raw-resize of children vector.
    int n_children = -1;
    core::TraitsXmlQuery::QueryIntAttribute("BlobtreeNodeLoader::LoadChildren", element_children,
                                            "number", &n_children);
    unsigned int n_children_uint = (unsigned int) n_children;

////TODO:@todo : not sure it is the right place ...
//    std::string label;
//    if (element_children->QueryStringAttribute("label",&label) != TIXML_NO_ATTRIBUTE)
//        blobtree_node_parent.set_label(label);

    blobtree_node_parent->nonconst_children().resize(n_children_uint, nullptr);
//    for(unsigned int i = 0; i<n_children_uint; ++i)
//    { //TODO:@todo : this loop should be useless           see above  ^
//        blobtree_node_parent.nonconst_children()[i] = nullptr;
//    }

    // Now iterate on children xml element and add them.
    const TiXmlElement* element_current = element_children->FirstChildElement();
    for(unsigned int i = 0; i<n_children_uint; ++i)
    {
        if(element_current == nullptr)
        {
            if (ork::Logger::ERROR_LOGGER != nullptr) {
                ork::Logger::ERROR_LOGGER->log("CONVOL", "BlobtreeNodeLoader::LoadChildren : _number_ attribute does not correspond to number of child defined in the node.'");
            }
            throw std::exception();
        }

        std::shared_ptr<BlobtreeNode> child_i = GetInstance().CreateBlobtreeNode(element_current,map_id_node, skel);

        std::string label;
        if (element_current->QueryStringAttribute("label",&label) != TIXML_NO_ATTRIBUTE)
            child_i->set_label(label);

        // This is OK since we have resized the node before.
        blobtree_node_parent->set_child(i, child_i);

        element_current = element_current->NextSiblingElement();
    }
}


} // Close namespace convol

} // Close namespace expressive
