#include <convol/xmlloaders/blobtreenode/NodeOperatorNArityLoader.h>

// core dependencies
#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

// convol dependencies
#include <convol/blobtreenode/NodeOperatorT.h>
#include <convol/factories/BlobtreeNodeFactory.h>
#include <convol/xmlloaders/blobtreenode/BlobtreeNodeLoader.h>

namespace expressive {

namespace convol {

auto NodeOperatorNArityLoader::CreateFromXml(const TiXmlElement *e,
                                             std::map<unsigned long, std::shared_ptr<BlobtreeNode>>& map_id_node,
                                             std::shared_ptr<Skeleton> skel)
-> std::shared_ptr<AbstractNodeOperatorNArity>
{
    assert(e->ValueStr().compare(AbstractNodeOperatorNArity::StaticName()) == 0);

    // In passing, get the scalar field node id.
    int node_id = -1;
    core::TraitsXmlQuery::QueryIntAttribute("NodeOperatorNXmlLoaderT::Load", e,
                                            "id", &node_id);

    // Check if the node has already been built, if not build it and insert it into the map
    std::shared_ptr<AbstractNodeOperatorNArity> node_operator = nullptr;
    auto it_node = map_id_node.find(node_id);
    if(it_node == map_id_node.end())
    {
        const TiXmlElement* element_blend_functor =
                core::TraitsXmlQuery::QueryFirstChildElement("NodeOperatorNLoader::Load",
                                                             e, "TFunctorBlendOperatorN");
        element_blend_functor = element_blend_functor->FirstChildElement();

        std::shared_ptr<core::Functor> blend_functor = core::FunctorFactory::GetInstance().CreateFunctorFromXml(element_blend_functor);

        node_operator = std::dynamic_pointer_cast<AbstractNodeOperatorNArity>(BlobtreeNodeFactory::GetInstance().CreateNewBlobtreeNode(e->ValueStr(), *blend_functor));
    }
    else
    {
        node_operator = std::dynamic_pointer_cast<AbstractNodeOperatorNArity>(it_node->second);
    }

    BlobtreeNodeLoader::LoadChildren(e, node_operator, map_id_node, skel);

    return node_operator;
}

//static BlobtreeNodeLoader::Type<AbstractNodeOperatorNArity, NodeOperatorNArityLoader> NodeOperatorNArityType;
    
} // Close namespace convol

} // Close namespace expressive
