#include <convol/xmlloaders/blobtreenode/BlobtreeRootLoader.h>

// core dependencies
#include <core/xmlloaders/TraitsXmlQuery.h>

// convol dependencies
#include <convol/xmlloaders/blobtreenode/BlobtreeNodeLoader.h>
//#include <convol/xmlloaders/blobtreenode/BlobtreeBasketLoader.h>

namespace expressive {

namespace convol {

std::shared_ptr<BlobtreeRoot> BlobtreeRootLoader::FillFromXml(std::shared_ptr<convol::BlobtreeRoot> bt_root, const TiXmlElement *e,
                                                              std::map<unsigned long, std::shared_ptr<BlobtreeNode>>& map_id_node,
                                                              Skeleton* skel)
{
    assert(e->ValueStr().compare(std::string("BlobtreeRoot")) == 0);

     const TiXmlElement* element_basket
             = core::TraitsXmlQuery::QueryFirstChildElement("BlobtreeRootLoader", e, "BlobtreeBasket");

     UNUSED(element_basket);
 //    BlobtreeBasketLoader::Load(element_basket, bt_root, map_id_node);
 ////TODO:@todo : add again later when everything is working fine ...

     BlobtreeNodeLoader::LoadChildren(e, bt_root, map_id_node, skel);

     return bt_root;
}

auto BlobtreeRootLoader::CreateFromXml(const TiXmlElement *e,
                                       std::map<unsigned long, std::shared_ptr<BlobtreeNode>>& map_id_node,
                                       Skeleton* skel)
-> std::shared_ptr<BlobtreeRoot>
{
    auto bt_root = std::make_shared<BlobtreeRoot>();
    return FillFromXml(bt_root, e, map_id_node, skel);
}
            
static BlobtreeNodeLoader::Type<BlobtreeRoot, BlobtreeRootLoader> BlobtreeRootType;

    
} // Close namespace convol

} // Close namespace expressive
