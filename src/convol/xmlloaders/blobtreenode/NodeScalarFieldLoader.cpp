#include <convol/xmlloaders/blobtreenode/NodeScalarFieldLoader.h>

// core dependencies
#include <core/xmlloaders/TraitsXmlQuery.h>

// convol dependencies
#include <convol/xmlloaders/blobtreenode/BlobtreeNodeLoader.h>


namespace expressive {

namespace convol {

auto NodeScalarFieldLoader::CreateFromXml(const TiXmlElement *e,
                                          std::map<unsigned long, std::shared_ptr<BlobtreeNode>>& map_id_node,
                                          Skeleton* /*skel*/)
-> std::shared_ptr<BlobtreeNode>
{
    assert(e->ValueStr().compare(NodeScalarField::StaticName()) == 0);

    // In passing, get the scalar field node id.
    int node_id = -1;
    core::TraitsXmlQuery::QueryIntAttribute("NodeScalarFieldLoader", e,
                                            "id", &node_id);

    auto it_node = map_id_node.find(node_id);
    if(it_node == map_id_node.end())
    {
        ork::Logger::ERROR_LOGGER->logf("CONVOL", "NodeScalarFieldLoader: Node %d not found in file.", node_id);
        throw std::exception();
//        return nullptr; //unreachable
        // not yet implemented since in all use we have now the skeleton is loaded first
    }else{
        return it_node->second;
    }
}
            
static BlobtreeNodeLoader::Type<NodeScalarField, NodeScalarFieldLoader> NodeScalarFieldType;

    
} // Close namespace convol

} // Close namespace expressive
