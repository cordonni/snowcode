#include <convol/xmlloaders/blobtreenode/TemplatedNodeOperatorLoader.h>

// core dependencies
#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/TraitsXmlQuery.h>

// convol dependencies
#include <convol/factories/BlobtreeNodeFactory.h>
#include <convol/xmlloaders/blobtreenode/BlobtreeNodeLoader.h>

// node operator to be registered
//#include <convol/blobtreenode/nodeoperator/CorrectedSumIntegralSurfaceT.h>
#include <convol/blobtreenode/NodeProceduralDetailT.h>
#include <convol/blobtreenode/NodeProceduralDetailParameterT.h>

namespace expressive {

namespace convol {

auto TemplatedNodeOperatorLoader::CreateFromXml(  const TiXmlElement *e,
                                                  std::map<unsigned long, std::shared_ptr<BlobtreeNode>>& map_id_node,
                                                  Skeleton* skel)
-> std::shared_ptr<BlobtreeNode>
{
    // In passing, get the scalar field node id.
    int node_id = -1;
    core::TraitsXmlQuery::QueryIntAttribute("TemplatedNodeOperatorLoader::Load", e,
                                            "id", &node_id);

    // Check if the node has already been built, if not build it and insert it into the map
    std::shared_ptr<BlobtreeNode> node_operator = nullptr;
    auto it_node = map_id_node.find(node_id);
    if(it_node == map_id_node.end())
    {
        std::string xml_functor_field = BlobtreeNodeFactory::GetInstance().GetXmlFunctorFieldFromBlobtreeNode(e->ValueStr());

        const TiXmlElement* element_functor =
                core::TraitsXmlQuery::QueryFirstChildElement("TemplatedNodeOperatorLoader::Load",
                                                             e, xml_functor_field.c_str()); //TFunctorBlendOperatorN TODO:@todo : define name to be retrieved, probably in the same way as in functors
        element_functor = element_functor->FirstChildElement();

        std::shared_ptr<core::Functor> functor = core::FunctorFactory::GetInstance().CreateFunctorFromXml(element_functor);

        node_operator = BlobtreeNodeFactory::GetInstance().CreateNewBlobtreeNode(e->ValueStr(), *functor);
        node_operator->InitFromXml(e);
    }
    else
    {
        node_operator = std::dynamic_pointer_cast<BlobtreeNode>(it_node->second);
    }

    BlobtreeNodeLoader::LoadChildren(e, node_operator, map_id_node, skel);

    return node_operator;
}

static BlobtreeNodeLoader::Type<AbstractNodeOperatorNArity, TemplatedNodeOperatorLoader> NodeOperatorNArityType;
static BlobtreeNodeLoader::Type<AbstractNodeProceduralDetail, TemplatedNodeOperatorLoader> NodeNodeProceduralDetailType;
static BlobtreeNodeLoader::Type<AbstractNodeProceduralDetailParameter, TemplatedNodeOperatorLoader> NodeNodeProceduralDetailParameterType;

    
} // Close namespace convol

} // Close namespace expressive
