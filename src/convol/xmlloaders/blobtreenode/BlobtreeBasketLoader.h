/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: BlobtreeBasketXmlLoaderT.h
 
 Language: C++
 
 License: Convol license
 
 \author: Maxime Quiblier
 E-Mail: maxime.quiblier@inrialpes.fr
 
 Description: Design to load a Blobtree basket.
 
 Platform Dependencies: None
 */

#pragma once
#ifndef CONVOL_BLOBTREE_BASKET_XML_LOADER_H_
#define CONVOL_BLOBTREE_BASKET_XML_LOADER_H_


// core dependencies
#include<core/CoreRequired.h>
    // tinyxml dependencies
    #include<core/utils/tinyxml/tinyxml.h>

// convol dependencies
    // blobtree node
    #include <convol/blobtreenode/BlobtreeBasket.h>
    #include <convol/blobtreenode/BlobtreeNode.h>
    #include <convol/blobtreenode/BlobtreeRoot.h>
    // xml
    #include <convol/xmlloaders/blobtreenode/BlobtreeNodeLoader.h>

// std dependencies
#include<string>
#include<map>

namespace expressive {

namespace convol {

class CONVOL_API BlobtreeBasketLoader
{
public:

    /** \brief Create and load the BlobtreeBasket corresponding to the given xml element in the given
     *         BlobtreeRoot.
     *
     *    \param element_blobtree_basket A reference to the Xml description of the BlobtreeBasket to be created.
     *    \param blobtree_root A reference to the BlobtreeRoot to be filled.
     *    \param map_id_node_objects Map in which the created elements will be mapped to their id.
     *
     *  \return Return true if the load is a success, false otherwise
     */
    static bool Load(const TiXmlElement* element_blobtree_basket, BlobtreeRoot& blobtree_root,
                     std::map<unsigned long, std::shared_ptr<BlobtreeNode> >& map_id_node_objects)
    {
        assert(element_blobtree_basket->ValueStr().compare(std::string("BlobtreeBasket")) == 0);

        // Nothing to do so far since the actual instance of a root basket is created with the root.
        BlobtreeBasket& blobtree_basket = blobtree_root.nonconst_basket();

        return BlobtreeNodeLoader::LoadChildren(element_blobtree_basket, blobtree_basket, map_id_node_objects);
    }

}; // End class BlobtreeBasketLoader declaration
      
} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_BLOBTREE_BASKET_XML_LOADER_H_
