/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: BlobtreeNodeLoader.h
 
 Language: C++
 
 License: Convol license
 
 \author: Maxime Quiblier
 E-Mail: maxime.quiblier@inrialpes.fr
 
 Description: Design to load a BlobtreeNode. This is recursive.
 
 Platform Dependencies: None
 */

#pragma once
#ifndef EXPRESSIVE_BLOBTREE_NODE_XML_LOADER_H_
#define EXPRESSIVE_BLOBTREE_NODE_XML_LOADER_H_

// core dependencies
#include<core/CoreRequired.h>
    #include<tinyxml/tinyxml.h>

// convol dependencies
#include <convol/blobtreenode/BlobtreeNode.h>
#include <convol/skeleton/Skeleton.h>

// std dependencies
#include<string>
#include<map>

namespace expressive {

namespace convol {

class CONVOL_API BlobtreeNodeLoader
{
public:

    // Convenience typedef
    // Map from a string to a function pointer. This is used to map the value of a given TiXmlNode to the loader of
    // the appropriate Blobtree node.
    typedef std::shared_ptr<BlobtreeNode> (*createFunc) (const TiXmlElement *, std::map<unsigned long, std::shared_ptr<BlobtreeNode>>&, Skeleton*);

    static BlobtreeNodeLoader &GetInstance();

    template<typename TBlobtreeNode>
    void RegisterBlobtreeNode(createFunc f);

    std::shared_ptr<BlobtreeNode> CreateBlobtreeNode(const TiXmlElement *e,
                                                     std::map<unsigned long, std::shared_ptr<BlobtreeNode>>& map_id_node,
                                                     Skeleton* skel);

    /** \brief Load children of the provided blobtreenode.
     *         Throw an exception in case of problem.
     */
    static void LoadChildren(const TiXmlElement* element_blobtree_node, std::shared_ptr<BlobtreeNode> blobtree_node_parent,
                             std::map<unsigned long, std::shared_ptr<BlobtreeNode> >& map_id_node, Skeleton* skel);


    template <class TBlobtreeNode, class TBlobtreeNodeLoader>
    class Type
    {
    public:
        static std::shared_ptr<BlobtreeNode> ctor (const TiXmlElement *e,
                                                   std::map<unsigned long, std::shared_ptr<BlobtreeNode>>& map_id_node, Skeleton* skel)
        {
            return TBlobtreeNodeLoader::CreateFromXml(e, map_id_node, skel);
        }

        Type()
        {
            BlobtreeNodeLoader::GetInstance().RegisterBlobtreeNode<TBlobtreeNode>(ctor);
        }
    };

protected:
    std::map<std::string, createFunc> types;


    /** \brief This function initializes the given xml-described node by initializing its children.
     *         If the id is in map_id_node_objects, the node has already been created so we just return it in res.
     *         Otherwise, BlobtreeNodeXmlLoaderT find the good loader among its registered node loaders to
     *         build the node.
     *
     *    \param element_blobtree_node A reference to the Xml description of the BlobtreeNode to be created.
     *    \param blobtree_node_parent A pointer to the corresponding BlobtreeNode of which we want the children to be built.
     *    \param map_id_node_objects Map in which already created node are mapped to their id.
     *
     *  \return Return true if the load is a success, false otherwise
     */
            
}; // End class BlobtreeNodeLoader declaration

template <typename TBlobtreeNode>
void BlobtreeNodeLoader::RegisterBlobtreeNode(createFunc f)
{
    types[TBlobtreeNode::StaticName()] = f;
    //TODO:@todo: would be better to warn if BlobtreeNode was already registered (which should not be the case)
}

} // Close namespace convol

} // Close namespace expressive

#endif // EXPRESSIVE_BLOBTREE_NODE_XML_LOADER_H_
