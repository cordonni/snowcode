/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: BlobtreeRootLoader.h
 
 Language: C++
 
 License: Convol license
 
 \author: Maxime Quiblier
 E-Mail: maxime.quiblier@inrialpes.fr
 
 Description: Design to load a Blobtree using it's root (BlobtreeRootT) from a xml.
 
 Platform Dependencies: None
 */

#pragma once
#ifndef CONVOL_BLOBTREE_ROOT_XML_LOADER_H_
#define CONVOL_BLOBTREE_ROOT_XML_LOADER_H_

// core dependencies
#include<core/CoreRequired.h>
    #include<tinyxml/tinyxml.h>

// convol dependencies
    #include <convol/skeleton/Skeleton.h>
    // blobtree node
    #include <convol/blobtreenode/BlobtreeNode.h>
    #include <convol/blobtreenode/BlobtreeRoot.h>

// std dependencies
#include<map>

namespace expressive {

namespace convol {

class CONVOL_API BlobtreeRootLoader
{
public:
//TODO:@todo : update the description of the function
    /** \brief Convenience function to set BlobtreeRoot attributes, build and load the associated 
     *         basket then call the children creation function and set the children correctly.
     *         Note that this function does not actually CREATE the BlobtreeRoot. It fills
     *         the one given as argument.
     *
     *    \param element_blobtree_root A reference to the Xml description of the BlobtreeRoot to be created.
     *    \param blobtree_root A reference to the BlobtreeRoot to be filled.
     *    \param map_id_node_objects Map in which the created elements will be mapped to their id.
     *
     *  \return Return true if the load is a success, false otherwise
     */
    static std::shared_ptr<BlobtreeRoot> FillFromXml(std::shared_ptr<convol::BlobtreeRoot> bt_root, const TiXmlElement *e,
                                                     std::map<unsigned long, std::shared_ptr<BlobtreeNode>>& map_id_node,
                                                     Skeleton* skel);

    static std::shared_ptr<BlobtreeRoot> CreateFromXml(const TiXmlElement *e,
                                                       std::map<unsigned long, std::shared_ptr<BlobtreeNode>>& map_id_node,
                                                       Skeleton* skel);
}; // End class BlobtreeRootLoader declaration
    
} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_BLOBTREE_ROOT_XML_LOADER_H_
