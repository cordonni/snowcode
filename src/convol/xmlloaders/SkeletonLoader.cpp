#include <convol/xmlloaders/SkeletonLoader.h>

#include <core/xmlloaders/GeometryPrimitiveLoader.h>
#include <core/xmlloaders/WeightedPointLoader.h>

#include <core/xmlloaders/TraitsXmlQuery.h>

// std dependencies
#include<sstream>

using namespace ork;
using namespace expressive::core;
using namespace std;

namespace expressive {

namespace convol {

SkeletonResource::SkeletonResource(ork::ptr<ResourceManager> manager, const std::string &name, ork::ptr<ResourceDescriptor> desc, const TiXmlElement *e)
    : ResourceTemplate<50, Skeleton>(manager, name, desc)
{
    e = e == NULL ? desc->descriptor : e;
    checkParameters(desc, e, "name,"); //TODO:@todo : skeleton has no name ?

    // map used to store assocition between id used in the xml file and the Vertex built
    std::map<unsigned long, shared_ptr<Vertex> > map_id_vertex;

    const TiXmlElement* element_current;

    /// Loading vertices ///

    element_current = TraitsXmlQuery::QueryFirstChildElement("SkeletonResource", e, "Vertices");
    element_current = element_current->FirstChildElement();
    while(element_current != nullptr)
    {
        shared_ptr<Vertex> vertex = nullptr;

        //TODO:@todo : this should probably be done inside a factory
        if(element_current->ValueStr().compare("NodePointSField") == 0) {
            // case of a point primitive requiring a specific loader
            auto s_ptr_geom_primitive =
                    core::GeometryPrimitiveLoader::GetInstance()->Create(element_current, map_id_vertex); //TODO:@todo should require no vertices ...

            std::shared_ptr<BlobtreeNode> bt_node_ptr = dynamic_pointer_cast<BlobtreeNode>(s_ptr_geom_primitive);
            std::shared_ptr<WeightedPoint> wp = dynamic_pointer_cast<WeightedPoint>(s_ptr_geom_primitive);
            int node_scalar_field_id = -1;
            if(bt_node_ptr != nullptr && wp != nullptr) {
                TraitsXmlQuery::QueryIntAttribute("SkeletonLoader", element_current, "id", &node_scalar_field_id);
                blobtree_root_->map_id_node_[node_scalar_field_id] = bt_node_ptr;

                vertex = Add(wp, false);
            } else {
                Logger::ERROR_LOGGER->logf("SKELETON", "Error while loading vertex node %d.", node_scalar_field_id);
            }
        } else {
            WeightedPoint wp;
            WeightedPointLoader::Load(element_current, wp);
            vertex = Add(wp, false);
        }

        int v_id = -1;
        TraitsXmlQuery::QueryIntAttribute("SkeletonLoader", element_current, "id", &v_id);
        map_id_vertex[v_id] = vertex;

        element_current = element_current->NextSiblingElement();
    }

    /// Loading edges ///

    element_current = TraitsXmlQuery::QueryFirstChildElement("SkeletonResource", e, "Edges");
    element_current = element_current->FirstChildElement();
    while(element_current != nullptr)
    {
        auto s_ptr_geom_primitive =
            core::GeometryPrimitiveLoader::GetInstance()->Create(element_current, map_id_vertex);

        // Only implicit primitives should be inserted in the BlobtreeRoot map
        auto bt_node_ptr = std::dynamic_pointer_cast<BlobtreeNode>(s_ptr_geom_primitive);
        int node_scalar_field_id = -1;
        if(bt_node_ptr != nullptr)
        {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///TODO:@todo : retrieving the id of the BlobtreeNode in the xml is ugly because it is "convol-style" xml
/// Should find a solution that work for both old and new formulation ...
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            const TiXmlElement* element_node = element_current->FirstChildElement("sf_blobtree_node");
            if(element_node != nullptr)
            {
                // old xml format (from ConvolLab)
                element_node = TraitsXmlQuery::QueryFirstChildElement("SkeletonLoader", element_node, "NodeScalarField");
                TraitsXmlQuery::QueryIntAttribute("SkeletonLoader", element_node, "id", &node_scalar_field_id);
            }
            else
            {
                TraitsXmlQuery::QueryIntAttribute("SkeletonLoader", element_current, "id", &node_scalar_field_id);
            }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            blobtree_root_->map_id_node_[node_scalar_field_id] = bt_node_ptr;
        }

        int nb_vertex = s_ptr_geom_primitive->GetSize();

        if (nb_vertex > 1) {
            std::vector<std::shared_ptr<Vertex> > vertices_;
            int id_vh;
            std::ostringstream name;
            for(int i = 0; i<nb_vertex; ++i)
            {
                name.str("");
                name << "id_vh" << i;

                TraitsXmlQuery::QueryIntAttribute("SkeletonLoader", element_current, name.str().c_str(), &id_vh);
                vertices_.push_back(map_id_vertex[id_vh]);
            }
            if (s_ptr_geom_primitive->isLoop()) {
                vertices_.push_back(vertices_[0]);
            }
            std::shared_ptr<WeightedGeometryPrimitive> edge = dynamic_pointer_cast<WeightedGeometryPrimitive>(s_ptr_geom_primitive);
            if (edge != nullptr) {
                Add(edge, vertices_, false);
            } else {
                Logger::ERROR_LOGGER->logf("SKELETON", "Error while loading edge node %d.", node_scalar_field_id);
            }
        } else {
            std::shared_ptr<WeightedPoint> vertex = dynamic_pointer_cast<WeightedPoint>(s_ptr_geom_primitive);
            if (vertex != nullptr) {
                Add(vertex, false);
            } else {
                Logger::ERROR_LOGGER->logf("SKELETON", "Error while loading vertex node %d.", node_scalar_field_id);
            }
        }

        element_current = element_current->NextSiblingElement();
    }
    changes_.Clear();
}

extern const char skeleton[] = "Skeleton";

static ResourceFactory::Type<skeleton, SkeletonResource> SkeletonType;

} // Close namespace convol

} // Close namespace expressive


