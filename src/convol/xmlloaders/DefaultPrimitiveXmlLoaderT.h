/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: DefaultPrimitiveXmlLoader

   Language: C++

   License: Convol license

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Header file for default loading primitives from a TiXmlElement.
                It uses a factory to create them. 
		
                It is used for NodeSegmentSField, NodeTriangleSField and NodeISSubdivisionCurveSField.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_DEFAULT_PRIMITIVE_XML_LOADER_H_
#define CONVOL_DEFAULT_PRIMITIVE_XML_LOADER_H_

#include <core/graph/Graph.h>
#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/TraitsXmlQuery.h>
#include <core/xmlloaders/GeometryPrimitiveLoader.h>

#include <core/geometry/PrimitiveFactory.h>

#include <exception>

//using namespace expressive::core;

namespace expressive {

namespace convol {

template<typename TAbstractPrimitive>
class DefaultPrimitiveXmlLoader
{
public:
    typedef typename TAbstractPrimitive::BaseGeometry BaseGeometry;

    static std::shared_ptr<TAbstractPrimitive> Create(const TiXmlElement *e,
                                              std::map<unsigned long, std::shared_ptr<core::WeightedVertex>>& map_id_vertex)
    {
        static std::string loader_name = TAbstractPrimitive::StaticName()+"XmlLoader";

        auto s_ptr_geom_primitive =
                std::dynamic_pointer_cast<BaseGeometry>(core::GeometryPrimitiveLoader::GetInstance()->Create(BaseGeometry::StaticName(), e, map_id_vertex));

        int node_scalar_field_id = -1;
        core::TraitsXmlQuery::QueryIntAttribute(loader_name.c_str(), e, "id", &node_scalar_field_id);

        const TiXmlElement* element_kernel
                = core::TraitsXmlQuery::QueryFirstChildElement(loader_name.c_str(), e, "TFunctorKernel");
        element_kernel = core::TraitsXmlQuery::QueryFirstChildElement(loader_name.c_str(), element_kernel);

        std::shared_ptr<core::Functor> kernel = core::FunctorFactory::GetInstance().CreateFunctorFromXml(element_kernel);

        std::shared_ptr<TAbstractPrimitive> node_primitive
                = std::dynamic_pointer_cast<TAbstractPrimitive>(core::PrimitiveFactory::GetInstance().CreateNewPrimitive(s_ptr_geom_primitive, *kernel));

        node_primitive->InitFromXml(e);

        return node_primitive;
    }

};


} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_DEFAULT_PRIMITIVE_XML_LOADER_H_

