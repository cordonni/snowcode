#include <convol/xmlloaders/DefaultPrimitiveXmlLoaderT.h>

// header file of primitive to register in the factory
#include <convol/primitives/NodePointSFieldT.h>
#include <convol/primitives/NodeSegmentSFieldT.h>
#include <convol/primitives/NodeTriangleSFieldT.h>
#include <convol/primitives/NodeISSubdivisionCurveT.h>

using namespace expressive::core;

namespace expressive {

namespace convol {

extern const char node_point_sfield[] = "NodePointSField";
static core::GeometryPrimitiveLoader::XmlType<node_point_sfield, DefaultPrimitiveXmlLoader<AbstractNodePointSField> > NodePointSFieldType;

extern const char node_segment_sfield[] = "NodeSegmentSField";
static core::GeometryPrimitiveLoader::XmlType<node_segment_sfield, DefaultPrimitiveXmlLoader<AbstractNodeSegmentSField> > NodeSegmentSFieldType;

extern const char node_triangle_sfield[] = "NodeTriangleSField";
static core::GeometryPrimitiveLoader::XmlType<node_triangle_sfield, DefaultPrimitiveXmlLoader<AbstractNodeTriangleSField> > NodeTriangleSFieldType;

extern const char node_is_subdivision_curve_sfield[] = "NodeISSubdivisionCurveSField";
static core::GeometryPrimitiveLoader::XmlType<node_is_subdivision_curve_sfield, DefaultPrimitiveXmlLoader<AbstractNodeISSubdivisionCurve> > NodeISSubdivisionCurveSFieldType;

} // Close namespace convol

} // Close namespace expressive


