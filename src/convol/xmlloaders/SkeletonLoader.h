/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: SkeletonLoader.h

   Language: C++

   License: Convol license

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Header file for loading a Skeleton from a TiXmlElement.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_SKELETON_LOADER_H_
#define CONVOL_SKELETON_LOADER_H_

#include <convol/skeleton/Skeleton.h>

#include <ork/resource/ResourceTemplate.h>

using namespace ork;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : it is important to note that this Loader store some information in the associated BlobtreeRoot
///              however, the latter is NOT loaded is this function (see ConvolObject.cpp)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace expressive {

namespace convol {

/// @cond RESOURCES

class SkeletonResource : public ResourceTemplate<50, Skeleton>
{
public:
    SkeletonResource(ork::ptr<ResourceManager> manager, const std::string &name, ork::ptr<ResourceDescriptor> desc, const TiXmlElement *e = nullptr);
};

/// @endcond



} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_SKELETON_LOADER_H_

