#include <convol/xmlloaders/primitives/NodeOLDPointSFieldLoader.h>

#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/TraitsXmlQuery.h>
#include <core/xmlloaders/GeometryPrimitiveLoader.h>
#include <core/xmlloaders/WeightedPointLoader.h>

#include <core/geometry/PrimitiveFactory.h>

#include <exception>

using namespace expressive::core;

namespace expressive {

namespace convol {

std::shared_ptr<AbstractNodePointSField> NodeOLDPointSFieldXmlLoader::Create(const TiXmlElement *e, std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex)
{
    std::shared_ptr<WeightedPoint> w_pt = WeightedPointLoader::Create(e, map_id_vertex);


    const TiXmlElement* element_current
            = TraitsXmlQuery::QueryFirstChildElement("SkelBasicPointLoader", e, "sf_blobtree_node");
    element_current = TraitsXmlQuery::QueryFirstChildElement("SkelBasicPointLoader", element_current, "NodeScalarField");

    int node_scalar_field_id = -1;
    TraitsXmlQuery::QueryIntAttribute("SkelBasicPointLoader", element_current, "id", &node_scalar_field_id);

    element_current = TraitsXmlQuery::QueryFirstChildElement("SkelBasicPointLoader", element_current, "PointSField");
    // Should use NodePointSField::StaticName instead of "..." function but we are currently loading convol style file

    const TiXmlElement* element_kernel
            = TraitsXmlQuery::QueryFirstChildElement("SkelBasicPointLoader", element_current, "TFunctorEvalPoint");
    element_kernel = TraitsXmlQuery::QueryFirstChildElement("SkelBasicPointLoader", element_kernel);

    std::shared_ptr<Functor> kernel = core::FunctorFactory::GetInstance().CreateFunctorFromXml(element_kernel);

    std::shared_ptr<AbstractNodePointSField> node_point
            = std::dynamic_pointer_cast<AbstractNodePointSField>(PrimitiveFactory::GetInstance().CreateNewPrimitive(w_pt, *kernel));

/// TODO:@todo : load Properties1D from old style xml syntax where there is no xml "balise" <Properties> (directly ScalarProperties and then VectorProperties ...)
///              if we change the syntax, an additional xml marker should be retrieved with a QueryFirstChildElement
    node_point->properties().InitFromXml(element_current);

    return node_point;
}

//extern const char node_point_sfield[] = "NodePointSField";
//static core::GeometryPrimitiveLoader::Type<node_point_sfield, NodePointSFieldLoader> NodePointSFieldType;

extern const char skel_basic_point[] = "SkelBasicPoint"; //TODO:@todo: loading from convol-stile xml format
static core::GeometryPrimitiveLoader::XmlType<skel_basic_point, NodeOLDPointSFieldXmlLoader> SkelBasicPointType;

} // Close namespace convol

} // Close namespace expressive


