#include <convol/xmlloaders/primitives/NodeOLDTriangleSFieldLoader.h>

#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/TraitsXmlQuery.h>
#include <core/xmlloaders/GeometryPrimitiveLoader.h>
#include <core/xmlloaders/geometry/WeightedTriangleLoader.h>

#include <core/geometry/PrimitiveFactory.h>

#include <exception>

using namespace expressive::core;

namespace expressive {

namespace convol {

std::shared_ptr<AbstractNodeTriangleSField> NodeOLDTriangleSFieldXmlLoader::Create(const TiXmlElement *e, std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex)
{
    std::shared_ptr<WeightedTriangle> w_seg = WeightedTriangleLoader::Create(e, map_id_vertex);


    const TiXmlElement* element_current
            = TraitsXmlQuery::QueryFirstChildElement("SkelBasicTriangleLoader", e, "sf_blobtree_node");
    element_current = TraitsXmlQuery::QueryFirstChildElement("SkelBasicTriangleLoader", element_current, "NodeScalarField");

    int node_scalar_field_id = -1;
    TraitsXmlQuery::QueryIntAttribute("SkelBasicTriangleLoader", element_current, "id", &node_scalar_field_id);

    element_current = TraitsXmlQuery::QueryFirstChildElement("SkelBasicTriangleLoader", element_current, "TriangleSField");
    // Should use NodeTriangleSField::StaticName instead of "..." function but we are currently loading convol style file

    const TiXmlElement* element_kernel
            = TraitsXmlQuery::QueryFirstChildElement("SkelBasicTriangleLoader", element_current, "TFunctorEvalTriangle");
    element_kernel = TraitsXmlQuery::QueryFirstChildElement("SkelBasicTriangleLoader", element_kernel);

    std::shared_ptr<Functor> kernel = core::FunctorFactory::GetInstance().CreateFunctorFromXml(element_kernel);

    std::shared_ptr<AbstractNodeTriangleSField> node_segment
            = std::dynamic_pointer_cast<AbstractNodeTriangleSField>(PrimitiveFactory::GetInstance().CreateNewPrimitive(w_seg, *kernel));

/// TODO:@todo : load Properties1D from old style xml syntax where there is no xml "balise" <Properties> (directly ScalarProperties and then VectorProperties ...)
///              if we change the syntax, an additional xml marker should be retrieved with a QueryFirstChildElement
    node_segment->properties().InitFromXml(element_current);

    return node_segment;
}

extern const char skel_basic_triangle[] = "SkelBasicTriangle"; //TODO:@todo: loading from convol-stile xml format
static core::GeometryPrimitiveLoader::XmlType<skel_basic_triangle, NodeOLDTriangleSFieldXmlLoader> SkelBasicTriangleType;

} // Close namespace convol

} // Close namespace expressive


