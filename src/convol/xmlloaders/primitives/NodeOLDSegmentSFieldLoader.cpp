#include <convol/xmlloaders/primitives/NodeOLDSegmentSFieldLoader.h>

#include <core/functor/FunctorFactory.h>
#include <core/xmlloaders/TraitsXmlQuery.h>
#include <core/xmlloaders/GeometryPrimitiveLoader.h>
#include <core/xmlloaders/geometry/WeightedSegmentLoader.h>

#include <core/geometry/PrimitiveFactory.h>

#include <exception>

using namespace expressive::core;

namespace expressive {

namespace convol {

std::shared_ptr<AbstractNodeSegmentSField> NodeOLDSegmentSFieldXmlLoader::Create(const TiXmlElement *e, std::map<unsigned long, std::shared_ptr<WeightedVertex>>& map_id_vertex)
{
    std::shared_ptr<WeightedSegment> w_seg = WeightedSegmentLoader::Create(e, map_id_vertex);


    const TiXmlElement* element_current
            = TraitsXmlQuery::QueryFirstChildElement("SkelBasicSegmentLoader", e, "sf_blobtree_node");
    element_current = TraitsXmlQuery::QueryFirstChildElement("SkelBasicSegmentLoader", element_current, "NodeScalarField");

    int node_scalar_field_id = -1;
    TraitsXmlQuery::QueryIntAttribute("SkelBasicSegmentLoader", element_current, "id", &node_scalar_field_id);

    element_current = TraitsXmlQuery::QueryFirstChildElement("SkelBasicSegmentLoader", element_current, "SegmentSField");
    // Should use NodeSegmentSField::StaticName instead of "..." function but we are currently loading convol style file

    const TiXmlElement* element_kernel
            = TraitsXmlQuery::QueryFirstChildElement("SkelBasicSegmentLoader", element_current, "TFunctorEvalSegment");
    element_kernel = TraitsXmlQuery::QueryFirstChildElement("SkelBasicSegmentLoader", element_kernel);

    std::shared_ptr<Functor> kernel = core::FunctorFactory::GetInstance().CreateFunctorFromXml(element_kernel);

    std::shared_ptr<AbstractNodeSegmentSField> node_segment
            = std::dynamic_pointer_cast<AbstractNodeSegmentSField>(PrimitiveFactory::GetInstance().CreateNewPrimitive(w_seg, *kernel));

/// TODO:@todo : load Properties1D from old style xml syntax where there is no xml "balise" <Properties> (directly ScalarProperties and then VectorProperties ...)
///              if we change the syntax, an additional xml marker should be retrieved with a QueryFirstChildElement
    node_segment->properties().InitFromXml(element_current);

    return node_segment;
}

//extern const char node_segment_sfield[] = "NodeSegmentSField";
//static core::GeometryPrimitiveLoader::Type<node_segment_sfield, NodeSegmentSFieldLoader> NodeSegmentSFieldType;

extern const char skel_basic_segment[] = "SkelBasicSegment"; //TODO:@todo: loading from convol-stile xml format
static core::GeometryPrimitiveLoader::XmlType<skel_basic_segment, NodeOLDSegmentSFieldXmlLoader> SkelBasicSegmentType;

} // Close namespace convol

} // Close namespace expressive


