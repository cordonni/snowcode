/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BlobtreeNodeFactory.h

   Language: C++

   License: Convol license

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Header file for a BlobtreeNode factory (created object should
                derive from BlobtreeNode).
                This factory can create the required object from a name
                (the one returned by StaticName of a given BlobtreeNode)
                and a Kernel.

   Platform Dependencies: None
*/

#pragma once
#ifndef EXPRESSIVE_BLOBTREE_NODE_FACTORY_T_H_
#define EXPRESSIVE_BLOBTREE_NODE_FACTORY_T_H_

// core dependencies
#include <core/CoreRequired.h>
    // functor
    #include <core/functor/Functor.h>
    #include <core/functor/FunctorTypeMapT.h>
    // utils
    #include <ork/core/Logger.h>

// convol dependencies
#include <convol/ConvolRequired.h>
    // blobtreenode
    #include <convol/blobtreenode/BlobtreeNode.h>

namespace expressive {

namespace convol {

/*! \brief ...
 */
class CONVOL_API BlobtreeNodeFactory
{
public:
    typedef std::shared_ptr<BlobtreeNode> (*BuilderFunction) (const core::Functor &);

    typedef core::FunctorTypeMapT<BuilderFunction> BtNodeFactory;
    typedef std::map<std::string, BtNodeFactory> MapperType; // 1 BtNodeFactory per kind of BlobtreeNode registered
    
    static BlobtreeNodeFactory& GetInstance();

    void SetDefaultBlobtreeNodeName(const std::string &node_name);

    std::shared_ptr<BlobtreeNode> CreateNewBlobtreeNode();

    std::shared_ptr<BlobtreeNode> CreateNewBlobtreeNode(const core::Functor &functor);

    std::shared_ptr<BlobtreeNode> CreateNewBlobtreeNode( const std::string &node_name,
                                                         const core::Functor &functor );

    std::string GetXmlFunctorFieldFromBlobtreeNode(std::string node_name);

    const BtNodeFactory& GetBtNodeFactory(const std::string &node_name) const;

    template<typename TFunctor, typename TBlobtreeNode>
    void RegisterTemplatedBlobtreeNode(BuilderFunction f);

    template <class TFunctor, class TBlobtreeNode>
    class Type
    {
    public:
        static std::shared_ptr<BlobtreeNode> ctor (const core::Functor &functor)
        {
            TFunctor const* casted_functor = dynamic_cast<TFunctor const*>(&functor);
            return std::make_shared<TBlobtreeNode>(*casted_functor);
        }
        
        Type()
        {
            BlobtreeNodeFactory::GetInstance().RegisterTemplatedBlobtreeNode<TFunctor,TBlobtreeNode>(ctor);
        }
    };

    template<typename TBlobtreeNode>
    class XmlFunctorField
    {
    public:
        XmlFunctorField()
        {
            BlobtreeNodeFactory::GetInstance().blobtree_node_xml_functor_field_[TBlobtreeNode::StaticName()]
                    = TBlobtreeNode::XmlFunctorField();
        }
    };

private:
    MapperType types;

    std::string default_blobtreenode_name_;

    std::map<std::string, std::string> blobtree_node_xml_functor_field_;
};
    


template <typename TFunctor, typename TBlobtreeNode>
void BlobtreeNodeFactory::RegisterTemplatedBlobtreeNode(BuilderFunction f)
{
    const core::FunctorTypeTree& type_tree = TFunctor::StaticType();

    if(!types[TBlobtreeNode::StaticName()].insert(type_tree,f))
    {
        ork::Logger::ERROR_LOGGER->log("CONVOL", "BlobtreeNodeFactory : BlobtreeNode<Functor> already registered" );
        throw std::exception();
    }
}

} // Close namespace convol

} // Close namespace expressive

#endif // EXPRESSIVE_BLOBTREE_NODE_FACTORY_T_H_
