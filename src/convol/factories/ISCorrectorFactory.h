/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ISCorrectorFactory.h

   Language: C++

   License: Convol license

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Header file for a IntegralSurfaceCorrector factory. 
                This factory can create the required object from a given Kernel.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_IS_CORRECTOR_FACTORY_T_H_
#define CONVOL_IS_CORRECTOR_FACTORY_T_H_

// core dependencies
#include <core/CoreRequired.h>
    // functor
    #include <core/functor/FunctorTypeMapT.h>


#include <convol/ConvolRequired.h>
    #include <convol/blobtreenode/nodeoperator/IntegralSurfaceCorrector.h>
    #include <convol/functors/kernels/Kernel.h>

namespace expressive {

namespace convol {

/*! \brief ...
 *  WARNING : Primitives registered in this factory should derive from a WeightedGeometryPrimitive
 *            and should contains a typedef defining BaseGeometry (cf in NodeSegmentSField).
 *            Furthermore primtives should be templated by a kernel and define FunctorKernel by a typedef.
 */
class CONVOL_API ISCorrectorFactory
{
public:
    typedef std::shared_ptr<AbstractIntegralSurfaceCorrector> (*BuilderFunction) (const convol::Kernel &ker, BlobtreeNode* );
    typedef typename core::FunctorTypeMapT<BuilderFunction> ISCFactory;
    

    static ISCorrectorFactory& GetInstance();

    std::shared_ptr<AbstractIntegralSurfaceCorrector> CreateNewCorrector(const convol::Kernel &homothetic_kernel, BlobtreeNode* bt_node);

    template<typename THomotheticKernel, typename TStandardKernel>
    void RegisterTemplatedCorrector(BuilderFunction f);

    template <class THomotheticKernel, class TStandardKernel>
    class Type
    {
    public:
        static std::shared_ptr<AbstractIntegralSurfaceCorrector> ctor (const convol::Kernel &ker, BlobtreeNode* bt_node)
        {
//            const std::shared_ptr<THomotheticKernel> casted_ker = std::dynamic_pointer_cast<THomotheticKernel>(ker);
//            if (casted_ker == nullptr) {
//                return nullptr;
//            }

//            FunctorFactory::GetInstance().CreateFunctor(TStandardKernel::StaticType());
            return std::make_shared<IntegralSurfaceCorrector<THomotheticKernel,TStandardKernel> >(static_cast<const THomotheticKernel&>(ker), bt_node);
        }
        
        Type()
        {
            ISCorrectorFactory::GetInstance().RegisterTemplatedCorrector<THomotheticKernel,TStandardKernel>(ctor);
        }
    };

private:
    ISCFactory types;
};
    
template <typename THomotheticKernel, typename TStandardKernel>
void ISCorrectorFactory::RegisterTemplatedCorrector(BuilderFunction f)
{
    const core::FunctorTypeTree& type_tree = THomotheticKernel::StaticType();
    if(!types.insert(type_tree,f))
    {
        ork::Logger::ERROR_LOGGER->log("CONVOL", "ISCorrectorFactory : ImplicitSurfaceCorrector<TStandardKernel> already registered under THomotheticKernel");
        throw std::exception();
    }
}

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_IS_CORRECTOR_FACTORY_T_H_
