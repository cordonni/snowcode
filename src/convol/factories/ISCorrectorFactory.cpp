#include <convol/factories/ISCorrectorFactory.h>

// core dependencies
#include <ork/core/Logger.h>
//#include <core/functor/FunctorFactory.h>

namespace expressive {

namespace convol {

ISCorrectorFactory& ISCorrectorFactory::GetInstance()
{
    static ISCorrectorFactory INSTANCE = ISCorrectorFactory();
    return INSTANCE;
}

auto ISCorrectorFactory::CreateNewCorrector(const convol::Kernel &homothetic_kernel, BlobtreeNode*  bt_node)
-> std::shared_ptr<AbstractIntegralSurfaceCorrector>
{
    BuilderFunction builder = nullptr;
    const core::FunctorTypeTree functor_type = homothetic_kernel.Type();

    if(types.find(functor_type, builder))
    {
        return builder(homothetic_kernel, bt_node);
    }
    else
    {
        ork::Logger::ERROR_LOGGER->log("CONVOL", "ISCorrectorFactory : functor not registered" );
        throw std::exception();
    }
}


} // Close namespace convol

} // Close namespace expressive

