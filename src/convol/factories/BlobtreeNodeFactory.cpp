#include <convol/factories/BlobtreeNodeFactory.h>
#include <core/functor/FunctorFactory.h>

using namespace expressive::core;

namespace expressive {

namespace convol {

BlobtreeNodeFactory& BlobtreeNodeFactory::GetInstance()
{
    static BlobtreeNodeFactory INSTANCE = BlobtreeNodeFactory();
    return INSTANCE;
}

void BlobtreeNodeFactory::SetDefaultBlobtreeNodeName(const std::string &node_name)
{
    default_blobtreenode_name_ = node_name;
}

auto BlobtreeNodeFactory::CreateNewBlobtreeNode()
-> std::shared_ptr<BlobtreeNode>
{
    std::shared_ptr<core::Functor> f = FunctorFactory::GetInstance().CreateDefaultFunctor();
    if (f == nullptr) {
        return nullptr;
    }
    return CreateNewBlobtreeNode(default_blobtreenode_name_, *f);
}

auto BlobtreeNodeFactory::CreateNewBlobtreeNode(const core::Functor &functor)
-> std::shared_ptr<BlobtreeNode>
{
    return CreateNewBlobtreeNode(default_blobtreenode_name_, functor);
}

auto BlobtreeNodeFactory::CreateNewBlobtreeNode(const std::string &node_name,
                                                const core::Functor &functor)
-> std::shared_ptr<BlobtreeNode>
{
    auto it = types.find(node_name);
    if(it != types.end())
    {
        BuilderFunction builder = nullptr;
        const core::FunctorTypeTree functor_type = functor.Type();
        
        if(it->second.find(functor_type, builder))
        {
            return builder(functor);
        }
        else
        {
            ork::Logger::ERROR_LOGGER->log("CONVOL", "BlobtreeNodeFactory : functor not registered for the primitive" );
            throw std::exception();
        }
    }
    else
    {
        ork::Logger::ERROR_LOGGER->log("CONVOL", "BlobtreeNodeFactory : node not registered" );
        throw std::exception();
    }
//    return nullptr; // unreachable
}

auto BlobtreeNodeFactory::GetBtNodeFactory(const std::string &node_name) const
-> const BtNodeFactory&
{
    auto it = types.find(node_name);
    if(it != types.end()) {
        return it->second;
    } else {
        ork::Logger::ERROR_LOGGER->log("CONVOL", "BlobtreeNodeFactory : node not registered" );
        throw std::exception();
    }
}


std::string BlobtreeNodeFactory::GetXmlFunctorFieldFromBlobtreeNode(std::string node_name)
{
    auto it = blobtree_node_xml_functor_field_.find(node_name);
    if(it != blobtree_node_xml_functor_field_.end()) {
        return it->second;
    } else {
        ork::Logger::ERROR_LOGGER->log("CONVOL", "BlobtreeNodeFactory : XmlFunctorField not regstered for the BlobtreeNode" );
        throw std::exception();
    }
//    return std::string(); // unreachable

}


} // Close namespace convol

} // Close namespace expressive

