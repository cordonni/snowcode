#include <convol/primitives/NodeSegmentSFieldT.h>

#include <core/geometry/PrimitiveFactory.h>

// List of all kernel that can be used with a NodeSegmentSField
// (this is required to register them in the PrimitiveFacotry)

#include <convol/functors/kernels/cauchyinverse/HomotheticCauchy.h>
#include <convol/functors/kernels/cauchyinverse/HomotheticInverse.h>
#include <convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.h>
#include <convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.h>

#include <convol/functors/kernels/cauchyinverse/HornusCauchy.h>
#include <convol/functors/kernels/cauchyinverse/HornusInverse.h>
#include <convol/functors/kernels/compactpolynomial/HornusCompactPolynomial.h>

#include <convol/functors/kernels/cauchyinverse/StandardCauchy.h>
#include <convol/functors/kernels/cauchyinverse/StandardInverse.h>
#include <convol/functors/kernels/compactpolynomial/StandardCompactPolynomial.h>

#include <convol/functors/kernels/HomotheticDistanceT.hpp>
#include <convol/functors/kernels/compactpolynomial/CompactPolynomial.h>

using namespace expressive::core;

namespace expressive {

namespace convol {

void AbstractNodeSegmentSField::InitFromXml(const TiXmlElement *element)
{
///// TODO:@todo : load Properties1D from old style xml syntax where there is no xml "balise" <Properties> (directly ScalarProperties and then VectorProperties ...)
/////              if we change the syntax, an additional xml marker should be retrieved with a QueryFirstChildElement
    properties_.InitFromXml(element);
}

//////////////////////////////
/// Homothetic integration ///
//////////////////////////////

static PrimitiveFactory::Type<  NodeSegmentSFieldT<HomotheticCompactPolynomial4> >
                                                                NodeSegmentSFieldT_HomotheticCompactPolynomial4_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HomotheticCompactPolynomial6> >
                                                                NodeSegmentSFieldT_HomotheticCompactPolynomial6_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HomotheticCompactPolynomial8> >
                                                                NodeSegmentSFieldT_HomotheticCompactPolynomial8_Type;

static PrimitiveFactory::Type<  NodeSegmentSFieldT<HomotheticCauchy3> >
                                                                NodeSegmentSFieldT_HomotheticCauchy3_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HomotheticCauchy4> >
                                                                NodeSegmentSFieldT_HomotheticCauchy4_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HomotheticCauchy5> >
                                                                NodeSegmentSFieldT_HomotheticCauchy5_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HomotheticCauchy6> >
                                                                NodeSegmentSFieldT_HomotheticCauchy6_Type;

static PrimitiveFactory::Type<  NodeSegmentSFieldT<HomotheticInverse3> >
                                                                NodeSegmentSFieldT_HomotheticInverse3_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HomotheticInverse4> >
                                                                NodeSegmentSFieldT_HomotheticInverse4_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HomotheticInverse5> >
                                                                NodeSegmentSFieldT_HomotheticInverse5_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HomotheticInverse6> >
                                                                NodeSegmentSFieldT_HomotheticInverse6_Type;

static PrimitiveFactory::Type<  NodeSegmentSFieldT<CompactedHomotheticInverse3> >
                                                                NodeSegmentSFieldT_CompactedHomotheticInverse3_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<CompactedHomotheticInverse4> >
                                                                NodeSegmentSFieldT_CompactedHomotheticInverse4_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<CompactedHomotheticInverse5> >
                                                                NodeSegmentSFieldT_CompactedHomotheticInverse5_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<CompactedHomotheticInverse6> >
                                                                NodeSegmentSFieldT_CompactedHomotheticInverse6_Type;

//////////////////////////
/// Hornus integration ///
//////////////////////////

static PrimitiveFactory::Type<  NodeSegmentSFieldT<HornusCompactPolynomial4> >
                                                                NodeSegmentSFieldT_HornusCompactPolynomial4_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HornusCompactPolynomial6> >
                                                                NodeSegmentSFieldT_HornusCompactPolynomial6_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HornusCompactPolynomial8> >
                                                                NodeSegmentSFieldT_HornusCompactPolynomial8_Type;

static PrimitiveFactory::Type<  NodeSegmentSFieldT<HornusCauchy3> >
                                                                NodeSegmentSFieldT_HornusCauchy3_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HornusCauchy4> >
                                                                NodeSegmentSFieldT_HornusCauchy4_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HornusCauchy5> >
                                                                NodeSegmentSFieldT_HornusCauchy5_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HornusCauchy6> >
                                                                NodeSegmentSFieldT_HornusCauchy6_Type;

static PrimitiveFactory::Type<  NodeSegmentSFieldT<HornusInverse3> >
                                                                NodeSegmentSFieldT_HornusInverse3_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HornusInverse4> >
                                                                NodeSegmentSFieldT_HornusInverse4_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HornusInverse5> >
                                                                NodeSegmentSFieldT_HornusInverse5_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<HornusInverse6> >
                                                                NodeSegmentSFieldT_HornusInverse6_Type;

////////////////////////////
/// Standard integration ///
////////////////////////////

static PrimitiveFactory::Type<  NodeSegmentSFieldT<StandardCompactPolynomial4> >
                                                                NodeSegmentSFieldT_StandardCompactPolynomial4_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<StandardCompactPolynomial6> >
                                                                NodeSegmentSFieldT_StandardCompactPolynomial6_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<StandardCompactPolynomial8> >
                                                                NodeSegmentSFieldT_StandardCompactPolynomial8_Type;

static PrimitiveFactory::Type<  NodeSegmentSFieldT<StandardCauchy3> >
                                                                NodeSegmentSFieldT_StandardCauchy3_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<StandardCauchy4> >
                                                                NodeSegmentSFieldT_StandardCauchy4_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<StandardCauchy5> >
                                                                NodeSegmentSFieldT_StandardCauchy5_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<StandardCauchy6> >
                                                                NodeSegmentSFieldT_StandardCauchy6_Type;

static PrimitiveFactory::Type<  NodeSegmentSFieldT<StandardInverse3> >
                                                                NodeSegmentSFieldT_StandardInverse3_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<StandardInverse4> >
                                                                NodeSegmentSFieldT_StandardInverse4_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<StandardInverse5> >
                                                                NodeSegmentSFieldT_StandardInverse5_Type;
static PrimitiveFactory::Type<  NodeSegmentSFieldT<StandardInverse6> >
                                                                NodeSegmentSFieldT_StandardInverse6_Type;

///////////////////////////
/// Homothetic distance ///
///////////////////////////

static PrimitiveFactory::Type<  NodeSegmentSFieldT< HomotheticDistanceT<CompactPolynomial<6> > > >
                                                                NodeSegmentSFieldT_HomotheticDistance_CompactPolynomial6_Type;


} // Close namespace convol

} // Close namespace expressive
