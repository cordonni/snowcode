/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file:  NodeTriangleSFieldT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header for a triangle generating scalar field.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_TRIANGLE_SFIELD_T_H_
#define CONVOL_TRIANGLE_SFIELD_T_H_

// core dependencies
#include <core/CoreRequired.h>
    // geometry
    #include <core/geometry/AABBox.h>
    #include <core/geometry/WeightedTriangle.h>
    // functors
    #include <core/functor/Functor.h>

// convol dependencies
#include <convol/blobtreenode/NodeScalarField.h>
#include <convol/properties/Properties0D.h>

namespace expressive {

namespace convol {

/*! \brief Superclass to  NodeTriangleSFieldT.
 *         This class has been added to grant access to geometrical data without knowing 
 *         which functor specializes the class  NodeTriangleSFieldT.
 *         ie if you know you have a  NodeTriangleSFieldT, but you don't know with which kernel,
 *            you can still access geometrical data.
 *          NodeTriangleSFieldT is the ONLY CLASS allowed to inherit from  AbstractNodeTriangleSFieldT !!!!
 *  
 */
class CONVOL_API AbstractNodeTriangleSField
        : public NodeScalarField, public core::WeightedTriangle
{
public:
    
    typedef core::WeightedTriangle BaseGeometry;

    typedef core::WeightedTriangle WeightedTriangle;

    EXPRESSIVE_MACRO_NAME("NodeTriangleSField")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractNodeTriangleSField(const Point& p1, const Point& p2, const Point& p3,
                           Scalar weight1, Scalar weight2, Scalar weight3)
        : NodeScalarField(StaticName()), WeightedTriangle(p1,p2,p3,weight1,weight2,weight3), properties_()
    {
        // a triangle must not be 0 in area
        assert(p1 != p2);  assert(p1 != p3); assert(p2 != p3);
    }
    AbstractNodeTriangleSField(const WeightedTriangle &w_tri)
        : NodeScalarField(StaticName()), WeightedTriangle(w_tri), properties_()
    { }
    // Copy constructor
    AbstractNodeTriangleSField(const AbstractNodeTriangleSField &rhs, bool copyParent = false)
        : NodeScalarField(rhs, copyParent), WeightedTriangle(rhs), properties_(rhs.properties_)
    { }

    virtual ~ AbstractNodeTriangleSField(){}

//    virtual std::string ParameterName() const
//    {
//    //TODO: @todo : il faudrait sans doute passer une fonction ParameterName dans la geometrie
//        std::ostringstream name;
//        name.setf(std::ios::scientific,std::ios::floatfield);
//        name.precision(2);
//        name << "[p1(" << triangle_.p1()[0] << "," << triangle_.p1()[1] << "," << triangle_.p1()[2] << "),";
//        name << "weight_p1(" << triangle_.weight_p1() << "),";
//        name << "p2(" <<  triangle_.p2()[0] << "," <<  triangle_.p2()[1] << "," <<  triangle_.p2()[2] << "),";
//        name << "weight_p2(" << triangle_.weight_p2() << ")]";
//        name << "p3(" <<  triangle_.p3()[0] << "," <<  triangle_.p3()[1] << "," <<  triangle_.p3()[2] << "),";
//        name << "weight_p3(" << triangle_.weight_p3() << ")]";
//        return name.str();
//    }

    ///////////
    /// Xml ///
    ///////////

    void InitFromXml(const TiXmlElement *element);

    // Forcing using baseGeometry::ToXml method.
    using BaseGeometry::ToXml;
    // Forcing using NodeScalarField::AddXmlAttributes method.
    using NodeScalarField::AddXmlAttributes;

    //    virtual TiXmlElement * GetXml() const = 0;
//    //-------------------------------------------------------------------------
//    void AddXmlSimpleAttribute(TiXmlElement * element) const
//    {
//        // Common ScalarField Complex attributes
//        Base::AddXmlSimpleAttribute(element);

//        element->SetDoubleAttribute("weight_p1",weight_p1());
//        element->SetDoubleAttribute("weight_p2",weight_p2());
//        element->SetDoubleAttribute("weight_p3",weight_p3());
//    }
//    //-------------------------------------------------------------------------
//    void AddXmlComplexAttribute(TiXmlElement * element) const
//    {
//        // Common ScalarField Complex attributes
//        Base::AddXmlComplexAttribute(element);

//        TiXmlElement * element_p1 = new TiXmlElement( "p1" );
//        element_p1->SetDoubleAttribute("x",p1()[0]);
//        element_p1->SetDoubleAttribute("y",p1()[1]);
//        element_p1->SetDoubleAttribute("z",p1()[2]);
//        element->LinkEndChild(element_p1);
//        TiXmlElement * element_p2 = new TiXmlElement( "p2" );
//        element_p2->SetDoubleAttribute("x",p2()[0]);
//        element_p2->SetDoubleAttribute("y",p2()[1]);
//        element_p2->SetDoubleAttribute("z",p2()[2]);
//        element->LinkEndChild(element_p2);
//        TiXmlElement * element_p3 = new TiXmlElement( "p3" );
//        element_p2->SetDoubleAttribute("x",p3()[0]);
//        element_p2->SetDoubleAttribute("y",p3()[1]);
//        element_p2->SetDoubleAttribute("z",p3()[2]);
//        element->LinkEndChild(element_p3);

//        // Scalar properties
//        TiXmlElement * element_scalar_prop = new TiXmlElement( "ScalarProperties" );
//        for(unsigned int i = 0; i<Traits::SF_SCALAR_PROPERTIES_CARDINAL; ++i)
//        {
//            // For now we know there is only 1 value per property. So no need to be as fancy as we were for Segments
//            TiXmlElement * element_scalar_prop_i = new TiXmlElement( Traits::getNameFromScalarProperty( ESFScalarProperties(i) ) );
//            element_scalar_prop_i->SetDoubleAttribute("value",scalar_properties_[i]);
//            element_scalar_prop->LinkEndChild(element_scalar_prop_i);
//        }
//        element->LinkEndChild(element_scalar_prop);
//        // Vector properties
//        TiXmlElement * element_vector_prop = new TiXmlElement( "VectorProperties" );
//        for(unsigned int i = 0; i<Traits::SF_VECTOR_PROPERTIES_CARDINAL; ++i)
//        {
//            TiXmlElement * element_vector_prop_i = new TiXmlElement( Traits::getNameFromVectorProperty( ESFVectorProperties(i) ) );
//            for(unsigned int t=0; t<Vector::size_; ++t) // for adaptability to any Vector dimension
//            {
//                std::ostringstream val_t;
//                val_t << "_" << t;
//                element_vector_prop_i->SetDoubleAttribute(val_t.str(),vector_properties_[i][t]);
//            }
//            element_vector_prop->LinkEndChild(element_vector_prop_i);
//        }
//        element->LinkEndChild(element_vector_prop);
//    }

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual inline Scalar Eval(const Point& point) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalProperties(const Point& point,
                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                ) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const =0;
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalHomotheticValues(const Point& point, Scalar& value_res, Vector& grad_res) const = 0;

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool IsCompact() const = 0;
    //-------------------------------------------------------------------------
    virtual inline Scalar GetDistanceBound(Scalar value) const = 0;
    //-------------------------------------------------------------------------
    virtual core::AABBox GetAxisBoundingBox(Scalar value) const = 0;

    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const = 0;

    /////////////////
    /// Modifiers ///
    /////////////////

    virtual void updated()
    {
        WeightedTriangle::updated();
        this->InvalidateBoundingBox();
    }

    /////////////////
    /// Accessors ///
    /////////////////

    // Return the TFunctorKernel (see template of NodeTriangleSFieldT)
    // WARNING : Should be use mostly for interface or research purposes.
    virtual const core::Functor& kernel_functor() const = 0;
    virtual core::Functor& kernel_functor() = 0;
    //-------------------------------------------------------------------------
    Properties0D& properties() {  return properties_; }
    const Properties0D& properties() const {  return properties_; }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    // WARNING : For now Triangle can have only 1 value per property
    Properties0D properties_;

};

/*! \brief Field generated around a triangle.
 *           The given  TFunctorKernel specify the function that
 *           compute the scalar field given a triangle. 
 *         WARNING : Some of this class's functions does not work properly if the middle of the triangle
 *                   is not included in the surface created by using the given iso value.
 *                   Ex : GetAPointAt
 *  
 */
template<typename  TFunctorKernel>
class  NodeTriangleSFieldT :
    public  AbstractNodeTriangleSField
{
public:
    typedef AbstractNodeTriangleSField::BaseGeometry BaseGeometry;
    typedef TFunctorKernel FunctorKernel;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

     NodeTriangleSFieldT(const Point& p1, const Point& p2, const Point& p3,
                    Scalar weight1, Scalar weight2,  Scalar weight3, 
                    const  TFunctorKernel& kernel)
        :  AbstractNodeTriangleSField(p1,p2,p3,weight1,weight2,weight3),
          kernel_(kernel)
    { }

     NodeTriangleSFieldT(const WeightedTriangle& w_tri, const TFunctorKernel& kernel)
         :   AbstractNodeTriangleSField(w_tri),
             kernel_(kernel)
     {}

     // Copy constructor
     NodeTriangleSFieldT(const  NodeTriangleSFieldT &rhs, bool copyParent = false)
        :  AbstractNodeTriangleSField(rhs, copyParent),
          kernel_(rhs.kernel_)
    { }

    virtual ~NodeTriangleSFieldT(){}

    //////////////////////
    /// Clone Function ///
    //////////////////////

    virtual  std::shared_ptr<ScalarField> CloneForEval() const
    {
        return std::make_shared<NodeTriangleSFieldT>(*this);
    }

     virtual std::shared_ptr<core::GeometryPrimitiveT<core::WeightedPoint> > clone(unsigned int /*start*/, unsigned int /*end*/) const
     {
         return std::make_shared<NodeTriangleSFieldT>(*this, true);
     }


//    //------------------------------------------------------------------------
//    virtual std::string FunctorName() const
//    {
//        std::ostringstream name;
//        name << "<";
//        name << kernel_.Name();
//        name << kernel_.FunctorName();
//        name << kernel_.ParameterName() << ">";
//        return name.str();
//    }

    ///////////////////
    // Xml functions //
    ///////////////////

     //-------------------------------------------------------------------------
     virtual void AddXmlAttributes(TiXmlElement * element) const
     {
         AbstractNodeTriangleSField::AddXmlAttributes(element);
         TiXmlElement *element_functor_eval_seg = new TiXmlElement( "TFunctorKernel" );
         element->LinkEndChild(element_functor_eval_seg);
         TiXmlElement * element_child = kernel_.ToXml();
         element_functor_eval_seg->LinkEndChild(element_child);

         TiXmlElement * element_properties = properties_.ToXml();
         element->LinkEndChild(element_properties);
     }
//    virtual TiXmlElement * GetXml() const
//    {
//        TiXmlElement *element_base = new TiXmlElement(  AbstractNodeTriangleSFieldT<typename  TFunctorKernel::Traits>::StaticName() );

//        AddXmlSimpleAttribute(element_base);
//        AddXmlComplexAttribute(element_base);
//        AddXmlFunctor(element_base);

//        return element_base;
//    }
//    //-------------------------------------------------------------------------
//    void AddXmlSimpleAttribute(TiXmlElement * element) const
//    {
//        Base::AddXmlSimpleAttribute(element);
//    }
//    //-------------------------------------------------------------------------
//    void AddXmlComplexAttribute(TiXmlElement * element) const
//    {
//        Base::AddXmlComplexAttribute(element);
//    }
//    //-------------------------------------------------------------------------
//    void AddXmlFunctor(TiXmlElement * element) const
//    {
//        TiXmlElement* element_functor_eval_triangle = new TiXmlElement( " TFunctorKernel" );
//        element->LinkEndChild(element_functor_eval_triangle);
//        TiXmlElement* element_child = kernel_.GetXml();
//        element_functor_eval_triangle->LinkEndChild(element_child);
//    }

    ////////////////////////////
    /// Evaluation Functions ///
    ////////////////////////////

    virtual inline Scalar Eval(const Point& point) const
    {
        return kernel_(this->opt_w_tri_, point);
    }
    //-------------------------------------------------------------------------
    virtual inline Vector EvalGrad(const Point& point, Scalar epsilon) const
    {
        return kernel_.EvalGrad(this->opt_w_tri_, point, epsilon);
    }
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        kernel_.EvalValueAndGrad(this->opt_w_tri_, point, epsilon_grad, value_res, grad_res);
    }
    //------------------------------------------------------------------------/
    virtual void EvalProperties(const Point& /*point*/,
                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                ) const
    {
        properties_.InlineEvalProperties(scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
    }
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
    {
       kernel_.EvalValueAndGrad(this->opt_w_tri_, point, epsilon_grad, value_res, grad_res);
       properties_.InlineEvalProperties(scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
    }
    //-------------------------------------------------------------------------
    virtual void EvalHomotheticValues(const Point& point, Scalar& value_res, Vector& grad_res) const
    {
        kernel_.EvalHomotheticValues(this->opt_w_tri_, point, value_res, grad_res);
    }


    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool IsCompact() const
    {
        return kernel_.isCompact();
    }

    virtual inline Scalar GetDistanceBound(Scalar value) const
    {
        return kernel_.GetEuclidDistBound(*this, value);
    }

    virtual core::AABBox GetAxisBoundingBox(Scalar value) const;

    /** \brief Gives a point where the potential created by the given triangle
    *        primitive is value. The triangle is considered alone.
    *        This algorithm uses Newton convergence and dichotomy.
    *        Restriction : the middle point of the given triangle must be such that the potential
    *        created by the triangle on its middle point is greater than this value.
    *        ie the surface defined by the given value includes the middle point of the triangle.
    *   \param value
    *    \param epsilon We want the result to be at least epsilon close to the surface with respect to the
    *                   distance Vector.norm(), we suppose this norm to be the one associated with the dot product Vector.operator |
    *    \return point p such that operator(seg,p) == iso_value
    */
    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const;

     virtual void GetPrecisionAxisBoundingBoxes(Scalar value, std::list<core::PrecisionAxisBoundingBox> &pboxes) const
     {
         opt_w_tri_.GetPrecisionAxisBoundingBoxes(value, pboxes);
     }

    ///////////////
    // Modifiers //
    ///////////////

    ///////////////
    // Accessors //
    ///////////////

     virtual const core::Functor& kernel_functor() const { return kernel_; }
     virtual core::Functor& kernel_functor() { return kernel_; }
     TFunctorKernel& kernel() { return kernel_; }
    //-------------------------------------------------------------------------
private:

     TFunctorKernel kernel_;

};

template<typename  TFunctorKernel>
core::AABBox  NodeTriangleSFieldT< TFunctorKernel>::GetAxisBoundingBox(Scalar value) const
{
    core::AABBox res = WeightedTriangle::GetAxisBoundingBox();

    Scalar dist_bound = GetDistanceBound(value);

    for(unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i)
    {
        res.min_[i] -= dist_bound;
        res.max_[i] += dist_bound;
    }

    return res;
}


} // Close namespace convol

} // Close namespace expressive

#include <convol/primitives/NodeTriangleSFieldT.hpp>

#endif // CONVOL_TRIANGLE_SFIELD_T_H_
