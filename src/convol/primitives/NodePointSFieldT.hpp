
#include <core/geometry/VectorTools.h> //TODO:@todo : I do not like this class !!!
#include <convol/tools/ConvergenceTools.h>

namespace expressive {

namespace convol {

template<typename  TFunctorKernel>
Point NodePointSFieldT< TFunctorKernel>::GetAPointAt(Scalar value, Scalar epsilon) const
{
    Vector search_dir = Vector::Zero();
    search_dir[0] = 1.0;

    Scalar max_bound = kernel_.GetEuclidDistBound(*this, value);
    Scalar min_bound = 0.0;
    Scalar start_absc = max_bound/2.0;

    // Accuracy for gradient computation
    // Chosen depending on the object size if epsilon is big, and to epsilon otherwise
    Scalar grad_epsilon = max_bound*0.0001;    // arbitrary
    if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }

    Point res;
    Convergence::SafeNewton1D(
                        *this, // the NodePointSField
                        *this, search_dir, //the Point // together give the line on which we step
                        min_bound,                                // security minimum bound
                        max_bound,                // security maximum bound
                        start_absc,            // point at which we start the search, given in 1D
                        value,
                        epsilon,
                        grad_epsilon,
                        10,
                        res);
    return res;
}


} // Close namespace convol
} // Close namespace expressive

