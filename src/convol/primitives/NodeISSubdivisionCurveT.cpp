#include <convol/primitives/NodeISSubdivisionCurveT.h>

#include <core/geometry/PrimitiveFactory.h>

// List of all kernel that can be used with a NodeSegmentSField
// (this is required to register them in the PrimitiveFacotry)


#include <convol/functors/kernels/cauchyinverse/HomotheticCauchy.h>
#include <convol/functors/kernels/cauchyinverse/HomotheticInverse.h>
#include <convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.h>
#include <convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.h>

using namespace expressive::core;

namespace expressive {

namespace convol {

void AbstractNodeISSubdivisionCurve::InitFromXml(const TiXmlElement *element)
{
    //TODO:@todo : rendre les choses plus propre

///// TODO:@todo : load Properties1D from old style xml syntax where there is no xml "balise" <Properties> (directly ScalarProperties and then VectorProperties ...)
/////              if we change the syntax, an additional xml marker should be retrieved with a QueryFirstChildElement
    properties_.InitFromXml(element);
}

void AbstractNodeISSubdivisionCurve::set_segments()
{
    vec_seg_.clear();

    if (subdivision_.size() < 2) {
        length_ = 0.0;
        return;
    }
    //TODO:@todo : parcourir les points pour créer de OptWeightedSegment et initialiser les bounding box associé
    auto it1 = subdivision_.begin();
    auto it2 = subdivision_.begin();
    ++it2;
    Scalar length = 0.0;
    for( ; it2 != subdivision_.end(); ++it1, ++it2)
    {
        //TODO:@todo : try to do this with a minimal amount of recopie and call to constructor!
        core::OptWeightedSegment seg(*it1, *it2);
        AABBox bbox; // will be initialized in PrepareForEval ...
        length += seg.length();
        vec_seg_.push_back(std::make_pair(bbox,seg));
    }

    if (isLoop()) {
        it2 = subdivision_.begin();
        //TODO:@todo : try to do this with a minimal amount of recopie and call to constructor!
        core::OptWeightedSegment seg(*it1, *it2);
        AABBox bbox; // will be initialized in PrepareForEval ...
        length += seg.length();
        vec_seg_.push_back(std::make_pair(bbox,seg));
    }

    length_ = length;
}



static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<HomotheticCompactPolynomial4> >
                                                                NodeISSubdivisionCurveT_HomotheticCompactPolynomial4_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<HomotheticCompactPolynomial6> >
                                                                NodeISSubdivisionCurveT_HomotheticCompactPolynomial6_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<HomotheticCompactPolynomial8> >
                                                                NodeISSubdivisionCurveT_HomotheticCompactPolynomial8_Type;

static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<HomotheticCauchy3> >
                                                                NodeISSubdivisionCurveT_HomotheticCauchy3_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<HomotheticCauchy4> >
                                                                NodeISSubdivisionCurveT_HomotheticCauchy4_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<HomotheticCauchy5> >
                                                                NodeISSubdivisionCurveT_HomotheticCauchy5_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<HomotheticCauchy6> >
                                                                NodeISSubdivisionCurveT_HomotheticCauchy6_Type;

static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<HomotheticInverse3> >
                                                                NodeISSubdivisionCurveT_HomotheticInverse3_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<HomotheticInverse4> >
                                                                NodeISSubdivisionCurveT_HomotheticInverse4_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<HomotheticInverse5> >
                                                                NodeISSubdivisionCurveT_HomotheticInverse5_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<HomotheticInverse6> >
                                                                NodeISSubdivisionCurveT_HomotheticInverse6_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<CompactedHomotheticInverse3> >
                                                                NodeISSubdivisionCurveT_CompactedHomotheticInverse3_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<CompactedHomotheticInverse4> >
                                                                NodeISSubdivisionCurveT_CompactedHomotheticInverse4_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<CompactedHomotheticInverse5> >
                                                                NodeISSubdivisionCurveT_CompactedHomotheticInverse5_Type;
static PrimitiveFactory::Type<  NodeISSubdivisionCurveT<CompactedHomotheticInverse6> >
                                                                NodeISSubdivisionCurveT_CompactedHomotheticInverse6_Type;

} // Close namespace convol

} // Close namespace expressive
