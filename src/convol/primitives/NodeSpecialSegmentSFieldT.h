/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: NodeSpecialSegmentSFieldT.h

   Language: C++

   License: Expressive license

   \author: Zanni Cédric, Maxime Quiblier
   E-Mail: cedric.zanni@inrialpes.fr

   Description: Header for a segment generated scalar field.
                TODO:@todo : add a more complete description

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_SPECIAL_SEGMENT_SFIELD_T_H_
#define CONVOL_SPECIAL_SEGMENT_SFIELD_T_H_

// core dependencies
#include <core/CoreRequired.h>
    // geometry
    #include <core/geometry/AABBox.h>
    #include <core/geometry/WeightedSegmentWithDensity.h>
    // functors
    #include <core/functor/Functor.h>

// convol dependencies
#include <convol/blobtreenode/NodeScalarField.h>
#include <convol/properties/Properties1D.h>
    // tools
    #include <convol/tools/VectorTools.h> //TODO:@todo : I do not like this class ...
    #include <convol/tools/ConvergenceTools.h>


namespace expressive {

namespace convol {

/*! \brief Superclass to NodeSpecialSegmentSFieldT.
 *         This class has been added to grant access to geometrical data without knowing
 *         which functor specializes the class NodeSpecialSegmentSFieldT.
 *         ie if you know you have a NodeSpecialSegmentSFieldT, but you don't know with which kernel,
 *            you can still access geometrical data.
 *          NodeSpecialSegmentSFieldT is the ONLY CLASS allowed to inherit from AbstractHomotheticSegmentSFieldT !!!!
 *
 */
class CONVOL_API AbstractNodeSpecialSegmentSField
        : public NodeScalarField, public core::WeightedSegmentWithDensity
{
public:
    typedef core::WeightedSegmentWithDensity WeightedSegmentWithDensity;

    EXPRESSIVE_MACRO_NAME("NodeSpecialSegmentSField")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////
    AbstractNodeSpecialSegmentSField(const core::WeightedPoint& p1, const core::WeightedPoint& p2) :
          NodeScalarField(StaticName()), WeightedSegmentWithDensity(p1,p2), properties_()
    {
        assert(this->length_!=0.0);    // a segment must not be 0 length, if you want a 0-length segment... well, use a point :-)
    }
    AbstractNodeSpecialSegmentSField(const Point& p1, const Point& p2,
                                 Scalar weight1, Scalar weight2,
                                 Scalar density1, Scalar density2)
        :   NodeScalarField(StaticName()), WeightedSegmentWithDensity(p1,p2,weight1,weight2, density1, density2), properties_()
    {
        assert(this->length_!=0.0);    // a segment must not be 0 length, if you want a 0-length segment... well, use a point :-)
    }
    AbstractNodeSpecialSegmentSField(const WeightedSegmentWithDensity &w_seg_d) :
        NodeScalarField(StaticName()), WeightedSegmentWithDensity(w_seg_d), properties_(){ }
    // Copy constructor
    AbstractNodeSpecialSegmentSField(const AbstractNodeSpecialSegmentSField &rhs) :
        NodeScalarField(rhs), WeightedSegmentWithDensity(rhs), properties_(rhs.properties_){ }

    //-------------------------------------------------------------------------
    virtual ~AbstractNodeSpecialSegmentSField() {}

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual Scalar Eval(const Point& point) const = 0;
    //-------------------------------------------------------------------------
    virtual inline Vector EvalGrad(const Point& point, const Scalar epsilon) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalProperties(const Point& point,
                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                ) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGrad(const Point& point, const Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGradAndProperties(const Point& point, const Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const = 0;

    /** \brief Compute the integral only on a given subset of the segment "implicitly" given
    *          by the sphere.
    *          WARNING :
    *               - the result isn't multiplied by the normalization_factor
    *               - this function is intended to be used only throught a "Witnessed" blending
    *
    *   \param clipping_sphere a sphere which center is the evaluation point and which radius
    *                           give the portion of skeleton to be used (clipping in warped space)
    *
    *   \return the scalar field value for the clipped segment.
    */
    virtual inline Scalar EvalClipped(const Sphere& clipping_sphere) const = 0;
    virtual inline void EvalValueAndGradClipped(const Sphere& clipping_sphere, Scalar epsilon_grad,
                                                Scalar& value_res, Vector& grad_res) const = 0;


    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool IsCompact() const = 0;
    //-------------------------------------------------------------------------
    virtual Scalar GetDistanceBound(Scalar value) const = 0;
    //-------------------------------------------------------------------------
    virtual core::AABBox GetAxisBoundingBox(Scalar value) const = 0;

    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const = 0;

    /////////////////
    /// Accessors ///
    /////////////////

    // Return the TFunctorKernel (see template of NodeSpecialSegmentSFieldT)
    // WARNING : Should be use mostly for interface or research purposes.
    virtual const core::Functor& kernel_functor() const = 0;
    //-------------------------------------------------------------------------
    Properties1D& properties() {  return properties_; }
    const Properties1D& properties() const {  return properties_; }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    Properties1D properties_;

};

/*! \brief Scalar Field generated around a segment.
 *           The given  TFunctorKernel specify the function that
 *           compute the scalar field given a segment.
 *         WARNING : Some of this class's functions does not work properly if the middle of the segment
 *                   is not included in the surface created by using the given iso value.
 *                   Ex : GetAPointAt
 *
 */
template<typename  TFunctorKernel>
class NodeSpecialSegmentSFieldT :
    public AbstractNodeSpecialSegmentSField
{
public:
    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////
     NodeSpecialSegmentSFieldT(const core::WeightedPoint& p1, const core::WeightedPoint& p2, const  TFunctorKernel& kernel)
        : AbstractNodeSpecialSegmentSField(p1,p2),
          kernel_(kernel)
    { }
     NodeSpecialSegmentSFieldT(const Point& p1, const Point& p2, Scalar weight1, Scalar weight2, Scalar density1, Scalar density2,
                          const  TFunctorKernel& kernel)
        : AbstractNodeSpecialSegmentSField(p1,p2,weight1,weight2,density1,density2),
          kernel_(kernel)
    { }
    // Copy constructor
     NodeSpecialSegmentSFieldT(const NodeSpecialSegmentSFieldT &rhs)
        :   AbstractNodeSpecialSegmentSField(rhs),
            kernel_(rhs.eval_functor_)
    { }

    virtual ~NodeSpecialSegmentSFieldT(){}

    //////////////////////
    /// Clone Function ///
    //////////////////////

    virtual NodeSpecialSegmentSFieldT *clone() const
    {
        return new NodeSpecialSegmentSFieldT(*this);
    }

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual inline Scalar Eval(const Point& point) const
    {
        return kernel_(*this, point);
    }
    //-------------------------------------------------------------------------
    virtual inline Vector EvalGrad(const Point& point, Scalar epsilon) const
    {
        return kernel_.EvalGrad(*this, point, epsilon);
    }
    //-------------------------------------------------------------------------
    virtual void EvalProperties(const Point& point,
                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                ) const
    {
        Vector p1_to_p = point - p1();
        Scalar s = p1_to_p | this->unit_dir_;
        s = s/this->length_;

        properties_.InlineEvalProperties(s, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
    }
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        kernel_.EvalValueAndGrad(*this, point, epsilon_grad, value_res, grad_res);
    }
    //-------------------------------------------------------------------------
//    //TODO:@todo optimize
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                    const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                    const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                    Scalar& value_res, Vector& grad_res,
                                                    std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                    std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                    ) const
    {
        kernel_.EvalValueAndGrad(*this, point, epsilon_grad, value_res, grad_res);

        Vector p1_to_p = point - p1();
        Scalar s = p1_to_p | this->unit_dir_;
        s = s/this->length_; //TODO:@todo : computation of s is redundant with what is done in the functor
        properties_.InlineEvalProperties(s, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
    }
    //-------------------------------------------------------------------------

    inline Scalar EvalClipped(const Sphere& clipping_sphere) const
    {
        return kernel_.EvalClipped(*this, clipping_sphere);
    }
    inline void EvalValueAndGradClipped(const Sphere& clipping_sphere, Scalar epsilon_grad,
                                                Scalar& value_res, Vector& grad_res) const
    {
        kernel_.EvalValueAndGradClipped(*this, clipping_sphere, epsilon_grad, value_res, grad_res);
    }

    virtual void EvalValueAndGradAndPropertiesClipped(const Sphere& clipping_sphere, Scalar epsilon_grad,
                                                      const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                      const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                      Scalar& value_res, Vector& grad_res,
                                                      std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                      std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                      ) const
    {
        kernel_.EvalValueAndGradClipped(*this, clipping_sphere, epsilon_grad, value_res, grad_res);

        Vector p1_to_p = point - p1();
        Scalar s = p1_to_p | this->unit_dir_;
        s = s/this->length_; //TODO:@todo : computation of s is redundant with what is done in the functor
        properties_.InlineEvalProperties(s, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
    }

    /** \brief Gives a point where the potential created by the given segment
    *        primitive is value. The segment is considered alone.
    *        This algorithm uses Newton convergence and dichotomy.
    *        Restriction : the middle point of the given segment must be such that the potential
    *        created by the segment on its middle point is greater than this value.
    *        ie the surface defined by the given value includes the middle point of the segment.
    *   \param value
    *    \param epsilon We want the result to be at least epsilon close to the surface with respect to the
    *                   distance Vector.norm(), we suppose this norm to be the one associated with the dot product Vector.operator |
    *    \return point p such that operator(seg,p) == iso_value
    */
    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const;


    ///////////////////////////
    /// BoundingB Functions ///
    ///////////////////////////

    virtual bool IsCompact() const
    {
        return kernel_.isCompact();
    }

    virtual inline Scalar GetDistanceBound(Scalar value) const
    {
        return kernel_.GetEuclidDistBound(*this, value);
    }

    virtual core::AABBox GetAxisBoundingBox(Scalar value) const;
    
    /////////////////
    /// Modifiers ///
    /////////////////

    // This function cannot be used to change the kernel type, only its attributes.
    void set_eval_functor(const  TFunctorKernel& new_functor)
    {
        kernel_ = new_functor;
    }

    /////////////////
    /// Accessors ///
    /////////////////
    
    virtual const Functor& kernel_functor() const { return kernel_; }
     TFunctorKernel& kernel() { return kernel_; }

    /////////////////
    /// Attributs ///
    /////////////////

private:
     TFunctorKernel kernel_;

};


template<typename  TFunctorKernel>
Point NodeSpecialSegmentSFieldT< TFunctorKernel>::GetAPointAt(Scalar value, Scalar epsilon) const
{
    //TODO:@todo : function should definitely be improved to be improved ...
    // WARNING : If this function shows bugs, that could be
    //             - because the segment or the epsilon value is too small. This could indeed lead to
    //               precision issue depending on the scalar type. This function is not safe regarding
    //               this risk.
    //             - because the given bounding distance is bad, in this case check GetBound

    // @todo find a way to check and tell the user that this should be verified.
    /*
        if(value > Eval((this->seg_.p1()+this->seg_.p2())/2.0))
        {
            std::cout << "WARNING (Restriction violation in FunctorCauchy::GetAPointAt) : the given value must be lower than the field value at segment's middle point. see FunctorCauchy::GetAPointAt description for more details." << std::endl;
            std::cout << "                                  No guarantee that a point will be found" << std::endl;
        }
        */

    Vector search_dir = VectorTools::GetOrthogonalRandVect(this->unit_dir_);
    // @todo this normamilization could be useless depending on GetOrthogonalRandVect... this last function should maybe return a normalized vector if the given vector is normalized...
    search_dir.normalize();

    // Accuracy for gradient computation
    // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
    Scalar grad_epsilon = this->length_*0.0001;    // arbitrary
    if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }

    Scalar max_bound = kernel_.GetEuclidDistBound(*this, value);
    if(max_bound != 0.0)
    {
        Scalar min_bound = 0.0;
        Scalar start_absc = max_bound/2.0;

        Point res;
        Convergence::SafeNewton1D(
                    *this,
                    (p1()+p2())/2.0, search_dir, // together give the line on which we step
                    min_bound,                                // security minimum bound
                    max_bound,                // security maximum bound
                    start_absc,            // point at which we start the search, given in 1D
                    value,
                    epsilon,
                    grad_epsilon,
                    10,
                    res);
        return res;
    }
    else
    {
        return (p1()+p2())/2.0;   // The surface does not exist, we return the middle of the segment
    }
}

template<typename  TFunctorKernel>
core::AABBox NodeSpecialSegmentSFieldT< TFunctorKernel>::GetAxisBoundingBox(Scalar value) const
{
    core::AABBox res;

    //TODO:@todo : use the bounding function of the segment

    Scalar dist_bound = GetDistanceBound(value);
    for(unsigned int i = 0; i < Vector::size_; ++i)
    {
        if((p1())[i] < (p2())[i])
        {
            res.min_[i] = (p1())[i] - dist_bound;
            res.max_[i] = (p2())[i] + dist_bound;
        }
        else
        {
            res.min_[i] = (p2())[i] - dist_bound;
            res.max_[i] = (p1())[i] + dist_bound;
        }
    }
    return res;
}

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_SPECIAL_SEGMENT_SFIELD_T_H_
