/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   @deprecated

   \file: SegmentSFieldT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header for a segment generated scalar field.
                This is both a BlobtreeNode and a GeometryPrimitive which mean that it can be directly
                inserted in both a Blobtree structures and a Skeleton.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_SEGMENT_SFIELD_T_H_
#define CONVOL_SEGMENT_SFIELD_T_H_

// core dependencies
#include<core/CoreRequired.h>
    // geometry
    #include<core/geometry/WeightedSegment.h>
    // functors
    #include<core/functor/Functor.h>

// convol dependencies
#include<convol/blobtreenode/NodeScalarField.h>
#include <convol/properties/Properties1D.h>

namespace expressive {

namespace convol {

/*!  \deprecated
 *   \brief Abstract class to SegmentSFieldT.
 * 
 *         This class has been added to grant access to geometrical data without knowing 
 *         which functor specializes the class SegmentSFieldT.
 *         ie if you know you have a SegmentSFieldT, but you don't know with which kernel, 
 *            you can still access geometrical data.
 *         SegmentSFieldT is the ONLY CLASS allowed to inherit from AbstractSegmentSFieldT !!!!
 *  
 */
class CONVOL_API AbstractNodeSegmentSField
        : public NodeScalarField, public core::WeightedSegment
{
public:
    
    typedef core::WeightedSegment BaseGeometry;

    typedef core::WeightedSegment WeightedSegment;

    EXPRESSIVE_MACRO_NAME("NodeSegmentSField")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////
    AbstractNodeSegmentSField(const core::WeightedPoint& p1, const core::WeightedPoint& p2)
        : NodeScalarField(StaticName()), WeightedSegment(p1,p2), properties_()
    {
        assert(this->length_!=0.0);    // a segment must not be 0 length, if you want a 0-length segment... well, use a point :-)
    }
    AbstractNodeSegmentSField(const Point& p1, const Point& p2, Scalar weight1, Scalar weight2)
        : NodeScalarField(StaticName()), WeightedSegment(p1,p2,weight1,weight2), properties_()
    {
        assert(this->length_!=0.0);    // a segment must not be 0 length, if you want a 0-length segment... well, use a point :-)
    }
    AbstractNodeSegmentSField(const WeightedSegment &w_seg)
        : NodeScalarField(StaticName()), WeightedSegment(w_seg), properties_(){ }
    // Copy constructor
    AbstractNodeSegmentSField(const AbstractNodeSegmentSField &rhs)
        : NodeScalarField(rhs), WeightedSegment(rhs), properties_(rhs.properties_){ }
    AbstractNodeSegmentSField(const AbstractNodeSegmentSField &rhs, unsigned int start, unsigned int end)
        : NodeScalarField(rhs, true), WeightedSegment(rhs, start, end), properties_(rhs.properties_){ }

    //-------------------------------------------------------------------------
    virtual ~AbstractNodeSegmentSField() {}


//    //-------------------------------------------------------------------------
//    virtual std::string ParameterName() const
//    {
//    //TODO: @todo : il faudrait sans doute passer une fonction ParameterName dans la geometrie
//        std::ostringstream name;
//        name.setf(std::ios::scientific,std::ios::floatfield);
//        name.precision(2);
//        name << "[p1(" << seg_.p1()[0] << "," << seg_.p1()[1] << "," << seg_.p1()[2] << "),";
//        name << "weight_p1(" << seg_.weight_p1() << "),";
//        name << "p2(" <<  seg_.p2()[0] << "," <<  seg_.p2()[1] << "," <<  seg_.p2()[2] << "),";
//        name << "weight_p2(" << seg_.weight_p2() << ")]";
//        return name.str();
//    }

    ///////////
    /// Xml ///
    ///////////

    // Forcing using baseGeometry::ToXml method.
    using BaseGeometry::ToXml;
    // Forcing using NodeScalarField::AddXmlAttributes method.
    using NodeScalarField::AddXmlAttributes;

    void InitFromXml(const TiXmlElement *element);

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual Scalar Eval(const Point& point) const = 0;
    //-------------------------------------------------------------------------
    virtual inline Vector EvalGrad(const Point& point, Scalar epsilon) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalProperties(const Point& point,
                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                ) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalHomotheticValues(const Point& point, Scalar& value_res, Vector& grad_res) const = 0;

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool IsCompact() const = 0;
    //-------------------------------------------------------------------------
    virtual Scalar GetDistanceBound(Scalar value) const = 0;
    //-------------------------------------------------------------------------
    virtual core::AABBox GetAxisBoundingBox(Scalar value) const = 0;

    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const = 0;

    /////////////////
    /// Accessors ///
    /////////////////

    // Return the TFunctorKernel (see template of SegmentSFieldT)
    // WARNING : Should be use mostly for interface or research purposes.
    virtual const core::Functor& kernel_functor() const = 0;
    virtual core::Functor& kernel_functor() = 0;
    //-------------------------------------------------------------------------
    Properties1D& properties() {  return properties_; }
    const Properties1D& properties() const {  return properties_; }

    inline void set_properties(const Properties1D &prop) { properties_ = prop; }

protected:
    Properties1D properties_;

//    //-------------------------------------------------------------------------
//    inline Scalar InlineEvalScalarProperty(const Point& point, ESFScalarProperties id) const
//    {
//        Vector p1_to_p = point - seg_.p1();
//        Scalar s = p1_to_p | seg_.unit_dir();
        
//        return InlineSwitchKeysNumber(s, scalar_properties_[id], Traits::kSFScalarPropertiesDefault[id]);
//    }
//    //-------------------------------------------------------------------------
//    inline Vector InlineEvalVectorProperty(const Point& point, ESFVectorProperties id) const
//    {
//        Vector p1_to_p = point - seg_.p1();
//        Scalar s = p1_to_p | seg_.unit_dir();

//        return InlineSwitchKeysNumber(s, vector_properties_[id], Traits::kSFVectorPropertiesDefault[id]);
//    }
};

/*! \brief Scalar Field generated around a segment.
 *           The given TFunctorKernel specify the function that
 *           compute the scalar field given a segment.
 *         WARNING : Some of this class's functions does not work properly if the middle of the segment
 *                   is not included in the surface created by using the given iso value.
 *                   Ex : GetAPointAt
 *
 */

template<typename TFunctorKernel>
class NodeSegmentSFieldT :
    public AbstractNodeSegmentSField
{
public:

    typedef AbstractNodeSegmentSField::BaseGeometry BaseGeometry;
    typedef TFunctorKernel FunctorKernel;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////
    NodeSegmentSFieldT(const core::WeightedPoint& p1, const core::WeightedPoint& p2, const TFunctorKernel& kernel)
        : AbstractNodeSegmentSField(p1,p2),
          kernel_(kernel)
    { }
    NodeSegmentSFieldT(const Point& p1, const Point& p2, Scalar weight1, Scalar weight2, const TFunctorKernel& kernel)
        : AbstractNodeSegmentSField(p1,p2,weight1,weight2),
          kernel_(kernel)
    { }
    NodeSegmentSFieldT(const WeightedSegment &w_seg,  const TFunctorKernel& kernel)
        :   AbstractNodeSegmentSField(w_seg),
            kernel_(kernel)
    { }
    // Copy constructor
    NodeSegmentSFieldT(const NodeSegmentSFieldT &rhs)
        :   AbstractNodeSegmentSField(rhs),
            kernel_(rhs.kernel_)
    { }
    NodeSegmentSFieldT(const NodeSegmentSFieldT &rhs, unsigned int start, unsigned int end)
        :   AbstractNodeSegmentSField(rhs, start, end),
            kernel_(rhs.kernel_)
    { }

    virtual ~NodeSegmentSFieldT(){}

    //////////////////////
    /// Clone Function ///
    //////////////////////

    virtual std::shared_ptr<ScalarField> CloneForEval() const
    {
        return std::make_shared<NodeSegmentSFieldT>(*this);
    }

    virtual std::shared_ptr<core::GeometryPrimitiveT<core::WeightedPoint> > clone(unsigned int start = -1, unsigned int end = -1) const
    {
        return std::make_shared<NodeSegmentSFieldT>(*this, start, end);
    }

//    //-------------------------------------------------------------------------
//    virtual std::string FunctorName() const
//    {
//        std::ostringstream name;
//        name << "<";
//        name << kernel_.Name() ;
//        name << kernel_.FunctorName() ;
//        name << kernel_.ParameterName() << ">";
//        return name.str();
//    }

    ///////////
    /// Xml ///
    ///////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        AbstractNodeSegmentSField::AddXmlAttributes(element);
        TiXmlElement *element_functor_eval_seg = new TiXmlElement( "TFunctorKernel" );
        element->LinkEndChild(element_functor_eval_seg);
        TiXmlElement * element_child = kernel_.ToXml();
        element_functor_eval_seg->LinkEndChild(element_child);

        TiXmlElement * element_properties = properties_.ToXml();
        element->LinkEndChild(element_properties);
    }

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////
    virtual inline Scalar Eval(const Point& point) const
    {
        return kernel_(this->opt_w_seg_, point);
    }
    //-------------------------------------------------------------------------
    virtual inline Vector EvalGrad(const Point& point, Scalar epsilon) const
    {
        return kernel_.EvalGrad(this->opt_w_seg_, point, epsilon);
    }
    //-------------------------------------------------------------------------
    virtual void EvalProperties(const Point& point,
                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                ) const
    {
        Vector p1_to_p = point - p1();
        Scalar s = p1_to_p.dot(this->unit_dir_);
        s = s/this->length_;

        properties_.InlineEvalProperties(s, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
    }
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        kernel_.EvalValueAndGrad(this->opt_w_seg_, point, epsilon_grad, value_res, grad_res);
    }
    //-------------------------------------------------------------------------
//    //TODO:@todo optimize
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                    const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                    const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                    Scalar& value_res, Vector& grad_res,
                                                    std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                    std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                    ) const
    {
        kernel_.EvalValueAndGrad(this->opt_w_seg_, point, epsilon_grad, value_res, grad_res);

        Vector p1_to_p = point - p1();
        Scalar s = p1_to_p.dot(this->unit_dir_);
        s = s/this->length_; //TODO:@todo : computation of s is redundant with what is done in the functor
        properties_.InlineEvalProperties(s, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
    }
    //-------------------------------------------------------------------------
    virtual void EvalHomotheticValues(const Point& point, Scalar& value_res, Vector& grad_res) const
    {
        kernel_.EvalHomotheticValues(this->opt_w_seg_, point, value_res, grad_res);
    }
    //-------------------------------------------------------------------------

    /** \brief Gives a point where the potential created by the given segment
    *        primitive is value. The segment is considered alone.
    *        This algorithm uses Newton convergence and dichotomy.
    *        Restriction : the middle point of the given segment must be such that the potential
    *        created by the segment on its middle point is greater than this value.
    *        ie the surface defined by the given value includes the middle point of the segment.
    *   \param value
    *    \param epsilon We want the result to be at least epsilon close to the surface with respect to the
    *                   distance Vector.norm(), we suppose this norm to be the one associated with the dot product Vector.operator |
    *    \return point p such that operator(seg,p) == iso_value
    */
    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const;


    ///////////////////////////
    /// BoundingB Functions ///
    ///////////////////////////
    virtual bool IsCompact() const
    {
        return kernel_.isCompact();
    }

    virtual inline Scalar GetDistanceBound(Scalar value) const
    {
        return kernel_.GetEuclidDistBound(this->opt_w_seg_, value);
    }

    virtual core::AABBox GetAxisBoundingBox(Scalar value) const;
    
    virtual void GetPrecisionAxisBoundingBoxes(Scalar value, std::list<core::PrecisionAxisBoundingBox> &pboxes) const
    {
        opt_w_seg_.GetPrecisionAxisBoundingBoxes(value, pboxes);
    }
////    virtual void GetPrecisionAxisBoundingBoxes(const Scalar value, std::list<PrecisionAxisBoundingBoxT<Traits> > &pboxes) const
////    {
////            Scalar mean_radius;
////
////            if (this->seg_.weight_p1() < this->seg_.weight_p2())
////                mean_radius = 0.75*this->seg_.weight_p1() + 0.25*this->seg_.weight_p2();
////            else
////                mean_radius = 0.25*this->seg_.weight_p1() + 0.75*this->seg_.weight_p2();

////            pboxes.push_back(PrecisionAxisBoundingBoxT<Traits>(GetAxisBoundingBox(value),mean_radius,0.5*mean_radius));
//////        kernel_.GetPrecisionAxisBoundingBoxes(this->seg_, value, pboxes);
////    }

    /////////////////
    /// Modifiers ///
    /////////////////

    virtual void updated()
    {
        WeightedSegment::updated();
        this->InvalidateBoundingBox();
    }

    // This function cannot be used to change the kernel type, only its attributes.
    void set_kernel(const TFunctorKernel& new_functor)
    {
        kernel_ = new_functor;
        this->InvalidateBoundingBox();
    }

    /////////////////
    /// Accessors ///
    /////////////////
    
    virtual const core::Functor& kernel_functor() const { return kernel_; }
    virtual core::Functor& kernel_functor() { return kernel_; }
    const TFunctorKernel& kernel() { return kernel_; }

    /////////////////
    /// Attributs ///
    /////////////////

private:
    TFunctorKernel kernel_;

};

} // Close namespace convol

} // Close namespace expressive

#include <convol/primitives/NodeSegmentSFieldT.hpp>

#endif // CONVOL_SEGMENT_SFIELD_T_H_
