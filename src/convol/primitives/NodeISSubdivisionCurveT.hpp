
#include <core/geometry/VectorTools.h> //TODO:@todo : I do not like this class !!!
#include <convol/tools/ConvergenceTools.h>

namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////

template<typename TFunctorKernel>
Scalar NodeISSubdivisionCurveT<TFunctorKernel>::Eval(const Point& point) const
{
    Scalar res = 0.0;
    for(auto &seg_info : vec_seg_)
    {
        if(seg_info.first.Contains(point))
        {
            res += kernel_.Eval(seg_info.second, point);
        }
    }
    return res;
}

template<typename TFunctorKernel>
Vector NodeISSubdivisionCurveT<TFunctorKernel>::EvalGrad(const Point& point, Scalar epsilon_grad) const
{
    Vector grad_res = Vector::Zero();
    for(auto &seg_info : vec_seg_)
    {
        if(seg_info.first.Contains(point))
        {
            grad_res += kernel_.EvalGrad(seg_info.second, point, epsilon_grad);
        }
    }
    return grad_res;
}

template<typename TFunctorKernel>
void NodeISSubdivisionCurveT<TFunctorKernel>::EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();

    Scalar value_aux;
    Vector grad_aux;
    for(auto &seg_info : vec_seg_)
    {
        if(seg_info.first.Contains(point))
        {
            kernel_.EvalValueAndGrad(seg_info.second, point, epsilon_grad, value_aux, grad_aux);
            value_res += value_aux;
            grad_res += grad_aux;
        }
    }
}
template<typename TFunctorKernel>
void NodeISSubdivisionCurveT<TFunctorKernel>::EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                   const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                   const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                   Scalar& value_res, Vector& grad_res,
                                   std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                   std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                  ) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();
//  //TODO:@todo : this should not be required
//    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
//    {
//        scal_prop_res[i] = 0.0;
//    }
//    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
//    {
//        vect_prop_res[i] = Vector::vectorized(0.0);
//    }

    Scalar value_aux;
    Vector grad_aux;

    Scalar current_abs = 0.0, w_abs = 0.0;

    for(auto &seg_info : vec_seg_)
    {
        if(seg_info.first.Contains(point))
        {
            kernel_.EvalValueAndGrad(seg_info.second, point, epsilon_grad, value_aux, grad_aux);
            value_res += value_aux;
            grad_res += grad_aux;

            //TODO:@todo : it would be more logical to take into account the weight variation in the computation of the abs ...
            Vector p_min_to_p = point - seg_info.second.p_min();
            Scalar tmp_abs = p_min_to_p.dot(seg_info.second.increase_unit_dir() ); // this is computed twice ...
            tmp_abs = current_abs + ((seg_info.second.orientation()) ? tmp_abs : (seg_info.second.length() - tmp_abs) );
            tmp_abs = (tmp_abs < 0.0) ? 0.0 : ((tmp_abs > length_) ? length_ : tmp_abs);

            w_abs += tmp_abs * value_aux;
        }
        current_abs += seg_info.second.length();
    }

    if(value_res != 0.0)
    {
        w_abs /= (value_res * length_ );
        properties_.InlineEvalProperties(w_abs, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
    }
}

template<typename TFunctorKernel>
void NodeISSubdivisionCurveT<TFunctorKernel>::EvalProperties(const Point& point,
                    const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                    const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                    std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                    std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                   ) const
{
    Scalar current_abs = 0.0, w_abs = 0.0, value_res = 0.0;
    Scalar value_aux;
    for(auto &seg_info : vec_seg_)
    {
        if(seg_info.first.Contains(point))
        {
            //TODO:@todo : it would be more logical to take into account the weight variation in the computation of the abs ...
            Vector p_min_to_p = point - seg_info.second.p_min();
            Scalar tmp_abs = p_min_to_p.dot(seg_info.second.increase_unit_dir()); // this is computed twice ...
            tmp_abs = current_abs + ((seg_info.second.orientation()) ? tmp_abs : (seg_info.second.length() - tmp_abs) );
            tmp_abs = (tmp_abs < 0.0) ? 0.0 : ((tmp_abs > length_) ? length_ : tmp_abs);

            value_aux = kernel_.Eval(seg_info.second, point);
            value_res += value_aux;
            w_abs += tmp_abs * value_aux;
        }
        current_abs += seg_info.second.length();
    }

    if(value_res > 0.0)
    {
        w_abs /= (value_res * length_ );
    }
    properties_.InlineEvalProperties(w_abs, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
}

/////////////////////////////////////////////////////////////////

template<typename TFunctorKernel>
void NodeISSubdivisionCurveT<TFunctorKernel>::EvalHomotheticValues(const Point& point, Scalar& value_res, Vector& grad_res) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();

    Scalar value_aux;
    Vector grad_aux;
    for(auto &seg_info : vec_seg_)
    {
        if(seg_info.first.Contains(point))
        {
            kernel_.EvalHomotheticValues(seg_info.second, point, value_aux, grad_aux);
            value_res += value_aux;
            grad_res += grad_aux;
        }
    }
}


//-------------------------------------------------------------------------
template<typename TFunctorKernel>
bool NodeISSubdivisionCurveT<TFunctorKernel>::EvalHomotheticValuesAndParameters(const Point& point, Scalar& value_res, Vector& grad_res,
                                               Scalar& abs, Vector& weighted_dir) const
{
    value_res    = 0.0;
    grad_res     = Vector::Zero();
    weighted_dir = Vector::Zero();

    Scalar field_aux, tmp_abs;
    Vector grad_aux;

    Scalar current_abs = 0.0, w_abs = 0.0;

    for(auto &seg_info : vec_seg_)
    {
        if(seg_info.first.Contains(point))
        {
            kernel_.EvalHomotheticValues(seg_info.second, point, field_aux, grad_aux);

            if(field_aux > 0.0)
            {
                value_res += field_aux;
                grad_res += grad_aux;

                //TODO:@todo : it would be more logical to take into account the weight variation in the computation of the abs ...

                Vector p_min_to_p = point - seg_info.second.p_min();
//                Vector dir =
                const Vector &dir = seg_info.second.increase_unit_dir();
                if(seg_info.second.orientation())
                {
                    weighted_dir += field_aux * dir;
                    tmp_abs = current_abs +  p_min_to_p.dot(seg_info.second.increase_unit_dir() );
                }
                else
                {
                    weighted_dir -= field_aux * dir;
                    tmp_abs = current_abs +  seg_info.second.length() - p_min_to_p.dot(seg_info.second.increase_unit_dir() ) ;
                }
                tmp_abs = (tmp_abs < 0.0) ? 0.0 : ((tmp_abs > length_) ? length_ : tmp_abs);

                w_abs += tmp_abs * field_aux;
            }

        }
        current_abs += seg_info.second.length();
    }

    if(value_res != 0.0)
    {
        abs = w_abs / (value_res * length_ );
        //TODO:@todo : should we return the abs or w_abs : there is an incoherency between abs and dir ...
//        properties_.InlineEvalProperties(w_abs, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
        return true;
    }
    abs = 0.0;
    return false;
}


//////////////////////////
/// Bounding functions ///
//////////////////////////

template<typename TFunctorKernel>
core::AABBox NodeISSubdivisionCurveT<TFunctorKernel>::GetAxisBoundingBox(Scalar value) const
{
    core::AABBox bt_node_abb = core::AABBox();
    Scalar dist_bound;
    for(auto &seg_info : vec_seg_)
    {
        core::AABBox tmp_aabb(seg_info.second.GetAxisBoundingBox()); //TODO:@todo : on peut envisager d'avoir directement une fct construisant la bounding box dans le kernel
        dist_bound = kernel_.GetEuclidDistBound1D(seg_info.second.WeightMax(), value);
        for(unsigned int i = 0; i < Vector:: RowsAtCompileTime; ++i)
        {
            tmp_aabb.min_[i] -= dist_bound;
            tmp_aabb.max_[i] += dist_bound;
        }

        bt_node_abb = core::AABBox::Union(bt_node_abb, tmp_aabb);
    }
    return  bt_node_abb;
}

template<typename TFunctorKernel>
const core::AABBox& NodeISSubdivisionCurveT<TFunctorKernel>::InitAxisBoundingBox(Scalar value)
{
    if(this->epsilon_bbox_ != value)
    {
        core::AABBox bt_node_abb = core::AABBox();
        Scalar dist_bound;
        for(auto &seg_info : vec_seg_)
        {
            seg_info.first = seg_info.second.GetAxisBoundingBox(); //TODO:@todo : on peut envisager d'avoir directement une fct construisant la bounding box dans le kernel
            dist_bound = kernel_.GetEuclidDistBound1D(seg_info.second.WeightMax(), value);
            for(unsigned int i = 0; i < Vector:: RowsAtCompileTime; ++i)
            {
                seg_info.first.min_[i] -= dist_bound;
                seg_info.first.max_[i] += dist_bound;
            }

            bt_node_abb = core::AABBox::Union(bt_node_abb, seg_info.first);
        }
        this->axis_bounding_box_ = bt_node_abb;

        this->epsilon_bbox_ = kernel_.isCompact() ? 0.0 : value;
//        this->BlobtreeNodeABBoxUpdated();
    }
    return this->axis_bounding_box_;
}

template<typename TFunctorKernel>
void NodeISSubdivisionCurveT<TFunctorKernel>::GetPrecisionAxisBoundingBoxes(Scalar value, std::list<core::PrecisionAxisBoundingBox> &pboxes) const
{
    for(auto &seg_info : vec_seg_)
        seg_info.second.GetPrecisionAxisBoundingBoxes(value, pboxes);
}





template<typename TFunctorKernel>
Point NodeISSubdivisionCurveT<TFunctorKernel>::GetAPointAt(Scalar value, Scalar epsilon) const
{
    //TODO:@todo : implementation provisoir ...
    core::PointCloud res_points;
    GetPrimitivesPointsAt(value, epsilon, res_points, axis_bounding_box());
    return res_points.begin()->p;
}

//TODO:@todo : implementation should be changed : for now return 1 point per segment
template<typename TFunctorKernel>
void NodeISSubdivisionCurveT<TFunctorKernel>::GetPrimitivesPointsAt(Scalar value, Scalar epsilon, core::PointCloud& res_points, const core::AABBox& b_box) const
{
    core::PointCloud res_points_tmp;

    for(auto &seg_info : vec_seg_)
    {
        if( b_box.Intersect(seg_info.first) )
        {
            Point init_point;
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        const core::OptWeightedSegment &seg = seg_info.second;

                        Vector search_dir = expressive::VectorTools::GetOrthogonalRandVect(seg.increase_unit_dir());
                        // @todo this normamilization could be useless depending on GetOrthogonalRandVect... this last function should maybe return a normalized vector if the given vector is normalized...
                        search_dir.normalize();

                        // Accuracy for gradient computation
                        // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
                        Scalar grad_epsilon = seg.length()*0.0001;    // arbitrary
                        if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }

                        Scalar max_bound = kernel_.GetEuclidDistBound(seg, value);
                        if(max_bound != 0.0)
                        {
            //                Scalar min_bound = 0.0;
            //                Scalar start_absc = max_bound/2.0;

            //                Point res;
            //                Convergence::SafeNewton1D(
            //                            *this,
            //                            (seg.points_[0]+seg.points_[1])/2.0, search_dir, // together give the line on which we step
            //                            min_bound,                                // security minimum bound
            //                            max_bound,                // security maximum bound
            //                            start_absc,            // point at which we start the search, given in 1D
            //                            value,
            //                            epsilon,
            //                            grad_epsilon,
            //                            10,
            //                            res);
                            init_point = (seg.p_min() + 0.5*seg.length()*seg.increase_unit_dir()) + search_dir*(seg.weight_min()+0.5*seg.length()*seg.unit_delta_weight());
                            // (seg.p1()+seg.p2())/2.0 + search_dir*((seg.weight_p1()+seg.weight_p2())*0.5);
                        }
                        else
                        {
                            init_point = (seg.p_min() + 0.5*seg.length()*seg.increase_unit_dir());
                            //(seg.p1()+seg.p2())/2.0;   // The surface does not exist, we return the middle of the segment
                        }

                        res_points.insert(init_point, this->EvalGrad(init_point, 0.0001));
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        }

    }

//        for(typename std::vector<shared_ptr<BlobtreeNode> >::const_iterator it = this->children_.begin(); it != this->children_.end(); it++)
//        {
//            if(b_box.Intersect((*it)->axis_bounding_box()))
//            {
//                (*it)->GetPrimitivesPointsAt(value, epsilon, res_points_tmp, b_box);
//                res_points.splice(res_points_tmp);
//            }
//        }

    // Now converge for all point according to the result of the node.
    Scalar bbox_size = this->axis_bounding_box_.ComputeSizeMax();
    // temporary variables for convergence function
    Scalar min_absc,max_absc;
    Point origin;
    for(auto it = res_points.begin(); it != res_points.end();)
    {
        Point& p = res_points.point(*it);
        Normal& n = res_points.normal(*it);

        if(n[0]!=0.0 || n[1]!=0.0 || n[2]!=0.0){
            min_absc = -bbox_size;
            max_absc = bbox_size;
            origin = p;
            Convergence::SafeNewton1D(  *this,
                                            origin,
                                            n,
                                            min_absc,
                                            max_absc,
                                            0.0,
                                            value,
                                            epsilon,
                                            epsilon*0.1,    // arbitrarily divide by 10 to have a better gradient estimation in case of numerical computing
                                            10,
                                            p);
            // Update the normal to the point
            n = -this->EvalGrad(p,epsilon*0.1);
            n.normalize(); //TODO:@todo  following function does not exist in eigen, in some cases, can be dangerous //n.normalize_cond();
            ++it;
        }
        else
        {
            // Discard the point since it's proabbly outside anything useful...
            it = res_points.erase(it);
        }
    }
}


} // namespace convol

} // namespace expressive
