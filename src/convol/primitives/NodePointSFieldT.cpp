#include <convol/primitives/NodePointSFieldT.h>

#include <core/geometry/PrimitiveFactory.h>

// List of all kernel that can be used with a NodePointSField
// (this is required to register them in the PrimitiveFacotry)

#include <convol/functors/kernels/cauchyinverse/HomotheticCauchy.h>
#include <convol/functors/kernels/cauchyinverse/HomotheticInverse.h>
#include <convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.h>
#include <convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.h>

using namespace expressive::core;

namespace expressive {

namespace convol {

void AbstractNodePointSField::InitFromXml(const TiXmlElement *element)
{
///// TODO:@todo : load Properties0D from old style xml syntax where there is no xml "balise" <Properties> (directly ScalarProperties and then VectorProperties ...)
/////              if we change the syntax, an additional xml marker should be retrieved with a QueryFirstChildElement
    properties_.InitFromXml(element);
}

static PrimitiveFactory::Type<  NodePointSFieldT<HomotheticCompactPolynomial4> >
                                                                NodePointSFieldT_HomotheticCompactPolynomial4_Type;
static PrimitiveFactory::Type<  NodePointSFieldT<HomotheticCompactPolynomial6> >
                                                                NodePointSFieldT_HomotheticCompactPolynomial6_Type;
static PrimitiveFactory::Type<  NodePointSFieldT<HomotheticCompactPolynomial8> >
                                                                NodePointSFieldT_HomotheticCompactPolynomial8_Type;

static PrimitiveFactory::Type<  NodePointSFieldT<HomotheticCauchy3> >
                                                                NodePointSFieldT_HomotheticCauchy3_Type;
static PrimitiveFactory::Type<  NodePointSFieldT<HomotheticCauchy4> >
                                                                NodePointSFieldT_HomotheticCauchy4_Type;
static PrimitiveFactory::Type<  NodePointSFieldT<HomotheticCauchy5> >
                                                                NodePointSFieldT_HomotheticCauchy5_Type;
static PrimitiveFactory::Type<  NodePointSFieldT<HomotheticCauchy6> >
                                                                NodePointSFieldT_HomotheticCauchy6_Type;

static PrimitiveFactory::Type<  NodePointSFieldT<HomotheticInverse3> >
                                                                NodePointSFieldT_HomotheticInverse3_Type;
static PrimitiveFactory::Type<  NodePointSFieldT<HomotheticInverse4> >
                                                                NodePointSFieldT_HomotheticInverse4_Type;
static PrimitiveFactory::Type<  NodePointSFieldT<HomotheticInverse5> >
                                                                NodePointSFieldT_HomotheticInverse5_Type;
static PrimitiveFactory::Type<  NodePointSFieldT<HomotheticInverse6> >
                                                                NodePointSFieldT_HomotheticInverse6_Type;
//
static PrimitiveFactory::Type<  NodePointSFieldT<CompactedHomotheticInverse3> >
                                                                NodePointSFieldT_CompactedHomotheticInverse3_Type;
static PrimitiveFactory::Type<  NodePointSFieldT<CompactedHomotheticInverse4> >
                                                                NodePointSFieldT_CompactedHomotheticInverse4_Type;
static PrimitiveFactory::Type<  NodePointSFieldT<CompactedHomotheticInverse5> >
                                                                NodePointSFieldT_CompactedHomotheticInverse5_Type;
static PrimitiveFactory::Type<  NodePointSFieldT<CompactedHomotheticInverse6> >
                                                                NodePointSFieldT_CompactedHomotheticInverse6_Type;

} // Close namespace convol
} // Close namespace expressive

