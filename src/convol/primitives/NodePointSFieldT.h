/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: NodePointSFieldT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
            Galel Koraa
   E-Mail: maxime.quiblier@inrialpes.fr
           galel.koraa@inrialpes.fr

   Description: Header for a point generating scalar field.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_POINT_SFIELD_H_
#define CONVOL_POINT_SFIELD_H_

// core dependencies
#include <core/CoreRequired.h>
    // geometry
    #include <core/geometry/AABBox.h>
    #include <core/geometry/WeightedPoint.h>
    // functors
    #include <core/functor/Functor.h>

// convol dependencies
#include <convol/blobtreenode/NodeScalarField.h>
#include <convol/properties/Properties0D.h>


namespace expressive {

namespace convol {

class AbstractNodePointSField
        : public NodeScalarField, public core::WeightedPoint
{
public:
    
    typedef core::WeightedPoint BaseGeometry;
    typedef core::WeightedPoint WeightedPoint;

    EXPRESSIVE_MACRO_NAME("NodePointSField")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractNodePointSField(const Point& point, Scalar weight)
        : NodeScalarField(StaticName()), WeightedPoint(point, weight), properties_()
    { }

    AbstractNodePointSField(const WeightedPoint& wp)
        : NodeScalarField(StaticName()), WeightedPoint(wp), properties_()
    { }

    // Copy constructor
    AbstractNodePointSField(const AbstractNodePointSField &rhs, bool copyParent = false)
        : NodeScalarField(rhs, copyParent), WeightedPoint(rhs), properties_(rhs.properties_)
    { }

    virtual ~AbstractNodePointSField(){}

//    virtual std::string ParameterName() const
//    {
//        std::ostringstream name;
//        name.setf(std::ios::scientific,std::ios::floatfield);
//        name.precision(2);
//        name << "[position(" << weighted_point_.position()[0] << "," << weighted_point_.position()[1] << "," << weighted_point_.position()[2] << "),";
//        name << "weight(" << weighted_point_.weight()<< ")]";
//        return name.str();
//    }

    ///////////
    /// Xml ///
    ///////////

    // Forcing using baseGeometry::ToXml method.
    using BaseGeometry::ToXml;
    using BaseGeometry::AddXmlAttributes;
//    // Forcing using NodeScalarField::AddXmlAttributes method.
//    using NodeScalarField::AddXmlAttributes;

    void CONVOL_API InitFromXml(const TiXmlElement *element);

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual Scalar Eval(const Point& point) const = 0;
    //----------------------------------------------------------------------------------------------------------------------
    virtual inline Vector EvalGrad(const Point& point, Scalar epsilon) const = 0;
    //----------------------------------------------------------------------------------------------------------------------
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalProperties(const Point& point,
                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                ) const = 0;
    //----------------------------------------------------------------------------------------------------------------------
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const = 0;
    //----------------------------------------------------------------------------------------------------------------------
    virtual void EvalHomotheticValues(const Point& point, Scalar& value_res, Vector& grad_res) const = 0;

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool IsCompact() const = 0;
    //-------------------------------------------------------------------------
    virtual inline Scalar GetDistanceBound(Scalar value) const = 0;
    //---------------------------------------------------------------------------------
    virtual core::AABBox GetAxisBoundingBox(Scalar value) const = 0;


    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const = 0;

    /////////////////
    /// Modifiers ///
    /////////////////

    inline void set_properties(const Properties0D &prop) { properties_ = prop; }

    /////////////////
    /// Accessors ///
    /////////////////

    // Return the TFunctorKernel (see template of NodePointSFieldT)
    // WARNING : Should be use mostly for interface or research purposes.
    virtual const core::Functor& kernel_functor() const = 0;
    virtual core::Functor& kernel_functor() = 0;
    //-------------------------------------------------------------------------
    Properties0D& properties() {  return properties_; }
    const Properties0D& properties() const {  return properties_; }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    // Properties to attach to primitives according to traits
    Properties0D properties_;

}; // END class AbstractPointSField class declaration


template<typename TFunctorKernel>
class NodePointSFieldT :
    public AbstractNodePointSField
{
public:
    typedef TFunctorKernel FunctorKernel;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    NodePointSFieldT(const Point& point, const Scalar weight,
                     const TFunctorKernel& kernel)
        :   AbstractNodePointSField(WeightedPoint(point, weight)),
            kernel_(kernel)
    { }

    NodePointSFieldT(const WeightedPoint& w_point,
                 const TFunctorKernel& kernel)
        :   AbstractNodePointSField(w_point),
            kernel_(kernel)
    { }
    //TODO:@todo : since weighted point aren't a GeometryPrimitive we can't create them use the primitive factory ...
    // Copy constructor
    NodePointSFieldT(const NodePointSFieldT &rhs, bool copyParent = false)
        :   AbstractNodePointSField(rhs, copyParent),
            kernel_(rhs.kernel_)
    { }

    virtual ~NodePointSFieldT(){}


    ///////////
    /// Xml ///
    ///////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        AbstractNodePointSField::AddXmlAttributes(element);
        TiXmlElement *element_functor_eval_point = new TiXmlElement( "TFunctorKernel" );
        element->LinkEndChild(element_functor_eval_point);
        TiXmlElement * element_child = kernel_.ToXml();
        element_functor_eval_point->LinkEndChild(element_child);

        TiXmlElement * element_properties = properties_.ToXml();
        element->LinkEndChild(element_properties);
    }

    //        // Save ScalarProperties
    //        TiXmlElement * element_scalar_prop = new TiXmlElement( "ScalarProperties" );
    //        for(unsigned int i = 0; i<Traits::SF_SCALAR_PROPERTIES_CARDINAL; ++i)
    //        {
    //            // For now we know there is only 1 value per property. So no need to be as fancy as we were for Segments
    //            TiXmlElement * element_scalar_prop_i = new TiXmlElement( Traits::getNameFromScalarProperty( ESFScalarProperties(i) ) );
    //            element_scalar_prop_i->SetDoubleAttribute("value",scalar_properties_[i]);
    //            element_scalar_prop->LinkEndChild(element_scalar_prop_i);
    //        }
    //        element->LinkEndChild(element_scalar_prop);

    //        // Save VectorProperties
    //        TiXmlElement * element_vector_prop = new TiXmlElement( "VectorProperties" );
    //        for(unsigned int i = 0; i<Traits::SF_VECTOR_PROPERTIES_CARDINAL; ++i)
    //        {
    //            TiXmlElement * element_vector_prop_i = new TiXmlElement( Traits::getNameFromVectorProperty( ESFVectorProperties(i) ) );
    //            for(unsigned int t=0; t<Vector::size_; ++t) // for adaptability to any Vector dimension
    //            {
    //                std::ostringstream val_t;
    //                val_t << "_" << t;
    //                element_vector_prop_i->SetDoubleAttribute(val_t.str(),vector_properties_[i][t]);
    //            }
    //            element_vector_prop->LinkEndChild(element_vector_prop_i);
    //        }
    //        element->LinkEndChild(element_vector_prop);

    //////////////////////
    /// Clone Function ///
    //////////////////////

    virtual std::shared_ptr<ScalarField> CloneForEval() const
    {
        return std::make_shared<NodePointSFieldT>(*this);
    }

    virtual std::shared_ptr<core::PointPrimitive> clone() const //(unsigned int /*start*/, unsigned int /*end*/) const
    {
        return std::make_shared<NodePointSFieldT>(*this, true);
    }

//    virtual inline std::string FunctorName() const
//    {
//        std::ostringstream name;
//        name << "<";
//        name << kernel_.Name() ;
//        name << kernel_.FunctorName() ;
//        name << kernel_.ParameterName() << ">";
//        return name.str();
//    }

//    ///////////////////
//    // Xml functions //
//    ///////////////////
//    virtual TiXmlElement * GetXml() const
//    {
//        TiXmlElement *element_base = new TiXmlElement(this->StaticName() );

//        AddXmlSimpleAttribute(element_base);
//        AddXmlComplexAttribute(element_base);
//        AddXmlFunctor(element_base);

//        return element_base;
//    }
//    //------------------------------------------------------------------------/
//    void AddXmlSimpleAttribute(TiXmlElement * element) const
//    {
//        Base::AddXmlSimpleAttribute(element);
//    }
//    //------------------------------------------------------------------------/
//    void AddXmlComplexAttribute(TiXmlElement * element) const
//    {
//        Base::AddXmlComplexAttribute(element);
//    }
//    //------------------------------------------------------------------------/
//    void AddXmlFunctor(TiXmlElement * element) const
//    {
//        TiXmlElement *element_functor_eval_point = new TiXmlElement( " TFunctorKernel" );
//        element->LinkEndChild(element_functor_eval_point);
//        TiXmlElement * element_child = kernel_.GetXml();
//        element_functor_eval_point->LinkEndChild(element_child);
//    }
   
    ////////////////////////////
    /// Evaluation Functions ///
    ////////////////////////////

    virtual Scalar Eval(const Point& point) const
    {
        return kernel_(*this, point);
    }
    //------------------------------------------------------------------------/
    virtual inline Vector EvalGrad(const Point& point, Scalar epsilon) const
    {
        return kernel_.EvalGrad(*this, point, epsilon);
    }
    //------------------------------------------------------------------------/
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        kernel_.EvalValueAndGrad(*this, point, epsilon_grad, value_res, grad_res);
    }
    //------------------------------------------------------------------------/
    virtual void EvalProperties(const Point& /*point*/,
                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                ) const
    {
        properties_.InlineEvalProperties(scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
    }
    //------------------------------------------------------------------------/
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                               const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                               const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                               Scalar& value_res, Vector& grad_res,
                                               std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               ) const
    {
        kernel_.EvalValueAndGrad(*this, point, epsilon_grad, value_res, grad_res);
        properties_.InlineEvalProperties(scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
    }
    //------------------------------------------------------------------------/
    virtual void EvalHomotheticValues(const Point& point, Scalar& value_res, Vector& grad_res) const
    {
        kernel_.EvalHomotheticValues(*this, point, value_res, grad_res);
    }

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual bool IsCompact() const
    {
        return kernel_.isCompact();
    }

    virtual inline Scalar GetDistanceBound(Scalar value) const
    {
        return kernel_.GetEuclidDistBound(*this, value);
    }

    virtual core::AABBox GetAxisBoundingBox(Scalar value) const;



    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const;


    virtual void GetPrecisionAxisBoundingBoxes(Scalar /*value*/, std::list<core::PrecisionAxisBoundingBox> &pboxes) const
    {
        //TODO:@todo : the way bounding box radius is computed has been hacked : the main reason was that
        // it gave horrible results for primitive with non compact support : we should find a better way to proceed

        const Scalar radius_factor = 1.25; //2.0;

        Scalar box_radius = radius_factor*weight_;

        core::AABBox box;
        for(unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i)
        {
            box.min_[i] = (*this)[i] - box_radius;
            box.max_[i] = (*this)[i] + box_radius;
        }
        pboxes.push_back(core::PrecisionAxisBoundingBox(box,weight_,0.5*weight_));
    }

    /////////////////
    /// Modifiers ///
    /////////////////

    virtual void updated()
    {
        WeightedPoint::updated();
        this->InvalidateBoundingBox();
    }

    // This function cannot be used to change the kernel type, only its attributes.
    void set_kernel(const TFunctorKernel& new_functor)
    {
        kernel_ = new_functor;
        this->InvalidateBoundingBox();
    }

    /////////////////
    /// Accessors ///
    /////////////////

    virtual const core::Functor& kernel_functor() const { return kernel_; }
    virtual core::Functor& kernel_functor() { return kernel_; }
    const TFunctorKernel& kernel() { return kernel_; }

    /////////////////
    /// Attributs ///
    /////////////////

private:
     TFunctorKernel kernel_;
};

template<typename  TFunctorKernel>
core::AABBox NodePointSFieldT< TFunctorKernel>::GetAxisBoundingBox(Scalar value) const
{
    core::AABBox res = WeightedPoint::GetAxisBoundingBox();

    Scalar dist_bound = GetDistanceBound(value);

    for(unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i)
    {
        res.min_[i] -= dist_bound;
        res.max_[i] += dist_bound;
    }

    return res;
}

} // Close namespace convol

} // Close namespace expressive

#include <convol/primitives/NodePointSFieldT.hpp>

#endif // CONVOL_POINT_SFIELD_H_
