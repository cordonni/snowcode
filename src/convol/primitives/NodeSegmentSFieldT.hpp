
#include <core/geometry/VectorTools.h> //TODO:@todo : I do not like this class !!!
#include <convol/tools/ConvergenceTools.h>

namespace expressive {

namespace convol {

template<typename TFunctorKernel>
Point NodeSegmentSFieldT<TFunctorKernel>::GetAPointAt(Scalar value, Scalar epsilon) const
{
    //TODO:@todo : function should definitely be improved to be improved ...
    // WARNING : If this function shows bugs, that could be
    //             - because the segment or the epsilon value is too small. This could indeed lead to
    //               precision issue depending on the scalar type. This function is not safe regarding
    //               this risk.
    //             - because the given bounding distance is bad, in this case check GetBound

    // @todo find a way to check and tell the user that this should be verified.
    /*
        if(value > Eval((this->seg_.p1()+this->seg_.p2())/2.0))
        {
        ork::Logger::WARNING_LOGGER->log("CONVOL", "Restriction violation in FunctorCauchy::GetAPointAt) : the given value must be lower than the field value at segment's middle point. see FunctorCauchy::GetAPointAt description for more details.");
        ork::Logger::WARNING_LOGGER->log("CONVOL", "No guarantee that a point will be found");
        }
        */

    Vector search_dir = expressive::VectorTools::GetOrthogonalRandVect(this->unit_dir_);
    // @todo this normamilization could be useless depending on GetOrthogonalRandVect... this last function should maybe return a normalized vector if the given vector is normalized...
    search_dir.normalize();

    // Accuracy for gradient computation
    // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
    Scalar grad_epsilon = this->length_*0.0001;    // arbitrary
    if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }

    Scalar max_bound = kernel_.GetEuclidDistBound(*this, value);
    if(max_bound != 0.0)
    {
        Scalar min_bound = 0.0;
        Scalar start_absc = max_bound/2.0;

        Point res;
        Convergence::SafeNewton1D(
                    *this,
                    (p1()+p2())/2.0, search_dir, // together give the line on which we step
                    min_bound,                                // security minimum bound
                    max_bound,                // security maximum bound
                    start_absc,            // point at which we start the search, given in 1D
                    value,
                    epsilon,
                    grad_epsilon,
                    10,
                    res);
        return res;
    }
    else
    {
        return (p1()+p2())/2.0;   // The surface does not exist, we return the middle of the segment
    }
}

template<typename TFunctorKernel>
auto NodeSegmentSFieldT<TFunctorKernel>::GetAxisBoundingBox(Scalar value) const -> core::AABBox
{
    core::AABBox res;

    //TODO:@todo : use the bounding function of the segment

    Scalar dist_bound = GetDistanceBound(value);
    for(unsigned int i = 0; i < Vector::RowsAtCompileTime; ++i)
    {
        if((p1())[i] < (p2())[i])
        {
            res.min_[i] = (p1())[i] - dist_bound;
            res.max_[i] = (p2())[i] + dist_bound;
        }
        else
        {
            res.min_[i] = (p2())[i] - dist_bound;
            res.max_[i] = (p1())[i] + dist_bound;
        }
    }
    return res;
}

} // Close namespace convol

} // Close namespace expressive

