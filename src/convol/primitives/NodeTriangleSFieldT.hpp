
#include <core/geometry/VectorTools.h> //TODO:@todo : I do not like this class ...
#include <convol/tools/ConvergenceTools.h>

namespace expressive {

namespace convol {

template<typename  TFunctorKernel>
Point  NodeTriangleSFieldT< TFunctorKernel>::GetAPointAt(Scalar value, Scalar epsilon) const
{
    // WARNING : If this function shows bugs, that could be
    //             - because the triangle or the epsilon value is too small. This could indeed lead to
    //               precision issue depending on the scalar type. This function is not safe regarding
    //               this risk.
    //             - because the given bounding distance is bad, in this case check GetBound

    // @todo find a way to check and tell the user that this should be verified.

    Vector search_dir = expressive::VectorTools::GetOrthogonalRandVect(this->unit_normal_);
    search_dir.normalize();

    // Accuracy for gradient computation
    Scalar grad_epsilon = epsilon*0.0001;    // arbitrary
    if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }

    Scalar max_bound = kernel_.GetEuclidDistBound(*this, value);
    if(max_bound != 0.0)
    {
        Scalar min_bound = 0.0;
        Scalar start_absc = max_bound/2.0;

        Point res;
        Convergence::SafeNewton1D(
                            *this,
                            (this->points_[0]+this->points_[1]+this->points_[2])/3.0, search_dir, // together give the line on which we step
                            min_bound,                                // security minimum bound
                            max_bound,                // security maximum bound
                            start_absc,            // point at which we start the search, given in 1D
                            value,
                            epsilon,
                            grad_epsilon,
                            10,
                            res);
        return res;
    }
    else
    {
        return (this->points_[0]+this->points_[1]+this->points_[2])/3.0;   // The surface does not exist, we return the middle of the triangle
    }
}

} // Close namespace convol

} // Close namespace expressive
