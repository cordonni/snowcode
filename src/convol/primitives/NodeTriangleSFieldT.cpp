#include <convol/primitives/NodeTriangleSFieldT.h>

#include <core/geometry/PrimitiveFactory.h>

// List of all kernel that can be used with a NodeTriangleSField
// (this is required to register them in the PrimitiveFacotry)

#include <convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.h>
#include <convol/functors/kernels/cauchyinverse/HomotheticCauchy.h>
#include <convol/functors/kernels/cauchyinverse/HomotheticInverse.h>
#include <convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.h>

#include <convol/functors/kernels/SemiNumericalHomotheticConvolT.h>

using namespace expressive::core;

namespace expressive {

namespace convol {


void AbstractNodeTriangleSField::InitFromXml(const TiXmlElement *element)
{
    //TODO:@todo : rendre les choses plus propre

///// TODO:@todo : load Properties0D from old style xml syntax where there is no xml "balise" <Properties> (directly ScalarProperties and then VectorProperties ...)
/////              if we change the syntax, an additional xml marker should be retrieved with a QueryFirstChildElement
    properties_.InitFromXml(element);
}

static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<HomotheticCompactPolynomial4> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_HomotheticCompactPolynomial4_Type;
static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<HomotheticCompactPolynomial6> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_HomotheticCompactPolynomial6_Type;
static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<HomotheticCompactPolynomial8> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_HomotheticCompactPolynomial8_Type;

static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<HomotheticCauchy3> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_HomotheticCauchy3_Type;
static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<HomotheticCauchy4> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_HomotheticCauchy4_Type;
static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<HomotheticCauchy5> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_HomotheticCauchy5_Type;
static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<HomotheticCauchy6> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_HomotheticCauchy6_Type;

static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<HomotheticInverse3> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_HomotheticInverse3_Type;
static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<HomotheticInverse4> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_HomotheticInverse4_Type;
static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<HomotheticInverse5> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_HomotheticInverse5_Type;
static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<HomotheticInverse6> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_HomotheticInverse6_Type;

static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<CompactedHomotheticInverse3> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_CompactedHomotheticInverse3_Type;
static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<CompactedHomotheticInverse4> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_CompactedHomotheticInverse4_Type;
static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<CompactedHomotheticInverse5> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_CompactedHomotheticInverse5_Type;
static PrimitiveFactory::Type<  NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<CompactedHomotheticInverse6> > >
                                                                NodeTriangleSFieldT_SemiNumericalHomotheticConvolT_CompactedHomotheticInverse6_Type;



} // Close namespace convol

} // Close namespace expressive
