/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: NodeISSubdivisionCurveT.h
 
 Language: C++
 
 License: Convol license
 
 \author: Cedric Zanni
 E-Mail:  cedric.zanni@inrialpes.fr
 
 Description: Class for the management of a special leaf consisting of a tesselated curve.
              It behave as a NodeOperatorNAry<Sum> except if inserted under a special node operator
              such as :
                      - GradientBasedIntegralSurfaceT
                      - NodeOperatorNCorrectedConvolutionT
                      - NodeOperatorNWitnessedBlendT

 Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_NODE_INTEGRAL_SURFACE_CURVE_T_H_
#define CONVOL_NODE_INTEGRAL_SURFACE_CURVE_T_H_

////////////////////////////////////////////////
/// TODO:@todo : prevoir mise a jour local ...
////////////////////////////////////////////////

// core dependencies
#include <core/CoreRequired.h>
    // geometry
//    #include <core/geometry/AABBox.h>
//    #include <core/geometry/PointCloud.h>
    #include <core/geometry/SubdivisionCurve.h>
    #include <core/geometry/WeightedPoint.h>
    #include <core/geometry/OptWeightedSegment.h>
    // functor
    #include <core/functor/Functor.h>

// convol dependencies
#include <convol/blobtreenode/NodeScalarField.h>
#include <convol/properties/Properties1D.h>

namespace expressive {

namespace convol {

class AbstractNodeISSubdivisionCurve :
    public NodeScalarField, public core::SubdivisionCurveT<core::WeightedPoint>
{
public:
    
    typedef core::SubdivisionCurveT<core::WeightedPoint> WeightedSubdivisionCurve;

    typedef core::SubdivisionCurveT<core::WeightedPoint> BaseGeometry;

    EXPRESSIVE_MACRO_NAME("NodeISSubdivisionCurveSField") // This macro defined functions
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractNodeISSubdivisionCurve()
        : NodeScalarField(StaticName()), WeightedSubdivisionCurve(), length_(0.0), properties_()
    {
        set_segments();
    }

    AbstractNodeISSubdivisionCurve(const WeightedSubdivisionCurve &w_curve)
        : NodeScalarField(StaticName()), WeightedSubdivisionCurve(w_curve), length_(0.0), properties_()
    {
        set_is_loop(w_curve.isLoop());
        set_segments();
    }

    AbstractNodeISSubdivisionCurve(const std::vector<core::WeightedPoint>& w_points, bool isLoop)
        : NodeScalarField(StaticName()), WeightedSubdivisionCurve(w_points, isLoop), length_(0.0), properties_()
    {
        set_segments();
    }

    // Copy constructor
    AbstractNodeISSubdivisionCurve(const AbstractNodeISSubdivisionCurve& rhs)
        : NodeScalarField(rhs), WeightedSubdivisionCurve(rhs), length_(rhs.length()), vec_seg_(rhs.vec_seg_), properties_(rhs.properties_) {}
    // Copy constructor
    AbstractNodeISSubdivisionCurve(const AbstractNodeISSubdivisionCurve& rhs, unsigned int start, unsigned int end)
        : NodeScalarField(rhs, true), WeightedSubdivisionCurve(rhs, start, end), length_(rhs.length()), vec_seg_(rhs.vec_seg_), properties_(rhs.properties_) {}

    virtual ~AbstractNodeISSubdivisionCurve() {}

    ///////////
    /// Xml ///
    ///////////

    // Forcing using baseGeometry::ToXml method.
    using BaseGeometry::ToXml;
    // Forcing using NodeScalarField::AddXmlAttributes method.
    using NodeScalarField::AddXmlAttributes;

    virtual void CONVOL_API InitFromXml(const TiXmlElement *element);

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual Scalar Eval(const Point& point) const = 0;
    //-------------------------------------------------------------------------
    virtual inline Vector EvalGrad(const Point& point, Scalar epsilon) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalProperties(const Point& point,
                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                ) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const = 0;
    //-------------------------------------------------------------------------
    virtual void EvalHomotheticValues(const Point& point, Scalar& value_res, Vector& grad_res) const = 0;

    //-------------------------------------------------------------------------
    virtual bool EvalHomotheticValuesAndParameters(const Point& point, Scalar& value_res, Vector& grad_res,
                                                   Scalar& abs, Vector& weighted_dir) const = 0;

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual Scalar GetDistanceBound(Scalar value) const = 0;
    //-------------------------------------------------------------------------
    virtual core::AABBox GetAxisBoundingBox(Scalar value) const = 0;

    virtual void GetPrecisionAxisBoundingBoxes(Scalar value, std::list<core::PrecisionAxisBoundingBox> &pboxes) const = 0;

    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const = 0;
    virtual void GetPrimitivesPointsAt(Scalar value, Scalar epsilon, core::PointCloud& res_points, const core::AABBox& b_box) const = 0;

    /////////////////
    /// Accessors ///
    /////////////////

    virtual const core::Functor& kernel_functor() const = 0;
    virtual core::Functor& kernel_functor() = 0;
    //-------------------------------------------------------------------------
    Properties1D& properties() {  return properties_; } //TODO:@todo : check where this is used and perhaps change it by a modifier
    const Properties1D& properties() const {  return properties_; }

    Scalar length() const { return length_; }

    /////////////////
    /// Modifiers ///
    /////////////////

    virtual void updated()
    {
        WeightedSubdivisionCurve::updated();
        this->InvalidateBoundingBox();
    }

    virtual void ComputeSubdivision()
    {
        WeightedSubdivisionCurve::ComputeSubdivision();
        set_segments();
    }

    inline void set_properties(const Properties1D &prop) { properties_ = prop; }

    /* set segment from points */
    void CONVOL_API set_segments();
    /* warning : with properties!!!! they should stay coherent : for now not the case !!! */
    /* warning : we should find a way to store the modified area when this method is called */

    /////////////////
    /// Accessor ///
    /////////////////

    const std::vector<std::pair<core::AABBox, core::OptWeightedSegment> >& vec_seg() { return vec_seg_; }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    Scalar length_;

    std::vector<std::pair<core::AABBox, core::OptWeightedSegment> > vec_seg_;

    Properties1D properties_; 
    //TODO:@todo : less expensive to compute than if stored with segment, 
    //             BUT require update in function of the modification of the segment that are a bit more tricky => TODO !!!
};




template<typename TFunctorKernel>
class NodeISSubdivisionCurveT
    : public AbstractNodeISSubdivisionCurve
{
public:
    
    typedef core::OptWeightedSegment OptWeightedSegment;

    typedef AbstractNodeISSubdivisionCurve::BaseGeometry BaseGeometry;
    typedef TFunctorKernel FunctorKernel;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    /*!
     *  WARNING : if this constructor is used, Init method should be called later (before effective use of the object)
     */
    NodeISSubdivisionCurveT(const TFunctorKernel& kernel)
        : AbstractNodeISSubdivisionCurve(), kernel_(kernel)
    { }
    //TODO:@todo : add a constructor with the other data from abstract type !

    NodeISSubdivisionCurveT(const TFunctorKernel& kernel, const std::vector<core::WeightedPoint>& w_points, bool isLoop = false)
        : AbstractNodeISSubdivisionCurve(w_points, isLoop), kernel_(kernel)
    { }

    NodeISSubdivisionCurveT(const WeightedSubdivisionCurve &w_curve, const TFunctorKernel& kernel)
        : AbstractNodeISSubdivisionCurve(w_curve), kernel_(kernel)
    { }

    //TODO:@todo : doc : operator de recopie for cloning
    NodeISSubdivisionCurveT(const NodeISSubdivisionCurveT &rhs)
        : AbstractNodeISSubdivisionCurve(rhs), kernel_(rhs.kernel_)
    { }
    NodeISSubdivisionCurveT(const NodeISSubdivisionCurveT &rhs, unsigned int start, unsigned int end)
        : AbstractNodeISSubdivisionCurve(rhs, start, end), kernel_(rhs.kernel_)
    { }


    virtual ~NodeISSubdivisionCurveT(){}
    
    //////////////////////
    /// Clone Function ///
    //////////////////////

    virtual std::shared_ptr<ScalarField> CloneForEval() const
    {
        return std::make_shared<NodeISSubdivisionCurveT>(*this);
    }

    virtual std::shared_ptr<core::GeometryPrimitiveT<core::WeightedPoint> > clone(unsigned int start, unsigned int end) const
    {
        return std::make_shared<NodeISSubdivisionCurveT>(*this, start, end);
    }

    ///////////
    /// Xml ///
    ///////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        AbstractNodeISSubdivisionCurve::AddXmlAttributes(element);
        TiXmlElement *element_functor_eval_seg = new TiXmlElement( "TFunctorKernel" );
        element->LinkEndChild(element_functor_eval_seg);
        TiXmlElement * element_child = kernel_.ToXml();
        element_functor_eval_seg->LinkEndChild(element_child);

        TiXmlElement * element_properties = properties_.ToXml();
        element->LinkEndChild(element_properties);
    }

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    Scalar Eval(const Point& point) const;
    Vector EvalGrad(const Point& point, Scalar epsilon_grad) const;
    void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;
    void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                       const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                       const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                       Scalar& value_res, Vector& grad_res,
                                       std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                       std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                      ) const;
    void EvalProperties(const Point& point,
                        const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                        const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                        std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                        std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                       ) const;

    //TODO:@todo : special function for special operator !!! add doc about that
    void EvalHomotheticValues(const Point& point, Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------
    bool EvalHomotheticValuesAndParameters(const Point& point, Scalar& value_res, Vector& grad_res,
                                           Scalar& abs, Vector& weighted_dir) const;

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////
    virtual bool IsCompact() const
    {
        return kernel_.isCompact();
    }

    virtual inline Scalar GetDistanceBound(Scalar value) const
    {
        Scalar max_weight = 0.0;
        for(auto &p : points_)
        {
            max_weight = (p.weight()>max_weight) ? p.weight() : max_weight;
        }
        return kernel_.GetEuclidDistBound1D(max_weight, value);
    }

    core::AABBox GetAxisBoundingBox(Scalar value) const;
    const core::AABBox& InitAxisBoundingBox(Scalar value);

    virtual void GetPrecisionAxisBoundingBoxes(Scalar value, std::list<core::PrecisionAxisBoundingBox> &pboxes) const;

//    virtual void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
//                                typename std::map<BlobtreeNode*, typename std::pair<core::AABBox, core::AABBox> >& updated_areas);

    virtual void GetPrimitivesPointsAt(Scalar value, Scalar epsilon, core::PointCloud& res_points, const core::AABBox& b_box) const;
    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const;


    /////////////////
    /// Modifiers ///
    /////////////////

    // This function cannot be used to change the kernel type, only its attributes.
    void set_kernel(const TFunctorKernel& kernel)
    {
        kernel_ = kernel;
    }

    /////////////////
    /// Accessors ///
    /////////////////

    const core::Functor& kernel_functor() const { return kernel_; }
    core::Functor& kernel_functor() { return kernel_; }
    const TFunctorKernel& kernel() const { return kernel_; }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    TFunctorKernel kernel_;
};

} // Close namespace convol

} // Close namespace expressive


// implementation file
#include <convol/primitives/NodeISSubdivisionCurveT.hpp>

#endif // CONVOL_NODE_INTEGRAL_SURFACE_CURVE_T_H_

