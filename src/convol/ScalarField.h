/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ScalarField.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail:  maxime.quiblier@inrialpes.fr

   Description: Header for a the scalar field super-class.

   Platform Dependencies: None
*/


//////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : uncomment features and update code when the feature became usefull ...
/// TODO:@todo : the way properties are handled should probably be changed ...
//////////////////////////////////////////////////////////////////////////////////////////


#pragma once
#ifndef CONVOL_SCALAR_FIELD_T_H_
#define CONVOL_SCALAR_FIELD_T_H_

// core dependencies
    // geometry
    #include<core/geometry/AABBox.h>
    #include<core/geometry/PrecisionAxisBoundingBox.h>

// convol dependencies
#include <convol/ConvolRequired.h>
//    // Tools
//    #include<convol/tools/GradientEvaluationTools.h>

//// std dependencies
//#include <string>
//#include <vector>
#include <list>

//// tinyxml dependencies
//#include<tinyxml/tinyxml.h>
#include "core/ExpressiveTraits.h"

namespace  expressive {

namespace convol {

/*!
 * \brief Abstract class for all scalar field classes.
 *
 *  In Convol a scalar field is a function Point -> Scalar.
 *  This function can be evaluated at any point in space in which the Point lie.
 *
 * \tparam TTraits The Traits type that must define : Scalar, Point, Vector, Normal, ESFScalarProperties, ESFVectorProperties
 */
class CONVOL_API ScalarField
{
public:

//    unsigned long AssignXmlId()
//    {
//        //static unsigned long counter = 0;
//        //return counter++;
//        return (unsigned long) this; // The above scheme has been changed to fall back to the ancient technique because of plugins systems :
//        // Indeed, since the function is static but not necessarily shared between libraries, problems can occured...
//    }

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////
    ScalarField()
    {
//        xml_id_ = AssignXmlId();
    }
    //-------------------------------------------------------------------------
    virtual ~ScalarField(){}

    ScalarField(const ScalarField &) { }

    /*!
     * \brief CloneForEval provide a "partial copy" of "this" that can be evaluated in a different thread.
     *        WARNING : This copy is not meant to be modified !
     * \return std::shared_ptr<ScalarField> which is the clone of "this".
     *         The clone can use different data structures in order to provide better performance.
     */
    virtual std::shared_ptr<ScalarField> CloneForEval() const = 0;

    ////////////////////
    // Name functions //
    ////////////////////
    /** \brief Give the name of the scalar field. Should be unique among Convol elements.
    *          Helps especially for XML load/save and research interfaces.
    *
    * \return A std::string
    */
//    virtual std::string Name() const = 0;
    //TODO:@todo : this should probably be a static function ... ?

//    /** \brief Give the name of Functors (if functors there is) specializing the sub-ScalarField class ( the scalar field. Should be unique among Convol elements.
//    *          Helps for research interfaces.
//    *
//    * \return A std::string
//    */
//    virtual std::string FunctorName() const
//    {
//        return "";
//    }
//    /** \brief Give the name of Parameters (if any). Only useful for research interfaces.
//    *
//    * \return A std::string containing a description of parameters.
//    */
//    virtual std::string ParameterName() const
//    {
//        return "";
//    }

//    virtual std::string UserFriendlyName() const
//    {
//        return this->Name() + this->FunctorName() + this->ParameterName();
//    }

    ///////////////////
    // Xml functions //
    ///////////////////

//	/** \brief Return a unique id for this ScalarField (among all other instantiated ScalarFields).
//	*		   Especially useful for XML.
//    */
//    unsigned long XmlId() const { return xml_id_; }

    
    //////////////////////////
    // Evaluation functions //
    //////////////////////////

    /** \brief Evaluate the scalar field in space at Point point.
    *
    *   \param point Point in space where we want the field value
    *   \return Field value at point
    */
    virtual Scalar Eval(const Point& point) const = 0;

    /** \brief Evaluate the gradient scalar field at point point in space.
    *          This function is called for any sub-class that does not
    *          re-implement it.
    *          In this case it performs a numerical computation.
    *   \param point Point in space where we want the gradient
    *   \param epsilon Accuracy for numerical gradient computation.
    *   \return gradient (not normalized)
    */
    virtual Vector EvalGrad(const Point& point, Scalar epsilon) const;

    /** \brief Evaluate the scalar field Properties at point point in space.
    *
    *   \param point Point in space where we want to evaluate the properties
    *   \param scal_prop_ids   Vector of Ids of scalar properties we want to be computed. Ids are defined in Traits::ESFScalarProperties.
    *   \param vect_prop_ids   Vector of Ids of vector properties we want to be computed. Ids are defined in Traits::ESFVectorProperties.
    *   \param scal_prop_res   Scalar properties result corresponding to ids in scal_prop_ids.
    *   \param vect_prop_res   Vector properties result corresponding to ids in vect_prop_ids.
    */
    virtual void EvalProperties(const Point& /*point*/,
                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                ) const
    {
        assert(false);
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = ExpressiveTraits::kSFScalarPropertiesDefault[scal_prop_ids[i]];
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = ExpressiveTraits::kSFVectorPropertiesDefault[vect_prop_ids[i]];
        }
    }
    /** \brief Evaluate the scalar field value and its gradient at point point in space.
    *
    *   \param point Point in space where we want the gradient
    *   \param epsilon_grad Accuracy for numerical gradient computation.
    *   \param value_res Resulting field value of the field.
    *   \param grad_res Resulting gradient value of the field.
    */
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        assert(false); // Not implemented. Implement it in your program or replace it using the 2 lines below. Why? becaus otherwise you don't know waht you do :)
        value_res = Eval(point);
        grad_res = EvalGrad(point, epsilon_grad);
    }

    /** \brief Evaluate the scalar field value, its gradient and its properties at point point in space.
    *
    *   \param point Point in space where we want the gradient
    *   \param epsilon_grad Accuracy for numerical gradient computation.
    *   \param scal_prop_ids    Vector of Ids of scalar properties we want to be computed. Ids are defined in Traits::ESFScalarProperties.
    *   \param vect_prop_ids    Vector of Ids of vector properties we want to be computed. Ids are defined in Traits::ESFVectorProperties.
    *
    *   \param value_res Resulting field value of the field.
    *   \param grad_res Resulting gradient value of the field.
    *   \param scal_prop_res Resulting Scalar properties corresponding to ids in scal_prop_ids.
    *   \param vect_prop_res Resulting Vector properties corresponding to ids in vect_prop_ids.
    */
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
    {
        assert(false); // Not implemented. Implement it in your program or replace it using the 2 lines below. Why? because otherwise you don't know waht you do :)
        EvalValueAndGrad(point, epsilon_grad, value_res, grad_res);
        EvalProperties(point, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
    }

    /** \brief Evaluate the gaussian curvature of the scalar field. WORKS ONLY IN 3D.
    *          Formulae are taken from Goldman R. Curvature formulas for implicit curves and surfaces. in Computer aided geometric design 22, 7 (2005). p632-658.
    *   
    *   \param point Point in space where we want the curvature
    *   \param epsilon Accuracy for numerical gradient and hessian computation.
    *   
    *   \return Numercial Gaussian curvature at point.
    *
    *   @todo : THIS WORKS ONLY IN 3D, could we find a way to make it work in 2D? (Actually Convol does not "work" in 2D, properly speaking since it has not been tested in 2D...).
    */
    virtual Scalar EvalGaussianCurvature(const Point& point, Scalar epsilon) const;


    /** \brief Special evaluation function that compute an homothetic value and a homothetic gradient.
     *         Used in GradientBasedIntegralSurface. WARNING : this can't be used with any scalar field...
    *
    *   \param point Point in space where we want the special values
    *   \param value_res Resulting homothetic field value of the field.
    *   \param grad_res Resulting homothetic gradient value of the field.
    */
    virtual void EvalHomotheticValues(const Point& point, Scalar& value_res, Vector& grad_res) const
    {
        assert(false); // No General implementation, only implemented in NodeScalarField and in PointSField, SegmentSField and TriangleSField
        UNUSED(point);
        value_res = 0.0;
        grad_res = Vector::Zero();
    }


//    /** \brief Evaluate the given Scalar Property
//    *
//    *   Dev note : for convenience when defining a new ScalarField, this fuinction is not pure virtual.
//    *              So you don't have to re-implement it if you do not need it, however, an assert will be
//    *              triggered if called without being redefined.
//    *              @todo Maybe should Logger::ERROR_LOGGER->log something in release...
//    *
//    *   \param point Point at which the property value is wanted.
//    *   \param id The property ID as defined in Traits.
//    *
//    *   \return the value of the given property at the given point.
//    */
//    virtual Scalar EvalScalarProperty(const Point& point, ESFScalarProperties id) const
//    {
//        UNUSED(point);
//        UNUSED(id);
//        assert(false);
//        return 0.0;
//    }
//    /** \brief Evaluate the given Scalar Property
//    *
//    *   Dev note : for convenience when defining a new ScalarField, this fuinction is not pure virtual.
//    *              So you don't have to re-implement it if you do not need it, however, an assert will be
//    *              triggered if called without being redefined.
//    *              @todo Maybe should Logger::ERROR_LOGGER->log something in release...
//    *
//    *   \param point Point at which the property value is wanted.
//    *   \param id The property ID as defined in Traits.
//    *
//    *   \return the value of the given property at the given point.
//    */
//    virtual Vector EvalVectorProperty(const Point& point, ESFVectorProperties id) const
//    {
//        UNUSED(point);
//        UNUSED(id);
//        assert(false);
//        return Vector::vectorized(0.0);
//    }

    /** \brief Find a point p in space where Eval(p) == value
    *        This is especially used for triangulation algorithms which needs
    *        a starting point on the surface.
    *
    *   Dev note : for convenience when defining a new ScalarField, this function is not pure virtual.
    *              So you don't have to re-implement it if you do not need it, however, an assert will be 
    *              triggered if called without being redefined.
    *              @todo Maybe should Logger::ERROR_LOGGER->logsomething in release...
    *
    *   \param value The value of the ScalarField we went for the point found.
    *   \param epsilon We want the result to be at least epsilon close to the surface with respect to the distance Traits::Vector.norm()
    *
    *   \return point p such that value-epsilon < Eval(p) < value+epsilon
    */
    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const
    {
        assert(false);
        UNUSED(value); 
        UNUSED(epsilon);
        return Point::Zero();
    }

    /** \brief Return wether the defined field has a local or global support
    *
    *   \return point p such that value-epsilon < Eval(p) < value+epsilon
    */
    virtual bool IsCompact() const
    {
        //TODO:@todo : je n'aime pas comment c'est fait, car cette fonction n'a pas tjrs de sens actuellement ...
        return false;
    }

    /** \brief Compute/Get the \link Convol::AABBox AABBox \endlink for this field, with resoect to the given scalar value.
    *          A bounding box is given relative to a scalar value, the result is guaranteed to be such that any point outside the box verify : ScalarField::Eval(p) < value
    *           
    *
    *   Dev note : for convenience when defining a new ScalarField, this function is not pure virtual.
    *              So you don't have to re-implement it if you do not need it, however, an assert will be 
    *              triggered if called without being redefined.
    *
    *   \param value ScalarField value to "fit", ie all points outside the resulting bounding box are guaranted to map to a ScalarField value less than value.
    *
    *   \return AABBox Axis aligned boundinx box as small verifying the return condition.
    */
    virtual core::AABBox GetAxisBoundingBox(Scalar value) const
    {
        // \todo Maybe should Logger::ERROR_LOGGER->log something in release...
        assert(false);
        UNUSED(value);
        core::AABBox res;
        return res;
    }

    virtual void GetPrecisionAxisBoundingBoxes(Scalar value, std::list<core::PrecisionAxisBoundingBox > &pboxes) const
    {
        // Dummy values ...
        pboxes.push_back(core::PrecisionAxisBoundingBox(GetAxisBoundingBox(value),0.4,0.1));
    }

//    ///////////////
//    // Modifiers //
//    ///////////////

//    /** \brief Set the given property with the given value.
//    *          It is a convenience function that will reset the set of values in the object to just this value.
//    *          Note that a \link Convol::ScalarField ScalarField \endlink can have a set of value for the same property, values that are interpolated in space depending on the \link Convol::ScalarField ScalarField \endlink.
//    *
//    *   \param val Scalar value to set for this property
//    *   \param id The property ID as defined in Traits.
//    */
//    virtual void SetScalarProperty(ESFScalarProperties id, Scalar val)
//    {
//        UNUSED(id);
//        UNUSED(val);
//        assert(false); // should not be called if not re-implemented
//    }
//    /** \brief Set the given property with the given value.
//    *          It is a convenience function that will reset the set of values in the object to just this value.
//    *          Note that a \link Convol::ScalarField ScalarField \endlink can have a set of value for the same property, values that are interpolated in space depending on the \link Convol::ScalarField ScalarField \endlink.
//    *
//    *   \param val Vector value to set for this property
//    *   \param id The property ID as defined in Traits.
//    */
//    virtual void SetVectorProperty(ESFVectorProperties id, const Vector& val)
//    {
//        UNUSED(id);
//        UNUSED(val);
//        assert(false); // should not be called if not re-implemented
//    }

//protected:

//	// Unique id among all instantiated ScalarField
//    unsigned long xml_id_;

//    ///////////////////
//    // Xml Functions //
//    ///////////////////
//    /** \brief To be called by any inherited class in the GetXml Function.
//    *
//    */
//    void AddXmlSimpleAttribute(TiXmlElement * element) const
//    {
//        element->SetAttribute("id",XmlId());
//    }
//    /** \brief To be called by any inherited class in the GetXml Function.
//    */
//    void AddXmlComplexAttribute(TiXmlElement * element) const
//    {
//        UNUSED(element);
//    }

}; // End class ScalarField definition

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_SCALAR_FIELD_T_H_
