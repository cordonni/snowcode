#include <convol/ScalarField.h>

// core dependencies
    // Tools
    #include<convol/tools/GradientEvaluationTools.h>

namespace  expressive {

namespace convol {


Vector ScalarField::EvalGrad(const Point& point, Scalar epsilon) const
{
    return GradientEvaluation::NumericalGradient6points(*this, point, epsilon);
}


Scalar ScalarField::EvalGaussianCurvature(const Point& point, Scalar epsilon) const
{
    assert(Point::RowsAtCompileTime == 3);

    Vector grad = EvalGrad(point,epsilon);
    Scalar Fxx, Fxy, Fxz, Fyx, Fyy, Fyz, Fzx, Fzy, Fzz;
    Vector aux = (EvalGrad(point + Point(epsilon,0.0,0.0), epsilon) - EvalGrad(point - Point(epsilon,0.0,0.0), epsilon))/epsilon;
    Fxx = aux[0];
    Fyx = aux[1];
    Fzx = aux[2];
    aux = (EvalGrad(point + Point(0.0,epsilon,0.0), epsilon) - EvalGrad(point - Point(0.0,epsilon,0.0), epsilon))/epsilon;
    Fxy = aux[0];
    Fyy = aux[1];
    Fzy = aux[2];
    aux = (EvalGrad(point + Point(0.0,0.0,epsilon), epsilon) - EvalGrad(point - Point(0.0,0.0,epsilon), epsilon))/epsilon;
    Fxz = aux[0];
    Fyz = aux[1];
    Fzz = aux[2];

    Vector HstarX;
    HstarX[0] = Fyy*Fzz - Fyz*Fzy;
    HstarX[1] = Fxz*Fzy - Fxy*Fzz;
    HstarX[2] = Fxy*Fyz - Fxz*Fyy;
    Vector HstarY;
    HstarY[0] = Fyz*Fzx - Fyx*Fzz ;
    HstarY[1] = Fxx*Fzz - Fxz*Fzx ;
    HstarY[2] = Fyx*Fxz - Fxx*Fyz ;
    Vector HstarZ;
    HstarZ[0] = Fyx*Fzy - Fyy*Fzx;
    HstarZ[1] = Fxy*Fzx - Fxx*Fzy;
    HstarZ[2] = Fxx*Fyy - Fxy*Fyx;

    aux[0] = grad.dot(HstarX);
    aux[1] = grad.dot(HstarY);
    aux[2] = grad.dot(HstarZ);

    Scalar res = aux.dot(grad);

    return res/(grad.squaredNorm()*grad.squaredNorm());
}


} // Close namespace convol

} // Close namespace expressive
