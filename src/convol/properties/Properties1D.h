/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: Properties1D.h

   Language: C++

   License: Expressive license

   \author: Maxime Quiblier, Cedric Zanni
   E-Mail:  maxime.quiblier@inrialpes.fr

   Description: .

   Platform Dependencies: None
*/


#pragma once
#ifndef CONVOL_PROPERTIES1D_T_H_
#define CONVOL_PROPERTIES1D_T_H_

// core dependencies
#include <core/CoreRequired.h>

#include <tinyxml/tinyxml.h>

#include <convol/ConvolRequired.h>

#include "core/ExpressiveTraits.h"

//////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : the way properties are handled should probably be changed
///  if the number of properties is not really dynamic then use an array instead of a vector otherwise : ok
/// TODO:@todo : should try to use move operator and constructor wherever possible
//////////////////////////////////////////////////////////////////////////////

namespace  expressive {

namespace convol {

/*!
 * \brief Class for a property constraint in 1 dimension.
          For a 1D field, the property is defined with respect to a Scalar (s).
 */
template<typename PropertyType>
struct PropConstraint1DT
{
    PropConstraint1DT()
    {}
    PropConstraint1DT(Scalar ss, const PropertyType& val) : s(ss), value(val)
    {}
    PropConstraint1DT(const PropConstraint1DT &rhs) : s(rhs.s), value(rhs.value)
    {}

    Scalar s;
    PropertyType value;
};


/*!
 * \brief Class for default handling of properties in 1D primitives (such as segment, arc of circle, ...).
 */
class CONVOL_API Properties1D
{
public :
    EXPRESSIVE_MACRO_NAME("Properties1D")
    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    Properties1D();

    Properties1D(const Properties1D &rhs);

    virtual ~Properties1D();

    /////////////////
    /// Modifiers ///
    /////////////////

    //    // Convenience function to set the value of an attribute.
    //    // WARNING : this reset the number of constraint to 1 (ie 1 color for the whole segment)
    //    virtual void SetScalarProperty(ESFScalarProperties id, Scalar val)
    //    {
    //        scalar_properties_[id].resize(1);
    //        scalar_properties_[id][0].s = 0.0;
    //        scalar_properties_[id][0].value =  val;
    //    }
    //    //-------------------------------------------------------------------------
    //    // Copy properties from another AbstractSegmentSField
    //    void CopyProperties(const AbstractSegmentSFieldT& other)
    //    {
    //        for(unsigned int i = 0; i<Traits::SF_SCALAR_PROPERTIES_CARDINAL; ++i){
    //            scalar_properties_[i] = other.scalar_properties()[i];
    //        }
    //        for(unsigned int i = 0; i<Traits::SF_VECTOR_PROPERTIES_CARDINAL; ++i){
    //            vector_properties_[i] = other.vector_properties()[i];
    //        }
    //    }

        // do scalar_properties_[id] = contraints;
    void SetScalarPropertyConstraints(ESFScalarProperties id, PropConstraint1DT<Scalar> constraints)
    {
        assert(constraints.s == 0.0);
        scalar_properties_[id].clear();
        scalar_properties_[id].push_back(constraints);
    }

    void SetScalarPropertyConstraints(ESFScalarProperties id, const std::vector< PropConstraint1DT<Scalar> >& contraints)
        {
            assert(contraints.size() > 0);
            assert(contraints[0].s == 0.0);
            assert(contraints.size()==1 || contraints[contraints.size()-1].s == 1.0);
            scalar_properties_[id] = contraints;
        }
    //    //-------------------------------------------------------------------------
    //    // s in [0.0,1.0]
    //    // Note : the std::vector scalar_properties_[id] must stay sorted by s.
    //    void AddScalarPropertyConstraint(ESFScalarProperties id, Scalar s, Scalar val)
    //    {
    //        assert(s>=0.0);
    //        assert(s<=1.0);
    //        std::vector< PropConstraint1DT<Scalar,Scalar> >& scalar_properties_id = scalar_properties_[id];
    //        unsigned int ssize = scalar_properties_id.size();
    //        if(ssize == 1)
    //        {
    //            scalar_properties_id.push_back(scalar_properties_id[0]);
    //            scalar_properties_id[1].s = 1.0;
    //        }
    //        for(unsigned int i = 0; i<ssize;++i)
    //        {
    //            if(s <= scalar_properties_id[i].s)
    //            {
    //                if(scalar_properties_id[i].s == s)
    //                {
    //                    scalar_properties_id[i].value = val;
    //                }
    //                else
    //                {
    //                    scalar_properties_id.push_back(scalar_properties_id[ssize-1]);
    //                    for(unsigned k = ssize-1; k>i; --k)
    //                    {
    //                        scalar_properties_id[k] = scalar_properties_id[k-1];
    //                    }
    //                    scalar_properties_id[i].s = s;
    //                    scalar_properties_id[i].value = val;
    //                }
    //                break;
    //            }
    //        }
    //    }
        //-------------------------------------------------------------------------
        // Convenience function to set the value of an attribute.
        // WARNING : this reset the number of constraint to 1 (ie 1 color for the whole segment)
        void SetVectorProperty(ESFVectorProperties id, const Vector& val)
        {
            vector_properties_[id].resize(1);
            vector_properties_[id][0].s = 0;
            vector_properties_[id][0].value = val;
        }
        //-------------------------------------------------------------------------
        // do vector_properties_[id] = contraints, so be careful the given constraints vector must be sorted by s
        void SetVectorPropertyConstraints(ESFVectorProperties id, const std::vector< PropConstraint1DT<Vector> >& contraints)
        {
            assert(contraints.size() > 0);
            assert(contraints[0].s == 0.0);
            assert(contraints.size()==1 || contraints[contraints.size()-1].s == 1.0);
            vector_properties_[id] = contraints;
        }
    //    //-------------------------------------------------------------------------
    //    // s in [0.0,1.0]
    //    // Note : the std::vector vector_properties_[id] must stay sorted by s.
    //    void AddVectorPropertyConstraint(ESFVectorProperties id, Scalar s, const Vector& val)
    //    {
    //        assert(s>=0.0);
    //        assert(s<=1.0);
    //        std::vector< PropConstraint1DT<Scalar,Vector> >& vector_properties_id = vector_properties_[id];
    //        unsigned int ssize = vector_properties_id.size();
    //        if(ssize == 1)
    //        {
    //            vector_properties_id.push_back(vector_properties_id[0]);
    //            vector_properties_id[1].s = 1.0;
    //        }
    //        for(unsigned int i = 0; i<ssize;++i)
    //        {
    //            if(s <= vector_properties_id[i].s)
    //            {
    //                if(vector_properties_id[i].s == s)
    //                {
    //                    vector_properties_id[i].value = val;
    //                }
    //                else
    //                {
    //                    vector_properties_id.push_back(vector_properties_id[ssize-1]);
    //                    for(unsigned k = ssize-1; k>i; --k)
    //                    {
    //                        vector_properties_id[k] = vector_properties_id[k-1];
    //                    }
    //                    vector_properties_id[i].s = s;
    //                    vector_properties_id[i].value = val;
    //                }
    //                break;
    //            }
    //        }
    //    }


    /////////////////
    /// Accessors ///
    /////////////////
    /// TODO:@todo : pourquoi ce n'est pas des references ?
    std::vector< PropConstraint1DT<Scalar> > const* scalar_properties() const { return scalar_properties_; }
    std::vector< PropConstraint1DT<Vector> > const* vector_properties() const { return vector_properties_; }
    //-------------------------------------------------------------------------


    //////////////////
    /// Evaluation ///
    //////////////////



    inline void InlineEvalProperties(Scalar s,
                                     const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                     const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                     std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                     std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                     ) const
    {
        const unsigned int n_scal_prop = (unsigned int) scal_prop_ids.size();
        for(unsigned int i = 0; i<n_scal_prop; ++i)
        {
            const std::vector< PropConstraint1DT<Scalar> >& aux_keys = scalar_properties_[scal_prop_ids[i]];
            scal_prop_res[i] = InlineSwitchKeysNumber<Scalar>(s,aux_keys, ExpressiveTraits::kSFScalarPropertiesDefault[scal_prop_ids[i]]);
        }

        const unsigned int n_vect_prop = (unsigned int) vect_prop_ids.size();
        for(unsigned int i = 0; i<n_vect_prop; ++i)
        {
            const std::vector< PropConstraint1DT<Vector> >& aux_keys = vector_properties_[vect_prop_ids[i]];
            vect_prop_res[i] = InlineSwitchKeysNumber<Vector>(s,aux_keys, ExpressiveTraits::kSFVectorPropertiesDefault[vect_prop_ids[i]]);
        }
    }


    //-------------------------------------------------------------------------
    // Convenience function for InlineEvalProperties
    // Be careful, s will be modified !!
    template<typename PropertyType>
    inline PropertyType InlineSwitchKeysNumber(Scalar s, const std::vector< PropConstraint1DT<PropertyType> >& aux_keys, const PropertyType default_res) const
    {
        switch(aux_keys.size())
        {
        case 0:
            assert(false);
            return default_res;
            break;
        case 1:
            return aux_keys[0].value;
            break;
        case 2:
        {
            if(s>1.0)
            {
                return aux_keys[1].value;
            }
            else
            {
                if(s<=0.0)
                {
                    return aux_keys[0].value;
                }
                else
                {
                    return (1.0-s)*aux_keys[0].value + s*aux_keys[1].value;
                }
            }
        }
            break;
        case 3:
        {
            if(s>1.0)
            {
                return aux_keys[2].value;
            }
            else
            {
                if(s<=0.0)
                {
                    return aux_keys[0].value;
                }
                else
                {
                    if(s<aux_keys[1].s)
                    {
                        Scalar t = s/aux_keys[1].s;
                        return (1.0-t)*aux_keys[0].value + t*aux_keys[1].value;
                    }
                    else
                    {
                        Scalar t = (s-aux_keys[1].s)/(1.0 - aux_keys[1].s);
                        return (1.0-t)*aux_keys[1].value + t*aux_keys[2].value;
                    }

                }
            }
        }
            break;
        default:
            assert(false); // not implemented
            return default_res;
            break;
        }
    }

    ////////////////////
    /// Xml function ///
    ////////////////////
    void InitFromXml(const TiXmlElement *e);

    /*!
     *  WARNING : this function can't handle a number of constraint (per property type)
     *            that require more than 2 digit (and it can't handle Vector of dimension
     *            greater than 9)
     */
    virtual TiXmlElement* ToXml(const char * name = nullptr) const;

protected:
    /////////////////
    /// Attributs ///
    /////////////////

    // Properties to attach to primitives according to traits
    // For segments, each property can be present as a vector of constraints (s, constraint_value) with s in [0,1]
    std::vector< PropConstraint1DT<Scalar> > scalar_properties_[ExpressiveTraits::SF_SCALAR_PROPERTIES_CARDINAL];
    std::vector< PropConstraint1DT<Vector> > vector_properties_[ExpressiveTraits::SF_VECTOR_PROPERTIES_CARDINAL];

}; // End class Properties1D definition

} // Close namespace convol

} // Close namespace expressive






//// Save properties as xml

//        // Save ScalarProperties
//        TiXmlElement * element_scalar_prop = new TiXmlElement( "ScalarProperties" );
//            for(unsigned int i = 0; i<Traits::SF_SCALAR_PROPERTIES_CARDINAL; ++i)
//            {
//                // Build XML entry < getNameFromScalarProperty size>
//                //                      <s     _0="..." _1="..." ... _size-1="..." />
//                //                      <value _0="..." _1="..." ... _size-1="..." />
//                //                 </getNameFromScalarProperty>
//                TiXmlElement * element_scalar_prop_i = new TiXmlElement( Traits::getNameFromScalarProperty( ESFScalarProperties(i) ) );
//                const std::vector< PropConstraint1DT<Scalar,Scalar> >& vec_prop = scalar_properties_[i];
//                element_scalar_prop_i->SetAttribute("size",vec_prop.size());
//                    TiXmlElement * element_seg_scal_prop_i_s = new TiXmlElement( "s" );
//                    TiXmlElement * element_seg_scal_prop_i_v = new TiXmlElement( "value" );
//                    for(unsigned int k=0; k<vec_prop.size(); ++k)
//                    {
//                        // Build string index
//                        std::ostringstream s_k;
//                        s_k << "_" << k;
//                        element_seg_scal_prop_i_s->SetDoubleAttribute(s_k.str(),vec_prop[k].s);
//                        element_seg_scal_prop_i_v->SetDoubleAttribute(s_k.str(),vec_prop[k].value);
//                    }
//                    element_scalar_prop_i->LinkEndChild(element_seg_scal_prop_i_s);
//                    element_scalar_prop_i->LinkEndChild(element_seg_scal_prop_i_v);
//                element_scalar_prop->LinkEndChild(element_scalar_prop_i);
//            }
//        element->LinkEndChild(element_scalar_prop);

//        // Save VectorProperties
//        TiXmlElement * element_vector_prop = new TiXmlElement( "VectorProperties" );
//            for(unsigned int i = 0; i<Traits::SF_VECTOR_PROPERTIES_CARDINAL; ++i)
//            {
//                // Build XML entry < getNameFromVectorProperty size>
//                //                      <s     _0="..." _1="..." ... _size-1="..." />
//                //                      <value _0_0="..." _0_1="..." _0_2="..." _1_0="..." ... _size-1_2="..." />
//                //                 </getNameFromVectorProperty>
//                TiXmlElement * element_vector_prop_i = new TiXmlElement( Traits::getNameFromVectorProperty( ESFVectorProperties(i) ) );
//                    const std::vector< PropConstraint1DT<Scalar,Vector> >& vec_prop = vector_properties_[i];
//                    element_vector_prop_i->SetAttribute("size",vec_prop.size());
//                    TiXmlElement * element_seg_vector_prop_i_s = new TiXmlElement( "s" );
//                    TiXmlElement * element_seg_vector_prop_i_v = new TiXmlElement( "value" );
//                    for(unsigned int k=0; k<vec_prop.size(); ++k)
//                    {
//                        // Build string index
//                        std::ostringstream s_k;
//                        s_k << "_" << k;
//                        element_seg_vector_prop_i_s->SetDoubleAttribute(s_k.str(),vec_prop[k].s);
//                        for(unsigned int t=0; t<Vector::size_; ++t) // for adaptability to any Vector dimension
//                        {
//                            std::ostringstream s_value;
//                            s_value << "_" << k << "_" << t;
//                            element_seg_vector_prop_i_v->SetDoubleAttribute(s_value.str(),vec_prop[k].value[t]);
//                        }
//                    }
//                    element_vector_prop_i->LinkEndChild(element_seg_vector_prop_i_s);
//                    element_vector_prop_i->LinkEndChild(element_seg_vector_prop_i_v);
//                element_vector_prop->LinkEndChild(element_vector_prop_i);
//            }
//        element->LinkEndChild(element_vector_prop);

#endif // CONVOL_PROPERTIES1D_T_H_
