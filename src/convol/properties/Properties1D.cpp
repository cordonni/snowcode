#include <convol/properties/Properties1D.h>

#include <core/xmlloaders/TraitsXmlQuery.h>

#include <cstdio>

#ifdef _MSC_VER
#ifndef snprintf
#define snprintf _snprintf
#endif
#endif

namespace  expressive {

namespace convol {

Properties1D::Properties1D()
{
    for(unsigned int i = 0; i<ExpressiveTraits::SF_SCALAR_PROPERTIES_CARDINAL; ++i){
        scalar_properties_[i].resize(1);
        scalar_properties_[i][0].s = 0.0;
        scalar_properties_[i][0].value = ExpressiveTraits::kSFScalarPropertiesDefault[i];
    }
    for(unsigned int i = 0; i<ExpressiveTraits::SF_VECTOR_PROPERTIES_CARDINAL; ++i){
        vector_properties_[i].resize(1);
        vector_properties_[i][0].s = 0.0;
        vector_properties_[i][0].value = ExpressiveTraits::kSFVectorPropertiesDefault[i];
    }
}

Properties1D::Properties1D(const Properties1D &rhs)
{
    std::copy(rhs.scalar_properties_, rhs.scalar_properties_ + ExpressiveTraits::SF_SCALAR_PROPERTIES_CARDINAL, scalar_properties_);
    std::copy(rhs.vector_properties_, rhs.vector_properties_ + ExpressiveTraits::SF_VECTOR_PROPERTIES_CARDINAL, vector_properties_);
}

Properties1D::~Properties1D()
{
}

void Properties1D::InitFromXml(const TiXmlElement *e)
{
    // if xml tag has a <Properties> tag instead of directly having <ScalarProperties> && <VectorProperties>, we recall that method with it.
    const TiXmlElement* element_prop_all = e->FirstChildElement(Name().c_str() );
    if (element_prop_all != nullptr) {
        return InitFromXml(element_prop_all);
    }

    const TiXmlElement* element_prop = e->FirstChildElement("ScalarProperties");
    if(element_prop != nullptr) {
        const TiXmlElement* element_i = element_prop->FirstChildElement();
        const TiXmlElement* element_i_s;
        const TiXmlElement* element_i_value;
        for(unsigned int i = 0; i<ExpressiveTraits::SF_SCALAR_PROPERTIES_CARDINAL; ++i)
        {
            // get property tags
            element_i_s = core::TraitsXmlQuery::QueryFirstChildElement("Properties1D::InitFromXml",
                                                                       element_i, "s");
            element_i_value = core::TraitsXmlQuery::QueryFirstChildElement("Properties1D::InitFromXml",
                                                                           element_i, "value");

            int size_elt_i = -1;
            core::TraitsXmlQuery::QueryIntAttribute("Properties1D::InitFromXml", element_i, "size",&size_elt_i);

            // constraints vector to be given to the segment
            std::vector< PropConstraint1DT<Scalar> > contraints(size_elt_i);
            // get values to put in the constraints vector
            for(int k=0; k<size_elt_i; ++k)
            {
                std::ostringstream s_k;
                s_k << "_" << k;
                core::TraitsXmlQuery::QueryScalarAttribute("Properties1D::InitFromXml",
                                                           element_i_s, s_k.str().c_str(), &(contraints[k].s));
                core::TraitsXmlQuery::QueryScalarAttribute("Properties1D::InitFromXml",
                                                           element_i_value, s_k.str().c_str(), &(contraints[k].value));
            }
            SetScalarPropertyConstraints(ExpressiveTraits::getScalarPropertyFromName(element_i->ValueStr()), contraints);

            // Get to the next XML element which souhld be the next property
            element_i = element_i->NextSiblingElement();
        }
    }

    element_prop = e->FirstChildElement("VectorProperties");
    if(element_prop != nullptr) {
        const TiXmlElement* element_i = element_prop->FirstChildElement();
        for(unsigned int i = 0; i<ExpressiveTraits::SF_VECTOR_PROPERTIES_CARDINAL; ++i)
        {
            // get property tags
            const TiXmlElement* element_i_s = core::TraitsXmlQuery::QueryFirstChildElement("Properties1D::InitFromXml",
                                                                       element_i, "s");
            const TiXmlElement* element_i_value = core::TraitsXmlQuery::QueryFirstChildElement("Properties1D::InitFromXml",
                                                                           element_i, "value");

            int size_elt_i = -1;
            core::TraitsXmlQuery::QueryIntAttribute("Properties1D::InitFromXml", element_i, "size",&size_elt_i);

            // constraints vector to be given to the segment
            std::vector< PropConstraint1DT<Vector> > contraints(size_elt_i);
            // get values to put in the constraints vector
            for(int k=0; k<size_elt_i; ++k)
            {
                std::ostringstream s_k;
                s_k << "_" << k;
                core::TraitsXmlQuery::QueryScalarAttribute("Properties1D::InitFromXml",
                                                           element_i_s, s_k.str().c_str(), &(contraints[k].s));

                Vector val;
                for(unsigned int t=0; t<Vector::RowsAtCompileTime; ++t) // for adaptability to any Vector dimension
                {
                    std::ostringstream val_k_t;
                    val_k_t << "_" << k << "_" << t;
                    core::TraitsXmlQuery::QueryScalarAttribute("Properties1D::InitFromXml",
                                                               element_i_value, val_k_t.str().c_str(), &(val[t]));
                }
                contraints[k].value = val;
            }
            SetVectorPropertyConstraints(ExpressiveTraits::getVectorPropertyFromName(element_i->ValueStr()), contraints);

            // Get to the next XML element which souhld be the next property
            element_i = element_i->NextSiblingElement();
        }
    }
}

TiXmlElement* Properties1D::ToXml(const char * name ) const
{
    TiXmlElement * element;
    if(name == NULL) {
        element = new TiXmlElement( this->Name() );
    } else {
        element = new TiXmlElement( name );
    }

    TiXmlElement * element_scalars = new TiXmlElement("ScalarProperties");
    for (unsigned int j = 0; j < ExpressiveTraits::SF_SCALAR_PROPERTIES_CARDINAL; ++j) {
        TiXmlElement *element_scalar = new TiXmlElement(ExpressiveTraits::getNameFromScalarProperty((ExpressiveTraits::ESFScalarProperties)j));
        element_scalar->SetAttribute("size", (int) scalar_properties_[j].size());
        element_scalars->LinkEndChild(element_scalar);

        TiXmlElement *element_s = new TiXmlElement("s");
        TiXmlElement *element_value = new TiXmlElement("value");
        element_scalar->LinkEndChild(element_s);
        element_scalar->LinkEndChild(element_value);

        for (unsigned int i = 0; i < scalar_properties_[j].size(); ++i) {
            char n[4]; // TODO:@todo : size of buffer should depend upon the number of constraint
            snprintf(n, 4, "_%d", i);
            element_s->SetDoubleAttribute(n, scalar_properties_[j][i].s);
            element_value->SetDoubleAttribute(n, scalar_properties_[j][i].value);
        }
    }

    TiXmlElement * element_vectors = new TiXmlElement("VectorProperties");
    for (unsigned int j = 0; j < ExpressiveTraits::SF_VECTOR_PROPERTIES_CARDINAL; ++j) {
        TiXmlElement *element_vector = new TiXmlElement(ExpressiveTraits::getNameFromVectorProperty((ExpressiveTraits::ESFVectorProperties)j));
        element_vector->SetAttribute("size", (int)vector_properties_[j].size());
        element_vectors->LinkEndChild(element_vector);

        TiXmlElement *element_s = new TiXmlElement("s");
        TiXmlElement *element_value = new TiXmlElement("value");
        element_vector->LinkEndChild(element_s);
        element_vector->LinkEndChild(element_value);

        for (unsigned int i = 0; i <  vector_properties_[j].size(); ++i) {
            for (unsigned int k = 0; k < Vector::RowsAtCompileTime; ++k) {
                char ns[4]; // TODO:@todo : size of buffer should depend upon the number of constraint (and size of Vector)
                char nv[6]; // cf up
                snprintf(ns, 4, "_%d", i);
                snprintf(nv, 6, "_%d_%d", i, k);
                element_s->SetDoubleAttribute(ns, vector_properties_[j][i].s);
                element_value->SetDoubleAttribute(nv, vector_properties_[j][i].value[k]);
            }
        }
    }

    element->LinkEndChild(element_scalars);
    element->LinkEndChild(element_vectors);
    return element;
}

} // Close namespace convol

} // Close namespace expressive
