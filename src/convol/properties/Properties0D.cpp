#include <convol/properties/Properties0D.h>

#include <core/xmlloaders/TraitsXmlQuery.h>

namespace  expressive {

namespace convol {

Properties0D::Properties0D()
{
    for(unsigned int i = 0; i<ExpressiveTraits::SF_SCALAR_PROPERTIES_CARDINAL; ++i){
        scalar_properties_[i]= ExpressiveTraits::kSFScalarPropertiesDefault[i];
    }
    for(unsigned int i = 0; i<ExpressiveTraits::SF_VECTOR_PROPERTIES_CARDINAL; ++i){
        vector_properties_[i] = ExpressiveTraits::kSFVectorPropertiesDefault[i];
    }
}

Properties0D::Properties0D(const Properties0D &rhs)
{
    std::copy(rhs.scalar_properties_, rhs.scalar_properties_+ExpressiveTraits::SF_SCALAR_PROPERTIES_CARDINAL, scalar_properties_);
    std::copy(rhs.vector_properties_, rhs.vector_properties_+ExpressiveTraits::SF_VECTOR_PROPERTIES_CARDINAL, vector_properties_);
}

Properties0D::~Properties0D()
{
}

void Properties0D::SetScalarProperty(ESFScalarProperties id, Scalar val)
{
    scalar_properties_[id] = val;
}

void Properties0D::SetVectorProperty(ESFVectorProperties id, const Vector& val)
{
    vector_properties_[id] = val;
}

void Properties0D::InitFromXml(const TiXmlElement *e)
{

    // if xml tag has a <Properties> tag instead of directly having <ScalarProperties> && <VectorProperties>, we recall that method with it.
    const TiXmlElement* element_prop_all = e->FirstChildElement(Name().c_str() );
    if (element_prop_all != nullptr) {
        return InitFromXml(element_prop_all);
    }

    const TiXmlElement* element_prop = e->FirstChildElement("ScalarProperties");
    if(element_prop != nullptr) {
        // Setup scalar and vector properties
        const TiXmlElement* element_i = element_prop->FirstChildElement();
        for(unsigned int i = 0; i<ExpressiveTraits::SF_SCALAR_PROPERTIES_CARDINAL; ++i)
        {
            double value = -1.0;
            core::TraitsXmlQuery::QueryDoubleAttribute("Properties0D::InitFromXml", element_i, "value", &value);

            SetScalarProperty(ExpressiveTraits::getScalarPropertyFromName(element_i->ValueStr()), value);

            // Get to the next XML element which souhld be the next property
            element_i = element_i->NextSiblingElement();
        }
    }

    element_prop = e->FirstChildElement("VectorProperties");
    if(element_prop != nullptr) {
        const TiXmlElement* element_i = element_prop->FirstChildElement();
        for(unsigned int i = 0; i<ExpressiveTraits::SF_VECTOR_PROPERTIES_CARDINAL; ++i)
        {
            Vector value;
            core::TraitsXmlQuery::QueryPointAttribute("Properties0D::InitFromXml",
                                                      element_prop, element_i->ValueStr().c_str(), value);

            SetVectorProperty(ExpressiveTraits::getVectorPropertyFromName(element_i->ValueStr()), value);

            // Get to the next XML element which souhld be the next property
            element_i = element_i->NextSiblingElement();
        }
    }
}

TiXmlElement* Properties0D::ToXml(const char * name) const
{
    TiXmlElement * element;
    if(name == NULL) {
        element = new TiXmlElement( this->Name() );
    } else {
        element = new TiXmlElement( name );
    }

    TiXmlElement * element_scalars = new TiXmlElement("ScalarProperties");
    for (unsigned int j = 0; j < ExpressiveTraits::SF_SCALAR_PROPERTIES_CARDINAL; ++j) {
        TiXmlElement *element_scalar = new TiXmlElement(ExpressiveTraits::getNameFromScalarProperty((ExpressiveTraits::ESFScalarProperties)j));
        element_scalars->LinkEndChild(element_scalar);

        element_scalar->SetDoubleAttribute("value", scalar_properties_[j]);
    }

    TiXmlElement * element_vectors = new TiXmlElement("VectorProperties");
    for (unsigned int j = 0; j < ExpressiveTraits::SF_VECTOR_PROPERTIES_CARDINAL; ++j) {
        TiXmlElement *element_vector = new TiXmlElement(ExpressiveTraits::getNameFromVectorProperty((ExpressiveTraits::ESFVectorProperties)j));
        element_vectors->LinkEndChild(element_vector);

        element_vector->SetAttribute("n", 3);
        element_vector->SetAttribute("x", (int)vector_properties_[j][0]);
        element_vector->SetAttribute("y", (int)vector_properties_[j][1]);
        element_vector->SetAttribute("z", (int)vector_properties_[j][2]);
    }

    element->LinkEndChild(element_scalars);
    element->LinkEndChild(element_vectors);

    return element;
}


} // Close namespace convol

} // Close namespace expressive
