/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: Properties0D.h

   Language: C++

   License: Expressive license

   \author: Maxime Quiblier, Cedric Zanni
   E-Mail:  maxime.quiblier@inrialpes.fr

   Description: .

   Platform Dependencies: None
*/


#pragma once
#ifndef CONVOL_PROPERTIES0D_T_H_
#define CONVOL_PROPERTIES0D_T_H_

// core dependencies
#include <core/CoreRequired.h>

#include <tinyxml/tinyxml.h>

#include <convol/ConvolRequired.h>

#include "core/ExpressiveTraits.h"

//////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : the way properties are handled should probably be changed
///  if the number of properties is not really dynamic then use an array instead of a vector otherwise : ok
/// TODO:@todo : should try to use move operator and constructor wherever possible
//////////////////////////////////////////////////////////////////////////////

namespace  expressive {

namespace convol {

/*!
 * \brief Class for a property constraint in 1 dimension.
          For a 1D field, the property is defined with respect to a Scalar (s).
 */
template<typename PropertyType>
struct PropConstraint0DT
{
    PropConstraint0DT()
    {}
    PropConstraint0DT(Scalar ss, const PropertyType& val) : s(ss), value(val)
    {}
    PropConstraint0DT(const PropConstraint0DT &rhs) : s(rhs.s), value(rhs.value)
    {}

    Scalar s;
    PropertyType value;
};

/*!
 * \brief Class for default handling of properties in 0D primitives (such as point, ...).
 */
class CONVOL_API Properties0D
{
public :
    EXPRESSIVE_MACRO_NAME("Properties0D")

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    Properties0D();

    Properties0D(const Properties0D &rhs);

    virtual ~Properties0D();

    /////////////////
    /// Modifiers ///
    /////////////////

    // Convenience function to set the value of an attribute.
    // do scalar_properties_[id] = contraints;
    virtual void SetScalarProperty(ESFScalarProperties id, Scalar val);

    //-------------------------------------------------------------------------
    virtual void SetVectorProperty(ESFVectorProperties id, const Vector& val);

//    //-------------------------------------------------------------------------
//    // Copy properties from another AbstractTriangleSField
//    void CopyProperties(const AbstractTriangleSFieldT& other)
//    {
//        for(unsigned int i = 0; i<Traits::SF_SCALAR_PROPERTIES_CARDINAL; ++i){
//            scalar_properties_[i] = other.scalar_properties()[i];
//        }
//        for(unsigned int i = 0; i<Traits::SF_VECTOR_PROPERTIES_CARDINAL; ++i){
//            vector_properties_[i] = other.vector_properties()[i];
//        }
//    }

//        // do scalar_properties_[id] = contraints;
//        void SetScalarPropertyConstraints(ESFScalarProperties id, const std::vector< PropConstraint1DT<Scalar> >& contraints)
//        {
//            assert(contraints.size() > 0);
//            assert(contraints[0].s == 0.0);
//            assert(contraints.size()==1 || contraints[contraints.size()-1].s == 1.0);
//            scalar_properties_[id] = contraints;
//        }

    //    //-------------------------------------------------------------------------
    //    // Convenience function to set the value of an attribute.
    //    // WARNING : this reset the number of constraint to 1 (ie 1 color for the whole segment)
    //    virtual void SetVectorProperty(ESFVectorProperties id, const Vector& val)
    //    {
    //        vector_properties_[id].resize(1);
    //        vector_properties_[id][0].s = 0;
    //        vector_properties_[id][0].value = val;
    //    }
        //-------------------------------------------------------------------------
//        // do vector_properties_[id] = contraints, so be careful the given constraints vector must be sorted by s
//        void SetVectorPropertyConstraints(ESFVectorProperties id, const std::vector< PropConstraint1DT<Vector> >& contraints)
//        {
//            assert(contraints.size() > 0);
//            assert(contraints[0].s == 0.0);
//            assert(contraints.size()==1 || contraints[contraints.size()-1].s == 1.0);
//            vector_properties_[id] = contraints;
//        }

    /////////////////
    /// Accessors ///
    /////////////////
    /// TODO:@todo : pourquoi ce n'est pas des references ?
    Scalar const* scalar_properties() const { return scalar_properties_; }
    Vector const* vector_properties() const { return vector_properties_; }

    //////////////////
    /// Evaluation ///
    //////////////////



    inline void InlineEvalProperties(const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                     const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                     std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                     std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                     ) const
    {
        unsigned int n_scal_prop = (unsigned int) scal_prop_ids.size();
        for(unsigned int i = 0; i<n_scal_prop; ++i)
        {
            scal_prop_res[i] = scalar_properties_[scal_prop_ids[i]];
        }
        unsigned int n_vect_prop = (unsigned int) vect_prop_ids.size();
        for(unsigned int i = 0; i<n_vect_prop; ++i)
        {
            vect_prop_res[i] = vector_properties_[vect_prop_ids[i]];
        }
    }

    ////////////////////
    /// Xml function ///
    ////////////////////

    void InitFromXml(const TiXmlElement *e);

    virtual TiXmlElement* ToXml(const char * name = nullptr) const;

protected:
    /////////////////
    /// Attributs ///
    /////////////////

    // Properties to attach to primitives according to traits
    Scalar scalar_properties_[ExpressiveTraits::SF_SCALAR_PROPERTIES_CARDINAL];
    Vector vector_properties_[ExpressiveTraits::SF_VECTOR_PROPERTIES_CARDINAL];

}; // End class Properties0D definition

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_PROPERTIES0D_T_H_
