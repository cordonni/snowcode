#include <convol/ConvolObject.h>

#include <core/scenegraph/SceneManager.h>

#include <ork/resource/ResourceTemplate.h>
#include <ork/resource/ResourceDescriptor.h>

#include <core/xmlloaders/TraitsXmlQuery.h>

#include <convol/xmlloaders/blobtreenode/BlobtreeRootLoader.h>
#include <convol/blobtreenode/NodeScalarField.h>
#include <convol/blobtreenode/NodeOperatorT.h>

using namespace ork;
using namespace expressive::core;
using namespace std;

namespace expressive {

namespace convol {


ConvolObject::ConvolObject()
    : skeleton_(nullptr), blobtree_root_(nullptr), implicit_surface_(nullptr)
{
    tri_mesh_ = std::make_shared<core::BasicTriMesh>(ork::TRIANGLES, ork::GPU_DYNAMIC);
}

ConvolObject::ConvolObject(std::shared_ptr<Skeleton> skel,
             std::shared_ptr<convol::BlobtreeRoot> bt,
             std::shared_ptr<convol::ImplicitSurface> is)
    : Serializable(),
      core::WeightedGraphListener(skel.get()),
//          core::ListenerT<BlobtreeRoot>(bt),
      core::ListenerT<ImplicitSurface>(is.get()),
      skeleton_(skel), blobtree_root_(bt), implicit_surface_(is)
{
    tri_mesh_ = std::make_shared<core::BasicTriMesh>(ork::TRIANGLES, ork::GPU_DYNAMIC);
}

ConvolObject::~ConvolObject()
{
    skeleton_ = nullptr;
    blobtree_root_ = nullptr;
    implicit_surface_ = nullptr;
    tri_mesh_ = nullptr;
}

void ConvolObject::swap(std::shared_ptr<ConvolObject> &) {
    assert(false && "You should redefine the swap method for this class");
}

void ConvolObject::updated(ImplicitSurface* /*t*/)
{
    if(auto_updating_polygonizer_ != nullptr)
       auto_updating_polygonizer_->ResetSurface();
}

//    void ConvolObject::updated(BlobtreeRoot* /*t*/)
//    {
//        if(auto_updating_polygonizer_ != nullptr)
//            auto_updating_polygonizer_->UpdateSurface();
//    }

void ConvolObject::updated(core::WorkerThread* /*t*/)
{
    NotifyUpdate();
}

void ConvolObject::set_area_updating_polygonizer(std::shared_ptr<tools::AreaUpdatingPolygonizerHandler> new_polygonizer)
{
    new_polygonizer->set_convol_object(this);
    auto_updating_polygonizer_ = new_polygonizer;
}

const std::shared_ptr<Skeleton> ConvolObject::skeleton() const
{
    return skeleton_;
}

std::shared_ptr<Skeleton> ConvolObject::nonconst_skeleton()
{
    return skeleton_;
}

const std::shared_ptr<BlobtreeRoot> ConvolObject::blobtree_root() const
{
    return blobtree_root_;
}

std::shared_ptr<BlobtreeRoot> ConvolObject::nonconst_blobtree_root()
{
    return blobtree_root_;
}

const std::shared_ptr<ImplicitSurface> ConvolObject::implicit_surface() const
{
    return implicit_surface_;
}

std::shared_ptr<ImplicitSurface> ConvolObject::nonconst_implicit_surface()
{
    return implicit_surface_;
}

const std::shared_ptr<tools::AreaUpdatingPolygonizerHandler> ConvolObject::auto_updating_polygonizer() const
{
    return auto_updating_polygonizer_;
}

std::shared_ptr<tools::AreaUpdatingPolygonizerHandler> ConvolObject::nonconst_auto_updating_polygonizer() const
{
    return auto_updating_polygonizer_;
}

std::shared_ptr<core::BasicTriMesh> ConvolObject::tri_mesh()
{
    return tri_mesh_;
}

std::mutex* ConvolObject::mutex_data_retrieval()
{
    return &mutex_data_retrieval_;
}

std::condition_variable* ConvolObject::cond_data_retrieval()
{
    return &cond_data_retrieval_;
}

void ConvolObject::Notify(const core::WeightedGraph::GraphChanges &/*changes*/)
{
    /*if (skeleton_ != nullptr) {
        for (auto v : changes.added_vertices_) {
            skeleton_->AddToBlobtree(dynamic_pointer_cast<NodeScalarField>(skeleton_->Get(v)->posptr()));
        }
        for (auto e : changes.added_edges_) {
            skeleton_->AddToBlobtree(dynamic_pointer_cast<NodeScalarField>(skeleton_->Get(e)->edgeptr()));
        }
    }
    */
    if(auto_updating_polygonizer_ != nullptr)
        auto_updating_polygonizer_->UpdateSurface();
}

TiXmlElement * ConvolObject::ToXml(const char *name) const
{
    TiXmlElement * element_base;
    if(name == NULL) {
        element_base = new TiXmlElement( this->Name() );
    } else {
        element_base = new TiXmlElement( name );
    }
    element_base->SetDoubleAttribute("iso_value", implicit_surface_->iso_value());
    element_base->SetDoubleAttribute("epsilon_bbox", implicit_surface_->epsilon_bbox());

    std::map<const void*, unsigned int> ids;
    TiXmlElement *element_skeleton = skeleton_->ToXml(ids);
    TiXmlElement *element_blobtree = blobtree_root_->ToXml(ids);

    element_base->LinkEndChild(element_skeleton);
    element_base->LinkEndChild(element_blobtree);

    return element_base;
}

/// @cond RESOURCES
    
class ConvolObjectResource : public ResourceTemplate<30, ConvolObject>
{
public:
    ConvolObjectResource(ork::ptr<ResourceManager> manager, const std::string &name,
                         ork::ptr<ResourceDescriptor> desc, const TiXmlElement *e = NULL)
       : ResourceTemplate<30, ConvolObject>(manager, name, desc)
    {
        e = e == NULL ? desc->descriptor : e;
        checkParameters(desc, e, "name,iso_value,epsilon_bbox,");

        // Load the skeleton

        const TiXmlElement* element_skeleton
            = TraitsXmlQuery::QueryFirstChildElement("ConvolObjectResource", e, "Skeleton");

        skeleton_ = std::dynamic_pointer_cast<Skeleton>(manager->loadResource(desc, element_skeleton));
        WeightedGraphListener::Init(skeleton_.get());
        // this fill a map in the blobtree_root_ containing association between id and created BlobtreeNode

        // Load the BlobtreeRoot

        blobtree_root_ = skeleton_->nonconst_blobtree_root();


        const TiXmlElement* element_bt_root
            = TraitsXmlQuery::QueryFirstChildElement("ConvolObjectResource", e, "BlobtreeRoot");
//            blobtree_root->BlobtreeAboutToBeModified(); //TODO:@todo : is that still useful ?
        //        blobtree_root_ = BlobtreeRootLoader::CreateFromXml(element_bt_root, blobtree_root_->map_id_node_);
        BlobtreeRootLoader::FillFromXml(blobtree_root_, element_bt_root, blobtree_root_->map_id_node_, skeleton_.get());
//            ListenerT<BlobtreeRoot>::Init(blobtree_root_);
//            blobtree_root->BlobtreeModified();

        // Create the implicit surface

        double iso_value = 1.0;
        TraitsXmlQuery::QueryDoubleAttribute("ConvolObjectResource", e, "iso_value", &iso_value);

        double epsilon_bbox = 0.0;
        TraitsXmlQuery::QueryDoubleAttribute("ConvolObjectResource", e, "epsilon_bbox", &epsilon_bbox);

        implicit_surface_ = std::make_shared<ImplicitSurface>(blobtree_root_, iso_value, epsilon_bbox);
        ListenerT<ImplicitSurface>::Init(implicit_surface_.get());

        //TODO:@todo : should unload the skeleton and implicit surface from the resource factory
        //(as well as all intermediate stuff), this should probably be handled by the function that call this method (loadFromFile)
    }
};

extern const char convolObject[] = "ConvolObject";

static ResourceFactory::Type<convolObject, ConvolObjectResource> ConvolObjectType;

/// @endcond
    
} // Close namespace convol

} // Close namespace expressive
