/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ImplicitSurfaceT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for Convol Implicit surface class.

   @todo Change convention about the surface being at 0.0.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_IMPLICIT_SURFACE_T_H_
#define CONVOL_IMPLICIT_SURFACE_T_H_

// core dependencies
#include <core/CoreRequired.h>
    // Geometry
    #include <core/geometry/AABBox.h>
    // tools
    #include <core/geometry/PointCloud.h>
    // utils
    #include <core/utils/ListenerT.h>

    // ScalarFields
    #include <convol/blobtreenode/BlobtreeRoot.h>


/////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo :
/////////////////////////////////////////////////////////////////////////////////////////


namespace expressive {

namespace convol {

/*! \brief Class to represent an implicit surface. Basically a scalar field and an iso value.
 *         See conventions in the detail decription below.
 *
 *  An implicit surface (or isosurface) is represented using a scalar field Function: Point -> Scalar
 *  and an iso value.
 *  Points on the surface are points for which : F(P) = iso_value
 *
 *  In the library Convol, the scalar field function associated with an implicit surface is always a \link Convol::BlobtreeRootT BlobtreeRootT \endlink (Which can embed a single \link Convol::NodeScalarFieldT NodeScalarFieldT \endlink ).
 *
 *  ***WARNING*** Conventions : 
 *  The scalar field is supposed to be such that F(p)-iso_value > 0.0 inside the surface,
 *  this is important for normal computation.
 *
 * \tparam TTraits The Traits type that must define : Scalar, Point, Vector, Normal, ESFScalarProperties, ESFVectorProperties
 */
class CONVOL_API ImplicitSurface
        : public ork::Object,  public core::ListenableT<ImplicitSurface>
{
public:

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////
    ImplicitSurface(std::shared_ptr<BlobtreeRoot> sf, Scalar iso_value = 1.0, Scalar epsilon_bbox = 0.001)
        : Object("ImplicitSurface"), blobtree_root_(sf), iso_value_(iso_value), epsilon_bbox_(epsilon_bbox) {}
    //TODO:@todo : as soon as all GetAxisBoundingBox in Convol work for 0.0, the 0.001 should be replace !
    //-------------------------------------------------------------------------
    virtual ~ImplicitSurface() {}

    ImplicitSurface *CloneForEval() const;

    /** \brief See \link Convol::ScalarFieldT::Eval ScalarFieldT::Eval \endlink
    */
    inline Scalar Eval(const Point& point) const { return blobtree_root_->Eval(point); }
    inline Scalar EvalMinusIsoValue(const Point& point) const { return blobtree_root_->Eval(point) - iso_value_; }
    inline Scalar EvalMinusIsoValue(Scalar x, Scalar y, Scalar z) const { return blobtree_root_->Eval(Point(x,y,z)) - iso_value_; }

    inline Vector EvalGrad(const Point& point, Scalar epsilon) const
    { 
        return blobtree_root_->EvalGrad(point,epsilon); 
    }
//    inline Scalar EvalScalarProperty(const Point& point, ESFScalarProperties id) const
//    {
//        return blobtree_root_->EvalScalarProperty(point, id);
//    }
    inline void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    { 
        blobtree_root_->EvalValueAndGrad(point,epsilon_grad, value_res, grad_res);
    }
    inline void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
    {
        blobtree_root_->EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, value_res, grad_res, scal_prop_res, vect_prop_res);
    }

    /** \brief Gives a point on the surface.
    *    \param epsilon If numerical approximation is needed, this gives how 
    *                   close to the surface the return point required to be 
    *                   (Geometricaly with respect to Vector)
    *   \return A point on the surface
    */
    inline Point GetAPointOnS(Scalar epsilon) const { return blobtree_root_->GetAPointAt(iso_value_, epsilon); }

    /** \brief  Give a list of points on the surface (at least 1 point for each \link Convol::NodeScalarFieldT NodeScalarFieldT \endlink in the blobtree.).
    *           Typically this will be used for seeding on all connex part of a surface.
    *
    *   \param epsilon The maximum distance to the surface required for each resulting point.
    *   \param res_points The returned \link Convol::PointCloudT \endlink filled with the computed points.
    */
    virtual void GetPrimitivesPoints(Scalar epsilon, core::PointCloud& res_points) const
    {
        blobtree_root_->GetPrimitivesPointsAt(iso_value_, epsilon, res_points,
                                              blobtree_root_->axis_bounding_box());
    }

    /** \brief  Give a list of points included on the surface and included in the bounding box
    *           (1 or 0 point for each \link Convol::NodeScalarFieldT NodeScalarFieldT \endlink in the blobtree.).
    *           Typically this will be used for seeding on all connex part of a surface.
    *
    *   \param bbox bounding box in which we are looking for point.
    *   \param epsilon The maximum distance to the surface required for each resulting point.
    *   \param res_points The returned \link Convol::PointCloudT \endlink filled with the computed points.
    */
    virtual void GetPrimitivesPointsInBoundingBox(const core::AABBox& b_box, Scalar epsilon, core::PointCloud& res_points) const
    {
        blobtree_root_->GetPrimitivesPointsAt(iso_value_, epsilon, res_points, b_box);
    }

    ///////////////
    // Modifiers //
    ///////////////
    
    inline void set_scalar_field(std::shared_ptr<BlobtreeRoot> scalar_field)
    {
        blobtree_root_ = scalar_field;
        NotifyUpdate();
    }
    inline void set_iso_value(Scalar iso_val)
    {
        iso_value_ = iso_val;
        NotifyUpdate();
    }

    inline void set_epsilon_bbox(Scalar eps_bbox) { epsilon_bbox_ = eps_bbox; }

    // Call PrepareForEval on the blobtree root
    void PrepareForEval()
    {
        blobtree_root_->PrepareForEval(epsilon_bbox_, 0.01);
    }

    core::AABBox GetAxisBoundingBox() const
    {
//        return blobtree_root_->GetAxisBoundingBox(epsilon_bbox_);
        return blobtree_root_->axis_bounding_box(); //TODO:@todo : prepare for eval should be called before doing any query on the Blobtree (either eval or bounding box)
    }

    ///////////////
    // Accessors //
    ///////////////

    inline const std::shared_ptr<BlobtreeRoot> blobtree_root() const { return blobtree_root_; }
    inline std::shared_ptr<BlobtreeRoot> nonconst_blobtree_root() { return blobtree_root_; }
    inline Scalar iso_value() const { return iso_value_; }

    inline const Scalar& epsilon_bbox() const { return epsilon_bbox_; }
    //TODO:@todo : why a const Scalar& instead of just a scalar ?

protected:
    // Deep copy constructor :
    // It should only be called by the protected subclass copy constructors
    // which should only be called by the "CloneForEval" function
    ImplicitSurface(const ImplicitSurface &rhs) :
        Object("ImplicitSurface"),
        ListenableT<ImplicitSurface>(rhs),
        blobtree_root_(std::dynamic_pointer_cast<BlobtreeRoot>(rhs.blobtree_root_->CloneForEval())), iso_value_(rhs.iso_value_),
        epsilon_bbox_(rhs.epsilon_bbox_){}

    std::shared_ptr<BlobtreeRoot> blobtree_root_;
private:
    Scalar iso_value_;

    // Blobtree evaluation parameters
    Scalar epsilon_bbox_;

}; // End class ImplicitSurfaceT declaration

class ClonedImplicitSurface : public ImplicitSurface
{
public:
    ClonedImplicitSurface(const ImplicitSurface &rhs) : ImplicitSurface(rhs){}
    ~ClonedImplicitSurface() { }
};

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_IMPLICIT_SURFACE_T_H_
