/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: BlobtreeNode.h
 
 Language: C++
 
 License: Convol license
 
 \author: Cedric Zanni
 E-Mail:  cedric.zanni@inrialpes.fr
 
 Description: Header for the blobtree node super-class.

 
 Platform Dependencies: None
 */

#pragma once
#ifndef CONVOL_BLOBTREE_NODE_H_
#define CONVOL_BLOBTREE_NODE_H_

// core dependencies
#include <core/CoreRequired.h>
    // utils
    #include <core/utils/Serializable.h>
    // geometry
    #include<core/geometry/AABBox.h>
    #include<core/geometry/PointCloud.h>
    #include<core/geometry/GeometryPrimitive.h>

// convol dependencies
    // ScalarFields
    #include<convol/ScalarField.h>
        // BlobtreeNode
        #include<convol/blobtreenode/listeners/BlobtreeNodeListener.h>
//    // Geometry
//    #include<Convol/include/geometry/PrecisionAxisBoundingBoxT.h>

//// std dependencies
//#include<map>
//#include<list>
#include<vector>
#include<set>
//#include<sstream>
//#include<string>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib) (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/xml/tinyxml/tinyxml.h>

// forward declaration
namespace expressive {
namespace convol {
    class BlobtreeRoot;
    class BlobtreeBasket;
}
}

/*!
 * \brief Super-class for node in the blobtree.
 *
 * Attributes :
 * - arity_ : number of son the node has/can have. -1 is N, then anyinteger is OK.
 * - parent_ : any node has/can have a parent in the tree.
 * - children_ : vector of actual children
 * - axis_bounding_box_ : bounding box containing space (as small as possible) where the Eval function will return a value lower than epsilon_bbox_
 * - epsilon_bbox_ : value for which the bounding box has been built. Set to -1.0 if the bounding box is not set.
 *
 */

namespace expressive {
namespace convol {

class CONVOL_API BlobtreeNode :
    public ScalarField, public expressive::core::Serializable, public std::enable_shared_from_this<BlobtreeNode>
{
public:
    friend class BlobtreeRoot;
    friend class BlobtreeBasket;

    // Construct



    /** \brief 
     *  
     *  The bounding box is initialized as empty (Traits::kScalarMax, Traits::kScalarMin)
     *  the reason for this init is that before having called PrepareForEval, there is nothing to be computed for this node. 
     *  However, all blobtree systems are on, and therefore will monitor changes in the bounding box in order to know which parts has been updated.
     */
    BlobtreeNode(const std::string& name)
        : Serializable(name.c_str()),
          arity_(0),
          parent_(nullptr),
          axis_bounding_box_(Vector::Constant(expressive::kScalarMax), Vector::Constant(expressive::kScalarMin)),
          epsilon_bbox_(-1.0)
    { }

    /** \brief 
     *  
     *  @todo see what we can do for this function. Be careful with the virtual.
     */
    virtual ~BlobtreeNode()
    {
        InvalidateBoundingBox();

        BlobtreeNodeAboutToBeDeleted();

        if(parent_ != NULL)
        {
            BlobtreeNode* parent = parent_;
            parent->RemoveChild(this);
        }
    }

    //TODO:@todo : we should think carefully on the way we manage cloning for evaluation and cloning for dedoubling an object
    virtual std::shared_ptr<ScalarField> CloneForEval() const = 0;
                                                                                                    
//    virtual std::string Name() const = 0;
    //TODO:@todo : this should probably be a static function ... ?

//    virtual std::string ParameterName() const
//    {
//        std::ostringstream name;
//        if(this->arity_ == -1)
//            name << "[arity(N)]";
//        else
//            name << "[arity(" << this->arity_ << ")]";
//        return name.str();
//    }

//    virtual std::string UserFriendlyName() const
//    {
//        return (this->label_.empty() ? Base::UserFriendlyName() : this->label_);
//    }

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual TiXmlElement * ToXml(const char *name = nullptr) const
    {
        TiXmlElement * element_base;
        if(name == NULL) {
            element_base = new TiXmlElement( this->Name() );
        } else {
            element_base = new TiXmlElement( name );
        }

        AddXmlAttributes(element_base);

        return element_base;
    }

    virtual TiXmlElement * ToXml(std::map<const void*, unsigned int> &ids, const char *name = nullptr) const
    {
        TiXmlElement * element_base;
        if(name == NULL) {
            element_base = new TiXmlElement( this->Name() );
        } else {
            element_base = new TiXmlElement( name );
        }

        AddXmlAttributes(element_base, ids);

        return element_base;
    }

    virtual void AddXmlAttributes(TiXmlElement *element) const
    {
        if (!label_.empty()) {
            element->SetAttribute("label",label_);
        }
        element->SetAttribute("arity",arity_);
        AddXmlChildren(element);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        if (!label_.empty()) {
            element->SetAttribute("label",label_);
        }
        element->SetAttribute("arity",arity_);
        AddXmlChildren(element, ids);
    }

    virtual void AddXmlChildren(TiXmlElement * element) const
    {
        TiXmlElement * element_children = new TiXmlElement( "children" );
        element_children->SetAttribute("number", (int) children_.size());
        element->LinkEndChild(element_children);

        for (unsigned int i = 0; i < children_.size(); ++i)
        {
            element_children->LinkEndChild(children_[i]->ToXml());
        }
    }

    virtual void AddXmlChildren(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        TiXmlElement * element_children = new TiXmlElement( "children" );
        element_children->SetAttribute("number", (int) children_.size());
        element->LinkEndChild(element_children);

        for (unsigned int i = 0; i < children_.size(); ++i)
        {

            core::Primitive *gp = dynamic_cast<core::Primitive*>(children_[i].get());
            auto mid = ids.find(gp); // looking for skeleton elements... // TODO : UGLY !!!
            if (mid != ids.end()) {
                TiXmlElement *child = new TiXmlElement("NodeScalarField"); // we assume here that if node is already registered somewhere, it IS a nodescalarfield.
                child->SetAttribute("id", mid->second);
                element_children->LinkEndChild(child);
            } else {
                element_children->LinkEndChild(children_[i]->ToXml(ids));
            }
        }
    }

    virtual void InitFromXml(const TiXmlElement * /*element*/) {}

    //////////////////////////
    // Evaluation functions //
    //////////////////////////

    /** \brief Evaluate the scalar field in space at point point
     *   \param point Point in space where we want the field value
     *   \return Field value at point
     */
    virtual Scalar Eval(const Point& point) const =0;

//    // For now all have the same method, proportionnaly to the field value
//    // (Except NodeScalarField which will have to provide something else
//    virtual void EvalProperties(const Point& point,
//                                    const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
//                                    const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
//                                    std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
//                                    std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
//                                    ) const
//    {
//        if(children_.size() != 0)
//        {
//            Scalar ratio = children_[0]->Eval(point);

//            std::vector<Scalar> scal_prop_child;
//            // Init properties res
//            children_[0]->EvalProperties(point, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
//            for(unsigned int k=0; k<scal_prop_ids.size(); ++k)
//            {
//                scal_prop_res[k] *= ratio;
//            }
//            for(unsigned int k=0; k<vect_prop_ids.size(); ++k)
//            {
//                vect_prop_res[k] *= ratio;
//            }

//            // Auxilliary
//            scal_prop_child.resize(scal_prop_ids.size());
//            std::vector<Vector> vect_prop_child;
//            vect_prop_child.resize(vect_prop_ids.size());
//            for(unsigned int i = 1; i<children_.size(); ++i)
//            {
//                Scalar val = children_[i]->Eval(point);
//                ratio += val;
//                children_[i]->EvalProperties(point, scal_prop_ids, vect_prop_ids, scal_prop_child, vect_prop_child);
//                for(unsigned int k=0; k<scal_prop_ids.size(); ++k)
//                {
//                    scal_prop_res[k] += val*scal_prop_child[k];
//                }
//                for(unsigned int k=0; k<vect_prop_ids.size(); ++k)
//                {
//                    vect_prop_res[k] += val*vect_prop_child[k];
//                }
//            }
//            if(ratio == 0.0)
//            {
//                for(unsigned int k = 0; k<scal_prop_ids.size(); ++k)
//                {
//                    scal_prop_res[k] = Traits::kSFScalarPropertiesDefault[scal_prop_ids[k]];
//                }
//                for(unsigned int k = 0; k<vect_prop_ids.size(); ++k)
//                {
//                    vect_prop_res[k] = Traits::kSFVectorPropertiesDefault[vect_prop_ids[k]];
//                }
//            }
//            else
//            {
//                for(unsigned int k=0; k<scal_prop_ids.size(); ++k)
//                {
//                    scal_prop_res[k] = scal_prop_res[k]/ratio;
//                }
//                for(unsigned int k=0; k<vect_prop_ids.size(); ++k)
//                {
//                    vect_prop_res[k] = vect_prop_res[k]/ratio;
//                }
//            }
//        }
//        else
//        {
//            for(unsigned int k=0; k<scal_prop_ids.size(); ++k)
//            {
//                scal_prop_res[k] = Traits::kSFScalarPropertiesDefault[scal_prop_ids[k]];
//            }
//            for(unsigned int k=0; k<vect_prop_ids.size(); ++k)
//            {
//                vect_prop_res[k] = Traits::kSFVectorPropertiesDefault[vect_prop_ids[k]];
//            }
//        }
//    }

    /** \brief Prepare the tree for evaluation.
     *         Especially build the bounding boxes.
     *         Since epsilon_bbox can also be used to determine modified regions, the function returns a bounding box list 
     *         containing all modified regions among the node children.
     *         Each node is responsible to add its previous and new bounding boxes to the list of updated areas.
     *         Since deletion are supposed to be handled by the root, deleted nodes have already notify the root and their bounding boxes before deletion have been stored
     *         in the root.
     *
     *   \param epsilon_bbox the epsilon parameter for bounding box computation. If == 0.0, all bounding boxes are initialized to infinitely large.
     *   \param accuracy_needed This gives an idea of the precision we want when evaluating. For instance, if we have a pre-computed grid in some node,
     *          this will determine the grid resolution.
     *   \param updated_areas Return in this parameter the areas that has been updated. 2 areas can be stored for each node. 
     *                        The beahaviour supposed is the following : if the node is not yet present in the map, add it with a pari corresponding to (New boucning box, old bounding box).
     *                                                                   If it is present, the old bounding box is kept and only the current boudning box is stored in the first element of the pair.
     *
     */
    virtual void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                                std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& updated_areas) = 0;
    
    /** \brief  Give a list of points on the surface for each primitive separately (Should be at least 1 by primitive) epsilon close 
    *           to the ScalarField iso value.
    *           Typically this will be used as for seeding.
    */
    virtual void GetPrimitivesPointsAt(Scalar value, Scalar epsilon, core::PointCloud& res_points, const core::AABBox& b_box) const;

    /////////////////////////////
    // BoundingBox computation //
    /////////////////////////////

    virtual core::AABBox GetAxisBoundingBox(Scalar value) const = 0;

    virtual void GetPrecisionAxisBoundingBoxes(Scalar value, std::list<core::PrecisionAxisBoundingBox> &pboxes) const
    {
        for(auto &child : children_) {
            child->GetPrecisionAxisBoundingBoxes(value, pboxes);
        }
//        for (typename std::vector<BlobtreeNode*>::const_iterator it=children_.begin();
//             it != children_.end(); ++it)
//        {
//            (*it)->GetPrecisionAxisBoundingBoxes(value, pboxes);
//        }
    }

    // Invalidate Bounding Box, ie put epsilon_bbox_ at -1.0 
    // Also recursively invalidate bounding boxes of parents.
    void InvalidateBoundingBox();

    /** \brief Invalidate the BlobtreeNode and all this children.
     *         eg. set all epsilon_bbox_ to -1
     */
    void InvalidateSubtreeBoundingBox();

    inline bool IsCompactAndValid() const
    {
        return epsilon_bbox_ == 0.;
    }

    // Should be called inside internal node at the end of the PrepareForEval
    // method (after the children update)
    void UpdateNodeCompacity();

    inline bool IsBoundingBoxInvalidated()
    {
        return epsilon_bbox_ == -1.;
    }

    // Notify the parent that the given area has been modified following a node deletion.
    // Remember that Other modifications area are kept in nodes and given to root when calling PrepareForEval.
    // This is especially used in BlobtreeRoot function that delete nodes in order to record changes.
    // most node will just forward the area to their parent. However, transformation nodes may want to transform it.
    virtual void NotifyParentOfAModifiedAreaAfterDeletion(const core::AABBox& modified_area)
    {
        if(parent_ != NULL)
        {
            parent_->NotifyParentOfAModifiedAreaAfterDeletion(modified_area);
        }
    }
   
    ///////////////
    // Modifiers //
    ///////////////

//    virtual void SetScalarProperty(ESFScalarProperties id, Scalar val)
//    {
//        for(typename std::vector<BlobtreeNode*>::iterator it = children_.begin(); it != children_.end(); ++it)
//        {
//            (*it)->SetScalarProperty(id, val);
//        }
//    }
    
    // WARNING : Be careful when using any direct manipulation of a node. 
    //           See BlobtreeRoot documentation for more details.
    // Recursively delete childs if the object has some
    void DeleteChildrenBlobtreeNode();

    // WARNING : Be careful when using any direct manipulation of a node. 
    //           See BlobtreeRoot documentation for more details.
    inline void set_parent(BlobtreeNode* bt_node)
    {
        parent_ = bt_node;
    }

    // WARNING : Be careful when using any direct manipulation of a node. 
    //           See BlobtreeRoot documentation for more details.
    // WARNING : Do not use this function for N-ary unless you REALLY know what you are doing.
    virtual void set_child(unsigned int i, std::shared_ptr<BlobtreeNode> bt_node);

    // WARNING : Be careful when using any direct manipulation of a node. 
    //           See BlobtreeRoot documentation for more details.
    // WARNING : Do not use this function for N-ary unless you REALLY REALLY know what you are doing.
    virtual void AddChild(std::shared_ptr<BlobtreeNode> bt_node);
    
    /** \brief Replace the given old_child with the given new_child.
     *         old_child parent is set to NULL.
     *         new_child parent is set to this.
     *
     *  WARNING : the old_child is not deleted.
     */
    void ReplaceChild(BlobtreeNode* old_child, std::shared_ptr<BlobtreeNode> new_child);

    /** \brief Remove the given child from the children vector.
     *         This function does not destroy the child.
     *   \param child child to be removed.
     *
     *  The default behaviour is the following (but can be reimplemented 
     *  for a specific node) :
     *   If the node has an arity equal to -1 (ie no specific arity),
     *   The reference to child is erased from the list and all the children's
     *   vector is re-organized by shifting :
     *               1     2     3     4
     *   children_ : a   child   b     c       BEFORE
     *               1     2     3      
     *   children_ : a     b     c             AFTER
     *
     *   Otherwise, the node has a specific arity, the reference
     *   to child in the children vector is replaced by NULL :
     *               1     2     3     4
     *   children_ : a   child   b     c       BEFORE
     *               1     2     3     4  
     *   children_ : a   NULL    b     c       AFTER
     *
     */
    void RemoveChild(BlobtreeNode *child);
    void RemoveChild(std::shared_ptr<BlobtreeNode> child);

    /** \brief Same as above but remove the ith child.
     *   \param i Must be such that i < children_.size()
     *
     */
    void RemoveChild(unsigned int i);

    /** \brief Check if the node is usable and in a correct state.
     *         That means there is no structure like one of the following :
     *         - A NULL child
     *         In one of those cases, destroy the node (this) and propagate the call for correction to the parent node.
     *         NB : in case of a binary node, the algorithm first check if one of the child is OK, if yes,
     *              this node is brought up at this' place.
     *              WARNING : "this" is deleted. The object is auto destroying.
     */
    virtual void RecursiveNodeCorrection();


    ///////////////
    // Accessors //
    ///////////////

    // WARNING : This one is used ONLY for improved efficiency.
    //           Don't use or use with EXTREME caution.
    inline BlobtreeNode* nonconst_parent() const
    {
        return parent_;
    }
    inline BlobtreeNode const* parent() const
    {
        return parent_;
    }
    inline BlobtreeNode* nonconst_ancestor() const
    {
        if (parent_ == nullptr) {
            return (BlobtreeNode*)this;
        }
        return parent_->nonconst_ancestor();
    }
    inline BlobtreeNode const* ancestor() const
    {
        if (parent_ == nullptr) {
            return (BlobtreeNode*)this;
        }
        return parent_->ancestor();
    }

    BlobtreeRoot* nonconst_root() const;

    BlobtreeRoot const* root() const;
    
    inline const std::shared_ptr<BlobtreeNode> child(unsigned int i) const
    {
        assert(i<children_.size());
        return children_[i];
    }

    inline bool HasChild(std::shared_ptr<BlobtreeNode> child) const
    {
        return std::find(children_.begin(), children_.end(), child) != children_.end();
    }

    // WARNING : This one is used ONLY for improved efficiency.
    //           Don't use or use with EXTREME caution.
    inline std::vector<std::shared_ptr<BlobtreeNode> >& nonconst_children()
    {
        return children_;
    }
    inline const std::vector<std::shared_ptr<BlobtreeNode> >& children() const
    {
        return children_;
    }

    inline int arity() const
    {
        return arity_;
    }
    
    inline unsigned int nb_children() const
    {
        return (unsigned int) children_.size();
    }

    ///////////////////////////////////////
    // Evaluation attributes : Modifiers //
    ///////////////////////////////////////

    void set_label(const std::string &new_label)
    {
        label_ = new_label;
    }

    // WARNING : use those 2 function with extreme caution. Their misuse can break consistency between axis_bounding_box_ and epsilon_bbox_
    // This allows you to manipulate the bounding box from "outside". This is really raw (no listener call to tell that the bounding box has been modified for instance).
    void set_axis_bounding_box(const core::AABBox& axis_bbox) {  axis_bounding_box_ = axis_bbox; BlobtreeNodeABBoxUpdated();}
    void set_epsilon_bbox(Scalar epsilon_bbox) { epsilon_bbox_ = epsilon_bbox; }

    ///////////////////////////////////////
    // Evaluation attributes : Accessors //
    ///////////////////////////////////////

    const core::AABBox& axis_bounding_box() const { return axis_bounding_box_; }
    Scalar epsilon_bbox() const { return epsilon_bbox_; }

    /////////////////////////
    // Listener Management //
    /////////////////////////

    void AddListener(BlobtreeNodeListener* listener)    
    { 
        listener_set_.insert(listener); 
    }
    void RemoveListener(BlobtreeNodeListener* listener) { listener_set_.erase(listener);  }

    virtual void swap(std::shared_ptr<BlobtreeNode> &) {
        assert(false && "You should redefine the swap method for this class");
    }

protected:
    // Deep copy constructor :
    // It should only be called by the protected subclass copy constructors
    // which should only be called by the "CloneForEval" function
    // listener_set is not copyed
    // TODO : What should we do if a parent exists already ? Maybe we could add a specific method in clone() to handle that ?
    BlobtreeNode(const BlobtreeNode &rhs, bool copyParent = false);

protected:
    // Used to give a label to the node, pure convenience for the final user.
    std::string label_;
protected:
    int arity_;    //the arity is the number of children of the current node (-1 is a n-ary node)

    BlobtreeNode* parent_;
    std::vector<std::shared_ptr<BlobtreeNode> > children_;

    ///////////////////////////
    // Evaluation attributes //
    ///////////////////////////

    core::AABBox axis_bounding_box_;
    Scalar epsilon_bbox_;   // == -1 if the bounding box is invalid

    // Listeners
    std::set<BlobtreeNodeListener*> listener_set_;

    ///////////////////////////////////////////////////////////////////////////
    // Listener calls //
    ////////////////////
    void BlobtreeNodeABBoxUpdated()
    {
        for( std::set<BlobtreeNodeListener*>::iterator it = listener_set_.begin(); it != listener_set_.end(); ++it)
        {
            (*it)->BlobtreeNodeABBoxUpdated(this);
        }
    }
    // Give listener the information about the incoming deletion.
    // Also 
    void BlobtreeNodeAboutToBeDeleted()
    {
        for( std::set<BlobtreeNodeListener*>::iterator it = listener_set_.begin(); it != listener_set_.end(); ++it)
        {
            (*it)->BlobtreeNodeAboutToBeDeleted(this);
        }
    }
    ///////////////////////////
    // END OF listener calls //
    ///////////////////////////////////////////////////////////////////////////

};
    
} // Close namespace convol

} // Close namespace expressive

    
#endif // CONVOL_BLOBTREE_NODE_H_
