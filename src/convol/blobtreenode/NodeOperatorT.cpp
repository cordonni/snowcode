#include <convol/blobtreenode/NodeOperatorT.h>

// convol dependencies
    // tools
    #include<convol/tools/ConvergenceTools.h>

#include <convol/factories/BlobtreeNodeFactory.h>

// List of all functor operator that can be used with a NodeOperator
// (this is required to register them in the BlobtreeNodeFacotry)
#include <convol/functors/operators/BlendSum.h>
#include <convol/functors/operators/BlendMax.h>
#include <convol/functors/operators/BlendRicci.h>
#include <convol/functors/operators/DistanceIntegralSurface.h>

namespace expressive {

namespace convol {

unsigned int NodeOperator::node_operator_current_id_ = 0;

//TODO:@todo : a remplacer (sans doute pas optimal & pas valable pour tout les mélanges !! (notament soustraction))
Point NodeOperator::GetAPointAt(Scalar value, Scalar epsilon) const
{
    Point res = Point::Zero();
    std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = this->children_.begin();
    while(it != this->children_.end() && res[0]==0.0 && res[1]==0.0 && res[2]==0.0)
    {
        Point init_point = (*it)->GetAPointAt(value, epsilon);
        Vector search_dir = this->EvalGrad(init_point, 0.0001); // arbitrary
        search_dir.normalize();

        // Accuracy for gradient computation
        // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
        Scalar grad_epsilon = 0.0001;   // arbitrary
        if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }
        Scalar r_min = -30.0, r_max = 30.0;
        Convergence::SafeNewton1D(
                    *(static_cast<const BlobtreeNode*>(this)),
                    init_point,
                    search_dir,
                    r_min,  //TODO : arbitrary                                // security minimum bound
                    r_max,    //TODO : arbitrary
                    0.0,            // point at which we start the search, given in 1D
                    value,
                    epsilon,
                    grad_epsilon,
                    20,
                    res);
        ++it;
    }

    return res;
}

// Register the name of the functor to be looked for in xml files

static BlobtreeNodeFactory::XmlFunctorField<AbstractNodeOperatorNArity> NodeOperatorNArity_XmlFunctorField;

// Register NodeOperatorNArity<FunctorOperator> in the BlobtreeNodeFactory

static BlobtreeNodeFactory::Type<  BlendSum, NodeOperatorNArityT<BlendSum> >
                                                    NodeOperatorNArityT_BlendSum_Type;
static BlobtreeNodeFactory::Type<  BlendMax, NodeOperatorNArityT<BlendMax> >
                                                    NodeOperatorNArityT_BlendMax_Type;
static BlobtreeNodeFactory::Type<  BlendRicci, NodeOperatorNArityT<BlendRicci> >
                                                    NodeOperatorNArityT_BlendRicci_Type;

static BlobtreeNodeFactory::Type<  DistanceIntegralSurface, NodeOperatorNArityT<DistanceIntegralSurface> >
                                                    NodeOperatorNArityT_DistanceIntegralSurface_Type;

} // Close namespace convol

} // Close namespace expressive

