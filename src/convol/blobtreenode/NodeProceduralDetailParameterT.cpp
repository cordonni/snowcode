#include <convol/blobtreenode/NodeProceduralDetailParameterT.h>

#include <convol/factories/BlobtreeNodeFactory.h>

// List of all functor operator that can be used with a NodeProceduralDetailParameter
#include <convol/functors/noises/ControllableGaborSurfaceNoise.h>

namespace  expressive {

namespace convol {

// Register the name of the functor to be looked for in xml files

static BlobtreeNodeFactory::XmlFunctorField<AbstractNodeProceduralDetailParameter> NodeProceduralDetailParameter_XmlFunctorField;

// Register NodeProceduralDetail<FunctorNosie> in the BlobtreeNodeFactory

static BlobtreeNodeFactory::Type<  ControllableGaborSurfaceNoiseParameters, NodeProceduralDetailParameterT<ControllableGaborSurfaceNoiseParameters> >
                                                    NodeProceduralDetailParameterT_ControllableGaborSurfaceNoiseParameter_Type;

} // Close namespace convol

} // Close namespace expressive
