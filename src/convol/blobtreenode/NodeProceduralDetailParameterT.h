/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: NodeProceduralDetailParameterT.h

 Language: C++

 License: Convol license

 \author: Cédric Zanni
 E-Mail:  cedric.zanni@inria.fr

 Description: Node operator with blending by summation with additional properties for GaborNoise

TODO:@todo : I want to change the system of noise properties (and all properties in general ...),
this should make this class useless.

    Platform Dependencies: None
 */

#pragma once
#ifndef NODE_PROCEDURAL_DETAIL_PARAMETER_H_
#define NODE_PROCEDURAL_DETAIL_PARAMETER_H_

#include <core/CoreRequired.h>

#include <convol/ConvolRequired.h>

#include <convol/functors/noises/PolynomialDeformationMap.h>
#include <convol/blobtreenode/NodeOperatorT.h>

#include<convol/functors/operators/BlendSum.h>

namespace expressive {

namespace convol {

class AbstractNodeProceduralDetailParameter :
        public NodeOperator
{
public:

    EXPRESSIVE_MACRO_NAME("NodeProceduralDetailParameter")

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractNodeProceduralDetailParameter(Scalar tilt_angle, Scalar rotate_angle)
        : tilt_angle_(tilt_angle), rotate_angle_(rotate_angle)
    {
        this->parent_ = nullptr;
        this->arity_ = -1;
    }

protected:
    AbstractNodeProceduralDetailParameter(const AbstractNodeProceduralDetailParameter &rhs) :
        NodeOperator(rhs), tilt_angle_(rhs.tilt_angle_), rotate_angle_(rhs.rotate_angle_), blend_operator_(rhs.blend_operator_){}
public:

    /////////////////////
    /// Xml functions ///
    /////////////////////

    static std::string XmlFunctorField() { return std::string("TNoiseParameter"); }

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        NodeOperator::AddXmlAttributes(element);

        element->SetDoubleAttribute("tilt_angle",tilt_angle_);
        element->SetDoubleAttribute("rotate_angle",rotate_angle_);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        NodeOperator::AddXmlAttributes(element,ids);

        element->SetDoubleAttribute("tilt_angle",tilt_angle_);
        element->SetDoubleAttribute("rotate_angle",rotate_angle_);
    }

    virtual void InitFromXml(const TiXmlElement *element) = 0;

    /////////////////
    /// Accessors ///
    /////////////////

    inline Scalar tilt_angle() const { return tilt_angle_; }
    inline Scalar rotate_angle() const { return rotate_angle_; }

    virtual Scalar interior_dist_limit() const = 0;
    virtual Scalar exterior_dist_limit() const = 0;

    virtual const core::Functor& noise_parameter() const = 0;
    virtual const PolynomialDeformationMap& deformation_map_functor() const  = 0;

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_tilt_angle(Scalar tilt_angle){ tilt_angle_ = tilt_angle; }
    void set_rotate_angle(Scalar rotate_angle){ rotate_angle_ = rotate_angle; }

    /////////////////
    /// Attributs ///
    /////////////////

protected:

    // Direction of migration
    Scalar tilt_angle_;
    Scalar rotate_angle_;

    BlendSum blend_operator_;
};




template<typename TNoiseParameter>//, typename TFunctorDeformationMap>
class NodeProceduralDetailParameterT :
        public AbstractNodeProceduralDetailParameter
{
public:
    typedef PolynomialDeformationMap TFunctorDeformationMap;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    NodeProceduralDetailParameterT(Scalar tilt_angle, Scalar rotate_angle,
                                   const TNoiseParameter& noise_parameter,
                                   const TFunctorDeformationMap& deformation_map_functor_)
        : AbstractNodeProceduralDetailParameter(tilt_angle, rotate_angle),
          noise_parameter_(noise_parameter), deformation_map_functor_(deformation_map_functor_) {}

    /**
     * @brief NodeProceduralDetailParameterT version of the constructor required by the factory.
     * Calling this constructor imply initializing node parameter by hand
     * @param noise_parameter
     */
    NodeProceduralDetailParameterT(const TNoiseParameter& noise_parameter)
        : AbstractNodeProceduralDetailParameter(0.0, 0.0),
          noise_parameter_(noise_parameter)
    {}

    /**
     * @brief NodeProceduralDetailParameterT version of the constructor required by the CloneForEval
     * @param rhs
     */
    NodeProceduralDetailParameterT(const NodeProceduralDetailParameterT &rhs) :
        AbstractNodeProceduralDetailParameter(rhs), noise_parameter_(rhs.noise_parameter_),
        deformation_map_functor_(rhs.deformation_map_functor_) {}

    ~NodeProceduralDetailParameterT(){}

    virtual std::shared_ptr<ScalarField> CloneForEval() const;

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        AbstractNodeProceduralDetailParameter::AddXmlAttributes(element);
        AddXmlFunctor(element);

    }
    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        AbstractNodeProceduralDetailParameter::AddXmlAttributes(element, ids);
        AddXmlFunctor(element);
    }
    void AddXmlFunctor(TiXmlElement * element) const
    {
        TiXmlElement *element_functor_noise = new TiXmlElement( XmlFunctorField() );
        element->LinkEndChild(element_functor_noise);
        TiXmlElement * element_child_noise = noise_parameter_.ToXml();
        element_functor_noise->LinkEndChild(element_child_noise);

        TiXmlElement *element_functor_deformation_map = new TiXmlElement( "TFunctorDeformationMap" );
        element->LinkEndChild(element_functor_deformation_map);
        TiXmlElement * element_child_deform = deformation_map_functor_.ToXml();
        element_functor_deformation_map->LinkEndChild(element_child_deform);
    }

    virtual void InitFromXml(const TiXmlElement *element);

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual Scalar Eval(const Point& point) const
    {
        return this->blend_operator_(this->children_, point);
    }
    virtual void EvalValueAndGrad(const Point& point,
                                  Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        return this->blend_operator_.EvalValueAndGrad(this->children_, point, epsilon_grad, value_res, grad_res);
    }
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
    {
        this->blend_operator_.EvalValueAndGradAndProperties(
                    this->children_,
                    point, epsilon_grad,
                    scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                    vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                    value_res, grad_res,
                    scal_prop_res,
                    vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                    );
    }

    virtual void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                                typename std::map<BlobtreeNode*, typename std::pair<core::AABBox, core::AABBox> >& updated_areas)
    {
        this->blend_operator_.PrepareForEval(*this, epsilon_bbox, accuracy_needed, updated_areas);
        this->UpdateNodeCompacity();
    }

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual core::AABBox GetAxisBoundingBox(Scalar value) const
    {
        return this->blend_operator_.GetAxisBoundingBox(this->children(), value);
    }

    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const;

    /////////////////
    /// Accessors ///
    /////////////////

    virtual const core::Functor& noise_parameter() const { return noise_parameter_; }
    inline const TNoiseParameter& noise_parameter() { return noise_parameter_; }
    virtual const PolynomialDeformationMap& deformation_map_functor() const { return deformation_map_functor_; }
    inline const TFunctorDeformationMap& deformation_map_functor() { return deformation_map_functor_; }

    virtual Scalar interior_dist_limit() const
    {
        return deformation_map_functor_.l_min();
    }
    virtual Scalar exterior_dist_limit() const
    {
        return deformation_map_functor_.l_max();
    }

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_noise_parameter(const TNoiseParameter& noise_parameter)
    {
        noise_parameter_ = noise_parameter;
    }
    void set_deformation_map_functor(const TFunctorDeformationMap& deformation_map_functor)
    {
        deformation_map_functor_ = deformation_map_functor;
    }

    /////////////////
    /// Attributs ///
    /////////////////

private :
    TNoiseParameter noise_parameter_;
    TFunctorDeformationMap deformation_map_functor_; // deformation map parameter

};

} // Close namespace Convol

} // Close namespace expressive

// implementation file
#include <convol/blobtreenode/NodeProceduralDetailParameterT.hpp>

#endif // NODEGABORT_H
