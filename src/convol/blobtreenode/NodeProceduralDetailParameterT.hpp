
#include <core/xmlloaders/TraitsXmlQuery.h>

#include <convol/tools/ConvergenceTools.h>

namespace  expressive {

namespace convol {

template<typename TNoiseParameter>//, typename TFunctorDeformationMap>
std::shared_ptr<ScalarField> NodeProceduralDetailParameterT<TNoiseParameter>/*,TFunctorDeformationMap>*/::CloneForEval() const
{
    return std::make_shared<NodeProceduralDetailParameterT<TNoiseParameter>/*,TFunctorDeformationMap>*/ >(*this);
}

template<typename TNoiseParameter>//, typename TFunctorDeformationMap>
void NodeProceduralDetailParameterT<TNoiseParameter>/*,TFunctorDeformationMap>*/::InitFromXml(const TiXmlElement *element)
{
    double tilt_angle;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailParameterT::InitFromXml",
                                               element, "tilt_angle", &tilt_angle);
    set_tilt_angle(tilt_angle);

    double rotate_angle;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailParameterT::InitFromXml",
                                               element, "rotate_angle", &rotate_angle);
    set_rotate_angle(rotate_angle);

    const TiXmlElement *elem_deform_map
            = core::TraitsXmlQuery::QueryFirstChildElement("NodeProceduralDetailT::InitFromXml",
                                                           element, "TFunctorDeformationMap");
    elem_deform_map = core::TraitsXmlQuery::QueryFirstChildElement("NodeProceduralDetailT::InitFromXml",
                                                                   elem_deform_map, "PolynomialDeformationMap");
    deformation_map_functor_.InitFromXml(elem_deform_map);
    //TODO:@todo : should not be needed for noise since it should already be initialized
}


template<typename TNoiseParameter>//, typename TFunctorDeformationMap>
Point NodeProceduralDetailParameterT<TNoiseParameter>/*,TFunctorDeformationMap>*/::GetAPointAt(Scalar value, Scalar epsilon) const
{
///TODO:@todo : methode provisoir : a remplacer !!!!
    if(this->children_.size() != 0)
    {
        auto iter_child = (this->children()).begin();
        for(; iter_child != (this->children()).end(); ++iter_child)
        {
            Point init_point = (*iter_child)->GetAPointAt(value, epsilon*0.1); // *0.1 sinon sa ne passe pas !!!!

            Vector search_dir = this->EvalGrad(init_point, 0.0001); //TODO : @todo : arbitrary
            search_dir.normalize();

            // Accuracy for gradient computation
            // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
            Scalar grad_epsilon = 0.0001;   // arbitrary
            if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }
            Scalar r_min = -30.0, r_max = 30.0; //TODO : @todo : arbitrary

            Point conv_point;
            Convergence::SafeNewton1D(
                        *(static_cast<const AbstractNodeProceduralDetailParameter*>(this)),
                        init_point,
                        search_dir,
                        r_min,  //TODO : arbitrary								// security minimum bound
                        r_max,	//TODO : arbitrary
                        0.0,			// point at which we start the search, given in 1D
                        value,
                        epsilon*0.1,
                        grad_epsilon,
                        20, conv_point);
            Scalar f_conv_p = this->Eval(conv_point);
            if( (f_conv_p-value)*(f_conv_p-value) < epsilon*epsilon )
            {
                return conv_point;
            }
        }
    }
    else
    {
        return Point::Zero();
    }
    return Point::Zero();
}

} // Close namespace convol

} // Close namespace expressive
