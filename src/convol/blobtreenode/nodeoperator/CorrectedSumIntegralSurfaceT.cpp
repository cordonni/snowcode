#include <convol/blobtreenode/nodeoperator/CorrectedSumIntegralSurfaceT.h>

// core dependencies
#include <core/xmlloaders/TraitsXmlQuery.h>

// convol dependencies
#include <convol/factories/BlobtreeNodeFactory.h>

// List of all functor operator that can be used with a NodeOperator
// (this is required to register them in the BlobtreeNodeFacotry)

#include <convol/functors/kernels/cauchyinverse/HomotheticInverse.h>
#include <convol/functors/kernels/cauchyinverse/HomotheticCauchy.h>
#include <convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.h>
#include <convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.h>

//#include <convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial4.h>
//#include <convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial8.h>

namespace expressive {
namespace convol {

//////////////////////////////////////////////////////////////////////////////////////////
/// Register CorrectedSumIntegralSurfaceT<FunctorOperator> in the BlobtreeNodeFactory ///
//////////////////////////////////////////////////////////////////////////////////////////

static BlobtreeNodeFactory::Type<  HomotheticCompactPolynomial4, CorrectedSumIntegralSurfaceT<HomotheticCompactPolynomial4> >
                                                    CorrectedSumIntegralSurfaceT_HomotheticCompactPolynomial4_Type;
static BlobtreeNodeFactory::Type<  HomotheticCompactPolynomial6, CorrectedSumIntegralSurfaceT<HomotheticCompactPolynomial6> >
                                                    CorrectedSumIntegralSurfaceT_HomotheticCompactPolynomial6_Type;
static BlobtreeNodeFactory::Type<  HomotheticCompactPolynomial8, CorrectedSumIntegralSurfaceT<HomotheticCompactPolynomial8> >
                                                    CorrectedSumIntegralSurfaceT_HomotheticCompactPolynomial8_Type;

static BlobtreeNodeFactory::Type<  HomotheticCauchy3, CorrectedSumIntegralSurfaceT<HomotheticCauchy3> >
                                                    CorrectedSumIntegralSurfaceT_HomotheticCauchy3_Type;
static BlobtreeNodeFactory::Type<  HomotheticCauchy4, CorrectedSumIntegralSurfaceT<HomotheticCauchy4> >
                                                    CorrectedSumIntegralSurfaceT_HomotheticCauchy4_Type;
static BlobtreeNodeFactory::Type<  HomotheticCauchy5, CorrectedSumIntegralSurfaceT<HomotheticCauchy5> >
                                                    CorrectedSumIntegralSurfaceT_HomotheticCauchy5_Type;
static BlobtreeNodeFactory::Type<  HomotheticCauchy6, CorrectedSumIntegralSurfaceT<HomotheticCauchy6> >
                                                    CorrectedSumIntegralSurfaceT_HomotheticCauchy6_Type;

static BlobtreeNodeFactory::Type<  HomotheticInverse3, CorrectedSumIntegralSurfaceT<HomotheticInverse3> >
                                                    CorrectedSumIntegralSurfaceT_HomotheticInverse3_Type;
static BlobtreeNodeFactory::Type<  HomotheticInverse4, CorrectedSumIntegralSurfaceT<HomotheticInverse4> >
                                                    CorrectedSumIntegralSurfaceT_HomotheticInverse4_Type;
static BlobtreeNodeFactory::Type<  HomotheticInverse5, CorrectedSumIntegralSurfaceT<HomotheticInverse5> >
                                                    CorrectedSumIntegralSurfaceT_HomotheticInverse5_Type;
static BlobtreeNodeFactory::Type<  HomotheticInverse6, CorrectedSumIntegralSurfaceT<HomotheticInverse6> >
                                                    CorrectedSumIntegralSurfaceT_HomotheticInverse6_Type;

static BlobtreeNodeFactory::Type<  CompactedHomotheticInverse3, CorrectedSumIntegralSurfaceT<CompactedHomotheticInverse3> >
                                                    CorrectedSumIntegralSurfaceT_CompactedHomotheticInverse3_Type;
static BlobtreeNodeFactory::Type<  CompactedHomotheticInverse4, CorrectedSumIntegralSurfaceT<CompactedHomotheticInverse4> >
                                                    CorrectedSumIntegralSurfaceT_CompactedHomotheticInverse4_Type;
static BlobtreeNodeFactory::Type<  CompactedHomotheticInverse5, CorrectedSumIntegralSurfaceT<CompactedHomotheticInverse5> >
                                                    CorrectedSumIntegralSurfaceT_CompactedHomotheticInverse5_Type;
static BlobtreeNodeFactory::Type<  CompactedHomotheticInverse6, CorrectedSumIntegralSurfaceT<CompactedHomotheticInverse6> >
                                                    CorrectedSumIntegralSurfaceT_CompactedHomotheticInverse6_Type;


} // Close namespace convol

} // Close namespace expressive

