/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: GradientBasedIntegralSurfaceT.h

   Language: C++

   License: Convol Licence

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inrialpes.fr

   Description: Header file for blending node using a method inspired from "Gradient-based implicit blend".
                An evaluation of this scalar field is done by post analyzing the relationship between
                Homothetic/Scalis scalar field and the associated homothetic gradient (gradient of Hornus scalar field).

                WARNING : all children must be templated by the same kind of kernel (with the same parameters),
                          for now it only works with HomotheticInverse"i" kernel ...

                NB : it is not a functor because we should test the type of the children when added

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_GRADIENT_BASED_INTEGRAL_SURFACE_H_
#define CONVOL_GRADIENT_BASED_INTEGRAL_SURFACE_H_


// core dependencies
#include <core/CoreRequired.h>

// convol dependencies
#include <convol/blobtreenode/NodeOperatorT.h>
    // blobtreenode
    #include <convol/blobtreenode/nodeoperator/IntegralSurfaceOperator.h>
    // functors
//    #include <core/functor/Functor.h>
    #include <convol/functors/operators/BlendSum.h>
    // primitives
    #include <convol/primitives/NodeSegmentSFieldT.h>
    #include <convol/primitives/NodeISSubdivisionCurveT.h>
    #include <convol/primitives/NodeTriangleSFieldT.h>
    #include <convol/primitives/NodePointSFieldT.h>
    // kernels
    #include <convol/functors/kernels/SemiNumericalHomotheticConvolT.h>

#include <convol/tools/FittedBVH.h>
#include <convol/tools/FBVHKernelEval.h>

//#define PROJECTION_EIGEN_POLYNOMIAL_ROOT
//#define LIMIT_OF_CORRECTION_EIGEN_POLYNOMIAL_ROOT

#if defined(LIMIT_OF_CORRECTION_EIGEN_POLYNOMIAL_ROOT) || defined(PROJECTION_EIGEN_POLYNOMIAL_ROOT)
#include <EigenUnsupported/Polynomials>
#endif

// should be defined to do simple directional blending (deadline ...)
//#define DIRECTIONAL_BLENDING

namespace expressive {

namespace convol {

class CONVOL_API AbstractGradientBasedIntegralSurface
        : public NodeOperator
{
public:
    EXPRESSIVE_MACRO_NAME("GradientBasedIntegralSurface")

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractGradientBasedIntegralSurface(Scalar special_parameter, bool analytical_projection = false)
        : NodeOperator(StaticName()), special_parameter_(special_parameter), analytical_projection_(analytical_projection)
    {
        this->parent_ = nullptr;
        this->arity_ = -1;

        ComputeHelpVariables();
    }
    ~AbstractGradientBasedIntegralSurface(){}

//    virtual std::string ParameterName() const;

//    ///////////////////
//    // Type Function //
//    ///////////////////

//    virtual ENodeOperatorType Type() const { return E_GradientBasedIntegralSurface; }

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        NodeOperator::AddXmlAttributes(element);
        element->SetDoubleAttribute("special_parameter", special_parameter_);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        NodeOperator::AddXmlAttributes(element, ids);
        element->SetDoubleAttribute("special_parameter", special_parameter_);
    }

    void InitFromXml(const TiXmlElement *element);

    /////////////////
    /// Accessors ///
    /////////////////

    Scalar special_parameter() const { return special_parameter_; }

    bool analytical_projection() const { return analytical_projection_; }

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_special_parameter(Scalar special_parameter)
    {
        special_parameter_ = special_parameter;
        ComputeHelpVariables();
        this->InvalidateBoundingBox();
    }

#if defined ( _WIN32 ) || defined( _WIN64 )
/// Because window compiler do not like standard (cf cbrt ...) /////
    /** \brief To solve the cube root of a number
    * \param x
    * \return the cube root of x
    */
    inline static Scalar cube_root(Scalar x)
    {
        return (x < 0.0) ?
                        -pow(-x,1.0/3.0)
                       : pow(x,1.0/3.0);
    }
////////////////////////////////////////////////////////////////////
#endif 

protected:

    //TODO:@todo : constructor required for the factories
    AbstractGradientBasedIntegralSurface() : NodeOperator(StaticName())
    {
        this->parent_ = nullptr;
        this->arity_ = -1;
    }

    // Copy constructor
    AbstractGradientBasedIntegralSurface(const AbstractGradientBasedIntegralSurface &rhs)
        : NodeOperator(rhs), special_parameter_(rhs.special_parameter_), analytical_projection_(rhs.analytical_projection_),
          proj_angle_(rhs.proj_angle_), tan_special_(rhs.tan_special_)
    {}


    /////////////////
    /// Attributs ///
    /////////////////

protected:
    // Correspond to ??? TODO:@todo complete when method is finished
    Scalar special_parameter_;

    bool analytical_projection_;

    // Help variables for computation
    Scalar proj_angle_; // alpha in the paper
    Scalar tan_special_;
//    Scalar i_p_tan_gamma; // = 1+tan(gamma) tel que gamma = alpha - pi/4
//    Scalar i_m_tan_gamma; // = 1-tan(gamma) tel que gamma = alpha - pi/4
//  // This value are now computed on the fly to ease the use of parametrized direction

    inline void ComputeHelpVariables()
    {
        proj_angle_ = M_PI*0.5*(1.0 - special_parameter_);

        tan_special_ = tan(proj_angle_-M_PI*0.25);
//        i_p_tan_gamma_ = 1.0 + tan_special_;
//        i_m_tan_gamma_ = 1.0 - tan_special_;
    }

    //////////////////////////////////////////////////////////////////
    #ifdef DIRECTIONAL_BLENDING
        mutable std::vector<Vector> vect_prop_res_dir_;
        mutable std::vector<Vector> vect_prop_res_dir_aux_;
        mutable std::vector<Scalar> scal_prop_res_empty_;

        mutable std::vector<Scalar> field_value_;
        mutable std::vector<Vector> weighted_dir_value_;

        // Stuff for Eval...Direction
        static const std::vector<ESFVectorProperties> vect_prop_ids_dir_;
        static std::vector<ESFVectorProperties> MakeEFSVectorPropertiesDir()
        {
            std::vector<ESFVectorProperties> res;
            res.push_back(Traits::E_Direction);
            return res;
        }

        static const std::vector<ESFScalarProperties> scal_prop_ids_empty_;
        static std::vector<ESFScalarProperties> MakeEFSScalarPropertiesEmpty()
        {
            std::vector<ESFScalarProperties> res;
            return res;
        }
    #endif
    //////////////////////////////////////////////////////////////////
};


//////////////////////////////////////////////////////////////////
#ifdef DIRECTIONAL_BLENDING
template<typename TTraits>
const typename std::vector<typename AbstractGradientBasedIntegralSurface<TTraits>::ESFVectorProperties> AbstractGradientBasedIntegralSurface<TTraits>::vect_prop_ids_dir_
        = AbstractGradientBasedIntegralSurface<TTraits>::MakeEFSVectorPropertiesDir();

template<typename TTraits>
const typename std::vector<typename AbstractGradientBasedIntegralSurface<TTraits>::ESFScalarProperties> AbstractGradientBasedIntegralSurface<TTraits>::scal_prop_ids_empty_
        = AbstractGradientBasedIntegralSurface<TTraits>::MakeEFSScalarPropertiesEmpty();
#endif
//////////////////////////////////////////////////////////////////

template<typename TFunctorKernel>
class OptGradientBasedIntegralSurfaceT;

/*! \brief Node for Gradient-based homothetic integral surfaces.
 * TODO:@todo : the 10 in num_kernel_ creation should be a parameter of the node
 */
template<typename TFunctorKernel>
class GradientBasedIntegralSurfaceT :
        public AbstractGradientBasedIntegralSurface, public IntegralSurfaceOperatorT<TFunctorKernel>
{
public:
    typedef NodeSegmentSFieldT<TFunctorKernel> NodeSegmentSFKer;
    typedef NodeISSubdivisionCurveT<TFunctorKernel> NodeISSubdivisionCurveSFKer;
    typedef NodePointSFieldT<TFunctorKernel> NodePointSFKer;
    typedef NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<TFunctorKernel> > NodeTriangleSFNumKer;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    GradientBasedIntegralSurfaceT(Scalar special_parameter, const TFunctorKernel& kernel, bool analytical_projection = false)
        : AbstractGradientBasedIntegralSurface(special_parameter, analytical_projection),
          IntegralSurfaceOperatorT<TFunctorKernel>(kernel), num_kernel_(15, kernel)
    {}

    //TODO:@todo : required for the factory : if this constructor is called then parameter should be initialized by hand afterward ...
    GradientBasedIntegralSurfaceT(const TFunctorKernel& kernel)
        : AbstractGradientBasedIntegralSurface(),
          IntegralSurfaceOperatorT<TFunctorKernel>(kernel), num_kernel_(15, kernel) { }

    // Copy constructor : required for cloning
    GradientBasedIntegralSurfaceT(const GradientBasedIntegralSurfaceT &rhs)
        :   AbstractGradientBasedIntegralSurface(rhs),
            IntegralSurfaceOperatorT<TFunctorKernel>(rhs), num_kernel_(rhs.num_kernel_) {}

    ~GradientBasedIntegralSurfaceT(){}

    virtual std::shared_ptr<ScalarField> CloneForEval() const;

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        AbstractGradientBasedIntegralSurface::AddXmlAttributes(element);
        IntegralSurfaceOperatorT<TFunctorKernel>::AddXmlAttributes(element);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        AbstractGradientBasedIntegralSurface::AddXmlAttributes(element, ids);
        IntegralSurfaceOperatorT<TFunctorKernel>::AddXmlAttributes(element, ids);
    }

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual Scalar Eval(const Point& point) const;
    virtual Vector EvalGrad(const Point& point, Scalar epsilon_grad) const;
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                               const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                               const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                               Scalar& value_res, Vector& grad_res,
                                               std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               ) const;
//    inline void EvalProperties(const Point& point,
//                               const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
//                               const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
//                               std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
//                               std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
//                              ) const;

    // Two following function works as a NodeOperatorNAry<BlendSum> ...
    void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                        typename std::map<BlobtreeNode*, typename std::pair<core::AABBox, core::AABBox> >& updated_areas);

    core::AABBox GetAxisBoundingBox(Scalar value) const;


    Scalar FieldCorrection(Scalar f_scalis, Scalar grad_norm_hornus, Scalar proj_angle, Scalar tan_special) const;

    Scalar AnalyticalProjection(Scalar f_scalis, Scalar grad_norm_hornus, Scalar tan_special) const;

    /////////////////
    /// Accessors ///
    /////////////////

    /////////////////
    /// Modifiers ///
    /////////////////

    void AddChild(std::shared_ptr<BlobtreeNode> bt_node);

protected:

    /////////////////
    /// Attributs ///
    /////////////////

    SemiNumericalHomotheticConvolT<TFunctorKernel>  num_kernel_;

    //////////////////////
    /// Help variables ///
    //////////////////////

    // Blending functor used to avoid implementation of some function
    BlendSum blend_sum_;

#if defined(LIMIT_OF_CORRECTION_EIGEN_POLYNOMIAL_ROOT) || defined(PROJECTION_EIGEN_POLYNOMIAL_ROOT)
    // "unsupported" eigen stuff to compute roots of a polynomial
    typedef Eigen::Matrix<Scalar,4,1> Polynomial3;
    mutable Eigen::PolynomialSolver<Scalar,3> poly_solver_;
    mutable std::vector<Scalar> roots_;
#endif

//////////////////////////////////////////////////////////////////
#ifdef DIRECTIONAL_BLENDING
    Scalar FieldCorrection(Scalar field, Scalar norm_grad, Scalar direction) const;

    Scalar AngleCoeff(Scalar cos_angle, Scalar blend_max, Scalar blend_min) const
    {
//        Scalar angle_coeff = (cos_angle<0) ? 1.0 : 1.0 - cos_angle*cos_angle;
//        Scalar angle_coeff = (cos_angle<0) ? 1.0 : 1.0 - cos_angle*cos_angle*cos_angle*cos_angle;
//        angle_coeff *= angle_coeff;
//        angle_coeff *= angle_coeff;

//        Scalar angle_coeff = (cos_angle<0) ? 1.0 : 1.0 - cos_angle*cos_angle;
//        angle_coeff = 1.0 - angle_coeff*angle_coeff;

        Scalar angle_coeff = 1.0 - cos_angle*cos_angle;
        angle_coeff = 1.0 - angle_coeff*angle_coeff*angle_coeff;

        return (blend_max - blend_min) * angle_coeff + blend_min;
        //TODO : @todo : prendre en compte nouvelle valeur des parametres...
    }
#endif
//////////////////////////////////////////////////////////////////
};


/////////////////
/// Attributs ///
/////////////////

template<typename TFunctorKernel>
class OptGradientBasedIntegralSurfaceT : public GradientBasedIntegralSurfaceT<TFunctorKernel>
{
public:
    EXPRESSIVE_MACRO_NAME("OptGradientBasedIntegralSurface") // This macro defined functions
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    typedef NodeSegmentSFieldT<TFunctorKernel> NodeSegmentSFKer;
    typedef NodeISSubdivisionCurveT<TFunctorKernel> NodeISSubdivisionCurveSFKer;
    typedef NodePointSFieldT<TFunctorKernel> NodePointSFKer;
    typedef NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<TFunctorKernel> > NodeTriangleSFNumKer;


//    typedef FittedBVH< std::pair<core::OptWeightedSegment, Properties1D> >::CDataIterator CDataIteratorSeg;
    typedef FittedBVH< std::pair<core::OptWeightedTriangle, Properties0D> >::CDataIterator CDataIteratorTri;
    typedef FittedBVH< std::pair<core::WeightedPoint, Properties0D> >::CDataIterator CDataIteratorPoint;

    OptGradientBasedIntegralSurfaceT(const GradientBasedIntegralSurfaceT<TFunctorKernel> &rhs)
        :   GradientBasedIntegralSurfaceT<TFunctorKernel>(rhs), f_bvh_segment_(4), f_bvh_triangle_(4), f_bvh_point_(4)
    {
        this->parent_ = nullptr;
        this->arity_ = 0;

        this->axis_bounding_box_ = rhs.axis_bounding_box();

        std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
        start_time = std::chrono::high_resolution_clock::now();

        // prevent multiple reallocation of the vector
//        f_bvh_segment_.data().reserve(rhs.nb_children()); //TODO:@todo : this is only ok if there is only NodeSegmentSField !!!

        for(auto &child : rhs.children())
        {
            auto node_segment = std::dynamic_pointer_cast<AbstractNodeSegmentSField>(child);
            if(node_segment != nullptr) {
                auto opt_w_seg = node_segment->opt_w_seg();
                f_bvh_segment_.data().push_back( std::make_pair(node_segment->axis_bounding_box(),
                                                        std::make_pair(opt_w_seg, node_segment->properties()) ) );
                f_bvh_segment_.barycenter().push_back(  opt_w_seg.Barycenter() );
                continue;
            }

            auto node_curve = std::dynamic_pointer_cast<AbstractNodeISSubdivisionCurve>(child);
            if(node_curve != nullptr) {
                for(auto &pair : node_curve->vec_seg())
                {
//TODO:@todo : should compute correctly the property to be attached from the unique property of ...
//TODO:@todo : following solution is ok if property is constant
                    f_bvh_segment_.data().push_back( std::make_pair(pair.first,
                                                                    std::make_pair(pair.second,node_curve->properties())) );
                    f_bvh_segment_.barycenter().push_back(pair.second.Barycenter());
                    continue;
                }
            }

            auto node_triangle = std::dynamic_pointer_cast<AbstractNodeTriangleSField>(child);
            if(node_triangle != nullptr) {
                auto opt_w_tri = node_triangle->opt_w_tri();
                f_bvh_triangle_.data().push_back( std::make_pair(node_triangle->axis_bounding_box(),
                                                        std::make_pair(opt_w_tri, node_triangle->properties()) ) );
                f_bvh_triangle_.barycenter().push_back(  opt_w_tri.Barycenter() );
                continue;
            }

            auto node_point = std::dynamic_pointer_cast<NodePointSFKer>(child);
            if(node_point != nullptr) {
                f_bvh_point_.data().push_back( std::make_pair(node_point->axis_bounding_box(),
                                                        std::make_pair( *(node_point.get()), node_point->properties()) ) );
                f_bvh_point_.barycenter().push_back( *(node_point.get()) );
                continue;
            }
        }

        f_bvh_segment_.Init(this->axis_bounding_box_);
        f_bvh_triangle_.Init(this->axis_bounding_box_);
        f_bvh_point_.Init(this->axis_bounding_box_);



        end_time =  std::chrono::high_resolution_clock::now();
        ork::Logger::DEBUG_LOGGER->logf("POLYGONIZER", "Fitted FBVH construction time : %f", std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000.0);
    }


    virtual Scalar Eval(const Point& point) const;
//    virtual Vector EvalGrad(const Point& point, Scalar epsilon_grad) const;
//    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                               const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                               const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                               Scalar& value_res, Vector& grad_res,
                                               std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               ) const;



protected:
    FBVHKernelEvalT<  std::pair<core::OptWeightedSegment, Properties1D>, TFunctorKernel> f_bvh_segment_;
    FBVHKernelEvalT<  std::pair<core::OptWeightedTriangle, Properties0D>, SemiNumericalHomotheticConvolT<TFunctorKernel> > f_bvh_triangle_;
    FBVHKernelEvalT<  std::pair<core::WeightedPoint, Properties0D>, TFunctorKernel> f_bvh_point_;
//    FittedBVH< std::pair<core::OptWeightedSegment, Properties1D> > f_bvh_segment_;
//    FittedBVH< std::pair<core::OptWeightedTriangle, Properties0D> > f_bvh_triangle_;
//    FittedBVH< std::pair<core::WeightedPoint, Properties0D> > f_bvh_point_;

    //TODO:@todo : for now keep the children to avoid re-implementation of  a lot of stuff
    // later their copy should be avoided !!!! => save time and memory
};



} // Close namespace convol

} // Close namespace expressive

// implementation file
#include <convol/blobtreenode/nodeoperator/GradientBasedIntegralSurfaceT.hpp>


#endif // CONVOL_GRADIENT_BASED_INTEGRAL_SURFACE_H_
