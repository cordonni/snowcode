
//#define PROJECTION_EIGEN_POLYNOMIAL_ROOT
//#define LIMIT_OF_CORRECTION_EIGEN_POLYNOMIAL_ROOT

#include <convol/tools/GradientEvaluationTools.h>
#include <convol/tools/SolvingEquation4T.h> //TODO:@todo

// should be defined to do simple directional blending (deadline ...)
//#define DIRECTIONAL_BLENDING

namespace expressive {

namespace convol {

template<typename TFunctorKernel>
std::shared_ptr<ScalarField> GradientBasedIntegralSurfaceT<TFunctorKernel>::CloneForEval() const
{
//        return std::make_shared<GradientBasedIntegralSurfaceT>(*this);
    return std::make_shared<OptGradientBasedIntegralSurfaceT<TFunctorKernel> >(*this);
}

/////////////////////////////////////////////
/// AbstractGradientBasedIntegralSurfaceT ///
/////////////////////////////////////////////



////////////////////////////////////
/// GradientBasedIntegralSurfaceT ///
////////////////////////////////////

//template<typename TFunctorKernel>
//std::string GradientBasedIntegralSurfaceT<TFunctorKernel>::FunctorName() const
//{
//    std::ostringstream name;
//    name << "<" << kernel_.Name() << ">";
//    return name.str();
//}


/////////////////
/// Modifiers ///
/////////////////

template<typename TFunctorKernel>
void GradientBasedIntegralSurfaceT<TFunctorKernel>::AddChild(std::shared_ptr<BlobtreeNode> bt_node)
{
    assert(bt_node->parent() == NULL); // please disconnect the node from its parent before anything.
    assert(bt_node != NULL && this->arity_ == -1);

    if(bt_node->parent() != NULL)
    {
        bt_node->nonconst_parent()->RemoveChild(bt_node);
    }
    bt_node->set_parent(this);
    this->children_.push_back(bt_node);

    // Test if the bt_node is either,
    //      - a NodeSegmentSField<TFunctorKernel>
    //      - a NodeISSubdivisionCurveSField<TFunctorKernel>
    //      - a NodePointSField<TFunctorKernel>
    //      - a NodeTriangleSField<TFunctorKernel>
    //
    // TODO: mutualize this code
    if(std::dynamic_pointer_cast<NodeSegmentSFKer>(bt_node) != nullptr)
    {
        static_cast<TFunctorKernel&>(std::static_pointer_cast<NodeSegmentSFKer>(bt_node)->kernel_functor()).set_scale(this->kernel_.scale());
    }
    else if(std::dynamic_pointer_cast<NodeISSubdivisionCurveSFKer>(bt_node) != nullptr)
    {
        static_cast<TFunctorKernel&>(std::static_pointer_cast<NodeISSubdivisionCurveSFKer>(bt_node)->kernel_functor()).set_scale(this->kernel_.scale());
    }
    else if(std::dynamic_pointer_cast<NodePointSFKer>(bt_node) != nullptr)
    {
        static_cast<TFunctorKernel&>(std::static_pointer_cast<NodePointSFKer>(bt_node)->kernel_functor()).set_scale(this->kernel_.scale());
    }
    else if(std::dynamic_pointer_cast<NodeTriangleSFNumKer>(bt_node) != nullptr)
    {
        static_cast<TFunctorKernel&>(std::static_pointer_cast<NodeTriangleSFNumKer>(bt_node)->kernel_functor()).set_scale(this->kernel_.scale());
    }
    else
    {
        // this primitive/kernel is not supported
        assert(false);
    }

    this->InvalidateBoundingBox();
}

//////////////////////////
// Evaluation functions //
//////////////////////////




template<typename TFunctorKernel>
auto GradientBasedIntegralSurfaceT<TFunctorKernel>::FieldCorrection(Scalar f_scalis, Scalar grad_norm_hornus, Scalar proj_angle, Scalar tan_special) const -> Scalar
{
    const Scalar i_p_tan_gamma = 1.0 + tan_special;
    const Scalar i_m_tan_gamma = 1.0 - tan_special;

    Scalar k = this->kernel_.degree_r();

    // can probaly be computed with slightly less operations...
    Scalar spe_x = pow(2.0 / f_scalis, 1.0 / (k-1.0) );
    Scalar spe_x_i = pow(spe_x, k);
    Scalar tmp = spe_x_i * grad_norm_hornus / ( 2.0 * (k-1.0));
    Scalar tmp_sqr = 1.0-tmp*tmp;
    if(tmp_sqr>0.0)
    {
//        Scalar dist = 2.0 * spe_x * sqrt(tmp_sqr);
//        Scalar d_sqr = dist*dist*0.25 ;
        Scalar d_sqr = spe_x * spe_x  * tmp_sqr;

        Scalar i_p_t_sqr = i_p_tan_gamma * i_p_tan_gamma;
        Scalar i_m_t_sqr = i_m_tan_gamma * i_m_tan_gamma;

#ifndef LIMIT_OF_CORRECTION_EIGEN_POLYNOMIAL_ROOT
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Analytical root : this is can become numerically instable in some cases ....
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Calcul de la limite en fonction de la dir de projection (resolution equation de degre 3)
        // (cas de tangence entre la courbe du cas special associe a  la valeur dist et la direction de projection)
        Scalar a = (k+1.0)*(k+1.0) * d_sqr*d_sqr * i_m_t_sqr;
//        Scalar b = -2.0*d_sqr* (k+1.0) * k * i_m_t_sqr;
        Scalar c = k*k * i_m_t_sqr + d_sqr * i_p_t_sqr;
        Scalar e = - i_p_t_sqr;

        Scalar b_div_3a = -(2.0/3.0)*(k/(k+1.0))/d_sqr;

        Scalar z;
        if( proj_angle == 0.0 )
        {
            z = (k/(k+1.0)) / d_sqr;
        }
        else
        {
            Scalar sqr_b_div_3a = b_div_3a*b_div_3a;
            Scalar p = (1.0/3.0) * c/a - sqr_b_div_3a;
            Scalar q = 0.5*(e-b_div_3a*c)/a + b_div_3a*sqr_b_div_3a;

            Scalar root;

            Scalar delta = q*q + p*p*p;
            // 1. 1 real solution
            if(delta>0){
                Scalar sqrt_delta = sqrt(delta);
#if defined ( _WIN32 ) || defined( _WIN64 )
                root = cube_root(-q+sqrt_delta) + cube_root(-q-sqrt_delta); // there is a new function for cubic_root : cbrt
#else
				root = cbrt(-q+sqrt_delta) + cbrt(-q-sqrt_delta); // there is a new function for cubic_root : cbrt
#endif
            } //cube_root
            // 2. 2 real solutions including 1 double
            else if(delta==0){
                root = 0.0;
//                assert(false); //root_1 = (3.0*q)/p; root_2 = (-3.0*q)/(2.0*p);
            }
            // 3. 3 real distinct solutions
            else{
                Scalar sqrt_p = sqrt(-p);
                Scalar teta = acos(q/(p*sqrt_p));
                if( proj_angle > 0.0 )
                    root = 2.0*sqrt_p * cos(teta/3.0);
                else
                    root = 2.0*sqrt_p * cos((teta+4.0*M_PI)/3.0); // this is for negative angle of projection
            }
////root = 2.0*sqrt_p*cos((teta+2.0*M_PI)/3.0);

            z = root - b_div_3a;
        }
#else
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Numerical root : this is more stable than analytical one ! but should be adapted to retrieve the right root when alpha outside [0;pi/2]
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        Scalar a = (k+1.0)*(k+1.0) * d_sqr*d_sqr * i_m_t_sqr;
        Scalar b = -2.0*d_sqr* (k+1.0) * k * i_m_t_sqr;
        Scalar c = k*k * i_m_t_sqr + d_sqr * i_p_t_sqr;
        Scalar e = - i_p_t_sqr;
        Polynomial3 poly(e,c,b,a);
        poly_solver_.compute(poly);
        roots_.clear();
//        poly_solver_.realRoots(roots_);
//        z = roots_[0]; // this is correct if and only
//        for(auto &r : roots_)
//        {
//            if()
//        }
        bool hasRoot;
        Scalar z = poly_solver_.greatestRealRoot(hasRoot);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif

        Scalar x_limit = 2.0 * pow(z, (k-1.0) / 2.0);

        Scalar y_limit = 2.0 * (k-1.0) * pow(z, k/2.0) * sqrt( 1.0 - d_sqr * z); // should use another equation to solve last numerical instability...
        if(f_scalis > x_limit && grad_norm_hornus < y_limit )//&& grad_norm_hornus<y_limit)
        { // Constant correction value on the problematic interval that is equal to the correction at the boundary
            if(this->analytical_projection_)
            {
//                return 0.0;
                return AnalyticalProjection(x_limit, y_limit, tan_special);
            }
            else
            {
//                return 0.0;
                Scalar relativ_horizontal2 = x_limit - pow(y_limit / (k-1.0) , (k-1.0)/k );
                Scalar relativ_vertical2   = (k-1.0) * pow(x_limit, k/(k-1.0) ) - y_limit;
//                const Scalar correction2 = ( relativ_horizontal2*relativ_vertical2) / (relativ_vertical2 + relativ_horizontal2 * this->tan_from_param_);
                const Scalar correction2 = i_m_tan_gamma*(relativ_horizontal2*relativ_vertical2)
                                            / (relativ_vertical2 * i_m_tan_gamma + relativ_horizontal2 * i_p_tan_gamma);

                if(x_limit < correction2) return 0.0; // this is done to handle negative anlge of projection !

                return x_limit - correction2;
            }
        }
    }

    // the basic correction provide good result (no cavity problem) for this (field,gradient) value
    if(this->analytical_projection_)
    {
        return AnalyticalProjection(f_scalis,grad_norm_hornus, tan_special);
    }
    else
    {
        Scalar relativ_horizontal = f_scalis - pow( grad_norm_hornus/(k-1.0), (k-1.0)/k );
        Scalar relativ_vertical   = (k-1.0) * pow( f_scalis, k/(k-1.0) ) - grad_norm_hornus;

        const Scalar correction = i_m_tan_gamma*(relativ_horizontal*relativ_vertical)
                                    / (relativ_vertical * i_m_tan_gamma + relativ_horizontal * i_p_tan_gamma);

        if(f_scalis < correction) return 0.0; // this is done to handle negative anlge of projection !

        return f_scalis - correction;
    }

//////////////////////////////////////////////////////////////////
//    // Correction by projection in a fixed direction
//    const Scalar correction = ( relativ_horizontal*relativ_vertical) / (relativ_vertical + relativ_horizontal * this->tan_from_param_);
//    return f_scalis - correction;
//////////////////////////////////////////////////////////////////
//    // Correction by projecting to the closest point
//    const Scalar correction = ( relativ_horizontal*relativ_vertical*relativ_vertical) / (relativ_vertical*relativ_vertical + relativ_horizontal*relativ_horizontal);
//    return f_scalis - correction;
//////////////////////////////////////////////////////////////////
}


template<typename TFunctorKernel>
auto GradientBasedIntegralSurfaceT<TFunctorKernel>::AnalyticalProjection(Scalar f_scalis, Scalar grad_norm_hornus, Scalar tan_special) const -> Scalar
{
    const Scalar i_p_tan_gamma = 1.0 + tan_special;
    const Scalar i_m_tan_gamma = 1.0 - tan_special;

    const Scalar k = this->kernel_.degree_r();

#ifdef PROJECTION_EIGEN_POLYNOMIAL_ROOT
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Eigen::VectorXd poly(Eigen::VectorXd::Zero(k+1));
    poly(0)             = -(i_m_tan_gamma*grad_norm_hornus + i_p_tan_gamma*f_scalis );
    poly(k-1)  = i_p_tan_gamma;
    poly(k)    = (k-1.0) * i_m_tan_gamma;

    Eigen::PolynomialSolver<Scalar,Eigen::Dynamic> poly_solver_n;
    poly_solver_n.compute(poly);

    bool hasRoot;
    return pow(poly_solver_n.greatestRealRoot(hasRoot), k-1.0);
#else
    //Only works for inverse 3 and 4 ...
    //TODO:@todo : make a version with numerical root finding for all degrees ...

    // Compute solutions of equation
    const Scalar a_0 = -(i_m_tan_gamma*grad_norm_hornus + i_p_tan_gamma*f_scalis );
    const Scalar a_km1 = i_p_tan_gamma;
    const Scalar a_k   = (k-1.0) * i_m_tan_gamma;

    // this is both not optimized and not numerically stable in some cases... just for test !
    std::vector<Scalar> solutions;
    switch(this->kernel_.degree())
    {
        case 3 :
            solutions =  SolvingEquation4::third_general(a_k,a_km1,0.0,a_0);
            break;
        case 4 :
            solutions =  SolvingEquation4::fourth_general(a_k,a_km1,0.0,0.0,a_0);
            break;
        default :
            return 0.0;
    }
    for(auto it = solutions.begin(); it != solutions.end(); ++it)
    { // NB : there should be only one positive root !
        if( (*it) >= 0.0)
            return pow(*it, k-1.0);
    }
    return 0.0; // that should  not happen !!!
#endif
}


#ifdef DIRECTIONAL_BLENDING
//TODO:@todo : hack pour faire exemple simple de directional blend ...
template<typename TFunctorKernel>
auto GradientBasedIntegralSurfaceT<TFunctorKernel>::Eval(const Point& point) const -> Scalar
{
    this->vect_prop_res_dir_.resize(this->nb_children());
    this->vect_prop_res_dir_aux_.resize(this->nb_children());
    this->field_value_.resize(this->nb_children());
    this->weighted_dir_value_.resize(this->nb_children());

    Scalar f_scalis = 0.0;
    Vector grad_hornus(0.0); // in fact this is Hornus only for skeleton of dimension 1
    this->vect_prop_res_dir_[0] =  Vector::vectorized(0.0);
    Scalar f_tmp; Vector grad_tmp;

    int i = 0;
    for(auto it = (this->children_).begin(); it != (this->children_).end(); ++it)
    {
        (*it)->EvalHomotheticValues(point,f_tmp,grad_tmp);
        (*it)->EvalProperties(point, this->scal_prop_ids_empty_, this->vect_prop_ids_dir_, this->scal_prop_res_empty_, this->vect_prop_res_dir_aux_);

        this->field_value_[i] = f_tmp;

        const NodeScalarField* node = static_cast<NodeScalarField*>(*it);
        this->weighted_dir_value_[i] = static_cast<const SegmentSFKer*>(node->scalar_field())->unit_dir();

//        this->weighted_dir_value_[i] = this->vect_prop_res_dir_aux_[0];
//        this->weighted_dir_value_[i].normalize_cond(); //TODO ????

        f_scalis += f_tmp;
        grad_hornus += grad_tmp;

        ++i;
    }

    const Scalar grad_norm_hornus = grad_hornus.norm();

    Scalar direction = 0.0;
    Scalar sum_weight = 0.0;
    for(int i = 0; i<this->nb_children(); ++i)
    {
        for(int j = 0; j<i; ++j)
        {
            Scalar blend_min = 0.99; //0.99; //test with even higher values
            Scalar blend_max_ij = 0.0001; //-0.9999;
            Scalar direction_ij = AngleCoeff( this->weighted_dir_value_[j] | this->weighted_dir_value_[i] ,
                                              blend_max_ij,
                                              blend_min);

            sum_weight += this->field_value_[i]*this->field_value_[j];
            direction += this->field_value_[i]*this->field_value_[j] * direction_ij;
        }
        for(int j = i+1; j<this->nb_children(); ++j)
        {
            Scalar blend_min = 0.99; //0.99; //test with even higher values
            Scalar blend_max_ij = 0.0001; //-0.9999;
            Scalar direction_ij = AngleCoeff( this->weighted_dir_value_[j] | this->weighted_dir_value_[i] ,
                                              blend_max_ij,
                                              blend_min);

            sum_weight += this->field_value_[i]*this->field_value_[j];
            direction += this->field_value_[i]*this->field_value_[j] * direction_ij;
        }
    }
    direction /= sum_weight;

    return FieldCorrection(f_scalis, grad_norm_hornus, direction);
}

template<typename TFunctorKernel>
auto GradientBasedIntegralSurfaceT<TFunctorKernel>::FieldCorrection(Scalar f_scalis, Scalar grad_norm_hornus, Scalar direction) const -> Scalar
{
    Scalar tan_from_param = tan(M_PI*0.5*(1.0 - direction)) ;

    Scalar k = ((Scalar)(kernel_.i_));

    Scalar f_horizontal_correction  = pow( grad_norm_hornus/(k-1.0), (k-1.0)/k );
    Scalar grad_vertical_correction = (k-1.0) * pow( f_scalis, k/(k-1.0) );

    Scalar relativ_horizontal = f_scalis - f_horizontal_correction;
    Scalar relativ_vertical   = grad_vertical_correction - grad_norm_hornus;

    Scalar spe_x = pow(2.0 / f_scalis, 1.0 / (k-1.0) );
    Scalar spe_x_i = pow(spe_x, k);
    Scalar tmp = spe_x_i * grad_norm_hornus / ( 2.0 * (k-1.0));
    Scalar tmp_sqr = tmp*tmp;
    if(tmp_sqr<1.0)
    {
        Scalar dist = 2.0 * spe_x * sqrt( 1.0 - tmp_sqr);

        Scalar d = dist*dist*0.25 ;
        Scalar t_sqr = tan_from_param * tan_from_param;

        // Calcul de la limite en fonction de la dir de projection (resolution equation de degre 3)
        // (cas de tangence entre la courbe du cas special associe a  la valeur dist et la direction de projection)
        Scalar a3 = (k+1.0)*(k+1.0) * d*d;
        Scalar a2 = -2.0*d* (k+1.0) * k;
        Scalar a1 = k*k + d * t_sqr;
        Scalar a0 = - t_sqr;
        std::vector<Scalar> solutions =  SolvingEquation4T<Scalar>::third_general(a3,a2,a1,a0);
        Scalar z;
        if( M_PI*0.5*(1.0 - direction) > 0.0 )
            z = solutions[0];
        else
            z = solutions[2]; // this is for negative angle of projection

        Scalar x_limit = 2.0 * pow(z, (k-1.0) / 2.0);

        if(x_limit<f_scalis)
        {
            // on calcul le y_limit associe au nouveau x_limit
            Scalar y_limit = 2.0 * (k-1.0) * sqrt( pow(2.0/x_limit , 2.0/(k-1.0) )  - d ) * pow(0.5 * x_limit ,  (k+1.0) / (k-1.0) );

            Scalar f_horizontal_correction2 = pow(y_limit / (k-1.0), (k-1.0)/k );
            Scalar grad_vertical_correction2 = (k-1.0) * pow(x_limit, k/(k-1.0) );
            Scalar relativ_horizontal2 = x_limit - f_horizontal_correction2;
            Scalar relativ_vertical2   = grad_vertical_correction2 - y_limit;
            const Scalar correction2 = ( relativ_horizontal2*relativ_vertical2) / (relativ_vertical2 + relativ_horizontal2 * tan_from_param);

            if(x_limit < correction2) return 0.0; // this is done to handle negative anlge of projection !

            return x_limit - correction2;
        }
    }

    // the basic correction provide good result (no cavity problem) for this (field,gradient) value
    const Scalar correction = ( relativ_horizontal*relativ_vertical) / (relativ_vertical + relativ_horizontal * tan_from_param);

    if(f_scalis < correction) return 0.0; // this is done to handle negative anlge of projection !

    return f_scalis - correction;
}

template<typename TFunctorKernel>
void GradientBasedIntegralSurfaceT<TFunctorKernel>::EvalValueAndGradAndProperties(
                        const Point& point, const Scalar epsilon_grad,
                        const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                        const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                        Scalar& value_res, Vector& grad_res,
                        std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                        std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                ) const
{
    value_res = Eval(point);
    grad_res = GradientEvaluationT<Traits>::NumericalGradient6points(*this, point, epsilon_grad);
    EvalProperties(point, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);

}
#else
template<typename TFunctorKernel>
auto GradientBasedIntegralSurfaceT<TFunctorKernel>::Eval(const Point& point) const -> Scalar
{
    Scalar f_scalis = 0.0;
    Vector grad_hornus = Vector::Zero(); // in fact this is Hornus only for skeleton of dimension 1
    Scalar f_tmp; Vector grad_tmp;
    for(auto &child : this->children_)
    {
        if(child->axis_bounding_box().Contains(point))
        {
            child->EvalHomotheticValues(point,f_tmp,grad_tmp);
            f_scalis += f_tmp;
            grad_hornus += grad_tmp;
        }
    }
    const Scalar grad_norm_hornus = grad_hornus.norm();

    return FieldCorrection(f_scalis, grad_norm_hornus, proj_angle_, tan_special_);
}

template<typename TFunctorKernel>
void GradientBasedIntegralSurfaceT<TFunctorKernel>::EvalValueAndGradAndProperties(
                        const Point& point, Scalar epsilon_grad,
                        const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                        const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                        Scalar& value_res, Vector& grad_res,
                        std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                        std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                ) const
{   //TODO:@todo : this function is to be rewrited has soon as we find a way to compute closed-form gradient (or a good approximation)
    Scalar f_scalis = 0.0;
    Vector grad_hornus = Vector::Zero(); // in fact this is Hornus only for skeleton of dimension 1

    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
    {
        scal_prop_res[i] = 0.0;
    }
    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
    {
        vect_prop_res[i] = Vector::Zero();
    }

    std::vector<Scalar> scal_prop_aux;
    scal_prop_aux.resize(scal_prop_ids.size());
    std::vector<Vector> vect_prop_aux;
    vect_prop_aux.resize(vect_prop_ids.size());
    Scalar f_tmp; Vector grad_tmp;

    for(auto &child : this->children_)
    {
        if(child->axis_bounding_box().Contains(point))
        {
            child->EvalHomotheticValues(point,f_tmp,grad_tmp);
            f_scalis += f_tmp;
            grad_hornus += grad_tmp;

            child->EvalProperties(point,
                                  scal_prop_ids, vect_prop_ids,
                                  scal_prop_aux, vect_prop_aux);

            for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
            {
                scal_prop_res[i] += f_tmp*scal_prop_aux[i];
            }
            for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
            {
                vect_prop_res[i] += f_tmp*vect_prop_aux[i];
            }
        }
    }
    const Scalar grad_norm_hornus = grad_hornus.norm();

    value_res = FieldCorrection(f_scalis, grad_norm_hornus, proj_angle_, tan_special_);

    if(f_scalis != 0.0)
    {
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = scal_prop_res[i]/f_scalis;
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = vect_prop_res[i]/f_scalis;
        }
    }
//    else
//    {
//        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
//        {
//            scal_prop_res[i] = Traits::kSFScalarPropertiesDefault[scal_prop_ids[i]];
//        }
//        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
//        {
//            vect_prop_res[i] = Traits::kSFVectorPropertiesDefault[vect_prop_ids[i]];
//        }
//    }

    //TODO:@todo : BEURK !!! cost a lot and is not precise ... to be changed :)
    Point p_minus = point;
    p_minus[0] -= epsilon_grad;
    grad_res[0] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[0] += epsilon_grad;
    p_minus[1] -= epsilon_grad;
    grad_res[1] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[1] += epsilon_grad;
    p_minus[2] -= epsilon_grad;
    grad_res[2] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[2] += epsilon_grad;
}
#endif



template<typename TFunctorKernel>
auto GradientBasedIntegralSurfaceT<TFunctorKernel>::EvalGrad(const Point& point, Scalar epsilon_grad) const -> Vector
{
    //TODO:@todo : find closed-form gradient or pseudo-analytic
    Scalar value_res = Eval(point);

    Vector grad_res = Vector::Zero();
    Point p_minus = point;
    p_minus[0] -= epsilon_grad;
    grad_res[0] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[0] += epsilon_grad;
    p_minus[1] -= epsilon_grad;
    grad_res[1] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[1] += epsilon_grad;
    p_minus[2] -= epsilon_grad;
    grad_res[2] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[2] += epsilon_grad;
    return grad_res;
}

template<typename TFunctorKernel>
void GradientBasedIntegralSurfaceT<TFunctorKernel>::EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
    value_res = Eval(point);

    Point p_minus = point;
    p_minus[0] -= epsilon_grad;
    grad_res[0] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[0] += epsilon_grad;
    p_minus[1] -= epsilon_grad;
    grad_res[1] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[1] += epsilon_grad;
    p_minus[2] -= epsilon_grad;
    grad_res[2] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[2] += epsilon_grad;
}


// Two following function works as a NodeOperatorNAry<BlendSum> ...
template<typename TFunctorKernel>
void GradientBasedIntegralSurfaceT<TFunctorKernel>::PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                            typename std::map<BlobtreeNode*, typename std::pair<core::AABBox, core::AABBox> >& updated_areas)
{
    blend_sum_.PrepareForEval(*this, epsilon_bbox, accuracy_needed, updated_areas);
    this->UpdateNodeCompacity();
}

template<typename TFunctorKernel>
core::AABBox GradientBasedIntegralSurfaceT<TFunctorKernel>::GetAxisBoundingBox(Scalar value) const
{
    return blend_sum_.GetAxisBoundingBox(this->children(), value);
}



////////////////////////////////////////
/// OptGradientBasedIntegralSurfaceT ///
////////////////////////////////////////

template<typename TFunctorKernel>
auto OptGradientBasedIntegralSurfaceT<TFunctorKernel>::Eval(const Point& point) const -> Scalar
{
    Scalar f_scalis = 0.0;
    Vector grad_hornus = Vector::Zero(); // in fact this is Hornus only for skeleton of dimension 1

    Scalar f_tmp;
    Vector grad_tmp;

    f_bvh_segment_.EvalHomotheticValues(point, f_tmp, grad_tmp, this->kernel_);
    f_scalis += f_tmp;
    grad_hornus += grad_tmp;

    f_bvh_triangle_.EvalHomotheticValues(point, f_tmp, grad_tmp, this->num_kernel_);
    f_scalis += f_tmp;
    grad_hornus += grad_tmp;

    f_bvh_point_.EvalHomotheticValues(point, f_tmp, grad_tmp, this->kernel_);
    f_scalis += f_tmp;
    grad_hornus += grad_tmp;


    if(f_scalis>0.0) {
        const Scalar grad_norm_hornus = grad_hornus.norm();
        return this->FieldCorrection(f_scalis, grad_norm_hornus, this->proj_angle_, this->tan_special_);
    } else return 0.0;
}

template<typename TFunctorKernel>
void OptGradientBasedIntegralSurfaceT<TFunctorKernel>::EvalValueAndGradAndProperties(
                        const Point& point, Scalar epsilon_grad,
                        const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                        const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                        Scalar& value_res, Vector& grad_res,
                        std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                        std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                ) const
{   //TODO:@todo : this function is to be rewrited has soon as we find a way to compute closed-form gradient (or a good approximation)
    Scalar f_scalis = 0.0;
    Vector grad_hornus = Vector::Zero(); // in fact this is Hornus only for skeleton of dimension 1

    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
    {
        scal_prop_res[i] = 0.0;
    }
    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
    {
        vect_prop_res[i] = Vector::Zero();
    }

    Scalar f_tmp;
    Vector grad_tmp;

    f_bvh_segment_.EvalHomotheticValuesAndProperties(point, f_tmp, grad_tmp,
                                                     scal_prop_ids, vect_prop_ids,
                                                     scal_prop_res, vect_prop_res,
                                                     this->kernel_);
    f_scalis += f_tmp;
    grad_hornus += grad_tmp;

    f_bvh_triangle_.EvalHomotheticValuesAndProperties(point, f_tmp, grad_tmp,
                                                      scal_prop_ids, vect_prop_ids,
                                                      scal_prop_res, vect_prop_res,
                                                      this->num_kernel_);
    f_scalis += f_tmp;
    grad_hornus += grad_tmp;

    f_bvh_point_.EvalHomotheticValuesAndProperties(point, f_tmp, grad_tmp,
                                                   scal_prop_ids, vect_prop_ids,
                                                   scal_prop_res, vect_prop_res,
                                                   this->kernel_);
    f_scalis += f_tmp;
    grad_hornus += grad_tmp;

    grad_res = Vector::Zero();
    if(f_scalis > 0.0) {
        const Scalar grad_norm_hornus = grad_hornus.norm();

        value_res = this->FieldCorrection(f_scalis, grad_norm_hornus, this->proj_angle_, this->tan_special_);

        if(f_scalis != 0.0)
        {
            for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
            {
                scal_prop_res[i] = scal_prop_res[i]/f_scalis;
            }
            for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
            {
                vect_prop_res[i] = vect_prop_res[i]/f_scalis;
            }
        }
    } else return;


//    else
//    {
//        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
//        {
//            scal_prop_res[i] = Traits::kSFScalarPropertiesDefault[scal_prop_ids[i]];
//        }
//        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
//        {
//            vect_prop_res[i] = Traits::kSFVectorPropertiesDefault[vect_prop_ids[i]];
//        }
//    }

    //TODO:@todo : BEURK !!! cost a lot and is not precise ... to be changed :)
    Point p_minus = point;
    p_minus[0] -= epsilon_grad;
    grad_res[0] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[0] += epsilon_grad;
    p_minus[1] -= epsilon_grad;
    grad_res[1] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[1] += epsilon_grad;
    p_minus[2] -= epsilon_grad;
    grad_res[2] = (value_res - Eval(p_minus))/epsilon_grad;
    p_minus[2] += epsilon_grad;
}
    
} // Close namespace convol

} // Close namespace expressive



////////////////////////////////////////////////////////
/// Old code
////////////////////////////////////////////////////////

//template<typename TFunctorKernel>
//auto GradientBasedIntegralSurfaceT<TFunctorKernel>::FieldCorrection(Scalar f_scalis, Scalar grad_norm_hornus) const -> Scalar
//{
//    Scalar k = ((Scalar)(kernel_.i_));

//    // can probaly be computed with slightly less operations...
//    Scalar spe_x = pow(2.0 / f_scalis, 1.0 / (k-1.0) );
//    Scalar spe_x_i = pow(spe_x, k);
//    Scalar tmp = spe_x_i * grad_norm_hornus / ( 2.0 * (k-1.0));
//    Scalar tmp_sqr = 1.0-tmp*tmp;
//    if(tmp_sqr>0.0)
//    {
////        Scalar dist = 2.0 * spe_x * sqrt(tmp_sqr);
////        Scalar d_sqr = dist*dist*0.25 ;
//        Scalar d_sqr = spe_x * spe_x  * tmp_sqr;

//        Scalar t_sqr = this->tan_from_param_ * this->tan_from_param_;

//        // Calcul de la limite en fonction de la dir de projection (resolution equation de degre 3)
//        // (cas de tangence entre la courbe du cas special associe a  la valeur dist et la direction de projection)
//        Scalar a = (k+1.0)*(k+1.0) * d_sqr*d_sqr;
////        Scalar b = -2.0*d_sqr* (k+1.0) * k;
//        Scalar c = k*k + d_sqr * t_sqr;
//        Scalar e = - t_sqr;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////      std::vector<Scalar> solutions =  SolvingEquation4T<Scalar>::third_general(a,b,c,e);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        Scalar b_div_3a = -(2.0/3.0)*(k/(k+1.0))/d_sqr;

//        Scalar z;
//        if( this->proj_angle_ == 0.0 )
//        {
//            z = (k/(k+1.0)) / d_sqr;
//        }
//        else
//        {
//            Scalar sqr_b_div_3a = b_div_3a*b_div_3a;
//            Scalar p = (1.0/3.0) * c/a - sqr_b_div_3a;
//            Scalar q = 0.5*(e-b_div_3a*c)/a + b_div_3a*sqr_b_div_3a;

//            Scalar root_1, root_2;

//            Scalar delta = q*q + p*p*p;
//            // 1. 1 real solution
//            if(delta>0){
//                Scalar sqrt_delta = sqrt(delta);
//                root_1 = cube_root(-q+sqrt_delta) + cube_root(-q-sqrt_delta); // there is a new function for cubic_root : cbrt
//            }
//            // 2. 2 real solutions including 1 double
//            else if(delta==0){
//                assert(false); //root_1 = (3.0*q)/p; root_2 = (-3.0*q)/(2.0*p);
//            }
//            // 3. 3 real distinct solutions
//            else{
//                Scalar sqrt_p = sqrt(-p);
//                Scalar teta = acos(q/(p*sqrt_p));
//                root_1 = 2.0*sqrt_p * cos(teta/3.0);
//                //Scalar x2 = 2.0*sqrt(-p/3.0)*cos((teta+2.0*M_PI)/3.0);
//                root_2 = 2.0*sqrt_p * cos((teta+4.0*M_PI)/3.0);
//            }

//            if( this->proj_angle_ > 0.0 )
//                z = root_1 - b_div_3a;
//            else
//                z = root_2 - b_div_3a; // this is for negative angle of projection
//        }


//        Scalar x_limit = 2.0 * pow(z, (k-1.0) / 2.0);

//        if(x_limit<f_scalis)
//        {
//            // Constant correction value on the problematic interval that is equal to the correction at the boundary
//            Scalar y_limit = 2.0 * pow(z, k/2.0) * sqrt( 1.0 - d_sqr * z);

//            if(this->analytical_projection_)
//            {
//                return AnalyticalProjection(x_limit,y_limit);
//            }
//            else
//            {
//                Scalar relativ_horizontal2 = x_limit - pow(y_limit , (k-1.0)/k );
//                Scalar relativ_vertical2   = (k-1.0) * ( pow(x_limit, k/(k-1.0) ) - y_limit );
////                const Scalar correction2 = ( relativ_horizontal2*relativ_vertical2) / (relativ_vertical2 + relativ_horizontal2 * this->tan_from_param_);
//                const Scalar correction2 = this->cotan_from_param_bis_*(relativ_horizontal2*relativ_vertical2) / (relativ_vertical2 * this->cotan_from_param_bis_ + relativ_horizontal2);

//                if(x_limit < correction2) return 0.0; // this is done to handle negative anlge of projection !

//                return x_limit - correction2;
//            }
//        }
//    }

//    // the basic correction provide good result (no cavity problem) for this (field,gradient) value
//    if(this->analytical_projection_)
//    {
//        return AnalyticalProjection(f_scalis,grad_norm_hornus);
//    }
//    else
//    {
//        Scalar relativ_horizontal = f_scalis - pow( grad_norm_hornus/(k-1.0), (k-1.0)/k );
//        Scalar relativ_vertical   = (k-1.0) * pow( f_scalis, k/(k-1.0) ) - grad_norm_hornus;

////        const Scalar correction = ( relativ_horizontal*relativ_vertical) / (relativ_vertical + relativ_horizontal * this->tan_from_param_);
//        const Scalar correction = this->cotan_from_param_bis_*(relativ_horizontal*relativ_vertical) / (relativ_vertical * this->cotan_from_param_bis_ + relativ_horizontal);

//        if(f_scalis < correction) return 0.0; // this is done to handle negative anlge of projection !

//        return f_scalis - correction;
//    }

////////////////////////////////////////////////////////////////////
////    // Correction by projection in a fixed direction
////    const Scalar correction = ( relativ_horizontal*relativ_vertical) / (relativ_vertical + relativ_horizontal * this->tan_from_param_);
////    return f_scalis - correction;
////////////////////////////////////////////////////////////////////
////    // Correction by projecting to the closest point
////    const Scalar correction = ( relativ_horizontal*relativ_vertical*relativ_vertical) / (relativ_vertical*relativ_vertical + relativ_horizontal*relativ_horizontal);
////    return f_scalis - correction;
////////////////////////////////////////////////////////////////////
//}
