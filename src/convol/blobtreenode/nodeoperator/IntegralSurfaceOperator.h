/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: IntegralSurfaceOperator.h
 
 Language: C++
 
 License: expressive license
 
 \author: Cedric Zanni
 E-Mail:  cedric.zanni@inrialpes.fr
 
 Description: Class from which NodeOperator that represent a IntegralSurfaces blending
              should derive. It contains an accessor to the kernel to be used for integration (which
              should be a homothetic kernel) and an AbstractIntegralSurfaceCorrector pointer.

 Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_INTEGRAL_SURFACE_OPERATOR_T_H_
#define CONVOL_INTEGRAL_SURFACE_OPERATOR_T_H_

// core dependencies
#include <core/CoreRequired.h>
    #include <core/functor/FunctorFactory.h>

// convol dependencies
#include <convol/functors/kernels/Kernel.h>
#include <convol/skeleton/Skeleton.h>
    #include <convol/blobtreenode/nodeoperator/IntegralSurfaceCorrector.h>
    #include <convol/factories/ISCorrectorFactory.h>

namespace expressive {

namespace convol {


class CONVOL_API AbstractIntegralSurfaceOperator
{
public:
    
//    EXPRESSIVE_MACRO_NAME("IntegralSurfaceOperator") // This macro defined functions

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractIntegralSurfaceOperator()
        : bt_node_(nullptr), graph_(nullptr), is_corrector_(nullptr) {}
    //-------------------------------------------------------------------------
    virtual ~AbstractIntegralSurfaceOperator() {}

    /** @brief Until this function is used, the class is badly initialized
     * TODO@todo : bt_node should be the pointer to the child class => ugly
     */
    virtual void Init(BlobtreeNode* bt_node, Skeleton* graph)
    {
        bt_node_ = bt_node;
        graph_ = graph;
        if(is_corrector_) is_corrector_->Init(graph_);
    }

    core::AABBox PrepareForEval(Scalar epsilon_bbox, Scalar /*accuracy_needed*/)
    {
        if(is_corrector_)
            return is_corrector_->PrepareForEval(epsilon_bbox);
        else
            return core::AABBox(); //empty bounding box
    }

    /////////////////
    /// Accessors ///
    /////////////////

    virtual const Kernel& kernel_functor() const = 0;

    std::shared_ptr<AbstractIntegralSurfaceCorrector> is_corrector() {return is_corrector_;}

    /////////////////
    /// Modifiers ///
    /////////////////

    virtual void AddCorrector() = 0;
    void RemoveCorrector() { is_corrector_ = nullptr; }

protected:
    // Deep copy constructor :
    // It should only be called by the protected subclass copy constructors
    // which should only be called by the "clone" function
    // listener_set is not copyed
    AbstractIntegralSurfaceOperator(const AbstractIntegralSurfaceOperator &rhs)
        : bt_node_(rhs.bt_node_), graph_(rhs.graph_), is_corrector_(nullptr)
    {
        if(rhs.is_corrector_ != nullptr)
            is_corrector_ = rhs.is_corrector_->clone();
    }

    /////////////////////
    /// Xml functions ///
    /////////////////////

    //-------------------------------------------------------------------------
    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
//        if(is_corrector_) is_corrector_->AddXmlAttributes(element);
        if(is_corrector_) element->SetAttribute("is_corrector", 1);
        else element->SetAttribute("is_corrector", 0);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &/*ids*/) const
    {
//        if(is_corrector_) is_corrector_->AddXmlAttributes(element);
        if(is_corrector_) element->SetAttribute("is_corrector", 1);
        else element->SetAttribute("is_corrector", 0);
    }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    BlobtreeNode* bt_node_; // required for correctors
    expressive::core::WeightedGraph* graph_; // required for correctors

    std::shared_ptr<AbstractIntegralSurfaceCorrector> is_corrector_;
};


template<typename TFunctorKernel>
class IntegralSurfaceOperatorT : public AbstractIntegralSurfaceOperator
{
public:
    

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    IntegralSurfaceOperatorT(const TFunctorKernel& kernel)
        : AbstractIntegralSurfaceOperator(), kernel_(kernel)    {}
    //-------------------------------------------------------------------------
    virtual ~IntegralSurfaceOperatorT() {}

    /////////////////////
    /// Xml functions ///
    /////////////////////

    //-------------------------------------------------------------------------
    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        AbstractIntegralSurfaceOperator::AddXmlAttributes(element);

        TiXmlElement * element_kernel = new TiXmlElement( "TFunctorKernel" );
        element->LinkEndChild(element_kernel);

        TiXmlElement * element_child = kernel_.ToXml();
        element_kernel->LinkEndChild(element_child);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        AbstractIntegralSurfaceOperator::AddXmlAttributes(element, ids);

        TiXmlElement * element_kernel = new TiXmlElement( "TFunctorKernel" );
        element->LinkEndChild(element_kernel);

        TiXmlElement * element_child = kernel_.ToXml();
        element_kernel->LinkEndChild(element_child);
    }

    /////////////////
    /// Accessors ///
    /////////////////

    const Kernel& kernel_functor() const { return kernel_; }
    const TFunctorKernel kernel() const { return kernel_; }

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_kernel(TFunctorKernel kernel)
    {
        assert(false); //TODO:@todo : we should update all the subskeleton accordingly
        kernel_ = kernel;
//        num_kernel_ = SemiNumericalHomotheticConvolT<TFunctorKernel>(10, kernel);
//        //TODO:@todo : warning the 10 should be a parameter of the node !!!
    }

    void AddCorrector()
    {
        is_corrector_ = ISCorrectorFactory::GetInstance().CreateNewCorrector(kernel_,bt_node_);

        if(graph_) is_corrector_->Init(graph_);
    }

protected:
    // Deep copy constructor :
    // It should only be called by the protected subclass copy constructors
    // which should only be called by the "clone" function
    // listener_set is not copyed
    IntegralSurfaceOperatorT(const IntegralSurfaceOperatorT &rhs)
        : AbstractIntegralSurfaceOperator(rhs), kernel_(rhs.kernel_) {}

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    TFunctorKernel kernel_;
};

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_INTEGRAL_SURFACE_OPERATOR_T_H_
