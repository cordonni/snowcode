/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: NodeSumOptimizedT.h
 
 Language: C++
 
 License: Convol license
 
 \author: Cedric Zanni
 E-Mail:  cedric.zanni@inrialpes.fr
 
 Description:
    TODO:@todo : description to be added as soon as it work as expected

 Platform Dependencies: None

*/

#pragma once
#ifndef CONVOL_NODE_SUM_OPTIMIZED_H_
#define CONVOL_NODE_SUM_OPTIMIZED_H_

// core dependencies
#include <core/CoreRequired.h>

// convol dependencies
#include<convol/ScalarField.h>
#include<convol/primitives/NodeSegmentSFieldT.h>
    // BlobtreeNode
    #include<convol/blobtreenode/BlobtreeNode.h>
    // Functors
    #include<core/functor/Functor.h>
//    #include<convol/include/functors/EnumFunctorType.h>

#include <convol/tools/FittedBVH.h>

#include <convol/blobtreenode/NodeScalarField.h>

/////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : still plenty things to be cleaned
#include <chrono>
/////////////////////////////////////////////////////////////////////////////////////////

#define USE_FBVH

namespace expressive {
namespace convol {

template <typename TFunctorKernel>
class NodeOptimizedSumT :
        public BlobtreeNode
{
public:
    EXPRESSIVE_MACRO_NAME("NodeOptimizedSum") // This macro defined functions
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    typedef FittedBVH<core::OptWeightedSegment>::CDataIterator CDataIterator;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    NodeOptimizedSumT(unsigned int k)
        : f_bvh_(k)
    {
        this->parent_ = nullptr;
        this->arity_ = 0;
    }
    NodeOptimizedSumT(const BlobtreeNode& bt_node, unsigned int k, TFunctorKernel kernel)
//    NodeOptimizedSumT(const typename std::vector<std::shared_ptr<BlobtreeNode> > &bt_node_vec, TFunctorKernel kernel)
        : BlobtreeNode(StaticName()), f_bvh_(k), kernel_(kernel)
    {
        this->parent_ = nullptr;
        this->arity_ = 0;

#ifdef USE_FBVH
        std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
        start_time = std::chrono::high_resolution_clock::now();

        // Compute the number of primitive (of DataType kind)
        unsigned int nb_primitive = bt_node.nb_children(); //TODO:@todo : for now this is the number of OptWeightedSegment contained in the given blobtreenode (and its children)
        // for now, assume there is only NodeSegmentSField as children

        // Initialize the vector that will contains the nodes
        f_bvh_.data().reserve(nb_primitive);

        //TODO:@todo : pour le moment, seulement prise en compte d'un noeud avec uniquement des NodeSegmentSField comme child !!!!
        // et donc DataType = OptWeightedSegment
        unsigned int id = 0;
        for(auto &child : bt_node.children()) {
            auto node_segment = std::dynamic_pointer_cast<AbstractNodeSegmentSField>(child);
            if(node_segment != nullptr) {
                auto opt_w_seg = node_segment->opt_w_seg();
                f_bvh_.data().push_back( std::make_pair(node_segment->axis_bounding_box(), opt_w_seg) );
                f_bvh_.barycenter().push_back(  opt_w_seg.Barycenter() );
                ++id;
            }
            else
            {
                assert(false); //TODO:@todo : remove later
            }
        }

        f_bvh_.Init(bt_node.axis_bounding_box());

        end_time =  std::chrono::high_resolution_clock::now();
        ork::Logger::DEBUG_LOGGER->logf("POLYGONIZER", "Fitted FBVH construction time : %f", std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000.0);

#endif
//#else // for now keep following in both cases since I do not want to change GetPoint and co
        vec_seg_.reserve(bt_node.nb_children());
        for(auto &node : bt_node.children())
        {
            std::shared_ptr<AbstractNodeSegmentSField> node_seg = std::dynamic_pointer_cast<AbstractNodeSegmentSField> (node);
            AABBox bbox = node->axis_bounding_box();
            //TODO:@todo : faire la suite avec des move & co
            core::OptWeightedSegment seg(*node_seg);
            Properties1D prop( node_seg->properties() );
            vec_seg_.push_back(std::make_tuple(bbox,seg,prop));
        }
//#endif
        this->axis_bounding_box_ = bt_node.axis_bounding_box();
    }

    ~NodeOptimizedSumT(){}

    virtual std::shared_ptr<ScalarField> CloneForEval() const
    {
        assert(false);
        return nullptr;
    }

    //////////////////////////
    // Evaluation functions //
    //////////////////////////
    
    virtual Scalar Eval(const Point &point) const;
    virtual Vector EvalGrad(const Point& point, Scalar epsilon) const;
    virtual void EvalValueAndGrad(const Point& point,
                                  Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                               const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                               const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                               Scalar& value_res, Vector& grad_res,
                                               std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               ) const;

    virtual void PrepareForEval(Scalar /*epsilon_bbox*/, Scalar /*accuracy_needed*/,
                                std::map<BlobtreeNode*, std::pair<AABBox, AABBox> >& /*updated_areas*/)
    {
        assert(false);
        /// TODO:@todo : for now this kind of node shoudl only be created by a CloneForEval function
        /// called upon a Blobtree that is already "PreparedForEval"
        //        blend_operator_.PrepareForEval(*this, epsilon_bbox, accuracy_needed, updated_areas);
        //        this->UpdateNodeCompacity();
    }

    //TODO:@todo : a remplacer (sans doute pas optimal & pas valable pour tout les mélanges !! (notament soustraction))
    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const;
    virtual void GetPrimitivesPointsAt(Scalar value, Scalar epsilon, core::PointCloud& res_points, const AABBox& b_box) const;

    /////////////////////////////
    // BoundingBox computation //
    /////////////////////////////

//    virtual AABBox GetAxisBoundingBox(Scalar value) const;
    virtual AABBox GetAxisBoundingBox(Scalar value) const
    {
        assert(false); // same reason as for PrepareForEval
        return AABBox();
    }

    virtual void GetPrecisionAxisBoundingBoxes(Scalar value, std::list<core::PrecisionAxisBoundingBox> &pboxes) const
    {
        for(auto &seg_info : vec_seg_)
            std::get<1>(seg_info).GetPrecisionAxisBoundingBoxes(value, pboxes);
    }

    ///////////////
    // Accessors //
    ///////////////

protected :
    FittedBVH<core::OptWeightedSegment> f_bvh_;

    std::vector<std::tuple<AABBox, core::OptWeightedSegment, Properties1D> > vec_seg_;


private:
    TFunctorKernel kernel_;
};

} // Close namespace convol

} // Close namespace expressive

#include <convol/blobtreenode/nodeoperator/NodeSumOptimized.hpp>

#endif // CONVOL_NODE_SUM_OPTIMIZED_H_
