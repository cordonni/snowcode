
namespace expressive {

namespace convol {


template< typename THomotheticKernel, typename TStandardKernel>
Scalar IntegralSurfaceCorrector<THomotheticKernel,TStandardKernel>::Eval(const Point& point) const
{
    Scalar res = 0.0;
    for(auto &node : point_corrector_) {
        if(node.second.first.Contains(point)) {
            res += homothetic_kernel_.Eval(node.second.second, point);
        }
    }

    for(auto &node : segment_corrector_) {
        if(node.second.first.Contains(point)) {
            res += homothetic_kernel_.Eval(node.second.second, point);
        }
    }
    return res;
}
template< typename THomotheticKernel, typename TStandardKernel>
Vector IntegralSurfaceCorrector<THomotheticKernel,TStandardKernel>::EvalGrad(const Point& point, Scalar epsilon_grad) const
{
    Vector grad_res = Vector::Zero();
    for(auto &node : point_corrector_) {
        if(node.second.first.Contains(point)) {
            grad_res += homothetic_kernel_.EvalGrad(node.second.second, point, epsilon_grad);
        }
    }
    Vector grad_tmp; Scalar f_tmp;
    for(auto &node : segment_corrector_) {
        if(node.second.first.Contains(point)) {
            homothetic_kernel_.EvalValueAndGrad(node.second.second, point, epsilon_grad, f_tmp, grad_tmp);
            grad_res += grad_tmp;
        }
    }
    return grad_res;
}
template< typename THomotheticKernel, typename TStandardKernel>
void IntegralSurfaceCorrector<THomotheticKernel,TStandardKernel>::EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();
    Scalar f_tmp;
    Vector grad_tmp;
    for(auto &node : point_corrector_) {
        if(node.second.first.Contains(point)) {
            homothetic_kernel_.EvalValueAndGrad(node.second.second, point, epsilon_grad, f_tmp, grad_tmp);
            value_res += f_tmp;
            grad_res += grad_tmp;
        }
    }

    for(auto &node : segment_corrector_) {
        if(node.second.first.Contains(point)) {
            homothetic_kernel_.EvalValueAndGrad(node.second.second, point, epsilon_grad, f_tmp, grad_tmp);
            value_res += f_tmp;
            grad_res += grad_tmp;
        }
    }
}


template< typename THomotheticKernel, typename TStandardKernel>
Scalar IntegralSurfaceCorrector<THomotheticKernel,TStandardKernel>::ComputeSegmentCorrectorLengthForUnitWeight()
{
///TODO:@todo : this computation should be replaced by closed-form expression in the different kernel useable

    core::OptWeightedSegment half_infinite_seg(WeightedPoint(0.0,0.0,0.0,1.0), WeightedPoint(-1000.0, 0.0, 0.0,1.0));

    Scalar min_pos = -0.5;  // on ne traite pas bien les segment n'englobant pas les extrémité dans des conditions raisonnable
    Scalar curr_pos = 1.0;
    Scalar max_pos = 1.0;

    Scalar field_value = homothetic_kernel_.Eval(half_infinite_seg, Point(curr_pos,0.0,0.0));
    Scalar error_sqr = (field_value - 1.0)*(field_value - 1.0);
    while(error_sqr > 1.0e-8)
    {
        if(field_value>1.0)
        {
            min_pos = curr_pos;
            curr_pos = 0.5*(min_pos+max_pos);
        }
        else
        {
            max_pos = curr_pos;
            curr_pos = 0.5*(min_pos+max_pos);
        }

        field_value = homothetic_kernel_.Eval(half_infinite_seg, Point(curr_pos,0.0,0.0));
        error_sqr = (field_value - 1.0)*(field_value - 1.0);
    }

    segment_length_corrector_ = 1.0 - curr_pos;
    return segment_length_corrector_;
}

template< typename THomotheticKernel, typename TStandardKernel>
core::AABBox IntegralSurfaceCorrector<THomotheticKernel,TStandardKernel>::CreateCorrectors(
        core::VertexId v_id, Scalar epsilon_bbox)
{
///TODO:@todo : this is a first implementation, could be slightly improve & optimized ...
/// for instance creating vector directly with the right size, taking into account the kind of primitives ...

    // Creation of the normalized version of the primitive associated to the vertex v_id
    // based on "semi-infinite" primitives leaving from the vertex in a single direction

    auto vertex = this->graph_->Get(v_id);

    core::WeightedPoint w_point = vertex->pos();

    Point position = w_point;
    Scalar weight  = w_point.weight();

    auto neightbor_vertices = vertex->GetAdjacentVertices();

    int n_adj_vertices = 0;
    std::vector<core::OptWeightedSegment> segments;
    std::vector<Scalar> delta_w_tab;
    std::vector<Vector> vertex_dir_tab;

    Scalar min_abs_delta = 1000000.0;

    WeightedPoint central_w_point(Point(0.0,0.0,0.0), weight);

    core::WeightedPoint w_point_tmp;
    while(neightbor_vertices->HasNext())
    {
        w_point_tmp = neightbor_vertices->Next()->pos();

        Vector dir = w_point_tmp - position;
        Scalar length = dir.norm();
        vertex_dir_tab.push_back(dir/length);

        Scalar delta_w = (w_point_tmp.weight() - weight) / length;
        delta_w_tab.push_back(delta_w);

        Scalar abs_delta_w = ( delta_w>0.0 ) ? delta_w : -delta_w;
        min_abs_delta = (min_abs_delta<abs_delta_w) ? min_abs_delta : abs_delta_w;

        if(abs_delta_w < 1.0e-6) {// arbitrary threshold
            // segment with constant weight
            segments.push_back(core::OptWeightedSegment(central_w_point, WeightedPoint(Point(1000.0,0.0,0.0), weight)));
            // 1000.0 because ??? -> should be changed in a function of weight (or better add the computation for half infinite line)
        } else {
            if(delta_w>0.0) {
                segments.push_back(core::OptWeightedSegment(central_w_point, WeightedPoint(Point(1000.0, 0.0, 0.0), weight+ 1000.0*delta_w)));
            } else {
                segments.push_back(core::OptWeightedSegment(central_w_point, WeightedPoint(Point(weight/abs_delta_w,0.0,0.0),1.0e-8)));
                // 1.0e-8 because I don't want a true zero
            }
        }

        ++n_adj_vertices;
    }


    ///TODO:@todo : following code should be the responsability from the node operator
    /// since the kind of blending impact on the required correction

    // Compute the field at the special location to obtain the minimal missing field value

    Point point_ortho = Point(0.0,0.0,weight);
    Scalar segments_field_ortho = 0.0;
    for(auto &seg : segments) {
        segments_field_ortho += homothetic_kernel_.Eval(seg, point_ortho);
    }

    // Compute the weight to be used for the correctors and add them
    // if needed in point_correctors_ and segment_correctors_

    Scalar point_density = 1.0 - segments_field_ortho; // weight of point corrector if used alone

    core::AABBox aabb = core::AABBox(); // Bounding boxes of corrections

    if(n_adj_vertices==1) {
        Scalar segment_length = weight * segment_length_corrector_; // length of segment corrector if no point is used

        // Interpolate between correction
        //      delta_weight = 0.0 => use segment alone
        //      delta_weight > 1.0 => use point alone

        Scalar abs_delta = (delta_w_tab[0]>0.0) ? delta_w_tab[0] : -delta_w_tab[0];

        Scalar segment_density = 0.0;
        if(delta_w_tab[0] < 0.0) {
            if(abs_delta<0.5) { // otherwise it is a clearly defined maxima and only point corrector used
                Scalar coeff = (1.0-2.0*abs_delta)*(1.0-2.0*abs_delta);
                point_density *= (1.0 - coeff);
                segment_density = coeff;
            }
        } else {
            // We will use only segment if required
            point_density = 0.0;

            if(abs_delta>2.0) {
                segment_density = 0.0;
            } else {
                segment_density = 1.0;

                // we modify the length of the segment, not its weight
                Scalar sqr_abs_delta = 1.0-0.25*abs_delta*abs_delta;
                Scalar coeff = sqr_abs_delta*sqr_abs_delta;
                segment_length *= coeff;
                if(segment_length < weight * 0.02) {
                    //done to prevent segment to be added to corrector if too short to be useful
                    segment_density = 0.0;
                }
            }
        }

        // Add corrector to associated blobtree node if needed
        if(point_density > 0.05) //arbitrary threshold
        {
            //TODO:@todo : this is basically the same code as in NodePointSField : I do not like this kind of recopy...
            core::AABBox tmp_aabb = w_point.GetAxisBoundingBox();
            Scalar dist_bound = homothetic_kernel_.GetEuclidDistBound(w_point, epsilon_bbox);
            tmp_aabb.min_.array() -= Point::Constant(dist_bound);
            tmp_aabb.max_.array() += Point::Constant(dist_bound);

            point_corrector_.insert( std::make_pair(v_id, std::make_pair(tmp_aabb, core::WeightedPointWithDensity(w_point, point_density)) ));
            aabb.Enlarge(tmp_aabb);
        }
        if(segment_density > 0.05)
        {
            core::CsteWeightedSegmentWithDensity seg_corrector(w_point, -vertex_dir_tab[0], segment_length, segment_density);
//            SegmentCorrector seg_corrector(w_point, WeightedPoint(position - segment_length*vertex_dir_tab[0], weight), segment_density);

            core::AABBox tmp_aabb = seg_corrector.GetAxisBoundingBox();
            Scalar dist_bound = homothetic_kernel_.GetEuclidDistBound(w_point, epsilon_bbox); //TODO:@todo ??? should not use the one of the point
            tmp_aabb.min_.array() -= Point::Constant(dist_bound);
            tmp_aabb.max_.array() += Point::Constant(dist_bound);

            segment_corrector_.insert(std::make_pair(v_id,std::make_pair(tmp_aabb,seg_corrector)));
            aabb.Enlarge(tmp_aabb);
        }
    }
    else if(n_adj_vertices>1) {
        // Add corrector to associated blobtree node if needed
        if(point_density > 0.05) { //TODO:@todo : arbitrary limit value !
            //TODO:@todo : this is basically the same code as in NodePointSField : I do not like this kind of recopy...
            core::AABBox tmp_aabb = w_point.GetAxisBoundingBox();
            Scalar dist_bound = homothetic_kernel_.GetEuclidDistBound(w_point, epsilon_bbox);
            tmp_aabb.min_.array() -= Point::Constant(dist_bound);
            tmp_aabb.max_.array() += Point::Constant(dist_bound);

            point_corrector_.insert( std::make_pair(v_id, std::make_pair(tmp_aabb, core::WeightedPointWithDensity(w_point, point_density)) ));
            aabb.Enlarge(tmp_aabb);
        }
    }
    return aabb;
}



///////////////////////////////////
/// OptIntegralSurfaceCorrector ///
///////////////////////////////////



template< typename THomotheticKernel, typename TStandardKernel>
Scalar OptIntegralSurfaceCorrectorT<THomotheticKernel,TStandardKernel>::Eval(const Point& point) const
{
    Scalar res = 0.0;
    res += f_bvh_point_.Eval(point, this->homothetic_kernel_);
    res += f_bvh_segment_.Eval(point, this->homothetic_kernel_);
    return res;
}
template< typename THomotheticKernel, typename TStandardKernel>
Vector OptIntegralSurfaceCorrectorT<THomotheticKernel,TStandardKernel>::EvalGrad(const Point& point, Scalar epsilon_grad) const
{
    Vector grad_res = Vector::Zero();

    grad_res += f_bvh_point_.EvalGrad(point, epsilon_grad, this->homothetic_kernel_);

    Scalar f_tmp; Vector grad_tmp;
    f_bvh_segment_.EvalValueAndGrad(point, epsilon_grad, f_tmp, grad_tmp, this->homothetic_kernel_);
    grad_res += grad_tmp;

    return grad_res;
}
template< typename THomotheticKernel, typename TStandardKernel>
void OptIntegralSurfaceCorrectorT<THomotheticKernel,TStandardKernel>::EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();

    Scalar f_tmp; Vector grad_tmp;

    f_bvh_point_.EvalValueAndGrad(point, epsilon_grad, f_tmp, grad_tmp, this->homothetic_kernel_);
    value_res += f_tmp;
    grad_res += grad_tmp;

    f_bvh_segment_.EvalValueAndGrad(point, epsilon_grad, f_tmp, grad_tmp, this->homothetic_kernel_);
    value_res += f_tmp;
    grad_res += grad_tmp;
}



} // Close namespace convol

} // Close namespace expressive

