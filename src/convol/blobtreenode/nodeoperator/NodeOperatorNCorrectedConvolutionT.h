/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: NodeOperatorNCorrectedConvolutionT.h

   Language: C++

   License: Convol Licence

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Header file for blending node doing convolution on a skeleton, "convolution" on points are blended with the classic convolution
                in order to insure maximal thickness. The points are managed by the skeleton.

                WARNING :   all children must be homothetic segment with a HomotheticCompactPolynomial6 kernel (with scale_ parameter equal to 2.0)
                NB : more than a blending node we can see this NodeOperator as a new kind of primitive !

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_NODE_CORRECTED_CONVOLUTION_H_
#define CONVOL_NODE_CORRECTED_CONVOLUTION_H_

// core dependencies
#include <core/CoreRequired.h>

// convol dependencies
#include<convol/primitives/NodePointSFieldT.h>
#include<convol/primitives/NodeSegmentSFieldT.h>
    // blobtreenode
    #include <convol/blobtreenode/NodeOperatorT.h>
    #include <convol/blobtreenode/NodeScalarField.h>
    // functors
    #include <convol/functors/operators/BlendSum.h> // for convenience
    #include <convol/functors/operators/BlendSumOptimized.h>


//// Convol dependencies
//    // ScalarFields
//    #include<Convol/include/ScalarFields/ScalarFieldT.h>
//    #include<Convol/include/ScalarFields/BlobtreeNode/BlobtreeNodeT.h>
//    // Functors
//    #include<Convol/include/functors/EnumFunctorType.h>
//    #include<Convol/include/geometry/PolyWeightedSegmentT.h>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/Xml/tinyxml/tinyxml.h>

//#include<stdio.h>

//// std dependencies
//#include <vector>
//#include <list>
//#include <string>
//#include <sstream>
//#include <set>

namespace expressive {

namespace convol {

// WARNING : This node contrary to usual n-ary nodes, will not destroy himself when the tree correction is called.
//           In other words, it can be empty and still be in the tree (That's not that much of a problem, since it should still evaluate fine).
template<typename TKernel>
class NodeOperatorNCorrectedConvolutionT :
    public NodeOperator
{
public:
    typedef NodeSegmentSFieldT<TKernel> SegmentSField;
    typedef NodePointSFieldT<TKernel> PointSField;

//    typedef BlendSumOptimizedT<Traits> BlendSumOptimized;

    EXPRESSIVE_MACRO_NAME("NodeCorrectedConvolution")
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    NodeOperatorNCorrectedConvolutionT()
    {
        this->parent_ = NULL;
        this->arity_ = -1;

        kernel_radius_factor_ = 2.0;

        // Compute one and for all the distance between a segment end point and the surface.
        // segment semi infini de poids constant
        SegmentSField seg_test = SegmentSField(Point(0.0,0.0,0.0), Point(-1000.0, 0.0, 0.0), 1.0, 1.0,
                                              TKernel(kernel_radius_factor_));

        Scalar min_pos = -0.5;  // on ne traite pas bien les segment n'englobant pas les extrémité dans des conditions raisonnable
        Scalar curr_pos = 1.0;
        Scalar max_pos = 1.0;

        Scalar field_value = seg_test.Eval(Point(curr_pos,0.0,0.0));
        Scalar error_sqr = (field_value - 1.0)*(field_value - 1.0);
        while(error_sqr > 1.0e-8)
        {
            if(field_value>1.0)
            {
                min_pos = curr_pos;
                curr_pos = 0.5*(min_pos+max_pos);
            }
            else
            {
                max_pos = curr_pos;
                curr_pos = 0.5*(min_pos+max_pos);
            }

            field_value = seg_test.Eval(Point(curr_pos,0.0,0.0));
            error_sqr = (field_value - 1.0)*(field_value - 1.0);
        }

        dist_from_unit_seg_to_iso_surface_ = curr_pos;

        //        blend_barthe_point_ = BlendBartheT<Traits>(1.0, 0.1, 0.1);
        //        blend_barthe_point_segment_ = BlendBartheT<Traits>(1.0, 0.40, 0.0);
    }
    //-------------------------------------------------------------------------
    virtual ~NodeOperatorNCorrectedConvolutionT()
    {
    }

    virtual NodeOperatorNCorrectedConvolutionT *clone() const;

//    virtual std::string ParameterName() const;

//    ////////////////////
//    // Type functions //
//    ////////////////////
//    virtual ENodeOperatorType Type() const { return E_NodeOperatorNCorrectedConvolution; }
    
//    ///////////////////
//    // Xml functions //
//    ///////////////////
//    virtual TiXmlElement * GetXml() const;
//    //-------------------------------------------------------------------------
//    // Same as above but only Get xml of this node, without recursively finding its children.
//    TiXmlElement * GetXmlNoChildren() const;
//    //-------------------------------------------------------------------------
//    void AddXmlSimpleAttribute(TiXmlElement * element) const;
//    //-------------------------------------------------------------------------
//    void AddXmlComplexAttribute(TiXmlElement * element) const;


    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////
    virtual Scalar Eval(const Point& point) const;
    //-------------------------------------------------------------------------
    virtual Vector EvalGrad(const Point& point, Scalar epsilon) const;
    //-------------------------------------------------------------------------
    virtual void EvalProperties(const Point& point,
                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                ) const;
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;
    //-------------------------------------------------------------------------
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                    const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                    const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                    Scalar& value_res, Vector& grad_res,
                                                    std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                    std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                    ) const;
                                                    


    virtual void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                                typename std::map<BlobtreeNode*, typename std::pair<core::AABBox, core::AABBox> >& updated_areas);

    //-------------------------------------------------------------------------
    virtual core::AABBox GetAxisBoundingBox(Scalar value) const;

    Point GetAPointAt(Scalar value, Scalar epsilon) const;

    virtual void AddChild(std::shared_ptr<BlobtreeNode> bt_node);


    void RemovePointCorrector(NodeScalarField* point_sfield)
    {
        point_corrector_.erase(point_sfield);
    }
    void AddPointCorrector(NodeScalarField* point_sfield)
    {
        point_corrector_.insert(point_sfield);
    }

    void RemoveSegmentCorrector(NodeScalarField* segment_sfield)
    {
        segment_corrector_.erase(segment_sfield);
    }
    void AddSegmentCorrector(NodeScalarField* segment_sfield)
    {
        segment_corrector_.insert(segment_sfield);
    }


    Scalar GetDistFromUnitSegToIsoSurface()
    {
        return dist_from_unit_seg_to_iso_surface_;
    }

    /////////////////
    /// Accessors ///
    /////////////////

    Scalar kernel_radius_factor(){ return kernel_radius_factor_; }

    /////////////////
    /// Modifiers ///
    /////////////////

    // Those type of nodes are managed by SkelVertexHandleCoorectorT.
    // Therefore this function is overwritten not to delete the node.
    virtual void RecursiveNodeCorrection()
    {
        if(this->children_.size() == 0)
        {
            BlobtreeNode* parent = this->parent_;
            parent->RecursiveNodeCorrection();
        }
    }
        
protected:
        NodeOperatorNCorrectedConvolutionT(const NodeOperatorNCorrectedConvolutionT &rhs) :
            NodeOperator(rhs), dist_from_unit_seg_to_iso_surface_(rhs.dist_from_unit_seg_to_iso_surface_),
            kernel_radius_factor_(rhs.kernel_radius_factor_)
        {
            for (typename std::set<NodeScalarField*>::const_iterator it = rhs.point_corrector_.begin();
                 it != rhs.point_corrector_.end(); ++it)
                point_corrector_.insert((*it)->CloneForEval());

            for (typename std::set<NodeScalarField*>::const_iterator it = rhs.segment_corrector_.begin();
                 it != rhs.segment_corrector_.end(); ++it)
                segment_corrector_.insert((*it)->CloneForEval());
        }

protected:

/*! NB : commentaire pas à jour
 * Vector containing the geometric primitive corresponding to point & segment corrector.
 */
std::set<NodeScalarField*> point_corrector_;
std::set<NodeScalarField*> segment_corrector_;

Scalar dist_from_unit_seg_to_iso_surface_;

Scalar kernel_radius_factor_;

BlendSum blend_sum_;

// Optimized operator (can't handle more than one vector of scalar_field (indeed some stuff are done during prepare for eval))
BlendSumOptimized blend_sum_main_seg_;
BlendSumOptimized blend_sum_corrector_seg_;
BlendSumOptimized blend_sum_corrector_point_;
};

/////////////////////
//// Xml functions //
/////////////////////
//template<typename TKernel>
//TiXmlElement * NodeOperatorNCorrectedConvolutionT<TKernel>::GetXml() const
//{
//    TiXmlElement *element_base = new TiXmlElement( StaticName() );

//    AddXmlSimpleAttribute(element_base);
//    AddXmlComplexAttribute(element_base);
//    Base::AddXmlChildren(element_base);

//    return element_base;
//}
////-------------------------------------------------------------------------
//template<typename TKernel>
//TiXmlElement * NodeOperatorNCorrectedConvolutionT<TKernel>::GetXmlNoChildren() const
//{
//    TiXmlElement *element_base = new TiXmlElement( StaticName() );

//    AddXmlSimpleAttribute(element_base);
//    AddXmlComplexAttribute(element_base);

//    return element_base;
//}
////-------------------------------------------------------------------------
//template<typename TKernel>
//void NodeOperatorNCorrectedConvolutionT<TKernel>::AddXmlSimpleAttribute(TiXmlElement * element) const
//{
//    // Common ScalarField Complex attributes
//    Base::AddXmlSimpleAttribute(element);
//}
////-------------------------------------------------------------------------
//template<typename TKernel>
//void NodeOperatorNCorrectedConvolutionT<TKernel>::AddXmlComplexAttribute(TiXmlElement * element) const
//{
//    // Common ScalarField Complex attributes
//    Base::AddXmlComplexAttribute(element);
//}
////-----------------------------------------------------------------------------
//template<typename TKernel>
//std::string NodeOperatorNCorrectedConvolutionT<TKernel>::ParameterName() const
//{
//    std::ostringstream name;

//    name.setf(std::ios::scientific,std::ios::floatfield);

//    name.precision(2);

//    name << "[arity(N)]";
////TODO : @todo : ....

//    return name.str();
//}


////////////////////////////
/// Evaluation functions ///
////////////////////////////

template<typename TKernel>
typename NodeOperatorNCorrectedConvolutionT<TKernel>::Scalar NodeOperatorNCorrectedConvolutionT<TKernel>::Eval(const Point& point) const
{
        // Eval correction from point
        Scalar res_point_corrector = blend_sum_corrector_point_.Eval(point); // point_corrector_

        // Eval correction from segment
        Scalar res_seg_corrector = blend_sum_corrector_seg_.Eval(point);  // segment_corrector_

        // Eval convolution on segment
        Scalar res_seg = blend_sum_main_seg_.Eval(point);   // this->children_

        // Blending of classic eval and correction
        return res_point_corrector + res_seg_corrector + res_seg;
}

//-------------------------------------------------------------------------
template<typename TKernel>
typename NodeOperatorNCorrectedConvolutionT<TKernel>::Vector NodeOperatorNCorrectedConvolutionT<TKernel>::EvalGrad(const Point& point, Scalar epsilon_grad) const
{
    //TODO : @todo : estimer le surcout de faire EvalValueAndGrad plutot que recoder les évaluations
    Scalar value_res = 0.0;
    Vector grad_res = Vector::vectorized(0.0);
    EvalValueAndGrad(point,epsilon_grad,value_res,grad_res);
    return grad_res;
}
//-------------------------------------------------------------------------
template<typename TKernel>
void NodeOperatorNCorrectedConvolutionT<TKernel>::EvalProperties(const Point& point,
                            const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                            const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                            std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                            std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                            ) const
{
    // Eval properties from convolution on segment only ...
    Scalar scal_res_seg;
    Vector grad_res_seg;
    blend_sum_main_seg_.EvalValueAndGradAndProperties(point, 1.0e-8,
                                             scal_prop_ids, vect_prop_ids,
                                             scal_res_seg, grad_res_seg, scal_prop_res, vect_prop_res); //this->children_
}
//-------------------------------------------------------------------------
template<typename TKernel>
void NodeOperatorNCorrectedConvolutionT<TKernel>::EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
        // Eval correction from point
        Scalar scal_point_corrector;
        Vector grad_point_corrector;
        blend_sum_corrector_point_.EvalValueAndGrad(point, epsilon_grad,
                                    scal_point_corrector, grad_point_corrector); //point_corrector_

        // Eval correction from segment
        Scalar scal_segment_corrector;
        Vector grad_segment_corrector;
        blend_sum_corrector_seg_.EvalValueAndGrad(point, epsilon_grad,
                                    scal_segment_corrector, grad_segment_corrector); //segment_corrector_

        // Eval convolution on segment
        Scalar scal_res_seg;
        Vector grad_res_seg;
        blend_sum_main_seg_.EvalValueAndGrad(point, epsilon_grad, scal_res_seg, grad_res_seg); //this->children_

        // Blending of classic eval and correction
        value_res = scal_point_corrector + scal_segment_corrector + scal_res_seg;
        grad_res = grad_point_corrector + grad_segment_corrector + grad_res_seg;
}
//-------------------------------------------------------------------------
template<typename TKernel>
void NodeOperatorNCorrectedConvolutionT<TKernel>::EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
{
    // Eval correction from point
    Scalar scal_point_corrector;
    Vector grad_point_corrector;
    blend_sum_corrector_point_.EvalValueAndGrad(point, epsilon_grad,
                                scal_point_corrector, grad_point_corrector); //point_corrector_

    // Eval correction from segment
    Scalar scal_segment_corrector;
    Vector grad_segment_corrector;
    blend_sum_corrector_seg_.EvalValueAndGrad(point, epsilon_grad,
                                scal_segment_corrector, grad_segment_corrector); //segment_corrector_

    // Eval convolution on segment
    Scalar scal_res_seg;
    Vector grad_res_seg;
    blend_sum_main_seg_.EvalValueAndGradAndProperties(point, epsilon_grad,
                                                      scal_prop_ids, vect_prop_ids,
                                                      scal_res_seg, grad_res_seg, scal_prop_res, vect_prop_res);//this->children_

    // Blending of classic eval and correction
    value_res = scal_point_corrector + scal_segment_corrector + scal_res_seg;
    grad_res = grad_point_corrector + grad_segment_corrector + grad_res_seg;
}


//TODO : @todo : implement it correctly
template<typename TKernel>
void NodeOperatorNCorrectedConvolutionT<TKernel>::PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                            typename std::map<BlobtreeNode*, typename std::pair<AABBox, AABBox> >& updated_areas)
{
    UNUSED(epsilon_bbox); UNUSED(accuracy_needed);

    //TODO : @todo : this function should be updated to take into account the correction scalar field
//        this->blend_sum_.PrepareForEval(*this, 0.0, 0.0, updated_areas);
    if(this->epsilon_bbox() != epsilon_bbox)
    {
        AABBox bbox_children = blend_sum_main_seg_.PrepareForEval(this->children(), epsilon_bbox, accuracy_needed,updated_areas);
        AABBox bbox_seg_corrector = blend_sum_corrector_seg_.PrepareForEval(segment_corrector_,epsilon_bbox, accuracy_needed,updated_areas);
        AABBox bbox_union = bbox_children.Union(bbox_seg_corrector);
        this->set_axis_bounding_box(bbox_union);
        this->set_epsilon_bbox(epsilon_bbox);
        this->UpdateNodeCompacity();

        // need an update but should not affect the bounding box ... nor the updated area (i think...)
        typename std::map<BlobtreeNode*, typename std::pair<AABBox, AABBox> > tmp_updated_areas;
        blend_sum_corrector_point_.PrepareForEval(point_corrector_,epsilon_bbox, accuracy_needed,tmp_updated_areas);;
    }

//    if(this->epsilon_bbox() != epsilon_bbox)
//    {
//        Scalar scal_n = (Scalar) this->children().size();
//        AABBox bt_node_abb = AABBox(); // this auxilliary is there to avoid calling set_axis_bounding_box which implies calls to node listeners.
//        for(typename std::vector<BlobtreeNode*>::const_iterator it = this->nonconst_children().begin(); it != this->nonconst_children().end(); ++it)
//        {
//            assert( (*it)!=NULL );
//            (*it)->PrepareForEval(epsilon_bbox, accuracy_needed/scal_n, updated_areas);
//            bt_node_abb = AABBox::Union(bt_node_abb, (*it)->axis_bounding_box());
//        }

//        this->set_axis_bounding_box(bt_node_abb);
//        this->set_epsilon_bbox(epsilon_bbox);
//    }
}

// @todo implement it correctly
template<typename TKernel>
typename NodeOperatorNCorrectedConvolutionT<TKernel>::AABBox NodeOperatorNCorrectedConvolutionT<TKernel>::GetAxisBoundingBox(Scalar value) const
{
    return this->blend_sum_.GetAxisBoundingBox(this->children(), value*0.001);
}


template<typename TKernel>
void NodeOperatorNCorrectedConvolutionT<TKernel>::AddChild(std::shared_ptr<BlobtreeNode> bt_node)
{
    assert(bt_node->parent() == NULL); // please disconnect the node from its parent before anything.
    assert(bt_node != NULL && this->arity_ == -1);

    if(bt_node->parent() != NULL)
    {
        bt_node->nonconst_parent()->RemoveChild(bt_node);
    }
    bt_node->set_parent(this);
    this->children_.push_back(bt_node);

    // Test if the bt_node is a NodeScalarField and, more precisely,
    // if it contains a SegmentSField<TFunctorKernel> as ScalarField
    assert( dynamic_cast<NodeScalarField *>(bt_node) != NULL);
    ScalarField *scalar_field = const_cast<ScalarField *>((static_cast<NodeScalarField *>(bt_node))->scalar_field());
    assert( dynamic_cast<SegmentSField *>(scalar_field) != NULL);
    SegmentSField *segment_field = static_cast<SegmentSField *>(scalar_field);
    
    TKernel ker = segment_field->eval_functor();
    ker.set_scale(kernel_radius_factor_);
    segment_field->set_eval_functor(ker);

    this->InvalidateBoundingBox();
}


//TODO : @todo : a remplacer (sans doute pas optimal & pas valable pour tout les mélanges !! 
template<typename TKernel>
typename NodeOperatorNCorrectedConvolutionT<TKernel>::Point NodeOperatorNCorrectedConvolutionT<TKernel>::GetAPointAt(Scalar value, Scalar epsilon) const
{
    Point res = Point::vectorized(0.0);
    typename std::vector<BlobtreeNode*>::const_iterator it = this->children_.begin();
    while(it != this->children_.end() && res[0]==0.0 && res[1]==0.0 && res[2]==0.0)
    {
        Point init_point = (*it)->GetAPointAt(value, epsilon);
        Vector search_dir = this->EvalGrad(init_point, 0.0001); // arbitrary
        search_dir.normalize();

        // Accuracy for gradient computation
        // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
        Scalar grad_epsilon = 0.0001;   // arbitrary
        if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }
        double r_min = -30.0, r_max = 30.0;
        ConvergenceT<Traits>::SafeNewton1D(
                                                        *(static_cast<const Base*>(this)),
                                                        init_point,
                                                        search_dir,
                                                        r_min,  //TODO : arbitrary                                // security minimum bound
                                                        r_max,    //TODO : arbitrary
                                                        0.0,            // point at which we start the search, given in 1D
                                                        value,
                                                        epsilon,
                                                        grad_epsilon,
                                                        20,
                                                        res);
        ++it;
    }

    return res;
}

template<typename TKernel>
class ClonedNodeOperatorNCorrectedConvolutionT :
        public NodeOperatorNCorrectedConvolutionT<TKernel>
{
public:
    typedef NodeOperatorNCorrectedConvolutionT<TKernel> Base;

    ClonedNodeOperatorNCorrectedConvolutionT(const Base &rhs) : Base(rhs)
    {
        //TODO:@todo : the main tree should be up-to-date otherwise ... argh !
        this->blend_sum_main_seg_.PrepareGrid(this->axis_bounding_box(), this->children());
        this->blend_sum_corrector_seg_.PrepareGrid(this->axis_bounding_box(), this->segment_corrector_);
        this->blend_sum_corrector_point_.PrepareGrid(this->axis_bounding_box(), this->point_corrector_);
    }

    ~ClonedNodeOperatorNCorrectedConvolutionT()
    {
        for (typename std::set<NodeScalarField*>::iterator it = Base::point_corrector_.begin();
             it != Base::point_corrector_.end(); ++it)
            delete *it;
        for (typename std::set<NodeScalarField*>::const_iterator it = Base::segment_corrector_.begin();
             it != Base::segment_corrector_.end(); ++it)
            delete *it;
    }
};

template<typename TKernel>
NodeOperatorNCorrectedConvolutionT<TKernel> *NodeOperatorNCorrectedConvolutionT<TKernel>::clone() const
{
    return new ClonedNodeOperatorNCorrectedConvolutionT<TKernel>(*this);
}

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_NODE_CORRECTED_CONVOLUTION_H_


