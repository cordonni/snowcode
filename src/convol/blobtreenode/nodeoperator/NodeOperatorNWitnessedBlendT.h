/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: NodeOperatorNWitnessedBlendT.h

   Language: C++

   License: Convol Licence

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inrialpes.fr

   Description: Header file for blending node using a method inspired from "Witnessed k-distance".
                An evaluation of this scalar field is done in 2 main steps:
                    - find the needed primitives among children_ according to the position of evaluation.
                    - compute the scalar field of the clipped primitives

                WARNING : all children must be templated by the same kind of kernel (with the same parameters)
                WARNING : it can only have SEGMENT scalar field as primitive ("pour le moment").
                          The weight of the primitive represent the wanted radius (which is not the case with classical convolution)

                NB : more than a blending node we can see this NodeOperator as a new kind of primitive !

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_NODE_WITNESSED_BLEND_H_
#define CONVOL_NODE_WITNESSED_BLEND_H_

//// std dependencies
//#include <vector>
//#include <list>
//#include <string>
//#include <sstream>

// core dependencies
#include <core/CoreRequired.h>
    // geometry
    #include <core/geometry/Sphere.h>

// convol dependencies
    // blobtreenode
    #include <convol/blobtreenode/NodeOperatorT.h>
    // functor
    #include <convol/functors/operators/BlendSum.h>

//    // ScalarFields
//    #include<Convol/include/ScalarFields/ScalarFieldT.h>
//    #include<Convol/include/ScalarFields/HomotheticSegmentSFieldT.h>
//    #include<Convol/include/ScalarFields/BlobtreeNode/NodeOperatorT.h>
//    #include<Convol/include/ScalarFields/BlobtreeNode/NodeScalarFieldT.h>
//    #include<Convol/include/ScalarFields/BlobtreeNode/BlobtreeNodeT.h>
//    // Functors
//    #include<Convol/include/functors/FunctorT.h>
//    #include<Convol/include/functors/EnumFunctorType.h>
//        // Operators
//        #include<Convol/include/functors/Operators/BlendOperators/BlendSumT.h>
//    // Geometry
//    #include<Convol/include/geometry/SphereT.h>
//    #include<Convol/include/geometry/WeightedSegmentWithDensityT.h>


//// Convol tools
//#include<Convol/include/tools/ConvergenceTools.h>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/Xml/tinyxml/tinyxml.h>

//#include<stdio.h>

//////////////////////////////////////////////////
/// TODO:@todo : REMISE AU PROPRE NECESSAIRE ...
//////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO:@todo : should update size of help vector in AddChildren, RemoveChildren, ...
// TODO:@todo : add needed stuff to work with DCOctree meshing method
// TODO:@todo : try to use the correction method ... if success the node should probably derive from NodeCorrectedConvolution
// TODO:@todo : use optimization structure to avoid all the bounding box test
// TODO:@todo : redondant computation during clipping (scalar product and norm)
///////////////////////////////////////////////////////////////////////////////////////////////////////

namespace expressive {

namespace convol {

class AbstractNodeOperatorNWitnessedBlend
        : public NodeOperator
{
public:
    EXPRESSIVE_MACRO_NAME("NodeWitnessedBlend") // This macro defined functions
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractNodeOperatorNWitnessedBlend(Scalar length = 2.0)
        : BlobtreeNode(StaticName()), clipping_length_(length)
    {
        this->parent_ = NULL;
        this->arity_ = -1;

//        this->clipping_length_ = length;
    }
    virtual ~AbstractNodeOperatorNWitnessedBlend(){}

//        virtual std::string ParameterName() const;

//        ///////////////////
//        // Type Function //
//        ///////////////////

//        virtual ENodeOperatorType Type() const { return E_NodeOperatorNWitnessedBlend; }

//        ///////////////////
//        // Xml functions //
//        ///////////////////

//        virtual TiXmlElement * GetXml() const = 0;

//        void AddXmlSimpleAttribute(TiXmlElement * element) const;
//        void AddXmlComplexAttribute(TiXmlElement * element) const;

    /////////////////
    /// Accessors ///
    /////////////////

    virtual const Functor& kernel_functor() const = 0;

    const Scalar clipping_length() const { return clipping_length_; }

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_clipping_length_(Scalar length)
    {
        clipping_length_ = length;
        this->InvalidateBoundingBox();
    }

   protected:
    AbstractNodeOperatorNWitnessedBlend(const AbstractNodeOperatorNWitnessedBlend &rhs)
        : NodeOperator(rhs), clipping_length_(rhs.clipping_length_){}

    /////////////////
    /// Attributs ///
    /////////////////
protected:
    // Correspond to sharpness of the blending (limit to 0 = generalized cylinder)
    Scalar clipping_length_;
};

/*! \brief Node for Witnessed Distance blending.
 */
template<typename TFunctorKernel>
class NodeOperatorNWitnessedBlendT :
        public AbstractNodeOperatorNWitnessedBlend
{
public:
    typedef HomotheticSegmentSFieldT<TFunctorKernel> HomotheticSSFKer;

    typedef WeightedSegmentWithDensityT<Traits> WeightedSegmentWithDensity;

    struct SegmentWithInfo
    {
        HomotheticSSFKer *seg_;
        Scalar uv_;
        Scalar d2_;

        SegmentWithInfo(HomotheticSSFKer *seg = NULL, Scalar uv = 0.0, Scalar d2 = 0.0)
            : seg_(seg), uv_(uv), d2_(d2) { }
    };

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    NodeOperatorNWitnessedBlendT(Scalar length, const TFunctorKernel& kernel)
        : AbstractNodeOperatorNWitnessedBlend(length), kernel_(kernel), error_threshold_(1.0e-10) //error threshold should be changed...
    {
        InitMaximalSphereRadius();
    }
    virtual ~NodeOperatorNWitnessedBlendT(){}

    virtual NodeOperatorNWitnessedBlendT *clone() const
    {
        return new NodeOperatorNWitnessedBlendT(*this);
    }

//    ////////////////////
//    // Name functions //
//    ////////////////////

//    virtual std::string FunctorName() const;

//    ///////////////////
//    // Xml functions //
//    ///////////////////

//    TiXmlElement * GetXml() const;
//    void AddXmlFunctor(TiXmlElement * element) const;


    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    Scalar Eval(const Point& point) const;
    Vector EvalGrad(const Point& point, Scalar epsilon_grad) const;
    void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;
    void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const;
    inline void EvalProperties(const Point& point,
                               const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                               const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                               std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                               std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                              ) const;

    // Two following function works as a NodeOperatorNAry<BlendSum> ...
    void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                        typename std::map<BlobtreeNode*, typename std::pair<AABBox, AABBox> >& updated_areas);
    AABBox GetAxisBoundingBox(Scalar value) const;

    /////////////////
    /// Accessors ///
    /////////////////

    const Functor& kernel_functor() const { return kernel_; }
    const TFunctorKernel kernel() const { return kernel_; }

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_kernel(TFunctorKernel kernel);

    void AddChild(std::shared_ptr<BlobtreeNode> bt_node);
//    void RemoveChild(BlobtreeNode* bt_node);

protected:
    NodeOperatorNWitnessedBlendT(const NodeOperatorNWitnessedBlendT &rhs) : AbstractNodeOperatorNWitnessedBlend(rhs),
        kernel_(rhs.kernel_), radius_upper_bound_(rhs.radius_upper_bound_),
        error_threshold_(rhs.error_threshold_),
        clipped_segment_(rhs.clipped_segment_.size(),NULL),
        segment_in_range_(rhs.segment_in_range_.size())
    { }

private:

    /**
     * @brief Compute the minimal value of higher bound for the clipping sphere
     *        radius given the epsilon in bbox
     *
     */
    void InitMaximalSphereRadius();

    /**
     * @brief Initilize the search of the clipping sphere,
     *        it store all the usefull segment inside segment_in_range_
     * @param point         evaluation point
     * @param init_radius   initial radius for the clipping sphere (function result)
     * @param radius_min    maximal radius for the clipping sphere (function result)
     * @param radius_max    minimal radius for the clipping sphere (function result)
     *
     *  @todo if blend become efficient enough to be used with lot of segment
     *       then use optimization structure to avoid all the bounding box test
     */
    inline void InitSearchOfClippingSphere(const Point& point, Sphere& init_sphere, Scalar& radius_min) const;

    /**
     * @brief Optimization of a sphere radius to clipped the right weighted length of skeleton
     *
     * @param point evaluation point
     *
     * @return a sphere clipping the wanted length of skeleton in the homothetic space
     */
    inline Sphere ComputeClippingSphere(const Point& point) const;

    /**
     * @brief Compute the clipping (length clipped but also its derivative)
     *        of a skeleton given a clipping sphere,
     *        store the clipped segment inside segment_in_range_
     *
     * @param clipping_sphere evaluation point with an associated radius
     * @param clipped_length
     * @param deriv_clipped_length
     */
    inline void ClipSkeleton(const Sphere& clipping_sphere, Scalar& clipped_length, Scalar& deriv_clipped_length) const;

    /**
     * @brief Compute a normalization factor that aim at having the right thickness,
     *        it is computed in fucntion of the clipped length
     *
     * @param clipping_sphere evaluation point with an associated radius
     *
     * @return a normalization factor
     */
    inline Scalar WitnessedNormalization(Scalar length) const;

    /**
     * @brief Compute the field value generated by the skeleton inside the clipping_sphere
     *
     * @param clipping_sphere evaluation point with an associated radius
     * @param normalization_length length to be used to compute the normalization factor
     *
     * @return a field value
     */
    inline Scalar EvalClippedPrimitives(const Sphere& clipping_sphere, Scalar normalization_length) const;

    inline void EvalValueAndGradClippedPrimitives(const Sphere& clipping_sphere, Scalar normalization_length,
                                                  Scalar& value_res, Vector& grad_res) const;

    inline void EvalValueAndGradAndPropertiesClippedPrimitives(const Sphere& clipping_sphere, Scalar normalization_length,
                                                               const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                               const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                               Scalar& value_res, Vector& grad_res,
                                                               std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                               std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                               ) const;

    /////////////////
    /// Attributs ///
    /////////////////

    TFunctorKernel kernel_;

    Scalar radius_upper_bound_; // Higher bound on the clipping sphere radius (computed from kernel properties and clipping_length?)

    // TODO : @todo : we should find a way to do clipping without this 2 parameters !
    Scalar error_threshold_;    // error accepted on the length clipped

    //////////////////////
    /// Help variables ///
    //////////////////////

    // Data used to find the part of skeleton to be used during evaluation of Salar Field

    /*!
     *  Vector containing pointer toward the primitives needed for evaluation (for a given point in space).
     *  It is upadted by TODO.
     */
    mutable std::vector<HomotheticSSFKer *> clipped_segment_;
//    mutable std::vector<HomotheticSSFKer *> segment_in_range_;
    mutable std::vector<SegmentWithInfo> segment_in_range_;
    // TODO:@todo : not thread safe

    // Blending functor used to avoid implementation of some function
    BlendSum blend_sum_;
};

} // Close namespace convol

} // Close namespace expressive

// implementation file
#include <convol/blobtreenode/nodeoperator/NodeOperatorNWitnessedBlendT.hpp>


#endif // CONVOL_NODE_WITNESSED_BLEND_H_
