#include <convol/blobtreenode/nodeoperator/IntegralSurfaceCorrector.h>

#include <convol/factories/ISCorrectorFactory.h>

// List of all kernel that can be used with a IntegralSurfaceCorrector
// (this is required to register them in the ISCorrectorFacotry)

#include <convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.h>
#include <convol/functors/kernels/compactpolynomial/StandardCompactPolynomial.h>

#include <convol/functors/kernels/cauchyinverse/HomotheticCauchy.h>
#include <convol/functors/kernels/cauchyinverse/StandardCauchy.h>

#include <convol/functors/kernels/cauchyinverse/HomotheticInverse.h>
#include <convol/functors/kernels/cauchyinverse/StandardInverse.h>

using namespace expressive::core;

namespace expressive {

namespace convol {

////////////////////////////
/// Evaluation functions ///
////////////////////////////

AABBox AbstractIntegralSurfaceCorrector::PrepareForEval(Scalar epsilon_bbox)
{
    AABBox aabb = AABBox();
    for(auto v_id : modified_vertices_)
    {
        aabb.Enlarge(CreateCorrectors(v_id, epsilon_bbox));
    }
    modified_vertices_.clear();

    return aabb;
}


///////////////////////////////
/// Graph Listener Function ///
///////////////////////////////

bool AbstractIntegralSurfaceCorrector::IsNodeVertices(core::VertexId id)
{
    auto edges = this->graph_->Get(id)->GetEdges();

    bool is_vertex = false;
    while (edges->HasNext()) {
        auto *primitive = dynamic_cast<BlobtreeNode*>(edges->Next()->edgeptr().get());
        if(primitive)
            is_vertex |= (primitive->parent() == this->bt_node_); // WARNING : what happen with deleted primitives, are they already disconnected ?
    }
    return is_vertex;
}


void AbstractIntegralSurfaceCorrector::EdgeAdded(core::EdgeId /*id*/)
{
//    std::cout<<"Call to Edge Added:" << id << std::endl;
    //TODO:@todo: question : does it inform existing vertices used in the edge?
}

void AbstractIntegralSurfaceCorrector::EdgeUpdated(core::EdgeId id)
{
    // required because deforming an edge through one of its vertices
    // doesn't inform the other vertices.
///TODO:@todo : work only if an edge has two vertices !
    auto edge = this->graph_->Get(id);
    VertexUpdated(edge->GetStartId());
    VertexUpdated(edge->GetEndId());
}

void AbstractIntegralSurfaceCorrector::EdgeRemoved(core::EdgeId /*id*/) {}

void AbstractIntegralSurfaceCorrector::VertexAdded(core::VertexId id)
{
    if(IsNodeVertices(id)) {
        modified_vertices_.insert(id);
    }
}

void AbstractIntegralSurfaceCorrector::VertexUpdated(core::VertexId id)
{
    if(IsNodeVertices(id)) {
        EraseCorrectors(id);
        modified_vertices_.insert(id);
    }
}

void AbstractIntegralSurfaceCorrector::VertexRemoved(core::VertexId id)
{
    // Since data are already deleted, we cannot test if vertex belong to the bt_node
    EraseCorrectors(id);
    modified_vertices_.erase(id);
}

void AbstractIntegralSurfaceCorrector::Notify(const core::WeightedGraph::GraphChanges &changes)
{
//    for (auto e : changes.removed_edges_)  EdgeRemoved(e);
    for (auto v : changes.removed_vertices_)  VertexRemoved(v);
    for (auto e : changes.added_vertices_) VertexAdded(e);
    for (auto v : changes.added_edges_)  EdgeAdded(v);
    for (auto e : changes.updated_edges_) EdgeUpdated(e);
    for (auto v : changes.updated_vertices_) VertexUpdated(v);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// Register IntegralSurfaceCorrectorT<HomotheticKernel, StandardKernel> in the ISCorrectorFactory ///
//////////////////////////////////////////////////////////////////////////////////////////////////////

static ISCorrectorFactory::Type<HomotheticCompactPolynomial4, StandardCompactPolynomial4>
                                                                 ISCorrector_HomotheticCompactPolynomial4_Type;
static ISCorrectorFactory::Type<HomotheticCompactPolynomial6, StandardCompactPolynomial6>
                                                                 ISCorrector_HomotheticCompactPolynomial6_Type;
static ISCorrectorFactory::Type<HomotheticCompactPolynomial8, StandardCompactPolynomial8>
                                                                 ISCorrector_HomotheticCompactPolynomial8_Type;


static ISCorrectorFactory::Type<HomotheticCauchy3, StandardCauchy3>
                                                      ISCorrector_HomotheticCauchy3_Type;
static ISCorrectorFactory::Type<HomotheticCauchy4, StandardCauchy4>
                                                      ISCorrector_HomotheticCauchy4_Type;
static ISCorrectorFactory::Type<HomotheticCauchy5, StandardCauchy5>
                                                      ISCorrector_HomotheticCauchy5_Type;
static ISCorrectorFactory::Type<HomotheticCauchy6, StandardCauchy6>
                                                      ISCorrector_HomotheticCauchy6_Type;


static ISCorrectorFactory::Type<HomotheticInverse3, StandardInverse3>
                                                      ISCorrector_HomotheticInverse3_Type;
static ISCorrectorFactory::Type<HomotheticInverse4, StandardInverse4>
                                                      ISCorrector_HomotheticInverse4_Type;
static ISCorrectorFactory::Type<HomotheticInverse5, StandardInverse5>
                                                      ISCorrector_HomotheticInverse5_Type;
static ISCorrectorFactory::Type<HomotheticInverse6, StandardInverse6>
                                                      ISCorrector_HomotheticInverse6_Type;

} // Close namespace convol

} // Close namespace expressive
