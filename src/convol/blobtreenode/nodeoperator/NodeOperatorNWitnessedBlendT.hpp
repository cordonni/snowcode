
//#include<Convol/include/geometry/WeightedPointT.h>

//TODO:@todo : in order to test some special blending we introduce the following MACRO
//#define WITNESSED_VAR_SHARPNESS
//#define WITNESSED_BALL_ANALYSIS
// When test will be finished, we should rewrite part of the code in a cleaner way.

namespace expressive {

namespace convol {

///////////////////////////////////////////
/// AbstractNodeOperatorNWitnessedBlendT //
///////////////////////////////////////////

//template<typename TTraits>
//std::string AbstractNodeOperatorNWitnessedBlendT<TTraits>::ParameterName() const
//{
//    std::ostringstream name;
//    name.setf(std::ios::scientific,std::ios::floatfield);
//    name.precision(2);

//    name << "[arity(N),";
//    name << "clipping_length(" << clipping_length_ << ")]";

//    return name.str();
//}

//template<typename TTraits>
//void AbstractNodeOperatorNWitnessedBlendT<TTraits>::AddXmlSimpleAttribute(TiXmlElement * element) const
//{
//    // Common ScalarField Complex attributes
//    Base::AddXmlSimpleAttribute(element);

//    element->SetDoubleAttribute("clipping_length", clipping_length_);
//}

//template<typename TTraits>
//void AbstractNodeOperatorNWitnessedBlendT<TTraits>::AddXmlComplexAttribute(TiXmlElement * element) const
//{
//    // Common ScalarField Complex attributes
//    Base::AddXmlComplexAttribute(element);
//}


////////////////////////////////////
/// NodeOperatorNWitnessedBlendT ///
////////////////////////////////////


//////////////////////
//// Name functions //
//////////////////////

//template<typename TFunctorKernel>
//std::string NodeOperatorNWitnessedBlendT<TFunctorKernel>::FunctorName() const
//{
//    std::ostringstream name;
//    name << "<" << kernel_.Name() << ">";
//    return name.str();
//}

/////////////////////
//// Xml functions //
/////////////////////

//template<typename TFunctorKernel>
//TiXmlElement * NodeOperatorNWitnessedBlendT<TFunctorKernel>::GetXml() const
//{
//    TiXmlElement *element_base = new TiXmlElement( this->Name() );

//    Base::AddXmlSimpleAttribute(element_base);
//    Base::AddXmlComplexAttribute(element_base);
//    AddXmlFunctor(element_base);

//    return element_base;
//}

//template<typename TFunctorKernel>
//void NodeOperatorNWitnessedBlendT<TFunctorKernel>::AddXmlFunctor(TiXmlElement * element) const
//{
//    TiXmlElement *element_functor_eval = new TiXmlElement( "TFunctorKernel" );
//    element->LinkEndChild(element_functor_eval);
//    TiXmlElement * element_child = kernel_.GetXml();
//    element_functor_eval->LinkEndChild(element_child);
//}


///////////////
// Modifiers //
///////////////

template<typename TFunctorKernel>
void NodeOperatorNWitnessedBlendT<TFunctorKernel>::set_kernel(TFunctorKernel kernel)
{
    kernel_ = kernel;
    InitMaximalSphereRadius();
}

template<typename TFunctorKernel>
void NodeOperatorNWitnessedBlendT<TFunctorKernel>::AddChild(std::shared_ptr<BlobtreeNode> bt_node)
{
    assert(bt_node->parent() == NULL); // please disconnect the node from its parent before anything.
    assert(bt_node != NULL && this->arity_ == -1);

    if(bt_node->parent() != NULL)
    {
        bt_node->nonconst_parent()->RemoveChild(bt_node);
    }
    bt_node->set_parent(this);
    this->children_.push_back(bt_node);

    // Test if the bt_node is a NodeScalarField and, more precisely,
    // if it contains a SegmentSField<TFunctorKernel> as ScalarField
    assert( dynamic_cast<NodeScalarField *>(bt_node) != NULL);
    ScalarField *scalar_field = const_cast<ScalarField *>((static_cast<NodeScalarField *>(bt_node))->scalar_field());
    assert( dynamic_cast<HomotheticSSFKer *>(scalar_field) != NULL);
    //static_cast<HomotheticSSFKer *>(scalar_field)->eval_functor().set_scale(kernel_.scale());

    //TODO:@todo : should try to avoid this (find a way to have MaximalSphereRadius independant from nb of skeleton : this should be possible)
    InitMaximalSphereRadius();

    this->InvalidateBoundingBox();
}

//////////////////////////
// Evaluation functions //
//////////////////////////

//template<typename TFunctorKernel>
//typename NodeOperatorNWitnessedBlendT<TFunctorKernel>::Scalar
//    NodeOperatorNWitnessedBlendT<TFunctorKernel>::Eval(const Point& point) const
template<typename TFunctorKernel>
auto NodeOperatorNWitnessedBlendT<TFunctorKernel>::Eval(const Point& point) const -> Scalar
{
    //TODO:@todo : it should be possible to have the same implementation for both cases: in order to do so, we should
    // change the way the upper_bound for clipping is working... and troncate the length only after the computation of
    // the effective clipping
    Sphere clipping_sphere(ComputeClippingSphere(point));
#ifndef WITNESSED_VAR_SHARPNESS
    Scalar effective_clipping_length = this->clipping_length_;
#else
    Scalar effective_clipping_length = 0.0;
    for(auto it = clipped_segment_.begin(); it != clipped_segment_.end(); ++it)
    {
        effective_clipping_length += (*it)->segment().HomotheticClippedLength(clipping_sphere);
    }
    //TODO:@todo : the clipping_sphere radius should be troncated here for CompactPolynomial Kernel
#endif
    return EvalClippedPrimitives(clipping_sphere, effective_clipping_length);
}

template<typename TFunctorKernel>
typename NodeOperatorNWitnessedBlendT<TFunctorKernel>::Vector
    NodeOperatorNWitnessedBlendT<TFunctorKernel>::EvalGrad(const Point& point, Scalar epsilon_grad) const
{
    //TODO:@todo : find closed-form gradient or pseudo-analytic or optimized clipping
    return GradientEvaluationT<Traits>::NumericalGradient6points(*this, point, epsilon_grad);
}

template<typename TFunctorKernel>
void NodeOperatorNWitnessedBlendT<TFunctorKernel>::EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
    UNUSED(epsilon_grad);
#if defined(WITNESSED_VAR_SHARPNESS) ||  defined(WITNESSED_BALL_ANALYSIS)
    Sphere clipping_sphere(ComputeClippingSphere(point));

    EvalValueAndGradClippedPrimitives(clipping_sphere, this->clipping_length_, value_res, grad_res);
#else
    value_res = Eval(point);
    grad_res = GradientEvaluationT<Traits>::NumericalGradient6points(*this, point,  0.001);//epsilon_grad);
#endif
}

template<typename TFunctorKernel>
void NodeOperatorNWitnessedBlendT<TFunctorKernel>::EvalValueAndGradAndProperties(
                        const Point& point, Scalar epsilon_grad,
                        const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                        const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                        Scalar& value_res, Vector& grad_res,
                        std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                        std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                ) const
{
    UNUSED(epsilon_grad);
#if defined(WITNESSED_VAR_SHARPNESS) ||  defined(WITNESSED_BALL_ANALYSIS)
    value_res = Eval(point);
    grad_res = GradientEvaluationT<Traits>::NumericalGradient6points(*this, point, 0.001);//epsilon_grad);
    EvalProperties(point, scal_prop_ids,vect_prop_ids,scal_prop_res,vect_prop_res);
#else
    Sphere clipping_sphere(ComputeClippingSphere(point));

    EvalValueAndGradAndPropertiesClippedPrimitives(clipping_sphere, this->clipping_length_,
                                                   scal_prop_ids,vect_prop_ids,
                                                   value_res, grad_res,
                                                   scal_prop_res, vect_prop_res );
#endif
}

template<typename TFunctorKernel>
inline void NodeOperatorNWitnessedBlendT<TFunctorKernel>::EvalProperties(
                        const Point& point,
                        const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                        const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                        std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                        std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                ) const
{
    //TODO : @todo : to be implemented
    UNUSED(point);
    UNUSED(scal_prop_ids); UNUSED(vect_prop_ids);
    UNUSED(scal_prop_res); UNUSED(vect_prop_res);
}

// Two following function works as a NodeOperatorNAry<BlendSum> ...
template<typename TFunctorKernel>
void NodeOperatorNWitnessedBlendT<TFunctorKernel>::PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                            typename std::map<BlobtreeNode*, typename std::pair<AABBox, AABBox> >& updated_areas)
{
    blend_sum_.PrepareForEval(*this, epsilon_bbox, accuracy_needed, updated_areas);
    this->UpdateNodeCompacity();

    InitMaximalSphereRadius();
}

template<typename TFunctorKernel>
AABBox NodeOperatorNWitnessedBlendT<TFunctorKernel>::GetAxisBoundingBox(const Scalar value) const
{
    return blend_sum_.GetAxisBoundingBox(this->children(), value); //value*0.001
}




//////////////////////////////////////
/// Function defining the blending ///
//////////////////////////////////////

template<typename TFunctorKernel>
void NodeOperatorNWitnessedBlendT<TFunctorKernel>::InitMaximalSphereRadius()
{
#ifndef WITNESSED_VAR_SHARPNESS
    // The worse case for precision is the one where all
    // the clipped lenght of the skeleton is in the same point of space
    Scalar epsilon = this->epsilon_bbox() / ( this->clipping_length_ * WitnessedNormalization(this->clipping_length_) );
    radius_upper_bound_ = kernel_.GetEuclidDistBound(WeightedPointT<Traits>(), epsilon );
#else
    radius_upper_bound_ = 2000.0;
#endif
}


template<typename TFunctorKernel>
typename NodeOperatorNWitnessedBlendT<TFunctorKernel>::Scalar
    NodeOperatorNWitnessedBlendT<TFunctorKernel>::EvalClippedPrimitives(const Sphere& clipping_sphere,
                                                                        Scalar normalization_length) const
{
    Scalar res = 0.0;
    for(auto it = clipped_segment_.begin(); it != clipped_segment_.end(); ++it)
    {
        res += (*it)->EvalClipped(clipping_sphere);
    }
#ifndef WITNESSED_BALL_ANALYSIS
    return res / WitnessedNormalization(normalization_length);
#else
    const Scalar base_normalization = WitnessedNormalization(normalization_length);
    const Scalar correction_max = base_normalization / ( normalization_length * kernel_(1.0));

    Scalar ball_factor;
    const Scalar limit = 1.8;
    if(clipping_sphere.radius_<limit)
    {
        Scalar tmp =(clipping_sphere.radius_ - limit) / (limit-1.0);
        tmp = 1.0-tmp*tmp;
        ball_factor = correction_max + tmp * (1.0-correction_max);
/*
        if(clipping_sphere.radius_>1.0)
        {
            Scalar tmp =(clipping_sphere.radius_ - 1.0) / (limit-1.0);
            tmp *= tmp;
            tmp = (1.0-tmp)*(1.0-tmp);

            ball_factor = 1.0 + (correction_max-1.0)*tmp;
        }
        else
        {
            ball_factor = correction_max;
        }
        */
    }
    else
    {
        ball_factor = 1.0;
    }
    return ball_factor * res / base_normalization;
#endif
}

template<typename TFunctorKernel>
void NodeOperatorNWitnessedBlendT<TFunctorKernel>::EvalValueAndGradClippedPrimitives(const Sphere& clipping_sphere,
                                                                                    Scalar normalization_length,
                                                                                    Scalar& value_res, Vector& grad_res) const
{
    value_res = 0.0;
    grad_res.vectorize(0.0);
    Scalar value_aux;
    Vector grad_aux;
    for(auto it = clipped_segment_.begin(); it != clipped_segment_.end(); ++it)
    {
        (*it)->EvalValueAndGradClipped(clipping_sphere, 0.0, value_aux, grad_aux);
        value_res += value_aux;
        grad_res += grad_aux;
    }

    Scalar normalization = 1.0 / WitnessedNormalization(normalization_length);
    value_res *= normalization;
    grad_res  *= normalization;
}

template<typename TFunctorKernel>
void NodeOperatorNWitnessedBlendT<TFunctorKernel>::EvalValueAndGradAndPropertiesClippedPrimitives(const Sphere& clipping_sphere,
                                                                                    Scalar normalization_length,
                                                                                     const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                                                     const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                                                     Scalar& value_res, Vector& grad_res,
                                                                                     std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                                                     std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.

                                                                                     ) const
{
    value_res = 0.0;
    grad_res.vectorize(0.0);
    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
    {
        scal_prop_res[i] = 0.0;
    }
    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
    {
        vect_prop_res[i] = Vector::vectorized(0.0);
    }

    Scalar value_aux;
    Vector grad_aux;
    std::vector<Scalar> scal_prop_aux;
    scal_prop_aux.resize(scal_prop_ids.size());
    std::vector<Vector> vect_prop_aux;
    vect_prop_aux.resize(vect_prop_ids.size());

    for(auto it = clipped_segment_.begin(); it != clipped_segment_.end(); ++it)
    {
        (*it)->EvalValueAndGradAndPropertiesClipped(clipping_sphere, 0.0, scal_prop_ids, vect_prop_ids, value_aux, grad_aux, scal_prop_aux, vect_prop_aux);
        value_res += value_aux;
        grad_res += grad_aux;

        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] += value_aux*scal_prop_aux[i];
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] += value_aux*vect_prop_aux[i];
        }
    }

    if(value_res != 0.0)
    {
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = scal_prop_res[i]/value_res;
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = vect_prop_res[i]/value_res;
        }
    }
    else
    {
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = Traits::kSFScalarPropertiesDefault[scal_prop_ids[i]];
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = Traits::kSFVectorPropertiesDefault[vect_prop_ids[i]];
        }
    }

    Scalar normalization = 1.0 / WitnessedNormalization(normalization_length);
    value_res *= normalization;
    grad_res  *= normalization;
}


template<typename TFunctorKernel>
typename NodeOperatorNWitnessedBlendT<TFunctorKernel>::Scalar
    NodeOperatorNWitnessedBlendT<TFunctorKernel>::WitnessedNormalization(Scalar length) const
{
    return kernel_.GetIsoValueForThickness(length, 1.0) / kernel_.normalization_factor_1D() ;
}

template<typename TFunctorKernel>
inline void NodeOperatorNWitnessedBlendT<TFunctorKernel>::InitSearchOfClippingSphere(const Point& point, Sphere& init_sphere, Scalar& radius_min) const
{
    //TODO:@todo : if blend become efficient enough to be used with lot of segment
    // then use optimization structure to avoid all the bounding box test

    segment_in_range_.clear();

    radius_min = radius_upper_bound_;

    NodeScalarField *child;
    HomotheticSSFKer *segment_field;
    Scalar dist, uv, d2;
    for(auto it = (this->children_).begin(); it != (this->children_).end(); it++)
    {
        child = static_cast<NodeScalarField *>(*it);
        if((*it)->axis_bounding_box().Contains(point))
        {
            segment_field = static_cast<HomotheticSSFKer *>(const_cast<ScalarField *>(child->scalar_field()));

            // Compute distance between point and segment in homothetic space (as well as recurrent geometric parameter)
            // TODO : could compute the main scalar product and norm and store them
            dist = segment_field->segment().HomotheticDistance(point);

            if(dist<radius_upper_bound_)
            {
                radius_min = (dist < radius_min) ? dist : radius_min;

                const Vector pmin_to_center( point - segment_field->segment().p_min() );
                uv = (segment_field->segment().increase_unit_dir() | pmin_to_center);
                d2 = pmin_to_center.sqrnorm();

                segment_in_range_.push_back(SegmentWithInfo(segment_field,uv,d2));
            }
        }
    }

    //TODO:@todo : find a better heuristic for the initial radius of the clipping sphere
    init_sphere.radius_ = sqrt( 0.25*this->clipping_length()*this->clipping_length() + radius_min*radius_min);
    init_sphere.radius_ = (init_sphere.radius_>radius_upper_bound_) ? radius_upper_bound_ : init_sphere.radius_;
}


template<typename TFunctorKernel>
typename NodeOperatorNWitnessedBlendT<TFunctorKernel>::Sphere
    NodeOperatorNWitnessedBlendT<TFunctorKernel>::ComputeClippingSphere(const Point& point) const
{
    // Find all clippable segments and store them in clippable_segment
    // Initialize min/max radius and clipping_sphere
    Scalar radius_min, radius_max = 1.5*radius_upper_bound_; // TODO:@todo : 1.5 factor to prevent a problem that occur with compact... indeed if we take the exact radius support there is case where there cant be convergence ...
    Sphere clipping_sphere(radius_upper_bound_,point);
    InitSearchOfClippingSphere(point, clipping_sphere, radius_min);

    //TODO:@todo : should take into account the fact that it is possible to have no possibility to clip
    // the wanted length especially with compact kernel... we should not take into account the kernel size
    // in this part of the method because : blablabla

    // Perform a Newton search with some safeguard //(the function is only C^1 by part
    // there is some discontinuity of derivative or even value (less chance to happen) )

    Scalar length_clipped, deriv_clipped_length;
    ClipSkeleton(clipping_sphere, length_clipped, deriv_clipped_length);

    // Compute error and update bound of search
    Scalar error = this->clipping_length_ - length_clipped;
    if(error > 0.0)
    { // not enough length is clipped
        radius_min = clipping_sphere.radius_;
    }
    else
    { // to much length have been clipped
        radius_max = clipping_sphere.radius_;

        // recopy the segments of clipped_segment_ in segment_in_area_
        //segment_in_area_ = clipped_segment_; // should test if really usefull
    }

    for(int i = 0; i<30; ++i) // done to prevent infinit loop
//    while(error*error > 1.0e-12) //error_threshold_)
    {
        // perform the newton step
        clipping_sphere.radius_ += 0.999 * error / deriv_clipped_length;
        //TODO:@todo : the 0.95* shoudl be replaced by an improved iteration scheme
        // indeed there is some case where newton can be blocked (due to inflexion point)

        // check if it is in agreement with bound of search
        if( radius_max < clipping_sphere.radius_ || clipping_sphere.radius_ < radius_min )
        {
            // probably better to use secant method...
            clipping_sphere.radius_ = 0.5*(radius_max + radius_min);
        }

        // compute new clipping and associated error
        ClipSkeleton(clipping_sphere, length_clipped, deriv_clipped_length);
        error = this->clipping_length_ - length_clipped;

        // update bound of search
        if(error > 0.0)
        { // not enough length is clipped
            radius_min = clipping_sphere.radius_;
        }
        else
        { // to much length have been clipped
            radius_max = clipping_sphere.radius_;

            // recopy the segments of clipped_segment_ in segment_in_area_
            //segment_in_area_ = clipped_segment_; // should test if really usefull
        }

        if(error*error < 1.0e-12)
        {
//            std::cout<<i<<std::endl;
//            std::cout<<"nb_it : "<<i<<std::endl;
            break;
        }
    }

//    if(error*error > 1.0e-8) std::cout<<30<<std::endl;
//    if(error*error > 1.0e-8) std::cout<<"error : "<<error*error<<std::endl;

    //TODO : @todo this is for compact support kernel (cf comment above)
    clipping_sphere.radius_ = (clipping_sphere.radius_ < radius_upper_bound_) ?  clipping_sphere.radius_ : radius_upper_bound_;

    return clipping_sphere;

    // TODO:@todo : we could use the clipped_length in radius_max and min... this could permit to use linear interpolation when newton break
    // there is some cases where this can break (no solution ...) : should take it into account...
}

#ifndef WITNESSED_VAR_SHARPNESS
template<typename TFunctorKernel>
void NodeOperatorNWitnessedBlendT<TFunctorKernel>::ClipSkeleton(const Sphere& clipping_sphere, Scalar& clipped_length, Scalar& deriv_clipped_length) const
{
    clipped_segment_.clear();

    clipped_length = 0.0;
    deriv_clipped_length = 0.0;
    Scalar length, deriv_length;
    for(auto it = segment_in_range_.begin(); it != segment_in_range_.end(); it++)
    {
        if(it->seg_->segment().HomotheticClippedLengthAndDerivative(clipping_sphere,
                                                                      it->uv_, it->d2_,
                                                                      length, deriv_length))
        {
            clipped_length += length;
            deriv_clipped_length += deriv_length;
            clipped_segment_.push_back(it->seg_);
        }
    }
}
#else
template<typename TFunctorKernel>
void NodeOperatorNWitnessedBlendT<TFunctorKernel>::ClipSkeleton(const Sphere& clipping_sphere, Scalar& clipped_length, Scalar& deriv_clipped_length) const
{
    Scalar length_in_sphere = 0.0;
    Scalar d_length_in_sphere = 0.0;

    clipped_segment_.clear();

    Scalar deriv_step = 0.000001;
    Sphere d_sphere = clipping_sphere; //tmp waiting closed-form derivative
    d_sphere.radius_ += deriv_step;

    Scalar length, deriv_length;
    for(auto it = segment_in_range_.begin(); it != segment_in_range_.end(); it++)
    {
        length = (*it)->segment().HomotheticClippedLengthWithSharpness(clipping_sphere);
//        Scalar tmp_radius = (clipping_sphere.radius_ >= 1.0) ?
//                                    1.0 + (*it)->segment().density_coeff(0)*(clipping_sphere.radius_-1.0)
//                                :   pow(clipping_sphere.radius_ , (*it)->segment().density_coeff(0));
//        length = (*it)->segment().HomotheticClippedLengthWithSharpness(Sphere(tmp_radius, clipping_sphere.center_));

        // compute derivative by numerical scheme waiting closed-form
        const Scalar tmp_l = (*it)->segment().HomotheticClippedLengthWithSharpness(d_sphere);
//        tmp_radius = (d_sphere.radius_ >= 1.0) ?
//                                 1.0 + (*it)->segment().density_coeff(0)*(d_sphere.radius_-1.0)
//                            :   pow(d_sphere.radius_ , (*it)->segment().density_coeff(0));
//        const Scalar tmp_l = (*it)->segment().HomotheticClippedLengthWithSharpness(Sphere(tmp_radius, d_sphere.center_));

        deriv_length = (tmp_l - length) / deriv_step;

        if(length > 0.0) //> 0.00000001
        {
            length_in_sphere   += length;
            d_length_in_sphere += deriv_length;
            clipped_segment_.push_back(*it);
        }
    }

    // "returned" value
    clipped_length = length_in_sphere;
    deriv_clipped_length = d_length_in_sphere;
}
#endif

} // Close namespace convol

} // Close namespace expressive
