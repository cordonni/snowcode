/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: CorrectedSumIntegralSurfaceT.h

   Language: C++

   License: Convol Licence

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inrialpes.fr

   Description: Header file for blending node doing scale-invariant integral surface on a skeleton (simple blending by summation).
                It use a IntegralSurfaceCorrector to improve the thickness of the generated surface.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_NODE_CORRECTED_INTEGRAL_SURFACE_H_
#define CONVOL_NODE_CORRECTED_INTEGRAL_SURFACE_H_

// core dependencies
#include <core/CoreRequired.h>

// convol dependencies
    // primitives
    #include <convol/primitives/NodeSegmentSFieldT.h>
    #include <convol/primitives/NodeISSubdivisionCurveT.h>
//    #include <convol/primitives/NodeTriangleSFieldT.h>
    #include <convol/primitives/NodePointSFieldT.h>
    // blobtreenode
    #include <convol/blobtreenode/NodeOperatorT.h>
    #include <convol/blobtreenode/nodeoperator/IntegralSurfaceOperator.h>
    // functors
    #include <convol/functors/operators/BlendSum.h> // for convenience

#include <convol/tools/FBVHKernelEval.h>

namespace expressive {
namespace convol {


class CONVOL_API AbstractCorrectedSumIntegralSurface : public NodeOperator {
public:
    EXPRESSIVE_MACRO_NAME("CorrectedSumIntegralSurface")

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractCorrectedSumIntegralSurface()
        : NodeOperator(StaticName())
    {
        this->parent_ = nullptr;
        this->arity_ = -1;
    }

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        NodeOperator::AddXmlAttributes(element);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        NodeOperator::AddXmlAttributes(element,ids);
    }


protected:
    // Copy constructor
    AbstractCorrectedSumIntegralSurface(const AbstractCorrectedSumIntegralSurface &rhs)
        : NodeOperator(rhs)
    {}
};


template<typename TFunctorKernel>
class OptCorrectedSumIntegralSurfaceT;


template<typename TFunctorKernel>
class CorrectedSumIntegralSurfaceT :
    public AbstractCorrectedSumIntegralSurface, public IntegralSurfaceOperatorT<TFunctorKernel>
{
public:
    typedef NodeSegmentSFieldT<TFunctorKernel> NodeSegmentSFKer;
    typedef NodeISSubdivisionCurveT<TFunctorKernel> NodeISSubdivisionCurveSFKer;
    typedef NodePointSFieldT<TFunctorKernel> NodePointSFKer;
//    typedef NodeTriangleSFieldT<SemiNumericalHomotheticConvolT<TFunctorKernel> > NodeTriangleSFNumKer;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    CorrectedSumIntegralSurfaceT(const TFunctorKernel& kernel)
        : AbstractCorrectedSumIntegralSurface(), IntegralSurfaceOperatorT<TFunctorKernel>(kernel)
    {
//        IntegralSurfaceOperatorT<TFunctorKernel>::AddCorrector();
    }

    // Copy constructor : required for cloning
    CorrectedSumIntegralSurfaceT(const CorrectedSumIntegralSurfaceT &rhs)
        :   AbstractCorrectedSumIntegralSurface(rhs), IntegralSurfaceOperatorT<TFunctorKernel>(rhs) {}

    ~CorrectedSumIntegralSurfaceT(){}

    virtual std::shared_ptr<ScalarField> CloneForEval() const;

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        AbstractCorrectedSumIntegralSurface::AddXmlAttributes(element);
        IntegralSurfaceOperatorT<TFunctorKernel>::AddXmlAttributes(element);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        AbstractCorrectedSumIntegralSurface::AddXmlAttributes(element, ids);
        IntegralSurfaceOperatorT<TFunctorKernel>::AddXmlAttributes(element, ids);
    }

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual Scalar Eval(const Point& point) const;
    virtual Vector EvalGrad(const Point& point, Scalar epsilon_grad) const;
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                               const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                               const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                               Scalar& value_res, Vector& grad_res,
                                               std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               ) const;

    // Two following function works as a NodeOperatorNAry<BlendSum> ...
    void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                        typename std::map<BlobtreeNode*, typename std::pair<core::AABBox, core::AABBox> >& updated_areas);

    core::AABBox GetAxisBoundingBox(Scalar value) const;

    /////////////////
    /// Accessors ///
    /////////////////

    /////////////////
    /// Modifiers ///
    /////////////////

    virtual void AddChild(std::shared_ptr<BlobtreeNode> bt_node);

protected:

    /////////////////
    /// Attributs ///
    /////////////////

    //////////////////////
    /// Help variables ///
    //////////////////////

    // Blending functor used to avoid implementation of some function
    BlendSum blend_sum_;
};


template<typename TFunctorKernel>
class OptCorrectedSumIntegralSurfaceT : public CorrectedSumIntegralSurfaceT<TFunctorKernel>
{
public:
    EXPRESSIVE_MACRO_NAME("OptCorrectedSumIntegralSurface") // This macro defined functions

//    typedef FittedBVH< std::pair<core::OptWeightedSegment, Properties1D> >::CDataIterator CDataIteratorSeg;
//    typedef FittedBVH< std::pair<core::OptWeightedTriangle, Properties0D> >::CDataIterator CDataIteratorTri;
//    typedef FittedBVH< std::pair<core::WeightedPoint, Properties0D> >::CDataIterator CDataIteratorPoint;

    OptCorrectedSumIntegralSurfaceT(const CorrectedSumIntegralSurfaceT<TFunctorKernel> &rhs);

    virtual Scalar Eval(const Point& point) const;
    virtual Vector EvalGrad(const Point& point, Scalar epsilon_grad) const;
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                               const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                               const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                               Scalar& value_res, Vector& grad_res,
                                               std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                               ) const;

protected:
    FBVHKernelEvalT<  std::pair<core::OptWeightedSegment, Properties1D>, TFunctorKernel> f_bvh_segment_;
//    FittedBVH< std::pair<core::OptWeightedSegment, Properties1D> > f_bvh_segment_;
//    FittedBVH< std::pair<core::OptWeightedTriangle, Properties0D> > f_bvh_triangle_;
//    FittedBVH< std::pair<core::WeightedPoint, Properties0D> > f_bvh_point_;
};


/*
////////////////////////////
/// Evaluation functions ///
////////////////////////////

template<typename TKernel>
typename NodeOperatorNCorrectedConvolutionT<TKernel>::Scalar NodeOperatorNCorrectedConvolutionT<TKernel>::Eval(const Point& point) const
{
        // Eval correction from point
        Scalar res_point_corrector = blend_sum_corrector_point_.Eval(point); // point_corrector_

        // Eval correction from segment
        Scalar res_seg_corrector = blend_sum_corrector_seg_.Eval(point);  // segment_corrector_

        // Eval convolution on segment
        Scalar res_seg = blend_sum_main_seg_.Eval(point);   // this->children_

        // Blending of classic eval and correction
        return res_point_corrector + res_seg_corrector + res_seg;
}

//-------------------------------------------------------------------------
template<typename TKernel>
typename NodeOperatorNCorrectedConvolutionT<TKernel>::Vector NodeOperatorNCorrectedConvolutionT<TKernel>::EvalGrad(const Point& point, Scalar epsilon_grad) const
{
    //TODO : @todo : estimer le surcout de faire EvalValueAndGrad plutot que recoder les évaluations
    Scalar value_res = 0.0;
    Vector grad_res = Vector::vectorized(0.0);
    EvalValueAndGrad(point,epsilon_grad,value_res,grad_res);
    return grad_res;
}
//-------------------------------------------------------------------------
template<typename TKernel>
void NodeOperatorNCorrectedConvolutionT<TKernel>::EvalProperties(const Point& point,
                            const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                            const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                            std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                            std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                            ) const
{
    // Eval properties from convolution on segment only ...
    Scalar scal_res_seg;
    Vector grad_res_seg;
    blend_sum_main_seg_.EvalValueAndGradAndProperties(point, 1.0e-8,
                                             scal_prop_ids, vect_prop_ids,
                                             scal_res_seg, grad_res_seg, scal_prop_res, vect_prop_res); //this->children_
}
//-------------------------------------------------------------------------
template<typename TKernel>
void NodeOperatorNCorrectedConvolutionT<TKernel>::EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
        // Eval correction from point
        Scalar scal_point_corrector;
        Vector grad_point_corrector;
        blend_sum_corrector_point_.EvalValueAndGrad(point, epsilon_grad,
                                    scal_point_corrector, grad_point_corrector); //point_corrector_

        // Eval correction from segment
        Scalar scal_segment_corrector;
        Vector grad_segment_corrector;
        blend_sum_corrector_seg_.EvalValueAndGrad(point, epsilon_grad,
                                    scal_segment_corrector, grad_segment_corrector); //segment_corrector_

        // Eval convolution on segment
        Scalar scal_res_seg;
        Vector grad_res_seg;
        blend_sum_main_seg_.EvalValueAndGrad(point, epsilon_grad, scal_res_seg, grad_res_seg); //this->children_

        // Blending of classic eval and correction
        value_res = scal_point_corrector + scal_segment_corrector + scal_res_seg;
        grad_res = grad_point_corrector + grad_segment_corrector + grad_res_seg;
}
//-------------------------------------------------------------------------
template<typename TKernel>
void NodeOperatorNCorrectedConvolutionT<TKernel>::EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
{
    // Eval correction from point
    Scalar scal_point_corrector;
    Vector grad_point_corrector;
    blend_sum_corrector_point_.EvalValueAndGrad(point, epsilon_grad,
                                scal_point_corrector, grad_point_corrector); //point_corrector_

    // Eval correction from segment
    Scalar scal_segment_corrector;
    Vector grad_segment_corrector;
    blend_sum_corrector_seg_.EvalValueAndGrad(point, epsilon_grad,
                                scal_segment_corrector, grad_segment_corrector); //segment_corrector_

    // Eval convolution on segment
    Scalar scal_res_seg;
    Vector grad_res_seg;
    blend_sum_main_seg_.EvalValueAndGradAndProperties(point, epsilon_grad,
                                                      scal_prop_ids, vect_prop_ids,
                                                      scal_res_seg, grad_res_seg, scal_prop_res, vect_prop_res);//this->children_

    // Blending of classic eval and correction
    value_res = scal_point_corrector + scal_segment_corrector + scal_res_seg;
    grad_res = grad_point_corrector + grad_segment_corrector + grad_res_seg;
}


//TODO : @todo : implement it correctly
template<typename TKernel>
void NodeOperatorNCorrectedConvolutionT<TKernel>::PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                            typename std::map<BlobtreeNode*, typename std::pair<core::AABBox, core::AABBox> >& updated_areas)
{
    UNUSED(epsilon_bbox); UNUSED(accuracy_needed);

    //TODO : @todo : this function should be updated to take into account the correction scalar field
//        this->blend_sum_.PrepareForEval(*this, 0.0, 0.0, updated_areas);
    if(this->epsilon_bbox() != epsilon_bbox)
    {
        core::AABBox bbox_children = blend_sum_main_seg_.PrepareForEval(this->children(), epsilon_bbox, accuracy_needed,updated_areas);
        core::AABBox bbox_seg_corrector = blend_sum_corrector_seg_.PrepareForEval(segment_corrector_,epsilon_bbox, accuracy_needed,updated_areas);
        core::AABBox bbox_union = bbox_children.Union(bbox_seg_corrector);
        this->set_axis_bounding_box(bbox_union);
        this->set_epsilon_bbox(epsilon_bbox);
        this->UpdateNodeCompacity();

        // need an update but should not affect the bounding box ... nor the updated area (i think...)
        typename std::map<BlobtreeNode*, typename std::pair<core::AABBox, core::AABBox> > tmp_updated_areas;
        blend_sum_corrector_point_.PrepareForEval(point_corrector_,epsilon_bbox, accuracy_needed,tmp_updated_areas);;
    }

//    if(this->epsilon_bbox() != epsilon_bbox)
//    {
//        Scalar scal_n = (Scalar) this->children().size();
//        core::AABBox bt_node_abb = core::AABBox(); // this auxilliary is there to avoid calling set_axis_bounding_box which implies calls to node listeners.
//        for(typename std::vector<BlobtreeNode*>::const_iterator it = this->nonconst_children().begin(); it != this->nonconst_children().end(); ++it)
//        {
//            assert( (*it)!=NULL );
//            (*it)->PrepareForEval(epsilon_bbox, accuracy_needed/scal_n, updated_areas);
//            bt_node_abb = core::AABBox::Union(bt_node_abb, (*it)->axis_bounding_box());
//        }

//        this->set_axis_bounding_box(bt_node_abb);
//        this->set_epsilon_bbox(epsilon_bbox);
//    }
}

// @todo implement it correctly
template<typename TKernel>
typename NodeOperatorNCorrectedConvolutionT<TKernel>::core::AABBox NodeOperatorNCorrectedConvolutionT<TKernel>::GetAxisBoundingBox(Scalar value) const
{
    return this->blend_sum_.GetAxisBoundingBox(this->children(), value*0.001);
}


template<typename TKernel>
void NodeOperatorNCorrectedConvolutionT<TKernel>::AddChild(BlobtreeNode* bt_node)
{
    assert(bt_node->parent() == NULL); // please disconnect the node from its parent before anything.
    assert(bt_node != NULL && this->arity_ == -1);

    if(bt_node->parent() != NULL)
    {
        bt_node->nonconst_parent()->RemoveChild(bt_node);
    }
    bt_node->set_parent(this);
    this->children_.push_back(bt_node);

    // Test if the bt_node is a NodeScalarField and, more precisely,
    // if it contains a SegmentSField<TFunctorKernel> as ScalarField
    assert( dynamic_cast<NodeScalarField *>(bt_node) != NULL);
    ScalarField *scalar_field = const_cast<ScalarField *>((static_cast<NodeScalarField *>(bt_node))->scalar_field());
    assert( dynamic_cast<SegmentSField *>(scalar_field) != NULL);
    SegmentSField *segment_field = static_cast<SegmentSField *>(scalar_field);
    
    TKernel ker = segment_field->eval_functor();
    ker.set_scale(kernel_radius_factor_);
    segment_field->set_eval_functor(ker);

    this->InvalidateBoundingBox();
}


//TODO : @todo : a remplacer (sans doute pas optimal & pas valable pour tout les mélanges !!
template<typename TKernel>
typename NodeOperatorNCorrectedConvolutionT<TKernel>::Point NodeOperatorNCorrectedConvolutionT<TKernel>::GetAPointAt(Scalar value, Scalar epsilon) const
{
    Point res = Point::vectorized(0.0);
    typename std::vector<BlobtreeNode*>::const_iterator it = this->children_.begin();
    while(it != this->children_.end() && res[0]==0.0 && res[1]==0.0 && res[2]==0.0)
    {
        Point init_point = (*it)->GetAPointAt(value, epsilon);
        Vector search_dir = this->EvalGrad(init_point, 0.0001); // arbitrary
        search_dir.normalize();

        // Accuracy for gradient computation
        // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
        Scalar grad_epsilon = 0.0001;   // arbitrary
        if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }
        double r_min = -30.0, r_max = 30.0;
        ConvergenceT<Traits>::SafeNewton1D(
                                                        *(static_cast<const Base*>(this)),
                                                        init_point,
                                                        search_dir,
                                                        r_min,  //TODO : arbitrary                                // security minimum bound
                                                        r_max,    //TODO : arbitrary
                                                        0.0,            // point at which we start the search, given in 1D
                                                        value,
                                                        epsilon,
                                                        grad_epsilon,
                                                        20,
                                                        res);
        ++it;
    }

    return res;
}

template<typename TKernel>
class ClonedNodeOperatorNCorrectedConvolutionT :
        public NodeOperatorNCorrectedConvolutionT<TKernel>
{
public:
    typedef NodeOperatorNCorrectedConvolutionT<TKernel> Base;

    ClonedNodeOperatorNCorrectedConvolutionT(const Base &rhs) : Base(rhs)
    {
        //TODO:@todo : the main tree should be up-to-date otherwise ... argh !
        this->blend_sum_main_seg_.PrepareGrid(this->axis_bounding_box(), this->children());
        this->blend_sum_corrector_seg_.PrepareGrid(this->axis_bounding_box(), this->segment_corrector_);
        this->blend_sum_corrector_point_.PrepareGrid(this->axis_bounding_box(), this->point_corrector_);
    }

    ~ClonedNodeOperatorNCorrectedConvolutionT()
    {
        for (typename std::set<NodeScalarField*>::iterator it = Base::point_corrector_.begin();
             it != Base::point_corrector_.end(); ++it)
            delete *it;
        for (typename std::set<NodeScalarField*>::const_iterator it = Base::segment_corrector_.begin();
             it != Base::segment_corrector_.end(); ++it)
            delete *it;
    }
};

template<typename TKernel>
NodeOperatorNCorrectedConvolutionT<TKernel> *NodeOperatorNCorrectedConvolutionT<TKernel>::clone() const
{
    return new ClonedNodeOperatorNCorrectedConvolutionT<TKernel>(*this);
}

*/

} // Close namespace convol

} // Close namespace expressive

// implementation file
#include <convol/blobtreenode/nodeoperator/CorrectedSumIntegralSurfaceT.hpp>

#endif // CONVOL_NODE_CORRECTED_INTEGRAL_SURFACE_H_


