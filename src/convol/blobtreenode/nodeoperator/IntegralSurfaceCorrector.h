/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: IntegralSurfaceCorrector.h
 
 Language: C++
 
 License: expressive license
 
 \author: Cedric Zanni
 E-Mail:  cedric.zanni@inrialpes.fr
 
 Description: Class for correcting field-value in the case of a graph-based integral surface.
              This kind of object can be inserted in some BlobtreeNode such as 
                   - CorrectedISSum
                   - GradientBasedIntegralSurface
                   - WitnessedIntegralSurface

              WARNING : the standard kernel template parameter should be the Standard Kernel "associated"
                        to the HomotheticKernel used for the BlobtreeNode to be corrected => it is not used anymore

              TODO:@todo : for now does not take into account the change in the tree to update the correctors
                           it should probably listen the blobtree node associated (for the AddChild and RemoveChild)

              TODO:@todo : a question to be answered is the degree of the kernel used for point corrector
                           we currently use the same degree as for the kernel on segment but it is probably better to use
                           the degree - 1 as the point in GradientBasedIntegralSurfaces

 Platform Dependencies: None

*/

#pragma once
#ifndef CONVOL_INTEGRAL_SURFACE_CORRECTOR_T_H_
#define CONVOL_INTEGRAL_SURFACE_CORRECTOR_T_H_

// core dependencies
#include <core/CoreRequired.h>
    // geometry
    #include <core/geometry/AABBox.h>
    #include <core/geometry/WeightedPointWithDensity.h>
    #include <core/geometry/CsteWeightedSegmentWithDensity.h>
    // graph
    #include <core/graph/Graph.h>

// convol dependencies
    #include <convol/functors/kernels/Kernel.h>
    #include <convol/ScalarField.h>
    #include <convol/blobtreenode/BlobtreeNode.h>
    #include <convol/primitives/NodeSegmentSFieldT.h>

    #include <convol/tools/FBVHKernelEval.h>
    #include <convol/properties/Properties0D.h>

namespace expressive {

namespace convol {


class CONVOL_API AbstractIntegralSurfaceCorrector
        : public core::WeightedGraphListener
{
public:
    
    EXPRESSIVE_MACRO_NAME("IntegralSurfaceCorrector") // This macro defined functions

    

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractIntegralSurfaceCorrector(BlobtreeNode* bt_node)
        : core::WeightedGraphListener(), bt_node_(bt_node), graph_(nullptr) {}
    //-------------------------------------------------------------------------
    virtual ~AbstractIntegralSurfaceCorrector() {}

    virtual std::shared_ptr<AbstractIntegralSurfaceCorrector> clone()
    {
        assert(false);
        return nullptr;
    }

    /**
     * @brief Init initialize the corrector, creating the link between
     * the skeleton and the blobtree node, it require the blobtreenode's children to be already
     * well initialized.
     * @param graph the graph which edges can  be element of the blobtreenode
     */
    void Init(expressive::core::WeightedGraph* graph)
    {
        graph_ = graph;
        core::WeightedGraphListener::Init(graph);

        // this is a brut force initialization,
        // it is fully completed after the first PrepareForEval
        auto vertices = graph_->GetVertices();
        while(vertices->HasNext()) {
            VertexAdded(vertices->NextId());
        }
    }

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * /*element*/) const
    {
        //TODO:@todo
    }

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual Scalar Eval(const Point& point) const = 0;
    virtual Vector EvalGrad(const Point& point, Scalar epsilon_grad) const = 0;
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const = 0;

    /**
     * @brief PrepareForEval This function compute the new primitive corrector from the set of VertexId modified_vertices_.
     * @param epsilon_bbox
     * @return bounding_box of the created primitives
     */
    core::AABBox PrepareForEval(Scalar epsilon_bbox);

    ///////////////////////////////
    /// Graph Listener Function ///
    ///////////////////////////////

    virtual void Notify(const core::WeightedGraph::GraphChanges &changes);

    virtual void EdgeAdded(expressive::core::EdgeId);
    virtual void EdgeUpdated(expressive::core::EdgeId);
    virtual void EdgeRemoved(core::EdgeId);
    virtual void VertexUpdated(core::VertexId);
    virtual void VertexAdded(core::VertexId);
    virtual void VertexRemoved(core::VertexId);

    virtual void updated(core::WeightedGraph * /*g*/) {}

    /**
     * @brief IsNodeVertices Check if VertexId id belong to a child primitive of this.
     * @warning this is currently a brut force search !
     */
    bool IsNodeVertices(core::VertexId id);

    ///////////////////////////
    /// Correctors creation ///
    ///////////////////////////

    /**
     * @brief CreateCorrectors create the correctors associated to a given Vertex,
     * provided they are required, and store them in corrector sets (defined in templated class).
     * @param v_id the id of the vertex for which corrector should be computed
     * @param epsilon_bbox
     * @param accuracy_needed
     * @return the bounding box associated to the created correctors
     */
    virtual core::AABBox CreateCorrectors(core::VertexId v_id, Scalar epsilon_bbox) = 0;
    /**
     * @brief EraseCorrectors remove the primitive associated to id from
     * the corrector sets (defined in templated derived class).
     * @param id
     */
    virtual void EraseCorrectors(core::VertexId id) = 0;

protected:
    // Deep copy constructor :
    // It should only be called by the protected subclass copy constructors
    // which should only be called by the "clone" function
    // listener_set is not copyed !
    AbstractIntegralSurfaceCorrector(const AbstractIntegralSurfaceCorrector &rhs)
        : core::WeightedGraphListener(),
          segment_length_corrector_(rhs.segment_length_corrector_),
          point_corrector_(rhs.point_corrector_),
          segment_corrector_(rhs.segment_corrector_)
    {
        assert(rhs.modified_vertices_.size() == 0 );
    }

    /////////////////
    /// Attributs ///
    /////////////////

protected:

    BlobtreeNode* bt_node_;
    expressive::core::WeightedGraph* graph_;

    Scalar segment_length_corrector_;

    /** @brief point_corrector_ point correctors associated to vertices */
    std::map<core::VertexId, std::pair<core::AABBox, core::WeightedPointWithDensity> >  point_corrector_;

    /** @brief segment_corrector_ segment correctors associated to vertices */
    std::map<core::VertexId, std::pair<core::AABBox, core::CsteWeightedSegmentWithDensity> >  segment_corrector_;

private:
    /**
     * @brief modified_vertices_ it is use to temporary store the vertices of
     * modified primitive and the 1-ring neighborhood until the next PrepareForEval
     */
    std::set<core::VertexId> modified_vertices_;
};



template<typename TFunctorKernel, typename TStandardKernel>
class OptIntegralSurfaceCorrectorT;


template< typename THomotheticKernel, typename TStandardKernel>
class IntegralSurfaceCorrector
        : public AbstractIntegralSurfaceCorrector
{
public:
    

    typedef core::WeightedPoint WeightedPoint;
    typedef NodeSegmentSFieldT<TStandardKernel> NSFSegmentStandardKernel;

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    /**
     * @brief IntegralSurfaceCorrector Create a corrector that still have to be initialized
     * by calling the function Init with the right graph as parameter.
     * @param ker
     * @param bt_node
     */
    IntegralSurfaceCorrector(const THomotheticKernel &ker, BlobtreeNode* bt_node)
        : AbstractIntegralSurfaceCorrector(bt_node),
          homothetic_kernel_(ker)
    {
        ComputeSegmentCorrectorLengthForUnitWeight();
    }
    //-------------------------------------------------------------------------
    virtual ~IntegralSurfaceCorrector() {}

    /**
     * @brief clone create a duplicate of the corrector with an optimization structure to
     * accelerate Eval function.
     * @return
     */
    virtual std::shared_ptr<AbstractIntegralSurfaceCorrector> clone()
    {
//        return std::make_shared<IntegralSurfaceCorrector<THomotheticKernel, TStandardKernel> > (*this);
        return std::make_shared<OptIntegralSurfaceCorrectorT<THomotheticKernel, TStandardKernel> > (*this);
    }

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * /*element*/) const
    {
        //TODO:@todo
    }

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual Scalar Eval(const Point& point) const;
    virtual Vector EvalGrad(const Point& point, Scalar epsilon_grad) const;
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;

    /**
     * @brief ComputeSegmentCorrectorLengthForUnitWeight
     *        This function compute the length of a segment corrector that is required to have
     *        the right thickness at the end of an half-infinite line with weight equal to one.
     *        It update segment_length_corrector_ and return the value found.
     *
     * @todo : it would be better to compute this value with a closed-form formula
     * @todo : this value probably depend on the kind of NodeOperator used...
     *         for now, it is based on a classic sum operator
     */
    Scalar ComputeSegmentCorrectorLengthForUnitWeight();

    ///////////////////////////
    /// Correctors creation ///
    ///////////////////////////

    virtual core::AABBox CreateCorrectors(core::VertexId v_id, Scalar epsilon_bbox);

    virtual void EraseCorrectors(core::VertexId id)
    {
        point_corrector_.erase(id);
        segment_corrector_.erase(id);
    }

public:
    // Deep copy constructor :
    // It should only be called by the protected subclass copy constructors
    // which should only be called by the "clone" function
    // listener_set is not copyed !
    IntegralSurfaceCorrector(const IntegralSurfaceCorrector &rhs)
        : AbstractIntegralSurfaceCorrector(rhs), homothetic_kernel_(rhs.homothetic_kernel_) {}

protected:

THomotheticKernel homothetic_kernel_;
};



template<typename TFunctorKernel, typename TStandardKernel>
class OptIntegralSurfaceCorrectorT : public IntegralSurfaceCorrector<TFunctorKernel, TStandardKernel>
{
public:
    EXPRESSIVE_MACRO_NAME("OptIntegralSurfaceCorrector") // This macro defined functions

    OptIntegralSurfaceCorrectorT(const IntegralSurfaceCorrector<TFunctorKernel, TStandardKernel> &rhs)
        : IntegralSurfaceCorrector<TFunctorKernel, TStandardKernel>(rhs), f_bvh_point_(4), f_bvh_segment_(4)
    {
        core::AABBox aabb_segment = core::AABBox();
        for(auto &node_segment : this->segment_corrector_) {
            aabb_segment.Enlarge(node_segment.second.first);
            f_bvh_segment_.data().push_back( std::make_pair(node_segment.second.first,
                                                        std::make_pair( node_segment.second.second, Properties0D()) ) );
            f_bvh_segment_.barycenter().push_back( node_segment.second.second.Barycenter() );
        }
        f_bvh_segment_.Init(aabb_segment);

        core::AABBox aabb_point = core::AABBox();
        for(auto &node_point : this->point_corrector_) {
            aabb_point.Enlarge(node_point.second.first);
            f_bvh_point_.data().push_back( std::make_pair(node_point.second.first,
                                                          std::make_pair( node_point.second.second, Properties0D()) ) );
            f_bvh_point_.barycenter().push_back( node_point.second.second );
        }
        f_bvh_point_.Init(aabb_point);
    }

    virtual Scalar Eval(const Point& point) const;
    virtual Vector EvalGrad(const Point& point, Scalar epsilon_grad) const;
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;

protected:
    FBVHKernelEvalT<  std::pair<core::WeightedPointWithDensity, Properties0D>, TFunctorKernel> f_bvh_point_;
    FBVHKernelEvalT<  std::pair<core::CsteWeightedSegmentWithDensity, Properties0D>, TFunctorKernel> f_bvh_segment_;
};


} // Close namespace convol

} // Close namespace expressive

// implementation file
#include <convol/blobtreenode/nodeoperator/IntegralSurfaceCorrector.hpp>

#endif // CONVOL_INTEGRAL_SURFACE_CORRECTOR_T_H_
