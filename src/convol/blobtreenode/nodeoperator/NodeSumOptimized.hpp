
// convol dependencies
    // tools
    #include<convol/tools/ConvergenceTools.h>

namespace expressive {

namespace convol {

#define USE_FBVH



#ifdef USE_FBVH

template<typename TFunctorEvalSegment>
Scalar NodeOptimizedSumT<TFunctorEvalSegment>::Eval(const Point &point) const
{
    Scalar res = 0.0;
    CDataIterator begin;
    CDataIterator end;
    if(f_bvh_.GetDataIterator(point, begin, end))
    {
        for(auto it = begin; it<end; ++it)
        {
            if(it->first.Contains(point)) // probably have to go through a get ... (due to properties)
                res += kernel_.Eval(it->second, point);
        }
    }
    return res;
//    Scalar res = 0.0;
//    const NodeBVH* node = f_bvh_.GetNode(point);
//    if(node) // check if not null
//    {
//        for(unsigned int id = node->first_primitive_; id<node->end_; ++id)
//        {
//            auto &data = f_bvh_.data_[id];
//            if(data.first.Contains(point)) // probably have to go through a get ... (due to properties)
//                res += kernel_.Eval(data.second, point);
//        }
//        for(unsigned int id : node->splitted_data_)
//        {
//            auto &data = f_bvh_.data_[id];
//            if(data.first.Contains(point)) // probably have to go through a get ... (due to properties)
//                res += kernel_.Eval(data.second, point);
//        }
//    }
//    return res;
}

template<typename TFunctorEvalSegment>
Vector NodeOptimizedSumT<TFunctorEvalSegment>::EvalGrad(const Point& point, Scalar epsilon) const
{
    Vector grad_res = Vector::Zero();
    CDataIterator begin;
    CDataIterator end;
    if(f_bvh_.GetDataIterator(point, begin, end))
    {
        for(auto it = begin; it<end; ++it)
        {
            if(it->first.Contains(point)) // probably have to go through a get ... (due to properties)
                grad_res += kernel_.EvalGrad(it->second, point, epsilon);
        }
    }
    return grad_res;

//    Vector grad_res = Vector::Zero();
//    const NodeBVH* node = f_bvh_.GetNode(point);
//    if(node) // check if not null
//    {
//        for(unsigned int id = node->first_primitive_; id<node->end_; ++id)
//        {
//            auto &data = f_bvh_.data_[id];
//            if(data.first.Contains(point)) // probably have to go through a get ... (due to properties)
//                grad_res += kernel_.EvalGrad(data.second, point, epsilon);
//        }
//        for(unsigned int id : node->splitted_data_)
//        {
//            auto &data = f_bvh_.data_[id];
//            if(data.first.Contains(point)) // probably have to go through a get ... (due to properties)
//                grad_res += kernel_.EvalGrad(data.second, point, epsilon);
//        }
//    }
//    return grad_res;
}

template<typename TFunctorEvalSegment>
void NodeOptimizedSumT<TFunctorEvalSegment>::EvalValueAndGrad(const Point& point,
                              Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();

    CDataIterator begin;
    CDataIterator end;
    if(f_bvh_.GetDataIterator(point, begin, end))
    {
        Scalar value_aux;
        Vector grad_aux;
        for(auto it = begin; it<end; ++it)
        {
            if(it->first.Contains(point)) { // probably have to go through a get ... (due to properties)
                kernel_.EvalValueAndGrad(it->second, point, epsilon_grad, value_aux, grad_aux);
                value_res += value_aux;
                grad_res += grad_aux;
            }
        }
    }

//    value_res = 0.0;
//    grad_res = Vector::Zero();

//    const NodeBVH* node = f_bvh_.GetNode(point);
//    if(node) // check if not null
//    {
//        Scalar value_aux;
//        Vector grad_aux;
//        for(unsigned int id = node->first_primitive_; id<node->end_; ++id)
//        {
//            auto &data = f_bvh_.data_[id];
//            if(data.first.Contains(point)) { // probably have to go through a get ... (due to properties)
//                kernel_.EvalValueAndGrad(data.second, point, epsilon_grad, value_aux, grad_aux);
//                value_res += value_aux;
//                grad_res += grad_aux;
//            }
//        }
//        for(unsigned int id : node->splitted_data_)
//        {
//            auto &data = f_bvh_.data_[id];
//            if(data.first.Contains(point)) { // probably have to go through a get ... (due to properties)
//                kernel_.EvalValueAndGrad(data.second, point, epsilon_grad, value_aux, grad_aux);
//                value_res += value_aux;
//                grad_res += grad_aux;
//            }
//        }
//    }
}

template<typename TFunctorEvalSegment>
void NodeOptimizedSumT<TFunctorEvalSegment>::EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                           const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                           const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                           Scalar& value_res, Vector& grad_res,
                                           std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                           std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                           ) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();
    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
    {
        scal_prop_res[i] = 0.0;
    }
    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
    {
        vect_prop_res[i] = Vector::Zero();
    }

    CDataIterator begin;
    CDataIterator end;
    if(f_bvh_.GetDataIterator(point, begin, end))
    {
        Scalar value_aux;
        Vector grad_aux;
        std::vector<Scalar> scal_prop_aux;
        scal_prop_aux.resize(scal_prop_ids.size());
        std::vector<Vector> vect_prop_aux;
        vect_prop_aux.resize(vect_prop_ids.size());

        for(auto it = begin; it<end; ++it)
        {
            if(it->first.Contains(point)) { // probably have to go through a get ... (due to properties)
                kernel_.EvalValueAndGrad(it->second, point, epsilon_grad, value_aux, grad_aux);
                value_res += value_aux;
                grad_res += grad_aux;

                //TODO:@todo : does not handle yet properties ...
            }
        }
    }

    if(value_res != 0.0)
    {
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = scal_prop_res[i]/value_res;
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = vect_prop_res[i]/value_res;
        }
    }



//    value_res = 0.0;
//    grad_res = Vector::Zero();
//    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
//    {
//        scal_prop_res[i] = 0.0;
//    }
//    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
//    {
//        vect_prop_res[i] = Vector::Zero();
//    }

//    const NodeBVH* node = f_bvh_.GetNode(point);
//    if(node) // check if not null
//    {
//        Scalar value_aux;
//        Vector grad_aux;
//        std::vector<Scalar> scal_prop_aux;
//        scal_prop_aux.resize(scal_prop_ids.size());
//        std::vector<Vector> vect_prop_aux;
//        vect_prop_aux.resize(vect_prop_ids.size());
//        for(unsigned int id = node->first_primitive_; id<node->end_; ++id)
//        {
//            auto &data = f_bvh_.data_[id];
//            if(data.first.Contains(point)) { // probably have to go through a get ... (due to properties)
//                kernel_.EvalValueAndGrad(data.second, point, epsilon_grad, value_aux, grad_aux);
//                value_res += value_aux;
//                grad_res += grad_aux;

//                //TODO:@todo : does not handle yet properties ...
//            }
//        }

//        //TODO:@todo : I dislike this additional loop, I would probably prefer to have duplicate data in first loop...
//        for(unsigned int id : node->splitted_data_)
//        {
//            auto &data = f_bvh_.data_[id];
//            if(data.first.Contains(point)) { // probably have to go through a get ... (due to properties)
//                kernel_.EvalValueAndGrad(data.second, point, epsilon_grad, value_aux, grad_aux);
//                value_res += value_aux;
//                grad_res += grad_aux;

//                //TODO:@todo : does not handle yet properties ...
//            }
//        }
//    }

/////////////////////////////////////////////////
//            //TODO:@todo : the parametrization of the properties is now wrong since it is based on the original point and not the one of the opt segment
//            Vector p1_to_p = point - seg.p_min();//seg.p1();
//            Scalar s = p1_to_p.dot(seg.increase_unit_dir()); //seg.unit_dir();
/////////////////////////////////////////////////
//            s = s/seg.length(); //TODO:@todo : computation of s is redundant with what is done in the functor
//            std::get<2>(seg_info).InlineEvalProperties(s, scal_prop_ids, vect_prop_ids, scal_prop_aux, vect_prop_aux);
////            std::get<1>(seg_info).EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, value_aux, grad_aux, scal_prop_aux, vect_prop_aux);

//            for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
//            {
//                scal_prop_res[i] += value_aux*scal_prop_aux[i];
//            }
//            for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
//            {
//                vect_prop_res[i] += value_aux*vect_prop_aux[i];
//            }

//    if(value_res != 0.0)
//    {
//        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
//        {
//            scal_prop_res[i] = scal_prop_res[i]/value_res;
//        }
//        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
//        {
//            vect_prop_res[i] = vect_prop_res[i]/value_res;
//        }
//    }

}

#else
template<typename TFunctorEvalSegment>
Scalar NodeOptimizedSumT<TFunctorEvalSegment>::Eval(const Point &point) const
{
    Scalar res = 0.0;
    for(auto &seg_info : vec_seg_)
    {
        if(std::get<0>(seg_info).Contains(point))
        {
            res += kernel_.Eval(std::get<1>(seg_info), point);
        }
    }
    return res;
}

template<typename TFunctorEvalSegment>
Vector NodeOptimizedSumT<TFunctorEvalSegment>::EvalGrad(const Point& point, Scalar epsilon) const
{
    Vector grad_res = Vector::Zero();
    for(auto &seg_info : vec_seg_)
    {
        if(std::get<0>(seg_info).Contains(point))
        {
            grad_res += kernel_.EvalGrad(std::get<1>(seg_info), point, epsilon);
        }
    }
    return grad_res;
}

template<typename TFunctorEvalSegment>
void NodeOptimizedSumT<TFunctorEvalSegment>::EvalValueAndGrad(const Point& point,
                              Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();

    Scalar value_aux;
    Vector grad_aux;
    for(auto &seg_info : vec_seg_)
    {
        if(std::get<0>(seg_info).Contains(point))
        {
            kernel_.EvalValueAndGrad(std::get<1>(seg_info), point, epsilon_grad, value_aux, grad_aux);
            value_res += value_aux;
            grad_res += grad_aux;
        }
    }

    //        return blend_operator_.EvalValueAndGrad(this->children_, point, epsilon_grad, value_res, grad_res);
}

template<typename TFunctorEvalSegment>
void NodeOptimizedSumT<TFunctorEvalSegment>::EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                           const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                           const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                           Scalar& value_res, Vector& grad_res,
                                           std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                           std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                           ) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();
    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
    {
        scal_prop_res[i] = 0.0;
    }
    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
    {
        vect_prop_res[i] = Vector::Zero();
    }

    Scalar value_aux;
    Vector grad_aux;
    std::vector<Scalar> scal_prop_aux;
    scal_prop_aux.resize(scal_prop_ids.size());
    std::vector<Vector> vect_prop_aux;
    vect_prop_aux.resize(vect_prop_ids.size());

    for(auto &seg_info : vec_seg_)
    {
        if(std::get<0>(seg_info).Contains(point))
        {
            const core::OptWeightedSegment &seg = std::get<1>(seg_info);
            kernel_.EvalValueAndGrad(seg, point, epsilon_grad, value_aux, grad_aux);
            value_res += value_aux;
            grad_res += grad_aux;
/*
///////////////////////////////////////////////
            //TODO:@todo : the parametrization of the properties is now wrong since it is based on the original point and not the one of the opt segment
            Vector p1_to_p = point - seg.p_min();//seg.p1();
            Scalar s = p1_to_p.dot(seg.increase_unit_dir()); //seg.unit_dir();
///////////////////////////////////////////////
            s = s/seg.length(); //TODO:@todo : computation of s is redundant with what is done in the functor
            std::get<2>(seg_info).InlineEvalProperties(s, scal_prop_ids, vect_prop_ids, scal_prop_aux, vect_prop_aux);
//            std::get<1>(seg_info).EvalValueAndGradAndProperties(point, epsilon_grad, scal_prop_ids, vect_prop_ids, value_aux, grad_aux, scal_prop_aux, vect_prop_aux);

            for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
            {
                scal_prop_res[i] += value_aux*scal_prop_aux[i];
            }
            for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
            {
                vect_prop_res[i] += value_aux*vect_prop_aux[i];
            }
*/
        }
    }

    if(value_res != 0.0)
    {
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = scal_prop_res[i]/value_res;
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = vect_prop_res[i]/value_res;
        }
    }

}
#endif

    //TODO:@todo : a remplacer (sans doute pas optimal & pas valable pour tout les mélanges !! (notament soustraction))
    template<typename TFunctorEvalSegment>
    Point NodeOptimizedSumT<TFunctorEvalSegment>::GetAPointAt(Scalar value, Scalar epsilon) const
    {
        //TODO:@todo : this should be updated !

        Point res = Point::Zero();
        auto it = vec_seg_.begin();
        while(it != vec_seg_.end() && res[0]==0.0 && res[1]==0.0 && res[2]==0.0)
        {
            Point init_point;
//            Point init_point = (*it)->GetAPointAt(value, epsilon);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            const core::OptWeightedSegment &seg = std::get<1>(*it);

            Vector search_dir = expressive::VectorTools::GetOrthogonalRandVect(seg.increase_unit_dir());
            // @todo this normamilization could be useless depending on GetOrthogonalRandVect... this last function should maybe return a normalized vector if the given vector is normalized...
            search_dir.normalize();

            // Accuracy for gradient computation
            // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
            Scalar grad_epsilon = seg.length()*0.0001;    // arbitrary
            if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }

            Scalar max_bound = kernel_.GetEuclidDistBound(seg, value);
            if(max_bound != 0.0)
            {
//                Scalar min_bound = 0.0;
//                Scalar start_absc = max_bound/2.0;

//                Point res;
//                Convergence::SafeNewton1D(
//                            *this,
//                            (seg.points_[0]+seg.points_[1])/2.0, search_dir, // together give the line on which we step
//                            min_bound,                                // security minimum bound
//                            max_bound,                // security maximum bound
//                            start_absc,            // point at which we start the search, given in 1D
//                            value,
//                            epsilon,
//                            grad_epsilon,
//                            10,
//                            res);
                init_point = (seg.p_min() + 0.5*seg.length()*seg.increase_unit_dir()) + search_dir*(seg.weight_min()+0.5*seg.length()*seg.unit_delta_weight());
                // (seg.p1()+seg.p2())/2.0 + search_dir*((seg.weight_p1()+seg.weight_p2())*0.5);
            }
            else
            {
                init_point = (seg.p_min() + 0.5*seg.length()*seg.increase_unit_dir());
                //(seg.p1()+seg.p2())/2.0;   // The surface does not exist, we return the middle of the segment
            }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            search_dir = this->EvalGrad(init_point, 0.0001); // arbitrary
            search_dir.normalize();

            // Accuracy for gradient computation
            // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
            grad_epsilon = 0.0001;   // arbitrary
            if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }
            double r_min = -30.0, r_max = 30.0;
            Convergence::SafeNewton1D(
                        *(static_cast<const BlobtreeNode*>(this)),
                        init_point,
                        search_dir,
                        r_min,  //TODO : arbitrary                                // security minimum bound
                        r_max,    //TODO : arbitrary
                        0.0,            // point at which we start the search, given in 1D
                        value,
                        epsilon,
                        grad_epsilon,
                        20,
                        res);
            ++it;
        }

        return res;
    }


    template<typename TFunctorEvalSegment>
    void NodeOptimizedSumT<TFunctorEvalSegment>::GetPrimitivesPointsAt(Scalar value, Scalar epsilon, core::PointCloud& res_points, const AABBox& b_box) const
    {
        core::PointCloud res_points_tmp;

        for(auto &seg_info : vec_seg_)
        {
            if( b_box.Intersect(std::get<0>(seg_info)) )
            {
                Point init_point;
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            const core::OptWeightedSegment &seg = std::get<1>(seg_info);

                            Vector search_dir = expressive::VectorTools::GetOrthogonalRandVect(seg.increase_unit_dir());
                            // @todo this normamilization could be useless depending on GetOrthogonalRandVect... this last function should maybe return a normalized vector if the given vector is normalized...
                            search_dir.normalize();

                            // Accuracy for gradient computation
                            // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
                            Scalar grad_epsilon = seg.length()*0.0001;    // arbitrary
                            if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }

                            Scalar max_bound = kernel_.GetEuclidDistBound(seg, value);
                            if(max_bound != 0.0)
                            {
                //                Scalar min_bound = 0.0;
                //                Scalar start_absc = max_bound/2.0;

                //                Point res;
                //                Convergence::SafeNewton1D(
                //                            *this,
                //                            (seg.points_[0]+seg.points_[1])/2.0, search_dir, // together give the line on which we step
                //                            min_bound,                                // security minimum bound
                //                            max_bound,                // security maximum bound
                //                            start_absc,            // point at which we start the search, given in 1D
                //                            value,
                //                            epsilon,
                //                            grad_epsilon,
                //                            10,
                //                            res);
                                init_point = (seg.p_min() + 0.5*seg.length()*seg.increase_unit_dir()) + search_dir*(seg.weight_min()+0.5*seg.length()*seg.unit_delta_weight());
                                // (seg.p1()+seg.p2())/2.0 + search_dir*((seg.weight_p1()+seg.weight_p2())*0.5);
                            }
                            else
                            {
                                init_point = (seg.p_min() + 0.5*seg.length()*seg.increase_unit_dir());
                                //(seg.p1()+seg.p2())/2.0;   // The surface does not exist, we return the middle of the segment
                            }

                            res_points.insert(init_point, this->EvalGrad(init_point, 0.0001));
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            }

        }

//        for(typename std::vector<shared_ptr<BlobtreeNode> >::const_iterator it = this->children_.begin(); it != this->children_.end(); it++)
//        {
//            if(b_box.Intersect((*it)->axis_bounding_box()))
//            {
//                (*it)->GetPrimitivesPointsAt(value, epsilon, res_points_tmp, b_box);
//                res_points.splice(res_points_tmp);
//            }
//        }





        // Now converge for all point according to the result of the node.
        Scalar bbox_size = this->axis_bounding_box_.ComputeSizeMax();
        // temporary variables for convergence function
        Scalar min_absc,max_absc;
        Point origin;
        for(auto it = res_points.begin(); it != res_points.end();)
        {
            Point& p = res_points.point(*it);
            Normal& n = res_points.normal(*it);

            if(n[0]!=0.0 || n[1]!=0.0 || n[2]!=0.0){
                min_absc = -bbox_size;
                max_absc = bbox_size;
                origin = p;
                Convergence::SafeNewton1D(  *this,
                                                origin,
                                                n,
                                                min_absc,
                                                max_absc,
                                                0.0,
                                                value,
                                                epsilon,
                                                epsilon*0.1,    // arbitrarily divide by 10 to have a better gradient estimation in case of numerical computing
                                                10,
                                                p);
                // Update the normal to the point
                n = -this->EvalGrad(p,epsilon*0.1);
                n.normalize(); //TODO:@todo  following function does not exist in eigen, in some cases, can be dangerous //n.normalize_cond();
                ++it;
            }
            else
            {
                // Discard the point since it's proabbly outside anything useful...
                it = res_points.erase(it);
            }
        }
    }

    /////////////////////////////
    // BoundingBox computation //
    /////////////////////////////

//    template<typename TFunctorEvalSegment>
//    AABBox NodeOptimizedSumT<TFunctorEvalSegment>::GetAxisBoundingBox(Scalar value) const
//    {
//        //TODO:@todo : arg : return the old value ...
//        AABBox res = AABBox();
//        for(auto &seg_info : vec_seg_)
//        {
//            res = AABBox::Union(res,std::get<0>(seg_info));
//        }
//        return res;

////        for(typename std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = blobtree_node_vector.begin(); it!=blobtree_node_vector.end(); ++it)
////        {
////            res = AABBox::Union(res,(*it)->GetAxisBoundingBox(value/((Scalar) blobtree_node_vector.size())));
////        }
////        return res;
//    }



} // Close namespace convol

} // Close namespace expressive

