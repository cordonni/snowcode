#include <convol/blobtreenode/nodeoperator/GradientBasedIntegralSurfaceT.h>


// core dependencies
#include <core/xmlloaders/TraitsXmlQuery.h>

// convol dependencies
#include <convol/factories/BlobtreeNodeFactory.h>

// List of all functor operator that can be used with a NodeOperator
// (this is required to register them in the BlobtreeNodeFacotry)
#include <convol/functors/kernels/cauchyinverse/HomotheticInverse.h>
#include <convol/functors/kernels/cauchyinverse/CompactedHomotheticInverse.h>

namespace expressive {

namespace convol {

void AbstractGradientBasedIntegralSurface::InitFromXml(const TiXmlElement *element)
{
    double special_parameter;
    core::TraitsXmlQuery::QueryDoubleAttribute("AbstractGradientBasedIntegralSurface::InitFromXml",
                                               element, "special_parameter", &special_parameter);
    set_special_parameter(special_parameter);
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Register GradientBasedIntegralSurfaceT<FunctorOperator> in the BlobtreeNodeFactory ///
//////////////////////////////////////////////////////////////////////////////////////////

static BlobtreeNodeFactory::Type<  HomotheticInverse3, GradientBasedIntegralSurfaceT<HomotheticInverse3> >
                                                    GradientBasedIntegralSurfaceT_HomotheticInverse3_Type;
static BlobtreeNodeFactory::Type<  HomotheticInverse4, GradientBasedIntegralSurfaceT<HomotheticInverse4> >
                                                    GradientBasedIntegralSurfaceT_HomotheticInverse4_Type;
static BlobtreeNodeFactory::Type<  HomotheticInverse5, GradientBasedIntegralSurfaceT<HomotheticInverse5> >
                                                    GradientBasedIntegralSurfaceT_HomotheticInverse5_Type;
static BlobtreeNodeFactory::Type<  HomotheticInverse6, GradientBasedIntegralSurfaceT<HomotheticInverse6> >
                                                    GradientBasedIntegralSurfaceT_HomotheticInverse6_Type;

static BlobtreeNodeFactory::Type<  CompactedHomotheticInverse3, GradientBasedIntegralSurfaceT<CompactedHomotheticInverse3> >
                                                    GradientBasedIntegralSurfaceT_CompactedHomotheticInverse3_Type;
static BlobtreeNodeFactory::Type<  CompactedHomotheticInverse4, GradientBasedIntegralSurfaceT<CompactedHomotheticInverse4> >
                                                    GradientBasedIntegralSurfaceT_CompactedHomotheticInverse4_Type;
static BlobtreeNodeFactory::Type<  CompactedHomotheticInverse5, GradientBasedIntegralSurfaceT<CompactedHomotheticInverse5> >
                                                    GradientBasedIntegralSurfaceT_CompactedHomotheticInverse5_Type;
static BlobtreeNodeFactory::Type<  CompactedHomotheticInverse6, GradientBasedIntegralSurfaceT<CompactedHomotheticInverse6> >
                                                    GradientBasedIntegralSurfaceT_CompactedHomotheticInverse6_Type;


} // Close namespace convol

} // Close namespace expressive

