
namespace expressive {

namespace convol {

////////////////////////////////////
/// CorrectedSumIntegralSurfaceT ///
////////////////////////////////////

template<typename TFunctorKernel>
std::shared_ptr<ScalarField> CorrectedSumIntegralSurfaceT<TFunctorKernel>::CloneForEval() const
{
//      return std::make_shared<CorrectedSumIntegralSurfaceT>(*this);
    return std::make_shared<OptCorrectedSumIntegralSurfaceT<TFunctorKernel> >(*this);
}

/////////////////
/// Modifiers ///
/////////////////

template<typename TFunctorKernel>
void CorrectedSumIntegralSurfaceT<TFunctorKernel>::AddChild(std::shared_ptr<BlobtreeNode> bt_node)
{
    assert(bt_node->parent() == NULL); // please disconnect the node from its parent before anything.
    assert(bt_node != NULL && this->arity_ == -1);

    if(bt_node->parent() != NULL)
    {
        bt_node->nonconst_parent()->RemoveChild(bt_node);
    }
    bt_node->set_parent(this);
    this->children_.push_back(bt_node);

    // Test if the bt_node is either,
    //      - a NodeSegmentSField<TFunctorKernel>
    //      - a NodeISSubdivisionCurveSField<TFunctorKernel>
    //      - a NodePointSField<TFunctorKernel>
    //      - a NodeTriangleSField<TFunctorKernel>
    if(std::dynamic_pointer_cast<NodeSegmentSFKer>(bt_node) != nullptr)
    {
        static_cast<TFunctorKernel&>(std::static_pointer_cast<NodeSegmentSFKer>(bt_node)->kernel_functor()).set_scale(this->kernel_.scale());
    }
    else if(std::dynamic_pointer_cast<NodeISSubdivisionCurveSFKer>(bt_node) != nullptr)
    {
        static_cast<TFunctorKernel&>(std::static_pointer_cast<NodeISSubdivisionCurveSFKer>(bt_node)->kernel_functor()).set_scale(this->kernel_.scale());
    }
    else if(std::dynamic_pointer_cast<NodePointSFKer>(bt_node) != nullptr)
    {
        static_cast<TFunctorKernel&>(std::static_pointer_cast<NodePointSFKer>(bt_node)->kernel_functor()).set_scale(this->kernel_.scale());
    }
//    else if(std::dynamic_pointer_cast<NodeTriangleSFNumKer>(bt_node) != nullptr)
//    {
//        std::static_pointer_cast<NodeTriangleSFNumKer>(bt_node)->eval_functor().point_contribution().set_scale(this->kernel_.scale());
//    }
    else
    {
        // this primitive/kernel is not supported
        assert(false);
    }

    this->InvalidateBoundingBox();
}

////////////////////////////
/// Evaluation functions ///
////////////////////////////

template<typename TFunctorKernel>
auto CorrectedSumIntegralSurfaceT<TFunctorKernel>::Eval(const Point& point) const -> Scalar
{
    Scalar res = blend_sum_.Eval(this->children_, point);

    if(this->is_corrector_) res += this->is_corrector_->Eval(point);

    return res;
}

template<typename TFunctorKernel>
auto CorrectedSumIntegralSurfaceT<TFunctorKernel>::EvalGrad(const Point& point, Scalar epsilon_grad) const -> Vector
{
    Vector grad_res = blend_sum_.EvalGrad(this->children_, point, epsilon_grad);

    if(this->is_corrector_) grad_res += this->is_corrector_->EvalGrad(point, epsilon_grad);

    return grad_res;
}

template<typename TFunctorKernel>
void CorrectedSumIntegralSurfaceT<TFunctorKernel>::EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
    blend_sum_.EvalValueAndGrad(this->children_, point, epsilon_grad, value_res, grad_res);

    if(this->is_corrector_) {
        Scalar tmp_val;
        Vector tmp_grad;
        this->is_corrector_->EvalValueAndGrad(point, epsilon_grad, tmp_val, tmp_grad);
        value_res += tmp_val;
        grad_res += tmp_grad;
    }
}

template<typename TFunctorKernel>
void CorrectedSumIntegralSurfaceT<TFunctorKernel>::EvalValueAndGradAndProperties(
                        const Point& point, Scalar epsilon_grad,
                        const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                        const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                        Scalar& value_res, Vector& grad_res,
                        std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                        std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                ) const
{
    blend_sum_.EvalValueAndGradAndProperties(this->children_, point, epsilon_grad,
                                             scal_prop_ids, vect_prop_ids,
                                             value_res, grad_res,
                                             scal_prop_res, vect_prop_res);

    if(this->is_corrector_) {
        Scalar tmp_val;
        Vector tmp_grad;
        this->is_corrector_->EvalValueAndGrad(point, epsilon_grad, tmp_val, tmp_grad);
        value_res += tmp_val;
        grad_res += tmp_grad;
    }
}

// Two following function works as a NodeOperatorNAry<BlendSum> ...
template<typename TFunctorKernel>
void CorrectedSumIntegralSurfaceT<TFunctorKernel>::PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                            typename std::map<BlobtreeNode*, typename std::pair<core::AABBox, core::AABBox> >& updated_areas)
{
    core::AABBox bbox_corrector = AbstractIntegralSurfaceOperator::PrepareForEval(epsilon_bbox, accuracy_needed);
    //TODO:@todo : should we use this bounding box ?

    blend_sum_.PrepareForEval(*this, epsilon_bbox, accuracy_needed, updated_areas);

    this->UpdateNodeCompacity();
}

template<typename TFunctorKernel>
core::AABBox CorrectedSumIntegralSurfaceT<TFunctorKernel>::GetAxisBoundingBox(Scalar value) const
{
    //TODO:@todo : we should add the bounding box of the corrector
    return blend_sum_.GetAxisBoundingBox(this->children(), value);
}


/////////////////////////////////////////
///// OptCorrectedSumIntegralSurfaceT ///
/////////////////////////////////////////

template<typename TFunctorKernel>
OptCorrectedSumIntegralSurfaceT<TFunctorKernel>::OptCorrectedSumIntegralSurfaceT(const CorrectedSumIntegralSurfaceT<TFunctorKernel> &rhs)
    :   CorrectedSumIntegralSurfaceT<TFunctorKernel>(rhs), f_bvh_segment_(4)//, f_bvh_triangle_(4), f_bvh_point_(4)
{
    this->parent_ = nullptr;
    this->arity_ = 0;

    this->axis_bounding_box_ = rhs.axis_bounding_box();

    std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
    start_time = std::chrono::high_resolution_clock::now();

    // prevent multiple reallocation of the vector
//        f_bvh_segment_.data().reserve(rhs.nb_children()); //TODO:@todo : this is only ok if there is only NodeSegmentSField !!!

    //TODO:@todo : children should already be initialized
    for(auto &child : this->children_)
//    for(auto &child : rhs.children())
    {
        auto node_segment = std::dynamic_pointer_cast<AbstractNodeSegmentSField>(child);
        if(node_segment != nullptr) {
            auto &opt_w_seg = node_segment->opt_w_seg();
            f_bvh_segment_.data().push_back( std::make_pair(node_segment->axis_bounding_box(),
                                                    std::make_pair(opt_w_seg, node_segment->properties()) ) );
            f_bvh_segment_.barycenter().push_back(  opt_w_seg.Barycenter() );
            continue;
        }

//        auto node_curve = std::dynamic_pointer_cast<AbstractNodeISSubdivisionCurve>(child);
//        if(node_curve != nullptr) {
//            for(auto &pair : node_curve->vec_seg())
//            {
////TODO:@todo : should compute correctly the property to be attached from the unique property of ...
////TODO:@todo : following solution is ok if property is constant
//                f_bvh_segment_.data().push_back( std::make_pair(pair.first,
//                                                                std::make_pair(pair.second,node_curve->properties())) );
//                f_bvh_segment_.barycenter().push_back(pair.second.Barycenter());
//                continue;
//            }
//        }

//        auto node_triangle = std::dynamic_pointer_cast<AbstractNodeTriangleSField>(child);
//        if(node_triangle != nullptr) {
//            auto opt_w_tri = node_triangle->opt_w_tri();
//            f_bvh_triangle_.data().push_back( std::make_pair(node_triangle->axis_bounding_box(),
//                                                    std::make_pair(opt_w_tri, node_triangle->properties()) ) );
//            f_bvh_triangle_.barycenter().push_back(  opt_w_tri.Barycenter() );
//            continue;
//        }

//        auto node_point = std::dynamic_pointer_cast<NodePointSFKer>(child);
//        if(node_point != nullptr) {
//            f_bvh_point_.data().push_back( std::make_pair(node_point->axis_bounding_box(),
//                                                    std::make_pair( *(node_point.get()), node_point->properties()) ) );
//            f_bvh_point_.barycenter().push_back( *(node_point.get()) );
//            continue;
//        }
    }

    f_bvh_segment_.Init(this->axis_bounding_box_);
//    f_bvh_triangle_.Init(this->axis_bounding_box_);
//    f_bvh_point_.Init(this->axis_bounding_box_);

    end_time =  std::chrono::high_resolution_clock::now();
    ork::Logger::DEBUG_LOGGER->logf("POLYGONIZER", "Fitted FBVH construction time : %f", std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000.0);
}


////////////////////////////
/// Evaluation functions ///
////////////////////////////



template<typename TFunctorKernel>
auto OptCorrectedSumIntegralSurfaceT<TFunctorKernel>::Eval(const Point& point) const -> Scalar
{
    Scalar res = 0.0;

    res += f_bvh_segment_.Eval(point, this->kernel_);

    if(this->is_corrector_) res += this->is_corrector_->Eval(point);

    return res;
}

template<typename TFunctorKernel>
Vector OptCorrectedSumIntegralSurfaceT<TFunctorKernel>::EvalGrad(
                        const Point& point, Scalar epsilon_grad
                ) const
{
    Vector grad_res = Vector::Zero();

    grad_res += f_bvh_segment_.EvalGrad(point, epsilon_grad, this->kernel_);

    if(this->is_corrector_) grad_res += this->is_corrector_->EvalGrad(point, epsilon_grad);

    return grad_res;
}

template<typename TFunctorKernel>
void OptCorrectedSumIntegralSurfaceT<TFunctorKernel>::EvalValueAndGrad(
                        const Point& point, Scalar epsilon_grad,
                        Scalar& value_res, Vector& grad_res
                ) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();

    Scalar f_tmp;
    Vector grad_tmp;

    f_bvh_segment_.EvalValueAndGrad(point, epsilon_grad, f_tmp, grad_tmp, this->kernel_);
    value_res += f_tmp;
    grad_res += grad_tmp;

    if(this->is_corrector_) {
        this->is_corrector_->EvalValueAndGrad(point, epsilon_grad, f_tmp, grad_tmp);
        value_res += f_tmp;
        grad_res += grad_tmp;
    }
}


template<typename TFunctorKernel>
void OptCorrectedSumIntegralSurfaceT<TFunctorKernel>::EvalValueAndGradAndProperties(
                        const Point& point, Scalar epsilon_grad,
                        const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                        const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                        Scalar& value_res, Vector& grad_res,
                        std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                        std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                ) const
{
    value_res = 0.0;
    grad_res = Vector::Zero();

    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
    {
        scal_prop_res[i] = 0.0;
    }
    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
    {
        vect_prop_res[i] = Vector::Zero();
    }


    Scalar f_tmp = 0.0;
    Vector grad_tmp = Vector::Zero();
    f_bvh_segment_.EvalValueAndGradAndProperties(point, epsilon_grad, value_res, grad_res,
                                                 scal_prop_ids, vect_prop_ids,
                                                 scal_prop_res, vect_prop_res,
                                                 this->kernel_);
    value_res += f_tmp;
    grad_res += grad_tmp;

    if(value_res > 0.0) {
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = scal_prop_res[i]/value_res;
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = vect_prop_res[i]/value_res;
        }
    }

    // Corrector have no properties => added to value_res after properties normalization
    if(this->is_corrector_) {
        this->is_corrector_->EvalValueAndGrad(point, epsilon_grad, f_tmp, grad_tmp);
        value_res += f_tmp;
        grad_res += grad_tmp;
    }
}
    

} // Close namespace convol

} // Close namespace expressive

