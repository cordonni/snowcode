#include <convol/blobtreenode/BlobtreeRoot.h>

// Core
#include<ork/resource/ResourceTemplate.h>

// Convol tools
#include<convol/tools/ConvergenceTools.h>

using namespace ork;
using namespace expressive::core;
using namespace std;

namespace expressive {

namespace convol {

void LoadBlobtreeNodeChildren(BlobtreeNode *node, std::shared_ptr<ResourceManager> manager, std::shared_ptr<ResourceDescriptor> desc, const TiXmlElement *e);

Scalar BlobtreeRoot::Eval(const Point& point) const
{
    if(this->children_.size() != 0)
    {
        return max_operator_(this->children_, point);
    }
    else
    {
        return 0.0;
    }
}

Vector BlobtreeRoot::EvalGrad(const Point& point, Scalar epsilon) const
{
    if(this->children_.size() != 0)
    {
        return max_operator_.EvalGrad(this->children_, point, epsilon);
    }
    else
    {
        return Vector::Zero();
    }
}

void BlobtreeRoot::EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
{
    if(this->children_.size() != 0)
    {
        return max_operator_.EvalValueAndGrad(this->children_, point, epsilon_grad, value_res, grad_res);
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
    }
}

void BlobtreeRoot::EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                            const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                            const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                            Scalar& value_res, Vector& grad_res,
                                            std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                            std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                            ) const
{
    if(this->children_.size() != 0)
    {
        return max_operator_.EvalValueAndGradAndProperties(this->children_, point, epsilon_grad,
                                                           scal_prop_ids, vect_prop_ids,
                                                           value_res, grad_res,
                                                           scal_prop_res, vect_prop_res);
    }
    else
    {
        value_res = 0.0;
        grad_res = Vector::Zero();
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = ExpressiveTraits::kSFScalarPropertiesDefault[scal_prop_ids[i]];
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = ExpressiveTraits::kSFVectorPropertiesDefault[vect_prop_ids[i]];
        }
    }
}

Point BlobtreeRoot::GetAPointAt(Scalar value, Scalar epsilon) const
{
    //Point res = Point::vectorized(0.0);
    //typename std::vector<BlobtreeNode*>::const_iterator it = this->children_.begin();
    //while(it != this->children_.end() && res[0]==0.0 && res[1]==0.0 && res[2]==0.0)
    //{
    //    res = (*it)->GetAPointAt(value, epsilon);
    //}
    //return res;
    //TODO : @todo : a remplacer (sans doute pas optimal & pas valable pour tout les m�langes !! (notament soustraction))
    Point res = Point::Zero();
    std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = this->children_.begin();
    while(it != this->children_.end() && res[0]==0.0 && res[1]==0.0 && res[2]==0.0)
    {
        Point init_point = (*it)->GetAPointAt(value, epsilon);
        Vector search_dir = this->EvalGrad(init_point, 0.0001); // arbitrary
        if(search_dir[0] != 0.0 || search_dir[1] != 0.0 || search_dir[2] != 0.0)
        {
            search_dir.normalize();

            // Accuracy for gradient computation
            // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
            Scalar grad_epsilon = 0.0001;   // arbitrary
            if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }
            Scalar r_min = -30.0, r_max = 30.0;
            Convergence::SafeNewton1D(
                *(static_cast<const BlobtreeNode*>(this)),
                init_point,
                search_dir,
                r_min,  //TODO : arbitrary                                // security minimum bound
                r_max,    //TODO : arbitrary
                0.0,            // point at which we start the search, given in 1D
                value,
                epsilon,
                grad_epsilon,
                20,
                res);
        }
        ++it;
    }

    return res;
}


void BlobtreeRoot::GetPrimitivesPointsAt(const Scalar value, const Scalar epsilon, PointCloud& res_points, const AABBox& b_box) const
{
    PointCloud res_points_tmp;
    for(std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it = this->children_.begin(); it != this->children_.end(); it++)
    {
        AABBox b = (*it)->axis_bounding_box();
        if(b_box.Intersect(b))
        {
            (*it)->GetPrimitivesPointsAt(value, epsilon, res_points, b_box);
            res_points.splice(res_points_tmp);
        }
    }

    // Now converge for all point according to the result of the node.
    Scalar bbox_size = this->axis_bounding_box_.ComputeSizeMax();
    // temporary variables for convergence function
    Scalar min_absc,max_absc;
    Point origin;
    for(PointCloud::iterator it = res_points.begin(); it != res_points.end();)
    {
        Point& p = res_points.point(*it);
        Normal& n = res_points.normal(*it);

        if(n[0]!=0.0 || n[1]!=0.0 || n[2]!=0.0){
            min_absc = -bbox_size;
            max_absc = bbox_size;
            origin = p;
            if(Convergence::SafeNewton1D(*this,
                origin,
                n,
                min_absc,
                max_absc,
                0.0,
                value,
                epsilon,
                epsilon*0.1,    // arbitrarily divide by 10 to have a better gradient estimation in case of numerical computing
                10,
                p))
            {
                // Update the normal to the point
                n = -this->EvalGrad(p,epsilon*0.1);
                n.normalize(); //TODO:@todo  following function does not exist in eigen, in some cases, can be dangerous //n.normalize_cond();

                // check if the new point is still in the bounding box
                if(b_box.Contains(p))
                {
                    ++it;
                }
                else
                {
                    // Discard the point since it's not in the wanted bounding box
                    it = res_points.erase(it);
                }
            }
            else
            {
                // Discard the point since it's proably too far
                it = res_points.erase(it);
            }
        }
        else
        {
            // Discard the point since it's proabbly outside anything useful...
            it = res_points.erase(it);
        }
    }
}


void BlobtreeRoot::AddNewNodeToRoot(std::shared_ptr<BlobtreeNode> new_bt_node)
{
    assert(new_bt_node != NULL);
    assert(new_bt_node->parent_ == NULL);
    assert(CheckSubTree(new_bt_node.get()));
    assert(dynamic_pointer_cast<BlobtreeRoot>(new_bt_node) == nullptr);

    // Inform the listeners of the tree modification to come
    BlobtreeAboutToBeModified();

    // Add new_bt_node to the root
    this->BlobtreeNode::AddChild(new_bt_node);

    // Inform the listener that the tree modification is over
    BlobtreeModified();

    assert(BlobtreeRoot::CheckTree()); //@todo : Development assertion, should be removed soon
    assert(BlobtreeRoot::CheckSubTree(basket_.get())); //@todo : Development assertion, should be removed soon
}


void BlobtreeRoot::AddChildToNAryNode(std::shared_ptr<BlobtreeNode> current_node, std::shared_ptr<BlobtreeNode> new_child)
{
    return AddChildToNAryNode(current_node.get(), new_child);
}

void BlobtreeRoot::AddChildToNAryNode(BlobtreeNode* current_node, std::shared_ptr<BlobtreeNode> new_child)
{
    assert(current_node != NULL);
    assert(current_node->arity_ == -1);
    assert(new_child != NULL);

    // Inform the listener of the tree modification to come
    BlobtreeAboutToBeModified();
    bool wasCompactAndValid = current_node->IsCompactAndValid() &&
                                     new_child->IsCompactAndValid();

    // Disconnect child_node from its parent (with recursive correction)
    BlobtreeNode* former_parent = new_child->nonconst_parent();
    if(former_parent != NULL)
    {
        wasCompactAndValid = wasCompactAndValid && former_parent->IsCompactAndValid();
        former_parent->RemoveChild(new_child);
        former_parent->RecursiveNodeCorrection();
    }

    // Add the child
    current_node->AddChild(new_child);
    if (wasCompactAndValid)
    {
        // TODO : maybe we can revalidate current_node, new_child and former_parent

        // current_node bbox to update computation : former bbox inter new_child bbox
        AABBox bbox = current_node->axis_bounding_box();//.Intersection(new_child->axis_bounding_box());
        std::map<BlobtreeNode*, std::pair<AABBox, AABBox> >::iterator it =
                updated_areas_.find(current_node);
        if (it != updated_areas_.end())
        {
            bbox = bbox.Union(it->second.second);
            it->second.second = bbox.Intersection(new_child->axis_bounding_box());
        }
        else
        {
            updated_areas_.insert(std::make_pair(current_node,std::make_pair(AABBox(),bbox.Intersection(new_child->axis_bounding_box()))));
        }

        if (former_parent != NULL)
        {
            // former_parent bbox to update computation : former_parent new bbox inter new_child bbox
            bbox = former_parent->GetAxisBoundingBox(0.);
            bbox = bbox.Intersection(new_child->axis_bounding_box());
            if (!bbox.IsEmpty())
            {
                it = updated_areas_.find(former_parent);
                if (it != updated_areas_.end())
                    it->second.second = bbox.Union(it->second.second);
                else
                    updated_areas_.insert(std::make_pair(former_parent,std::make_pair(AABBox(),bbox)));
            }
        }
    }

    // Inform the listener that the tree modification is over
    BlobtreeModified();
    assert(BlobtreeRoot::CheckTree());
    assert(BlobtreeRoot::CheckSubTree(basket_.get()));
}


void BlobtreeRoot::AddChildrenToNAryNode(std::shared_ptr<BlobtreeNode> current_node, std::vector<std::shared_ptr<BlobtreeNode> > &new_children)
{
    return AddChildrenToNAryNode(current_node.get(), new_children);
}

void BlobtreeRoot::AddChildrenToNAryNode(BlobtreeNode* current_node, std::vector<std::shared_ptr<BlobtreeNode> > &new_children)
{
    //TODO:@todo : PrepareForEval&AutoUpdate
    //TODO:@todo : this code should definitely be checked ...
    assert(current_node != NULL);
    assert(current_node->arity_ == -1);


    // Inform the listener of the tree modification to come
    BlobtreeAboutToBeModified();

    bool wasCompactAndValid = current_node->IsCompactAndValid();
    AABBox bboxToUpdate;
    AABBox bboxParent;

    for(std::vector<std::shared_ptr<BlobtreeNode> >::iterator it = new_children.begin(); it != new_children.end(); ++it)
    {
        assert(*it!=NULL);
        wasCompactAndValid = wasCompactAndValid && (*it)->IsCompactAndValid();

        // Disconnect new node (*it) from its parent (with recursive correction)
        {
            BlobtreeNode* parent = (*it)->nonconst_parent();
            if(parent != NULL)
            {
                wasCompactAndValid = wasCompactAndValid && parent->IsCompactAndValid();

                parent->RemoveChild(*it);
                parent->RecursiveNodeCorrection();

                if (parent != NULL && wasCompactAndValid)
                {
                    // former_parent bbox to update computation : former_parent new bbox inter new_child bbox
                    bboxParent = parent->GetAxisBoundingBox(0.);
                    bboxParent = bboxParent.Intersection((*it)->axis_bounding_box());
                    if (!bboxParent.IsEmpty())
                    {
                        auto it_u_a = updated_areas_.find(parent);
                        if (it_u_a != updated_areas_.end())
                            it_u_a->second.second = bboxParent.Union(it_u_a->second.second);
                        else
                            updated_areas_.insert(std::make_pair(parent,std::make_pair(AABBox(),bboxParent)));
                    }
                }
            }
        }

        // Add the child
        current_node->AddChild(*it);

        if (this->IsCompactAndValid())
            bboxToUpdate = bboxToUpdate.Union((*it)->axis_bounding_box());
    }

    //TODO:@todo : PrepareForEval&AutoUpdate
    // This is done in "brute force" way
    if (wasCompactAndValid)
    {
        // TODO : maybe we can revalidate current_node, new_child and former_parent
        AABBox bbox = current_node->axis_bounding_box();//.Intersection(new_child->axis_bounding_box());
        std::map<BlobtreeNode*, std::pair<AABBox, AABBox> >::iterator it =
                updated_areas_.find(current_node);
        if (it != updated_areas_.end())
        {
            bbox = bbox.Union(it->second.second);
            it->second.second = bbox.Intersection(bboxToUpdate);
        }
        else
        {
            updated_areas_.insert(std::make_pair(current_node,std::make_pair(AABBox(),bbox.Intersection(bboxToUpdate))));
        }
    }

    // Inform the listener that the tree modification is over
    BlobtreeModified();

    assert(BlobtreeRoot::CheckTree());
    assert(BlobtreeRoot::CheckSubTree(basket_.get()));
}


bool BlobtreeRoot::CheckSubTree(BlobtreeNode *bt_node)
{
    assert(bt_node != NULL);

    if(bt_node != this && bt_node != basket_.get())
    {
        if(bt_node->arity_ >= 0)
        {
            if( (bt_node->children_).size() != (unsigned int) bt_node->arity_ ) return false;
        }
        else
        {
            return true; // finally we allow n ary nodes with no children.
            // if( (bt_node->children_).size() == 0 ) return false;
        }
    }
    for(std::vector<std::shared_ptr<BlobtreeNode> >::iterator it = (bt_node->children_).begin(); it != (bt_node->children_).end(); ++it)
    {
        if( *it == NULL) return false;
        if( (*it)->parent_ != bt_node ) return false;
        if( !CheckSubTree((*it).get()) ) return false;
    }

    // If we arrive here, there is the good number of children and they are all correctly constructed
    return true;
}


void BlobtreeRoot::MoveToBasket(std::shared_ptr<BlobtreeNode> bt_node)
{
    assert(dynamic_pointer_cast<BlobtreeRoot>(bt_node) == nullptr);
    assert(dynamic_pointer_cast<BlobtreeBasket>(bt_node) == nullptr);
    assert(bt_node != nullptr);
    assert(CheckParent(bt_node));

    // Inform the listener of the tree modification to come
    BlobtreeAboutToBeModified();

    // Disconnect bt_node from its parent (with recursive correction)
    {
        BlobtreeNode* parent = bt_node->nonconst_parent();
        if(parent != NULL)
        {
            parent->RemoveChild(bt_node);
            parent->RecursiveNodeCorrection();
        }
    }

    // Add bt_node to the basket
    basket_->AddChild(bt_node);
    bt_node->InvalidateSubtreeBoundingBox();

    // Inform the listener that the tree modification is over
    BlobtreeModified();

    assert(BlobtreeRoot::CheckTree());//TODO : @todo : provisoir
    assert(BlobtreeRoot::CheckSubTree(basket_.get()));//TODO : @todo : provisoir
}


void BlobtreeRoot::DeleteNode(std::shared_ptr<BlobtreeNode> bt_node)
{
    assert(bt_node != NULL);
    assert(CheckParent(bt_node));
    assert(dynamic_pointer_cast<BlobtreeRoot>(bt_node) == nullptr);
    assert(dynamic_pointer_cast<BlobtreeBasket>(bt_node) == nullptr);

    // Inform the listener of the tree modification to come
    BlobtreeAboutToBeModified();

    // Disconnect bt_node from its parent (with recursive correction)
    {
        BlobtreeNode* parent = bt_node->nonconst_parent();
        if(parent != NULL)
        {
            parent->RemoveChild(bt_node);
            parent->RecursiveNodeCorrection();
        }
    }

    // Add bt_node to the basket
    bt_node->DeleteChildrenBlobtreeNode();
//    delete bt_node;
    bt_node = nullptr;

    // Inform the listener that the tree modification is over
    BlobtreeModified();

    assert(BlobtreeRoot::CheckTree());//TODO : @todo : provisoir
    assert(BlobtreeRoot::CheckSubTree(basket_.get()));//TODO : @todo : provisoir
}


void BlobtreeRoot::MoveToRoot(std::shared_ptr<BlobtreeNode> bt_node)
{
    assert(bt_node != NULL);
    assert(CheckParent(bt_node));
    assert(dynamic_pointer_cast<BlobtreeRoot>(bt_node) == nullptr);
    assert(dynamic_pointer_cast<BlobtreeBasket>(bt_node) == nullptr);

    // Inform the listener of the tree modification to come
    BlobtreeAboutToBeModified();

    // Disconnect bt_node from its parent (with recursive correction)
    BlobtreeNode* parent = bt_node->nonconst_parent();
    if(parent != NULL)
    {
        parent->RemoveChild(bt_node);
        parent->RecursiveNodeCorrection();
    }

    // Add bt_node to the root (and invalidate the root)
    this->BlobtreeNode::AddChild(bt_node);
    bt_node->InvalidateBoundingBox(); //TODO:@todo : this should be checked (this is needed when bt_node come from basket, but perhaps not from the main tree)

    // Inform the listener that the tree modification is over
    BlobtreeModified();

    assert(BlobtreeRoot::CheckTree());
    assert(BlobtreeRoot::CheckSubTree(basket_.get()));
}


void BlobtreeRoot::SwitchNode(std::shared_ptr<BlobtreeNode> old_bt_node, std::shared_ptr<BlobtreeNode> new_bt_node)
{
    assert(old_bt_node != NULL);
    assert(new_bt_node != NULL);
    assert(old_bt_node->parent() != NULL);
    assert(new_bt_node->parent() == NULL);
    assert(CheckParent(old_bt_node));
    assert(CheckSubTree(old_bt_node.get()));
//        assert(CheckSubTree(new_bt_node));
    assert(old_bt_node->arity_ == new_bt_node->arity_);

    // Inform the listener of the tree modification to come
    BlobtreeAboutToBeModified();

    BlobtreeNode* father = old_bt_node->parent_;

    /////////////////////////////////////////////////////////////////////////////////
    //TODO:@todo : PrepareForEval&AutoUpdate
    //TODO:@todo : this is not a good way to proceed : should find a better solution
    NotifyParentOfAModifiedAreaAfterDeletion(old_bt_node->axis_bounding_box());//NewUpdatedAreaFromDeletedNode(old_bt_node->axis_bounding_box());
    father->InvalidateBoundingBox();
    /////////////////////////////////////////////////////////////////////////////////

    // Correct the relationship with father
    old_bt_node->parent_ = NULL;
    new_bt_node->parent_ = father;
    for(std::vector<std::shared_ptr<BlobtreeNode> >::iterator it = (father->children_).begin(); it!=(father->children_).end(); ++it)
    {
        if( *it == old_bt_node )
        {
            *it = new_bt_node;
            break;
        }
    }

    // TODO : @todo : to be checked
    // This step depend of the arity because N-ary node doesn't update the size of their childen vector at creation
    if(new_bt_node->arity() == -1)
    {
        // Correct the relationship with all the children
        for(std::vector<std::shared_ptr<BlobtreeNode> >::iterator it = (old_bt_node->children_).begin(); it != (old_bt_node->children_).end(); ++it)
        {
            (*it)->parent_ = new_bt_node.get();
            (new_bt_node->children_).push_back(*it);
            (*it) = NULL;
        }
    }
    else
    {
        // Correct the relationship with all the children
        int i = 0;
        for(std::vector<std::shared_ptr<BlobtreeNode> >::iterator it = (old_bt_node->children_).begin(); it != (old_bt_node->children_).end(); ++it)
        {
            (*it)->parent_ = new_bt_node.get();
            new_bt_node->children_[i] = *it;
            (*it) = NULL;
            ++i;
        }
    }

    new_bt_node->InvalidateBoundingBox();

//    delete old_bt_node; // TODO Check this deletion !!
    old_bt_node = nullptr;

    // Inform the listener that the tree modification is over
    BlobtreeModified();

    assert(BlobtreeRoot::CheckTree());
    assert(BlobtreeRoot::CheckSubTree(basket_.get()));
}


void BlobtreeRoot::ReplaceNode(std::shared_ptr<BlobtreeNode> old_subtree, std::shared_ptr<BlobtreeNode>  new_subtree)
{

    assert(old_subtree != NULL);
    assert(old_subtree->parent() != NULL);
    assert(CheckParent(old_subtree));

    // Inform the listener of the tree modification to come
    BlobtreeAboutToBeModified();

    // Disconnect new_subtree from its parent (with recursive correction)
    BlobtreeNode* parent = new_subtree->nonconst_parent();
    parent->RemoveChild(new_subtree);
    parent->RecursiveNodeCorrection();

    //
    (old_subtree->parent_)->ReplaceChild(old_subtree.get(), new_subtree);

    // Save the old subtree in the basket
    basket_->AddChild(old_subtree);

    // Inform the listener that the tree modification is over
    BlobtreeModified();


    assert(BlobtreeRoot::CheckTree());//TODO : @todo : provisoir
    assert(BlobtreeRoot::CheckSubTree(basket_.get()));//TODO : @todo : provisoir
}


void BlobtreeRoot::InsertUnaryNode(std::shared_ptr<BlobtreeNode> current_node, std::shared_ptr<BlobtreeNode> new_node)
{
    assert(current_node != NULL);
    assert(current_node->parent() != NULL);
    assert(CheckParent(current_node));
    assert(new_node != NULL);
    assert(new_node->arity_ == 1);
    assert(new_node->parent() == NULL && new_node->children()[0] == NULL);

    // Inform the listener of the tree modification to come
    BlobtreeAboutToBeModified();

    BlobtreeNode* father = current_node->nonconst_parent();

    // Update of the parent_node
    father->ReplaceChild(current_node.get(), new_node);

    new_node->set_child(0, current_node);

    // Inform the listener that the tree modification is over
    BlobtreeModified();

    assert(BlobtreeRoot::CheckTree());
    assert(BlobtreeRoot::CheckSubTree(basket_.get()));
}

void BlobtreeRoot::InsertBinaryNode(std::shared_ptr<BlobtreeNode>  current_node, std::shared_ptr<BlobtreeNode>  new_node_op, std::shared_ptr<BlobtreeNode>  child_node, int pos)
{
    assert(current_node != NULL);
    assert(current_node->parent() != NULL);
    assert(CheckParent(current_node));
    assert(new_node_op != NULL);
    assert(new_node_op->arity_ == 2);
    assert(new_node_op->parent() == NULL && new_node_op->children()[0] == NULL && new_node_op->children()[1] == NULL);
    assert(child_node != NULL);
    assert(CheckParent(child_node));
    assert(pos == 0 || pos == 1);

    // Inform the listener of the tree modification to come
    BlobtreeAboutToBeModified();

    (current_node->parent_)->ReplaceChild(current_node.get(), new_node_op);

    // Disconnect child_node from its parent (with recursive correction)
    BlobtreeNode* parent = child_node->nonconst_parent();
    if(parent != NULL)
    {
        parent->RemoveChild(child_node);
        parent->RecursiveNodeCorrection();
    }

    // Set the children of the new node
    int pos_other = (pos == 0) ? 1 : 0;
    new_node_op->set_child(pos,current_node);
    new_node_op->set_child(pos_other,child_node);

    // Inform the listener that the tree modification is over
    BlobtreeModified();

    assert(BlobtreeRoot::CheckTree());
    assert(BlobtreeRoot::CheckSubTree(basket_.get()));
}


bool BlobtreeRoot::CheckParent(std::shared_ptr<BlobtreeNode> bt_node)
{
    assert(bt_node != NULL);

    BlobtreeNode* parent = bt_node->parent_;
    if( parent == NULL)
    {
        return false;
    }
    else
    {
        for(std::vector<std::shared_ptr<BlobtreeNode> >::iterator it = (parent->children_).begin(); it!=(parent->children_).end(); ++it)
        {
            if( *it == bt_node ) return true;
        }
    }

    return false;
}


bool BlobtreeRoot::CheckChildren(std::shared_ptr<BlobtreeNode> bt_node)
{
    assert(bt_node != NULL);
    assert(bt_node->arity_ >= -1);

    // Check arity with respect to children number
    if( bt_node->arity_ >= 0 )
    {
        if( ((int) bt_node->nb_children()) != bt_node->arity_ ) return false;
    }
    else
    {
        if(bt_node->arity_ == 0)
        {
            bool res = bt_node->nb_children() == 0 ? true : false;
            return res;
        }
    }

    for(std::vector<std::shared_ptr<BlobtreeNode> >::iterator it = (bt_node->children_).begin(); it!=(bt_node->children_).end(); ++it)
    {
        if( *it == NULL )
        {
            return false;
        }
        else
        {
            if( (*it)->parent_ != bt_node.get()) return false;
        }
    }

    return true;
}


std::shared_ptr<ScalarField> BlobtreeRoot::CloneForEval() const
{
    return std::make_shared<ClonedBlobtreeRoot>(*this);
}


/// @cond RESOURCES

class BlobtreeRootResource : public ResourceTemplate<20, BlobtreeRoot>
{
public:
    BlobtreeRootResource(ptr<ResourceManager> manager, const string &name, ptr<ResourceDescriptor> desc, const TiXmlElement *e = NULL) :
        ResourceTemplate<20, BlobtreeRoot>(manager, name, desc)
    {
        e = e == NULL ? desc->descriptor : e;
        checkParameters(desc, e, "name,");

        const TiXmlElement* basket_element = e->FirstChildElement("BlobtreeBasket");
        if (e == NULL) {
            Logger::ERROR_LOGGER->log("RESOURCES", "ERROR in BlobtreeRoot::LoadChildren : No _BlobtreeBasket_ attribute.");
        }

        shared_ptr<BlobtreeBasket> basket = dynamic_pointer_cast<BlobtreeBasket>(manager->loadResource(desc, basket_element));
        // TODO : add init method and check basket initialization.
//        init(basket);

        LoadBlobtreeNodeChildren(this, manager, desc, e);
    }
};

extern const char blobtreeRoot[] = "blobtreeRoot";

static ResourceFactory::Type<blobtreeRoot, BlobtreeRootResource> BlobtreeRootType;

/// @endcond


} // Close namespace convol

} // Close namespace expressive
