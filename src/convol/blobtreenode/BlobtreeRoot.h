/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: BlobtreeRoot.h
 
 Language: C++
 
 License: Convol license
 
 \author: Cedric Zanni
 E-Mail:  cedric.zanni@inrialpes.fr
 
 Description: Class for the management  of a blobtree.
                The parent_ attribute should be always NULL
                The blending of root childrens is always done with a max operator

 Platform Dependencies: None
 */

#pragma once
#ifndef CONVOL_BLOBTREE_ROOT_H_
#define CONVOL_BLOBTREE_ROOT_H_

// core dependencies
#include <core/CoreRequired.h>

//// convol dependencies
//    // Macro
//    #include <Convol/include/Macro/CONVOL_MACRO_NAME.h>
    // Convol BlobtreeNode
    #include<convol/blobtreenode/BlobtreeNode.h>
    #include<convol/blobtreenode/BlobtreeBasket.h>
    #include<convol/blobtreenode/listeners/BlobtreeRootListener.h>
    // functor
    #include<convol/functors/operators/BlendMax.h>

////std dependencies
//#include<vector>
//#include<string>
#include<list>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/xml/tinyxml/tinyxml.h>

namespace expressive {

namespace convol {

class CONVOL_API BlobtreeRoot :
    public BlobtreeNode
{
public:
    EXPRESSIVE_MACRO_NAME("BlobtreeRoot")

    BlobtreeRoot()
        : BlobtreeNode(StaticName())
    {
        this->arity_ = -1;
        
        basket_ = std::make_shared<BlobtreeBasket>();
    }
    BlobtreeRoot(typename std::vector<std::shared_ptr<BlobtreeNode> >bt_node_vec)
        : BlobtreeNode(StaticName())
    {
        this->arity_ = -1;

        for(typename std::vector<std::shared_ptr<BlobtreeNode> >::iterator it = bt_node_vec.begin(); it != bt_node_vec.end(); ++it)
        {
            assert( *it != NULL );
            this->BlobtreeNode::AddChild(*it);
        }
        basket_ = std::make_shared<BlobtreeBasket>();
    }
    
    virtual ~BlobtreeRoot()
    {
        this->DeleteChildrenBlobtreeNode();
        basket_->Clear();
    }
    
    virtual std::shared_ptr<ScalarField> CloneForEval() const;


    ///////////////////
    // Xml functions //
    ///////////////////
    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        BlobtreeNode::AddXmlAttributes(element);

        TiXmlElement * element_basket = basket_->ToXml();
        element->LinkEndChild(element_basket);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        BlobtreeNode::AddXmlAttributes(element, ids);

        TiXmlElement * element_basket = basket_->ToXml();
        element->LinkEndChild(element_basket);
    }

    //////////////////////////
    // Evaluation functions //
    //////////////////////////

    virtual Scalar Eval(const Point& point) const;
    virtual Vector EvalGrad(const Point& point, Scalar epsilon) const;
    virtual void EvalValueAndGrad(const Point& point, Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;

    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const;

    /*! \brief Prepare the blobtree root for eval if it is not prepared...
     *         last_modified_areas_ is updated if the prepare for eval is called, updated areas are added to last_modified_areas_
     *         You are responsible for cleaning last_modified_areas_ 
     * 
     *   \param epsilon_bbox : the potential computation limit, this will determine the bounding box.
     *   \param accuracy_needed : The geometrical accuracy we need. This give a clue about of accurate preparation computation needs to be.
     *                            For instance, if a node pre-compute a potential grid, knowing that the accuracy needed is 0.1 will
     *                            tell it not to build a too fine grid.
     */
    void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed)
    {
        assert(epsilon_bbox >= 0.0);
        if(this->epsilon_bbox_ != epsilon_bbox)
        {
            max_operator_.PrepareForEval(*this, epsilon_bbox, accuracy_needed, updated_areas_);
            this->epsilon_bbox_ = epsilon_bbox;
            this->UpdateNodeCompacity();        
        }
        //TODO:@todo :  Following should not be needed ...
//        if (!updated_areas_.empty())
//        {
//            NewUpdatedAreas(updated_areas_);
//            updated_areas_.clear();
//        }
    }
    virtual void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
        typename std::map<BlobtreeNode* , typename std::pair<core::AABBox, core::AABBox> >& updated_areas)
    {
        UNUSED(epsilon_bbox);
        UNUSED(accuracy_needed);
        UNUSED(updated_areas);
        assert(false); // don't use this one... You should not need it. If you do, then implement it @todo
    }

    void ClearUpdatedAreas()
    {
        updated_areas_.clear();
    }
    void ClearAreasFromDeletedNodes()
    {
        updated_areas_.clear();
    }

    ////////////////////////
    // Bounding functions //
    ////////////////////////

    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const;

    // same as the ine in Blobtree node except it discards points that do not verify the condition to be epsilon close to the surface.
    virtual void GetPrimitivesPointsAt(Scalar value, Scalar epsilon, core::PointCloud& res_points, const core::AABBox& b_box) const;


    /////////////////////////////
    // BoundingBox computation //
    /////////////////////////////

    virtual core::AABBox GetAxisBoundingBox(Scalar value) const
    {
        return max_operator_.GetAxisBoundingBox(this->children_, value);
    }

    virtual void NotifyParentOfAModifiedAreaAfterDeletion(const core::AABBox& modified_area)
    {
        last_modified_areas_from_deleted_nodes_.push_back(modified_area);
    }

    
    ///////////////////////////////////////////////////////////////////////////
    //              Function for the Management of the blobtree              //
    // WARNING : This function shouldn't be called with a root tree as param //
    ///////////////////////////////////////////////////////////////////////////

    /** \brief Clear the whole tree.
     *
     */
    void Clear()
    {
        basket_->Clear();
        this->DeleteChildrenBlobtreeNode();
        map_id_node_.clear();
    }

    /** \brief Add a new blobtree to the root. This is the only way to insert a new blobtree node
     *         to the root
     * \param new_bt_node New node to connect to the root. Its parent must be NULL.
     * 
     */
    void AddNewNodeToRoot(std::shared_ptr<BlobtreeNode> new_bt_node);

    /** \brief Add a child to the current node. 
     *         WARNING : Only available for N-Ary nodes.
     *
     *  \param current_node N-ary node to which the new child will be added. Must not be NULL.
     *  \param new_child Child to add to current node. Must not be NULL.
     *
     */
    void AddChildToNAryNode(BlobtreeNode* current_node, std::shared_ptr<BlobtreeNode> new_child);
    void AddChildToNAryNode(std::shared_ptr<BlobtreeNode> current_node, std::shared_ptr<BlobtreeNode> new_child);

   /** \brief Add a list of node to the current node. 
     *        WARNING : Only available for N-Ary nodes.
     *
     *  \param current_node N-ary node to which the new child will be added. Must not be NULL.
     *  \param new_children Children to add to current node.
     *
     */
   void AddChildrenToNAryNode(std::shared_ptr<BlobtreeNode> current_node, std::vector<std::shared_ptr<BlobtreeNode> >& new_children);
   void AddChildrenToNAryNode(BlobtreeNode* current_node, std::vector<std::shared_ptr<BlobtreeNode> >& new_children);

    /** \brief Check the correctness of the BlobTree Root Node itself 
     * 
     *   \return Correctness of the the Blobtree Root Node
     */
    inline bool CheckTree()
    {
        return CheckSubTree(this);
    }
    
    /** \brief Check if the SubTree btn is correctly built.
     *         Correctly built mean there is not NULL pointer in unexpected places.
     *         That does not mean the tree is prepared for evaluation.
     *
     *   \param bt_node Pointer to the Blobtree Node to be checked. Must not be NULL.
     *   \return Correctness of the subtree
     * 
     */
    bool CheckSubTree(BlobtreeNode* bt_node);
           
    /** \brief Cut the subtree starting at the indicated Blobtree Node,
     *          paste it in basket and correct the remaining tree if needed.
     *
     *   \param bt_node Pointer to the BlobtreeNode to take and move to basket. Should be in the tree, should not be the root nor the basket.
     *
     */
    void MoveToBasket(std::shared_ptr<BlobtreeNode> bt_node);

    /** \brief Delete the given node. It can be in the basket or not.
     *
     *   \param bt_node Pointer to the BlobtreeNode to take and delete. Should be in the tree (possibly in the basket), should not be the root nor the basket.
     *
     */
    void DeleteNode(std::shared_ptr<BlobtreeNode> bt_node);

    /** \brief Delete all nodes. It can be in the basket or not.
     *
     *
     */
    void deleteAllNode(){
        for(unsigned int i=0;i<this->nb_children();i++){
            this->DeleteNode(this->nonconst_children()[i]);
        }
    }

    /** \brief Cut the subtree starting at the indicated Blobtree Node,
     *          paste it in root and correct the remaining tree if needed.
     *
     *  \param bt_node *   \param bt_node Pointer to the BlobtreeNode to take and move to root. Should be in the tree, should not be the root nor the basket.
     *
     */
    void MoveToRoot(std::shared_ptr<BlobtreeNode> bt_node);

        
    /** \brief Replace a node by another, the two node must have the same arity. The replaced node will be destroyed.
     *         Note that since does not switch 2 trees but really 2 nodes. children from the old one are transfered to
     *         children of the new one.
     *
     *   \param old_bt_node Blobtree Node to be replaced. Its parent must not be NULL (Thus it cannot be the root nor the basket).
     *   \param new_bt_node Replacement Node. Its parent must be NULL.
     *
     */
    void SwitchNode(std::shared_ptr<BlobtreeNode> old_bt_node, std::shared_ptr<BlobtreeNode> new_bt_node);
    
    /** \brief Replace a node by another one (and so replace the respective subtrees),
     *         The old-one is sent to the basket.
     *         If the new one was connected (had a non-NULL parent), it is disconnected
     *         and its parent is recursively corrected.
     *
     *   \param old_subtree The subtree to be changed. 
     *   \param new_subtree Replacement subtree, which should be well structured (thus do not contain old_subtree). Must be connected to a tree (no NULL parent pointer)
     *
     *   Guaranties that the resulting tree will be valid if and only if new_subtree and old_subtree are both valid
     *   (with and exception with respect to new_subtree parent that is allowed to be NULL.
     *
     */
    void ReplaceNode(std::shared_ptr<BlobtreeNode> old_subtree, std::shared_ptr<BlobtreeNode> new_subtree);
    
    /** \brief Insert a new unary node above a given node
     *
     *   \param current_node A node in the tree.
     *   \param new_node A created unary node with a NULL parent and a unique NULL child
     *
     *   \return void
     */
    void InsertUnaryNode(std::shared_ptr<BlobtreeNode> current_node, std::shared_ptr<BlobtreeNode> new_node);
    
     /** \brief Insert a new binary node above a given node
     *
     *   \param current_node A node in the tree, above which the new_node_op will be inserted
     *   \param new_node_op A created binary node with a NULL parent and two NULL children
     *   \param child_node The other node for this binary node. Notice that this node can be in the tree. 
     *                     In this last case, it will be disconnected and its father will be recursively corrected.
     *   \param pos Equal 0 or 1. Defines the position of current_node in the resulting binary new_node_op.
     *  
     *   WARNING : of course child_node must not be in the parenthood/childhood of current_node and vice versa.
     *
     *   \return void
     */
    void InsertBinaryNode(std::shared_ptr<BlobtreeNode> current_node, std::shared_ptr<BlobtreeNode> new_node_op, std::shared_ptr<BlobtreeNode> child_node, int pos = 0);
    
     /** \brief Insert a new n-ary node above a given node.
     *
     *   \param current_node A node in the tree, above which the new_node_op will be inserted.
     *   \param new_node_op A previously created N-ary node with a NULL parent and NULL children
     *   \param v_children_node The other nodes for this n-ary node. Notice that those node can be in the tree. In this last case, they will be disconnected and their fathers will be recursively corrected.
     *  
     *   WARNING : of course node of the children in v_children_node is allowed to be in the parenthood/childhood of current_node and vice versa.
     *
     *   \return void
     */
    template<typename stdContainer_BlobtreeNodePointer_>
    void InsertNAryNode(std::shared_ptr<BlobtreeNode> current_node, std::shared_ptr<BlobtreeNode> new_node_op, stdContainer_BlobtreeNodePointer_& v_children_node);

    
    ///////////////
    // Modifiers //
    ///////////////

    // A root node is always correct.

    virtual void RecursiveNodeCorrection() { }

    void ClearBasket()
    {
        // Inform the listener of the tree modification to come
        BlobtreeAboutToBeModified();

        basket_->Clear();

        // Inform the listener that the tree modification is over
        BlobtreeModified();
    }
    
    ///////////////
    // Accessors //
    ///////////////

    std::shared_ptr<BlobtreeBasket> basket() { return basket_; }

    const std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& updated_areas() { return updated_areas_; }
    const std::list<core::AABBox>& last_modified_areas_from_deleted_nodes() { return last_modified_areas_from_deleted_nodes_; }

    /////////////////////////
    // Listener Management //
    /////////////////////////

    const std::list<BlobtreeRootListener*> &ListenerList() const { return listener_list_; }
    void AddListener(BlobtreeRootListener* listener)    { listener_list_.push_back(listener); }
    void RemoveListener(BlobtreeRootListener* listener) {
		for(typename std::list<BlobtreeRootListener*>::iterator it = listener_list_.begin(); it != listener_list_.end(); ++it) {
			if( *it == listener ) { listener_list_.erase(it); break; }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Listener calls //
    ////////////////////
    // Listener calls are public since we can manipulate the blobtree with raw BlobtreeNode's functions.
    // In this case it's important to have access to these functions.

    inline void BlobtreeAboutToBeModified()
    {
        for( typename std::list<BlobtreeRootListener*>::iterator it = listener_list_.begin(); it != listener_list_.end(); ++it)
        {
            (*it)->BlobtreeAboutToBeModified();
        }
    }
    inline void BlobtreeModified()
    {
        for( typename std::list<BlobtreeRootListener*>::iterator it = listener_list_.begin(); it != listener_list_.end(); ++it)
        {
            (*it)->BlobtreeModified();
        }
    }
//    // This function is called by PrepareForEval when updated areas have been detected.
//    inline void NewUpdatedAreas(const std::map<BlobtreeNode*, std::pair<AABBox, AABBox> >& updated_areas)
//    {
//        for( typename std::list<BlobtreeRootListener*>::iterator it = listener_list_.begin(); it != listener_list_.end(); ++it)
//        {
//            (*it)->NewUpdatedAreas(updated_areas);
//        }
//    }
//    inline void NewUpdatedAreaFromDeletedNode(const AABBox& updated_area_from_deleted_node)
//    {
//        for( typename std::list<BlobtreeRootListener*>::iterator it = listener_list_.begin(); it != listener_list_.end(); ++it)
//        {
//            (*it)->NewUpdatedAreaFromDeletedNode(updated_area_from_deleted_node);
//        }
//    }
    ///////////////////////////
    // END OF listener calls //
    ///////////////////////////////////////////////////////////////////////////

    /**
     * List of every node mapped to their ID. Mostly usefull for loading/saving.
     */
    std::map<unsigned long, std::shared_ptr<BlobtreeNode> > map_id_node_;

    virtual void swap(std::shared_ptr<BlobtreeRoot> &) {

        assert(false && "You should redefine the swap method for this class");
    }

    /** \brief Check that the relationship between bt_node and its parent is correct.
    *          That means bt_node has a parent and its parents has it among its children.
    *
    *   \param bt_node the node to check
    *
    *   \return true if everything is fine, wrong otherwise.
    */
    bool CheckParent(std::shared_ptr<BlobtreeNode> bt_node);

protected :
    // @warning listener_list is not copied
    // @warning basket_ is not copied
    BlobtreeRoot(const BlobtreeRoot &rhs) : BlobtreeNode(rhs), basket_(new BlobtreeBasket()){}
protected :
    // basket_ is used to store blobtree nodes which must not be evaluated
    std::shared_ptr<BlobtreeBasket> basket_;

    // Listener of this object
    std::list<BlobtreeRootListener*> listener_list_; ///TODO:@todo : in blobtreeNode listener are stored in a set, I see no point in having different container in both places

    // The root is acting like a max node for evaluation.
    BlendMax max_operator_;

    // List of the updated_areas_ used in "PrepareForEval"
    std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> > updated_areas_;
    std::list<core::AABBox> last_modified_areas_from_deleted_nodes_;
    // List of modified area kept for the tree.
    // This structure stores 2 bounding box per node (maximum). When modifying a node and calling PrepareForEval, the old bounding box
    // is stored in the second element of the pair. This second element will not change until cleaning of the map.
    // Then all modification will update the first element of the pair to store the current bounding box.

    /** \brief Check that the relationships between bt_node and its children are correct.
    *          That means all children of bt_node is not null and has it as its parent.
    *
    *   \param bt_node the node to check
    *
    *   \return true if everything is fine, wrong otherwise.
    */
    bool CheckChildren(std::shared_ptr<BlobtreeNode> bt_node);
    
};


class ClonedBlobtreeRoot : public BlobtreeRoot
{
public:
    ClonedBlobtreeRoot(const BlobtreeRoot &rhs) : BlobtreeRoot(rhs){}

    ~ClonedBlobtreeRoot()
    {
        BlobtreeRoot::DeleteChildrenBlobtreeNode();
    }
};









template<typename stdContainer_BlobtreeNodePointer_>
void BlobtreeRoot::InsertNAryNode(std::shared_ptr<BlobtreeNode> current_node, std::shared_ptr<BlobtreeNode>  new_node_op, stdContainer_BlobtreeNodePointer_& v_children_node)
{
    assert(current_node != NULL);
    assert(current_node->parent() != NULL);
    assert(CheckParent(current_node));
    assert(new_node_op != NULL);
    assert(new_node_op->parent() == NULL);
    assert(new_node_op->arity_ == -1);
    assert(new_node_op->nb_children() == 0);

    // Inform the listener of the tree modification to come
    BlobtreeAboutToBeModified();

    (current_node->parent_)->ReplaceChild(current_node.get(), new_node_op);

    for(typename std::vector<std::shared_ptr<BlobtreeNode> >::iterator it = v_children_node.begin(); it != v_children_node.end(); ++it)
    {
        assert( *it != NULL );

        // Disconnect new child node (*it) from its parent (with recursive correction)
        BlobtreeNode* parent = (*it)->nonconst_parent();
        if(parent != NULL)
        {
            parent->RemoveChild(*it);
            parent->RecursiveNodeCorrection();
        }

        // Connect child_node (*it) to the new_node_op
        new_node_op->AddChild(*it);
    }

    // Connect curent_node to the new_node_op
    new_node_op->AddChild(current_node);

    // Inform the listener that the tree modification is over
    BlobtreeModified();

    assert(BlobtreeRoot::CheckTree());//TODO : @todo : provisoire
    assert(BlobtreeRoot::CheckSubTree(basket_.get()));//TODO : @todo : provisoire
}
    
} // Close namespace convol

} // Close namespace expressive

    
#endif // CONVOL_BLOBTREE_ROOT_H_
