/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: NodeGaborT.h
 
 Language: C++
 
 License: Convol license
 
 \author: Cédric Zanni
 E-Mail:  cedric.zanni@inrialpes.fr
 
 Description: Class representing a N-ary node that add procedural implicit details thanks to a noise function.
              Reference : (useful to understand what does parameters stand for)
              Procedural Noise using Sparse Gabor Convolution, Lagae, Lefebvre, Drettakis, Dutre, ACM SIGGRAPH Conference proceedings

 Platform Dependencies: None
 */

#pragma once
#ifndef NODE_PROCEDURAL_DETAIL_H_
#define NODE_PROCEDURAL_DETAIL_H_

// core dependencies
#include <core/CoreRequired.h>

// Convol dependencies
#include <convol/ConvolRequired.h>

#include <convol/blobtreenode/NodeOperatorT.h>
#include <convol/functors/noises/PolynomialDeformationMap.h>
#include <convol/tools/GradientEvaluationTools.h>

#include <convol/blobtreenode/NodeProceduralDetailParameterT.h>

// Operator
#include<convol/functors/operators/BlendSum.h>

#include "commons/EigenVectorTraits.h"


/**
 * TODO:@todo :
 *
 * - iso-value limits should be chosen automatically in function of details and primitive used
 *
 */

namespace expressive {

namespace convol {

class CONVOL_API AbstractNodeProceduralDetail :
        public NodeOperator
{
public:
    EXPRESSIVE_MACRO_NAME("NodeProceduralDetail")

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    AbstractNodeProceduralDetail(Scalar iso_value, Scalar iso_detail_inf, Scalar iso_detail_sup)
        : iso_value_(iso_value), iso_detail_inf_(iso_detail_inf), iso_detail_sup_(iso_detail_sup)
    {
        this->parent_ = nullptr;
        this->arity_ = -1;

        this->vect_prop_res_dir_.resize(1);
        this->vect_prop_res_dir_aux_.resize(1);
    }

protected:
    AbstractNodeProceduralDetail(const AbstractNodeProceduralDetail &rhs)
        : NodeOperator(rhs), iso_value_(rhs.iso_value_), iso_detail_inf_(rhs.iso_detail_inf_),
          iso_detail_sup_(rhs.iso_detail_sup_), blend_operator_(rhs.blend_operator_),
          vect_prop_res_dir_(rhs.vect_prop_res_dir_), vect_prop_res_dir_aux_(rhs.vect_prop_res_dir_aux_) {}

public:

    /////////////////////
    /// Xml functions ///
    /////////////////////

    static std::string XmlFunctorField() { return std::string("TFunctorNoise"); }

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        NodeOperator::AddXmlAttributes(element);

        element->SetDoubleAttribute("iso_value",iso_value_);
        element->SetDoubleAttribute("iso_detail_inf",iso_detail_inf_);
        element->SetDoubleAttribute("iso_detail_sup",iso_detail_sup_);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        NodeOperator::AddXmlAttributes(element,ids);

        element->SetDoubleAttribute("iso_value",iso_value_);
        element->SetDoubleAttribute("iso_detail_inf",iso_detail_inf_);
        element->SetDoubleAttribute("iso_detail_sup",iso_detail_sup_);
    }

    virtual void InitFromXml(const TiXmlElement *element) = 0;

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    Scalar Eval(const Point& point) const;

    void EvalValueAndGrad(const Point& point,
                                  Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const;
    void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                       const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                       const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                       Scalar& value_res, Vector& grad_res,
                                       std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                       std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                       ) const;

    /**
     * @brief EvalProperties compute properties assuming a blending operator,
     * the properties computed are the one at computation point, it does not
     * take into account field deformation in any way !
     * Using this function imply redundancy of field computation !
     * @param scal_prop_ids
     * @param vect_prop_ids
     * @param scal_prop_res
     * @param vect_prop_res
     */
    void EvalProperties(const Point& point,
                        const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                        const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                        std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                        std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                       ) const;

    /**
     * @brief EvalWithoutNoise Compute the scalar field value without the noise deformation
     * @param point
     * @return
     */
    inline Scalar EvalWithoutNoise(const Point& point) const
    {
        return blend_operator_.Eval(this->children_,point);
    }
    inline void EvalValueAndGradWithoutNoise(const Point& point, Scalar epsilon_grad,
                                             Scalar& value_res, Vector& grad_res) const
    {
        blend_operator_.EvalValueAndGrad(this->children_,point,epsilon_grad, value_res, grad_res);
    }
    // nb contrairement à NodePreoceduralDetailParameter cette fct renvoir dir = (\int f dir) / \int f
    void EvalValueGradDirWithoutNoise(const Point& point, Scalar epsilon_grad,
                                      Scalar& value_res, Vector& grad_res, Vector& dir_res,
                                      std::vector<Scalar>& children_field) const;

    virtual Point GetAPointAtWithoutNoise(Scalar value, Scalar epsilon) const = 0;

protected :

    /*!
     * \brief ComputationOrthoBasis compute an orthogonal basis: one direction is
     * the provided normal and another one is as aligned as possible with the vector dir.
     * \param dir normalized weighted direction of the field
     * \param normal required normal for the frame
     * \param main_dir
     * \param ortho_dir
     * @return return the quality of the generated basis (when tend toward zero, this means that we are
     * close to a singularity in the frame field).
     */
     Scalar ComputeOrthoBasis(const Vector& dir, const Vector& normal,
                              Vector& main_dir, Vector& ortho_dir) const;

    /**
     * @brief ProjectionParameterInterpolation compute several information about the required deformation.
     * @param children_field
     * @param is_inside indicate whether the computation point is inside the object or not
     * @param field field value at computation point
     * @param dist_lim
     * @param tilt_angle
     * @param rotate_angle
     */
    void ProjectionParameterInterpolation(typename std::vector<Scalar>& vec_field_value,
                                          bool is_inside, Scalar field,
                                          Scalar& dist_lim, Scalar& tilt_angle, Scalar& rotate_angle) const;

    /**
     * @brief DirProjection compute the direction of projection associated to tilt
     * and rotate angle parameters in function of the local frame.
     * @param normal
     * @param tangent
     * @param ortho
     * @param basis_quality
     * @return a vector defining the direction to search the iso-surface
     */
    Vector DirProjection(const Vector& normal, const Vector& tangent, const Vector& ortho,
                         Scalar tilt_angle, Scalar rotate_angle,
                         Scalar basis_quality) const;

    /**
     * @brief ComputeDeformationAmplitude this function call EvalDeformationAmplitude
     * to compute the required amplitude of deformation and smooth it out in the neightboroud
     * of discontinuites ... A COMPLETER ET REVOIR
     *
     * TODO:@todo : should take into account viewing angle to prevent discontinuities
     *
     * @param projected_point point onto the implicit surface
     * @param local_dir_projection direction of projection
     * @return
     */
    void ComputeDeformationAmplitude(const Point &p_proj, const Vector &local_dir_projection,
                                     Scalar& f_proj, Vector& grad_f_proj,
                                     Scalar& deformation_amplitude, Scalar& d_limit_min, Scalar& d_limit_max) const;
    void ComputeDeformationAmplitudeAndGrad(const Point &p_proj, const Vector &local_dir_projection,
                                            Scalar& f_proj, Vector& grad_f_proj,
                                            Scalar& deformation_amplitude, Vector& grad_deform_amplitude,
                                            Scalar& d_limit_min, Scalar& d_limit_max) const;

    /**
     * @brief EvalDeformationAmplitude
     */
    virtual void EvalDeformationAmplitude(const Point &projected_point,
                                          const Vector& normal, const Vector& main_dir, const Vector& ortho_dir,
                                          const std::vector<Scalar>& children_field, Scalar field,
                                          Scalar& deformation_amplitude, Scalar& limit_min, Scalar& limit_max) const = 0;
    virtual void EvalDeformationAmplitudeAndGrad(const Point &p_proj,
                                                 const Vector& normal, const Vector& main_dir, const Vector& ortho_dir,
                                                 const Vector& dir_proj,
                                                 const std::vector<Scalar>& children_field, Scalar field,
                                                 Scalar& deformation_amplitude, Vector& grad_deform_amplitude,
                                                 Scalar& limit_min, Scalar& limit_max) const = 0;

    /**
     * @brief EvalWarpedField
     *
     * TODO:@todo : will require additionnal parameter to smooth out the deformation
     * in the region of required compression
     * TODO:@todo : should apply smoothing near limits that does not depend upon the distance to iso-surface...
     * TODO:@todo : for now, only take into account the tilt angle and not the rotate angle...
     *
     * @param dist_to_iso the signed distance to the iso-value, negative if inside the object (f_point>iso)
     * @return
     */
    Scalar EvalWarpedField(Scalar f_point, const Vector& grad_f_point,
                           Scalar f_proj, const Vector& grad_f_proj,
                           Scalar dist_to_iso, Scalar tilt_angle,
                           Scalar d, Scalar l_min, Scalar l_max) const;
    void EvalWarpedFieldAndGrad(const Point& point,// to be able to compute either numerical gradient or closed form approx
                                Scalar f_point, const Vector& grad_f_point,
                                Scalar f_proj, const Vector& grad_f_proj,
                                Scalar s_dist_to_proj, const Vector& grad_s_dist_to_proj,
                                Scalar tilt_angle,
                                Scalar displacement, const Vector& grad_displacement ,
                                Scalar l_min, Scalar l_max,
                                Scalar& res, Vector& grad_res) const;

//    /**
//     * @brief BruteForceGradientDeformationAmplitude
//     * WARNING  : this function is only used for testing intermediate computation
//     * TODO:@todo : should be removed as soon as the whole code is validated ...
//     */
//    Vector BruteForceGradientDeformationAmplitude(const Point& point, Scalar epsilon) const
//    {
//        Scalar f_point;
//        Vector grad_f_point, dir_f_point;
//        std::vector<Scalar> children_field;
//        children_field.resize(this->children_.size());

//        Vector normal, main_dir, ortho_dir;
//        Scalar basis_quality ;

//        Scalar f_proj;
//        Vector grad_f_proj;
//        Scalar d_limit_min, d_limit_max;
//        Scalar l_dist_lim, l_tilt_angle, l_rotate_angle;
//        bool is_inside;
//        Vector local_dir_projection;

//        Vector grad;
//        Point p_plus = point;
//        Point p_minus = point;

//        for(unsigned int i = 0; i<Point::RowsAtCompileTime; i++)
//        {
//            p_plus[i] += epsilon;
//            p_minus[i] -= epsilon;

//            // plus /////
//            this->EvalValueGradDirWithoutNoise(p_plus, epsilon, f_point, grad_f_point, dir_f_point, children_field);
//            normal = -grad_f_point.normalized();
//            dir_f_point.normalize(); ///TODO:@todo : if ||dir_f_point|| -> 0 use grad_f_point only for projection
//            basis_quality = 1.0;
////            basis_quality = ComputeOrthoBasis(dir_f_point, normal, main_dir, ortho_dir);
////            basis_quality *= basis_quality; //value in [0;1]
//            is_inside = f_point > this->iso_value_;
//            ProjectionParameterInterpolation(children_field, is_inside, f_point,
//                                             l_dist_lim, l_tilt_angle, l_rotate_angle);
//            local_dir_projection = DirProjection(normal, main_dir, ortho_dir,
//                                                 l_tilt_angle, l_rotate_angle,
//                                                 basis_quality, is_inside);
//            Point projected_point_plus;
//            Projection(p_plus, f_point, grad_f_point,
//                       local_dir_projection, l_dist_lim,
//                       projected_point_plus);
//            Scalar deformation_amplitude_plus;
//            this->ComputeDeformationAmplitude(projected_point_plus, local_dir_projection,
//                                              f_proj, grad_f_proj,
//                                              deformation_amplitude_plus, d_limit_min, d_limit_max);
//            // minus /////
//            this->EvalValueGradDirWithoutNoise(p_minus, epsilon, f_point, grad_f_point, dir_f_point, children_field);
//            normal = -grad_f_point.normalized();
//            dir_f_point.normalize(); ///TODO:@todo : if ||dir_f_point|| -> 0 use grad_f_point only for projection
//            basis_quality = 1.0;
////            basis_quality = ComputeOrthoBasis(dir_f_point, normal, main_dir, ortho_dir);
////            basis_quality *= basis_quality; //value in [0;1]
//            is_inside = f_point > this->iso_value_;
//            ProjectionParameterInterpolation(children_field, is_inside, f_point,
//                                             l_dist_lim, l_tilt_angle, l_rotate_angle);
//            local_dir_projection = DirProjection(normal, main_dir, ortho_dir,
//                                                 l_tilt_angle, l_rotate_angle,
//                                                 basis_quality, is_inside);
//            Point projected_point_minus;
//            Projection(p_minus, f_point, grad_f_point,
//                       local_dir_projection, l_dist_lim,
//                       projected_point_minus);
//            Scalar deformation_amplitude_minus;
//            this->ComputeDeformationAmplitude(projected_point_minus, local_dir_projection,
//                                              f_proj, grad_f_proj,
//                                              deformation_amplitude_minus, d_limit_min, d_limit_max);

//            grad[i] = (deformation_amplitude_plus - deformation_amplitude_minus)/(2.0*epsilon);
//            p_plus[i] -= epsilon;
//            p_minus[i] += epsilon;
//        }

//        return grad;
//    }

    ///TODO:@todo find a way to handle this easily from the chosen kernel
    /// such as automatic choice of the degree (warning: not the one of the
    /// integrated kernel but the one of the resulting "reference" field
    Scalar LocalApproxInverse(Scalar x, Scalar grad_norm, Scalar degree) const
    {
        return pow(1.0 + grad_norm*x/degree, -degree);
    }
    void LocalApproxInverse(Scalar x, Scalar grad_norm, Scalar degree,
                            Scalar& val, Scalar& dx_val) const
    {
        Scalar tmp = grad_norm/degree;
        val = pow(1.0 + tmp*x, -degree);
        dx_val = - grad_norm * val / (1.0 + tmp*x);
    }
    Scalar LocalApproxCompactPolynomial(Scalar x, Scalar grad_norm, Scalar scale, Scalar degree) const
    {
        Scalar scale_sqr = scale*scale;
        Scalar inv_scale_sqr = 1.0/scale_sqr;
        Scalar dist = (scale_sqr - 1.0)*grad_norm*x/(2.0*degree) + 1.0;
        Scalar normalization = 1.0/pow(1.0 - inv_scale_sqr, degree); //TODO:@todo : we should be able to retrieve it from the chosen kernel
        Scalar v_sqr = dist*dist*inv_scale_sqr;
        if(v_sqr<1.0)
            return normalization * pow(1.0 - v_sqr, degree);
        else
            return 0.0;
    }
    void LocalApproxCompactPolynomial(Scalar x, Scalar grad_norm, Scalar scale, Scalar degree,
                                      Scalar& val, Scalar& dx_val) const
    {
        Scalar scale_sqr = scale*scale;
        Scalar inv_scale_sqr = 1.0/scale_sqr;
        Scalar dist = (scale_sqr - 1.0)*grad_norm*x/(2.0*degree) + 1.0;
        Scalar normalization = 1.0/pow(1.0 - inv_scale_sqr, degree); //TODO:@todo : we should be able to retrieve it from the chosen kernel
        Scalar v_sqr = dist*dist*inv_scale_sqr;
        if(v_sqr<1.0) {
            Scalar pow_v = pow(1.0 - v_sqr, degree-1.0);
            val = normalization * pow_v * (1.0 - v_sqr);
            dx_val = - normalization * ( (scale_sqr - 1.0)*grad_norm * dist * inv_scale_sqr ) * pow_v;
        } else {
            val = 0.0;
            dx_val = 0.0;
        }
    }

    /**
     * @brief WeightingFunction an arbitrary weighting function with a bump in d
     * and the interval [l_min;l_max] as support
     * @param x
     * @param d value for which the weight is 1
     * @param l_min value for which the weight is 0
     * @param l_max value for which the weight is 0
     * @return a scalar value between 0 and 1
     */
    Scalar Weight(Scalar x, Scalar d, Scalar l_min, Scalar l_max) const;
    void WeightValAndDeriv(Scalar x, Scalar d, Scalar l_min, Scalar l_max,
                           Scalar& res, Scalar& res_dx1, Scalar& res_dx2) const;

    /**
     * @brief Projection project point onto the iso-surface in the required direction within a maximal distance
     *
     * WARNING : assume that with a step of dist_lim we cannot cross two time the iso-surface
     * ie. assume details are somewhat smaller than the surface to be deformed
     *
     * TODO:@todo : doing cubic interpolation could provide a great improvement
     * in the case where the scalar product between dir_proj and the grad become negatif
     * TODO:@todo : do not take into account local minima of field that higher
     * than the iso-value of interest (which are problematic!)
     *
     * TODO:@todo : should remember value on the bound of interval of research
     * TODO:@todo : for improve stability, could also use a bound on the maximal step size
     *
     * TODO:@todo : for now when change of sign, use dichotomy to determine what is
     * the case => slow
     *
     * @param init_point        Point to be projected
     * @param f_init_point      Scalar value at the initial point
     * @param dir_projection    Normalized direction of projection
     * @param dist_lim          Maximal signed distance of projection
     * @param res_point         Result of projection if it has succeed
     * @return return true if the projection succeed, false otherwise
     */
    bool Projection(const Point& init_point,
                    Scalar f_init_point, const Vector& grad_f_init,
                    Vector& dir_projection, Scalar dist_lim, Vector& res_point) const;

public :


    /////////////////
    /// Accessors ///
    /////////////////

    inline Scalar iso_value() const { return this->iso_value_; }
    inline Scalar iso_detail_inf() const { return this->iso_detail_inf_; }
    inline Scalar iso_detail_sup() const { return this->iso_detail_sup_; }

    virtual const core::Functor& noise_functor() const = 0;

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_iso_value(Scalar iso_value){ this->iso_value_ = iso_value; }
    void set_iso_detail_inf(Scalar iso_detail_inf){ this->iso_detail_inf_ = iso_detail_inf; }
    void set_iso_detail_sup(Scalar iso_detail_sup){ this->iso_detail_sup_ = iso_detail_sup; }

    /////////////////
    /// Attributs ///
    /////////////////

protected :
    /** @brief iso_value_ iso-value corresponding to the surface to be deformed */
    Scalar iso_value_;
    /** @brief iso_detail_inf_ limit of deformation outside the object */
    Scalar iso_detail_inf_;
    /** @brief iso_detail_inf_ limit of deformation inside the object */
    Scalar iso_detail_sup_;

    BlendSum blend_operator_; //TODO:@todo : if we successully re-work the property system, this could be avoided...

public:
    // Stuff for Eval...Direction
    static const std::vector<ESFVectorProperties> vect_prop_ids_dir_;
    static const std::vector<ESFScalarProperties> scal_prop_ids_empty_;

protected:
    static std::vector<ESFVectorProperties> MakeEFSVectorPropertiesDir()
    {
        std::vector<ESFVectorProperties> res;
        res.push_back(ExpressiveTraits::E_Direction);
        return res;
    }


    static std::vector<ESFScalarProperties> MakeEFSScalarPropertiesEmpty()
    {
        std::vector<ESFScalarProperties> res;
        return res;
    }

protected:
    /*! vector used to save important value during the evaluation: currently not thread safe*/
    mutable std::vector<Vector> vect_prop_res_dir_;
    mutable std::vector<Vector> vect_prop_res_dir_aux_;
    mutable std::vector<Scalar> scal_prop_res_empty_;
};


template<typename TFunctorNoise>
class NodeProceduralDetailT :
        public AbstractNodeProceduralDetail
{
public:
    typedef PolynomialDeformationMap TFunctorDeformationMap;

    typedef NodeProceduralDetailParameterT<TFunctorNoise> NodeProceduralDetailParameter;
    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    NodeProceduralDetailT(Scalar iso_value, Scalar iso_detail_inf, Scalar iso_detail_sup,
                          const TFunctorNoise& noise_functor)
        : AbstractNodeProceduralDetail(iso_value,iso_detail_inf,iso_detail_sup),
          noise_functor_(noise_functor)
    {
        this->vect_prop_res_dir_.resize(1);
        this->vect_prop_res_dir_aux_.resize(1);
    }

    /**
     * @brief NodeProceduralDetailT version of the constructor required by the factory.
     * Calling this constructor imply initializing node parameter by hand
     * @param noise_functor
     */
    NodeProceduralDetailT(const TFunctorNoise& noise_functor)
        : AbstractNodeProceduralDetail(1.0, 0.5, 1.5),
          noise_functor_(noise_functor)
    {
        this->vect_prop_res_dir_.resize(1);
        this->vect_prop_res_dir_aux_.resize(1);
    }

    /**
     * @brief NodeProceduralDetailT version of the constructor required by the CloneForEval
     * @param rhs
     */
    NodeProceduralDetailT(const NodeProceduralDetailT &rhs) : AbstractNodeProceduralDetail(rhs),
        noise_functor_(rhs.noise_functor_),
        deformation_map_functor_(rhs.deformation_map_functor_){}

    ~NodeProceduralDetailT(){}


    virtual std::shared_ptr<ScalarField> CloneForEval() const;

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        AbstractNodeProceduralDetail::AddXmlAttributes(element);
        AddXmlFunctor(element);
    }
    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        AbstractNodeProceduralDetail::AddXmlAttributes(element, ids);
        AddXmlFunctor(element);
    }
    void AddXmlFunctor(TiXmlElement * element) const
    {
        TiXmlElement *element_functor_noise = new TiXmlElement( XmlFunctorField() );
        element->LinkEndChild(element_functor_noise);
        TiXmlElement * element_child_noise = noise_functor_.ToXml();
        element_functor_noise->LinkEndChild(element_child_noise);

        TiXmlElement *element_functor_deformation_map = new TiXmlElement( "TFunctorDeformationMap" );
        element->LinkEndChild(element_functor_deformation_map);
        TiXmlElement * element_child_deform = deformation_map_functor_.ToXml();
        element_functor_deformation_map->LinkEndChild(element_child_deform);
    }

    virtual void InitFromXml(const TiXmlElement *element);

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    void EvalDeformationAmplitude(const Point &p_proj,
                                  const Vector& normal, const Vector& main_dir, const Vector& ortho_dir,
                                  const std::vector<Scalar>& children_field, Scalar field,
                                  Scalar& deformation_amplitude, Scalar& limit_min, Scalar& limit_max) const
    {
        Scalar noise;
        Vector grad_noise;
        noise_functor_.ComputeInterpolatedNoiseAndGrad(
                    p_proj,
                    normal, main_dir, ortho_dir,
                    children_field, field,
                    this->children_,
                    [](const std::vector<std::shared_ptr<BlobtreeNode> >&  v, int i) -> const typename TFunctorNoise::NoiseParameters&
                    { return (std::static_pointer_cast<NodeProceduralDetailParameterT<typename  TFunctorNoise::NoiseParameters> >(v[i]))->noise_parameter(); },
                    noise, grad_noise
        );

        noise = (noise < 0.0) ? 0.0 : ((noise > 1.0) ? 1.0 : noise); // clipping of the noise

        Scalar deriv_amplitude;
        deformation_map_functor_.ComputeInterpolatedDeformation(
                    noise,
                    children_field, field,
                    this->children_,
                    [](const std::vector<std::shared_ptr<BlobtreeNode> >&  v, int i) -> const TFunctorDeformationMap&
                    { return (std::static_pointer_cast<NodeProceduralDetailParameterT<typename  TFunctorNoise::NoiseParameters> >(v[i]))->deformation_map_functor(); },
                    deformation_amplitude, deriv_amplitude,
                    limit_min, limit_max
        );
    }

    void EvalDeformationAmplitudeAndGrad(const Point &p_proj,
                                         const Vector& normal, const Vector& main_dir, const Vector& ortho_dir,
                                         const Vector& dir_proj,
                                         const std::vector<Scalar>& children_field, Scalar field,
                                         Scalar& deformation_amplitude, Vector& grad_deform_amplitude,
                                         Scalar& limit_min, Scalar& limit_max) const
    {
        Scalar noise;
        Vector grad_noise;
        noise_functor_.ComputeInterpolatedNoiseAndGrad(
                    p_proj,
                    normal, main_dir, ortho_dir,
                    children_field, field,
                    this->children_,
                    [](const std::vector<std::shared_ptr<BlobtreeNode> >&  v, int i) -> const typename TFunctorNoise::NoiseParameters&
                    { return (std::static_pointer_cast<NodeProceduralDetailParameterT<typename  TFunctorNoise::NoiseParameters> >(v[i]))->noise_parameter(); },
                    noise, grad_noise
        );
        noise = (noise < 0.0) ? 0.0 : ((noise > 1.0) ? 1.0 : noise); // clipping of the noise
        // => can create visible normal discontinuity => should find alternative solution

        Scalar deriv_amplitude;
        deformation_map_functor_.ComputeInterpolatedDeformation(
                    noise,
                    children_field, field,
                    this->children_,
                    [](const std::vector<std::shared_ptr<BlobtreeNode> >&  v, int i) -> const TFunctorDeformationMap&
                    { return (std::static_pointer_cast<NodeProceduralDetailParameterT<typename  TFunctorNoise::NoiseParameters> >(v[i]))->deformation_map_functor(); },
                    deformation_amplitude, deriv_amplitude,
                    limit_min, limit_max
        );

        //TODO:@todo : this is an approximation
        EigenVectorTraits::Vector dir_proj_e(dir_proj.x, dir_proj.y, dir_proj.z);
        EigenVectorTraits::Vector normal_e(normal.x, normal.y, normal.z);
        EigenVectorTraits::Vector grad_noise_e(grad_noise.x, grad_noise.y, grad_noise.z);

        EigenVectorTraits::Vector tmp = (EigenVectorTraits::Matrix3d::Identity() - (1.0/(dir_proj_e.dot(normal_e))) * dir_proj_e * normal_e.transpose()) * (deriv_amplitude * grad_noise_e);
        grad_deform_amplitude = Vector(tmp[0], tmp[1], tmp[2]);
//        grad_deform_amplitude.noalias() = (Matrix3::Identity() - (1.0/(dir_proj.dot(normal))) * dir_proj * normal.transpose()) * (deriv_amplitude * grad_noise);
    }

    ///////////////////////////
    /// Evaluation functions //
    ///////////////////////////

    virtual void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                                typename std::map<BlobtreeNode*, typename std::pair<core::AABBox, core::AABBox> >& updated_areas)
    {
        this->blend_operator_.PrepareForEval(*this, epsilon_bbox, accuracy_needed, updated_areas);
        this->UpdateNodeCompacity();
    }

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual core::AABBox GetAxisBoundingBox(Scalar value) const
    {
        return this->blend_operator_.GetAxisBoundingBox(this->children(), value);
    }

    ///TODO:@todo : to be improved (cleaner, faster and more reliable code...)
    virtual Point GetAPointAtWithoutNoise(Scalar value, Scalar epsilon) const;
    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const;

    /////////////////
    /// Accessors ///
    /////////////////

    virtual const core::Functor& noise_functor() const { return noise_functor_; }
    inline const TFunctorNoise& noise_functor() { return noise_functor_; }
    inline const TFunctorDeformationMap& deformation_map_functor() const { return deformation_map_functor_; }

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_noise_functor(const TFunctorNoise& noise_functor)
    {
        noise_functor_ = noise_functor;
    }
    void set_deformation_map_functor(const TFunctorDeformationMap& deformation_map_functor)
    {
        deformation_map_functor_ = deformation_map_functor;
    }


    /////////////////
    /// Attributs ///
    /////////////////

private:
    TFunctorNoise noise_functor_;

    TFunctorDeformationMap deformation_map_functor_; //THIS ONE IS TO BE CHANGED

};

} // Close namespace Convol

} // Close namespace expressive

// implementation file
#include <convol/blobtreenode/NodeProceduralDetailT.hpp>

#endif
