#include <convol/blobtreenode/NodeScalarField.h>

using namespace expressive::core;

namespace expressive {

namespace convol {


void NodeScalarField::PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                            std::map<BlobtreeNode*, std::pair<AABBox, AABBox> >& updated_areas)
{
    //@to_be_confirmed
    UNUSED(accuracy_needed);

    if (epsilon_bbox != this->epsilon_bbox_ && !this->IsCompactAndValid())
    {
        std::map<BlobtreeNode*, std::pair<AABBox, AABBox> >::iterator it = updated_areas.find(this);
        if(it != updated_areas.end())
        {
            InitAxisBoundingBox(epsilon_bbox);
            (*it).second.first = this->axis_bounding_box();
        }
        else
        {
            std::pair<AABBox, AABBox> bbox_pair(AABBox(), this->axis_bounding_box());
            InitAxisBoundingBox(epsilon_bbox);
            bbox_pair.first = this->axis_bounding_box();
            updated_areas.insert(std::pair<BlobtreeNode*, std::pair<AABBox, AABBox> >(this, bbox_pair));
        }
    }
}


void NodeScalarField::GetPrimitivesPointsAt(Scalar value, Scalar epsilon, core::PointCloud& res_points, const AABBox& b_box) const
{
    //TODO : @todo : perhaps use the axis bounding box in order to improve GetAPointAt
    UNUSED(b_box);

    Point p = GetAPointAt(value,epsilon);
    Normal n = -EvalGrad(p,epsilon*0.1);
    Scalar nrm = n.norm();
    // If the gradient is null, then we give some random direction in order to avoid convergence issue when going up the blobtree.
    if(nrm == 0.0){
        n = Vector::Zero();
        n[0] = 1.0;
        res_points.insert(p,n);
    }
    else
    {
        n.normalize();
        res_points.insert(p,n);
    }
}


auto NodeScalarField::InitAxisBoundingBox(Scalar value) -> const AABBox&
{
    if(this->epsilon_bbox_ != value)
    {
        this->axis_bounding_box_ = GetAxisBoundingBox(value);
        this->epsilon_bbox_ = IsCompact() ? 0.0 : value;
        this->BlobtreeNodeABBoxUpdated();
    }
    return this->axis_bounding_box_;
}


} // Close namespace convol

} // Close namespace expressive
