/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: NodeScalarFieldT.h
 
 Language: C++
 
 License: Convol license
 
 \author: Cedric Zanni
 E-Mail:  cedric.zanni@inrialpes.fr
 \author: Maxime Quiblier
 E-Mail:  maxime.quiblier@inrialpes.fr
 
 Description: Class for basic node operators in the blobtree.
              They usually need template argument functor to define what kind of merge they actually do (Sum, Max, etc...).

 Platform Dependencies: None

*/

#pragma once
#ifndef CONVOL_NODE_OPERATOR_H_
#define CONVOL_NODE_OPERATOR_H_

// core dependencies
#include <core/CoreRequired.h>
    // functor
    #include<core/functor/Functor.h>
//    #include<core/functor/EnumFunctorType.h>

// convol dependencies
//    // Macro
//    #include<Convol/include/Macro/CONVOL_MACRO_NAME.h>
//    #include<Convol/include/Macro/CONVOL_MACRO_FUNCTOR.h>
#include<convol/ScalarField.h>
    // BlobtreeNode
    #include<convol/blobtreenode/BlobtreeNode.h>
//    #include<Convol/include/ScalarFields/blobtreenode/EnumNodeOperatorType.h>
    
//// std dependencies
//#include <string>
//#include <vector>
//#include <sstream>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/xml/tinyxml/tinyxml.h>

namespace expressive {
namespace convol {

class CONVOL_API NodeOperator :
    public BlobtreeNode
{
public:
    NodeOperator() : BlobtreeNode("NodeOperator"), id_(node_operator_current_id_++){}
    NodeOperator(const std::string &name) : BlobtreeNode(name.c_str()), id_(node_operator_current_id_++) {}
    NodeOperator(const NodeOperator &rhs) : BlobtreeNode(rhs), id_(node_operator_current_id_++) {}
    ~NodeOperator(){}

    static std::string StaticName()
    {
        return std::string("NodeOperator");
    }

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement *element) const
    {
        BlobtreeNode::AddXmlAttributes(element);
        element->SetAttribute("id", (int)(uintptr_t)this);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        BlobtreeNode::AddXmlAttributes(element, ids);
        element->SetAttribute("id", (int)(uintptr_t)this);
        ids[this] = (unsigned int) (uintptr_t) this;
    }

    virtual void InitFromXml(const TiXmlElement * /*element*/) {}

    //////////////////////
    /// Eval functions ///
    //////////////////////

    virtual Scalar Eval(const Point& point) const =0;

    //TODO:@todo : a remplacer (sans doute pas optimal & pas valable pour tout les mélanges !! (notament soustraction))
    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const;

protected:
    unsigned int id_;

    static unsigned int node_operator_current_id_;
};


// This class is just here for all NodeOperator2 to share some data.
class AbstractNodeOperator2 :
    public NodeOperator
{
public:

    static std::string StaticName()
    {
        return std::string("NodeOperator2");
    }

    /////////////////////
    /// Xml functions ///
    /////////////////////

    static std::string XmlFunctorField() { return std::string("TFunctorBlendOperator2"); }

    ///////////////
    // Accessors //
    ///////////////
    virtual const core::Functor& blend_operator() const = 0;

};

template <typename TFunctorBlendOperator2>
class NodeOperator2T :
    public AbstractNodeOperator2
{
public:
    NodeOperator2T()
    {
        this->parent_ = NULL;
        
        this->arity_ = 2;
        this->children_.resize(2,NULL);
    }
    NodeOperator2T(BlobtreeNode* blobtree_node_1, BlobtreeNode* blobtree_node_2)
    {
        assert(blobtree_node_1 != NULL && blobtree_node_2 != NULL);
        
        this->parent_ = NULL;

        this->arity_ = 2;
        this->children_.resize(2,NULL);
        this->children_[0] = blobtree_node_1;
                blobtree_node_1->set_parent(this);
        this->children_[1] = blobtree_node_2;        
                blobtree_node_2->set_parent(this);
    }
    NodeOperator2T(TFunctorBlendOperator2 blend_operator)
    {
        this->parent_ = NULL;

        this->arity_ = 2;
        this->children_.resize(2,NULL);

        blend_operator_ = blend_operator;
    }
    NodeOperator2T(BlobtreeNode* blobtree_node_1, BlobtreeNode* blobtree_node_2, TFunctorBlendOperator2 blend_operator)
    {
        assert(blobtree_node_1 != NULL && blobtree_node_2 != NULL);
        
        this->parent_ = NULL;
        
        this->arity_ = 2;
        this->children_.resize(2,NULL);
        this->children_[0] = blobtree_node_1;
        blobtree_node_1->set_parent(this);
        this->children_[1] = blobtree_node_2;        
        blobtree_node_2->set_parent(this);
        
        blend_operator_ = blend_operator;
    }

    NodeOperator2T(const NodeOperator2T &rhs) : AbstractNodeOperator2(rhs),
        blend_operator_(rhs.blend_operator_){}

    ~NodeOperator2T(){}

    virtual std::shared_ptr<NodeOperator2T> clone() const
    {
        return std::make_shared<NodeOperator2T>(*this);
    }

    virtual std::shared_ptr<ScalarField> CloneForEval() const
    {
        return std::make_shared<NodeOperator2T>(*this);
    }
//    virtual string FunctorName() const
//    {
//        std::ostringstream name;
//        name << "<";
//        name << blend_operator_.Name() ;
//        name << blend_operator_.FunctorName() ;
//        name << blend_operator_.ParameterName() << ">";
//        return name.str();
//    }

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        AbstractNodeOperator2::AddXmlAttributes(element);

        TiXmlElement * element_blend_operator = new TiXmlElement( XmlFunctorField() );
        element->LinkEndChild(element_blend_operator);

        TiXmlElement * element_child = blend_operator_.ToXml();
        element_blend_operator->LinkEndChild(element_child);
    }


    //////////////////////////
    // Evaluation functions //
    //////////////////////////
    
    virtual Scalar Eval(const Point& point) const
    {
        assert(this->children_[0] != NULL && this->children_[1] != NULL);

        return blend_operator_(*(this->children_[0]), *(this->children_[1]), point);
    }
    virtual void EvalValueAndGrad(  const Point& point,
                                    Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        blend_operator_.EvalValueAndGrad(*(this->children_[0]), *(this->children_[1]), point, epsilon_grad, value_res, grad_res);
    }
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
    {
        blend_operator_.EvalValueAndGradAndProperties(*(this->children_[0]), *(this->children_[1]),
                                                       point, epsilon_grad,
                                                       scal_prop_ids, vect_prop_ids,
                                                       value_res, grad_res, scal_prop_res, vect_prop_res);
    }

    virtual void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                                std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& updated_areas)
    {
        blend_operator_.PrepareForEval(*this, epsilon_bbox, accuracy_needed, updated_areas);
    }

    /////////////////////////////
    // BoundingBox computation //
    /////////////////////////////

    virtual core::AABBox GetAxisBoundingBox(Scalar value) const
    {
        return blend_operator_.GetAxisBoundingBox(this->children(), value);
    }

    ///////////////
    // Accessors //
    ///////////////

    virtual const core::Functor& blend_operator() const { return blend_operator_; }
    const TFunctorBlendOperator2& blend_operator()
    {
        return blend_operator_;
    }

protected:
private:

    TFunctorBlendOperator2 blend_operator_;
};


// This class is just here for all NodeOperatorNArity to share some data.
class AbstractNodeOperatorNArity :
    public NodeOperator
{
public:
    EXPRESSIVE_MACRO_NAME("NodeOperatorNArity")

    /////////////////////
    /// Xml functions ///
    /////////////////////

    static std::string XmlFunctorField() { return std::string("TFunctorBlendOperatorN"); }

    ///////////////
    // Accessors //
    ///////////////
    virtual const core::Functor& blend_operator() const = 0;
};


template <typename TFunctorBlendOperatorN>
class NodeOperatorNArityT :
    public AbstractNodeOperatorNArity
{
public:
    NodeOperatorNArityT()
    {
        this->parent_ = nullptr;
        this->arity_ = -1;
    }
    NodeOperatorNArityT(typename std::vector<BlobtreeNode*> bt_node_vec)
    {
        this->parent_ = nullptr;
        this->arity_ = -1;

        for(typename std::vector<BlobtreeNode*>::iterator it = bt_node_vec.begin(); it != bt_node_vec.end(); ++it)
        {
            assert( *it != NULL );
            (this->children_).push_back((*it)->shared_from_this());
            (*it)->parent_ = this;
        }
    }
    NodeOperatorNArityT(TFunctorBlendOperatorN blend_operator)
        : blend_operator_(blend_operator)
    {
        this->parent_ = nullptr;
        this->arity_ = -1;
    }
    NodeOperatorNArityT(typename std::vector<BlobtreeNode*> bt_node_vec, TFunctorBlendOperatorN blend_operator)
    {
        this->parent_ = nullptr;
        this->arity_ = -1;
        
        for(typename std::vector<BlobtreeNode*>::iterator it = bt_node_vec.begin(); it != bt_node_vec.end(); ++it)
        {
            assert( *it != NULL );
            (this->children_).push_back((*it)->shared_from_this());
            (*it)->parent_ = this;
        }                
    
        blend_operator_ = blend_operator;
    }
    // Copy constructor required for  cloning
    NodeOperatorNArityT(const NodeOperatorNArityT &rhs) : AbstractNodeOperatorNArity(rhs),
        blend_operator_(rhs.blend_operator_){}

    ~NodeOperatorNArityT(){}
  
    virtual std::shared_ptr<ScalarField> CloneForEval() const
    {
        return std::make_shared<NodeOperatorNArityT>(*this);
    }
//    ////////////////////
//    // Name functions //
//    ////////////////////
//    virtual inline std::string FunctorName() const
//    {
//        std::ostringstream name;
//        name << "<";
//        name << blend_operator_.Name() ;
//        name << blend_operator_.FunctorName() ;
//        name << blend_operator_.ParameterName() << ">";
//        return name.str();
//    }

    /////////////////////
    /// Xml functions ///
    /////////////////////

    virtual void AddXmlAttributes(TiXmlElement * element) const
    {
        AbstractNodeOperatorNArity::AddXmlAttributes(element);

        TiXmlElement * element_blend_operator = new TiXmlElement( XmlFunctorField() );
        element->LinkEndChild(element_blend_operator);

        TiXmlElement * element_child = blend_operator_.ToXml();
        element_blend_operator->LinkEndChild(element_child);
    }

    virtual void AddXmlAttributes(TiXmlElement * element, std::map<const void*, unsigned int> &ids) const
    {
        AbstractNodeOperatorNArity::AddXmlAttributes(element, ids);

        TiXmlElement * element_blend_operator = new TiXmlElement( XmlFunctorField() );
        element->LinkEndChild(element_blend_operator);

        TiXmlElement * element_child = blend_operator_.ToXml();
        element_blend_operator->LinkEndChild(element_child);
    }


    //////////////////////////
    // Evaluation functions //
    //////////////////////////
    
    virtual Scalar Eval(const Point &point) const
    {
        return blend_operator_(this->children_, point);
    }
    virtual Vector EvalGrad(const Point& point, Scalar epsilon) const
    {
        return blend_operator_.EvalGrad(this->children_, point, epsilon);
    }
    virtual void EvalValueAndGrad(const Point& point,
                                  Scalar epsilon_grad, Scalar& value_res, Vector& grad_res) const
    {
        return blend_operator_.EvalValueAndGrad(this->children_, point, epsilon_grad, value_res, grad_res);
    }
    virtual void EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                                const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                                const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                                Scalar& value_res, Vector& grad_res,
                                                std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                                ) const
    {
        return blend_operator_.EvalValueAndGradAndProperties(this->children_,
                                                             point, epsilon_grad,
                                                             scal_prop_ids, vect_prop_ids,
                                                             value_res, grad_res, scal_prop_res, vect_prop_res);
    }

    virtual void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                                std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& updated_areas)
    {
        blend_operator_.PrepareForEval(*this, epsilon_bbox, accuracy_needed, updated_areas);
        this->UpdateNodeCompacity();
    }

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual core::AABBox GetAxisBoundingBox(Scalar value) const
    {
        return blend_operator_.GetAxisBoundingBox(this->children(), value);
    }

    ///////////////
    // Accessors //
    ///////////////

    virtual const core::Functor& blend_operator() const { return blend_operator_; }
    const TFunctorBlendOperatorN& blend_operator()
    {
        return blend_operator_;
    }

private:

    TFunctorBlendOperatorN blend_operator_;
};

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_NODE_OPERATOR_H_
