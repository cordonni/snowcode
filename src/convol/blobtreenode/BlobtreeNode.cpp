#include <convol/blobtreenode/BlobtreeNode.h>

// core
#include <ork/resource/ResourceTemplate.h>

// Convol tools
#include <convol/blobtreenode/BlobtreeRoot.h>
#include <convol/tools/ConvergenceTools.h>

using namespace expressive::core;
using namespace ork;
using namespace std;

namespace expressive {

namespace convol {

BlobtreeNode::BlobtreeNode(const BlobtreeNode &rhs, bool copyParent) : ScalarField(rhs), Serializable(rhs.getClass()),
    std::enable_shared_from_this<BlobtreeNode>(), label_(rhs.label_),
    arity_(rhs.arity_), parent_(copyParent ? rhs.parent_ : nullptr), axis_bounding_box_(rhs.axis_bounding_box_),
    epsilon_bbox_(copyParent ? -1.0 : 0.0) //TODO:@todo : WHY 0.0 ?
{
    children_.reserve(rhs.children_.size());
    for ( std::vector<std::shared_ptr<BlobtreeNode> >::const_iterator it=rhs.children_.begin();
         it != rhs.children_.end(); ++it)
    {
        std::shared_ptr<BlobtreeNode> newChild = std::dynamic_pointer_cast<BlobtreeNode>((*it)->CloneForEval());
        newChild->parent_ = this;
        children_.push_back(newChild);
    }
}

BlobtreeRoot* BlobtreeNode::nonconst_root() const
{
    return dynamic_cast<BlobtreeRoot*>(nonconst_ancestor());
}

BlobtreeRoot const* BlobtreeNode::root() const
{
    return dynamic_cast<BlobtreeRoot const*>(ancestor());
}

/**
 * @brief LoadBlobtreeNodeChildren This method is the base load method for every type of BlobtreeNode.
 * all of them will call this eventually.
 * @param node
 * @param manager
 * @param desc
 * @param e
 */
void LoadBlobtreeNodeChildren(BlobtreeNode *node, std::shared_ptr<ResourceManager> manager, std::shared_ptr<ResourceDescriptor> desc, const TiXmlElement *e)
{
    e = e == NULL ? desc->descriptor : e;
    // checkParameters() should be placed in the classes that inherit from BlobtreeNode.

    // Getting the "children" node.
    const TiXmlElement *children = e->FirstChildElement("children");
    if (children != NULL) {
        int num_children = 0;

        // Checking it's flags "number" & "label".
        Resource::getIntParameter(desc, children, "number", &num_children);
        if (children->Attribute("label") != NULL) {
            node->set_label(Resource::getParameter(desc, children, "label"));
        }

        // Loop through children nodes, and add them to our BlobtreeNode.
        node->nonconst_children().resize(num_children, NULL);
        const TiXmlElement *child = children->FirstChildElement();
        for (int i = 0; i < num_children; ++i) {
            if (child != NULL) {
                // TODO : Transform #children_ into a vector of shared_ptr (or anything similar).
                // resourceLoader only loads shared_ptrs
                // Then, we can actually add children to a blobtreenode from here.
                shared_ptr<BlobtreeNode> child_node = dynamic_pointer_cast<BlobtreeNode>(manager->loadResource(desc, child));
                node->set_child(i, child_node);

                child = child->NextSiblingElement();
            }
        }
    } else {
        Logger::ERROR_LOGGER->log("RESOURCES", "ERROR in BlobtreeNodeXmlLoaderT::LoadChildren : No _children_ attribute.");
        assert(false);
        return;
    }
}

void BlobtreeNode::GetPrimitivesPointsAt(Scalar value, Scalar epsilon, core::PointCloud& res_points, const AABBox& b_box) const
{
    PointCloud res_points_tmp;
    for( std::vector<shared_ptr<BlobtreeNode> >::const_iterator it = this->children_.begin(); it != this->children_.end(); it++)
    {
        if(b_box.Intersect((*it)->axis_bounding_box()))
        {
            (*it)->GetPrimitivesPointsAt(value, epsilon, res_points_tmp, b_box);
            res_points.splice(res_points_tmp);
        }
    }

    // Now converge for all point according to the result of the node.
    Scalar bbox_size = this->axis_bounding_box_.ComputeSizeMax();
    // temporary variables for convergence function
    Scalar min_absc,max_absc;
    Point origin;
    for( PointCloud::iterator it = res_points.begin(); it != res_points.end();)
    {
        Point& p = res_points.point(*it);
        Normal& n = res_points.normal(*it);

        if(n[0]!=0.0 || n[1]!=0.0 || n[2]!=0.0){
            min_absc = -bbox_size;
            max_absc = bbox_size;
            origin = p;
            Convergence::SafeNewton1D(  *this,
                                            origin,
                                            n,
                                            min_absc,
                                            max_absc,
                                            0.0,
                                            value,
                                            epsilon,
                                            epsilon*0.1,    // arbitrarily divide by 10 to have a better gradient estimation in case of numerical computing
                                            10,
                                            p);
            // Update the normal to the point
            n = -this->EvalGrad(p,epsilon*0.1);
            n.normalize(); //TODO:@todo  following function does not exist in eigen, in some cases, can be dangerous //n.normalize_cond();
            ++it;
        }
        else
        {
            // Discard the point since it's proabbly outside anything useful...
            it = res_points.erase(it);
        }
    }
}


void BlobtreeNode::InvalidateSubtreeBoundingBox()
{
    epsilon_bbox_ = -1.0;
    for (auto it=children_.begin(); it != children_.end(); ++it)
    {
        (*it)->InvalidateSubtreeBoundingBox();
    }
}


void BlobtreeNode::InvalidateBoundingBox()
{
    // The invalidation is lazy. It supposes that if a node has not a valid bounding box,
    // its parent has also already an invalid bbox.
    if(!IsBoundingBoxInvalidated())
    {
        epsilon_bbox_ = -1.0;
        if(parent_ != NULL)
        {
            parent_->InvalidateBoundingBox();
        }
    }
}


void BlobtreeNode::UpdateNodeCompacity()
{
    if (!IsCompactAndValid() && !children_.empty())
    {
        assert(epsilon_bbox_ > 0.);
        for ( std::vector<shared_ptr<BlobtreeNode> >::iterator it = children_.begin();
             it != children_.end(); ++it)
            if (!(*it)->IsCompactAndValid())
                return;

        epsilon_bbox_ = 0.;
    }
}


void BlobtreeNode::DeleteChildrenBlobtreeNode()
{
    for( std::vector<shared_ptr<BlobtreeNode> >::iterator it = children_.begin(); it != children_.end(); ++it)
    {
        if( (*it) != NULL )
        {
            (*it)->DeleteChildrenBlobtreeNode();
            (*it)->parent_ = NULL;
//            delete (*it); // TODO : check this !! should not be necessary anymore.
//            (*it) = NULL;
        }
    }

    children_.clear();
}


void BlobtreeNode::set_child(unsigned int i, shared_ptr<BlobtreeNode> bt_node)
{
//        //TODO:@todo : has for AddChild I think this should be a condition to be checked
//        assert(bt_node->parent() == NULL); // please disconnect the node from its parent before anything.
    assert(arity_ == -1 || int(i) < arity_);
    assert(i<children_.size());

    if(bt_node->parent() != NULL)
    {
        bt_node->nonconst_parent()->RemoveChild(bt_node);
    }
    bt_node->parent_ = this;
    children_[i] = bt_node;

    InvalidateBoundingBox();
}


void BlobtreeNode::AddChild(std::shared_ptr<BlobtreeNode> bt_node)
{
    assert(bt_node->parent() == NULL); // please disconnect the node from its parent before anything.
    assert(bt_node != NULL && (arity_ == -1 && bt_node != NULL)); // TODO : @todo

    if(bt_node->parent() != NULL)
    {
        bt_node->nonconst_parent()->RemoveChild(bt_node);
    }
    bt_node->parent_ = this;
    children_.push_back(bt_node);

    InvalidateBoundingBox();
}


void BlobtreeNode::ReplaceChild(BlobtreeNode* old_child, std::shared_ptr<BlobtreeNode> new_child)
{
    assert(new_child->parent() == NULL); // please disconnect the node from its parent before anything.
    for(std::vector<std::shared_ptr<BlobtreeNode> >::iterator it = children_.begin(); it != children_.end(); ++it)
    {
        if((*it).get() == old_child)
        {
            old_child->parent_ = NULL;
            *it = new_child;
            new_child->parent_ = this;
            break;
        }
    }

    InvalidateBoundingBox();
}


void BlobtreeNode::RemoveChild(shared_ptr<BlobtreeNode> child)
{
    RemoveChild(child.get());
}

void BlobtreeNode::RemoveChild(BlobtreeNode* child)
{
    for(std::vector<std::shared_ptr<BlobtreeNode> >::iterator it = children_.begin(); it != children_.end(); ++it)
    {
        if( (*it).get() == child )
        {
            (*it)->NotifyParentOfAModifiedAreaAfterDeletion((*it)->axis_bounding_box());
            if(arity_ == -1)
            {
                children_.erase(it);
            }
            else
            {
                *it = NULL;
            }
            child->parent_ = NULL;

            break;
        }
    }

    InvalidateBoundingBox();
}


void BlobtreeNode::RemoveChild(unsigned int i)
{
    assert(i<children_.size());
    children_[i]->NotifyParentOfAModifiedAreaAfterDeletion(children_[i]->axis_bounding_box());
    children_[i]->parent_ = NULL;
    if(arity_ == -1)
    {
        // We suppose that order does not matter
        if(children_.size() > 1)
        {
            children_[i] = children_[children_.size()-1];
            children_.pop_back();
        }
        else
        {
            children_.clear();
        }
    }
    else
    {
        children_[i] = NULL;
    }

    InvalidateBoundingBox();
}


void BlobtreeNode::RecursiveNodeCorrection()
{
    switch (arity_)
    {
    case -1:
        {
            if(children_.size() == 0)
            {
                BlobtreeNode* parent = parent_;
                parent->RemoveChild(this);
                parent->RecursiveNodeCorrection();
                //                    delete this; TODO : check this
            }
        }
        break;
    case 2:
        {
            if(children_[0] != NULL)
            {
                if(children_[1] == NULL)
                {
                    std::shared_ptr<BlobtreeNode> removed_child = children_[0];
                    RemoveChild(uint(0));
                    parent_->ReplaceChild(this,removed_child);
//                    delete this; TODO : check this
                }
            }
            else if(children_[1] != NULL)
            {
                std::shared_ptr<BlobtreeNode> removed_child = children_[1];
                RemoveChild(1);
                parent_->ReplaceChild(this,removed_child);
                delete this;
            }
            else
            {
                BlobtreeNode* parent = parent_;
                parent->RemoveChild(this);
                parent->RecursiveNodeCorrection();
                delete this;
            }
        }
        break;
    default:
        {
            assert(false); // not implemented for this arity
        }
        break;
    }
}

} // Close namespace convol

} // Close namespace expressive
