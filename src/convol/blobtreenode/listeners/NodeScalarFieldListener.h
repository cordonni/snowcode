/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: NodeScalarFieldListenerT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for NodeScalarFieldT listener class.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_NODE_SCALAR_FIELD_LISTENER_T_H_
#define CONVOL_NODE_SCALAR_FIELD_LISTENER_T_H_

// core dependencies
#include <convol/ConvolRequired.h>

namespace expressive {

namespace convol {


// Convol Skeleton dependencies
class NodeScalarField;

/*! \brief Class to listen to a skeleton basic segments events.
 */
class CONVOL_API NodeScalarFieldListener
{
public:
    NodeScalarFieldListener () {}
    virtual ~NodeScalarFieldListener () {}

    // This function is called just before node destruction
    virtual void NodeScalarFieldDeleting(NodeScalarField *) {}
    virtual void NodeScalarFieldBBoxUpdated(NodeScalarField *) {}
};

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_NODE_SCALAR_FIELD_T_H_
