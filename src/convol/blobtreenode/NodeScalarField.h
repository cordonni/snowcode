/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: NodeScalarFieldT.h
 
 Language: C++
 
 License: Convol license
 
 \author: Cedric Zanni
 E-Mail:  cedric.zanni@inrialpes.fr
 \author: Maxime Quiblier
 E-Mail:  maxime.quiblier@inrialpes.fr
 
 Description: Class for the management leaf. A leaf is a node containg a ScalarField.

 Platform Dependencies: None

*/

#pragma once
#ifndef CONVOL_NODE_SCALAR_FIELD_T_H_
#define CONVOL_NODE_SCALAR_FIELD_T_H_

// core dependencies
#include <core/CoreRequired.h>
    // geometry
    #include<core/geometry/AABBox.h>
    #include<core/geometry/PointCloud.h>

// convol dependencies
//    // macro
//    #include<Convol/include/Macro/CONVOL_MACRO_NAME.h>
    // ScalarFields
    #include<convol/ScalarField.h>
    // BlobtreeNode
    #include<convol/blobtreenode/BlobtreeNode.h>
    #include<convol/blobtreenode/listeners/NodeScalarFieldListener.h>

//// std dependencies
//#include <string>
//#include <set>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib) (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/xml/tinyxml/tinyxml.h>

namespace expressive {
namespace convol {

class CONVOL_API NodeScalarField :
    public BlobtreeNode
{
public:

    EXPRESSIVE_MACRO_NAME("NodeScalarField") // This macro defined functions
    //-------------------------------------------------------------------------
    // static const std::string& StaticName();
    //-------------------------------------------------------------------------
    // virtual std::string Name() const;
    //-------------------------------------------------------------------------

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////

    NodeScalarField(const std::string& name) : BlobtreeNode(name.c_str()) //TODO:@todo : update serializable and intermediate constructor to take a std::string as input instead of a char*
    {
        this->parent_ = nullptr;
        this->arity_ = 0;
    }
    //-------------------------------------------------------------------------
    virtual ~NodeScalarField()
    {
        NodeScalarFieldDeleting();
    }

    virtual std::shared_ptr<ScalarField> CloneForEval() const = 0;

//    //-------------------------------------------------------------------------
//    virtual std::string UserFriendlyName() const
//    {
//        return (this->label_.empty() ? scalar_field_->UserFriendlyName() : this->label_);
//    }

//    virtual std::string FunctorName() const
//    {
//        return "";
//    }
//    //-------------------------------------------------------------------------
//    virtual std::string ParameterName() const
//    {
//        return "";
//    }

//    ///////////////////
//    // Xml functions //
//    ///////////////////

    //-------------------------------------------------------------------------
    virtual void AddXmlAttributes(TiXmlElement * /*element*/) const
    {
        // Important de be redefined so that the following call never gets true.
        // Since NodeScalarField is supposed to be a leaf.
//        BlobtreeNode::AddXmlAttributes(element);
    }

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    virtual void PrepareForEval(Scalar epsilon_bbox, Scalar accuracy_needed,
                                std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& updated_areas);


    /** \brief  Give a list of points on the surface for each primitive separately (Should be at least 1 by primitive) epsilon close 
    *           to the ScalarField iso value.
    *           Typically this will be used as for seeding.
    */
    virtual void GetPrimitivesPointsAt(Scalar value, Scalar epsilon, core::PointCloud& res_points, const core::AABBox& b_box) const;


    /** \brief Find a point p in space where Eval(p) == value
    *        This is especially used for triangulation algorithms which needs
    *        a starting point on the surface.
    *   \param value
    *    \param epsilon We want the result to be at least epsilon close to the surface with respect to the distance Traits::Vector.norm()
    *   \return point p such that Eval(p) == value
    */
    virtual Point GetAPointAt(Scalar value, Scalar epsilon) const = 0;

    //////////////////////////
    /// Bounding functions ///
    //////////////////////////

    virtual const core::AABBox& InitAxisBoundingBox(Scalar value);

    virtual core::AABBox GetAxisBoundingBox(Scalar value) const = 0;

    virtual void GetPrecisionAxisBoundingBoxes(Scalar /*value*/, std::list<core::PrecisionAxisBoundingBox> &/*pboxes*/) const
    {
        assert(false && "GetPrecisionAxisBoundingBoxes not implemented for the given subtype of NodeScalarField");
    }

    ///////////////////////////
    /// Listener Management ///
    ///////////////////////////

    void AddListener(NodeScalarFieldListener* listener)    
    { 
        listener_set_.insert(listener); 
    }
    void RemoveListener(NodeScalarFieldListener* listener) 
    { 
        listener_set_.erase(listener);  
    }

protected:
    // Deep copy constructor :
    // It should only be called by the protected subclass copy constructors
    // which should only be called by the "clone" function
    // listener_set is not copyed
    NodeScalarField(const NodeScalarField &rhs, bool copyParent = false) : BlobtreeNode(rhs, copyParent) {}

private:
    // Listener
    std::set<NodeScalarFieldListener*> listener_set_;

    ///////////////////////////////////////////////////////////////////////////
    // Listener calls //
    ////////////////////
    void NodeScalarFieldDeleting()
    {
        for( std::set<NodeScalarFieldListener*>::iterator it = listener_set_.begin(); it != listener_set_.end(); ++it)
        {
            (*it)->NodeScalarFieldDeleting(this);
        }
    }
    ///////////////////////////
    // END OF listener calls //
    ///////////////////////////////////////////////////////////////////////////

};

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_NODE_SCALAR_FIELD_T_H_
