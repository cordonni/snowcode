/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


/*!
 \file: BlobtreeBasket.h
 
 Language: C++
 
 License: Convol license
 
 \author: Cedric Zanni
 E-Mail:  cedric.zanni@inrialpes.fr
 
 Description: Header for a trash blobtree (supposed to be right under a BlobtreeRoot).
              The subtree is not avaluated when calling the root evaluation functions.
 
 Platform Dependencies: None
 */

#pragma once
#ifndef CONVOL_BLOBTREE_BASKE_H_
#define CONVOL_BLOBTREE_BASKE_H_

// core dependencies
#include <core/CoreRequired.h>

// convol dependencies
    // Convol BlobtreeNode
    #include <convol/blobtreenode/BlobtreeNode.h>
//    // Convol Functor
//    #include<Convol/include/functors/Operators/BlendOperators/BlendMaxT.h>

//// std dependencies
//#include<vector>
//#include<set>
//#include<string>

//// tinyxml dependencies (tinyxml has been added to Convol core when it was decided to use Convol as a static lib)
//#include<Convol/include/tools/xml/tinyxml/tinyxml.h>

namespace expressive {

namespace convol {

class CONVOL_API BlobtreeBasket :
    public BlobtreeNode
{
public:
    EXPRESSIVE_MACRO_NAME("BlobtreeBasket")

    BlobtreeBasket() : BlobtreeNode(StaticName()){
        this->parent_ = NULL;
        this->arity_ = -1;
    }

    // Copy constructor : required for blobtree cloning
    BlobtreeBasket(const BlobtreeBasket &rhs) : BlobtreeNode(rhs){}

    ~BlobtreeBasket(){
        Clear();
    }

    ///TODO:@todo : their is no meanging in cloning the Basket before an evaluation ...
    virtual std::shared_ptr<ScalarField> CloneForEval() const
    {
        return std::make_shared<BlobtreeBasket>(*this);
    }
    
    virtual Scalar Eval(const Point& /*point*/) const{
        assert(false); // You should not call eval on the trash node
        return 0.0;
    }

    virtual void PrepareForEval(Scalar /*epsilon_bbox*/, Scalar /*accuracy_needed*/,
                                std::map<BlobtreeNode*, std::pair<core::AABBox, core::AABBox> >& /*updated_areas*/)
    {
        assert(false);
    }
    
    /////////////////////////////
    // BoundingBox computation //
    /////////////////////////////

    virtual bool IsCompact() const
    {
        assert(false);
        return false;
    }

    virtual core::AABBox GetAxisBoundingBox(Scalar /*value*/) const
    {
        assert(false);
        return core::AABBox();
        ///TODO:@todo : there is no meaning in doing this, basket exist to ignore temporary some scalar field...
        //return max_operator_.GetAxisBoundingBox(this->children_, value);
    }

    ///////////////////////////////////////////////////////////////////////////
    //              Function for the Management of the basket                //
    ///////////////////////////////////////////////////////////////////////////
    
    /** \brief Empty the trash and recursievly delete all the children.
     */
    //TODO:@todo : what happen to the node scalar field ...
    void Clear(){
        this->DeleteChildrenBlobtreeNode();
        //ClearChildren(); avec un assert dans blobtreeNode
        //children_.clear();
    }

    void DeleteBlobtreeNodeAndChildren(BlobtreeNode* bt_node_to_delete)
    {
       this->RemoveChild(bt_node_to_delete);
       bt_node_to_delete->DeleteChildrenBlobtreeNode();
       delete bt_node_to_delete;
    }
    
    ///////////////
    // Modifiers //
    ///////////////
    
    /** \brief Check if the node is usable and in a correct state. For a basket node, it is always the case.
     *        
     */
    virtual void RecursiveNodeCorrection() { }

};
    
} // Close namespace convol

} // Close namespace expressive

    
#endif // CONVOL_BLOBTREE_BASKE_H_
