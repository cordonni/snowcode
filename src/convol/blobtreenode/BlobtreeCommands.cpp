#include <convol/blobtreenode/BlobtreeCommands.h>

using namespace expressive::core;
using namespace std;

namespace expressive {

namespace convol {


SetGradientBasedIntegralSurfaceSpecialParameterCommand
::SetGradientBasedIntegralSurfaceSpecialParameterCommand(ImplicitSurface* implicit_surface, AbstractGradientBasedIntegralSurface* op, Scalar special_parameter, Scalar old_special_parameter, Command *parent) :
    Command("SetGradientBasedIntegralSurfaceSpecialParameterCommand", parent), implicit_surface_(implicit_surface), operator_(op), special_parameter_(special_parameter), old_special_parameter_(old_special_parameter == 0.0 ? op->special_parameter() : old_special_parameter)
{
}

SetGradientBasedIntegralSurfaceSpecialParameterCommand
::~SetGradientBasedIntegralSurfaceSpecialParameterCommand()
{
}

bool SetGradientBasedIntegralSurfaceSpecialParameterCommand
::execute()
{
    operator_->set_special_parameter(special_parameter_);
    implicit_surface_->NotifyUpdate();
    return true;
}

bool SetGradientBasedIntegralSurfaceSpecialParameterCommand
::undo()
{
    operator_->set_special_parameter(old_special_parameter_);
    implicit_surface_->NotifyUpdate();
    return true;
}

std::shared_ptr<Command> SetGradientBasedIntegralSurfaceSpecialParameterCommand
::getPart(double begin, double end)
{
    Scalar dif = special_parameter_ - old_special_parameter_;
    Scalar old = old_special_parameter_ + begin * dif / 100;
    Scalar cur = old_special_parameter_ + end * dif / 100;
    return std::make_shared<SetGradientBasedIntegralSurfaceSpecialParameterCommand>(implicit_surface_, operator_, cur, old, this);
}

}

}
