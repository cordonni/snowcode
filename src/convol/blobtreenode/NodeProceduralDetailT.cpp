#include <convol/blobtreenode/NodeProceduralDetailT.h>

#include <convol/factories/BlobtreeNodeFactory.h>

// List of all functor operator that can be used with a NodeProceduralDetail
#include <convol/functors/noises/ControllableGaborSurfaceNoise.h>

#define APPROX_CLOSED_FORM_GRAD

namespace  expressive {

namespace convol {


Scalar AbstractNodeProceduralDetail::Eval(const Point& point) const
{
    Scalar f_point;
    Vector grad_f_point, dir_f_point;
    std::vector<Scalar> children_field;
    children_field.resize(this->children_.size());
    this->EvalValueGradDirWithoutNoise(point, 1.0e-6, f_point, grad_f_point, dir_f_point, children_field);
    // NB : This function will save the field of each children in children_field
    ///TODO:@todo : do not like this : dynamic allocation that could be avoided (but at the cost of a code harder to read)

    // Test whether a deformation of the field should be applied or not
    if(f_point > this->iso_detail_inf_ && f_point < this->iso_detail_sup_) {
        ///TODO:@todo : the limit should be automatic and depend on both field value and gradient

        Vector normal = -grad_f_point.normalized();
        dir_f_point.normalize(); ///TODO:@todo : if ||dir_f_point|| -> 0 use grad_f_point only for projection
        Vector main_dir, ortho_dir;
        Scalar basis_quality = ComputeOrthoBasis(dir_f_point, normal, main_dir, ortho_dir);
        basis_quality *= basis_quality; //value in [0;1]

        Scalar l_dist_lim, l_tilt_angle, l_rotate_angle;
        bool is_inside = f_point > this->iso_value_;
        ProjectionParameterInterpolation(children_field, is_inside, f_point,
                                         l_dist_lim, l_tilt_angle, l_rotate_angle);
        ///TODO:@todo : this information will be required earlier if precise limit for deformation
        /// is to be used...

        Vector local_dir_projection = DirProjection(normal, main_dir, ortho_dir,
                                                    l_tilt_angle, l_rotate_angle,
                                                    basis_quality);


        Point projected_point;
        if( !Projection(point, f_point, grad_f_point,
                        local_dir_projection, l_dist_lim,
                        projected_point))   {
            return f_point;
        }

        /// Compute local height of details
        Scalar f_proj;
        Vector grad_f_proj;
        Scalar d_limit_min, d_limit_max;
        Scalar deformation_amplitude;
        this->ComputeDeformationAmplitude(projected_point, local_dir_projection,
                                          f_proj, grad_f_proj,
                                          deformation_amplitude, d_limit_min, d_limit_max);
        ///TODO:@todo : one of the two limit should be blended/replaced with the limit computed at computation point
        /// and the deformation_amplitude should be updated if required...
        if(is_inside) {
            // blend d_limit_min
        } else {
            // blend d_limit_max
        }

        Scalar s_dist_to_proj = (projected_point-point).dot(local_dir_projection);

        return EvalWarpedField(f_point, grad_f_point,
                               f_proj, grad_f_proj,
                               s_dist_to_proj, l_tilt_angle,
                               deformation_amplitude, d_limit_min, d_limit_max);
    } else {
        return f_point;
    }
    /*
                // Smoothing near iso_detail by reducing the amplitude
                Scalar norm_iso;
                if(f_point > this->iso_value_)
                    norm_iso = 1.0 - (this->iso_detail_sup_-f_point)/(this->iso_detail_sup_-this->iso_value_);
                else
                    norm_iso = 1.0 - (f_point-this->iso_detail_inf_)/(this->iso_value_-this->iso_detail_inf_);
                if(norm_iso>0.75) {
                    norm_iso -= 0.75; norm_iso *= 4.0;
                    detail_field *= (2.0*norm_iso-3.0)*norm_iso*norm_iso+1.0;
                }
    */
}

Scalar AbstractNodeProceduralDetail::ComputeOrthoBasis(const Vector& dir, const Vector& normal,
                                                       Vector& main_dir, Vector& ortho_dir)
const
{
    ortho_dir = normal.cross(dir);
    Scalar is_ortho_dir_normal = ortho_dir.norm();
    ortho_dir /= is_ortho_dir_normal;

    main_dir = ortho_dir.cross(normal);

    return is_ortho_dir_normal;
}


void AbstractNodeProceduralDetail::ProjectionParameterInterpolation(typename std::vector<Scalar>& vec_field_value,
                                                                    bool is_inside, Scalar field,
                                                                    Scalar& dist_lim, Scalar& tilt_angle, Scalar& rotate_angle)
const
{
    dist_lim = 0.0; // maximal distance to iso-surface for field deformation
    tilt_angle = 0.0;
    rotate_angle = 0.0;

    auto iter_child = this->children_.begin();
    for(Scalar field_value : vec_field_value) {
        auto node_detail_param = (std::static_pointer_cast<AbstractNodeProceduralDetailParameter>(*iter_child));
        tilt_angle +=  field_value * node_detail_param->tilt_angle();
        rotate_angle += field_value * node_detail_param->rotate_angle();

        if(is_inside)
            dist_lim += field_value * node_detail_param->interior_dist_limit();
        else
            dist_lim += field_value * node_detail_param->exterior_dist_limit();

        ++iter_child;
    }

    dist_lim /= field; // // div by \sum_{i} children_field[i]
    tilt_angle /= field;
    rotate_angle /= field;
}

Vector AbstractNodeProceduralDetail::DirProjection(const Vector& normal, const Vector& tangent, const Vector& ortho,
                                                   Scalar tilt_angle, Scalar rotate_angle,
                                                   Scalar basis_quality) const
{
    // Correction of tilt_angle_ according to base quality !!!
    Scalar correction = exp(-0.001/basis_quality); ///TODO:@todo : should think about it : it is unstable numeric point of view!!!!
    Scalar corrected_tilt_angle = correction * tilt_angle;

    const Scalar sin_tilt = sin(corrected_tilt_angle);
    Vector dir_projection =   cos(corrected_tilt_angle)    * normal
                            + (sin_tilt*cos(rotate_angle)) * tangent
                            + (sin_tilt*sin(rotate_angle)) * ortho;

    return -dir_projection;
}

void AbstractNodeProceduralDetail::ComputeDeformationAmplitude(const Point &p_proj, const Vector &local_dir_projection,
                                                               Scalar& f_proj, Vector& grad_f_proj,
                                                               Scalar& deformation_amplitude, Scalar& d_limit_min, Scalar& d_limit_max) const
{
    Vector dir_f_proj;
    std::vector<Scalar> children_field;
    children_field.resize(this->children_.size());
    this->EvalValueGradDirWithoutNoise(p_proj, 1.0e-6,
                                       f_proj, grad_f_proj, dir_f_proj,
                                       children_field);

    ///TODO:@todo :  Si dir_f_point -> 0 aie !!! on approche d'une singularité du champ

    Vector normal_p_proj = -grad_f_proj.normalized();
    dir_f_proj.normalize();
    Vector main_dir_p_proj, ortho_dir_p_proj;
    Scalar projected_basis_quality = ComputeOrthoBasis(dir_f_proj, normal_p_proj,
                                                       main_dir_p_proj, ortho_dir_p_proj);

    UNUSED(projected_basis_quality); //TODO:@todo : this should be used i think : check why it is not ...

    EvalDeformationAmplitude(p_proj,
                             normal_p_proj, main_dir_p_proj, ortho_dir_p_proj,
                             children_field, f_proj,
                             deformation_amplitude, d_limit_min, d_limit_max);


    Scalar ps_view_surface_quality = local_dir_projection.dot(normal_p_proj);
    // when this value tend toward zero, there is a discontinuity in the projection
    // in the neighboroud of the computation point (ie. projection onto contour)

    UNUSED(ps_view_surface_quality); ///TODO:@todo : this variable should be used in a near futur
//    if( ps_view_surface_quality < epsilon)

}


void AbstractNodeProceduralDetail::ComputeDeformationAmplitudeAndGrad(const Point &p_proj, const Vector &local_dir_projection,
                                                                      Scalar& f_proj, Vector& grad_f_proj,
                                                                      Scalar& deformation_amplitude, Vector& grad_deform_amplitude,
                                                                      Scalar& d_limit_min, Scalar& d_limit_max) const
{
    Vector dir_f_proj;
    std::vector<Scalar> children_field;
    children_field.resize(this->children_.size());
    this->EvalValueGradDirWithoutNoise(p_proj, 1.0e-6,
                                       f_proj, grad_f_proj, dir_f_proj,
                                       children_field);

    ///TODO:@todo :  Si dir_f_point -> 0 aie !!! on approche d'une singularité du champ

    Vector normal_p_proj = -grad_f_proj.normalized();
    dir_f_proj.normalize();
    Vector main_dir_p_proj, ortho_dir_p_proj;
    Scalar projected_basis_quality = ComputeOrthoBasis(dir_f_proj, normal_p_proj,
                                                       main_dir_p_proj, ortho_dir_p_proj);

    UNUSED(projected_basis_quality); //TODO:@todo : this should be used i think : check why it is not ...

    EvalDeformationAmplitudeAndGrad(p_proj,
                                    normal_p_proj, main_dir_p_proj, ortho_dir_p_proj,
                                    local_dir_projection,
                                    children_field, f_proj,
                                    deformation_amplitude, grad_deform_amplitude,
                                    d_limit_min, d_limit_max);


    Scalar ps_view_surface_quality = local_dir_projection.dot(normal_p_proj);
    // when this value tend toward zero, there is a discontinuity in the projection
    // in the neighboroud of the computation point (ie. projection onto contour)

    UNUSED(ps_view_surface_quality); ///TODO:@todo : this variable should be used in a near futur
//    if( ps_view_surface_quality < epsilon)
}




Scalar AbstractNodeProceduralDetail::EvalWarpedField(Scalar f_point, const Vector& /*grad_f_point*/,
                                                     Scalar /*f_proj*/, const Vector& grad_f_proj,
                                                     Scalar dist_to_iso, Scalar tilt_angle,
                                                     Scalar d, Scalar l_min, Scalar l_max) const
{
    //TODO:@todo : pour l'article si on pouvait demontrer que dans le cas de noyaux inverse, c'est toujour mieux
    // que l'approx DL1, se serait interessant... (pour prevenir les critiques, ils faudrait aussi montrer que
    // sa marche avec DL1 mais déplacement maximal plus faible (beaucoup plus faible pour inverse...))
    //TODO:@todo : faire appel a la courbure au niveau de l'iso permettrait d'optimiser la parametre supplémentaire
    // necessaire pour que le cas du noyau compact approxime tout a fait les zones de mélange entre primitive orthogonal
    // et peut etre même angle quelcquonque => cela permettrait d'obtenir une justification plus claire pour l'article...
    //NB : f_proj should be equal to iso ... so should not be required as parameter
    // Calcul de la Hessienne => code maple et automatisation de la simplification ?

    Scalar grad_norm = grad_f_proj.norm();

//    Scalar w = Weight(dist_to_iso, d, l_min, l_max);
//    return (1.0 - w) * (f_point - LocalApproxInverse(dist_to_iso, grad_norm, 3.0))
//            + LocalApproxInverse(dist_to_iso - w * d, grad_norm, 3.0);

    Scalar cos_t_a = cos(tilt_angle);
    Scalar dist_c = cos_t_a * dist_to_iso;
    Scalar d_c = cos_t_a * d;
    Scalar w = Weight(dist_to_iso,d,l_min,l_max);
/////TODO:@todo : should find a way to change the approx function used depending on a new kernel attribute
///// the easiest way is probably to rely on lambda function
//    return  (1.0 - w) * (f_point - LocalApproxInverse(dist_c, grad_norm, 3.0))
//            + LocalApproxInverse(dist_c - w * d_c, grad_norm, 3.0); //2.0, 4.0
    return  (1.0 - w) * (f_point - LocalApproxCompactPolynomial(dist_c, grad_norm, 2.0, 4.0))
            + LocalApproxCompactPolynomial(dist_c - w * d_c, grad_norm, 2.0, 4.0);
}

//TODO:@todo : will need some additional derivative as parameter
void AbstractNodeProceduralDetail::EvalWarpedFieldAndGrad(
        const Point& point,
        Scalar f_point, const Vector& grad_f_point,
        Scalar /*f_proj*/, const Vector& grad_f_proj,
        Scalar s_dist_to_proj, const Vector& grad_s_dist_to_proj,
        Scalar tilt_angle,
        Scalar displacement, const Vector& grad_displacement,
        Scalar l_min, Scalar l_max,
        Scalar& res, Vector& grad_res) const
{
    Scalar grad_norm = grad_f_proj.norm();

    Scalar cos_t_a = cos(tilt_angle);
    Scalar dist_c = cos_t_a * s_dist_to_proj;
    Scalar d_c = cos_t_a * displacement;

    Scalar w, dx_w, dy_w;
    WeightValAndDeriv(s_dist_to_proj, displacement, l_min,l_max,
                      w, dx_w, dy_w);

    Scalar LA_point, dx_LA_point;
    LocalApproxCompactPolynomial(dist_c, grad_norm, 2.0, 4.0,
                                 LA_point, dx_LA_point);

    Scalar LA_warp_point, dx_LA_warp_point;
    LocalApproxCompactPolynomial(dist_c - w * d_c, grad_norm, 2.0, 4.0,
                                 LA_warp_point, dx_LA_warp_point);

    Scalar error_approx_LA_point = f_point - LA_point;
    res =  (1.0 - w) * error_approx_LA_point + LA_warp_point;

#ifdef APPROX_CLOSED_FORM_GRAD
    UNUSED(point);
    Vector grad_w = dx_w * grad_s_dist_to_proj
                    + dy_w * grad_displacement;
                   /* + dz_w * grad_l_min + dv_w * grad_l_max */

    grad_res = (1.0-w) * (grad_f_point - cos_t_a * dx_LA_point * grad_s_dist_to_proj /* + dy_LA_point * grad__grad_f_proj */)
            - grad_w * error_approx_LA_point
            + (cos_t_a * dx_LA_warp_point) * (grad_s_dist_to_proj - w * grad_displacement - grad_w * displacement);
         /* + dy_LA_warp_point * grad__grad_f_proj */

//    // Comparison to numerical gradient for testing
//    Vector grad_res_2 = GradientEvaluation::NumericalGradient6points(*this, point, 1.0e-6/*epsilon_grad*/);
//    if(std::fabs((grad_res.normalized()).dot(grad_res_2.normalized())) < 0.9)
//        std::cout << "angle " << (grad_res.normalized()).dot(grad_res_2.normalized())  << std::endl;
//    if((grad_res -grad_res_2).norm()/grad_res_2.norm() > 0.1)
//            std::cout << (grad_res -grad_res_2).norm()/grad_res_2.norm()<< " gradNum " << grad_res_2.norm() << " gradApprox  " << grad_res.norm() << std::endl;


#else
    grad_res = GradientEvaluation::NumericalGradient6points(*this, point, 1.0e-6/*epsilon_grad*/);
#endif
}
Scalar AbstractNodeProceduralDetail::Weight(Scalar x, Scalar d, Scalar l_min, Scalar l_max) const
{
    if(x < d) {
        if(x>l_min) {
            Scalar ratio = (x-d)/(l_min-d);
            Scalar val = 1.0 - ratio*ratio;
            return val*val*val;
        } else {
            return 0.0;
        }
    } else {
        if(x<l_max) {
            Scalar ratio = (x-d)/(l_max-d);
            Scalar val = 1.0 - ratio*ratio;
            return val*val*val;
        } else {
            return 0.0;
        }
    }
}
void AbstractNodeProceduralDetail::WeightValAndDeriv(Scalar x, Scalar d, Scalar l_min, Scalar l_max,
                                                     Scalar& res, Scalar& res_dx1, Scalar& res_dx2) const
{
    if(x < d) {
        if(x>l_min) {
            Scalar ratio = (x-d)/(l_min-d);
            Scalar val = 1.0 - ratio*ratio;
            Scalar val_sqr = val*val;
            res = val*val_sqr;
            res_dx1 = (-6.0/(l_min-d)) * ratio * val_sqr;
            res_dx2 = 6.0 * ((l_min-x)/(l_min-d)) * ratio * val_sqr;
        } else {
            res = 0.0;
            res_dx1 = 0.0;
            res_dx2 = 0.0;
        }
    } else {
        if(x<l_max) {
            Scalar ratio = (x-d)/(l_max-d);
            Scalar val = 1.0 - ratio*ratio;
            Scalar val_sqr = val*val;
            res = val*val_sqr;
            res_dx1 = (-6.0/(l_max-d)) * ratio * val_sqr;
            res_dx2 = 6.0 * ((l_max-x)/(l_max-d)) * ratio * val_sqr;
        } else {
            res = 0.0;
            res_dx1 = 0.0;
            res_dx2 = 0.0;
        }
    }
}




bool AbstractNodeProceduralDetail::Projection(const Point& init_point,
                                              Scalar f_init_point, const Vector& grad_f_init,
                                              Vector& dir_projection, Scalar dist_lim, Vector& res_point)
const
{
    // dir_projection.dot(grad_fx) should always be >= 0
    Scalar x = 0.0;
    Scalar x_e = dist_lim; // this is a signed value, < 0 if init_point inside the iso-volume

    Scalar fx = f_init_point;
    Vector grad_fx = grad_f_init;

    Scalar error = this->iso_value_ - fx;
    Scalar ps_dir_grad = dir_projection.dot(grad_fx);

    // first step to detect overshoot and point too far away
    Scalar newton_step = error / ps_dir_grad;

    Scalar new_x, new_fx, new_error, new_ps_dir_grad;
    Vector new_pos, new_grad_fx;

int plop = 0;
    int k = 0;  // Pour ne pas boucler indéfiniment
    while(error*error > 1.0e-13)
    {
        newton_step = error / ps_dir_grad;

        if( (error>0.0 && newton_step > x_e-x) || (error<0.0 && newton_step < x_e-x)) {
            newton_step = 0.5*(x_e-x);
            ++plop;

            new_pos = init_point + x_e * dir_projection;
            this->EvalValueAndGradWithoutNoise(new_pos, 1.0e-6, new_fx, new_grad_fx);
            new_error = this->iso_value_ - new_fx;
            new_ps_dir_grad = dir_projection.dot(new_grad_fx);

            if(new_error*error>0.0) {
                if(new_ps_dir_grad<0.0) {
////////////////////////////////////////////////////////////////////////////////////////////
                    // simple (poor) heuristic to be changed
                    Scalar step_size = 0.5*(x_e-x);
                    while(true) {
//                            ++k;

                        Scalar tmp_x = x + step_size;
                        new_pos = init_point + tmp_x * dir_projection;
                        this->EvalValueAndGradWithoutNoise(new_pos, 1.0e-6, new_fx, new_grad_fx);
                        new_error = this->iso_value_ - new_fx;
                        new_ps_dir_grad = dir_projection.dot(new_grad_fx);
                        if(error*new_error<0.0) {
                            x_e = tmp_x;
                            newton_step = 0.5*(x_e-x);
                            break;
                        } else if(new_ps_dir_grad>0.0) {
                            //no change of signe but "acceptable gradient"
                            if(     (new_error>0.0 && new_error>new_ps_dir_grad*step_size )
                                    || (new_error<0.0 && new_error<new_ps_dir_grad*step_size) ) {
                                // if the gradient is too small in comparison to the error, we consider that we are around a local maxima
//                                    std::cout<< "Consider we have found a local extrema... stop" << std::endl;
                                return false;
                            }
                            x = tmp_x;
                            newton_step = 0.5*(x_e-x);
                            break;
                        }

                        step_size *= 0.5;
                    }
////////////////////////////////////////////////////////////////////////////////////////////
                } else {
                    return false;
                }
            } else {
                if(new_ps_dir_grad>=0.0) {
                    if(newton_step/(x-x_e)<0.5) {
//                        if(fabs(newton_step)<fabs(half_l)) {
                        Scalar tmp = x_e;
                        x_e = x;
                        x = tmp;
                        newton_step = new_error / new_ps_dir_grad;
                    }
                }
            }
        }

        new_x = x + newton_step;
        new_pos = init_point + new_x * dir_projection; // if previous test correct, this should be in the range...
        this->EvalValueAndGradWithoutNoise(new_pos, 1.0e-6, new_fx, new_grad_fx);

        // Analysis of the result
        new_error = this->iso_value_ - new_fx; // we could check for a change of sign (which mean the iso as been crossed)
        new_ps_dir_grad = dir_projection.dot(new_grad_fx);
        if(error*new_error<=0.0) {
            // an iso-value as been crossed
            if(new_ps_dir_grad < 0.0) {
                // we know that we have crossed an iso-value of interest
                // BUT if newton step is done after that it will converge to the wrong point
////////////////////////////////////////////////////////////////////////////////////////////
                x_e = new_x;
                // simple (poor) heuristic to be changed
                Scalar step_size = 0.5*(x_e-x);
                while(true) {
//                        ++k;

                    Scalar tmp_x = x + step_size;
                    new_pos = init_point + tmp_x * dir_projection;
                    this->EvalValueAndGradWithoutNoise(new_pos, 1.0e-6, new_fx, new_grad_fx);
                    new_error = this->iso_value_ - new_fx;
                    new_ps_dir_grad = dir_projection.dot(new_grad_fx);

                    if(new_ps_dir_grad>0.0) {
                        if(error*new_error<=0.0) {
                            x_e = x;
                            x = tmp_x;
                            break;
                        } else {
                            x = tmp_x;
                            x_e = x + 2.0 * step_size;
                            break;
                        }
                    } else {
                        step_size *= 0.5;
                    }
                }
////////////////////////////////////////////////////////////////////////////////////////////
            } else {
                //TODO:@todo : with inverse kernel this can be really slow
                x_e = x;
                x = new_x;
            }
        } else {
            if(new_ps_dir_grad < 0.0) {
                // a local extrema have been crossed BUT we do not know whether it imply a change of iso value
                // if newton step is done after that it will converge to the wrong point
                // IDEA : rely on some cubic interpolation to find as fast as possible it we have to continue the search
                // ie localize the extrema and check its value...

////////////////////////////////////////////////////////////////////////////////////////////
                x_e = new_x;
                // simple (poor) heuristic to be changed
                Scalar step_size = 0.5*(x_e-x);
                while(true) {
//                        ++k;

                    Scalar tmp_x = x + step_size;
                    new_pos = init_point + tmp_x * dir_projection;
                    this->EvalValueAndGradWithoutNoise(new_pos, 1.0e-6, new_fx, new_grad_fx);
                    new_error = this->iso_value_ - new_fx;
                    new_ps_dir_grad = dir_projection.dot(new_grad_fx);
                    if(error*new_error<0.0) {
                        x_e = tmp_x;
                        new_error = error; // to prevent change later
                        new_ps_dir_grad = error / 0.5*(x_e-x); // this is equivalent to performing a dichotomy step in the next loop turn
                        break;
                    } else if(new_ps_dir_grad>0.0) {
                        //no change of signe but "acceptable gradient"
                        if(     (new_error>0.0 && new_error>new_ps_dir_grad*step_size )
                                || (new_error<0.0 && new_error<new_ps_dir_grad*step_size) ) {
                            // if the gradient is too small in comparison to the error, we consider that we are around a local maxima
                            //                                    std::cout<< "Consider we have found a local extrema... stop" << std::endl;
                            std::cout<< "failure" << error << std::endl;
                            return false;
                        }
                        x = tmp_x;
                        break;
                    }

                    step_size *= 0.5;
                }
////////////////////////////////////////////////////////////////////////////////////////////
            } else {
                //TODO:@todo : with inverse kernel this can be really slow
                x = new_x;
            }
        }
        ps_dir_grad = new_ps_dir_grad;
        error = new_error;

        if(k>80) {
            if(error*error<1.0e-13)
                break;
            else {
                std::cout<< "too much iteration " << error << std::endl;
                std::cout<<"k " << k << " plop " << plop << std::endl;

//                    std::cout<<"newtonstep " << error / ps_dir_grad <<std::endl;

//                    std::cout<< f_init_point << std::endl;
//                    std::cout<< iso_value_  - this->EvalWithoutNoise(init_point + x * dir_projection)<< " "
//                             << iso_value_  - this->EvalWithoutNoise(init_point + x_e * dir_projection) << std::endl;
//                    std::cout<< iso_value_ -  this->EvalWithoutNoise(init_point + (x+error / ps_dir_grad) * dir_projection) << std::endl;

//                    new_pos = init_point + dist_lim * dir_projection;
//                    this->EvalValueAndGradWithoutNoise(new_pos, 1.0e-6, new_fx, new_grad_fx);
//                    new_error = this->iso_value_ - new_fx;
//                    new_ps_dir_grad = dir_projection.dot(new_grad_fx);

//                    std::cout<< "xe_init" << iso_value_ -  new_fx << std::endl;
//                    if(new_ps_dir_grad<0.0) std::cout << "ARGHHH" << std::endl;

//                    new_error = this->iso_value_ - f_init_point;
//                    new_ps_dir_grad = dir_projection.dot(grad_f_init);
//                    std::cout<<new_error/new_ps_dir_grad<<std::endl;

//                    std::cout<<x << " " << x_e << " " << dist_lim<<std::endl;
//                    std::cout<< x-x_e<<std::endl;
                return false;
            }
        }
        else
            ++k;
    }

//    std::cout<<"k " << k << " plop " << plop << std::endl;
    res_point = init_point + x * dir_projection;
    return true;

/*

    //if(fabs(newton_step)>fabs(0.5*dist_lim))
    if( (error>0.0 && newton_step>0.5*dist_lim)
        || (error<0.0 && newton_step<0.5*dist_lim) )
    {
//            if(newton_step>0.5*dist_lim) {
            new_x = 0.5*dist_lim;
            new_pos = init_point + (0.5*dist_lim) * dir_projection;
            this->EvalValueAndGradWithoutNoise(new_pos, 1.0e-6, new_fx, new_grad_fx);
            new_error = this->iso_value_ - new_fx;

if(dir_projection.dot(new_grad_fx)<0.0) {std::cout<<"argh of the zero type" << std::endl;}
            if(new_error*error>0.0) {
                // no iso-value detected
///TODO:@todo : this part should receive a better treatment...
///TODO:@todo : we should also check sign of ps_dir_grad to check that we do not have crossed a local maxima
                new_pos = init_point + dist_lim * dir_projection;
                this->EvalValueAndGradWithoutNoise(new_pos, 1.0e-6, new_fx, new_grad_fx);
                new_error = this->iso_value_ - new_fx;
if(dir_projection.dot(new_grad_fx)<0.0) {std::cout<<"argh of the zero type" << std::endl;}

                if(new_error*error>0.0) {
                    return false;
                } else {
                    x_e = 0.5*dist_lim;
                    x = dist_lim;

                    error = new_error;
                    ps_dir_grad = dir_projection.dot(new_grad_fx);
                }
            } else {
                // an iso-value was crossed
                new_ps_dir_grad = dir_projection.dot(new_grad_fx);
                if(new_ps_dir_grad<0.0) {
                    std::cout<<"argh of the first type" << std::endl;
                    //TODO:@todo : probably rely on some cubic interpolation to find a new interval
                    return false;
                } else {
                    x_e = 0.0;
                    x = 0.5*dist_lim;

                    error = new_error;
                    ps_dir_grad = new_ps_dir_grad;
                }
            }
//            }
    }

    int k = 0;  // Pour ne pas boucler indéfiniment
    while(error*error > 1.0e-13)
    {
        newton_step = error / ps_dir_grad;

        if( (error>0.0 && x+newton_step > x_e) || (error<0.0 && x+newton_step < x_e)) {
            newton_step = 0.5*(x_e-x);
        }

        new_x = x + newton_step;
        new_pos = init_point + new_x * dir_projection; // if previous test correct, this should be in the range...
        this->EvalValueAndGradWithoutNoise(new_pos, 1.0e-6, new_fx, new_grad_fx);

        // Analysis of the result
        new_error = this->iso_value_ - new_fx; // we could check for a change of sign (which mean the iso as been crossed)
        new_ps_dir_grad = dir_projection.dot(new_grad_fx);
        if(error*new_error<0.0) {
            // the iso-value as been crossed
            if(new_ps_dir_grad < 0.0) {
                return false;
            } else {
                x_e = x;
                x = new_x;
            }
        } else {
            if(new_ps_dir_grad < 0.0) {
                return false;
            } else {
                x = new_x;
            }
        }
        ps_dir_grad = new_ps_dir_grad;
        error = new_error;

        if(k>20) {
            return false; //break;
        }
        else
            ++k;
    }
    res_point = init_point + x * dir_projection;
    return true;
*/

//// Newton method
//    int k = 0;  // Pour ne pas boucler indéfiniment
//    while(error*error > 1.0e-13)
//    {
//        x += error / ps_dir_grad;
//        Vector new_pos = init_point + x * dir_projection;
//        Scalar new_fx; Vector new_grad_fx;
//        this->EvalValueAndGradWithoutNoise(new_pos, 1.0e-6, new_fx, new_grad_fx);

//        error = this->iso_value_ - new_fx; // we could check for a change of sign (which mean the iso as been crossed)
//        ps_dir_grad = dir_projection.dot(new_grad_fx);

//        if(k>20) {
//            std::cout<< "too much iteration" << std::endl;
//            return false; //break;
//        } else
//            ++k;
//    }
//    res_point = init_point + x * dir_projection;
//    return true;
}


void AbstractNodeProceduralDetail::EvalValueAndGrad(const Point& point,
                                                    Scalar /*epsilon_grad*/, Scalar& value_res, Vector& grad_res) const
{
    Scalar f_point;
    Vector grad_f_point, dir_f_point;
    std::vector<Scalar> children_field;
    children_field.resize(this->children_.size());
    this->EvalValueGradDirWithoutNoise(point, 1.0e-6, f_point, grad_f_point, dir_f_point, children_field);
    // NB : This function will save the field of each children in children_field
    ///TODO:@todo : do not like this : dynamic allocation that could be avoided (but at the cost of a code harder to read)

    // Test whether a deformation of the field should be applied or not
    if(f_point > this->iso_detail_inf_ && f_point < this->iso_detail_sup_) {
        ///TODO:@todo : the limit should be automatic and depend on both field value and gradient

        Vector normal = -grad_f_point.normalized();
        dir_f_point.normalize(); ///TODO:@todo : if ||dir_f_point|| -> 0 use grad_f_point only for projection
        Vector main_dir, ortho_dir;
        Scalar basis_quality = ComputeOrthoBasis(dir_f_point, normal, main_dir, ortho_dir);
        basis_quality *= basis_quality; //value in [0;1]

        Scalar l_dist_lim, l_tilt_angle, l_rotate_angle;
        bool is_inside = f_point > this->iso_value_;
        ProjectionParameterInterpolation(children_field, is_inside, f_point,
                                         l_dist_lim, l_tilt_angle, l_rotate_angle);
        ///TODO:@todo : this information will be required earlier if precise limit for deformation
        /// is to be used...

        Vector local_dir_projection = DirProjection(normal, main_dir, ortho_dir,
                                                    l_tilt_angle, l_rotate_angle,
                                                    basis_quality);

        Point projected_point;
        if( Projection(point, f_point, grad_f_point,
                        local_dir_projection, l_dist_lim,
                        projected_point))   {
            /// Compute local height of details
            Scalar f_proj;
            Vector grad_f_proj;
            Scalar d_limit_min, d_limit_max;
            Scalar deformation_amplitude;
            Vector grad_deform_amplitude;
    ///TODO:@todo : commencer par un gradient numerique...
            this->ComputeDeformationAmplitudeAndGrad(projected_point, local_dir_projection,
                                                     f_proj, grad_f_proj,
                                                     deformation_amplitude, grad_deform_amplitude,
                                                     d_limit_min, d_limit_max);
            ///TODO:@todo : one of the two limit should be blended/replaced with the limit computed at computation point
            /// and the deformation_amplitude should be updated if required...
            if(is_inside) {
                // blend d_limit_min
            } else {
                // blend d_limit_max
            }

//            Vector grad_res_2 = BruteForceGradientDeformationAmplitude(point,1.0e-6);
////            std::cout << grad_res_2.normalized().dot(grad_f_proj.normalized()) << std::endl;

//            if(std::fabs((grad_deform_amplitude.normalized()).dot(grad_res_2.normalized())) < 0.9)
//                std::cout << "angle " << (grad_deform_amplitude.normalized()).dot(grad_res_2.normalized())  << std::endl;
//            else if((grad_deform_amplitude -grad_res_2).norm()/grad_res_2.norm() > 0.1) {
//                std::cout << (grad_deform_amplitude -grad_res_2).norm()/grad_res_2.norm()<< " gradNum " << grad_res_2.norm() << " gradApprox  " << grad_deform_amplitude.norm() << std::endl;
//            } else {
//                std::cout << "acceptable" << std::endl;
//            }

            Scalar s_dist_to_proj = (projected_point-point).dot(local_dir_projection);
            //TODO:@todo : following is an approximation : check its quality
            Vector normal_proj = -grad_f_proj.normalized(); //this value is computed three times
            Scalar fct_view_angle = 1.0/local_dir_projection.dot(normal_proj); //this value is also computed in ComputeDeformationAmplitude
            Vector grad_s_dist_to_proj = - fct_view_angle * normal_proj; // since both formula are renormalized by u.dot(n) we could just use directly grad_f_point instead of normal_proj

            EvalWarpedFieldAndGrad(point,
                                   f_point, grad_f_point,
                                   f_proj, grad_f_proj,
                                   s_dist_to_proj, grad_s_dist_to_proj,
                                   l_tilt_angle,
                                   deformation_amplitude, grad_deform_amplitude,
                                   d_limit_min, d_limit_max,
                                   value_res, grad_res);
        } else {
            value_res = f_point;
            grad_res = grad_f_point;
        }

    } else {
        value_res = f_point;
        grad_res = grad_f_point;
    }
}


//TODO:@todo : repetition of code for computing properties is a nightmare ...
// this code is copied (with slight differences) in numerous classes => big problem
void AbstractNodeProceduralDetail::EvalValueAndGradAndProperties(const Point& point, Scalar epsilon_grad,
                                           const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
                                           const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
                                           Scalar& value_res, Vector& grad_res,
                                           std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                           std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
                                           ) const
{
    EvalValueAndGrad(point,epsilon_grad, value_res, grad_res);

///TODO:@todo : this could have been computed more efficiently if done in the first EvalWithoutNoise of EvalValueAndGrad !
    EvalProperties(point, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
}



const typename std::vector<ESFVectorProperties> AbstractNodeProceduralDetail::vect_prop_ids_dir_
= AbstractNodeProceduralDetail::MakeEFSVectorPropertiesDir();

const typename std::vector<ESFScalarProperties> AbstractNodeProceduralDetail::scal_prop_ids_empty_
= AbstractNodeProceduralDetail::MakeEFSScalarPropertiesEmpty();

///TODO:@todo : there should be a way to not code this function if property system was better...
/// due to saving individual field properties, it is a little more tricky ... BUT should still be possible
void AbstractNodeProceduralDetail::EvalValueGradDirWithoutNoise(const Point& point, Scalar epsilon_grad,
                                                                Scalar& value_res, Vector& grad_res, Vector& dir_res,
                                                                std::vector<Scalar>& children_field) const
{
    // NB : we can't juste use blend_operator_.EvalValueAndGradAndProperties(...) because we want to save the field value of each child...
    value_res = 0.0;
    grad_res = Vector::Zero();
    vect_prop_res_dir_[0] = Vector::Zero();

    Scalar value_aux = 0.0;
    Vector grad_aux = Vector::Zero();
    auto it_field = children_field.begin();
    for (auto &child : this->children_) {
        if(child->axis_bounding_box().Contains(point)) {
            child->EvalValueAndGradAndProperties(point, epsilon_grad,
                                                 scal_prop_ids_empty_,
                                                 vect_prop_ids_dir_,
                                                 value_aux, grad_aux,
                                                 scal_prop_res_empty_,
                                                 vect_prop_res_dir_aux_
                                                 );

            value_res += value_aux;
            grad_res += grad_aux;
            vect_prop_res_dir_[0] += value_aux * vect_prop_res_dir_aux_[0];

            // Save each child's field for further use
            (*it_field) = value_aux;
        } else {
            (*it_field) = 0.0;
        }
        ++it_field;
    }
    dir_res = (1.0/value_res) * vect_prop_res_dir_[0];
}



void AbstractNodeProceduralDetail::EvalProperties(
        const Point& point,
        const std::vector<ESFScalarProperties>& scal_prop_ids,  // list of scalar properties we want to evaluate. Should not contain a given value more than once.
        const std::vector<ESFVectorProperties>& vect_prop_ids,  // list of vector properties we want to evaluate. Should not contain a given value more than once.
        std::vector<Scalar>& scal_prop_res,  // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
        std::vector<Vector>& vect_prop_res   // result corresponding to ids in  array_scal_prop_ids. SHOULD ALREADY be resized correctly.
        ) const
{
    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
    {
        scal_prop_res[i] = 0.0;
    }
    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
    {
        vect_prop_res[i] = Vector::Zero();
    }
    Scalar value_res_tmp = 0.0;
    Vector grad_res_tmp = Vector::Zero(); UNUSED(grad_res_tmp);
    Scalar value_aux = 0.0;
    Vector grad_aux = Vector::Zero();
    std::vector<Scalar> scal_prop_aux;
    scal_prop_aux.resize(scal_prop_ids.size());
    std::vector<Vector> vect_prop_aux;
    vect_prop_aux.resize(vect_prop_ids.size());
    for(auto it = this->children_.begin(); it != this->children_.end(); it++)
    {
        (*it)->EvalValueAndGradAndProperties(point, 1.0e-6, scal_prop_ids, vect_prop_ids, value_aux, grad_aux, scal_prop_aux, vect_prop_aux);
        value_res_tmp += value_aux;
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] += value_aux*scal_prop_aux[i];
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] += value_aux*vect_prop_aux[i];
        }
    }
    if(value_res_tmp != 0.0)
    {
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = scal_prop_res[i]/value_res_tmp;
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = vect_prop_res[i]/value_res_tmp;
        }
    }
    else
    {
        for(unsigned int i = 0; i<scal_prop_ids.size(); ++i)
        {
            scal_prop_res[i] = ExpressiveTraits::kSFScalarPropertiesDefault[scal_prop_ids[i]];
        }
        for(unsigned int i = 0; i<vect_prop_ids.size(); ++i)
        {
            vect_prop_res[i] = ExpressiveTraits::kSFVectorPropertiesDefault[vect_prop_ids[i]];
        }
    }
}


// Register the name of the functor to be looked for in xml files

static BlobtreeNodeFactory::XmlFunctorField<AbstractNodeProceduralDetail> NodeProceduralDetail_XmlFunctorField;

// Register NodeProceduralDetail<FunctorNosie> in the BlobtreeNodeFactory

static BlobtreeNodeFactory::Type<  ControllableGaborSurfaceNoise, NodeProceduralDetailT<ControllableGaborSurfaceNoise> >
                                                    NodeProceduralDetailT_ControllableGaborSurfaceNoise_Type;

} // Close namespace convol

} // Close namespace expressive
