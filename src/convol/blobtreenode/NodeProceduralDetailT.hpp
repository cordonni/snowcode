
#include <core/xmlloaders/TraitsXmlQuery.h>

#include <convol/tools/ConvergenceTools.h>

namespace  expressive {

namespace convol {


template<typename TFunctorNoise>//, typename TFunctorDeformationMap>
std::shared_ptr<ScalarField> NodeProceduralDetailT<TFunctorNoise>/*,TFunctorDeformationMap>*/::CloneForEval() const
{
    return std::make_shared<NodeProceduralDetailT<TFunctorNoise>/*,TFunctorDeformationMap>*/ >(*this);
}

template<typename TFunctorNoise>//, typename TFunctorDeformationMap>
void NodeProceduralDetailT<TFunctorNoise>/*,TFunctorDeformationMap>*/::InitFromXml(const TiXmlElement *element)
{
    double iso_value;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "iso_value", &iso_value);
    set_iso_value(iso_value);

    double iso_detail_inf;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "iso_detail_inf", &iso_detail_inf);
    set_iso_detail_inf(iso_detail_inf);

    double iso_detail_sup;
    core::TraitsXmlQuery::QueryDoubleAttribute("NodeProceduralDetailT::InitFromXml",
                                               element, "iso_detail_sup", &iso_detail_sup);
    set_iso_detail_sup(iso_detail_sup);
}

///TODO:@todo : this is horrible, we should be able to remove this function by coding it somewhere else in a more logical way !
template<typename TFunctorNoise>//, typename TFunctorDeformationMap>
Point NodeProceduralDetailT<TFunctorNoise>/*,TFunctorDeformationMap>*/::GetAPointAtWithoutNoise(Scalar value, Scalar epsilon) const
{
    if(this->children_.size() != 0)
    {
        auto iter_child = (this->children()).begin();
        for(; iter_child != (this->children()).end(); ++iter_child)
        {
            Point origin = (*iter_child)->GetAPointAt(value, epsilon);

            Scalar f_p;
            Vector search_dir_unit;
            this->EvalValueAndGradWithoutNoise(origin, 0.0001,f_p, search_dir_unit); //TODO : @todo : arbitrary
            search_dir_unit.normalize();

            // Accuracy for gradient computation
            // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
            Scalar grad_epsilon = 0.0001;   // arbitrary
            if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }
            Scalar min_absc = -10.0, max_absc = 10.0; //TODO : @todo : arbitrary

            const Scalar epsilon_d = epsilon*0.1;
            const unsigned int n_max_step = 20;
            Scalar curr_point_absc = 0.0;

            Scalar curr_field_value;    // contain the field value at curr_point
            // Newton step until we overpass the surface
            // the minimum step is set to epsilon, that ensure we will cross the surface.
            Scalar grad;
            Scalar step;
            unsigned int i = 0;
            unsigned int consecutive_small_steps = 0;
            while( max_absc - min_absc > epsilon_d && i < n_max_step && consecutive_small_steps < 2)
            {
                curr_field_value = this->EvalWithoutNoise(origin + curr_point_absc*search_dir_unit) ;
                grad = (this->EvalWithoutNoise(origin + (curr_point_absc+grad_epsilon)*search_dir_unit)-curr_field_value)/grad_epsilon;
                if(grad != 0.0)
                {
                    step = (value-curr_field_value)/grad;
                    if(step > epsilon_d || step < -epsilon_d)
                    {
                        curr_point_absc += step;
                        consecutive_small_steps = 0;
                    }
                    else
                    {
                        if(step>0.0)
                        {
                            curr_point_absc += epsilon_d;
                        }
                        else
                        {
                            curr_point_absc -= epsilon_d;
                        }
                        consecutive_small_steps++;
                    }


                    // If the newton step took us above the max limit, or under the min limit
                    // we perform a kind of dichotomy step to bring back the current point into the boundaries
                    if(curr_point_absc >= max_absc || curr_point_absc <= min_absc)
                    {
                        curr_point_absc = (max_absc+min_absc)/2.0;
                        curr_field_value = this->EvalWithoutNoise(origin + curr_point_absc*search_dir_unit);
                    }
                    else
                    {
                        // Update min_absc and max_absc depending on the field value
                        Scalar new_curr_field_value = this->EvalWithoutNoise(origin + curr_point_absc*search_dir_unit);
                        if( (curr_field_value-value)*(new_curr_field_value-value) > 0.0 ) // the step did not lead us to cross the surface
                        {
                            if(step>0.0)
                            {
                                min_absc = curr_point_absc;
                            }
                            else
                            {
                                max_absc = curr_point_absc;
                            }
                        }
                        else
                        {
                            if(step>0.0)
                            {
                                max_absc = curr_point_absc;
                            }
                            else
                            {
                                min_absc = curr_point_absc;
                            }
                        }

                        curr_field_value = new_curr_field_value;
                    }
                }
                else
                {
                    // Kind of dichotomy step
                    curr_point_absc = (max_absc+min_absc)/2.0;
                    curr_field_value = this->EvalWithoutNoise(origin + curr_point_absc*search_dir_unit);
                }

                ++i;
            }


            Point conv_point = origin + curr_point_absc*search_dir_unit;

            Scalar f_conv_p = this->EvalWithoutNoise(conv_point);
            if( (f_conv_p-value)*(f_conv_p-value) < epsilon*epsilon )
            {
                return conv_point;
            }
        }
    }
    else
    {
        return Point::Zero();
    }
    return Point::Zero();
}

///TODO:@todo : this is horrible, we should be able to remove this function by coding it somewhere else in a more logical way !
template<typename TFunctorNoise>//, typename TFunctorDeformationMap>
Point NodeProceduralDetailT<TFunctorNoise>/*,TFunctorDeformationMap>*/::GetAPointAt(Scalar value, Scalar epsilon) const
{
    if(this->children_.size() != 0)
    {
        auto iter_child = (this->children()).begin();
        for(; iter_child != (this->children()).end(); ++iter_child)
        {
            Point init_point = (*iter_child)->GetAPointAt(value, epsilon);

            Vector search_dir = this->EvalGrad(init_point, 0.0001); //TODO : @todo : arbitrary
            search_dir.normalize();

            // Accuracy for gradient computation
            // Chosen depending on the segment length if epsilon is big, and to epsilon otherwise
            Scalar grad_epsilon = 0.0001;   // arbitrary
            if(grad_epsilon>epsilon) { grad_epsilon = epsilon; }
            Scalar r_min = -30.0, r_max = 30.0; //TODO : @todo : arbitrary

            Point conv_point;
            Convergence::SafeNewton1D(
                    *(static_cast<const AbstractNodeProceduralDetail*>(this)),
                    init_point,
                    search_dir,
                    r_min,  //TODO : arbitrary                                // security minimum bound
                    r_max,    //TODO : arbitrary
                    0.0,            // point at which we start the search, given in 1D
                    value,
                    epsilon*0.1,
                    grad_epsilon,
                    20, conv_point );
            Scalar f_conv_p = this->Eval(conv_point);
            if( (f_conv_p-value)*(f_conv_p-value) < epsilon*epsilon )
            {
                return conv_point;
            }
        }
    }
    else
    {
        return Point::Zero();
    }
    return Point::Zero();
}






} // Close namespace convol

} // Close namespace expressive
