
#include <convol/ImplicitSurface.h>

namespace expressive {

namespace convol {

ImplicitSurface *ImplicitSurface::CloneForEval() const
{
    return new ClonedImplicitSurface(*this);
}

} // Close namespace convol

} // Close namespace expressive
