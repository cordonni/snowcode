/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ???

   Language: C++

   License: Convol license

   \author:

   Description:

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_OBJECT_H_
#define CONVOL_OBJECT_H_

// core dependencies
#include <core/CoreRequired.h>
#include <core/utils/Serializable.h>
#include <core/geometry/BasicTriMesh.h>
#include <core/utils/ListenerT.h>
#include <core/workerthread/WorkerThread.h>

// convol dependencies
#include <convol/ImplicitSurface.h>
#include <convol/skeleton/Skeleton.h>
#include <convol/blobtreenode/BlobtreeRoot.h>
#include <convol/tools/updating/AreaUpdatingPolygonizerHandler.h>

/////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : temporary data structure waiting for the final solution,
///                 encapsule skeleton et implicit surface
/////////////////////////////////////////////////////////////////////////////////////////


namespace expressive {

namespace convol {

class CONVOL_API ConvolObject
        :   public core::Serializable,
            public core::ListenableT<ConvolObject>,
            public core::ListenerT<core::WorkerThread>,
            public core::WeightedGraphListener,
            //public core::ListenerT<BlobtreeRoot>,
            public core::ListenerT<ImplicitSurface>
{
public:
    EXPRESSIVE_MACRO_NAME("ConvolObject")

    ///////////////////////////////
    /// Constructors/destructor ///
    ///////////////////////////////
    ConvolObject(std::shared_ptr<Skeleton> skel,
                 std::shared_ptr<convol::BlobtreeRoot> bt,
                 std::shared_ptr<convol::ImplicitSurface> is);

    virtual ~ConvolObject();

    virtual void swap(std::shared_ptr<ConvolObject> &);

    /////////////////
    /// Listening ///
    /////////////////

    virtual void updated(ImplicitSurface* /*t*/);

//    virtual void updated(BlobtreeRoot* /*t*/);

    void Notify(const core::WeightedGraph::GraphChanges & /*changes*/);

    virtual void updated(core::WorkerThread* /*t*/);


    ///////////////////
    // Xml functions //
    ///////////////////

    virtual TiXmlElement * ToXml(const char *name = nullptr) const;

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_area_updating_polygonizer(std::shared_ptr<tools::AreaUpdatingPolygonizerHandler> new_polygonizer);

    /////////////////
    /// Accessors ///
    /////////////////

    const std::shared_ptr<Skeleton> skeleton() const;
    std::shared_ptr<Skeleton> nonconst_skeleton();

    const std::shared_ptr<BlobtreeRoot> blobtree_root() const;
    std::shared_ptr<BlobtreeRoot> nonconst_blobtree_root();

    const std::shared_ptr<ImplicitSurface> implicit_surface() const;
    std::shared_ptr<ImplicitSurface> nonconst_implicit_surface();

    const std::shared_ptr<tools::AreaUpdatingPolygonizerHandler> auto_updating_polygonizer() const;
    std::shared_ptr<tools::AreaUpdatingPolygonizerHandler> nonconst_auto_updating_polygonizer() const;

    std::shared_ptr<core::BasicTriMesh> tri_mesh();

    std::mutex* mutex_data_retrieval();
    std::condition_variable* cond_data_retrieval();

protected:
    ConvolObject();

    std::shared_ptr<Skeleton> skeleton_;
    std::shared_ptr<BlobtreeRoot> blobtree_root_;
    std::shared_ptr<ImplicitSurface> implicit_surface_;

    std::shared_ptr<core::BasicTriMesh> tri_mesh_; //TODO:@todo : this should be replaced by an AbstractClass (for instance when half-edge structure is required)

    std::shared_ptr<tools::AreaUpdatingPolygonizerHandler> auto_updating_polygonizer_;

    // synchronization between auto update and the data (currently the trimesh) retriever
    std::mutex              mutex_data_retrieval_;
    std::condition_variable cond_data_retrieval_;

}; // End class ConvolObject declaration


} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_OBJECT_H_
