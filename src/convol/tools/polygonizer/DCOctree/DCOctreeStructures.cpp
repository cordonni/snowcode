#include<convol/tools/polygonizer/DCOctree/DCOctreeStructures.h>

#include<core/geometry/BasicRay.h>

#include<list>
#include<vector>
#include<array>
#include<algorithm>

namespace expressive {
namespace convol {
namespace tools {

namespace DCOctreeStructures{
/*
const int HASHBIT = 5;
const int HASHSIZE = (size_t)(1<<(3*HASHBIT));
const int MASK = ((1<<HASHBIT)-1);*/

// vertices_map[i][j] = offset in jth dimension of the ith child of an octree node
const unsigned int vertices_map[8][3] = {{0,0,0},{0,0,1},{0,1,0},{0,1,1},{1,0,0},{1,0,1},{1,1,0},{1,1,1}};
// edgevmap[i][0] = first vertex index of the ith edge of a cube
// edgevmap[i][0] = second vertex index of the ith edge of a cube
const int edgevmap[12][2] = {{0,4},{1,5},{2,6},{3,7},{0,2},{1,3},{4,6},{5,7},{0,1},{2,3},{4,5},{6,7}};
// node_proc_face_mask[i][0] = first child index of the ith face of the node
// node_proc_face_mask[j][1] = second child index of the ith face of the node
// node_proc_face_mask[j][2] = direction of the ith face normal (x->0,y->1,z->2)
const int node_proc_face_mask[12][3] =
    {{0,4,0},{1,5,0},{2,6,0},{3,7,0},{0,2,1},{4,6,1},
     {1,3,1},{5,7,1},{0,1,2},{2,3,2},{4,5,2},{6,7,2}} ;
// 0<=j<4 : node_proc_edge_mask[i][j] = jth child index of the ith edge of the node
// node_proc_edge_mask[i][4] = direction of the ith edge of the node (x->0,y->1,z->2)
const int node_proc_edge_mask[6][5] = {{0,1,2,3,0},{4,5,6,7,0},{0,4,1,5,1},{2,6,3,7,1},{0,2,4,6,2},{1,3,5,7,2}} ;

const int face_proc_face_mask[3][4][2] = {
    {{4,0},{5,1},{6,2},{7,3}},
    {{2,0},{6,4},{3,1},{7,5}},
    {{1,0},{3,2},{5,4},{7,6}}
};
const int face_proc_edge_fathers_mask0[4] = { 0, 0, 1, 1 };
const int face_proc_edge_fathers_mask1[4] = { 0, 1, 0, 1 };
const int *face_proc_edge_fathers_mask[2] = { face_proc_edge_fathers_mask0, face_proc_edge_fathers_mask1} ;
const int face_proc_edge_mask[3][4][6] = {
    {{4,0,5,1,1,1},{6,2,7,3,1,1},{4,6,0,2,0,2},{5,7,1,3,0,2}},
    {{2,3,0,1,0,0},{6,7,4,5,0,0},{2,0,6,4,1,2},{3,1,7,5,1,2}},
    {{1,0,3,2,1,0},{5,4,7,6,1,0},{1,5,0,4,0,1},{3,7,2,6,0,1}}
};

const int edge_proc_edge_mask[3][2][4] = {
    {{3,2,1,0},{7,6,5,4}},
    {{5,1,4,0},{7,3,6,2}},
    {{6,4,2,0},{7,5,3,1}},
};

const int add_face_proc_edge_mask[3][4] = {{3,2,1,0},{7,5,6,4},{11,10,9,8}};
const int add_face_proc_vertices_mask[4] = {0,1,3,2};

const bool subdivide_proc_shared_vertex_mask[27] =
{ false, true , false,      true, true, true,      false, true, false,
  true,  true,  true,       true, false, true,     true, true, true,
  false, true, false,       true, true, true,      false, true, false};
const int subdivide_proc_inherited_vertex_mask[8] = {0,2,6,8,18,20,24,26};
const int subdivide_proc_childs_vertex_mask[8][8] =
{{0,1,3,4,9,10,12,13}, {1,2,4,5,10,11,13,14} ,{3,4,6,7,12,13,15,16}, {4,5,7,8,13,14,16,17},
 {9,10,12,13,18,19,21,22}, {10,11,13,14,19,20,22,23}, {12,13,15,16,21,22,24,25}, {13,14,16,17,22,23,25,26}};

// give a child cell index according to its left bottom near corner position
// idx = child_position_mask[(position[0]%length)>=length/2 ? 1 : 0]
//                          [(position[1]%length)>=length/2 ? 1 : 0]
//                          [(position[2]%length)>=length/2 ? 1 : 0]
const int child_position_mask[2][2][2] = {{{0,1},{2,3}},{{4,5},{6,7}}};

const int node_neighbour_mask[8][6][2] =
    {{ {4,1}, {2,1}, {1,1} },
     { {5,1}, {3,1}, {0,0} },
     { {6,1}, {0,0}, {3,1} },
     { {7,1}, {1,0}, {2,0} },
     { {0,0}, {6,1}, {5,1} },
     { {1,0}, {7,1}, {4,0} },
     { {2,0}, {4,0}, {7,1} },
     { {3,0}, {5,0}, {6,0} }};


int HASH( int i, int j,int k)
{
    return ( ((( (((i>>HASHBIT2)&MASK)<<HASHBIT) | ((j>>HASHBIT2)&MASK) )<<HASHBIT) | ((k>>HASHBIT2)&MASK)) );
}

unsigned char neighbour_node_idx(unsigned char node_idx, unsigned char offset_flags)
{
    node_idx ^= ((offset_flags%3)!=1 ? 1 : 0);
    offset_flags /= 3;
    node_idx ^= ((offset_flags%3)!=1 ? 2 : 0);
    offset_flags /= 3;
    node_idx ^= ((offset_flags%3)!=1 ? 4 : 0);
    return node_idx;
}

bool is_neighbour_node_outside_parent(unsigned char node_idx, unsigned char offset_flags)
{
    if ( (offset_flags % 3) == ( (node_idx & 1) ? 2 : 0) )
        return true;
    offset_flags /= 3;
    if ( (offset_flags % 3) == ( (node_idx & 2) ? 2 : 0) )
        return true;
    offset_flags /= 3;
    if ( (offset_flags % 3) == ( (node_idx & 4) ? 2 : 0) )
        return true;
    return false;
}

} // Close namespace DCOctreeStructures
} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive
