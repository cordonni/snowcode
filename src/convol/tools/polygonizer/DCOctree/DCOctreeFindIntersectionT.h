/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#pragma once
#ifndef DC_OCTREE_FIND_INTERSECTION_T_H
#define DC_OCTREE_FIND_INTERSECTION_T_H

#include <cassert>
#include <list>
#include <array>

// core dependencies
#include <core/geometry/AABBox.h>

// convol dependencies
#include <convol/tools/polygonizer/DCOctree/DCOctreeStructures.h>


namespace expressive {
namespace convol {
namespace tools {

template<typename DCOctree>
class DCOctreeRayIntersectionT
{
public:

    typedef typename DCOctree::Traits Traits;
    typedef typename DCOctree::DCOctreeNode DCOctreeNode;
    typedef typename Traits::Scalar Scalar;
    typedef typename Traits::Point Point;
    typedef typename Traits::Vector Vector;

    typedef typename DCOctreeNode::TriMesh TriMesh;
    typedef typename TriMesh::VertexHandle Vertex;
    typedef typename TriMesh::FaceHandle Face;
    typedef typename TriMesh::ConstVertexFaceIter ConstVertexFaceIter;
    typedef typename TriMesh::ConstFaceVertexIter ConstFaceVertexIter;

    typedef Convol::BasicRayT<Traits> BasicRay;
    typedef Convol::tools::DCOctreeStructures::NodePositionInfosT<DCOctree> NodePositionInfos;

    static std::pair<DCOctreeNode *, NodePositionInfos>
        FindIntersection(const BasicRay& ray, DCOctree const *octree,
                         Scalar &outTime)
    {
        DCOctreeRayIntersectionT<DCOctree> functor(octree, ray);
        std::pair<DCOctreeNode *, NodePositionInfos> ans = std::make_pair(octree->root, NodePositionInfos(octree));
        if (octree->tri_mesh_->n_vertices()>0)
            functor(&(ans.first),ans.second,outTime);
        return ans;
    }

    static std::pair<DCOctreeNode *, NodePositionInfos>
        FindIntersection(const BasicRay& ray, DCOctree const *octree,
                         Scalar &outTime, DCOctreeNode *guess_node, const NodePositionInfos &guess_position)
    {
        DCOctreeRayIntersectionT<DCOctree> functor(octree, ray);
        std::pair<DCOctreeNode *, NodePositionInfos> ans = std::make_pair(guess_node, guess_position);
        functor(ans.first,ans.second,outTime);
        return ans;
    }

private:
    class NodeChildIndexesComparator
    {
    public:
        NodeChildIndexesComparator(const BasicRay &ray)
        {
            using namespace DCOctreeStructures;
            for (unsigned char i=0; i<8; ++i)
                index_weights[i] = ((Vector(vertices_map[i][0], vertices_map[i][1], vertices_map[i][2])-ray.starting_point()) | ray.direction());
        }

        bool operator ()(unsigned char a, unsigned char b)
        {
            return index_weights[a]<index_weights[b];
        }

    private:
        std::array<Scalar, 8> index_weights;
    };

    DCOctreeRayIntersectionT(DCOctree const *octree, const BasicRay &ray) : octree_(octree), ray_(ray)
    {
        for (unsigned char i=0; i<8; ++i)
            ordered_indexes_[i]=i;

        std::sort(ordered_indexes_.begin(), ordered_indexes_.end(), NodeChildIndexesComparator(ray));
    }

    std::pair<DCOctreeNode *, NodePositionInfos> getFirstIntersectedParent(
            DCOctreeNode *node, const NodePositionInfos &infos)
    {
        DCOctreeNode *current_node = node;
        NodePositionInfos current_position = infos;
        unsigned char intersection_flags  = get_intersection_flags(current_position);

        while (intersection_flags == NO_OFFSET && current_node != NULL)
        {
            current_node = current_node->father;
            current_position = current_position.parent();
            intersection_flags = get_intersection_flags(current_position);
        }

        return std::make_pair(current_node,current_position);
    }

    /* Get the neighbour flag from the node position infos, in order to iterate
        to the next intersected node
        if ray_ does not intersect the node, the returned flag will be equals to
        NO_OFFSET
    */
    unsigned char get_intersection_flags(const NodePositionInfos &infos)
    {
        unsigned char ans = NO_OFFSET;
        const AABBox bbox = infos.getAxisBoundingBox(octree_);
        const Point &orig = ray_.starting_point();
        const Vector &dir = ray_.direction();
        const Vector &min = bbox.min_;
        const Vector &max = bbox.max_;

        static const NodeOffsetFlag cell_offset[6] = { DECREASE_X, DECREASE_Y, DECREASE_Z,
                                                     INCREASE_X, INCREASE_Y, INCREASE_Z };

        for (unsigned char i=0; i<3; ++i)
        {
            // We check only face common to a further node
            if (dir[i]>=0.) continue;

            const Scalar t = (min[i] - orig[i]) / dir[i];
            const Point hitpoint = orig + dir*t;

            const unsigned char c1=(i+1)%3;
            const unsigned char c2=(i+2)%3;

            if (hitpoint[c1]>=min[c1] && hitpoint[c1]<=max[c1]
                    && hitpoint[c2]>=min[c2] && hitpoint[c2]<=max[c2])
            {
                ans += cell_offset[i];
            }
        }
        for (unsigned char i=0; i<3; ++i)
        {
            // We check only face common to a further node
            if (dir[i]<=0.) continue;

            const Scalar t = (max[i] - orig[i]) / dir[i];
            const Point hitpoint = orig + dir*t;

            const unsigned char c1=(i+1)%3;
            const unsigned char c2=(i+2)%3;

            if (hitpoint[c1]>=min[c1] && hitpoint[c1]<=max[c1]
                    && hitpoint[c2]>=min[c2] && hitpoint[c2]<=max[c2])
            {
                ans += cell_offset[i+3];
            }
        }
        return ans;
    }

    bool operator()(DCOctreeNode **inout_node,
                    NodePositionInfos &inout_position,
                    Scalar &outTime)
    {
        using namespace DCOctreeStructures;
        DCOctreeNode *current_node;
        NodePositionInfos current_position;
        const TriMesh &mesh = *octree_->tri_mesh_;
        
#ifdef CONVOL_USE_LibCPPOx
		std::tie(current_node, current_position) = getFirstIntersectedParent(*inout_node, inout_position);
#else
		boost::tie(current_node, current_position) = getFirstIntersectedParent(*inout_node, inout_position);
#endif
        if (current_node == NULL)
            return false;
        bool node_intersection_found = true;

        while (node_intersection_found)
        {
            if (current_node->is_leaf())
            {
                if (current_node->vertex.is_valid())
                {
                    // test every mesh faces ...
                    for (ConstVertexFaceIter vf_it = mesh.cvf_begin(current_node->vertex);
                         vf_it.is_valid(); ++vf_it)
                    {
                        Point vertex1, vertex2, vertex3;
                        ConstFaceVertexIter fv_it = mesh.cfv_begin(*vf_it);
                        vertex1 = mesh.point(*fv_it); ++fv_it;
                        vertex2 = mesh.point(*fv_it); ++fv_it;
                        vertex3 = mesh.point(*fv_it);

                        // trans_ray direction should be normalized
                        Vector u = vertex2 - vertex1;
                        Vector v = vertex3 - vertex1;

                        Scalar delta = -(u % v) | ray_.direction();
                        if( delta != 0.0  )
                        {
                            Vector w = ray_.starting_point() - vertex1;
                            Scalar a = (- (w % v) | ray_.direction())/delta;
                            Scalar b = (- (u % w) | ray_.direction())/delta;
                            Scalar t = ((u % v) | w)/delta;
                            if(t>0.0 && a >=0.0 && b>=0.0 && a+b<=1)
                            {
                                outTime = t;
                                *inout_node = current_node;
                                inout_position = current_position;
                                return true;
                            }
                        }
                    }
                }
                unsigned char intersection_flags = get_intersection_flags(current_position);
                if (intersection_flags == NO_OFFSET)
                    return false;
#ifdef CONVOL_USE_LibCPPOx
				std::tie(current_node, current_position) = DCOctreeVisitorT<DCOctree>::GetNeighbour(current_node, current_position, intersection_flags);
#else
				boost::tie(current_node, current_position) = DCOctreeVisitorT<DCOctree>::GetNeighbour(current_node, current_position, intersection_flags);
#endif
				if (current_node == NULL)
                    return false;
            }
            else
            {
                node_intersection_found = false;
                for (unsigned char i=0; i<8; ++i)
                {
                    unsigned char idx = ordered_indexes_[i];
                    DCOctreeNode *node = current_node->childs[idx];
                    NodePositionInfos info = current_position.child(idx);
                    unsigned char intersection_flags = get_intersection_flags(info);
                    if (intersection_flags != NO_OFFSET)
                    {
                        current_node = node;
                        current_position = info;
                        node_intersection_found = true;
                        break;
                    }
                }
            } 
        }
        return false;
    }
private:
    DCOctree const *octree_;
    BasicRay ray_;
    std::array<unsigned char, 8> ordered_indexes_;
};

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive

#endif //DC_OCTREE_FIND_INTERSECTION_T_H
