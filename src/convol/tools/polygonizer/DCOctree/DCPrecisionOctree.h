/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: DCPrecisionOctree.h

   Language: C++

   License: Convol Licence

   \author: Amaury Jung
   E-Mail: amaury.jung@inria.fr


   Platform Dependencies: None
*/

#ifndef CONVOL_DC_PRECISION_OCTREE_H_
#define CONVOL_DC_PRECISION_OCTREE_H_

// core dependencies
#include<core/geometry/PrecisionAxisBoundingBox.h>

// Convol dependencies
#include<convol/ConvolRequired.h>
#include<convol/ImplicitSurface.h>

#include <array>

namespace expressive {
namespace convol {
namespace tools {

class DCPrecisionOctreeNode
{
public:

    typedef DCPrecisionOctreeNode const *Hint;

    DCPrecisionOctreeNode(const core::AABBox &bbox = core::AABBox()) :
        feature_precision_(std::numeric_limits<Scalar>::infinity()),
        detail_precision_(std::numeric_limits<Scalar>::infinity()),
        max_feature_precision_(std::numeric_limits<Scalar>::infinity()),
        max_detail_precision_(std::numeric_limits<Scalar>::infinity()),
        bbox_(bbox)
    {
        childs_.fill(NULL);
    }

    DCPrecisionOctreeNode(const core::AABBox &bbox,
                           const DCPrecisionOctreeNode &father) :
        feature_precision_(father.feature_precision_),
        detail_precision_(father.detail_precision_),
        max_feature_precision_(father.max_feature_precision_),
        max_detail_precision_(father.max_detail_precision_),
        bbox_(bbox)
    {
        childs_.fill(NULL);
    }

    ~DCPrecisionOctreeNode()
    {
        if (!is_leaf())
            for (unsigned int i=0; i<8; ++i)
                delete childs_[i];
    }

    void clear_precision_on_area(const core::AABBox &request_bbox)
    {
        if (request_bbox.Intersect(bbox_))
        {
            this->max_feature_precision_ = this->max_detail_precision_ =
                    std::numeric_limits<Scalar>::infinity();

            if (!this->is_leaf())
            {      
                if (request_bbox.Contains(bbox_))
                {
                    this->feature_precision_ = this->detail_precision_ =
                                        std::numeric_limits<Scalar>::infinity();
                    collapse();
                }
                else
                {
                    for (unsigned int i=0; i<8; ++i)
                    {
                        childs_[i]->clear_precision_on_area(request_bbox);
                        this->feature_precision_ = std::min(this->feature_precision_, childs_[i]->feature_precision_);
                        this->detail_precision_ = std::min(this->detail_precision_, childs_[i]->detail_precision_);
                    }
                }
            }
            else
            {
                this->feature_precision_ = this->detail_precision_ =
                                    std::numeric_limits<Scalar>::infinity();
            }
        }
    }

    void add_precision_box(const core::PrecisionAxisBoundingBox &precbbox, int height)
    { 
        if (precbbox.Intersect(bbox_))
        {
            if (precbbox.ContainsOrEquals(bbox_))
            {
                this->max_feature_precision_ = std::min(this->max_feature_precision_, precbbox.feature_precision);
                this->max_detail_precision_ = std::min(this->max_detail_precision_ ,precbbox.detail_precision);

                if (precbbox.feature_precision <= this->feature_precision_
                        && precbbox.detail_precision <= this->detail_precision_)
                {
                    this->feature_precision_ = precbbox.feature_precision;
                    this->detail_precision_ = precbbox.detail_precision;
                    collapse();
                    return;
                }
            }

            if (height > 0 && is_leaf())
            {
                const Vector this_cell_size = bbox_.max_ - bbox_.min_;
                const Scalar length = std::max(std::max(this_cell_size[0],this_cell_size[1]),this_cell_size[2]);
                if (length > precbbox.detail_precision)
                      subdivide();
            }

            this->feature_precision_ = std::min(this->feature_precision_, precbbox.feature_precision);
            this->detail_precision_  = std::min(this->detail_precision_, precbbox.detail_precision);

            if (!is_leaf()){
                const unsigned int child_height = height - 1;

                if (precbbox.feature_precision >= this->max_feature_precision_ &&
                    precbbox.detail_precision  >= this->max_feature_precision_)
                    return;

                this->max_feature_precision_ = precbbox.feature_precision;
                this->max_detail_precision_ = precbbox.detail_precision;
                for (unsigned int i=0; i<8; ++i)
                {        
                    childs_[i]->add_precision_box(precbbox, child_height);
                    this->max_feature_precision_ = std::max(this->max_feature_precision_, childs_[i]->max_feature_precision_);
                    this->max_detail_precision_ = std::max(this->max_detail_precision_, childs_[i]->max_detail_precision_);
                }
            }
        }
    }

    void GetPrecision(const core::AABBox &request,
                      Scalar &feature_precision, Scalar &detail_precision) const
    {
        if (request.Contains(bbox_) || (this->is_leaf() && request.Intersect(bbox_)))
        {
            feature_precision = std::min(feature_precision, this->feature_precision_);
            detail_precision = std::min(detail_precision, this->detail_precision_);
        }
        else if (!this->is_leaf() && request.Intersect(bbox_))
        {
            for (unsigned int i=0; i<8; ++i)
                childs_[i]->GetPrecision(request, feature_precision, detail_precision);
        }
    }

    Hint GetHint(const core::AABBox &request) const
    {
        if (bbox_.ContainsOrEquals(request))
        {
            if (!this->is_leaf())
                for (unsigned int i=0; i<8; ++i)
                {
                    Hint hint = childs_[i]->GetHint(request);
                    if (hint != NULL)
                        return hint;
                }
            return this;
        }
        else
        {
            return NULL;
        }
    }

    bool FeaturePrecisionLowerThan(const core::AABBox &request,
                                   Scalar precision) const
    {
        if (request.Intersect(bbox_))
        {
            if (!this->is_leaf())
            {
                if (this->feature_precision_ > precision)
                {
                    return false;
                }
                else if (request.Contains(bbox_) || this->max_feature_precision_ <= precision)
                {
                    return true;
                }

                for (unsigned int i=0; i<8; ++i)
                    if (childs_[i]->FeaturePrecisionLowerThan(request, precision))
                        return true;
                return false;
            }
            else
            {
                return (this->feature_precision_ <= precision);
            }
        }
        else
        {
            return false;
        }
    }

    bool DetailPrecisionLowerThan(const core::AABBox &request,
                                   Scalar precision) const
    {
        if (request.Intersect(bbox_))
        {
            if (!this->is_leaf())
            {
                if (this->detail_precision_ > precision)
                {
                    return false;
                }
                else if (request.ContainsOrEquals(bbox_) || this->max_detail_precision_ <= precision)
                    return true;

                for (unsigned int i=0; i<8; ++i)
                    if (childs_[i]->DetailPrecisionLowerThan(request, precision))
                        return true;
                return false;
            }
            else
            {
                return (this->detail_precision_ <= precision);
            }
        }
        else
        {
            return false;
        }
    }

    static core::AABBox child_bounding_box(const core::AABBox &thisbbox, unsigned int child_idx)
    {
        static const float bbox_mask[8][3][2][2] = {
        { {{1.,0.},{0.5,0.5}}  , {{1.,0.},{0.5,0.5}}  , {{1.,0.},{0.5,0.5}}   },
        { {{1.,0.},{0.5,0.5}}  , {{1.,0.},{0.5,0.5}}  , {{0.5,0.5},{0.0,1.0}} },
        { {{1.,0.},{0.5,0.5}}  , {{0.5,0.5},{0.0,1.0}}, {{1.,0.},{0.5,0.5}}   },
        { {{1.,0.},{0.5,0.5}}  , {{0.5,0.5},{0.0,1.0}}, {{0.5,0.5},{0.0,1.0}} },
        { {{0.5,0.5},{0.0,1.0}}, {{1.,0.},{0.5,0.5}}  , {{1.,0.},{0.5,0.5}}   },
        { {{0.5,0.5},{0.0,1.0}}, {{1.,0.},{0.5,0.5}}  , {{0.5,0.5},{0.0,1.0}} },
        { {{0.5,0.5},{0.0,1.0}}, {{0.5,0.5},{0.0,1.0}}, {{1.,0.},{0.5,0.5}}   },
        { {{0.5,0.5},{0.0,1.0}}, {{0.5,0.5},{0.0,1.0}}, {{0.5,0.5},{0.0,1.0}} }
        };

        return core::AABBox(
                    Vector(thisbbox.min_[0]*bbox_mask[child_idx][0][0][0]
                           +thisbbox.max_[0]*bbox_mask[child_idx][0][0][1],
                           thisbbox.min_[1]*bbox_mask[child_idx][1][0][0]
                           +thisbbox.max_[1]*bbox_mask[child_idx][1][0][1],
                           thisbbox.min_[2]*bbox_mask[child_idx][2][0][0]
                           +thisbbox.max_[2]*bbox_mask[child_idx][2][0][1]),
                    Vector(thisbbox.min_[0]*bbox_mask[child_idx][0][1][0]
                           +thisbbox.max_[0]*bbox_mask[child_idx][0][1][1],
                           thisbbox.min_[1]*bbox_mask[child_idx][1][1][0]
                           +thisbbox.max_[1]*bbox_mask[child_idx][1][1][1],
                           thisbbox.min_[2]*bbox_mask[child_idx][2][1][0]
                           +thisbbox.max_[2]*bbox_mask[child_idx][2][1][1]));
    }

    bool is_leaf() const
    {
        return childs_[0] == NULL;
    }

    void clear()
    {
        this->feature_precision_ = this->detail_precision_ = std::numeric_limits<Scalar>::infinity();
        this->collapse();
    }

    void reset(const core::AABBox &bbox)
    {
        bbox_ = bbox;
        clear();
    }

    const core::AABBox &bounding_box() const
    {
        return bbox_;
    }
private:
    void collapse()
    {
        if (!is_leaf())
            for (unsigned int i=0; i<8; ++i)
            {
                delete childs_[i];
                childs_[i] = NULL;
            }
    }

    void subdivide()
    {
        if (is_leaf())
            for (unsigned int i=0; i<8; ++i)
            {
                childs_[i] = new DCPrecisionOctreeNode(child_bounding_box(bbox_,i), *this);
            }
    }
private:
    Scalar feature_precision_;
    Scalar detail_precision_;
    Scalar max_feature_precision_;
    Scalar max_detail_precision_;
    core::AABBox bbox_;
    std::array<DCPrecisionOctreeNode *, 8> childs_;
};


class DCPrecisionOctree
{
public:
    typedef typename DCPrecisionOctreeNode::Hint Hint;

    DCPrecisionOctree(ImplicitSurface const *surface) : surface_(surface),
        root_(new DCPrecisionOctreeNode()), max_depth_(16),
        feature_modifier_(1.), detail_modifier_(1.)
    {}

    ~DCPrecisionOctree()
    {
        delete root_;
    }

    void SetSurface(ImplicitSurface const *surface)
    {
        surface_ = surface;
    }

    void Update(const core::AABBox& area_to_update)
    {
        const std::shared_ptr<BlobtreeRoot> bt_root = surface_->blobtree_root();
        std::list<core::PrecisionAxisBoundingBox > precboxes;

        root_->clear_precision_on_area(area_to_update);
        bt_root->GetPrecisionAxisBoundingBoxes(surface_->iso_value(),precboxes);
        DCPrecisionOctreeNode *node = const_cast<DCPrecisionOctreeNode *>(root_->GetHint(area_to_update));
        if (node == nullptr)
            node = root_;

        for (typename std::list<core::PrecisionAxisBoundingBox >::const_iterator it = precboxes.begin();
             it != precboxes.end(); ++it)
            if (it->Intersect(area_to_update))
                node->add_precision_box(*it, max_depth_);
    }

    /// @deprecated
    void GetPrecision(const core::AABBox &request,
                      Scalar &feature_precision, Scalar &detail_precision) const
    {
        feature_precision = detail_precision = std::numeric_limits<Scalar>::infinity();
        root_->GetPrecision(request, feature_precision, detail_precision);
        feature_precision *= feature_modifier_;
        detail_precision *= detail_modifier_;
    }

    void SetPrecisionModifiers(Scalar feature, Scalar detail)
    {
       feature_modifier_ = feature;
       detail_modifier_ = detail;
    }

    Hint GetDefaultHint() const
    {
        return root_;
    }

    void UpdateHint(const core::AABBox &bbox, Hint &hint) const
    {
        if (hint == NULL)
        {
            hint = root_;
        }
        else
        {
            assert(hint->bounding_box().ContainsOrEquals(bbox) || hint == root_);
        }

        hint = hint->GetHint(bbox);

        if (hint == NULL)
        {
            hint = root_;
            assert(!root_->bounding_box().ContainsOrEquals(bbox));
        }
    }

    bool FeaturePrecisionLowerThan(const core::AABBox &request, Scalar precision,
                                   Hint hint=NULL) const
    {
        if (precision > 0. && feature_modifier_ > 0.)
        {
            if (hint == NULL){
                UpdateHint(request,hint);
            }
            else
            {
//                assert(hint->bounding_box().ContainsOrEquals(request)); //TODO:@todo : this warning crash a little bit to often and i don't know why
            }
            return hint->FeaturePrecisionLowerThan(request, precision/feature_modifier_);
        }
        else
        {
            return true;
        }
    }

    bool DetailPrecisionLowerThan(const core::AABBox &request, Scalar precision,
                                  Hint hint=NULL) const
    {
        if (precision > 0. && detail_modifier_ > 0.)
        {
            if (hint == NULL){
                UpdateHint(request,hint);
            }
            else
            {
//                assert(hint->bounding_box().ContainsOrEquals(request)); //TODO:@todo : this warning crash a little bit to often and i don't know why
            }
            return hint->DetailPrecisionLowerThan(request, precision/detail_modifier_);
        }
        else
        {
            return true;
        }
    }

    void Reset(const core::AABBox &octree_area)
    {
        //bbox_ = octree_area;
        //octree_area.Scale(1. + 2./(1<<max_depth_));
        root_->reset(octree_area);

        const std::shared_ptr<BlobtreeRoot> bt_root = surface_->blobtree_root();
        std::list<core::PrecisionAxisBoundingBox > precboxes;

        bt_root->GetPrecisionAxisBoundingBoxes(surface_->iso_value(),precboxes);

        for (typename std::list<core::PrecisionAxisBoundingBox >::const_iterator it = precboxes.begin();
             it != precboxes.end(); ++it)
                root_->add_precision_box(*it,max_depth_);
    }

private:
    ImplicitSurface const *surface_;

    DCPrecisionOctreeNode *root_;
    int max_depth_;

    Scalar feature_modifier_, detail_modifier_;
};

/*****************************************************************************/
/***************************** DEBUG STRUCTURES ******************************/
/*****************************************************************************/

class BruteForcePrecisionFunction
{
public:

    typedef typename std::list<core::PrecisionAxisBoundingBox >::const_iterator Hint;

    BruteForcePrecisionFunction(ImplicitSurface const *surface) : surface_(surface),
        feature_modifier_(1.), detail_modifier_(1.)
    {}

    void SetSurface(ImplicitSurface const *surface)
    {
        surface_ = surface;
    }

    void Update(const core::AABBox&)
    {
        precboxes_.clear();
        surface_->blobtree_root()->GetPrecisionAxisBoundingBoxes(
                    surface_->iso_value(), precboxes_);
    }

    /// @deprecated
    void GetPrecision(const core::AABBox &request,
                      Scalar &feature_precision, Scalar &detail_precision) const
    {
        feature_precision = detail_precision = std::numeric_limits<Scalar>::infinity();

        for (Hint it = precboxes_.begin();
             it != precboxes_.end(); ++it)
        {
            if (request.Intersect(*it))
            {
                feature_precision = std::min(feature_precision, it->feature_precision);
                detail_precision = std::min(detail_precision, it->detail_precision);
            }
        }

        feature_precision *= feature_modifier_;
        detail_precision *= detail_modifier_;
    }

    void SetPrecisionModifiers(Scalar feature, Scalar detail)
    {
       feature_modifier_ = feature;
       detail_modifier_ = detail;
    }

    Hint GetDefaultHint() const
    {
        return precboxes_.begin();
    }

    void UpdateHint(const core::AABBox &bbox, Hint &hint) const
    {
        if (hint == Hint())
        {
            hint = GetDefaultHint();
        }

        Scalar min_detail = std::numeric_limits<Scalar>::infinity();

        if (hint == Hint() || hint == precboxes_.end() || bbox.Intersect(*hint))
            for (Hint it = precboxes_.begin();
                 it != precboxes_.end(); ++it)
            {
                if (bbox.Intersect(*it))
                {
                     if (it->detail_precision < min_detail)
                     {
                         min_detail = it->detail_precision;
                         hint = it;
                     }
                }
            }
    }

    bool FeaturePrecisionLowerThan(const core::AABBox &request, Scalar precision,
                                   Hint hint) const
    {
        precision /= feature_modifier_;
        if (hint != Hint() && hint != precboxes_.end() &&
                hint->feature_precision <= precision &&
                request.Intersect(*hint))
            return true;
        else
            for (Hint it = precboxes_.begin();
                 it != precboxes_.end(); ++it)
                if (it->feature_precision <= precision && request.Intersect(*it))
                    return true;
        return false;
    }

    bool DetailPrecisionLowerThan(const core::AABBox &request, Scalar precision,
                                  Hint hint) const
    {
        precision /= detail_modifier_;
        if (hint != Hint() && hint != precboxes_.end() &&
                hint->detail_precision <= precision &&
                request.Intersect(*hint))
            return true;
        else
            for (Hint it = precboxes_.begin();
                 it != precboxes_.end(); ++it)
                if (it->detail_precision <= precision && request.Intersect(*it))
                    return true;
        return false;
    }

    void Reset(core::AABBox octree_area)
    {
        Update(octree_area);
    }

private:
    ImplicitSurface const *surface_;

    std::list<core::PrecisionAxisBoundingBox > precboxes_;

    Scalar feature_modifier_, detail_modifier_;
};

template<typename PrecFunction1,
         typename PrecFunction2>
class PrecisionFunctionComparator
{
public:

    struct Hint
    {
        typename PrecFunction1::Hint hint1;
        typename PrecFunction2::Hint hint2;
    };

    PrecisionFunctionComparator(ImplicitSurface const *surface) : func1(surface),
        func2(surface)
    {}

    void SetSurface(ImplicitSurface const *surface)
    {
        func1.SetSurface(surface);
        func2.SetSurface(surface);
    }

    void Update(const core::AABBox &bbox)
    {
        func1.Update(bbox);
        func2.Update(bbox);
    }

    /// Deprecated
    void GetPrecision(const core::AABBox &request,
                      Scalar &feature_precision, Scalar &detail_precision) const
    {
        Scalar feat, detail;
        func1.GetPrecision(request,feature_precision,detail_precision);
        func2.GetPrecision(request,feat,detail);
        assert(feature_precision == feat);
        assert(detail_precision == detail);
    }

    void SetPrecisionModifiers(Scalar feature, Scalar detail)
    {
        func1.SetPrecisionModifiers(feature,detail);
        func2.SetPrecisionModifiers(feature,detail);
    }

    Hint GetDefaultHint() const
    {
        Hint hint;
        hint.hint1 = func1.GetDefaultHint();
        hint.hint2 = func2.GetDefaultHint();
        return hint;
    }

    void UpdateHint(const core::AABBox &bbox, Hint &hint) const
    {
        func1.UpdateHint(bbox, hint.hint1);
        func2.UpdateHint(bbox, hint.hint2);
    }

    void PrecisionMismatch(const core::AABBox &request) const
    {
        std::cerr << "Precision Mismatch : " << "{ ["
                  << request.min_[0] << ", " << request.min_[1] << ", " << request.min_[2] << "], ["
                  << request.max_[0] << ", " << request.max_[1] << ", " << request.max_[2] << "] }" << std::endl;
        Scalar feat1, det1, feat2, det2;
        func1.GetPrecision(request,feat1,det1);
        func2.GetPrecision(request,feat2,det2);
        std::cerr << " Feature Precision : " << feat1 << " vs " << feat2 << std::endl;
        std::cerr << " Detail  Precision : " << det1  << " vs "  << det2 << std::endl;
    }

    bool FeaturePrecisionLowerThan(const core::AABBox &request, Scalar precision,
                                   Hint hint) const
    {
        bool ans1 = func1.FeaturePrecisionLowerThan(request, precision,hint.hint1);
        bool ans2 = func2.FeaturePrecisionLowerThan(request, precision,hint.hint2);
        if (ans1 != ans2)
        {
            PrecisionMismatch(request);
            func1.FeaturePrecisionLowerThan(request, precision,hint.hint1);
            func2.FeaturePrecisionLowerThan(request, precision,hint.hint2);
            assert(ans2);
        }
        return ans1 || ans2;
    }

    bool DetailPrecisionLowerThan(const core::AABBox &request, Scalar precision,
                                  Hint hint) const
    {
        bool ans1 = func1.DetailPrecisionLowerThan(request, precision,hint.hint1);
        bool ans2 = func2.DetailPrecisionLowerThan(request, precision,hint.hint2);
        if (ans1 != ans2)
        {
            PrecisionMismatch(request);
            func1.DetailPrecisionLowerThan(request, precision,hint.hint1);
            func2.DetailPrecisionLowerThan(request, precision,hint.hint2);
            assert(ans2);
        }
        return ans1 || ans2;
    }

    void Reset(core::AABBox octree_area)
    {
        func1.Reset(octree_area);
        func2.Reset(octree_area);
    }

private:
    PrecFunction1 func1;
    PrecFunction2 func2;
};

struct PrecisionFunctionToUse
{
    typedef DCPrecisionOctree Function;
    // For Debug Only !!!!
    //typedef BruteForcePrecisionFunctionT<Traits> Function;
    //typedef PrecisionFunctionComparatorT<Traits, BruteForcePrecisionFunctionT, DCPrecisionOctreeT> Function;
};

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive

#endif
