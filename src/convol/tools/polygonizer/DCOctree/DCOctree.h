/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: DCOctree.h

   Language: C++

   License: Convol Licence

   \author: Amaury Jung
   E-Mail: amaury.jung@inria.fr


   Platform Dependencies: None
*/


#ifndef CONVOL_DC_OCTREE_H_
#define CONVOL_DC_OCTREE_H_

#ifndef QT_NO_DEBUG
//#define DC_OCTREE_SIZE_CHECKS
//#define DCOCTREE_DISPLAY_TIMES
//#define DC_OCTREE_DISPLAY_WARNINGS
//#define DC_OCTREE_ACCURATE_BBOXES
#endif


// core dependencies
#include<core/geometry/AABBox.h>

// Convol dependencies
#include<convol/ConvolRequired.h>
#include<convol/ImplicitSurface.h>
    // Tools
//    #include<Convol/include/tools/SimpleAxisGridT.h>
    #include<convol/tools/ConvergenceTools.h>

// Convol-Tools dependencies
    // Polygonizer
    #include<convol/tools/polygonizer/DCOctree/DCOctreeStructures.h>


namespace expressive {
namespace convol {
namespace tools {


template< typename TTriMesh, typename TVertexProcessor >
class DCOctreeNodeT;



template< typename TTriMesh, typename TVertexProcessor >
class DCOctreeT
{
public:
    typedef DCOctreeT<TTriMesh, TVertexProcessor> DCOctree;
    typedef DCOctreeNodeT<TTriMesh, TVertexProcessor> DCOctreeNode;
    typedef DCOctreeStructures::GridHashTableT<Scalar> GridHashTable;

//    typedef tools::SimpleAxisGridT<Traits,Scalar> SimpleAxisGrid;

    typedef TTriMesh TriMesh;

    typedef typename TriMesh::VertexHandle Vertex;
    typedef typename TriMesh::FaceHandle Face;
    typedef typename TriMesh::HalfedgeHandle Halfedge; // only here because we can't do otherwise
    typedef typename TriMesh::EdgeHandle Edge;

    ImplicitSurface const *function;

    std::shared_ptr<TriMesh> tri_mesh_;

    DCOctreeT(ImplicitSurface *f, std::shared_ptr<TriMesh> mesh, unsigned int _max_depth = DCOctreeStructures::OCTREE_MAX_DEPTH) : function(f),
        tri_mesh_(mesh), max_depth(_max_depth << 0), nb_evals(0),
        n_updated_vertices(0), n_updated_triangles(0), root(new DCOctreeNode())
      #ifdef DC_OCTREE_MULTI_THREAD_SUPPORT
        , polygonizer_pool_(1),
        worker_pool_(boost::thread::hardware_concurrency())
      #endif
    {}

    ~DCOctreeT()
    {
        delete root;
    }

    Point GetWorldPoint(const unsigned int position[3]) const
    {
        return Point( Scalar(position[0])*cell_size_[0] + corner_000_[0],
                      Scalar(position[1])*cell_size_[1] + corner_000_[1],
                      Scalar(position[2])*cell_size_[2] + corner_000_[2]);
    }

    Point GetWorldPoint(unsigned int x, unsigned int y, unsigned int z) const
    {
        return Point( Scalar(x)*cell_size_[0] + corner_000_[0],
                      Scalar(y)*cell_size_[1] + corner_000_[1],
                      Scalar(z)*cell_size_[2] + corner_000_[2]);
    }

    core::AABBox GetWorldBoundingBox(const unsigned int position[3], unsigned int length) const
    {
        return core::AABBox(GetWorldPoint(position),//-Vector::vectorized(unit_length_),
                               GetWorldPoint(position[0]+length,
                                             position[1]+length,
                                             position[2]+length));//+Vector::vectorized(unit_length_));
    }

    template <class PrecisionFunction>
    bool IncreasePrecision(const PrecisionFunction &precision);

    template <typename PrecisionFunction>
    void UpdateSurface(const core::AABBox &bbox, const PrecisionFunction &precision,
                       bool multi_thread = false);

    template<class PrecisionFunction>
    void ResetSurface(const PrecisionFunction &precision,
                      core::AABBox bbox = core::AABBox(),
                      bool multi_thread = true);

    // ResetSurfaceShould MUST BE CALLED after this function call
    void SetWorldBoundingBox(const core::AABBox &globalbbox)
    {
        corner_000_ = globalbbox.min_;
        const Vector size = globalbbox.max_ - globalbbox.min_;
        cell_size_ = size/Scalar(1<<max_depth);
        unit_length_ = std::min(std::min(cell_size_[0],cell_size_[1]),cell_size_[2]);
    }

    core::AABBox GlobalBoundingBox() const
    {
        unsigned int pos[3] = {0,0,0};
        return GetWorldBoundingBox(pos,1<<max_depth);
    }

    Scalar GetWorldLength(unsigned int length)
    {
        return length*unit_length_;
    }

public:
    unsigned char max_depth;
    unsigned char percentage_updated_vertices;
    unsigned char percentage_updated_triangles;

    unsigned int cleaning_pass_time;
    unsigned int tree_update_pass_time;
    unsigned int correction_pass_time;
    unsigned int tesselation_pass_time;

    unsigned int corrected_vertices;
    unsigned int nb_evals;

    unsigned int n_updated_vertices;
    unsigned int n_updated_triangles;
private :
    Point  corner_000_;
    Vector cell_size_;

    Scalar unit_length_;

public :
    GridHashTable vertices_table;
    DCOctreeNode *root;

#ifdef DC_OCTREE_MULTI_THREAD_SUPPORT
    boost::threadpool::pool polygonizer_pool_;
    boost::threadpool::pool worker_pool_;
#endif
};

//#include<Convol-Tools/include/Polygonizer/DCOctree/DCOctreeNode.h>

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive


#endif
