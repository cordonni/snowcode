//----------------------------------------------------------------------------
// ThreeD Quadric Error Function
//----------------------------------------------------------------------------

#include <convol/tools/polygonizer/DCOctree/qef.h>


/*

  Numerical functions for computing minimizers of a least-squares system
  of equations.

  Copyright (C) 2011 Scott Schaefer

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  (LGPL) as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


//#include "eigen.hpp"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define ROTATE(a,i,j,k,l) g=a[i][j];h=a[k][l];a[i][j]=g-s*(h+g*tau);a[k][l]=h+s*(g-h*tau);

// used to select solving method in calcPoint()
int method = 3;

// for reducing two upper triangular systems of equations into 1
void qr ( float *mat1, float *mat2, float *rvalue )
{
    int i, j;
    float temp1 [ 8 ] [ 4 ];

    for ( i = 0; i < 4; i++ )
    {
        for ( j = 0; j < i; j++ )
        {
            temp1 [ i ] [ j ] = 0;
            temp1 [ i + 4 ] [ j ] = 0;
        }
        for ( j = i; j < 4; j++ )
        {
            temp1 [ i ] [ j ] = mat1 [ ( 7 * i - i * i ) / 2 + j ];
            temp1 [ i + 4 ] [ j ] = mat2 [ ( 7 * i - i * i ) / 2 + j ];
        }
    }

    qr ( temp1, 8, rvalue );
}

// WARNING: destroys eqs in the process
void qr ( float eqs[][4], int num, float *rvalue )
{
    int i, j, k;

    qr ( eqs, num, 0.000001f );
    for ( i = 0; i < 10; i++ )
    {
        rvalue [ i ] = 0;
    }

    k = 0;
    for ( i = 0; i < num && i < 4; i++ )
    {
        for ( j = i; j < 4; j++ )
        {
            rvalue [ k++ ] = eqs [ i ] [ j ];
        }
    }
}

void qr ( float eqs[][4], int num, float tol )
{
    UNUSED(tol);
    int i, j, k;
    float a, b, mag, temp;

    for ( i = 0; i < 4 && i < num; i++ )
    {
        for ( j = i + 1; j < num; j++ )
        {
            a = eqs [ i ] [ i ];
            b = eqs [ j ] [ i ];

            if ( fabs ( a ) > 0.000001f || fabs ( b ) > 0.000001f )
            {
                mag = (float)sqrt ( a * a + b * b );
                a /= mag;
                b /= mag;

                for ( k = 0; k < 4; k++ )
                {
                    temp = a * eqs [ i ] [ k ] + b * eqs [ j ] [ k ];
                    eqs [ j ] [ k ] = b * eqs [ i ] [ k ] - a * eqs [ j ] [ k ];
                    eqs [ i ] [ k ] = temp;
                }
            }
        }
        for ( j = i - 1; j >= 0; j-- )
        {
            if ( eqs [ j ] [ j ] < 0.000001f && eqs [ j ] [ j ] > -0.000001f )
            {
                a = eqs [ i ] [ i ];
                b = eqs [ j ] [ i ];

                if ( fabs ( a ) > 0.000001f || fabs ( b ) > 0.000001f )
                {
                    mag = (float)sqrt ( a * a + b * b );
                    a /= mag;
                    b /= mag;

                    for ( k = 0; k < 4; k++ )
                    {
                        temp = a * eqs [ i ] [ k ] + b * eqs [ j ] [ k ];
                        eqs [ j ] [ k ] = b * eqs [ i ] [ k ] - a * eqs [ j ] [ k ];
                        eqs [ i ] [ k ] = temp;
                    }
                }
            }
        }
    }

}

void jacobi ( float u[][3], float d[], float v[][3] )
{
    int j, iq, ip, i;
    float tresh, theta, tau, t, sm, s, h, g, c, b [ 3 ], z [ 3 ];
    float a [ 3 ] [ 3 ];

    a [ 0 ] [ 0 ] = u [ 0 ] [ 0 ];
    a [ 0 ] [ 1 ] = u [ 0 ] [ 1 ];
    a [ 0 ] [ 2 ] = u [ 0 ] [ 2 ];
    a [ 1 ] [ 0 ] = u [ 1 ] [ 0 ];
    a [ 1 ] [ 1 ] = u [ 1 ] [ 1 ];
    a [ 1 ] [ 2 ] = u [ 1 ] [ 2 ];
    a [ 2 ] [ 0 ] = u [ 2 ] [ 0 ];
    a [ 2 ] [ 1 ] = u [ 2 ] [ 1 ];
    a [ 2 ] [ 2 ] = u [ 2 ] [ 2 ];

    for ( ip = 0; ip < 3; ip++ )
    {
        for ( iq = 0; iq < 3; iq++ )
        {
            v [ ip ] [ iq ] = 0.0f;
        }
        v [ ip ] [ ip ] = 1.0f;
    }

    for ( ip = 0; ip < 3; ip++ )
    {
        b [ ip ] = a [ ip ] [ ip ];
        d [ ip ] = b [ ip ];
        z [ ip ] = 0.0f;
    }

    for ( i = 1; i <= 50; i++ )
    {
        sm = 0.0f;
        for ( ip = 0; ip < 2; ip++ )
        {
            for ( iq = ip + 1; iq < 3; iq++ )
            {
                sm += (float)fabs ( a [ ip ] [ iq ] );
            }
        }

        if ( sm == 0.0f )
        {
            // sort the stupid things and transpose
            a [ 0 ] [ 0 ] = v [ 0 ] [ 0 ];
            a [ 0 ] [ 1 ] = v [ 1 ] [ 0 ];
            a [ 0 ] [ 2 ] = v [ 2 ] [ 0 ];
            a [ 1 ] [ 0 ] = v [ 0 ] [ 1 ];
            a [ 1 ] [ 1 ] = v [ 1 ] [ 1 ];
            a [ 1 ] [ 2 ] = v [ 2 ] [ 1 ];
            a [ 2 ] [ 0 ] = v [ 0 ] [ 2 ];
            a [ 2 ] [ 1 ] = v [ 1 ] [ 2 ];
            a [ 2 ] [ 2 ] = v [ 2 ] [ 2 ];

            if ( fabs ( d [ 0 ] ) < fabs ( d [ 1 ] ) )
            {
                sm = d [ 0 ];
                d [ 0 ] = d [ 1 ];
                d [ 1 ] = sm;

                sm = a [ 0 ] [ 0 ];
                a [ 0 ] [ 0 ] = a [ 1 ] [ 0 ];
                a [ 1 ] [ 0 ] = sm;
                sm = a [ 0 ] [ 1 ];
                a [ 0 ] [ 1 ] = a [ 1 ] [ 1 ];
                a [ 1 ] [ 1 ] = sm;
                sm = a [ 0 ] [ 2 ];
                a [ 0 ] [ 2 ] = a [ 1 ] [ 2 ];
                a [ 1 ] [ 2 ] = sm;
            }
            if ( fabs ( d [ 1 ] ) < fabs ( d [ 2 ] ) )
            {
                sm = d [ 1 ];
                d [ 1 ] = d [ 2 ];
                d [ 2 ] = sm;

                sm = a [ 1 ] [ 0 ];
                a [ 1] [ 0 ] = a [ 2 ] [ 0 ];
                a [ 2 ] [ 0 ] = sm;
                sm = a [ 1 ] [ 1 ];
                a [ 1 ] [ 1 ] = a [ 2 ] [ 1 ];
                a [ 2 ] [ 1 ] = sm;
                sm = a [ 1 ] [ 2 ];
                a [ 1 ] [ 2 ] = a [ 2 ] [ 2 ];
                a [ 2 ] [ 2 ] = sm;
            }
            if ( fabs ( d [ 0 ] ) < fabs ( d [ 1 ] ) )
            {
                sm = d [ 0 ];
                d [ 0 ] = d [ 1 ];
                d [ 1 ] = sm;

                sm = a [ 0 ] [ 0 ];
                a [ 0 ] [ 0 ] = a [ 1 ] [ 0 ];
                a [ 1 ] [ 0 ] = sm;
                sm = a [ 0 ] [ 1 ];
                a [ 0 ] [ 1 ] = a [ 1 ] [ 1 ];
                a [ 1 ] [ 1 ] = sm;
                sm = a [ 0 ] [ 2 ];
                a [ 0 ] [ 2 ] = a [ 1 ] [ 2 ];
                a [ 1 ] [ 2 ] = sm;
            }

            v [ 0 ] [ 0 ] = a [ 0 ] [ 0 ];
            v [ 0 ] [ 1 ] = a [ 0 ] [ 1 ];
            v [ 0 ] [ 2 ] = a [ 0 ] [ 2 ];
            v [ 1 ] [ 0 ] = a [ 1 ] [ 0 ];
            v [ 1 ] [ 1 ] = a [ 1 ] [ 1 ];
            v [ 1 ] [ 2 ] = a [ 1 ] [ 2 ];
            v [ 2 ] [ 0 ] = a [ 2 ] [ 0 ];
            v [ 2 ] [ 1 ] = a [ 2 ] [ 1 ];
            v [ 2 ] [ 2 ] = a [ 2 ] [ 2 ];

            return;
        }

        if ( i < 4 )
        {
            tresh = 0.2f * sm / 9;
        }
        else
        {
            tresh = 0.0f;
        }

        for ( ip = 0; ip < 2; ip++ )
        {
            for ( iq = ip + 1; iq < 3; iq++ )
            {
                g = 100.0f * (float)fabs ( a [ ip ] [ iq ] );
                if ( i > 4 && (float)( fabs ( d [ ip ] ) + g ) == (float)fabs ( d [ ip ] )
                    && (float)( fabs ( d [ iq ] ) + g ) == (float)fabs ( d [ iq ] ) )
                {
                    a [ ip ] [ iq ] = 0.0f;
                }
                else
                {
                    if ( fabs ( a [ ip ] [ iq ] ) > tresh )
                    {
                        h = d [ iq ] - d [ ip ];
                        if ( (float)( fabs ( h ) + g ) == (float)fabs ( h ) )
                        {
                            t = ( a [ ip ] [ iq ] ) / h;
                        }
                        else
                        {
                            theta = 0.5f * h / ( a [ ip ] [ iq ] );
                            t = 1.0f / ( (float)fabs ( theta ) + (float)sqrt ( 1.0f + theta * theta ) );
                            if ( theta < 0.0f )
                            {
                                t = -1.0f * t;
                            }
                        }

                        c = 1.0f / (float)sqrt ( 1 + t * t );
                        s = t * c;
                        tau = s / ( 1.0f + c );
                        h = t * a [ ip ] [ iq ];
                        z [ ip ] -= h;
                        z [ iq ] += h;
                        d [ ip ] -= h;
                        d [ iq ] += h;
                        a [ ip ] [ iq ] = 0.0f;
                        for ( j = 0; j <= ip - 1; j++ )
                        {
                            ROTATE ( a, j, ip, j, iq )
                        }
                        for ( j = ip + 1; j <= iq - 1; j++ )
                        {
                            ROTATE ( a, ip, j, j, iq )
                        }
                        for ( j = iq + 1; j < 3; j++ )
                        {
                            ROTATE ( a, ip, j, iq, j )
                        }
                        for ( j = 0; j < 3; j++ )
                        {
                            ROTATE ( v, j, ip, j, iq )
                        }
                    }
                }
            }
        }

        for ( ip = 0; ip < 3; ip++ )
        {
            b [ ip ] += z [ ip ];
            d [ ip ] = b [ ip ];
            z [ ip ] = 0.0f;
        }
    }
    printf ( "too many iterations in jacobi\n" );
//    exit ( 1 );
}

int estimateRank ( float *a )
{
    float w [ 3 ];
    float u [ 3 ] [ 3 ];
    float mat [ 3 ] [ 3 ];
    int i;

    mat [ 0 ] [ 0 ] = a [ 0 ];
    mat [ 0 ] [ 1 ] = a [ 1 ];
    mat [ 0 ] [ 2 ] = a [ 2 ];
    mat [ 1 ] [ 1 ] = a [ 3 ];
    mat [ 1 ] [ 2 ] = a [ 4 ];
    mat [ 2 ] [ 2 ] = a [ 5 ];
    mat [ 1 ] [ 0 ] = a [ 1 ];
    mat [ 2 ] [ 0 ] = a [ 2 ];
    mat [ 2 ] [ 1 ] = a [ 4 ];

    jacobi ( mat, w, u );

    if ( w [ 0 ] == 0.0f )
    {
        return 0;
    }
    else
    {
        for ( i = 1; i < 3; i++ )
        {
            if ( w [ i ] < 0.1f )
            {
                return i;
            }
        }

        return 3;
    }

}

void matInverse ( float mat[][3], float midpoint[], float rvalue[][3], float w[], float u[][3] )
{
    UNUSED(midpoint);
    // there is an implicit assumption that mat is symmetric and real
    // U and V in the SVD will then be the same matrix whose rows are the eigenvectors of mat
    // W will just be the eigenvalues of mat
//	float w [ 3 ];
//	float u [ 3 ] [ 3 ];
    int i;

    jacobi ( mat, w, u );

    if ( w [ 0 ] == 0.0f )
    {
//		printf ( "error: largest eigenvalue is 0!\n" );
    }
    else
    {
        for ( i = 1; i < 3; i++ )
        {
            if ( w [ i ] < 0.001f ) // / w [ 0 ] < TOLERANCE )
            {
                    w [ i ] = 0;
            }
            else
            {
                w [ i ] = 1.0f / w [ i ];
            }
        }
        w [ 0 ] = 1.0f / w [ 0 ];
    }

    rvalue [ 0 ] [ 0 ] = w [ 0 ] * u [ 0 ] [ 0 ] * u [ 0 ] [ 0 ] +
                    w [ 1 ] * u [ 1 ] [ 0 ] * u [ 1 ] [ 0 ] +
                    w [ 2 ] * u [ 2 ] [ 0 ] * u [ 2 ] [ 0 ];
    rvalue [ 0 ] [ 1 ] = w [ 0 ] * u [ 0 ] [ 0 ] * u [ 0 ] [ 1 ] +
                    w [ 1 ] * u [ 1 ] [ 0 ] * u [ 1 ] [ 1 ] +
                    w [ 2 ] * u [ 2 ] [ 0 ] * u [ 2 ] [ 1 ];
    rvalue [ 0 ] [ 2 ] = w [ 0 ] * u [ 0 ] [ 0 ] * u [ 0 ] [ 2 ] +
                    w [ 1 ] * u [ 1 ] [ 0 ] * u [ 1 ] [ 2 ] +
                    w [ 2 ] * u [ 2 ] [ 0 ] * u [ 2 ] [ 2 ];
    rvalue [ 1 ] [ 0 ] = w [ 0 ] * u [ 0 ] [ 1 ] * u [ 0 ] [ 0 ] +
                    w [ 1 ] * u [ 1 ] [ 1 ] * u [ 1 ] [ 0 ] +
                    w [ 2 ] * u [ 2 ] [ 1 ] * u [ 2 ] [ 0 ];
    rvalue [ 1 ] [ 1 ] = w [ 0 ] * u [ 0 ] [ 1 ] * u [ 0 ] [ 1 ] +
                    w [ 1 ] * u [ 1 ] [ 1 ] * u [ 1 ] [ 1 ] +
                    w [ 2 ] * u [ 2 ] [ 1 ] * u [ 2 ] [ 1 ];
    rvalue [ 1 ] [ 2 ] = w [ 0 ] * u [ 0 ] [ 1 ] * u [ 0 ] [ 2 ] +
                    w [ 1 ] * u [ 1 ] [ 1 ] * u [ 1 ] [ 2 ] +
                    w [ 2 ] * u [ 2 ] [ 1 ] * u [ 2 ] [ 2 ];
    rvalue [ 2 ] [ 0 ] = w [ 0 ] * u [ 0 ] [ 2 ] * u [ 0 ] [ 0 ] +
                    w [ 1 ] * u [ 1 ] [ 2 ] * u [ 1 ] [ 0 ] +
                    w [ 2 ] * u [ 2 ] [ 2 ] * u [ 2 ] [ 0 ];
    rvalue [ 2 ] [ 1 ] = w [ 0 ] * u [ 0 ] [ 2 ] * u [ 0 ] [ 1 ] +
                    w [ 1 ] * u [ 1 ] [ 2 ] * u [ 1 ] [ 1 ] +
                    w [ 2 ] * u [ 2 ] [ 2 ] * u [ 2 ] [ 1 ];
    rvalue [ 2 ] [ 2 ] = w [ 0 ] * u [ 0 ] [ 2 ] * u [ 0 ] [ 2 ] +
                    w [ 1 ] * u [ 1 ] [ 2 ] * u [ 1 ] [ 2 ] +
                    w [ 2 ] * u [ 2 ] [ 2 ] * u [ 2 ] [ 2 ];
}

float calcError ( float a[][3], float b[], float btb, float point[] )
{
    float rvalue = btb;

    rvalue += -2.0f * ( point [ 0 ] * b [ 0 ] + point [ 1 ] * b [ 1 ] + point [ 2 ] * b [ 2 ] );
    rvalue += point [ 0 ] * ( a [ 0 ] [ 0 ] * point [ 0 ] + a [ 0 ] [ 1 ] * point [ 1 ] + a [ 0 ] [ 2 ] * point [ 2 ] );
    rvalue += point [ 1 ] * ( a [ 1 ] [ 0 ] * point [ 0 ] + a [ 1 ] [ 1 ] * point [ 1 ] + a [ 1 ] [ 2 ] * point [ 2 ] );
    rvalue += point [ 2 ] * ( a [ 2 ] [ 0 ] * point [ 0 ] + a [ 2 ] [ 1 ] * point [ 1 ] + a [ 2 ] [ 2 ] * point [ 2 ] );

    return rvalue;
}

float *calcNormal ( float halfA[], float norm[], float expectedNorm[] )
{
    UNUSED(halfA);
/*
    float a [ 3 ] [ 3 ];
    float w [ 3 ];
    float u [ 3 ] [ 3 ];

    a [ 0 ] [ 0 ] = halfA [ 0 ];
    a [ 0 ] [ 1 ] = halfA [ 1 ];
    a [ 0 ] [ 2 ] = halfA [ 2 ];
    a [ 1 ] [ 1 ] = halfA [ 3 ];
    a [ 1 ] [ 2 ] = halfA [ 4 ];
    a [ 1 ] [ 0 ] = halfA [ 1 ];
    a [ 2 ] [ 0 ] = halfA [ 2 ];
    a [ 2 ] [ 1 ] = halfA [ 4 ];
    a [ 2 ] [ 2 ] = halfA [ 5 ];

    jacobi ( a, w, u );

    if ( u [ 1 ] != 0 )
    {
        if ( w [ 1 ] / w [ 0 ] > 0.2f )
        {
            // two dominant eigen values, just return the expectedNorm
            norm [ 0 ] = expectedNorm [ 0 ];
            norm [ 1 ] = expectedNorm [ 1 ];
            norm [ 2 ] = expectedNorm [ 2 ];
            return;
        }
    }

    norm [ 0 ] = u [ 0 ] [ 0 ];
    norm [ 1 ] = u [ 0 ] [ 1 ];
    norm [ 2 ] = u [ 0 ] [ 2 ];
*/
    float dot = norm [ 0 ] * expectedNorm [ 0 ] + norm [ 1 ] * expectedNorm [ 1 ] +
                norm [ 2 ] * expectedNorm [ 2 ];

    if ( dot < 0 )
    {
        norm [ 0 ] *= -1.0f;
        norm [ 1 ] *= -1.0f;
        norm [ 2 ] *= -1.0f;

        dot *= -1.0f;
    }

    if ( dot < 0.707f )
    {
        return expectedNorm;
    }
    else
    {
        return norm;
    }
}

void descent ( float A[][3], float B[], float guess[], const expressive::core::AABBox &box )
{
    int i;
    float r [ 3 ];
    float delta, delta0;
    int n = 10;
    float alpha, div;
    float newPoint [ 3 ];
    float c;
    float store [ 3 ];

    store [ 0 ] = guess [ 0 ];
    store [ 1 ] = guess [ 1 ];
    store [ 2 ] = guess [ 2 ];

    if ( method == 2 || method == 0 ) {

        i = 0;
        r [ 0 ] = B [ 0 ] - ( A [ 0 ] [ 0 ] * guess [ 0 ] + A [ 0 ] [ 1 ] * guess [ 1 ] + A [ 0 ] [ 2 ] * guess [ 2 ] );
        r [ 1 ] = B [ 1 ] - ( A [ 1 ] [ 0 ] * guess [ 0 ] + A [ 1 ] [ 1 ] * guess [ 1 ] + A [ 1 ] [ 2 ] * guess [ 2 ] );
        r [ 2 ] = B [ 2 ] - ( A [ 2 ] [ 0 ] * guess [ 0 ] + A [ 2 ] [ 1 ] * guess [ 1 ] + A [ 2 ] [ 2 ] * guess [ 2 ] );

        delta = r [ 0 ] * r [ 0 ] + r [ 1 ] * r [ 1 ] + r [ 2 ] * r [ 2 ];
        delta0 = delta * TOLERANCE * TOLERANCE;

        while ( i < n && delta > delta0 ) {
            div = r [ 0 ] * ( A [ 0 ] [ 0 ] * r [ 0 ] + A [ 0 ] [ 1 ] * r [ 1 ] + A [ 0 ] [ 2 ] * r [ 2 ] );
            div += r [ 1 ] * ( A [ 1 ] [ 0 ] * r [ 0 ] + A [ 1 ] [ 1 ] * r [ 1 ] + A [ 1 ] [ 2 ] * r [ 2 ] );
            div += r [ 2 ] * ( A [ 2 ] [ 0 ] * r [ 0 ] + A [ 2 ] [ 1 ] * r [ 1 ] + A [ 2 ] [ 2 ] * r [ 2 ] );

            if ( fabs ( div ) < 0.0000001f )
                break;

            alpha = delta / div;

            newPoint [ 0 ] = guess [ 0 ] + alpha * r [ 0 ];
            newPoint [ 1 ] = guess [ 1 ] + alpha * r [ 1 ];
            newPoint [ 2 ] = guess [ 2 ] + alpha * r [ 2 ];

            guess [ 0 ] = newPoint [ 0 ];
            guess [ 1 ] = newPoint [ 1 ];
            guess [ 2 ] = newPoint [ 2 ];

            r [ 0 ] = B [ 0 ] - ( A [ 0 ] [ 0 ] * guess [ 0 ] + A [ 0 ] [ 1 ] * guess [ 1 ] + A [ 0 ] [ 2 ] * guess [ 2 ] );
            r [ 1 ] = B [ 1 ] - ( A [ 1 ] [ 0 ] * guess [ 0 ] + A [ 1 ] [ 1 ] * guess [ 1 ] + A [ 1 ] [ 2 ] * guess [ 2 ] );
            r [ 2 ] = B [ 2 ] - ( A [ 2 ] [ 0 ] * guess [ 0 ] + A [ 2 ] [ 1 ] * guess [ 1 ] + A [ 2 ] [ 2 ] * guess [ 2 ] );

            delta = r [ 0 ] * r [ 0 ] + r [ 1 ] * r [ 1 ] + r [ 2 ] * r [ 2 ];

            i++;
        }

        if ( guess [ 0 ] >= box.min()[0] && guess [ 0 ] <= box.max()[0] &&
            guess [ 1 ] >= box.min()[1] && guess [ 1 ] <= box.max()[1] &&
            guess [ 2 ] >= box.min()[2] && guess [ 2 ] <= box.max()[2] )
        {
            return;
        }
    } // method 2 or 0

    if ( method == 0 || method == 1 ) {
        c = A [ 0 ] [ 0 ] + A [ 1 ] [ 1 ] + A [ 2 ] [ 2 ];
        if ( c == 0 )
            return;

        c = ( 0.75f / c );

        guess [ 0 ] = store [ 0 ];
        guess [ 1 ] = store [ 1 ];
        guess [ 2 ] = store [ 2 ];

        r [ 0 ] = B [ 0 ] - ( A [ 0 ] [ 0 ] * guess [ 0 ] + A [ 0 ] [ 1 ] * guess [ 1 ] + A [ 0 ] [ 2 ] * guess [ 2 ] );
        r [ 1 ] = B [ 1 ] - ( A [ 1 ] [ 0 ] * guess [ 0 ] + A [ 1 ] [ 1 ] * guess [ 1 ] + A [ 1 ] [ 2 ] * guess [ 2 ] );
        r [ 2 ] = B [ 2 ] - ( A [ 2 ] [ 0 ] * guess [ 0 ] + A [ 2 ] [ 1 ] * guess [ 1 ] + A [ 2 ] [ 2 ] * guess [ 2 ] );

        for ( i = 0; i < n; i++ ) {
            guess [ 0 ] = guess [ 0 ] + c * r [ 0 ];
            guess [ 1 ] = guess [ 1 ] + c * r [ 1 ];
            guess [ 2 ] = guess [ 2 ] + c * r [ 2 ];

            r [ 0 ] = B [ 0 ] - ( A [ 0 ] [ 0 ] * guess [ 0 ] + A [ 0 ] [ 1 ] * guess [ 1 ] + A [ 0 ] [ 2 ] * guess [ 2 ] );
            r [ 1 ] = B [ 1 ] - ( A [ 1 ] [ 0 ] * guess [ 0 ] + A [ 1 ] [ 1 ] * guess [ 1 ] + A [ 1 ] [ 2 ] * guess [ 2 ] );
            r [ 2 ] = B [ 2 ] - ( A [ 2 ] [ 0 ] * guess [ 0 ] + A [ 2 ] [ 1 ] * guess [ 1 ] + A [ 2 ] [ 2 ] * guess [ 2 ] );
        }
    }
/*
    if ( guess [ 0 ] > store [ 0 ] + 1 || guess [ 0 ] < store [ 0 ] - 1 ||
        guess [ 1 ] > store [ 1 ] + 1 || guess [ 1 ] < store [ 1 ] - 1 ||
        guess [ 2 ] > store [ 2 ] + 1 || guess [ 2 ] < store [ 2 ] - 1 )
    {
        printf ( "water let point go from %f,%f,%f to %f,%f,%f\n",
            store [ 0 ], store [ 1 ], store [ 2 ], guess [ 0 ], guess [ 1 ], guess [ 2 ] );
        printf ( "A is %f,%f,%f %f,%f,%f %f,%f,%f\n", A [ 0 ] [ 0 ], A [ 0 ] [ 1 ], A [ 0 ] [ 2 ],
            A [ 1 ] [ 0 ] , A [ 1 ] [ 1 ], A [ 1 ] [ 2 ], A [ 2 ] [ 0 ], A [ 2 ] [ 1 ], A [ 2 ] [ 2 ] );
        printf ( "B is %f,%f,%f\n", B [ 0 ], B [ 1 ], B [ 2 ] );
        printf ( "bounding box is %f,%f,%f to %f,%f,%f\n",
            box.min().x, box.min().y, box.min().z, box.max().x, box.max().y, box.max().z );
    }
*/
}

float calcPoint ( float halfA[], float b[], float btb, float midpoint[], float rvalue[], const expressive::core::AABBox &box, float *mat )
{
    float newB [ 3 ];
    float a [ 3 ] [ 3 ];
    float inv [ 3 ] [ 3 ];
    float w [ 3 ];
    float u [ 3 ] [ 3 ];

    a [ 0 ] [ 0 ] = halfA [ 0 ];
    a [ 0 ] [ 1 ] = halfA [ 1 ];
    a [ 0 ] [ 2 ] = halfA [ 2 ];
    a [ 1 ] [ 1 ] = halfA [ 3 ];
    a [ 1 ] [ 2 ] = halfA [ 4 ];
    a [ 1 ] [ 0 ] = halfA [ 1 ];
    a [ 2 ] [ 0 ] = halfA [ 2 ];
    a [ 2 ] [ 1 ] = halfA [ 4 ];
    a [ 2 ] [ 2 ] = halfA [ 5 ];

    switch ( method ) // by default method = 3
    {
    case 0:
    case 1:
    case 2:
        rvalue [ 0 ] = midpoint [ 0 ];
        rvalue [ 1 ] = midpoint [ 1 ];
        rvalue [ 2 ] = midpoint [ 2 ];

        descent ( a, b, rvalue, box );
        return calcError ( a, b, btb, rvalue );
        break;
    case 3:
        matInverse( a, midpoint, inv, w, u );
        newB [ 0 ] = b [ 0 ] - a [ 0 ] [ 0 ] * midpoint [ 0 ] - a [ 0 ] [ 1 ] * midpoint [ 1 ] - a [ 0 ] [ 2 ] * midpoint [ 2 ];
        newB [ 1 ] = b [ 1 ] - a [ 1 ] [ 0 ] * midpoint [ 0 ] - a [ 1 ] [ 1 ] * midpoint [ 1 ] - a [ 1 ] [ 2 ] * midpoint [ 2 ];
        newB [ 2 ] = b [ 2 ] - a [ 2 ] [ 0 ] * midpoint [ 0 ] - a [ 2 ] [ 1 ] * midpoint [ 1 ] - a [ 2 ] [ 2 ] * midpoint [ 2 ];
        rvalue [ 0 ] = inv [ 0 ] [ 0 ] * newB [ 0 ] + inv [ 1 ] [ 0 ] * newB [ 1 ] + inv [ 2 ] [ 0 ] * newB [ 2 ] + midpoint [ 0 ];
        rvalue [ 1 ] = inv [ 0 ] [ 1 ] * newB [ 0 ] + inv [ 1 ] [ 1 ] * newB [ 1 ] + inv [ 2 ] [ 1 ] * newB [ 2 ] + midpoint [ 1 ];
        rvalue [ 2 ] = inv [ 0 ] [ 2 ] * newB [ 0 ] + inv [ 1 ] [ 2 ] * newB [ 1 ] + inv [ 2 ] [ 2 ] * newB [ 2 ] + midpoint [ 2 ];
        return calcError ( a, b, btb, rvalue );
        break;
    case 4:
        method = 3;
        calcPoint ( halfA, b, btb, midpoint, rvalue, box, mat );
        method = 4;
/*
        int rank;
        float eqs [ 4 ] [ 4 ];

        // form the square matrix
        eqs [ 0 ] [ 0 ] = mat [ 0 ];
        eqs [ 0 ] [ 1 ] = mat [ 1 ];
        eqs [ 0 ] [ 2 ] = mat [ 2 ];
        eqs [ 0 ] [ 3 ] = mat [ 3 ];
        eqs [ 1 ] [ 1 ] = mat [ 4 ];
        eqs [ 1 ] [ 2 ] = mat [ 5 ];
        eqs [ 1 ] [ 3 ] = mat [ 6 ];
        eqs [ 2 ] [ 2 ] = mat [ 7 ];
        eqs [ 2 ] [ 3 ] = mat [ 8 ];
        eqs [ 3 ] [ 3 ] = mat [ 9 ];
        eqs [ 1 ] [ 0 ] = eqs [ 2 ] [ 0 ] = eqs [ 2 ] [ 1 ] = eqs [ 3 ] [ 0 ] = eqs [ 3 ] [ 1 ] = eqs [ 3 ] [ 2 ] = 0;

        // compute the new QR decomposition and rank
        rank = qr ( eqs );

        method = 2;
        calcPoint ( halfA, b, btb, midpoint, rvalue, box, mat );
        method = 4;
*/
/*
        if ( rank == 0 )
        {
            // it's zero, no equations
            rvalue [ 0 ] = midpoint [ 0 ];
            rvalue [ 1 ] = midpoint [ 1 ];
            rvalue [ 2 ] = midpoint [ 2 ];
        }
        else
        {
            if ( rank == 1 )
            {
                // one equation, it's a plane
                float temp = ( eqs [ 0 ] [ 0 ] * midpoint [ 0 ] + eqs [ 0 ] [ 1 ] * midpoint [ 1 ] + eqs [ 0 ] [ 2 ] * midpoint [ 2 ] - eqs [ 0 ] [ 3 ] ) /
                            ( eqs [ 0 ] [ 0 ] * eqs [ 0 ] [ 0 ] + eqs [ 0 ] [ 1 ] * eqs [ 0 ] [ 1 ] + eqs [ 0 ] [ 2 ] * eqs [ 0 ] [ 2 ] );

                rvalue [ 0 ] = midpoint [ 0 ] - temp * eqs [ 0 ] [ 0 ];
                rvalue [ 1 ] = midpoint [ 1 ] - temp * eqs [ 0 ] [ 1 ];
                rvalue [ 2 ] = midpoint [ 2 ] - temp * eqs [ 0 ] [ 2 ];
            }
            else
            {
                if ( rank == 2 )
                {
                    // two equations, it's a line
                    float a, b, c, d, e, f, g;

                    // reduce back to upper triangular
                    qr ( eqs, 2, 0.000001f );

                    a = eqs [ 0 ] [ 0 ];
                    b = eqs [ 0 ] [ 1 ];
                    c = eqs [ 0 ] [ 2 ];
                    d = eqs [ 0 ] [ 3 ];
                    e = eqs [ 1 ] [ 1 ];
                    f = eqs [ 1 ] [ 2 ];
                    g = eqs [ 1 ] [ 3 ];

                    // solved using the equations
                    // ax + by + cz = d
                    //      ey + fz = g
                    // minimize (x-px)^2 + (y-py)^2 + (z-pz)^2
                    if ( a > 0.000001f || a < -0.000001f )
                    {
                        if ( e > 0.00000f || e < -0.000001f )
                        {
                            rvalue [ 2 ] = ( -1 * b * d * e * f + ( a * a + b * b ) * g * f + c * e * ( d * e - b * g ) +
                                a * e * ( ( b * f - c * e ) * midpoint [ 0 ] - a * f * midpoint [ 1 ] + a * e * midpoint [ 2 ] ) ) /
                                ( a * a * ( e * e + f * f ) + ( c * e - b * f ) * ( c * e - b * f ) );
                            rvalue [ 1 ] = ( g - f * rvalue [ 2 ] ) / e;
                            rvalue [ 0 ] = ( d - b * rvalue [ 1 ] - c * rvalue [ 2 ] ) / a;
                        }
                        else
                        {
                            // slightly degenerate case where e==0
                            rvalue [ 2 ] = g / f;
                            rvalue [ 1 ] = ( b * d * f - b * c * g - a * b * f * midpoint [ 0 ] + a * a * f * midpoint [ 1 ] ) /
                                ( a * a * f + b * b * f );
                            rvalue [ 0 ] = ( d - b * rvalue [ 1 ] - c * rvalue [ 2 ] ) / a;
                        }
                    }
                    else
                    {
                        // degenerate case where a==0 so e == 0 (upper triangular)

                        rvalue [ 2 ] = g / f;
                        rvalue [ 1 ] = ( d - c * rvalue [ 2 ] ) / b;
                        rvalue [ 0 ] = midpoint [ 0 ];
                    }

                }
                else
                {
                    // must be three equations or more now... solve using back-substitution
                    rvalue [ 2 ] = mat [ 8 ] / mat [ 7 ];
                    rvalue [ 1 ] = ( mat [ 6 ] - mat [ 5 ] * rvalue [ 2 ] ) / mat [ 4 ];
                    rvalue [ 0 ] = ( mat [ 3 ] - mat [ 2 ] * rvalue [ 2 ] - mat [ 1 ] * rvalue [ 1 ] ) / mat [ 0 ];
                }
            }
        }
*/

        float ret;
        float tmp;

        ret = mat [ 9 ] * mat [ 9 ];

        tmp = mat [ 0 ] * rvalue [ 0 ] + mat [ 1 ] * rvalue [ 1 ] + mat [ 2 ] * rvalue [ 2 ] - mat [ 3 ];
        ret += tmp * tmp;

        tmp = mat [ 4 ] * rvalue [ 1 ] + mat [ 5 ] * rvalue [ 2 ] - mat [ 6 ];
        ret += tmp * tmp;

        tmp = mat [ 7 ] * rvalue [ 2 ] - mat [ 8 ];
        ret += tmp * tmp;

        return ret;

        break;
    case 5: // do nothing, return midpoint
        rvalue [ 0 ] = midpoint [ 0 ];
        rvalue [ 1 ] = midpoint [ 1 ];
        rvalue [ 2 ] = midpoint [ 2 ];

        return calcError ( a, b, btb, rvalue );
    }

    return 0 ;
}
//namespace expressive {
//namespace convol {
//namespace tools {

////----------------------------------------------------------------------------

//#define MAXROWS 12
//#define EPSILON 1e-5

////----------------------------------------------------------------------------

//void QEF::evaluate(
//    double mat[][3], double *vec, int rows,
//    Vector *point)
//{
//    // perform singular value decomposition on matrix mat
//    // into u, v and d.
//    // u is a matrix of rows x 3 (same as mat);
//    // v is a square matrix 3 x 3 (for 3 columns in mat);
//    // d is vector of 3 values representing the diagonal
//    // matrix 3 x 3 (for 3 colums in mat).
//    double u[MAXROWS][3], v[3][3], d[3];
//    computeSVD(mat, u, v, d, rows);

//    // solve linear system given by mat and vec using the
//    // singular value decomposition of mat into u, v and d.
//    if (d[2] < 0.1)
//        d[2] = 0.0;
//    if (d[1] < 0.1)
//        d[1] = 0.0;
//    if (d[0] < 0.1)
//        d[0] = 0.0;

//    double x[3];
//    solveSVD(u, v, d, vec, x, rows);

//    *point = Vector((float)x[0], (float)x[1], (float)x[2]);
//}

////----------------------------------------------------------------------------

//void QEF::computeSVD(
//    double mat[][3],                // matrix (rows x 3)
//    double u[][3],                  // matrix (rows x 3)
//    double v[3][3],                 // matrix (3x3)
//    double d[3],                    // vector (1x3)
//    int rows)
//{
//    memcpy(u, mat, rows * 3 * sizeof(double));

//    double *tau_u = d;
//    double tau_v[2];

//    factorize(u, tau_u, tau_v, rows);

//    unpack(u, v, tau_u, tau_v, rows);

//    diagonalize(u, v, tau_u, tau_v, rows);

//    singularize(u, v, tau_u, rows);
//}

////----------------------------------------------------------------------------

//void QEF::factorize(
//    double mat[][3],                // matrix (rows x 3)
//    double tau_u[3],                // vector, (1x3)
//    double tau_v[2],                // vector, (1x2)
//    int rows)
//{
//    int y;

//    // bidiagonal factorization of (rows x 3) matrix into :-
//    // tau_u, a vector of 1x3 (for 3 columns in the matrix)
//    // tau_v, a vector of 1x2 (one less column than the matrix)

//    for (int i = 0; i < 3; ++i) {

//        // set up a vector to reference into the matrix
//        // from mat(i,i) to mat(m,i), that is, from the
//        // i'th column of the i'th row and down all the way
//        // through that column
//        double *ptrs[MAXROWS];
//        int num_ptrs = rows - i;
//        for (int q = 0; q < num_ptrs; ++q)
//            ptrs[q] = &mat[q + i][i];

//        // perform householder transformation on this vector
//        double tau = factorize_hh(ptrs, num_ptrs);
//        tau_u[i] = tau;

//        // all computations below this point are performed
//        // only for the first two columns:  i=0 or i=1
//        if (i + 1 < 3) {

//            // perform householder transformation on the matrix
//            // mat(i,i+1) to mat(m,n), that is, on the sub-matrix
//            // that begins in the (i+1)'th column of the i'th
//            // row and extends to the end of the matrix at (m,n)
//            if (tau != 0.0) {
//                for (int x = i + 1; x < 3; ++x) {
//                    double wx = mat[i][x];
//                    for (y = i + 1; y < rows; ++y)
//                        wx += mat[y][x] * (*ptrs[y - i]);
//                    double tau_wx = tau * wx;
//                    mat[i][x] -= tau_wx;
//                    for (y = i + 1; y < rows; ++y)
//                        mat[y][x] -= tau_wx * (*ptrs[y - i]);
//                }
//            }

//            // perform householder transformation on i'th row
//            // (remember at this point, i is either 0 or 1)

//            // set up a vector to reference into the matrix
//            // from mat(i,i+1) to mat(i,n), that is, from the
//            // (i+1)'th column of the i'th row and all the way
//            // through to the end of that row
//            ptrs[0] = &mat[i][i + 1];
//            if (i == 0) {
//                ptrs[1] = &mat[i][i + 2];
//                num_ptrs = 2;
//            } else // i == 1
//                num_ptrs = 1;

//            // perform householder transformation on this vector
//            tau = factorize_hh(ptrs, num_ptrs);
//            tau_v[i] = tau;

//            // perform householder transformation on the sub-matrix
//            // mat(i+1,i+1) to mat(m,n), that is, on the sub-matrix
//            // that begins in the (i+1)'th column of the (i+1)'th
//            // row and extends to the end of the matrix at (m,n)
//            if (tau != 0.0) {
//                for (y = i + 1; y < rows; ++y) {
//                    double wy = mat[y][i + 1];
//                    if (i == 0)
//                        wy += mat[y][i + 2] * (*ptrs[1]);
//                    double tau_wy = tau * wy;
//                    mat[y][i + 1] -= tau_wy;
//                    if (i == 0)
//                        mat[y][i + 2] -= tau_wy * (*ptrs[1]);
//                }
//            }

//        }  // if (i + 1 < 3)
//    }
//}

////----------------------------------------------------------------------------

//double QEF::factorize_hh(double *ptrs[], int n)
//{
//    double tau = 0.0;

//    if (n > 1) {
//        double xnorm;
//        if (n == 2)
//            xnorm = fabs(*ptrs[1]);
//        else {
//            double scl = 0.0;
//            double ssq = 1.0;
//            for (int i = 1; i < n; ++i) {
//                double x = fabs(*ptrs[i]);
//                if (x != 0.0) {
//                    if (scl < x) {
//                        ssq = 1.0 + ssq * (scl / x) * (scl / x);
//                        scl = x;
//                    } else
//                        ssq += (x / scl) * (x / scl);
//                }
//            }
//            xnorm = scl * sqrt(ssq);
//        }

//        if (xnorm != 0.0) {
//            double alpha = *ptrs[0];
//            double beta = sqrt(alpha * alpha + xnorm * xnorm);
//            if (alpha >= 0.0)
//                beta = -beta;
//            tau = (beta - alpha) / beta;

//            double scl = 1.0 / (alpha - beta);
//            *ptrs[0] = beta;
//            for (int i = 1; i < n; ++i)
//                *ptrs[i] *= scl;
//        }
//    }

//    return tau;
//}

////----------------------------------------------------------------------------

//void QEF::unpack(
//    double u[][3],                  // matrix (rows x 3)
//    double v[3][3],                 // matrix (3x3)
//    double tau_u[3],                // vector, (1x3)
//    double tau_v[2],                // vector, (1x2)
//    int rows)
//{
//    int i, y;

//    // reset v to the identity matrix
//    v[0][0] = v[1][1] = v[2][2] = 1.0;
//    v[0][1] = v[0][2] = v[1][0] = v[1][2] = v[2][0] = v[2][1] = 0.0;

//    for (i = 1; i >= 0; --i) {
//        double tau = tau_v[i];

//        // perform householder transformation on the sub-matrix
//        // v(i+1,i+1) to v(m,n), that is, on the sub-matrix of v
//        // that begins in the (i+1)'th column of the (i+1)'th row
//        // and extends to the end of the matrix at (m,n).  the
//        // householder vector used to perform this is the vector
//        // from u(i,i+1) to u(i,n)
//        if (tau != 0.0) {
//            for (int x = i + 1; x < 3; ++x) {
//                double wx = v[i + 1][x];
//                for (y = i + 1 + 1; y < 3; ++y)
//                    wx += v[y][x] * u[i][y];
//                double tau_wx = tau * wx;
//                v[i + 1][x] -= tau_wx;
//                for (y = i + 1 + 1; y < 3; ++y)
//                    v[y][x] -= tau_wx * u[i][y];
//            }
//        }
//    }

//    // copy superdiagonal of u into tau_v
//    for (i = 0; i < 2; ++i)
//        tau_v[i] = u[i][i + 1];

//    // below, same idea for u:  householder transformations
//    // and the superdiagonal copy

//    for (i = 2; i >= 0; --i) {
//        // copy superdiagonal of u into tau_u
//        double tau = tau_u[i];
//        tau_u[i] = u[i][i];

//        // perform householder transformation on the sub-matrix
//        // u(i,i) to u(m,n), that is, on the sub-matrix of u that
//        // begins in the i'th column of the i'th row and extends
//        // to the end of the matrix at (m,n).  the householder
//        // vector used to perform this is the i'th column of u,
//        // that is, u(0,i) to u(m,i)
//        if (tau == 0.0) {
//            u[i][i] = 1.0;
//            if (i < 2) {
//                u[i][2] = 0.0;
//                if (i < 1)
//                    u[i][1] = 0.0;
//            }
//            for (y = i + 1; y < rows; ++y)
//                u[y][i] = 0.0;
//        } else {
//            for (int x = i + 1; x < 3; ++x) {
//                double wx = 0.0;
//                for (y = i + 1; y < rows; ++y)
//                    wx += u[y][x] * u[y][i];
//                double tau_wx = tau * wx;
//                u[i][x] = -tau_wx;
//                for (y = i + 1; y < rows; ++y)
//                    u[y][x] -= tau_wx * u[y][i];
//            }
//            for (y = i + 1; y < rows; ++y)
//                u[y][i] = u[y][i] * -tau;
//            u[i][i] = 1.0 - tau;
//        }
//    }
//}

////----------------------------------------------------------------------------

//void QEF::diagonalize(
//    double u[][3],                  // matrix (rows x 3)
//    double v[3][3],                 // matrix (3x3)
//    double tau_u[3],                // vector, (1x3)
//    double tau_v[2],                // vector, (1x2)
//    int rows)
//{
//    int i, j;

//    chop(tau_u, tau_v, 3);

//    // progressively reduce the matrices into diagonal form

//    int b = 3 - 1;
//    while (b > 0) {
//        if (tau_v[b - 1] == 0.0)
//            --b;
//        else {
//            int a = b - 1;
//            while (a > 0 && tau_v[a - 1] != 0.0)
//                --a;
//            int n = b - a + 1;

//            double u1[MAXROWS][3];
//            double v1[3][3];
//            for (j = a; j <= b; ++j) {
//                for (i = 0; i < rows; ++i)
//                    u1[i][j - a] = u[i][j];
//                for (i = 0; i < 3; ++i)
//                    v1[i][j - a] = v[i][j];
//            }

//            qrstep(u1, v1, &tau_u[a], &tau_v[a], rows, n);

//            for (j = a; j <= b; ++j) {
//                for (i = 0; i < rows; ++i)
//                    u[i][j] = u1[i][j - a];
//                for (i = 0; i < 3; ++i)
//                    v[i][j] = v1[i][j - a];
//            }

//            chop(&tau_u[a], &tau_v[a], n);
//        }
//    }
//}

////----------------------------------------------------------------------------

//void QEF::chop(double *a, double *b, int n)
//{
//    double ai = a[0];
//    for (int i = 0; i < n - 1; ++i) {
//        double bi = b[i];
//        double ai1 = a[i + 1];
//        if (fabs(bi) < EPSILON * (fabs(ai) + fabs(ai1)))
//            b[i] = 0.0;
//        ai = ai1;
//    }
//}

////----------------------------------------------------------------------------

//void QEF::qrstep(
//    double u[][3],                  // matrix (rows x cols)
//    double v[][3],                  // matrix (3 x cols)
//    double tau_u[],                 // vector (1 x cols)
//    double tau_v[],                 // vector (1 x cols - 1)
//    int rows, int cols)
//{
//    int i;

//    if (cols == 2) {
//        qrstep_cols2(u, v, tau_u, tau_v, rows);
//        return;
//    }

//    if (cols == 1) {
//    char *bomb = 0;
//    *bomb = 0;
//    }

//    // handle zeros on the diagonal or at its end
//    for (i = 0; i < cols - 1; ++i)
//        if (tau_u[i] == 0.0) {
//            qrstep_middle(u, tau_u, tau_v, rows, cols, i);
//            return;
//        }
//    if (tau_u[cols - 1] == 0.0) {
//        qrstep_end(v, tau_u, tau_v, cols);
//        return;
//    }

//    // perform qr reduction on the diagonal and off-diagonal

//    double mu = qrstep_eigenvalue(tau_u, tau_v, cols);
//    double y = tau_u[0] * tau_u[0] - mu;
//    double z = tau_u[0] * tau_v[0];

//    double ak = 0.0;
//    double bk = 0.0;
//    double zk;
//    double ap = tau_u[0];
//    double bp = tau_v[0];
//    double aq = tau_u[1];
//    double bq = tau_v[1];

//    for (int k = 0; k < cols - 1; ++k) {
//        double c, s;

//        // perform Givens rotation on V

//        computeGivens(y, z, &c, &s);

//        for (i = 0; i < 3; ++i) {
//            double vip = v[i][k];
//            double viq = v[i][k + 1];
//            v[i][k] = vip * c - viq * s;
//            v[i][k + 1] = vip * s + viq * c;
//        }

//        // perform Givens rotation on B

//        double bk1 = bk * c - z * s;
//        double ap1 = ap * c - bp * s;
//        double bp1 = ap * s + bp * c;
//        double zp1 = aq * -s;
//        double aq1 = aq * c;

//        if (k > 0)
//            tau_v[k - 1] = bk1;

//        ak = ap1;
//        bk = bp1;
//        zk = zp1;
//        ap = aq1;

//        if (k < cols - 2)
//            bp = tau_v[k + 1];
//        else
//            bp = 0.0;

//        y = ak;
//        z = zk;

//        // perform Givens rotation on U

//        computeGivens(y, z, &c, &s);

//        for (i = 0; i < rows; ++i) {
//            double uip = u[i][k];
//            double uiq = u[i][k + 1];
//            u[i][k] = uip * c - uiq * s;
//            u[i][k + 1] = uip * s + uiq * c;
//        }

//        // perform Givens rotation on B

//        double ak1 = ak * c - zk * s;
//        bk1 = bk * c - ap * s;
//        double zk1 = bp * -s;

//        ap1 = bk * s + ap * c;
//        bp1 = bp * c;

//        tau_u[k] = ak1;

//        ak = ak1;
//        bk = bk1;
//        zk = zk1;
//        ap = ap1;
//        bp = bp1;

//        if (k < cols - 2)
//            aq = tau_u[k + 2];
//        else
//            aq = 0.0;

//        y = bk;
//        z = zk;
//    }

//    tau_v[cols - 2] = bk;
//    tau_u[cols - 1] = ap;
//}

////----------------------------------------------------------------------------

//void QEF::qrstep_middle(
//    double u[][3],                  // matrix (rows x cols)
//    double tau_u[],                 // vector (1 x cols)
//    double tau_v[],                 // vector (1 x cols - 1)
//    int rows, int cols, int col)
//{
//    double x = tau_v[col];
//    double y = tau_u[col + 1];
//    for (int j = col; j < cols - 1; ++j) {
//        double c, s;

//        // perform Givens rotation on U

//        computeGivens(y, -x, &c, &s);
//        for (int i = 0; i < rows; ++i) {
//            double uip = u[i][col];
//            double uiq = u[i][j + 1];
//            u[i][col] = uip * c - uiq * s;
//            u[i][j + 1] = uip * s + uiq * c;
//        }

//        // perform transposed Givens rotation on B

//        tau_u[j + 1] = x * s + y * c;
//        if (j == col)
//            tau_v[j] = x * c - y * s;

//        if (j < cols - 2) {
//            double z = tau_v[j + 1];
//            tau_v[j + 1] *= c;
//            x = z * -s;
//            y = tau_u[j + 2];
//        }
//    }
//}

////----------------------------------------------------------------------------

//void QEF::qrstep_end(
//    double v[][3],                  // matrix (3 x 3)
//    double tau_u[],                 // vector (1 x 3)
//    double tau_v[],                 // vector (1 x 2)
//    int cols)
//{
//    double x = tau_u[1];
//    double y = tau_v[1];

//    for (int k = 1; k >= 0; --k) {
//        double c, s;

//        // perform Givens rotation on V

//        computeGivens(x, y, &c, &s);

//        for (int i = 0; i < 3; ++i) {
//            double vip = v[i][k];
//            double viq = v[i][2];
//            v[i][k] = vip * c - viq * s;
//            v[i][2] = vip * s + viq * c;
//        }

//        // perform Givens rotation on B

//        tau_u[k] = x * c - y * s;
//        if (k == 1)
//            tau_v[k] = x * s + y * c;
//        if (k > 0) {
//            double z = tau_v[k - 1];
//            tau_v[k - 1] *= c;

//            x = tau_u[k - 1];
//            y = z * s;
//        }
//    }
//}

////----------------------------------------------------------------------------

//double QEF::qrstep_eigenvalue(
//    double tau_u[],                 // vector (1 x 3)
//    double tau_v[],                 // vector (1 x 2)
//    int cols)
//{
//    double ta = tau_u[1] * tau_u[1] + tau_v[0] * tau_v[0];
//    double tb = tau_u[2] * tau_u[2] + tau_v[1] * tau_v[1];
//    double tab = tau_u[1] * tau_v[1];
//    double dt = (ta - tb) / 2.0;
//    double mu;
//    if (dt >= 0.0)
//        mu = tb - (tab * tab) / (dt + sqrt(dt * dt + tab * tab));
//    else
//        mu = tb + (tab * tab) / (sqrt(dt * dt + tab * tab) - dt);
//    return mu;
//}

////----------------------------------------------------------------------------

//void QEF::qrstep_cols2(
//    double u[][3],                  // matrix (rows x 2)
//    double v[][3],                  // matrix (3 x 2)
//    double tau_u[],                 // vector (1 x 2)
//    double tau_v[],                 // vector (1 x 1)
//    int rows)
//{
//    int i;
//    double tmp;

//    // eliminate off-diagonal element in [ 0  tau_v0 ]
//    //                                   [ 0  tau_u1 ]
//    // to make [ tau_u[0]  0 ]
//    //         [ 0         0 ]

//    if (tau_u[0] == 0.0) {
//        double c, s;

//        // perform transposed Givens rotation on B
//        // multiplied by X = [ 0 1 ]
//        //                   [ 1 0 ]

//        computeGivens(tau_v[0], tau_u[1], &c, &s);

//        tau_u[0] = tau_v[0] * c - tau_u[1] * s;
//        tau_v[0] = tau_v[0] * s + tau_u[1] * c;
//        tau_u[1] = 0.0;

//        // perform Givens rotation on U

//        for (i = 0; i < rows; ++i) {
//            double uip = u[i][0];
//            double uiq = u[i][1];
//            u[i][0] = uip * c - uiq * s;
//            u[i][1] = uip * s + uiq * c;
//        }

//        // multiply V by X, effectively swapping first two columns

//        for (i = 0; i < 3; ++i) {
//            tmp = v[i][0];
//            v[i][0] = v[i][1];
//            v[i][1] = tmp;
//        }
//    }

//    // eliminate off-diagonal element in [ tau_u0  tau_v0 ]
//    //                                   [ 0       0      ]

//    else if (tau_u[1] == 0.0) {
//        double c, s;

//        // perform Givens rotation on B

//        computeGivens(tau_u[0], tau_v[0], &c, &s);

//        tau_u[0] = tau_u[0] * c - tau_v[0] * s;
//        tau_v[0] = 0.0;

//        // perform Givens rotation on V

//        for (i = 0; i < 3; ++i) {
//            double vip = v[i][0];
//            double viq = v[i][1];
//            v[i][0] = vip * c - viq * s;
//            v[i][1] = vip * s + viq * c;
//        }
//    }

//    // make colums orthogonal,

//    else {
//        double c, s;

//        // perform Schur rotation on B

//        computeSchur(tau_u[0], tau_v[0], tau_u[1], &c, &s);

//        double a11 = tau_u[0] * c - tau_v[0] * s;
//        double a21 = - tau_u[1] * s;
//        double a12 = tau_u[0] * s + tau_v[0] * c;
//        double a22 = tau_u[1] * c;

//        // perform Schur rotation on V

//        for (i = 0; i < 3; ++i) {
//            double vip = v[i][0];
//            double viq = v[i][1];
//            v[i][0] = vip * c - viq * s;
//            v[i][1] = vip * s + viq * c;
//        }

//        // eliminate off diagonal elements

//        if ((a11 * a11 + a21 * a21) < (a12 * a12 + a22 * a22)) {

//            // multiply B by X

//            tmp = a11;
//            a11 = a12;
//            a12 = tmp;
//            tmp = a21;
//            a21 = a22;
//            a22 = tmp;

//            // multiply V by X, effectively swapping first
//            // two columns

//            for (i = 0; i < 3; ++i) {
//                tmp = v[i][0];
//                v[i][0] = v[i][1];
//                v[i][1] = tmp;
//            }
//        }

//        // perform transposed Givens rotation on B

//        computeGivens(a11, a21, &c, &s);

//        tau_u[0] = a11 * c - a21 * s;
//        tau_v[0] = a12 * c - a22 * s;
//        tau_u[1] = a12 * s + a22 * c;

//        // perform Givens rotation on U

//        for (i = 0; i < rows; ++i) {
//            double uip = u[i][0];
//            double uiq = u[i][1];
//            u[i][0] = uip * c - uiq * s;
//            u[i][1] = uip * s + uiq * c;
//        }
//    }
//}

////----------------------------------------------------------------------------

//void QEF::computeGivens(
//    double a, double b, double *c, double *s)
//{
//    if (b == 0.0) {
//        *c = 1.0;
//        *s = 0.0;
//    } else if (fabs(b) > fabs(a)) {
//        double t = -a / b;
//        double s1 = 1.0 / sqrt(1 + t * t);
//        *s = s1;
//        *c = s1 * t;
//    } else {
//        double t = -b / a;
//        double c1 = 1.0 / sqrt(1 + t * t);
//        *c = c1;
//        *s = c1 * t;
//    }
//}

////----------------------------------------------------------------------------

//void QEF::computeSchur(
//    double a1, double a2, double a3,
//    double *c, double *s)
//{
//    double apq = a1 * a2 * 2.0;

//    if (apq == 0.0) {
//        *c = 1.0;
//        *s = 0.0;
//    } else {
//        double t;
//        double tau = (a2 * a2 + (a3 + a1) * (a3 - a1)) / apq;
//        if (tau >= 0.0)
//            t = 1.0 / (tau + sqrt(1.0 + tau * tau));
//        else
//            t = - 1.0 / (sqrt(1.0 + tau * tau) - tau);
//        *c = 1.0 / sqrt(1.0 + t * t);
//        *s = t * (*c);
//    }
//}

////----------------------------------------------------------------------------

//void QEF::singularize(
//    double u[][3],                  // matrix (rows x 3)
//    double v[3][3],                 // matrix (3x3)
//    double d[3],                    // vector, (1x3)
//    int rows)
//{
//    int i, j, y;

//    // make singularize values positive

//    for (j = 0; j < 3; ++j)
//        if (d[j] < 0.0) {
//            for (i = 0; i < 3; ++i)
//                v[i][j] = -v[i][j];
//            d[j] = -d[j];
//        }

//    // sort singular values in decreasing order

//    for (i = 0; i < 3; ++i) {
//        double d_max = d[i];
//        int i_max = i;
//        for (j = i + 1; j < 3; ++j)
//            if (d[j] > d_max) {
//                d_max = d[j];
//                i_max = j;
//            }

//        if (i_max != i) {
//            // swap eigenvalues
//            double tmp = d[i];
//            d[i] = d[i_max];
//            d[i_max] = tmp;

//            // swap eigenvectors
//            for (y = 0; y < rows; ++y) {
//                tmp = u[y][i];
//                u[y][i] = u[y][i_max];
//                u[y][i_max] = tmp;
//            }
//            for (y = 0; y < 3; ++y) {
//                tmp = v[y][i];
//                v[y][i] = v[y][i_max];
//                v[y][i_max] = tmp;
//            }
//        }
//    }
//}

////----------------------------------------------------------------------------

//void QEF::solveSVD(
//    double u[][3],                  // matrix (rows x 3)
//    double v[3][3],                 // matrix (3x3)
//    double d[3],                    // vector (1x3)
//    double b[],                     // vector (1 x rows)
//    double x[3],                    // vector (1x3)
//    int rows)
//{
//    static double zeroes[3] = { 0.0, 0.0, 0.0 };

//    int i, j;

//    // compute vector w = U^T * b

//    double w[3];
//    memcpy(w, zeroes, sizeof(w));
//    for (i = 0; i < rows; ++i) {
//        if (b[i] != 0.0)
//            for (j = 0; j < 3; ++j)
//                w[j] += b[i] * u[i][j];
//    }

//    // introduce non-zero singular values in d into w

//    for (i = 0; i < 3; ++i) {
//        if (d[i] != 0.0)
//            w[i] /= d[i];
//    }

//    // compute result vector x = V * w

//    for (i = 0; i < 3; ++i) {
//        double tmp = 0.0;
//        for (j = 0; j < 3; ++j)
//            tmp += w[j] * v[i][j];
//        x[i] = tmp;
//    }
//}

//} // namespace tools
//} // namespace convol
//} // namespace expressive
