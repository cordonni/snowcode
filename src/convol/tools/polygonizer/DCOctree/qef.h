/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

//----------------------------------------------------------------------------
// ThreeD Quadric Error Function
// TODO:@todo : add a proper header
//----------------------------------------------------------------------------

/*

  Numerical functions for computing minimizers of a least-squares system
  of equations.

  Copyright (C) 2011 Scott Schaefer

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  (LGPL) as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#ifndef EIGEN_H
#define EIGEN_H

#include <core/geometry/AABBox.h>

#define TOLERANCE 0.0001f

/**
 * Uses a jacobi method to return the eigenvectors and eigenvalues
 * of a 3x3 symmetric matrix.  Note: "a" will be destroyed in this
 * process.  "d" will contain the eigenvalues sorted in order of
 * decreasing modulus and v will contain the corresponding eigenvectors.
 *
 * @param a the 3x3 symmetric matrix to calculate the eigensystem for
 * @param d the variable to hold the eigenvalues
 * @param v the variables to hold the eigenvectors
 */
void jacobi ( float a[][3], float d[], float v[][3] );

/**
 * Inverts a 3x3 symmetric matrix by computing the pseudo-inverse.
 *
 * @param mat the matrix to invert
 * @param midpoint the point to minimize towards
 * @param rvalue the variable to store the pseudo-inverse in
 * @param w the place to store the inverse of the eigenvalues
 * @param u the place to store the eigenvectors
 */
void matInverse ( float mat[][3], float midpoint[], float rvalue[][3], float w[], float u[][3] );

/**
 * Calculates the L2 norm of the residual (the error)
 * (Transpose[A].A).x = Transpose[A].B
 *
 * @param a the matrix Transpose[A].A
 * @param b the matrix Transpose[A].B
 * @param btb the value Transpose[B].B
 * @param point the minimizer found
 *
 * @return the error of the minimizer
 */
float calcError ( float a[][3], float b[], float btb, float point[] );

/**
 * Calculates the normal.  This function is not called and doesn't do
 * anything right now.  It was originally meant to orient the normals
 * correctly that came from the principle eigenvector, but we're flat
 * shading now.
 */
float *calcNormal ( float halfA[], float norm[], float expectedNorm[] );

/**
 * Calculates the minimizer of the given system and returns its error.
 *
 * @param halfA the compressed form of the symmetric matrix Transpose[A].A
 * @param b the matrix Transpose[A].B
 * @param btb the value Transpose[B].B
 * @param midpoint the point to minimize towards
 * @param rvalue the place to store the minimizer
 * @param box the volume bounding the voxel this QEF is for
 *
 * @return the error of the minimizer
 */
float calcPoint ( float halfA[], float b[], float btb, float midpoint[], float rvalue[], const expressive::core::AABBox &, float *mat );


void qr ( float eqs[][4], int num, float *rvalue );
void qr ( float *mat1, float *mat2, float *rvalue );
void qr ( float eqs[][4], int num = 4, float tol = 0.000001f );

int estimateRank ( float *a );

#endif
//#ifndef _THREED_QEF_H
//#define _THREED_QEF_H

//#include <core/CoreRequired.h>

//namespace expressive {
//namespace convol {
//namespace tools {


///**
// * QEF, a class implementing the quadric error function
// *      E[x] = P - Ni . Pi
// *
// * Given at least three points Pi, each with its respective
// * normal vector Ni, that describe at least two planes,
// * the QEF evalulates to the point x.
// */
//class QEF
//{
//public:
//    static void evaluate(
//        double mat[][3], double *vec, int rows,
//        Vector *point);
    
//    // compute svd

//    static void computeSVD(
//        double mat[][3],                // matrix (rows x 3)
//        double u[][3],                  // matrix (rows x 3)
//        double v[3][3],                 // matrix (3x3)
//        double d[3],                    // vector (1x3)
//        int rows);

//    // factorize

//    static void factorize(
//        double mat[][3],                // matrix (rows x 3)
//        double tau_u[3],                // vector (1x3)
//        double tau_v[2],                // vectors, (1x2)
//        int rows);

//    static double factorize_hh(double *ptrs[], int n);

//    // unpack

//    static void unpack(
//        double u[][3],                  // matrix (rows x 3)
//        double v[3][3],                 // matrix (3x3)
//        double tau_u[3],                // vector, (1x3)
//        double tau_v[2],                // vector, (1x2)
//        int rows);

//    // diagonalize

//    static void diagonalize(
//        double u[][3],                  // matrix (rows x 3)
//        double v[3][3],                 // matrix (3x3)
//        double tau_u[3],                // vector, (1x3)
//        double tau_v[2],                // vector, (1x2)
//        int rows);

//    static void chop(double *a, double *b, int n);

//    static void qrstep(
//        double u[][3],                  // matrix (rows x cols)
//        double v[][3],                  // matrix (3 x cols)
//        double tau_u[],                 // vector (1 x cols)
//        double tau_v[],                 // vector (1 x cols - 1)
//        int rows, int cols);

//    static void qrstep_middle(
//        double u[][3],                  // matrix (rows x cols)
//        double tau_u[],                 // vector (1 x cols)
//        double tau_v[],                 // vector (1 x cols - 1)
//        int rows, int cols, int col);

//    static void qrstep_end(
//        double v[][3],                  // matrix (3 x 3)
//        double tau_u[],                 // vector (1 x 3)
//        double tau_v[],                 // vector (1 x 2)
//        int cols);

//    static double qrstep_eigenvalue(
//        double tau_u[],                 // vector (1 x 3)
//        double tau_v[],                 // vector (1 x 2)
//        int cols);

//    static void qrstep_cols2(
//        double u[][3],                  // matrix (rows x 2)
//        double v[][3],                  // matrix (3 x 2)
//        double tau_u[],                 // vector (1 x 2)
//        double tau_v[],                 // vector (1 x 1)
//        int rows);

//    static void computeGivens(
//        double a, double b, double *c, double *s);

//    static void computeSchur(
//        double a1, double a2, double a3,
//        double *c, double *s);

//    // singularize

//    static void singularize(
//        double u[][3],                  // matrix (rows x 3)
//        double v[3][3],                 // matrix (3x3)
//        double d[3],                    // vector, (1x3)
//        int rows);

//    // solve svd
//    static void solveSVD(
//        double u[][3],                  // matrix (rows x 3)
//        double v[3][3],                 // matrix (3x3)
//        double d[3],                    // vector (1x3)
//        double b[],                     // vector (1 x rows)
//        double x[3],                    // vector (1x3)
//        int rows);
//};


//} // namespace tools
//} // namespace convol
//} // namespace expressive

//#endif // _THREED_QEF_H
