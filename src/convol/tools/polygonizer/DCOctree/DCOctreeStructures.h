/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: DCOctree.h

   Language: C++

   License: Convol Licence

   \author: Amaury Jung
   E-Mail: amaury.jung@inria.fr


   Platform Dependencies: None
*/

#ifndef CONVOL_DC_OCTREE_STRUCTURES_H_
#define CONVOL_DC_OCTREE_STRUCTURES_H_

#define DC_OCTREE_HASH_USE_SET


#include <core/geometry/AABBox.h>
#include "ork/core/Logger.h"

#ifdef DC_OCTREE_HASH_USE_SET
#include <set>
#else
#include <algorithm>
#endif

#include <array>
#include <iostream>
#include <list>
#include <cassert>

#ifdef DC_OCTREE_MULTI_THREAD_SUPPORT
///////////// thread //////////
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/bind.hpp>

#include <boost/threadpool.hpp>
#endif


namespace expressive {
namespace convol {
namespace tools {

namespace DCOctreeStructures{

enum NodeOffsetFlag
{
    NO_OFFSET = 13,
    INCREASE_X = 9,
    DECREASE_X = -9,
    INCREASE_Y = 3,
    DECREASE_Y = -3,
    INCREASE_Z = 1,
    DECREASE_Z = -1
};

unsigned char neighbour_node_idx(unsigned char node_idx, unsigned char offset_flags);

bool is_neighbour_node_outside_parent(unsigned char node_idx, unsigned char offset_flags);

const int OCTREE_MAX_DEPTH = 16;
const int HASHBIT = 5;
const int HASHBIT2 = OCTREE_MAX_DEPTH - HASHBIT - 3;
const int HASHSIZE = (unsigned int)(1<<(3*HASHBIT));
const int MASK = ((1<<HASHBIT)-1);

// vertices_map[i][j] = offset in jth dimension of the ith child of an octree node
extern const unsigned int vertices_map[8][3];
// edgevmap[i][0] = first vertex index of the ith edge of a cube
// edgevmap[i][0] = second vertex index of the ith edge of a cube
extern const int edgevmap[12][2];
// node_proc_face_mask[i][0] = first child index of the ith face of the node
// node_proc_face_mask[j][1] = second child index of the ith face of the node
// node_proc_face_mask[j][2] = direction of the ith face normal (x->0,y->1,z->2)
extern const int node_proc_face_mask[12][3];
// 0<=j<4 : node_proc_edge_mask[i][j] = jth child index of the ith edge of the node
// node_proc_edge_mask[i][4] = direction of the ith edge of the node (x->0,y->1,z->2)
extern const int node_proc_edge_mask[6][5];

extern const int face_proc_face_mask[3][4][2];

extern const int *face_proc_edge_fathers_mask[2];
extern const int face_proc_edge_mask[3][4][6];

extern const int edge_proc_edge_mask[3][2][4];

extern const int add_face_proc_edge_mask[3][4];
extern const int add_face_proc_vertices_mask[4];

extern const bool subdivide_proc_shared_vertex_mask[27];
extern const int subdivide_proc_inherited_vertex_mask[8];
extern const int subdivide_proc_childs_vertex_mask[8][8];

// give a child cell index according to its left bottom near corner position
// idx = child_position_mask[(position[0]%length)>=length/2 ? 1 : 0]
//                          [(position[1]%length)>=length/2 ? 1 : 0]
//                          [(position[2]%length)>=length/2 ? 1 : 0]
extern const int child_position_mask[2][2][2];

int HASH( int i, int j,int k);

template<typename T>
class GridHashTableVertexT
{
public:
    GridHashTableVertexT(unsigned int _i, unsigned int _j, unsigned int _k, const T &_t) :
        i(_i), j(_j), k(_k), value(_t){}

    bool operator==(const GridHashTableVertexT &g) const
    {
        return (i==g.i && j==g.j && k==g.k);
    }

    bool operator<(const GridHashTableVertexT &g) const
    {
        return i<g.i || (i==g.i && (j<g.j || (j==g.j && k<g.k)));
    }

    unsigned int i,j,k;
    T value;
};

#ifdef DC_OCTREE_MULTI_THREAD_SUPPORT

using boost::unique_lock;
using boost::mutex;

template< typename T>
class GridHashTableT
{
public:
    typedef T value_type;
    typedef GridHashTableVertexT<T> Vertex;

#ifdef DC_OCTREE_HASH_USE_SET
    typedef std::set<Vertex> VList;
#else
    typedef std::list<Vertex> VList;
#endif

    bool insert(unsigned int i, unsigned int j, unsigned int k, const T &t)
    {
        return (multi_thread_support ? insert_mt(i,j,k,t) : insert_st(i,j,k,t));
    }

    bool get(unsigned int i, unsigned int j, unsigned int k, T &t)
    {
        return (multi_thread_support ? get_mt(i,j,k,t)    : get_st(i,j,k,t));
    }

    /*
        Warning : NOT THREAD SAFE!!!
    */
    void clear()
    {
        for (int i=0; i<HASHSIZE; ++i)
            table[i].clear();
    }

    void enable_multi_thread_support(bool enable)
    {
        multi_thread_support = enable;
    }
private :
    bool insert_st(unsigned int i, unsigned int j, unsigned int k, const T &t)
    {
        const int idx=HASH(i,j,k);
#ifdef DC_OCTREE_HASH_USE_SET
        return table[idx].insert(Vertex(i,j,k,t)).second;
#else
        Vertex v(i,j,k,t);
        if (std::find(table[idx].begin(),table[idx].end(),v) != table[idx].end())
            return false;
        table[idx].push_front(v);
        return true;
#endif

    }

    bool insert_mt(unsigned int i, unsigned int j, unsigned int k, const T &t)
    {
        const int idx=HASH(i,j,k);
        unique_lock<mutex> lock(mutexes[idx]);
#ifdef DC_OCTREE_HASH_USE_SET
        return table[idx].insert(Vertex(i,j,k,t)).second;
#else
        Vertex v(i,j,k,t);
        if (std::find(table[idx].begin(),table[idx].end(),v) != table[idx].end())
            return false;
        table[idx].push_front(v);
        return true;
#endif

    }

    bool get_mt(unsigned int i, unsigned int j, unsigned int k, T &t)
    {
        const int idx=HASH(i,j,k);

        Vertex v(i,j,k,T());
        unique_lock<mutex> lock(mutexes[idx]);
#ifdef DC_OCTREE_HASH_USE_SET
        typename VList::const_iterator q = table[idx].find(v);
#else
        typename VList::const_iterator q = std::find(table[idx].begin(),table[idx].end(),v);
#endif
        if (q==table[idx].end())
        {
            return false;
        }
        else
        {
            t = q->value;
            return true;
        }

    }

    bool get_st(unsigned int i, unsigned int j, unsigned int k, T &t)
    {
        const int idx=HASH(i,j,k);

        Vertex v(i,j,k,T());
#ifdef DC_OCTREE_HASH_USE_SET
        typename VList::const_iterator q = table[idx].find(v);
#else
        typename VList::const_iterator q = std::find(table[idx].begin(),table[idx].end(),v);
#endif
        if (q==table[idx].end())
        {
            return false;
        }
        else
        {
            t = q->value;
            return true;
        }
    }


private:
    std::array<VList, HASHSIZE> table;
    std::array<mutex, HASHSIZE> mutexes;
    bool multi_thread_support;
}; // End class GridHashTableT declaration

#else

template< typename T>
class GridHashTableT
{
public:
    typedef T value_type;
    typedef GridHashTableVertexT<T> Vertex;

#ifdef DC_OCTREE_HASH_USE_SET
    typedef std::set<Vertex> VList;
#else
    typedef std::list<Vertex> VList;
#endif

    /*
        Try to insert item t on the (i,j,k) grid position.
        If the position is already used, it returns false
        (and the item in the grid is updated) otherwise,
        the item is added on the position and the function
        returns true
    */
    bool insert(unsigned int i, unsigned int j, unsigned int k, const T &t)
    {
        const int idx=HASH(i,j,k);
#ifdef DC_OCTREE_HASH_USE_SET
        return table[idx].insert(Vertex(i,j,k,t)).second;
#else
        Vertex v(i,j,k,t);
        if (std::find(table[idx].begin(),table[idx].end(),v) != table[idx].end())
            return false;
        table[idx].push_front(v);
        return true;
#endif

    }

    /*
        Try to get the item on the (i,j,k) grid position.
        If the position is not used, the function return false
        and t is undefined otherwise, t contains the value of
        the item and the function returns true
    */
    bool get(unsigned int i, unsigned int j, unsigned int k, T &t)
    {
        const int idx=HASH(i,j,k);

        Vertex v(i,j,k,T());
#ifdef DC_OCTREE_HASH_USE_SET
        typename VList::const_iterator q = table[idx].find(v);
#else
        typename VList::const_iterator q = std::find(table[idx].begin(),table[idx].end(),v);
#endif
        if (q==table[idx].end())
        {
            return false;
        }
        else
        {
            t = q->value;
            return true;
        }

    }

    /*
        Clear the hashtable
    */
    void clear()
    {
        for (int i=0; i<HASHSIZE; ++i)
            table[i].clear();
    }

    void display_stats()
    {
        unsigned int non_empty=0;
        unsigned int entries=0;
        unsigned int max_entries=0;
        for (int i=0; i<HASHSIZE; ++i)
            if (!table[i].empty())
            {
                non_empty++;
                entries+=table[i].size();
                if (table[i].size()>max_entries)
                    max_entries=table[i].size();
            }


        ork::Logger::DEBUG_LOGGER->logf("CONVOL", "[DC Octree HashTable ] : %d/%d keys used", non_empty, table.size());
        if (entries>0) {
            ork::Logger::DEBUG_LOGGER->logf("CONVOL", "[DC Octree HashTable ] : Collision max rate : %f", 100.*max_entries/static_cast<double>(entries));
        }
    }

private:
    std::array<VList, HASHSIZE> table;
}; // End class GridHashTableT declaration

#endif // DC_OCTREE_MULTI_THREAD_SUPPORT

template<typename DCOctree>
struct NodePositionInfosT
{
    unsigned int position[3];
    unsigned int length;

    typedef typename DCOctree::DCOctreeNode DCOctreeNode;
    typedef typename DCOctree::Traits Traits;

    NodePositionInfosT(){}

    // Create root positions infos
    NodePositionInfosT(DCOctree const  *tree) : length(1 << tree->max_depth)
    {
        position[0] = position[1] = position[2] = 0;
    }

    NodePositionInfosT(const NodePositionInfosT &rhs) : length(rhs.length)
    {
        std::copy(rhs.position,rhs.position+3,position);
    }

    core::AABBox getAxisBoundingBox(const DCOctree *tree) const
    {
        return tree->GetWorldBoundingBox(position,length);
    }

    NodePositionInfosT child(unsigned char child_idx) const
    {
        NodePositionInfosT ans;
        assert(child_idx<8);
        ans.length = length/2;
        ans.position[0] = position[0] + ans.length*vertices_map[child_idx][0];
        ans.position[1] = position[1] + ans.length*vertices_map[child_idx][1];
        ans.position[2] = position[2] + ans.length*vertices_map[child_idx][2];
        return ans;
    }

    NodePositionInfosT parent() const
    {
        NodePositionInfosT ans;
        ans.length = length*2;
        ans.position[0] = position[0] - (position[0]%ans.length);
        ans.position[1] = position[1] - (position[1]%ans.length);
        ans.position[2] = position[2] - (position[2]%ans.length);
        return ans;
    }
};


template<typename DCOctree>
class DCOctreeVisitorT
{
public:
    typedef typename DCOctree::DCOctreeNode DCOctreeNode;
    typedef typename DCOctree::Traits Traits;

    typedef NodePositionInfosT<DCOctree> NodePositionInfos;


    struct FacePositionInfos
    {
        unsigned int position[3];
        unsigned int length[2];
        unsigned char dir;
    };

    struct EdgePositionInfos
    {
        unsigned int position[3];
        unsigned int length[4];
        unsigned char dir;
    };

    virtual ~DCOctreeVisitorT() {}

    virtual void VisitTree(DCOctree *tree)
    {
        VisitNode(tree->root, NodePositionInfos(tree));
    }

    virtual void VisitNode(DCOctreeNode *node, const NodePositionInfos &infos) = 0;

    static std::pair<DCOctreeNode *, NodePositionInfos> GetNeighbour(DCOctreeNode *node,
             const NodePositionInfos &infos, unsigned char offset_flags)
    {
        DCOctreeNode *current_node = node;
        NodePositionInfos current_infos(infos);
        unsigned char current_idx;
        std::list<unsigned char> child_idx_stack;

        do
        {
            DCOctreeNode *parent = current_node->father;
            if (parent == NULL)
                return std::make_pair(parent,current_infos);
            current_idx = std::find(parent->childs.begin(), parent->childs.end(), current_node) - parent->childs.begin();
            current_node = parent;
            current_infos = current_infos.parent();
            child_idx_stack.push_front(neighbour_node_idx(current_idx,offset_flags));

        } while (is_neighbour_node_outside_parent(current_idx, offset_flags));

        while (!current_node->is_leaf() && !child_idx_stack.empty())
        {
            current_idx = child_idx_stack.front();
            child_idx_stack.pop_front();
            current_node = current_node->childs[current_idx];
            current_infos = current_infos.child(current_idx);
        }

        return std::make_pair(current_node, current_infos);
    }
protected:
    virtual void VisitFace(DCOctreeNode *nodes[2], const FacePositionInfos &infos) = 0;
    virtual void VisitEdge(DCOctreeNode *nodes[4], const EdgePositionInfos &infos) = 0;

    void NodeNodeRecursion(DCOctreeNode *node, const NodePositionInfos &infos)
    {
        NodePositionInfos child_info;
        child_info.length = infos.length/2;

        // 8 node calls
        for (unsigned int i=0; i<8; ++i)
        {
            child_info.position[0] = infos.position[0] + child_info.length*vertices_map[i][0];
            child_info.position[1] = infos.position[1] + child_info.length*vertices_map[i][1];
            child_info.position[2] = infos.position[2] + child_info.length*vertices_map[i][2];
            VisitNode(node->childs[i], child_info);
        }
    }
    void NodeFaceRecursion(DCOctreeNode *node, const NodePositionInfos &infos)
    {
        DCOctreeNode *childs[2];
        FacePositionInfos childs_info;
        const unsigned int length2 = infos.length/2;
        childs_info.length[0] = childs_info.length[1] = length2;

        // 12 face calls
        for (unsigned int i=0; i<12; ++i)
        {
            {
                const unsigned int lbn_child_idx = node_proc_face_mask[i][0];
                childs_info.position[0] = infos.position[0] + length2*vertices_map[lbn_child_idx][0];
                childs_info.position[1] = infos.position[1] + length2*vertices_map[lbn_child_idx][1];
                childs_info.position[2] = infos.position[2] + length2*vertices_map[lbn_child_idx][2];
                childs[0] = node->childs[lbn_child_idx];
                childs[1] = node->childs[node_proc_face_mask[i][1]];
                childs_info.dir = node_proc_face_mask[i][2];
            }
            VisitFace(childs, childs_info);
        }
    }
    void NodeEdgeRecursion(DCOctreeNode *node, const NodePositionInfos &infos)
    {
        DCOctreeNode *childs[4];
        EdgePositionInfos childs_info;
        const unsigned int length2 = infos.length/2;
        childs_info.length[0] = childs_info.length[1] = childs_info.length[2]
                              = childs_info.length[3] = length2;

        // 6 edges calls
        for (unsigned int i=0; i<6; ++i)
        {
            {
                const unsigned int lbn_child_idx = node_proc_edge_mask[i][0];
                childs_info.position[0] = infos.position[0] + length2*vertices_map[lbn_child_idx][0];
                childs_info.position[1] = infos.position[1] + length2*vertices_map[lbn_child_idx][1];
                childs_info.position[2] = infos.position[2] + length2*vertices_map[lbn_child_idx][2];
                childs_info.dir = node_proc_edge_mask[i][4];
                childs[0] = node->childs[lbn_child_idx];
                childs[1] = node->childs[node_proc_edge_mask[i][1]];
                childs[2] = node->childs[node_proc_edge_mask[i][2]];
                childs[3] = node->childs[node_proc_edge_mask[i][3]];
            }
            VisitEdge(childs, childs_info);
        }
    }

    void FaceFaceRecursion(DCOctreeNode *nodes[2], const FacePositionInfos &infos)
    {
        if (nodes[0]->is_leaf() && nodes[1]->is_leaf())
                return;

        DCOctreeNode *childs[2];
        FacePositionInfos childs_info;
        childs_info.dir = infos.dir;
        childs_info.length[0] = (nodes[0]->is_leaf() ? infos.length[0] : infos.length[0]/2);
        childs_info.length[1] = (nodes[1]->is_leaf() ? infos.length[1] : infos.length[1]/2);
        const unsigned int minlength2 = std::min(childs_info.length[0], childs_info.length[1]);

        //  4 faces calls
        for (unsigned int i=0; i<4; ++i)
        {
            const unsigned int child0idx = face_proc_face_mask[infos.dir][i][0];
            childs_info.position[0] = infos.position[0] + minlength2*vertices_map[child0idx][0];
            childs_info.position[1] = infos.position[1] + minlength2*vertices_map[child0idx][1];
            childs_info.position[2] = infos.position[2] + minlength2*vertices_map[child0idx][2];

            childs[0] = (nodes[0]->is_leaf() ? nodes[0] : nodes[0]->childs[ face_proc_face_mask[infos.dir][i][0] ]);
            childs[1] = (nodes[1]->is_leaf() ? nodes[1] : nodes[1]->childs[ face_proc_face_mask[infos.dir][i][1] ]);

            VisitFace(childs, childs_info);
        }
    }
    void FaceEdgeRecursion(DCOctreeNode *nodes[2], const FacePositionInfos &infos)
    {
        DCOctreeNode *childs[4];
        EdgePositionInfos childs_info;
        childs_info.dir = infos.dir;

        // 4 edge calls
        for (unsigned int i=0; i<4; ++i)
        {
            {
                int const *fathers = face_proc_edge_fathers_mask[ face_proc_edge_mask[infos.dir][i][4] ];
                for (unsigned int j=0; j<4; ++j)
                {
                    const int k = fathers[j];
                    if (nodes[k]->is_leaf())
                    {
                        childs[j] = nodes[k];
                        childs_info.length[j] = infos.length[k];
                    }
                    else
                    {
                        childs[j] = nodes[k]->childs[ face_proc_edge_mask[infos.dir][i][j]];
                        childs_info.length[j] = infos.length[k]/2;
                    }
                }
            }
            VisitEdge(childs, childs_info);
        }
    }

    void EdgeEdgeRecursion(DCOctreeNode *nodes[4], const EdgePositionInfos &infos)
    {
        DCOctreeNode *childs[4];
        EdgePositionInfos childs_info;
        childs_info.dir = infos.dir;

        // 2 edge calls
        for (unsigned int i=0; i<2; ++i)
        {
            for (unsigned int j=0; j<4; ++j)
                if (nodes[j]->isleaf())
                {
                    childs[j] = nodes[j];
                    childs_info.length[j] = infos.length[j];
                }
                else
                {
                    childs[j] = nodes[j]->childs[ edge_proc_edge_mask[infos.dir][i][j] ];
                    childs_info.length[j] = infos.length[j]/2;
                }

            VisitEdge(childs, childs_info);
        }
    }
};

} // Close namespace DCOctreeStructures
} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive

#endif
