/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: DCOctreeNode.h

   Language: C++

   License: Convol Licence

   \author: Amaury Jung
   E-Mail: amaury.jung@inria.fr


   Platform Dependencies: None
*/


#ifndef CONVOL_DC_OCTREE_NODE_H_
#define CONVOL_DC_OCTREE_NODE_H_

#define MAX_DEPTH

// core dependencies
#include<core/geometry/AABBox.h>

// Convol dependencies
#include<convol/ConvolRequired.h>
#include<convol/ImplicitSurface.h>
    // Tools
//    #include<convol/include/tools/SimpleAxisGridT.h>
    #include<convol/tools/ConvergenceTools.h>

// Convol-Tools dependencies
    // Polygonizer
    #include<convol/tools/polygonizer/DCOctree/DCOctree.h>
 
#include "ork/core/Logger.h"

#include <array>
 
// std dependencies
#include <time.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <limits>

#include <chrono>


//////////////////////////////////////////////////////////////////////////
///TODO:@todo : temporary
//TODO:@todo : following file should have a proper header since i get it from
//                  http://www.sandboxie.com/misc/isosurf/isosurfaces.html
#include <convol/tools/polygonizer/DCOctree/qef.h>
////////////////////////////////////////////////////////////////////////////

namespace expressive {
namespace convol {
namespace tools {

using namespace DCOctreeStructures;
using std::array;

template<typename Scalar>
int sign(Scalar x){
    if (x>0)
        return 1;
    else if (x<0)
        return -1;
    else
        return 0;
}

/////////////////////////////////////////////////////////////////////////////////
//// DCOctreeNode : Common (Single/Multi Thread Versions) Methods Definitions ///
/////////////////////////////////////////////////////////////////////////////////

template< typename TTriMesh, typename TVertexProcessor>
class DCOctreeNodeT
{
public:
    typedef TVertexProcessor VertexProcessor;

    typedef DCOctreeT<TTriMesh, TVertexProcessor> DCOctree;
    typedef DCOctreeNodeT<TTriMesh, TVertexProcessor> DCOctreeNode;

//    typedef SimpleAxisGridT<Traits,Scalar> SimpleAxisGrid;

    typedef TTriMesh TriMesh;
    typedef typename TriMesh::VertexHandle Vertex;

    DCOctreeNodeT(DCOctreeNode *_father = NULL) : father(_father), to_tesselate(false)
    {
        childs.fill(NULL);
    }

    ~DCOctreeNodeT()
    {
        if (!is_leaf())
            for (unsigned int i=0; i<8; ++i)
                delete childs[i];
    }

////////////////////// COMMON /////////////////////////////////////////////////

    /**
      \brief returns true iff this node is a leaf (eg : a node that may contains
      a surface vertex and which not have children cells
    */
    bool is_leaf() const { return childs[0] == NULL; }

    /**
      \brief put every valid vertices into valid_vertices
    */
    void get_valid_vertices(std::vector<Vertex *> &valid_vertices);

    /**
      \brief invalidates every surface vertices contains in bbox and put every
             valid vertices (which are not inside bbox) on valid_vertices list.
             position and length contains the position and size information of
             this
    */
    void invalidate_vertices(const core::AABBox &bbox, unsigned int position[3],
                             unsigned int length, DCOctree *tree,
                             std::vector<Vertex *> &valid_vertices);

    // return true if at least one surface vertex was found

    /** \brief
      Recursive function that will create faces in tri_mesh according to the
      vertices values of this and its children.
      this->to_tesselate will be false after this function call.
      Depth is the depth in the octree structure
     */
    void node_proc_contour(unsigned int depth, std::shared_ptr<TriMesh> tri_mesh);

    /** \brief
      Recursive function that ensures the surface to be closed and avoids unexpected
      configurations during faces creation
    */
    void node_proc_topology(unsigned int length, const unsigned int position[3],
                            DCOctree *tree);

    template<class PrecisionFunction>
    bool increase_precision_st(unsigned int position[3], unsigned int length,
                            const PrecisionFunction &precision, DCOctree *tree,
                            typename PrecisionFunction::Hint hint = typename PrecisionFunction::Hint()) {
		const unsigned int length2 = length/2;
		unsigned int child_position[3];
		bool precision_increased = false;

		if (this->is_leaf() && this->vertex.is_valid() && length > 1)
		{
            core::AABBox thisbbox = tree->GetWorldBoundingBox(position,length);
			const Scalar dlength = tree->GetWorldLength(length);
			//precision.UpdateHint(thisbbox,hint);

			if (precision.DetailPrecisionLowerThan(thisbbox,dlength,hint))
			{
				tree->tri_mesh_->delete_vertex(this->vertex,false);
				this->vertex.invalidate();
                subdivide(core::AABBox(),position,length,tree);
				for (unsigned int i=0; i<8; ++i)
				{
					child_position[0] = position[0] + length2*vertices_map[i][0];
					child_position[1] = position[1] + length2*vertices_map[i][1];
					child_position[2] = position[2] + length2*vertices_map[i][2];
	#ifdef DC_OCTREE_SIZE_CHECKS
					this->childs[i]->position_[0] = child_position[0];
					this->childs[i]->position_[1] = child_position[1];
					this->childs[i]->position_[2] = child_position[2];
					this->childs[i]->size_ = length2;
	#endif //DC_OCTREE_SIZE_CHECKS
					this->childs[i]->collapse_st(child_position,length2,tree);
				}
				precision_increased = true;
			}
		}
	#ifdef DC_OCTREE_SIZE_CHECKS
		else if (this->is_leaf() && length > 1)
		{
			bool sgn = this->values[0] > 0;
			for (unsigned int i=0; i<8; ++i)
				assert((this->values[i] > 0) == sgn);
		}
	#endif

		if (!this->is_leaf() && !precision_increased)
		{
            core::AABBox thisbbox = tree->GetWorldBoundingBox(position,length);
			precision.UpdateHint(thisbbox,hint);

			for (unsigned int i=0; i<8; ++i)
			{
				child_position[0] = position[0] + length2*vertices_map[i][0];
				child_position[1] = position[1] + length2*vertices_map[i][1];
				child_position[2] = position[2] + length2*vertices_map[i][2];

				precision_increased = this->childs[i]->increase_precision_st(child_position,
									   length2,precision,tree,hint) ||
										precision_increased;
			}
		}

		return this->to_tesselate = precision_increased;
	}

    bool contains_vertex() const;
protected :
    /**
      \brief Clear the node and his childrens recursively (it removes the
      contained valid vertices
      Single Thread version
     */
    void clear_st(std::shared_ptr<TriMesh> tri_mesh);

    /**
      \brief Common subdivide function (called by st and mt recursive_subdivide
        functions).
        It subdivides the current node and computes every childs isovalues
    */
    void subdivide(const core::AABBox &bbox, unsigned int position[3],
                   unsigned int length, DCOctree *tree);

    /**
      \brief Returns if it exists the sub-child of this whose position is position
      and whose length is required_length.
      actual_length is the size of this
      Otherwise, it returns the deepest sub-child of this which contains position
      \warning this should contains "positions"
    */
    DCOctreeNode *find_child_with_length(unsigned int position[3], unsigned int &actual_length,
                                         unsigned int required_length);

    static void face_proc_contour(DCOctreeNode *nodes[2], unsigned int depth[2],
                                  int dir, std::shared_ptr<TriMesh> tri_mesh);

    static void edge_proc_contour(DCOctreeNode *nodes[4], unsigned int depth[4],
                                  int dir, std::shared_ptr<TriMesh> tri_mesh);

    static void add_face(DCOctreeNode *nodes[4], unsigned int depth[4],
                         int dir, std::shared_ptr<TriMesh> tri_mesh);

    static void face_proc_topology(DCOctreeNode *nodes[2], unsigned int lengthes[2],
                                   unsigned int position[3], int dir, DCOctree *tree);

    static void edge_proc_topology(DCOctreeNode *nodes[4], unsigned int lengthes[4],
                                   unsigned int position[3], int dir, DCOctree *tree);

////////////////////////// SINGLE THREAD //////////////////////////////////
public :
    /**
      \brief According to the precision function and to the vertices values (recomputed
      if they are inside bbox), this will be subdivided of collapsed.
      If compute_vertices is false, no vertices values will be computed (no matter
      the bbox) otherwise only vertices inside bbox will be computed.
      position and length contains the position and size informations of this.
      Iff a face is detected and should later be created, the function will return
      true and this->to_tesselate will be equals to true
     */
    template<typename PrecisionFunction>
    bool update_st(const core::AABBox &bbox, unsigned int position[3], unsigned int length,
                const PrecisionFunction &precision, DCOctree *tree,
                typename PrecisionFunction::Hint hint = typename PrecisionFunction::Hint(),
                bool compute_vertices=true, bool topology_subdivision = false) {
		#ifdef DC_OCTREE_SIZE_CHECKS
			this->position_[0] = position[0];
			this->position_[1] = position[1];
			this->position_[2] = position[2];
			this->size_ = length;
		#endif //DC_OCTREE_SIZE_CHECKS

            bool intersects_bbox_vertex = false;

            if (compute_vertices) //this is only used for the highest computed node in the octree (for Reset it is the root)
				for (unsigned int i=0; i<8; ++i)
				{
					const int x=position[0]+vertices_map[i][0]*length;
					const int y=position[1]+vertices_map[i][1]*length;
					const int z=position[2]+vertices_map[i][2]*length;
					Point p = tree->GetWorldPoint(x,y,z);
					if (bbox.Contains(p) &&
						!tree->vertices_table.get(x,y,z,this->values[i]))
					{
						intersects_bbox_vertex=true;
						++(tree->nb_evals);
						this->values[i] = tree->function->EvalMinusIsoValue(p[0], p[1], p[2]);
						tree->vertices_table.insert(x,y,z,this->values[i]);
					}
				}

            // test if there is a crossing of the iso-surface
            bool simple = true;
            int sign = 0;
            for (unsigned int i=0; i<8; ++i){
				if (sign*this->values[i]<0)
					simple = false;
				if (this->values[i]>0)
					sign = 1;
				else if (this->values[i]<0)
					sign = -1;
			}

            core::AABBox thisbbox = tree->GetWorldBoundingBox(position,length);

			if (intersects_bbox_vertex || thisbbox.Intersect(bbox))
			{
				if (sign==0)
					simple=false;
				const Scalar dlength = tree->GetWorldLength(length);

				precision.UpdateHint(thisbbox,hint);

                if(topology_subdivision)
                {
                    //TODO:@todo : what the use of simple, should we test it here ?
//                    if(length > 6/*2*/)
                        return collapse_st_topology(bbox,position, length, precision, tree, hint);
//                    else
//                        return this->to_tesselate = false; // pas sur du tout
                }
                else
                {
                    if ( ( precision.FeaturePrecisionLowerThan(thisbbox, dlength, hint)
                           || (!simple && precision.DetailPrecisionLowerThan(thisbbox,dlength, hint)))
                         && length > 1  && recursive_subdivide_st(bbox,position, length, precision, tree, hint,false)) {
                        return this->to_tesselate = true;
                    } else {
                        //                    return collapse_st(bbox, position, length, tree);
                        return collapse_st_topology(bbox,position, length, precision, tree, hint);
                    }
                }
            }
			else if (this->is_leaf())
			{
                return collapse_st(position, length, tree);
			}
			else
			{
				this->to_tesselate = false;
				return this->contains_vertex();
			}
		}

protected:
    /**
        \brief Subdivides an octree node and compute its children vertices values
        It also recursively update the newly computed children
        It returns true if least one surface vertex was found
    */
    template<typename PrecisionFunction>
    bool recursive_subdivide_st(const core::AABBox &bbox, unsigned int position[3],
                   unsigned int length, const PrecisionFunction &precision,
                   DCOctree *tree, typename PrecisionFunction::Hint hint,
                   bool topology_subdivision);

    // Return true if a surface vertex was detected
    bool collapse_st(unsigned int position[3], unsigned int length, DCOctree *tree);

    // can call recursive_subdivide_st
    template<typename PrecisionFunction>
    bool collapse_st_topology(const core::AABBox &bbox, unsigned int position[3],
                              unsigned int length, const PrecisionFunction &precision,
                              DCOctree *tree, typename PrecisionFunction::Hint hint);

    /** \brief
      Returns the sub-child of this which size is required_length and which position
      is "position".
      actual_length is the size of this.
      this may be recursively subdived if necessary (thus vertices values may be
      computed)
      */
    DCOctreeNode *subdivide_to_length_st(unsigned int position[3], unsigned int actual_length,
                             unsigned int required_length, DCOctree *tree);

public:
////////////////////////////////// MULTI_THREAD ////////////////////////////////
#ifdef DC_OCTREE_MULTI_THREAD_SUPPORT
    typedef std::array<unsigned int,3> PositionArray;

    template<class PrecisionFunction>
    void update_mt(const core::AABBox &bbox, unsigned int position[3], unsigned int length,
                const PrecisionFunction &precision, DCOctree *tree);

    template<class PrecisionFunction>
    bool increase_precision_mt(unsigned int position[3], unsigned int length,
                            const PrecisionFunction &precision, DCOctree *tree);

protected:

    /**
      \brief According to the precision function and to the vertices values (recomputed
      if they are inside bbox), this will be subdivided of collapsed.
      If compute_vertices is false, no vertices values will be computed (no matter
      the bbox) otherwise only vertices inside bbox will be computed.
      position and length contains the position and size informations of this.
      Iff a face is detected and should later be created, the function will return
      true and this->to_tesselate will be equals to true
     */
    template<class PrecisionFunction>
    void update_mt(const core::AABBox &bbox, PositionArray position, unsigned int length,
                const PrecisionFunction &precision, DCOctree *tree,
                typename PrecisionFunction::Hint hint = typename PrecisionFunction::Hint(),
                bool compute_vertices=true);

    void create_and_set_vertex(std::shared_ptr<TriMesh> trimesh, const Point &P)
    {
        this->vertex = trimesh->add_vertex(P);
    }

    /**
    \brief Subdivides an octree node and compute its children vertices values
    It also recursively update the newly computed children
    It returns true if least one surface vertex was found
    */
    template<class PrecisionFunction>
    void recursive_subdivide_mt(const core::AABBox &bbox, unsigned int position[3],
                   unsigned int length, const PrecisionFunction &precision,
                   DCOctree *tree, typename PrecisionFunction::Hint hint);


    void collapse_mt(unsigned int position[3], unsigned int length, DCOctree *tree);

    void clear_mt(DCOctree *tree);

    DCOctreeNode *subdivide_to_length_mt(unsigned int position[3],
                                         unsigned int actual_length,
                             unsigned int required_length, DCOctree *tree);

    void subdivide_and_collapse_mt(PositionArray position, unsigned int length,
                                   DCOctree *tree);

    template<class PrecisionFunction>
    void increase_precision_mt(unsigned int position[3], unsigned int length,
          const PrecisionFunction &precision, DCOctree *tree,
          typename PrecisionFunction::Hint hint = typename PrecisionFunction::Hint());
#endif
public:
    DCOctreeNode *father;
    array<Scalar, 8> values;

    array<DCOctreeNode *, 8> childs; // internal node case
    Vertex vertex;                 // leaf case
    bool to_tesselate;

#ifdef DC_OCTREE_MULTI_THREAD_SUPPORT
    bool contains_vertex_;
#endif

#ifdef DC_OCTREE_SIZE_CHECKS
    unsigned int position_[3];
    unsigned int size_;
#endif
};


////////////////////////////////////////////////////////////////////////////////
//////////////////// DC_OCTREE_NODE IMPLEMENTATIONS ////////////////////////////
////////////////////////////////////////////////////////////////////////////////


template< typename TTriMesh, typename TVertexProcessor>
void DCOctreeNodeT<TTriMesh, TVertexProcessor>::get_valid_vertices(
        std::vector<Vertex *> &valid_vertices)
{
    if (!is_leaf())
        for (unsigned int i=0; i<8; ++i)
            childs[i]->get_valid_vertices(valid_vertices);
    else if (vertex.is_valid())
        valid_vertices.push_back(&vertex);
}

template< typename TTriMesh, typename TVertexProcessor>
void DCOctreeNodeT<TTriMesh, TVertexProcessor>::invalidate_vertices(
        const core::AABBox &bbox, unsigned int position[3], unsigned int length,
        DCOctree *tree, std::vector<Vertex *> &valid_vertices)
{
    core::AABBox thisbbox=tree->GetWorldBoundingBox(position,length);
    if (thisbbox.Intersect(bbox))
    {
        if (is_leaf())
        {
            if (vertex.is_valid())
            {
                tree->tri_mesh_->delete_vertex(vertex,false);
                vertex.invalidate();
            }
        }
        else
        {
            unsigned int length2 = length/2;
            unsigned int child_position[3];

            for (unsigned int i=0; i<8; ++i)
            {
                child_position[0] = position[0] + length2*vertices_map[i][0];
                child_position[1] = position[1] + length2*vertices_map[i][1];
                child_position[2] = position[2] + length2*vertices_map[i][2];
                childs[i]->invalidate_vertices(bbox,child_position,length2,tree,valid_vertices);
            }
        }
    }
    else
    {
        if (is_leaf())
        {
            if (vertex.is_valid())
                valid_vertices.push_back(&vertex);
        }
        else
        {
            for (unsigned int i=0; i<8; ++i)
            {
                childs[i]->get_valid_vertices(valid_vertices);
            }
        }
    }
}

template< typename TTriMesh, typename TVertexProcessor>
void DCOctreeNodeT<TTriMesh, TVertexProcessor>::node_proc_contour(
        unsigned int depth, std::shared_ptr<TriMesh> tri_mesh)
{
    if (!is_leaf() && to_tesselate)
    {
        const unsigned int child_depth = depth+1;
        unsigned int child_depthes[4] = { child_depth, child_depth, child_depth, child_depth };
        DCOctreeNode *child_nodes[4];

        // 12 face calls
        for (unsigned int i=0; i<12; ++i)
        {
            child_nodes[0] = childs[node_proc_face_mask[i][0]];
            child_nodes[1] = childs[node_proc_face_mask[i][1]];
            face_proc_contour(child_nodes, child_depthes, node_proc_face_mask[i][2], tri_mesh);
        }

        // 6 edges calls
        for (unsigned int i=0; i<6; ++i)
        {
            child_nodes[0] = childs[node_proc_edge_mask[i][0]];
            child_nodes[1] = childs[node_proc_edge_mask[i][1]];
            child_nodes[2] = childs[node_proc_edge_mask[i][2]];
            child_nodes[3] = childs[node_proc_edge_mask[i][3]];
            edge_proc_contour(child_nodes, child_depthes, node_proc_edge_mask[i][4], tri_mesh);
        }

        // 8 node calls
        for (unsigned int i=0; i<8; ++i)
            childs[i]->node_proc_contour(child_depth, tri_mesh);
    }

    to_tesselate = false;
}

template< typename TTriMesh, typename TVertexProcessor>
void DCOctreeNodeT<TTriMesh, TVertexProcessor>::node_proc_topology(
        unsigned int length, const unsigned int position[3], DCOctree *tree)
{
#ifdef DC_OCTREE_SIZE_CHECKS
    assert(size_==length);
    for (unsigned int i=0; i<3; ++i)
        assert(position_[i] == position[i]);
#endif //DC_OCTREE_SIZE_CHECKS

    if (!is_leaf() && to_tesselate)
    {
        const unsigned int length2 = length/2;
        // Position of the left bottom near child called in a recursive function
        // if its size is length/2 (this child does not have to have this size)
        unsigned int childs_position[3];
        unsigned int childs_lengthes[4] = { length2, length2, length2, length2 };
        DCOctreeNode *childs_nodes[4];

        // 6 edges calls
        for (unsigned int i=0; i<6; ++i)
        {
            const unsigned int lbn_child_idx = node_proc_edge_mask[i][0];
            childs_position[0] = position[0] + length2*vertices_map[lbn_child_idx][0];
            childs_position[1] = position[1] + length2*vertices_map[lbn_child_idx][1];
            childs_position[2] = position[2] + length2*vertices_map[lbn_child_idx][2];
            childs_nodes[0] = childs[lbn_child_idx];
            childs_nodes[1] = childs[node_proc_edge_mask[i][1]];
            childs_nodes[2] = childs[node_proc_edge_mask[i][2]];
            childs_nodes[3] = childs[node_proc_edge_mask[i][3]];
            edge_proc_topology(childs_nodes, childs_lengthes, childs_position, node_proc_edge_mask[i][4], tree);
        }

        // 12 face calls
        for (unsigned int i=0; i<12; ++i)
        {
            const unsigned int lbn_child_idx = node_proc_face_mask[i][0];
            childs_position[0] = position[0] + length2*vertices_map[lbn_child_idx][0];
            childs_position[1] = position[1] + length2*vertices_map[lbn_child_idx][1];
            childs_position[2] = position[2] + length2*vertices_map[lbn_child_idx][2];
            childs_nodes[0] = childs[lbn_child_idx];
            childs_nodes[1] = childs[node_proc_face_mask[i][1]];
            face_proc_topology(childs_nodes, childs_lengthes, childs_position, node_proc_face_mask[i][2], tree);
        }

        // 8 node calls
        for (unsigned int i=0; i<8; ++i)
        {
            childs_position[0] = position[0] + length2*vertices_map[i][0];
            childs_position[1] = position[1] + length2*vertices_map[i][1];
            childs_position[2] = position[2] + length2*vertices_map[i][2];
            childs[i]->node_proc_topology(length2, childs_position, tree);
        }
    }
}

/*template< typename TTriMesh, typename TVertexProcessor>
template<class PrecisionFunction>
bool DCOctreeNodeT<TTriMesh, TVertexProcessor>::increase_precision_st(
        unsigned int position[3], unsigned int length,
        const PrecisionFunction &precision, DCOctree *tree,
        typename PrecisionFunction::Hint hint)
{
    const unsigned int length2 = length/2;
    unsigned int child_position[3];
    bool precision_increased = false;

    if (this->is_leaf() && this->vertex.is_valid() && length > 1)
    {
        core::AABBox thisbbox = tree->GetWorldBoundingBox(position,length);
        const Scalar dlength = tree->GetWorldLength(length);
        //precision.UpdateHint(thisbbox,hint);

        if (precision.DetailPrecisionLowerThan(thisbbox,dlength,hint))
        {
            tree->tri_mesh_->delete_vertex(this->vertex,false);
            this->vertex.invalidate();
            subdivide(core::AABBox(),position,length,tree);
            for (unsigned int i=0; i<8; ++i)
            {
                child_position[0] = position[0] + length2*vertices_map[i][0];
                child_position[1] = position[1] + length2*vertices_map[i][1];
                child_position[2] = position[2] + length2*vertices_map[i][2];
#ifdef DC_OCTREE_SIZE_CHECKS
                this->childs[i]->position_[0] = child_position[0];
                this->childs[i]->position_[1] = child_position[1];
                this->childs[i]->position_[2] = child_position[2];
                this->childs[i]->size_ = length2;
#endif //DC_OCTREE_SIZE_CHECKS
                this->childs[i]->collapse_st(child_position,length2,tree);
            }
            precision_increased = true;
        }
    }
#ifdef DC_OCTREE_SIZE_CHECKS
    else if (this->is_leaf() && length > 1)
    {
        bool sgn = this->values[0] > 0;
        for (unsigned int i=0; i<8; ++i)
            assert((this->values[i] > 0) == sgn);
    }
#endif

    if (!this->is_leaf() && !precision_increased)
    {
        core::AABBox thisbbox = tree->GetWorldBoundingBox(position,length);
        precision.UpdateHint(thisbbox,hint);

        for (unsigned int i=0; i<8; ++i)
        {
            child_position[0] = position[0] + length2*vertices_map[i][0];
            child_position[1] = position[1] + length2*vertices_map[i][1];
            child_position[2] = position[2] + length2*vertices_map[i][2];

            precision_increased = this->childs[i]->increase_precision_st(child_position,
                                   length2,precision,tree,hint) ||
                                    precision_increased;
        }
    }

    return this->to_tesselate = precision_increased;
}*/

template< typename TTriMesh, typename TVertexProcessor>
bool DCOctreeNodeT<TTriMesh, TVertexProcessor>::contains_vertex() const
{
    if (is_leaf())
        return vertex.is_valid();
    else
        for (unsigned int i=0; i<8; ++i)
            if (childs[i]->contains_vertex())
                return true;
    return false;
}

template< typename TTriMesh, typename TVertexProcessor>
void DCOctreeNodeT<TTriMesh, TVertexProcessor>::clear_st(std::shared_ptr<TriMesh> tri_mesh)
{
    if (!is_leaf())
    {
        for (unsigned int i=0; i<8; ++i)
           childs[i]->clear_st(tri_mesh);
    }
    else if (vertex.is_valid())
    {
        tri_mesh->delete_vertex(vertex,false);
        vertex.invalidate();
    }

}

template< typename TTriMesh, typename TVertexProcessor>
void DCOctreeNodeT<TTriMesh, TVertexProcessor>::subdivide(const core::AABBox &bbox,
                                                          unsigned int position[3],
                                                          unsigned int length,
                                                          DCOctree *tree)
{
    const bool was_leaf = this->is_leaf();
    const unsigned int length2 = length/2;

    if (was_leaf)
        for (typename array<DCOctreeNode *,8>::iterator it=this->childs.begin(); it!=this->childs.end(); ++it)
            *it = new DCOctreeNode(static_cast<DCOctreeNode *>(this));

    // Compute childs vertex values
    {
        array<Scalar, 27> childs_values;
        array<bool, 27> value_is_updated;
        value_is_updated.fill(false);
        unsigned int idx = 0;
        for (unsigned int i=0; i<3; ++i)
            for (unsigned int j=0; j<3; ++j)
                for (unsigned int k=0; k<3; ++k, ++idx)
                    if (subdivide_proc_shared_vertex_mask[idx])
                    {
                        // This vertex position may be shared with others cells
                        // So we search or add it on vertices_table

                        const int x=position[0]+i*length2;
                        const int y=position[1]+j*length2;
                        const int z=position[2]+k*length2;

                        Point p = tree->GetWorldPoint(x,y,z);

                        if (was_leaf || bbox.Contains(p))
                        {
                            if (!tree->vertices_table.get(x,y,z,childs_values[idx]))
                            {
                                ++(tree->nb_evals);
                                childs_values[idx] = tree->function->EvalMinusIsoValue(p[0], p[1], p[2]);
                                tree->vertices_table.insert(x,y,z,childs_values[idx]);
                            }
                            value_is_updated[idx] = true;
                        }
                    }

        // Middle vertex position :
        // This vertex will never be requested by another cell, so we compute
        // it once and we do not add it to vertices_table
        const int xm=position[0]+length2;
        const int ym=position[1]+length2;
        const int zm=position[2]+length2;
        Point middle = tree->GetWorldPoint(xm,ym,zm);

        if (was_leaf || bbox.Contains(middle))
        {
            ++(tree->nb_evals);
            childs_values[13] = tree->function->EvalMinusIsoValue(middle[0],middle[1],middle[2]);
            // This value will never be requested
            //tree->vertices_table.insert(xm,ym,zm,childs_values[13]);
            value_is_updated[13] = true;
        }

        // Copy the values from the vertices of the mother cell
        for (unsigned int i=0; i<8; ++i)
        {
            const int idx = subdivide_proc_inherited_vertex_mask[i];
            childs_values[idx] = this->values[i];
            value_is_updated[idx] = true;
        }

        // Fills childs cells with the computed vertices values
        for (unsigned int i=0; i<8; ++i)
            for (unsigned int j=0; j<8; ++j)
            {
                const int idx = subdivide_proc_childs_vertex_mask[i][j];
                if (value_is_updated[idx])
                    this->childs[i]->values[j] = childs_values[idx];
            }
    }
}

template< typename TTriMesh, typename TVertexProcessor>
typename DCOctreeNodeT<TTriMesh, TVertexProcessor>::DCOctreeNode
*DCOctreeNodeT<TTriMesh, TVertexProcessor>::find_child_with_length(
        unsigned int position[3], unsigned int &actual_length,
        unsigned int required_length)
{
#ifdef DC_OCTREE_SIZE_CHECKS
    unsigned int actual_position0[3] = { position[0] - (position[0] % actual_length),
                                         position[1] - (position[1] % actual_length),
                                         position[2] - (position[2] % actual_length) };
    assert(this->size_==actual_length);
    for (unsigned int i=0; i<3; ++i)
    {
        assert(this->position_[i] == actual_position0[i]);
        assert(this->position_[i] <= position[i]);
        assert(position[i] < (this->position_[i] + this->size_));
    }
#endif //DC_OCTREE_SIZE_CHECKS

    if (actual_length>required_length && !is_leaf())
    {
        const unsigned int length2 = actual_length / 2;
        const unsigned int child_idx = child_position_mask[(position[0]%actual_length)>=length2 ? 1 : 0]
                                                                                                  [(position[1]%actual_length)>=length2 ? 1 : 0]
                                                                                                                                          [(position[2]%actual_length)>=length2 ? 1 : 0];

        actual_length = length2;
        return childs[child_idx]->find_child_with_length(position,actual_length,required_length);
    }
    else
    {
        return static_cast<DCOctreeNode *>(this);
    }
}

template< typename TTriMesh, typename TVertexProcessor>
void DCOctreeNodeT<TTriMesh, TVertexProcessor>::face_proc_contour(DCOctreeNode *nodes[2],
                              unsigned int depth[2], int dir, std::shared_ptr<TriMesh> tri_mesh)
{
    if ((nodes[0]->to_tesselate || nodes[1]->to_tesselate) &&
            !(nodes[0]->is_leaf() && nodes[1]->is_leaf()))
    {

        DCOctreeNode *child_nodes[4];
        unsigned int child_depthes[4] = { (nodes[0]->is_leaf() ? depth[0] : depth[0]+1),
                                          (nodes[1]->is_leaf() ? depth[1] : depth[1]+1)};

        // 4 face calls
        for (unsigned int i=0; i<4; ++i)
        {
            child_nodes[0] = (nodes[0]->is_leaf() ? nodes[0] : nodes[0]->childs[ face_proc_face_mask[dir][i][0] ]);
            child_nodes[1] = (nodes[1]->is_leaf() ? nodes[1] : nodes[1]->childs[ face_proc_face_mask[dir][i][1] ]);
            face_proc_contour(child_nodes, child_depthes, dir, tri_mesh);
        }

        // 4 edge calls
        for (unsigned int i=0; i<4; ++i)
        {
            int const *fathers = face_proc_edge_fathers_mask[ face_proc_edge_mask[dir][i][4] ];
            for (unsigned int j=0; j<4; ++j)
            {
                const int k = fathers[j];
                if (nodes[k]->is_leaf())
                {
                    child_nodes[j] = nodes[k];
                    child_depthes[j] = depth[k];
                }
                else
                {
                    child_nodes[j] = nodes[k]->childs[ face_proc_edge_mask[dir][i][j]];
                    child_depthes[j] = depth[k]+1;
                }
            }
            edge_proc_contour(child_nodes, child_depthes, face_proc_edge_mask[dir][i][5], tri_mesh);
        }
    }
}

template< typename TTriMesh, typename TVertexProcessor>
void DCOctreeNodeT<TTriMesh, TVertexProcessor>::edge_proc_contour(DCOctreeNode *nodes[4],
                               unsigned int depth[4], int dir, std::shared_ptr<TriMesh> tri_mesh)
{
    if (!(nodes[0]->to_tesselate || nodes[1]->to_tesselate ||
          nodes[2]->to_tesselate || nodes[3]->to_tesselate))
        return;

    const bool isleaf[4] = { nodes[0]->is_leaf(), nodes[1]->is_leaf(),
                             nodes[2]->is_leaf(), nodes[3]->is_leaf()};

    if (isleaf[0] && isleaf[1] && isleaf[2] && isleaf[3])
    {
        add_face(nodes,depth,dir,tri_mesh);
    }
    else
    {
        DCOctreeNode *enodes[4];
        unsigned int edepth[4] = { (isleaf[0] ? depth[0] : depth[0]+1),
                                   (isleaf[1] ? depth[1] : depth[1]+1),
                                   (isleaf[2] ? depth[2] : depth[2]+1),
                                   (isleaf[3] ? depth[3] : depth[3]+1)};

        // 2 edge calls
        for (unsigned int i=0; i<2; ++i)
        {
            for (unsigned int j=0; j<4; ++j)
                enodes[j] = (isleaf[j] ? nodes[j] :
                                         nodes[j]->childs[ edge_proc_edge_mask[dir][i][j] ]);

            edge_proc_contour(enodes, edepth, dir, tri_mesh);
        }
    }
}

template< typename TTriMesh, typename TVertexProcessor>
void DCOctreeNodeT<TTriMesh, TVertexProcessor>::add_face(DCOctreeNode *nodes[4],
                              unsigned int depth[4], int dir, std::shared_ptr<TriMesh> tri_mesh)
{
    std::vector<Vertex> vertices;
    std::map<int,bool> indexes_to_tesselate;

    bool flip;
    {

        const int argmaxdepth = (const int) (std::max_element(depth,depth+4) - depth);
        const int edge_id = add_face_proc_edge_mask[dir][argmaxdepth];

        const bool v1positive = nodes[argmaxdepth]->values[edgevmap[edge_id][0]] > 0;
        const bool v2positive = nodes[argmaxdepth]->values[edgevmap[edge_id][1]] > 0;

        if (v1positive == v2positive)
            return;

        flip = v1positive;
    }

    for (unsigned int i=0; i<4; ++i)
    {
        DCOctreeNode const *node = nodes[add_face_proc_vertices_mask[i]];
        const Vertex v = node->vertex;
        if (v.is_valid() && indexes_to_tesselate.find(v.idx())==indexes_to_tesselate.end())
        {
            vertices.push_back(v);
            indexes_to_tesselate.insert(std::make_pair(v.idx(),node->to_tesselate));
        } else if (!v.is_valid())
        {
#ifdef DC_OCTREE_DISPLAY_WARNINGS
            std::cerr << "Warning : Encounter invalid vertex during tesselation" << std::endl;
#endif
        }

    }

    if (vertices.size()<3)
    {
#ifdef DC_OCTREE_DISPLAY_WARNINGS
        std::cerr << "Warning : found a face with " << vertices.size() << " vertice(s)" << std::endl;
#endif
        return;
    }

    if (flip)
        std::swap(vertices[0],vertices[2]);

    if (vertices.size() == 4)
    {
        // Quad case

        // we measure the colinearity between faces and vertex normal
        // to find the best split of the "quad" into triangles
        // NB : an alternative would be to compute the field value at the point
        // in the middle of the tetrahedra defined by the vertices and check wether it
        // is inside or outside of the

        const core::DefaultMeshVertex& p0 = tri_mesh->point(vertices[0]);
        const core::DefaultMeshVertex& p1 = tri_mesh->point(vertices[1]);
        const core::DefaultMeshVertex& p2 = tri_mesh->point(vertices[2]);
        const core::DefaultMeshVertex& p3 = tri_mesh->point(vertices[3]);

        // edges
        Vector e01 = p1-p0, e12 = p2-p1, e23 = p3-p2, e30 = p0-p3;

//Vector face_n1, vertex_n1;
        // split 1 test normal of p1 and p3
        Scalar factor_split_1 =   (tri_mesh->normal(vertices[1])).dot((e01.cross(e12)).normalized())
                                + (tri_mesh->normal(vertices[3])).dot((e23.cross(e30)).normalized());

        // split 2 test normals of p0 and p2
        Scalar factor_split_2 =   (tri_mesh->normal(vertices[0])).dot((e30.cross(e01)).normalized())
                                + (tri_mesh->normal(vertices[2])).dot((e12.cross(e23)).normalized());

        //TODO:@todo :doesn't seems to work as well as I would like
        if(factor_split_1 > factor_split_2)
        {
            if (indexes_to_tesselate[vertices[0].idx()]
                    || indexes_to_tesselate[vertices[1].idx()]
                    || indexes_to_tesselate[vertices[2].idx()])
                tri_mesh->add_face(vertices[0],vertices[1],vertices[2]);
            if (indexes_to_tesselate[vertices[2].idx()]
                    || indexes_to_tesselate[vertices[3].idx()]
                    || indexes_to_tesselate[vertices[0].idx()])
                tri_mesh->add_face(vertices[2],vertices[3],vertices[0]);
        }
        else
        {
            if (indexes_to_tesselate[vertices[0].idx()]
                    || indexes_to_tesselate[vertices[1].idx()]
                    || indexes_to_tesselate[vertices[3].idx()])
                tri_mesh->add_face(vertices[0],vertices[1],vertices[3]);
            if (indexes_to_tesselate[vertices[2].idx()] ||
                    indexes_to_tesselate[vertices[3].idx()] ||
                    indexes_to_tesselate[vertices[1].idx()])
                tri_mesh->add_face(vertices[2],vertices[3],vertices[1]);
        }
    }
    else // if (vertices.size() == 3)
    {
        // Triangle case
        tri_mesh->add_face(vertices[0],vertices[1],vertices[2]);
    }
}


template< typename TTriMesh, typename TVertexProcessor>
void DCOctreeNodeT<TTriMesh, TVertexProcessor>::face_proc_topology(DCOctreeNode *nodes[2],
                                                                   unsigned int lengthes[2],
                                                                   unsigned int position[3],
                                                                   int dir, DCOctree *tree)
{
#ifdef DC_OCTREE_SIZE_CHECKS
    assert(nodes[0]->size_==lengthes[0]);
    assert(nodes[1]->size_==lengthes[1]);
    if (lengthes[0] <= lengthes[1]){
        for (unsigned int i=0; i<3; ++i)
            assert(nodes[0]->position_[i] == position[i]);
    }
#endif //DC_OCTREE_SIZE_CHECKS

    if ((nodes[0]->to_tesselate || nodes[1]->to_tesselate) &&
            !(nodes[0]->is_leaf() && nodes[1]->is_leaf()))
    {
        unsigned int minlength;
        unsigned int minlength_idx;
        if (lengthes[0]<=lengthes[1] && !nodes[0]->is_leaf())
        {
            minlength = lengthes[0];
            minlength_idx = 0;
        }
        else
        {
            minlength = lengthes[1];
            minlength_idx = 1;
        }
        const unsigned int minlength2 = minlength/2;
        DCOctreeNode *enodes[4];

        unsigned int cpos[3] = { position[0], position[1], position[2]};

        unsigned int elengthes[4];
        int const *fathers;

        // Check if a subdivision is required
        if (nodes[1-minlength_idx]->is_leaf())
        {
            assert(!nodes[minlength_idx]->is_leaf());
            const int center_sign = sign(nodes[minlength_idx]
                                         ->childs[face_proc_face_mask[dir][0][minlength_idx]]
                                         ->values[face_proc_face_mask[dir][3][minlength_idx]]);
            bool to_subdivide = false;
            for (unsigned int i=1; i<=2; ++i)
            {
                const int sgn1 = sign(nodes[minlength_idx]
                                      ->childs[face_proc_face_mask[dir][0][minlength_idx]]
                                      ->values[face_proc_face_mask[dir][i][minlength_idx]]);
                if (sgn1 != center_sign)
                {
                    const int sgn2 = sign(nodes[minlength_idx]
                                          ->childs[face_proc_face_mask[dir][3][minlength_idx]]
                                          ->values[face_proc_face_mask[dir][3-i][minlength_idx]]);
                    if (sgn2 != center_sign)
                    {
                        to_subdivide = true;
                        break;
                    }
                }
            }
            if (to_subdivide)
            {
                if (minlength_idx==0)
                    cpos[dir] += minlength;
                nodes[1-minlength_idx]=nodes[1-minlength_idx]->subdivide_to_length_st(cpos,lengthes[1-minlength_idx],minlength2,tree)->father;
                lengthes[1-minlength_idx]=minlength;
            }
        }

        unsigned int epos[3];

        // 4 edge calls
        for (unsigned int i=0; i<4; ++i)
        {
            const unsigned int child0idx = face_proc_edge_mask[dir][i][0];

            cpos[0] = position[0] + minlength2*vertices_map[child0idx][0];
            cpos[1] = position[1] + minlength2*vertices_map[child0idx][1];
            cpos[2] = position[2] + minlength2*vertices_map[child0idx][2];
            fathers = face_proc_edge_fathers_mask[ face_proc_edge_mask[dir][i][4] ];
            for (unsigned int j=0; j<4; ++j)
            {
                const int k = fathers[j];

                const int offset_idx2 = node_proc_edge_mask[2*face_proc_edge_mask[dir][i][5]][j];
                enodes[j] = nodes[k];
                elengthes[j] = lengthes[k];
                epos[0] = cpos[0] + vertices_map[offset_idx2][0]*minlength2;
                epos[1] = cpos[1] + vertices_map[offset_idx2][1]*minlength2;
                epos[2] = cpos[2] + vertices_map[offset_idx2][2]*minlength2;

                enodes[j] = enodes[j]->find_child_with_length(epos,elengthes[j],minlength2);
            }
            edge_proc_topology(enodes, elengthes, cpos, face_proc_edge_mask[dir][i][5], tree);
        }

        DCOctreeNode *fnodes[2] = { nodes[0], nodes[1] };
        unsigned int flengthes[2] = { lengthes[0], lengthes[1] };

        // 4 face calls
        for (unsigned int i=0; i<4; ++i)
        {
            const unsigned int child0idx = face_proc_face_mask[dir][i][0];
            cpos[0] = position[0] + minlength2*vertices_map[child0idx][0];
            cpos[1] = position[1] + minlength2*vertices_map[child0idx][1];
            cpos[2] = position[2] + minlength2*vertices_map[child0idx][2];

            for (int j=0; j<2; ++j)
            {
                fnodes[j] = nodes[j];
                flengthes[j] = lengthes[j];

                if (!fnodes[j]->is_leaf())
                {
                    std::copy(cpos,cpos+3,epos);
                    if (j==1)
                        epos[dir]+=minlength2;
                    fnodes[j] = fnodes[j]->find_child_with_length(epos,flengthes[j],minlength2);
                }
            }

            face_proc_topology(fnodes, flengthes, cpos, dir, tree);
        }
    }
}

template< typename TTriMesh, typename TVertexProcessor>
void DCOctreeNodeT<TTriMesh, TVertexProcessor>::edge_proc_topology(DCOctreeNode *nodes[4], unsigned int lengthes[4],
                                                                   unsigned int position[3], int dir, DCOctree *tree)
{
#ifdef DC_OCTREE_SIZE_CHECKS
    assert(nodes[0]->size_==lengthes[0]);
    if (lengthes[0] <= lengthes[1] && lengthes[0]<=lengthes[2] && lengthes[0]<=lengthes[3]){
        for (unsigned int i=0; i<3; ++i)
            assert(nodes[0]->position_[i] == position[i]);
    }
#endif //DC_OCTREE_SIZE_CHECKS

    if (!(nodes[0]->to_tesselate || nodes[1]->to_tesselate ||
          nodes[2]->to_tesselate || nodes[3]->to_tesselate))
        return;

    if (!(nodes[0]->is_leaf() && nodes[1]->is_leaf() && nodes[2]->is_leaf() && nodes[3]->is_leaf()))
    {
        unsigned int cpos[3], epos[3];
        DCOctreeNode *enodes[4] = {nodes[0], nodes[1], nodes[2], nodes[3] };
        /*unsigned int elengthes[4] = { (nodes[0]->is_leaf() ? lengthes[0] : lengthes[0]/2),
                                                                       (nodes[1]->is_leaf() ? lengthes[1] : lengthes[1]/2),
                                                                       (nodes[2]->is_leaf() ? lengthes[2] : lengthes[2]/2),
                                                                       (nodes[3]->is_leaf() ? lengthes[3] : lengthes[3]/2)};*/
        unsigned int elengthes[4] = { lengthes[0], lengthes[1], lengthes[2], lengthes[3] };

        unsigned int minlength = lengthes[0];
        unsigned int minlength_idx = 0;

        for (unsigned int i=1; i<4; ++i)
            if (lengthes[i]<=minlength && !nodes[i]->is_leaf())
            {
                minlength = lengthes[i];
                minlength_idx = i;
            }
        const int length2 = minlength/2;

#ifdef DC_OCTREE_SIZE_CHECKS
        for (unsigned int i=0; i<4; ++i)
        {
            assert(nodes[i]->size_==lengthes[i]);
            const int offset_idx = node_proc_edge_mask[2*dir][i];
            cpos[0] = position[0] + vertices_map[offset_idx][0]*minlength;
            cpos[0] -= cpos[0]%nodes[i]->size_;
            cpos[1] = position[1] + vertices_map[offset_idx][1]*minlength;
            cpos[1] -= cpos[1]%nodes[i]->size_;
            cpos[2] = position[2] + vertices_map[offset_idx][2]*minlength;
            cpos[2] -= cpos[2]%nodes[i]->size_;
            for (unsigned int j=0; j<3; ++j)
                assert(nodes[i]->position_[j] == cpos[j]);
        }
#endif //DC_OCTREE_SIZE_CHECKS

        // Check if a subdivision is required
        {
            const int center_sgn = sign(nodes[minlength_idx]
                                        ->childs[edge_proc_edge_mask[dir][0][minlength_idx]]
                                        ->values[edge_proc_edge_mask[dir][1][minlength_idx]]);
            const int sign1 = sign(nodes[minlength_idx]
                                   ->values[edge_proc_edge_mask[dir][0][minlength_idx]]);
            const int sign2 = sign(nodes[minlength_idx]
                                   ->values[edge_proc_edge_mask[dir][1][minlength_idx]]);
            if ((sign1 != center_sgn) && (sign2 != center_sgn))
            {
                // subdivision
                for (unsigned int i=0; i<4; ++i)
                {
                    const int offset_idx = node_proc_edge_mask[2*dir][i];
                    cpos[0] = position[0] + vertices_map[offset_idx][0]*minlength;
                    cpos[1] = position[1] + vertices_map[offset_idx][1]*minlength;
                    cpos[2] = position[2] + vertices_map[offset_idx][2]*minlength;
                    nodes[i] = nodes[i]->subdivide_to_length_st(cpos,lengthes[i],length2,tree)->father;
                    lengthes[i] = minlength;
                }
            }
        }

        // 2 edge calls
        for (unsigned int i=0; i<2; ++i)
        {
            const int offset_idx = edge_proc_edge_mask[dir][i][0];

            cpos[0] = position[0] + vertices_map[offset_idx][0]*length2;
            cpos[1] = position[1] + vertices_map[offset_idx][1]*length2;
            cpos[2] = position[2] + vertices_map[offset_idx][2]*length2;

            for (unsigned int j=0; j<4; ++j){

                const int offset_idx2 = node_proc_edge_mask[2*dir][j];
                enodes[j] = nodes[j];
                elengthes[j] = lengthes[j];
                epos[0] = cpos[0] + vertices_map[offset_idx2][0]*length2;
                epos[1] = cpos[1] + vertices_map[offset_idx2][1]*length2;
                epos[2] = cpos[2] + vertices_map[offset_idx2][2]*length2;

                enodes[j] = enodes[j]->find_child_with_length(epos,elengthes[j],length2);
            }

            edge_proc_topology(enodes, elengthes, cpos, dir, tree);
        }
    }
}

/*template< typename TTriMesh, typename TVertexProcessor>
template<class PrecisionFunction>
bool DCOctreeNodeT<TTriMesh, TVertexProcessor>::update_st(const core::AABBox &bbox,
            unsigned int position[3], unsigned int length,
            const PrecisionFunction &precision, DCOctree *tree,
            typename PrecisionFunction::Hint hint, bool compute_vertices)
{
#ifdef DC_OCTREE_SIZE_CHECKS
    this->position_[0] = position[0];
    this->position_[1] = position[1];
    this->position_[2] = position[2];
    this->size_ = length;
#endif //DC_OCTREE_SIZE_CHECKS

    bool simple = true;
    bool intersects_bbox_vertex = false;
    int sign = 0;

    if (compute_vertices)
        for (unsigned int i=0; i<8; ++i)
        {
            const int x=position[0]+vertices_map[i][0]*length;
            const int y=position[1]+vertices_map[i][1]*length;
            const int z=position[2]+vertices_map[i][2]*length;
            Point p = tree->GetWorldPoint(x,y,z);
            if (bbox.Contains(p) &&
                !tree->vertices_table.get(x,y,z,this->values[i]))
            {
                intersects_bbox_vertex=true;
                ++(tree->nb_evals);
                this->values[i] = tree->function->EvalMinusIsoValue(p[0], p[1], p[2]);
                tree->vertices_table.insert(x,y,z,this->values[i]);
            }
        }

    for (unsigned int i=0; i<8; ++i){
        if (sign*this->values[i]<0)
            simple = false;
        if (this->values[i]>0)
            sign = 1;
        else if (this->values[i]<0)
            sign = -1;
    }

    core::AABBox thisbbox = tree->GetWorldBoundingBox(position,length);

    if (intersects_bbox_vertex || thisbbox.Intersect(bbox))
    {
        if (sign==0)
            simple=false;
        const Scalar dlength = tree->GetWorldLength(length);

        precision.UpdateHint(thisbbox,hint);

        if ((precision.FeaturePrecisionLowerThan(thisbbox, dlength, hint) ||
             (!simple && precision.DetailPrecisionLowerThan(thisbbox,dlength, hint)))
                && length > 1  && recursive_subdivide_st(bbox,position, length, precision, tree, hint))
            return this->to_tesselate = true;
        else
            return collapse_st(position, length, tree);
    }
    else if (this->is_leaf())
    {
        return collapse_st(position, length, tree);
    }
    else
    {
        this->to_tesselate = false;
        return this->contains_vertex();
    }
}*/

template< typename TTriMesh, typename TVertexProcessor>
template<class PrecisionFunction>
bool DCOctreeNodeT<TTriMesh, TVertexProcessor>::recursive_subdivide_st(
               const core::AABBox &bbox, unsigned int position[3],
               unsigned int length, const PrecisionFunction &precision,
               DCOctree *tree, typename PrecisionFunction::Hint hint,
               bool topology_subdivision)
{
    const unsigned int length2 = length/2;
    unsigned int child_position[3];
    bool vertex_found = false;

    if (this->vertex.is_valid())
    {
        tree->tri_mesh_->delete_vertex(this->vertex,false);
        this->vertex.invalidate();
    }

    subdivide(bbox,position,length,tree);

    // Recursively updates children
    for (unsigned int i=0; i<8; ++i)
    {
        child_position[0] = position[0] + length2*vertices_map[i][0];
        child_position[1] = position[1] + length2*vertices_map[i][1];
        child_position[2] = position[2] + length2*vertices_map[i][2];
        vertex_found = this->childs[i]->update_st(bbox,child_position,length2, precision, tree, hint, false, topology_subdivision) || vertex_found;
    }
    return vertex_found;
}

//////////////////////////////////////////////////
#define USE_QEF
#define USE_BARYCENTER
//////////////////////////////////////////////////

template< typename TTriMesh, typename TVertexProcessor>
bool DCOctreeNodeT<TTriMesh, TVertexProcessor>::collapse_st(
        unsigned int position[3], unsigned int length, DCOctree *tree)
{
    if (!this->is_leaf())
    {
        for (typename array<DCOctreeNode *, 8>::iterator it=this->childs.begin();
             it!=this->childs.end(); ++it)
        {
            (*it)->clear_st(tree->tri_mesh_);
            delete *it;
            *it = NULL;
        }
    }

    // Compute the intersection information on all edges


    Point points[12];

    Vector mass_point = Vector::Zero();

#ifdef USE_QEF
    bool intersect[12];

    UNUSED(intersect);
    Vector normals[12];
    Vector mass_normal = Vector::Zero();
#endif

//    Eigen::Matrix3d A_I = Eigen::Matrix3d::Zero(); //TODO:@todo
//    Vector b_I = Vector::Zero();

#ifdef USE_QEF
    float mp[3]; // this is the minimizer point of the QEF
    int index; // vertex index in PLY file
    float ata[6], atb[3], btb = 0.f; // QEF data


    UNUSED(mp);
    UNUSED(index);

    core::AABBox aabb; //stupid to compute it the way that follow ...
#endif

    unsigned int n_faces = 0;
    for (unsigned int i=0; i<12; ++i)
    {
        const unsigned int i1=edgevmap[i][0], i2=edgevmap[i][1];
        const Scalar v1=this->values[i1], v2=this->values[i2];
        if (v1*v2<=0.0 && (v1!=0.0 || v2!=0.0))
        { // there is an intersection
#ifdef USE_QEF
            intersect[i] = true;
#endif
            n_faces++;

            const Point P1=tree->GetWorldPoint(position[0] + length*vertices_map[i1][0],
                                               position[1] + length*vertices_map[i1][1],
                                               position[2] + length*vertices_map[i1][2]);
            const Point P2=tree->GetWorldPoint(position[0] + length*vertices_map[i2][0],
                                               position[1] + length*vertices_map[i2][1],
                                               position[2] + length*vertices_map[i2][2]);

#ifdef USE_QEF
            aabb = aabb.Enlarge(P1); //stupid
            aabb = aabb.Enlarge(P2); //stupid
#endif

#ifdef USE_BARYCENTER
            //TODO:@todo : to obtain better result, the placement of the point should be improved
            points[i] = P1 + (v1/(v1-v2))*(P2-P1); // beurk ...
#else
            Point pos, neg;
            Scalar epsilon = 0.01;
            //TODO:@todo : the two can be on the iso at the same time ... in this case better to take the middle
            if(fabs(v1) < epsilon) points[i] = P1;
            else if(fabs(v2) < epsilon) points[i] = P2;
            else
            {
                if (v1 < 0.0) { pos = P2; neg = P1; }
                else          { pos = P1; neg = P2; }
                Vector search_dir = neg - pos;
                Scalar norm = search_dir.norm();
//                assert(tree->function->Eval(neg) <= tree->function->iso_value());
//                assert(tree->function->Eval(pos) >= tree->function->iso_value());
                points[i] = Convergence::SafeNewton1DDichotomy(*(tree->function->blobtree_root()),
                                                               pos, search_dir, 0.0, norm, norm*v1/(v1-v2),//0.5*norm,
                                                               tree->function->iso_value(), epsilon*search_dir.norm(), epsilon*0.01);
                Scalar val = fabs(tree->function->EvalMinusIsoValue(points[i]));
                if(val>0.02) {
                    ork::Logger::DEBUG_LOGGER->logf("POLYGONIZER", "%f", val);
                }
            }
#endif
            mass_point += points[i];

#ifdef USE_QEF
            ///TODO:@todo this is not really smart to compute the normal only now since they could be use to check if subdivision is required ...
            normals[i] = -tree->function->EvalGrad(points[i], 0.001); //TODO:@todo : should they be normalized or not ?
//assert(normals[i].norm() > 0.00000001);
            normals[i].normalize(); // apriori oui
            // TODO:@todo : warning if null normal !!!

            mass_normal += normals[i];

//            A_I += normals[i] * normals[i].transpose();
//            b_I += (-normals[i].dot(points[i])) * normals[i];

            // QEF
            ata[ 0 ] += (float) ( normals[i][ 0 ] * normals[i][ 0 ] );
            ata[ 1 ] += (float) ( normals[i][ 0 ] * normals[i][ 1 ] );
            ata[ 2 ] += (float) ( normals[i][ 0 ] * normals[i][ 2 ] );
            ata[ 3 ] += (float) ( normals[i][ 1 ] * normals[i][ 1 ] );
            ata[ 4 ] += (float) ( normals[i][ 1 ] * normals[i][ 2 ] );
            ata[ 5 ] += (float) ( normals[i][ 2 ] * normals[i][ 2 ] );
            double pn = points[i][0] * normals[i][0] + points[i][1] * normals[i][1] + points[i][2] * normals[i][2] ;
            atb[ 0 ] += (float) ( normals[i][ 0 ] * pn ) ;
            atb[ 1 ] += (float) ( normals[i][ 1 ] * pn ) ;
            atb[ 2 ] += (float) ( normals[i][ 2 ] * pn ) ;
            btb += (float) pn * (float) pn ;
#endif
        }
        else
        {
#ifdef USE_QEF
            intersect[i] = false;

            const Point P1=tree->GetWorldPoint(position[0] + length*vertices_map[i1][0],
                                               position[1] + length*vertices_map[i1][1],
                                               position[2] + length*vertices_map[i1][2]);
            const Point P2=tree->GetWorldPoint(position[0] + length*vertices_map[i2][0],
                                               position[1] + length*vertices_map[i2][1],
                                               position[2] + length*vertices_map[i2][2]);
            aabb = aabb.Enlarge(P1); //stupid
            aabb = aabb.Enlarge(P2); //stupid
#endif
        }
    }


    if (n_faces>0)
    {
        mass_point /= ((double)n_faces);

#ifdef USE_QEF
        mass_normal.normalize();


        //TODO:@todo : temporary test to prevent non manifold triangle that leave the surface in some place
        //TODO:@todo : a possible reason of this weird triangle could be a wrong return value or to_tesselate in some place
        Point new_point = mass_point;
        if(length>1) // we do not want to call a subdivide if we are already at the lower level
        {
            Scalar error = tree->function->Eval(new_point);
            if(error<0.85*tree->function->iso_value())
            {
//                std::cout<< "too far outside" << std::endl;
                if (this->vertex.is_valid())
                {
                    tree->tri_mesh_->delete_vertex(this->vertex,false);
                    this->vertex.invalidate();
                }
                return this->to_tesselate = false;
            }
        }

//        // Solve
//        // eigen.hpp
//        // calculate minimizer point, and return error
//        // QEF: ata, atb, btb
//        // pt is the average of the intersection points
//        // mp is the result
//        // box is a bounding-box for this node
//        // mat is storage for calcPoint() ?
//        float mp[3]; // will contains the results...
//        float mat[10] ;
//        float tmp[3] = {(float)mass_point[0], (float)mass_point[1], (float)mass_point[2]};
//        float error = calcPoint( ata, atb, btb, tmp, mp, aabb, mat ) ;

//        Point new_point(mp[0], mp[1], mp[2]);
//        if(!aabb.Contains(new_point)) new_point = mass_point;
#else
        Point new_point = mass_point;
#endif

//        // Compute the supporting plane quadric (which purpose is the numerical stability of the system)
//        Eigen::Matrix3d A_S = Eigen::Matrix3d::Zero(); //TODO:@todo
//        Vector b_S = Vector::Zero();

//        for (unsigned int i=0; i<12; ++i)
//        {
//            if (intersect[i])
//            {
//                //TODO:@todo : there is some check to perform on possible alignement
//                Vector e = mass_point - points[i];
////assert(e.norm() > 0.00000001);
//                e.normalize(); //TODO:@todo : should check if it is null ...
//                Vector n_e = mass_normal + normals[i];
//                n_e.normalize();
////assert(e.dot(n_e) != e.norm()*n_e.norm());
//                Vector n_s = n_e.cross(e);
//                n_s.normalize();

////                A_S += n_s * n_s.transpose();
////                b_S += (n_s.dot(mass_point)) * n_s;
//            }
//        }

//        Scalar coeff = 0.5;
        // this coeff should belong to [0;1]
        // when it tend toward zero, the system tend to the original one which could be instable but better reconstruct sharp edges

//        Eigen::Matrix3d A = A_I + coeff*A_S;
//        Vector b = b_I + coeff*b_S;

//        Point new_point = -A_I.colPivHouseholderQr().solve(b_I);
//        Point new_point = -A.colPivHouseholderQr().solve(b);
//        Point new_point = -(A_I.transpose() * A_I).ldlt().solve(A_I.transpose() * b_I);
//Point new_point = mass_point;

        bool was_valid = this->vertex.is_valid();
        if (was_valid)
            tree->tri_mesh_->point(this->vertex) = new_point;
        else
            this->vertex = tree->tri_mesh_->add_point(new_point);

        ++(tree->nb_evals);
        TVertexProcessor::Process(*(tree->function), tree->tri_mesh_, this->vertex);

        this->to_tesselate = !was_valid;
        return true;
    }
    else
    {
        if (this->vertex.is_valid())
        {
            tree->tri_mesh_->delete_vertex(this->vertex,false);
            this->vertex.invalidate();
        }
        return this->to_tesselate = false;
    }
}

template< typename TTriMesh, typename TVertexProcessor>
template<class PrecisionFunction>
bool DCOctreeNodeT<TTriMesh, TVertexProcessor>::collapse_st_topology(
        const core::AABBox &bbox, unsigned int position[3],
        unsigned int length, const PrecisionFunction &precision,
        DCOctree *tree, typename PrecisionFunction::Hint hint)
{
    if (!this->is_leaf())
    {
        for (typename array<DCOctreeNode *, 8>::iterator it=this->childs.begin();
             it!=this->childs.end(); ++it)
        {
            (*it)->clear_st(tree->tri_mesh_);
            delete *it;
            *it = NULL;
        }
    }

    // Compute the intersection information on all edges

    Point points[12];

    Vector mass_point = Vector::Zero();

#ifdef USE_QEF
    bool intersect[12];
    Vector normals[12];
    Vector mass_normal = Vector::Zero();
#endif

//    Eigen::Matrix3d A_I = Eigen::Matrix3d::Zero(); //TODO:@todo
//    Vector b_I = Vector::Zero();

#ifdef USE_QEF
    float mp[3]; // this is the minimizer point of the QEF
    int index; // vertex index in PLY file
    float ata[6], atb[3], btb = 0.f; // QEF data

    UNUSED(mp);
    UNUSED(index);
    core::AABBox aabb; //stupid to compute it the way that follow ...
#endif

    unsigned int n_faces = 0;
    for (unsigned int i=0; i<12; ++i)
    {
        const unsigned int i1=edgevmap[i][0], i2=edgevmap[i][1];
        const Scalar v1=this->values[i1], v2=this->values[i2];
        if (v1*v2<=0.0 && (v1!=0.0 || v2!=0.0))
        { // there is an intersection
#ifdef USE_QEF
            intersect[i] = true;
#endif
            n_faces++;

            const Point P1=tree->GetWorldPoint(position[0] + length*vertices_map[i1][0],
                                               position[1] + length*vertices_map[i1][1],
                                               position[2] + length*vertices_map[i1][2]);
            const Point P2=tree->GetWorldPoint(position[0] + length*vertices_map[i2][0],
                                               position[1] + length*vertices_map[i2][1],
                                               position[2] + length*vertices_map[i2][2]);

#ifdef USE_QEF
            aabb = aabb.Enlarge(P1); //stupid
            aabb = aabb.Enlarge(P2); //stupid
#endif

#ifdef USE_BARYCENTER
            //TODO:@todo : to obtain better result, the placement of the point should be improved
            points[i] = P1 + (v1/(v1-v2))*(P2-P1); // beurk ...
#else
            Point pos, neg;
            Scalar epsilon = 0.01;
            //TODO:@todo : the two can be on the iso at the same time ... in this case better to take the middle
            if(fabs(v1) < epsilon) points[i] = P1;
            else if(fabs(v2) < epsilon) points[i] = P2;
            else
            {
                if (v1 < 0.0) { pos = P2; neg = P1; }
                else          { pos = P1; neg = P2; }
                Vector search_dir = neg - pos;
                Scalar norm = search_dir.norm();
//                assert(tree->function->Eval(neg) <= tree->function->iso_value());
//                assert(tree->function->Eval(pos) >= tree->function->iso_value());
                points[i] = Convergence::SafeNewton1DDichotomy(*(tree->function->blobtree_root()),
                                                               pos, search_dir, 0.0, norm, norm*v1/(v1-v2),//0.5*norm,
                                                               tree->function->iso_value(), epsilon*search_dir.norm(), epsilon*0.01);
                Scalar val = fabs(tree->function->EvalMinusIsoValue(points[i]));
//                if(val>0.02) {
//                    ork::Logger::DEBUG_LOGGER->logf("POLYGONIZER", "%f", val);
//                }
            }
#endif
            mass_point += points[i];

#ifdef USE_QEF
            ///TODO:@todo this is not really smart to compute the normal only now since they could be use to check if subdivision is required ...
            normals[i] = -tree->function->EvalGrad(points[i], 0.001); //TODO:@todo : should they be normalized or not ?
//assert(normals[i].norm() > 0.00000001);
            normals[i].normalize(); // apriori oui
            // TODO:@todo : warning if null normal !!!

            mass_normal += normals[i];

//            A_I += normals[i] * normals[i].transpose();
//            b_I += (-normals[i].dot(points[i])) * normals[i];

            // QEF
            ata[ 0 ] += (float) ( normals[i][ 0 ] * normals[i][ 0 ] );
            ata[ 1 ] += (float) ( normals[i][ 0 ] * normals[i][ 1 ] );
            ata[ 2 ] += (float) ( normals[i][ 0 ] * normals[i][ 2 ] );
            ata[ 3 ] += (float) ( normals[i][ 1 ] * normals[i][ 1 ] );
            ata[ 4 ] += (float) ( normals[i][ 1 ] * normals[i][ 2 ] );
            ata[ 5 ] += (float) ( normals[i][ 2 ] * normals[i][ 2 ] );
            double pn = points[i][0] * normals[i][0] + points[i][1] * normals[i][1] + points[i][2] * normals[i][2] ;
            atb[ 0 ] += (float) ( normals[i][ 0 ] * pn ) ;
            atb[ 1 ] += (float) ( normals[i][ 1 ] * pn ) ;
            atb[ 2 ] += (float) ( normals[i][ 2 ] * pn ) ;
            btb += (float) pn * (float) pn ;
#endif
        }
        else
        {
#ifdef USE_QEF
            intersect[i] = false;

            const Point P1=tree->GetWorldPoint(position[0] + length*vertices_map[i1][0],
                                               position[1] + length*vertices_map[i1][1],
                                               position[2] + length*vertices_map[i1][2]);
            const Point P2=tree->GetWorldPoint(position[0] + length*vertices_map[i2][0],
                                               position[1] + length*vertices_map[i2][1],
                                               position[2] + length*vertices_map[i2][2]);
            aabb = aabb.Enlarge(P1); //stupid
            aabb = aabb.Enlarge(P2); //stupid
#endif
        }
    }


    if (n_faces>0)
    {
        mass_point /= ((double)n_faces);

#ifdef USE_QEF
        mass_normal.normalize();


        /// Compute the angle between all gradient : if too important recursively subdivide
        if(length>1) // we do not want to call a subdivide if we are already at the lower level
        {
            for (unsigned int i=0; i<12; ++i) {
                if(intersect[i]) {
                    for (unsigned int j=i+1; j<12; ++j) {
                        if(intersect[j]) {
                            Scalar cos_angle = normals[i].dot(normals[j]);
                            if(cos_angle< 0.87/*0.8*/) //TODO:@todo : this is an arbitrary factor
                            {
                                //tester si profondeur maximal ...
                                return this->to_tesselate = recursive_subdivide_st(bbox,position, length, precision, tree, hint, true);

//                                    return recursive_subdivide_st(bbox,position, length, precision, tree, hint, true);

    //                            if (this->vertex.is_valid())
    //                            {
    //                                tree->tri_mesh_->delete_vertex(this->vertex,false);
    //                                this->vertex.invalidate();
    //                            }
    //                            return this->to_tesselate = false;
                            }
                        }
                    }
                }
            }
        }

//TODO:@todo : temporary test to prevent non manifold triangle that leave the surface in some place
//TODO:@todo : a possible reason of this weird triangle could be a wrong return value or to_tesselate in some place
Point new_point = mass_point;
//if(length>1) // we do not want to call a subdivide if we are already at the lower level
//{
//    Scalar error = tree->function->Eval(new_point);
//    if(error<0.99*tree->function->iso_value())
//    {
//        std::cout<< "too far outside" << std::endl;
//        return this->to_tesselate = recursive_subdivide_st(bbox,position, length, precision, tree, hint, true);
//    }
//}

/*
        // Solve
        // eigen.hpp
        // calculate minimizer point, and return error
        // QEF: ata, atb, btb
        // pt is the average of the intersection points
        // mp is the result
        // box is a bounding-box for this node
        // mat is storage for calcPoint() ?
        float mp[3]; // will contains the results...
        float mat[10] ;
        float tmp[3] = {(float)mass_point[0], (float)mass_point[1], (float)mass_point[2]};
        float error = calcPoint( ata, atb, btb, tmp, mp, aabb, mat ) ;
//        if (error>0.05) {
//            return recursive_subdivide_st(bbox,position, length, precision, tree, hint, true);
////            std::cout<<error<<std::endl;
////            if (this->vertex.is_valid())
////            {
////                tree->tri_mesh_->delete_vertex(this->vertex,false);
////                this->vertex.invalidate();
////            }
////            return this->to_tesselate = false;
//        }

        Point new_point(mp[0], mp[1], mp[2]);
        if(!aabb.Contains(new_point))
            new_point = mass_point;
*/
#else
        Point new_point = mass_point;
#endif

//        // Compute the supporting plane quadric (which purpose is the numerical stability of the system)
//        Eigen::Matrix3d A_S = Eigen::Matrix3d::Zero(); //TODO:@todo
//        Vector b_S = Vector::Zero();

//        for (unsigned int i=0; i<12; ++i)
//        {
//            if (intersect[i])
//            {
//                //TODO:@todo : there is some check to perform on possible alignement
//                Vector e = mass_point - points[i];
////assert(e.norm() > 0.00000001);
//                e.normalize(); //TODO:@todo : should check if it is null ...
//                Vector n_e = mass_normal + normals[i];
//                n_e.normalize();
////assert(e.dot(n_e) != e.norm()*n_e.norm());
//                Vector n_s = n_e.cross(e);
//                n_s.normalize();

////                A_S += n_s * n_s.transpose();
////                b_S += (n_s.dot(mass_point)) * n_s;
//            }
//        }

//        Scalar coeff = 0.5;
        // this coeff should belong to [0;1]
        // when it tend toward zero, the system tend to the original one which could be instable but better reconstruct sharp edges

//        Eigen::Matrix3d A = A_I + coeff*A_S;
//        Vector b = b_I + coeff*b_S;

//        Point new_point = -A_I.colPivHouseholderQr().solve(b_I);
//        Point new_point = -A.colPivHouseholderQr().solve(b);
//        Point new_point = -(A_I.transpose() * A_I).ldlt().solve(A_I.transpose() * b_I);
//Point new_point = mass_point;

//#define TOPOLOGY_CHECK
//TODO:@todo : may cause some problem when called from the topology correction
#ifdef TOPOLOGY_CHECK
        bool topology_check = false;

        Scalar val; Vector grad;
        tree->function->EvalValueAndGrad(new_point, 0.000001, val, grad); //TODO:@todo : should they be normalized or not ?
        grad.normalize();
        //warning : the second test should still be in the cell
        Scalar l = 0.5*tree->GetWorldLength(length);
        if(val>tree->function->iso_value())
        { //test toward outside
//            //TODO:@todo :  should test that the modified point is inside the box ...
            topology_check = (tree->function->EvalMinusIsoValue(new_point-l*grad) > 0.0) ; // true if point is inside
        }
        else
        { //test toward inside
            topology_check = (tree->function->EvalMinusIsoValue(new_point-l*grad) > 0.0) ; // true if point is inside
        }
//        if(val<0.8*tree->function->iso_value()) topology_check = true;

        //TESTER SI DEHORS si oui tester si légèrement vers intérieur est dehors sinon légèrement vers l'ex...

        if(topology_check) {
//            if (this->vertex.is_valid())
//            {
//                tree->tri_mesh_->delete_vertex(this->vertex,false);
//                this->vertex.invalidate();
//            }
//            return this->to_tesselate = false;
//            //TODO:@todo : require additional parameter to collapse function ...
            return recursive_subdivide_st(bbox,position, length, precision, tree, hint, true);
        } else {
#endif
            bool was_valid = this->vertex.is_valid();
            if (was_valid)
                tree->tri_mesh_->point(this->vertex) = new_point;
            else
                this->vertex = tree->tri_mesh_->add_point(new_point);

            ++(tree->nb_evals);
            TVertexProcessor::Process(*(tree->function), tree->tri_mesh_, this->vertex);
            this->to_tesselate = !was_valid;
            return true;
#ifdef TOPOLOGY_CHECK
        }
#endif
    }
    else
    {
        if (this->vertex.is_valid())
        {
            tree->tri_mesh_->delete_vertex(this->vertex,false);
            this->vertex.invalidate();
        }
        return this->to_tesselate = false;
    }


/////////////////////////////////////////////////////////////////////////////////////////////////
/// OLD Version
/////////////////////////////////////////////////////////////////////////////////////////////////
    // Compute vertex position
    //    Point P(0.,0.,0.);
    //    unsigned int n_faces = 0;
    //    for (unsigned int i=0; i<12; ++i)
    //    {
    //        const unsigned int i1=edgevmap[i][0], i2=edgevmap[i][1];
    //        const Scalar v1=this->values[i1], v2=this->values[i2];
    //        if (v1*v2<=0.0 && (v1!=0.0 || v2!=0.0))
    //        {
    //            n_faces++;
    //            const Point P1=tree->GetWorldPoint(position[0] + length*vertices_map[i1][0],
    //                                               position[1] + length*vertices_map[i1][1],
    //                                               position[2] + length*vertices_map[i1][2]);
    //            const Point P2=tree->GetWorldPoint(position[0] + length*vertices_map[i2][0],
    //                                               position[1] + length*vertices_map[i2][1],
    //                                               position[2] + length*vertices_map[i2][2]);
    //            P += P1 + (v1/(v1-v2))*(P2-P1);
    //        }
    //    }
    //    if (n_faces>0)
    //    {
    //        P /= n_faces;
    //        bool was_valid = this->vertex.is_valid();
    //        if (was_valid)
    //            tree->tri_mesh_->point(this->vertex) = P;
    //        else
    //            this->vertex = tree->tri_mesh_->add_vertex(P);

    //        ++(tree->nb_evals);
    //        TVertexProcessor::Process(*(tree->function), tree->tri_mesh_, this->vertex);
    //        this->to_tesselate = !was_valid;
    //        return true;
    //    }
    //    else
    //    {
    //        if (this->vertex.is_valid())
    //        {
    //            tree->tri_mesh_->delete_vertex(this->vertex,false);
    //            this->vertex.invalidate();
    //        }
    //        return this->to_tesselate = false;
    //    }
/////////////////////////////////////////////////////////////////////////////////////////////////




//    bool intersect[12];

//    Point points[12];
//    Vector normals[12];

//    Vector mass_point = Vector::Zero();

//    unsigned int n_faces = 0; //TODO:@todo : what with this name ?
//    for (unsigned int i=0; i<12; ++i)
//    {
//        const unsigned int i1=edgevmap[i][0], i2=edgevmap[i][1];
//        const Scalar v1=this->values[i1], v2=this->values[i2];
//        if (v1*v2<=0.0 && (v1!=0.0 || v2!=0.0))
//        { // there is an intersection
//            const Point P1=tree->GetWorldPoint(position[0] + length*vertices_map[i1][0],
//                                               position[1] + length*vertices_map[i1][1],
//                                               position[2] + length*vertices_map[i1][2]);
//            const Point P2=tree->GetWorldPoint(position[0] + length*vertices_map[i2][0],
//                                               position[1] + length*vertices_map[i2][1],
//                                               position[2] + length*vertices_map[i2][2]);

//            //TODO:@todo : to obtain better result, the placement of the point should be improved
//            points[i] = P1 + (v1/(v1-v2))*(P2-P1); // beurk ...
//            normals[i] = tree->function->EvalGrad(points[i], 0.001); //TODO:@todo : should they be normalized or not ?
//            normals[i].normalize(); // apriori oui

//            mass_point += points[i];

//            intersect[i] = true;
//            n_faces++;
//        }
//        else
//        {
//            intersect[i] = false;
//        }
//    }
//    mass_point /= ((double)n_faces);

////     Vector new_point_normal = Vector::Zero();

/////TODO:@todo : should use eigen ///////
//    double matrix[12][3];
//    double vector[12];
//    int rows = 0;
//////////////////////////////////////////

//    for (unsigned int i=0; i<12; ++i)
//    {
//        if (intersect[i])
//        {
//            const Vector &normal = normals[i];

//            matrix[rows][0] = normal[0];
//            matrix[rows][1] = normal[1];
//            matrix[rows][2] = normal[2];

//            vector[rows] = normal.dot(points[i] - mass_point);
////            Vector p = *points[i].v - massPoint;
////            vector[rows] = (double)(normal * p);

////            new_point_normal += normal; // for now it is correcly recomputed afteward...

//            ++rows;
//        }

//    }
////    new_point_normal.normalize();

//    Point new_point;
//    QEF::evaluate(matrix, vector, rows, &new_point);
//    new_point += mass_point;

//    if (n_faces>0)
//    {
//        bool was_valid = this->vertex.is_valid();
//        if (was_valid)
//            tree->tri_mesh_->point(this->vertex) = new_point;
//        else
//            this->vertex = tree->tri_mesh_->add_vertex(new_point);

//        ++(tree->nb_evals);
//        TVertexProcessor::Process(*(tree->function), tree->tri_mesh_, this->vertex);
//        this->to_tesselate = !was_valid;
//        return true;
//    }
//    else
//    {
//        if (this->vertex.is_valid())
//        {
//            tree->tri_mesh_->delete_vertex(this->vertex,false);
//            this->vertex.invalidate();
//        }
//        return this->to_tesselate = false;
//    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //assert(!vertex.is_valid());
}

template< typename TTriMesh, typename TVertexProcessor>
typename DCOctreeNodeT<TTriMesh, TVertexProcessor>::DCOctreeNode
*DCOctreeNodeT<TTriMesh, TVertexProcessor>::subdivide_to_length_st(
        unsigned int position[3], unsigned int actual_length,
        unsigned int required_length, DCOctree *tree)
{
#ifdef DC_OCTREE_SIZE_CHECKS
    unsigned int actual_position0[3] = { position[0] - (position[0] % actual_length),
                                         position[1] - (position[1] % actual_length),
                                         position[2] - (position[2] % actual_length) };
    assert(this->size_==actual_length);
    for (unsigned int i=0; i<3; ++i)
        assert(this->position_[i] == actual_position0[i]);
#endif //DC_OCTREE_SIZE_CHECKS

    if (actual_length>required_length)
    {
        const unsigned int length2 = actual_length/2;
        const unsigned int child_idx = child_position_mask[(position[0]%actual_length)>=length2 ? 1 : 0]
                                                                                                  [(position[1]%actual_length)>=length2 ? 1 : 0]
                                                                                                                                          [(position[2]%actual_length)>=length2 ? 1 : 0];

        if (this->is_leaf())
        {
            if (this->vertex.is_valid())
            {
                tree->tri_mesh_->delete_vertex(this->vertex,false);
                this->vertex.invalidate();
            }

            unsigned int actual_position[3] = { position[0] - (position[0] % actual_length),
                                                position[1] - (position[1] % actual_length),
                                                position[2] - (position[2] % actual_length) };
            unsigned int child_position[3];
            subdivide(core::AABBox(),actual_position,actual_length,tree);

            for (unsigned int i=0; i<8; ++i)
            {
#ifdef DC_OCTREE_SIZE_CHECKS
                this->childs[i]->position_[0] = actual_position[0] + length2*vertices_map[i][0];
                this->childs[i]->position_[1] = actual_position[1] + length2*vertices_map[i][1];
                this->childs[i]->position_[2] = actual_position[2] + length2*vertices_map[i][2];
                this->childs[i]->size_ = length2;
#endif //DC_OCTREE_SIZE_CHECKS

                if (i!=child_idx || length2==required_length)
                {
                    child_position[0] = actual_position[0] + length2*vertices_map[i][0];
                    child_position[1] = actual_position[1] + length2*vertices_map[i][1];
                    child_position[2] = actual_position[2] + length2*vertices_map[i][2];
                    this->childs[i]->collapse_st(child_position,length2,tree);
                }
            }

            this->to_tesselate = this->contains_vertex();
            if (this->to_tesselate)
            {
                DCOctreeNode *node = this->father;
                while (node != NULL && node->to_tesselate == false)
                {
                    node->to_tesselate = true;
                    node = node->father;
                }
            }
        }

        return this->childs[child_idx]->subdivide_to_length_st(position,length2,required_length,tree);
    }
    else
    {
        return this;
    }
}



////////////////////////////////////////////////////////////////////////////////
///////////////////////// DC_OCTREE IMPLEMENTATIONS ////////////////////////////
////////////////////////////////////////////////////////////////////////////////

template< typename TTriMesh, typename TVertexProcessor>
template <class PrecisionFunction>
bool DCOctreeT<TTriMesh, TVertexProcessor>::IncreasePrecision(const PrecisionFunction &precision)
{
    std::vector<Halfedge*> hh_to_update; // just because needed in the garbage collection function...
    std::vector<Face*> fh_to_update;     // idem
    std::vector<Vertex*> vh_to_update;

    const unsigned int length = 1 << max_depth;
    unsigned int pos[3] = {0,0,0};

    this->nb_evals = 0;

#ifdef DC_OCTREE_MULTI_THREAD_SUPPORT
    this->vertices_table.enable_multi_thread_support(false);
#endif

#ifdef CONVOL_USE_LibCPPOx
    auto timeA = std::chrono::high_resolution_clock::now();
#endif
    this->n_updated_vertices  = tri_mesh_->n_vertices();
    this->n_updated_triangles = tri_mesh_->n_faces();
    const bool precision_increased = this->root->increase_precision_st(pos, length,
                                        precision, this);

#ifdef CONVOL_USE_LibCPPOx
    auto timeB = std::chrono::high_resolution_clock::now();
#endif

    if (precision_increased)
    {
#ifdef CONVOL_USE_LibCPPOx
        this->tree_update_pass_time = std::chrono::duration_cast<std::chrono::milliseconds>(timeB - timeA).count();
#endif
        this->root->get_valid_vertices(vh_to_update);
        tri_mesh_->garbage_collection(vh_to_update, hh_to_update, fh_to_update, true, true, true);

#ifdef CONVOL_USE_LibCPPOx
        timeA = std::chrono::high_resolution_clock::now();
        this->cleaning_pass_time = std::chrono::duration_cast<std::chrono::milliseconds>(timeA - timeB).count();
#endif
        const unsigned int nvertices_before_correction = tri_mesh_->n_vertices();
        this->root->node_proc_topology(length,pos,this);
#ifdef CONVOL_USE_LibCPPOx
        timeB = std::chrono::high_resolution_clock::now();
#endif
        this->corrected_vertices = tri_mesh_->n_vertices() - nvertices_before_correction;
 #ifdef CONVOL_USE_LibCPPOx
        this->correction_pass_time = std::chrono::duration_cast<std::chrono::milliseconds>(timeB - timeA).count();
#endif
        this->root->node_proc_contour(0,tri_mesh_);
 #ifdef CONVOL_USE_LibCPPOx
        timeA = std::chrono::high_resolution_clock::now();
        this->tesselation_pass_time = std::chrono::duration_cast<std::chrono::milliseconds>(timeA - timeB).count();
#endif
        this->n_updated_vertices  = tri_mesh_->n_vertices() - this->n_updated_vertices;
        this->n_updated_triangles = tri_mesh_->n_faces() - this->n_updated_triangles;

        return true;
    }
    return false;
}

template< typename TTriMesh, typename TVertexProcessor>
template <class PrecisionFunction>
void DCOctreeT<TTriMesh, TVertexProcessor>::UpdateSurface(const core::AABBox &bbox,
                                                          const PrecisionFunction &precision,
                                                          bool multithread
                                                          )
{
    std::vector<Halfedge*> hh_to_update; // just because needed in the garbage collection function...
    std::vector<Face*> fh_to_update;     // idem
    std::vector<Edge*> eh_to_update;     // idem
    std::vector<Vertex*> vh_to_update;

    const unsigned int length = 1 << max_depth;
    unsigned int position[3] = { 0, 0, 0 };
    this->nb_evals = 0;


    auto timeA = std::chrono::high_resolution_clock::now();

    this->vertices_table.clear();
    this->root->invalidate_vertices(bbox, position, length, this, vh_to_update);
    tri_mesh_->garbage_collection(vh_to_update, hh_to_update, fh_to_update, eh_to_update, true, true, true, true);
    this->n_updated_vertices  = (uint) tri_mesh_->n_vertices();
    this->n_updated_triangles = (uint) tri_mesh_->n_faces();

    auto timeB = std::chrono::high_resolution_clock::now();
    this->cleaning_pass_time = (uint) std::chrono::duration_cast<std::chrono::milliseconds>(timeB - timeA).count();

    unsigned int pos[3] = {0,0,0};

#ifdef DC_OCTREE_MULTI_THREAD_SUPPORT
    this->vertices_table.enable_multi_thread_support(multithread);
    if (multithread)
        this->root->update_mt(bbox, pos, length, precision, this);
    else
#else
    UNUSED(multithread);
#endif
    this->root->update_st(bbox, pos, length, precision, this);

    timeA = std::chrono::high_resolution_clock::now();
    this->tree_update_pass_time = (uint) std::chrono::duration_cast<std::chrono::milliseconds>(timeA - timeB).count();

    const unsigned int nvertices_before_correction = (uint) tri_mesh_->n_vertices();
    this->root->node_proc_topology(length,position,this);
    this->corrected_vertices = (uint) (tri_mesh_->n_vertices() - nvertices_before_correction);

    timeB = std::chrono::high_resolution_clock::now();
    this->correction_pass_time = (uint) std::chrono::duration_cast<std::chrono::milliseconds>(timeB - timeA).count();

    this->root->node_proc_contour(0,tri_mesh_);

    timeA = std::chrono::high_resolution_clock::now();
    this->tesselation_pass_time = (uint) std::chrono::duration_cast<std::chrono::milliseconds>(timeA - timeB).count();

    this->n_updated_vertices  = (uint) tri_mesh_->n_vertices() - this->n_updated_vertices;
    this->n_updated_triangles = (uint) tri_mesh_->n_faces() - this->n_updated_triangles;
}

template< typename TTriMesh, typename TVertexProcessor>
template<class PrecisionFunction>
void DCOctreeT<TTriMesh, TVertexProcessor>::ResetSurface(const PrecisionFunction &precision,
                                                         core::AABBox bbox, bool multithread
                                                         )
{
    const unsigned int length = 1 << max_depth;
    unsigned int pos[3] = { 0, 0, 0};
    this->nb_evals = 0;
    if (bbox.IsEmpty())
        bbox = GetWorldBoundingBox(pos, length);
	
    auto timeA = std::chrono::high_resolution_clock::now();

    tri_mesh_->clear();
    delete this->root;
    this->root = new DCOctreeNode();
    this->vertices_table.clear();

    auto timeB = std::chrono::high_resolution_clock::now();
    this->cleaning_pass_time = (uint) std::chrono::duration_cast<std::chrono::milliseconds>(timeB - timeA).count();
    ork::Logger::DEBUG_LOGGER->logf("CONVOL", "Cleaning pass time %d", this->cleaning_pass_time);

#ifdef DC_OCTREE_MULTI_THREAD_SUPPORT
    this->vertices_table.enable_multi_thread_support(multithread);
    if (multithread)
        this->root->update_mt(bbox, pos, length, precision, this);
    else
#else
    UNUSED(multithread);
#endif
    this->root->update_st(bbox, pos, length, precision, this);

    timeA = std::chrono::high_resolution_clock::now();
    this->tree_update_pass_time = (uint) std::chrono::duration_cast<std::chrono::milliseconds>(timeA - timeB).count();
    ork::Logger::DEBUG_LOGGER->logf("CONVOL", "tree_update_pass_time %d", this->tree_update_pass_time);

    const unsigned int nvertices_before_correction = (const uint) tri_mesh_->n_vertices();
    this->root->node_proc_topology(length,pos,this);
    this->corrected_vertices = (uint) (tri_mesh_->n_vertices() - nvertices_before_correction);

    timeB = std::chrono::high_resolution_clock::now();
    this->correction_pass_time = (uint) std::chrono::duration_cast<std::chrono::milliseconds>(timeB - timeA).count();
    ork::Logger::DEBUG_LOGGER->logf("CONVOL", "correction_pass_time %d", this->correction_pass_time);

    this->root->node_proc_contour(0,tri_mesh_);

    timeA = std::chrono::high_resolution_clock::now();
    this->tesselation_pass_time = (uint) std::chrono::duration_cast<std::chrono::milliseconds>(timeA - timeB).count();
    ork::Logger::DEBUG_LOGGER->logf("CONVOL", "tesselation_pass_time %d", this->tesselation_pass_time);

    this->n_updated_vertices  = (uint) tri_mesh_->n_vertices();
    this->n_updated_triangles = (uint) tri_mesh_->n_faces();

    ork::Logger::DEBUG_LOGGER->logf("CONVOL", "Updated triangles %d", this->n_updated_triangles);
}

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive


#endif
