/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BloomenthalPolygonizerT.h

   Language: C++

   License: Public Domain

   \author: Jules Bloomenthal
   \author: J. Andreas B�rentzen ( Adapted for C++, 2003)
   \author: Maxime Quiblier (Adapted for Convol, 2011)
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for Bloomenthal public domain PolygonizerT.
                cf. http://www.unchainedgeometry.com/jbloom/
                Description from J. Andreas B�rentzen :
                This is Jules Bloomenthal's implicit surface PolygonizerT from GRAPHICS
                GEMS IV. Bloomenthal's PolygonizerT is still used and the present code
                is simply the original code morphed into C++.

   Limitations: So far, Convol is OpenMesh dependant. Therefore we use OpenMesh.

   @todo Find a way to easily link Convol to other mesh handlers.
   @todo Clean this templated code, it's not really nice.

   Platform Dependencies: None
*/

/* Original Comment (May not be very useful since I destroyed the code structure - Maxime Quiblier)

 * C code from the article
 * "An Implicit Surface PolygonizerT"
 * http::www.unchainedgeometry.com/jbloom/papers/PolygonizerT.pdf
 * by Jules Bloomenthal, jules@bloomenthal.com
 * in "Graphics Gems IV", Academic Press, 1994 */

/* PolygonizerT.c - implicit surface PolygonizerT, translated from Mesa
 *
 * Authored by Jules Bloomenthal, Xerox PARC.
 * Copyright (c) Xerox Corporation, 1991.  All rights reserved.
 * Permission is granted to reproduce, use and distribute this code for
 * any and all purposes, provided that this notice appears in all copies.  */

/* A \brief Explanation
 *
 * The main data structures in the PolygonizerT represent a hexahedral lattice,
 * ie, a collection of semi-adjacent cubes, represented as cube centers, corners,
 * and edges. The centers and corners are three-dimensional indices rerpesented
 * by integer i,j,k. The edges are two three-dimensional indices, represented
 * by integer i1,j1,k1,i2,j2,k2. These indices and associated data are stored
 * in hash tables.
 *
 * The routine first allocates memory for the hash tables for the cube centers,
 * corners, and edges that define the polygonizing lattice. It then finds a start
 * point, ie, the center of the first lattice cell. It pushes this cell onto an
 * initially empty stack of cells awaiting processing. It creates the first cell
 * by computing its eight corners and assigning them an implicit value.
 *
 * The routine then enters a loop in which a cell is popped from the stack,
 * becoming the 'active' cell c. c is (optionally) decomposed (ie, subdivided)
 * into six tetrahedra; within each transverse tetrahedron (ie, those that
 * intersect the surface), one or two triangles are produced.
 *
 * The six faces of c are tested for intersection with the implicit surface; for
 * a transverse face, a new cube is generated and placed on the stack.

 * Some of the more important routines include:
 *
 * testface (called by polygonize): test given face for surface intersection;
 *    if transverse, create new cube by creating four new corners.
 * setcorner (called by polygonize, testface): create new cell corner at given
 *    (i,j,k), compute its implicit value, and add to corners hash table.
 * find (called by polygonize): search for point with given polarity
 * dotet (called by polygonize) set edge vertices, output triangle by
 *    invoking callback
 *
 * The section Cubical Polygonization contains routines to polygonize directly
 * from the lattice cell rather than first decompose it into tetrahedra;
 * dotet, however, is recommended over docube.
 *
 * The section Storage provides routines to handle the linked lists
 * in the hash tables.
 *
 * The section Vertices contains the following routines.
 * vertid (called by dotet): given two corner indices defining a cell edge,
 *    test whether the edge has been stored in the hash table; if so, return its
 *    associated vertex index. If not, compute intersection of edge and implicit
 *    surface, compute associated surface normal, add vertex to mesh array, and
 *    update hash tables
 * converge (called by polygonize, vertid): find surface crossing on edge */

#ifndef CONVOL_BLOOMENTHAL_PolygonizerT_H
#define CONVOL_BLOOMENTHAL_PolygonizerT_H

// Convol dependencies
#include<Convol/include/ConvolRequired.h>
#include<Convol/include/ImplicitSurfaceT.h>
    // Geometry
    #include<Convol/include/geometry/AxisBoundingBoxT.h>
    // tools
    #include<Convol/include/tools/SimpleAxisGridT.h>
//    #include<Convol/include/tools/Array3DT.h>
    #include<Convol/include/tools/ConvergenceTools.h>

    // Polygonizer
    #include<Convol-Tools/include/Polygonizer/MarchingCubesTables.h>
    #include<Convol-Tools/include/Polygonizer/BasicBloomenthalPolygonizerStructureT.h>


// Original algorithm dependencies
#include <deque>
#include <list>
#include <vector>
#include <math.h>
#include <iostream>

//#include <sys/types.h>
//#include <stdlib.h>


namespace Convol {

namespace tools {

    using namespace marchingCubesTables;
    using namespace BasicBPStruct;

/*!
 * \brief Class that perform a polygonisation of a surface by following
 *
 * \tparam TVertexProcessor Static functor that can compute all necessary elements on resulting final vertices. Must define the Traits definition to use here, and the function void TVertexProcessor::Process(const ImplicitSurface implicit_surface, TriMesh* tri_mesh, const VertexHandle& vh ). See Convol::tools::NormalColorsVertexProcessorT for an example of such a functor.
 */

/** PolygonizerT is the class used to perform polygonization.*/
template< typename TTriMesh, typename TVertexProcessor >
class BloomenthalPolygonizerT
{
public:
    typedef typename TVertexProcessor::Traits Traits;

    typedef typename Traits::Scalar Scalar;
    typedef typename Traits::Point Point;
    typedef typename Traits::Normal Normal;
    typedef typename Traits::Vector Vector;

    typedef typename Traits::Color Color;

    typedef typename Traits::ESFScalarProperties ESFScalarProperties;
    typedef typename Traits::ESFVectorProperties ESFVectorProperties;

    typedef ImplicitSurfaceT<Traits> ImplicitSurface;

    typedef AxisBoundingBoxT<Traits> AxisBoundingBox;
    typedef PointCloudT<Traits> PointCloud;

        typedef typename TTriMesh::VertexHandle VertexHandle;
        typedef typename TTriMesh::FaceHandle FaceHandle;
        typedef typename TTriMesh::HalfedgeHandle HalfedgeHandle; // only here because we can't do otherwise

    typedef SimpleAxisGridT<Traits,Scalar> ScalarSimpleAxisGrid;

    typedef CornerElementT<Traits> Corner;
    typedef CornerElementT<Traits> CornerElement;
    typedef EdgeElementT<Traits,VertexHandle> EdgeElement;
    typedef CubeT<Traits> Cube;

    typedef typename CenterTable::CenterList CenterList;

    typedef CornerTableT<Traits> CornerTable;
    typedef typename CornerTable::CornerList CornerList;

    typedef EdgeTableT<Traits,VertexHandle> EdgeTable;
    typedef typename EdgeTable::EdgeList EdgeList;

    enum ToTetraHedralize
    {
        TET = 0,  // use tetrahedral decomposition
        NOTET = 1  // no tetrahedral decomposition
    };


    /*! \brief Constructor of PolygonizerT. The first argument is the ImplicitFunction
     *           that we wish to polygonize. The second argument is the size of the
     *           polygonizing cell. The final arg. is the limit to how far away we will
     *           look for components of the implicit surface.
     */
    BloomenthalPolygonizerT(ImplicitSurface* implicit_surface, Scalar size, Scalar epsilon, TTriMesh* tri_mesh)
        : implicit_surface_(implicit_surface), size_(size), epsilon_(epsilon), tri_mesh_(tri_mesh),
          corners_(/*CornerTable()*/), edges_(/*EdgeTable()*/), centers_(/*CenterTable()*/)
    {
        // This marching cube will use normals and colors. So that's it...
        if(!tri_mesh_->has_vertex_normals())
        {
            tri_mesh_->request_vertex_normals();
        }
        if(!tri_mesh_->has_vertex_colors())
        {
            tri_mesh_->request_vertex_colors();
        }
    }

    ~BloomenthalPolygonizerT()
    {
        ClearTables();
    }

    //
    bool PolygonizeSurface(bool tet);

    //
    void InitCubeListToProcess();
    //
    void March(int mode);

    // Clearing algorithm
    void ClearMesh();
    void ClearTables();
    void ClearAll();

    /** Return number of vertices generated after the polygonization.
            Call this function only when march has been called. */
    unsigned int no_vertices() const
    {
      if(tri_mesh_ == NULL) { return 0; }
      return tri_mesh_->n_vertices();
    }

    ///////////////
    // Modifiers //
    ///////////////

    void set_size(Scalar size)
    {
        size_ = size;
    }

    ///////////////
    // Accessors //
    ///////////////

    ImplicitSurface const* implicit_surface() const { return implicit_surface_; }
    ImplicitSurface* nonconst_implicit_surface() { return implicit_surface_; }

    const BlobtreeRootT<Traits>& blobtree_root() const { return *(implicit_surface_->blobtree_root()); }
    BlobtreeRootT<Traits>& nonconst_blobtree_root() { return *(implicit_surface_->nonconst_blobtree_root()); }

    void setImplicitSurface(ImplicitSurface *surf){ implicit_surface_ = surf; }

private:

    ImplicitSurface* implicit_surface_;

    Scalar size_;         // cube size

    Scalar epsilon_;    // Each vertex of the resulting mesh is guaranted to be epsilon close to the surface.
                        // The method used is a directionnal Newton algorithm.
                        // Note : If epsilon is set < 0.0, Newton will not be used and the guarantee is size/2.0 (linear interpolation)

    TTriMesh* tri_mesh_;

    // Global list of corners (keeps track of memory)
    CornerTable corners_; // corner value hash table
    // NB : it should not be necessary to have pointer here : indeed it is a list so iterator are not invalidated when adding element : should check if it is still the case with  deque
    EdgeTable edges_;
    CenterTable centers_;

    std::deque< Cube > cubes_;                        // active cubes

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// TODO:@todo following function are common with AreaUpdatingBloomenthal (which should probably derived from this one )
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public: //TODO:@todo : should be private
    //
    bool AddCubeToProcess(int i, int j, int k, bool force_set = false);
private:

    Corner* setcorner (int i, int j, int k);

    inline void testface (int i, int j, int k, Cube* old,
                        int face, int c1, int c2, int c3, int c4);

    const VertexHandle& vertid (Corner* c1, Corner* c2);

    inline int dotet (Cube* cube, int c1, int c2, int c3, int c4);

    inline int docube (Cube* cube);

    inline int triangle (const VertexHandle& v1, const VertexHandle& v2, const VertexHandle& v3)
    {
        tri_mesh_->add_face(v1,v2,v3);
        return 1;
    }

};


template< typename TTriMesh, typename TVertexProcessor >
void BloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::InitCubeListToProcess()
{
    // Get points on the surface for each primive.
    PointCloud initial_points;
    implicit_surface_->GetPrimitivesPoints(0.1*size_, initial_points);

    // Get cubes corresponding to initial points on the surface
    for(auto it = initial_points.begin(); it != initial_points.end(); ++it)
    {
        Point &v = initial_points.point(*it);
        // Get cube index
        int i = (int)floor(v[0]/size_ + 0.5);
        int j = (int)floor(v[1]/size_ + 0.5);
        int k = (int)floor(v[2]/size_ + 0.5);

        AddCubeToProcess(i,j,k);
    }
}

template< typename TTriMesh, typename TVertexProcessor >
bool BloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::AddCubeToProcess(int i, int j, int k, bool force_set)
{
    // Check that cube isn't already in the hashset
    if( (!centers_.set_center(i, j, k)) || force_set)
    { // cube doesn't exist yet :  create it
        Cube cube;
        cube.i = i;
        cube.j = j;
        cube.k = k;

        // Compute Corner information
        bool positif = false;
        bool negatif = false;
        for (int n = 0; n < 8; n++)
        {
            cube.corners_[n] = setcorner(i+BIT(n,2), j+BIT(n,1), k+BIT(n,0));
            if(cube.corners_[n]->value < 0.0)
                negatif = true;
            else
                positif = true;
        }

        // Push the cube in the list if needed
        if(positif && negatif)
        { // there is a change of sign on at least one edge
            cubes_.push_front(cube);
            return true;
        }
        else
        {
            // should remove the cube of centers' list (important for auto update stuff)
        }
    }
    return false;
}

template< typename TTriMesh, typename TVertexProcessor >
bool BloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::PolygonizeSurface(bool tet)
{
    ClearAll();

    InitCubeListToProcess();
    March(tet?TET:NOTET);

    return true;
}


template< typename TTriMesh, typename TVertexProcessor >
void BloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::ClearMesh()
{
    tri_mesh_->clear();
    tri_mesh_->garbage_collection();
}

template< typename TTriMesh, typename TVertexProcessor >
void BloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::ClearTables()
{
    cubes_.clear(); // should be empty ...

    corners_.Reset();
    edges_.Reset();
    centers_.Reset();
}

template< typename TTriMesh, typename TVertexProcessor >
void BloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::ClearAll()
{
    ClearMesh();
    ClearTables();
}

    // ----------------------------------------------------------------------

/* setcorner: return corner with the given lattice location
     set (and cache) its function value */
template< typename TTriMesh, typename TVertexProcessor >
typename BloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::Corner* BloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::setcorner (int i, int j, int k)
{
    int index = Convol::tools::BasicBPStruct::HASH(i, j, k);
    typename CornerList::const_iterator it = corners_.table_[index].begin();
    for (; it != corners_.table_[index].end(); ++it)
    {
        if ((*it)->i == i && (*it)->j == j && (*it)->k == k)
        {
            return (*it);
        }
    }

    // Create the new corner ...
    Scalar value = implicit_surface_->EvalMinusIsoValue(Point( ((Scalar)i-.5)*size_, ((Scalar)j-.5)*size_, ((Scalar)k-.5)*size_));
    corners_.table_[index].push_front(new CornerElement(i,j,k,value));
    return corners_.table_[index].front();
}



  /* testface: given cube at lattice (i, j, k), and four corners of face,
   * if surface crosses face, compute other four corners of adjacent cube
   * and add new cube to cube stack */
  template< typename TTriMesh, typename TVertexProcessor >
  void BloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::testface (int i, int j, int k, Cube* old,
                                                    int face, int c1, int c2, int c3, int c4)
  {
    Cube new_obj;

    static int facebit[6] = {2, 2, 1, 1, 0, 0};
    int n, pos = old->corners_[c1]->value > 0.0 ? 1 : 0, bit = facebit[face];

    /* test if no surface crossing, cube out of bounds, or already visited: */
    if ((old->corners_[c2]->value > 0) == pos &&
                (old->corners_[c3]->value > 0) == pos &&
                (old->corners_[c4]->value > 0) == pos) return;
    if (centers_.set_center(i, j, k)) return;

    /* create new_obj cube: */
    new_obj.i = i;
    new_obj.j = j;
    new_obj.k = k;
    for (n = 0; n < 8; n++) new_obj.corners_[n] = 0;
    new_obj.corners_[FLIP(c1, bit)] = old->corners_[c1];
    new_obj.corners_[FLIP(c2, bit)] = old->corners_[c2];
    new_obj.corners_[FLIP(c3, bit)] = old->corners_[c3];
    new_obj.corners_[FLIP(c4, bit)] = old->corners_[c4];
    for (n = 0; n < 8; n++)
      if (new_obj.corners_[n] == 0)
                new_obj.corners_[n] = setcorner(i+BIT(n,2), j+BIT(n,1), k+BIT(n,0));

    // Add new cube to top of stack
    cubes_.push_front(new_obj);
  }


  /* vertid: return index for vertex on edge:
   * c1->value and c2->value are presumed of different sign
   * return saved index if any; else compute vertex and save */
  template< typename TTriMesh, typename TVertexProcessor >
  const typename BloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::VertexHandle& BloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::vertid (Corner* c1, Corner* c2)
  {
    const VertexHandle& v_handle = edges_.getedge(c1->i, c1->j, c1->k, c2->i, c2->j, c2->k);

    if (v_handle.is_valid()) return v_handle;                 /* previously computed */

    Point a( ((Scalar)c1->i-.5)*size_, ((Scalar)c1->j-.5)*size_, ((Scalar)c1->k-.5)*size_);
    Point b( ((Scalar)c2->i-.5)*size_, ((Scalar)c2->j-.5)*size_, ((Scalar)c2->k-.5)*size_);

    Point v;
    if(epsilon_<=0.0)
    {
        Scalar p = c2->value - c1->value;
        if(p != 0.0)
        {
            v = a + (- c1->value/p)*(b-a);
        }
    }
    else
    {
        Point pos, neg;
        if (c1->value < 0.0) {
          pos = b; neg = a;
        }
        else {
          pos = a; neg = b;
        }
        Vector search_dir = neg - pos;
        v = ConvergenceT<Traits>::SafeNewton1DDichotomy(*(implicit_surface_->blobtree_root()),
                    pos,
                    search_dir,
                          0.0,
                          size_,
                    size_*0.5,
                    implicit_surface_->iso_value(),
                    epsilon_,
                    epsilon_*0.001);
    }


    // @todo externalize that, and also optimize (maybe some vector init can be outsided)... ????

    // Save vertex
    VertexHandle new_v_handle = tri_mesh_->add_vertex(v);

    // Compute normal and other vertex attributes ...
    TVertexProcessor::Process(*implicit_surface_, tri_mesh_, new_v_handle);

    return edges_.setedge(c1->i, c1->j, c1->k, c2->i, c2->j, c2->k, new_v_handle);
  }


  /**** Tetrahedral Polygonization ****/

  /* dotet: triangulate the tetrahedron
   * b, c, d should appear clockwise when viewed from a
   * return 0 if client aborts, 1 otherwise */
  template< typename TTriMesh, typename TVertexProcessor >
  int BloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::dotet (Cube* cube, int c1, int c2, int c3, int c4)
  {
    Corner *a = cube->corners_[c1];
    Corner *b = cube->corners_[c2];
    Corner *c = cube->corners_[c3];
    Corner *d = cube->corners_[c4];
    int index = 0, apos, bpos, cpos, dpos;
    if ((apos = (a->value > 0.0)) != 0) index += 8;
    if ((bpos = (b->value > 0.0)) != 0) index += 4;
    if ((cpos = (c->value > 0.0)) != 0) index += 2;
    if ((dpos = (d->value > 0.0)) != 0) index += 1;
    /* index is now 4-bit number representing one of the 16 possible cases */
    // TODO : @todo : The default initialization of the ei has been added to prevent warning
    VertexHandle e1 = VertexHandle(), e2 = VertexHandle(), e3 = VertexHandle(),
                 e4 = VertexHandle(), e5 = VertexHandle(), e6 = VertexHandle();
    if (apos != bpos) e1 = vertid(a, b);
    if (apos != cpos) e2 = vertid(a, c);
    if (apos != dpos) e3 = vertid(a, d);
    if (bpos != cpos) e4 = vertid(b, c);
    if (bpos != dpos) e5 = vertid(b, d);
    if (cpos != dpos) e6 = vertid(c, d);
    /* 14 productive tetrahedral cases (0000 and 1111 do not yield polygons */
    switch (index) {
    case 1:     return triangle(e5, e6, e3);
    case 2:     return triangle(e2, e6, e4);
    case 3:     return triangle(e3, e5, e4) &&
                             triangle(e3, e4, e2);
    case 4:     return triangle(e1, e4, e5);
    case 5:     return triangle(e3, e1, e4) &&
                             triangle(e3, e4, e6);
    case 6:     return triangle(e1, e2, e6) &&
                             triangle(e1, e6, e5);
    case 7:     return triangle(e1, e2, e3);
    case 8:     return triangle(e1, e3, e2);
    case 9:     return triangle(e1, e5, e6) &&
                             triangle(e1, e6, e2);
    case 10: return triangle(e1, e3, e6) &&
                             triangle(e1, e6, e4);
    case 11: return triangle(e1, e5, e4);
    case 12: return triangle(e3, e2, e4) &&
                             triangle(e3, e4, e5);
    case 13: return triangle(e6, e2, e4);
    case 14: return triangle(e5, e3, e6);
    }
    return 1;
  }


  /**** Cubical Polygonization (optional) ****/

  /* docube: triangulate the cube directly, without decomposition */
  template< typename TTriMesh, typename TVertexProcessor >
  int BloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::docube (Cube* cube)
  {
    int index = 0;
    for (int i = 0; i < 8; i++)
      if (cube->corners_[i]->value > 0.0)
          index += (1<<i);

    const IntLists& intlists = get_cubetable_entry(index);
    auto polys = intlists.begin();
    for (; polys != intlists.end(); ++polys)
    {
        auto edges = polys->begin();
        VertexHandle a = VertexHandle(), b = VertexHandle();
        int count = 0;
        for (; edges != polys->end(); ++edges)
        {
            Corner *c1 = cube->corners_[corner1[(*edges)]];
            Corner *c2 = cube->corners_[corner2[(*edges)]];
            VertexHandle c = vertid(c1, c2);
            if (++count > 2 && ! triangle(a, b, c))
                return 0;
            if (count < 3)
                a = b;
            b = c;
        }
    }
    return 1;
  }

  /**** Marching step of Polygonization ****/

  template< typename TTriMesh, typename TVertexProcessor >
  void BloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::March(int mode)
  {
    int noabort;

    while (!cubes_.empty())
    {
        /* process active cubes till none left */
        Cube c = cubes_.front();

        noabort = mode == TET?
            /* either decompose into tetrahedra and polygonize: */
            dotet(&c, LBN, LTN, RBN, LBF) &&
            dotet(&c, RTN, LTN, LBF, RBN) &&
            dotet(&c, RTN, LTN, LTF, LBF) &&
            dotet(&c, RTN, RBN, LBF, RBF) &&
            dotet(&c, RTN, LBF, LTF, RBF) &&
            dotet(&c, RTN, LTF, RTF, RBF)
            :
            /* or polygonize the cube directly: */
            docube(&c);
        if (! noabort) throw std::string("aborted");

        /* pop current cube from stack */
        cubes_.pop_front();

        /* test six face directions, maybe add to stack: */
        testface(c.i-1, c.j, c.k, &c, L, LBN, LBF, LTN, LTF);
        testface(c.i+1, c.j, c.k, &c, R, RBN, RBF, RTN, RTF);
        testface(c.i, c.j-1, c.k, &c, B, LBN, LBF, RBN, RBF);
        testface(c.i, c.j+1, c.k, &c, T, LTN, LTF, RTN, RTF);
        testface(c.i, c.j, c.k-1, &c, N, LBN, LTN, RBN, RTN);
        testface(c.i, c.j, c.k+1, &c, F, LBF, LTF, RBF, RTF);
    }
  }


} // Close namespace tools
} // Close namespace Convol

#endif // CONVOL_BLOOMENTHAL_PolygonizerT_H
