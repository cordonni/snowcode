/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: AreaUpdatingBloomenthalPolygonizerT.h

   Language: C++

   License: Convol Licence

   \author: Amaury Jung
   E-Mail: amaury.jung@inrialpes.fr

   Description: TODO
                WARNING : It is designed to work on its own. When deleted, all vertices it had created are deleted.

   Platform Dependencies: None
*/


#ifndef CONVOL_AREA_UPDATING_DUAL_CONTOURING_H_
#define CONVOL_AREA_UPDATING_DUAL_CONTOURING_H_

//#ifdef CONVOL_USE_LibCPPOx
//#define DC_OCTREE_MULTI_THREAD_SUPPORT
//#endif

// core
#include<core/geometry/AABBox.h>

// Convol dependencies
#include<convol/ConvolRequired.h>
#include<convol/ImplicitSurface.h>

// Convol-Tools dependencies
    // Polygonizer
    #include<convol/tools/polygonizer/AreaUpdatingPolygonizer.h>

    #include<convol/tools/polygonizer/DCOctree/DCOctreeNode.h>
//    #include<convol/tools/polygonizer/DCOctree/DCOctreeParallel.h>

#include <limits>
//#include <type_traits>

#include <chrono>


namespace expressive {
namespace convol {
namespace tools {

template< typename PrecisionFunction>
class FeatureOnlyPrecisionFunction
{
public:
    typedef typename PrecisionFunction::Hint Hint;

    FeatureOnlyPrecisionFunction(PrecisionFunction *precision) :
        precision_(precision){}

    void GetPrecision(const core::AABBox &request,
                      Scalar &feature_precision, Scalar &detail_precision) const
    {
        Scalar ignored_detail_precision;
        precision_->GetPrecision(request,feature_precision,ignored_detail_precision);
        detail_precision = feature_precision;
    }

    void SetPrecisionModifiers(Scalar feature, Scalar detail)
    {
        precision_->SetPrecisionModifiers(feature,detail);
    }

    bool FeaturePrecisionLowerThan(const core::AABBox &request, Scalar precision) const
    {
        return precision_->FeaturePrecisionLowerThan(request,precision);
    }

    bool DetailPrecisionLowerThan(const core::AABBox &request, Scalar precision) const
    {
        return precision_->FeaturePrecisionLowerThan(request,precision);
    }

    bool FeaturePrecisionLowerThan(const core::AABBox &request, Scalar precision, Hint hint) const
    {
        return precision_->FeaturePrecisionLowerThan(request,precision, hint);
    }

    bool DetailPrecisionLowerThan(const core::AABBox &request, Scalar precision, Hint hint) const
    {
        return precision_->FeaturePrecisionLowerThan(request,precision, hint);
    }

    Hint GetDefaultHint() const
    {
        return precision_->GetDefaultHint();
    }

    void UpdateHint(const core::AABBox &bbox, Hint &hint) const
    {
        precision_->UpdateHint(bbox,hint);
    }

    void Update(const core::AABBox &bbox)
    {
        precision_->Update(bbox);
    }

    void Reset(const core::AABBox &bbox)
    {
        precision_->Reset(bbox);
    }
private:
    PrecisionFunction *precision_;
};

template< typename TTriMesh, typename TVertexProcessor, class PrecisionFunction>
class AreaUpdatingDualContouringT  : public AreaUpdatingPolygonizer//T<typename TVertexProcessor::Traits>
{
public:
//    typedef AreaUpdatingPolygonizerT<typename TVertexProcessor::Traits> Base;

    // TVerterProcessor::Traits must be equal to PrecisionFunction::Traits
//    typedef typename std::enable_if< std::is_same<
//            typename TVertexProcessor::Traits,
//            typename PrecisionFunction::Traits>::value ,
//            typename TVertexProcessor::Traits>::type Traits;

    typedef TTriMesh TriMesh;

    typedef DCOctreeT<TTriMesh, TVertexProcessor> DCOctree;


    AreaUpdatingDualContouringT(ImplicitSurface* implicit_surface,
                                std::shared_ptr<TriMesh> tri_mesh,
                                PrecisionFunction *precision,
                                unsigned int max_depth)
        : AreaUpdatingPolygonizer(implicit_surface), octree(implicit_surface, tri_mesh, max_depth), precision_(precision),
          multi_thread_support(false)
    {
// This was moved in the vertices and is no longer in the mesh. This will be checked at compile time now.
//        // This dual contouring will use normals and colors. So that's it...
//        if(!tri_mesh->has_vertex_normals())
//        {
//            tri_mesh->request_vertex_normals();
//        }
//        // @todo : remove it if finally it is decided to set colors externally
//        if(!tri_mesh->has_vertex_colors())
//        {
//            tri_mesh->request_vertex_colors();
//        }
    }

    bool EnhanceSurface()
    {
        return octree.IncreasePrecision(*precision_);
    }

    bool UpdateSurfaceInArea(const core::AABBox& area_to_update,
                             unsigned int /*&prec_time*/,
                             bool coarse_update = false)
    {
        if(area_to_update.IsEmpty())
            return false;

        assert(precision_ != NULL);
        FeatureOnlyPrecisionFunction<PrecisionFunction> feature_only(precision_);
		
//        auto timeA = std::chrono::high_resolution_clock::now();

        lock_mesh();
        precision_->Update(area_to_update);

//        auto timeB = std::chrono::high_resolution_clock::now();
//        prec_time = std::chrono::duration_cast<std::chrono::milliseconds>(timeB - timeA).count();
//        ork::Logger::DEBUG_LOGGER->logf("POLYGONIZER", "UpdatePrecisionData : %f", prec_time);

        if(!this->is_valid_mesh_ || area_to_update.Contains(implicit_surface()->GetAxisBoundingBox()))
        {
            this->is_valid_mesh_ = true;
            if (coarse_update)
                octree.ResetSurface(feature_only);
            else
                octree.ResetSurface(*precision_);
        }
        else
        {
            bool multithread = false;
            if (multi_thread_support)
            {
                core::AABBox global_bbox = octree.GlobalBoundingBox();
                core::AABBox updated_box = area_to_update.Intersection(global_bbox);
                Scalar updated_ratio = ((updated_box.max_[0] - updated_box.min_[0]) / (global_bbox.max_[0] - global_bbox.min_[0]))
                                      *((updated_box.max_[1] - updated_box.min_[1]) / (global_bbox.max_[1] - global_bbox.min_[1]))
                                      *((updated_box.max_[2] - updated_box.min_[2]) / (global_bbox.max_[2] - global_bbox.min_[2]));
                multithread = updated_ratio > 0.2;
            }

            if (coarse_update)
                octree.UpdateSurface(area_to_update,feature_only, multithread);
            else
                octree.UpdateSurface(area_to_update, *precision_, multithread);

        }
        unlock_mesh();

        return true;
    }

    void ResetSurface(const core::AABBox& whole_area, unsigned int /*&prec_time*/)
    {       
//        auto timeA = std::chrono::high_resolution_clock::now();
        lock_mesh();

        precision_->Reset(whole_area);

//        auto timeB = std::chrono::high_resolution_clock::now();
//        prec_time = std::chrono::duration_cast<std::chrono::milliseconds>(timeB - timeA).count();
//        ork::Logger::DEBUG_LOGGER->logf("POLYGONIZER", "UpdatePrecisionData : %f", prec_time);

        octree.SetWorldBoundingBox(whole_area);
        octree.ResetSurface(*precision_,core::AABBox(),multi_thread_support);

        unlock_mesh();
    }

    ///////////////
    // Accessors //
    ///////////////

    void enable_multi_thread_support(bool enable)
    {
#ifndef DC_OCTREE_MULTI_THREAD_SUPPORT
        enable = false;
#endif
        multi_thread_support = enable;
    }

    virtual void setImplicitSurface(ImplicitSurface *surface)
    {
        AreaUpdatingPolygonizer::setImplicitSurface(surface);

        octree.function = surface;
        precision_->SetSurface(surface);
    }

    void setTrimesh(std::shared_ptr<TriMesh> mesh)
    {
        octree.tri_mesh_ = mesh;
    }

//    ImplicitSurface const* implicit_surface() const { return octree.function; }
//    ImplicitSurface* nonconst_implicit_surface() { return octree.function; }

//    const BlobtreeRootT<Traits>& blobtree_root() const { return *(octree.function->blobtree_root()); }
//    BlobtreeRootT<Traits>& nonconst_blobtree_root() { return *(octree.function->nonconst_blobtree_root()); }

    unsigned int no_vertices() const
    {
      if(octree.tri_mesh_ == NULL) { return 0; }
      return octree.tri_mesh_->n_vertices();
    }

    DCOctree const* dc_octree() const  { return &octree; }

    /**
     * @brief lock_mesh
     * pseudo locks and unlocks the mesh so we can safely update it.
     */
    void lock_mesh()
    {
        octree.tri_mesh_->lock();
    }

    void unlock_mesh()
    {
        octree.tri_mesh_->unlock();
    }

private:
    DCOctree octree;
    PrecisionFunction *precision_;
    bool multi_thread_support;
};

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive

#endif // CONVOL_AREA_UPDATING_DUAL_CONTOURING_H_

