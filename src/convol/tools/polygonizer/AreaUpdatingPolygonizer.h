/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: AreaUpdatingPolygonizerT.h

   Language: C++

   License: Convol Licence

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inrialpes.fr

   Description: Header file for a base class describing the main functions
                to be defined in area updating polygonizers.

   Platform Dependencies: None
*/

#ifndef CONVOL_AREA_UPDATING_POLYGONIZER_H_
#define CONVOL_AREA_UPDATING_POLYGONIZER_H_

// core dependencies
    // Geometry
    #include<core/geometry/AABBox.h>

// convol dependencies
#include<convol/ImplicitSurface.h>

namespace expressive {

namespace convol {

namespace tools {

/*!
 * \brief Abstract class for a marching cube that keep a surface and can update it in a given area.
 *
 * \tparam TVertexProcessor Static functor that can compute all necessary elements on resulting final vertices. Must define the Traits definition to use here, and the function void TVertexProcessor::Process(const ImplicitSurface implicit_surface, TriMesh* tri_mesh, const VertexHandle& vh ). See Convol::tools::NormalColorsVertexProcessorT for an example of such a functor.
 */
class CONVOL_API AreaUpdatingPolygonizer
{
public:
//    typedef typename Traits::ESFScalarProperties ESFScalarProperties;
//    typedef typename Traits::ESFVectorProperties ESFVectorProperties;

    AreaUpdatingPolygonizer(ImplicitSurface* implicit_surface)
        : implicit_surface_(implicit_surface), is_valid_mesh_(false)
    {}

//    AreaUpdatingPolygonizerT(ImplicitSurface* implicit_surface, Scalar size, Scalar epsilon, TTriMesh* tri_mesh)
//        : implicit_surface_(implicit_surface), size_(size), epsilon_(epsilon), tri_mesh_(tri_mesh),
//          corners_(/*CornerTable()*/), edges_(/*EdgeTable()*/), centers_(/*CenterTable()*/), n_deleted_faces_(0)
//    {
//        // This marching cube will use normals and colors. So that's it...
//        if(!tri_mesh_->has_vertex_normals())
//        {
//            tri_mesh_->request_vertex_normals();
//        }
//        // @todo : remove it if finally it is decided to set colors externally
//        if(!tri_mesh_->has_vertex_colors())
//        {
//            tri_mesh_->request_vertex_colors();
//        }
//    }

    virtual ~AreaUpdatingPolygonizer()
    {
//        ClearAll();
    }

//    //
//    bool UpdateSurfaceInArea(bool tet, const AxisBoundingBox& area_to_update);

//    // ensure that the value in this area are cleaned (clean mesh and hash_table in this area)
//    void ClearAreaOfInterest(const AxisBoundingBox& area_to_update);
//    //
//    void March(int mode);

//    // Clearing algorithm
//    void ClearAll();

//    //    void GarbageCollection();

//    /** Return number of vertices generated after the polygonization.
//            Call this function only when march has been called. */
//    unsigned int no_vertices() const
//    {
//      if(tri_mesh_ == NULL) { return 0; }
//      return tri_mesh_->n_vertices();
//    }

    ///////////////
    // Modifiers //
    ///////////////

    void InvalidateTriMesh()
    {
        is_valid_mesh_ = false;
    }

    ///////////////
    // Accessors //
    ///////////////

    ImplicitSurface const* implicit_surface() const { return implicit_surface_; }
    ImplicitSurface* nonconst_implicit_surface() { return implicit_surface_; }

    const BlobtreeRoot& blobtree_root() const { return *(implicit_surface_->blobtree_root()); }
    BlobtreeRoot& nonconst_blobtree_root() { return *(implicit_surface_->nonconst_blobtree_root()); }

    virtual void setImplicitSurface(ImplicitSurface *surf){ implicit_surface_ = surf; }

    /**
     * @brief lock_mesh
     * pseudo locks and unlocks the mesh so we can safely update it.
     */
    virtual void lock_mesh() = 0;
    virtual void unlock_mesh() = 0;

protected:
    ImplicitSurface *implicit_surface_;

//    TTriMesh* tri_mesh_; // TODO : can be added if and only if we template the class by the kind of mesh ...

//    uint n_deleted_faces_;

    bool is_valid_mesh_; // used to invalid the mesh. This means the mesh is not in an expected state (Ex : it has been modified externally).
};


} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive


#endif // CONVOL_AREA_UPDATING_POLYGONIZER_H_
