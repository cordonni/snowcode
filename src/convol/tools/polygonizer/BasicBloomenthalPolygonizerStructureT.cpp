#include<convol/tools/polygonizer/BasicBloomenthalPolygonizerStructureT.h>

#include<assert.h>

//  Some additional information about the following code (technical documentation).
//  
//      * Edges numeration :
//          For a cube with i,j,k == 0,0,0 :
//          (0,0,0) < - > (1,0,0) is edge number 0
//          (1,0,0) < - > (1,1,0) is edge number 1
//          (1,1,0) < - > (0,1,0) is edge number 2 
//          (0,1,0) < - > (0,0,0) is edge number 3
//          (0,0,1) < - > (1,0,1) is edge number 4
//          (1,0,1) < - > (1,1,1) is edge number 5
//          (1,1,1) < - > (0,1,1) is edge number 6
//          (0,1,1) < - > (0,0,1) is edge number 7
//          (0,0,0) < - > (0,0,1) is edge number 8
//          (1,0,0) < - > (1,0,1) is edge number 9
//          (1,1,0) < - > (1,1,1) is edge number 10
//          (0,1,0) < - > (0,1,1) is edge number 11
//


namespace expressive {

namespace convol {

namespace tools {

namespace BasicBPStruct {

    using namespace marchingCubesTables;

    const IntLists& get_cubetable_entry(int i)
    {
        //TODO:@todo : this is probably unacceptable if we want to run two marching cube at the same time ...
        static CubeTable c;
        return c.get_lists(i);
    }

    int CenterTable::set_center(int i, int j, int k)
    {
        int index = HASH(i,j,k);

        CenterList::const_iterator q(table_[index].begin()), end(table_[index].end());

        for( ; q != end; ++q)
        {
            if(q->i == i && q->j==j && q->k == k) return 1;
        }

        CenterElement elem(i,j,k);
        table_[index].push_front(elem);
        return 0;
    }


  /* nextcwedge: return next clockwise edge from given edge around given face */
  int CubeTable::nextcwedge (int edge, int face)
  {
      switch (edge) {
      case LB: return (face == L)? LF : BN;
      case LT: return (face == L)? LN : TF;
      case LN: return (face == L)? LB : TN;
      case LF: return (face == L)? LT : BF;
      case RB: return (face == R)? RN : BF;
      case RT: return (face == R)? RF : TN;
      case RN: return (face == R)? RT : BN;
      case RF: return (face == R)? RB : TF;
      case BN: return (face == B)? RB : LN;
      case BF: return (face == B)? LB : RF;
      case TN: return (face == T)? LT : RN;
      case TF: return (face == T)? RT : LF;
      default: assert(false); return BN;
      }
  }

  /* otherface: return face adjoining edge that is not the given face */

  int CubeTable::otherface (int edge, int face)
  {
    int other = leftface[edge];
    return face == other? rightface[edge] : other;
  }

  CubeTable::CubeTable(): ctable(256)
  {
      int i, e, c, done[12], pos[8];
      for (i = 0; i < 256; i++)
      {
          for (e = 0; e < 12; e++)
              done[e] = 0;
          for (c = 0; c < 8; c++)
              pos[c] = BIT(i, c);
          for (e = 0; e < 12; e++)
              if (!done[e] && (pos[corner1[e]] != pos[corner2[e]]))
              {
                  IntList ints;
                  int start = e, edge = e;

                  /* get face that is to right of edge from pos to neg corner: */
                  int face = pos[corner1[e]]? rightface[e] : leftface[e];
                  bool ttrue = true;
                  while (ttrue)
                  {
                      edge = nextcwedge(edge, face);
                      done[edge] = 1;
                      if (pos[corner1[edge]] != pos[corner2[edge]])
                      {
                          ints.push_front(edge);
                          if (edge == start)
                              break;
                          face = otherface(edge, face);
                      }
                  }
                  ctable[i].push_front(ints);
              }
      }
  }

} // Close  namespace BasicBPStruct

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive
