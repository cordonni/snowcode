/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

//#ifndef BMESHPOLYGONIZER_H
//#define BMESHPOLYGONIZER_H

//#include <convol/tools/polygonizer/AreaUpdatingPolygonizer.h>
//#include <convol/skeleton/Skeleton.h>

//#include <core/geometry/AbstractTriMesh.h>

//namespace expressive {

//namespace convol {

//namespace tools {

////template< typename TTriMesh, typename TVertexProcessor >
//class CONVOL_API BMeshPolygonizerT : public AreaUpdatingPolygonizer
//{
//public:
//    typedef core::AbstractTriMesh TTriMesh;
//    typedef typename TTriMesh::VertexHandle VertexHandle;
//    typedef typename TTriMesh::FaceHandle FaceHandle;
////    typedef typename TTriMesh::HalfedgeHandle HalfedgeHandle; // only here because we can't do otherwise

//    struct ResultQuad
//    {
//        Vector v[4];
//        VertexHandle i0, i1, i2, i3;

//        void setVertices(const Vector &a, const Vector &b, const Vector &c, const Vector &d) { v[0] = a; v[1] = b; v[2] = c; v[3] = d; }
//        void setIndices(VertexHandle a, VertexHandle b, VertexHandle c, VertexHandle d) { i0 = a; i1 = b; i2 = c; i3 = d; }
//    };

//    BMeshPolygonizerT(ImplicitSurface* implicit_surface, Skeleton *skeleton, Scalar epsilon, std::shared_ptr<TTriMesh> tri_mesh) :
//        AreaUpdatingPolygonizer(implicit_surface), skeleton_(skeleton), epsilon_(epsilon), tri_mesh_(tri_mesh)
//    {

//        Skeleton::VertexIteratorPtr vertices = skeleton_->GetVertices();
//        while(vertices->HasNext()) {
//            updated_vertices_.insert(vertices->Next()->id());
//        }
//    }

//    Vector Rotate(const Vector &p, const Vector &v, Scalar radians);

//    void UpdateSurfaceInArea(const AABBox& area_to_update);
//    void Sweep(Skeleton::VertexPtr vertex, Skeleton::VertexPtr prev, ResultQuad &result);
//    void MakeSphere(Skeleton::VertexPtr vertex, ResultQuad &result);
//    void MakeCap(Skeleton::VertexPtr vertex, Skeleton::VertexPtr prev, ResultQuad &result);
//    void MakeElbow(Skeleton::VertexPtr vertex, Skeleton::VertexPtr prev, ResultQuad &result);
//    void MakeJoint(Skeleton::VertexPtr vertex, Skeleton::VertexPtr prev, ResultQuad &result);

//    ResultQuad AddSegmentedSweep(ResultQuad &startQuad,
//                                 const Vector &end0, const Vector &end1, const Vector &end2, const Vector &end3,
//                                 Skeleton::VertexPtr vertex,
//                                 Scalar endRadius);
//    void MakeStartOfSweep(Skeleton::VertexPtr vertex, Skeleton::VertexPtr neighbor, Vector &v0, Vector &v1, Vector &v2, Vector &v3);

//    /**
//     * @brief lock_mesh
//     * pseudo locks and unlocks the mesh so we can safely update it.
//     */
//    void lock_mesh()
//    {
//        tri_mesh_->lock();
//    }

//    void unlock_mesh()
//    {
//        tri_mesh_->unlock();
//    }

//protected:

//    Skeleton *skeleton_;

//    Scalar epsilon_;    // Each vertex of the resulting mesh is guaranted to be epsilon close to the surface.

//    std::shared_ptr<TTriMesh> tri_mesh_;

//    std::set<Skeleton::VertexId> updated_vertices_;
//};

//Vector BMeshPolygonizerT::Rotate(const Vector &p, const Vector &v, Scalar radians)
//{
//    Vector temp = p;

//    Vector axisX = ((fabsf(v.dot(Vector(1.0, 0.0, 0.0))) < 0.75) ? Vector(1.0, 0.0, 0.0): Vector(0.0, 1.0, 0.0)).cross(v).normalized();
//    Vector axisY = v.cross(axisX);

//    Scalar x = temp.dot(axisX);
//    Scalar y = temp.dot(axisY);
//    Scalar z = temp.dot(v);
//    Scalar sinA = std::sin(radians);
//    Scalar cosA = std::cos(radians);

//    return axisX * (x * cosA - y * sinA) + axisY * (y * cosA + x * sinA) + v * z;
//}

////template<typename TTriMesh, typename TVertexProcessor>
//void BMeshPolygonizerT::UpdateSurfaceInArea(const AABBox& area_to_update)
//{
//    lock_mesh();

//    tri_mesh_->clear();
//    ResultQuad result;


//    while(updated_vertices_.size() != 0) {
//        std::shared_ptr<Skeleton::Vertex> v = skeleton_->Get(*updated_vertices_.begin());
//        Sweep(v, nullptr, result);
//    }

//    unlock_mesh();
//}

////template<typename TTriMesh, typename TVertexProcessor>
//void BMeshPolygonizerT::Sweep(Skeleton::VertexPtr vertex, Skeleton::VertexPtr prev, ResultQuad &result)
//{
//    ork::Logger::DEBUG_LOGGER->logf("POLYGONIZER", "Sweep %f ::: %d", vertex->internal_id(), (prev == nullptr ? -1 : prev->internal_id()));
//    updated_vertices_.erase(vertex->id());
//    unsigned int edges = vertex->GetEdgeCount();
//    if (edges == 0) {
//        MakeSphere(vertex, result);
//    } else if (edges == 1) {
//        MakeCap(vertex, prev, result);
//    } else if (edges == 2) {
//        MakeElbow(vertex, prev, result);
//    } else {
//        MakeJoint(vertex, prev, result);
//    }
//}

//void BMeshPolygonizerT::MakeSphere(Skeleton::VertexPtr vertex, ResultQuad &result)
//{
//    ork::Logger::DEBUG_LOGGER->logf("POLYGONIZER", "Make sphere %d", vertex->internal_id());
//    const core::WeightedPoint &pos = vertex->pos();
//    Scalar r = pos.weight();
//    Vector x,y,z;
//    x = Vector(r, 0.0, 0.0);
//    y = Vector(0.0, r, 0.0);
//    z = Vector(0.0, 0.0, r);

//    Vector v0, v1, v2, v3, v4, v5, v6, v7;
//    v0 = pos;
//    v1 = pos;
//    v2 = pos;
//    v3 = pos;
//    v4 = pos;
//    v5 = pos;
//    v6 = pos;
//    v7 = pos;

//    v0 += y + x;
//    v0 += z;
//    v1 -= y - x;
//    v1 += z;
//    v2 -= y - x;
//    v2 -= z;
//    v3 += y + x;
//    v3 -= z;
//    v4 += y - x;
//    v4 += z;
//    v5 -= y + x;
//    v5 += z;
//    v6 -= y + x;
//    v6 -= z;
//    v7 += y - x;
//    v7 -= z;

//    VertexHandle vh0 = tri_mesh_->add_vertex(v0);
//    VertexHandle vh1 = tri_mesh_->add_vertex(v1);
//    VertexHandle vh2 = tri_mesh_->add_vertex(v2);
//    VertexHandle vh3 = tri_mesh_->add_vertex(v3);
//    VertexHandle vh4 = tri_mesh_->add_vertex(v4);
//    VertexHandle vh5 = tri_mesh_->add_vertex(v5);
//    VertexHandle vh6 = tri_mesh_->add_vertex(v6);
//    VertexHandle vh7 = tri_mesh_->add_vertex(v7);

//    tri_mesh_->add_face(vh0, vh1, vh2);
//    tri_mesh_->add_face(vh0, vh2, vh3);

//    tri_mesh_->add_face(vh7, vh6, vh5);
//    tri_mesh_->add_face(vh7, vh5, vh4);

//    tri_mesh_->add_face(vh7, vh4, vh0);
//    tri_mesh_->add_face(vh7, vh0, vh3);

//    tri_mesh_->add_face(vh6, vh7, vh3);
//    tri_mesh_->add_face(vh6, vh3, vh2);

//    tri_mesh_->add_face(vh6, vh2, vh1);
//    tri_mesh_->add_face(vh6, vh1, vh5);

//    tri_mesh_->add_face(vh0, vh4, vh5);
//    tri_mesh_->add_face(vh0, vh5, vh1);
//}

//void BMeshPolygonizerT::MakeStartOfSweep(Skeleton::VertexPtr vertex, Skeleton::VertexPtr prev, Vector &v0, Vector &v1, Vector &v2, Vector &v3)
//{
//    //this is an end node. find the local vectors
//    const core::WeightedPoint &p = vertex->pos();
//    Vector boneDirection = prev->pos() - p;
//    Vector x,y,z;
//    x = -boneDirection.normalized();
//    y = (fabsf(x.dot(Vector(0.0, 1.0, 0.0))) < 0.5 ? Vector(0.0, 1.0, 0.0) : Vector(1.0, 0.0, 0.0)).cross(x).normalized();
//    z = x.cross(y).normalized();

//    float r = p.weight();
//    x *= r;
//    y *= r;
//    z *= r;

//    //make the quad cap
//    v0 = p + y + z;
//    v1 = p - y + z;
//    v2 = p - y - z;
//    v3 = p + y - z;
//}

//void BMeshPolygonizerT::MakeCap(Skeleton::VertexPtr vertex, Skeleton::VertexPtr prev, ResultQuad &result)
//{
//    if (prev == nullptr) {
//        MakeSphere(vertex, result);
//        return;
//    }
//    Vector x = -(prev->pos() - vertex->pos()).normalized() * vertex->pos().weight();
//    Vector v0, v1, v2, v3, v4, v5, v6, v7;
//    MakeStartOfSweep(vertex, prev, v4, v5, v6, v7);
//    v0 = v4 + x;
//    v1 = v5 + x;
//    v2 = v6 + x;
//    v3 = v7 + x;

//    VertexHandle vh0 = tri_mesh_->add_vertex(v0);
//    VertexHandle vh1 = tri_mesh_->add_vertex(v1);
//    VertexHandle vh2 = tri_mesh_->add_vertex(v2);
//    VertexHandle vh3 = tri_mesh_->add_vertex(v3);
//    VertexHandle vh4 = tri_mesh_->add_vertex(v4);
//    VertexHandle vh5 = tri_mesh_->add_vertex(v5);
//    VertexHandle vh6 = tri_mesh_->add_vertex(v6);
//    VertexHandle vh7 = tri_mesh_->add_vertex(v7);

//    //add the quad to the cap
//    tri_mesh_->add_quad_face(vh0, vh1, vh2, vh3);

//    //now add the sweep to the center of the sphere
//    tri_mesh_->add_quad_face(vh4, vh5, vh1, vh0);
//    tri_mesh_->add_quad_face(vh5, vh6, vh2, vh1);
//    tri_mesh_->add_quad_face(vh6, vh7, vh3, vh2);
//    tri_mesh_->add_quad_face(vh7, vh4, vh0, vh3);

//    result.setVertices(v4, v5, v6, v7);
//    result.setIndices(vh4, vh5, vh6, vh7);
//}

//BMeshPolygonizerT::ResultQuad BMeshPolygonizerT::AddSegmentedSweep(ResultQuad &startQuad,
//                             const Vector &end0, const Vector &end1, const Vector &end2, const Vector &end3,
//                             Skeleton::VertexPtr startVertex,
//                             Scalar endRadius)
//{
//    const core::WeightedPoint &pos = startVertex->pos();
//    Scalar startRadius = pos.weight();

//    Vector start = (tri_mesh_->point(startQuad.i0) + tri_mesh_->point(startQuad.i1) + tri_mesh_->point(startQuad.i2) + tri_mesh_->point(startQuad.i3)) / 4.0;
//    Vector end = (end0 + end1 + end2 + end3) / 4;
//    Scalar startToEnd = (end - start).norm();
//    const int divisions = std::max(0, (int) ((startToEnd - startRadius - endRadius) / (startRadius + endRadius)));
//    VertexHandle i0 = startQuad.i0;
//    VertexHandle i1 = startQuad.i1;
//    VertexHandle i2 = startQuad.i2;
//    VertexHandle i3 = startQuad.i3;

//    ResultQuad endQuad;

//    for (int i = 0; i <= divisions; i++)
//    {
//        if (i == divisions)
//        {
//            // special-case the end, which is at a different angle
//            endQuad.i0 = tri_mesh_->add_vertex(end0);
//            endQuad.i1 = tri_mesh_->add_vertex(end1);
//            endQuad.i2 = tri_mesh_->add_vertex(end2);
//            endQuad.i3 = tri_mesh_->add_vertex(end3);
//        }
//        else
//        {
//            // calculate how far along the bone we are, starting from after startRadius and ending before endRadius
//            // this will go negative if the spheres are intersecting, but in that case divisions == 0 so it doesn't matter
//            Scalar percent = (divisions > 1) ? (Scalar)i / (Scalar)(divisions - 1) : 0.0;
//            Scalar scale = (startRadius + (startToEnd - startRadius - endRadius) * percent) / startToEnd;
//            Vector offset = start + (end - start) * scale;

//            // interpolate the position along the bone, growing or shrinking based on startRadius, endRadius, and how far along the bone we are
//            scale = (startRadius + (endRadius - startRadius) * scale) / startRadius;
//            endQuad.i0 = tri_mesh_->add_vertex(offset + (startQuad.v[0] - start) * scale);
//            endQuad.i1 = tri_mesh_->add_vertex(offset + (startQuad.v[1] - start) * scale);
//            endQuad.i2 = tri_mesh_->add_vertex(offset + (startQuad.v[2] - start) * scale);
//            endQuad.i3 = tri_mesh_->add_vertex(offset + (startQuad.v[3] - start) * scale);
//        }

//        // generate the quads

//        tri_mesh_->add_quad_face(endQuad.i0, endQuad.i1, i1, i0);
//        tri_mesh_->add_quad_face(endQuad.i1, endQuad.i2, i2, i1);
//        tri_mesh_->add_quad_face(endQuad.i2, endQuad.i3, i3, i2);
//        tri_mesh_->add_quad_face(endQuad.i3, endQuad.i0, i0, i3);

//        i0 = endQuad.i0;
//        i1 = endQuad.i1;
//        i2 = endQuad.i2;
//        i3 = endQuad.i3;
//    }

//    return endQuad;
//}

//void BMeshPolygonizerT::MakeElbow(Skeleton::VertexPtr vertex, Skeleton::VertexPtr prev, ResultQuad &result)
//{
//    Skeleton::VertexPtr next = nullptr;
//    auto neighbors = vertex->GetAdjacentVertices();
//    while (neighbors->HasNext()) {
//        next = neighbors->Next();
//        if (next != prev) {
//            break;
//        }
//    }

//    ResultQuad last;
//    Sweep(next, vertex, last);

//    const core::WeightedPoint &cur_pos = vertex->pos();
//    const core::WeightedPoint &next_pos = next->pos();


//    if (prev == nullptr)
//    {
//        // we are the root node and we have one child, so cap off the root's end
//        float scale = cur_pos.weight() / next_pos.weight();
//        Vector v[4];
//        for (int j = 0; j < 4; j++) {
//            v[j] = cur_pos + (last.v[j] - next_pos) * scale;
//        }
//        ResultQuad tmpQuad = AddSegmentedSweep(last, v[0], v[1], v[2], v[3], next, cur_pos.weight());

//        // int i = mesh.vertices.count() - 4;

//        Vector offset = cur_pos + (cur_pos - next_pos).normalized() * cur_pos.weight();
//        VertexHandle v0 = tri_mesh_->add_vertex(offset + (last.v[0] - next_pos) * scale);
//        VertexHandle v1 = tri_mesh_->add_vertex(offset + (last.v[1] - next_pos) * scale);
//        VertexHandle v2 = tri_mesh_->add_vertex(offset + (last.v[2] - next_pos) * scale);
//        VertexHandle v3 = tri_mesh_->add_vertex(offset + (last.v[3] - next_pos) * scale);

//        tri_mesh_->add_quad_face(tmpQuad.i0, v0, v1, tmpQuad.i1);
//        tri_mesh_->add_quad_face(tmpQuad.i1, v1, v2, tmpQuad.i2);
//        tri_mesh_->add_quad_face(tmpQuad.i2, v2, v3, tmpQuad.i3);
//        tri_mesh_->add_quad_face(tmpQuad.i3, v3, v0, tmpQuad.i0);
//        tri_mesh_->add_quad_face(v0, v3, v2, v1);

//        return;
//    }
//    const core::WeightedPoint &prev_pos = prev->pos();

//    // calculate rotation
//    Vector childDirection = next_pos - cur_pos;
//    Vector parentDirection = prev_pos - cur_pos;
//    Vector rotationAxis = childDirection.cross(parentDirection).normalized();
//    Scalar rotationAngle = -std::acos(-childDirection.dot(parentDirection) / childDirection.norm() / parentDirection.norm());

//    // rotate 50% for elbow
//    Scalar scale = cur_pos.weight() / next_pos.weight();
//    Vector v[4];
//    for (int j = 0; j < 4; j++) {
//        v[j] = cur_pos + Rotate(last.v[j] - next_pos, rotationAxis, rotationAngle / 2.0) * scale;
//    }
//    ResultQuad tmpQuad = AddSegmentedSweep(last, v[0], v[1], v[2], v[3], next, cur_pos.weight());

//    // rotate 100% for the next step
//    for (int j = 0; j < 4; j++) {
//        v[j] = cur_pos + Rotate(last.v[j] - next_pos, rotationAxis, rotationAngle) * scale;
//    }

//    result.setIndices(tmpQuad.i0, tmpQuad.i1, tmpQuad.i2, tmpQuad.i3);
//    result.setVertices(v[0], v[1], v[2], v[3]);
//}

//void BMeshPolygonizerT::MakeJoint(Skeleton::VertexPtr vertex, Skeleton::VertexPtr prev, ResultQuad &result)
//{

//    std::vector<ResultQuad> quads;

//    Skeleton::VertexIteratorPtr neighbors = vertex->GetAdjacentVertices();
//    const core::WeightedPoint &pos = vertex->pos();
//    while (neighbors->HasNext()) {
//        Skeleton::VertexPtr child = neighbors->Next();

//        ResultQuad last;
//        Sweep(child, vertex, last);

//        // move the quad center from child to ball
//        const core::WeightedPoint &child_pos = child->pos();
//        float scale = pos.weight() / child_pos.weight();
//        Vector v[4];
//        for (int j = 0; j < 4; j++) {
//            v[j] = pos + (child_pos - pos).normalized() * pos.weight() + (last.v[j] - child_pos) * scale;
//        }
//        ResultQuad tmpQuad = AddSegmentedSweep(last, v[0], v[1], v[2], v[3], child, pos.weight());

//        // remember the last quad for convex hull
//        quads += tmpQuad;
//    }

//    // if there's a parent, we have to do a join using a convex hull
//    if (prev != nullptr)
//    {
//        // create the quad that will be swept up the parent after this
//        int i = mesh.vertices.count();
//        Vector v0, v1, v2, v3;
////        Ball &parent = mesh.balls[ball.parentIndex];
//        const core::WeightedPoint &parent_pos = prev->pos();
//        Vector offset = (parent_pos - pos).normalized * pos.weight();
//        MakeStartOfSweep(vertex, prev, v0, v1, v2, v3);

//        VertexHandle vh0 = tri_mesh_->add_vertex(v0 + offset);
//        VertexHandle vh1 = tri_mesh_->add_vertex(v1 + offset);
//        VertexHandle vh2 = tri_mesh_->add_vertex(v2 + offset);
//        VertexHandle vh3 = tri_mesh_->add_vertex(v3 + offset);

//        result.setIndices(vh0, vh1, vh2, vh3);
//        result.setVertices(v0, v1, v2, v3);
//        quads += result;
//    }

//    // run the convex hull
//    TTriMesh temp;
//    std::map<Vector, VertexHandle> vectorHandles;
//    foreach (const ResultQuad &quad, quads)
//    {
//        // read the quad vertices
//        Vector v0 = quad.v[0];
//        Vector v1 = quad.v[1];
//        Vector v2 = quad.v[2];
//        Vector v3 = quad.v[3];

//        // hack: temporarily shrink the size of the quad so things are much less likely to intersect
//        Vector center = (v0 + v1 + v2 + v3) / 4;
//        const float percent = 0.99;
//        v0 += (center - v0) * percent;
//        v1 += (center - v1) * percent;
//        v2 += (center - v2) * percent;
//        v3 += (center - v3) * percent;

//        // add the vertices to the input of the convex hull algorithm
//        temp.add_vertex(v0);
//        temp.add_vertex(v1);
//        temp.add_vertex(v2);
//        temp.add_vertex(v3);

//        // and remember what maps where for reconstruction
//        vectorHandles[v0] = quad.i0;
//        vectorHandles[v1] = quad.i1;
//        vectorHandles[v2] = quad.i2;
//        vectorHandles[v3] = quad.i3;
//    }
//    ConvexHull3D::run(temp);
//    foreach (const Triangle &tri, temp.triangles)
//    {
//        Vector3 v0 = temp.vertices[tri.a.index].pos;
//        Vector3 v1 = temp.vertices[tri.b.index].pos;
//        Vector3 v2 = temp.vertices[tri.c.index].pos;

//        // if we generate more vertices, then some of the old vertices were inside the convex hull
//        // we can't deal with that so skip these (don't add any triangles to the mesh with new vertices)
//        if (!indexForVector.contains(v0) ||
//            !indexForVector.contains(v1) ||
//            !indexForVector.contains(v2))
//        {
//            continue;
//        }

//        Triangle tri2(indexForVector[v0], indexForVector[v1], indexForVector[v2]);
//        bool allOnSameQuad = false;

//        // is the triangle all on the same quad?
//        foreach (const Quad &quad, quads)
//        {
//            if (quad.a.index != tri2.a.index && quad.b.index != tri2.a.index && quad.c.index != tri2.a.index && quad.d.index != tri2.a.index) continue;
//            if (quad.a.index != tri2.b.index && quad.b.index != tri2.b.index && quad.c.index != tri2.b.index && quad.d.index != tri2.b.index) continue;
//            if (quad.a.index != tri2.c.index && quad.b.index != tri2.c.index && quad.c.index != tri2.c.index && quad.d.index != tri2.c.index) continue;
//            allOnSameQuad = true;
//            break;
//        }

//        // only add the triangle if it won't be inside the mesh
//        if (!allOnSameQuad) mesh.triangles += tri2;
//    }
//}

//}

//}

//}


//#endif // BMESHPOLYGONIZER_H
