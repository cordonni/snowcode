/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: VertexProcessorT.h

   Language: C++

   License: Convol Licence

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file for default static functors to specialize template polygonizer class.
                Those functors actually define what is done on geometry of resulting meshes.
                Of course you can build your own processors, those are here as examples.
                This one compute informations such as normal and color on vertices of a mesh .

   Platform Dependencies: None
*/

#ifndef CONVOL_VERTEX_PROCESSOR_H_
#define CONVOL_VERTEX_PROCESSOR_H_


// Convol dependencies
#include<convol/ImplicitSurface.h>

// std dependencies 
#include<vector>
//#include<math.h>

//////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : the way properties are handled should probably be changed
///  if the number of properties is not really dynamic then use an array instead of a vector otherwise : ok
//////////////////////////////////////////////////////////////////////////////


namespace expressive {

namespace convol {

namespace tools {


/*!
 * \brief Compute normal and colors for the given vertex.  Rely on the presence of E_Red, E_Green and E_Blue as ScalarField properties in Traits.
 *
 * \tparam TTraits The Traits type that must define : Scalar, Point, Vector, Normal, Color, ESFScalarProperties (among which E_Red, E_Green and E_Blue), ESFVectorProperties
 */
class CONVOL_API NormalColorsVertexProcessor{
public:

//    typedef typename Traits::ESFScalarProperties ESFScalarProperties;
//    typedef typename Traits::ESFVectorProperties ESFVectorProperties;

    template<typename TTriMesh>
    inline static void Process(const ImplicitSurface& implicit_surface, std::shared_ptr<TTriMesh> tri_mesh, const typename TTriMesh::VertexHandle& vh ){

        static const std::vector<ESFScalarProperties> colors = BuildScalarPropertiesVector();
        std::vector<Scalar> scal_prop_res = BuildScalarPropertiesResult(); //TODO:@todo : this should be done through a move operator and move constructors...

#ifdef DIRECTION_PROPERTIES_FOR_GABOR
        static const std::vector<ESFVectorProperties> vector_prop = BuildVectorPropertiesVector(); // if we want to use gabor
        std::vector<Vector> vect_prop_res = BuildVectorPropertiesResult();
#else
        static const std::vector<ESFVectorProperties> vector_prop; // empty we need no vector property
        std::vector<Vector> vect_prop_res;
#endif

        Scalar value_res;
        Vector grad_res;

//        assert(tri_mesh->has_vertex_colors());

        //TODO:@todo : the epsilon for gradient evaluation should not be hardcoded in this function !!!!
        implicit_surface.EvalValueAndGradAndProperties(tri_mesh->point(vh),0.0001/*0.000000001*/, colors, vector_prop, value_res, grad_res, scal_prop_res, vect_prop_res); //TODO : @todo : a changer

        grad_res.normalize();
//        assert(!isnan(grad_res[0]));
//        assert(!isnan(grad_res[1]));
//        assert(!isnan(grad_res[2]));

        tri_mesh->set_normal(vh, -grad_res);
        tri_mesh->set_color(vh, Color(scal_prop_res[0], scal_prop_res[1], scal_prop_res[2]));
//        auto &v = tri_mesh->vertex(vh);
//        v.set_normal(-grad_res);
//        tri_mesh->set_normal(vh, -grad_res);
//        Color col;
//        col[0] = scal_prop_res[0];
//        col[1] = scal_prop_res[1];
//        col[2] = scal_prop_res[2]; //TODO:@todo : beurk ....construire en place !!!
//        v.set_color(scal_prop_res[0], scal_prop_res[1], scal_prop_res[2]);
    }

private:
    inline static std::vector<ESFScalarProperties> BuildScalarPropertiesVector(){
        std::vector<ESFScalarProperties> res;
        res.resize(3);
        res[0] = ExpressiveTraits::E_Red;
        res[1] = ExpressiveTraits::E_Green;
        res[2] = ExpressiveTraits::E_Blue; //TODO:@todo : beurk ... construire en place (initializer liste ...)
        return res;
    }

    inline static std::vector<Scalar> BuildScalarPropertiesResult(){
        std::vector<Scalar> res;
        res.resize(3); //TODO:@todo : beurk ... construire en place (initializer liste ...)
        return res;
    }

    inline static std::vector<ESFVectorProperties> BuildVectorPropertiesVector(){
        std::vector<ESFVectorProperties> res;
        res.resize(1);
        res[0] = ExpressiveTraits::E_Direction; //TODO:@todo : beurk ... construire en place (initializer liste ...)
        return res;
    }
    inline static std::vector<Vector> BuildVectorPropertiesResult(){
        std::vector<Vector> res;
        res.resize(1); //TODO:@todo : beurk ... construire en place (initializer liste ...)
        return res;
    }

};


} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive

#endif // CONVOL_VERTEX_PROCESSOR_H_
