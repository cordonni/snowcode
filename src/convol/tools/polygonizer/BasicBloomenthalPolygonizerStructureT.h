/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BasicBloomenthalPolygonizerStructureT.h

   Language: C++

   License: Convol Licence

   \author: Cedric Zanni, Maxime Quiblier
   E-Mail: cedric.zanni@inrialpes.fr

   Description: TODO : @todo : !!! This code come from ... see bloomenthal file ...

   Platform Dependencies: None
*/

//  Some additional information about the following code (technical documentation).
//  
//      * Edges numeration :
//          For a cube with i,j,k == 0,0,0 :
//          (0,0,0) < - > (1,0,0) is edge number 0
//          (1,0,0) < - > (1,1,0) is edge number 1
//          (1,1,0) < - > (0,1,0) is edge number 2 
//          (0,1,0) < - > (0,0,0) is edge number 3
//          (0,0,1) < - > (1,0,1) is edge number 4
//          (1,0,1) < - > (1,1,1) is edge number 5
//          (1,1,1) < - > (0,1,1) is edge number 6
//          (0,1,1) < - > (0,0,1) is edge number 7
//          (0,0,0) < - > (0,0,1) is edge number 8
//          (1,0,0) < - > (1,0,1) is edge number 9
//          (1,1,0) < - > (1,1,1) is edge number 10
//          (0,1,0) < - > (0,1,1) is edge number 11
//

#ifndef CONVOL_BASIC_BP_STRUCT_H_
#define CONVOL_BASIC_BP_STRUCT_H_

// core dependencies
#include<core/CoreRequired.h>

// convol dependencies
#include<convol/ConvolRequired.h>
    // Polygonizer
    #include<convol/tools/polygonizer/MarchingCubesTables.h>

// std dependencies 
#include<vector>
#include<list>
#include<deque>

namespace expressive {

namespace convol {

namespace tools {

namespace BasicBPStruct {

    using namespace marchingCubesTables;

//    template< typename Traits >
//    struct CornerT {                               // corner of a cube
//        int i, j, k;                                // (i, j, k) is index within lattice
//        typename Traits::Scalar x, y, z, value;        // location and function value
//    };

    struct CornerElement {                    //list of corners
        int i, j, k;                            // corner id
        Scalar value;            // corner value
        CornerElement(int _i, int _j, int _k,
                      Scalar _value):
          i(_i), j(_j), k(_k), value(_value) {}
    };

    struct Cube {                    // partitioning cell (cube)
        int i, j, k;                // lattice location of cube
        CornerElement *corners_[8];    // eight corners
    };

    struct CenterElement {        // list of cube locations
        int i, j, k;                // cube location
        CenterElement(int _i, int _j, int _k)
            : i(_i), j(_j), k(_k) {}
    };

    template< typename TVertexHandle>
    struct EdgeElementT {            // list of edges
        int i1, j1, k1, i2, j2, k2; // edge corner ids
        TVertexHandle v_handle; // vertex handle
    };

    const int RES =    10; /* # converge iterations    */

    const int L =    0;  /* left direction:    -x, -i */
    const int R =    1;  /* right direction:    +x, +i */
    const int B =    2;  /* bottom direction: -y, -j */
    const int T =    3;  /* top direction:    +y, +j */
    const int N =    4;  /* near direction:    -z, -k */
    const int F =    5;  /* far direction:    +z, +k */
    const int LBN =    0;  /* left bottom near corner  */
    const int LBF =    1;  /* left bottom far corner   */
    const int LTN =    2;  /* left top near corner     */
    const int LTF =    3;  /* left top far corner      */
    const int RBN =    4;  /* right bottom near corner */
    const int RBF =    5;  /* right bottom far corner  */
    const int RTN =    6;  /* right top near corner    */
    const int RTF =    7;  /* right top far corner     */
    /* the LBN corner of cube (i, j, k), corresponds with location
    * ((i-.5)*size, (j-.5)*size, (k-.5)*size) */

    const int HASHBIT = 5;
    const int HASHSIZE = (size_t)(1<<(3*HASHBIT));
    const int MASK = ((1<<HASHBIT)-1);

    inline int HASH( int i, int j,int k)
    {
        return ( ((( ((i&MASK)<<HASHBIT) | (j&MASK) )<<HASHBIT) | (k&MASK)) );
    }
    inline int BIT(int i, int bit)
    {
        return (i>>bit)&1;
    }
    // flip the given bit of i
    inline int FLIP(int i, int bit)
    {
        return i^1<<bit;
    }


    template< typename TVertexHandle >
    class EdgeTableT
    {
    public:
        typedef EdgeElementT<TVertexHandle> EdgeElement;
//        typedef std::deque<EdgeElement > EdgeList;
        typedef std::list<EdgeElement> EdgeList;

    public : // TODO:@todo : this is temporary ... !
        std::vector<EdgeList> table_;           /* edge and vertex handle hash table */

        TVertexHandle invalid_handle_;
    public:
        EdgeTableT(): table_(2*HASHSIZE), invalid_handle_(TVertexHandle(-1)) {}
        ~EdgeTableT()
        {
            Reset();
            table_.clear();
        }

        inline const TVertexHandle& setedge (int i1, int j1, int k1,
                                             int i2, int j2, int k2, const TVertexHandle&  v_handle);
        inline const TVertexHandle& getedge (int i1, int j1, int k1,
                                             int i2, int j2, int k2);

        void Reset()
        {
            auto vector_it(table_.begin()), vector_end(table_.end());
            for( ; vector_it != vector_end; ++vector_it)
            {
                (*vector_it).clear();
            }
//          // TODO : @todo : I do not know what is more efficient clear all and resize or clear each EdgeList one by one
//          table_.clear();
//          table_.resize(2*HASHSIZE);
        }
    };

    /* setedge: set vertex id for edge */
    template< typename TVertexHandle >
    const TVertexHandle& EdgeTableT<TVertexHandle>::setedge (int i1, int j1, int k1,
                                  int i2, int j2, int k2, const TVertexHandle& v_handle)
    {
      unsigned int index;

      if (i1>i2 || (i1==i2 && (j1>j2 || (j1==j2 && k1>k2)))) {
        int t=i1; i1=i2; i2=t; t=j1; j1=j2; j2=t; t=k1; k1=k2; k2=t;
      }
      index = HASH(i1, j1, k1) + HASH(i2, j2, k2);

      EdgeElement new_obj;
      new_obj.i1 = i1;
      new_obj.j1 = j1;
      new_obj.k1 = k1;
      new_obj.i2 = i2;
      new_obj.j2 = j2;
      new_obj.k2 = k2;
      new_obj.v_handle = v_handle;
      table_[index].push_front(new_obj);
      return table_[index].front().v_handle;
    }


    /* getedge: return vertex id for edge; return -1 if not set */
    template< typename TVertexHandle>
    const TVertexHandle& EdgeTableT<TVertexHandle>::getedge (int i1, int j1, int k1, int i2, int j2, int k2)
    {
        if (i1>i2 || (i1==i2 && (j1>j2 || (j1==j2 && k1>k2)))) {
            int t=i1; i1=i2; i2=t; t=j1; j1=j2; j2=t; t=k1; k1=k2; k2=t;
        };
        int hashval = HASH(i1, j1, k1)+HASH(i2, j2, k2);
        typename EdgeList::const_iterator q(table_[hashval].begin()), end(table_[hashval].end());
        for (; q != end; ++q)
            if (q->i1 == i1 && q->j1 == j1 && q->k1 == k1 &&
                q->i2 == i2 && q->j2 == j2 && q->k2 == k2)
                    return q->v_handle;
        return invalid_handle_;
    }

    // ----------------------------------------------------------------------

    class CONVOL_API CenterTable
    {
    public:
//        typedef std::deque<CenterElement> CenterList;
        typedef std::list<CenterElement> CenterList;

    public : // TODO : @todo : this is temporary ... !
        std::vector<CenterList> table_; /* center and triangle handle hash table */

    public:
        CenterTable(): table_(HASHSIZE){}

        ~CenterTable()
        {
            Reset();
            table_.clear();
        }

        int set_center(int i, int j, int k);

        void Reset()
        {
            auto vector_it(table_.begin()), vector_end(table_.end());
            for( ; vector_it != vector_end; ++vector_it)
            {
                (*vector_it).clear();
            }
//          // TODO : @todo : I do not know what is more efficient clear all and resize or clear each EdgeList one by one
        }
    };

    class CONVOL_API CornerTable
    {
    public:
//        typedef std::deque<CornerElement*> CornerList;
        typedef std::list<CornerElement*> CornerList;

    public : // TODO : @todo : this is temporary ... !
        std::vector<CornerList> table_;           /* corner hash table */

    public:
        CornerTable(): table_(HASHSIZE) {}

        ~CornerTable()
        {
            Reset();
            table_.clear();
        }

        void Reset()
        {
            auto vector_it(table_.begin()), vector_end(table_.end());
            for( ; vector_it != vector_end; ++vector_it)
            {
//                while(!vector_it->empty())
//                {
//                    delete vector_it->back();
//                    vector_it->pop_back();
//                }
                auto it = vector_it->begin();
                while(it != vector_it->end())
                {
                    delete (*it);
                    it = vector_it->erase(it);
                }
            }
        }
    };




  /**** Tetrahedral Polygonization ****/

  /**** Cubical Polygonization (optional) ****/

  const int LB =    0;  /* left bottom edge    */
  const int LT =    1;  /* left top edge    */
  const int LN =    2;  /* left near edge    */
  const int LF =    3;  /* left far edge    */
  const int RB =    4;  /* right bottom edge */
  const int RT =    5;  /* right top edge    */
  const int RN =    6;  /* right near edge    */
  const int RF =    7;  /* right far edge    */
  const int BN =    8;  /* bottom near edge    */
  const int BF =    9;  /* bottom far edge    */
  const int TN =    10; /* top near edge    */
  const int TF =    11; /* top far edge    */

  typedef std::list<int> IntList;
  typedef std::list<IntList> IntLists;

  class CONVOL_API CubeTable
  {
      std::vector<IntLists> ctable;
      int nextcwedge (int edge, int face);
      int otherface (int edge, int face);

  public:
      CubeTable();

      const IntLists& get_lists(int i) const
      {
          return ctable[i];
      }
  };

  /*            edge: LB, LT, LN, LF, RB, RT, RN, RF, BN, BF, TN, TF */
  const int corner1[12]       = {LBN,LTN,LBN,LBF,RBN,RTN,RBN,RBF,LBN,LBF,LTN,LTF};
  const int corner2[12]       = {LBF,LTF,LTN,LTF,RBF,RTF,RTN,RTF,RBN,RBF,RTN,RTF};
  const int leftface[12]       = {B,  L,  L,  F,  R,  T,  N,  R,  N,  B,  T,  F};
  /* face on left when going corner1 to corner2 */
  const int rightface[12]   = {L,  T,  N,  L,  B,  R,  R,  F,  B,  F,  N,  T};
  /* face on right when going corner1 to corner2 */

  const CONVOL_API IntLists& get_cubetable_entry(int i);



} // Close  namespace BasicBPStruct

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive


#endif // CONVOL_BASIC_BP_STRUCT_H_
