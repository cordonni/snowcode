/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: AreaUpdatingBloomenthalPolygonizerT.h

   Language: C++

   License: Convol Licence

   \author: Cedric Zanni, Maxime Quiblier
   E-Mail: cedric.zanni@inrialpes.fr

   Description: Header file for a special marching cube operating at given resolution but with no space constraint.
                It use the principle of Bloomenthal polygonisation process, ie - it follow the surface

                WARNING : It is designed to work on its own. When deleted, all vertices it had created are deleted. 

   Platform Dependencies: None
*/

//  Some additional information about the following code (technical documentation).
//  
//      * Edges numeration :
//          For a cube with i,j,k == 0,0,0 :
//          (0,0,0) < - > (1,0,0) is edge number 0
//          (1,0,0) < - > (1,1,0) is edge number 1
//          (1,1,0) < - > (0,1,0) is edge number 2 
//          (0,1,0) < - > (0,0,0) is edge number 3
//          (0,0,1) < - > (1,0,1) is edge number 4
//          (1,0,1) < - > (1,1,1) is edge number 5
//          (1,1,1) < - > (0,1,1) is edge number 6
//          (0,1,1) < - > (0,0,1) is edge number 7
//          (0,0,0) < - > (0,0,1) is edge number 8
//          (1,0,0) < - > (1,0,1) is edge number 9
//          (1,1,0) < - > (1,1,1) is edge number 10
//          (0,1,0) < - > (0,1,1) is edge number 11
//

#ifndef CONVOL_AREA_UPDATING_BLOOMENTHAL_H_
#define CONVOL_AREA_UPDATING_BLOOMENTHAL_H_


// core dependencies
    // geometry
    #include<core/geometry/AABBox.h>

// convol dependencies
#include<convol/ImplicitSurface.h>
    // tools
//    #include<Convol/include/tools/SimpleAxisGridT.h>
//    #include<Convol/include/tools/Array3DT.h>
    #include<convol/tools/ConvergenceTools.h>
        // Polygonizer
        #include<convol/tools/polygonizer/AreaUpdatingPolygonizer.h>
        #include<convol/tools/polygonizer/MarchingCubesTables.h>
        #include<convol/tools/polygonizer/BasicBloomenthalPolygonizerStructureT.h>

#include <ork/core/Logger.h>

//// std dependencies
//#include<deque>
//#include<list>
//#include<vector>
//#include<math.h>
//#include<iostream>

// std dependencies
#if defined(CONVOL_WIN_PLATFORM) || defined(CONVOL_MACOS_PLATFORM)
#include<time.h>
#else
#include <chrono>
#endif

namespace expressive {

namespace convol {

namespace tools {

//namespace AreaUpdatingBloomenthal {

//    using namespace marchingCubesTables;
//    using namespace BasicBPStruct;

/*!
 * \brief Abstract class for a marching cube that keep a surface and can update it in a given area.
 *
 * \tparam TVertexProcessor Static functor that can compute all necessary elements on resulting final vertices. Must define the Traits definition to use here, and the function void TVertexProcessor::Process(const ImplicitSurface implicit_surface, TriMesh* tri_mesh, const VertexHandle& vh ). See Convol::tools::NormalColorsVertexProcessorT for an example of such a functor.
 */
template< typename TTriMesh, typename TVertexProcessor >
class AreaUpdatingBloomenthalPolygonizerT : public AreaUpdatingPolygonizer
{
public:
//    typedef typename Traits::ESFScalarProperties ESFScalarProperties;
//    typedef typename Traits::ESFVectorProperties ESFVectorProperties;

        typedef typename TTriMesh::VertexHandle VertexHandle;
        typedef typename TTriMesh::FaceHandle FaceHandle;
        typedef typename TTriMesh::HalfedgeHandle HalfedgeHandle; // only here because we can't do otherwise
        typedef typename TTriMesh::EdgeHandle EdgeHandle;

//    typedef SimpleAxisGridT<Traits,Scalar> ScalarSimpleAxisGrid;

    typedef BasicBPStruct::Cube Cube;
    typedef BasicBPStruct::CornerElement Corner;
//    typedef CornerElement CornerElement;
    typedef BasicBPStruct::EdgeElementT<VertexHandle> EdgeElement;

    typedef typename BasicBPStruct::CenterTable::CenterList CenterList;

//    typedef CornerTable CornerTable;
    typedef typename BasicBPStruct::CornerTable::CornerList CornerList;

    typedef BasicBPStruct::EdgeTableT<VertexHandle> EdgeTable;
    typedef typename EdgeTable::EdgeList EdgeList;

    enum ToTetraHedralize
    {
        TET = 0,  // use tetrahedral decomposition
        NOTET = 1  // no tetrahedral decomposition
    };
    
    AreaUpdatingBloomenthalPolygonizerT(ImplicitSurface* implicit_surface, Scalar size, Scalar epsilon, std::shared_ptr<TTriMesh> tri_mesh)
        : AreaUpdatingPolygonizer(implicit_surface), size_(size), epsilon_(epsilon), tri_mesh_(tri_mesh),
          corners_(/*CornerTable()*/), edges_(/*EdgeTable()*/), centers_(/*CenterTable()*/), n_deleted_faces_(0)
    {
        // This was moved in the vertices and is no longer in the mesh. This will be checked at compile time now.
//        // This marching cube will use normals and colors. So that's it...
//        if(!tri_mesh_->has_vertex_normals())
//        {
//            tri_mesh_->request_vertex_normals();
//        }
//        // @todo : remove it if finally it is decided to set colors externally
//        if(!tri_mesh_->has_vertex_colors())
//        {
//            tri_mesh_->request_vertex_colors();
//        }
    }

    ~AreaUpdatingBloomenthalPolygonizerT()
    {
        // TODO@todo : this line crashes !
//        tri_mesh_->garbage_collection();

        cubes_.clear(); // should be empty ...

        corners_.Reset();
        edges_.Reset();
        centers_.Reset();
    }

    //
    bool UpdateSurfaceInArea(bool tet, const core::AABBox& area_to_update);

    // ensure that the value in this area are cleaned (clean mesh and hash_table in this area)
    void ClearAreaOfInterest(std::shared_ptr<TTriMesh> tri_mesh, const core::AABBox& area_to_update);
    //
    void InitCubeListToProcess(const core::AABBox& area_to_update);
    //
    bool AddCubeToProcess(int i, int j, int k, bool force_set = false);
    //
    void March(std::shared_ptr<TTriMesh> tri_mesh, int mode);
    //
    void ResetSurface();

    // Clearing algorithm
    void ClearAll(std::shared_ptr<TTriMesh> tri_mesh);

    //    void GarbageCollection();

    /** Return number of vertices generated after the polygonization.
            Call this function only when march has been called. */
    unsigned int no_vertices(std::shared_ptr<TTriMesh> tri_mesh) const
    {
      if(tri_mesh == nullptr) { return 0; }
      return tri_mesh->n_vertices();
    }

    ///////////////
    //  getters  //
    ///////////////
    Scalar size() const
    {
        return size_;
    }

    ///////////////
    // Modifiers //
    ///////////////

    void set_size(Scalar size)
    {
        size_ = size;
    }

    /**
     * @brief lock_mesh
     * pseudo locks and unlocks the mesh so we can safely update it.
     */
    void lock_mesh()
    {
        tri_mesh_->lock();
    }

    void unlock_mesh()
    {
        tri_mesh_->unlock();
    }
private:

    Scalar size_;         // cube size

    Scalar epsilon_;    // Each vertex of the resulting mesh is guaranted to be epsilon close to the surface.
                        // The method used is a directionnal Newton algorithm.
                        // Note : If epsilon is set < 0.0, Newton will not be used and the guarantee is size/2.0 (linear interpolation)

    std::shared_ptr<TTriMesh> tri_mesh_;

    BasicBPStruct::CornerTable corners_; // corner value hash table
    // NB : it should not be necessary to have pointer here : indeed it is a list so iterator are not invalidated when adding element : should check if it is still the case with  deque

    EdgeTable edges_;

    BasicBPStruct::CenterTable centers_;

    uint n_deleted_faces_;

    std::deque< BasicBPStruct::Cube > cubes_;                        // active cubes

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////


    Corner* setcorner (int i, int j, int k);

    inline void testface (int i, int j, int k, Cube* old,
                        int face, int c1, int c2, int c3, int c4);

    const VertexHandle& vertid (std::shared_ptr<TTriMesh> tri_mesh, Corner* c1, Corner* c2);

    inline int dotet (std::shared_ptr<TTriMesh> tri_mesh, Cube* cube, int c1, int c2, int c3, int c4);

    inline int docube (std::shared_ptr<TTriMesh> tri_mesh, Cube* cube);

    inline int triangle (std::shared_ptr<TTriMesh> tri_mesh, const VertexHandle& v1, const VertexHandle& v2, const VertexHandle& v3)
    {
        tri_mesh->add_face(v1,v2,v3);
        return 1;
    }

    void UpdateRealTriMesh(std::shared_ptr<TTriMesh> mesh);

};

template< typename TTriMesh, typename TVertexProcessor >
void AreaUpdatingBloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::InitCubeListToProcess(const core::AABBox& area_to_update)
{
    // Get points on the surface for each primive.
    core::PointCloud initial_points;
    AreaUpdatingPolygonizer::implicit_surface_->GetPrimitivesPointsInBoundingBox(area_to_update, 0.1*size_, initial_points);

    // Get cubes corresponding to initial points on the surface
    for(auto it = initial_points.begin(); it != initial_points.end(); ++it)
    {
        Point &v = initial_points.point(*it);
        if(area_to_update.Contains(v))
        {
            // Get cube index
            int i = (int)floor(v[0]/size_ + 0.5);
            int j = (int)floor(v[1]/size_ + 0.5);
            int k = (int)floor(v[2]/size_ + 0.5);

            AddCubeToProcess(i,j,k);
        }
    }
}

template< typename TTriMesh, typename TVertexProcessor >
bool AreaUpdatingBloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::AddCubeToProcess(int i, int j, int k, bool force_set)
{
    // Check that cube isn't already in the hashset
    if( (!centers_.set_center(i, j, k)) || force_set)
    { // cube doesn't exist yet :  create it
        Cube cube;
        cube.i = i;
        cube.j = j;
        cube.k = k;

        // Compute Corner information
        bool positif = false;
        bool negatif = false;
        for (int n = 0; n < 8; n++)
        {
            cube.corners_[n] = setcorner(i+BasicBPStruct::BIT(n,2), j+BasicBPStruct::BIT(n,1), k+BasicBPStruct::BIT(n,0));
            if(cube.corners_[n]->value < 0.0)
                negatif = true;
            else
                positif = true;
        }

        // Push the cube in the list if needed
        if(positif && negatif)
        { // there is a change of sign on at least one edge
            cubes_.push_front(cube);
            return true;
        }
        else
        {
            // should remove the cube of centers' list (important for auto update stuff)
        }
    }
    return false;
}

template< typename TTriMesh, typename TVertexProcessor >
bool AreaUpdatingBloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::UpdateSurfaceInArea(bool tet, const core::AABBox& area_to_update)
{
    // Timer variables
#if defined(CONVOL_WIN_PLATFORM) || defined(CONVOL_MACOS_PLATFORM)
    clock_t start_time, end_time;
    start_time = clock();
#else
    std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
    start_time = std::chrono::high_resolution_clock::now();
#endif

//    lock_mesh();
    std::shared_ptr<TTriMesh> tmp_mesh = tri_mesh_;//tri_mesh_->clone();
    if(area_to_update.IsEmpty()) { /*unlock_mesh(); */return false; }

    if(!this->is_valid_mesh_ || area_to_update.Contains(this->implicit_surface_->GetAxisBoundingBox())){
        ClearAll(tmp_mesh);
        this->is_valid_mesh_ = true;
    }else{
        ClearAreaOfInterest(tmp_mesh, area_to_update);
        //TODO:@todo:correction
    }


    InitCubeListToProcess(area_to_update);
    March(tmp_mesh, tet?TET:NOTET);

//    UpdateRealTriMesh(tmp_mesh);
//    tmp_mesh = nullptr;

    // Timer
#if defined(CONVOL_WIN_PLATFORM) || defined(CONVOL_MACOS_PLATFORM)
    end_time = clock();
    double time_taken = ((double) (end_time - start_time))/((double) CLOCKS_PER_SEC);
#else
    end_time =  std::chrono::high_resolution_clock::now();
    double time_taken = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000.;
#endif
    ork::Logger::INFO_LOGGER->logf("POLYGONIZER", "Time taken : %f", time_taken);

    return true;
}

template< typename TTriMesh, typename TVertexProcessor >
void AreaUpdatingBloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::ClearAreaOfInterest(std::shared_ptr<TTriMesh> tri_mesh, const core::AABBox& area_to_update)
{
    // Delete elements in the specified area
    int min_i = (int) floor(area_to_update.min_[0]/size_);
    int min_j = (int) floor(area_to_update.min_[1]/size_);
    int min_k = (int) floor(area_to_update.min_[2]/size_);
    int max_i = (int) floor(area_to_update.max_[0]/size_)+1;
    int max_j = (int) floor(area_to_update.max_[1]/size_)+1;
    int max_k = (int) floor(area_to_update.max_[2]/size_)+1;

    // set of elements to update (ensure handles are valid after the garbage collection).
    std::vector<HalfedgeHandle*> hh_to_update;  // just because needed in the garbage collection function...
    std::vector<FaceHandle*> fh_to_update;      // just because needed in the garbage collection function...
    std::vector<EdgeHandle*> eh_to_update;      // just because needed in the garbage collection function...
    std::vector<VertexHandle*> vh_to_update;
    vh_to_update.reserve(tri_mesh->n_vertices());


    auto hash_edge_it(edges_.table_.begin()), hash_edge_end(edges_.table_.end());
    for( ; hash_edge_it != hash_edge_end; ++hash_edge_it)
    {
        EdgeList& edge_list = *hash_edge_it;

        auto e_it(edge_list.begin());
        while(e_it != edge_list.end())
        {
            // Test if it should be deleted ...
            if( (     e_it->i1 > min_i && e_it->i1 <= max_i
                  &&  e_it->j1 > min_j && e_it->j1 <= max_j
                  &&  e_it->k1 > min_k && e_it->k1 <= max_k )
                ||
                (     e_it->i2 > min_i && e_it->i2 <= max_i
                  &&  e_it->j2 > min_j && e_it->j2 <= max_j
                  &&  e_it->k2 > min_k && e_it->k2 <= max_k )
               ) // test to be checked ...
            {

               // if(!tri_mesh_->status(e_it->v_handle).deleted())
                    // @todo remettre a true
                    tri_mesh->delete_vertex(e_it->v_handle, false);
                ++n_deleted_faces_;
                // if edges_ table is implemented with a list
                e_it = edge_list.erase(e_it);
//                // if edges_ table is implemented with a deque
//                *e_it = edge_list.back();
//                edge_list.pop_back();
            }
            else
            {
                vh_to_update.push_back(&(e_it->v_handle));
                ++e_it;
            }
        }
    }

    auto hash_corner_it(corners_.table_.begin()), hash_corner_end(corners_.table_.end());
    for( ; hash_corner_it != hash_corner_end; ++hash_corner_it)
    {
        CornerList &corner_list = *hash_corner_it;

        auto c_it(corner_list.begin());
        while(c_it != corner_list.end())
        {
            if(     (*c_it)->i >= min_i && (*c_it)->i <=max_i
                &&  (*c_it)->j >= min_j && (*c_it)->j <=max_j
                &&  (*c_it)->k >= min_k && (*c_it)->k <=max_k )
            {
                delete (*c_it);

                // if corners_ table is implemented with a list
                c_it = corner_list.erase(c_it);
//                // if corners_ table is implemented with a deque
//                *c_it = corner_list.back();
//                corner_list.pop_back();
            }
            else
            {
                ++c_it;
            }
        }
    }

    // iterate over all the hash table and update  ??? is it more efficient than the other way around ??? do not know
    auto hash_center_it(centers_.table_.begin()), hash_center_end(centers_.table_.end());
    for( ; hash_center_it != hash_center_end; ++hash_center_it)
    {
        CenterList& center_list = *hash_center_it;

        auto c_it(center_list.begin());
        while(c_it != center_list.end())
        {
            if(     c_it->i >= min_i && c_it->i <=max_i
                &&  c_it->j >= min_j && c_it->j <=max_j
                &&  c_it->k >= min_k && c_it->k <=max_k )
            {
                if (c_it->i == min_i || c_it->i == max_i
                 || c_it->j == min_j || c_it->j == max_j
                 || c_it->k == min_k || c_it->k == max_k )
                {
                    AddCubeToProcess(c_it->i, c_it->j, c_it->k, true);
                    ++c_it;
                }
                else
                {
                    // if centers_ table is implemented with a list
                    c_it = center_list.erase(c_it);
    //                // if centers_ table is implemented with a deque
    //                *c_it = center_list.back();
    //                center_list.pop_back();
                }
            }
            else
            {
                ++c_it;
            }
        }
    }

//    if(n_deleted_faces_ >= tri_mesh->n_vertices()/2)
//    {
        tri_mesh->garbage_collection(vh_to_update, hh_to_update, fh_to_update, eh_to_update, true, true, true); // pourquoi true true true ???
        n_deleted_faces_ = 0;
//    }
}

template< typename TTriMesh, typename TVertexProcessor >
void AreaUpdatingBloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::ClearAll(std::shared_ptr<TTriMesh> tri_mesh)
{
    tri_mesh->clear();
    tri_mesh->garbage_collection();

    cubes_.clear(); // should be empty ...

    corners_.Reset();
    edges_.Reset();
    centers_.Reset();

    n_deleted_faces_ = 0;
}


template< typename TTriMesh, typename TVertexProcessor >
void AreaUpdatingBloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::ResetSurface()
{
    ork::Logger::INFO_LOGGER->log("POLYGONIZER", "Mesh clear...");

    {
        lock_mesh();
        std::lock_guard<std::mutex> mlock(this->tri_mesh_->get_buffers_mutex());
        ClearAll(this->tri_mesh_);
        unlock_mesh();
    }

    core::AABBox abb = this->implicit_surface_->GetAxisBoundingBox();
    UpdateSurfaceInArea(false, abb);

}

    // ----------------------------------------------------------------------

/* setcorner: return corner with the given lattice location
     set (and cache) its function value */
template< typename TTriMesh, typename TVertexProcessor >
typename AreaUpdatingBloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::Corner* AreaUpdatingBloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::setcorner (int i, int j, int k)
{
    int index = convol::tools::BasicBPStruct::HASH(i, j, k);
    typename CornerList::const_iterator it = corners_.table_[index].begin();
    for (; it != corners_.table_[index].end(); ++it)
    {
        if ((*it)->i == i && (*it)->j == j && (*it)->k == k)
        {
            return (*it);
        }
    }

    // Create the new corner ...
    Scalar value = this->implicit_surface_->EvalMinusIsoValue(Point( ((Scalar)i-.5)*size_, ((Scalar)j-.5)*size_, ((Scalar)k-.5)*size_));
    corners_.table_[index].push_front(new BasicBPStruct::CornerElement(i,j,k,value));
    return corners_.table_[index].front();
}



  /* testface: given cube at lattice (i, j, k), and four corners of face,
   * if surface crosses face, compute other four corners of adjacent cube
   * and add new cube to cube stack */
  template< typename TTriMesh, typename TVertexProcessor >
  void AreaUpdatingBloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::testface (int i, int j, int k, Cube* old,
                                                    int face, int c1, int c2, int c3, int c4)
  {
    Cube new_obj;

    static int facebit[6] = {2, 2, 1, 1, 0, 0};
    int n, pos = old->corners_[c1]->value > 0.0 ? 1 : 0, bit = facebit[face];

    /* test if no surface crossing, cube out of bounds, or already visited: */
    if ((old->corners_[c2]->value > 0) == pos &&
                (old->corners_[c3]->value > 0) == pos &&
                (old->corners_[c4]->value > 0) == pos) return;
    if (centers_.set_center(i, j, k)) return;

    /* create new_obj cube: */
    new_obj.i = i;
    new_obj.j = j;
    new_obj.k = k;
    for (n = 0; n < 8; n++) new_obj.corners_[n] = 0;
    new_obj.corners_[BasicBPStruct::FLIP(c1, bit)] = old->corners_[c1];
    new_obj.corners_[BasicBPStruct::FLIP(c2, bit)] = old->corners_[c2];
    new_obj.corners_[BasicBPStruct::FLIP(c3, bit)] = old->corners_[c3];
    new_obj.corners_[BasicBPStruct::FLIP(c4, bit)] = old->corners_[c4];
    for (n = 0; n < 8; n++)
      if (new_obj.corners_[n] == 0)
                new_obj.corners_[n] = setcorner(i+BasicBPStruct::BIT(n,2), j+BasicBPStruct::BIT(n,1), k+BasicBPStruct::BIT(n,0));

    // Add new cube to top of stack
    cubes_.push_front(new_obj);
  }


  /* vertid: return index for vertex on edge:
   * c1->value and c2->value are presumed of different sign
   * return saved index if any; else compute vertex and save */
  template< typename TTriMesh, typename TVertexProcessor >
  const typename AreaUpdatingBloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::VertexHandle& AreaUpdatingBloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::vertid (std::shared_ptr<TTriMesh> tri_mesh, Corner* c1, Corner* c2)
  {
    const VertexHandle& v_handle = edges_.getedge(c1->i, c1->j, c1->k, c2->i, c2->j, c2->k);

    if (v_handle.is_valid()) return v_handle;                 /* previously computed */

    Point a( ((Scalar)c1->i-.5)*size_, ((Scalar)c1->j-.5)*size_, ((Scalar)c1->k-.5)*size_);
    Point b( ((Scalar)c2->i-.5)*size_, ((Scalar)c2->j-.5)*size_, ((Scalar)c2->k-.5)*size_);

    Point v;
    if(epsilon_<=0.0)
    {
        Scalar p = c2->value - c1->value;
        if(p != 0.0)
        {
            v = a + (- c1->value/p)*(b-a);
        }
    }
    else
    {
        Point pos, neg;
        if (c1->value < 0.0) {
          pos = b; neg = a;
        }
        else {
          pos = a; neg = b;
        }
        Vector search_dir = neg - pos;
        v = Convergence::SafeNewton1DDichotomy(*(this->implicit_surface_->blobtree_root()),
                    pos,
                    search_dir,
                          0.0,
                          size_,
                    size_*0.5,
                    this->implicit_surface_->iso_value(),
                    epsilon_,
                    epsilon_*0.001);
    }


    // @todo externalize that, and also optimize (maybe some vector init can be outsided)... ????

    // Save vertex
    VertexHandle new_v_handle = tri_mesh->add_point(v);

    // Compute normal and other vertex attributes ...
    TVertexProcessor::Process(*(this->implicit_surface_), tri_mesh, new_v_handle);

    return edges_.setedge(c1->i, c1->j, c1->k, c2->i, c2->j, c2->k, new_v_handle);
  }


  /**** Tetrahedral Polygonization ****/

  /* dotet: triangulate the tetrahedron
   * b, c, d should appear clockwise when viewed from a
   * return 0 if client aborts, 1 otherwise */
  template< typename TTriMesh, typename TVertexProcessor >
  int AreaUpdatingBloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::dotet (std::shared_ptr<TTriMesh> tri_mesh, Cube* cube, int c1, int c2, int c3, int c4)
  {
    Corner *a = cube->corners_[c1];
    Corner *b = cube->corners_[c2];
    Corner *c = cube->corners_[c3];
    Corner *d = cube->corners_[c4];
    int index = 0, apos, bpos, cpos, dpos;
    if ((apos = (a->value > 0.0)) != 0) index += 8;
    if ((bpos = (b->value > 0.0)) != 0) index += 4;
    if ((cpos = (c->value > 0.0)) != 0) index += 2;
    if ((dpos = (d->value > 0.0)) != 0) index += 1;
    /* index is now 4-bit number representing one of the 16 possible cases */
    // TODO : @todo : The default initialization of the ei has been added to prevent warning
    VertexHandle e1 = VertexHandle(), e2 = VertexHandle(), e3 = VertexHandle(),
                 e4 = VertexHandle(), e5 = VertexHandle(), e6 = VertexHandle();
    if (apos != bpos) e1 = vertid(tri_mesh, a, b);
    if (apos != cpos) e2 = vertid(tri_mesh, a, c);
    if (apos != dpos) e3 = vertid(tri_mesh, a, d);
    if (bpos != cpos) e4 = vertid(tri_mesh, b, c);
    if (bpos != dpos) e5 = vertid(tri_mesh, b, d);
    if (cpos != dpos) e6 = vertid(tri_mesh, c, d);
    /* 14 productive tetrahedral cases (0000 and 1111 do not yield polygons */
    switch (index) {
    case 1:     return triangle(tri_mesh, e5, e6, e3);
    case 2:     return triangle(tri_mesh, e2, e6, e4);
    case 3:     return triangle(tri_mesh, e3, e5, e4) &&
                             triangle(tri_mesh, e3, e4, e2);
    case 4:     return triangle(tri_mesh, e1, e4, e5);
    case 5:     return triangle(tri_mesh, e3, e1, e4) &&
                             triangle(tri_mesh, e3, e4, e6);
    case 6:     return triangle(tri_mesh, e1, e2, e6) &&
                             triangle(tri_mesh, e1, e6, e5);
    case 7:     return triangle(tri_mesh, e1, e2, e3);
    case 8:     return triangle(tri_mesh, e1, e3, e2);
    case 9:     return triangle(tri_mesh, e1, e5, e6) &&
                             triangle(tri_mesh, e1, e6, e2);
    case 10: return triangle(tri_mesh, e1, e3, e6) &&
                             triangle(tri_mesh, e1, e6, e4);
    case 11: return triangle(tri_mesh, e1, e5, e4);
    case 12: return triangle(tri_mesh, e3, e2, e4) &&
                             triangle(tri_mesh, e3, e4, e5);
    case 13: return triangle(tri_mesh, e6, e2, e4);
    case 14: return triangle(tri_mesh, e5, e3, e6);
    }
    return 1;
  }


  /**** Cubical Polygonization (optional) ****/

  /* docube: triangulate the cube directly, without decomposition */
  template< typename TTriMesh, typename TVertexProcessor >
  int AreaUpdatingBloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::docube (std::shared_ptr<TTriMesh> tri_mesh, Cube* cube)
  {
    int index = 0;
    for (int i = 0; i < 8; i++)
      if (cube->corners_[i]->value > 0.0)
          index += (1<<i);

    const BasicBPStruct::IntLists& intlists = BasicBPStruct::get_cubetable_entry(index);
    auto polys = intlists.begin();
    for (; polys != intlists.end(); ++polys)
    {
        auto edges = polys->begin();
        VertexHandle a = VertexHandle(), b = VertexHandle();
        int count = 0;
        for (; edges != polys->end(); ++edges)
        {
            Corner *c1 = cube->corners_[BasicBPStruct::corner1[(*edges)]];
            Corner *c2 = cube->corners_[BasicBPStruct::corner2[(*edges)]];
            VertexHandle c = vertid(tri_mesh, c1, c2);
            if (++count > 2 && ! triangle(tri_mesh, a, b, c))
                return 0;
            if (count < 3)
                a = b;
            b = c;
        }
    }
    return 1;
  }

  /**** Marching step of Polygonization ****/

  template< typename TTriMesh, typename TVertexProcessor >
  void AreaUpdatingBloomenthalPolygonizerT<TTriMesh,TVertexProcessor>::March(std::shared_ptr<TTriMesh> tri_mesh, int mode)
  {
    int noabort;

    while (!cubes_.empty())
    {
        /* process active cubes till none left */
        Cube c = cubes_.front();

        noabort = mode == TET?
            /* either decompose into tetrahedra and polygonize: */
            dotet(tri_mesh, &c, BasicBPStruct::LBN, BasicBPStruct::LTN, BasicBPStruct::RBN, BasicBPStruct::LBF) &&
            dotet(tri_mesh, &c, BasicBPStruct::RTN, BasicBPStruct::LTN, BasicBPStruct::LBF, BasicBPStruct::RBN) &&
            dotet(tri_mesh, &c, BasicBPStruct::RTN, BasicBPStruct::LTN, BasicBPStruct::LTF, BasicBPStruct::LBF) &&
            dotet(tri_mesh, &c, BasicBPStruct::RTN, BasicBPStruct::RBN, BasicBPStruct::LBF, BasicBPStruct::RBF) &&
            dotet(tri_mesh, &c, BasicBPStruct::RTN, BasicBPStruct::LBF, BasicBPStruct::LTF, BasicBPStruct::RBF) &&
            dotet(tri_mesh, &c, BasicBPStruct::RTN, BasicBPStruct::LTF, BasicBPStruct::RTF, BasicBPStruct::RBF)
            :
            /* or polygonize the cube directly: */
            docube(tri_mesh, &c);
        if (! noabort) throw std::string("aborted");

        /* pop current cube from stack */
        cubes_.pop_front();

        /* test six face directions, maybe add to stack: */
        testface(c.i-1, c.j, c.k, &c, BasicBPStruct::L, BasicBPStruct::LBN, BasicBPStruct::LBF, BasicBPStruct::LTN, BasicBPStruct::LTF);
        testface(c.i+1, c.j, c.k, &c, BasicBPStruct::R, BasicBPStruct::RBN, BasicBPStruct::RBF, BasicBPStruct::RTN, BasicBPStruct::RTF);
        testface(c.i, c.j-1, c.k, &c, BasicBPStruct::B, BasicBPStruct::LBN, BasicBPStruct::LBF, BasicBPStruct::RBN, BasicBPStruct::RBF);
        testface(c.i, c.j+1, c.k, &c, BasicBPStruct::T, BasicBPStruct::LTN, BasicBPStruct::LTF, BasicBPStruct::RTN, BasicBPStruct::RTF);
        testface(c.i, c.j, c.k-1, &c, BasicBPStruct::N, BasicBPStruct::LBN, BasicBPStruct::LTN, BasicBPStruct::RBN, BasicBPStruct::RTN);
        testface(c.i, c.j, c.k+1, &c, BasicBPStruct::F, BasicBPStruct::LBF, BasicBPStruct::LTF, BasicBPStruct::RBF, BasicBPStruct::RTF);
    }
}

template<typename  TTriMesh, typename TVertexProcessor>
void AreaUpdatingBloomenthalPolygonizerT<TTriMesh, TVertexProcessor>::UpdateRealTriMesh(std::shared_ptr<TTriMesh> mesh)
{
    lock_mesh();
    std::lock_guard<std::mutex> mlock(this->tri_mesh_->get_buffers_mutex());
    this->tri_mesh_->copyDataFrom(mesh.get());
    unlock_mesh();
}

//} // Close  namespace AreaUpdatingBloomenthal


} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive


#endif // CONVOL_AREA_BLOOMENTHAL_H_
