/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: FBVHKernelEval.h

   Language: C++

   License: Convol license

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Fitted BVH with function to evaluate a scalar field thanks
                to a single kernel.

   Limitations: 3D only

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_FBVH_KERNEL_EVAL_H_
#define CONVOL_FBVH_KERNEL_EVAL_H_

#include <core/CoreRequired.h>
    #include <core/geometry/OptWeightedSegment.h>


#include <convol/ConvolRequired.h>
    #include <convol/tools/FittedBVH.h>
    #include <convol/properties/Properties1D.h>

namespace expressive {

namespace convol {


namespace computeProperties { // use anonymous namespace if possible ?

// I really do not like it ....
template < typename PairGeomProp >
/*CONVOL_API*/ void ComputeProperties(const PairGeomProp& pair,
                                  const Point& /*point*/,
                                  const std::vector<ESFScalarProperties>& scal_prop_ids,
                                  const std::vector<ESFVectorProperties>& vect_prop_ids,
                                  std::vector<Scalar>& scal_prop_res,
                                  std::vector<Vector>& vect_prop_res)
{
    pair.second.InlineEvalProperties(scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
}

template <>
CONVOL_API void ComputeProperties< std::pair<core::OptWeightedSegment, Properties1D> > (const std::pair<core::OptWeightedSegment, Properties1D>& pair,
                                                                                        const Point& point,
                                                                                        const std::vector<ESFScalarProperties>& scal_prop_ids,
                                                                                        const std::vector<ESFVectorProperties>& vect_prop_ids,
                                                                                        std::vector<Scalar>& scal_prop_res,
                                                                                        std::vector<Vector>& vect_prop_res);
} // end namespace computeProperties


template < typename DataType, typename TFunctorKernel >
class FBVHKernelEvalT : public FittedBVH<DataType>
{
public:
//    typedef typename std::vector< std::pair<AABBox, DataType> > DataVector;
    typedef typename FittedBVH<DataType>::CDataIterator CDataIterator;

    FBVHKernelEvalT(unsigned int k) : FittedBVH<DataType>(k) {}

    ~FBVHKernelEvalT() {}

    ////////////////////////////
    /// Evaluation functions ///
    ////////////////////////////

    inline Scalar Eval (const Point& point, const TFunctorKernel &kernel) const
    {
        Scalar f_res = 0.0;
        CDataIterator begin_data;
        CDataIterator end_data;
        if(this->GetDataIterator(point, begin_data, end_data)) {
            for(auto it = begin_data; it<end_data; ++it) {
                if(it->first.Contains(point)) {
                    f_res += kernel.Eval(it->second.first, point);
                }
            }
        }
        return f_res;
    }

    inline Vector EvalGrad (const Point& point, Scalar epsilon_grad, const TFunctorKernel &kernel) const
    {
        Vector grad_res = Vector::Zero();
        CDataIterator begin_data;
        CDataIterator end_data;
        if(this->GetDataIterator(point, begin_data, end_data)) {
            for(auto it = begin_data; it<end_data; ++it) {
                if(it->first.Contains(point)) {
                    grad_res += kernel.EvalGrad(it->second.first, point, epsilon_grad);
                }
            }
        }
        return grad_res;
    }

    inline void EvalValueAndGrad (const Point& point, Scalar epsilon_grad,
                                  Scalar &value_res, Vector &grad_res,
                                  const TFunctorKernel &kernel) const
    {
        Scalar f_tmp; Vector grad_tmp;

        value_res = 0.0;
        grad_res = Vector::Zero();
        CDataIterator begin_data;
        CDataIterator end_data;
        if(this->GetDataIterator(point, begin_data, end_data)) {
            for(auto it = begin_data; it<end_data; ++it) {
                if(it->first.Contains(point)) { // probably have to go through a get ... (due to properties)
                    kernel.EvalValueAndGrad(it->second.first, point, epsilon_grad, f_tmp, grad_tmp);
                    value_res += f_tmp;
                    grad_res += grad_tmp;
                }
            }
        }
    }

    inline void EvalValueAndGradAndProperties (const Point& point, Scalar epsilon_grad,
                                               Scalar &value_res, Vector &grad_res,
                                               const std::vector<ESFScalarProperties>& scal_prop_ids,
                                               const std::vector<ESFVectorProperties>& vect_prop_ids,
                                               std::vector<Scalar>& scal_prop_res,
                                               std::vector<Vector>& vect_prop_res,
                                               const TFunctorKernel &kernel) const
    {
        std::vector<Scalar> scal_prop_aux;
        scal_prop_aux.resize(scal_prop_ids.size());
        std::vector<Vector> vect_prop_aux;
        vect_prop_aux.resize(vect_prop_ids.size());
        Scalar f_tmp; Vector grad_tmp;

        value_res = 0.0;
        grad_res = Vector::Zero();

        CDataIterator begin_data;
        CDataIterator end_data;
        if(this->GetDataIterator(point, begin_data, end_data)) {
            for(auto it = begin_data; it<end_data; ++it) {
                if(it->first.Contains(point)) {
                    kernel.EvalValueAndGrad(it->second.first, point, epsilon_grad, f_tmp, grad_tmp);
                    value_res += f_tmp;
                    grad_res += grad_tmp;

                    computeProperties::ComputeProperties<DataType> (it->second, point,
                                                                    scal_prop_ids, vect_prop_ids,
                                                                    scal_prop_aux, vect_prop_aux);

                    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i) {
                        scal_prop_res[i] += f_tmp*scal_prop_aux[i];
                    }
                    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i) {
                        vect_prop_res[i] += f_tmp*vect_prop_aux[i];
                    }
                }
            }
        }
    }



    inline void EvalHomotheticValues (const Point& point, Scalar &f_res, Vector &grad_res, const TFunctorKernel &kernel) const
    {
        Scalar f_tmp; Vector grad_tmp;

        f_res = 0.0;
        grad_res = Vector::Zero();

        CDataIterator begin_data;
        CDataIterator end_data;
        if(this->GetDataIterator(point, begin_data, end_data)) {
            for(auto it = begin_data; it<end_data; ++it) {
                if(it->first.Contains(point)) {
                    kernel.EvalHomotheticValues(it->second.first, point, f_tmp, grad_tmp);
                    f_res += f_tmp;
                    grad_res += grad_tmp;
                }
            }
        }
    }

    inline void EvalHomotheticValuesAndProperties (const Point& point,
                                                   Scalar &f_res, Vector &grad_res,
                                                   const std::vector<ESFScalarProperties>& scal_prop_ids,
                                                   const std::vector<ESFVectorProperties>& vect_prop_ids,
                                                   std::vector<Scalar>& scal_prop_res,
                                                   std::vector<Vector>& vect_prop_res,
                                                   const TFunctorKernel &kernel) const
    {
        Scalar f_tmp; Vector grad_tmp;

        std::vector<Scalar> scal_prop_aux;
        scal_prop_aux.resize(scal_prop_ids.size());
        std::vector<Vector> vect_prop_aux;
        vect_prop_aux.resize(vect_prop_ids.size());

        f_res = 0.0;
        grad_res = Vector::Zero();

        CDataIterator begin_data;
        CDataIterator end_data;
        if(this->GetDataIterator(point, begin_data, end_data)) {
            for(auto it = begin_data; it<end_data; ++it) {
                if(it->first.Contains(point)) {
                    kernel.EvalHomotheticValues(it->second.first, point, f_tmp, grad_tmp);
                    f_res += f_tmp;
                    grad_res += grad_tmp;

                    computeProperties::ComputeProperties<DataType> (it->second, point,
                                                                    scal_prop_ids, vect_prop_ids,
                                                                    scal_prop_aux, vect_prop_aux);

                    for(unsigned int i = 0; i<scal_prop_ids.size(); ++i) {
                        scal_prop_res[i] += f_tmp*scal_prop_aux[i];
                    }
                    for(unsigned int i = 0; i<vect_prop_ids.size(); ++i) {
                        vect_prop_res[i] += f_tmp*vect_prop_aux[i];
                    }
                }
            }
        }
    }
};


} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_FBVH_KERNEL_EVAL_H_
