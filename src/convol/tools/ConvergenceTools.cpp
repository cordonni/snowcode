#include<convol/tools/ConvergenceTools.h>

// convol dependencies
#include<convol/ScalarField.h>

using namespace expressive::core;

namespace expressive {

namespace convol {

bool Convergence::SafeNewton1D(  const ScalarField& pot,
                                 const Point& origin,
                                 const Vector& search_dir,
                                 Scalar& min_absc,
                                 Scalar& max_absc,
                                 Scalar starting_point_absc,
                                 Scalar value,
                                 Scalar epsilon,
                                 Scalar grad_epsilon,
                                 unsigned int n_max_step,
                                 Point& res_point)
{
    assert(search_dir[0] != 0.0 || search_dir[1] != 0.0 || search_dir[2] != 0.0);

    Vector search_dir_unit = search_dir;
    search_dir_unit.normalize();

    Scalar curr_point_absc = starting_point_absc;

    Scalar curr_field_value;    // contain the field value at curr_point

    //assert((pot.Eval(origin + min_absc*search_dir_unit)-value)*(pot.Eval(origin + max_absc*search_dir_unit)-value) < 0.0);

    // Newton step until we overpass the surface
    // the minimum step is set to epsilon, that ensure we will cross the surface.
    Scalar grad;
    Scalar step;
    unsigned int i = 0;
    unsigned int consecutive_small_steps = 0;
    while( max_absc - min_absc > epsilon && i < n_max_step && consecutive_small_steps < 2)
    {
        curr_field_value = pot.Eval(origin + curr_point_absc*search_dir_unit) ;
        grad = (pot.Eval(origin + (curr_point_absc+grad_epsilon)*search_dir_unit)-curr_field_value)/grad_epsilon;
        if(grad != 0.0)
        {
            step = (value-curr_field_value)/grad;
            if(step > epsilon || step < -epsilon)
            {
                curr_point_absc += step;
                consecutive_small_steps = 0;
            }
            else
            {
                if(step>0.0)
                {
                    curr_point_absc += epsilon;
                }
                else
                {
                    curr_point_absc -= epsilon;
                }
                consecutive_small_steps++;
            }


            // If the newton step took us above the max limit, or under the min limit
            // we perform a kind of dichotomy step to bring back the current point into the boundaries
            if(curr_point_absc >= max_absc || curr_point_absc <= min_absc)
            {
                curr_point_absc = (max_absc+min_absc)/2.0;
                curr_field_value = pot.Eval(origin + curr_point_absc*search_dir_unit);
            }
            else
            {
                // Update min_absc and max_absc depending on the field value
                Scalar new_curr_field_value = pot.Eval(origin + curr_point_absc*search_dir_unit);
                if( (curr_field_value-value)*(new_curr_field_value-value) > 0.0 ) // the step did not lead us to cross the surface
                {
                    if(step>0.0)
                    {
                        min_absc = curr_point_absc;
                    }
                    else
                    {
                        max_absc = curr_point_absc;
                    }
                }
                else
                {
                    if(step>0.0)
                    {
                        max_absc = curr_point_absc;
                    }
                    else
                    {
                        min_absc = curr_point_absc;
                    }
                }

                curr_field_value = new_curr_field_value;
            }
        }
        else
        {
            // Kind of dichotomy step
            curr_point_absc = (max_absc+min_absc)/2.0;
            curr_field_value = pot.Eval(origin + curr_point_absc*search_dir_unit);
        }

        ++i;
    }
    res_point = origin + curr_point_absc*search_dir_unit;

    if(i>=n_max_step)
    {
        return false;
    }
    else
    {
        return true;
    }
}


} // Close namespace convol

} // Close namespace expressive

