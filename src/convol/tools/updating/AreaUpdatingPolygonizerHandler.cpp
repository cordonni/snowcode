#include <convol/tools/updating/AreaUpdatingPolygonizerHandler.h>

#include <convol/ConvolObject.h>

namespace expressive {
namespace convol {
namespace tools {

AreaUpdatingPolygonizerHandler::AreaUpdatingPolygonizerHandler(ConvolObject* convol_object, std::mutex &action_mutex)
    : Object("AreaUpdatingPolygonizer"), convol_object_(convol_object), action_mutex_(action_mutex)
{
    if(convol_object_ != nullptr)
    {
        worker_thread_ = std::unique_ptr<core::WorkerThread>(new core::WorkerThread(convol_object_->mutex_data_retrieval(), convol_object_->cond_data_retrieval()));
        worker_thread_->AddListener(convol_object_);
    }
}

AreaUpdatingPolygonizerHandler::~AreaUpdatingPolygonizerHandler()
{
    if(convol_object_ != nullptr) {
        worker_thread_->RemoveListener(convol_object_);
    }
}

void AreaUpdatingPolygonizerHandler::set_convol_object(ConvolObject * convol_object)
{
    //TODO:@todo : probably something else to be done here... ResetBackgroundTask ?

    if(convol_object_ != nullptr)
        worker_thread_->RemoveListener(convol_object_);

    convol_object_ = convol_object;
//    worker_thread_ = std::unique_ptr<core::WorkerThread>(new core::WorkerThread(convol_object->mutex_data_retrieval(), convol_object->cond_data_retrieval()));
    //TODO:@todo : above line would cause a system_exception at the thread_.join(), I don't know yet why...
    worker_thread_->set_data_retrieval_protection(convol_object_->mutex_data_retrieval(), convol_object_->cond_data_retrieval());

    worker_thread_->AddListener(convol_object_);
}

////////////////////
/// Task request ///
////////////////////

void AreaUpdatingPolygonizerHandler::ResetSurface()
{
    worker_thread_->requestNewTask( CreateResetSurfaceBackgroundTask() );
    new_action_performed_ = true;
}
void AreaUpdatingPolygonizerHandler::UpdateSurface()
{
    worker_thread_->requestNewTask( CreateAreaUpdateSurfaceBackgroundTask() );
    new_action_performed_ = true;
}

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive
