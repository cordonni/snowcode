#include <convol/tools/updating/AreaUpdatingDualContouring/AreaDCBackgroundTasks.h>

#include <convol/tools/updating/AreaUpdatingDualContouring/AreaUpdatingDualContouringHandler.h>

namespace expressive {
using namespace core;

namespace convol {
namespace tools {

/////////////////////////////////////////
/// AreaDCUpdateSurfaceBackgroundTask ///
/////////////////////////////////////////

AreaDCUpdateSurfaceBackgroundTask::AreaDCUpdateSurfaceBackgroundTask(AreaUpdatingDualContouringHandler *polygonizer_handler)
    : SurfaceCreationBackgroundTask(polygonizer_handler), polygonizer_(polygonizer_handler->polygonizer()){}


void AreaDCUpdateSurfaceBackgroundTask::DoIt()
{
    // Copying of the implicit surface to garantee thread safety
    convol::ImplicitSurface *surface_copy;
    AABBox updated_areas;
    AggregateModifiedAreaAndCreateClone(surface_copy,updated_areas);
    // Above function prevent any action in the meanwhile

    std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
    start_time = std::chrono::high_resolution_clock::now();

    // Effective computation of the surface
    polygonizer_.setImplicitSurface(surface_copy);
    unsigned int time = 0; //TODO:@todo ???
    polygonizer_.UpdateSurfaceInArea(updated_areas, time);
    polygonizer_.setImplicitSurface(nullptr);

    end_time =  std::chrono::high_resolution_clock::now();
//    double time_taken = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000.;
//    std::cout<< "BP:Update : " << time_taken << std::endl;

    delete surface_copy;
}

std::shared_ptr<core::AbstractBackgroundTask> AreaDCUpdateSurfaceBackgroundTask::Merge(std::shared_ptr<core::AbstractBackgroundTask> pending_task)
{
    if (pending_task == nullptr)
        return shared_from_this();

    std::shared_ptr<AreaDCUpdateSurfaceBackgroundTask> previous_update = std::dynamic_pointer_cast<AreaDCUpdateSurfaceBackgroundTask>(pending_task);
    if (previous_update != nullptr)
    {
        // there is nothing to do since bounding-box aggregation is done at the last moment
        return shared_from_this();
    }

    std::shared_ptr<AreaDCResetSurfaceBackgroundTask> previous_reset = std::dynamic_pointer_cast<AreaDCResetSurfaceBackgroundTask>(pending_task);
    if (previous_reset)
    {
        return previous_reset;
    }

    // This task has a sense only after a bloomenthal area update task, so any other task can discard it  //TODO:@todo => not sure to understand ...
    return pending_task;
}


/////////////////////////////////////////
/// AreaDCResetSurfaceBackgroundTask ///
/////////////////////////////////////////


AreaDCResetSurfaceBackgroundTask::AreaDCResetSurfaceBackgroundTask(AreaUpdatingDualContouringHandler *polygonizer_handler)
    : SurfaceCreationBackgroundTask(polygonizer_handler), polygonizer_(polygonizer_handler->polygonizer()){}


void AreaDCResetSurfaceBackgroundTask::DoIt()
{
    // Copying of the implicit surface to garantee thread safety
    convol::ImplicitSurface *surface_copy;
    CreateClone(surface_copy);
    // Above function prevent any action in the meanwhile

    std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
    start_time = std::chrono::high_resolution_clock::now();

    ////////////////////////////////////////////////////////////////////////
    //TODO:@todo : temporary solution !!!!
//    AABBox world_bbox(Point(-16.0, -16.0, -16.0), Point(16.0, 16.0, 16.0));
    Vector world_min = surface_copy->GetAxisBoundingBox().min() - Vector::Constant(2.0);
    Vector world_max = surface_copy->GetAxisBoundingBox().max() + Vector::Constant(2.0);
    AABBox world_bbox(world_min, world_max);
//AABBox world_bbox(surface_copy->GetAxisBoundingBox());
    ////////////////////////////////////////////////////////////////////////

    // Effective computation of the surface
    polygonizer_.setImplicitSurface(surface_copy);
    unsigned int time = 0;
    polygonizer_.ResetSurface(world_bbox, time);
    polygonizer_.setImplicitSurface(nullptr);

    end_time =  std::chrono::high_resolution_clock::now();
    double time_taken = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000.;
    ork::Logger::INFO_LOGGER->logf("POLYGONIZER", "BP:Reset : %f", time_taken);

    delete surface_copy;
}

std::shared_ptr<core::AbstractBackgroundTask> AreaDCResetSurfaceBackgroundTask::Merge(std::shared_ptr<core::AbstractBackgroundTask> /*pending_task*/)
{
    // AreaDCResetSurfaceBackgroundTask discards any other task
    return shared_from_this();
}


} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive
