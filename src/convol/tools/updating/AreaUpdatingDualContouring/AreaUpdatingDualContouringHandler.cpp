#include <convol/tools/updating/AreaUpdatingDualContouring/AreaUpdatingDualContouringHandler.h>

#include <convol/tools/updating/AreaUpdatingDualContouring/AreaDCBackgroundTasks.h>

namespace expressive {
namespace convol {
namespace tools {

std::shared_ptr<core::AbstractBackgroundTask> AreaUpdatingDualContouringHandler::CreateResetSurfaceBackgroundTask()
{
    return std::make_shared<AreaDCResetSurfaceBackgroundTask>(this);
}

std::shared_ptr<core::AbstractBackgroundTask> AreaUpdatingDualContouringHandler::CreateAreaUpdateSurfaceBackgroundTask()
{
    return std::make_shared<AreaDCUpdateSurfaceBackgroundTask>(this);
}

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive
