/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: AreaUpdatingDualContouringState.h

   Language: C++

   License: Convol Licence

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Project: Convol-ConvolLab

   Description: Header file for a state that manage an AreaUpdateDualContouring polygonizer based on  some Qt widgets.
                

   Platform Dependencies: None
*/

#pragma once
#ifndef AREA_UPDATING_DUAL_CONTOURING_STATE_H
#define AREA_UPDATING_DUAL_CONTOURING_STATE_H

#include <core/geometry/AbstractTriMesh.h>

// convol dependencies
#include <convol/ConvolObject.h>
    // polygonizer
    #include <convol/tools/polygonizer/VertexProcessorT.h>
    #include <convol/tools/polygonizer/AreaUpdatingDualContouringT.h>
    #include <convol/tools/polygonizer/DCOctree/DCPrecisionOctree.h>
    // updating
    #include <convol/tools/updating/AreaUpdatingPolygonizerHandler.h>


///////////////////////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : ne prend pas encore en compte les changements de paramètre du polygonizer !!!
///////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace expressive {
namespace convol {
namespace tools {

class CONVOL_API AreaUpdatingDualContouringHandler : public AreaUpdatingPolygonizerHandler
{
public:
    
    typedef convol::tools::PrecisionFunctionToUse::Function PrecisionFunction;
    typedef convol::tools::AreaUpdatingDualContouringT<core::AbstractTriMesh,
                                                       convol::tools::NormalColorsVertexProcessor,
                                                       PrecisionFunction > AreaUpdatingDualContouring;

//TODO:@todo : should take some parameters ...
    AreaUpdatingDualContouringHandler(convol::ConvolObject* convol_object, std::mutex &action_mutex, unsigned int max_depth)
        : AreaUpdatingPolygonizerHandler(convol_object, action_mutex),
          precision_(nullptr),
          polygonizer_(nullptr, convol_object->tri_mesh(), &precision_, max_depth)
    {
        //TODO:@todo : valeur a peut pres interessante pour le moment :
        // feature  : 2.5 à 1.8
        // detail   : 5 (juste le featue est calcule), 1.8 commence a etre jolie, 1.2 Nice
//        precision_.SetPrecisionModifiers(2.5/*feature*/, 1.5/*detail*/);
//        precision_.SetPrecisionModifiers(5.0/*feature*/, 5/*detail*/);
//        precision_.SetPrecisionModifiers(5.0/*feature*/, 1.6/*detail*/);
//        precision_.SetPrecisionModifiers(3.0/*feature*/, 1.4/*detail*/);


//          precision_.SetPrecisionModifiers(2.0/*feature*/, 2.0/*detail*/); //assez bien
        precision_.SetPrecisionModifiers(2.0/*feature*/, 1.6/*detail*/); //bien (encore quelque defaut)

//        precision_.SetPrecisionModifiers(2.0/*feature*/, 1.2/*detail*/); //nice
        precision_.SetPrecisionModifiers(2.0/*feature*/, 1.0/*detail*/); //bien (encore quelque defaut)
//        precision_.SetPrecisionModifiers(2.0/*feature*/, 0.8/*detail*/); //really nice
    }

    virtual std::shared_ptr<core::AbstractBackgroundTask> CreateResetSurfaceBackgroundTask();
    virtual std::shared_ptr<core::AbstractBackgroundTask> CreateAreaUpdateSurfaceBackgroundTask();

    /////////////////
    /// Accessors ///
    /////////////////
    AreaUpdatingDualContouring& polygonizer() { return polygonizer_; }

protected :
    PrecisionFunction precision_; //TODO:@todo ???
    AreaUpdatingDualContouring polygonizer_;

};

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive

#endif // AREA_UPDATING_DUAL_CONTOURING_STATE_H
