#include <convol/tools/updating/SurfaceCreationBackgroundTask.h>

#include <convol/ConvolObject.h>

#include <convol/tools/updating/AreaUpdatingPolygonizerHandler.h>

namespace expressive {

using namespace core;

namespace convol {
namespace tools {

SurfaceCreationBackgroundTask::SurfaceCreationBackgroundTask(AreaUpdatingPolygonizerHandler *polygonizer_handler)
    : AbstractBackgroundTask(),
      convol_object_(polygonizer_handler->convol_object()),
      action_mutex_(polygonizer_handler->action_mutex()),
      new_action_performed_(polygonizer_handler->new_action_performed())
{}

//TODO:@todo : there is too much redundancy with the following function ...
void SurfaceCreationBackgroundTask::CreateClone(convol::ImplicitSurface* &surface_copy)
{
    // Lock mutex : ensure that the blobtree_root is not modified at the same time (would cause uncoherent data...)
    std::lock_guard<std::mutex> mlock(action_mutex_);

    // Initialize all the bounding box in the blobtree
    convol_object_->implicit_surface()->PrepareForEval(); //TODO:@todo : pas sur que cela doive etre fait ici ...

    // Clean updated areas from blobtree root
    convol_object_->blobtree_root()->ClearUpdatedAreas();
    convol_object_->blobtree_root()->ClearAreasFromDeletedNodes();

    // Create a clone of the implicit surface to ensure thread safety
    surface_copy = convol_object_->implicit_surface()->CloneForEval(); //TODO:@todo : should later take the area to be updated as parameter

    new_action_performed_ = false; // this handled the latest performed action (this is ensured by the mutex!)
}


void SurfaceCreationBackgroundTask::AggregateModifiedAreaAndCreateClone(convol::ImplicitSurface* &surface_copy, AABBox &updated_areas)
{
    // Lock mutex : ensure that the blobtree_root is not modified at the same time (would cause uncoherent data...)
    std::lock_guard<std::mutex> mlock(action_mutex_);

    // Initialize all the bounding box in the blobtree
    convol_object_->implicit_surface()->PrepareForEval(); //TODO:@todo : pas sur que cela doive etre fait ici ...

    // Perform the aggregation of updated bounding box stored in the blobtree root
    updated_areas = AABBox();
    for(auto & pair : convol_object_->blobtree_root()->updated_areas() )
    {
        updated_areas = updated_areas.Union(pair.second.first);
        updated_areas = updated_areas.Union(pair.second.second);
    }
    for(auto & box : convol_object_->blobtree_root()->last_modified_areas_from_deleted_nodes() )
    {
        updated_areas = updated_areas.Union(box);
    }

    // Clean updated areas from blobtree root
    convol_object_->blobtree_root()->ClearUpdatedAreas();
    convol_object_->blobtree_root()->ClearAreasFromDeletedNodes();

    // Create a clone of the implicit surface to ensure thread safety
    surface_copy = convol_object_->implicit_surface()->CloneForEval(); //TODO:@todo : should later take the area to be updated as parameter

    new_action_performed_ = false; // this handled the latest performed action (this is ensured by the mutex!)
}

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive
