/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: AreaUpdatingPolygonizer.h

   Language: C++

   License: expressive Licence

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Project: ???

   Description: Header file for ???

   Platform Dependencies: None
*/

#pragma once
#ifndef AREA_UPDATING_POLYGONIZER_H
#define AREA_UPDATING_POLYGONIZER_H

// core dependencies
#include <core/CoreRequired.h>
    #include <core/geometry/AABBox.h>
    #include <core/workerthread/WorkerThread.h>

// convol  dependencies
#include <convol/tools/updating/SurfaceCreationBackgroundTask.h>


namespace expressive {
namespace convol {

class ConvolObject; //#include <convol/tools/ConvolObject.h>

namespace tools {

class CONVOL_API AreaUpdatingPolygonizerHandler : public ork::Object
{
public:
    AreaUpdatingPolygonizerHandler(ConvolObject* convol_object, std::mutex &action_mutex);
    ~AreaUpdatingPolygonizerHandler();

public :

    virtual std::shared_ptr<core::AbstractBackgroundTask> CreateResetSurfaceBackgroundTask() = 0;
    virtual std::shared_ptr<core::AbstractBackgroundTask> CreateAreaUpdateSurfaceBackgroundTask() = 0;

    /////////////////
    /// Accessors ///
    /////////////////

    convol::ConvolObject* convol_object() { return convol_object_; }
    std::mutex& action_mutex() { return action_mutex_; }

    bool& new_action_performed(){ return new_action_performed_; }
    // can be used by specific polygonizer to interrupt their work in order to take into account new information

    /////////////////
    /// Modifiers ///
    /////////////////

    void set_convol_object(ConvolObject * convol_object);

    ////////////////////
    /// Task request ///
    ////////////////////

    // All this slot correspond to work ask by a new action,
    // for this reason the boolean new_action_performed_ should be set to true !
    void ResetSurface();
    void UpdateSurface();

protected:
    std::unique_ptr<core::WorkerThread> worker_thread_;

    convol::ConvolObject *convol_object_; // object to polygonize

    std::mutex &action_mutex_;
    // action have to be prevented during the cloning of the implicit surface (otherwise uncoherent data can arise !)

    bool new_action_performed_; // true if a new action has been performed during the current work
    // it is reseted to false in SurfaceCreationBackgroundTask::AggregateModifiedAreaAndCreateClone and CreateClone
    // it is set to true when a new task is required (UpdateSurface ResetSurface)
    // this is only used by polygonizer capable of segmenting their work (such as progressive multi-resolution)
};


} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive

#endif
