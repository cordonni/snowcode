/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: AreaUpdatingBloomenthalPolygonizerState.h

   Language: C++

   License: Convol Licence

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Project: Convol-ConvolLab

   Description: Header file for a state that manage an AreaUpdateBloomenthalPolygonizer polygonizer based on  some Qt widgets.
                

   Platform Dependencies: None
*/

#pragma once
#ifndef AREA_UPDATING_BLOOMENTHAL_POLYGONIZER_STATE_H
#define AREA_UPDATING_BLOOMENTHAL_POLYGONIZER_STATE_H

#include <core/geometry/AbstractTriMesh.h>

// convol dependencies
#include <convol/ConvolObject.h>
    // polygonizer
    #include <convol/tools/polygonizer/VertexProcessorT.h>
    #include <convol/tools/polygonizer/AreaUpdatingBloomenthalPolygonizerT.h>
    // updating
    #include <convol/tools/updating/AreaUpdatingPolygonizerHandler.h>


///////////////////////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : ne prend pas encore en compte les changements de paramètre du polygonizer !!!
///////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace expressive {
namespace convol {
namespace tools {

class CONVOL_API AreaUpdatingBloomenthalPolygonizerHandler : public AreaUpdatingPolygonizerHandler
{
//    Q_OBJECT
public:
    typedef convol::tools::AreaUpdatingBloomenthalPolygonizerT<core::AbstractTriMesh, convol::tools::NormalColorsVertexProcessor > AreaUpdatingBloomenthalPolygonizer;

    AreaUpdatingBloomenthalPolygonizerHandler(convol::ConvolObject* convol_object, std::mutex &action_mutex,
                                              Scalar size, Scalar epsilon)
        : AreaUpdatingPolygonizerHandler(convol_object, action_mutex), polygonizer_(nullptr, size, epsilon, convol_object->tri_mesh())
    {}

    ~AreaUpdatingBloomenthalPolygonizerHandler()
    {
        worker_thread_->interrupt();
        worker_thread_->WaitForWorkDone();
    }

    virtual std::shared_ptr<core::AbstractBackgroundTask> CreateResetSurfaceBackgroundTask();
    virtual std::shared_ptr<core::AbstractBackgroundTask> CreateAreaUpdateSurfaceBackgroundTask();

    /////////////////
    /// Accessors ///
    /////////////////
    AreaUpdatingBloomenthalPolygonizer& polygonizer() { return polygonizer_; }

protected :
    AreaUpdatingBloomenthalPolygonizer polygonizer_;
};

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive

#endif // AREA_UPDATING_BLOOMENTHAL_POLYGONIZER_STATE_H
