#include <convol/tools/updating/AreaUpdatingBloomenthalPolygonizer/AreaBPBackgroundTasks.h>

#include <convol/tools/updating/AreaUpdatingBloomenthalPolygonizer/AreaUpdatingBloomenthalPolygonizerHandler.h>

#include <chrono>

namespace expressive {
using namespace core;

namespace convol {
namespace tools {

/////////////////////////////////////////
/// AreaBPUpdateSurfaceBackgroundTask ///
/////////////////////////////////////////

AreaBPUpdateSurfaceBackgroundTask::AreaBPUpdateSurfaceBackgroundTask(AreaUpdatingBloomenthalPolygonizerHandler *polygonizer_handler)
    : SurfaceCreationBackgroundTask(polygonizer_handler), polygonizer_(polygonizer_handler->polygonizer()){}


void AreaBPUpdateSurfaceBackgroundTask::DoIt()
{
    // Copying of the implicit surface to garantee thread safety
    convol::ImplicitSurface *surface_copy;
    AABBox updated_areas;
    AggregateModifiedAreaAndCreateClone(surface_copy,updated_areas);
    // Above function prevent any action in the meanwhile

    std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
    start_time = std::chrono::high_resolution_clock::now();

    // Effective computation of the surface
    polygonizer_.setImplicitSurface(surface_copy);
    polygonizer_.UpdateSurfaceInArea(false, updated_areas);
    polygonizer_.setImplicitSurface(nullptr);

    end_time =  std::chrono::high_resolution_clock::now();
//    double time_taken = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000.;
//    std::cout<< "BP:Update : " << time_taken << std::endl;

    delete surface_copy;
}

std::shared_ptr<core::AbstractBackgroundTask> AreaBPUpdateSurfaceBackgroundTask::Merge(std::shared_ptr<core::AbstractBackgroundTask> pending_task)
{
    if (pending_task == nullptr)
        return shared_from_this();

    std::shared_ptr<AreaBPUpdateSurfaceBackgroundTask> previous_update = std::dynamic_pointer_cast<AreaBPUpdateSurfaceBackgroundTask>(pending_task);
    if (previous_update != nullptr)
    {
        // there is nothing to do since bounding-box aggregation is done at the last moment
        return shared_from_this();
    }

    std::shared_ptr<AreaBPResetSurfaceBackgroundTask> previous_reset = std::dynamic_pointer_cast<AreaBPResetSurfaceBackgroundTask>(pending_task);
    if (previous_reset)
    {
        return previous_reset;
    }

    // This task has a sense only after a bloomenthal area update task, so any other task can discard it  //TODO:@todo => not sure to understand ...
    return pending_task;
}


/////////////////////////////////////////
/// AreaBPResetSurfaceBackgroundTask ///
/////////////////////////////////////////


AreaBPResetSurfaceBackgroundTask::AreaBPResetSurfaceBackgroundTask(AreaUpdatingBloomenthalPolygonizerHandler *polygonizer_handler)
    : SurfaceCreationBackgroundTask(polygonizer_handler), polygonizer_(polygonizer_handler->polygonizer()){}


void AreaBPResetSurfaceBackgroundTask::DoIt()
{
    // Copying of the implicit surface to garantee thread safety
    convol::ImplicitSurface *surface_copy;
    CreateClone(surface_copy);
    // Above function prevent any action in the meanwhile

    std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
    start_time = std::chrono::high_resolution_clock::now();

    // Effective computation of the surface
    polygonizer_.setImplicitSurface(surface_copy);
    polygonizer_.ResetSurface();
    polygonizer_.setImplicitSurface(nullptr);

    end_time =  std::chrono::high_resolution_clock::now();
    double time_taken = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000.;
    ork::Logger::INFO_LOGGER->logf("POLYGONIZER", "BP:Reset : %f", time_taken);

    delete surface_copy;
}

std::shared_ptr<core::AbstractBackgroundTask> AreaBPResetSurfaceBackgroundTask::Merge(std::shared_ptr<core::AbstractBackgroundTask> /*pending_task*/)
{
    // AreaBPResetSurfaceBackgroundTask discards any other task
    return shared_from_this();
}

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive

//void AreaBPUpdateSurfaceBackgroundTask::GUITaskDoneCallback()
//{
//    state_->updateUIAfterSurfaceCreation(object_,time_taken_,prec_time_);
//}
