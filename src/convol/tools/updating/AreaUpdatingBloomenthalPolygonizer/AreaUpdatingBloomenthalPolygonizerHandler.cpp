#include <convol/tools/updating/AreaUpdatingBloomenthalPolygonizer/AreaUpdatingBloomenthalPolygonizerHandler.h>

#include <convol/tools/updating/AreaUpdatingBloomenthalPolygonizer/AreaBPBackgroundTasks.h>

namespace expressive {
namespace convol {
namespace tools {

std::shared_ptr<core::AbstractBackgroundTask> AreaUpdatingBloomenthalPolygonizerHandler::CreateResetSurfaceBackgroundTask()
{
    return std::make_shared<AreaBPResetSurfaceBackgroundTask>(this);
}

std::shared_ptr<core::AbstractBackgroundTask> AreaUpdatingBloomenthalPolygonizerHandler::CreateAreaUpdateSurfaceBackgroundTask()
{
    return std::make_shared<AreaBPUpdateSurfaceBackgroundTask>(this);
}

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive

//void AreaUpdatingBloomenthalPolygonizerState::updateUIAfterSurfaceCreation(Object *object, unsigned int time_taken, unsigned int /*prec_time*/)
//{
//    if (!this->object_pool_->is_valid(object))
//        return;

//    //TODO:@todo : en terme de recopie de donnée, c'est stupide : me semble plus logique d'utiliser directement le basic tri mesh de l'objet conserné...
//    *(object->basic_tri_mesh()) = local_tri_mesh_;

//    // Update UI
//    this->label_nb_of_vertices_->setText(QString("%1").arg(local_tri_mesh_.n_vertices()));
//    this->label_nb_of_triangles_->setText(QString("%1").arg(local_tri_mesh_.n_faces()));

//    if (time_taken >= 0.)
//        this->label_time_->setText(QString("%1s").arg(time_taken/1000.));

//    emit surfaceUpdated();
//}
