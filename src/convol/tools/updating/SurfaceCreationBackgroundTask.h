/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SURFACECREATIONBACKGROUNDTASK_H
#define SURFACECREATIONBACKGROUNDTASK_H

// core dependencies
#include <core/geometry/AABBox.h>
    #include <core/workerthread/AbstractBackgroundTask.h>

// convol dependencies
#include <convol/ImplicitSurface.h>


namespace expressive {
namespace convol {

class ConvolObject; //#include <convol/ConvolObject.h>

namespace tools {

class AreaUpdatingPolygonizerHandler; //#include <convol/tools/updating/AreaUpdatingPolygonizerHandler.h>

class CONVOL_API SurfaceCreationBackgroundTask : public core::AbstractBackgroundTask
{
public :
    SurfaceCreationBackgroundTask(AreaUpdatingPolygonizerHandler *polygonizer_handler);

    virtual std::shared_ptr<AbstractBackgroundTask> Merge(std::shared_ptr<AbstractBackgroundTask> pendingTask) = 0;
    virtual void DoIt() = 0;

protected:
    void CreateClone(convol::ImplicitSurface* &surface_copy);
    void AggregateModifiedAreaAndCreateClone(convol::ImplicitSurface* &surface_copy, core::AABBox &updated_areas);

    ////////////////
    /// Attibuts ///
    ////////////////

    convol::ConvolObject *convol_object_; // contient un maillage et une surface implicit
    std::mutex &action_mutex_;
    bool &new_action_performed_;

    unsigned int prec_time_, time_taken_;

    //AreaUpdatingPolygonizerHandler &polygonizer_handler; //TODO:@todo : usefull only for call-back function (which are not yet present)
};

} // Close namespace tools
} // Close namespace convol
} // Close namespace expressive

#endif // SURFACECREATIONBACKGROUNDTASK_H
