/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: SolvingEquation4T.h

   Language: C++

   License: Convol license

   \author: Galel Koraa
   E-Mail: galel.koraa@inria.fr

   Description: Solving first,second,third and fourth degree equations
   Warning : don't work for float type
   Platform Dependencies: None
*/

#pragma once
#ifndef SOLVING_EQUATION_4_H
#define SOLVING_EQUATION_4_H

#include <math.h>
#include <vector>

#include <core/CoreRequired.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////
///TODO:@todo : I do not like this class (both inefficient and numerically not stable in some cases)
/////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace expressive {

namespace convol {

class SolvingEquation4
{
public:

    //----------------------------------------------------------------------------------------------------------//
    /** \brief To solve the cube root of a number
    * \param x
    * \return the cube root of x
    */
    static Scalar cube_root(Scalar x)
    {
        return (x < 0) ?
                        -pow(-x,1.0/3.0)
                       : pow(x,1.0/3.0);
    }
    //----------------------------------------------------------------------------------------------------------//
    /** \brief Solving first degree equations : a0 + a1x = 0
    *   \param a0
    *   \param a1
    *   \return std::vector<Scalar> : the solutions of the equation. (if std::vector is empty, there are not solutions)
    */
    static std::vector<Scalar> first(Scalar a0, Scalar a1){
    //    std::cout<<"Solving of "<<a0<<" + "<<a1<<"x = 0"<<std::endl;
        std::vector<Scalar> vect;
        if(a1==0.0){
            if(a0==0){
    //            std::cout<<"Indeterminate equation."<<std::endl;
            }
            else{
    //            std::cout<<"Impossible equation."<<std::endl;
            }
        }
        else {
            Scalar x = -a0/a1;
            vect.push_back(x);
        }
        return vect;
    }
    //----------------------------------------------------------------------------------------------------------//
    /** \brief Solving second degree equations : a0 + a1x + a2x² = 0
    *   \param a0
    *   \param a1
    *   \param a2
    *   \return std::vector<Scalar> : the solutions of the equation. (if std::vector is empty, there are not solutions)
    */
    static std::vector<Scalar> second(Scalar a0, Scalar a1, Scalar a2){
    //    std::cout<<"Solving of "<<a0<<" + "<<a1<<"x + "<<a2<<"x² = 0"<<std::endl;
        std::vector<Scalar> solution;
        if(a2==0.0){
    //        std::cout<<"First degree equation."<<std::endl;
            solution = first(a0,a1);
        }
        else{
             Scalar Delta = a1*a1-4.0*a2*a0;
             if (Delta>0.0){
    //             std::cout<<"two real distinct solutions :"<<std::endl;
                 Scalar x1 = (-a1+sqrt(Delta))/(2.0*a2);
                 Scalar x2 = (-a1-sqrt(Delta))/(2.0*a2);
                 solution.push_back(x1);
                 solution.push_back(x2);
    //             std::cout<<"x1 ="<<x1<<std::endl;
    //             std::cout<<"x2 ="<<x2<<std::endl;
         }
         else if (Delta==0.0){
                Scalar x = -a1/(2.0*a2);
                solution.push_back(x);
    //            std::cout<<"1 only real solution"<<std::endl;
             }
             else{
    //             std::cout<<"Not real solution"<<std::endl;
             }
        }
        return solution;
    }
    //----------------------------------------------------------------------------------------------------------//
    /** \brief Solving third degree equations for this particular case : x³ + px + q = 0
    *   \param p
    *   \param q
    *   \return std::vector<Scalar> : the solutions of the equation. (if std::vector is empty, there are not solutions)
    */
    static std::vector<Scalar> third_particular(Scalar p, Scalar q){
    //    std::cout<<"Solving of "<<q<<" + "<<p<<"x + "<<"x³ = 0"<<std::endl;
        std::vector<Scalar> solution;

        // Case 1 : x³=0 (1 real triple solution)
        if(p==0 && q==0){
            solution.push_back(0.0);solution.push_back(0.0);solution.push_back(0.0);
        }
        // Case 2 : x³ + q = 0 (1 real solution)
        else if(p==0 && q!=0){
            Scalar x1 = cube_root(-q);
            solution.push_back(x1);
        }
        // Case 3 : x³ + px = 0
        else if(p!=0 && q==0){
            // 1. 3 real solutions
            if(p<0){
                Scalar x1 = 0.0;
                Scalar x2 = sqrt(-p);
                Scalar x3 = -sqrt(-p);
                solution.push_back(x1);solution.push_back(x2);solution.push_back(x3);
            }
            // 2. 1 real solution
            else{
                solution.push_back(0.0);
            }
        }
        // Case 4 : General case
        else{
            Scalar delta = (pow(q,2)/4.0) + (pow(p,3)/27.0);
            // 1. 1 real solution
            if(delta>0){
                Scalar sqrt_delta = sqrt(delta);
                Scalar x1 = cube_root((-q/2.0)+sqrt_delta) + cube_root((-q/2.0)-sqrt_delta);
                solution.push_back(x1);
            }
            // 2. 2 real solutions including 1 double
            else if(delta==0){
                Scalar x1 = (3.0*q)/p;
                Scalar x2 = (-3.0*q)/(2.0*p);
                solution.push_back(x1);
                solution.push_back(x2);solution.push_back(x2);
            }
            // 3. 3 real distinct solutions
            else{
                Scalar teta = acos(((3.0*q)/(2.0*p))*sqrt(-3.0/p));
                Scalar x1 = 2.0*sqrt(-p/3.0)*cos(teta/3.0);
                Scalar x2 = 2.0*sqrt(-p/3.0)*cos((teta+2.0*M_PI)/3.0);
                Scalar x3 = 2.0*sqrt(-p/3.0)*cos((teta+4.0*M_PI)/3.0);
                solution.push_back(x1);solution.push_back(x2);solution.push_back(x3);
            }
        }
        return solution;
    }
    //----------------------------------------------------------------------------------------------------------//
    /** \brief Solving third degree equations for the general case : ax³ + bx² + cx + d = 0
    *   \param a
    *   \param b
    *   \param c
    *   \param d
    *   \return std::vector<Scalar> : the solutions of the equation. (if std::vector is empty, there are not solutions)
    */
    static std::vector<Scalar> third_general(Scalar a, Scalar b, Scalar c,Scalar d){

   //     std::cout<<"Solving of "<<d<<" + "<<c<<"x + "<<b<<"x² + "<<a<<"x³ = 0"<<std::endl;
        std::vector<Scalar> solution;
        if(a == 0){
   //         std::cout<<"Second degree equation."<<std::endl;
            solution = second(d,c,b);
        }
        // Case 1 : ax³ = 0 (1 real triple solution)
        else if(b==0.0 && c==0.0 && d==0.0){
             solution.push_back(0.0);
             solution.push_back(0.0);
             solution.push_back(0.0);
        }
        // Case 2 : ax³ + d = 0 (1 real solution)
        else if (b==0.0 && c==0.0 && d!=0.0){
            Scalar x1 = cube_root(-d/a);
            solution.push_back(x1);
        }
        // Case 3 : ax³ + cx = 0
        else if (b==0.0 && c!=0.0 && d==0.0){
            // 1. if a and c have the same sign : (1 real solution)
            if(a*c>=0){
                  solution.push_back(0.0);
            }
            // 2. else (3 real solutions)
            else{
                Scalar x1 =0.0;
                Scalar x2 =sqrt(-(c/a));
                Scalar x3 =-sqrt(-(c/a));
                solution.push_back(x1);
                solution.push_back(x2);
                solution.push_back(x3);
            }
        }
        // Case 4 : ax³ + cx +d = 0
        else if (b==0.0 && c!=0.0 && d!=0.0){
            Scalar p, q;
            p = c/a; q = d/a;
            // Solving of equation :  x³ + px + q =0
            solution = third_particular(p,q);
        }
        // Case 5 : ax³ + bx² = 0 (2 real solutions including 1 double)
        else if (b!=0.0 && c==0.0 && d==0.0){
                Scalar x3= -b/a;
                solution.push_back(0.0); solution.push_back(0.0); solution.push_back(x3);
        }
        // Case 6 : ax³ + bx² +cx = 0
        else if(b!=0.0 && c!=0.0 && d==0){
            solution.push_back(0.0);
            // The 2 others solutions x1 et x2 are the solutions of equation : ax²+bx+c=0
            std::vector<Scalar> solution2 = second(c, b, a);
            int size = (int) solution2.size();
            for(int i=0;i<size;i++){
                solution.push_back(solution2.at(i));
            }
        }
        // Case 7 : Other cases
        else{
            Scalar p = (c/a)-((b*b)/(3.0*a*a));
            Scalar q = (d/a) - ((b*c)/(3.0*pow(a,2))) + ((2.0*pow(b,3))/(27.0*pow(a,3)));
            // Find X1, X2 and X3 in solving X3 + pX + q = 0
            std::vector<Scalar> solutionX = third_particular(p,q);
            int size = (int) solutionX.size();
            for(int i=0;i<size;i++){
                Scalar x = solutionX.at(i)-(b/(3.0*a));
                solution.push_back(x);
            }
        }
        return solution;

    }
    //----------------------------------------------------------------------------------------------------------//
    /** \brief Solving fourth degree equations for this particular case : x⁴ + px³ + qx + r = 0
    *   \param p
    *   \param q
    *   \param r
    *   \return std::vector<Scalar> : the solutions of the equation. (if std::vector is empty, there are not solutions)
    */
    static std::vector<Scalar> fourth_particular(Scalar p, Scalar q, Scalar r){
    //    std::cout<<"Solving of "<<r<<" + "<<q<<"x + "<<p<<"x³ + x⁴ = 0"<<std::endl;
        std::vector<Scalar> solution;

        // Case 1 : x⁴ = 0 (1 real quadruple solution)
        if((p==0.0) && (q==0.0) && (r==0.0)){
            solution.push_back(0.0);solution.push_back(0.0);solution.push_back(0.0);solution.push_back(0.0);
        }
        // Case 2 : x⁴+r  = 0
        else if((p==0.0) && (q==0.0) && (r!=0.0)){
    //        std::cout<<"Cas part 2 "<<std::endl;
            // 2 real solutions
            if(r<0.0){
                Scalar x1 = pow(-r,(1.0/4.0));
                Scalar x2 = -x1;
                solution.push_back(x1); solution.push_back(x2);
            }
            else{
    //            std::cout<<"Not real solutions."<<std::endl;
            }
        }
        // Case 3 : x⁴+qx=0 (2 real solutions)
        else if((p==0.0) && (q!=0.0) && (r==0.0)){
            Scalar x2 = cube_root(-q);
            solution.push_back(0.0); solution.push_back(x2);
        }
        // Cas 4 : x⁴ + px² = 0
        else if((p!=0.0) && (q==0.0) && (r==0.0)){
            //1. 1 real double solution
            if(p>0.0){
                solution.push_back(0.0);solution.push_back(0.0);
            }
            //2. 3 real solutions including 1 double
            else if(p<0.0){
                Scalar x3 = sqrt(-p);
                Scalar x4 = -x3;
                solution.push_back(0.0); solution.push_back(0.0); solution.push_back(x3); solution.push_back(x4);
            }
            else {
    //            std::cout<<"Not real solutions."<<std::endl;
            }
        }
        // Case 5 : x⁴ + px² + r = 0
        else if ((p!=0.0) && (q==0.0) && (r!=0.0)){
            // Solving second degree equation : y² + py + r = 0
            std::vector<Scalar> sol_y = second(r,p,1.0);
            int size = (int) sol_y.size();
            // There are 2 solutions ( y² + py + r = 0)
            if(size==2){
                Scalar y1 = sol_y.at(0); Scalar y2 = sol_y.at(1);
               //1. y1>0 and y2>05
                if((y1>0.0) && (y2>0.0)){
                    //a. 4 real disctinct solutions
                    if(y1!=y2){
                        Scalar x1 = sqrt(y1);
                        Scalar x3 = sqrt(y2);
                        solution.push_back(x1);solution.push_back(-x1);solution.push_back(x3);solution.push_back(-x3);
                    }
                    //b. 2 real double solutions
                    else{
                        Scalar x1 = sqrt(y1);
                        solution.push_back(x1);solution.push_back(x1);solution.push_back(-x1);solution.push_back(-x1);
                    }
                }
                //2. 2 real solutions
                else if((y1>0.0) && (y2<0.0)){
                    Scalar x1 = sqrt(y1);
                    solution.push_back(x1);solution.push_back(-x1);
                }
                //3. 2 real solutions
                else if((y1<0.0) && (y2>0.0)){
                     Scalar x2 = sqrt(y2);
                     solution.push_back(x2);solution.push_back(-x2);
                }
                else {
                    std::cout<<"Not solutions"<<std::endl;
                }
            }
            else{
                std::cout<<"Not solutions"<<std::endl;
            }
        }
        // Case 6 : x⁴ + px² + qx = 0
        else if ((p!=0.0) && (q!=0.0) && (r==0.0)){
                solution.push_back(0.0);
                // The three others real solutions are the real solutions of this equation : x³+px+q = 0
                std::vector<Scalar> sol3 = third_general(1.0, 0.0, p,q);
                int size = (int) sol3.size();
                for(int i=0;i<size;i++){
                    solution.push_back(sol3.at(i));
                }
        }
        // Case 7: Other cases : Solving third degree equation :  u³ + 2p u² + (p²-4r) u - q² = 0
        else{
            std::vector<Scalar> sol3 = third_general(1.0,2.0*p,(pow(p,2.0)-4.0*r),-pow(q,2.0));
            // We take one real solution positive
            int size = (int) sol3.size();
            Scalar U = 0.0;
            int i=0;
            while (U<=0.0 && i<size){
                U = sol3.at(i);
                i++;
            }
            Scalar u = sqrt(U);
            Scalar v = (((p+pow(u,2.0))*u)-q)/(2*u);
            Scalar w = (((p+pow(u,2.0))*u)+q)/(2*u);

            //The solution of equation are the solutions of 2 equations of the second degree
            // x² + ux + v = 0
            // x² - ux + w = 0
            std::vector<Scalar> sol4 = second(v,u,1.0);
            std::vector<Scalar> sol5 = second(w,-u,1.0);
            int size4 = (int) sol4.size();
            for(int i=0;i<size4;i++){
                solution.push_back(sol4.at(i));
            }
            int size5 = (int) sol5.size();
            for(int i=0;i<size5;i++){
                solution.push_back(sol5.at(i));
            }
        }
        return solution;
    }
    //----------------------------------------------------------------------------------------------------------//
    /** \brief Solving fourth degree equations for the general case : ax⁴ + bx³ + cx² + dx + e = 0
    *   \param a
    *   \param b
    *   \param c
    *   \param d
    *   \param e
    *   \return std::vector<Scalar> : the solutions of the equation. (if std::vector is empty, there are not solutions)
    */
    static std::vector<Scalar> fourth_general(Scalar a, Scalar b, Scalar c, Scalar d, Scalar e)
    {
        std::vector<Scalar> solution;
        if(a==0.0){
            solution = third_general(b,c,d,e);
        }
        //Case 1 : ax⁴ + cx² + dx + e = 0
        else if(b==0.0){
            Scalar p = c/a; Scalar q = d/a; Scalar r = e/a;
            // The solutions of equation are the solutions of equation : x⁴ + px³ + qx + r = 0
            solution = fourth_particular(p,q,r);
        }
        //Case 2 : ax⁴ + bx³ = 0 (2 real solutions)
        else if((b!=0.0) && (c==0.0) && (d==0.0) && (e==0.0)){
            Scalar x4 = -b/a;
            solution.push_back(0.0);solution.push_back(0.0);solution.push_back(0.0);solution.push_back(x4);
        }
        //Case 3 : ax⁴ + bx³ + cx² = 0
        else if((b!=0.0) && (c!=0.0) && (d==0.0) && (e==0.0)){
            solution.push_back(0.0);solution.push_back(0.0);
            // + real solutions of second degree equation : ax² + bx + c = 0
            std::vector<Scalar> sol2 = second(c,b,a);
            int size = (int) sol2.size();
            for(int i=0;i<size;i++){
                solution.push_back(sol2.at(i));
            }
        }
        //Case 4 : ax⁴ + bx³ + cx² + dx = 0
        else if((b!=0.0) && (d!=0.0) && (e==0)){
            solution.push_back(0.0);
            // + + real solutions of third degree equation : ax³ + bx² + cx + d = 0
            std::vector<Scalar> sol3 = third_general(a, b,c,d);
            int size = (int) sol3.size();
            for(int i=0;i<size;i++){
                solution.push_back(sol3.at(i));
            }
        }
        //Case 5 : Other cases
        else{
            Scalar p = (c/a) - ((3.0*pow(b,2))/(8.0*pow(a,2)));
            Scalar q = (d/a) - ((b*c)/(2.0*pow(a,2))) + ((pow(b,3))/(8.0*pow(a,3)));
            Scalar r = (e/a) -((b*d)/(4.0*pow(a,2))) + ((pow(b,2)*c)/(16.0*pow(a,3))) - ((3.0*pow(b,4))/(256.0*pow(a,4)));
            std::vector<Scalar> sol= fourth_particular(p,q,r);
            int size = (int) sol.size();
            Scalar x;
            for(int i=0;i<size;i++){
                x = sol.at(i)-(b/(4.0*a));
                solution.push_back(x);
            }
        }
        return solution;
    }
};

} // Close namespace Convol

} // Close namespace expressive

#endif // SOLVING_EQUATION4_H
