/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: FittedBVH.h

   Language: C++

   License: Convol license

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Fitted BVH is an extension of Bounding Volume Hierarchy 
                introduced in Fitted BVH for Fast Raytracing of Metaballs
                by O. Gourmel et Al at Eurographics 2010.

   Limitations: 3D only

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_FITTED_BVH_H_
#define CONVOL_FITTED_BVH_H_

#include <core/CoreRequired.h>
    #include <core/geometry/AABBox.h>

#include <convol/ConvolRequired.h>

#include "ork/core/Logger.h"

/////////////////////////////////////////////////////////////////
/// TODO:@todo : evaluer la qualité de la strucutre construit (profondeur, nombre de primitive par feuille,
///              impact sur temps de calcul, nombre de cache miss, nombre de splitted metaballs...)
/// TODO:@todo : take into account splitted data in the computation of the cost (and don't take into account empty volume in the computation of the proba)
/// TODO:@todo : pour le moment on travail avec BoundingBox pour un certain nombre de test, il faudrait peut etre travailler sur
///             bounding volume plus précis pour avoir moins de splitted metaballs (augmente en revanche le coup de calcul de cette structure)
/// TODO:@todo : cost function should be templated since it depends on the kind of application ...
/// TODO:@todo : probably better to have as left child the most probable node ... should improve indirect branching during query
/////////////////////////////////////////////////////////////////

#define COPY_SPLITTED_METABALLS
//TODO:@todo : later we should have a base structure with two inherited class with the two strategies

namespace expressive {

namespace convol {


class CONVOL_API NodeBVH
{
public:
    template < typename DataType >
    friend class FittedBVH;

    NodeBVH(const core::AABBox &bounding_box, unsigned int first_primitive, unsigned int end)
        : bounding_box_(bounding_box), is_leaf_(true),
          first_primitive_(first_primitive), end_(end)
    {}

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    core::AABBox bounding_box_;

    bool is_leaf_;

    // link to the child
    unsigned int child_left_, child_right_;

    // if leaf, link to the data
    unsigned int first_primitive_;
    unsigned int end_; // "as in a std container"

protected:
    std::vector<unsigned int> splitted_data_; // this is only used during the build process
};

template < typename DataType >
class FittedBVH
{
public:
    

    typedef typename std::vector< std::pair<core::AABBox, DataType> > DataVector;
    typedef typename DataVector::const_iterator CDataIterator;

    FittedBVH(unsigned int k) : k_(k) {}

    ~FittedBVH() {}

    /*!
     * \brief GetDataIterator get iterator toward data that have some influence at the query point
     * \param point a query point
     * \param begin a ref to a const_iterator that will be initialized toward the first data
     * \param end   a ref to a const_iterator that will be initialized toward the end data
     * \return true if data where found for the query point (in this case begin and end are initialized)
     */
    inline bool GetDataIterator(const Point& point, CDataIterator& begin, CDataIterator& end) const
    {
        unsigned int node_id = 0;
        //        do {
        //            if(nodes_[nodes_[node_id].child_left_].bounding_box_.Contains(point))
        //                node_id = nodes_[node_id].child_left_;
        //            else if(nodes_[nodes_[node_id].child_right_].bounding_box_.Contains(point))
        //                node_id = nodes_[node_id].child_right_;
        //            else
        //                return false;
        //        } while (!nodes_[node_id].is_leaf_);
                 while (!nodes_[node_id].is_leaf_) {
                    if(nodes_[nodes_[node_id].child_left_].bounding_box_.Contains(point))
                        node_id = nodes_[node_id].child_left_;
                    else if(nodes_[nodes_[node_id].child_right_].bounding_box_.Contains(point))
                        node_id = nodes_[node_id].child_right_;
                    else
                        return false;
                }

        begin = data_.cbegin() + nodes_[node_id].first_primitive_;
        end   = data_.cbegin() + nodes_[node_id].end_;
        return true;
    }

    /*!
     * \brief Init initialize this optimization structure assuming that data_ and barycenter_ are already initilized
     * \param aabb bounding box of all the data
     */
    void Init(const core::AABBox &aabb);

    /*!
     * \brief Recursively create a subtree of the hierarchy.
     *        This is done locally in nodes_ and data_ vectors (as well as barycenter_)
     *
     * \param parent_id index of the node that is checked for subdivision
     */
    void BuildNode(unsigned int parent_id);

    /*!
     * \brief ComputeSplittedData update splitted data of child_node and enlarge the given data aabb accordingly
     * \param parent        parent of child_node
     * \param child_node    node to which splitted datas will be added
     * \param other_node    sibling of child_node
     * \param child_data_bbox original aabb of the child_node datas which will be enlarge by the splitted data aabb
     */
    void ComputeSplittedData(const NodeBVH& parent, NodeBVH& child_node, const NodeBVH& other_node, core::AABBox& child_data_bbox);

/////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO:@todo : this functions should be changed depending on the purpose of the data structure
    Scalar Cost(int nb_data)
    {
        return (Scalar) nb_data;
    }
    Scalar ConditionalProba(core::AABBox &child_box, core::AABBox &parent_box)
    {
        //TODO:@todo : should not take into account empty space, because no evaluation are done there during most of the meshing method
        // and the cost should depend on the kind of task performed
        core::AABBox intersection = core::AABBox::Intersection(parent_box, child_box);
        return intersection.Volume() / parent_box.Volume(); // should divide by the sum of the intersection of child with volume
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////


    /*!
     * \brief BinID
     * \param k0
     * \param k1
     * \param pos
     * \return
     */
    inline unsigned int BinID (Scalar k0, Scalar k1, Scalar pos) const { return (unsigned int) (k1 * (pos - k0));  }

    /////////////////
    /// Accessors ///
    /////////////////

    /// This two functions are provided to initialize data before the Init function is called
    DataVector& data() { return data_; }
    std::vector<Point>& barycenter() { return barycenter_; }

    /////////////////
    /// Attributs ///
    /////////////////

protected:
    // the tree hierarchy
    std::vector<NodeBVH> nodes_;

    // the content of the leaves
    std::vector< std::pair<core::AABBox, DataType> > data_;

    unsigned int k_; // the number of bin to find best partition

    /// following attributs are only used during the initialization process

    std::vector<unsigned int> v_old_index_; //during creation data_ vector is not modified, indexes_ is used as indirection instead

    std::vector<Point> barycenter_; //TODO:@todo : should be a vector passed by reference ... could be given as parameter of the Init function


    /*!
     * \brief FinalizeConstruction clear all the data that where only required during the construction process
     *          and call CopyData on the root node of the structure
     */
    void FinalizeConstruction();

    /*!
     * \brief CopyData recursive function that reorder the data and copy the splitted data as new data elements
     * \param node_id index of the node which data should copied into data (effective copy only if it is a leaf)
     * \param offset the number of splitted data already copied into data (it is updated at the end of the function)
     * \param data_tmp vector containing the original data
     */
    void CopyData(unsigned int node_id, unsigned int &offset, const std::vector<std::pair<core::AABBox, DataType> >& data_tmp);


//    //TODO:@todo : rough approximation to be improved ...
//    inline void ComputeSplittingInformationApprox(const NodeBVH& parent, const AABBox& node_bbox,/*NodeBVH& child_node, const NodeBVH& other_node,*/
//                                                  AABBox& splitted_data_bbox, unsigned int& nb_split);

};

//    /*!
//     * \brief GetNode return nullptr if no leaf node overlap the query point, otherwise return pointer to the node
//     * \param point a query point
//     * \return
//     *
//     * \warning This function should be avoided in the case where COPY_SPLITTED_METABALLS is defined
//     */
//    inline const NodeBVH* GetNode(const Point& point) const
//    {
//        unsigned int node_id = 0;
//        do {
//            if(nodes_[nodes_[node_id].child_left_].bounding_box_.Contains(point))
//                node_id = nodes_[node_id].child_left_;
//            else if(nodes_[nodes_[node_id].child_right_].bounding_box_.Contains(point))
//                node_id = nodes_[node_id].child_right_;
//            else
//                return nullptr;
//        } while (!nodes_[node_id].is_leaf_);

//        return &(nodes_[node_id]);
//    }

} // Close namespace convol

} // Close namespace expressive

#include <convol/tools/FittedBVH.hpp>

#endif // CONVOL_FITTED_BVH_H_
