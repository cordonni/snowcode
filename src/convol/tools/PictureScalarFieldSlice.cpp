#include <convol/tools/PictureScalarFieldSlice.h>

namespace expressive {

namespace convol {

void PictureScalarFieldSlice::set_plane_and_length(Point origin, Vector normal, Vector dir1, Scalar length1, Scalar length2)
{
    origin_ = origin;
    normal_ = normal;

    dir1_ = dir1;
    dir2_ = normal.cross(dir1);

    length_dir1_ = length1;
    length_dir2_ = length2;
}

void PictureScalarFieldSlice::add_iso(Scalar iso)
{
    for(auto iter_iso = list_iso_.begin(); iter_iso != list_iso_.end(); ++iter_iso) {
        if( *iter_iso > iso) {
            list_iso_.insert(iter_iso, iso);
            return;
        }
    }
    list_iso_.push_back(iso);
}

void PictureScalarFieldSlice::clear_iso(Scalar iso)
{
    for(auto iter_iso = list_iso_.begin(); iter_iso != list_iso_.end(); ++iter_iso) {
        if( *iter_iso == iso) {
            list_iso_.erase(iter_iso);
            break;
        }
    }
}

void PictureScalarFieldSlice::update_field_value(){

    const Scalar dist_between_sample_dir1 = length_dir1_/((Scalar) res_dir1_);
    const Scalar dist_between_sample_dir2 = length_dir2_/((Scalar) res_dir2_);

    Point curent_sample;
    unsigned int curent_index;
    
    for(unsigned int i=0; i<res_dir1_; i++) {
        // Initialisation of the sample position (and index) for a line (in the direction dir2_) of the picture
        curent_sample =    origin_ + (dist_between_sample_dir2/2.0 - length_dir2_/2.0) * dir2_
                + (( 0.5 + (Scalar)i )*dist_between_sample_dir1 - length_dir1_/2.0 ) * dir1_;
        curent_index = i*res_dir2_;
        
        // Computation of the scalar field for a line (in the direction dir2_) of the picture
        for(unsigned int j=0; j<res_dir2_; j++) {
            Scalar field_value = scalar_field_->Eval(curent_sample);

            tab_field_value_[curent_index] =  field_value;
            
            curent_sample += dist_between_sample_dir2 * dir2_;
            curent_index++;
        }
    }
    
}

void PictureScalarFieldSlice::update_picture_field(){
    
    const Scalar HALF_PI = M_PI / 2.0;

    Scalar field_value;
    uint8 color_pixel;

    unsigned int curent_index;

    // Setting of the pixel color according to the field value
    for(unsigned int i=0; i<res_dir1_; i++) {

        curent_index = i*res_dir2_;

        for(unsigned int j=0; j<res_dir2_; j++) {
            field_value = tab_field_value_[curent_index];

            if(field_value>=0.0) {
                //Computation of the grayscale color according to value of scalar field
                color_pixel = (uint8) ( 255.0*
                                        ( sin(log(pow(field_value/wave_begining_,scaling_factor_) + 1.0)/log(2.0) * HALF_PI * 3.0)
                                          + 1.0
                                          ) * 0.5 );

                // Saving of the color
                tab_picture_field_[4*curent_index  ] = (uint8) 255;         // Alpha
                tab_picture_field_[4*curent_index+1] = color_pixel; // R
                tab_picture_field_[4*curent_index+2] = color_pixel; // G
                tab_picture_field_[4*curent_index+3] = color_pixel; // B
            } else {
                // WARNING : some time we might still want to vizualize negative values ...
                color_pixel = (uint8) ( 255.0*
                                        ( sin(log(pow(-field_value/wave_begining_,scaling_factor_) + 1.0)/log(2.0) * HALF_PI * 3.0)
                                          + 1.0
                                          ) * 0.5 );

                // Saving of the color
                tab_picture_field_[4*curent_index  ] = (uint8) 255;         // Alpha
                tab_picture_field_[4*curent_index+1] = color_pixel/2; // R
                tab_picture_field_[4*curent_index+2] = color_pixel/2; // G
                tab_picture_field_[4*curent_index+3] = color_pixel; // B
            }

            // Drawing of the iso-surfaces

            int nb_iso = (int) list_iso_.size();
            int index_iso = 1;

            typename std::list<Scalar>::iterator iter_iso;
            for(iter_iso = list_iso_.begin(); iter_iso != list_iso_.end(); ++iter_iso)
            {
                if(field_value > (*iter_iso) * 0.97 && field_value < (*iter_iso) * 1.03)
                {
                    // Choice of the color according to relative length of the iso to be drawn
                    uint8 color_iso_R = (uint8) (255.0*((float)index_iso/(float)nb_iso));
                    uint8 color_iso_B = (uint8) (255.0 - ((short)color_iso_R));

                    // Saving of the color:
                    tab_picture_field_[4*curent_index  ] = (uint8) 255;
                    tab_picture_field_[4*curent_index+1] = color_iso_R;
                    tab_picture_field_[4*curent_index+2] = 0;
                    tab_picture_field_[4*curent_index+3] = color_iso_B;
                }

                //WARNING : for negative field...
                if(-field_value > (*iter_iso) * 0.97 && -field_value < (*iter_iso) * 1.03)
                {
                    // Choice of the color according to relative length of the iso to be drawn
                    uint8 color_iso_R = (uint8) (255.0*((float)index_iso/(float)nb_iso));
                    uint8 color_iso_G = (uint8) (255.0 - ((short)color_iso_R));

                    // Saving of the color:
                    tab_picture_field_[4*curent_index  ] = (uint8) 255;
                    tab_picture_field_[4*curent_index+1] = color_iso_R;
                    tab_picture_field_[4*curent_index+2] = color_iso_G;
                    tab_picture_field_[4*curent_index+3] = 0;
                }

                ++index_iso;
            }
            if(field_value == 0.0) {
                tab_picture_field_[4*curent_index+1] = '\0';
                tab_picture_field_[4*curent_index+2] = '\0';
                tab_picture_field_[4*curent_index+3] = '\255'; // TODO : @todo : is this standard? using (char ) 255 throw a warning in visual
            }

            curent_index++;

        }
    }

    // Drawing of the projection of the skeleton

    //TODO
    // Drawing of the projection of the skeleton (if the field is generated by a skeleton)
    // The color used should depend of the distance of the skeleton to the plane
    // (with a blending according to the distance: avoid problem due to surface skeleton)

}

//void PictureScalarFieldSlice::update_picture_territory(std::vector< std::set<BlobtreeNodeT<Traits>*> > connex_node_tab)
//{
//    const Scalar HALF_PI = M_PI / 2.0;
//    uint8 color_pixel;

//    const Scalar dist_between_sample_dir1 = length_dir1_/((Scalar) res_dir1_);
//    const Scalar dist_between_sample_dir2 = length_dir2_/((Scalar) res_dir2_);

//    Point curent_sample;
//    unsigned int curent_index;

//    for(unsigned int i=0; i<res_dir1_; i++)
//    {

//        // Initialisation of the sample position (and index) for a line (in the direction dir2_) of the picture
//        curent_sample =    origin_ + (dist_between_sample_dir2/2.0 - length_dir2_/2.0) * dir2_
//                + (( 0.5 + (Scalar)i )*dist_between_sample_dir1 - length_dir1_/2.0 ) * dir1_;
//        curent_index = i*res_dir2_;

//        // Computation of the scalar field for a line (in the direction dir2_) of the picture
//        for(unsigned int j=0; j<res_dir2_; j++)
//        {
//            //            Scalar field_value = scalar_field_->Eval(curent_sample);
//            Scalar field_value = 0.0;
//            Scalar max_field = 0.0;
//            Scalar max_2_field = 0.0;
//            for(auto it_connex = connex_node_tab.begin(); it_connex != connex_node_tab.end(); ++it_connex)
//            {
//                Scalar field_connex = 0.0;
//                for(auto it_prim = it_connex->begin(); it_prim != it_connex->end(); ++it_prim)
//                {
//                    field_connex += (*it_prim)->Eval(curent_sample);
//                }

//                // get the two highest value
//                if(field_connex>=max_2_field)
//                {
//                    if(field_connex>=max_field)
//                    {
//                        max_2_field = max_field;
//                        max_field = field_connex;
//                    }
//                    else
//                    {
//                        max_2_field = field_connex;
//                    }
//                }

//                field_value += field_connex;
//            }

//            bool territory_limit = (max_2_field >= 0.9 * max_field) ;

//            tab_field_value_[curent_index] =  field_value;



//            //             Computation of the grayscale color according to value of scalar field
//            //            color_pixel = (uint8) ( 255.0* (sin(log(field_value + 1.0)/log(2.0) * HALF_PI * 3.0) + 1.0) *0.5 );
//            color_pixel = (uint8) ( 255.0*
//                                    ( sin(log(pow(field_value/wave_begining_,scaling_factor_) + 1.0)/log(2.0) * HALF_PI * 3.0)
//                                      + 1.0
//                                      ) * 0.5 );
//            // Saving of the color
//            tab_picture_field_[4*curent_index  ] = (uint8) 255;         // Alpha
//            tab_picture_field_[4*curent_index+1] = color_pixel; // R
//            tab_picture_field_[4*curent_index+2] = color_pixel; // G
//            tab_picture_field_[4*curent_index+3] = color_pixel; // B

//            // Drawing of the iso-surfaces
//            int nb_iso = list_iso_.size();
//            int index_iso = 1;

//            typename std::list<Scalar>::iterator iter_iso;
//            for(iter_iso = list_iso_.begin(); iter_iso != list_iso_.end(); ++iter_iso)
//            {
//                if(field_value > (*iter_iso) * 0.97 && field_value < (*iter_iso) * 1.03)
//                {
//                    // Choice of the color according to relative length of the iso to be drawn
//                    uint8 color_iso_R = (uint8) (255.0*((float)index_iso/(float)nb_iso));
//                    uint8 color_iso_B = (uint8) (255.0 - ((short)color_iso_R));

//                    // Saving of the color:
//                    tab_picture_field_[4*curent_index  ] = (uint8) 255;
//                    tab_picture_field_[4*curent_index+1] = color_iso_R;
//                    tab_picture_field_[4*curent_index+2] = 0;
//                    tab_picture_field_[4*curent_index+3] = color_iso_B;
//                }
//                ++index_iso;
//            }
//            if(field_value == 0.0)
//            {
//                tab_picture_field_[4*curent_index+1] = '\0';
//                tab_picture_field_[4*curent_index+2] = '\0';
//                tab_picture_field_[4*curent_index+3] = '\255'; // TODO : @todo : is this standard? using (char ) 255 throw a warning in visual
//            }

//            // Drawing of the territory limits
//            if(territory_limit)
//            {
//                tab_picture_field_[4*curent_index+1] = '\0';
//                tab_picture_field_[4*curent_index+2] = '\255'; // TODO : @todo : is this standard? using (char ) 255 throw a warning in visual
//                tab_picture_field_[4*curent_index+3] = '\0';
//            }
//            ///////////-----------------------------------------

//            curent_sample += dist_between_sample_dir2 * dir2_;
//            curent_index++;
//        }
//    }
//}

char *PictureScalarFieldSlice::tab_picture_field() const
{
    return tab_picture_field_;
}

} // Close namespace convol
} // Close namespace expressive
