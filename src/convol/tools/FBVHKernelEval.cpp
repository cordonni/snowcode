#include <convol/tools/FBVHKernelEval.h>


namespace expressive {

namespace convol {

namespace computeProperties {

template <>
CONVOL_API void ComputeProperties< std::pair<core::OptWeightedSegment, Properties1D> > (const std::pair<core::OptWeightedSegment, Properties1D>& pair,
                                                                                        const Point& point,
                                                                                        const std::vector<ESFScalarProperties>& scal_prop_ids,
                                                                                        const std::vector<ESFVectorProperties>& vect_prop_ids,
                                                                                        std::vector<Scalar>& scal_prop_res,
                                                                                        std::vector<Vector>& vect_prop_res)
{
    //TODO:@todo : this is correct for segment but not for subdivision curve because
    // evaluation of properties is currently not subdivision invariant (problematic)

    Vector p_min_to_p = point - pair.first.p_min();
    Scalar s = p_min_to_p.dot(pair.first.increase_unit_dir() ) / pair.first.length();
    s = ((pair.first.orientation()) ? s : (1.0 - s) );
    //TODO:@todo : computation of s is redundant with what is done in the functor
    pair.second.InlineEvalProperties(s, scal_prop_ids, vect_prop_ids, scal_prop_res, vect_prop_res);
}

} // Close namespace computeProperties


} // Close namespace convol

} // Close namespace expressive
