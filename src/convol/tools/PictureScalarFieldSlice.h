/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
 \file: PictureScalarFieldSlice.h
 
 Language: C++
 
 License: Convol license
 
 \author: Cedric Zanni
 E-Mail: cedric.zanni@inrialpes.fr
 
 Description:    Computation of a picture of a scalar field in a given plane.
                Can draw specified iso-value in color (red for the highest, 
                blue for the lowest)
 
    @todo:    problem with the color used for iso
 
    @todo: use DL1 to choose if pixel are close enought to iso of interest
 
    @todo: add the possibility to draw the projection of the skeleton for scalar field defined by skeleton
    @todo: be carefull, the same iso can be registered several time in the list (but clear iso remove only the first one encountered)
    @todo: are convertion from unsigned short to char correct? NO !!!!
    @todo: understand why iso-color are slightly modified
 
 Platform Dependencies: None
 */

#ifndef CONVOL_PICTURE_SCALAR_FIELD_SLICE_H_
#define CONVOL_PICTURE_SCALAR_FIELD_SLICE_H_

#include <convol/ConvolRequired.h>
#include <convol/ScalarField.h>

//#include<vector>
//#include<list>
//#include<math.h>
//
////TODO:@todo : temporary : to be removed as soon as the "territory function" has been removed
//#include<set>
//#include<Convol/include/ScalarFields/BlobtreeNode/BlobtreeNodeT.h>

namespace expressive {

    namespace convol {

/*! \brief    Class to compute a picture of sectional view of a given scalar field
 *            Can draw specified iso-value in color (red for the highest,
 *            blue for the lowest)
 *            The resolution of the picture can't be modified after the creation
 *            See conventions in the detail decription below.
 *
 *  ***WARNING*** Conventions : 
 *    The resolution of the picture (res_dir1_ and res_dir2_) should be even
 *    normal_, dir_1_ and dir_2_ should be an orthonormal frame
 *  ***WARNING*** the first dir is designated by "true", the second by "false"
 */
class CONVOL_API PictureScalarFieldSlice
{
public:

    PictureScalarFieldSlice(ScalarField* scalar_field,
                            const Point& origin,
                            const Vector& dir1, const Vector& dir2,
                            const Scalar length_dir1, const Scalar length_dir2,
                            unsigned int res1, unsigned int res2,
                            const Scalar scaling_factor, const Scalar wave_begining)
        :   res_dir1_(res1), res_dir2_(res2),
            scalar_field_(scalar_field),
            origin_(origin), normal_(dir1.cross(dir2)),
            dir1_(dir1), dir2_(dir2),
            length_dir1_(length_dir1), length_dir2_(length_dir2),
            scaling_factor_(scaling_factor), wave_begining_(wave_begining)
    {
        assert(!(res1 & 1) && !(res2 & 1)); // test if res1 and res2 are even

        tab_field_value_ = new Scalar[res_dir1_*res_dir2_];
        tab_picture_field_ = new char[4*res_dir1_*res_dir2_];
    }
    
    ~PictureScalarFieldSlice()
    {
        delete [] tab_field_value_;
        delete [] tab_picture_field_;
        list_iso_.clear();
    }

    /////////////////
    /// Modifiers ///
    /////////////////
    
    inline void set_scalar_field(ScalarField const* scalar_field) { scalar_field_ = scalar_field; }
    
    inline void set_origin(const Point& origin){ origin_ = origin; }

    inline void set_dir1(Vector dir1)
    {
        dir1_ = dir1;
        normal_ = dir1_.cross(dir2_);
    }
    inline void set_dir2(Vector dir2)
    {
        dir2_ = dir2;
        normal_ = dir1_.cross(dir2_);
    }
    inline void set_resolution(unsigned int res1, unsigned int res2)
    {
        res_dir1_ = res1;
        res_dir2_ = res2;
        delete [] tab_field_value_;
        delete [] tab_picture_field_;
        tab_field_value_ = new Scalar[res_dir1_*res_dir2_];
        tab_picture_field_ = new char[4*res_dir1_*res_dir2_];
    }
    inline void set_length(Scalar length1, Scalar length2)
    {
        length_dir1_ = length1;
        length_dir2_ = length2;
    }

    inline void set_scaling_factor(Scalar scaling_factor)
    {
        scaling_factor_ = scaling_factor;
    }
    inline void set_wave_begining(Scalar wave_begining)
    {
        wave_begining_ = wave_begining;
    }

    void set_plane_and_length(Point origin, Vector normal, Vector dir1, Scalar length1, Scalar length2);

    /** \brief Add iso to list_iso_ (and keep it sorted)
     *   Use bubble sorting to insert the iso in list_iso
     *   \return void.
     */
    void add_iso(Scalar iso);
    void clear_iso(Scalar iso);
    void clear_all_iso()
    {
        list_iso_.clear();
    }

    /** \brief Evaluate the scalar field for all the pixel of the picture plane
     *   This values are saved in tab_field_value_ .
     *   \return void.
     */
    void update_field_value();

    /** \brief Compute the color of all the pixel of the picture plane
     *   The color is a blend of red and blue if the value is near an iso of interest
     *   The color is a grayscale otherwise 
     *   This values are saved in tab_picture_field_ .
     *   update_field_value need to be called before
     *   \return void.
     */
    void update_picture_field();

//    //TODO:@todo : temporary
//    void update_picture_territory(std::vector< std::set<BlobtreeNodeT<Traits>*> > connex_node_tab);

    ///////////////
    // Accessors //
    ///////////////
    
//    /** \brief Return the pointer to the table with all the pixel color of the image
//     *   \return tab_picture_field(char*)
//     */
//    // Maxime : I commented this functiun since it gives an unsafe access we should not need
//    inline char* tab_picture_field(){ return tab_picture_field_; }

    // WARNING : does not really work as expected...
    inline unsigned int tab_picture_value(unsigned int index) const
    {
        return *((unsigned int*) (tab_picture_field_ + 4*index));
    }
    inline char tab_picture_value(unsigned int index, unsigned int channel) const
    {
        return tab_picture_field_[4*index+channel];
    }
    
    inline Point origin() const { return origin_; }
    inline Vector normal() const { return normal_; }

    inline const Vector& dir1() const { return dir1_; }
    inline const Vector& dir2() const { return dir2_; }
    inline const Scalar& length_dir1() const { return length_dir1_; }
    inline const Scalar& length_dir2() const { return length_dir2_; }

    inline unsigned int res_dir1() const { return res_dir1_; }
    inline unsigned int res_dir2() const { return res_dir2_; }

    inline const Scalar& scaling_factor() const { return scaling_factor_; }
    inline const Scalar& wave_begining() const { return wave_begining_; }
    
    char *tab_picture_field() const;
private:

    //Picture
    unsigned int res_dir1_; // Should be even
    unsigned int res_dir2_; // Should be even

    Scalar* tab_field_value_;
    char* tab_picture_field_;
    
    //ImplicitSurface
    ScalarField const* scalar_field_;
    
    //Plane
    Point origin_;
    Vector normal_;

    Vector dir1_;
    Vector dir2_;

    Scalar length_dir1_;
    Scalar length_dir2_;


        // parameter of the mapping between field value and color
        Scalar scaling_factor_;
        Scalar wave_begining_;

    //Iso-value to be drawn
    std::list<Scalar> list_iso_;
};

} // Close namespace convol
} // Close namespace expressive

    
#endif // CONVOL_PICTURE_SCALAR_FIELD_SLICE_H_
