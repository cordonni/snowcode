#define COPY_SPLITTED_METABALLS

namespace expressive {

namespace convol {


template<typename DataType>
void FittedBVH<DataType>::Init(const core::AABBox &aabb)
{
    if(data_.size() == 0) {
        nodes_.push_back( NodeBVH(core::AABBox(), 0, 0) ) ;
    } else {
        v_old_index_.resize(data_.size());
        for(unsigned int id = 0; id<data_.size(); ++id)
            v_old_index_[id] = id;

        nodes_.reserve(2*data_.size()-1);
        nodes_.push_back( NodeBVH(aabb, 0, (unsigned int) data_.size()) ) ;

        BuildNode(0);

        FinalizeConstruction();
    }
}

template<typename DataType>
void FittedBVH<DataType>::BuildNode(unsigned int parent_id)
{
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///TODO:@todo : this first binning does not take into account the splitted_data in computation cost (this is probably problematic)
///TODO:@todo : check ratio between bbox nb of data and nb of splitted data (would provide an idea of the necessity of taking into account splitted data in cost computation)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    NodeBVH &parent = nodes_[parent_id]; // because its re-used everywhere

    if ( parent.end_ - parent.first_primitive_ < 6 ) {
        //TODO:@todo : the limit should be choosen carefully
        return;
    }

    /// Find best splitting plane by binning ///

    // Compute bounding box of barycenters
    core::AABBox barycenter_bbox = core::AABBox();
    for (unsigned int i= parent.first_primitive_; i < parent.end_; ++i) {
        barycenter_bbox.Enlarge(barycenter_[v_old_index_[i]]);
    }

    // Choose main direction (for now max dimension of barycenter_bbox )
    std::ptrdiff_t index_max;
    Scalar norm_max_dim = (barycenter_bbox.max() - barycenter_bbox.min()).maxCoeff(&index_max);
    unsigned int dim = (unsigned int) index_max; // equal to either 0,1 or 2

    // Compute the value required for the BinID function (that associate a position to a bin).
    Scalar k1 = ((Scalar)k_) * (0.9999999) / norm_max_dim;
    Scalar k0 = barycenter_bbox.min()[dim];
    // binID = (unsigned int) (k1 * c_k - k0); // which is kind of chip

    /// Create the bins ///
    // This are some "quicksort step" using the splitting planes information as pivot
    // NB : this manipulation of data_ left the splitted id unchanged since they do not "belong" to the parent node

    std::vector<unsigned int> v_bin_end(k_); // will contains at index i the first element that is next to bin if id i
    std::vector<core::AABBox> v_bin_data_bbox(k_); // this creates empty bounding-boxes for each bin

    v_bin_end[0] = parent.first_primitive_;
    for (unsigned int bin_id = 0; bin_id<k_-1; ++bin_id) {
        core::AABBox& bin_data_bbox = v_bin_data_bbox[bin_id];

        unsigned int left = v_bin_end[bin_id], right = parent.end_-1;
        while(left < right) {
            while( BinID(k0, k1, barycenter_[v_old_index_[left]][dim]) <= bin_id && left < parent.end_) {
                bin_data_bbox.Enlarge(data_[v_old_index_[left]].first);
                ++left;
            }
            while( BinID(k0, k1, barycenter_[v_old_index_[right]][dim]) > bin_id && right >= v_bin_end[bin_id] ) {
                --right;
            }

            if(left < right) {
                std::swap(v_old_index_[left], v_old_index_[right]);
                bin_data_bbox.Enlarge(data_[v_old_index_[left]].first);
            }
        }

        v_bin_end[bin_id]   =  left;
        v_bin_end[bin_id+1] =  left; // first element to be tested for next loop
    }

    // a last loop to compute data_bbox of last bin
    core::AABBox& bin_data_bbox = v_bin_data_bbox[k_-1];
    for(unsigned int i = v_bin_end[k_-1]; i < parent.end_; ++i) {
        bin_data_bbox.Enlarge(data_[v_old_index_[i]].first);
    }


    // Compute the cost of each possible subdivision
    // this is done using a double loop and then aggregating results

    std::vector<Scalar> cost_l_r(k_-1);
    Scalar previous_cost = 0.0;
    std::vector<core::AABBox> aabb_l_r(k_-1);
    core::AABBox &previous_aabb = aabb_l_r[0];
    for (unsigned int bin_id = 0; bin_id < k_-1; ++bin_id)
    {
        previous_cost = previous_cost + Cost( v_bin_end[bin_id]-parent.first_primitive_ );

        aabb_l_r[bin_id] = previous_aabb.Union(v_bin_data_bbox[bin_id]);
        previous_aabb = aabb_l_r[bin_id];
//        data_box.Enlarge(v_bin_data_bbox[bin_id]);

        //TODO:@todo : should take into account splitted bounding-box

        cost_l_r[bin_id] = ConditionalProba(aabb_l_r[bin_id], parent.bounding_box_ ) * previous_cost;
    }

    std::vector<Scalar> cost_r_l(k_-1);
    previous_cost = 0.0;
//    data_box = core::AABBox();
    std::vector<core::AABBox> aabb_r_l(k_-1);
    previous_aabb = aabb_r_l[0];
//    core::AABBox splitted_aabb;
//    unsigned int splitted_primitive;
    for (unsigned int bin_id = 0; bin_id < k_-1; ++bin_id)
    {
        previous_cost = previous_cost + Cost( parent.end_ - v_bin_end[k_-2-bin_id] +1 );

        aabb_l_r[bin_id] = previous_aabb.Union(v_bin_data_bbox[k_-1-bin_id]);
        previous_aabb = aabb_l_r[bin_id];
//        data_box.Enlarge(v_bin_data_bbox[k_-1-bin_id]);

//        //TODO:@todo : should take into account splitted bounding-box
//        ComputeSplittingInformationApprox(); //only take into account splitted informationfrom outside : not good enough

        cost_r_l[bin_id] =  ConditionalProba(aabb_l_r[bin_id], parent.bounding_box_ ) * previous_cost;
    }

    // Find minimal cost

    Scalar min_cost = Cost( parent.end_ - parent.first_primitive_);// the cost of parent
    unsigned int bin_limit = k_;
        // will be the first bin of the second box if a split that cost less than the parent is found
        // if still equal to k_ then splitting the node should be avoided
    for (unsigned int limit = 1; limit<k_; ++limit)
    {
        Scalar current_cost = cost_l_r[limit-1] + cost_r_l[limit-1];
        if(current_cost < min_cost) {
            min_cost = current_cost;
            bin_limit = limit;
        }
    }
    unsigned int next_to_limit = v_bin_end[bin_limit-1];

    // check if the subdivision is interesting or not
    if(bin_limit == k_)
        return ;

    ///  Create the child nodes ///

    parent.is_leaf_ = false;

    Scalar limit_dim = barycenter_bbox.min()[dim] + (((Scalar)bin_limit) / ((Scalar)k_)) * norm_max_dim ;
    unsigned int left_id = (unsigned int) nodes_.size();
    parent.child_left_ = left_id;
    Point min = Vector::Constant(expressive::kScalarMin*0.25);
    Point max = Vector::Constant(expressive::kScalarMax*0.25);
    max[dim] = limit_dim;
    core::AABBox bound_lower = core::AABBox(min, max);
    nodes_.push_back( NodeBVH(core::AABBox::Intersection(parent.bounding_box_, bound_lower),
                              parent.first_primitive_, next_to_limit ) );
//    nodes_.push_back( NodeBVH(node_bbox.splitLeftAlong(splitting_plane)) ) ; //TODO:@todo : intersection avec bbox semi infini...
    NodeBVH &left_child = nodes_[left_id];

    unsigned int right_id = (unsigned int) nodes_.size();
    parent.child_right_ = right_id;

    min = Vector::Constant(expressive::kScalarMin*0.25);
    min[dim] = limit_dim;
    max = Vector::Constant(expressive::kScalarMax*0.25);
    core::AABBox bound_upper = core::AABBox(min, max);
    nodes_.push_back( NodeBVH(core::AABBox::Intersection(parent.bounding_box_, bound_upper),
                              next_to_limit, parent.end_ ) );
//    nodes_.push_back( NodeBVH(node_bbox.splitRightAlong(splitting_plane)) ) ; //TODO:@todo : intersection avec bbox semi infini...
    NodeBVH &right_child = nodes_[right_id];


    // Compute boubing-box data for each child node
    core::AABBox left_data_bbox;
    for (unsigned int id = 0; id<bin_limit; ++id)
        left_data_bbox.Enlarge(v_bin_data_bbox[id]);
    core::AABBox right_data_bbox;
    for (unsigned int id = bin_limit; id<k_; ++id)
        right_data_bbox.Enlarge(v_bin_data_bbox[id]);


    ///  Compute the splitted data of each children and create subtree recursively if needed ///

    ComputeSplittedData(parent, left_child, right_child, left_data_bbox);
    left_child.bounding_box_ = core::AABBox::Intersection(left_child.bounding_box_, left_data_bbox); //TODO:@todo : would be nice to have a version of Intersection done in place

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO:@todo : optimize the result by further tightenning bounding box and removing unrequired splitted data
    // Then tighten the BoundingBox (by considering the number of child (or the) etc ...
    // relire article sur ce point, mais on doit pouvoir faire quelque chose en considérant la longueur de squelette et les poids)
    // and optimize out some splitMBall (re-read the conditions)
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    BuildNode(left_id);


    ComputeSplittedData(parent, right_child, left_child, right_data_bbox);
    right_child.bounding_box_ = core::AABBox::Intersection(right_child.bounding_box_, right_data_bbox); //TODO:@todo : would be nice to have a version of Intersection done in place

    /////////////////////////////////////////////////
    //TODO:@todo : optimize ... cf above ^
    /////////////////////////////////////////////////

    BuildNode(right_id);
}


template<typename DataType>
void FittedBVH<DataType>::ComputeSplittedData(const NodeBVH& parent, NodeBVH& child_node, const NodeBVH& other_node, core::AABBox& child_data_bbox)
{
    // iterate over split data from parent
    for(unsigned int id : parent.splitted_data_)
    {
        const core::AABBox &bbox = data_[id].first;
        if(bbox.Intersect(child_node.bounding_box_))
        {
            child_node.splitted_data_.push_back(id);
            child_data_bbox = child_data_bbox.Union(bbox); //TODO:@todo : it would be nice to have an enlarge function that work as the one with point as parameter
        }
    }
    // iterate over right data
    for(unsigned int id = other_node.first_primitive_; id < other_node.end_; ++id)
    {
        const core::AABBox &bbox = data_[v_old_index_[id]].first;
        if(bbox.Intersect(child_node.bounding_box_))
        {
            child_node.splitted_data_.push_back(v_old_index_[id]);
            child_data_bbox = child_data_bbox.Union(bbox); //TODO:@todo : would be nice to have an enlarge function that work as the one with point as parameter
        }
    }

}


template<typename DataType>
void FittedBVH<DataType>::FinalizeConstruction()
{
    barycenter_.clear();

#ifdef COPY_SPLITTED_METABALLS
    unsigned int nb_splitted_metaballs = 0;
    for(NodeBVH &node : nodes_) {
        if(node.is_leaf_) {
//                std::cout<<"Node info nb_primitive : " << node.end_ - node.first_primitive_ << " nb_splitted : " << node.splitted_data_.size() <<std::endl;
            nb_splitted_metaballs += (unsigned int) node.splitted_data_.size();
        } else {
            node.splitted_data_.clear();
        }
    }

    auto data_tmp = data_;
    data_.resize(data_.size() + nb_splitted_metaballs);

    unsigned int offset = 0;
    CopyData(0, offset, data_tmp);

    ork::Logger::DEBUG_LOGGER->logf("POLYGONIZER", "Total number of splitted data : %f", offset);
#else
    //TODO:@todo : could probably do this more efficiently ...
    std::map<unsigned int, unsigned int> mapping_old_to_new_index;
    for(unsigned int id = 0; id < data_.size(); ++id)
        mapping_old_to_new_index[v_old_index_[id]] = id;

    // update data_ (use a temporary vector beurk)
    auto data_tmp = data_;
    for(unsigned int id = 0; id < data_.size(); ++id)
        data_[id] = data_tmp[v_old_index_[id]];
    // if the number of split tend to be bounded by acceptable number, we can copy the splitted data as well
    // take more place in memory but improve data locality during evaluation ...

    for(NodeBVH &node : nodes_) {
        if(node.is_leaf_) {
            //                std::cout<<"Node info nb_primitive : " << node.end_ - node.first_primitive_ << " nb_splitted : " << node.splitted_data_.size() <<std::endl;
            for (unsigned int& splitted_index : node.splitted_data_)
                splitted_index = mapping_old_to_new_index[splitted_index];
        } else {
            node.splitted_data_.clear();
        }
    }
#endif

}


template<typename DataType>
void FittedBVH<DataType>::CopyData(unsigned int node_id, unsigned int &offset, const std::vector<std::pair<core::AABBox, DataType> >& data_tmp)
{
    auto &node = nodes_[node_id];

    if(node.is_leaf_) {
        for(unsigned int id=node.first_primitive_; id<node.end_; ++id)
            data_[id+offset] = data_tmp[v_old_index_[id]];

        for(unsigned int id=0; id<node.splitted_data_.size(); ++id)
            data_[id+node.end_+offset] = data_tmp[node.splitted_data_[id]];

        node.first_primitive_ += offset;
        offset += (unsigned int) node.splitted_data_.size();
        node.end_ += offset;

        node.splitted_data_.clear();
    } else {
        CopyData(node.child_left_, offset, data_tmp);
        CopyData(node.child_right_, offset, data_tmp);
    }
}


////TODO:@todo : rough approximation to be improved ...
//template<typename DataType>
//void FittedBVH<DataType>::ComputeSplittingInformationApprox(const NodeBVH& parent, const core::AABBox& node_bbox,/*NodeBVH& child_node, const NodeBVH& other_node,*/
//                                                            core::AABBox& splitted_data_bbox, unsigned int& nb_split)
//{
//    nb_split = 0;
//    splitted_data_bbox = core::AABBox();
//    // iterate over split data from parent
//    for(unsigned int id : parent.splitted_data_) {
//        const core::AABBox &bbox = data_[id].first;
//        //            if(bbox.Intersect(child_node.bounding_box_)) {
//        if(bbox.Intersect(node_bbox)) {
//            ++nb_split;
//            splitted_data_bbox.Enlarge(bbox);
//        }
//    }
//    ///TODO:@todo : following information are not yet computed when this function is called : find a way to improve it...
//    //        // iterate over right data
//    //        for(unsigned int id = other_node.first_primitive_; id < other_node.end_; ++id) {
//    //            const core::AABBox &bbox = data_[v_old_index_[id]].first;
//    //            if(bbox.Intersect(child_node.bounding_box_)) {
//    //                ++nb_split;
//    //                splitted_data_bbox.Enlarge(bbox);
//    //            }
//    //        }
//}


} // Close namespace convol

} // Close namespace expressive
