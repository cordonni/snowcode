/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: GradientEvaluationToolsT.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: General function concerning Vectors.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONVOL_GRADIENT_EVALUATION_TOOLS_H_
#define CONVOL_GRADIENT_EVALUATION_TOOLS_H_

#include<convol/ConvolRequired.h>

namespace expressive {

namespace convol {

class CONVOL_API GradientEvaluation
{
public:
    
    // Good when Type defines Eval with only 1 param : Eval(const Point&)
    template<typename Type>
    inline static Vector NumericalGradient6points(const Type& object,
                                                  const Point& point, const Scalar epsilon)
    {
        Vector grad;
        Point p_plus = point;
        Point p_minus = point;

        for(unsigned int i = 0; i<Point::RowsAtCompileTime; i++)
        {
            p_plus[i] += epsilon;
            p_minus[i] -= epsilon;
            grad[i] = (object.Eval(p_plus) - object.Eval(p_minus))/(2.0*epsilon);
            p_plus[i] -= epsilon;
            p_minus[i] += epsilon;
        }

        return grad;
    }

    // Good when Type defines Eval with only 2 params : Eval(const Param1& param_1, const Point&)
    template<typename Type, typename Param1>
    inline static Vector NumericalGradient6points(const Type& object,
                                                  const Param1& param_1,
                                                  const Point& point, const Scalar epsilon)
    {
        Vector grad;
        Point p_plus = point;
        Point p_minus = point;

        for(unsigned int i = 0; i<Point::RowsAtCompileTime; i++)
        {
            p_plus[i] += epsilon;
            p_minus[i] -= epsilon;
            grad[i] = (object.Eval(param_1, p_plus) - object.Eval(param_1, p_minus))/(2.0*epsilon);
            p_plus[i] -= epsilon;
            p_minus[i] += epsilon;
        }

        return grad;
    }

    // Good when Type defines Eval with only 3 params : Eval(const Param1& param_1, const Param1& param_2, const Point&)
    template<typename Type, typename Param1, typename Param2>
    inline static Vector NumericalGradient6points(const Type& object,
        const Param1& param_1,
        const Param2& param_2,
        const Point& point, const Scalar epsilon)
    {
        Vector grad;
        Point p_plus = point;
        Point p_minus = point;

        for(unsigned int i = 0; i<Point::RowsAtCompileTime; i++)
        {
            p_plus[i] += epsilon;
            p_minus[i] -= epsilon;
            grad[i] = (object.Eval(param_1,param_2, p_plus) - object.Eval(param_1,param_2, p_minus))/(2.0*epsilon);
            p_plus[i] -= epsilon;
            p_minus[i] += epsilon;
        }

        return grad;
    }
    

}; // End class GradientEvaluationT declaration

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_GRADIENT_EVALUATION_TOOLS_H_
