/**
Copyright (c) 2017, INPG
Main authors : Cédric Zanni, Maxime Quiblier, Antoine Begault
All rights reserved.

Redistribution and use in source and binary forms of this module (see bellow for a complete list of what is not included in this license), with or without modification, are permitted only for non-commercial uses, provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ConvergenceTools.h

   Language: C++

   License: Convol license

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Contains convergence tools
                (Newton method etc...)

   Platform Dependencies: None
*/


#pragma once
#ifndef CONVOL_CONVERGENCE_TOOLS_H_
#define CONVOL_CONVERGENCE_TOOLS_H_



// convol dependencies
#include<convol/ConvolRequired.h>
#include<convol/ScalarField.h>


////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : these functions should probably be improved ...
/// TODO:@todo : add function (and check them) as they are needed in other codes ...
////////////////////////////////////////////////////////////////////////////////////////


namespace expressive {

namespace convol {


class CONVOL_API Convergence
{
public:


/** \brief This algorithm uses Newton convergence to find a point epsilon close to
*        a point "p" such that the given potential "pot" evaluated at "p" is "value".
*        The search is constrained on line defined by (origin, search_dir), and between bounds
*        defined by min_absc and max_absc which are the abscissae on the line with respect
*        to origin and search_dir. search_dir should be normalized.
*        The starting point is given with an abscissa : origin + starting_point_absc*search_dir
*
*   \param pot 
*   \param origin Point choosen as origin in the search line frame.
*   \param search_dir unit vector that, together with origin, defines the searching line
*   \param min_absc Minimum abscissa on the line : the algorithm will not search for a point below this abscissa.
*   \param max_absc Maximum abscissa on the line : the algorithm will not search for a point above this abscissa.
*   \param starting_point_absc Abscissa of the starting point, with respect to the search dir.
*   \param value The potential value we are looking for on the line with respect to pot.Eval(..)
*   \param epsilon We want the result to be at least epsilon close to the surface with respect to the
*                   distance Vector.norm(), we suppose this norm to be the one associated with the dot product Vector.operator |
*   \param grad_epsilon Accuracy in gradient computation.
*   \param n_max_step Maximum of newton step before giving up.
*
*    \return true if a point p such that |pot.Eval(p) - value| < epsilon was found.
*           false and the current searching point otherwise.
*   
*
*   @todo write documentation to talk about failure cases.
*   @todo Should not normalise search_dir. Change that here and in all part of code where this is used.
*/
static bool SafeNewton1D(   const ScalarField& pot,
                            const Point& origin,
                            const Vector& search_dir,
                            Scalar& min_absc,
                            Scalar& max_absc,
                            Scalar starting_point_absc,
                            Scalar value,
                            Scalar epsilon,
                            Scalar grad_epsilon,
                            unsigned int n_max_step,
                            Point& res_point);



//                                                                new_point, new_normal, // Don't worry new_normal will be normlized in this function...
//                                                                min_absc, max_absc, 0.0,
//                                                                implicit_surface_->iso_value(),
//                                                                epsilon_, epsilon_grad_,
//                                                                10,
//                                                                conv_res
//static bool SafeNewton1DAnalyticalGrad(   const ScalarField&    pot,
//                            const Point&     origin,
//                                  Scalar  field_at_origin,
//                            const Vector& grad_at_origin,
//                                  Scalar& min_absc,
//                                  Scalar& max_absc,
//                            const Scalar starting_point_absc,
//                            const Scalar value, // value to be find
//                            const Scalar epsilon,
//                            const Scalar grad_epsilon,
//                            const unsigned int n_max_step,
//                                  Point& res_point) // on devrait aussi avoir le gradient et la valeur si on le souhaite ...
//{
//    assert(grad_at_origin[0] != 0.0 || grad_at_origin[1] != 0.0 || grad_at_origin[2] != 0.0);

//    Vector search_dir_unit = grad_at_origin;
//    search_dir_unit.normalize();

//    Scalar curr_point_absc = starting_point_absc;

//    Scalar curr_field_value = field_at_origin;    // contain the field value at curr_point
//    Vector curr_grad = grad_at_origin;

//    // Newton step until we overpass the surface
//    // the minimum step is set to epsilon, that ensure we will cross the surface.
//    Scalar dir_derivative;
//    Scalar step;
//    unsigned int i = 0;
//    unsigned int consecutive_small_steps = 0;
//    while( max_absc - min_absc > epsilon && i < n_max_step && consecutive_small_steps < 2)
//    {
//        dir_derivative = curr_grad | search_dir_unit; //grad = (pot.Eval(origin + (curr_point_absc+grad_epsilon)*search_dir_unit)-curr_field_value)/grad_epsilon;

//        // a relire
//        if(dir_derivative != 0.0)
//        {
//            step = (value-curr_field_value)/dir_derivative;
//            //taille de pas minimal (epsilon)
//            if(step > epsilon || step < -epsilon)
//            {
//                curr_point_absc += step;
//                consecutive_small_steps = 0;
//            }
//            else
//            {
//                if(step>0.0)
//                {
//                    curr_point_absc += epsilon;
//                }
//                else
//                {
//                    curr_point_absc -= epsilon;
//                }
//                consecutive_small_steps++;
//            }


//            // If the newton step took us above the max limit, or under the min limit
//            // we perform a kind of dichotomy step to bring back the current point into the boundaries
//            if(curr_point_absc >= max_absc || curr_point_absc <= min_absc)
//            {
//                // si on est partie dans les choux ...
//                curr_point_absc = (max_absc+min_absc)/2.0;
//                pot.EvalValueAndGrad(origin + curr_point_absc*search_dir_unit, grad_epsilon, curr_field_value, curr_grad);
//            }
//            else
//            {
//                // Update min_absc and max_absc depending on the field value
//                Scalar new_curr_field_value;
//                pot.EvalValueAndGrad(origin + curr_point_absc*search_dir_unit, grad_epsilon, new_curr_field_value, curr_grad);
//                if( (curr_field_value-value)*(new_curr_field_value-value) > 0.0 ) // the step did not lead us to cross the surface
//                {
//                    if(step>0.0)
//                    {
//                        min_absc = curr_point_absc;
//                    }
//                    else
//                    {
//                        max_absc = curr_point_absc;
//                    }
//                }
//                else
//                {
//                    if(step>0.0)
//                    {
//                        max_absc = curr_point_absc;
//                    }
//                    else
//                    {
//                        min_absc = curr_point_absc;
//                    }
//                }

//                curr_field_value = new_curr_field_value;
//            }
//        }
//        else
//        {
//            // Kind of dichotomy step
//            curr_point_absc = (max_absc+min_absc)/2.0;
//            pot.EvalValueAndGrad(origin + curr_point_absc*search_dir_unit, grad_epsilon, curr_field_value, curr_grad);
//        }

//        ++i;
//    }
//    res_point = origin + curr_point_absc*search_dir_unit;

//    if(i>=n_max_step)
//    {
//        return false;
//    }
//    else
//    {
//        return true;
//    }
//}




//
inline static Point SafeNewton1DDichotomy( const ScalarField&    pot,
                                           const Point&     origin,    // the abs of this point must be 0
                                           const Vector& search_dir,   // NB : all the abs are defined according to this orientation !
                                           Scalar inside_absc,
                                           Scalar outside_absc,
                                           const Scalar starting_point_absc,
                                           const Scalar value,
                                           const Scalar epsilon,
                                           const Scalar grad_epsilon)
{
    // if outside_abs is smaller than inside_abs we want to reverse all the abs (and, of course, the direction)
    Vector search_dir_unit = (outside_absc-inside_absc)*search_dir;
    search_dir_unit.normalize();

    if(outside_absc<inside_absc)
    {
        outside_absc *= -1.0;
        inside_absc *= -1.0;
    }

//    assert(pot.Eval(origin + outside_absc*search_dir_unit) <= value);
//    assert(pot.Eval(origin + inside_absc*search_dir_unit) >= value);


    Scalar curr_point_absc = starting_point_absc;

    Scalar curr_field_value;    // contain the field value at curr_point

    // Newton step until we overpass the surface
    // the minimum step is set to epsilon, that ensure we will cross the surface.
    Scalar grad;
    Scalar step;
    while( outside_absc - inside_absc > epsilon)
    {
        curr_field_value = pot.Eval(origin + curr_point_absc*search_dir_unit) ;
        grad = (pot.Eval(origin + (curr_point_absc+grad_epsilon)*search_dir_unit)-curr_field_value)/grad_epsilon;
        if(grad != 0.0)
        {
            step = (value-curr_field_value)/grad;
            if(step > epsilon || step < -epsilon)
            {
                curr_point_absc += step;
            }
            else
            {
                if(step>0.0)
                {
                    curr_point_absc += epsilon;
                }
                else
                {
                    curr_point_absc -= epsilon;
                }
            }


            // If the newton step took us above the max limit, or under the min limit
            // we perform a dichotomy step to bring back the current point into the boundaries
            if(curr_point_absc >= outside_absc || curr_point_absc <= inside_absc)
            {
                curr_point_absc = (outside_absc+inside_absc)/2.0;
            }
        }
        else
        {
            // Dichotomy step
            curr_point_absc = (outside_absc+inside_absc)/2.0;
            curr_field_value = pot.Eval(origin + curr_point_absc*search_dir_unit);
        }

        // Update min_absc and max_absc depending on the field value
        curr_field_value = pot.Eval(origin + curr_point_absc*search_dir_unit);
        if( curr_field_value > value ) // the current point is inside
        {
            inside_absc = curr_point_absc;
        }
        else
        {
            outside_absc = curr_point_absc;
        }
    }
    return origin + curr_point_absc*search_dir_unit;
}




//// Limitations: 3D only, but can easily be rewritten for nD
//// The algorithm stops when :
//// - 2 consecutive steps are smaller than epsilon
//// - OR n_max_step is reached
//// @todo write documentation to talk about failure cases.
//static Point SafeNewton3D(    const ScalarField&    pot,
//                    const Point&  starting_point,
//                    const Scalar value,
//                    const Scalar epsilon,
//                    const Scalar grad_epsilon,
//                    const unsigned int n_max_step,
//                    const Scalar& r_max)
//{
//        Point moving_point = starting_point;

//        Scalar curr_field_value;    // contain the field value at curr_point

//        Vector grad;
//        Point last_moving_point;
//        unsigned int i = 1;
//        unsigned int consecutive_small_steps = 0;
//        while( consecutive_small_steps != 2 && i<=n_max_step)
//        {
//            last_moving_point = moving_point;

//            curr_field_value = pot.Eval(moving_point) ;
//            grad = pot.EvalGrad(moving_point, grad_epsilon);
//            if(grad[0] != 0.0 || grad[1] != 0.0 || grad[2] != 0.0 )
//            {
//                Scalar step = (value-curr_field_value)/grad.sqrnorm();
//                if(step < epsilon && step > -epsilon)
//                {
//                    if(step>0.0)
//                    {
//                        step = epsilon/grad.norm();
//                    }
//                    else
//                    {
//                        step = -epsilon/grad.norm();
//                    }
//                    consecutive_small_steps++;
//                }
//                else
//                {
//                    consecutive_small_steps = 0;
//                }
//                moving_point += step*grad;

//                // If the newton step took us above the limit distance from the starting point,
//                // we use a dichotomy between its last position and the current one.
//                if( (moving_point-starting_point).sqrnorm() > r_max*r_max)
//                {
//                    if( (pot.Eval(moving_point)-value)*(curr_field_value-value) < 0.0)   // can only use dichotomy if one point is inside and one outside among (moving_point and last_moving_point)
//                    {
//                        moving_point = 0.5*(moving_point+last_moving_point);
//                    }
//                    else
//                    {
//                        // In this case we have no clue what to do, so just break...
//                        break;
//                    }
//                }
//            }
//            else
//            {
//                // Check the point between last_moving_point and starting_point which is closest to the surface and return it.
//                if(fabs(pot.Eval(last_moving_point)-value) > fabs(pot.Eval(starting_point)-value))
//                {
//                    return starting_point;
//                }
//                else
//                {
//                    return last_moving_point;
//                }
//            }

//            ++i;

//        }
//        return moving_point;
//}

//// Limitations: 3D only, but can easily be rewritten for nD
//static Point NStepSafeNewton3D(    const ScalarField&    pot,
//                                const Point&  starting_point,
//                                const Scalar& r_max,
//                                unsigned int n_step,
//                                const Scalar value,
//                                const Scalar grad_epsilon)
//{
//    Point moving_point = starting_point;

//    Scalar curr_field_value;    // contain the field value at curr_point

//    Vector grad;
//    Point last_moving_point;
//    for(unsigned int i = 1; i<=n_step; ++i)
//    {
//        last_moving_point = moving_point;

//        curr_field_value = pot.Eval(moving_point) ;
//        grad = pot.EvalGrad(moving_point, grad_epsilon);  //(pot.Eval(origin + (curr_point_absc+grad_epsilon)*search_dir)-curr_field_value)/grad_epsilon;
//        if(grad[0] != 0.0 || grad[1] != 0.0 || grad[2] != 0.0 )
//        {
//            Scalar step = (value-curr_field_value)/grad.sqrnorm();
//            moving_point += step*grad;

//            // If the newton step took us above the limit distance from the starting point,
//            // we use a dichotomy between its last position and the current one.
//            if( (moving_point-starting_point).sqrnorm() > r_max*r_max)
//            {
//                moving_point = 0.5*(moving_point+last_moving_point);
//            }
//        }
//        else
//        {
//            return moving_point;
//        }

//    }
//    return moving_point;
//}

//// Limitations: 3D only, but can easily be rewritten for nD
//// Perform steping toward intersection of 2 scalar fields using newton method.
//static Point IntersectionNStepSafeNewton3D(    const ScalarField&    sf1,
//                                                const ScalarField&    sf2,
//                                                const Point&  starting_point,
//                                                const Scalar& r_max,
//                                                unsigned int n_step,
//                                                const Scalar value_1,
//                                                const Scalar value_2,
//                                                const Scalar grad_epsilon)
//{
//    Point moving_point = starting_point;

//    Scalar curr_field_value;    // contain the field value at curr_point

//    Vector grad;
//    Point last_moving_point;
//    for(unsigned int i = 1; i<=n_step; ++i)
//    {
//        // 1 step in intersection direction
//        last_moving_point = moving_point;

//        curr_field_value = sf1.Eval(moving_point) - sf2.Eval(moving_point) ;
//        grad = sf1.EvalGrad(moving_point, grad_epsilon) - sf2.EvalGrad(moving_point, grad_epsilon);
//        if(grad[0] != 0.0 || grad[1] != 0.0 || grad[2] != 0.0 )
//        {
//            Scalar step = (value_1-value_2-curr_field_value)/grad.sqrnorm();
//            moving_point += step*grad;

//            // If the newton step took us above the limit distance from the starting point,
//            // we use a dichotomy between its last position and the current one.
//            if( (moving_point-starting_point).sqrnorm() > r_max*r_max)
//            {
//                moving_point = 0.5*(moving_point+last_moving_point);
//            }
//        }
//        else
//        {
//            return moving_point;
//        }

//        last_moving_point = moving_point;

//        curr_field_value = sf1.Eval(moving_point) ;
//        grad = sf1.EvalGrad(moving_point, grad_epsilon);  //(pot.Eval(origin + (curr_point_absc+grad_epsilon)*search_dir)-curr_field_value)/grad_epsilon;
//        if(grad[0] != 0.0 || grad[1] != 0.0 || grad[2] != 0.0 )
//        {
//            Scalar step = (value_1-curr_field_value)/grad.sqrnorm();
//            moving_point += step*grad;

//            // If the newton step took us above the limit distance from the starting point,
//            // we use a dichotomy between its last position and the current one.
//            if( (moving_point-starting_point).sqrnorm() > r_max*r_max)
//            {
//                moving_point = 0.5*(moving_point+last_moving_point);
//            }
//        }
//        else
//        {
//            return moving_point;
//        }

//        last_moving_point = moving_point;

//        curr_field_value = sf2.Eval(moving_point) ;
//        grad = sf2.EvalGrad(moving_point, grad_epsilon);  //(pot.Eval(origin + (curr_point_absc+grad_epsilon)*search_dir)-curr_field_value)/grad_epsilon;
//        if(grad[0] != 0.0 || grad[1] != 0.0 || grad[2] != 0.0 )
//        {
//            Scalar step = (value_2-curr_field_value)/grad.sqrnorm();
//            moving_point += step*grad;

//            // If the newton step took us above the limit distance from the starting point,
//            // we use a dichotomy between its last position and the current one.
//            if( (moving_point-starting_point).sqrnorm() > r_max*r_max)
//            {
//                moving_point = 0.5*(moving_point+last_moving_point);
//            }
//        }
//        else
//        {
//            return moving_point;
//        }

//    }
//    return moving_point;
//}


//    typedef typename Traits::TriMesh TriMesh;
//    typedef typename TriMesh::VertexHandle VertexHandle;
//    typedef typename TriMesh::FaceHandle FaceHandle;
//    typedef typename TriMesh::EdgeHandle EdgeHandle;
//    typedef typename TriMesh::HalfedgeHandle HalfedgeHandle;

//inline static void MeshToIsoSurface(TriMesh& mesh,
//                                    const ScalarField& scalar_field,
//                                    const Scalar iso_value,
//                                    const Scalar epsilon,
//                                    const Scalar grad_epsilon,
//                                    const unsigned int n_max_step,    // use 10 if you don't know
//                                    Scalar r_max )              // Put -1.0 if you dont know
//{
//    if(r_max<0.0)
//    {
//        r_max = scalar_field.GetAxisBoundingBox(iso_value).ComputeSizeMax();
//    }

//    std::vector<Scalar> scal_prop_res(Traits::SF_SCALAR_PROPERTIES_CARDINAL);
//    std::vector<Vector> vect_prop_res(Traits::SF_SCALAR_PROPERTIES_CARDINAL);

//    for (typename TriMesh::VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it)
//    {
//        mesh.set_point(*v_it, SafeNewton3D( scalar_field,
//                                           mesh.point(*v_it),
//                                           iso_value,
//                                           epsilon,
//                                           grad_epsilon,
//                                           n_max_step,
//                                           r_max));
//        Scalar val;
//        Normal n;
//        scalar_field.EvalValueAndGradAndProperties(mesh.point(*v_it),grad_epsilon, Traits::kAllScalarProperties, Traits::kAllVectorProperties, val, n, scal_prop_res, vect_prop_res); //TODO : @todo : a changer
//        n.normalize();                                                     /* normalize gradient*/
//        mesh.set_normal(*v_it, -n);
//        Color col;
//        col[0] = scal_prop_res[Traits::E_Red];
//        col[1] = scal_prop_res[Traits::E_Green];
//        col[2] = scal_prop_res[Traits::E_Blue];
//        mesh.set_color(*v_it, col);
//    }
//}

}; // End class Convergence declaration

} // Close namespace convol

} // Close namespace expressive

#endif // CONVOL_CONVERGENCE_TOOLS_H_
