/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef MATHS_H
#define MATHS_H

#define _USE_MATH_DEFINES
#include <cmath>

#ifndef __APPLE__
#if defined _USE_MATH_DEFINES && !defined _MATH_DEFINES_DEFINED
    #define _MATH_DEFINES_DEFINED
    #define M_E		2.7182818284590452354
    #define M_LOG2E		1.4426950408889634074
    #define M_LOG10E	0.43429448190325182765
    #define M_LN2		0.69314718055994530942
    #define M_LN10		2.30258509299404568402
    #define M_PI		3.14159265358979323846
    #define M_PI_2		1.57079632679489661923
    #define M_PI_4		0.78539816339744830962
    #define M_1_PI		0.31830988618379067154
    #define M_2_PI		0.63661977236758134308
    #define M_2_SQRTPI	1.12837916709551257390
    #define M_SQRT2		1.41421356237309504880
    #define M_SQRT1_2	0.70710678118654752440
#endif
#endif

namespace expressive
{

/**
 * Returns x converted from radians to degrees (like the GLSL function).
 */
template<typename T>
inline T degrees(T x)
{
    return x * (180. / M_PI);
}

/**
 * Returns x converted from degrees to radians (like the GLSL function).
 */
template<typename T>
inline T radians(T x)
{
    return x * (M_PI / 180.);
}

/**
 * Returns a value between 0 and 1 depending on the value of t compared to
 * a and b (like GLSL's smoothstep).
 *
 * @return 0 if t < a, 1 if t > b, and a smooth transition between a and b.
 */
template<typename T>
inline T smoothStep(T a, T b, T t)
{
    if (t <= a) {
        return 0;
    }
    if (t >= b) {
        return 1;
    }
    T x = (t - a) / (b - a);
    return x * x * (static_cast<T>(3) - static_cast<T>(2) * x);
}

/**
 * Returns v rounded to the nearest integer.
 */
template<typename T>
inline T round(T v)
{
    return v >= (T) 0 ? floor(v + (T) 0.5L) : ceil(v - (T) 0.5L);
}

/**
 * Returns (1-t) a + t b (like in GLSL).
 */
template<typename T>
inline T mix(T a, T b, T t)
{
    return ((T) 1 - t) * a + t * b;
}

/**
 * Returns the arccosinus of x clamped to [-1,1].
 */
template<typename T>
inline T safe_acos(T x)
{
    if (x <= -1) {
        x = -1;
    } else if (x >= 1) {
        x = 1;
    }
    return std::acos(x);
}

/**
 * Returns the arcsinus of x clamped to [-1,1].
 */
template<typename T>
inline T safe_asin(T x)
{
    if (x <= -1) {
        x = -1;
    } else if (x >= 1) {
        x = 1;
    }
    return std::asin(x);
}

/**
 * Returns a value between 0 and 1 depending on the value of t compared to
 * a and b (NOT like GLSL).
 *
 * @return 0 if t < a, 1 if t > b, and (t - a) / (b - a) otherwise.
 */
template<typename T>
inline T step(T a, T b, T t)
{
    if (t <= a) {
        return 0;
    }
    if (t >= b) {
        return 1;
    }
    return (t - a) / (b - a);
}

} // namespace expressive


#endif // MATHS_H
