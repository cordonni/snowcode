#include "pluginTools/GLExpressiveEngine.h"

// core dependencies
#include "commons/Maths.h"

#include "core/geometry/MeshTools.h"
#include "core/functor/FunctorFactory.h"
#include "core/geometry/PrimitiveFactory.h"
#include "core/geometry/VectorTools.h"
#include "core/geometry/WeightedSegment.h"
#include "core/scenegraph/Light.h"
#include "core/scenegraph/Overlay.h"
#include "core/utils/ConfigLoader.h"

// convol dependencies
#include "convol/functors/kernels/compactpolynomial/HomotheticCompactPolynomial.h"
#include "convol/factories/BlobtreeNodeFactory.h"
#include "convol/blobtreenode/NodeOperatorT.h"

// plugintools dependencies
#include "pluginTools/gui/qml/QmlGlWidget.h"
#include "pluginTools/statemachine/StateMachine.h"
#include "pluginTools/gui/qml/QmlTypeRegisterer.h"
#include "pluginTools/scenemanipulator/CameraManipulator.h"
#include "pluginTools/scenemanipulator/SceneManipulator.h"

#include "ork/render/FrameBuffer.h"

#if defined(_MSC_VER)
#pragma warning(push, 0)
#pragma warning(disable: 4242)
#endif //_WIN32

// Qt dependencies
#include <QQmlContext>
#include <QtQuick/QQuickItem>
#include <QtQuickWidgets/QQuickWidget>

#include "GL/glew.h"

#if defined(_MSC_VER)
#pragma warning(pop)
#endif //_WIN32

//#ifdef __APPLE__
////#include <GLUT/glut.h>
//#else
//#include <GL/glut.h>
//#endif

//using namespace ork;
using namespace std;
using namespace expressive::core;
using namespace expressive::convol;
//using namespace expressive::ogretools;

namespace expressive {

namespace plugintools {

//MyScene::MyScene(ptr<ork::ResourceManager> resource_manager)
//{
//    UNUSED(resource_manager);
////    alpha = 20.0;
////    theta = 45.0;
////    fov = 80.0;
////    dist = 20.0;
////    program1 = resource_manager->loadResource("camera;spotlight;plastic;").cast<Program>();
////    program2 = resource_manager->loadResource("camera;spotlight;texturedPlastic;").cast<Program>();

//////    cube = resource_manager->loadResource("cube.mesh").cast<MeshBuffers>();
//////    plane = resource_manager->loadResource("plane.mesh").cast<MeshBuffers>();

////    cube = new Mesh<P3_N3_UV_C, unsigned int>(TRIANGLES, GPU_STATIC);
////    cube->addAttributeType(0, 3, A32F, false);
////    cube->addAttributeType(1, 3, A32F, false);
////    cube->addAttributeType(2, 2, A32F, false);
////    cube->addAttributeType(3, 4, A8UI, true);
////    cube->addVertex(P3_N3_UV_C(-1, -1, +1, 0, 0, +1, 0, 0, 255, 0, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, -1, +1, 0, 0, +1, 1, 0, 255, 0, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, +1, +1, 0, 0, +1, 1, 1, 255, 0, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, +1, +1, 0, 0, +1, 1, 1, 255, 0, 0, 0));
////    cube->addVertex(P3_N3_UV_C(-1, +1, +1, 0, 0, +1, 0, 1, 255, 0, 0, 0));
////    cube->addVertex(P3_N3_UV_C(-1, -1, +1, 0, 0, +1, 0, 0, 255, 0, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, -1, +1, +1, 0, 0, 0, 0, 0, 255, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, -1, -1, +1, 0, 0, 1, 0, 0, 255, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, +1, -1, +1, 0, 0, 1, 1, 0, 255, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, +1, -1, +1, 0, 0, 1, 1, 0, 255, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, +1, +1, +1, 0, 0, 0, 1, 0, 255, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, -1, +1, +1, 0, 0, 0, 0, 0, 255, 0, 0));
////    cube->addVertex(P3_N3_UV_C(-1, +1, +1, 0, +1, 0, 0, 0, 0, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(+1, +1, +1, 0, +1, 0, 1, 0, 0, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(+1, +1, -1, 0, +1, 0, 1, 1, 0, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(+1, +1, -1, 0, +1, 0, 1, 1, 0, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, +1, -1, 0, +1, 0, 0, 1, 0, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, +1, +1, 0, +1, 0, 0, 0, 0, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(+1, -1, -1, 0, 0, -1, 0, 0, 0, 255, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, -1, -1, 0, 0, -1, 1, 0, 0, 255, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, +1, -1, 0, 0, -1, 1, 1, 0, 255, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, +1, -1, 0, 0, -1, 1, 1, 0, 255, 255, 0));
////    cube->addVertex(P3_N3_UV_C(+1, +1, -1, 0, 0, -1, 0, 1, 0, 255, 255, 0));
////    cube->addVertex(P3_N3_UV_C(+1, -1, -1, 0, 0, -1, 0, 0, 0, 255, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, -1, -1, -1, 0, 0, 0, 0, 255, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, -1, +1, -1, 0, 0, 1, 0, 255, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, +1, +1, -1, 0, 0, 1, 1, 255, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, +1, +1, -1, 0, 0, 1, 1, 255, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, +1, -1, -1, 0, 0, 0, 1, 255, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, -1, -1, -1, 0, 0, 0, 0, 255, 0, 255, 0));
////    cube->addVertex(P3_N3_UV_C(-1, -1, -1, 0, -1, 0, 0, 0, 255, 255, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, -1, -1, 0, -1, 0, 1, 0, 255, 255, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, -1, +1, 0, -1, 0, 1, 1, 255, 255, 0, 0));
////    cube->addVertex(P3_N3_UV_C(+1, -1, +1, 0, -1, 0, 1, 1, 255, 255, 0, 0));
////    cube->addVertex(P3_N3_UV_C(-1, -1, +1, 0, -1, 0, 0, 1, 255, 255, 0, 0));
////    cube->addVertex(P3_N3_UV_C(-1, -1, -1, 0, -1, 0, 0, 0, 255, 255, 0, 0));

////    cube2 = new core::AbstractTriMesh();
////    cube2->addVertex(DefaultMeshVertex(-1, -1, +1, 0, 0, +1, 0, 0, 255, 0, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, -1, +1, 0, 0, +1, 1, 0, 255, 0, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, +1, +1, 0, 0, +1, 1, 1, 255, 0, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, +1, +1, 0, 0, +1, 1, 1, 255, 0, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, +1, +1, 0, 0, +1, 0, 1, 255, 0, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, -1, +1, 0, 0, +1, 0, 0, 255, 0, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, -1, +1, +1, 0, 0, 0, 0, 0, 255, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, -1, -1, +1, 0, 0, 1, 0, 0, 255, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, +1, -1, +1, 0, 0, 1, 1, 0, 255, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, +1, -1, +1, 0, 0, 1, 1, 0, 255, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, +1, +1, +1, 0, 0, 0, 1, 0, 255, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, -1, +1, +1, 0, 0, 0, 0, 0, 255, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, +1, +1, 0, +1, 0, 0, 0, 0, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, +1, +1, 0, +1, 0, 1, 0, 0, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, +1, -1, 0, +1, 0, 1, 1, 0, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, +1, -1, 0, +1, 0, 1, 1, 0, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, +1, -1, 0, +1, 0, 0, 1, 0, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, +1, +1, 0, +1, 0, 0, 0, 0, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, -1, -1, 0, 0, -1, 0, 0, 0, 255, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, -1, -1, 0, 0, -1, 1, 0, 0, 255, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, +1, -1, 0, 0, -1, 1, 1, 0, 255, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, +1, -1, 0, 0, -1, 1, 1, 0, 255, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, +1, -1, 0, 0, -1, 0, 1, 0, 255, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, -1, -1, 0, 0, -1, 0, 0, 0, 255, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, -1, -1, -1, 0, 0, 0, 0, 255, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, -1, +1, -1, 0, 0, 1, 0, 255, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, +1, +1, -1, 0, 0, 1, 1, 255, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, +1, +1, -1, 0, 0, 1, 1, 255, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, +1, -1, -1, 0, 0, 0, 1, 255, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, -1, -1, -1, 0, 0, 0, 0, 255, 0, 255, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, -1, -1, 0, -1, 0, 0, 0, 255, 255, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, -1, -1, 0, -1, 0, 1, 0, 255, 255, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, -1, +1, 0, -1, 0, 1, 1, 255, 255, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(+1, -1, +1, 0, -1, 0, 1, 1, 255, 255, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, -1, +1, 0, -1, 0, 0, 1, 255, 255, 0, 0));
////    cube2->addVertex(DefaultMeshVertex(-1, -1, -1, 0, -1, 0, 0, 0, 255, 255, 0, 0));

////    plane = new Mesh<P3_N3_UV_C, unsigned int>(TRIANGLES, GPU_STATIC);
////    plane->addAttributeType(0, 3, A32F, false);
////    plane->addAttributeType(1, 3, A32F, false);
////    plane->addAttributeType(2, 2, A32F, false);
////    plane->addAttributeType(3, 4, A8UI, true);
////    plane->addVertex(P3_N3_UV_C(-10, +10, 0, 0, 0, +1, 0, 0, 248, 166, 10, 0));
////    plane->addVertex(P3_N3_UV_C(+10, +10, 0, 0, 0, +1, 1, 0, 248, 166, 10, 0));
////    plane->addVertex(P3_N3_UV_C(+10, -10, 0, 0, 0, +1, 1, 1, 248, 166, 10, 0));
////    plane->addVertex(P3_N3_UV_C(+10, -10, 0, 0, 0, +1, 1, 1, 248, 166, 10, 0));
////    plane->addVertex(P3_N3_UV_C(-10, -10, 0, 0, 0, +1, 0, 1, 248, 166, 10, 0));
////    plane->addVertex(P3_N3_UV_C(-10, +10, 0, 0, 0, +1, 0, 0, 248, 166, 10, 0));

////    //////////////////////////////
////    /// Just loading a .obj file and adding it to the scene.
////    ///
////    std::shared_ptr<AbstractTriMesh> mesh = std::make_shared<AbstractTriMesh>();
////    mesh->LoadFromObjFile("../../resources/meshes/ChevalierTPose.obj");
////    node = make_shared<core::SceneNode>(nullptr, nullptr, "montest");
////    node->set_tri_mesh(mesh);
////    mesh->FillColor(Color(1.0, 1.0, 0.5, 0.5));
////    node->Translate(Point( 0.0, 10.0, 0.0), SceneNode::TS_WORLD);

////    nodeRef = make_shared<core::SceneNode>(nullptr, nullptr, "montest2");
////    nodeRef->set_tri_mesh(mesh);
////    nodeRef->tri_mesh()->FillColor(Color(1.0, 0.0, 0.0, 1.0));

////    worldCamera = program1->getUniform3f("worldCameraPos"); // camera only set ONCE for both programs.
////    worldToScreenU = program1->getUniformMatrix4f("worldToScreen");
////    localToWorld1 = program1->getUniformMatrix4f("localToWorld"); /// positions, however, are set twice because they are different.
////    localToWorld2 = program2->getUniformMatrix4f("localToWorld");
//}

MyScene::MyScene(ork::ptr<core::SceneManager> scene_manager, ork::ptr<ork::ResourceManager> resource_manager)
{
    light = make_shared<core::Light>(scene_manager.get(), Point(00, 0, 20.0), Point(0.0, 0.0, 0.0));
    scene_manager->AddOverlay(light);

    std::shared_ptr<BasicTriMesh> plane = std::make_shared<BasicTriMesh>();
    plane->add_vertex(DefaultMeshVertex(-100, 0, +100, 0, 0, +1, 0, 0, 248, 166, 10, 0));
    plane->add_vertex(DefaultMeshVertex(+100, 0, +100, 0, 0, +1, 1, 0, 248, 166, 10, 0));
    plane->add_vertex(DefaultMeshVertex(+100, 0, -100, 0, 0, +1, 1, 1, 248, 166, 10, 0));
    plane->add_vertex(DefaultMeshVertex(+100, 0, -100, 0, 0, +1, 1, 1, 248, 166, 10, 0));
    plane->add_vertex(DefaultMeshVertex(-100, 0, -100, 0, 0, +1, 0, 1, 248, 166, 10, 0));
    plane->add_vertex(DefaultMeshVertex(-100, 0, +100, 0, 0, +1, 0, 0, 248, 166, 10, 0));
    std::shared_ptr<Overlay> plane_node = std::make_shared<Overlay>(scene_manager.get(), "virtualplane", Overlay::O_3DOVERLAY, "objectMethod");
    plane_node->set_tri_mesh(plane);
    plane_node->SetModule(resource_manager->loadResource("gridShader").cast<ork::Module>());
    plane_node->removeDefaultValues();
    scene_manager->AddOverlay(plane_node);

    ork::ptr<Overlay> log = new Overlay(scene_manager.get(), "loggerOverlay", Overlay::O_FOREGROUND, "logMethod");
    log->removeDefaultValues();
    scene_manager->AddOverlay(log);

    ork::ptr<Overlay> info = new Overlay(scene_manager.get(), "infoOverlay", Overlay::O_FOREGROUND, "infoMethod");
    info->removeDefaultValues();
    scene_manager->AddOverlay(info);

    ork::ptr<Overlay> background = new Overlay(scene_manager.get(), "backgroundOverlay", Overlay::O_BACKGROUND, "overlayMethod");
#ifdef __APPLE__
    background->SetModule(resource_manager->loadResource("backgroundShader_MAC").cast<ork::Module>());
#else
    background->SetModule(resource_manager->loadResource("backgroundShader").cast<ork::Module>());
#endif
    background->removeDefaultValues();
    scene_manager->AddOverlay(background);
}

GLExpressiveEngine::GLExpressiveEngine(QQuickView* quickWindow, const std::string expressiveConfig):
    QObject(quickWindow),
    ExpressiveEngine(),
    quick_window_(quickWindow), //    main_widget_(nullptr),
    m_initialized(false),
    m_valid_viewport(false),
    dirty_resources_(false)
  //    camera_object_(nullptr)
{
    Init(expressiveConfig);
}

GLExpressiveEngine::~GLExpressiveEngine()
{
    m_scene = nullptr;
    state_machine_ = nullptr;
    scene_manipulator_ = nullptr;
}

void GLExpressiveEngine::Init(const string &expressiveConfig)
{
    timer_.start();
    t_ = 0.0;
    dt_ = 0.0;

    ExpressiveEngine::Init(expressiveConfig);
    InitStateMachine();
    if (quick_window_ != nullptr) {
        qmlRegisterType<GLExpressiveEngine>("Expressive", 1, 0, "ExpressiveEngine");
        qml_context_ = quick_window_->rootContext();
        AddContent();
        //        RegisterQmlTypes();
        InitFactories();
        connect(quick_window_, &QQuickView::beforeRendering, this, &GLExpressiveEngine::DeferredInit, Qt::DirectConnection);
        //        connect(this, &GLExpressiveEngine::glInitialized, this,&GLExpressiveEngine::AddContent);
    } else {
        qmlRegisterType<GLExpressiveEngine>("Expressive", 1, 0, "ExpressiveEngine");
        InitWindow();
        AddContent();
        InitFactories();
        DeferredInit();
    }
}

void GLExpressiveEngine::InitWindow()
{
    QQuickWidget *view = new QQuickWidget;
    qml_context_ = view->rootContext();
    view->show();
    view->setResizeMode(QQuickWidget::SizeRootObjectToView);
    AddContent();
    view->setSource(QUrl(Config::getStringParameter("default_gui_file").c_str()));
}

void GLExpressiveEngine::InitFactories()
{
    FunctorFactory::GetInstance().SetDefaultFunctor(HomotheticCompactPolynomial6::StaticType());
    PrimitiveFactory::GetInstance().SetDefaultPrimitive(make_shared<WeightedSegment>());
    BlobtreeNodeFactory::GetInstance().SetDefaultBlobtreeNodeName(AbstractNodeOperatorNArity::StaticName());
}

void GLExpressiveEngine::InitStateMachine()
{
    state_machine_ =  std::make_shared<StateMachine>(this);
}

QQmlContext* GLExpressiveEngine::qml_context() const
{
    return qml_context_;
}

QQuickView* GLExpressiveEngine::quick_window() const
{
    return quick_window_;
}

std::shared_ptr<SceneManipulator> GLExpressiveEngine::scene_manipulator() const
{
    return scene_manipulator_;
}

std::shared_ptr<StateMachine> GLExpressiveEngine::state_machine() const
{
    return state_machine_;
}

/////////////////
/// Modifiers ///
/////////////////
void GLExpressiveEngine::set_scene_manipulator(std::shared_ptr<SceneManipulator> scene_manipulator)
{
    scene_manipulator_ = scene_manipulator;
}

void GLExpressiveEngine::RegisterQmlTypes()
{
    //    qmlRegisterType<GLExpressiveEngine>("Expressive", 1, 0, "ExpressiveEngine");
    //    qmlRegisterType<StateMachine>("Expressive", 1, 0, "StateMachine");
    //    qmlRegisterType<QmlGlWidget>("Expressive", 1, 0, "GLWidget");
}

void GLExpressiveEngine::DeferredInit()
{
    disconnect(quick_window_, &QQuickView::beforeRendering, this, &GLExpressiveEngine::DeferredInit);

    glewExperimental = GL_TRUE;
    auto glew_err = glewInit();
    if(glew_err!=GLEW_OK) {
        assert(false && "Error in glewInit");
    } else {
        auto err = glGetError();
        UNUSED(err); // only used on assertion, i.e. not in release
        assert(err == GL_NO_ERROR || err == GL_INVALID_ENUM);
        // the only opengl error to be ignored is GL_INVALID_ENUM since it is
        // a recurent problem depending on the plateform and glew version
        // SEE :  https://www.opengl.org/wiki/OpenGL_Loading_Library
    }

    //TODO:@todo : the vao value should be remembered because it is required later !
    GLuint vao=0;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

//    glBindVertexArray(1);
    assert(ork::FrameBuffer::getError() == GL_NO_ERROR);

    camera_manager_ = make_shared<Camera>(scene_manager_.get(), Vector(0.0, 10.0, 20.0), Vector(0.0, 0.0, 0.0));
    camera_manager_->set_manipulator(make_shared<CameraManipulator>(camera_manager_.get()));
    scene_manager_->set_current_camera(camera_manager_);
    //    scene_manager_->AddNode(camera_manager_);

    m_scene = make_shared<MyScene>(scene_manager(), resource_manager());

    //    if (quick_window_ == nullptr) {

    //    }
    m_initialized = true;

    emit glInitialized();
}

void GLExpressiveEngine::AddContent()
{
    //    quick_window_->rootContext()->setContextProperty("window", quick_window_);
    //    quick_window_->rootContext()->setContextProperty("expressive_engine", this);
    //    quick_window_->rootContext()->setContextProperty("state_machine", state_machine_.get());
    qml_context_->setContextProperty("window", quick_window_);
    qml_context_->setContextProperty("expressive_engine", this);
    qml_context_->setContextProperty("state_machine", state_machine_.get());

    //    emit initialized();
}

QSGTexture* GLExpressiveEngine::CreateTextureFromId(uint id, const QSize &size, QQuickWindow::CreateTextureOptions options) const
{
    return quick_window_->createTextureFromId(id, size, options);
}

void GLExpressiveEngine::Redisplay()
{
    ork::Timer t_all;
    ork::Timer t_resetstates;
    ork::Timer t_fbstates;
    ork::Timer t_update;
    ork::Timer t_draw;
    //    ork::Timer t_rotate;

    t_all.start();
    if (!m_initialized) {
        DeferredInit();
    }

    if (dirty_resources_) {
        resource_manager_->updateResources();
        dirty_resources_ = false;
    }

    double newT = timer_.end();
    this->dt_ = newT - this->t_;
    this->t_ = newT;

    t_resetstates.start();
    ork::FrameBuffer::resetAllStates();
    t_resetstates.end();

    t_fbstates.start();
    ork::ptr<ork::FrameBuffer> fb = ork::FrameBuffer::getDefault();
    fb->checkParametersState();
    t_fbstates.end();

    if(m_valid_viewport)
    {

        t_update.start();
        scene_manager_->Update(t_, dt_);
        t_update.end();

        t_draw.start();
        scene_manager_->Draw();
        t_draw.end();
    }
}

void GLExpressiveEngine::Resize(const Vector2i &size)
{
    if (m_initialized) {
        auto fb = ork::FrameBuffer::getDefault();
        Vector4i vp = fb->getViewport();
        if (vp[2] != size[0] || vp[3] != size[1]) {
            fb->setViewport(Vector4i(0, 0, size[0], size[1]));
            camera_manager_->UpdateCameraToScreen();
        }

        if(size[0] && size[1])
            m_valid_viewport = true;
    }
}

void GLExpressiveEngine::WindowChanged()
{
    dirty_resources_ = true;
}

void GLExpressiveEngine::NotifySceneInitialized()
{
    emit sceneInitialized();
}

//static QmlTypeRegisterer::Type<GLExpressiveEngine> GLExpressiveEngineQmlType("Expressive", 1, 0, "ExpressiveEngine");

} // namespace plugintools

} // namespace expressive
