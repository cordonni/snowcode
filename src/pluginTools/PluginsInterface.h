/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: PluginsInterfaces.h

   Language: C++

   License: Convol Licence

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Description: Header file defining interfaces for ConvolLab plugins.

                Plugins are very very free in Convol, they have access to all global variables of the application.

   Platform Dependencies: None
*/


#ifndef EXPRESSIVE_PLUGIN_INTERFACES_H
#define EXPRESSIVE_PLUGIN_INTERFACES_H

// plugintools dependencies
//#include "pluginTools/gui/QOrkWidget.h"
#include <pluginTools/gui/MainWindow.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
// Qt dependencies
#include <QtPlugin>
#include <QState>
#include <QString>
#include <QLabel>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{
namespace core
{
    class CommandManager;
    class ExpressiveEngine;
}
}

namespace expressive
{

namespace plugintools
{

/** \brief Interface for plug ins adding a mode to the application.
*
*          Modes can have an associated panel.
*          They are plugged in the ConvolLab StateMachine via the (ConvolLab::)StateMachine::main_ogre_widget_parallel_state_.
*
*/
class ModePluginInterface
{
public:
    virtual ~ModePluginInterface() {}

    // return the name of the plugin, must be unique or the plugin won't be loaded.
    virtual const QString& Name() const = 0;

    virtual const QLabel& BriefDescription() const = 0;

   /** \brief Initialized the plugin. This function is called in (ConvolLab::) StateMachine::LoadPlugins. It does build everything needed by the plugins, and especially its StateMachine.
    *
    *   \param convol_globals_manager One of the main variables of ConvolLab, see (ConvolLab::) namespace mainVariables.
    *   \param convol_ogre_globals_manager One of the main variables of ConvolLab, see (ConvolLab::) namespace mainVariables.
    *   \param ogre_widgets_manager One of the main variables of ConvolLab, see (ConvolLab::) namespace mainVariables.
    *   \param panel_manager One of the main variables of ConvolLab, see (ConvolLab::) namespace mainVariables.
    *   \param main_window MainWindow of the application, defined in ConvolLabLib and instanciated in ConvolLab.
    *   \param main_parallel_state Useless??
    *   \param main_ogre_widget_parallel_state The state associated with the actual states of the main GL Widget. Should (Must?) be the parent of the state returned by ModeMainState.
    */
    virtual void Init(core::ExpressiveEngine& engine,
                      QWidget* widget,
//                      PanelManager& panel_manager,
//                      qttools::MainWindow& main_window,
                      core::CommandManager& undo_redo_handler,
                      QState& main_parallel_state,
                      QState& main_widget_parallel_state) = 0;

   /** \brief return the main state of the mode, this state must be a child of the main_ogre_widget_parallel_state given to Init.
    *         This state is setup when calling Init.
    */
     virtual QState* ModeMainState() const = 0;


   /** \brief Return the panel associated with the plug in (NULL if no panel needed)
    */
    virtual QWidget* PanelWidget() const = 0;

};

/** \brief Interface for plug ins adding a "parallel" (running in a parallel state but in the same thread) process to the application.
*
*          Parallel processes can have an associated panel.
*          They are plugged in the ConvolLab StateMachine via the (ConvolLab::)StateMachine::main_parallel_state_.
*
*/
class ParallelPluginInterface
{
public:
    virtual ~ParallelPluginInterface() {}

    // return the name of the plugin, must be unique or the plugin won't be loaded.
    virtual QString& Name() = 0;

    virtual QLabel& BriefDescription() = 0;

   /** \brief Initialized the plugin. This function is called in (ConvolLab::) StateMachine::LoadPlugins. It does build everything needed by the plugins, and especially its StateMachine.
    *
    *   \param convol_globals_manager One of the main variables of ConvolLab, see (ConvolLab::) namespace mainVariables.
    *   \param convol_ogre_globals_manager One of the main variables of ConvolLab, see (ConvolLab::) namespace mainVariables.
    *   \param ogre_widgets_manager One of the main variables of ConvolLab, see (ConvolLab::) namespace mainVariables.
    *   \param panel_manager One of the main variables of ConvolLab, see (ConvolLab::) namespace mainVariables.
    *   \param main_window MainWindow of the application, defined in ConvolLabLib and instanciated in ConvolLab.
    *   \param main_parallel_state Should (Must?) be the parent of the state returned by ModeMainState.
    */
    virtual void Init(core::ExpressiveEngine& engine,
        QWidget* widget,
//        PanelManager& panel_manager,
//        qttools::MainWindow& main_window,
        QState& main_parallel_state) = 0;

    /** \brief return the main state of the mode, this state must be a child of the main_parallel_state given to Init.
    *         This state is setup when calling Init.
    */
    virtual QState* ModeMainState() = 0;
    /** \brief Return the panel associated with the plug in (NULL if no panel needed)
    */
    virtual QWidget* PanelWidget() = 0;
};
//! [1]

} // namespace plugintools

} // namespace expressive

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(expressive::plugintools::ModePluginInterface,
                    "inria.expressive.ModePluginInterface/1.0")
Q_DECLARE_INTERFACE(expressive::plugintools::ParallelPluginInterface,
                    "inria.expressive.ParallelPluginInterface/1.0")
QT_END_NAMESPACE

#endif // EXPRESSIVE_PLUGIN_INTERFACES_H
