//#include "SimpleConvolObject.h"
//#include "core/resource/ResourceTemplate.h"

//#include <regex>

//using namespace std;

//namespace expressive
//{

//namespace plugintools
//{

//SimpleConvolObject::SimpleConvolObject()
//{
//}

///// @cond RESOURCES

//class SimpleSkeletonResource : public core::ResourceTemplate<30, ConvolObject>
//{
//public:
//    SimpleConvolObjectResource(ResourceManager *manager, const std::string &name,
//                         std::shared_ptr<ResourceDescriptor> desc, const TiXmlElement *e = NULL)
//       : ResourceTemplate<30, ConvolObject>(manager, name, desc)
//    {
//        e = e == NULL ? desc->descriptor : e;
//        checkParameters(desc, e, "name,file,default_weight,has_weights,dimensions,delimiter,vertex_starter,edge_starter,n_vertices,n_edges,");

//        string filename = e->Attribute("file");

//        if (filename.length() == 0) {
//            return;
//        }

//        float default_weight = 1.0;
//        bool has_weights = false;
//        int dim = 3;
//        string delimiter = " ";
//        string vertex_starter = "v";
//        string edge_starter = "e";
//        int n_vertices = 0;
//        int n_edges = 0;

//        if (e->Attribute("has_weights") != nullptr) {
//            has_weights = e->Attribute("has_weights") == "true";
//        } else if (e->Attribute("default_weight") != nullptr) {
//            getFloatParameter(desc, e, "default_weight", &default_weight);
//        }

//        if (e->Attribute("dimensions") != nullptr) {
//            getIntParameter(desc, e, "dimensions", &dim);
//        }

//        if (e->Attribute("delimiter") != nullptr) {
//            delimiter = e->Attribute("delimiter");
//        }

//        if (e->Attribute("vertex_starter") != nullptr) {
//            vertex_starter = e->Attribute("vertex_starter");
//        }

//        if (e->Attribute("edge_starter") != nullptr) {
//            vertex_starter = e->Attribute("edge_starter");
//        }

//        if (e->Attribute("n_vertices") != nullptr) {
//            getIntParameter(desc, e, "n_vertices", &n_vertices);
//        }

//        if (e->Attribute("n_edges") != nullptr) {
//            getIntParameter(desc, e, "n_edges", &n_edges);
//        }

//        string line;
//        ifstream myfile(filename);
//        bool readSizes = (vertex_starter == " ") && (n_vertices == 0);

//        if (readSizes) {
//            getline(myfile, line);
//            sscanf(line.c_str(), "%d", &n_vertices);
//            getline(myfile, line);
//            sscanf(line.c_str(), "%d", &n_edges);
//        }

//        skeleton_ = std::make_share<Skeleton>();



//    }
//};

//extern const char simpleSkeleton[] = "SimpleSkeleton";

//static core::ResourceFactory::Type<simpleSkeleton, SimpleSkeletonResource> SimpleSkeletonType;

//} // namespace plugintools

//} // namespace expressive
