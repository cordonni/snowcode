#include "pluginTools/gui/qml/SceneNodeInfo/QmlImplicitSurfaceSceneNodeInfo.h"

// plugintools dependencies
#include "pluginTools/gui/qml/QmlTypeRegisterer.h"
#include "pluginTools/gui/qml/QmlSceneNodeInfoFactory.h"
#include "pluginTools/convol/ImplicitSurfaceSceneNode.h"

// convol dependencies
#include <convol/tools/updating/AreaUpdatingBloomenthalPolygonizer/AreaUpdatingBloomenthalPolygonizerHandler.h>

using namespace expressive::core;
using namespace std;

namespace expressive
{

namespace plugintools
{

QmlImplicitSurfaceSceneNodeInfo::QmlImplicitSurfaceSceneNodeInfo(core::SceneNode* node) :
    QmlSceneNodeInfo(node),
    m_issn(static_cast<ImplicitSurfaceSceneNode*>(node))
{
    auto getPointScaleL = [=]()->double { return m_issn->displayed_skeleton()->point_scale(); };
    auto setPointScaleL = [=](double size) { m_issn->displayed_skeleton()->set_point_scale(size); };
    auto getGeometryScaleL = [=]()->double { return m_issn->displayed_skeleton()->geometry_scale(); };
    auto setGeometryScaleL = [=](double size) { m_issn->displayed_skeleton()->set_geometry_scale(size); };
    auto getDisplayOctreeL = [=]()->bool { return m_issn->octree() != nullptr; };
    auto setDisplayOctreeL = [=](bool b) { m_issn->DisplayOctree(b); };

    auto getPolygonizerResolutionL = [=]()->double
    {
        std::shared_ptr<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler> updater = dynamic_pointer_cast<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler>(m_issn->convol_object()->auto_updating_polygonizer());
        if (updater != nullptr) {
            return updater->polygonizer().size();
        }

        return 0.0;
    };
    auto setPolygonizerResolutionL = [=](double res)
    {
        std::shared_ptr<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler> updater = dynamic_pointer_cast<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler>(m_issn->convol_object()->auto_updating_polygonizer());
        if (updater != nullptr) {
            updater->polygonizer().set_size(res);
            updater->ResetSurface();
        }
    };

    m_parameters.push_back(std::make_shared<QmlDoubleValueHandler>("point scale", getPointScaleL, setPointScaleL));
    m_parameters.push_back(std::make_shared<QmlDoubleValueHandler>("geometry scale", getGeometryScaleL, setGeometryScaleL));
    m_parameters.push_back(std::make_shared<QmlDoubleValueHandler>("resolution ", getPolygonizerResolutionL, setPolygonizerResolutionL));
    m_parameters.push_back(std::make_shared<QmlBoolValueHandler>("octree ", getDisplayOctreeL, setDisplayOctreeL));

}

//void QmlImplicitSurfaceSceneNodeInfo::setPolygonizerResolution(double res)
//{
//    std::shared_ptr<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler> updater = dynamic_pointer_cast<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler>(m_issn->convol_object()->auto_updating_polygonizer());
//    if (updater != nullptr) {
//        updater->polygonizer().set_size(res);
//        updater->ResetSurface();
//    }
//}

//double QmlImplicitSurfaceSceneNodeInfo::getPolygonizerResolution()
//{
//    std::shared_ptr<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler> updater = dynamic_pointer_cast<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler>(m_issn->convol_object()->auto_updating_polygonizer());
//    if (updater != nullptr) {
//        return updater->polygonizer().size();
//    }

//    return 0.0;
//}

//void QmlImplicitSurfaceSceneNodeInfo::setGraphVertexSize(double size)
//{
//    m_issn->displayed_skeleton()->set_point_scale(size);
//}

//void QmlImplicitSurfaceSceneNodeInfo::setGraphEdgeSize(double size)
//{
//        m_issn->displayed_skeleton()->set_geometry_scale(size);
//}

//double QmlImplicitSurfaceSceneNodeInfo::getGraphVertexSize()
//{
//    return m_issn->displayed_skeleton()->point_scale();
//}

//double QmlImplicitSurfaceSceneNodeInfo::getGraphEdgeSize()
//{
//    return m_issn->displayed_skeleton()->geometry_scale();
//}

void ImplicitSurfaceSceneNodeInfoRegisterFunction()
{
    qmlRegisterType<QmlImplicitSurfaceSceneNodeInfo>("Expressive", 1, 0, "ImplicitSurfaceSceneNodeInfo");
}
//static QmlTypeRegisterer::TypeRegistererCallback QmlISSNodeInfoType(ImplicitSurfaceSceneNodeInfoRegisterFunction);
QML_TYPE_REGISTERER_CALLBACK(ImplicitSurfaceSceneNodeInfoRegisterFunction);

static QmlSceneNodeInfoFactory::Type<ImplicitSurfaceSceneNode, QmlImplicitSurfaceSceneNodeInfo> QmlISSNodeInfoGenerator;

}

}

