/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QMLVALUEHANDLER_H
#define QMLVALUEHANDLER_H

#include <pluginTools/GLExpressiveEngine.h>
#include "qtTools/QtTools.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QVector3D>
#include <QQuickItem>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

namespace plugintools
{

class PLUGINTOOLS_API QmlValueHandler : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString type READ type CONSTANT)
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(bool constant READ constant CONSTANT)

public:
    QmlValueHandler(QQuickItem *parent = nullptr);
    QmlValueHandler(const QmlValueHandler &other);
    QmlValueHandler(const QString &name);

    virtual ~QmlValueHandler();

public slots:
    //////
    /// Getters
    ///
    virtual QString type() const;
    QString name() const;
    bool constant() const;

    virtual void notifyChanged() {printf("raté\n");}

protected:
    QString m_name;

    bool m_constant;
};
/*
//#define DEFINE_QML_VALUE_HANDLER(CLASSNAME, TYPE, QML_TYPE_NAME) \
//class PLUGINTOOLS_API CLASSNAME :  public QmlValueHandler { \
//    Q_OBJECT\
//    Q_PROPERTY(TYPE value READ value WRITE set_value NOTIFY valueChanged)\
//\
//    typedef std::function<TYPE()> Getter;\
//    typedef std::function<void(TYPE)> Setter;\
//\
//public:\
//    CLASSNAME() : QmlValueHandler("value") {}\
//\
//    CLASSNAME(const QString &name, Getter getter, Setter setter) :\
//        QmlValueHandler(name),\
//        m_getter(getter),\
//        m_setter(setter)\
//    {\
//    }\
//\
//    virtual ~CLASSNAME() {}\
//\
//    virtual QString type() const { return "##QML_TYPE_NAME"; }\
//\
//signals:\
//    void valueChanged();\
//\
//public slots:\
//    TYPE value() const { return m_getter(); }\
//    void set_value( TYPE value) { m_setter(value); }\
//\
//protected:\
//    Getter m_getter;\
//    Setter m_setter;\
//};

//DEFINE_QML_VALUE_HANDLER(QmlFloatValueHandler, float, "float")
//DEFINE_QML_VALUE_HANDLER(QmlBoolValueHandler, bool, "bool")
//DEFINE_QML_VALUE_HANDLER(QmlDoubleValueHandler, double, "double")
//DEFINE_QML_VALUE_HANDLER(QmlScalarValueHandler, Scalar, "scalar")

//typedef QmlValueHandlerT<float> QmlFloatValueHandler;
//typedef QmlValueHandlerT<bool> QmlBoolValueHandler;
//typedef QmlValueHandlerT<double> QmlDoubleValueHandler;
//typedef QmlValueHandlerT<Scalar> QmlScalarValueHandler;
*/
class PLUGINTOOLS_API QmlFloatValueHandler :  public QmlValueHandler {
    Q_OBJECT
    Q_PROPERTY(float value READ value WRITE set_value NOTIFY valueChanged)

//    typedef float (*Getter)();
//    typedef void (*Setter)(float);

    typedef std::function<float()> Getter;
    typedef std::function<void(float)> Setter;
public:
    QmlFloatValueHandler();
    QmlFloatValueHandler(const QString &name, Getter getter, Setter setter);

    virtual ~QmlFloatValueHandler();

    virtual QString type() const;

public slots:
    float value() const;
    void set_value(float value);
    virtual void notifyChanged();

signals:
    void valueChanged();

protected:
    Getter m_getter;
    Setter m_setter;
};


class PLUGINTOOLS_API QmlDoubleValueHandler :  public QmlValueHandler {
    Q_OBJECT
    Q_PROPERTY(double value READ value WRITE set_value NOTIFY valueChanged)

//    typedef double (*Getter)();
//    typedef void (*Setter)(double);

    typedef std::function<double()> Getter;
    typedef std::function<void(double)> Setter;
public:
    QmlDoubleValueHandler();
    QmlDoubleValueHandler(const QString &name, Getter getter, Setter setter);

    virtual ~QmlDoubleValueHandler();

    virtual QString type() const;

public slots:
    double value() const;
    void set_value(double value);

    virtual void notifyChanged();

signals:
    void valueChanged();

protected:
    Getter m_getter;
    Setter m_setter;
};


class PLUGINTOOLS_API QmlBoolValueHandler :  public QmlValueHandler {
    Q_OBJECT
    Q_PROPERTY(bool value READ value WRITE set_value NOTIFY valueChanged)

//    typedef bool (*Getter)();
//    typedef void (*Setter)(bool);

    typedef std::function<bool()> Getter;
    typedef std::function<void(bool)> Setter;
public:
    QmlBoolValueHandler();
    QmlBoolValueHandler(const QString &name, Getter getter, Setter setter);

    virtual ~QmlBoolValueHandler();

    virtual QString type() const;

    virtual void notifyChanged();

public slots:
    bool value() const;
    void set_value(bool value);

signals:
    void valueChanged();

protected:
    Getter m_getter;
    Setter m_setter;
};

class PLUGINTOOLS_API QmlVectorValueHandler :  public QmlValueHandler {
    Q_OBJECT
    Q_PROPERTY(QVector3D value READ value WRITE set_value NOTIFY valueChanged)

//    typedef Point (*Getter)();
//    typedef void (*Setter)(Point);
    typedef std::function<Point()> Getter;
    typedef std::function<void(Point)> Setter;

public:
    QmlVectorValueHandler();
    QmlVectorValueHandler(const QString &name, Getter getter, Setter setter);

    virtual ~QmlVectorValueHandler();

    virtual QString type() const;

public slots:
    QVector3D value() const;
    void set_value(QVector3D value);
    virtual void notifyChanged();

signals:
    void valueChanged();

protected:
    Getter m_getter;
    Setter m_setter;
};

}

}

Q_DECLARE_METATYPE(QQmlListProperty<expressive::plugintools::QmlValueHandler>)
Q_DECLARE_METATYPE(QQmlListProperty<expressive::plugintools::QmlFloatValueHandler>)
Q_DECLARE_METATYPE(QQmlListProperty<expressive::plugintools::QmlBoolValueHandler>)
//Q_DECLARE_METATYPE(QQmlListProperty<expressive::plugintools::QmlScalarValueHandler>)
Q_DECLARE_METATYPE(QQmlListProperty<expressive::plugintools::QmlDoubleValueHandler>)
Q_DECLARE_METATYPE(QQmlListProperty<expressive::plugintools::QmlVectorValueHandler>)

#endif // QMLSCENEMANAGER_H
