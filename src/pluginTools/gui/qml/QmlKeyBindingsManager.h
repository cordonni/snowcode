/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QML_KEY_BINDING_H_
#define QML_KEY_BINDING_H_

#include "pluginTools/gui/qml/QmlHandlerBase.h"
#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QList>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

namespace plugintools
{

/**
 * @brief The QmlKeyBinding class represents a key binding : A key is mapped to a given function in a module.
 * This class is only used to show key bindings to the user.
 */
class QmlKeyBinding : public QQuickItem
{
    Q_OBJECT

//    Q_PROPERTY(QString module READ module)
    Q_PROPERTY(QString key READ key CONSTANT)
    Q_PROPERTY(QString description READ description CONSTANT)
    Q_PROPERTY(QString long_description READ long_description CONSTANT)

public:
    QmlKeyBinding(QQuickItem * parent = nullptr);
    QmlKeyBinding(const QmlKeyBinding &other);
    QmlKeyBinding(/*const std::string &module, */const std::string &key, const std::string &description, const std::string &long_description = "");
    virtual ~QmlKeyBinding();

//    QString module() const;
    QString key() const;
    QString description() const;
    QString long_description() const;

protected:
//    QString m_module;
    QString m_key;
    QString m_description;
    QString m_long_description;
};


class QmlModuleKeyBindings : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE set_name NOTIFY nameChanged)
    Q_PROPERTY(QQmlListProperty<expressive::plugintools::QmlKeyBinding> key_bindings READ get_key_bindings NOTIFY keyBindingsChanged)

public:
    QmlModuleKeyBindings(QQuickItem *parent = nullptr);

    QmlModuleKeyBindings(const std::string &module_name);

    QmlModuleKeyBindings(const QmlModuleKeyBindings &other);

    virtual ~QmlModuleKeyBindings();

    QQmlListProperty<QmlKeyBinding> get_key_bindings();

    void add_key_binding(std::shared_ptr<QmlKeyBinding> key_binding);

    unsigned int get_usage_count(const QString &key);

    void set_name(const QString &name);

    QString name() const;

signals:
    void keyBindingsChanged();
    void nameChanged();

protected:
    QString m_name;

    std::vector< std::shared_ptr<QmlKeyBinding> > m_key_bindings;
};

/**
 * @brief The QmlKeyBindingsManager class handles and shows key bindings. It also enables developpers to check if a key binding is conflicting somewhere.
 * This class is only used to show key bindings to the user.
 */
class QmlKeyBindingsManager : public QmlHandlerBase
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<expressive::plugintools::QmlModuleKeyBindings> modules READ get_modules NOTIFY keyBindingsChanged)

public:
    QmlKeyBindingsManager(QQuickItem *parent = nullptr);

    virtual ~QmlKeyBindingsManager();

    QQmlListProperty<QmlModuleKeyBindings> get_modules();

    Q_INVOKABLE int get_usage_count(const QString &key) const;

    virtual void scene_initialized();

signals:
    void keyBindingsChanged();

protected:
//    QList<QmlModuleKeyBindings* > m_modules;
    std::vector<std::shared_ptr<QmlModuleKeyBindings> > m_modules;

};

}

}

Q_DECLARE_METATYPE(QQmlListProperty<expressive::plugintools::QmlKeyBinding>)
Q_DECLARE_METATYPE(QQmlListProperty<expressive::plugintools::QmlModuleKeyBindings>)

#endif // QML_KEY_BINDING_H_

