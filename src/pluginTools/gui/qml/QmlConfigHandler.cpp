#include <pluginTools/gui/qml/QmlConfigHandler.h>
#include <pluginTools/gui/qml/QmlTypeRegisterer.h>

namespace expressive
{

namespace plugintools
{

QmlConfigParam::QmlConfigParam(QQuickItem *parent) :
    QQuickItem(parent)
{
}

QmlConfigParam::QmlConfigParam(const QmlConfigParam &other) :
    QQuickItem(nullptr),
    m_name(other.name()),
    m_type(other.type())
{
}

QmlConfigParam::QmlConfigParam(ParameterType type, const std::string &name) :
    QQuickItem(nullptr),
    m_name(name.c_str()),
    m_type(type)
{
}

QmlConfigParam::~QmlConfigParam()
{
}

QString QmlConfigParam::name() const
{
    return m_name;
}

QmlConfigParam::ParameterType QmlConfigParam::type() const
{
    return m_type;
}

bool QmlConfigParam::bool_value() const
{
    return Config::getFloatParameter(m_name.toStdString()) > 0.5f;
}

float QmlConfigParam::float_value() const
{
    return Config::getFloatParameter(m_name.toStdString());
}

QString QmlConfigParam::string_value() const
{
    return QString(Config::getStringParameter(m_name.toStdString()).c_str());
}

void QmlConfigParam::set_value(bool value)
{
    Config::setParameter(m_name.toStdString(), value);
}

void QmlConfigParam::set_value(float value)
{
    Config::setParameter(m_name.toStdString(), value);
}

void QmlConfigParam::set_value(const QString& value)
{
    Config::setParameter(m_name.toStdString(), value.toStdString());
}

QmlConfigHandler::QmlConfigHandler() :
    QmlHandlerBase(nullptr)
{
}

QmlConfigHandler::~QmlConfigHandler()
{
    clear();
}

void QmlConfigHandler::clear()
{
//    for (unsigned int i = 0; i < m_params.size(); ++i) {
//        delete m_params[i];
//    }

    m_params.clear();
}

void QmlConfigHandler::scene_initialized()
{
    QmlHandlerBase::scene_initialized();
    auto floatParams = Config::getFloatParameters();
    auto stringParams = Config::getStringParameters();

    clear();
    for (auto p : floatParams) {
        m_params.push_back(QmlConfigParam(QmlConfigParam::E_FLOAT, p.first));
    }

    for (auto p : stringParams) {
        m_params.push_back(QmlConfigParam(QmlConfigParam::E_STRING, p.first));
    }

    emit parametersChanged();
}

QQmlListProperty<QmlConfigParam> QmlConfigHandler::getParameters()
{
    return QQmlListProperty<QmlConfigParam>(this, &m_params, qmlContainerCount<std::vector<QmlConfigParam>>, qmlContainerAt<std::vector<QmlConfigParam>>);
}

void QmlConfigHandlerRegisterer()
{
    qmlRegisterType<QmlConfigParam>("Expressive", 1, 0, "ConfigParam");
    qmlRegisterType<QmlConfigHandler>("Expressive", 1, 0, "ConfigHandler");
}

//static QmlTypeRegisterer::Type<QmlConfigHandler> QmlConfigHandlerType ("Expressive", 1, 0, "ConfigHandler");
//static QmlTypeRegisterer::TypeRegistererCallback QmlConfigHandlerType(QmlConfigHandlerRegisterer);
QML_TYPE_REGISTERER_CALLBACK(QmlConfigHandlerRegisterer);

} // namespace plugintools

} // namespace expressive
