#include "pluginTools/gui/qml/QmlSceneNodeInfoFactory.h"

using namespace expressive::core;

namespace expressive
{

namespace plugintools
{

void QmlSceneNodeInfoFactory::AddType(const std::string &type, createFunc f)
{
    types[type] = f;
}

std::shared_ptr<QmlSceneNodeInfo> QmlSceneNodeInfoFactory::Create(core::SceneNode* node)
{
    return GetSingleton()->CreateNodeInfo(node);
}

std::shared_ptr<QmlSceneNodeInfo> QmlSceneNodeInfoFactory::CreateNodeInfo(core::SceneNode* node)
{
    auto it2 = types.find(node->Name());
    if (it2 != types.end()) {
        return it2->second(node);
    }

    return std::make_shared<QmlSceneNodeInfo>(node);
}

} // namespace core

} // namespace expressive
