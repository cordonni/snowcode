/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <core/scenegraph/Camera.h>
#include "pluginTools/gui/qml/QmlHandlerBase.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QQmlParserStatus>
#include <QQuickItem>

#include <QVector3D>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

namespace plugintools
{

/**
 * @brief The QmlCameraHandler class allows to manipulate a Camera from a QML Context.
 * @see Camera
 * Note that a few properties MUST be defined in the qml file before using a QMLSketchingModeler :
 * - engine : The QMLExpressiveEngine that contains the context, scene related classes.
 */
class PLUGINTOOLS_API QmlCameraHandler: public QmlHandlerBase
{
    Q_OBJECT

    Q_ENUMS(CameraPosition)
    Q_PROPERTY(QVector3D position READ position WRITE set_position NOTIFY position_changed)

public:
    enum CameraPosition {
        E_DEFAULT,
        E_FRONT,
        E_BACK,
        E_LEFT,
        E_RIGHT,
        E_TOP,
        E_BOTTOM,
        E_RECORDED
    };

    QmlCameraHandler(GLExpressiveEngine *engine = nullptr, core::Camera *camera = nullptr);

    virtual ~QmlCameraHandler();

    virtual void scene_initialized();

    /////
    /// QML SETTERS
    ///
public slots:
    void set_position(const QVector3D &pos);
    void set_position(CameraPosition pos);
    void record_current_view();

    /////
    /// QML GETTERS
    ///
    QVector3D position() const;

signals:
    void position_changed();

protected:
    std::shared_ptr<core::Camera> m_camera;

};

} // namespace plugintools

} // namespace expressive
