#include "QmlHandlerBase.h"

namespace expressive
{

namespace plugintools
{

QmlHandlerBase::QmlHandlerBase(GLExpressiveEngine *engine, QQuickItem *parent) :
    QQuickItem(parent),
    m_engine(engine)
{
    if (engine != nullptr) {
        connect(engine, SIGNAL(sceneInitialized()), this, SLOT(scene_initialized()));
        manipulator_ = m_engine->scene_manipulator().get();
    }
}

QmlHandlerBase::~QmlHandlerBase()
{
}

QObject *QmlHandlerBase::engine()
{
    return m_engine;
}

void QmlHandlerBase::setEngine(QObject *engine)
{
    if (m_engine == nullptr) { // if it was not initialized yet.
        connect(engine, SIGNAL(sceneInitialized()), this, SLOT(scene_initialized()));
    }
    m_engine = dynamic_cast<GLExpressiveEngine*>(engine);
    manipulator_ = m_engine->scene_manipulator().get();
}

void QmlHandlerBase::scene_initialized()
{
    disconnect(m_engine, SIGNAL(sceneInitialized()), this, SLOT(scene_initialized()));
}

}

}
