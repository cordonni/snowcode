#include "pluginTools/gui/qml/GlRenderer.h"

#include "ork/render/FrameBuffer.h"
#include "ork/render/Mesh.h"

#include <fstream>

using namespace ork;
using namespace std;

namespace expressive
{

namespace plugintools
{

GlRenderer::~GlRenderer()
{
}

struct P4 {
    float x, y, z, w;
    P4() {}

    P4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w)
    {
    }
};

void GlRenderer::paint()
{
    if (m_engine != nullptr) {
        m_engine->Redisplay();
    }
}

} // namespace plugintools

} // namespace expressive

