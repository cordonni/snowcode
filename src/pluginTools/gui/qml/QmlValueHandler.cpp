#include "pluginTools/gui/qml/QmlValueHandler.h"
#include <pluginTools/gui/qml/QmlTypeRegisterer.h>

namespace expressive
{

namespace plugintools
{

QmlValueHandler::QmlValueHandler(QQuickItem *parent) :
    QQuickItem(parent)
{}

QmlValueHandler::QmlValueHandler(const QmlValueHandler &other) :
    QQuickItem(),
    m_name(other.m_name),
    m_constant(true)
{}

QmlValueHandler::QmlValueHandler(const QString &name) :
    QQuickItem(nullptr),
    m_name(name),
    m_constant(true)
{}

QmlValueHandler::~QmlValueHandler()
{}

QString QmlValueHandler::type() const
{
    return "error";
}

QString QmlValueHandler::name() const
{
    return m_name;
}

bool QmlValueHandler::constant() const
{
    return m_constant;
}

QmlFloatValueHandler::QmlFloatValueHandler() :
    QmlValueHandler("value")
{
}

QmlFloatValueHandler::QmlFloatValueHandler(const QString &name, Getter getter, Setter setter) :
    QmlValueHandler(name),
    m_getter(getter),
    m_setter(setter)
{
    m_constant = m_setter == nullptr;
}

QmlFloatValueHandler::~QmlFloatValueHandler()
{
}

QString QmlFloatValueHandler::type() const
{
    return "float";
}

float QmlFloatValueHandler::value() const
{
    return m_getter();
}

void QmlFloatValueHandler::set_value(float value)
{
    m_setter(value);
}

void QmlFloatValueHandler::notifyChanged()
{
    emit valueChanged();
}

QmlDoubleValueHandler::QmlDoubleValueHandler() :
    QmlValueHandler("value")
{
}

QmlDoubleValueHandler::QmlDoubleValueHandler(const QString &name, Getter getter, Setter setter) :
    QmlValueHandler(name),
    m_getter(getter),
    m_setter(setter)
{
    m_constant = m_setter == nullptr;
}

QmlDoubleValueHandler::~QmlDoubleValueHandler()
{
}

QString QmlDoubleValueHandler::type() const
{
    return "double";
}

double QmlDoubleValueHandler::value() const
{
    return m_getter();
}

void QmlDoubleValueHandler::set_value(double value)
{
    if (m_setter) {
        m_setter(value);
    }
}

void QmlDoubleValueHandler::notifyChanged()
{
    emit valueChanged();
}

QmlBoolValueHandler::QmlBoolValueHandler() :
    QmlValueHandler("value")
{
}

QmlBoolValueHandler::QmlBoolValueHandler(const QString &name, Getter getter, Setter setter) :
        QmlValueHandler(name),
        m_getter(getter),
        m_setter(setter)
{
    m_constant = m_setter == nullptr;
}

QmlBoolValueHandler::~QmlBoolValueHandler()
{
}

QString QmlBoolValueHandler::type() const
{
    return "bool";
}

bool QmlBoolValueHandler::value() const
{
    return m_getter();
}

void QmlBoolValueHandler::set_value(bool value)
{
    if (m_setter) {
        m_setter(value);
    }
}

void QmlBoolValueHandler::notifyChanged()
{
    emit valueChanged();
}

QmlVectorValueHandler::QmlVectorValueHandler() :
    QmlValueHandler("vector")
{
}

QmlVectorValueHandler::QmlVectorValueHandler(const QString &name, Getter getter, Setter setter) :
        QmlValueHandler(name),
        m_getter(getter),
        m_setter(setter)
{
    m_constant = m_setter == nullptr;
}

QmlVectorValueHandler::~QmlVectorValueHandler()
{
}

QString QmlVectorValueHandler::type() const
{
    return "vector";
}

QVector3D QmlVectorValueHandler::value() const
{
    return qttools::VectorToQt(m_getter());
}

void QmlVectorValueHandler::set_value(QVector3D value)
{
    if (m_setter) {
        m_setter(qttools::QtToVector(value));
    }
}

void QmlVectorValueHandler::notifyChanged()
{
    emit valueChanged();
}

void QmlValueHandlerRegisterer()
{
    qmlRegisterType<QmlValueHandler>("Expressive", 1, 0, "ValueHandler");
    qmlRegisterType<QmlFloatValueHandler>("Expressive", 1, 0, "FloatValueHandler");
    qmlRegisterType<QmlBoolValueHandler>("Expressive", 1, 0, "BoolValueHandler");
    qmlRegisterType<QmlDoubleValueHandler>("Expressive", 1, 0, "DoubleValueHandler");
//    qmlRegisterType<QmlScalarValueHandler>("Expressive", 1, 0, "ScalarValueHandler");
    qmlRegisterType<QmlVectorValueHandler>("Expressive", 1, 0, "VectorValueHandler");
}


//static QmlTypeRegisterer::TypeRegistererCallback QmlValueHandlerType(QmlValueHandlerRegisterer);
QML_TYPE_REGISTERER_CALLBACK(QmlValueHandlerRegisterer);


}

}
