#include "pluginTools/gui/qml/QmlGlWidget.h"
#include "pluginTools/gui/qml/QmlTypeRegisterer.h"
#include "pluginTools/scenemanipulator/SceneManipulator.h"

#include "qtTools/QtTools.h"

#include "GL/glew.h"
#include "ork/render/FrameBuffer.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QQuickView>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

using namespace std;

namespace expressive
{

namespace plugintools
{

QmlGlWidget::QmlGlWidget(QQuickItem *parent) :
    QmlHandlerBase(nullptr, parent),
    m_initialized(false),
    is_touch_event_(false)
{
    qmlRegisterType<QmlGlWidget>("Expressive", 1, 0, "GLWidget");
    grabMouse();
    setAcceptedMouseButtons(Qt::AllButtons);
    setAcceptHoverEvents(true);
    setFlag(ItemHasContents);
    setSmooth(false);
    setFocus(true);



    connect(this, SIGNAL(windowChanged(QQuickWindow*)), this, SLOT(handleWindowChanged(QQuickWindow*)));
    //    connect(window(), SIGNAL(windowStateChanged(Qt::WindowState)), this, SLOT(redisplay()));
    startTimer(1); // Forces redisplay at regular delays.
}

void QmlGlWidget::handleWindowChanged(QQuickWindow *win)
{
    if (win) {
        //        auto w = dynamic_cast<QQuickView*>(win);
        connect(win, SIGNAL(beforeSynchronizing()), this, SLOT(sync()), Qt::DirectConnection);
        //        connect(w, SIGNAL(sceneGraphInvalidated()), this, SLOT(cleanup()), Qt::DirectConnection);
        //        connect(w, SIGNAL(frameSwapped()), this, SLOT(redisplay()), Qt::DirectConnection);
        win->setClearBeforeRendering(false);
    }
}

void QmlGlWidget::sync()
{
    if (!m_initialized) {
        connect(window(), SIGNAL(sceneGraphInvalidated()), this, SLOT(cleanup()), Qt::DirectConnection);
        //        connect(window(), SIGNAL(frameSwapped()), this, SLOT(redisplay()), Qt::DirectConnection);
        connect(window(), SIGNAL(beforeRendering()), this, SLOT(redisplay()), Qt::DirectConnection);
        m_initialized = true;
    }
    if (manipulator_ == nullptr) {
        manipulator_ = m_engine->scene_manipulator().get();
    }
    m_engine->Resize(qttools::QtToVector(window()->size() * window()->devicePixelRatio()));
    if (window()) {
        window()->update();
    }
}

QSGNode *QmlGlWidget::updatePaintNode(QSGNode *n, UpdatePaintNodeData *data)
{
    //    ork::Timer t;
    //    t.start();
    //    redisplay();
    return QQuickItem::updatePaintNode(n, data);
}

void QmlGlWidget::cleanup()
{
    //    if (m_renderer) {
    //        delete m_renderer;
    //        m_renderer = 0;
    //    }
}

///////////////////////////////////////////////////////////////////////////////
// Events
bool QmlGlWidget::event(QEvent *event)
{
    //    int eventEnumIndex = QEvent::staticMetaObject.indexOfEnumerator("Type");
    //    QString name = QEvent::staticMetaObject.enumerator(eventEnumIndex).valueToKey(event->type());

    //    bool is_handled_event = true;
    //    switch(event->type()) {
    //    case QEvent::Timer:
    //        is_handled_event = false;
    //        break;
    //    default:
    //        is_handled_event = true;
    //    }

    //    if (is_handled_event) {
    //        if (name.isEmpty()) {
    //            printf("event %d\n", event->type());
    //        } else {
    //            printf("%s (%d)\n", name.toStdString().c_str(), event->type());
    //        }
    //    }


    switch(event->type()) {
    case QEvent::FocusIn:
    case QEvent::WindowStateChange:
        m_engine->WindowChanged();
        return QQuickItem::event(event);
        // Touch Events
    case QEvent::TouchEnd:
        is_touch_event_ = false;
        this->touchEnd(event);
        return true;
    case QEvent::TouchBegin:
        is_touch_event_ = true;
        this->touchBegin(event);
        return true;
    case QEvent::TouchUpdate:
        this->touchUpdate(event);
        return true;

        // Mouse Events
    case QEvent::MouseMove:
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseButtonDblClick:
        if (is_touch_event_) { event->ignore(); return false; }

        // Other Events
    default:
        if (manipulator_ != nullptr && manipulator_->event(event)) {
            return true;
        }
        return QQuickItem::event(event);
    }
}

void QmlGlWidget::mousePressEvent(QMouseEvent *event)
{
    this->setFocus(true);
    if (manipulator_ != nullptr) {
        manipulator_->mousePressEvent(event);
    }
}

void QmlGlWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (manipulator_ != nullptr) {
        manipulator_->mouseMoveEvent(event);
    }
}

void QmlGlWidget::hoverMoveEvent(QHoverEvent *event)
{
    //, Qt::MouseButtons buttons, Qt::KeyboardModifiers modifiers )
    if (manipulator_ != nullptr) {
        QMouseEvent *e = new QMouseEvent(
                    QMouseEvent::MouseMove,
                    event->pos(),
                    Qt::NoButton,
                    Qt::MouseButtons(Qt::NoButton),
                    Qt::NoModifier);
        manipulator_->mouseMoveEvent(e);
        delete e;
    }
}

void QmlGlWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (manipulator_ != nullptr) {
        manipulator_->mouseReleaseEvent(event);
    }
}

void QmlGlWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
    this->setFocus(true);
    if (manipulator_ != nullptr) {
        manipulator_->mouseDoubleClickEvent(event);
    }
}

void QmlGlWidget::wheelEvent(QWheelEvent *event)
{
    this->setFocus(true);
    if (manipulator_ != nullptr) {
        manipulator_->wheelEvent(event);
    }
}

bool QmlGlWidget::touchBegin(QEvent *event)
{
    this->setFocus(true);
    if (manipulator_ != nullptr) {
        return manipulator_->touchBegin(event);
    }
    return false;
}

bool QmlGlWidget::touchUpdate(QEvent *event)
{
    this->setFocus(true);
    if (manipulator_ != nullptr) {
        return manipulator_->touchUpdate(event);
    }
    return false;
}

bool QmlGlWidget::touchEnd(QEvent *event)
{
    if (manipulator_ != nullptr) {
        return manipulator_->touchEnd(event);
    }
    return false;
}

void QmlGlWidget::keyPressEvent(QKeyEvent *event)
{
    if (manipulator_ != nullptr) {
        manipulator_->keyPressEvent(event);
    }
}

void QmlGlWidget::keyReleaseEvent(QKeyEvent *event)
{
    if (manipulator_ != nullptr) {
        manipulator_->keyReleaseEvent(event);
    }
}

void QmlGlWidget::tabletEvent(QTabletEvent * /*event*/)
{
    if (manipulator_ != nullptr) {
    }
}

void QmlGlWidget::geometryChanged(const QRectF & newGeometry, const QRectF & oldGeometry)
{
    if (manipulator_ != nullptr && newGeometry.size() != oldGeometry.size()) {
        manipulator_->resizeEvent(newGeometry.size().toSize(), oldGeometry.size().toSize());
    }
}


void QmlGlWidget::resetOpenGLState()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    int maxAttribs;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &maxAttribs);
    for (int i=0; i<maxAttribs; ++i) {
        glVertexAttribPointer(i, 4, GL_FLOAT, GL_FALSE, 0, 0);
        glDisableVertexAttribArray(i);
    }


    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_STENCIL_TEST);
    glDisable(GL_SCISSOR_TEST);

    glColorMask(true, true, true, true);
    glClearColor(0, 0, 0, 0);

    glDepthMask(true);
    glDepthFunc(GL_LESS);
    glClearDepthf(1);

    glStencilMask(0xff);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    glStencilFunc(GL_ALWAYS, 0, 0xff);

    glDisable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ZERO);

    glUseProgram(0);

    ork::FrameBuffer::setDefault();

}

void QmlGlWidget::redisplay()
{
   m_engine->Redisplay();

    if (manipulator_ != nullptr) {
        manipulator_->redisplay();
    }
    resetOpenGLState();

    assert(glGetError() == 0);

    //    window()->update();
}

void QmlGlWidget::timerEvent(QTimerEvent *)
{
    //    if (manipulator_ == nullptr) {
    //        manipulator_ = m_ogreEngineItem->scene_manipulator();
    //    }
    update();
    //        window()->update();
    //        redisplay();
    //    redisplay();
}

//static QmlTypeRegisterer::Type<QmlGlWidget> QmlGlWidgetType("Expressive", 1, 0, "GLWidget");

QML_TYPE_REGISTERER(QmlGlWidget, GLWidget, "Expressive", 1, 0)

}

}
