#include <pluginTools/gui/qml/QmlCameraHandler.h>
#include <pluginTools/gui/qml/QmlTypeRegisterer.h>

#include "pluginTools/scenemanipulator/CameraManipulator.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QVector3D>
#include <qtTools/QtTools.h>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

namespace plugintools
{

QmlCameraHandler::QmlCameraHandler(GLExpressiveEngine *engine, core::Camera *camera) :
    QmlHandlerBase(engine),
    m_camera(camera)
{
}

QmlCameraHandler::~QmlCameraHandler()
{
}

void QmlCameraHandler::scene_initialized()
{
    if (m_camera == nullptr) {
        m_camera = m_engine->camera_manager();

    }

    QmlHandlerBase::scene_initialized();
}

void QmlCameraHandler::set_position(const QVector3D &pos)
{
    if (m_camera != nullptr) {
        m_camera->SetPos(qttools::QtToVector(pos));

        emit position_changed();
    }
}

void QmlCameraHandler::set_position(CameraPosition pos)
{
    if (m_camera != nullptr) {
        switch (pos) {
        case E_DEFAULT:  std::dynamic_pointer_cast<CameraManipulator>(m_camera->manipulator())->moveCameraTo(m_camera->GetDefaultView()); break;
        case E_FRONT:    std::dynamic_pointer_cast<CameraManipulator>(m_camera->manipulator())->moveCameraTo(m_camera->GetFrontView()); break;
        case E_BACK:     std::dynamic_pointer_cast<CameraManipulator>(m_camera->manipulator())->moveCameraTo(m_camera->GetBackView()); break;
        case E_TOP:      std::dynamic_pointer_cast<CameraManipulator>(m_camera->manipulator())->moveCameraTo(m_camera->GetTopView()); break;
        case E_BOTTOM:   std::dynamic_pointer_cast<CameraManipulator>(m_camera->manipulator())->moveCameraTo(m_camera->GetBottomView()); break;
        case E_LEFT:     std::dynamic_pointer_cast<CameraManipulator>(m_camera->manipulator())->moveCameraTo(m_camera->GetLeftView()); break;
        case E_RIGHT:    std::dynamic_pointer_cast<CameraManipulator>(m_camera->manipulator())->moveCameraTo(m_camera->GetRightView()); break;
        case E_RECORDED: std::dynamic_pointer_cast<CameraManipulator>(m_camera->manipulator())->moveCameraTo(m_camera->GetRecordedView()); break;
        }
    }
}

QVector3D QmlCameraHandler::position() const
{
    if (m_camera != nullptr) {
        return qttools::VectorToQt(m_camera->GetPointInWorld());
    }
    return QVector3D(0.0, 0.0, 0.0);
}

void QmlCameraHandler::record_current_view()
{
    if (m_camera != nullptr) {
        m_camera->RecordCurrentView();
    }
}

void dummyCameraRegisterer()
{
    qmlRegisterType<QmlCameraHandler>("Expressive", 1, 0, "Camera");
}

//static QmlTypeRegisterer::Type<QmlCameraHandler> QmlCameraHandlerType ("Expressive", 1, 0, "Camera");

QML_TYPE_REGISTERER(QmlCameraHandler, Camera, "Expressive", 1, 0)

} // namespace plugintools

} // namespace expressive
