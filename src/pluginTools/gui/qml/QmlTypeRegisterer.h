/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QMLTYPEREGISTERER_H
#define QMLTYPEREGISTERER_H

#include "ork/core/Logger.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QtQml>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <cstdio>

namespace expressive
{

namespace plugintools
{

/**
 * @brief The QmlTypeRegisterer class used to allow registering classes with the Qml setup.
 * However, since one of the last builds, Qt doesn't seem to accept qmlRegisterType calls
 * before the creation of QApplication. Instead, we will use Q_COREAPP_STARTUP_FUNCTION.
 * 2 macros have beed defined :
 * QML_TYPE_REGISTERER_CALLBACK : Will use a user-defined callback (usefull when registering order is important)
 * QML_TYPE_REGISTERER : Will automatically register the type when required.
 * QmlTypeRegisterer is kept for memory.
 */

////// SIMPLE CALLBACK MACRO
#define QML_TYPE_REGISTERER_CALLBACK(callback) Q_COREAPP_STARTUP_FUNCTION(callback)

////// THIS ONES EXPANDS TO THE EQUIVALENT OF WHAT COULD BE PASSED TO THE PREVIOUS MACRO.
#define QML_TYPE_REGISTERER(c_class_name, qml_class_name, package, MAJOR_VERSION, MINOR_VERSION) \
void Qml##qml_class_name##Registerer() \
{ \
    ork::Logger::INFO_LOGGER->log("QML", "Registering " #c_class_name ":" #qml_class_name "...");\
    qmlRegisterType<c_class_name>(package, MAJOR_VERSION, MINOR_VERSION, #qml_class_name);\
}\
\
Q_COREAPP_STARTUP_FUNCTION(Qml##qml_class_name##Registerer)


//class QmlTypeRegisterer
//{
//public:
//    QmlTypeRegisterer();

//    template <class T>
//    class Type
//    {
//    public:
//        Type(const char *uri, int versionMajor, int versionMinor, const char *qmlName)
//        {
//            qmlRegisterType<T>(uri, versionMajor, versionMinor, qmlName);
//        }
//    };

//    class TypeRegistererCallback
//    {
//    public:
//        typedef void (*callbackFunc)();
//        TypeRegistererCallback(callbackFunc callback)
//        {
////            Q_COREAPP_STARTUP_FUNCTION(callback)
////            callback();
//        }
//    };
//};

} // namespace plugintools

} // namespace expressive

#endif // QMLTYPEREGISTERER_H
