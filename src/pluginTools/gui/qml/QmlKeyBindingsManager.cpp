#include "pluginTools/gui/qml/QmlKeyBindingsManager.h"
#include "pluginTools/gui/qml/QmlTypeRegisterer.h"
#include "pluginTools/scenemanipulator/SceneManipulator.h"

#include "core/utils/KeyBindingsList.h"

namespace expressive
{

namespace plugintools
{

QmlKeyBinding::QmlKeyBinding(QQuickItem * parent) :
    QQuickItem(parent),
    m_key("default key"),
    m_description("a key used for tests"),
    m_long_description("a long description !!")
{
}

QmlKeyBinding::QmlKeyBinding(const QmlKeyBinding &other) :
    QQuickItem(NULL),
    m_key(other.m_key),
    m_description(other.m_description),
    m_long_description(other.m_long_description)
{
}

QmlKeyBinding::QmlKeyBinding(/*const std::string &module, */const std::string &key, const std::string &description, const std::string &long_description) :
    QQuickItem(nullptr),
//    m_module(module.cstr()),
    m_key(key.c_str()),
    m_description(description.c_str()),
    m_long_description(long_description.c_str())
{
}

QmlKeyBinding::~QmlKeyBinding()
{
}

//QString QmlKeyBinding::module() const
//{
//    return m_module;
//}

QString QmlKeyBinding::key() const
{
    return m_key;
}

QString QmlKeyBinding::description() const
{
    return m_description;
}

QString QmlKeyBinding::long_description() const
{
    return m_long_description;
}

QmlModuleKeyBindings::QmlModuleKeyBindings(QQuickItem *parent) :
    QQuickItem(parent)
{
}

QmlModuleKeyBindings::QmlModuleKeyBindings(const std::string &module_name) :
    QQuickItem(nullptr),
    m_name(module_name.c_str())
{
}

QmlModuleKeyBindings::QmlModuleKeyBindings(const QmlModuleKeyBindings &other) :
    QQuickItem(NULL),
    m_name(other.m_name),
    m_key_bindings(other.m_key_bindings)
{
}

QmlModuleKeyBindings::~QmlModuleKeyBindings()
{
//    for (int i = 0; i < m_key_bindings.size(); ++i) {
//        delete m_key_bindings[i];
//    }
    m_key_bindings.clear();
}

QQmlListProperty<QmlKeyBinding> QmlModuleKeyBindings::get_key_bindings()
{
    return QQmlListProperty<QmlKeyBinding>(this, &m_key_bindings, qmlContainerCount<std::vector<std::shared_ptr<QmlKeyBinding>>>, qmlSharedPtrContainerAt<std::vector<std::shared_ptr<QmlKeyBinding>>>);
}

void QmlModuleKeyBindings::add_key_binding(std::shared_ptr<QmlKeyBinding> key_binding)
{
    m_key_bindings.push_back(key_binding);
    emit keyBindingsChanged();
}

unsigned int QmlModuleKeyBindings::get_usage_count(const QString &key)
{
    unsigned int cpt = 0;
    for (unsigned int i = 0; i < (unsigned int) m_key_bindings.size(); ++i) {
        if (m_key_bindings[i]->key() == key) {
            cpt++;
        }
    }

    return cpt;
}

void QmlModuleKeyBindings::set_name(const QString &name)
{
    m_name = name;
}

QString QmlModuleKeyBindings::name() const
{
    return m_name;
}

QmlKeyBindingsManager::QmlKeyBindingsManager(QQuickItem *parent) :
    QmlHandlerBase(nullptr, parent)
{
}

QmlKeyBindingsManager::~QmlKeyBindingsManager()
{
//    for (int i = 0; i < m_modules.size(); ++i) {
//        delete m_modules[i];
//    }
    m_modules.clear();
}


QQmlListProperty<QmlModuleKeyBindings> QmlKeyBindingsManager::get_modules()
{
    return QQmlListProperty<QmlModuleKeyBindings>(this, &m_modules, qmlContainerCount<std::vector<std::shared_ptr<QmlModuleKeyBindings> > >, qmlSharedPtrContainerAt<std::vector<std::shared_ptr<QmlModuleKeyBindings> >>);
}

int QmlKeyBindingsManager::get_usage_count(const QString &key) const
{
    unsigned int cpt = 0;
    for (unsigned int i = 0; i < (unsigned int) m_modules.size(); ++i) {
        cpt += m_modules[i]->get_usage_count(key);
    }

    return cpt;
}

void QmlKeyBindingsManager::scene_initialized()
{
    QmlHandlerBase::scene_initialized();
    core::KeyBindingsList kb = m_engine->scene_manipulator()->GetKeyBindings();

    for (auto it : kb.m_bindings) {
        std::shared_ptr<QmlModuleKeyBindings> module = std::make_shared<QmlModuleKeyBindings>(it.first);
        m_modules.push_back(module);
        for (auto it2 : it.second) {
            module->add_key_binding(std::make_shared<QmlKeyBinding>(it2.second.key, it2.second.description, it2.second.long_description));
        }
    }

    emit keyBindingsChanged();
}

void QmlKeyBindingsRegisterer()
{
    qmlRegisterType<QmlKeyBinding>("Expressive", 1, 0, "KeyBinding");
    qmlRegisterType<QmlModuleKeyBindings>("Expressive", 1, 0, "ModuleKeyBindings");
    qmlRegisterType<QmlKeyBindingsManager>("Expressive", 1, 0, "KeyBindings");
}

//static QmlTypeRegisterer::Type<QmlKeyBinding> QmlKeyBindingType("Expressive", 1, 0, "KeyBinding");
//static QmlTypeRegisterer::Type<QmlModuleKeyBindings> QmlModuleKeyBindingsType("Expressive", 1, 0, "ModuleKeyBindings");
//static QmlTypeRegisterer::Type<QmlKeyBindingsManager> QmlKeyBindingsType("Expressive", 1, 0, "KeyBindings");
//static QmlTypeRegisterer::TypeRegistererCallback QmlKeyBindingType(QmlKeyBindingsRegisterer);

QML_TYPE_REGISTERER_CALLBACK(QmlKeyBindingsRegisterer);

}

}
