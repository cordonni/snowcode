#include "pluginTools/gui/qml/QmlTileParameter.h"
#include "pluginTools/gui/qml/QmlTypeRegisterer.h"

namespace expressive
{

namespace plugintools
{

static QmlTypeRegisterer::Type<QmlTileParameter> QmlTileParameterType("Expressive", 1, 0, "QmlTileParameter");

}

}
