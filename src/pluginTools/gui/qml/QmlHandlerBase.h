/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QMLHANDLERBASE_H
#define QMLHANDLERBASE_H
#include <pluginTools/GLExpressiveEngine.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QQuickItem>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

namespace plugintools
{

// utility functions for getting counts and items in custom std::vector instead of using QQmlLists for storing stuff.
template<typename CONTAINER, typename T>
inline int qmlContainerCount(QQmlListProperty<T> *l)
{
    return (int)((CONTAINER *)(l->data))->size();
}

template<typename CONTAINER, typename T>
inline T* qmlContainerAt(QQmlListProperty<T> *l, int index)
{
    return &((CONTAINER *)(l->data))->at(index);
}

template<typename CONTAINER, typename T>
T* qmlSharedPtrContainerAt(QQmlListProperty<T> *l, int index)
{
    return ((CONTAINER *)(l->data))->at(index).get();
}

/**
 * @brief The QmlHandlerBase class offers a quick initialization for your classes
 * for which you want a qml gui.
 * You may want to redefine the #scene_initialized() method in order to update the fields for your class.
 */
class PLUGINTOOLS_API QmlHandlerBase : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QObject *engine READ engine WRITE setEngine)

public:
    QmlHandlerBase(GLExpressiveEngine *engine = nullptr, QQuickItem *parent = nullptr);

    virtual ~QmlHandlerBase();

    QObject *engine();
    virtual void setEngine(QObject *engine);

public slots:
    /**
     * Forces to resend the data to qml since qml tried to get it when engine was not created yet.
     */
    virtual void scene_initialized();

protected:
    GLExpressiveEngine *m_engine;

    SceneManipulator *manipulator_;

};
}

}

#endif // QMLHANDLERBASE_H
