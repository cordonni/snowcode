/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QML_TILE_PARAMETER_H
#define QML_TILE_PARAMETER_H

#include <QObject>

namespace expressive
{

namespace plugintools
{

class QmlTile;

class QmlTileParameter : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString type READ type)
    Q_PROPERTY(QString name READ name)
    Q_PROPERTY(QString value READ value WRITE set_value NOTIFY valueChanged)
    Q_PROPERTY(bool writable READ writable)

public:
    QmlTileParameter() :
        m_owner(nullptr),
        m_name(""),
        m_writable(false)
    {
    }

    QmlTileParameter(QmlTile *owner, const std::string &name, bool writable = true) :
        m_owner(owner),
        m_name(name),
        m_writable(writable)
    {
    }

public slots:
    QString name() const { return QString(m_name.c_str()); }
    bool writable() const { return m_writable; }

    virtual QString type() const { return "none"; }
    virtual QString value() const { return ""; }
    virtual void set_value(const QString &/*value*/) {}

signals:
    void valueChanged();


protected:
    QmlTile *m_owner; // < parent QmlTile.
    std::string m_name; // < Name of the parameter.
    bool m_writable; // < whether this parameter is editable or not.
};

class QmlTileBoolParameter : public QmlTileParameter
{
    Q_OBJECT
public:
    typedef void (*WriteCallback)(bool b);
    typedef bool (*ReadCallback)();
    QmlTileBoolParameter(QmlTile *owner, const std::string &name, ReadCallback readCallback, WriteCallback writeCallback, bool writable = true) :
        QmlTileParameter(owner, name, writable),
        m_readCallback(readCallback),
        m_writeCallback(writeCallback)
    {
    }


public slots:
    virtual QString type() const { return "bool"; }
    virtual QString value() const { return QString(std::to_string(m_readCallback()).c_str()); }
    virtual void set_value(const QString &value) { m_writeCallback((bool) value.toInt()); }

protected:
    ReadCallback m_readCallback; // < callback for getXXX();
    WriteCallback m_writeCallback; // < callback for setXXX(m_value);
};

class QmlTileStringParameter : public QmlTileParameter
{
    Q_OBJECT
public:
    typedef void (*WriteCallback)(std::string b);
    typedef std::string (*ReadCallback)();
    QmlTileStringParameter(QmlTile *owner, const std::string &name, ReadCallback readCallback, WriteCallback writeCallback, bool writable = true) :
        QmlTileParameter(owner, name, writable),
        m_readCallback(readCallback),
        m_writeCallback(writeCallback)
    {
    }

public slots:
    virtual QString type() const { return "string"; }
    virtual QString value() const { return QString(m_readCallback().c_str()); }
    virtual void set_value(const QString &value) { m_writeCallback(value.toStdString()); }

protected:
    ReadCallback m_readCallback; // < callback for getXXX();
    WriteCallback m_writeCallback; // < callback for setXXX(m_value);
};

}

}

#endif // QML_TILE_PARAMETER_H
