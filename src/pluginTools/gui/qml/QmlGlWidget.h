/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QMLGLWIDGET_H
#define QMLGLWIDGET_H

#include "pluginTools/gui/qml/QmlHandlerBase.h"

namespace expressive
{

namespace plugintools
{

class QmlGlWidget : public QmlHandlerBase
{
    Q_OBJECT
public:
    QmlGlWidget(QQuickItem *parent = nullptr);

    virtual ~QmlGlWidget() {}

    virtual QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *);

protected:
   void resetOpenGLState();

public slots:
    void sync();
    void cleanup();

protected slots:
    void handleWindowChanged(QQuickWindow *win);
    void redisplay();

protected:
    void timerEvent(QTimerEvent *);

    ///////////////////////////////////////////////////////////////////////////////
    // Events
    virtual bool event(QEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void hoverMoveEvent(QHoverEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void mouseDoubleClickEvent(QMouseEvent *event);
#ifndef QT_NO_WHEELEVENT
    virtual void wheelEvent(QWheelEvent *event);
#endif
    virtual bool touchBegin(QEvent *event);
    virtual bool touchUpdate(QEvent *event);
    virtual bool touchEnd(QEvent *event);
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);
    virtual void tabletEvent(QTabletEvent *event);

    virtual void geometryChanged(const QRectF & newGeometry, const QRectF & oldGeometry);

    bool m_initialized;

    bool is_touch_event_;
};

}

}

#endif //QMLGLWIDGET_H
