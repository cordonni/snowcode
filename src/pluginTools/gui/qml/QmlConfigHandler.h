/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QMLCONFIGHANDLER_H_
#define QMLCONFIGHANDLER_H_

#include <core/utils/ConfigLoader.h>
#include "pluginTools/gui/qml/QmlHandlerBase.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QQuickItem>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

namespace plugintools
{


class PLUGINTOOLS_API QmlConfigParam : public QQuickItem
{
    Q_OBJECT

    Q_ENUMS(ParameterType)

    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(ParameterType type READ type CONSTANT)
    Q_PROPERTY(float float_value READ float_value WRITE set_value NOTIFY value_changed)
    Q_PROPERTY(QString string_value READ string_value WRITE set_value NOTIFY value_changed)
    Q_PROPERTY(bool bool_value READ bool_value WRITE set_value NOTIFY value_changed)

public:
    enum ParameterType {
        E_FLOAT,
        E_BOOL,
        E_STRING
    };
    QmlConfigParam(QQuickItem *parent = nullptr);
    QmlConfigParam(const QmlConfigParam &other);

    QmlConfigParam(ParameterType type, const std::string &name);
    virtual ~QmlConfigParam();

    QString name() const;
    bool bool_value() const;
    float float_value() const;
    QString string_value() const;
    ParameterType type() const;

    void set_value(bool value);
    void set_value(float value);
    void set_value(const QString &value);

signals:
    void value_changed();

protected:
    QString m_name;

    ParameterType m_type;
};

/**
 * @brief The QmlConfigHandler class allows to manipulate global options from a QML Context.
 * @see Config.h
 */
class PLUGINTOOLS_API QmlConfigHandler: public QmlHandlerBase
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<expressive::plugintools::QmlConfigParam> parameters READ getParameters NOTIFY parametersChanged)
public:
    QmlConfigHandler();

    virtual ~QmlConfigHandler();

    QQmlListProperty<QmlConfigParam> getParameters();

    void scene_initialized();

signals:
    void parametersChanged();

protected:
    void clear();

    std::vector<QmlConfigParam> m_params;

};

//Q_DECLARE_METATYPE(QQmlListProperty<expressive::plugintools::QmlStringParam>)


} // namespace plugintools

} // namespace expressive

Q_DECLARE_METATYPE(QQmlListProperty<expressive::plugintools::QmlConfigParam>)


#endif // QMLCONFIGHANDLER_H_
