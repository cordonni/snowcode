#include "pluginTools/gui/qml/QmlSceneManager.h"
#include <pluginTools/gui/qml/QmlTypeRegisterer.h>
#include <pluginTools/gui/qml/QmlSceneNodeInfoFactory.h>

#include "core/scenegraph/Overlay.h"
#include "qtTools/QtTools.h"

#include <functional>

namespace expressive
{

namespace plugintools
{

QmlSceneNodeInfo::QmlSceneNodeInfo(QQuickItem *parent) :
    QQuickItem(parent),
    core::SceneNodeListener(),
    m_id(-1),
    m_node(nullptr),
    m_parent_id(-1)
{
}

QmlSceneNodeInfo::QmlSceneNodeInfo(const QmlSceneNodeInfo &other) :
    QQuickItem(nullptr),
    core::SceneNodeListener(other.target_),
    m_id(other.id()),
    m_node(other.m_node),
    m_parent_id(other.m_parent_id),
    m_children(other.m_children)
{
}

QmlSceneNodeInfo::QmlSceneNodeInfo(core::SceneNode* node) :
    QQuickItem(nullptr),
    core::SceneNodeListener(node),
    m_id(node->id()),
    m_node(node),
    m_parent_id(node->parent() ? node->parent()->id() : -1)
{
    // create the list of lambdas used for our parameters.
    auto getVisibleL = [=]()->bool { return m_node->visible();};
    auto setVisibleL = [=](bool b) {m_node->set_visible(b);};
    auto getSelectedL = [=]()->bool { return m_node->selected();};
    auto setSelectedL = [=](bool b) {m_node->set_selected(b);};
    auto getWireframeL = [=]()->bool { return m_node->wireframe();};
    auto setWireframeL = [=](bool b) {m_node->DisplayWireframe(b);};
    auto getAdjustSizeL = [=]()->bool { return m_node->adjust_size_to_params();};
    auto setAdjustSizeL = [=](bool b) {m_node->set_adjust_size_to_param(b);};
    auto getPositionL = [=]() -> Point { return m_node->GetPointInWorld(); };//std::bind(&core::SceneNode::GetPointInWorld, m_node, std::placeholders::_1);
    auto setPositionL = [=](const Point &p) { m_node->SetPos(p, core::SceneNode::TS_WORLD); };//std::bind(static_cast<void(core::SceneNode::*)(const Point&, core::SceneNode::TransformSpace)>(&core::SceneNode::SetPos), m_node, std::placeholders::_1, core::SceneNode::TS_WORLD);

    auto getMeshVerticesL = [=]() -> double { return m_node->tri_mesh() ? m_node->tri_mesh()->n_vertices() : 0.0; };
    auto getMeshEdgesL = [=]() -> double { return m_node->tri_mesh() ? m_node->tri_mesh()->n_edges() : 0.0; };
    auto getMeshFacesL = [=]() -> double { return m_node->tri_mesh() ? m_node->tri_mesh()->n_faces() : 0.0; };

    m_parameters.push_back(std::make_shared<QmlBoolValueHandler>("visible", getVisibleL, setVisibleL));
    m_parameters.push_back(std::make_shared<QmlBoolValueHandler>("selected", getSelectedL, setSelectedL));
    m_parameters.push_back(std::make_shared<QmlBoolValueHandler>("wireframe", getWireframeL, setWireframeL));
    m_parameters.push_back(std::make_shared<QmlBoolValueHandler>("adjust size to params", getAdjustSizeL, setAdjustSizeL));
    m_parameters.push_back(std::make_shared<QmlVectorValueHandler>("position", getPositionL, setPositionL));
    m_parameters.push_back(std::make_shared<QmlDoubleValueHandler>("mesh vertices", getMeshVerticesL, nullptr));
    m_parameters.push_back(std::make_shared<QmlDoubleValueHandler>("mesh edges", getMeshEdgesL, nullptr));
    m_parameters.push_back(std::make_shared<QmlDoubleValueHandler>("mesh faces", getMeshFacesL, nullptr));
}

QmlSceneNodeInfo::~QmlSceneNodeInfo()
{
//    printf("Delete node info %d\n", m_id);
}

unsigned int QmlSceneNodeInfo::id() const
{
    return m_id;
}

QString QmlSceneNodeInfo::type() const
{
    return QString(m_node->Name().c_str());
}

QString QmlSceneNodeInfo::name() const
{
    return QString(m_node->name().c_str());
}

bool QmlSceneNodeInfo::visible() const
{
    return m_node->visible();
}

bool QmlSceneNodeInfo::hovered() const
{
    return m_node->hovered();
}

bool QmlSceneNodeInfo::selected() const
{
    return m_node->selected();
}

bool QmlSceneNodeInfo::wireframe() const
{
    return m_node->wireframe();
}

bool QmlSceneNodeInfo::adjust_size_to_params() const
{
    return m_node->adjust_size_to_params();
}

bool QmlSceneNodeInfo::is_overlay() const
{
    return dynamic_cast<core::Overlay*>(m_node) != nullptr;
}

QVector3D QmlSceneNodeInfo::position() const
{
    return qttools::VectorToQt(m_node->GetPointInWorld());
}

QVector3D QmlSceneNodeInfo::orientation() const
{
    return qttools::VectorToQt(m_node->GetRealDirection());
}

QVector3D QmlSceneNodeInfo::scale() const
{
    return qttools::VectorToQt(m_node->GetScale());
}


unsigned int QmlSceneNodeInfo::meshVertices() const
{
    return m_node->tri_mesh() == nullptr ? 0 : (uint) m_node->tri_mesh()->n_vertices();
}

unsigned int QmlSceneNodeInfo::meshEdges() const
{
    return m_node->tri_mesh() == nullptr ? 0 : (uint) m_node->tri_mesh()->n_edges();
}

unsigned int QmlSceneNodeInfo::meshFaces() const
{
    return m_node->tri_mesh() == nullptr ? 0 : (uint) m_node->tri_mesh()->n_faces();
}

QQmlListProperty<QmlSceneNodeInfo> QmlSceneNodeInfo::getChildren()
{
    return QQmlListProperty<QmlSceneNodeInfo>(this, &m_children, qmlContainerCount<std::vector<std::shared_ptr<QmlSceneNodeInfo>>>, qmlSharedPtrContainerAt<std::vector<std::shared_ptr<QmlSceneNodeInfo>>>);
}

QQmlListProperty<QmlValueHandler> QmlSceneNodeInfo::getParameters()
{
    return QQmlListProperty<QmlValueHandler>(this, &m_parameters, qmlContainerCount<std::vector<std::shared_ptr<QmlValueHandler>>>, qmlSharedPtrContainerAt<std::vector<std::shared_ptr<QmlValueHandler>>>);
}

void QmlSceneNodeInfo::set_visible(bool b)
{
    m_node->set_visible(b);
}

void QmlSceneNodeInfo::set_wireframe(bool b)
{
    m_node->DisplayWireframe(b);
}

void QmlSceneNodeInfo::set_selected(bool b, bool clear_selection)
{
    if (b && clear_selection) {
        m_node->owner()->RemoveAllFromSelection();
    }
    if (b) {
        m_node->owner()->AddToSelection(m_node->id());
    } else {
        m_node->owner()->RemoveFromSelection(m_node->id());
    }
//    m_node->set_selected(b);
}

void QmlSceneNodeInfo::set_hovered(bool /*b*/)
{
//    m_node->set_hovered(b);
    m_node->owner()->SetHoveredNode(m_node);
}

void QmlSceneNodeInfo::set_adjust_size_to_params(bool b)
{
    m_node->set_adjust_size_to_param(b);
    m_node->Update();
}

void QmlSceneNodeInfo::set_position(const QVector3D &p)
{
    m_node->SetPos(qttools::QtToVector(p));
}

void QmlSceneNodeInfo::set_orientation(const QVector3D &p)
{
    m_node->LookAt(qttools::QtToVector(p), core::SceneNode::TS_WORLD);
}

void QmlSceneNodeInfo::set_scale(const QVector3D &p)
{
    m_node->SetScale(qttools::QtToVector(p));
}

void QmlSceneNodeInfo::clear_selection()
{
    m_node->owner()->RemoveAllFromSelection();
}

void QmlSceneNodeInfo::updated(core::SceneNode* /*node*/)
{
    emit positionChanged();
    emit orientationChanged();
    emit scaleChanged();
    emit meshVerticesChanged();
    emit meshEdgesChanged();
    emit meshFacesChanged();
}

void QmlSceneNodeInfo::SceneNodeSelectedStateChanged(NodeId /*node*/)
{
    emit selectedChanged();
}

void QmlSceneNodeInfo::SceneNodeHoveredStateChanged(NodeId /*node*/)
{
    emit hoveredChanged();
}

void QmlSceneNodeInfo::SceneNodeVisibilityStateChanged(NodeId /*node*/)
{
    emit visibleChanged();
    m_parameters[0]->notifyChanged();
}

void QmlSceneNodeInfo::SceneNodeWireframeStateChanged(NodeId /*node*/)
{
    emit wireframeChanged();
}

void QmlSceneNodeInfo::SceneNodeAdjustSizeToParamsChanged(NodeId /*node*/)
{
    emit adjust_size_to_paramsChanged();
}

void QmlSceneNodeInfo::add_child(std::shared_ptr<QmlSceneNodeInfo> child)
{
    m_children.push_back(child);
    emit childrenChanged();
}

QmlSceneManager::QmlSceneManager() :
    QmlHandlerBase(),
    m_scene(nullptr)
{
}

QmlSceneManager::~QmlSceneManager()
{
//    m_overlays.clear();
    m_nodes.clear();
    m_selected_nodes.clear();
    m_mapping.clear();
}

QQmlListProperty<QmlSceneNodeInfo> QmlSceneManager::getNodes()
{
    return QQmlListProperty<QmlSceneNodeInfo>(this, &m_nodes, qmlContainerCount<std::vector<std::shared_ptr<QmlSceneNodeInfo>>>, qmlSharedPtrContainerAt<std::vector<std::shared_ptr<QmlSceneNodeInfo>>>);
}

QQmlListProperty<QmlSceneNodeInfo> QmlSceneManager::getSelectedNodes()
{
    return QQmlListProperty<QmlSceneNodeInfo>(this, &m_selected_nodes, qmlContainerCount<std::vector<std::shared_ptr<QmlSceneNodeInfo>>>, qmlSharedPtrContainerAt<std::vector<std::shared_ptr<QmlSceneNodeInfo>>>);
}

QQmlListProperty<QmlSceneNodeInfo> QmlSceneManager::getChildren(QmlSceneNodeInfo &node)
{
    return node.getChildren();
}

void QmlSceneManager::scene_initialized()
{
    QmlHandlerBase::scene_initialized();
    m_scene = m_engine == nullptr ? nullptr : m_engine->scene_manager().get();
    SceneManagerListener::Init(m_scene);

    if (m_scene) {
        initialize_nodes();
    }
}

void QmlSceneManager::initialize_nodes()
{
    auto nodes = m_scene->root_nodes();
    while (nodes->HasNext()) {
        auto node = nodes->Next();
        register_scene_node(node);
//        m_nodes.push_back(std::make_shared<QmlSceneNodeInfo>(node.get()));
    }
    emit nodesChanged();
}

void QmlSceneManager::register_scene_node(std::shared_ptr<core::SceneNode> node)
{
    if (m_mapping.find(node->id()) != m_mapping.end()) {
        return;
    }
    if (node->parent() != nullptr) {

        auto it = m_mapping.find(node->parent()->id());
        if (it == m_mapping.end()) {
            return;//register_scene_node(n);
        }
    }

    std::shared_ptr<QmlSceneNodeInfo> info = QmlSceneNodeInfoFactory::Create(node.get());//std::make_shared<QmlSceneNodeInfo>(node.get());
    m_mapping[node->id()] = info;
    if (node->parent() != nullptr) {
        m_mapping[node->parent()->id()]->add_child(info);
    } else {
        m_nodes.push_back(info);
    }


    core::SceneNode::ChildIterator children = node->children();
    while (children.HasNext()) {
        register_scene_node(children.Next());
    }

    emit childrenChanged(*info);
}

void QmlSceneManager::SceneNodeAdded(NodeId node)
{
    register_scene_node(m_scene->get_node(node));
    emit nodesChanged();
}

void QmlSceneManager::SceneNodeRemoved(NodeId node)
{
//    printf("scene node removed  %d\n", node);
    auto it = m_mapping.find(node);
    if (it != m_mapping.end()) {
        std::shared_ptr<QmlSceneNodeInfo> info = it->second;
        m_nodes.erase(std::remove(m_nodes.begin(), m_nodes.end(), info), m_nodes.end());
        m_selected_nodes.erase(std::remove(m_selected_nodes.begin(), m_selected_nodes.end(), info), m_selected_nodes.end());
        if (info->m_parent_id != (uint)-1) {
            auto &v = m_mapping[info->m_parent_id]->m_children;
            v.erase(std::remove(v.begin(), v.end(), info), v.end());
        }
        m_mapping.erase(it);
    }
    emit selectedNodesChanged();
    emit nodesChanged();
}

void QmlSceneManager::cleared(core::SceneManager * /*manager*/)
{
    m_mapping.clear();
    m_nodes.clear();
    m_selected_nodes.clear();
    emit selectedNodesChanged();
    emit nodesChanged();
}

void QmlSceneManager::SceneNodeSelected(NodeId node)
{
//    printf("scene node selected %d\n", node);
    auto it = m_mapping.find(node);
    if (it != m_mapping.end()) {
        if (std::find(m_selected_nodes.begin(), m_selected_nodes.end(), it->second) == m_selected_nodes.end()) {
//            printf("added to selected %d\n", node);
            m_selected_nodes.push_back(it->second);
        }
    }
    emit selectedNodesChanged();
}

void QmlSceneManager::SceneNodeUnselected(NodeId node)
{
//    printf("scene node unselected %d\n", node);
    auto it = m_mapping.find(node);
    if (it != m_mapping.end()) {
        std::shared_ptr<QmlSceneNodeInfo> info = it->second;
        m_selected_nodes.erase(std::remove(m_selected_nodes.begin(), m_selected_nodes.end(), info), m_selected_nodes.end());
    }
    emit selectedNodesChanged();

}

void QmlSceneManagerRegisterer()
{
    qmlRegisterType<QmlSceneNodeInfo>("Expressive", 1, 0, "SceneNodeInfo");
    qmlRegisterType<QmlSceneManager>("Expressive", 1, 0, "SceneManager");
}


//static QmlTypeRegisterer::TypeRegistererCallback QmlSceneManagerType(QmlSceneManagerRegisterer);
QML_TYPE_REGISTERER_CALLBACK(QmlSceneManagerRegisterer);


}

}
