/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QMLSCENEMANAGER_H
#define QMLSCENEMANAGER_H

#include <core/scenegraph/SceneManager.h>
#include <core/scenegraph/listeners/SceneManagerListener.h>
#include <core/scenegraph/listeners/SceneNodeListener.h>
#include <core/scenegraph/SceneManager.h>
#include "pluginTools/gui/qml/QmlHandlerBase.h"

#include <pluginTools/gui/qml/QmlValueHandler.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include "QVector3D"
#include <QQuickItem>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

namespace plugintools
{

class QmlSceneManager;

class PLUGINTOOLS_API QmlSceneNodeInfo : public QQuickItem, public core::SceneNodeListener
{
    Q_OBJECT
    Q_PROPERTY(unsigned int id READ id CONSTANT)
    Q_PROPERTY(QString type READ type CONSTANT)
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(bool visible READ visible WRITE set_visible NOTIFY visibleChanged)
    Q_PROPERTY(bool hovered READ hovered WRITE set_hovered NOTIFY hoveredChanged)
    Q_PROPERTY(bool selected READ selected WRITE set_selected NOTIFY selectedChanged)
    Q_PROPERTY(bool wireframe READ wireframe WRITE set_wireframe NOTIFY wireframeChanged)
    Q_PROPERTY(bool adjust_size_to_params READ adjust_size_to_params WRITE set_adjust_size_to_params NOTIFY adjust_size_to_paramsChanged)
    Q_PROPERTY(bool is_overlay READ is_overlay CONSTANT)

    Q_PROPERTY(QVector3D position READ position WRITE set_position NOTIFY positionChanged)
    Q_PROPERTY(QVector3D orientation READ orientation WRITE set_orientation NOTIFY orientationChanged)
    Q_PROPERTY(QVector3D scale READ scale WRITE set_scale NOTIFY scaleChanged)

    Q_PROPERTY(unsigned int meshVertices READ meshVertices NOTIFY meshVerticesChanged)
    Q_PROPERTY(unsigned int meshEdges READ meshEdges NOTIFY meshEdgesChanged)
    Q_PROPERTY(unsigned int meshFaces READ meshFaces NOTIFY meshFacesChanged)

    Q_PROPERTY(QQmlListProperty<expressive::plugintools::QmlSceneNodeInfo> children READ getChildren NOTIFY childrenChanged)
    Q_PROPERTY(QQmlListProperty<expressive::plugintools::QmlValueHandler> parameters READ getParameters CONSTANT)
public:
    QmlSceneNodeInfo(QQuickItem *parent = nullptr);
    QmlSceneNodeInfo(const QmlSceneNodeInfo &other);
    QmlSceneNodeInfo(core::SceneNode* node);
    virtual ~QmlSceneNodeInfo();

public slots:
    //////
    /// Getters
    ///
    unsigned int id() const;
    virtual QString type() const;
    QString name() const;
    bool visible() const;
    bool hovered() const;
    bool selected() const;
    bool adjust_size_to_params() const;
    bool wireframe() const;
    bool is_overlay() const;

    QVector3D position() const;
    QVector3D orientation() const;
    QVector3D scale() const;

    unsigned int meshVertices() const;
    unsigned int meshEdges() const;
    unsigned int meshFaces() const;

    QQmlListProperty<expressive::plugintools::QmlSceneNodeInfo> getChildren();
    QQmlListProperty<expressive::plugintools::QmlValueHandler> getParameters();

    //////
    /// Setters
    ///
    void set_visible(bool b);
    void set_wireframe(bool b);
    Q_INVOKABLE void set_selected(bool b, bool clear_selection = true);
    void set_hovered(bool b);
    void set_adjust_size_to_params(bool b);

    void set_position(const QVector3D &p);
    void set_orientation(const QVector3D &p);
    void set_scale(const QVector3D &p);

    Q_INVOKABLE void clear_selection();

    /////
    /// SceneNodeListener stuff
    ///
    void updated(core::SceneNode* node = nullptr);
    void SceneNodeSelectedStateChanged(NodeId node);
    void SceneNodeHoveredStateChanged(NodeId node);
    void SceneNodeVisibilityStateChanged(NodeId node);
    void SceneNodeWireframeStateChanged(NodeId node);
    void SceneNodeAdjustSizeToParamsChanged(NodeId node);

signals:
    void visibleChanged();
    void hoveredChanged();
    void selectedChanged();
    void wireframeChanged();
    void adjust_size_to_paramsChanged();
    void positionChanged();
    void orientationChanged();
    void scaleChanged();
    void meshVerticesChanged();
    void meshEdgesChanged();
    void meshFacesChanged();
    void childrenChanged();

protected:
    void add_child(std::shared_ptr<QmlSceneNodeInfo> child);

    core::SceneNode::NodeId m_id;
    core::SceneNode *m_node;

    core::SceneNode::NodeId m_parent_id;
    std::vector<std::shared_ptr<QmlSceneNodeInfo>> m_children;
    std::vector<std::shared_ptr<QmlValueHandler>> m_parameters;

    friend class QmlSceneManager;

};

class PLUGINTOOLS_API QmlSceneManager : public QmlHandlerBase, public core::SceneManagerListener
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<expressive::plugintools::QmlSceneNodeInfo> nodes READ getNodes NOTIFY nodesChanged)
    Q_PROPERTY(QQmlListProperty<expressive::plugintools::QmlSceneNodeInfo> selected_nodes READ getSelectedNodes NOTIFY selectedNodesChanged)

public:
    QmlSceneManager();

    virtual ~QmlSceneManager();

    QQmlListProperty<QmlSceneNodeInfo> getNodes();
    QQmlListProperty<QmlSceneNodeInfo> getSelectedNodes();
    QQmlListProperty<QmlSceneNodeInfo> getChildren(QmlSceneNodeInfo &node);

    void scene_initialized();

    void initialize_nodes();

    /////
    /// SceneManagerListener stuff
    ///
    void SceneNodeAdded(NodeId node);
    void SceneNodeRemoved(NodeId node);
    void cleared(core::SceneManager *manager);
    void SceneNodeSelected(NodeId node);
    void SceneNodeUnselected(NodeId node);

signals:
    void nodesChanged();
    void selectedNodesChanged();
    void childrenChanged(const QmlSceneNodeInfo &node);
    void overlaysChanged();

protected:
    void register_scene_node(std::shared_ptr<core::SceneNode> node);

    core::SceneManager* m_scene;

//    std::vector<std::shared_ptr<QmlSceneNodeInfo>> m_overlays;
//    std::vector<std::shared_ptr<QmlSceneNodeInfo>> m_nodes;
    std::vector<std::shared_ptr<QmlSceneNodeInfo>> m_nodes;
    std::vector<std::shared_ptr<QmlSceneNodeInfo>> m_selected_nodes;

    std::map<core::SceneNode::NodeId, std::shared_ptr<QmlSceneNodeInfo>> m_mapping;
};

}

}

Q_DECLARE_METATYPE(QQmlListProperty<expressive::plugintools::QmlSceneNodeInfo>)

#endif // QMLSCENEMANAGER_H
