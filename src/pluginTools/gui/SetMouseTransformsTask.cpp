#include "pluginTools/gui/SetMouseTransformsTask.h"

#include "core/ExpressiveEngine.h"

#include "ork/render/FrameBuffer.h"
#include "ork/resource/ResourceTemplate.h"
#include "ork/scenegraph/SceneManager.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QApplication>
#include <QCursor>
#include <QWidget>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

using namespace expressive;
using namespace expressive::core;
using namespace ork;
using namespace std;

namespace expressive
{

namespace plugintools
{

SetMouseTransformsTask::SetMouseTransformsTask() : AbstractTask("SetMouseTransformsTask")
{
}

SetMouseTransformsTask::SetMouseTransformsTask(QualifiedName m,
        const char *wmp, const char *smp, const char *ss) :
    AbstractTask("SetMouseTransformsTask")
{
    init(m, wmp, smp, ss);
}

void SetMouseTransformsTask::init(QualifiedName m,
        const char *wmp, const char *smp, const char *ss)
{
    this->m = m;
    this->wmp = wmp;
    this->smp = smp;
    this->ss = ss;
    this->screenMousePos = NULL;
    this->screenSize = NULL;
}

SetMouseTransformsTask::~SetMouseTransformsTask()
{
}

ptr<Task> SetMouseTransformsTask::getTask(ptr<Object> context)
{
    ptr<ork::SceneNode> n = context.cast<Method>()->getOwner();

    if (m.target.size() > 0 && module == NULL) {
        module = m.getTarget(n)->getModule(m.name);
        if (module == NULL) {
            if (Logger::ERROR_LOGGER != NULL) {
                Logger::ERROR_LOGGER->log("SCENEGRAPH", "SetMouseTransforms: cannot find " + m.target + "." + m.name + " module");
            }
            throw exception();
        }
    } else if (m.name.size() > 0 && module == NULL) {
        module = n->getOwner()->getResourceManager()->loadResource(m.name).cast<Module>();
        if (module == NULL) {
            if (Logger::ERROR_LOGGER != NULL) {
                Logger::ERROR_LOGGER->log("SCENEGRAPH", "SetMouseTransforms: cannot find " + m.name + " module");
            }
            throw exception();
        }
    }

    return new Impl(n, this);
}

void SetMouseTransformsTask::swap(ptr<SetMouseTransformsTask> t)
{
    std::swap(module, t->module);
    std::swap(m, t->m);
    std::swap(wmp, t->wmp);
    std::swap(smp, t->smp);
    std::swap(ss, t->ss);

    if (lastProg != NULL) {
        worldMousePos = wmp == NULL ? NULL : lastProg->getUniform3f(wmp);
        screenMousePos = smp == NULL ? NULL : lastProg->getUniform2f(smp);
        screenSize = ss == NULL ? NULL : lastProg->getUniform2f(ss);
    }
}

SetMouseTransformsTask::Impl::Impl(ptr<ork::SceneNode> context, ptr<SetMouseTransformsTask> source) :
    Task("SetTransforms", true, 0), context(context), source(source)
{
}

SetMouseTransformsTask::Impl::~Impl()
{
}

bool SetMouseTransformsTask::Impl::run()
{
    if (Logger::DEBUG_LOGGER != NULL) {
        Logger::DEBUG_LOGGER->log("SCENEGRAPH", "SetMouseTransforms");
    }

    ptr<Program> prog = NULL;
    if (source->module != NULL && !source->module->getUsers().empty()) {
        const std::set<Program*> &users = source->module->getUsers();
        for (Program *p : users) {
            if (source->wmp && p->hasUniform(source->wmp)) {
                prog = p;
            }
            else if (source->smp && p->hasUniform(source->smp)) {
                prog = p;
            }
            else if (source->ss && p->hasUniform(source->ss)) {
                prog = p;
            }

        }
//        prog = *(source->module->getUsers().begin());
    } else {
        prog = ork::SceneManager::getCurrentProgram();
    }

    if (prog == NULL) {
        return true;
    }
    if (Logger::DEBUG_LOGGER != NULL) {
        Logger::DEBUG_LOGGER->logf("SCENEGRAPH", "SetMouseTransforms %d", prog.get());
    }

    if (prog != source->lastProg) {
        source->worldMousePos = source->wmp == NULL ? NULL : prog->getUniform3f(source->wmp);
        source->screenMousePos = source->smp == NULL ? NULL : prog->getUniform2f(source->smp);
        source->screenSize = source->ss == NULL ? NULL : prog->getUniform2f(source->ss);
        source->lastProg = prog;
    }

    if (source->worldMousePos != NULL) {

        Point2 p2d = ExpressiveEngine::GetSingleton()->mouse_screen_position();
        Point  p3d = ExpressiveEngine::GetSingleton()->camera_manager()->GetPointInParallelWorldPlane(ExpressiveEngine::GetSingleton()->camera_manager()->target(), p2d[0], p2d[1]);

        source->worldMousePos->set(p3d.cast<float>());
    }

    if (source->screenMousePos != NULL) {
        Point2 p = ExpressiveEngine::GetSingleton()->mouse_screen_position();
        source->screenMousePos->set(p.cast<float>());
    }

    if (source->screenSize != NULL) {
        auto fb = FrameBuffer::getDefault();
        source->screenSize->set(Vector2f((float)fb->getViewport()[2], (float)fb->getViewport()[3]));
    }

    return true;
}

/// @cond RESOURCES

class SetMouseTransformsTaskResource : public ResourceTemplate<40, SetMouseTransformsTask>
{
public:
    SetMouseTransformsTaskResource(ptr<ResourceManager> manager, const string &name, ptr<ResourceDescriptor> desc, const TiXmlElement *e = NULL) :
        ResourceTemplate<40, SetMouseTransformsTask>(manager, name, desc)
    {
        e = e == NULL ? desc->descriptor : e;
        checkParameters(desc, e, "worldMousePos,screenMousePos,screenSize,module,");

        const char *s = e->Attribute("module");
        QualifiedName module = QualifiedName(s == NULL ? "" : string(s));

        const char *worldMousePos = e->Attribute("worldMousePos");
        const char *screenMousePos = e->Attribute("screenMousePos");
        const char *screenSize = e->Attribute("screenSize");
        init(module, worldMousePos, screenMousePos, screenSize);
    }
};

extern const char setMouseTransforms[] = "setMouseTransforms";

static ResourceFactory::Type<setMouseTransforms, SetMouseTransformsTaskResource> SetMouseTransformsTaskType;

/// @endcond

}

}
