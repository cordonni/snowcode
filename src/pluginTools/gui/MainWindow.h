/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// core dependencies
#include <core/utils/Serializable.h>

// qttools dependencies
#include <qtTools/gui/QWidgetHandler.h>

// plugintools dependencies
#include "pluginTools/PluginToolsRequired.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

// Qt dependencies
#include <QActionGroup>
#include <QMainWindow>
#include <QGridLayout>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <memory>

using namespace expressive::core;

namespace Ui {
class MainWindow;
}

namespace expressive {

namespace plugintools {

class StateMachine;

class PLUGINTOOLS_API MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    explicit MainWindow(QWidget *mainWidget, QWidget *secondaryWidget, QWidget *parent = 0);
    virtual ~MainWindow();

    void setMainWidget(QWidget *widget);
    void setSecondaryWidget(QWidget *widget);

    QAction* AddModeAction(const QString &name);
    QAction* GetModeActionFromName(const QString& mode_name);

    virtual void SetStateMachine(std::shared_ptr<StateMachine> state_machine);

signals:
    void LoadFile(bool dynamic_loading);
    void SaveScene(bool static_saving);
    void ClearScene();

public slots:
    void on_actionExit_triggered();
    void on_actionLoad_triggered();
    void on_actionSave_triggered();
    void on_actionClear_triggered();

protected:
    void init(QWidget *mainWidget, QWidget *secondaryWidget, QWidget *parent = 0);

    virtual void initMenus();
    virtual void initDefaultView(QWidget *mainWidget, QWidget *secondaryWidget);


private:

    Ui::MainWindow *ui;

    QActionGroup *mode_action_group_;

    std::map<std::string, QAction*> action_active_map_;

    QGridLayout *main_layout_;

    std::shared_ptr<StateMachine> state_machine_;

};

/**
 * This class is required to be able to load a QWidget without memory issues.
 * see expressive::qtTools::QWidgetHandler.
 */
class MainWindowHandler : public expressive::qttools::QWidgetHandler
{
public:
    MainWindowHandler() : QWidgetHandler() {}
    virtual ~MainWindowHandler() {}

    //    ///////////////////
    //    // Xml functions //
    //    ///////////////////

    virtual TiXmlElement * ToXml(const char * /*name */= nullptr) const
    {
        assert(false && "Not done yet");
        return nullptr;
    }
};

} // namespace qttools

} // namespace expressive

#endif // MAINWINDOW_H
