/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef _ORK_SET_MOUSE_TRANSFORMS_TASK_H_
#define _ORK_SET_MOUSE_TRANSFORMS_TASK_H_

#include "ork/scenegraph/AbstractTask.h"
#include "ork/render/Program.h"

#include "pluginTools/PluginToolsRequired.h"

namespace expressive
{

namespace plugintools
{

/**
 * An AbstractTask to set transformation matrices in programs.
 * @ingroup scenegraph
 */
class PLUGINTOOLS_API SetMouseTransformsTask : public ork::AbstractTask
{
public:
    /**
     * Creates a new SetMouseTransformsTask.
     *
     * @param m a "node.module" qualified name. The first part specifies the
     *      scene node that contains the module. The second part specifies the
     *      name of a module in this node. This module is used to find the
     *      uniforms that this task must set.
     * @param smp the vec2 uniform to be set to the current mouse position in screen space.
     * @param ss the vec2 uniform to be set to the current screen size.
     */
    SetMouseTransformsTask(QualifiedName m,
                           const char *wmp, const char *smp, const char *ss);

    /**
     * Deletes this SetMouseTransformsTask.
     */
    virtual ~SetMouseTransformsTask();

    virtual ork::ptr<ork::Task> getTask(ork::ptr<ork::Object> context);

protected:
    /**
     * Creates an uninitialized SetMouseTransformsTask.
     */
    SetMouseTransformsTask();

    /**
     * Initializes this SetMouseTransformsTask.
     *
     * See #SetMouseTransformsTask.
     */
    void init(QualifiedName m,
              const char *wmp, const char *smp, const char *ss);

    /**
     * Swaps this SetMouseTransformsTask with the given one.
     *
     * @param t a SetMouseTransformsTask.
     */
    void swap(ork::ptr<SetMouseTransformsTask> t);

private:
    QualifiedName m;

    ork::ptr<ork::Module> module;

    ork::ptr<ork::Program> lastProg;

    ork::ptr<ork::Uniform3f> worldMousePos;

    ork::ptr<ork::Uniform2f> screenMousePos;

    ork::ptr<ork::Uniform2f> screenSize;

    const char *wmp;

    const char *smp;

    const char *ss;

    /**
     * An ork::Task to set transformation matrices in programs.
     */
    class Impl : public ork::Task
    {
    public:
        /**
         * Creates a new SetMouseTransformsTask::Task.
         *
         * @param context the ork::SceneNode that contains the Method to which
         *      'source' belongs.
         * @param source the SetMouseTransformsTask that created this task.
         */
        Impl(ork::ptr<ork::SceneNode> context, ork::ptr<SetMouseTransformsTask> source);

        /**
         * Deletes this SetMouseTransformsTask::Task
         */
        virtual ~Impl();

        virtual bool run();

    private:
        /**
         * The ork::SceneNode that contains the Method to which #source belongs.
         */
        ork::ptr<ork::SceneNode> context;

        /**
         * The SetMouseTransformsTask that created this task.
         */
        ork::ptr<SetMouseTransformsTask> source;
    };

    friend class Impl;
};

}

}

#endif
