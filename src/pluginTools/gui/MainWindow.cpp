#include <pluginTools/gui/MainWindow.h>

#include <ork/core/Logger.h>
#include <ork/resource/ResourceTemplate.h>

// plugintools dependencies
#include <pluginTools/statemachine/StateMachine.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include "ui_mainwindow.h"
#include <QSettings>
#include <QFileDialog>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

using namespace ork;
using namespace expressive::core;
using namespace expressive::qttools;
using namespace std;

namespace expressive {

namespace plugintools {

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    main_layout_(nullptr)
{
    init(NULL, NULL, parent);
}

MainWindow::MainWindow(QWidget *mainWidget, QWidget *secondaryWidget, QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      main_layout_(nullptr)
{
    init(mainWidget, secondaryWidget, parent);
}

void MainWindow::init(QWidget *mainWidget, QWidget *secondaryWidget, QWidget *parent)
{
    if (parent != this->parent()) {
        this->setParent(parent);
    }
    ui->setupUi(this);

    setAttribute( Qt::WA_DeleteOnClose );

    mode_action_group_ = new QActionGroup(this);
    mode_action_group_->setExclusive(true);

    // TODO : add the basic selection/manipulation mode, and add a simple navigation mode (no edition allowed).
//    mode_action_group_->addAction(ui->action_active_view_selection_mode);
//    mode_action_group_->addAction(ui->action_active_vessels_mode);

    initMenus();
    initDefaultView(mainWidget, secondaryWidget);
}

void MainWindow::initMenus()
{
//    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(on_actionExit_triggered()));
//    connect(ui->actionLoad, SIGNAL(triggered()), this, SLOT(on_actionLoad_triggered()));
    connect(ui->actionLoadDynamic, SIGNAL(triggered()), this, SLOT(on_actionLoad_triggered()));
//    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(on_actionSave_triggered()));
    connect(ui->actionSaveObj, SIGNAL(triggered()), this, SLOT(on_actionSave_triggered()));
//    connect(ui->actionClear, SIGNAL(triggered()), this, SLOT(on_actionClear_triggered()));
}

void MainWindow::initDefaultView(QWidget *mainWidget, QWidget *secondaryWidget)
{
    main_layout_ = dynamic_cast<QGridLayout*>(ui->centralwidget->layout()->children().at(0));
    if (mainWidget != NULL) {
        main_layout_->addWidget(mainWidget, 0, 0);
    }
    if (secondaryWidget != NULL) {
        main_layout_->addWidget(secondaryWidget, 0 , 1);
    }
    // TODO add spacers?
    // mainLayout->addWidget(new QSpacerItem());
}

void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionLoad_triggered()
{

    bool dynamic_loading = false;
    if (QObject::sender() == ui->actionLoadDynamic) {
        dynamic_loading = true;
    }
    emit LoadFile(dynamic_loading);
}

void MainWindow::on_actionSave_triggered()
{
    bool static_saving = false;
    if (QObject::sender() == ui->actionSaveObj) {
        static_saving = true;
    }
    emit SaveScene(static_saving);
}

void MainWindow::on_actionClear_triggered()
{
    emit ClearScene();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setMainWidget(QWidget *widget)
{
    if (widget != NULL) {
        main_layout_->addWidget(widget);
//        ui->centralwidget->layout()->addWidget(widget);
    }
}

void MainWindow::setSecondaryWidget(QWidget *widget)
{
    if (widget != NULL) {
//        ui->centralwidget->layout()->addWidget(widget);
        dynamic_cast<QGridLayout*>(ui->centralwidget->layout())->addWidget(widget, 0, 2);
    }
}

QAction *MainWindow::AddModeAction(const QString &mode_name)
{
    QString action_name = QString::fromUtf8("ActionActive").append(mode_name);
    QAction* action_active_mode = new QAction(this);
    action_active_mode->setObjectName(action_name);
    action_active_mode->setCheckable(true);
    this->ui->menuMode->addAction(action_active_mode);
    action_active_mode->setText(mode_name);

    mode_action_group_->addAction(action_active_mode);

    std::map<std::string, QAction*>::iterator it = action_active_map_.find(action_name.toStdString());
    if(it != action_active_map_.end()){
        Logger::ERROR_LOGGER->logf("GUI", "Error, the following name is already used as a mode name : %s", mode_name.toStdString().c_str());
        return NULL;
    }else{
        action_active_map_.insert(std::pair<std::string, QAction*>(action_name.toStdString(), action_active_mode));
    }
    return action_active_mode;
}

QAction* MainWindow::GetModeActionFromName(const QString& mode_name)
{
    QString action_name = QString::fromUtf8("ActionActive").append(mode_name);
    std::map<std::string, QAction*>::iterator it = action_active_map_.find(action_name.toStdString());
    if(it == action_active_map_.end()){
        Logger::ERROR_LOGGER->logf("GUI", "Error, the following name is not an action name : %s", mode_name.toStdString().c_str());
        return NULL;
    }else{
        return it->second;
    }
}

void MainWindow::SetStateMachine(std::shared_ptr<StateMachine> state_machine)
{
    state_machine_ = state_machine;
    connect(this, SIGNAL(ClearScene()), state_machine_.get(), SLOT(clearScene()));
    connect(this, SIGNAL(SaveScene(bool)), state_machine_.get(), SLOT(saveScene(bool)));
    connect(this, SIGNAL(LoadFile(bool)), state_machine_.get(), SLOT(loadFile(bool)));

}

/// @cond RESOURCES

class MainWindowResource : public ResourceTemplate<40, MainWindowHandler>
{
public:
    MainWindowResource(ptr<ResourceManager> manager, const string &name, ptr<ResourceDescriptor> desc, const TiXmlElement *e = NULL) :
        ResourceTemplate<40, MainWindowHandler>(manager, name, desc)
    {
        e = e == NULL ? desc->descriptor : e;
        checkParameters(desc, e, "name,mainWidget,secondaryWidget,parent,qml,");

        QWidget* mainWidget = NULL;
        QWidget* secondaryWidget = NULL;
        QWidget* parent = NULL;
//        string qmlFile = "";

        if (e->Attribute("mainWidget") != NULL) {
            mainWidget = dynamic_pointer_cast<QWidgetHandler>(manager->loadResource(getParameter(desc, e, "mainWidget")))->getWidget();
        }
        if (e->Attribute("secondaryWidget") != NULL) {
            secondaryWidget = dynamic_pointer_cast<QWidgetHandler>(manager->loadResource(getParameter(desc, e, "secondaryWidget")))->getWidget();
        }
        if (e->Attribute("parent") != NULL) {
            parent = dynamic_pointer_cast<QWidgetHandler>(manager->loadResource(getParameter(desc, e, "parent")))->getWidget();
        }
//        if (e->Attribute("qml") != NULL) {
//            qmlFile = e->Attribute("qml");
//        }

        MainWindow *window = new MainWindow(mainWidget, secondaryWidget, parent);
        init(window);
//        init(mainWidget, secondaryWidget, parent);
    }
};

extern const char mainWindow[] = "mainWindow";

static ResourceFactory::Type<mainWindow, MainWindowResource> MainWindowType;

/// @endcond

} // namespace qttools

} // namespace expressive
