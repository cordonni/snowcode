/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DEFORMABLE_L_SYSTEM_H
#define DEFORMABLE_L_SYSTEM_H

#include <pluginTools/PluginToolsRequired.h>

#include <pluginTools/procedural/generation/distribution/LSystem.h>

#include <pluginTools/procedural/interactor/InteractorPrimitive.h>
#include <pluginTools/procedural/deformable/DeformableCapsule.h>

#define DEF_BRANCH_LENGTH   DEF_CAPSULE_LENGTH
#define DEF_BRANCH_HEIGHT   DEF_CAPSULE_LENGTH
#define DEF_BRANCH_DEPTH    DEF_CAPSULE_DEPTH

namespace expressive  {
namespace plugintools {

/**
 * \brief DeformableLSystem class
 * inherits from LSystem for the static structure
 * inherits from various interactors for manipulation
 */
template<class Subelement>
class PLUGINTOOLS_API DeformableLSystem :
        public LSystem<Subelement>,
        public FieldInteractor,
        public AffineInteractor,
        public TranslationInteractor,
        public SplitInteractor,
        public MergeInteractor
{

public:

    //--------------------------------------------
    //
    // Constructors
    //
    //--------------------------------------------

    /**
     * @brief DeformableLSystem default constructor
     * @param scene_manager
     * @param manipulator
     * @param f
     * @param parent
     * @param name
     */
    DeformableLSystem(core::SceneManager* scene_manager = nullptr,
                            std::shared_ptr<core::SceneNodeManipulator> manipulator = nullptr,
                            core::Frame f = LSystem<Subelement>::DefaultFrame(),
                            core::SceneNode *parent = nullptr,
                            const string &name = LSystem<Subelement>::BuildName());

    /**
     * @brief DeformableLSystem copy constructor
     * @param to_copy : DeformableLSystem to be copied
     */
    DeformableLSystem(const DeformableLSystem<Subelement>& to_copy);




    //--------------------------------------------
    //
    // Transformation application function (inherited from Interactors)
    //
    //--------------------------------------------

    /**
     * @brief apply function for AffineTransformation (inherited from AffineInteractor)
     * @param def : AffineTransformation to be applied
     * @return : true if transformation application succeded
     */
    virtual bool apply(const AffineTransformation& def);

    /**
     * @brief apply function for AffineTransformation (inherited from AffineInteractor)
     * @param def : AffineTransformation to be applied
     * @return : true if transformation application succeded
     */
    virtual bool apply(const TranslationTransformation& def);

    /**
     * @brief apply function for DeformationTransformation (inherited from FieldInteractor)
     * @param def : DeformationTransformation to be applied
     * @return : true if transformation application succeded
     */
    virtual bool apply(const DeformationTransformation &def);

    /**
     * @brief apply function for MergingTransformation (inherited from MergeInteractor)
     * @param def : MergingTransformation to be applied
     * @return : true if transformation application succeded
     */
    virtual bool apply(const MergingTransformation& def);

    /**
     * @brief apply function for SplittingTransformation (inherited from SplitInteractor)
     * @param def : SplittingTransformation to be applied
     * @return : true if transformation application succeded
     */
    virtual bool apply(const SplittingTransformation& def);






    //--------------------------------------------
    //
    // Specific transformation function (used in apply functions)
    //
    //--------------------------------------------

    /**
     * @brief merge_positions function merges two trees (ie. deletes the one contained in def.target and places this in the middle)
     * @param def : MergingTransformation containing merging parameters
     * @return : true if success
     */
    bool merge_positions(const MergingTransformation& def);

    /**
     * @brief split_trunc function splits m_branch
     * @param child holds the adress of the child which will be created
     */
    void split_trunc(shared_ptr<DeformableLSystem<Subelement> > child);

    /**
     * @brief forward_children function transmits all DeformableLSystem children of this toward another element
     * @param child is the other element toward which children will be transmitted
     */
    void forward_children(shared_ptr<DeformableLSystem<Subelement> > child);

    /**
     * @brief applyToCapsEnd function applies the given transformation to the end of m_branch
     * @param def : AffineTransformation to be applied
     * @return true if success
     */
    bool applyToCapsEnd(const AffineTransformation& def);

    /**
     * @brief applyToCapsEnd function applies the given transformation to the end of m_branch
     * @param def : DeformationTransformation to be applied
     * @return true if success
     */
    bool applyToCapsEnd(const DeformationTransformation& def);

    /**
     * @brief ScaleOrtho function scales m_branch in it's xy plane
     * @param def
     * @param backward
     */
    void ScaleOrtho(const DeformationTransformation& def, bool backward);
    void ScaleOrtho(const float def, bool forward);
    void backward_scale(const float s);


    /**
     * @brief applyToBranch function apply the transformation to m_branch
     * @param def : transformation
     * @return : true if succeeds
     */
    bool applyToBranch(const AffineTransformation& def);

    /**
     * @brief applyToBranch function apply the transformation to m_branch
     * @param def : transformation
     * @return : true if succeeds
     */
    bool applyToBranch(const DeformationTransformation& def);

    /**
     * @brief apply_to_all_children function apply the transformation to m_branch and forwards it to all children
     * @param def : transformation
     * @return : true if succeeds
     */
    bool apply_to_all_children(const AffineTransformation& def);

    /**
     * @brief apply_deformation function apply the transformation to m_branch and forwards it to all children
     * @param def : transformation
     * @return : true if succeeds
     */
    bool apply_deformation(const DeformationTransformation& def);


    /**
     * @brief merge function : collapses branch if it is too small, and forwards this order to all children
     */
    void merge();

    /**
     * @brief split function : splits branch if it is too big, and forwards this order to all children
     */
    void split();

    /**
     * @brief gen_new_child function : creates a new sub_branch to this
     */
    void gen_new_child();

    /**
     * @brief connect_subparts function : displace every children for it to be connected to their parents (in p0 + pz)
     */
    void connect_subparts();



    void backward_displace(const Vector& displacement, const DeformableLSystem<Subelement> *const origin, const double coeff = 2.0);

    void forward_displace(const Vector& displacement, const double coeff = 2.0);

    void forward_displace(const expressive::Transform& displacement);




    //--------------------------------------------
    //
    // Sub-elements and sub-lsystems creation function (inherited from LSystem)
    //
    //--------------------------------------------

    /**
     * @brief create_subelement : creates a new sub-element (for generating m_branch)
     * @param scene_manager
     * @param parent
     * @param name
     * @param manipulator
     * @param f
     * @return
     */
    inline virtual shared_ptr<Subelement> create_subelement(core::SceneManager* scene_manager = nullptr,
                                                            core::SceneNode *parent = nullptr,
                                                            const string &name = Subelement::BuildName(),
                                                            std::shared_ptr<core::SceneNodeManipulator> manipulator = nullptr,
                                                            core::Frame f = Subelement::DefaultFrame())
    { return make_shared<Subelement>(scene_manager, manipulator, f, parent, name); }

    /**
     * @brief create_subelement by copy
     * @param to_copy
     * @return
     */
    inline virtual shared_ptr<Subelement> create_subelement (shared_ptr<Subelement> to_copy)
    { return make_shared<Subelement>(*to_copy); }


    /**
     * @brief create_branch function creates a new deformable l-system (for creating a child)
     * @param scene_manager
     * @param parent
     * @param name
     * @param manipulator
     * @param f
     * @return
     */
    inline virtual shared_ptr<LSystem<Subelement> > create_branch(core::SceneManager* scene_manager = nullptr,
                                                                  core::SceneNode *parent = nullptr,
                                                                  const string &name = LSystem<Subelement>::BuildName(),
                                                                  std::shared_ptr<core::SceneNodeManipulator> manipulator = nullptr,
                                                                  core::Frame f = LSystem<Subelement>::DefaultFrame())
    { return make_shared<DeformableLSystem<Subelement>>(scene_manager, manipulator, f, parent, name); }

    /**
     * @brief create_branch by copy
     * @param to_copy
     * @return
     */
    inline virtual shared_ptr<LSystem<Subelement> > create_branch (shared_ptr<DeformableLSystem<Subelement>> to_copy)
    { return make_shared<DeformableLSystem<Subelement>>(*to_copy); }




    //--------------------------------------------
    //
    // Utility functions (casting attributes into appropriate type)
    //
    //--------------------------------------------

    /**
     * @brief mParent return the SceneNode::parent_ attribute casted to DeformableLSystem<Subelement>
     * @return casted parent
     */
    inline DeformableLSystem<Subelement>* mParent() { return dynamic_cast<DeformableLSystem<Subelement>*>(this->parent_);}




    //--------------------------------------------
    //
    // Static attributes for parametrizing deformation behaviors
    //
    //--------------------------------------------

    static bool DEF_CAPS_BRANCH_PERFORM_INTERPRETATION;         /// \brief enables/disable deformation interpretation
    static bool DEF_CAPS_BRANCH_PERFORM_MERGE;                  /// \brief enables/disable branch merging when too small
    static bool DEF_CAPS_BRANCH_PERFORM_SPAWN;                  /// \brief enables/disable branch splitting when too big
    static bool DEF_PERFORM_CAPS_BRANCH_SUBSPAWN;               /// \brief enables/disable subbranches generation while splitting
    static bool DEF_PERFORM_BACKWARD_DIFF;                      /// \brief enables/disable defomration backward diffusion
    static bool DEF_RADIUS_MODE;                                /// \brief enables/disable radius modification

};


/**
 * @brief DeformableLSTree typedef : the typical tree uses DeformableCapsule as a specialisation type
 */
typedef DeformableLSystem<DeformableCapsule> DeformableLSTree;


} // namespace plugintools
} // namespace expressive


#include <pluginTools/procedural/deformable/DeformableLSystem.hpp>


#endif // DEFORMABLE_L_SYSTEM_H
