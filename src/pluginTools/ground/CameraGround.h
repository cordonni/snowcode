/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef CAMERAGROUND_H
#define CAMERAGROUND_H

#include <ExpressiveRequired.h>
#include <pluginTools/PluginToolsRequired.h>
#include <core/scenegraph/TerrainNode.h>

namespace expressive
{

namespace plugintools
{

// virtual base class for camera intersection with the ground
// overload it with a proper implementation
// the height function should return a pair of
//  - a bool   : existance of ground at that place
//  - a double  : height of the ground

class PLUGINTOOLS_API CameraGround
{
public:
    CameraGround() {}
    virtual ~CameraGround() {}

    // default behaviour : no ground
    virtual std::pair<bool, double> height(Vector2) { return {false, 0.0}; }
};

// simple example that place the ground at a given height
class PLUGINTOOLS_API CameraGroundIso : public CameraGround
{
public:
    CameraGroundIso(double h = 0.0) : height_ {h} {}
    virtual ~CameraGroundIso() {}

    virtual std::pair<bool, double> height(Vector2) { return {true, height_}; }

protected:
    double height_;
};

// simple example that place the ground at a given height
class PLUGINTOOLS_API CameraGroundTerrain : public CameraGround
{
public:
    CameraGroundTerrain(std::shared_ptr<core::TerrainNode> t) : terrain_ {t} {}
    virtual ~CameraGroundTerrain() {}

    virtual std::pair<bool, double> height(Vector2 p) { return terrain_->height(p); }

protected:
    std::shared_ptr<core::TerrainNode> terrain_;
};


}
}
#endif // CAMERAGROUND_H
