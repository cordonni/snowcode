//#include "pluginTools/inputcontroller/SelectionOverlay.h"
//#include "core/geometry/MeshTools.h"
//#include "ork/render/Value.h"

//using namespace expressive::core;
//using namespace ork;

//namespace expressive
//{

//namespace plugintools
//{

//SelectionOverlay::SelectionOverlay(SceneManipulator* manipulator) :
//    SceneManipulatorListener(manipulator),
//{

//    auto mesh = MeshTools::CreateQuad(Point::Zero());
//    overlay_->set_tri_mesh(mesh);
//    overlay_->internal_node()->addValue(new Value2f("pos", Point2f(0.f, 0.f)));
//    overlay_->internal_node()->addValue(new Value2f("scale", Point2f(1.0f, 1.0f)));
//    overlay_->set_visible(false);

//    manipulator->scene_manager()->AddOverlay(overlay_);
//}

//SelectionOverlay::~SelectionOverlay()
//{
//}

//bool SelectionOverlay::mouseReleased     (std::shared_ptr<core::InputController> event)
//{
//    overlay_->set_visible(false);
//    return false;
//}

//bool SelectionOverlay::mouseMoved        (std::shared_ptr<core::InputController> event)
//{
//    if (target_->panning_selection()) {
//        std::cout << "selection::moved" << std::endl;
//        std::shared_ptr<MouseInputController> mIC = target_->mouse_input_controller();
//        overlay_->set_visible(true);
//        Point2 begin = target_->selection_start_point();
//        Point2 end = mIC->get2DInput();
//        overlay_->internal_node()->addValue(new Value2f("pos", ((end + begin) / 2.0).cast<float>()));
//        overlay_->internal_node()->addValue(new Value2f("scale", (end - begin).cast<float>()));
//    }
//    return false;
//}

//} // namespace plugintools

//} // namespace expressive
