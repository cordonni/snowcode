/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef TOUCHOVERLAY_H
#define TOUCHOVERLAY_H

#include <core/CoreRequired.h>

//#include "core/scenegraph/Overlay.h"
#include "pluginTools/scenemanipulator/SceneManipulatorListener.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QPointF>
#ifdef _MSC_VER
#pragma warning( pop )
#endif
#include <vector>
#include <memory>

namespace expressive {

namespace core
{
    class Overlay;
}

namespace plugintools {

// Todo@antoine : Better handling for fingers
class PLUGINTOOLS_API TouchOverlay : public SceneManipulatorListener {
public:
    // Constructors/Destructors
    TouchOverlay(SceneManipulator *manipulator);
    virtual ~TouchOverlay();

//    inline void setBorderSize(int w, int h)
//        { layers_[11]->setDimensions(w, h); }

    void reset();
    void show(int index, bool show = true);
    void setPos(int index, const Point2 &pos);
    void setRadius(int index, float radius);
    void setRadius(float radius);
    void setColor(int index, const Color &color);
    void setColor(const Color &color);
//    void setH1Color(const Color &color);
//    void setH2Color(const Color &color);
//    void setThumbsColor(const Color &color);
//    void setThumb1Color(const Color &color);
//    void setThumb2Color(const Color &color);

    virtual bool touchBegin (std::shared_ptr<core::InputController> event);
    virtual bool touchUpdate(std::shared_ptr<core::InputController> event);
    virtual bool touchEnd   (std::shared_ptr<core::InputController> event);

protected :
    // Layers:
    // Layer 0-9 Finger Position
    // Layer 10  Thumb Limit Disk
    // Layer 11  Border Layer
    std::vector<std::shared_ptr<core::Overlay> > m_fingers;
    std::vector<Color> m_colors;
    std::vector<float> m_radii;
    std::shared_ptr<core::Camera> m_camera;

    Color m_H1Color;
    Color m_H2Color;
    Color m_thumbsColor;
};

} // namespace plugintools
} // namespace expressive

#endif // TOUCHOVERLAY_H
