#include <pluginTools/inputcontroller/MultiTouchInputController.h>

// QT
#include<QTouchEvent>
#include<QStateMachine>

using namespace expressive::core;

namespace expressive {
namespace plugintools {

///////////////////////////////////////////////////
// Constructeurs/Destructeurs
///////////////////////////////////////////////////
MultiTouchInputController::MultiTouchInputController () :
	Base       (),
	time_      (std::chrono::system_clock::now()),
	thumb_id_  (0),
    thumb_dist_(500.f),
	hand1_     (NULL),
	hand2_     (NULL) {

	// Base Data
	this->max_old_input_ = 15;
}

MultiTouchInputController::~MultiTouchInputController() {}

///////////////////////////////////////////////////
// Getters
///////////////////////////////////////////////////
void MultiTouchInputController::registerEvent (QEvent* event) {

    QInputController::registerEvent(event);
    // Event must be a QTouchEvent:

    QTouchEvent* touch_event = dynamic_cast<QTouchEvent*>(event);
	assert(touch_event);

	// Type == Begin?
	double_touch_ = false;
	if (touch_event->type() == QEvent::TouchBegin) { 
		restart_ = true; 
		
		// Double Click?
		std::chrono::time_point<std::chrono::system_clock> time = std::chrono::system_clock::now();
		
        int dt = (int)std::chrono::duration_cast<std::chrono::milliseconds>(time - time_).count();
		time_ = time;
		
		double_touch_ = dt<=600;
	}

	// Type == End?
	if (touch_event->type() == QEvent::TouchEnd) { 
		restart_     = false;
		old_restart_ = true;

		deleteOldInputs();

		// Store Inputs:
		inputs_2D_.clear();
		inputs_3D_.clear();
		id_inputs_.clear();

		return;
	}

	// All Touch Points
	const QList<QTouchEvent::TouchPoint>& touch_points = touch_event->touchPoints();
	unsigned size = unsigned(touch_points.size());

	// Check Ids
	if (size == 1) { thumb_id_ = 0; }
	else if (!restart_) {
		for (unsigned i=0; i<size; i++) {
			if (i==id_inputs_.size()) { break; }

			// Check Id
			unsigned id = touch_points[i].id();
	
			// If Id change: Thumb Registration to do?
			if (id != id_inputs_[i]) { 
				restart_  = true;
				if (size == 2 && thumb_id_ > i) { thumb_id_--; }	// Only happen in removing finger case
			}
		}
	}

	// Store Old Input
	storeOldInputs(size);

	// Store Inputs:
	inputs_2D_.clear();
	inputs_3D_.clear();
	id_inputs_.clear();
	for (unsigned i=0; i<size; i++) {
		// Input
		QTouchEvent::TouchPoint t_pt = touch_points[i];

		// Store Data
        const QPoint &p = t_pt.pos().toPoint();
        inputs_2D_.push_back(Point2(p.x(), p.y()));
		id_inputs_.push_back(t_pt.id ());
	}

    inputs_3D_ = std::vector<Point>(inputs_2D_.size());
	
	// Two Handed Check?
	if (thumb_dist_ >= 0.f) { separateHands(); }

	// Thumb Registration?
	if (restart_)          { thumbRegistration(); }
	if (thumb_id_ >= size) { thumb_id_       = 0; }

	old_restart_ = restart_;
	restart_     = false;
}

void MultiTouchInputController::registerEvent(MultiTouchInputController* ic_mt, std::vector<unsigned> id_hand) {
	// Init Data
    unsigned size = (uint)id_hand.size();

	// Check Ids
	if (size == 1) { thumb_id_ = 0; }
	else if (!restart_) {
		for (unsigned i=0; i<size; i++) {
			if (i==id_inputs_.size()) { break; }

			// Check Id
			unsigned id = ic_mt->getIdInputs()[id_hand[i]];
	
			// If Id change: Thumb Registration to do?
			if (id != id_inputs_[i]) { 
				restart_  = true;
				if (size == 2 && thumb_id_ > i) { thumb_id_--; }	// Only happen in removing finger case
			}
		}
	}

	// Store Old Input
	storeOldInputs(size);

	// Store Inputs:
	inputs_2D_.clear();
	inputs_3D_.clear();
	id_inputs_.clear();
	for (unsigned i=0; i<size; i++) { 
		inputs_2D_.push_back(ic_mt->get2DInput (  id_hand[i])); 
		id_inputs_.push_back(ic_mt->getIdInputs()[id_hand[i]]);
	}

    inputs_3D_ = std::vector<Point>(inputs_2D_.size());

	// Thumb Registration?
	if (restart_)          { thumbRegistration(); }
	if (thumb_id_ >= size) { thumb_id_       = 0; }

	old_restart_ = restart_;
	restart_     = false;
}

Scalar MultiTouchInputController::getScaleData(bool ratio) {
    assert(size()>=2);

    // Finger Data
    Point2 v_0 = last_inputs_2D_[1] - last_inputs_2D_[0];
    Point2 v_t = inputs_2D_     [1] - inputs_2D_     [0];

    return ratio? (v_t.norm()/v_0.norm()) : (v_t.norm()-v_0.norm());
}

Scalar MultiTouchInputController::getRotationData() {
    assert(size()>=2);

    // Finger Data
    Point2 v_0 = last_inputs_2D_[1] - last_inputs_2D_[0]; v_0.normalize();
    Point2 v_t = inputs_2D_     [1] - inputs_2D_     [0]; v_t.normalize();

    // Angle Data
    Scalar res = (v_0.dot(v_t)); res = acos(res>1.?1.:(res<-1.?-1.:res));
    Scalar crs = v_0[0]*v_t[1] - v_0[1]*v_t[0];

    return (crs<0?-1:1)*res;
}

Vector MultiTouchInputController::getTranslateData() {
    // Finger Data
    return inputs_3D_[thumb_id_] - last_thumb_3D_;
}

void MultiTouchInputController::phaseAnalysis(bool& ph_t, bool& ph_r, bool& ph_s, Scalar  th_t, Scalar  th_r, Scalar  th_s) {
	// Init
    Scalar  tra = 0.f; ph_t = false;
    Scalar  rot = 0.f; ph_r = false;
    Scalar  sca = 0.f; ph_s = false;

	if (old_restart_) {
		// ReInit Old Data
		old_tra_ = tra;
		old_rot_ = rot;
		old_sca_ = sca;

        // ReInit Starting Point
		start_inputs_2D_.clear();
        for (unsigned i=0; i<inputs_2D_.size(); i++) {
            start_inputs_2D_.push_back(inputs_2D_[i]);
        }

		return;
	}

	// Translation Data
    Point2 vi;
    if(start_inputs_2D_.empty())
        return;
	for (unsigned i=0; i<old_inputs_2D_.size(); i++) {
		vi  = old_inputs_2D_[i][thumb_id_] - start_inputs_2D_[thumb_id_];
        tra += vi.norm();
	}
    vi  = inputs_2D_[thumb_id_] - start_inputs_2D_[thumb_id_];
    tra = (tra + vi.norm())/(old_inputs_2D_.size()+1);

	// Is Translation Phase?
    Scalar  d_tra = (tra - old_tra_);
	old_tra_   = tra;
	ph_t       = (d_tra*d_tra>th_t*th_t);

	if (inputs_2D_.size()>=2) {
		// Starting Point Vector
        Point2 v_0  = start_inputs_2D_[0] - start_inputs_2D_[1];
        Scalar  l_0  = v_0.norm();
        Scalar  v_0x = Scalar (v_0[0])/l_0;
        Scalar  v_0y = Scalar (v_0[1])/l_0;
			
        Point2 v_t; Scalar  l_t, th, v_tx, v_ty;
		for (unsigned i=0; i<old_inputs_2D_.size(); i++) {
			// Scaling Old Data
			v_t  = old_inputs_2D_[i][0] - old_inputs_2D_[i][1];
            l_t  = v_t.norm();
			sca += l_t/l_0;

			// Rotation Old Data
            v_tx = Scalar (v_t[0])/l_t;
            v_ty = Scalar (v_t[1])/l_t;
			th = v_0x*v_tx + v_0y*v_ty;
			rot += 180.f/M_PI*acos(th);
		}

		// Scaling Data
		v_t = inputs_2D_[0] - inputs_2D_[1];
        l_t = v_t.norm();
		sca += l_t/l_0;

		sca /= old_inputs_2D_.size()+1;

		// Rotation Data
        v_tx = Scalar (v_t[0])/l_t;
        v_ty = Scalar (v_t[1])/l_t;
		th = v_0x*v_tx + v_0y*v_ty;
		rot += 180.f/M_PI*acos(th);

		rot /= old_inputs_2D_.size()+1;
			
		// Is Rotation Phase?
        Scalar  d_rot = (rot - old_rot_);
		old_rot_   = rot;
		ph_r       = (d_rot*d_rot>th_r*th_r);

		// Is Scaling Phase?
        Scalar  d_sca = (sca - old_sca_);
		old_sca_   = sca;
		ph_s       = (d_sca*d_sca>th_s*th_s);
	}
}

///////////////////////////////////////////////////
// Setters
///////////////////////////////////////////////////
void MultiTouchInputController::thumbRegistration() {
	// The Controller must have 3 to 5 fingers
    unsigned size = (uint)inputs_2D_.size();
	if (size<3 || size>5) { return; }

	// Barycenter
    Point2 barycenter = Point2(0, 0);
	for (unsigned i=0; i<size; i++) { barycenter += inputs_2D_[i]; }
	barycenter /= size;

	// Refence Vector
    Scalar  x_ref = Scalar (inputs_2D_[0][0] - barycenter[0]);
    Scalar  y_ref = Scalar (inputs_2D_[0][1] - barycenter[1]);
    Scalar  n_ref = sqrt(x_ref*x_ref + y_ref*y_ref);
	
	// Angle from reference Vector
    Scalar  angle[6]; angle[0] = 0.f; angle[size] = Scalar (2.*M_PI);
	for (unsigned i=1; i<size; i++) {
        Scalar  x = Scalar (inputs_2D_[i][0] - barycenter[0]);
        Scalar  y = Scalar (inputs_2D_[i][1] - barycenter[1]);
        Scalar  n = sqrt(x*x + y*y);

		angle[i] = (x_ref*y - y_ref*x<=0.f ? 
			2.f*M_PI - acos((x_ref*x + y_ref*y)/(n_ref*n)): 
		               acos((x_ref*x + y_ref*y)/(n_ref*n)));
	}

	// Angle Order
	unsigned order[6]; order[0] = 0; order[size] = size;
	for (unsigned i=1; i<size; i++) {
        Scalar  d_l = angle[order[i-1]];
        Scalar  d_2 = 7.f;

		for (unsigned j=1; j<size; j++) {
			if (angle[j]<=d_l) { continue; }
			if (angle[j]< d_2) {
				d_2 = angle[j];
				order[i] = j;
			}
		}
	}

	// Max Spanning Angle ( = Sum of the two adjacent angles )
    Scalar      max = 0.f;
	for (unsigned i=0; i<size; i++) {
        Scalar  sp_ang;
		if   (i==0) { sp_ang = angle[order  [1]] - angle[order[size-1]] + 2.f*M_PI; }
		else        { sp_ang = angle[order[i+1]] - angle[order[i   -1]]; }

		if (sp_ang > max) { 
			max       = sp_ang;
			thumb_id_ = order[i];
		}
	}
}

void MultiTouchInputController::store_current_3D() {
    if (isTwoHanded()) {
        hand1_->store_current_3D();
        hand2_->store_current_3D();
    }
    last_thumb_3D_ = inputs_3D_[thumb_id_];
}

void MultiTouchInputController::store_current_2D() {
    if (isTwoHanded()) {
        hand1_->store_current_2D();
        hand2_->store_current_2D();
    }
    last_inputs_2D_.clear();
    for (unsigned i=0; i<inputs_2D_.size(); i++) { last_inputs_2D_.push_back(inputs_2D_[i]); }
}

///////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////
void MultiTouchInputController::allInput2DTo3D(Camera *camera, Point ref_pt, bool old /* = false */ ) {
    Base::allInput2DTo3D(camera, ref_pt, old);

	// Store in Hand1 & Hand2
    if (hand1_) { hand1_->allInput2DTo3D(camera, ref_pt, old); }
    if (hand2_) { hand2_->allInput2DTo3D(camera, ref_pt, old); }
}

void MultiTouchInputController::storeOldInputs(unsigned nb) {
	// Check Same Numbers
	if (restart_ || nb != inputs_2D_.size()) { deleteOldInputs(); return; }

	// Store Old Inputs
	old_inputs_2D_.push_back(inputs_2D_);
	old_inputs_3D_.push_back(inputs_3D_);

	// Check Historical Size
	if (old_inputs_2D_.size() > max_old_input_) {
		old_inputs_2D_.erase(old_inputs_2D_.begin());
		old_inputs_3D_.erase(old_inputs_3D_.begin());
	}
}

void MultiTouchInputController::deleteOldInputs() {
	for (unsigned i=0; i<old_inputs_2D_.size(); i++) {
		old_inputs_2D_[i].clear();
		old_inputs_3D_[i].clear();
	}

	old_inputs_2D_.clear();
	old_inputs_3D_.clear();

	restart_ = true;
}

void MultiTouchInputController::separateHands() {
	// Init Hands Ids;
	id_hand1_.clear();
	id_hand2_.clear();

	// Check Two Hand
	if (id_inputs_.size() == 1) { id_hand1_.push_back(0); }
	else {
		unsigned n = 0;
		for (auto it = id_inputs_.begin(); it != id_inputs_.end(); it++) {
			unsigned id = *it;
			unsigned n2 = n;

			// Check Hand1:
			if (hand1_) { 
				for (auto it2 = hand1_->getIdInputs().begin(); it2 != hand1_->getIdInputs().end(); it2++) {
					if (id == *it2) { id_hand1_.push_back(n++); break; }
				}
			}
			if (n2 != n) { continue; }

			// Check Hand2:
			if (hand2_) { 
				for (auto it2 = hand2_->getIdInputs().begin(); it2 != hand2_->getIdInputs().end(); it2++) {
					if (id == *it2) { id_hand2_.push_back(n++); break; }
				}
			}
			if (n2 != n) { continue; }

			// New Point
			if (hand1_ && hand2_) {
                Point2 v1 = inputs_2D_[n] - hand1_->get2DInput(hand1_->getThumbId());
                Point2 v2 = inputs_2D_[n] - hand2_->get2DInput(hand2_->getThumbId());

                if (v1.squaredNorm() <= v2.squaredNorm()) {
//                    if (v1.x()*v1.x() + v1.y()*v1.y() <= v2.x()*v2.x() + v2.y()*v2.y()) {
                    id_hand1_.push_back(n++);
                } else {
                    id_hand2_.push_back(n++);
                }
			} else if (hand1_) {
                Point2 v = inputs_2D_[n] - inputs_2D_[thumb_id_];
                if (v.squaredNorm() < thumb_dist_*thumb_dist_) { id_hand1_.push_back(n++); }
				else                                                     { id_hand2_.push_back(n++); }
			} else {
                Point2 v = inputs_2D_[n] - inputs_2D_[thumb_id_];
                if (v.squaredNorm() < thumb_dist_*thumb_dist_) { id_hand2_.push_back(n++); }
				else                                                     { id_hand1_.push_back(n++); }
			}
		}
	}

	// Reset Two Hands?
	if (restart_) {
		if (hand1_) { hand1_->setRestart(restart_); }
		if (hand2_) { hand2_->setRestart(restart_); }
	}

	if (id_hand1_.size() == 0) { if ( hand1_) { delete hand1_; hand1_ = NULL; } }
    else                       { if (!hand1_) { hand1_  = new MultiTouchInputController(); } hand1_->registerEvent(this, id_hand1_); }
	if (id_hand2_.size() == 0) { if ( hand2_) { delete hand2_; hand2_ = NULL; } }
    else                       { if (!hand2_) { hand2_  = new MultiTouchInputController(); } hand2_->registerEvent(this, id_hand2_); }
}

} // namespace plugintools
} // namespace expressive

