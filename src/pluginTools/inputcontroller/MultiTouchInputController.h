/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ICMultiTouch.h

   Language: C++

   License: Convol Licence

   \author: Remi Brouet
   E-Mail: remi.brouet@inria.fr

   Description: Header file for MT input controllers

   Platform Dependencies: None
*/

#ifndef IC_MULTITOUCH_H
#define IC_MULTITOUCH_H

#include <pluginTools/inputcontroller/QInputController.h>

// std
#include<chrono>

namespace expressive {
namespace plugintools {

class MultiTouchInputController : public QInputController {
public:
    typedef QInputController Base;
	
	// Constructeurs / Destructeurs
    MultiTouchInputController();
    virtual ~MultiTouchInputController();
	
	// Getters
    virtual void registerEvent(QEvent*);
    virtual void registerEvent(MultiTouchInputController*, std::vector<unsigned>);

    // Getters: 2D Data
    Scalar getScaleData    (bool ratio = true);
    Scalar getRotationData ();
    Vector getTranslateData();

    inline virtual bool isRestart    () const { return old_restart_; }
    inline         bool isTwoHanded  () const { return hand1_ && hand2_; }
    inline         bool isDoubleTouch() const { return double_touch_; }
	
    inline Scalar   getThumbDist () const { return thumb_dist_; }
    inline unsigned getThumbId   () const { return thumb_id_; }
    inline unsigned getH1ThumbId () const { return id_hand1_[hand1_->getThumbId()]; }
    inline unsigned getH2ThumbId () const { return id_hand2_[hand2_->getThumbId()]; }

    inline MultiTouchInputController* getHand1     () const { return hand1_; }
    inline MultiTouchInputController* getHand2     () const { return hand2_; }
    inline std::vector<unsigned>&     getIdInputs  () { return id_inputs_; }
    inline std::vector<unsigned>&     getH1IdInputs() { return hand1_->getIdInputs(); }
    inline std::vector<unsigned>&     getH2IdInputs() { return hand2_->getIdInputs(); }

    void phaseAnalysis(bool& ph_t, bool& ph_r, bool& ph_s, Scalar  th_t = 0.5f, Scalar  th_r = 0.25f, Scalar  th_s = 0.0015f);

	// Setters
	void thumbRegistration();

    void store_current_3D();
    void store_current_2D();
	
    inline void setHand1    (MultiTouchInputController* hand1) { hand1_      = hand1; }
    inline void setHand2    (MultiTouchInputController* hand2) { hand2_      = hand2; }
    inline void setThumbDist(Scalar                     tDist) { thumb_dist_ = tDist; }

	// Function
    virtual void allInput2DTo3D(core::Camera *camera, Point ref_pt, bool old = false);

protected:
	// Data
	bool old_restart_;

	// Double Click Data
    bool double_touch_;
	std::chrono::time_point<std::chrono::system_clock> time_;

	// Thumb Registration Data	
	unsigned              thumb_id_;
	std::vector<unsigned> id_inputs_;

	// Phase Analysis Data
    Scalar  old_tra_;
    Scalar  old_rot_;
    Scalar  old_sca_;

    Point                last_thumb_3D_;
    std::vector<Point2> last_inputs_2D_;
    std::vector<Point2> start_inputs_2D_;

	// Two Hands Data
    Scalar                     thumb_dist_;
    MultiTouchInputController* hand1_;
    MultiTouchInputController* hand2_;
    std::vector<unsigned>      id_hand1_;
    std::vector<unsigned>      id_hand2_;

	// Functions
	void storeOldInputs (unsigned nb);
	void deleteOldInputs();

	void separateHands();
};

typedef MultiTouchInputController MultiTouchIC;

} // namespace plugintools
} // namespace expressive


#endif // IC_MULTITOUCH_H
