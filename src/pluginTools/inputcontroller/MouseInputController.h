/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ICMouse.h

   Language: C++

   License: Convol Licence

   \author: Remi Brouet
   E-Mail: remi.brouet@inria.fr

   Description: Header file for Mouse input controllers 

   Platform Dependencies: None
*/

#ifndef IC_MOUSE_H
#define IC_MOUSE_H

#include <pluginTools/inputcontroller/QInputController.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QMouseEvent>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

namespace plugintools
{

class PLUGINTOOLS_API MouseInputController : public QInputController {
public:
	
    typedef QInputController Base;

	// Constructeurs / Destructeurs
    MouseInputController(uint width = 1, uint height = 1);
    virtual ~MouseInputController();
	
	// Getters
    virtual void registerEvent(QEvent*);

	// Setters
	inline void setRestart(bool restart) { restart_ = restart; }

    inline int wheel_delta    () const { return wheel_delta_; }
    inline int old_wheel_delta() const { return old_wheel_delta_; }

    inline Qt::MouseButton button    () const { return button_; }
    inline Qt::MouseButton old_button() const { return old_button_; }

    inline bool mouse_in_window() const { return m_mouse_in_window; }
    inline void set_mouse_in_window(bool b) { m_mouse_in_window = b; }

protected:
    // Button Data
    Qt::MouseButton button_;
    Qt::MouseButton old_button_;

    // Wheel Data
    int wheel_delta_;
    int old_wheel_delta_;

    bool m_mouse_in_window;
};

typedef MouseInputController MouseIC;

} // namespace plugintools
} // namespace expressive

#endif // IC_MOUSE_H
