//#include <pluginTools/inputcontroller/InputControllerViewer.h>

//#include <pluginTools/scenemanipulator/SceneManipulator.h>

//using namespace expressive::core;
//using namespace std;

//namespace expressive{
//namespace plugintools{

//InputControllerViewer::InputControllerViewer(std::shared_ptr<SceneManipulator> scene_manipulator) :
//    SceneManipulatorListener(scene_manipulator),
//    scene_manipulator_(scene_manipulator) {
//    // Touch Data
//    touch_overlay_ = std::make_shared<TouchOverlay>();
//    touch_overlay_->setSize(10, 2.*scene_manipulator->multitouch_input_controller()->getThumbDist());
//    // Pen Data
//    pen_overlay_ = std::make_shared<PenOverlay>();
//    pen_overlay_->setPenSize(scene_manipulator->pen_size());

//    // Widget Data
//}

//bool InputControllerViewer::touchBegin (std::shared_ptr<InputController> event) {
//    // Touch Event
//    shared_ptr<MultiTouchIC> touchEvent = static_pointer_cast<MultiTouchIC>(event);

//    // Point Data
//    Point2 point = touchEvent->get2DInput(touchEvent->getThumbId());

//    // Show Overlay
//    touch_overlay_->show();

//    // Show Layers (Which is the Thumb)
//    touch_overlay_->setPos( 0, point);
//    touch_overlay_->setPos(10, point);
//    touch_overlay_->show  ( 0);

//    // Update Scene
//    scene_manipulator_->scene_manager()->NotifyUpdate();

//    return false;
//}

//bool InputControllerViewer::touchUpdate ( std::shared_ptr<InputController> event) {
//    // Touch Event
//    shared_ptr<MultiTouchIC> touchEvent = static_pointer_cast<MultiTouchIC>(event);

//    // Hide All & Show Overlay
//    touch_overlay_->hide();
//    touch_overlay_->show();

//    if (touchEvent->isTwoHanded()) {
//        //Show First Hand
//        auto     h1     = touchEvent->getHand1();
//        unsigned thumb1 = h1->getThumbId();

//        // Show Thumb
//        touch_overlay_->setPos(0, h1->get2DInput(thumb1));
//        touch_overlay_->show  (0);

//        // Show Others
//        unsigned i_o = 2;
//        for (unsigned i=0; i<h1->size(); i++) {
//            if (i == thumb1) { continue; }

//            // Render Finger
//            touch_overlay_->setPos(i_o, h1->get2DInput(i));
//            touch_overlay_->show  (i_o++);
//        }

//        //Show Second Hand
//        auto     h2     = touchEvent->getHand2();
//        unsigned thumb2 = h2->getThumbId();

//        // Show Thumb
//        touch_overlay_->setPos(1, h2->get2DInput(thumb2));
//        touch_overlay_->show  (1);

//        // Show Others
//        i_o = 6;
//        for (unsigned i=0; i<h2->size(); i++) {
//            if (i == thumb2) { continue; }

//            // Render Finger
//            touch_overlay_->setPos(i_o, h2->get2DInput(i));
//            touch_overlay_->show  (i_o++);
//        }

//    } else {
//        //Show First Hand
//        auto     h1     = touchEvent;
//        unsigned thumb1 = h1->getThumbId();

//        // Show Thumb
//        touch_overlay_->setPos( 0, h1->get2DInput(thumb1));
//        touch_overlay_->setPos(10, h1->get2DInput(thumb1));

//        touch_overlay_->show  ( 0);
//        touch_overlay_->show  (10);

//        // Show Others
//        unsigned i_o = 2;
//        for (unsigned i=0; i<h1->size(); i++) {
//            if (i == thumb1) { continue; }

//            // Render Finger
//            touch_overlay_->setPos(i_o, h1->get2DInput(i));
//            touch_overlay_->show  (i_o++);
//        }
//    }

//    // Update Scene
//    scene_manipulator_->scene_manager()->NotifyUpdate();

//    return false;
//}

//bool InputControllerViewer::touchEnd(std::shared_ptr<InputController>) {
//    // Overlay Hide
//    touch_overlay_->hide();
//    scene_manipulator_->scene_manager()->NotifyUpdate();

//    return false;
//}

///*
//bool InputControllerViewer::penBegin ( std::shared_ptr<InputController> event){

//    return false;
//}
//bool InputControllerViewer::penUpdate ( std::shared_ptr<InputController> event){

//    return false;
//}
//bool InputControllerViewer::penEnd ( std::shared_ptr<InputController> event){

//    return false;
//}
//*/

//void InputControllerViewer::updated (SceneManipulator *scene_manipulator) {
//    // Camera Manager
//    core::Camera* camera_manager = scene_manipulator->scene_manager()->current_camera();

//    if (scene_manipulator->is_pen_envent()){
//        // Pen Data
//        Scalar ratio = camera_manager->GetRatioWorldView();
//        pen_overlay_->setPenSize(scene_manipulator->pen_size()*ratio);
//        //Need to be reinplaced by real pen event when plugin between tablet and Qt work together
//        pen_overlay_->setPos(scene_manipulator->mouse_input_controller()->get2DInput());
//        pen_overlay_->show();
//    } else {
//        // Touch Data
//        pen_overlay_->hide();

//        // Screen Size
//        int w = camera_manager->GetActualWidth ();
//        int h = camera_manager->GetActualHeight();

//        // Thumb Disk Layer
//        touch_overlay_->setSize(10, 2.*scene_manipulator->multitouch_input_controller()->getThumbDist());

//        // Border Layer
//        touch_overlay_->setBorderSize(w+2, h+2);
//     }
//}

//} // namespace plugintools
//} // namespace expressive

