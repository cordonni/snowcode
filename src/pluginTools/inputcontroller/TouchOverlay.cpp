#include <pluginTools/inputcontroller/TouchOverlay.h>

#include "core/ExpressiveEngine.h"
#include "core/scenegraph/Overlay.h"
#include "core/geometry/MeshTools.h"

using namespace expressive::core;

namespace expressive{
namespace plugintools{

TouchOverlay::TouchOverlay(SceneManipulator *manipulator) :
    SceneManipulatorListener(manipulator),
    m_camera(nullptr),
    m_H1Color(Color(148.f / 255.f, 87.f / 255.f, 255.f / 255.f, 128.f / 255.f)),
    m_H2Color(Color(87.f / 255.f, 109.f / 255.f, 255.f / 255.f, 128.f / 255.f)),
    m_thumbsColor(Color(255.f / 255.f, 87.f / 255.f, 109.f / 255.f, 128.f / 255.f))
{
    std::shared_ptr<SceneManager> scene_manager = manipulator == nullptr ? ExpressiveEngine::GetSingleton()->scene_manager() : manipulator->scene_manager();

    m_camera = scene_manager->current_camera();

    // Fill the Overlay with 12 Layers
    // Layer 0-9 Finger Position
    // Layer 10  Thumb Limit Disk
    // Layer 11  Border Layer
    for (int i=0; i<10; i++) {
        // Layers Attribute
        switch (i) {
        case 0:
        case 1:
            // Thumbs
            m_colors.push_back(m_thumbsColor);
            m_radii.push_back(0.1f);
            break;
        case 2:
        case 3:
        case 4:
        case 5:
            // First Hand Fingers
            m_colors.push_back(m_H1Color);
            m_radii.push_back(0.05f);
            break;
        default:
            // Second Hand Fingers
            m_colors.push_back(m_H2Color);
            m_radii.push_back(0.05f);
            break;
        }

        // Create Layers
        std::string name = "FingerPositionLayer" + i;
        std::shared_ptr<Overlay> finger = std::make_shared<Overlay>(scene_manager.get(), name, Overlay::O_FOREGROUND, "overlayMethod");
        finger->set_visible(false);

        std::shared_ptr<AbstractTriMesh> mesh = MeshTools::CreateDisk();
        mesh->FillColor(m_colors[i]);
        finger->set_tri_mesh(mesh);
        finger->internal_node()->addValue(new ork::Value2f("pos", Point2f(0.f, 0.f)));
        finger->internal_node()->addValue(new ork::Value2f("scale", Point2f(m_radii[i], m_radii[i])));

        // Store Layers
        m_fingers.push_back(finger);
        scene_manager->AddOverlay(finger);
    }

//    // Thumb Limit Disk
//    Ogre::OverlayContainer* th_limit_disk = static_cast<Ogre::OverlayContainer*>(overlayManager.createOverlayElement("Panel", "Thumb_Disk"));
//    th_limit_disk->setMetricsMode(Ogre::GMM_PIXELS);
//    th_limit_disk->setMaterialName("ThumbDistBlack");
//    th_limit_disk->hide();

//    // Border Layer
//    Ogre::OverlayContainer* border_layer = static_cast<Ogre::OverlayContainer*>(overlayManager.createOverlayElement("Panel", "Border_Layer"));
//    border_layer->setMetricsMode(Ogre::GMM_PIXELS);
//    border_layer->setMaterialName("BorderBlack");
//    border_layer->hide();
//    border_layer->setPosition(-1., -1.);

//    // Store Layer
//    layers_.push_back(th_limit_disk);
//    layers_.push_back(border_layer );

//    sizes_ .push_back(0.);
//    sizes_ .push_back(0.);

//    overlay_->add2D(th_limit_disk);
//    overlay_->add2D(border_layer);
}

TouchOverlay::~TouchOverlay()
{
    std::shared_ptr<SceneManager> scene_manager = target_ == nullptr ? ExpressiveEngine::GetSingleton()->scene_manager() : target_->scene_manager();
    if (scene_manager != nullptr) {
        for (unsigned int i = 0; i < m_fingers.size(); ++i) {
            scene_manager->RemoveOverlay(m_fingers[i]->id());
        }
    }

    m_fingers.clear();
    m_colors.clear();
    m_radii.clear();
}

void TouchOverlay::reset()
{
    for (unsigned int i = 0; i < m_fingers.size(); ++i) {
        m_fingers[i]->set_visible(false);
    }
}

void TouchOverlay::show(int index, bool show)
{
    m_fingers[index]->set_visible(show);
}

void TouchOverlay::setPos(int index, const Point2 &pos)
{
    m_fingers[index]->internal_node()->addValue(new ork::Value2f("pos", m_camera->GetNormalizedScreenCoordinate(pos).cast<float>()));
}

void TouchOverlay::setRadius(int index, float radius)
{
    Point2f scale;
    scale[0] = radius * 2.f / m_camera->GetActualWidth();
    scale[1] = radius * 2.f / m_camera->GetActualHeight();
    m_radii[index] = radius;
    m_fingers[index]->internal_node()->addValue(new ork::Value2f("scale", scale));
}

void TouchOverlay::setRadius(float radius)
{
    Point2f scale;
    scale[0] = radius * 2.f / m_camera->GetActualWidth();
    scale[1] = radius * 2.f / m_camera->GetActualHeight();

    for (unsigned int i = 0; i < m_fingers.size(); ++i) {
        m_radii[i] = radius;
        m_fingers[i]->internal_node()->addValue(new ork::Value2f("scale", (i < 2 ? 1.f : 0.5f) * scale));
    }
}

void TouchOverlay::setColor(int index, const Color &color)
{
    m_colors[index] = color;
    m_fingers[index]->tri_mesh()->FillColor(color);
}

void TouchOverlay::setColor(const Color &color)
{
    for (unsigned int i = 0; i < m_fingers.size(); ++i) {
        m_colors[i] = color;
        m_fingers[i]->tri_mesh()->FillColor(color);
    }
}

bool TouchOverlay::touchBegin (std::shared_ptr<core::InputController> event)
{
    std::shared_ptr<MultiTouchIC> e = std::dynamic_pointer_cast<MultiTouchIC>(event);

//    setFingerRadius(m_fingerRadius);

    for (unsigned int i = 0; i < e->size(); ++i) {
        setPos(i, e->get2DInput(i));
        show(i);
    }
//    if (e->isTwoHanded()) {
//        MultiTouchInputController *hand1 = e->getHand1();
//        MultiTouchInputController *hand2 = e->getHand2();


//    } else {
//        for ()
//    }



    return false;
}

bool TouchOverlay::touchUpdate(std::shared_ptr<core::InputController> event)
{
    std::shared_ptr<MultiTouchIC> e = std::dynamic_pointer_cast<MultiTouchIC>(event);

    for (unsigned int i = 0; i < e->size(); ++i) {
        setPos(i, e->get2DInput(i));
        show(i);
    }
    for (unsigned int i = e->size(); i < m_fingers.size(); ++i) {
        show(i, false);
    }

    return false;
}

bool TouchOverlay::touchEnd   (std::shared_ptr<core::InputController> event)
{
    std::shared_ptr<MultiTouchIC> e = std::dynamic_pointer_cast<MultiTouchIC>(event);

    for (unsigned int i = 0; i < m_fingers.size(); ++i) {
        show(i, false);
    }

    return false;
}

} // namespace plugintools

} // namespace expressive
