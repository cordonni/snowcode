#include <pluginTools/inputcontroller/PenInputController.h>

// QT
#include<QStateMachine>

namespace expressive
{

namespace plugintools
{

///////////////////////////////////////////////////
// Constructeurs/Destructeurs
///////////////////////////////////////////////////
PenInputController::PenInputController () : Base(), type_(QEvent::None)
{
}

PenInputController::~PenInputController() {}

///////////////////////////////////////////////////
// Getters
///////////////////////////////////////////////////
void PenInputController::registerEvent(QEvent* event) {
    QInputController::registerEvent(event);

    // Event must be a QKeyboardEvent:
    QStateMachine::WrappedEvent* wrapped_event = dynamic_cast<QStateMachine::WrappedEvent*>(event);
    // assert(wrapped_event);

    QEvent *subevent = (wrapped_event == nullptr) ? event : wrapped_event->event();

    QTabletEvent* pen_event = dynamic_cast<QTabletEvent*>(subevent);
    if (pen_event != nullptr) {
        type_ = pen_event->type();
        std::cout << "resister pen event!" << std::endl;
    }
}

} // namespace plugintools

} // namespace expressive
