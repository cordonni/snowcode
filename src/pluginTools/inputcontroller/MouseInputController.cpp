#include <pluginTools/inputcontroller/MouseInputController.h>

// QT
#include<QMouseEvent>
#include<QStateMachine>

namespace expressive
{

namespace plugintools
{

///////////////////////////////////////////////////
// Constructeurs/Destructeurs
///////////////////////////////////////////////////
MouseInputController::MouseInputController (uint width, uint height) : Base(width, height) {
    inputs_2D_ = std::vector<Point2>(1);
    inputs_3D_ = std::vector<Point>(1);

    old_inputs_2D_ = std::vector< std::vector<Point2> >(1);
    old_inputs_3D_ = std::vector< std::vector<Point> >(1);

    old_inputs_2D_[0] = std::vector<Point2>(1);
    old_inputs_3D_[0] = std::vector<Point>(1);

    m_mouse_in_window = true;
}

MouseInputController::~MouseInputController() {}

///////////////////////////////////////////////////
// Getters
///////////////////////////////////////////////////
void MouseInputController::registerEvent(QEvent* event) {
    QInputController::registerEvent(event);

    // Event must be a QMouseEvent:
    QStateMachine::WrappedEvent* wrapped_event = dynamic_cast<QStateMachine::WrappedEvent*>(event);
    // assert(wrapped_event);

    QEvent *subevent = (wrapped_event == nullptr) ? event : wrapped_event->event();

    QMouseEvent* mouse_event = dynamic_cast<QMouseEvent*>(subevent);
    if (mouse_event != nullptr) {

        // Type == Press?
        if (mouse_event->type() == QEvent::MouseButtonPress) {
            restart_ = true;
            pressed_ = true;
        } else if (mouse_event->type() == QEvent::MouseButtonRelease) {
            pressed_ = false;
        }

        QPoint p = mouse_event->pos();
#ifdef __APPLE__
        // Handle Apple retina display...
        p *= 2.0;
        //TODO:@todo : we should rather use *= ...->devicePixelRatio();
        // from either QWindow, QScreen and QGuiApplication, the problem is to get it here
#endif


        // Store old Input:
        if (!restart_) {
            old_inputs_2D_[0][0] = inputs_2D_[0];
            old_inputs_3D_[0][0] = inputs_3D_[0];
        } else {
            old_inputs_2D_[0][0] = Point2(p.x(), p.y());
        }

        restart_ = false;

        // Store Input:
        inputs_2D_[0] = Point2(p.x(), p.y());

        old_button_ = button_;
        button_     = mouse_event->button();

        if (button_ == Qt::NoButton && !restart_) {
            button_ = old_button_;
        }

    } else {
        QWheelEvent* wheel_event = dynamic_cast<QWheelEvent*>(subevent);

        if (wheel_event != nullptr) {
            old_wheel_delta_ = wheel_delta_;
            wheel_delta_     = wheel_event->delta() > 0 ? 1 : -1;

            old_inputs_2D_[0][0] = inputs_2D_[0];
            old_inputs_3D_[0][0] = inputs_3D_[0];

            // Store Input:
            QPoint p = wheel_event->pos();
#ifdef __APPLE__
        // Handle Apple retina display...
        p *= 2.0;
        //TODO:@todo : we should rather use *= ...->devicePixelRatio();
        // from either QWindow, QScreen and QGuiApplication, the problem is to get it here
#endif
            inputs_2D_[0] = Point2(p.x(), p.y());

            restart_ = true;
        }
    }
}

} // namespace plugintools

} // namespace expressive

