#include <pluginTools/inputcontroller/QInputController.h>


using namespace expressive::core;

namespace expressive {
namespace plugintools {

///////////////////////////////////////////////////
// Constructeurs/Destructeurs
///////////////////////////////////////////////////
QInputController::QInputController (uint width , uint height ) : InputController(width, height) {}
QInputController::~QInputController() {}

} // namespace plugintools
} // namespace expressive
