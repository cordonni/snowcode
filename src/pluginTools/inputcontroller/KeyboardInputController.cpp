#include <pluginTools/inputcontroller/KeyboardInputController.h>

// QT
#include<QStateMachine>

namespace expressive
{

namespace plugintools
{

///////////////////////////////////////////////////
// Constructeurs/Destructeurs
///////////////////////////////////////////////////
KeyboardInputController::KeyboardInputController () : Base(), key_(Qt::Key_unknown), type_(QEvent::None)
{
}

KeyboardInputController::~KeyboardInputController() {}

///////////////////////////////////////////////////
// Getters
///////////////////////////////////////////////////
void KeyboardInputController::registerEvent(QEvent* event) {
    QInputController::registerEvent(event);

    // Event must be a QKeyboardEvent:
    QStateMachine::WrappedEvent* wrapped_event = dynamic_cast<QStateMachine::WrappedEvent*>(event);
    // assert(wrapped_event);

    QEvent *subevent = (wrapped_event == nullptr) ? event : wrapped_event->event();

    QKeyEvent* key_event = dynamic_cast<QKeyEvent*>(subevent);
    if (key_event != nullptr) {
        type_ = key_event->type();
        key_ = key_event->key();
        text_ = key_event->text().toStdString();
    }
}

} // namespace plugintools

} // namespace expressive

