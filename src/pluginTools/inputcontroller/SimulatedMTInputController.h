/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ICMTSimulated.h

   Language: C++

   License: Convol Licence

   \author: Remi Brouet
   E-Mail: remi.brouet@inria.fr

   Description: Header file for MT input controllers, simulated by mouse

   Platform Dependencies: None
*/

#ifndef IC_MT_SIMULATED_H
#define IC_MT_SIMULATED_H

#define DISTANCE_MIN 20.f

#include <pluginTools/inputcontroller/MultiTouchInputController.h>

namespace expressive
{

namespace plugintools
{

class SimulatedMTInputController : public MultiTouchInputController {
public:

    typedef MultiTouchInputController Base;
	
	// Constructeurs / Destructeurs
    SimulatedMTInputController();
    virtual ~SimulatedMTInputController();
	
	// Getters
    virtual void registerEvent(QEvent*);

	// Setters
	inline void restartAll() { 
		cur_id_ = 0; 
		max_id_ = 0; 

        inputs_2D_.clear();
		id_inputs_.clear(); 
		deleteOldInputs(); 
	}

protected:
	// Data
	unsigned cur_id_;
	unsigned max_id_;

	// Functions
    bool checkNearestPoint(Point2 p);
};

typedef SimulatedMTInputController SimulatedMTIC;

} // namespace plugintools

} // namespace expressive


#endif // IC_MT_SIMULATED_H
