#include <pluginTools/inputcontroller/SimulatedMTInputController.h>

// QT
#include<QKeyEvent>
#include<QMouseEvent>
#include<QStateMachine>

namespace expressive
{

namespace plugintools
{

///////////////////////////////////////////////////
// Constructeurs/Destructeurs
///////////////////////////////////////////////////
SimulatedMTInputController::SimulatedMTInputController ()
	: Base(), 
	cur_id_(0), 
	max_id_(0) {}

SimulatedMTInputController::~SimulatedMTInputController() {}

///////////////////////////////////////////////////
// Getters
///////////////////////////////////////////////////
void SimulatedMTInputController::registerEvent (QEvent* event) {

    QInputController::registerEvent(event);
    // Event must be a QTouchEvent:
    QStateMachine::WrappedEvent* wrapped_event = dynamic_cast<QStateMachine::WrappedEvent*>(event);
    assert(wrapped_event);

    QKeyEvent* key_event = dynamic_cast<QKeyEvent*>(wrapped_event->event());
	if (key_event) { restartAll(); return; }

    QMouseEvent* mouse_event = dynamic_cast<QMouseEvent*>(wrapped_event->event());
	assert(mouse_event);

	// Type == Begin?
    double_touch_ = false;
    const QPoint &pos = mouse_event->pos();
    Point2 p = Point2(pos.x(), pos.y());

	if (mouse_event->buttons() == 0) { event->ignore(); return; }
	if (mouse_event->type   () == QEvent::MouseButtonPress) { 
        if (!checkNearestPoint(p)) {
            cur_id_ = max_id_++;
        } else if (max_id_ == 1) {
			// Double Click?
			std::chrono::time_point<std::chrono::system_clock> time = std::chrono::system_clock::now();
		
            int dt = (int) std::chrono::duration_cast<std::chrono::milliseconds>(time - this->time_).count();
			this->time_ = time;

			this->double_touch_ = dt<=600;
			if (double_touch_) { restart_ = true; }
		}
	}

	// Store Old Input
	storeOldInputs(max_id_);

	// Store Input:
	if (cur_id_ < inputs_2D_.size()) {
		inputs_2D_[cur_id_] = p;
	} else {
        inputs_2D_.push_back(p);
		id_inputs_.push_back(max_id_-1);
	}

	inputs_3D_.clear();
    inputs_3D_ = std::vector<Point>(inputs_2D_.size());

	// Two Handed Check?
	if (thumb_dist_ >= 0.f) { separateHands(); }

	// Thumb Registration?
	thumb_id_ = 0;

	old_restart_ = restart_;
	restart_     = false;
}

///////////////////////////////////////////////////
// Getters
///////////////////////////////////////////////////
bool SimulatedMTInputController::checkNearestPoint(Point2 p) {
	if (inputs_2D_.size() == 0) { return false; }

	// Check each store point distance:
	for (unsigned i=0; i<inputs_2D_.size(); i++) {
        Point2 v = inputs_2D_[i] - p;
        Scalar   d = v.norm(); //sqrt(v.x()*v.x() + v.y()*v.y());
		if (d < DISTANCE_MIN) {
			cur_id_ = i;
			return true;
		}
	}

	// Else:
	return false;
}

} // namespace plugintools

} // namespace expressive
