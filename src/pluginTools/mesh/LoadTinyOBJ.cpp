

#include <pluginTools/PluginToolsRequired.h>

#include <pluginTools/mesh/LoadTinyOBJ.h>
#include <pluginTools/mesh/tiny_obj_loader.h>

#include <iostream>



using namespace std;

namespace expressive  {

using namespace core;

namespace plugintools {

void LoadTinyOBJ(const std::string& file, shared_ptr<BasicTriMesh> mesh)
{

    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string err;

    bool ok = tinyobj::LoadObj(shapes,
                               materials,
                               err,
                               file.c_str());

    if(!ok)
    {
        cout << err << endl;
        return;
    }


    bool only_tri = true;
//    for(tinyobj::shape_t& s : shapes)
//    {
//        cout << "shape '" << s.name << "':" << endl;

//        cout << endl << "positions ( " << s.mesh.positions.size() << " - " << s.mesh.positions.size()/3 << " v3 ):" << endl;
//        for(float p : s.mesh.positions)
//            cout << p << endl;

//        cout << endl << "normals ( " << s.mesh.normals.size() << " - " << s.mesh.normals.size()/3 << " v3 ):" << endl;
//        for(float p : s.mesh.normals)
//            cout << p << endl;

//        cout << endl << "texture coordinates ( " << s.mesh.texcoords.size() << " - " << s.mesh.texcoords.size()/2 << " v2 ):" << endl;
//        for(float p : s.mesh.texcoords)
//            cout << p << endl;

//        cout << endl << "indices ( " << s.mesh.indices.size() << " - " << s.mesh.indices.size()/3 << " tri ):" << endl;
//        for(unsigned int p : s.mesh.indices)
//            cout << p << endl;

//        cout << endl << "vertices per face ( " << s.mesh.num_vertices.size() << " ):" << endl;
//        for(unsigned int p : s.mesh.num_vertices)
//        {
//            cout << p << endl;
//            if(p != 3)
//                only_tri = false;
//        }
//    }

//    shared_ptr<BasicTriMesh> mesh = make_shared<BasicTriMesh>();

    if(shapes.size() == 0)
    {
        cerr << "No shape defined inside input file !" << endl;
        return;
    }

    tinyobj::shape_t& s = shapes[0];

    bool has_normals = false;
    bool has_texcoord = false;
    if(s.mesh.normals.size() != 0)
        has_normals = true;
    if(s.mesh.texcoords.size() != 0)
        has_texcoord = true;

    if((s.mesh.normals.size() != s.mesh.positions.size() && has_normals) || (s.mesh.texcoords.size()/2 != s.mesh.positions.size()/3 && has_texcoord))
    {
        cerr << "Vert data count mismatching ! "
             << s.mesh.positions.size() << " positions components, "
             << s.mesh.normals.size()  << " normal components, "
             << s.mesh.texcoords.size()  << " textcoords components, "
             << endl;
        return;
    }

    if(s.mesh.indices.size()/3 != s.mesh.num_vertices.size())
    {
        cerr << "Face data count mismatching ! "
             << s.mesh.indices.size() << " indices components, "
             << s.mesh.num_vertices.size()  << " num vertices components, "
             << endl;
        return;
    }

    if(!only_tri)
    {
        cerr << "Faces are not all triangles !" << endl;
        return;
    }

    for(uint i=0; i<s.mesh.positions.size() / 3; i++)
    {
        if(!has_normals && !has_texcoord)
        {
            mesh->add_vertex(Point(s.mesh.positions[3*i], s.mesh.positions[3*i+1], s.mesh.positions[3*i+2]));
        }
        else if(has_normals && !has_texcoord)
        {
            mesh->add_point(Point(s.mesh.positions[3*i], s.mesh.positions[3*i+1], s.mesh.positions[3*i+2]),
                            Normal(s.mesh.normals[3*i], s.mesh.normals[3*i+1], s.mesh.normals[3*i+2]));
        }
        else if(!has_normals && has_texcoord)
        {
            mesh->add_point(Point(s.mesh.positions[3*i], s.mesh.positions[3*i+1], s.mesh.positions[3*i+2]),
                            Normal::Zero(),
                            Vector2(s.mesh.texcoords[2*i], s.mesh.texcoords[2*i+1]),
                            Color::One());
        }
        else
        {
            mesh->add_point(Point(s.mesh.positions[3*i], s.mesh.positions[3*i+1], s.mesh.positions[3*i+2]),
                            Normal(s.mesh.normals[3*i], s.mesh.normals[3*i+1], s.mesh.normals[3*i+2]),
                            Vector2(s.mesh.texcoords[2*i], s.mesh.texcoords[2*i+1]),
                            Color::One());
        }
    }

    for(uint i=0; i<s.mesh.indices.size() / 3; i++)
    {
        mesh->add_face(s.mesh.indices[3*i],
                       s.mesh.indices[3*i+1],
                       s.mesh.indices[3*i+2]);
    }

    if(!has_normals)
        mesh->ComputeNormals();

}

} // namespace plugintools
} // namespace expressive

