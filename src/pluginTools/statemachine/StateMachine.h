/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_STATEMACHINE_H_
#define EXPRESSIVE_STATEMACHINE_H_

// plugintools dependencies
#include <pluginTools/PluginToolsRequired.h>
#include <pluginTools/PluginsInterface.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
// Qt dependencies
#include <QDir>
#include <QStateMachine>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

namespace plugintools
{

class PLUGINTOOLS_API StateMachine : public QStateMachine
{
    Q_OBJECT
public:
    StateMachine();
    StateMachine(/*qttools::MainWindow *parent, */core::ExpressiveEngine *engine);

    virtual ~StateMachine() {}

    void LoadPlugins();

    inline QState *main_window_menu_parallel_state() const { return main_window_menu_parallel_state_; }

    inline QState *main_widget_parallel_state() const { return main_widget_parallel_state_; }

    static void setSaveFunction(void(*)(std::string));
    static void setLoadFunction(void(*)(std::string));

public slots:
    void sceneInitialized();

    void clearScene();
    void saveScene(bool static_saving);
    void loadFile(bool dynamic_loading);

protected:

    void initMenus();

    core::ExpressiveEngine *engine_;

//    QDir plugins_dir_;

//    std::set<ModePluginInterface*> mode_plugins_set_;

    QState *main_window_menu_parallel_state_;

    QState *main_widget_parallel_state_;

//    std::set<QState*> mode_states_set_;

    static void(*saveFunctionPtr_)(std::string);
    static void(*loadFunctionPtr_)(std::string);

};

} // namespace plugintools

} // namespace expressive

#endif // EXPRESSIVE_STATEMACHINE_H_
