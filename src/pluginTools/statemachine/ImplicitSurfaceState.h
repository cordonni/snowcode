/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef IMPLICITSURFACESTATE_H
#define IMPLICITSURFACESTATE_H
#include "pluginTools/gui/qml/QmlHandlerBase.h"


namespace expressive
{

namespace plugintools
{

class ImplicitSurfaceState : public QmlHandlerBase
{
    Q_OBJECT

    Q_PROPERTY(double polygonizer_resolution READ getPolygonizerResolution WRITE setPolygonizerResolution NOTIFY polygonizerResolutionChanged)
    Q_PROPERTY(double graph_edge_size READ getGraphEdgeSize WRITE setGraphEdgeSize NOTIFY graphEdgeSizeChanged)
    Q_PROPERTY(double graph_vertex_size READ getGraphVertexSize WRITE setGraphVertexSize NOTIFY graphVertexSizeChanged)

public:
    ImplicitSurfaceState();

public slots:
    /**
     * Forces to resend the data to qml since qml tried to get it when engine was not created yet.
     */
    void scene_initialized();

    // polygonizer stuff
    void setPolygonizerResolution(double res);
    double getPolygonizerResolution();

    // graph stuff
    void setGraphVertexSize(double size);
    void setGraphEdgeSize(double size);
    double getGraphVertexSize();
    double getGraphEdgeSize();
    void openBlobtreeView();
    void openDisplayScalarField();

signals:
    void polygonizerResolutionChanged();
    void graphEdgeSizeChanged();
    void graphVertexSizeChanged();

};

} // namespace plugintools

} // namespace expressive

#endif // IMPLICITSURFACESTATE_H
