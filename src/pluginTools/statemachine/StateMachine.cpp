#include <pluginTools/statemachine/StateMachine.h>

// core dependencies
#include "core/ExpressiveEngine.h"

#include "core/utils/ConfigLoader.h"
#include "core/geometry/BasicTriMesh.h"

#include "pluginTools/gui/qml/QmlTypeRegisterer.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
// qt dependencies
#include <QApplication>
#include <QPluginLoader>
#include <QSettings>
#include <QFileDialog>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

using namespace std;
using namespace expressive::core;
//using namespace expressive::ogretools;
//using namespace expressive::qttools;

namespace expressive
{

namespace plugintools
{

void(*StateMachine::saveFunctionPtr_)(std::string) = nullptr;
void(*StateMachine::loadFunctionPtr_)(std::string) = nullptr;

void StateMachine::setSaveFunction(void(*f)(std::string)) { saveFunctionPtr_ = f; }
void StateMachine::setLoadFunction(void(*f)(std::string)) { loadFunctionPtr_ = f; }

StateMachine::StateMachine() :
    QStateMachine()
{
}

StateMachine::StateMachine(/*qttools::MainWindow *parent, */ExpressiveEngine *engine) :
    QStateMachine(/*parent*/), /*main_window_(parent), */engine_(engine)

{
    QState * main_state = new QState(QState::ParallelStates);
    addState(main_state);

    qmlRegisterType<StateMachine>("Expressive", 1, 0, "StateMachine");
    setInitialState(main_state);
    start();

    main_window_menu_parallel_state_ = new QState(main_state);
    main_widget_parallel_state_ = new QState(main_state);
    main_widget_parallel_state_->setInitialState(new QState(main_widget_parallel_state_));

    LoadPlugins();

    initMenus();
}

void StateMachine::initMenus()
{
//    connect(main_window_, SIGNAL(clearScene()), this, SLOT(clearScene()));
//    connect(main_window_, SIGNAL(saveScene(bool)), this, SLOT(saveScene(bool)));
//    connect(main_window_, SIGNAL(loadFile(bool)), this, SLOT(loadFile(bool)));
}

void StateMachine::clearScene()
{
    this->engine_->scene_manager()->clear(false, true);
}

void StateMachine::saveScene(bool static_saving)
{
    if (static_saving) {
        QString directory = "../../resources/meshes/";
        QString saved_file = QFileDialog::getSaveFileName(nullptr, tr("Save Obj File"), directory, tr("Obj File (*.obj);;All Files (*)"));

        if (saved_file.isEmpty()) {
            return;
        }

        string f = saved_file.toStdString();
        if (f.substr(f.find_last_of(".") + 1) != "obj") {
            f += ".obj";
        }

        std::shared_ptr<SceneNode> n = nullptr;
        SceneManager::NodeIteratorPtr selection = this->engine_->scene_manager()->selected_nodes();
        if (selection->HasNext()) {
            n = selection->Next();
        } else {
            SceneManager::NodeIteratorPtr nodes = this->engine_->scene_manager()->root_nodes();
            if (nodes->HasNext()) {
                n = nodes->Next();
            }
        }

        if (n != nullptr) {
            n->SaveToObjFile(f);
        }

    } else {
        QString directory = Config::getStringParameter("default_examples_path").c_str();
        QString saved_file = QFileDialog::getSaveFileName(nullptr, tr("Save Xml File"), directory, tr("Xml File (*.xml);;All Files (*)"));

        if (saved_file.isEmpty()) {
            return;
        }

        string f = saved_file.toStdString();
        if (f.substr(f.find_last_of(".") + 1) != "xml") {
            f += ".xml";
        }

        if(saveFunctionPtr_)
            saveFunctionPtr_(f);
        else
            this->engine_->scene_manager()->SaveToXmlFile(f);
    }
}

void StateMachine::loadFile(bool dynamic_loading)
{
    QString directory = Config::getStringParameter("default_examples_path").c_str();
    QString loaded_file = QFileDialog::getOpenFileName(nullptr, tr("Load Object File"), directory, tr("Saved File (*.xml *.obj);;All Files (*)"));

    if (loaded_file.isEmpty()) {
        return;
    }

    string name = loaded_file.toStdString();
    string lowername = name;
    std::transform(lowername.begin(), lowername.end(), lowername.begin(), ::tolower);

    if(loadFunctionPtr_)
    {
        loadFunctionPtr_(name);
        return;
    }

    if (lowername.find(".xml") != string::npos) {
        std::shared_ptr<Serializable> res = engine_->resource_manager()->loadFromFile(name).cast<Serializable>();

        engine_->scene_manager()->AddNodeFromSerializable(res, dynamic_loading);
    } else if (lowername.find(".obj") != string::npos) {
        string node_name = lowername + "_" + to_string(engine_->scene_manager()->const_next_node_id());
        std::shared_ptr<SceneNode> res = make_shared<SceneNode>(engine_->scene_manager().get(), nullptr, node_name);
        std::shared_ptr<BasicTriMesh> mesh = make_shared<BasicTriMesh>();
        mesh->LoadFromObjFile(name);
        res->set_tri_mesh(mesh);
        engine_->scene_manager()->AddNode(res);
    } else if (lowername.find(".list") != string::npos) {
        string node_name = lowername + "_" + to_string(engine_->scene_manager()->const_next_node_id());
        std::shared_ptr<SceneNode> res = make_shared<SceneNode>(engine_->scene_manager().get(), nullptr, node_name);
        std::shared_ptr<BasicTriMesh> mesh = make_shared<BasicTriMesh>();
        mesh->LoadFromObjFile(name);
        res->set_tri_mesh(mesh);
        engine_->scene_manager()->AddNode(res);
    }
}

void StateMachine::sceneInitialized()
{
    // TODO OPENGL
//    emit polygonizerResolutionChanged();
//    emit graphEdgeSizeChanged();
//    emit graphVertexSizeChanged();
}

//std::shared_ptr<ImplicitSurfaceSceneNode> getImplicitSurface(std::shared_ptr<SceneManager> manager)
//{
    // TODO OPENGL
//    SceneManager::NodeIteratorPtr selection = manager->selected_nodes();
//    while (selection->HasNext()) {
//        std::shared_ptr<SceneNode> n = selection->Next();
//        std::shared_ptr<ImplicitSurfaceSceneNode> is = dynamic_pointer_cast<ImplicitSurfaceSceneNode>(n);
//        if (is != nullptr) {
//            return is;
//        }
//    }

//    selection = manager->nodes();
//    while (selection->HasNext()) {
//        std::shared_ptr<SceneNode> n = selection->Next();
//        std::shared_ptr<ImplicitSurfaceSceneNode> is = dynamic_pointer_cast<ImplicitSurfaceSceneNode>(n);
//        if (is != nullptr) {
//            return is;
//        }
//    }
//    return nullptr;
//}

//void StateMachine::setPolygonizerResolution(double res)
//{

//    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(engine_->scene_manager());
//    if (is != nullptr) {
//        std::shared_ptr<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler> updater = dynamic_pointer_cast<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler>(is->convol_object()->auto_updating_polygonizer());
//        if (updater != nullptr) {
//            updater->polygonizer().set_size(res);
//            updater->ResetSurface();
//        }
//    }
//}

//double StateMachine::getPolygonizerResolution()
//{
//    // TODO OPENGL

////    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(engine_->scene_manager());
////    if (is != nullptr) {
////        std::shared_ptr<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler> updater = dynamic_pointer_cast<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler>(is->convol_object()->auto_updating_polygonizer());
////        if (updater != nullptr) {
////            return updater->polygonizer().size();
////        }
////    }

////    return 0.0;
//}

//void StateMachine::setGraphVertexSize(double size)
//{
//    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(engine_->scene_manager());
//    if (is != nullptr) {
//        is->displayed_skeleton()->set_point_scale(size);
//    }
//}

//void StateMachine::setGraphEdgeSize(double size)
//{
//    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(engine_->scene_manager());
//    if (is != nullptr) {
//        is->displayed_skeleton()->set_geometry_scale(size);
//    }
//}

//double StateMachine::getGraphVertexSize()
//{

//    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(engine_->scene_manager());
//    if (is != nullptr) {
//        return is->displayed_skeleton()->point_scale();
//    }
//    return 0.0;
//}

//double StateMachine::getGraphEdgeSize()
//{
//    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(engine_->scene_manager());
//    if (is != nullptr) {
//        return is->displayed_skeleton()->geometry_scale();
//    }
//    return 0.0;
//}

//void StateMachine::openBlobtreeView()
//{
//    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(engine_->scene_manager());
//    if (is != nullptr) {
//        BlobtreeViewWidget *bt_view_widget = new BlobtreeViewWidget(is->convol_object());
//        bt_view_widget->show();
//    }
//}

//void StateMachine::openDisplayScalarField()
//{
//    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(ogre_globals_manager_->scene_manager());
//    if (is != nullptr) {
//        DisplayScalarFieldSliceWidget *display_scalar_field_slice_widget = new DisplayScalarFieldSliceWidget(
//                    ogre_globals_manager_->main_camera(),
//                    ogre_globals_manager_->ogre_scene_manager(),
//                    is);
//        display_scalar_field_slice_widget->show();
//    }
//}

void StateMachine::LoadPlugins() {
    // Plug-In Init
     QDir plugins_dir_ = QDir(QApplication::applicationDirPath());
#if defined(Q_OS_WIN)
#elif defined(Q_OS_MAC) // TODO voir ce qu'il faut faire sous mac et sur Ubuntu
    if (plugins_dir_.dirName() == "MacOS") {
        plugins_dir_.cdUp();
        plugins_dir_.cdUp();
        plugins_dir_.cdUp();
    }
#endif

    bool exists = plugins_dir_.cd("plugins/");
    if (!exists) {
        return;
    }

    std::set<ModePluginInterface*> mode_plugins_set_;
    std::set<QState*> mode_states_set_;

    //! [6]
    foreach (QString fileName, plugins_dir_.entryList(QDir::Files)) {
        QPluginLoader loader(plugins_dir_.absoluteFilePath(fileName));
        QObject *plugin = loader.instance();
        if (plugin != NULL) {
            ModePluginInterface *mode_plugin = qobject_cast<ModePluginInterface*>(plugin);
            if (mode_plugin != NULL) {

                // TODO add listener here so that they will add their menus etc themselves.
//                QAction* new_action_mode = main_window_->AddModeAction(mode_plugin->Name());
                mode_plugin->Init(*engine_,
                                  nullptr, //*engine_->main_ogre_widget(), /// <<<--- TODO OPENGL
//                                  mainVariables::panel_manager,
//                                  *main_window_,
                                  *engine_->command_manager(),
                                  *main_window_menu_parallel_state_,
                                  *main_widget_parallel_state_);

                QState* new_mode_state = mode_plugin->ModeMainState();
                mode_states_set_.insert(new_mode_state);

// TODO replace this !!!
//                for(auto it = mode_states_set_.begin(); it != mode_states_set_.end(); ++it) {
//                    (*it)->addTransition(new_action_mode,SIGNAL(triggered()), new_mode_state);
//                }

                // Get through plugins to add the transition necessary to get from the current state to all those of the other modes.
//                for(auto mod_plug_it = mode_plugins_set_.begin(); mod_plug_it != mode_plugins_set_.end(); ++mod_plug_it) {
//                    new_mode_state->addTransition(main_window_->GetModeActionFromName((*mod_plug_it)->Name()),SIGNAL(triggered()), (*mod_plug_it)->ModeMainState());
//                }
                mode_plugins_set_.insert(mode_plugin);

                // If the plugin has a panel, attach it to the application Docks
                if(mode_plugin->PanelWidget() != NULL) {
                    QString panel_string = mode_plugin->Name();
                    panel_string.append(QString("Panel"));
                    // TODO the panel manager !
//                    mainVariables::panel_manager.AddPanel(panel_string, mode_plugin->Name(), mode_plugin->PanelWidget(), Qt::RightDockWidgetArea);
                }
            }
        } else {
            ork::Logger::ERROR_LOGGER->logf("STATEMACHINE", loader.errorString().toStdString().c_str());
        }
    }
}

//static QmlTypeRegisterer::Type<StateMachine> StateMachineType("Expressive", 1, 0, "StateMachine");
QML_TYPE_REGISTERER(StateMachine, StateMachine, "Expressive", 1, 0)

} // namespace plugintools

} // namespace expressive
