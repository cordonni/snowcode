
#include <pluginTools/statemachine/states/OgreWidgetUpdateState.h>

namespace expressive
{

namespace plugintools
{

void OgreWidgetUpdateState::onEntry(QEvent *event)
{
    UNUSED(event);
    ogre_widget_->update();
    emit updated();
}

} // namespace plugintools

} // namespace expressive
