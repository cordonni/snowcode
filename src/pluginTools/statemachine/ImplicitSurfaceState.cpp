#include "pluginTools/statemachine/ImplicitSurfaceState.h"

// convol dependencies
#include <convol/tools/updating/AreaUpdatingBloomenthalPolygonizer/AreaUpdatingBloomenthalPolygonizerHandler.h>

// plugintools dependencies
#include "pluginTools/gui/qml/QmlTypeRegisterer.h"
#include <pluginTools/blobtreemanagement/BlobtreeViewWidget.h>
#include "pluginTools/convol/ImplicitSurfaceSceneNode.h"
#include <pluginTools/convolwidgets/tools/DisplayScalarFieldSliceWidget.h>

#include <memory>

using namespace expressive::core;
using namespace std;

namespace expressive
{

namespace plugintools
{

ImplicitSurfaceState::ImplicitSurfaceState() :
    QmlHandlerBase()
{
    qmlRegisterType<ImplicitSurfaceState>("Expressive", 1, 0, "ImplicitSurfaceHandler");
}

void ImplicitSurfaceState::scene_initialized()
{
    disconnect(m_engine, SIGNAL(sceneInitialized()), this, SLOT(scene_initialized()));
    emit polygonizerResolutionChanged();
    emit graphEdgeSizeChanged();
    emit graphVertexSizeChanged();
}

std::shared_ptr<ImplicitSurfaceSceneNode> getImplicitSurface(std::shared_ptr<SceneManager> manager)
{
    SceneManager::NodeIteratorPtr selection = manager->selected_nodes();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> n = selection->Next();
        std::shared_ptr<ImplicitSurfaceSceneNode> is = dynamic_pointer_cast<ImplicitSurfaceSceneNode>(n);
        if (is != nullptr) {
            return is;
        }
    }

    selection = manager->nodes();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> n = selection->Next();
        std::shared_ptr<ImplicitSurfaceSceneNode> is = dynamic_pointer_cast<ImplicitSurfaceSceneNode>(n);
        if (is != nullptr) {
            return is;
        }
    }
    return nullptr;
}

void ImplicitSurfaceState::setPolygonizerResolution(double res)
{
    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(m_engine->scene_manager());
    if (is != nullptr) {
        std::shared_ptr<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler> updater = dynamic_pointer_cast<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler>(is->convol_object()->auto_updating_polygonizer());
        if (updater != nullptr) {
            updater->polygonizer().set_size(res);
            updater->ResetSurface();
        }
    }
}

double ImplicitSurfaceState::getPolygonizerResolution()
{
    if (m_engine != nullptr) {
        std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(m_engine->scene_manager());
        if (is != nullptr) {
            std::shared_ptr<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler> updater = dynamic_pointer_cast<convol::tools::AreaUpdatingBloomenthalPolygonizerHandler>(is->convol_object()->auto_updating_polygonizer());
            if (updater != nullptr) {
                return updater->polygonizer().size();
            }
        }
    }

    return 0.0;
}

void ImplicitSurfaceState::setGraphVertexSize(double size)
{
    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(m_engine->scene_manager());
    if (is != nullptr) {
        is->displayed_skeleton()->set_point_scale(size);
    }
}

void ImplicitSurfaceState::setGraphEdgeSize(double size)
{
    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(m_engine->scene_manager());
    if (is != nullptr) {
        is->displayed_skeleton()->set_geometry_scale(size);
    }
}

double ImplicitSurfaceState::getGraphVertexSize()
{
    if (m_engine != nullptr) {
        std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(m_engine->scene_manager());
        if (is != nullptr) {
            return is->displayed_skeleton()->point_scale();
        }
    }
    return 0.0;
}

double ImplicitSurfaceState::getGraphEdgeSize()
{
    if (m_engine != nullptr) {
        std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(m_engine->scene_manager());
        if (is != nullptr) {
            return is->displayed_skeleton()->geometry_scale();
        }
    }
    return 0.0;
}

void ImplicitSurfaceState::openBlobtreeView()
{
    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(m_engine->scene_manager());
    if (is != nullptr) {
        BlobtreeViewWidget *bt_view_widget = new BlobtreeViewWidget(is->convol_object());
        bt_view_widget->show();
    }
}

void ImplicitSurfaceState::openDisplayScalarField()
{
    std::shared_ptr<ImplicitSurfaceSceneNode> is = getImplicitSurface(m_engine->scene_manager());
    if (is != nullptr) {
        DisplayScalarFieldSliceWidget *display_scalar_field_slice_widget = new DisplayScalarFieldSliceWidget(
                    m_engine->camera_manager().get(),
                    m_engine->scene_manager().get(),
                    is);
        display_scalar_field_slice_widget->show();
    }
}

void ImplicitStateRegisterFunction()
{
    qmlRegisterType<ImplicitSurfaceState>("Expressive", 1, 0, "ImplicitSurfaceHandler");
}

//static QmlTypeRegisterer::TypeRegistererCallback StateMachineType(ImplicitStateRegisterFunction);
QML_TYPE_REGISTERER_CALLBACK(ImplicitStateRegisterFunction);

} // namespace plugintools

} // namespace expressive
