#include "pluginTools/convol/ImplicitSurfaceSceneNode.h"

//
#include "core/scenegraph/SceneManager.h"
#include "core/scenegraph/SceneCommands.h"
#include "core/utils/ConfigLoader.h"

#include "convol/tools/updating/AreaUpdatingBloomenthalPolygonizer/AreaUpdatingBloomenthalPolygonizerHandler.h"
#include "convol/tools/updating/AreaUpdatingDualContouring/AreaUpdatingDualContouringHandler.h"

#include "core/scenegraph/SceneNodeFactory.h"

#include "core/scenegraph/OctreeNode.h"
#include "pluginTools/scenemanipulator/GraphSceneNodeManipulator.h"

using namespace std;
using namespace expressive::core;
using namespace expressive::convol;

//TODO:@todo : tmp for test ...
//#define USE_DUAL_CONTOURING

namespace expressive {
namespace plugintools{

//ImplicitSurfaceSceneNode::ImplicitSurfaceSceneNode()
//{
//}

ImplicitSurfaceSceneNode::ImplicitSurfaceSceneNode(core::SceneManager* scene_manager,
                         SceneNode *parent,
                         const string &name,
//                  CameraMan * camera_man,
//                             bool update,
                         std::shared_ptr<convol::ConvolObject> convol_obj,
                         const Vector& scale,
                         bool dynamic)
    : SceneNode(scene_manager, parent, name, StaticName(), scale),
      dynamic_(dynamic),
      convol_object_(convol_obj)
{
    Init(scene_manager->command_manager()->action_mutex());
}

ImplicitSurfaceSceneNode::ImplicitSurfaceSceneNode(core::SceneManager* scene_manager,
                         SceneNode *parent,
                         const string &name,
//                  CameraMan * camera_man,
//                             bool update,
                         std::shared_ptr<void> convol_obj ,
                         const Vector& scale,
                         bool dynamic)
    : SceneNode(scene_manager, parent, name, StaticName(), scale),
      dynamic_(dynamic),
      convol_object_(static_pointer_cast<ConvolObject>(convol_obj))
{
    Init(scene_manager->command_manager()->action_mutex());
}

void ImplicitSurfaceSceneNode::Init(std::mutex& action_mutex)
{
    node_->removeFlag("object");
    node_->addFlag("transparent");
//    node_->addModule("material", owner_->resource_manager()->loadResource("hoverablePlastic").cast<ork::Module>());
    octree_ = nullptr;
    this->set_tri_mesh(convol_object_->tri_mesh()->clone());

    convol_object_->AddListener(this);
    AABBox b = convol_object_->skeleton()->axis_bounding_box();
    Scalar size = 0;
    for (unsigned int i = 0; i < Point::RowsAtCompileTime; ++i) {
        size = max(size, b.width(i));
    }
    std::shared_ptr<tools::AreaUpdatingPolygonizerHandler> a_u_pol = nullptr;
    if (Config::getBoolParameter("use_dual_contouring")) {
        a_u_pol = std::make_shared<expressive::convol::tools::AreaUpdatingDualContouringHandler>(convol_object_.get(), action_mutex, Config::getIntParameter("polygonizer_max_depth"));
    } else {
        a_u_pol = std::make_shared<expressive::convol::tools::AreaUpdatingBloomenthalPolygonizerHandler>(convol_object_.get(), action_mutex, Config::getFloatParameter("polygonizer_default_resolution"), Config::getFloatParameter("polygonizer_default_epsilon"));
//        auto a_u_pol = std::make_shared<expressive::convol::tools::AreaUpdatingBloomenthalPolygonizerHandler>(convol_object_.get(), action_mutex, 0.01 * size, -1.0);
    }
    convol_object_->set_area_updating_polygonizer(a_u_pol);

    //TODO:@todo : this is really important (calling directly the content of UpdateMesh in update_required would cause
    // the applicationt to crash ... (problem with Ork/OpenGl called from a different thread ...)
    connect(this, &ImplicitSurfaceSceneNode::update_required, this, &ImplicitSurfaceSceneNode::CopyUpdatedMesh, Qt::QueuedConnection);

    a_u_pol->ResetSurface();

//    displayed_skeleton_ = nullptr;
    AddSkeletonToScene();
}

void ImplicitSurfaceSceneNode::DisplaySkeleton(bool visible)
{
    displayed_skeleton_->set_visible(visible);
}

void ImplicitSurfaceSceneNode::DisplayOctree(bool shown)
{
    if (shown) {
        convol_object_->tri_mesh()->createOctree();
        octree_ = make_shared<OctreeNode>(owner_, this, name_ + "_octree", convol_object_->tri_mesh()->octree());
        AddChild(octree_);
    } else {
        RemoveChild(octree_);
        octree_ = nullptr;
    }
}

std::shared_ptr<OctreeNode> ImplicitSurfaceSceneNode::octree() const
{
    return octree_;
}

//void ImplicitSurfaceSceneNode::Refine(Scalar factor)
//{
//    convol_object_->auto_updating_polygonizer()->
//}

ImplicitSurfaceSceneNode::~ImplicitSurfaceSceneNode()
{
    convol_object_->RemoveListener(this);

    disconnect(this, &ImplicitSurfaceSceneNode::update_required, this, &ImplicitSurfaceSceneNode::CopyUpdatedMesh);
}

/////////////////
/// Listening ///
/////////////////

std::shared_ptr<convol::ConvolObject> ImplicitSurfaceSceneNode::convol_object() const
{
    return convol_object_;
}

void ImplicitSurfaceSceneNode::updated(convol::ConvolObject * /*t*/)
{
    emit update_required();
//    UpdateMesh();
}

TiXmlElement* ImplicitSurfaceSceneNode::ToXml(const char *name ) const
{
    return convol_object_->ToXml(name);
}

void ImplicitSurfaceSceneNode::AddSkeletonToScene()
{
    displayed_skeleton_ = make_shared<WeightedGraphSceneNode>(owner_,
                                                              this,
                                                              name_ + "_skeleton",
                                                              convol_object_->nonconst_skeleton(),
                                                              Config::getFloatParameter("graph_default_vertex_size"),
                                                              Config::getFloatParameter("graph_default_edge_size"));
    AddChild(displayed_skeleton_, false);

    if (dynamic_) {
        std::shared_ptr<Camera> cam = owner_->current_camera();


        std::shared_ptr<GraphSceneNodeManipulator> graph_manipulator = make_shared<GraphSceneNodeManipulator>(owner_, cam.get());
        displayed_skeleton_->set_manipulator(graph_manipulator);
        this->set_manipulator_proxy(displayed_skeleton_);
    }
}

void ImplicitSurfaceSceneNode::ImplicitSurfaceSceneNode::CopyUpdatedMesh ()
{
    {
        std::unique_lock<std::mutex> mlock(*(convol_object_->mutex_data_retrieval()));
        std::lock_guard<std::mutex> mlock2(this->tri_mesh_->get_buffers_mutex());
        this->tri_mesh_->copyDataFrom(convol_object_->tri_mesh().get());
        this->node_->setLocalBounds(this->tri_mesh_->bounding_box().toBox3());
        convol_object_->cond_data_retrieval()->notify_one();
        CommandManager::GetSingleton()->run(std::make_shared<UpdateMeshCommand>(owner_, id()));
    }
    owner_->NotifyUpdate();
}

void ImplicitSurfaceSceneNode::TransferSceneDeformationToModel()
{
   expressive::Matrix4 m = GetWorldToLocal();
   this->convol_object()->skeleton()->ApplyDeformation(m);
   this->convol_object()->skeleton()->NotifyUpdate();
}

static SceneNodeFactory::DynamicableType<ConvolObject, ImplicitSurfaceSceneNode> ImplicitSurfaceSceneNodeDynamicType;

} // namespace pluginTools

}  // end of namespace expressive
