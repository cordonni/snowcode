/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef IMPLICITSURFACESCENENODE_H
#define IMPLICITSURFACESCENENODE_H

// core dependencies
#include "core/utils/ListenerT.h"
#include "core/scenegraph/SceneNode.h"
#include "core/scenegraph/graph/GraphSceneNode.h"

// convol dependencies
#include "convol/ConvolObject.h"

// plugintools
#include "pluginTools/PluginToolsRequired.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
// Qt dependencies
#include <QObject> // only for connect
#ifdef _MSC_VER
#pragma warning( pop )
#endif


namespace expressive{

namespace core
{
    class OctreeNode;
}

namespace plugintools{

class PLUGINTOOLS_API ImplicitSurfaceSceneNode
        :   public QObject,
            public core::SceneNode,
            public core::ListenerT<convol::ConvolObject>
{
    Q_OBJECT
public:
    
    EXPRESSIVE_MACRO_NAME("ImplicitSurfaceSceneNode")

    ImplicitSurfaceSceneNode(core::SceneManager* scene_manager,
                             SceneNode *parent,
                             const std::string &name,
                             std::shared_ptr<convol::ConvolObject> convol_obj ,
                             const Vector &scale = Vector::Ones(),
                             bool dynamic = true);

    ImplicitSurfaceSceneNode(core::SceneManager* scene_manager,
                             SceneNode *parent,
                             const std::string &name,
                             std::shared_ptr<void> convol_obj ,
                             const Vector &scale = Vector::Ones(),
                             bool dynamic = true);

    virtual ~ImplicitSurfaceSceneNode();

    /////////////////
    /// Listening ///
    /////////////////

    virtual void updated(convol::ConvolObject * /*t*/);

    virtual TiXmlElement* ToXml(const char *name = nullptr) const;

    void DisplaySkeleton(bool shown = true);
    void DisplayOctree(bool shown = true);
    std::shared_ptr<core::OctreeNode> octree() const;

    inline std::shared_ptr<core::WeightedGraphSceneNode> displayed_skeleton() const { return displayed_skeleton_; }

//    virtual void Refine(Scalar factor);

    std::shared_ptr<convol::ConvolObject> convol_object() const;

    virtual void TransferSceneDeformationToModel();

    virtual void ToggleVisible(bool applyToChildren) {
        SceneNode::ToggleVisible(applyToChildren && displayed_skeleton_ != nullptr && displayed_skeleton_->visible());
    }

    virtual void ToggleWireframe(bool applyToChildren) {
        SceneNode::ToggleWireframe(applyToChildren && displayed_skeleton_ != nullptr && displayed_skeleton_->visible());
    }

public slots :
    void CopyUpdatedMesh ();

signals:
    void update_required();

    /////////////////
    /// Accessors ///
    /////////////////

    /////////////////
    /// Attributs ///
    /////////////////
protected:

    virtual void Init(std::mutex& action_mutex);

    virtual void AddSkeletonToScene();

    bool dynamic_;

    std::mutex mutex_; //TODO:@todo : nothing to do here ... except if command manager tries to lock it before performing action on the convol_object_ !!!

    std::shared_ptr<convol::ConvolObject> convol_object_;

    std::shared_ptr<core::WeightedGraphSceneNode> displayed_skeleton_;

    std::shared_ptr<core::OctreeNode> octree_;
};

} // namespace plugintools

} // end of namespace expressive

#endif // IMPLICITSURFACESCENENODE_H
