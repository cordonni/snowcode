/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef ComputePrimitiveDirection_H
#define ComputePrimitiveDirection_H

#include <pluginTools/PluginToolsRequired.h>

#include <convol/skeleton/Skeleton.h>

// Dependencies: Boost
#include <boost/graph/depth_first_search.hpp>

namespace expressive{

namespace plugintools{

/**
 * @brief The ComputePrimitiveDirection class provide a way to compute automatically an orientation for
 * the graph needed by the generation of geometric details through gabor noise.
 *
 * TODO:@todo : for now it only work for segment primitives in the graph, should be adapted to provide good result
 * at least for subdivision curves, triangles and points primitives.
 *
 * TODO:@todo : for now the algorithm only take into account the weight for initializing the "diffusion process"
 * in fact, they should also be taken into account to provide a better propagation when loops are present in
 * the skeleton
 *
 * TODO:@todo : if we have hierarchical information, they should be used to improve the generated direction
 * TODO:@todo : we should take into account if different connected part are close to one another
 */
class PLUGINTOOLS_API ComputePrimitiveDirection //: public boost::default_dfs_visitor
{
public:

    ComputePrimitiveDirection() {}

    void Process(convol::Skeleton* graph);

    virtual ~ComputePrimitiveDirection(){}


    class MaxWeightVisitor : public boost::default_dfs_visitor
    {
    public:
        MaxWeightVisitor(std::vector< std::pair<core::VertexId, Scalar> > &tab_vw_max)
            : boost::default_dfs_visitor(), tab_vw_max_(tab_vw_max) {}
        MaxWeightVisitor(const MaxWeightVisitor &other)
            : boost::default_dfs_visitor(), tab_vw_max_(other.tab_vw_max_) {}
        ~MaxWeightVisitor() {}

        void start_vertex(core::VertexId s, const convol::Skeleton& g);
        void discover_vertex(core::VertexId v, const convol::Skeleton& g);

    protected :
        std::vector< std::pair<core::VertexId, Scalar> > &tab_vw_max_;
    };

    class InitDirectionVisitor : public boost::default_dfs_visitor
    {
    public:
        InitDirectionVisitor(std::map<core::VertexId, Vector> &dir_tab,
                             std::map<core::EdgeId, Vector> &orientation_tab)
            : boost::default_dfs_visitor(), dir_tab_(dir_tab), orientation_tab_(orientation_tab) {}
        InitDirectionVisitor(const InitDirectionVisitor &other)
            : boost::default_dfs_visitor(), dir_tab_(other.dir_tab_), orientation_tab_(other.orientation_tab_) {}

        void start_vertex(core::VertexId s, const convol::Skeleton& g);
        void discover_vertex(core::VertexId s, const convol::Skeleton& g);
        void tree_edge(core::EdgeId e, const convol::Skeleton& g);

    protected :
        std::map<core::VertexId, Vector> &dir_tab_;
        std::map<core::EdgeId, Vector> &orientation_tab_;

    };

    /////////////////
    /// Accessors ///
    /////////////////

    /////////////////
    /// Attributs ///
    /////////////////
protected:

};

} // namespace plugintools
} // end of namespace expressive

#endif // ComputePrimitiveDirection_H
