#include <pluginTools/convol/ComputePrimitiveDirection.h>


//TODO:@todo : the code should be adapted to be more generic (for now only work with segment primitives)
#include <convol/primitives/NodeSegmentSFieldT.h>

namespace expressive {
namespace plugintools{

void ComputePrimitiveDirection::MaxWeightVisitor::start_vertex(core::VertexId v, const convol::Skeleton& g)
{
    // new connected part found
    tab_vw_max_.push_back( std::make_pair(v, g.Get(v)->pos().weight()));
}

void ComputePrimitiveDirection::MaxWeightVisitor::discover_vertex(core::VertexId v, const convol::Skeleton& g)
{
    Scalar vertex_weight = g.Get(v)->pos().weight();
    if(vertex_weight>tab_vw_max_.back().second) {
        tab_vw_max_.back().first = v;
        tab_vw_max_.back().second = vertex_weight;
    }
}


void ComputePrimitiveDirection::InitDirectionVisitor::start_vertex(core::VertexId v, const convol::Skeleton& g)
{
    // Find edge with minimal weight variation

    Scalar delta_w_min = 1000000.0;
    const core::WeightedPoint &center = g.Get(v)->pos();
    auto adj_v = g.Get(v)->GetAdjacentVertices();
    core::VertexId v_id = v;
    while (adj_v->HasNext()) {
        core::WeightedVertexPtr n = adj_v->Next();
        const core::WeightedPoint &wp = n->pos();

        Scalar delta_w = (center.weight() - wp.weight()) / (center-wp).norm();
        if(delta_w < delta_w_min) {
            delta_w_min = delta_w;
            v_id = n->id();
        }
    }

    // Find corresponding edge and tag it with an orientation !!!

    auto iter_edge = g.Get(v)->GetEdges();
    while(iter_edge->HasNext()) {

        auto e = iter_edge->Next();

        if(e->GetStartId() == v_id || e->GetEndId() == v_id) {
            // the initial orientation is arbitrary until we take into account
            // scalar field and weighted direction of other connected part
            Vector init_dir = e->GetStart()->pos() - e->GetEnd()->pos();

            // test if this connected part is in the area of influence of another part (already treated)
            const std::vector<ESFScalarProperties> s_prop;
            std::vector<Scalar> s_prop_res;
            std::vector<ESFVectorProperties> v_prop = {ExpressiveTraits::E_Direction};
            std::vector<Vector> v_prop_res;// = { Vector::Zero() };
            v_prop_res.resize(1); v_prop_res[0] = Vector::Zero();
            Scalar val = 0.0; Vector grad = Vector::Zero();
            g.blobtree_root().EvalValueAndGradAndProperties(g.Get(v)->pos(), 1.0e-6, s_prop, v_prop, val,grad, s_prop_res, v_prop_res);

            Vector weighted_dir = v_prop_res[0];
            if(val > 1.0e-1 && weighted_dir.norm() > 1.0e-1 ) {
                if(weighted_dir.dot(init_dir)<0.0) init_dir *= -1.0;
            }

            orientation_tab_.insert(std::make_pair(e->id(),init_dir));

            std::shared_ptr<convol::AbstractNodeSegmentSField> node_seg
                    = std::dynamic_pointer_cast<convol::AbstractNodeSegmentSField> (e->nonconst_edgeptr());
            if(node_seg != nullptr) {
                node_seg->properties().SetVectorProperty(expressive::ExpressiveTraits::E_Direction,init_dir);
            } else assert(false);
            break;
        }
    }
}



void ComputePrimitiveDirection::InitDirectionVisitor::discover_vertex(core::VertexId s, const convol::Skeleton& g)
{
    bool source_edge_found = false;

    Vector src_dir = Vector::Zero();
    std::list< std::pair<core::VertexId, Vector> > l_v_dir;

    //TODO:@todo : the choice of the main dir should be improved

    Vector dir;
    bool orientation = false;
    auto iter_edge = g.Get(s)->GetEdges();
    while(iter_edge->HasNext())
    {
        auto e = iter_edge->Next();

        // compute direction from edge vertices
        dir = e->GetStart()->pos() - e->GetEnd()->pos();
        if(e->GetStartId()==s) dir*=-1.0;
        dir.normalize();

        auto src_edge_dir = orientation_tab_.find(e->id());
        if(src_edge_dir!=orientation_tab_.end()) {
            assert(!source_edge_found); //there should be only one src edge

            src_dir = -dir;

            orientation = (src_dir.dot(src_edge_dir->second) > 0.0);

            source_edge_found = true;
        } else {
            l_v_dir.push_back(
                        std::make_pair( (e->GetStartId()==s) ?  e->GetEndId() : e->GetStartId(),
                                        dir ));
        }
    }

    if(!source_edge_found) {
        assert(false);
        // this is the first vertex of a connected part of the graph
        // should not happen with the new initialization ...
    } else {
        // Compute maximal angle
        Scalar max_dot = -2.0;
        Vector main_dir = Vector::Zero();
        Scalar tmp_dot;
        for(auto &pair : l_v_dir) {
            tmp_dot = src_dir.dot(pair.second);
            if(tmp_dot>max_dot) {
                max_dot = tmp_dot;
                main_dir = src_dir+pair.second;
                if (!orientation) main_dir *= -1.0;
                main_dir.normalize();
            }
        }

        // inserted direction should create a positive scalar product with the one of the incomming edge
        this->dir_tab_.insert( std::make_pair(s, main_dir ));
    }
}

void ComputePrimitiveDirection::InitDirectionVisitor::tree_edge(core::EdgeId e, const convol::Skeleton& g)
{
    core::VertexId source_id = boost::source(e, g);
    core::VertexId target_id = boost::target(e, g);

    Vector main_dir = this->dir_tab_[source_id];
    Vector dir_edge = g.Get(target_id)->pos() - g.Get(source_id)->pos();

    if(main_dir.dot(dir_edge) < 0.0)
        dir_edge *= -1.0; // if acute angle then reverse edge direction

    if(orientation_tab_.find(e) == orientation_tab_.end()) { //otherwise first branch treated in the current connected part
        std::shared_ptr<convol::AbstractNodeSegmentSField> node_seg = std::dynamic_pointer_cast<convol::AbstractNodeSegmentSField> (g.Get(e)->nonconst_edgeptr());
        if(node_seg != nullptr) {
            node_seg->properties().SetVectorProperty(expressive::ExpressiveTraits::E_Direction,dir_edge);
            orientation_tab_.insert(std::make_pair(e,dir_edge));
        } else {
            assert(false);
        }
    }
}

void ComputePrimitiveDirection::Process(convol::Skeleton* graph)
{
    std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
    start_time = std::chrono::high_resolution_clock::now();

    // Initialize all primitive direction attribute to zero (in order to handle correctly unconnected branches)

    auto iter_edges = graph->GetEdges();
    while (iter_edges->HasNext()) {
        std::shared_ptr<convol::AbstractNodeSegmentSField> node_seg
                = std::dynamic_pointer_cast<convol::AbstractNodeSegmentSField> (iter_edges->Next()->nonconst_edgeptr());
        if(node_seg != nullptr) {
            node_seg->properties().SetVectorProperty(expressive::ExpressiveTraits::E_Direction,Vector::Zero());
        }
    }

    // Compute vertex corresponding to weight maxima for each connected part of the graph

    convol::Skeleton::VertexColorPropertyMap color_map = graph->vertex_color_property_map();
    std::vector< std::pair<core::VertexId, Scalar> > tab_vw_max_;
    MaxWeightVisitor vis(tab_vw_max_);
    boost::depth_first_search(*graph, boost::visitor(vis).color_map(color_map).vertex_index_map(graph->vertex_index_property_map()));

    //TODO:@todo : sort tab_vw_max_ by weight...

    // Initialize the direction for the primitives

    auto vertices = graph->GetVertices();
    while (vertices->HasNext()) {
        auto v = vertices->NextId();
        boost::put(color_map, v, boost::white_color);
    }

    std::map<core::VertexId, Vector> dir_tab;
    std::map<core::EdgeId, Vector> orientation_tab_;

    //iterate over connected part
    for (auto & pair_id_w : tab_vw_max_) {
        core::VertexId id = pair_id_w.first;

        if (boost::get(color_map, id) != boost::black_color) {
            InitDirectionVisitor vis(dir_tab, orientation_tab_);
            boost::depth_first_visit(*graph, id, vis, color_map);
        }
    }


    end_time =  std::chrono::high_resolution_clock::now();
    std::cout<< "Computation of direction: "
             << std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000. << std::endl;

    // find the vertex of maximal weight per connected part
//    depth_first_search(graph,

    // sort them to treat global maximal weight first

    // process each connected part
}

} // namespace pluginTools

}  // end of namespace expressive
