#include <pluginTools/convolwidgets/BasicOperatorWidget.h>

#include <pluginTools/convolwidgets/FunctorWidgetFactory.h>

namespace expressive {

static FunctorWidgetFactory::Type<BlendSumWidget> BlendSumWidgetType;
static FunctorWidgetFactory::Type<BlendMaxWidget> BlendMaxWidgetType;

} //end of namespace expressive
