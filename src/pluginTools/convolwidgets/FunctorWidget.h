/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: FunctorWidget.h

   Language: C++

   License: Convol Licence

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Project: Convol-ConvolLab

   Description: Header file for FunctorWidget.
                This is the widget that contains the UI about Functor widget.

   Platform Dependencies: None
*/

#pragma once
#ifndef FUNCTOR_WIDGET_H
#define FUNCTOR_WIDGET_H

#include <core/functor/Functor.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QWidget>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {

class AbstractFunctorWidget : public QWidget
{
    Q_OBJECT

public:
    AbstractFunctorWidget(QWidget* parent = 0)
        : QWidget(parent) { }
    ~AbstractFunctorWidget() {}

    virtual std::string Name() const  = 0;

    virtual TiXmlElement* ToXml() const = 0;

    virtual void UpdateWidgetFromNode(const core::Functor* functor) = 0;
};

} //end of namespace expressive

/*
    virtual const FunctorTypeTree& KernelType() const = 0;

    // Create a new kernel and return the functor pointer to it.
    virtual Functor* CreateFunctorKernel() const = 0;

    // virtual function that takes a Functor as parameter and update the widget accordingly.
    virtual void UpdateFromKernel(const Functor& ker) = 0;
    // virtual function that takes a type and update the widget type.
    // this is especially usefull to swuitch from Numerical to non numerical, or for complex kernels.
    virtual void UpdateType(const FunctorTypeTree& ker_type) = 0;

    // Enable analytical. By default when analytical calculation is enabled it is prefered
    virtual void EnableAnalytical()
    {
        assert(false);
    }

signals:
    // Signal emitted any time the kernel parameters have changed
    void parametersChanged(); 

protected slots: 
    void EmitSignalParametersChanged()
    {
        emit parametersChanged();
    }
*/

#endif // FUNCTOR_WIDGET_H
