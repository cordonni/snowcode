#include <pluginTools/convolwidgets/tools/DisplayScalarFieldSliceWidget.h>

#include "ork/render/Texture2D.h"

#include "core/ExpressiveEngine.h"

#include "core/scenegraph/Overlay.h"

// Convol dependencies
#include <convol/ScalarField.h>
#include <convol/blobtreenode/BlobtreeRoot.h>

#include <pluginTools/convolwidgets/tools/PictureScalarFieldManualObjectFactory.h>

//// Convol-LabLib Dependencies
//#include<Convol-ConvolLabLib/include/Object/Object.h>
//#include<Convol-ConvolLabLib/include/Object/ObjectsPool.h>

using namespace expressive::core;
//using namespace ork;

namespace expressive {

namespace plugintools
{

//////////////////////////////////////
// Constructeurs / Destructeurs
DisplayScalarFieldSliceWidget::DisplayScalarFieldSliceWidget(
        Camera* camera,
        SceneManager* scene_manager,
        std::shared_ptr<plugintools::ImplicitSurfaceSceneNode> implicit_surface_scene_node,
        QWidget* parent)
    : QWidget(parent),
      slice_(nullptr),
      camera_(camera),
      scene_manager_(scene_manager),
      node_(nullptr),
      implicit_surface_scene_node_(implicit_surface_scene_node)
{
    setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    // Widget Connection
    connect(dsb_cam_dist, SIGNAL(valueChanged(double)), this, SLOT(CreateSlice()));
    connect(Compute_Dist, SIGNAL(clicked           ()), this, SLOT(ComputeDist()));
    connect(Create_Slice, SIGNAL(clicked           ()), this, SLOT(CreateSlice()));
    connect(Delete_Slice, SIGNAL(clicked           ()), this, SLOT(DeleteSlice()));
}

DisplayScalarFieldSliceWidget::~DisplayScalarFieldSliceWidget() {
    DeleteSlice();
}

void DisplayScalarFieldSliceWidget::ComputeDist()
{
    dsb_cam_dist->setValue((camera_->GetPointInWorld() - camera_->target()).norm());
}

void DisplayScalarFieldSliceWidget::CreateSlice() {

    convol::ImplicitSurface* surface_copy;
    {
///TODO:@todo : it require a mutex to ensure data safety during the cloning
//    std::lock_guard<std::mutex> mlock(action_mutex_);
        implicit_surface_scene_node_->convol_object()->implicit_surface()->PrepareForEval();

        // Create a clone of the implicit surface to ensure thread safety
        surface_copy = implicit_surface_scene_node_->convol_object()->implicit_surface()->CloneForEval();
    }
	
    // Create PictureScalarField
    Vector origin = camera_->GetPointInWorld() + dsb_cam_dist->value()*camera_->GetRealDirection();
    Vector up     = camera_->GetRealUp      ();
    Vector right  = camera_->GetRealRight   ();

    // Changing Frame
    Scalar scale = 1.0/ implicit_surface_scene_node_->GetScale()[0];

    DeleteSlice();

    // PictureScalarField Setup
    slice_ = new convol::PictureScalarFieldSlice(
                surface_copy->nonconst_blobtree_root().get(),
                origin, up, right,
                scale*dsb_length->value(), scale*dsb_length->value(),
                sb_res->value(), sb_res->value(),
                dsb_sca_fac->value(), dsb_wav_beg->value());
	
    slice_->add_iso(dsb_iso1->value());
    slice_->add_iso(dsb_iso2->value());
    slice_->add_iso(dsb_iso3->value());

    slice_->update_field_value  ();
    slice_->update_picture_field();

    slice_->set_origin(origin);
    slice_->set_dir1  (up    );
    slice_->set_dir2  (right );
    slice_->set_length(dsb_length->value(), dsb_length->value());

    // creating a QImage and save it
    QImage image(slice_->res_dir1(),slice_->res_dir2(),QImage::Format_ARGB32);
    //init all pixel one by one ...
    QColor color;
    for(unsigned int i = 0; i<slice_->res_dir1(); ++i) {
        for(unsigned int j = 0; j<slice_->res_dir1(); ++j) {
            unsigned int index = slice_->res_dir1()*i+j;
            color.setRgbF(
                        (float)((uint8) (slice_->tab_picture_value(index,1))) / 255.0,
                        (float)((uint8) (slice_->tab_picture_value(index,2))) / 255.0,
                        (float)((uint8) (slice_->tab_picture_value(index,3))) / 255.0
                        );
            image.setPixel(i,j,color.rgba());
//            image.setPixel(i,j,slice_->tab_picture_value(slice_->res_dir1()*i+j));

        }
    }
    image.save("/tmp/TestSliceImageSVG.png");
    ork::ptr<ork::Texture2D> tex = new ork::Texture2D(slice_->res_dir1(), slice_->res_dir2(), ork::RGBA8, ork::RGBA, ork::UNSIGNED_BYTE, ork::Texture::Parameters(), ork::Buffer::Parameters(), ork::CPUBuffer(slice_->tab_picture_field()));
    node_ = std::make_shared<Overlay>(scene_manager_, "SliceImage", Overlay::O_FOREGROUND, expressive::core::ExpressiveEngine::GetSingleton()->resource_manager()->loadResource("objectMethod").cast<ork::TaskFactory>(), tex);
    node_->SetModule(expressive::core::ExpressiveEngine::GetSingleton()->resource_manager()->loadResource("texturedPlastic").cast<ork::Module>());

    std::shared_ptr<BasicTriMesh> mesh = std::make_shared<BasicTriMesh>();

    Point p0 = slice_->origin() - 0.5*(slice_->length_dir1()*slice_->dir1() + slice_->length_dir2()*slice_->dir2());
    Point p1 = p0 + slice_->length_dir1()*slice_->dir1();
    Point p2 = p0 + slice_->length_dir2()*slice_->dir2();
    Point p3 = p2 + slice_->length_dir1()*slice_->dir1();

    float margin_dir1 = 1.0f/(((float) slice_->res_dir1())*2.0f);
    float margin_dir2 = 1.0f/(((float) slice_->res_dir2())*2.0f);

    mesh->add_vertex(DefaultMeshVertex(p0).set_UV(margin_dir2, margin_dir1));
    mesh->add_vertex(DefaultMeshVertex(p1).set_UV(margin_dir2, 1.0f - margin_dir1));
    mesh->add_vertex(DefaultMeshVertex(p2).set_UV(1.0f - margin_dir2, margin_dir1));
    mesh->add_vertex(DefaultMeshVertex(p3).set_UV(1.0f - margin_dir2, 1.0f - margin_dir1));
    mesh->add_face(0, 1, 3);
    mesh->add_face(0, 3, 2);

    node_->set_tri_mesh(mesh);
    scene_manager_->AddOverlay(node_);

    delete surface_copy;

    emit updatedSliceView();
}

void DisplayScalarFieldSliceWidget::DeleteSlice() {
    if(slice_) {
        delete slice_;
        slice_ = nullptr;
    }

    if (node_ != nullptr) {
        scene_manager_->RemoveOverlay(node_->id());
        node_ = nullptr;
    }
}

}

} // end of namespace expressive
