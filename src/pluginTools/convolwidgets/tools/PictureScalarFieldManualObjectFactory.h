/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#pragma once
#ifndef CONVOL_PICTURE_SCALAR_FIELD_MANUAL_OBJECT_FACTORY_H
#define CONVOL_PICTURE_SCALAR_FIELD_MANUAL_OBJECT_FACTORY_H

#include <convol/tools/PictureScalarFieldSlice.h>

//// Ogre dependencies
//#ifdef _MSC_VER
//    #pragma warning(push, 0)
//#endif
//#include<OGRE/OgreVector3.h>
//#include<OGRE/OgreTexture.h>
//#include<OGRE/OgreSceneManager.h>
//#include<OGRE/OgreManualObject.h>
//#include<OGRE/OgreTextureManager.h>
//#include<OGRE/OgreMaterialManager.h>
//#include<OGRE/OgreHardwarePixelBuffer.h>
//#include<OGRE/OgreResourceGroupManager.h>
//#include<OGRE/OgreTechnique.h>
//#ifdef _MSC_VER
//    #pragma warning(pop)
//#endif

namespace expressive {
//namespace plugintools {

class PictureScalarFieldManualObjectFactory {
public:
//    typedef typename Ogre::Real Real;

//	// Static Function // TODO OPENGL
//    static Ogre::ManualObject* Build(Ogre::SceneManager* scene_manager, const convol::PictureScalarFieldSlice& picture_sf) {
//        // Texture Setup
//		Ogre::TexturePtr texture = Ogre::TextureManager::getSingleton().getByName("PictureScalarFieldPlane_texture");
//		if (!texture.isNull()) { Ogre::TextureManager::getSingleton().remove("PictureScalarFieldPlane_texture"); }
//		texture = Ogre::TextureManager::getSingleton().createManual("PictureScalarFieldPlane_texture",
//			Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,
//			picture_sf.res_dir1(), picture_sf.res_dir2(),
//			0, Ogre::PF_BYTE_BGRA, Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);

//		// Pixel Setup
//		Ogre::HardwarePixelBufferSharedPtr pixelBuffer = texture->getBuffer();

//        // Lock Pixel Buffer
//        pixelBuffer->lock(Ogre::HardwareBuffer::HBL_DISCARD);

//        // Reset the image.
//        Ogre::uint32* dest_buff = static_cast<Ogre::uint32*> (pixelBuffer->getCurrentLock().data);
//        for (Ogre::uint i = 0; i < picture_sf.res_dir1()*picture_sf.res_dir2(); i++, dest_buff++) {
//            *dest_buff = 0xFF0000000
//				| ((((Ogre::uint32) picture_sf.tab_picture_value(i,1)) & 0x000000FF) << 16)
//				| ((((Ogre::uint32) picture_sf.tab_picture_value(i,2)) & 0x000000FF) <<  8)
//				| ((((Ogre::uint32) picture_sf.tab_picture_value(i,3)) & 0x000000FF) <<  0);
//        }

//        // Unlock the pixel buffer
//        pixelBuffer->unlock();

//		// Material Setup
//        Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName(Ogre::String("PictureScalarFieldPlaneBaseMaterial"));
//		material->getTechnique(0)->getPass(0)->removeAllTextureUnitStates();
//		material->getTechnique(0)->getPass(0)->createTextureUnitState("PictureScalarFieldPlane_texture");
		
//		// Margin
//        float margin_dir1 = 1.0f/(((float) picture_sf.res_dir1())*2.0f);
//        float margin_dir2 = 1.0f/(((float) picture_sf.res_dir2())*2.0f);

//		// Quad Position
//        Point p0 = picture_sf.origin() - 0.5*(picture_sf.length_dir1()*picture_sf.dir1() + picture_sf.length_dir2()*picture_sf.dir2());
//        Point p1 = p0 + picture_sf.length_dir1()*picture_sf.dir1();
//        Point p2 = p0 + picture_sf.length_dir2()*picture_sf.dir2();
//        Point p3 = p2 + picture_sf.length_dir1()*picture_sf.dir1();
		
//        // Setup Manual Object
//		Ogre::ManualObject* manual_object = scene_manager->createManualObject("PictureScalarFieldPlaneManualObject");

//        manual_object->begin("PictureScalarFieldPlaneBaseMaterial", Ogre::RenderOperation::OT_TRIANGLE_LIST);
//        manual_object->position    (Ogre::Vector3(p0[0], p0[1], p0[2]));
//        manual_object->textureCoord(margin_dir2, margin_dir1);

//        manual_object->position    (Ogre::Vector3(p1[0], p1[1], p1[2]));
//        manual_object->textureCoord(margin_dir2, 1.0f-margin_dir1);

//        manual_object->position    (Ogre::Vector3(p2[0], p2[1], p2[2]));
//        manual_object->textureCoord(1.0f-margin_dir2, margin_dir1);

//        manual_object->position    (Ogre::Vector3(p3[0], p3[1], p3[2]));
//        manual_object->textureCoord(1.0f-margin_dir2, 1.0f-margin_dir1);

//        manual_object->triangle(0, 1, 3);
//		manual_object->triangle(0, 3, 2);
//        manual_object->end();

//        return manual_object;
//    }
};

//} // Close namespace plugintools
} // Close namespace expressive

#endif // CONVOL_PICTURE_SCALAR_FIELD_TO_MANUAL_OBJECT_H
