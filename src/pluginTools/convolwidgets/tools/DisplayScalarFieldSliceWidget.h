/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#pragma once
#ifndef DISPLAY_SCALAR_FIELD_SLICE_PANEL_H
#define DISPLAY_SCALAR_FIELD_SLICE_PANEL_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include "ui_DisplayScalarFieldSliceWidget.h"
#include <QWidget>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <convol/ConvolObject.h>
#include <convol/blobtreenode/BlobtreeNode.h>
#include <convol/tools/PictureScalarFieldSlice.h>

#include <pluginTools/convol/ImplicitSurfaceSceneNode.h>


namespace expressive {

namespace plugintools
{

class DisplayScalarFieldSliceWidget : public QWidget, private Ui::DisplayScalarFieldSliceWidget {
    Q_OBJECT

public:

    DisplayScalarFieldSliceWidget(
            core::Camera* camera,
            core::SceneManager* scene_manager,
            std::shared_ptr<plugintools::ImplicitSurfaceSceneNode> implicit_surface_scene_node,
            QWidget* parent = 0);
    ~DisplayScalarFieldSliceWidget();

signals:
    void updatedSliceView();

private slots:
    void ComputeDist();
    void CreateSlice();
    void DeleteSlice();

//    /////////////////////////////////////////////////////
//    //TODO:@todo temporary ...
//    // Create a slice with additionnal information based on the skeleton structure
//    void CreateSkeletonTerritorySlice();
//    /////////////////////////////////////////////////////

private:
    // ScalarFieldSlice Data
    convol::PictureScalarFieldSlice* slice_  ;

    // Scenegraph Data
    core::Camera *camera_;
    core::SceneManager* scene_manager_;
    std::shared_ptr<core::Overlay> node_;

    // This is the implicit surface that is represented by the ScalarFieldSlice controlled by this widget
    std::shared_ptr<plugintools::ImplicitSurfaceSceneNode> implicit_surface_scene_node_;

    void CreateSceneNode();
};

}

} // end of namespace expressive

#endif // DISPLAY_SCALAR_FIELD_SLICE_PANEL_H
