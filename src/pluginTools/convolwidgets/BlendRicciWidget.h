/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: HomotheticInverseWidget.h

   Language: C++

   License: Convol Licence

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Project: Convol-ConvolLab

   Description: Header file for FunctorWidget.
                This is the widget that contains the UI about Functor widget.

   Platform Dependencies: None
*/

#pragma once
#ifndef BLEND_RICCI_WIDGET_H
#define BLEND_RICCI_WIDGET_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include "ui_BlendRicciWidget.h"
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <core/functor/Functor.h>
#include <convol/functors/operators/BlendRicci.h>

#include <pluginTools/convolwidgets/FunctorWidget.h>

namespace expressive {

class BlendRicciWidget : public AbstractFunctorWidget, public Ui::BlendRicciWidget
{
    Q_OBJECT

public:
    BlendRicciWidget(QWidget* parent = 0)
        : AbstractFunctorWidget(parent)
    {
        setupUi(this);
    }
    ~BlendRicciWidget() {}

    virtual std::string Name()  const { return StaticName(); }
    static std::string StaticName() {
        return convol::BlendRicci::StaticName();
    }

    virtual TiXmlElement* ToXml() const
    {
        TiXmlElement *element = new TiXmlElement( Name() );
        element->SetDoubleAttribute("exposant",dbl_spin_box_exposant->value());
        return element;
    }

    virtual void UpdateWidgetFromNode(const core::Functor* functor)
    {
        const convol::BlendRicci* casted_functor = dynamic_cast<const convol::BlendRicci*> (functor);
        dbl_spin_box_exposant->setValue(casted_functor->exposant());
    }

};

} //end of namespace expressive

#endif // BLEND_RICCI_WIDGET_H
