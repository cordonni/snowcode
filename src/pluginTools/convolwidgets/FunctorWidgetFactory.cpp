#include <pluginTools/convolwidgets/FunctorWidgetFactory.h>


namespace expressive {

FunctorWidgetFactory& FunctorWidgetFactory::GetInstance()
{
    static FunctorWidgetFactory INSTANCE = FunctorWidgetFactory();
    return INSTANCE;
}

bool FunctorWidgetFactory::find(const std::string &functor_name) const
{
    auto it = types.find(functor_name);
    return (it != types.end());
}

auto FunctorWidgetFactory::CreateNewFunctorWidget(const std::string &functor_name)
-> AbstractFunctorWidget*
{
    auto it = types.find(functor_name);
    if(it != types.end())
    {
        return it->second();
    }
    else
    {
        ork::Logger::ERROR_LOGGER->log("PLUGINTOOLS", "FunctorWidgetFactory : primitive not registered" );
        throw std::exception();
    }
//    return nullptr; // unreachable
}


} // Close namespace expressive
