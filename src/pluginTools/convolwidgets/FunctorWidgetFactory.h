/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: FunctorWidgetFactory.h

   Language: C++

   License: Convol license

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Header file for a FunctorWidget factory (created object should
                derive from AbstractFunctorWidget).
                This factory can create the required object from a name
                (the one returned by StaticName of a given Functor).

   Platform Dependencies: None
*/

#pragma once
#ifndef EXPRESSIVE_FUNCTOR_WIDGET_FACTORY_T_H_
#define EXPRESSIVE_FUNCTOR_WIDGET_FACTORY_T_H_

// core dependencies
#include <core/CoreRequired.h>
    // utils
    #include <ork/core/Logger.h>

#include <pluginTools/convolwidgets/FunctorWidget.h>

namespace expressive {

/*! \brief ...
 */
class FunctorWidgetFactory
{
public:
    typedef AbstractFunctorWidget* (*BuilderFunction) ();
    typedef typename std::map<std::string, BuilderFunction> MapperType;
    
    static FunctorWidgetFactory& GetInstance();

    AbstractFunctorWidget* CreateNewFunctorWidget(const std::string &functor_name);

    bool find(const std::string &functor_name) const;

    template<typename TFunctorWidget>
    void RegisterFunctorWidget(BuilderFunction f);

    template <class TFunctorWidget>
    class Type
    {
    public:
        static AbstractFunctorWidget* ctor ()
        {
            return new TFunctorWidget();
        }
        
        Type()
        {
            FunctorWidgetFactory::GetInstance().RegisterFunctorWidget<TFunctorWidget>(ctor);
        }
    };

private:
    MapperType types;
};
    


template <typename TFunctorWidget>
void FunctorWidgetFactory::RegisterFunctorWidget(BuilderFunction f)
{
    types[TFunctorWidget::StaticName()] = f;
}

} // Close namespace expressive

#endif // EXPRESSIVE_FUNCTOR_WIDGET_FACTORY_T_H_
