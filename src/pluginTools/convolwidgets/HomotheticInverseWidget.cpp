#include <pluginTools/convolwidgets/HomotheticInverseWidget.h>

#include <pluginTools/convolwidgets/FunctorWidgetFactory.h>

namespace expressive {

//TiXmlElement* HomotehticInverseWidget::ToXml() const
//{
//}

//void HomotehticInverseWidget::UpdateWidgetFromNode(const core::Functor* functor)
//{
//}

static FunctorWidgetFactory::Type<HomotheticInverse3Widget> HomotheticInverse3WidgetType;
static FunctorWidgetFactory::Type<HomotheticInverse4Widget> HomotheticInverse4WidgetType;

} //end of namespace expressive
