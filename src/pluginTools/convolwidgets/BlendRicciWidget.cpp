#include <pluginTools/convolwidgets/BlendRicciWidget.h>

#include <pluginTools/convolwidgets/FunctorWidgetFactory.h>

namespace expressive {

static FunctorWidgetFactory::Type<BlendRicciWidget> BlendRicciWidgetType;

} //end of namespace expressive
