/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BasicOperatorWidget.h

   Language: C++

   License: Convol Licence

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Project: ????

   Description: Header file for some opertor's widget that do not require any
                parameters.

   Platform Dependencies: None
*/

#pragma once
#ifndef BASIC_OPERATOR_WIDGET_H
#define BASIC_OPERATOR_WIDGET_H

#include <core/functor/Functor.h>
#include <convol/functors/operators/BlendMax.h>
#include <convol/functors/operators/BlendSum.h>

#include <pluginTools/convolwidgets/FunctorWidget.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QWidget>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {

/////////////////////////////////////////////////////////////////////////////
///TODO:@todo : should find a simple way to have a single widget class for
/// all the inverse kernel ...
/////////////////////////////////////////////////////////////////////////////

class BlendMaxWidget : public AbstractFunctorWidget
{
    Q_OBJECT

public:
    BlendMaxWidget(QWidget* parent = 0)
        : AbstractFunctorWidget(parent) {}
    ~BlendMaxWidget() {}

    virtual std::string Name() const { return StaticName(); }
    static std::string StaticName() {
        return convol::BlendMax::StaticName();
    }

    virtual TiXmlElement* ToXml() const
    {
        TiXmlElement *element = new TiXmlElement( Name() );
        return element;
    }

    virtual void UpdateWidgetFromNode(const core::Functor* /*functor*/) {}
};

class BlendSumWidget : public AbstractFunctorWidget
{
    Q_OBJECT

public:
    BlendSumWidget(QWidget* parent = 0)
        : AbstractFunctorWidget(parent) {}
    ~BlendSumWidget() {}

    virtual std::string Name()  const  { return StaticName(); }
    static std::string StaticName() {
        return convol::BlendSum::StaticName();
    }

    virtual TiXmlElement* ToXml() const
    {
        TiXmlElement *element = new TiXmlElement( Name() );
        return element;
    }

    virtual void UpdateWidgetFromNode(const core::Functor* /*functor*/) {}
};

} //end of namespace expressive

#endif // OPERATOR_WIDGET_H
