/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#pragma once
#ifndef GL_EXPRESSIVE_ENGINE_H_
#define GL_EXPRESSIVE_ENGINE_H_

// ork dependencies
#include "ork/core/Timer.h"
#include "ork/render/Program.h"
#include "ork/render/Mesh.h"

// core dependencies
#include "core/ExpressiveEngine.h"
#include "core/geometry/AbstractTriMesh.h"


// plugintools dependencies
#include "pluginTools/PluginToolsRequired.h"
//#include "pluginTools/scenemanipulator/SceneManipulator.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
// Qt dependencies
#include <QQuickView>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

//namespace core {
//template<typename T> clasAbstractTriMeshshT;
//class DefaultMeshVertex;
//typedeAbstractTriMeshshT<DefaultMeshVertexAbstractTriMeshsh;
//}

namespace plugintools
{

class StateMachine;
class SceneManipulator;


/**
 * Dummy default scene.
 * Contains the background plane, the overlays, and the lights.
 */
class PLUGINTOOLS_API MyScene
{
public:

    struct P3_N3_UV_C {

        float x, y, z, nx, ny, nz, u, v;

        unsigned char r, g, b, a;

        P3_N3_UV_C()
        {
        }

        P3_N3_UV_C(float x, float y, float z, float nx, float ny, float nz,
                float u, float v, unsigned char r, unsigned char g, unsigned char b, unsigned char a) :
            x(x), y(y), z(z), nx(nx), ny(ny), nz(nz), u(u), v(v), r(r), g(g), b(b), a(a)
        {
        }
    };

//    MyScene(ork::ptr<ork::ResourceManager> resource_manager);
    MyScene(ork::ptr<core::SceneManager> scene_manager, ork::ptr<ork::ResourceManager> resource_manager);
    virtual ~MyScene() {}

    float alpha;
    float theta;
    float fov;
    float dist;
    ork::ptr<ork::Program> program1;
    ork::ptr<ork::Program> program2;
    ork::ptr<ork::Mesh<P3_N3_UV_C, unsigned int> > cube;
    ork::ptr<core::AbstractTriMesh> cube2;
    ork::ptr<ork::Mesh<P3_N3_UV_C, unsigned int> > plane;

    std::shared_ptr<core::SceneNode> node;
    std::shared_ptr<core::SceneNode> nodeRef;
    std::shared_ptr<core::SceneNode> light;

    ork::ptr<ork::Uniform3f> worldCamera;
    ork::ptr<ork::UniformMatrix4f> worldToScreenU;
    ork::ptr<ork::UniformMatrix4f> localToWorld1;
    ork::ptr<ork::UniformMatrix4f> localToWorld2;
//    ork::ptr<ork::MeshBuffers>  cube;
//    ork::ptr<ork::MeshBuffers>  plane;
};

class PLUGINTOOLS_API GLExpressiveEngine :
        public QObject,
        public core::ExpressiveEngine
{
    Q_OBJECT
//    Q_PROPERTY(QObject *state_machine READ state_machine)

public:
    GLExpressiveEngine(QQuickView *quickWindow = nullptr, const std::string expressiveConfig = "defaultConfig");
//    GLExpressiveEngine(QQuickItem *parent = nullptr);
    virtual ~GLExpressiveEngine();

    /////////////////
    /// Accessors ///
    /////////////////
    QQmlContext* qml_context() const;
    QQuickView *quick_window() const;
    std::shared_ptr<SceneManipulator> scene_manipulator() const;
    std::shared_ptr<StateMachine> state_machine() const;

    /////////////////
    /// Modifiers ///
    /////////////////
    void set_scene_manipulator(std::shared_ptr<SceneManipulator> scene_manipulator);

    /**
     * Not sure this is still used.
     */
    QSGTexture* CreateTextureFromId(uint id, const QSize &size, QQuickWindow::CreateTextureOptions options = QQuickWindow::CreateTextureOption(0)) const;

signals:
    /**
     * glInitialized gets triggered when opengl has been setup.
     */
    void glInitialized();

    /**
     * initialized gets triggered when the entire engine has been setup.
     */
    void initialized();

    /**
     * sceneInitialized gets triggered when the scene has been added to the engine and that the program has finished loading.
     * This is probably the signal you want to link to if you're looking here.
     */
    void sceneInitialized();

public slots:
    void DeferredInit();
    void AddContent();

    void Redisplay();
    void Resize(const Vector2i &size);
    void WindowChanged();
    void NotifySceneInitialized();

protected:
    virtual void Init(const std::string &expressiveConfig = "defaultConfig");
    void InitWindow();
    void InitFactories();
    void InitScene();
    void InitStateMachine();

    virtual void RegisterQmlTypes();

private:

    /// Widgets
    QQmlContext* qml_context_;
    QQuickView * quick_window_;

    std::shared_ptr<MyScene> m_scene;

    /// State Machine
    std::shared_ptr<StateMachine> state_machine_;

    /// Scene Manipulation stuff
    std::shared_ptr<SceneManipulator> scene_manipulator_;

    bool m_initialized;
    bool m_valid_viewport;

    ork::Timer timer_;

    double t_;

    double dt_;

    bool dirty_resources_;

};

} // namespace plugintools

} // namespace expressive

#endif // GL_EXPRESSIVE_ENGINE_H_
