/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SKETCHINGMODELER_H
#define SKETCHINGMODELER_H

// core dependencies
#include <core/image/ImageT.h>
#include <core/image/Skeletonizer.h>
#include <core/graph/Graph.h>
#include <core/geometry/Polygon2D.h>
#include <core/scenegraph/listeners/SceneManagerListener.h>

// convol dependencies
#include <convol/blobtreenode/BlobtreeNode.h>

// plugintools dependencies
#include <pluginTools/scenemanipulator/SceneManipulatorListener.h>
#include <pluginTools/convol/ImplicitSurfaceSceneNode.h>


namespace expressive
{

namespace plugintools
{

class DrawSketchTask;
class SketchingModeler;

class PLUGINTOOLS_API SketchingModelerListener : public core::ListenerT<SketchingModeler>
{
public:
    SketchingModelerListener(SketchingModeler* target = nullptr);

    virtual ~SketchingModelerListener();

    virtual void brushSizeChanged() = 0;
    virtual void enableStateChanged() = 0;
};

/**
 * @brief The SketchingModeler class
 * This class is a SceneManipulatorListener that allows to draw sketchings on the screen
 * and to use that input in order to find a skeleton.
 * How it works :
 * - First step : draw on screen, and records everything in a ImageT<Color>.
 * - Export the distance map of that image.
 * - Export the contours of that image.
 * - Possibly simplify the contours to have quicker results.
 * - Apply Voronoi on the (simplified) contours
 * - Remove the edges from the voronoi diagram that can't be part of the skeleton (outside of the distance map for example).
 * - Apply SAT on that final graph to determine which part should not be kept.
 * TODO : Add parameters for tweaking detection (such as the mask used for distance map computation,
 * merging distances for branches & voronoi inputs, SAT ratio parameter etc...).
 * TODO : Update overlays when resizing window.
 */
class PLUGINTOOLS_API SketchingModeler : public SceneManipulatorListener, public core::SceneManagerListener, public core::ListenableT<SketchingModeler>
{
public:
    EXPRESSIVE_MACRO_NAME("SketchingModeler")

    enum SketchMode {
        E_PAINT = 0,
        E_ERASE,
        E_FILL
    };

    enum BehaviorMode {
        E_ADD = 0,
        E_REMOVE,
        E_CUT
    };

    enum SymmetryMode {
        E_NO_SYMMETRY = 0,
        E_VERTICAL_SYMMETRY = 1,
        E_HORIZONTAL_SYMMETRY = 2,
        E_BOTH_SYMMETRY = 3
    };

    enum DepthSelectionMode {
        E_PLANE,
        E_SURFACE
    };

    SketchingModeler(SceneManipulator* manipulator, uint width = 500, uint height = 400);

    virtual ~SketchingModeler();

    /////
    /// SceneManipulatorListener Events
    ///
    virtual bool wheelEvent        (std::shared_ptr<core::InputController> event);
    virtual bool mousePressed      (std::shared_ptr<core::InputController> event);
    virtual bool mouseReleased     (std::shared_ptr<core::InputController> event);
    virtual bool mouseMoved        (std::shared_ptr<core::InputController> event);
    virtual bool keyPressed        (std::shared_ptr<core::InputController> event);
    virtual bool resize(int, int);
    virtual void updated(SceneManipulator *);
    virtual void updated(core::SceneManager *);


    ////
    /// SceneManagerListener Events
    ///
    virtual void SceneNodeAdded(NodeId /*node*/);
    virtual void SceneNodeRemoved(NodeId node);
    virtual void SceneNodeSelected(NodeId node);
    virtual void SceneNodeUnselected(NodeId node);
    virtual void cleared(core::SceneManager *manager);

    ////
    /// Own events
    virtual void NotifyBrushSizeChanged();
    virtual void NotifyEnableChanged();

    ////
    /// SETTERS
    ///
    virtual void set_brush_size(Scalar s);
    virtual void set_brush_color(Color c);
//    virtual void set_draw_on_surface(bool b);
    virtual void set_sketch_mode(SketchMode m);
    virtual void set_behavior_mode(BehaviorMode m);
    virtual void set_symmetry_mode(SymmetryMode m);
    virtual void set_depth_selection_mode(DepthSelectionMode m );
    virtual void show_skeleton(bool b);

    ////
    /// GETTERS
    ///
    std::shared_ptr<core::SceneManager> scene_manager() const;
    bool enabled() const;
    Scalar brush_size() const;
    Color brush_color() const;
    bool show_skeleton() const;
//    inline bool draw_on_surface() const;
    SketchMode sketch_mode() const;
    BehaviorMode behavior_mode() const;
    SymmetryMode symmetry_mode() const;    
    DepthSelectionMode depth_selection_mode() const;
    uint width() const;
    uint height() const;
    std::shared_ptr<core::ImageT<Color> > draw_map() const;
    uint8* texture_data() const;
    Scalar distance_to_hovered_item() const;
    bool show_distance_to_hovered_item() const;
    Point focus_point() const;

    ////
    /// OTHERS
    ///

    /**
     * @brief enable Enables this tool : It will start catching events and display it's overlays.
     */
    void enable(bool enabled = true);

    /**
     * Validates the current drawing.
     */
    void Validate();

    /**
     * @brief Clear reinitialize the textures used for displaying the current sketching. it is possible to
     *         use a default color, only used to differentiate the "drawing" mode from the "normal" mode (you should set some transparency there).
     * @param r
     * @param g
     * @param b
     * @param a
     */
    void Clear(Scalar r = 0.0, Scalar g = 0.0, Scalar b = 0.0, Scalar a = 0.5);

    /**
     * @brief set_current_node Sets the node in which the drawings will be added.
     */
    void set_current_node(ImplicitSurfaceSceneNode* node);

    /**
     * Gets the list of commands and help for this tool.
     */
    virtual core::KeyBindingsList GetKeyBindings() const;

    void LoadContourFromImage(const std::string &filename);
    void LoadContourFromGraph(const std::string &filename);
    void LoadContourFromFile();
protected:
    struct PointSymmetryList
    {
        PointSymmetryList(Scalar x, Scalar y, SymmetryMode mode)
        {
            Scalar x2 = 1.0 - x;
            Scalar y2 = 1.0 - y;

            if ((mode & E_VERTICAL_SYMMETRY) && (x != 0.5)) {
                points_.push_back(Point2(x2, y));
            }
            if ((mode & E_HORIZONTAL_SYMMETRY) && (y != 0.5)) {
                points_.push_back(Point2(x, y2));
            }
            if ((mode & E_HORIZONTAL_SYMMETRY) && (mode & E_VERTICAL_SYMMETRY) && (x != 0.5) && (y != 0.5)) {
                points_.push_back(Point2(x2, y2));
            }
        }

        inline unsigned int GetSymmetryCount() { return (uint) points_.size(); }
        inline Point2 &GetSymmetry(unsigned int index) { return points_[index]; }

    protected:
        std::vector<Point2> points_;
    };

    SketchingModeler();

    /**
     * @brief init defered init method.
     * @see SketchingModeler().
     */
    void init(SceneManipulator* scene_manipulator, uint width = 500, uint height = 400);

    /**
     * Creates a TaskFactory that will display the content of this sketcher.
     */
    virtual ork::ptr<ork::TaskFactory> create_draw_task();

    /**
     * @brief init_textured_overlay initializes the required material / textures used for drawing.
     */
    void InitTexturedOverlay();

    /**
     * @brief DrawBrush draws a brush (circle) at the given coordinate, with given color, with given radius.
     * @param x brush x coordinate
     * @param y brush y coordinate
     * @param radius brush radius
     * @param c color brush color
     * @param symmetry_mode Whether or not symmetry must be used.
     */
    void DrawBrush(Scalar x, Scalar y, Scalar radius, const Color &c, SymmetryMode symmetry_mode = E_NO_SYMMETRY);

    /**
     * @brief DrawBrush Same as above, but creates a straight line between 2 points, which can have 2 different radiuses.
     * @param x1 brush x coordinate of the first point.
     * @param y1 brush y coordinate of the first point.
     * @param x2 brush x coordinate of the second point.
     * @param y2 brush y coordinate of the second point.
     * @param radius1 brush radius of the first point.
     * @param radius2 brush radius of the second point.
     * @param c brush color.
     * @param symmetry_mode Whether or not symmetry must be used.
     */
    void DrawBrush(Scalar x1, Scalar y1, Scalar x2, Scalar y2, Scalar radius1, Scalar radius2, const Color &c, SymmetryMode symmetry_mode = E_NO_SYMMETRY);

    /**
     * @brief DrawPolygon Draws a given polygon in the current sketch.
     * @param poly a closed polygon.
     */
    void DrawPolygon(const core::Polygon2D &poly, const Color &c);

    /**
     * @brief ExtractSkeleton Extracts the skeleton from the current recorded image.
     * @return a graph containing the 2D screen skeleton.
     */
    std::shared_ptr<core::WeightedGraph2D> ExtractSkeleton();

    /**
     * @brief AddSkeletonToScene adds the given skeleton to scene.
     *                           If the sketching started on an existing node, and if that node is able to use a Graph
     *                           as input for new data, the new sketch will be added to that node.
     *                           Otherwise, if no item were selected or if it's not compatible with Graph, a new ImplicitSurfaceSceneNode will
     *                           be added.
     * @param skeleton           The skeleton we want to add to the scene.
     */
    void AddSkeletonToScene(std::shared_ptr<core::WeightedGraph2D> skeleton);

    std::shared_ptr<ImplicitSurfaceSceneNode> CreateImplicitSurfaceSceneNode();

    ///////
    /// Scene and SceneNode
    ///
    /**
     * @brief scene_ the scene to which we want to add stuff when sketching.
     */
    std::shared_ptr<core::SceneManager> scene_;

    /**
     * @brief current_node_ node on which we are currently drawing onto.
     */
    ImplicitSurfaceSceneNode* current_node_;

    /**
     * @brief camera_ the camera on which user is drawing. we'll add sketch on a parrallel plane.
     */
    core::Camera* camera_;

    std::shared_ptr<convol::BlobtreeNode> parent_node_;

    /**
     * @brief draw_map_ the Image used to actually do the computations.
     */
    std::shared_ptr<core::ImageT<Color> > draw_map_;


    /**
     * Temporary items used when computing final graph.
     * Used for debug display.
     */
    core::ImageT<Color> contours_;
    core::ImageT<float> distances_;
    std::vector<std::vector<Point2> > simplified_branches_;
    std::vector<core::Segment2D> res_voronoi_;
    std::shared_ptr<core::WeightedGraph2D> unfiltered_graph_;
    std::mutex mutex_;



    /**
     * @brief texture_name_ the dynamic texture name, used to be able to retrieve it (we cant store ogre's textures).
     */
    std::string texture_name_;
    std::string overlay_name_;
    std::string material_name_;
    std::string pencil_material_name_;

    /**
     * @brief initialized_ Whether or not the modeler is initialized.
     */
    bool initialized_;

    ///////
    /// SketchingModeler options
    ///
    /**
     * @brief enabled_ Whether or not the Modeler is activated.
     */
    bool enabled_;

    /**
     * @brief drawing_ Whether or not the user is currently drawing.
     */
    bool drawing_;

    /**
     * @brief width_ width of the image used for computation
     */
    uint width_;

    /**
     * @brief height_ height of the image used for computation
     */
    uint height_;

    /**
     * @brief brush_size_ current brush size.
     */
    Scalar brush_size_;
    Scalar previous_brush_size_;
    Point2 previous_brush_pos_;

    /**
     * @brief brush_color_ current brush color
     */
    Color brush_color_;

    /**
     * @brief show_skeleton_ Whether or not we want to display the current skeleton.
     */
    bool show_skeleton_;

    /**
     * @brief sketch_mode_ The sketch mode (painting or erasing ?).
     */
    SketchMode sketch_mode_;

    /**
     * @brief behavior_mode_ The behavior mode (adding or removing ?).
     */
    BehaviorMode behavior_mode_;

    /**
     * @brief symmetry_mode_ Determines if symmetry will be added when drawing.
     */
    SymmetryMode symmetry_mode_;

    /**
     * @brief depth_selection_mode_ Selects how to add stuff : Either just draw along a plane, or try to stick as much as possible to the existing surface.
     */
    DepthSelectionMode depth_selection_mode_;

    //////
    /// Rendering related stuff.
    ///
    /**
     * @brief camera_width_ Current screen width. Must be reset when changing window size.
     */
    Scalar camera_width_;

    /**
     * @brief camera_height_ Current screen height . Must be reset when changing window size.
     */
    Scalar camera_height_;

    /**
     * @brief locked_texture_data_ when locked, this contains the actual data of the displayed texture.
     */
    uint8 *locked_texture_data_;

    /**
     * @brief hovered_item_ the currently hovered item.
     */
    core::SceneNode* hovered_item_;

    /**
     * @brief distance_to_hovered_item_ distance to the currently hovered item.
     */
    Scalar distance_to_hovered_item_;

    /**
     * @brief show_plane_ selectes whether or not the drawer should display the distance plane or not.
     */
    bool show_distance_to_hovered_item_;

    /**
     * The exact intersection point of the cursor with hovered_item_.
     */
    Point focus_point_;

    friend class DrawSketchTask;

};

} // namespace plugintools

} // namespace expressive

#endif // SKETCHINGMODELER_H
