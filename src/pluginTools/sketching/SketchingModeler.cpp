#include "pluginTools/sketching/SketchingModeler.h"

#include "core/ExpressiveEngine.h"

#include "core/geometry/SubdivisionCurve.h"
#include "core/geometry/VectorTools.h"
#include "core/graph/algorithms/SATFiltering.h"
#include "core/graph/GraphProfile.h"
#include "core/image/DistanceImageTools.h"
#include "core/scenegraph/Overlay.h"
#include "core/utils/KeyBindingsList.h"

#include "convol/ImplicitSurface.h"
#include "convol/ConvolObject.h"
#include "convol/primitives/NodeISSubdivisionCurveT.h"
#include "convol/primitives/NodeSegmentSFieldT.h"
#include "convol/primitives/NodePointSFieldT.h"
#include "convol/functors/operators/BlendSum.h"
#include "convol/functors/operators/BlendDiff.h"
#include "convol/blobtreenode/NodeOperatorT.h"

//#include "convol/blobtreenode/nodeoperator/NodeOperatorNCorrectedConvolutionT.h"
#include "convol/blobtreenode/nodeoperator/CorrectedSumIntegralSurfaceT.h"
#include "convol/factories/BlobtreeNodeFactory.h"

#include "pluginTools/sketching/DrawSketchTask.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QFileDialog>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

//#define USE_SUBDIVISION_CURVES

using namespace expressive::core;
using namespace expressive::convol;
using namespace std;

namespace expressive
{

namespace plugintools
{

SketchingModelerListener::SketchingModelerListener(SketchingModeler* target) :
    ListenerT<SketchingModeler>(target)
{
}

SketchingModelerListener::~SketchingModelerListener()
{
}

SketchingModeler::SketchingModeler(SceneManipulator* scene_manipulator, uint width, uint height) :
    SceneManipulatorListener(),
    SceneManagerListener(),
    scene_(nullptr),
    current_node_(nullptr),
    camera_(nullptr),
    parent_node_(nullptr),
    draw_map_(nullptr),
    contours_(width, height),
    distances_(width, height),
    unfiltered_graph_(nullptr),
    texture_name_(""),
    overlay_name_(""),
    material_name_(""),
    pencil_material_name_(""),
    initialized_(false),
    enabled_(false),
//    draw_on_surface_(true),
    drawing_(false),
    width_(width),
    height_(height),
    brush_size_(0.05),
    previous_brush_size_(0.05),
    previous_brush_pos_(Point2::Zero()),
    brush_color_(Color(100, 100, 200, 200)),
    show_skeleton_(false),
    sketch_mode_(E_PAINT),
    behavior_mode_(E_ADD),
    symmetry_mode_(E_NO_SYMMETRY),
    depth_selection_mode_(E_SURFACE),
    camera_width_(1.0),
    camera_height_(1.0),
    locked_texture_data_(nullptr),
    hovered_item_(nullptr),
    distance_to_hovered_item_(-1.0),
    focus_point_(Point::Zero())
{
    init(scene_manipulator, width, height);
}

SketchingModeler::SketchingModeler() :
    SceneManipulatorListener(),
    SceneManagerListener(),
    scene_(nullptr),
    current_node_(nullptr),
    camera_(nullptr),
    parent_node_(nullptr),
    draw_map_(nullptr),
    contours_(1, 1),
    distances_(1, 1),
    unfiltered_graph_(nullptr),
    texture_name_(""),
    overlay_name_(""),
    material_name_(""),
    pencil_material_name_(""),
    initialized_(false),
    enabled_(false),
//    draw_on_surface_(true),
    drawing_(false),
    width_(0),
    height_(0),
    brush_size_(0.05),
    previous_brush_size_(0.05),
    previous_brush_pos_(Point2::Zero()),
    brush_color_(Color(100, 100, 200, 200)),
    show_skeleton_(false),
    sketch_mode_(E_PAINT),
    behavior_mode_(E_ADD),
    symmetry_mode_(E_NO_SYMMETRY),
    depth_selection_mode_(E_SURFACE),
    camera_width_(1.0),
    camera_height_(1.0),
    locked_texture_data_(nullptr),
    hovered_item_(nullptr),
    distance_to_hovered_item_(-1.0),
    focus_point_(Point::Zero())
{
}

void SketchingModeler::init(SceneManipulator* scene_manipulator, uint width, uint height)
{
    if (scene_manipulator != nullptr) {
        scene_ = scene_manipulator->scene_manager();
        camera_ = scene_->current_camera().get();
        SceneManipulatorListener::Init(scene_manipulator);
        SceneManagerListener::Init(scene_manipulator->scene_manager().get());
    }

    draw_map_ = make_shared<ImageT<Color> >(width, height);

    srand((uint)time(0));
}

SketchingModeler::~SketchingModeler()
{
    if (locked_texture_data_ != nullptr) {
        delete[] locked_texture_data_;
        locked_texture_data_ = nullptr;
    }
}

ork::ptr<ork::TaskFactory> SketchingModeler::create_draw_task()
{
    return new DrawSketchTask(this);
}

void SketchingModeler::InitTexturedOverlay()
{
    ork::ptr<Overlay> n = new Overlay(scene_.get(), "sketchingOverlay", Overlay::O_FOREGROUND, create_draw_task());
    scene_->AddOverlay(n);

    if (camera_ != nullptr) {
        camera_width_ = camera_->GetActualWidth();
        camera_height_ = camera_->GetActualHeight();
    }

    initialized_ = true;
}

void SketchingModeler::enable(bool enabled)
{
    enabled_ = enabled;
    if (enabled)
    {
        if (!initialized_) {
            InitTexturedOverlay();
        }
        Clear();
//        overlay_->show();
        scene_->RemoveAllFromSelection();
        scene_->SetHoveredNode(nullptr);
    } else {
        Clear();
//        overlay_->hide();
    }
}

void SketchingModeler::Validate() {
    //            tmp_overlay_->clear();
    std::shared_ptr<WeightedGraph2D> res = ExtractSkeleton();
    AddSkeletonToScene(res);

    Clear();

}

void SketchingModeler::Clear(Scalar r, Scalar g, Scalar b, Scalar a)
{
    hovered_item_ = nullptr;

    Vector cameraDir = camera_->GetPointInWorld(Point(0.0, 0.0, -1.0)) - camera_->GetPointInWorld();
    Vector ray = camera_->target() - camera_->GetPointInWorld();
    distance_to_hovered_item_ = ray.dot(cameraDir);

    drawing_ = false;

    draw_map_ = make_shared<ImageT<Color> >(width_, height_);

    for (unsigned int x = 0; x < width_; ++x) {
        for (unsigned int y = 0; y < height_; ++y) {
            (*draw_map_)(x, y) = Color(r, g, b, a);//Color::Zero();
        }
    }
}

void SketchingModeler::SceneNodeRemoved(NodeId node)
{
    if (current_node_ != nullptr && node == current_node_->id()) {
        current_node_ = nullptr;
        parent_node_ = nullptr;
    }
}

void SketchingModeler::SceneNodeSelected(NodeId /*node*/)
{
}

void SketchingModeler::SceneNodeUnselected(NodeId /*node*/)
{
}

void SketchingModeler::cleared(SceneManager * /*manager*/)
{
    current_node_ = nullptr;
}

void SketchingModeler::NotifyBrushSizeChanged()
{
    for (auto listener : listeners_) {
        auto l = dynamic_cast<SketchingModelerListener*>(listener);
        if (l != nullptr) {
            l->brushSizeChanged();
        }
    }
}

void SketchingModeler::NotifyEnableChanged()
{
    for (auto listener : listeners_) {
        auto l = dynamic_cast<SketchingModelerListener*>(listener);
        if (l != nullptr) {
            l->enableStateChanged();
        }
    }
}

void SketchingModeler::set_brush_color(Color c)
{
    brush_color_ = c;
}

bool SketchingModeler::show_skeleton() const
{
    if (current_node_ && current_node_->displayed_skeleton()) {
        return current_node_->displayed_skeleton()->visible();
    }
    return show_skeleton_;
}

void SketchingModeler::show_skeleton(bool b)
{
    show_skeleton_ = b;
    if (current_node_ && current_node_->displayed_skeleton()) {
        current_node_->displayed_skeleton()->set_visible(b);
    }
}

core::KeyBindingsList SketchingModeler::GetKeyBindings() const
{
    KeyBindingsList k;
    k.AddKey("Sketching Modeler", "S", "Enable Sketching mode");

    return k;
}

void SketchingModeler::set_current_node(ImplicitSurfaceSceneNode* node)
{
    current_node_ = node;
    if (node != nullptr) {
        parent_node_= make_shared<NodeOperatorNArityT<BlendSum> > ();
//        parent_node_= BlobtreeNodeFactory::GetInstance().CreateNewBlobtreeNode("CorrectedSumIntegralSurface", npss->kernel_functor());//make_shared<OptCorrectedSumIntegralSurfaceT<>> (npss->kernel_functor());
        node->convol_object()->blobtree_root()->AddNewNodeToRoot(parent_node_);

        if (current_node_->displayed_skeleton()) {
            current_node_->displayed_skeleton()->set_visible(show_skeleton_);
        }
    }
}

bool SketchingModeler::wheelEvent(std::shared_ptr<core::InputController> event)
{
    bool res = false;
    if (enabled_) {
        std::shared_ptr<MouseInputController> mic = dynamic_pointer_cast<MouseInputController>(event);

        std::shared_ptr<MouseIC> m = dynamic_pointer_cast<MouseIC>(event);

        if (m->modifiers() & Qt::AltModifier) {
            if (distance_to_hovered_item_ > 0.0) {
                distance_to_hovered_item_ += 0.1 * (mic->wheel_delta() > 0.0 ? 1.0 : -1.0);
//                std::cout << distance_to_hovered_item_ << std::endl;
            }
            res = true;
        } else if (m->modifiers() & Qt::ControlModifier){
            if (mic->wheel_delta() > 0.0) {
                set_brush_size(brush_size_ * 1.1);
            } else {
                if (brush_size_ > 0.01) {
                    set_brush_size(brush_size_ / 1.1);
                }
            }
            NotifyBrushSizeChanged();
            res = true;
        }
    }
    return res;
}

bool SketchingModeler::mouseReleased(std::shared_ptr<core::InputController> /*event*/)
{
    if (enabled_ && drawing_) {
        return true;
    }

    return false;
}

bool SketchingModeler::mousePressed  (std::shared_ptr<core::InputController> event)
{
    if (enabled_) {
        std::shared_ptr<MouseIC> e = dynamic_pointer_cast<MouseIC>(event);

        if ((e->button() == Qt::LeftButton)) {
            drawing_ = true;
            Point2 p = event->get2DInput();
            p[0] /= camera_width_;
            p[1] /= camera_height_;
//            depth_marker_->setVisible(true);
            if (current_node_ == nullptr) {
                ImplicitSurfaceSceneNode* n = dynamic_cast<ImplicitSurfaceSceneNode*>(hovered_item_);
                if (n != nullptr) {
                    set_current_node(n);
                }
            }

            if (e->modifiers() & Qt::ControlModifier) {
                DrawBrush(p[0], p[1], previous_brush_pos_[0], previous_brush_pos_[1], brush_size_, previous_brush_size_, brush_color_, symmetry_mode_);
            } else {
                DrawBrush(p[0], p[1], brush_size_, brush_color_, symmetry_mode_);
            }
            previous_brush_size_ = brush_size_;
            previous_brush_pos_ = p;
            show_distance_to_hovered_item_ = true;
            return true;
        } else if ((e->button() == Qt::RightButton) && drawing_) {
            Validate();
            return false;//true;
        } else if (e->button() == Qt::MiddleButton && drawing_) {
            Clear();
            return true;
        }
    }
    return false;
}

bool SketchingModeler::mouseMoved    (std::shared_ptr<core::InputController> event)
{
    if (enabled_) {
        std::shared_ptr<MouseIC> e = dynamic_pointer_cast<MouseIC>(event);
        Point2 p = event->get2DInput();
//        pencil_panel_->setPosition(p[0] / camera_width_ - brush_size_ / 2.0, p[1] / camera_height_ - brush_size_ / 2.0);

        if (hovered_item_ != nullptr) {
            hovered_item_->set_hovered(false);
        }
        if (!drawing_) {
            if (event->getPressed() == false) {
                Scalar dist;
                hovered_item_ = scene_->GetCameraRayIntersection(this->camera_, p[0], p[1], focus_point_, dist, true);

                Vector cameraDir = camera_->GetPointInWorld(Point(0.0, 0.0, -1.0)) - camera_->GetPointInWorld();
                Vector ray = Vector::Zero();
                if (hovered_item_ != nullptr) {
                    hovered_item_->set_hovered(false);
                    ray = focus_point_ - camera_->GetPointInWorld();
                    distance_to_hovered_item_ = ray.dot(cameraDir);
                    show_distance_to_hovered_item_ = true;
                } else {
//                    show_distance_to_hovered_item_ = false;
                }
                return true;
            } else {
                if (e->button() != Qt::LeftButton) {
                    show_distance_to_hovered_item_ = false;
                }
                return false;
            }
        } else {
            if (event->getPressed() && e->button() == Qt::LeftButton) {
                p[0] /= camera_width_;
                p[1] /= camera_height_;
                DrawBrush(p[0], p[1], previous_brush_pos_[0], previous_brush_pos_[1], brush_size_, previous_brush_size_, brush_color_, symmetry_mode_);
                previous_brush_size_ = brush_size_;
                previous_brush_pos_ = p;

                return true;
            }
        }
    }
    return false;
}

bool SketchingModeler::keyPressed(std::shared_ptr<core::InputController> event)
{
    std::shared_ptr<KeyboardIC> e = dynamic_pointer_cast<KeyboardIC>(event);
    if (e->key() == Qt::Key_S) {
        enable(!enabled_);
        NotifyEnableChanged();
        return true;
    }
    if (enabled_) {
        if (e->key() == Qt::Key_L) {
            LoadContourFromFile();
        }
        if (e->key() == Qt::Key_M) {
            LoadContourFromGraph("../../examples/wish/contours/herisson.txt");
        }
        if (e->key() == Qt::Key_P) {
            LoadContourFromGraph("../../examples/wish/contours/hippocampe.txt");
        }
    }

    return false;
}


bool SketchingModeler::resize(int width, int height)
{
    width_ = width;
    height_ = height;

    if (camera_ != nullptr) {
        camera_width_ = camera_->GetActualWidth();
        camera_height_ = camera_->GetActualHeight();
    }


    draw_map_ = make_shared<ImageT<Color> >(width, height);
    Clear();

    return false;
}

void SketchingModeler::updated(SceneManipulator *)
{
}

void SketchingModeler::updated(core::SceneManager *)
{
}

void SketchingModeler::SceneNodeAdded(NodeId /*node*/)
{
}

void SketchingModeler::set_brush_size(Scalar s)
{
    brush_size_ = s;
}

void SketchingModeler::set_sketch_mode(SketchMode m)
{
    sketch_mode_ = m;
}

void SketchingModeler::set_behavior_mode(BehaviorMode m)
{
    behavior_mode_ = m;
}

void SketchingModeler::set_symmetry_mode(SymmetryMode m)
{
    symmetry_mode_ = m;
}

void SketchingModeler::set_depth_selection_mode(DepthSelectionMode m )
{
    depth_selection_mode_ = m;
}

std::shared_ptr<core::SceneManager> SketchingModeler::scene_manager() const
{
    return scene_;
}

bool SketchingModeler::enabled() const
{
    return enabled_;
}

Scalar SketchingModeler::brush_size() const
{
    return brush_size_;
}

Color SketchingModeler::brush_color() const
{
    return brush_color_;
}

//    bool SketchingModeler::draw_on_surface() const { return draw_on_surface_; }
SketchingModeler::SketchMode SketchingModeler::sketch_mode() const
{
    return sketch_mode_;
}

SketchingModeler::BehaviorMode SketchingModeler::behavior_mode() const
{
    return behavior_mode_;
}

SketchingModeler::SymmetryMode SketchingModeler::symmetry_mode() const
{
    return symmetry_mode_;
}

SketchingModeler::DepthSelectionMode SketchingModeler::depth_selection_mode() const
{
    return depth_selection_mode_;
}

uint SketchingModeler::width() const
{
    return width_;
}

uint SketchingModeler::height() const
{
    return height_;
}

std::shared_ptr<core::ImageT<Color> > SketchingModeler::draw_map() const
{
    return draw_map_;
}

uint8 *SketchingModeler::texture_data() const
{
    return locked_texture_data_;
}

Scalar SketchingModeler::distance_to_hovered_item() const
{
    return distance_to_hovered_item_;
}

bool SketchingModeler::show_distance_to_hovered_item() const
{
    return show_distance_to_hovered_item_;
}

Point SketchingModeler::focus_point() const
{
    return focus_point_;
}

void SketchingModeler::DrawBrush(Scalar x, Scalar y, Scalar radius, const Color &c, SymmetryMode symmetry)
{
    Color uncolored = Color(0.f, 0.f, 0.f, 0.5f);

    uint w = (uint)(radius * min(width_, height_) / 2);
    uint h = (uint)(radius * min(width_, height_) / 2);

    int X = (int)(x * width_);
    int Y = (int)(y * height_);

    if (sketch_mode_ == E_FILL) {
        std::set<std::pair<int, int> > candidates;
        candidates.insert(make_pair(X, Y));
        while (candidates.size() > 0) {
            std::pair<int, int> candidate = *candidates.begin();
            candidates.erase(candidate);
            X = candidate.first;
            Y = candidate.second;
            (*draw_map_)(X, Y) = c;
            for (int i = X-1; i < X+2; ++i) {
                for (int j = Y-1; j < Y+2; ++j) {
                    if (i >=0 && i < (int)width_ && j >= 0 && j < (int)height_) {
                        if ((*draw_map_)(i, j) == uncolored) {
                            candidates.insert(make_pair(i, j));
                        }
                    }
                }
            }
        }
    } else {
        for(int i_x = (int)(X-w-1); i_x<=(int)(X+w+1); ++i_x)
        {
            for(int i_y = (int)(Y-h-1); i_y<=(int)(Y+h+1); ++i_y)
            {
                if(i_x>=0 && i_x<(int)width_ && i_y>=0 && i_y<(int)height_
                   && float((i_x-X)*(i_x-X)+(i_y-Y)*(i_y-Y))< (int)w*h)
                {
                    if (sketch_mode_ == E_PAINT) {
                        (*draw_map_)(i_x, i_y) = c;
                    } else if (sketch_mode_ == E_ERASE){
                        (*draw_map_)(i_x, i_y) = uncolored;
                    }
                }
            }
        }
    }

    if (symmetry != E_NO_SYMMETRY) {
        PointSymmetryList points(x, y, symmetry);
        for (unsigned int i = 0; i < points.GetSymmetryCount(); ++i) {
            Point2 &p = points.GetSymmetry(i);
            DrawBrush(p[0], p[1], radius, c);
        }
    }
}

void SketchingModeler::DrawBrush(Scalar x1, Scalar y1, Scalar x2, Scalar y2, Scalar radius1, Scalar radius2, const Color &c, SymmetryMode symmetry)
{
    Scalar curX = x1;
    Scalar curY = y1;
    Scalar curRadius = radius1;
    Scalar len = sqrt(std::pow(x2-x1, 2) + std::pow(y2-y1, 2));

    Scalar noffset = 4.0 * len / min(radius1, radius2);

    Scalar XOffset = (x2 - x1) / noffset;
    Scalar YOffset = (y2 - y1) / noffset;
    Scalar RadiusOffset = (radius2 - radius1) / noffset;

    for (Scalar i = 0.0; i < noffset; ++i) {
        DrawBrush(curX, curY, curRadius, c, symmetry);

        curX += XOffset;
        curY += YOffset;
        curRadius += RadiusOffset;
    }
}

void SketchingModeler::DrawPolygon(const Polygon2D& p, const Color &c)
{
    Color uncolored = Color(0.f, 0.f, 0.f, 0.5f);

    AABBox2D b = p.bounding_box();
    Scalar wScale = b.width() / width_;
    Scalar hScale = b.height() / height_;
    Scalar xOffset = b.min()[0];
    Scalar yOffset = b.min()[1];
    for (unsigned int i = 0; i < width_; ++i) {
        for (unsigned int j = 0; j < height_; ++j) {
            if (p.contains(Vector2(i * wScale + xOffset, j * hScale + yOffset))) {
                if (sketch_mode_ == E_PAINT) {
                    (*draw_map_)(i, j) = c;
                } else if (sketch_mode_ == E_ERASE){
                    (*draw_map_)(i, j) = uncolored;
                }
            }
        }
    }
}

std::shared_ptr<core::WeightedGraph2D> SketchingModeler::ExtractSkeleton()
{
    std::lock_guard<std::mutex> mlock(mutex_);
    Color uncolored = Color(0.f, 0.f, 0.f, 0.5f);

    //// Get Contour
    ImageT<Color> contours = draw_map_->Contours(uncolored, Color(0, 0, 0, 0));//uncolored);
    contours_.CopyDataFrom(contours);
//    DisplayImage(contours);
    ///// Create distance map.
    ImageT<float> distances(draw_map_->width(), draw_map_->height());
    // 5 7 11 = default distance filter parameters. TODO : make them editable.
    DistanceImageTools::BuildDistanceImage<float, Color>(1.f, sqrt(2.f), sqrt(5.f), *draw_map_, uncolored, distances);
//    DistanceImageTools::BuildDistanceImage<int, Color>(5, 7, 11, *draw_map_, uncolored, distances);
    distances_.CopyDataFrom(distances);

    //// Extract contours as list of branches.
    std::vector<std::list<Skeletonizer::PixelInfo> *> branches;
    Skeletonizer::ExtractLines(branches, contours);

    // TODO refactor : too many copies !
    //// Make a first pass to simplify those branches : Applying RDP's simplification algorithm on each branch.
    simplified_branches_.clear();
//    vector<vector<Point2> > simplified_branches_;
//    vector<vector<Point2> > original_branches;
    for (unsigned int i = 0; i < branches.size(); ++i) {
        simplified_branches_.push_back(vector<Point2> ());
        vector<Point2> input;
        for (auto it : *(branches[i])) {
            input.push_back(it.pos);
        }
        VectorTools::Simplify(input, simplified_branches_.back(), 1);
    }

    for (unsigned int i = 0; i < branches.size(); ++i) {
        delete branches[i];
    }

    ///// Create a voronoi diagram from the given contours.
    res_voronoi_.clear();
    Skeletonizer::ExtractSkeletonVoronoi(simplified_branches_, res_voronoi_);

    ///// Extract a graph from that voronoi diagram. This method also removes everything that cannot possibly be part of
    /// the skeleton (i.e. outside of the distance map).
    /// We also simplify the resulting graph by merging vertices that are too close from one another.
    unfiltered_graph_ = Skeletonizer::CreateFilteredGraph(distances, res_voronoi_);
    unfiltered_graph_->MergeCloseVertices(1.0 * width_ / 100.0, false);


//    //// Apply SAT on the resulting graph to remove unnecessary segments (if they are absorbed, they can be eliminated).
//    SATSegmentFilteringVisitor<WeightedGraph2D> visitor(unfiltered_graph_, 1.5, true);
//    std::set<WeightedGraph2D::VertexId> extremities;
//    unfiltered_graph_->GetExtremities(extremities);
//    for (auto it : extremities) {
//        try {
//            boost::breadth_first_search(*unfiltered_graph_, it, boost::visitor(visitor).color_map(unfiltered_graph_->vertex_color_property_map()).vertex_index_map(unfiltered_graph_->vertex_index_property_map()));
//        } catch (int err) {
//            if (err != 1) {
//                throw (err);
//            }
//        }
//    }

//    std::cout << "unfiltered " << unfiltered_graph_->to_string(false) << std::endl;

    ///// Finally retrieves the resulting 2D graph (and possible the residuals, but not required here).
    std::shared_ptr<WeightedGraph2D> final = unfiltered_graph_;
//    std::shared_ptr<WeightedGraph2D> final = visitor.GetFilteredGraph();

    GraphAlgorithms<WeightedGraph2D>::SimplifyVisitor simplifier;
    simplifier.Simplify(final, 20.0);//2.0 * width_ / 100.0);

    return final;
}

void SketchingModeler::AddSkeletonToScene(std::shared_ptr<core::WeightedGraph2D> skeleton)
{
    if (skeleton == nullptr) {
        return;
    }
    if (current_node_ == nullptr) {
        if (hovered_item_ != nullptr) {
            current_node_ = dynamic_cast<ImplicitSurfaceSceneNode*>(hovered_item_->ancestor());
            if (current_node_ == nullptr) {
                current_node_ = CreateImplicitSurfaceSceneNode().get();
            }
        } else {
            current_node_ = CreateImplicitSurfaceSceneNode().get();
        }
    }


    Point target = camera_->target();

    // add the item inside the plane of currently hovered item.
    if (distance_to_hovered_item_> 0.0) {
        target = camera_->GetPointInWorld(Point(0.0, 0.0, -distance_to_hovered_item_));
    }

//    std::shared_ptr<WeightedGeometryPrimitive> s = PrimitiveFactory::GetInstance().CreateNewPrimitive(make_shared<WeightedSegment>)
    WeightedGraph* graph = current_node_->convol_object()->graph();
    std::shared_ptr<BlobtreeRoot> bt_root = current_node_->convol_object()->blobtree_root();

    Scalar xratio = (Scalar)camera_width_ / width_;
    Scalar yratio = (Scalar)camera_height_ / height_;

    map<VertexId, VertexId> ids;
//    auto edges = skeleton->GetEdges();

    const WeightedGraph2D::PrimitiveMap &primitives = skeleton->GetPrimitives();

    // Color properties
    Properties0D prop0;
    prop0.SetScalarProperty(expressive::ESFScalarProperties::E_Red, brush_color_[0]);
    prop0.SetScalarProperty(expressive::ESFScalarProperties::E_Green, brush_color_[1]);
    prop0.SetScalarProperty(expressive::ESFScalarProperties::E_Blue, brush_color_[2]);

    Properties1D prop1;
    prop1.SetScalarPropertyConstraints(expressive::ESFScalarProperties::E_Red, PropConstraint1DT<Scalar>(0.0, brush_color_[0]));
    prop1.SetScalarPropertyConstraints(expressive::ESFScalarProperties::E_Green, PropConstraint1DT<Scalar>(0.0, brush_color_[1]));
    prop1.SetScalarPropertyConstraints(expressive::ESFScalarProperties::E_Blue, PropConstraint1DT<Scalar>(0.0, brush_color_[2]));


    if (primitives.size() == 0 && skeleton->GetVertexCount() == 0) {
        ork::Logger::ERROR_LOGGER->log("SKETCHING", "Nothing added to the shape. Maybe you drew something too small ?");
        return;
    }

    Scalar distance_to_surface = expressive::kScalarMax;
    Vector cameraDir = camera_->GetPointInWorld(Point(0.0, 0.0, -1.0)) - camera_->GetPointInWorld();

    WeightedGraph2D::VertexIteratorPtr vertices = skeleton->GetVertices();
    while(vertices->HasNext()) {
        std::shared_ptr<WeightedVertex2D> v = vertices->Next();
        if (v->GetEdgeCount()) {
            continue;
        }
        Point2 p = v->pos();
        p[0] *= xratio;
        p[1] *= yratio;
        distance_to_surface = -1.0;
        std::shared_ptr<WeightedPoint> cur_vertex = nullptr;
        // create 3D equivalent of point.
        if (depth_selection_mode_ == E_SURFACE && hovered_item_ != nullptr) {
            BasicRay ray = camera_->GetBasicRay(p);
            SceneNode *n = hovered_item_->IsIntersectingRay(ray, distance_to_surface, false);
            if (n != nullptr) {
                Point tmp = ray.direction() * distance_to_surface;
                target = camera_->GetPointInLocal(target);
                target[2] = -tmp.dot(cameraDir);//-distance_to_surface;
                target = camera_->GetPointInWorld(target);
            }
        }

        Point pos3D = camera_->GetPointInParallelWorldPlane(target, p[0], p[1]);
        Scalar w3D = (camera_->GetPointInParallelWorldPlane(target, p[0], p[1] + v->pos().weight() * yratio) - pos3D).norm();
        cur_vertex = std::make_shared<WeightedPoint>(pos3D, w3D);

        std::shared_ptr<Primitive> s = PrimitiveFactory::GetInstance().CreateNewPrimitive(cur_vertex);
        auto wps = dynamic_pointer_cast<WeightedPoint>(s);
        auto npss = dynamic_pointer_cast<AbstractNodePointSField>(s);
        if (wps != nullptr) {
            auto v3D = current_node_->convol_object()->graph()->Add(wps);
            ids[v->id()] = v3D->id();
        }

        if (parent_node_ == nullptr) {
            parent_node_= BlobtreeNodeFactory::GetInstance().CreateNewBlobtreeNode("CorrectedSumIntegralSurface", npss->kernel_functor());//make_shared<OptCorrectedSumIntegralSurfaceT<>> (npss->kernel_functor());
            bt_root->AddNewNodeToRoot(parent_node_);
        }

        if (npss != nullptr) {
            npss->set_properties(prop0);
            if (behavior_mode_ == E_ADD) {
                bt_root->AddChildToNAryNode(parent_node_, npss);
            } else if (behavior_mode_ == E_REMOVE) {
                // Add a Blenddiff node above existing node.
                std::shared_ptr<BlobtreeNode> diff = make_shared<NodeOperator2T<BlendDiff> > ();
                bt_root->InsertBinaryNode(parent_node_, diff, npss);

                // todo : attach parent_node to the hovered_item.
                // And re-add a BlendSum so we can still add material as if nothing happened, and add diff inside of it.
//                parent_node_= make_shared<NodeOperatorNArityT<BlendSum> > ();
                parent_node_= BlobtreeNodeFactory::GetInstance().CreateNewBlobtreeNode("CorrectedSumIntegralSurface", npss->kernel_functor());//make_shared<OptCorrectedSumIntegralSurfaceT<>> (npss->kernel_functor());

                bt_root->AddNewNodeToRoot(parent_node_);
                bt_root->AddChildToNAryNode(parent_node_, diff);
            } else {
//                Logger::ERROR_LOGGER->log("SKETCHING", "Cut mode not defined yet.");
            }
        }
    }

    for (auto it : primitives) {
        WeightedGraph2D::EdgeIteratorPtr edges = primitives.GetEdges(it.first);
        std::vector<std::shared_ptr<WeightedVertex> > vertices;

        uint begin = 0;

        while (edges->HasNext()) {
            std::shared_ptr<WeightedEdge2D> edge = edges->Next();
            for (uint i = begin; i < 2; ++i) {
                shared_ptr<WeightedVertex2D> v = edge->GetVertex(i);
                Point2 p = v->pos();
                p[0] *= xratio;
                p[1] *= yratio;
                auto id  = ids.find(v->id());
                distance_to_surface = -1.0;
                if (id != ids.end()) {
                    vertices.push_back(graph->Get(id->second));
                } else {
                    // create 3D equivalent of point.
                    if (depth_selection_mode_ == E_SURFACE && hovered_item_ != nullptr) {
                        BasicRay ray = camera_->GetBasicRay(p);
                        SceneNode *n = hovered_item_->IsIntersectingRay(ray, distance_to_surface, false);
                        if (n != nullptr) {
                            Point tmp = ray.direction() * distance_to_surface;
                            target = camera_->GetPointInLocal(target);
                            target[2] = -tmp.dot(cameraDir);//-distance_to_surface;
                            target = camera_->GetPointInWorld(target);
//                            cout << "distance : " << distance_to_surface << endl;
                        }
                    }

                    Point pos3D = camera_->GetPointInParallelWorldPlane(target, p[0], p[1]);
                    Scalar w3D = (camera_->GetPointInParallelWorldPlane(target, p[0], p[1] + v->pos().weight() * yratio) - pos3D).norm();
                    auto v3D = graph->Add(make_shared<WeightedPoint>(pos3D, w3D));
                    ids[v->id()] = v3D->id();
                    vertices.push_back(v3D);
                }
            }
            begin = 1;
        }

#ifdef USE_SUBDIVISION_CURVES
        std::shared_ptr<WeightedSubdivisionCurve> seg = make_shared<WeightedSubdivisionCurve>();
#else
        std::shared_ptr<WeightedSegment> seg = make_shared<WeightedSegment>();
#endif
        for (uint i = 0; i < vertices.size(); ++i) {
            seg->SetPoint(i, vertices[i]->pos());
        }

        std::shared_ptr<Primitive> s = PrimitiveFactory::GetInstance().CreateNewPrimitive(seg);
        auto wgps = dynamic_pointer_cast<WeightedGeometryPrimitive>(s);
#ifdef USE_SUBDIVISION_CURVES
        auto niss = dynamic_pointer_cast<AbstractNodeISSubdivisionCurve>(s);
#else
        auto niss = dynamic_pointer_cast<AbstractNodeSegmentSField>(s);
#endif
        if (wgps != nullptr) {
            current_node_->convol_object()->graph()->Add(wgps, vertices);
        }

        if (parent_node_ == nullptr) {
//            parent_node_= make_shared<NodeOperatorNArityT<BlendSum> > ();
            parent_node_= BlobtreeNodeFactory::GetInstance().CreateNewBlobtreeNode("CorrectedSumIntegralSurface", niss->kernel_functor());//make_shared<OptCorrectedSumIntegralSurfaceT<>> (npss->kernel_functor());
            bt_root->AddNewNodeToRoot(parent_node_);
        }

        if (niss != nullptr) {
            niss->set_properties(prop1);
            if (behavior_mode_ == E_ADD) {
                bt_root->AddChildToNAryNode(parent_node_, niss);
            } else if (behavior_mode_ == E_REMOVE) {
                // Add a Blenddiff node above existing node.
                std::shared_ptr<BlobtreeNode> diff = make_shared<NodeOperator2T<BlendDiff> > ();
                bt_root->InsertBinaryNode(parent_node_, diff, niss);

                // todo : attach parent_node to the hovered_item.
                // And re-add a BlendSum so we can still add material as if nothing happened, and add diff inside of it.
//                parent_node_= make_shared<NodeOperatorNArityT<BlendSum> > ();
                parent_node_= BlobtreeNodeFactory::GetInstance().CreateNewBlobtreeNode("CorrectedSumIntegralSurface", niss->kernel_functor());//make_shared<OptCorrectedSumIntegralSurfaceT<>> (npss->kernel_functor());
                bt_root->AddNewNodeToRoot(parent_node_);
                bt_root->AddChildToNAryNode(parent_node_, diff);
            } else {
                ork::Logger::ERROR_LOGGER->log("SKETCHING", "Cut mode not defined yet.");
            }
        }
    }

    graph->NotifyUpdate();
}

std::shared_ptr<ImplicitSurfaceSceneNode> SketchingModeler::CreateImplicitSurfaceSceneNode()
{
    shared_ptr<expressive::convol::Skeleton> skel = make_shared<expressive::convol::Skeleton>();
    shared_ptr<ConvolObject> convol_object = std::make_shared<ConvolObject>(skel, skel->nonconst_blobtree_root(),
                                                   std::make_shared<ImplicitSurface>(skel->nonconst_blobtree_root(), 1.0, 0.001));

    shared_ptr<expressive::plugintools::ImplicitSurfaceSceneNode> is_scene_node =
        make_shared<expressive::plugintools::ImplicitSurfaceSceneNode>(scene_.get(), nullptr, "object1",
                                                           convol_object, Vector::Ones(), true);

    is_scene_node->displayed_skeleton()->set_visible(show_skeleton_);
    scene_->AddNode(is_scene_node);
    scene_->NotifyUpdate();

    return is_scene_node;
}

void SketchingModeler::LoadContourFromGraph(const string &filename)
{
    enable(true);
//    depth_marker_->setVisible(true);
    std::shared_ptr<GraphProfile> profile = SceneManipulatorListener::target_->engine()->resource_manager()->loadResource<GraphProfile>("contour2DGraphProfile");
    std::shared_ptr<Graph2D> g = profile->LoadGraph<Graph2D>(filename);

    Polygon2D p;
    std::shared_ptr<Edge2D> e = g->GetEdges()->Next();
    std::shared_ptr<Vertex2D> cur   = e->GetEnd();
    std::shared_ptr<Vertex2D> first = e->GetStart();
    p.add_vertex(first->pos());
    while (cur != first) {
        p.add_vertex(cur->pos());
        Graph2D::EdgeIteratorPtr edges = cur->GetEdges();
        std::shared_ptr<Edge2D> tmpE = edges->Next();
        if (tmpE == e) {
            tmpE = edges->Next();
        }
        e = tmpE;
        cur = e->GetOpposite(cur);
    }

    std::shared_ptr<WeightedGraph2D> res = ExtractSkeleton();
    AddSkeletonToScene(res);
    Clear();
}

void SketchingModeler::LoadContourFromFile()
{
     QString directory = Config::getStringParameter("default_examples_path").c_str();

     QString loaded_file = QFileDialog::getOpenFileName(nullptr, "Load File", directory, "Graph File (*.txt);;All Files (*)");
     LoadContourFromGraph(loaded_file.toStdString());
}

} // namespace plugintools

} // namespace expressive
