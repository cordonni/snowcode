#include "pluginTools/sketching/DrawSketchTask.h"

#include "core/ExpressiveEngine.h"
#include "pluginTools/sketching/SketchingModeler.h"

#include <GL/glew.h>
//#ifdef __APPLE__
////#include <GLUT/glut.h>
//#else
//#include <GL/glut.h>
//#endif

using namespace ork;
using namespace std;
using namespace expressive;
using namespace expressive::core;

namespace expressive
{

namespace plugintools
{

DrawSketchTask::DrawSketchTask(SketchingModeler *owner, bool draw_texture)
    : AbstractTask("DrawSketchTask"),
      owner_(owner),
      tex_(new Texture2D(owner->width(), owner->height(), ork::RGBA8, ork::RGBA, ork::UNSIGNED_BYTE, Texture::Parameters(), Buffer::Parameters(), CPUBuffer())),
      floatTex_(new Texture2D(owner->width(), owner->height(), ork::R32F, ork::RED, ork::FLOAT, Texture::Parameters(), Buffer::Parameters(), CPUBuffer()))
{
    program_ = ExpressiveEngine::GetSingleton()->resource_manager()->loadResource("mouse;sketching;").cast<Program>();
//    tex_ = ExpressiveEngine::GetSingleton()->resource_manager()->loadResource("checker").cast<Texture2D>();
    texU_ = program_->getUniformSampler("tex");
    floatTexU_ = program_->getUniformSampler("floatTex");

    worldToScreenU_ = program_->getUniformMatrix4f("worldToScreen");
    draw_floatsU_ = program_->getUniform1f("drawFloats");
    draw_debugU_ = program_->getUniform1f("drawDebug");
    draw_textureU_ = program_->getUniform1f("drawTexture");

    distanceU_ = program_->getUniform1f("distanceToScene");
    focusPointU_ = program_->getUniform3f("focusPoint");
    brush_sizeU_ = program_->getUniform1f("brushSize");
    brush_colorU_ = program_->getUniform4f("brushColor");

    draw_textureU_->set(draw_texture ? 1.f : 0.f);
    texU_->set(tex_);
    floatTexU_->set(floatTex_);


}


ork::ptr<ork::Task> DrawSketchTask::getTask(ork::ptr<ork::Object> /*context*/)
{
    return new Impl(this);
}

DrawSketchTask::Impl::Impl(DrawSketchTask *owner) :
    ork::Task("DrawSketch", true, 0),
    owner_(owner)
{
}

DrawSketchTask::Impl::~Impl()
{
}

bool DrawSketchTask::Impl::run()
{
    if (owner_->owner_->enabled()) {
        ork::ptr<ork::FrameBuffer> fb = ork::SceneManager::getCurrentFrameBuffer();
        std::lock_guard<std::mutex> mlock(owner_->owner_->mutex_);

        // setup common values
        owner_->distanceU_->set((GLfloat)owner_->owner_->distance_to_hovered_item());
        owner_->focusPointU_->set(owner_->owner_->focus_point().cast<float>());
        owner_->brush_colorU_->set(owner_->owner_->brush_color());
        owner_->brush_sizeU_->set((GLfloat)owner_->owner_->brush_size());

        // setup base sketching drawing.
        owner_->draw_debugU_->set(0.f);
        owner_->draw_floatsU_->set(0.f);
        owner_->tex_->setImage(owner_->owner_->width(), owner_->owner_->height(), ork::RGBA, ork::FLOAT, CPUBuffer(owner_->owner_->draw_map()->tab()));

        if (owner_->owner_->show_distance_to_hovered_item()) { // distance plane rendering
            owner_->draw_textureU_->set(0.f);
            owner_->worldToScreenU_->setMatrix(ExpressiveEngine::GetSingleton()->camera_manager()->GetWorldToScreen().cast<float>());
            fb->draw2DQuad(owner_->program_);

        }
        // drawing texture rendering
        owner_->draw_textureU_->set(1.0f);
        owner_->distanceU_->set((GLfloat)owner_->owner_->distance_to_hovered_item());
        fb->draw2DQuad(owner_->program_);

        if (false) {
            owner_->draw_debugU_->set(0.f);
            // setup debug drawings
            // First : contours
            owner_->tex_->setImage(owner_->owner_->width(), owner_->owner_->height(), ork::RGBA, ork::FLOAT, CPUBuffer(owner_->owner_->contours_.tab()));
            fb->draw2DQuad(owner_->program_);

            // then distances.
            owner_->draw_floatsU_->set(1.f);
            owner_->floatTex_->setImage(owner_->owner_->width(), owner_->owner_->height(), ork::RED, ork::FLOAT, CPUBuffer(owner_->owner_->distances_.tab()));
            fb->draw2DQuad(owner_->program_);
        }
    }

    return true;
}

} // namespace plugintools

} // namespace expressive
