/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <pluginTools/sketching/SketchingModeler.h>
#include "pluginTools/gui/qml/QmlHandlerBase.h"

#include <qtTools/QtTools.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include <QQmlParserStatus>
#include <QQuickItem>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{

namespace plugintools
{

/**
 * @brief The QmlSketchingModeler class allows to use a SketchingModeler from a QML Context.
 * @see SketchingModeler
 * Note that a few properties MUST be defined in the qml file before using a QMLSketchingModeler :
 * - engine : The QMLExpressiveEngine that contains the context, scene related classes.
 * A few other may be defined in the qml file, that will influence the rendering/computation :
 * - width/height : the width and height of the grid used to draw.
 * - sketch_mode : Either E_ADD, E_ERASE or E_CUT, to be able to draw or erase material from an object. @see QmlSketchingModeler::QMLSketchMode
 * - symmetry_mode : User can enable symmetry in either directions. @see QmlSketchingModeler::QMLSymmetryMode
 * - depth_selection_mode : User can select the type of projection the sketching will do (either planar or surfacic). @see QmlSketchingModeler::QMLDepthSelectionMode
 * - brush_size : default pencil brush size.
 * - color : default pencil brush color.
 */
class PLUGINTOOLS_API QmlSketchingHandler: public QmlHandlerBase, public SketchingModelerListener
{
    Q_OBJECT

    Q_ENUMS(QMLSketchMode)
    Q_ENUMS(QMLBehaviorMode)
    Q_ENUMS(QMLSymmetryMode)
    Q_ENUMS(QMLDepthSelectionMode)

    Q_PROPERTY(uint width READ width /*WRITE set_width */NOTIFY width_changed)
    Q_PROPERTY(uint height READ height /*WRITE set_height */NOTIFY height_changed)
    Q_PROPERTY(QMLSketchMode sketch_mode READ sketch_mode WRITE set_sketch_mode NOTIFY sketch_mode_changed)
    Q_PROPERTY(QMLBehaviorMode behavior_mode READ behavior_mode WRITE set_behavior_mode NOTIFY behavior_mode_changed)
    Q_PROPERTY(QMLSymmetryMode symmetry_mode READ symmetry_mode WRITE set_symmetry_mode NOTIFY symmetry_mode_changed)
    Q_PROPERTY(QMLDepthSelectionMode depth_selection_mode READ depth_selection_mode WRITE set_depth_selection_mode NOTIFY depth_selection_mode_changed)
    Q_PROPERTY(double brush_size READ brush_size WRITE set_brush_size NOTIFY brush_size_changed)
    Q_PROPERTY(QColor color READ qcolor WRITE set_color NOTIFY color_changed)
    Q_PROPERTY(bool enabled READ enabled WRITE enable NOTIFY enable_state_changed)
    Q_PROPERTY(bool show_skeleton READ show_skeleton WRITE show_skeleton NOTIFY show_skeleton_state_changed)

public:
    enum QMLSketchMode {
        E_PAINT = 0,
        E_ERASE,
        E_FILL
    };

    enum QMLBehaviorMode {
        E_ADD = 0,
        E_REMOVE,
        E_CUT
    };

    typedef plugintools::SketchingModeler::SketchMode SketchMode;
    typedef plugintools::SketchingModeler::BehaviorMode BehaviorMode;
    typedef plugintools::SketchingModeler::SymmetryMode SymmetryMode;
    typedef plugintools::SketchingModeler::DepthSelectionMode DepthSelectionMode;

    enum QMLSymmetryMode {
        E_NO_SYMMETRY = 0,
        E_VERTICAL_SYMMETRY = 1,
        E_HORIZONTAL_SYMMETRY = 2,
        E_BOTH_SYMMETRY = 3
    };

    enum QMLDepthSelectionMode {
        E_PLANE,
        E_SURFACE
    };

    QmlSketchingHandler(GLExpressiveEngine *engine = nullptr, SketchingModeler *sketcher = nullptr);

    virtual ~QmlSketchingHandler();

    virtual void scene_initialized();

    /////
    /// QML SETTERS
    ///
//    void set_width(uint width) { m_sketcher->set_width(width); emit width_changed(); }
//    void set_height(uint height) { height_ = height; emit height_changed(); }
    void set_brush_size(double s);
    void set_color(QColor color);
    void set_sketch_mode(QMLSketchMode m);
    void set_behavior_mode(QMLBehaviorMode m);
    void set_symmetry_mode(QMLSymmetryMode m);
    void set_depth_selection_mode(QMLDepthSelectionMode m);
    void enable(bool b);
    void show_skeleton(bool b);

    /////
    /// QML GETTERS
    ///
    uint width() const;
    uint height() const;
    double brush_size() const;
    QColor qcolor() const;
    QMLSketchMode sketch_mode() const;
    QMLBehaviorMode behavior_mode() const;
    QMLSymmetryMode symmetry_mode() const;
    QMLDepthSelectionMode depth_selection_mode() const;
    bool enabled() const;
    bool show_skeleton() const;

    /////
    /// SketchingModeler Events
    ///
    void brushSizeChanged();
    void enableStateChanged();
    void showSkeletonStateChanged();

public slots:
    void validate();
    void clear();

signals:
    void width_changed();
    void height_changed();
    void color_changed();
    void brush_size_changed();
    void sketch_mode_changed();
    void behavior_mode_changed();
    void symmetry_mode_changed();
    void depth_selection_mode_changed();
    void enable_state_changed();
    void show_skeleton_state_changed();


protected:
    std::shared_ptr<SketchingModeler> m_sketcher;

    double m_init_brush_size;
    QColor m_init_brush_color;
    QMLSketchMode m_init_sketch_mode;
    QMLBehaviorMode m_init_behavior_mode;
    QMLSymmetryMode m_init_symmetry_mode;
    QMLDepthSelectionMode m_init_depth_mode;
    bool m_init_enabled;
    bool m_init_show_skeleton;

};

} // namespace plugintools

} // namespace expressive
