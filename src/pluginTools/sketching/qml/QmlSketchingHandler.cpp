#include <pluginTools/sketching/qml/QmlSketchingHandler.h>
#include <pluginTools/gui/qml/QmlTypeRegisterer.h>

namespace expressive
{

namespace plugintools
{

QmlSketchingHandler::QmlSketchingHandler(GLExpressiveEngine *engine, SketchingModeler *sketcher) :
    QmlHandlerBase(engine),
    SketchingModelerListener(sketcher),
    m_sketcher(sketcher),
    m_init_brush_size(0.05),
    m_init_brush_color(QColor(100, 100, 200, 200)),
    m_init_sketch_mode(E_PAINT),
    m_init_behavior_mode(E_ADD),
    m_init_symmetry_mode(E_NO_SYMMETRY),
    m_init_depth_mode(E_SURFACE),
    m_init_enabled(false),
    m_init_show_skeleton(false)
{
}

QmlSketchingHandler::~QmlSketchingHandler()
{
}

void QmlSketchingHandler::scene_initialized()
{
    if (m_sketcher == nullptr) {
        m_sketcher = std::make_shared<SketchingModeler>(m_engine->scene_manipulator().get());
        m_engine->scene_manipulator()->AddListener(m_sketcher.get());

        target_ = m_sketcher.get();
        target_->AddListener(this);

        m_sketcher->set_brush_color(qttools::QtToColor(m_init_brush_color));
        m_sketcher->set_brush_size(m_init_brush_size);
        m_sketcher->set_sketch_mode((SketchMode)m_init_sketch_mode);
        m_sketcher->set_behavior_mode((BehaviorMode)m_init_behavior_mode);
        m_sketcher->set_symmetry_mode((SymmetryMode)m_init_symmetry_mode);
        m_sketcher->set_depth_selection_mode((DepthSelectionMode)m_init_depth_mode);
        m_sketcher->enable(m_init_enabled);
        m_sketcher->show_skeleton(m_init_show_skeleton);
    }
//        if (m_sketcher == nullptr && m_engine != nullptr && m_engine->scene_manipulator() != nullptr) {
//            for (core::ListenerT<SceneManipulator> *l : m_engine->scene_manipulator()->GetListeners()) {
//                SketchingModeler *sm = dynamic_cast<SketchingModeler*>(l);
//                if (sm != nullptr && sm->Name() == "SketchingModeler") {
//                    m_sketcher = sm;
//                    break;
//                }
//            }
//        }
    emit width_changed();
    emit height_changed();
    emit color_changed();
    emit brush_size_changed();
    emit sketch_mode_changed();
    emit behavior_mode_changed();
    emit symmetry_mode_changed();
    emit depth_selection_mode_changed();
    emit show_skeleton_state_changed();
    QmlHandlerBase::scene_initialized();
}

/////
/// QML SETTERS
///
//    inline void set_width(uint width) { m_sketcher->set_width(width); emit width_changed(); }
//    inline void set_height(uint height) { height_ = height; emit height_changed(); }
void QmlSketchingHandler::set_brush_size(double s)
{
    if (m_sketcher != nullptr) {
        m_sketcher->set_brush_size(s);
        emit brush_size_changed();
    } else {
        m_init_brush_size = s;
    }
}

void QmlSketchingHandler::set_color(QColor color)
{
    if (m_sketcher != nullptr) {

        m_sketcher->set_brush_color(qttools::QtToColor(color));
        emit color_changed();
    } else {
        m_init_brush_color = color;
    }
}

void QmlSketchingHandler::set_sketch_mode(QMLSketchMode m)
{
    if (m_sketcher != nullptr) {
        m_sketcher->set_sketch_mode((SketchMode)m);
        emit sketch_mode_changed();
    } else {
        m_init_sketch_mode = m;
    }
}

void QmlSketchingHandler::set_behavior_mode(QMLBehaviorMode m)
{
    if (m_sketcher != nullptr) {
        m_sketcher->set_behavior_mode((BehaviorMode)m);
        emit behavior_mode_changed();
    } else {
        m_init_behavior_mode = m;
    }
}

void QmlSketchingHandler::set_symmetry_mode(QMLSymmetryMode m)
{
    if (m_sketcher != nullptr) {
        m_sketcher->set_symmetry_mode((SymmetryMode)m);
        emit symmetry_mode_changed();
    } else {
        m_init_symmetry_mode = m;
    }
}

void QmlSketchingHandler::set_depth_selection_mode(QMLDepthSelectionMode m)
{
    if (m_sketcher != nullptr) {
        m_sketcher->set_depth_selection_mode((DepthSelectionMode)m);
        emit depth_selection_mode_changed();
    } else {
        m_init_depth_mode = m;
    }
}

void QmlSketchingHandler::enable(bool b)
{
    if (m_sketcher != nullptr) {
        m_sketcher->enable(b);
        emit enable_state_changed();
    } else {
        m_init_enabled = b;
    }
}

void QmlSketchingHandler::show_skeleton(bool b)
{
    if (m_sketcher != nullptr) {
        m_sketcher->show_skeleton(b);
        emit show_skeleton_state_changed();
    } else {
        m_init_show_skeleton = b;
    }
}

void QmlSketchingHandler::validate()
{
    m_sketcher->Validate();
}

void QmlSketchingHandler::clear()
{
    m_sketcher->Clear();
}

/////
/// QML GETTERS
///
uint QmlSketchingHandler::width() const
{
    if (m_sketcher == nullptr) {
        return 0;
    }
    return m_sketcher->width();
}

uint QmlSketchingHandler::height() const
{
    if (m_sketcher == nullptr) {
        return 0;
    }
    return m_sketcher->height();
}

double QmlSketchingHandler::brush_size() const
{
    if (m_sketcher == nullptr) {
        return m_init_brush_size;
    }
    return m_sketcher->brush_size();
}

QColor QmlSketchingHandler::qcolor() const
{
    if (m_sketcher == nullptr) {
        return m_init_brush_color;
    }
    return qttools::ColorToQt(m_sketcher->brush_color());
}

QmlSketchingHandler::QMLSketchMode QmlSketchingHandler::sketch_mode() const
{
    if (m_sketcher == nullptr) {
        return m_init_sketch_mode;
    }
    return (QMLSketchMode)m_sketcher->sketch_mode();
}

QmlSketchingHandler::QMLBehaviorMode QmlSketchingHandler::behavior_mode() const
{
    if (m_sketcher == nullptr) {
        return m_init_behavior_mode;
    }
    return (QMLBehaviorMode)m_sketcher->behavior_mode();
}

QmlSketchingHandler::QMLSymmetryMode QmlSketchingHandler::symmetry_mode() const
{
    if (m_sketcher == nullptr) {
        return m_init_symmetry_mode;
    }
    return (QMLSymmetryMode) m_sketcher->symmetry_mode();
}

QmlSketchingHandler::QMLDepthSelectionMode QmlSketchingHandler::depth_selection_mode() const
{
    if (m_sketcher == nullptr) {
        return m_init_depth_mode;
    }
    return (QMLDepthSelectionMode) m_sketcher->depth_selection_mode();
}

bool QmlSketchingHandler::enabled() const
{
    if (m_sketcher == nullptr) {
        return m_init_enabled;
    }
    return m_sketcher->enabled();
}

bool QmlSketchingHandler::show_skeleton() const
{
    if (m_sketcher == nullptr) {
        return m_init_show_skeleton;
    }
    return m_sketcher->show_skeleton();
}

void QmlSketchingHandler::brushSizeChanged()
{
    emit brush_size_changed();
}

void QmlSketchingHandler::enableStateChanged()
{
    emit enable_state_changed();
}

void QmlSketchingHandler::showSkeletonStateChanged()
{
    emit show_skeleton_state_changed();
}

void sketchingRegisterer()
{
    qmlRegisterType<QmlSketchingHandler>("Expressive", 1, 0, "SketchingModeler");
}

//static QmlTypeRegisterer::Type<QmlSketchingHandler> QmlSketchingHandlerType ("Expressive", 1, 0, "SketchingModeler");
QML_TYPE_REGISTERER_CALLBACK(sketchingRegisterer)

} // namespace plugintools

} // namespace expressive
