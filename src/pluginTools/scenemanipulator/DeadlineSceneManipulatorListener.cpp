#include "pluginTools/scenemanipulator/DeadlineSceneManipulatorListener.h"

// core dependencies
#include "core/scenegraph/SceneCommands.h"
#include "core/utils/KeyBindingsList.h"

// convol dependencies
#include "convol/blobtreenode/BlobtreeCommands.h"
#include "convol/blobtreenode/nodeoperator/GradientBasedIntegralSurfaceT.h"

// plugintools dependencies
#include "pluginTools/convol/ImplicitSurfaceSceneNode.h"

// ork dependencies
#include "ork/core/Logger.h"

using namespace expressive::core;
using namespace expressive::convol;
using namespace std;

namespace expressive
{

namespace plugintools
{

DeadlineSceneManipulatorListener::DeadlineSceneManipulatorListener(SceneManipulator* target) :
    SceneManipulatorListener(target)
{
}

DeadlineSceneManipulatorListener::~DeadlineSceneManipulatorListener()
{
}

bool DeadlineSceneManipulatorListener::keyPressed (std::shared_ptr<core::InputController> event)
{
    std::shared_ptr<KeyboardIC> k = dynamic_pointer_cast<KeyboardIC>(event);
    switch(k->key()) {
    case Qt::Key_A:
        TransferSceneDeformationToModels();
        return true;
    case Qt::Key_R:
        StartRotatingSelectedNodes();
        return true;
    case Qt::Key_Minus:
        ChangeSelectionSpecialParameter(0.8);
        return true;
    case Qt::Key_Plus:
        RotateChangeAndRotate();
        return true;
    }
    return false;
}

core::KeyBindingsList DeadlineSceneManipulatorListener::GetKeyBindings() const
{
    KeyBindingsList k;
    k.AddKey("Deadline Manipulator", "A", "Transfer Scene Deformation To Models");
    k.AddKey("Deadline Manipulator", "R", "StartRotatingSelectedNodes");
    k.AddKey("Deadline Manipulator", "+", "RotateChangeAndRotate");
    k.AddKey("Deadline Manipulator", "-", "ChangeSelectionSpecialParameter");

    return k;
}

void DeadlineSceneManipulatorListener::TransferSceneDeformationToModels()
{
    std::set<SceneManager::NodeId> ids = target_->GetManipulatedNodeIds();

    SceneManager::NodeIteratorPtr nodes = make_shared<SceneManager::NodeSetIterator>(target_->scene_manager().get(), ids);
    while (nodes->HasNext()) {
        std::shared_ptr<SceneNode> n = nodes->Next();
        n->TransferSceneDeformationToModel();
    }
}

void DeadlineSceneManipulatorListener::StartRotatingSelectedNodes(Scalar nbCircles, unsigned int nbFrames)
{
    std::shared_ptr<SceneManager> scene_manager = target_->scene_manager();

    std::set<SceneManager::NodeId> nodes = target_->scene_manager()->selected_node_ids();
    Point cent = scene_manager->GetSelectionBarycenter();
    if (nodes.size() == 0) {
        auto realnodes = scene_manager->nodes();
        while (realnodes->HasNext()) {
            nodes.insert(realnodes->Next()->id());
        }
        cent = scene_manager->GetSceneBarycenter();
    }
//    std::shared_ptr<SceneNode> n = manager_->get_node(id);
    ScriptPlayer * s = ScriptPlayer::GetSingleton();
    Point up = Point::Zero();
    Point right = Point::Zero();
    //n->GetPointInWorld(n->bounding_box().ComputeCenter());
    up[1] = 1.0;
    right[0] = 1.0;
    ScriptElement elt(std::make_shared<RotateNodeCommand>(scene_manager.get(), nodes, up, right, cent, 360.0 * nbCircles, 0.0), 0, nbFrames);
    s->clear();
    s->addScriptElement(elt);

    target_->runScript((uint) nodes.size(), true);
}

void DeadlineSceneManipulatorListener::ChangeSelectionSpecialParameter(Scalar)
{
    std::shared_ptr<SceneManager> scene_manager = target_->scene_manager();
    Scalar value1 = 0.23;
    Scalar value2 = 0.85;
    unsigned int nbFrames = 30;
    unsigned int updatedNodes = 0;

//    std::shared_ptr<SceneNode> n = manager_->get_node(id);
    ScriptPlayer * s = ScriptPlayer::GetSingleton();

    std::set<SceneManager::NodeId> ids;
    ids = scene_manager->selected_node_ids();

    if (ids.size() == 0) {
        auto realnodes = scene_manager->nodes();
        while (realnodes->HasNext()) {
            ids.insert(realnodes->Next()->id());
        }
    }

    s->clear();

    SceneManager::NodeIteratorPtr nodes = make_shared<SceneManager::NodeSetIterator>(scene_manager.get(), ids);
    while (nodes->HasNext()) {
        std::shared_ptr<SceneNode> n = nodes->Next();
        std::shared_ptr<ImplicitSurfaceSceneNode> is = dynamic_pointer_cast<ImplicitSurfaceSceneNode>(n);
        if (is != nullptr) {
            std::shared_ptr<convol::BlobtreeNode> bn = is->convol_object()->blobtree_root()->child(0);
            if (bn != nullptr) {
                std::shared_ptr<convol::AbstractGradientBasedIntegralSurface> gb = std::dynamic_pointer_cast<convol::AbstractGradientBasedIntegralSurface>(bn);
                if (gb != nullptr) {
                    gb->set_special_parameter(value1);

                    SceneManager::screenshot();
                    ScriptElement elt(std::make_shared<convol::SetGradientBasedIntegralSurfaceSpecialParameterCommand>(is->convol_object()->implicit_surface().get(), gb.get(), value2, value1), 0, nbFrames);
                    s->addScriptElement(elt);
                    updatedNodes++;
                }
            }
        }
    }

    target_->runScript(updatedNodes, true);
}

void DeadlineSceneManipulatorListener::RotateChangeAndRotate()
{
    std::shared_ptr<SceneManager> scene_manager = target_->scene_manager();

    Scalar value1 = 0.0;
    Scalar value2 = 0.25;
    unsigned int firstFrameChange = 48;
    unsigned int nbFramesChangeParam = 60;
    Scalar nbCircles = 1;
    unsigned int nbFramesRotation = 48;

//    std::shared_ptr<SceneNode> n = manager_->get_node(id);
    ScriptPlayer * s = ScriptPlayer::GetSingleton();
    Point cent = scene_manager->GetSelectionBarycenter();
    Point up = Point::Zero();
    Point right = Point::Zero();
    //n->GetPointInWorld(n->bounding_box().ComputeCenter());
    up[1] = 1.0;
    right[0] = 1.0;

    std::set<SceneManager::NodeId> ids;
    std::set<SceneManager::NodeId> updatedNodes;
    ids = scene_manager->selected_node_ids();

    if (ids.size() == 0) {
        auto realnodes = scene_manager->nodes();
        while (realnodes->HasNext()) {
            ids.insert(realnodes->Next()->id());
        }
        cent = scene_manager->GetSceneBarycenter();
    }

    s->clear();


    // setup parameter change
    SceneManager::NodeIteratorPtr nodes = make_shared<SceneManager::NodeSetIterator>(scene_manager.get(), ids);
    while (nodes->HasNext()) {
        std::shared_ptr<SceneNode> n = nodes->Next();
        std::shared_ptr<ImplicitSurfaceSceneNode> is = dynamic_pointer_cast<ImplicitSurfaceSceneNode>(n);
        if (is != nullptr) {
            std::shared_ptr<convol::BlobtreeNode> bn = is->convol_object()->blobtree_root()->child(0);
            if (bn != nullptr) {
                std::shared_ptr<convol::AbstractGradientBasedIntegralSurface> gb = std::dynamic_pointer_cast<convol::AbstractGradientBasedIntegralSurface>(bn);
                if (gb != nullptr) {
                    gb->set_special_parameter(value1);
                    SceneManager::screenshot();
                    ScriptElement elt(std::make_shared<convol::SetGradientBasedIntegralSurfaceSpecialParameterCommand>(is->convol_object()->implicit_surface().get(), gb.get(), value2, value1), firstFrameChange, firstFrameChange + nbFramesChangeParam);
                    updatedNodes.insert(is->id());
                    s->addScriptElement(elt);
                }
            }
        }
    }

    //setup the rotation before the change :

    ScriptElement elt2(std::make_shared<RotateNodeCommand>(scene_manager.get(), ids, up, right, cent, 360.0 * nbCircles, 0.0), 0, nbFramesRotation);
    s->addScriptElement(elt2);
    ScriptElement elt3(std::make_shared<RotateNodeCommand>(scene_manager.get(), ids, up, right, cent, 360.0 * nbCircles, 0.0), firstFrameChange + nbFramesChangeParam, firstFrameChange + nbFramesChangeParam + nbFramesRotation);
    s->addScriptElement(elt3);

    target_->runScript((int) updatedNodes.size(), true);
}

} // namespace plugintools

} // namespace expressive
