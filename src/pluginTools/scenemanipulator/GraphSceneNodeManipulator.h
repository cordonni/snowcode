/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef GRAPHSCENENODEMANIPULATOR_H
#define GRAPHSCENENODEMANIPULATOR_H

// dependencies ork
#include "ork/core/Logger.h"

// Dependencies: core
#include "core/scenegraph/SceneNodeManipulator.h"
#include "core/scenegraph/graph/PointSceneNode.h"
#include "core/scenegraph/graph/GraphSceneNode.h"

// Dependencies: pluginTools
#include "pluginTools/PluginToolsRequired.h"


//TODO:@todo : ARAP is coming ...
#include <convol/skeleton/Skeleton.h>
#include <core/graph/deformer/SkeletalARAPDeformer.hpp>

namespace expressive {
namespace plugintools {

class SymmetryManipulator;
/**
 * @brief The GraphSceneNodeManipulator class handles a WeightedGraph in the scene and allows edition of it.
 */
class PLUGINTOOLS_API GraphSceneNodeManipulator : public core::SceneNodeManipulator {
public:
    
    EXPRESSIVE_MACRO_NAME("GraphSceneNodeManipulator")

    enum TouchState { T_IDLE, T_HAND1, T_2H_TWIST, T_2H_SCALING};

    // Constructor / Destructor

    // Todo : add a constructor that takes a graph and creates a DisplayedGraphSceneNode.
    GraphSceneNodeManipulator(core::SceneManager *scene_manager, core::Camera *camera_manager);
    virtual ~GraphSceneNodeManipulator();

    // Mouse Event
    virtual bool wheelEvent        (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool mousePressed      (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool mouseMoved        (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool mouseReleased     (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool mouseDoubleClicked(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);

    //TODO:@todo : ARAP
    virtual bool mouseMovedARAP        (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);

    // Keyboard Event
    virtual bool keyPressed (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool keyReleased(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);

    // Touch Event
    virtual bool touchBegin (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool touchUpdate(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool touchEnd   (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);

    //Pen Event
    virtual bool penBegin (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr, Scalar pen_size =-1);
    virtual bool penUpdate(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr, Scalar pen_size =-1);
    virtual bool penEnd(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);

protected:
    void MergeSelectedVertices(std::shared_ptr<core::WeightedGraphSceneNode> graph, core::SceneManager *manager);
    bool DeleteSelection(std::shared_ptr<core::WeightedGraphSceneNode> graph, core::SceneManager *manager);

    /**
     * @brief GetNearestGraphNode Returns the id of the nearest vertex to a given 2D position.
     * @param target
     * @param displayed_graph
     * @return
     */
    core::VertexId GetNearestGraphNode(const Point &target, std::shared_ptr<core::WeightedGraphSceneNode> displayed_graph);

    /**
     * @brief GetNearestGraphNode same as #GetNearestGraphNode, but able to subdivide an edge at a specific position if it is closer to the given point.
     * @param target
     * @param displayed_graph
     * @param subdivision_threshold
     * @return
     */
    std::shared_ptr<core::WeightedPointSceneNode> GetNearestGraphNode(const Point &target, std::shared_ptr<core::WeightedGraphSceneNode> displayed_graph, Scalar subdivision_threshold);

    // Scene Data
    core::SceneManager*  scene_manager_;
    core::Camera* camera_manager_;

    // Other Manipulator
    SymmetryManipulator* symmetry_manipulator_;

    // Transform Data
    bool moving_;
    bool rotating_;

    // Touch Data
    bool       init_touch_;
    TouchState t_state_;

    core::VertexId start_;
    core::VertexId next_;
    core::VertexId end_;

    std::set<core::VertexId> chain_;

    //Symetric touch data
    core::VertexId sstart_;
    core::VertexId snext_;
    core::VertexId send_;
    std::set<core::VertexId> schain_;

    //Pen data
    Vector starting_direction_;
    Point starting_pos_;
    bool deletion_request_;

    //Symetry Data
    std::map<core::VertexId ,core::VertexId> symetry_couples_;
    Vector symetry_axis_;
    std::shared_ptr<Point> symetry_start_;
    bool is_symetry_activated_;
    //Fonction used for symetry
    inline void addToSymetryMap(core::VertexId id_on_plan){symetry_couples_.insert(std::pair<core::VertexId,core::VertexId>(id_on_plan,id_on_plan));}
    inline void addToSymetryMap(core::VertexId idOri, core::VertexId idSym){symetry_couples_.insert(std::pair<core::VertexId,core::VertexId>(idOri,idSym));
                                                                            symetry_couples_.insert(std::pair<core::VertexId,core::VertexId>(idSym,idOri));}
    //Symetry converterers
    inline core::VertexId getSymetric(core::VertexId id){
        if (symetry_couples_.find(id)!=symetry_couples_.end()){return symetry_couples_.at(id);}
        return NULL;
    }

    inline Vector getSymetric(Vector p){
        if(symetry_start_){
            return p-2*(p-(*symetry_start_)).dot(symetry_axis_)*symetry_axis_;
        }
        std::cout << "symétrie topologique : pas de symétrique de points possible" << std::endl;
        return Vector(0,0,0);
    }

    //Renvoie le symetrique d'un point donné
    //Dans le cas d'une symétrie classique, la position est directement renvoyée
    //Dans le cas d'une symétrie topologique, la position du vertex symetrique au vertex passé
    inline core::WeightedPoint getSymetric(core::WeightedPoint p){
        core::WeightedPoint wp = p;
        wp.set_pos(getSymetric(Point(p[0],p[1],p[2])));
        return wp;
    }

    inline core::WeightedPoint getTopologicPos(core::VertexId idInit, std::shared_ptr<core::WeightedGraphSceneNode> displayed_graph){
        core::VertexId vsym = getSymetric(idInit);
        if(!vsym){
//            std::cout << "Le vertex renvoyé ne possède pas de symétrique" << std::endl;
            ork::Logger::ERROR_LOGGER->log("SCENEMANIPULATOR", "Le vertex renvoyé ne possède pas de symétrique");
            expressive_assert(vsym);
            return displayed_graph->graph()->Get(idInit)->pos();
        }

        return displayed_graph->graph()->Get(vsym)->pos();
    }

    inline core::WeightedEdgePtr getSymetric(core::WeightedEdgePtr edgePtr, std::shared_ptr<core::WeightedGraphSceneNode> displayed_graph){

        auto e_it = displayed_graph->graph()->GetEdges();
        std::cout << "Linking Vertexes:" << std::endl;
        core::VertexId vs = getSymetric(edgePtr->GetStartId());
        core::VertexId ve = getSymetric(edgePtr->GetEndId());
        if( ve && vs ){
            auto e2_it = displayed_graph->graph()->GetEdges();
            std::cout << "Searshing symetric..." << std::endl;
            while(e2_it->HasNext()) {
                auto e = e_it->Next();
                if(e->GetStartId() == vs || e->GetEndId() == ve){
                    std::cout << "Symetric edge found" << std::endl;
                    return e;
                }
            }
        }

        return NULL;
    }

    inline Vector getDirection(std::shared_ptr<core::WeightedVertex> vertex){
        Point p1 = vertex->pos();
        auto it = vertex->GetAdjacentVertices();
        if(it->HasNext()){
            core::WeightedVertexPtr v = it->Next();
            return p1-v->pos();
        }
        return Vector(0,0,0);
    }

    inline Vector getDirection(core::VertexId idInit, std::shared_ptr<core::WeightedGraphSceneNode> displayed_graph){
        return getDirection(displayed_graph->graph()->Get(idInit));
    }

    void mapToSymetric(std::shared_ptr<core::SceneNode> n, bool is_wp);

    void removeFromMap(core::VertexId id){
        core::VertexId vsym = getSymetric(id);
        symetry_couples_.erase(id);
        symetry_couples_.erase(vsym);
    }

    std::shared_ptr<core::Vertex> waiting_vertex_;

    // Other Data
    Point2 initial_position_;
    Scalar rotation_speed_;
    Scalar moving_speed_;

    // Command Data
    std::shared_ptr<core::Command> command_;

    //TODO:@todo : first step for ARAP
    bool arap_is_active_;
    std::shared_ptr<core::SkeletalARAPDeformerT<convol::Skeleton> > arap_deformer_;
};

} // namespace plugintools
} // namespace expressive

#endif // GRAPHSCENENODEMANIPULATOR_H
