#include <pluginTools/scenemanipulator/SceneManipulatorListener.h>

#include "core/utils/KeyBindingsList.h"

namespace expressive
{

namespace plugintools
{


SceneManipulatorListener::SceneManipulatorListener(SceneManipulator* target) :
    ListenerT<SceneManipulator>(target)
{
}

SceneManipulatorListener::~SceneManipulatorListener()
{
}

// Mouse Events
bool SceneManipulatorListener::wheelEvent        (std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

bool SceneManipulatorListener::mousePressed      (std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

bool SceneManipulatorListener::mouseReleased     (std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

bool SceneManipulatorListener::mouseMoved        (std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

bool SceneManipulatorListener::mouseDoubleClicked(std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

// Keyboard Events
bool SceneManipulatorListener::keyPressed (std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

bool SceneManipulatorListener::keyReleased(std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

// Touch Events
bool SceneManipulatorListener::touchBegin (std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

bool SceneManipulatorListener::touchUpdate(std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

bool SceneManipulatorListener::touchEnd   (std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

// Touch Events
bool SceneManipulatorListener::penBegin (std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

bool SceneManipulatorListener::penUpdate(std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

bool SceneManipulatorListener::penEnd   (std::shared_ptr<core::InputController> /*event*/)
{
    return false;
}

core::KeyBindingsList SceneManipulatorListener::GetKeyBindings() const
{
    return core::KeyBindingsList();
}

bool SceneManipulatorListener::redisplay(double t, double dt)
{
    UNUSED(t);
    UNUSED(dt);
    return false;
}

bool SceneManipulatorListener::resize(int, int)
{
    return false;
}

void SceneManipulatorListener::updated(SceneManipulator *)
{
}

} // namespace plugintools

} // namespace expressive
