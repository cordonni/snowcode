#include <pluginTools/scenemanipulator/DefaultSceneNodeManipulator.h>

// core dependencies
#include <core/scenegraph/SceneCommands.h>
#include <core/commands/ScriptPlayer.h>

// plugintools dependencies
#include <pluginTools/inputcontroller/MouseInputController.h>
#include <pluginTools/inputcontroller/KeyboardInputController.h>

using namespace expressive::core;
using namespace std;

namespace expressive {
namespace plugintools {

DefaultSceneNodeManipulator::DefaultSceneNodeManipulator(SceneManager* scene_manager, Camera* camera_manager) :
    scene_manager_ (scene_manager),
    camera_manager_(camera_manager),
    moving_        (false),
    rotating_      (false),
    rotation_speed_(0.1),
    moving_speed_  (1.0) {}

DefaultSceneNodeManipulator::~DefaultSceneNodeManipulator() {}

bool DefaultSceneNodeManipulator::UseProxyManipulator(std::shared_ptr<core::SceneNode> node)
{
//    if (!node->selected()) {
        if (node->HasManipulatorProxy() &&
//                node->manipulator_proxy()->HasSelectedChild() &&
                node->manipulator_proxy()->manipulator() != nullptr) {
            return true;
        }
//    }
    return false;
}

bool DefaultSceneNodeManipulator::wheelEvent(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->wheelEvent(event, n->manipulator_proxy())) {
            return true;
        }
    }
    std::shared_ptr<MouseIC> e = dynamic_pointer_cast<MouseIC>(event);
    if (e == nullptr || n == nullptr) {
        assert(false);
        return false;
    }

    if (moving_ || rotating_) {
        return true; // avoids conflicts with moving/rotating
    }

    if (n->hovered() || (n->selected() && (e->modifiers() & Qt::ControlModifier))) {
        Scalar scale = 1.0 + 0.2 * (Scalar)e->wheel_delta();

        scene_manager_->command_manager()->run(std::make_shared<ScaleNodeCommand>(scene_manager_, n->id(), Point(scale, scale, scale)));

        return true;
    }
    return false;
}

bool DefaultSceneNodeManipulator::mousePressed(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->mousePressed(event, n->manipulator_proxy())) {
            return true;
        }
    }

    std::shared_ptr<MouseIC> e = dynamic_pointer_cast<MouseIC>(event);
    if (e == nullptr || n == nullptr) {
        assert(false);
        return false;
    }

    if (!moving_ && !rotating_) {
        if (n->selected() && e->button() != Qt::NoButton) {
            if (scene_manager_->intersects(n->id(), camera_manager_, e->getOld2DInput())) {
                if (e->button() == Qt::RightButton) {
                    rotating_ = true;
                } else if (e->button() == Qt::LeftButton) {
                    moving_ = true;
                }
                return true;
            }
        }
    }
    return false;
}

bool DefaultSceneNodeManipulator::mouseReleased(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    if (moving_ || rotating_) {
        n->NotifyUpdate();
    }

    moving_ = false;
    rotating_ = false;

    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->mouseReleased(event, n->manipulator_proxy())) {
            return true;
        }
    }

    return false;
}

bool DefaultSceneNodeManipulator::mouseMoved(std::shared_ptr<core::InputController> event, std::shared_ptr<SceneNode> n)
{
//    cout << "NODE::mouse move" << endl;
    std::shared_ptr<MouseIC> e = dynamic_pointer_cast<MouseIC>(event);
    if (e == nullptr || n == nullptr) {
        assert(false);
        return false;
    }

    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->mouseMoved(event, n->manipulator_proxy())) {
            return true;
        }
    }

    if (moving_) {
        Point2 oldScreenPos = e->getOld2DInput();
        Point2 screenPos = e->get2DInput();

        Point worldPos = n->GetPointInWorld();
        Point oldP     = camera_manager_->GetPointInParallelWorldPlane(worldPos, oldScreenPos[0], oldScreenPos[1]);
        Point p     = camera_manager_->GetPointInParallelWorldPlane(worldPos, screenPos[0], screenPos[1]);
        n->Translate(p - oldP, SceneNode::TS_WORLD);
        return true;
    }
    if (rotating_) {
        Point2 oldP = e->getOld2DInput();
        Point2 newP = e->get2DInput();

        Scalar x = (newP[0] - oldP[0]) * rotation_speed_;
        Scalar y = (newP[1] - oldP[1]) * rotation_speed_;

        // Camera Data
        Point cent = n->GetPointInWorld(n->bounding_box().ComputeCenter());

        Point up    = camera_manager_->GetRealUp();
        Point right = camera_manager_->GetRealRight();
//        Point up    = n->GetPointInLocal(cent + camera_manager_->GetRealUp());
//        Point right = n->GetPointInLocal(cent + camera_manager_->GetRealRight());

        scene_manager_->command_manager()->run(std::make_shared<RotateNodeCommand>(scene_manager_, n->id(), up, right, cent, x, y));
//        n->Rotate(up, cent, x);
//        n->Rotate(right, cent, y);

        return true;
    }
    return false;
}

bool DefaultSceneNodeManipulator::mouseDoubleClicked(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->mouseDoubleClicked(event, n->manipulator_proxy())) {
            return true;
        }
    }

    return false;
}


bool DefaultSceneNodeManipulator::keyPressed(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->keyPressed(event, n->manipulator_proxy())) {
            return true;
        }
    }

    std::shared_ptr<KeyboardIC> e = static_pointer_cast<KeyboardIC>(event);
    if (e == nullptr || n == nullptr) {
        assert(false);
        return false;
    }

//    switch (e->key()) {
//    default:
//        break;
//    }

    return false;
}
bool DefaultSceneNodeManipulator::keyReleased(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->keyReleased(event, n->manipulator_proxy())) {
            return true;
        }
    }

    std::shared_ptr<KeyboardIC> e = static_pointer_cast<KeyboardIC>(event);
    if (e == nullptr || n == nullptr) {
        assert(false);
        return false;
    }

//    switch (e->key()) {
//    default:
//        break;
//    }

    return false;
}

bool DefaultSceneNodeManipulator::touchBegin(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n){
    // Check TouchIC and SceneNode
    shared_ptr<MultiTouchIC> e = static_pointer_cast<MultiTouchIC>(event);
    if (e == nullptr || n == nullptr) { assert(false); return false; }

    //Proxy touchBegin
    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->touchBegin(event, n->manipulator_proxy()))
            { return true; }
    }

    // Register 3D Points
    e->allInput2DTo3D(camera_manager_, camera_manager_->target(), true);

    // Set Thumb Distance
    e->setThumbDist(500.);

    // Register Phase Analysis
    bool tr = false, rt = false, sc = false;
    e->phaseAnalysis(tr, rt, sc);

    // Store Data
    e->store_current_2D();
    e->store_current_3D();

    // Begin Camera Motion
    moving_  = true;
    t_state_ = T_IDLE;

    return true;
}

bool DefaultSceneNodeManipulator::touchUpdate(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n){
    // Check TouchIC and SceneNode
    shared_ptr<MultiTouchIC> e = static_pointer_cast<MultiTouchIC>(event);
    assert(e!=nullptr && n!=nullptr);

    // Proxy Case
    bool use_proxy = UseProxyManipulator(n);
    if(e->size()<2) {
        in_deformation_ = false;
        e->setThumbDist(500.);
        if (use_proxy){
            if (n->manipulator_proxy()->manipulator()->touchUpdate(event, n->manipulator_proxy()))
                { return true; }
        }
    } else {
        if (e->size()==2 && e->isRestart()) { in_deformation_ = DeformationDetection(e); }
        if (use_proxy && in_deformation_){
            e->setThumbDist(0.5);
            if (n->manipulator_proxy()->manipulator()->touchUpdate(event, n->manipulator_proxy()))
                { return true; }
            e->setThumbDist(500.);
        }
    }

    // Register 3D Points
    e->allInput2DTo3D(camera_manager_, camera_manager_->target(), true);

    // Register 2Handed
    bool two_handed = e->isTwoHanded();

    // Register Phase Analysis
    bool tr1 = false, rt1 = false, sc1 = false;
    bool tr2 = false, rt2 = false, sc2 = false;

    if (two_handed) {
        // Store Data
        if (e->getHand1()->isRestart()) {
            e->getHand1()->store_current_2D();
            e->getHand1()->store_current_3D();
        }
        if (e->getHand2()->isRestart()) {
            e->getHand2()->store_current_2D();
            e->getHand2()->store_current_3D();
        }

        // Phase Analysis
        e->getHand1()->phaseAnalysis(tr1, rt1, sc1);
        e->getHand2()->phaseAnalysis(tr2, rt2, sc2);
    } else {
        // Store Data
        if (e->isRestart()) {
            e->store_current_2D();
            e->store_current_3D();
        }

        // Phase Analysis
        e->phaseAnalysis(tr1, rt1, sc1);
    }

    // Object Motion
    if (moving_) {
        switch (t_state_) {

        case T_IDLE: {
            if (two_handed) {
                if      (tr2)        { t_state_ = T_2H_TRANSLATION; }
                else if (sc2)        { t_state_ = T_2H_SCALING;     }
                else if (tr1 && rt1) { t_state_ = T_TR_AND_ROT;     }
                else if (tr1)        { t_state_ = T_TRANSLATION;    }
                else if (rt1)        { t_state_ = T_ROTATION;       }
                else if (sc1)        { t_state_ = T_SCALING;        }
            } else {
                if      (tr1 && rt1) { t_state_ = T_TR_AND_ROT;  }
                else if (tr1)        { t_state_ = T_TRANSLATION; }
                else if (rt1)        { t_state_ = T_ROTATION;    }
                else if (sc1)        { t_state_ = T_SCALING;     }
            }
            break;
        }

        case T_TRANSLATION: {
            // State Changes:
            if (!tr1) { t_state_ = T_IDLE; return true; }
            if ( rt1) { t_state_ = T_TR_AND_ROT; }

            // Motion
            MultiTouchIC* mt = e.get();
            if (two_handed) { mt = mt->getHand1(); }

            Vector v = mt->getTranslateData();
            n ->Translate(v, SceneNode::TS_WORLD);
            mt->store_current_3D();

            // Translation: Set Target
//            camera_manager_->set_target(camera_manager_->target() + v);
            break;
        }

        case T_ROTATION: {

            // State Changes:
            if (!rt1) { t_state_ = T_IDLE; return true; }
            if ( tr1) { t_state_ = T_TR_AND_ROT; }

            // Motion
            MultiTouchIC* mt = e.get();
            if (two_handed) { mt = mt->getHand1();}

//            float angle = mt->getRotationData();

            Point2 v_0 = mt->get2DInput(0) - mt->getOld2DInput(0); /*v_0.normalize();*/
            Point2 v_1 = mt->get2DInput(1) - mt->getOld2DInput(1); /*v_1.normalize();*/
            Point2 v = (v_0 + v_1) * rotation_speed_;

            Point cent = n->GetPointInWorld(n->bounding_box().ComputeCenter());
            Point up    = camera_manager_->GetRealUp();
            Point right = camera_manager_->GetRealRight();

            scene_manager_->command_manager()->run(std::make_shared<RotateNodeCommand>(scene_manager_, n->id(), up, right, cent, v[0], v[1]));

//            n->Rotate(camera_manager_->GetRealDirection(), n->GetPointInWorld()/*camera_manager_->target()*/, mt->getRotationData()*57.2957795131, SceneNode::TS_WORLD); // *180./M_PI;

            break;
        }

        case T_TR_AND_ROT: {
            // State Changes:
            if      (!tr1 && !rt1) { t_state_ = T_IDLE; return true; }
            else if ( tr1 && !rt1) { t_state_ = T_ROTATION; }
            else if (!tr1)         { t_state_ = T_TRANSLATION; }

            // Motion
            MultiTouchIC* mt = e.get();
            if (two_handed) { mt = mt->getHand1(); }

            // Translation
            Vector v = mt->getTranslateData();
            n ->Translate(v, SceneNode::TS_WORLD);
            mt->store_current_3D();

            // Translation: Set Target
//            camera_manager_->set_target(camera_manager_->target() + v);

            // Rotation
            Point center = n->GetPointInWorld(n->bounding_box().ComputeCenter());
            n->Rotate(camera_manager_->GetRealDirection(), center/*n->GetPointInWorld()*//*camera_manager_->target()*/, mt->getRotationData()*57.2957795131, SceneNode::TS_WORLD); // *180./M_PI;
            break;
        }

        case T_SCALING: {
            // State Changes:
            if (!sc1) { t_state_ = T_IDLE; return true; }

            // Motion
            MultiTouchIC* mt = e.get();
            if (two_handed) { mt = mt->getHand1(); }

            Scalar scale = mt->getScaleData();
            Point  v_sc  = Point(scale, scale, scale);
            n->Scale(v_sc, n->bounding_box().ComputeCenter()/*camera_manager_->target()*/);
            break;
        }

        case T_2H_TRANSLATION: {
            // State Changes:
            if (!tr2) { t_state_ = T_IDLE; return true; }

            // Motion

            // Translation
            Vector v = e->getHand1()->getTranslateData();
            n->Translate(v, SceneNode::TS_WORLD);
            e->getHand1()->store_current_3D();

            // Rotation
            auto   p = e->getHand2()->get2DDelta(e->getHand2()->getThumbId());
            Scalar x = p[0];
            Scalar y = p[1];

            // Rotation: Yaw & Pitch
            Point center = n->GetPointInWorld(n->bounding_box().ComputeCenter());
            n->Rotate(camera_manager_->GetRealUp   (), center/*camera_manager_->target()*/, x/30., SceneNode::TS_WORLD);
            n->Rotate(camera_manager_->GetRealRight(), center/*camera_manager_->target()*/, y/30., SceneNode::TS_WORLD);

            // Translation: Set Target
//            camera_manager_->set_target(camera_manager_->target() + v);

            break;
        }

        case T_2H_SCALING: {

            // State Changes:
            if (!sc2) { t_state_ = T_IDLE; return true; }

            // Motion
           // Depth Translation Data
//            Scalar object_dist = camera_manager_->GetObjectDistance();
            Scalar scale = e->getHand2()->getScaleData(true);
            n->Scale(scale);

//            // Depth Translation
//            if (scale <0){
//                if (object_dist < 1000){
////                    Vector vd = -scale/3. * camera_manager_->GetObjectDirection();
////                    n->Translate(vd ,SceneNode::TS_WORLD);
//                    // Depth Translation: Set Target
////                    camera_manager_->set_target(camera_manager_->target() + vd);
//                }
//            }else{
//                if (object_dist > 5){
////                    Vector vd = -scale/3. * camera_manager_->GetObjectDirection();
////                    n->Translate(vd ,SceneNode::TS_WORLD);
//                    // Depth Translation: Set Target
////                    camera_manager_->set_target(camera_manager_->target() + vd);
//                }
//            }
           break;
        }
        default: assert(false); // Should Not Happen
        }
    }

    if (t_state_ != T_IDLE) { e->store_current_2D(); }
    return true;
}

bool DefaultSceneNodeManipulator::touchEnd(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n){
    // Check TouchIC and SceneNode
    shared_ptr<MultiTouchIC> e = static_pointer_cast<MultiTouchIC>(event);
    assert(e!=nullptr && n!=nullptr);

    //proxy touchEnd
    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->touchEnd(event, n->manipulator_proxy()))
            { return true; }
    }

    // Stop Object Motion
    moving_ = false;
    return true;
}

bool DefaultSceneNodeManipulator::penBegin(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n, Scalar pen_size){
    // Check TouchIC and SceneNode
    assert(n && pen_size>=0);
    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->penBegin(event, n->manipulator_proxy(), pen_size)) {
            return true;
        }
    }
    return false;
}

bool DefaultSceneNodeManipulator::penUpdate(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n, Scalar pen_size){
    assert(n && pen_size>=0);
    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->penUpdate(event, n->manipulator_proxy(), pen_size)) {
            return true;
        }
    }
    return false;
}

bool DefaultSceneNodeManipulator::penEnd(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n, Scalar pen_size){
    UNUSED(pen_size);
    assert(n && pen_size>=0);
    if (UseProxyManipulator(n)) {
        if (n->manipulator_proxy()->manipulator()->penEnd(event, n->manipulator_proxy())) {
            return true;
        }
    }
    return false;
}

bool DefaultSceneNodeManipulator::DeformationDetection(std::shared_ptr<MultiTouchIC> event){
    // If one finger: Not a deformation
    if (event->getIdInputs().size() < 2) { return false; }

    // Register Thumb
    unsigned thumbId = event->getThumbId();

    // Register Associate 2D Inputs
    Point2 p0 = event->get2DInput(thumbId);
    Point2 p1 = event->get2DInput(thumbId == 0 ? 1 : 0);

    // Register Associate Scene Node
    Point target;
    SceneNode *n0 = scene_manager_->GetCameraRayIntersection(camera_manager_, p0[0], p0[1], target, true);
    SceneNode *n1 = scene_manager_->GetCameraRayIntersection(camera_manager_, p1[0], p1[1], target, true);

    // Is Deformation
    return n0 && n1 && (n0->id() == n1->id());
}


} // namespace plugintools

} // namespace expressive

