#include "pluginTools/scenemanipulator/GraphSceneNodeManipulator.h"

// Dependencies: core
#include "core/geometry/AdvancedVectorTools.h"
#include "core/geometry/WeightedSegment.h"
#include "core/geometry/WeightedTriangle.h"
#include "core/graph/GraphCommands.h"
#include "core/graph/deformer/BasicDeformer.h"
#include "core/scenegraph/graph/GeometryPrimitiveSceneNode.h"

// Dependencies: pluginTools
#include "pluginTools/inputcontroller/KeyboardInputController.h"
#include "pluginTools/inputcontroller/MouseInputController.h"
#include "pluginTools/inputcontroller/MultiTouchInputController.h"
#include "pluginTools/scenemanipulator/SceneManipulator.h"
#include "pluginTools/scenemanipulator/SymmetryManipulator.h"

//TODO:@todo : done waiting for a clean solution
#include "pluginTools/convol/ComputePrimitiveDirection.h"

using namespace std;
using namespace expressive::core;

namespace expressive {

namespace plugintools {

GraphSceneNodeManipulator::GraphSceneNodeManipulator(SceneManager *scene_manager, Camera *camera_manager) :
    scene_manager_     (scene_manager),
    camera_manager_    (camera_manager),
    moving_            (false),
    rotating_          (false),
    starting_direction_(-1,-1,-1),
    deletion_request_  (false),
    symetry_axis_(camera_manager_->GetRealRight()),
    symetry_start_(NULL),
    waiting_vertex_ (NULL),
    initial_position_  (Point2::Zero()),
    rotation_speed_    (0.1),
    moving_speed_      (1.0),
    command_           (nullptr)
{
    //TODO:@todo : ARAP
    arap_is_active_ = false;
    arap_deformer_ = nullptr;

   // Test Remi
   symmetry_manipulator_ = new SymmetryManipulator(scene_manager_, camera_manager_);
   symmetry_manipulator_->addPlane(new SymmetryPlane(Point(0., 0., 0.), Vector(1., 0., 0.)));
}

GraphSceneNodeManipulator::~GraphSceneNodeManipulator() {}

void GraphSceneNodeManipulator::MergeSelectedVertices(std::shared_ptr<WeightedGraphSceneNode> graph, SceneManager* scene_manager)
{
    std::set<VertexId> v;
    graph->graph()->GetSelectedVerticesIds(v, true);

    scene_manager->RemoveAllFromSelection();

    std::shared_ptr<MergeVerticesCommand<WeightedPoint> >command = std::make_shared<MergeVerticesCommand<WeightedPoint> > (graph->graph(), v);
    scene_manager->command_manager()->run(command);
//    graph->graph()->NotifyUpdate();

    scene_manager->AddToSelection(graph->GetNode(command->point_id_)->id());
}

bool GraphSceneNodeManipulator::DeleteSelection(std::shared_ptr<WeightedGraphSceneNode> graph, SceneManager *scene_manager)
{
    const std::set<EdgeId> &edges = graph->graph()->GetSelectedEdgesIds();
    const std::set<VertexId> &vertices = graph->graph()->GetSelectedVerticesIds();

    std::shared_ptr<WeightedVertex> v;
    WeightedGraph::VertexIteratorPtr vertices_iterator = std::make_shared<WeightedGraph::VertexSetIterator>(graph->graph(), vertices.begin(), vertices.end());
    if(vertices_iterator->HasNext()){
        v = vertices_iterator->Next();
        VertexId vsym = getSymetric(v->id());
        if(vsym && is_symetry_activated_){
            scene_manager_->AddToSelection(graph->GetNode(vsym)->id());
        }
        removeFromMap(v->id());
    }

    if (edges.size() > 0 || vertices.size() > 0) {
        scene_manager->command_manager()->run(std::make_shared<DeletePrimitiveCommand<WeightedPoint> >(graph->graph(), edges, vertices));
        //    graph->graph()->NotifyUpdate();

        return true;
    } else {
        // remove the graph if no specific elements were selected.
        // only if graph has no parent. Otherwise, user probably has
        // selected the parent and we will let it decide on what to do with the graph.
        if (graph->parent() == nullptr) {
            scene_manager->RemoveNode(graph->id());
            return true;
        }
    }

    return false;

//    scene_manager->RemoveAllFromSelection();
}

///////////////////////////////////////////////////////////////////////////////
// Keyboard Event

bool GraphSceneNodeManipulator::keyPressed(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    std::shared_ptr<KeyboardIC> e = std::static_pointer_cast<KeyboardIC>(event);
    if (e == nullptr || n == nullptr) {
        assert(false);
        return false;
    }

    std::shared_ptr<core::WeightedGraphSceneNode> graph = std::dynamic_pointer_cast<core::WeightedGraphSceneNode>(n);

    switch (e->key()) {

    case Qt::Key_G:
        {

            //TODO:@todo : first step for ARAP
            //TODO:@todo : is complete nonsense if the graph is changing...
            if(arap_is_active_) {
                arap_is_active_ = false;
                arap_deformer_ = nullptr;
                return true;
            } else {
                convol::Skeleton* skel = dynamic_cast<convol::Skeleton*>(graph->graph());
                if(skel != nullptr) {
                    std::set<VertexId> ids;
                    graph->graph()->GetSelectedVerticesIds(ids, true);
                    if(!ids.empty()) {
                        arap_is_active_ = true;
                        arap_deformer_ = std::make_shared<core::SkeletalARAPDeformerT<convol::Skeleton> >(110);
                        arap_deformer_->Precompute(skel, ids);
                    }
                    return true;
                }
            }
            return false;
            //TODO:@todo : set a boolean to true saying that we want to do ARAP


            // initializing stuff for ARAP

            return true;
        }
    case Qt::Key_B:
        {
            ComputePrimitiveDirection cpd;
            convol::Skeleton* skel = dynamic_cast<convol::Skeleton*>(graph->graph());
            if(skel != nullptr) {
                cpd.Process(skel);
                return true;
            }
            return false;
        }
    case Qt::Key_A:
       {
            std::set<VertexId> ids;
            graph->graph()->GetSelectedVerticesIds(ids, true);
            WeightedGraph::VertexIteratorPtr vertices_iterator = std::make_shared<WeightedGraph::VertexSetIterator>(graph->graph(), ids.begin(), ids.end());

            if (vertices_iterator->HasNext()) {
                std::shared_ptr<WeightedVertex> v1 = vertices_iterator->Next();

                if (vertices_iterator->HasNext()) {
                    std::shared_ptr<WeightedVertex> v2 = vertices_iterator->Next();
                    symmetry_manipulator_->addSymmetry(0, v1->id(), v2->id());
                } else { symmetry_manipulator_->addSymmetry(0, v1->id(), v1->id()); }
            }
        }
        break;
    case Qt::Key_M:
        if (graph != nullptr) {
            MergeSelectedVertices(graph, scene_manager_);
            return true;
        }
        break;
    case Qt::Key_Delete:
        if (graph != nullptr) {
            if (DeleteSelection(graph, scene_manager_)) {
                return true;
            }
        }
    default:
        break;
    }
    return false;
}

bool GraphSceneNodeManipulator::keyReleased(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    std::shared_ptr<KeyboardIC> e = std::static_pointer_cast<KeyboardIC>(event);
    if (e == nullptr || n == nullptr) {
        assert(false);
        return false;
    }

//    switch (e->key()) {
//    default:
//        break;
//    }

    return false;
}

///////////////////////////////////////////////////////////////////////////////
// Mouse Event

bool GraphSceneNodeManipulator::mousePressed(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n) 
{
    std::shared_ptr<MouseIC> e = std::dynamic_pointer_cast<MouseIC>(event);
    assert(e && n);

    std::shared_ptr<WeightedGraphSceneNode> graph = std::dynamic_pointer_cast<WeightedGraphSceneNode>(n);
    if (graph == nullptr) {  return false; }

    if (!moving_ && !rotating_) {
        if (n->selected() || n->HasSelectedChild()) {
            if (scene_manager_->intersects(n->id(), camera_manager_, e->get2DInput())) { // cond. to check if mouse is actually on the node
                if (e->button() == Qt::LeftButton) {
                    moving_ = true;
                    initial_position_ = event->get2DInput();
                }
                return true;
            }
        }
    }
    return false;
}

bool GraphSceneNodeManipulator::mouseDoubleClicked(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    std::shared_ptr<MouseIC> e = std::dynamic_pointer_cast<MouseIC>(event);
    assert(e && n);

    bool controlling_ = ( e->modifiers()  & Qt::ControlModifier) != 0;

    // if ctrl is pressed and item is selected
    if (controlling_ && (n->selected() || n->HasSelectedChild())) {
        BasicRay ray = camera_manager_->GetBasicRay(e->get2DInput()[0], e->get2DInput()[1]);
        Scalar dist;
        if (n->IsIntersectingRay(ray, dist)) {
            std::set<std::shared_ptr<SceneNode> > nodes;
            SceneManager::NodeIteratorPtr it = this->scene_manager_->selected_nodes();
            while (it->HasNext()) { nodes.insert(it->Next()); }

            this->scene_manager_->RemoveAllFromSelection();

            for (std::shared_ptr<SceneNode> node : nodes) {
                std::shared_ptr<WeightedGraphElementSceneNode> element = std::dynamic_pointer_cast<WeightedGraphElementSceneNode>(node);
                std::shared_ptr<SceneNode> newNode;
                if (element != nullptr) {
                    newNode = element->Subdivide(n->GetPointInLocal(ray.starting_point() + ray.direction() * dist));
                    if( newNode != nullptr) {
                        this->scene_manager_->AddToSelection(newNode->id());
                        moving_ = true;
                        initial_position_ = event->get2DInput();
                        command_ = nullptr;
                    }
                }
            }
            return true;
        }
    }


    //camera_manager_->set_target(n->GetPointInWorld(Point::Zero()));
    return false;
}

bool GraphSceneNodeManipulator::mouseMoved(std::shared_ptr<core::InputController> event, std::shared_ptr<SceneNode> n)
{
    //TODO:@todo
    if(arap_is_active_) {
        return mouseMovedARAP(event,n);
    }

	std::shared_ptr<MouseIC> e = std::dynamic_pointer_cast<MouseIC>(event);
    assert(e && n);

    if (moving_) {
        std::shared_ptr<WeightedGraphSceneNode> graph = std::dynamic_pointer_cast<WeightedGraphSceneNode>(n);
        if (graph == nullptr) {  return false; }

        Point2 oldScreenPos = e->getOld2DInput();
        Point2 screenPos    = e->get2DInput   ();

        // Browsing every selected nodes to find the GraphElement ones, and gather the list of vertices.
        std::set<VertexId> ids;
        graph->graph()->GetSelectedVerticesIds(ids, true);
        WeightedGraph::VertexIteratorPtr vertices_iterator = std::make_shared<WeightedGraph::VertexSetIterator>(graph->graph(), ids.begin(), ids.end());

        // Command Data
        bool new_command = false;
        if (command_ == nullptr) {
            new_command = true;
            command_ = std::make_shared<MovePointCommand<WeightedPoint> >(graph->graph());
        }
        std::shared_ptr<MovePointCommand<WeightedPoint> > mp_command = std::dynamic_pointer_cast<MovePointCommand<WeightedPoint> >(command_);

        // Move Point
        std::set<VertexId> chains;
        while (vertices_iterator->HasNext()) {
            std::shared_ptr<WeightedVertex> v = vertices_iterator->Next();
            chains.insert(v->id());

            WeightedPoint localpos = v->pos();
            Point         worldpos = graph->GetPointInWorld(localpos);

            Point oldPos = graph->GetPointInLocal(camera_manager_->GetPointInParallelWorldPlane(worldpos, oldScreenPos[0], oldScreenPos[1]));
            Point newPos = graph->GetPointInLocal(camera_manager_->GetPointInParallelWorldPlane(worldpos, screenPos   [0], screenPos   [1]));
            localpos.set_pos(localpos + newPos - oldPos);

            mp_command->MovePoint(v, localpos);
        }

        // Test
        symmetry_manipulator_->geometricalSymmetry(n, chains, mp_command);

        if (new_command) { scene_manager_->command_manager()->run   (command_); }
        else             { scene_manager_->command_manager()->update(command_); }

        return true;
    }
    return false;
}
////    if (rotating_) {
////        Point2 oldP = e->getOld2DInput();
////        Point2 newP = e->get2DInput();

////        Scalar x = (newP[0] - oldP[0]) * rotation_speed_;
////        Scalar y = (newP[1] - oldP[1]) * rotation_speed_;

////        // Camera Data
////        Point cent = n->GetPointInWorld();

////        Point up   = camera_manager_->GetRealUp();
////        Point right = camera_manager_->GetRealRight();
//////        Point up   = n->GetPointInLocal(cent + camera_manager_->GetRealUp());
//////        Point right = n->GetPointInLocal(cent + camera_manager_->GetRealRight());

////        n->Rotate(up, cent, x);
////        n->Rotate(right, cent, y);

////        return true;
////    }


bool GraphSceneNodeManipulator::mouseMovedARAP(std::shared_ptr<core::InputController> event, std::shared_ptr<SceneNode> n)
{
    std::shared_ptr<MouseIC> e = dynamic_pointer_cast<MouseIC>(event);
    assert(e && n);

    if (moving_) {
        std::shared_ptr<WeightedGraphSceneNode> graph = dynamic_pointer_cast<WeightedGraphSceneNode>(n);
        if (graph == nullptr) {  return false; }

        Point2 oldScreenPos = e->getOld2DInput();
        Point2 screenPos    = e->get2DInput   ();

        std::set<VertexId> ids;

        std::shared_ptr<SceneNode> hovered = scene_manager_->hovered_node();
        if (hovered != nullptr && hovered->selected() && hovered->IsChildOf(graph.get())) { // if hovered item in scene is a graph element, we only move it.
            std::shared_ptr<WeightedGraphElementSceneNode> element = dynamic_pointer_cast<WeightedGraphElementSceneNode>(hovered);
            element->GetVertices(ids);
        } else return false;

//        // Browsing every selected nodes to find the GraphElement ones, and gather the list of vertices.
//        std::set<VertexId> ids;
//        graph->graph()->GetSelectedVerticesIds(ids, true);
        WeightedGraph::VertexIteratorPtr vertices_iterator = std::make_shared<WeightedGraph::VertexSetIterator>(graph->graph(), ids.begin(), ids.end());

        // Command Data
        bool new_command = false;
        if (command_ == nullptr) { new_command = true; command_ = std::make_shared<MovePointCommand<WeightedPoint> >(graph->graph()); }
        std::shared_ptr<MovePointCommand<WeightedPoint> > mp_command = std::dynamic_pointer_cast<MovePointCommand<WeightedPoint> >(command_);

        convol::Skeleton* skel = dynamic_cast<convol::Skeleton*>(graph->graph());

        // Move Point
        while (vertices_iterator->HasNext()) {
            std::shared_ptr<WeightedVertex> v = vertices_iterator->Next();

            WeightedPoint localpos = v->pos();
            Point         worldpos = graph->GetPointInWorld(localpos);

            Point oldPos = graph->GetPointInLocal(camera_manager_->GetPointInParallelWorldPlane(worldpos, oldScreenPos[0], oldScreenPos[1]));
            Point newPos = graph->GetPointInLocal(camera_manager_->GetPointInParallelWorldPlane(worldpos, screenPos   [0], screenPos   [1]));
            localpos.set_pos(localpos + newPos - oldPos);

            skel->MovePoint(v->id(), localpos);
//            mp_command->MovePoint(v, localpos);
        }

        std::set<VertexId> ids2;
        graph->graph()->GetSelectedVerticesIds(ids2, true);
        arap_deformer_->UpdateHandle(skel, ids2);

//        if (new_command) { scene_manager_->command_manager()->run   (command_); }
//        else             { scene_manager_->command_manager()->update(command_); }

//TODO:@todo : should be done before the move comment of the point that being moved but
// then we have to update the point in another way and it might not be really safe
        arap_deformer_->Solve();

        // Move Point : not quite efficient ...

        auto& M = arap_deformer_->new_vertices();
        auto vertices = skel->GetVertices();
        while (vertices->HasNext()) {
            auto v = vertices->Next();

            auto search = arap_deformer_->v_map.find(v->id());
            assert(search != arap_deformer_->v_map.end());

            WeightedPoint localpos = v->pos();
            localpos.set_pos(M.block<1,3>(search->second,0).transpose());

//            Point         worldpos = graph->GetPointInWorld(localpos);
//            Point oldPos = graph->GetPointInLocal(camera_manager_->GetPointInParallelWorldPlane(worldpos, oldScreenPos[0], oldScreenPos[1]));
//            Point newPos = graph->GetPointInLocal(camera_manager_->GetPointInParallelWorldPlane(worldpos, screenPos   [0], screenPos   [1]));
//            localpos.set_pos(localpos + newPos - oldPos);

            mp_command->MovePoint(v, localpos);
        }

        if (new_command) { scene_manager_->command_manager()->run   (command_); }
        else             { scene_manager_->command_manager()->update(command_); }

        return true;
    }
    return false;
}

bool GraphSceneNodeManipulator::mouseReleased(std::shared_ptr<core::InputController> /*event*/, std::shared_ptr<core::SceneNode> /*n*/) {
    //TODO:@todo : ARAP
//    if(arap_is_active_) {
//        //TODO:@todo : we should get the new position and update them ...
//        arap_deformer_->Solve();

//        std::shared_ptr<DisplayedWeightedGraphSceneNode> graph = dynamic_pointer_cast<DisplayedWeightedGraphSceneNode>(n);
//        std::shared_ptr<convol::Skeleton> skel = std::dynamic_pointer_cast<convol::Skeleton>(graph->graph());

//        bool new_command = false;
//        if (command_ == nullptr) { new_command = true; command_ = new MovePointCommand<WeightedPoint>(graph->graph()); }
//        MovePointCommand<WeightedPoint>* mp_command = dynamic_cast<MovePointCommand<WeightedPoint>*>(command_);

//        // Move Point : not quite efficient ...

//        auto& M = arap_deformer_->new_vertices();
//        auto vertices = skel->GetVertices();
//        while (vertices->HasNext()) {
//            auto v = vertices->Next();

//            auto search = arap_deformer_->v_map.find(v->id());
//            assert(search != arap_deformer_->v_map.end());

//            WeightedPoint localpos = v->pos();
//            localpos.set_pos(M.block<1,3>(search->second,0).transpose());

////            Point         worldpos = graph->GetPointInWorld(localpos);
////            Point oldPos = graph->GetPointInLocal(camera_manager_->GetPointInParallelWorldPlane(worldpos, oldScreenPos[0], oldScreenPos[1]));
////            Point newPos = graph->GetPointInLocal(camera_manager_->GetPointInParallelWorldPlane(worldpos, screenPos   [0], screenPos   [1]));
////            localpos.set_pos(localpos + newPos - oldPos);

//            mp_command->MovePoint(v, localpos);
//        }

//        if (new_command) { scene_manager_->command_manager()->run   (command_); }
//        else             { scene_manager_->command_manager()->update(command_); }
//    }

    moving_   = false;
    rotating_ = false;
    command_  = nullptr;

    return false;
}

bool GraphSceneNodeManipulator::wheelEvent(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    std::shared_ptr<MouseIC> e = std::dynamic_pointer_cast<MouseIC>(event);
    if (e == nullptr || n == nullptr) {
        assert(false);
        return false;
    }

    bool controlling_ = (e->modifiers() & Qt::ControlModifier) != 0;
    if (!controlling_ ) { return false; }

    std::shared_ptr<WeightedGraphSceneNode> graph = std::dynamic_pointer_cast<WeightedGraphSceneNode>(n);
    if (graph == nullptr) {
        return false;
    }

    if (moving_ || rotating_) {
        command_  = nullptr;
//        return true; // avoids conflicts with moving/rotating
    }

    BasicRay ray = camera_manager_->GetBasicRay(e->get2DInput()[0], e->get2DInput()[1]);

    std::shared_ptr<SceneNode> hovered = scene_manager_->hovered_node();
    if (hovered != nullptr && hovered->selected() && hovered->IsChildOf(graph.get())) { // if hovered item in scene is a graph element, we only move it.
        std::set<VertexId> vertices;
        std::shared_ptr<WeightedGraphElementSceneNode> element = std::dynamic_pointer_cast<WeightedGraphElementSceneNode>(hovered);
        element->GetVertices(vertices);

        symmetry_manipulator_->geometricalSymmetry(n, vertices);
        std::shared_ptr<ChangeVerticesWeightCommand<WeightedPoint> > command = std::make_shared<ChangeVerticesWeightCommand<WeightedPoint> >(element->displayed_graph()->graph(), vertices, 1.0 + 0.1 * (Scalar)e->wheel_delta());

        scene_manager_->command_manager()->run(command);

        return true;
    } else { /* if (e->modifiers() & Qt::ControlModifier) */

        std::set<VertexId> ids;
        graph->graph()->GetSelectedVerticesIds(ids, true);
        if (ids.size() > 0) {
            symmetry_manipulator_->geometricalSymmetry(n, ids);
            std::shared_ptr<ChangeVerticesWeightCommand<WeightedPoint> > command  = std::make_shared<ChangeVerticesWeightCommand<WeightedPoint> >(graph->graph(), ids, 1.0 + 0.1 * (Scalar)e->wheel_delta());
            scene_manager_->command_manager()->run(command);
            return true;
        }
    }

    return false;
}

///////////////////////////////////////////////////////////////////////////////
// Touch Event

bool GraphSceneNodeManipulator::touchBegin(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    UNUSED(n);
    // Register MultiTouch IC
    shared_ptr<MultiTouchIC> e = static_pointer_cast<MultiTouchIC>(event);
    assert (e!=nullptr && n!= nullptr);

    // Register Hand Data
    e->allInput2DTo3D(camera_manager_, camera_manager_->target(), true);
    e->store_current_2D();

    // Is Not Deformation State
    t_state_    = T_IDLE;
    init_touch_ = true;
    return false;
}

bool GraphSceneNodeManipulator::touchUpdate(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    // Typedef: Deformers
    typedef scale_BFS_V             <WeightedGraph> scale;
    typedef twist_BFS_V             <WeightedGraph> twist;
    typedef rotate_BFS_V            <WeightedGraph> rotate;
    typedef searchShortestPath_BFS_V<WeightedGraph> searchShortestPath;

    // Register MultiTouch IC
    shared_ptr<MultiTouchIC> e = static_pointer_cast<MultiTouchIC>(event);
    assert (e!=nullptr && n!= nullptr);

    // Is Not Deformation State
    if(e->getIdInputs().size()<2) { init_touch_ = true; return false;}

    // Register 3D Points
    e->allInput2DTo3D(camera_manager_, camera_manager_->target(), true);

    // Register Graph Data
    std::shared_ptr<WeightedGraphSceneNode> displayed_graph = std::dynamic_pointer_cast<WeightedGraphSceneNode>(n);
    WeightedGraph*                          graph           = displayed_graph->graph();

    // Register Chain Data
    if (init_touch_) {
        t_state_    = T_IDLE;
        command_    = nullptr;
        init_touch_ = false;

        // Register ThumbId
        unsigned thumbId = e->getThumbId();

        // Register 3D Points
        Point p1 = e->get3DInput(thumbId);
        Point p2 = e->get3DInput(thumbId==0?1:1);

        // Get Nearest Vertex
        start_ = GetNearestGraphNode(n->GetPointInLocal(p1), displayed_graph);
        end_   = GetNearestGraphNode(n->GetPointInLocal(p2), displayed_graph);

        // Not a chain (TODO After)
//        sstart_ = getSymetric(start_);
//        send_   = getSymetric(end_  );
        if (start_ == end_) {
            chain_.clear();
            chain_.insert(start_);
            e->store_current_2D();
            return true;
        }

        // Perform searchShortestPath
        searchShortestPath::Attributes attributes;
        searchShortestPath sp_vis(*graph, start_, end_, attributes);
        try {
            boost::breadth_first_search(*graph, start_, boost::visitor(sp_vis).color_map(graph->vertex_color_property_map()).vertex_index_map(graph->vertex_index_property_map()));
        } catch (int e) { if (e!=42) { throw (e); } }

        // Init Chains
        chain_ .clear();
//        schain_.clear();

        // Register Data & Chains
        next_ = end_;
        while(attributes[next_] != start_) { 
			chain_ .insert(next_); 
			next_ = attributes[next_];  
		}

        // Finish Register Chain
        chain_.insert(next_ );
        chain_.insert(start_);

        // First Step: No transformation to perform yet
        e->store_current_2D(); 
		return true;
    }

    // Register Phase Analysis
    bool tr1 = false, rt1 = false, sc1 = false;
    bool tr2 = false, rt2 = false, sc2 = false;

    bool two_handed = e->isTwoHanded();
    if (two_handed) {
        // Store Data
        if (e->getHand1()->isRestart()) {
            e->getHand1()->store_current_2D();
            e->getHand1()->store_current_3D();
        }
        if (e->getHand2()->isRestart()) {
            e->getHand2()->store_current_2D();
            e->getHand2()->store_current_3D();
        }

        // Phase Analysis
        e->getHand1()->phaseAnalysis(tr1, rt1, sc1);
        e->getHand2()->phaseAnalysis(tr2, rt2, sc2);

    } else {
        // Store Data
        if (e->isRestart()) {
            e->store_current_2D();
            e->store_current_3D();
        }

        // Phase Analysis
        e->phaseAnalysis(tr1, rt1, sc1);
    }

    // Transformation:
    switch(t_state_)  {
    case T_IDLE: {
        if (two_handed) {
            if      (tr2) { t_state_ = T_2H_TWIST; }
            else if (sc2) { t_state_ = T_2H_SCALING; command_ = nullptr; }

        } else {
            if (tr1 || rt1 || sc1) { t_state_ = T_HAND1; }
        }
        break;
    }

    case T_HAND1: {
        // State Changes:
        if (!tr1 && !rt1 && !sc1) { t_state_ = T_IDLE; return true; }

        if (start_ == end_) {
            e->store_current_2D();
            graph->NotifyUpdate();
            return true;
        }

        //start_ = GetNearestGraphNode(n->GetPointInLocal(p1), displayed_graph, 40.);
        // Command Data
        bool new_command = false;
        if (command_ == nullptr) { command_ = std::make_shared<MovePointCommand<WeightedPoint> >(graph); }
        std::shared_ptr<MovePointCommand<WeightedPoint> > mp_command = std::dynamic_pointer_cast<MovePointCommand<WeightedPoint> >(command_);

        // Init Data
        Vector axis  = displayed_graph->GetPointInLocal(camera_manager_->GetRealDirection()) - displayed_graph->GetPointInLocal();
        Scalar theta = e->getRotationData();
        Scalar ratio = e->getScaleData();

        // Init Graph Search
        std::set<VertexId> ids_rt, ids_sc;
        rotate rt_vis(*graph, mp_command, ids_rt, start_, next_, theta, axis);
        scale  sc_vis(*graph, mp_command, ids_sc, start_, end_ , ratio, chain_);

        // Perform Rotation
        boost::breadth_first_search(*graph, next_, boost::visitor(rt_vis).color_map(graph->vertex_color_property_map()).vertex_index_map(graph->vertex_index_property_map()));
        symmetry_manipulator_->geometricalSymmetry(n, ids_rt, mp_command);

        if (new_command) { scene_manager_->command_manager()->run   (command_); }
        else             { scene_manager_->command_manager()->update(command_); }

        // Perform Scaling
        boost::breadth_first_search(*graph, end_ , boost::visitor(sc_vis).color_map(graph->vertex_color_property_map()).vertex_index_map(graph->vertex_index_property_map()));
        symmetry_manipulator_->geometricalSymmetry(n, ids_sc, mp_command);

        scene_manager_->command_manager()->update(command_);

        break;
    }

    case T_2H_TWIST: {
        // State Changes:
        if (!tr2) { t_state_ = T_IDLE; return true; }

        // Impossible case
        if (start_ == end_) {
            e->store_current_2D();
            graph->NotifyUpdate();
            return true;
        }

        // Command Data
        bool new_command = false;
        if (command_ == nullptr) { command_ = std::make_shared<MovePointCommand<WeightedPoint> >(graph); }
        std::shared_ptr<MovePointCommand<WeightedPoint> > mp_command = std::dynamic_pointer_cast<MovePointCommand<WeightedPoint> >(command_);


        // Init Data
        Vector axis  = (*graph)[end_]->pos() - (*graph)[start_]->pos();
        Scalar theta = e->getHand2()->get2DDelta(e->getHand2()->getThumbId())[1]/20.;

        // Init Graph Search
        std::set<VertexId> ids;
        twist tw_vis(*graph, mp_command, ids, start_, end_, theta, axis, chain_);

        // Perform Graph Search
        boost::breadth_first_search(*graph, end_, boost::visitor(tw_vis).color_map(graph->vertex_color_property_map()).vertex_index_map(graph->vertex_index_property_map()));
        symmetry_manipulator_->geometricalSymmetry(n, ids, mp_command);

        if (new_command) { scene_manager_->command_manager()->run   (command_); }
        else             { scene_manager_->command_manager()->update(command_); }
        break;
    }

    case T_2H_SCALING: {
        // State Changes:
        if (!sc2) { t_state_ = T_IDLE; command_ = nullptr; return true; }

        // Ratio Data
        Scalar ratio = e->getHand2()->getScaleData();

        // Command Data
        bool new_command = false;
        if (command_ == nullptr) { command_ = std::make_shared<ChangeVerticesWeightCommand<WeightedPoint> >(graph, chain_, ratio); }
        std::shared_ptr<ChangeVerticesWeightCommand<WeightedPoint> > cw_command = std::dynamic_pointer_cast<ChangeVerticesWeightCommand<WeightedPoint> >(command_);
        cw_command->set_factor(ratio);

        if (new_command) { scene_manager_->command_manager()->run   (command_); }
        else             { scene_manager_->command_manager()->update(command_); }
        break;
    }

    default: assert(false); // Should Not Happen
    }

    // Store Data
    e->store_current_2D();
    graph->NotifyUpdate();

    return true;
}

bool GraphSceneNodeManipulator::touchEnd(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    UNUSED(n);
    // Register MultiTouch IC
    shared_ptr<MultiTouchIC> e = static_pointer_cast<MultiTouchIC>(event);
    assert(e != nullptr && n != nullptr);

    // Is Not Deformation State
    init_touch_ = false;
    return true;
}

bool GraphSceneNodeManipulator::penBegin(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n, Scalar pen_size)
{
    command_ = nullptr; //TODO:@todo : should be required

    std::shared_ptr<MouseIC> e = std::dynamic_pointer_cast<MouseIC>(event);
    assert(e && n);

    bool controlling_ = ( pen_size != -1);

    // if ctrl is pressed and item is selected
    if (controlling_ && (n->selected() || n->HasSelectedChild())) {
        //Graph Data
        std::shared_ptr<WeightedGraphSceneNode> displayed_graph = std::dynamic_pointer_cast<WeightedGraphSceneNode>(n);

        //Get or create nearest vertex
        e->allInput2DTo3D(camera_manager_,camera_manager_->target(), true);
        Point p1 = e->get3DInput();
        std::shared_ptr<WeightedPointSceneNode> start = GetNearestGraphNode(n->GetPointInLocal(p1), displayed_graph, 10.);

        //Create the new vertex allows to subdivide on an edge
        std::shared_ptr<SceneNode> newNode = start->Subdivide(start->vertex()->pos(), pen_size);

        if( newNode != nullptr) {
            scene_manager_->RemoveAllFromSelection();
            scene_manager_->AddToSelection(newNode->id());
        }

        if (!moving_ && e->button() == Qt::LeftButton) {
            moving_ = true;
            initial_position_ = event->get2DInput();
        }
        return true;
    }
    return false;
}

bool GraphSceneNodeManipulator::penUpdate(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n, Scalar pen_size)
{

    //TODO:@todo : there is a big problem with this function when it comes to do-undo-redo, since it use multiple of command instead of a single composit one ...

    //Intial Verification
    std::shared_ptr<MouseIC> e = std::dynamic_pointer_cast<MouseIC>(event);
    assert(e && n);
    std::shared_ptr<WeightedGraphSceneNode> graph = std::dynamic_pointer_cast<WeightedGraphSceneNode>(n);
    if (graph == nullptr) {
        return false;
    }

    //Moving point
    Point2 oldScreenPos = e->getOld2DInput();
    Point2 screenPos = e->get2DInput();
//    std::set<VertexId> ids;
//    graph->graph()->GetSelectedVerticesIds(ids, true);
//    Graph::VertexIteratorPtr vertices_iterator = std::make_shared<Graph::VertexSetIterator>(graph->graph().get(), ids.begin(), ids.end());//graph->graph()->GetSelectedVertices();

    WeightedGraph::VertexIteratorPtr vertices_iterator = graph->graph()->GetSelectedVertices();

    std::shared_ptr<WeightedVertex> v_sym;
    if (vertices_iterator->HasNext()) { // if there is a current selection, we can extend it.
        // we suppose here that only 1 point can be selected.
        std::shared_ptr<WeightedVertex> v = vertices_iterator->Next();
        if (is_symetry_activated_ && getSymetric(v->id())){
            v_sym  = graph->graph()->Get( getSymetric(v->id()) );
        }else {
            v_sym = NULL;
        }
        WeightedPoint pos = v->pos();
        Point worldPos = graph->GetPointInWorld(pos);
        Point oldPos = graph->GetPointInLocal(camera_manager_->GetPointInParallelWorldPlane(worldPos, oldScreenPos[0], oldScreenPos[1]));
        Point newPos = graph->GetPointInLocal(camera_manager_->GetPointInParallelWorldPlane(worldPos, screenPos   [0], screenPos   [1]));
        pos.set_pos(pos + newPos - oldPos);
        if (is_symetry_activated_ && v_sym && v_sym ==v){
            pos = pos - pos.dot(symetry_axis_)*symetry_axis_;
            std::cout << "Point on axis" << std::endl;
        }
        // Moving point.

        // Command Data
        bool new_command = false;
        if (command_ == nullptr) { new_command = true; command_ = std::make_shared<MovePointCommand<WeightedPoint> >(graph->graph()); }
        std::shared_ptr<MovePointCommand<WeightedPoint> > mp_command = std::dynamic_pointer_cast<MovePointCommand<WeightedPoint> >(command_);

        // Move Point
        mp_command->MovePoint(v, pos);

        if (new_command) { scene_manager_->command_manager()->run   (command_); }
        else             { scene_manager_->command_manager()->update(command_); }

        graph->graph()->NotifyUpdate();

        ////////////////////////////////////////////////////////////
        //Code used for material creation : following the user's line

        Scalar epsilon = 0.000000001;
        //Detection of angle in movement
        //save initial directionj
        if (abs(starting_direction_[0]) <= epsilon && abs(starting_direction_[1]) <= epsilon && abs(starting_direction_[2]) <= epsilon) {
            starting_direction_ = newPos - oldPos;
            starting_direction_.normalize();
            starting_pos_ = oldPos;
        }
        //Compute threshold and actual direction
        Scalar sensibility_ = 0.85;  //1 : normal,  <1 : sensitive, >1 : brut
        Scalar width = (pen_size != 0 ? pen_size : pos.weight());
        Vector actual_direction = newPos - starting_pos_;
        Scalar len = actual_direction.norm();
        Scalar dot = actual_direction.dot(starting_direction_);
        //optimisation to rise the position fidelity
        WeightedPoint correctionPos = pos;
//        correctionPos = dot*starting_direction_ + starting_pos_;
        Scalar correction_percent = 0.50;
        correctionPos = correction_percent*dot*starting_direction_ + (1-correction_percent)*actual_direction + starting_pos_;
        actual_direction.normalize();
        dot = actual_direction.dot(starting_direction_);
        Scalar threshold_ = max ( width / sqrt (1- dot*dot), width)*sensibility_;

        //angle detected ?
        if ( len > threshold_) {
//            //Vertex move for a higher fidelity line
//            if ((pos-correctionPos).norm() < 0.5*len){
//            graph->graph()->MovePoint(v->id(), correctionPos);
//            if (move_point_command_ == nullptr) {
//                move_point_command_ = new MovePointCommand(graph->graph().get(), v->id(), correctionPos);
//            } else {
//                move_point_command_->SetFinalPos(v->id(), correctionPos);
//            }
//            //Symetric displacement if needed
//            if (is_symetry_activated_ && v_sym && v_sym != v){
//                if(symetry_start_){
//                    WeightedPoint symPos = getSymetric(correctionPos);
//                    graph->graph()->MovePoint(v_sym->id(), symPos);
//                    if (move_point_command_ == nullptr) {
//                        move_point_command_ = new MovePointCommand(graph->graph().get(), v_sym->id(), symPos);
//                    } else {
//                        move_point_command_->SetFinalPos(v_sym->id(), symPos);
//                    }
//                }else{
//                    //////TODOOOOOOOOOOOOOOOOOO
//                    Vector move = correctionPos - pos;
//                    Scalar theta = acos(getDirection(v).dot(getDirection(v_sym)));
//                    Matrix rotate_matrix_; rotate_matrix_ = Eigen::AngleAxis<Scalar>(theta, camera_manager_->GetRealDirection());
//                    Vector newMove = rotate_matrix_ * move;
//                    WeightedPoint symPos = correctionPos;
//                    symPos.set_pos(getTopologicPos(v->id(), graph) + newMove);
//                    graph->graph()->MovePoint(v_sym->id(), symPos);
//                    if (move_point_command_ == nullptr) {
//                        move_point_command_ = new MovePointCommand(graph->graph().get(), v_sym->id(), symPos);
//                    } else {
//                        move_point_command_->SetFinalPos(v_sym->id(), symPos);
//                    }
//                }
//            }
//            }

            //Create the next vertex
            std::shared_ptr<SceneNode> newNode = graph->GetNode(v->id())->Subdivide(pos, pen_size);

            //Add the new vertex to selection
            if( newNode != nullptr) {
                scene_manager_->RemoveAllFromSelection();
                scene_manager_->AddToSelection(newNode->id());
            }
            //Reset the initial condition
            starting_direction_ = Vector(0.0, 0.0, 0.0);
            deletion_request_ = false;
            len = 0;
        }

        /////////////////////
        //Eraser Option (does not work that much a that time)

//        //Detect erase mode
//        if (!deletion_request_ && len > 1.1 * width){
//            //The curser is far enough to accept eraser mode
//            deletion_request_ = true;
//        } else if (deletion_request_ && len < width){
//            //The curser is near enough to erase

//            //Link with adjacent vertex
//            std::shared_ptr<Vertex> nextV = v->GetAdjacentVertices()->Next();
//            Point adj_pos = nextV->pos();

//            // Actually deleting the last point.
//            DeleteSelection(graph, scene_manager_);

//            //Get nearest vertex and add it to selection
//            scene_manager_->AddToSelection(graph->GetNode(nextV->id())->id());

//            starting_direction_ = newPos - adj_pos;
//            starting_direction_.normalize();
//            starting_pos_ = adj_pos;

//            deletion_request_ = false;
//        }

    }
    return true;
}

bool GraphSceneNodeManipulator::penEnd(std::shared_ptr<core::InputController> /*event*/, std::shared_ptr<core::SceneNode> /*n*/)
{
    moving_ = false;
    rotating_ = false;
    command_ = nullptr;

    return false;
}


///////////////////////////////////////////////////////////////////////////////
// Other Function

VertexId GraphSceneNodeManipulator::GetNearestGraphNode(const Point &target, std::shared_ptr<WeightedGraphSceneNode> displayed_graph)
{
    // Init Research Data
    Scalar   min = expressive::kScalarMax;
    VertexId res = NULL;

    // Research Nearest Vertex
    WeightedPoint wp;
    auto v_it = displayed_graph->graph()->GetVertices();
    while(v_it->HasNext()) {
        auto v = v_it->Next();

        // Is nearer?
        Scalar l = (v->pos() - target).squaredNorm();
        if (l < min) {
            min = l;
            res = v->id();//displayed_graph->GetNode(v->id());
            wp = v->pos();
        }
    }
    return res;
}

std::shared_ptr<WeightedPointSceneNode> GraphSceneNodeManipulator::GetNearestGraphNode(const Point &target, std::shared_ptr<WeightedGraphSceneNode> displayed_graph, Scalar sub_thr)
{
    VertexId id = GetNearestGraphNode(target, displayed_graph);

    std::shared_ptr<WeightedPointSceneNode> res = std::dynamic_pointer_cast<WeightedPointSceneNode>(displayed_graph->GetNode(id));

    // First Case: No subdivision or nothing found (second case should only happen when graph is empty).
    if (sub_thr < 0.0 || res == nullptr) {
        return res;
    }

    // Second Case: Close to Vertex
    std::shared_ptr<WeightedVertex> v = displayed_graph->graph()->Get(id);
    const WeightedPoint &pos = v->pos();
    auto p1 = camera_manager_->WorldToViewplane(displayed_graph->GetPointInWorld(target));
    auto p2 = camera_manager_->WorldToViewplane(displayed_graph->GetPointInWorld(pos));
    if ((p2-p1).norm() < sub_thr) {
        return res;
    }

    // Last Case: Subdivision
    Point   near_point;
    WeightedEdgePtr near_edge;
    // Init Data
    Scalar min = expressive::kScalarMax;
    auto e_it = displayed_graph->graph()->GetEdges();
    // We can only iterate on the edges of the returned vertex, uch less computation!
//    auto e_it = v->GetEdges();
    while(e_it->HasNext()) {
        auto e = e_it->Next();

        Point p;
        std::shared_ptr<WeightedGeometryPrimitive> segs = std::dynamic_pointer_cast<WeightedGeometryPrimitive>(e->edgeptr());
        Point p0 = segs->GetPoint(0);
        for (unsigned int i = 1; i < segs->GetSize(); ++i) {
            Point p1 = segs->GetPoint(i);
            p = expressive::VectorTools::ProjectionToSegment(target, p0, p1 - p0, true);
            Scalar l = (p - target).squaredNorm();
            if (l < min) {
                min = l;
                near_point = p;
                near_edge = displayed_graph->graph()->GetEdge(segs, i - 1);
            }

            p0 = p1;
        }
    }

    // Subdivide?
    std::shared_ptr<core::SceneNode> newNode = displayed_graph->GetNode(near_edge->id())->Subdivide(near_point);
    if(is_symetry_activated_){
        WeightedEdgePtr symEdge = getSymetric(near_edge, displayed_graph);
        if( symEdge ) {
            std::shared_ptr<core::SceneNode> symNode;
            if (symetry_start_){
                Point sym_point = getSymetric(near_point);
                symNode = displayed_graph->GetNode(symEdge->id())->Subdivide(sym_point);
            }else{
                Scalar barycentre = (near_point - near_edge->GetStart()->pos()).norm();
                Vector newDir = symEdge->GetEnd()->pos() - symEdge->GetStart()->pos();
                newDir.normalize();
                Point sym_point = symEdge->GetStart()->pos()+ barycentre * newDir;
                symNode = displayed_graph->GetNode(symEdge->id() )->Subdivide(sym_point);
            }
            mapToSymetric(newNode, true);
            mapToSymetric(symNode, true);
        }else{
            std::cout << "symId invalide" << std::endl;
        }
    }
    return std::dynamic_pointer_cast<WeightedPointSceneNode> (newNode);
}

void GraphSceneNodeManipulator::mapToSymetric (std::shared_ptr<core::SceneNode> /*n*/, bool /*is_wp*/){
////TODO:@todo : this function cause to much problem to be activatided in Master ...

//    std::shared_ptr<Vertex> v;

//    if (is_wp){
//        std::shared_ptr<WeightedPoint> wp = std::dynamic_pointer_cast<WeightedPoint>(n);
//        v = wp->vertex();
//    }else{
//        std::shared_ptr<GraphSceneNode> graph = std::dynamic_pointer_cast<GraphSceneNode>(n);
//        assert(graph);

//        std::set<VertexId> ids;
//        graph->graph()->GetSelectedVerticesIds(ids, true);
//        Graph::VertexIteratorPtr vertices_iterator = std::make_shared<Graph::VertexSetIterator>(graph->graph().get(), ids.begin(), ids.end());//graph->graph()->GetSelectedVertices();
//        if(vertices_iterator->HasNext()){
//            v = vertices_iterator->Next();
//        }else{
//            std::cout << "nothing selected" << std::endl;
//            assert(false);
//        }
//    }

//    if (waiting_vertex_ == NULL){
//        waiting_vertex_ = v;
//    }else{
//        addToSymetryMap(v->id(), waiting_vertex_->id());
//        std::cout << "couple added : "<< v->id();
//        std::cout << ", " << waiting_vertex_->id() << std::endl;
//        waiting_vertex_ = NULL;
//    }
}

} // namespace plugintools

} // namespace expressive
