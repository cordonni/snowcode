/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef EXPRESSIVE_SCENE_MANIPULATOR_H_
#define EXPRESSIVE_SCENE_MANIPULATOR_H_

#include "ork/core/Timer.h"

// Dependencies: core
#include "core/commands/ScriptPlayer.h"
//#include "core/commands/CommandManager.h"
#include "core/scenegraph/listeners/SceneManagerListener.h"
//#include "core/scenegraph/SceneNodeManipulator.h"
//#include "core/ExpressiveEngine.h"

// Dependencies: pluginTools
#include "pluginTools/inputcontroller/MouseInputController.h"
#include "pluginTools/inputcontroller/KeyboardInputController.h"
#include "pluginTools/inputcontroller/MultiTouchInputController.h"
//#include "pluginTools/scenemanipulator/DefaultSceneNodeManipulator.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
// Dependencies: Qt
#include <QState>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive
{
namespace core
{
    class ExpressiveEngine;
    class SceneNodeManipulator;
    class KeyBindingsList;
}
}

namespace expressive {

namespace plugintools {

class DefaultSceneNodeManipulator;

class PLUGINTOOLS_API SceneManipulator : public QObject, public core::SceneManagerListener, public core::ScriptListener, public core::ListenableT<SceneManipulator> {
    Q_OBJECT
public:
    

    // Constructor / Destructor
    SceneManipulator(core::ExpressiveEngine* engine);
    virtual ~SceneManipulator();

    virtual void Init(core::ExpressiveEngine* engine);

    // Mouse Events
    virtual void wheelEvent           (QWheelEvent*);
    virtual void mousePressEvent      (QMouseEvent*);
    virtual void mouseMoveEvent       (QMouseEvent*);
    virtual void mouseReleaseEvent    (QMouseEvent*);
    virtual void mouseDoubleClickEvent(QMouseEvent*);

    // Keyboard Events
    virtual void keyPressEvent  (QKeyEvent*);
    virtual void keyReleaseEvent(QKeyEvent*);

    // Touch Events
    virtual bool touchBegin (QEvent*);
    virtual bool touchUpdate(QEvent*);
    virtual bool touchEnd   (QEvent*);

    // Display events
    virtual void resizeEvent(const QSize &newSize, const QSize &);
    virtual void redisplay();

    // Pen Events
    virtual bool penBegin (QMouseEvent *);
    virtual bool penUpdate(QMouseEvent *);
    virtual bool penEnd   (QMouseEvent *);

    // Other Events
    virtual bool event(QEvent *);

    //////
    /// ACCESSORS
    std::shared_ptr<core::SceneManager> scene_manager() const;
    std::shared_ptr<MouseIC> mouse_input_controller() const;
    std::shared_ptr<KeyboardIC> keyboard_input_controller() const;
    std::shared_ptr<MultiTouchIC> multitouch_input_controller() const;
    Scalar pen_size() const;
    bool is_pen_envent() const;
    bool pen_on_surface() const;
    core::ExpressiveEngine *engine() const;
    bool panning_selection() const;
    Point2 selection_start_point() const;
    Point last_hovered_pos() const;

    //////
    /// SETTERS
    void set_camera_manipulator(std::shared_ptr<core::SceneNodeManipulator> manipulator);

    virtual void SceneNodeAdded  (NodeId node);
    virtual void SceneNodeRemoved(NodeId node);
    virtual void SceneNodeSelected(NodeId node);
    virtual void SceneNodeUnselected(NodeId node);

    virtual void updated(core::SceneManager*);
    virtual void cleared(core::SceneManager*);
    virtual void frameDoneCallback(int frame);

    void UpdateSceneNodeMesh(NodeId node);

    virtual core::SceneManager::NodeIteratorPtr selection_handlers() const;

    void Refine(Scalar factor);

    void ToggleWireframe();
    void ToggleSkeletons();
    void ToggleVisibility();

    void ShowHelp();
    virtual core::KeyBindingsList GetKeyBindings() const;

    std::set<core::SceneManager::NodeId> GetManipulatedNodeIds();

    /**
     * Runs the script that is currently stored in the default ScriptPlayer.
     * @param nNodes the number of nodes that must be updated at each frame.
     * A frame will only be computed once every node of the previous frame are ready.
     * (Mostly usefull for nodes that take time to compute, like implicit surfaces).
     * @param removeSelection Whether or not the selection should be cleared. You may want to apply a script on current selection.
     */
    void runScript(int nNodes, bool removeSelection = true);

    void RefreshSelectionHandlers();

    // Selection
    void GetSelectionInArea(const core::AABBox2D &box, std::set<core::SceneManager::NodeId> &selected_nodes); //< Selection in a box defined in screen space
    void GetSelectionInArea(const Point2 & center, Scalar radius, std::set<core::SceneManager::NodeId> &selected_nodes); //< Selection in a circle defined around mouse in screen space.
    core::SceneNode* GetSelection(std::shared_ptr<MouseIC>      event, Point& target, bool add_to_selection = true);
    core::SceneNode* GetSelection(std::shared_ptr<MultiTouchIC> event, Point& target, bool add_to_selection = true);
    core::SceneNode* GetSelection(std::shared_ptr<MultiTouchIC> event, Point& target, int finger_id, bool add_to_selection = true);

signals :
    void all_nodes_updated();
    void MeshNeedsUpdating(unsigned int node);
public slots:
    void UpdateMesh(unsigned int node);

protected:
    core::ExpressiveEngine *engine_;

    ///////////////////
    // InputController
    std::shared_ptr<MouseIC>      mouse_input_controller_;
    std::shared_ptr<KeyboardIC>   keyboard_input_controller_;
    std::shared_ptr<MultiTouchIC> multitouch_input_controller_;

    void DeleteSelection();

    ///////////////////
    // Others
    void CheckManipulator(std::shared_ptr<core::SceneNode> node);

    // Shared Ptr Managers
    std::shared_ptr<core::SceneManager>   scene_manager_;
    std::shared_ptr<core::CommandManager> command_manager_;

    // Shared Ptr Scene Manipulator
    std::shared_ptr<core::SceneNodeManipulator>  camera_manipulator_;
    std::shared_ptr<DefaultSceneNodeManipulator> default_manipulator_;

//    std::set<SceneManager::NodeId> selected_nodes_;

//    SceneNode * camera_target_;

//    Point camera_target_pos_;

    bool panning_selection_;
    Point2 selection_start_point_;
    std::set<NodeId> panning_selection_handlers_;
    std::set<NodeId> selection_handlers_;
    std::shared_ptr<core::Overlay> selection_overlay_;

    Point last_hovered_pos_;
    ork::Timer timer_;
    double lastFrameTime_;
    unsigned int waiting_nodes_;
    unsigned int updated_nodes_;

    //Pen Data
    //Used to convert mouse event into pen event
    bool is_pen_event_;
    //Used to know if the pen is touching the surface
    bool pen_on_surface_;
    //Used to know to wanted size of pen (if real pen is working, not needed anymore because of pressure touch)
    Scalar pen_size_;

};

} // namespace plugintools
} // namespace expressive

#endif // EXPRESSIVE_SCENE_MANIPULATOR_H_
