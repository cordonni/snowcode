#include "pluginTools/scenemanipulator/SymmetryManipulator.h"

#include "core/scenegraph/graph/GraphSceneNode.h"

using namespace std;
using namespace expressive::core;

namespace expressive {

namespace plugintools {

//////////////////////////////////////////////////////////////////////
/// SymmetryStruct: getSymmetryPoint
//////////////////////////////////////////////////////////////////////
SymmetryStruct::~SymmetryStruct() 
{
}

Point SymmetryPlane::getSymmetryPoint(Point p)
    { return p - 2.*((p - center_).dot(normal_)/(normal_.dot(normal_)))*normal_; }

Point SymmetryPlane::getProjectedPoint(Point p)
    { return p -    ((p - center_).dot(normal_)/(normal_.dot(normal_)))*normal_; }


//////////////////////////////////////////////////////////////////////
/// Constructor / Destructor
//////////////////////////////////////////////////////////////////////

SymmetryManipulator::SymmetryManipulator(SceneManager* scene_manager, Camera* camera_manager)
    : scene_manager_ (scene_manager),
      camera_manager_(camera_manager) {}

SymmetryManipulator::~SymmetryManipulator() 
{
}

//////////////////////////////////////////////////////////////////////
/// Symmetry Event
//////////////////////////////////////////////////////////////////////

bool SymmetryManipulator::geometricalSymmetry(std::shared_ptr<SceneNode> n, std::set<WeightedVertexId> ids, std::shared_ptr<MovePointCommand<WeightedPoint> > command) {
    // Init Graph
    std::shared_ptr<WeightedGraphSceneNode> displayed_graph = dynamic_pointer_cast<WeightedGraphSceneNode>(n);
    WeightedGraph*                          graph           = displayed_graph->graph();

    // For each planes of symmetry
    for(unsigned i = (uint) planes_.size(); i!=0; i--) {
        Symmetries      symmap_ = symmetries_[i-1];
        SymmetryStruct* struct_ = planes_    [i-1];

        // Symmetric of Moved Point
        for (auto id : ids) {
            auto it =  symmap_.find(id);
            if  (it == symmap_.end()) { continue; }

            // Set Symmetric Point
            Point         pos;
            WeightedPoint point;
            if (it->second == id) {
                // Position Data
                point = (*graph)[id]->pos   ();
                pos   = struct_->getProjectedPoint(point);
            } else {
                // Position Data
                point = (*graph)[it->second]->pos();
                pos   = struct_->getSymmetryPoint((*graph)[id]->pos());
            }

            // Store New Position
            point.set_pos(pos);
            command->MovePoint(graph->Get(it->second), point);
        }
    }

    return true;
}

bool SymmetryManipulator::geometricalSymmetry(std::shared_ptr<SceneNode>, std::set<WeightedVertexId>& ids) {
    // For each planes of symmetry
    for(unsigned i = (uint) planes_.size(); i!=0; i--) {
        Symmetries      symmap_ = symmetries_[i-1];

        // Symmetric of Moved Point
        for (auto id : ids) {
            auto it =  symmap_.find(id);
            if  (it == symmap_.end()) { continue; }
            if  (it->second == id)    { continue; }

            ids.insert(it->second);
        }
    }

    return true;
}

//////////////////////////////////////////////////////////////////////
/// Symmetry Axis
//////////////////////////////////////////////////////////////////////

void SymmetryManipulator::addPlane(SymmetryPlane* p) {
    planes_    .push_back(p);
    symmetries_.push_back(Symmetries());
}

void SymmetryManipulator::setPlane(unsigned id, SymmetryPlane* p)
    { planes_[id] = p; }

void SymmetryManipulator::remPlane(unsigned id) {
    // Clear Symmetry Vertex
    symmetries_[id].clear();

    // Clear Symmetry Plane
    planes_    .erase(planes_    .begin()+id);
    symmetries_.erase(symmetries_.begin()+id);
}

//////////////////////////////////////////////////////////////////////
/// Symmetry Vertex
//////////////////////////////////////////////////////////////////////

void SymmetryManipulator::addSymmetry(unsigned plane_id, WeightedVertexId v_id1, WeightedVertexId v_id2) {
    symmetries_[plane_id].insert(Symmetry(v_id1, v_id2));
    symmetries_[plane_id].insert(Symmetry(v_id2, v_id1));
}

void SymmetryManipulator::remSymmetry(unsigned plane_id, WeightedVertexId v_id1, WeightedVertexId v_id2) {
    symmetries_[plane_id].erase(v_id1);
    symmetries_[plane_id].erase(v_id2);
}

WeightedVertexId SymmetryManipulator::getSymmetry(unsigned plane_id, WeightedVertexId v_id) {
    auto it = symmetries_[plane_id].find(v_id);
    if (it!=symmetries_[plane_id].end()) { return it->second; }
    else                                 { return 0; }
}

} // namespace plugintools
} // namespace expressive
