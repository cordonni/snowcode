#include "pluginTools/scenemanipulator/SceneManipulator.h"

// core dependencies
#include "core/geometry/MeshTools.h"
#include "core/scenegraph/SceneCommands.h"
#include "core/scenegraph/SceneNodeManipulator.h"
#include "core/ExpressiveEngine.h"
#include "core/utils/KeyBindingsList.h"

// plugintools dependencies
#include "pluginTools/scenemanipulator/DefaultSceneNodeManipulator.h"
#include "pluginTools/scenemanipulator/SceneManipulatorListener.h"
#include "pluginTools/scenemanipulator/CameraManipulator.h"

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
// Qt dependencies
#include <QEventLoop>
#include <QMouseEventTransition>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

/////// TODO TOBEDELETED !!! DEADLINE SIGG2014
// convol dependencies
#include <pluginTools/convol/ImplicitSurfaceSceneNode.h>
#include <convol/blobtreenode/BlobtreeCommands.h>

#include "ork/scenegraph/ShowLogTask.h"
#include "ork/render/FrameBuffer.h"

#include "core/scenegraph/Overlay.h"


using namespace std;
using namespace expressive::core;

namespace expressive {
namespace plugintools {

SceneManipulator::SceneManipulator(ExpressiveEngine* engine) :
    SceneManagerListener(engine->scene_manager().get()),
    engine_(engine),
    panning_selection_(false),
    selection_overlay_(std::make_shared<Overlay>(engine->scene_manager().get(), "SelectionOverlay", Overlay::O_FOREGROUND, "overlayMethod", "selection_box")),
    waiting_nodes_(0),
    updated_nodes_(0)
{
    Init(engine);
}

SceneManipulator::~SceneManipulator()
{
}

void SceneManipulator::Init(ExpressiveEngine* engine) {
    scene_manager_   = engine->scene_manager  ();
    command_manager_ = engine->command_manager();

    mouse_input_controller_      = make_shared<MouseIC>     ();
    keyboard_input_controller_   = make_shared<KeyboardIC>  ();
    multitouch_input_controller_ = make_shared<MultiTouchIC>();

    //Pen data
    is_pen_event_ = false;
    pen_size_ = 0.2;

    camera_manipulator_  = engine->camera_manager()->manipulator();

    default_manipulator_ = make_shared<DefaultSceneNodeManipulator>(scene_manager_.get(), engine->camera_manager().get());

    // Check already existing nodes' manipulators.
    auto selection = scene_manager_->nodes();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        CheckManipulator(node);
    }

    connect(this, SIGNAL(MeshNeedsUpdating(unsigned int)),  this, SLOT(UpdateMesh(unsigned int)));

    auto mesh = MeshTools::CreateQuad(Point::Zero());
    selection_overlay_->set_tri_mesh(mesh);
    scene_manager_->AddOverlay(selection_overlay_);
//    selection_overlay_->internal_node()->addValue(new Value2f("pos", Point2f(0.f, 0.f)));
//    selection_overlay_->internal_node()->addValue(new Value2f("scale", Point2f(1.0f, 1.0f)));
    selection_overlay_->set_visible(false);


    lastFrameTime_ = 0.0;
    timer_.start();
}

std::shared_ptr<core::SceneManager> SceneManipulator::scene_manager() const
{
    return scene_manager_;
}

std::shared_ptr<MultiTouchIC> SceneManipulator::multitouch_input_controller() const
{
    return multitouch_input_controller_;
}

std::shared_ptr<MouseIC> SceneManipulator::mouse_input_controller() const
{
    return mouse_input_controller_;
}

Scalar SceneManipulator::pen_size() const
{
    return pen_size_;
}

bool SceneManipulator::is_pen_envent() const
{
    return is_pen_event_;
}

bool SceneManipulator::pen_on_surface() const
{
    return pen_on_surface_;
}

ExpressiveEngine *SceneManipulator::engine() const
{
    return engine_;
}

bool SceneManipulator::panning_selection() const
{
    return panning_selection_;
}

Point2 SceneManipulator::selection_start_point() const
{
    return selection_start_point_;
}

Point SceneManipulator::last_hovered_pos() const
{
    return last_hovered_pos_;
}

void SceneManipulator::set_camera_manipulator(std::shared_ptr<SceneNodeManipulator> manipulator)
{
    scene_manager_->current_camera()->set_manipulator(manipulator);
    camera_manipulator_ = manipulator;
}

void SceneManipulator::Refine(Scalar /*factor*/) {
//    auto selection = selection_handlers();
//    if (selection_handlers_.size() == 0) {

//    } else {
//        while (selection.HasNext()) {
//        }
//    }
}

void SceneManipulator::ToggleWireframe() {
    auto selection = selection_handlers();
    if (selection_handlers_.size() == 0) {
        auto nodes = scene_manager_->root_nodes();//scene_manager_->nodes();
        while (nodes->HasNext()) {
            std::shared_ptr<SceneNode> n = nodes->Next();
            if (n != nullptr) {
                n->ToggleWireframe(false);
            }
        }
    } else {
        while (selection->HasNext()) {
            std::shared_ptr<SceneNode> n = selection->Next();
            if (n != nullptr) {
                n->ToggleWireframe(false);
            }
        }
    }
}

void SceneManipulator::ShowHelp()
{
    KeyBindingsList kb = GetKeyBindings();

    for (auto it : kb.m_bindings) {
        ork::Logger::INFO_LOGGER->logf("HELP", "%s", it.first.c_str());
        for (auto it2 : it.second) {
            ork::Logger::INFO_LOGGER->logf("HELP", "%8s : %s", it2.first.c_str(), it2.second.description.c_str());
        }
    }
}

KeyBindingsList SceneManipulator::GetKeyBindings() const
{
    KeyBindingsList kbl = camera_manipulator_->GetKeyBindings();

    kbl.AddKey("Base", "F1", "Display/hide logs");
    kbl.AddKey("Base", "F12", "Take a screenshot. Saved in /tmp/");
    kbl.AddKey("Base", "A", "TransferSceneDeformationToModels");
    kbl.AddKey("Base", "D", "ToggleSkeletons");
    kbl.AddKey("Base", "H", "Show this help");
    kbl.AddKey("Base", "P", "Enable/Disable Pen mode");
    kbl.AddKey("Base", "R", "StartRotatingSelectedNodes");
    kbl.AddKey("Base", "V", "ToggleVisibility");
    kbl.AddKey("Base", "W", "ToggleWireframe");
//    kbl.AddKey("Base", "+", "Rotate Change And Rotate");
//    kbl.AddKey("Base", "-", "Change Selection Special Parameter");
    kbl.AddKey("Base", "Del", "Delete Selection");
    kbl.AddKey("Base", "CTRL+Z", "Undo");

    for (auto listener : listeners_) {
        kbl.MergeWith(dynamic_cast<SceneManipulatorListener*>(listener)->GetKeyBindings());
    }

    auto selection = selection_handlers();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        kbl.MergeWith(node->manipulator()->GetKeyBindings());
    }

    return kbl;
}

void SceneManipulator::updated(SceneManager * /*s*/)
{
    updated_nodes_++;
    if (waiting_nodes_ == updated_nodes_) {
        emit all_nodes_updated();
    }
}

void SceneManipulator::cleared(core::SceneManager*)
{
    selection_handlers_.clear();
}

void SceneManipulator::frameDoneCallback(int /*frame*/)
{
    if (waiting_nodes_ > 0 && updated_nodes_ < waiting_nodes_) {
        QEventLoop loop;
        connect(this, SIGNAL(all_nodes_updated()), &loop, SLOT(quit()));
        loop.exec();
    }
    updated_nodes_ = 0;
}

void SceneManipulator::ToggleSkeletons()
{
    std::set<NodeId> ids = GetManipulatedNodeIds();

    SceneManager::NodeIteratorPtr nodes = make_shared<SceneManager::NodeSetIterator>(scene_manager_.get(), ids);
    while (nodes->HasNext()) {
        SceneNode* n = nodes->Next()->ancestor();
        ImplicitSurfaceSceneNode* is = dynamic_cast<ImplicitSurfaceSceneNode*>(n);
        if (is != nullptr) {
            is->DisplaySkeleton(!is->displayed_skeleton()->visible());
        }
    }
}

void SceneManipulator::ToggleVisibility()
{
    auto selection = selection_handlers();
    if (selection_handlers_.size() == 0) {
        auto nodes = scene_manager_->root_nodes();
        while (nodes->HasNext()) {
            std::shared_ptr<SceneNode> n = nodes->Next();
            if (n != nullptr) {
                n->ToggleVisible(true);
            }
        }
    } else {
        while (selection->HasNext()) {
            std::shared_ptr<SceneNode> n = selection->Next();
            if (n != nullptr) {
                n->ToggleVisible(false);
            }
        }
    }
}

std::set<core::SceneManager::NodeId> SceneManipulator::GetManipulatedNodeIds()
{
    std::set<NodeId> ids;
    ids = scene_manager_->selected_node_ids();

    if (ids.size() == 0) {
        auto nodes = scene_manager_->nodes();
        while (nodes->HasNext()) {
            ids.insert(nodes->Next()->id());
        }
    }
    return ids;
}

void SceneManipulator::runScript(int nNodes, bool removeSelection)
{
    ScriptPlayer * s = ScriptPlayer::GetSingleton();

    s->AddListener(this);
    waiting_nodes_ = nNodes;
    updated_nodes_ = 0;
    if (removeSelection) {
        scene_manager_->RemoveAllFromSelection();
    }

    s->playAll(1, scene_manager_.get());

    s->RemoveListener(this);
}

///////////////////////////////////////////////////////////////////////////////
// Mouse Events

void SceneManipulator::mousePressEvent(QMouseEvent* event)
{
    //If pen is enabled : launch Pen event instead
    if (is_pen_event_ && penBegin(event)){return;}

    // Register Event
    mouse_input_controller_->registerEvent(event);
    mouse_input_controller_->allInput2DTo3D(scene_manager_->current_camera().get(), scene_manager_->current_camera()->target(), true);

    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->mousePressed(mouse_input_controller_)) {
            scene_manager_->NotifyUpdate();
            return ;
        }
    }

    // Left Click: Select Object
    if (event->button() == Qt::LeftButton) {
        if (scene_manager_->hovered_node() != nullptr) {
            std::shared_ptr<SceneNode> n = scene_manager_->hovered_node();
            bool addIt = true;
            if (!(mouse_input_controller()->modifiers() & Qt::ControlModifier)) {
                if (n->selected() == false) {
                    scene_manager_->RemoveAllFromSelection();
                    selection_handlers_.clear();
                }
            } else {
                if (n->selected() && scene_manager_->selected_node_count() > 1) {
                    addIt = false;
                    scene_manager_->RemoveFromSelection(n->id());
                    selection_handlers_.erase(n->manipulation_handler_id());
//                    selection_handlers_.erase(n->ancestor()->id());
                }
            }
            // make sure that selection handlers don't create duplicated actions :
            // for example, if both the SceneNode and it's proxy are displayed and selected, mouse actions will occur twice on the proxy, which we don't want.
            // There are no such issues for items without a proxy.
            if (n->HasManipulatorProxy() && selection_handlers_.find(n->manipulator_proxy()->id()) != selection_handlers_.end()) {
                addIt = false;
            }

            if (addIt) {
//                selection_handlers_.insert(n->manipulation_handler_id());
                scene_manager_->AddToSelection(n->id());

                BasicRay ray = scene_manager_->current_camera()->GetBasicRay(event->x(),  event->y());
                Scalar dist_max = expressive::kScalarMax;
                AbstractTriMesh::FaceHandle fh(-1);
//                n->tri_mesh()->update_bounding_box();
                n->tri_mesh()->FindIntersection(ray, dist_max, fh);

            }

        } else {
            Point target = Point(scene_manager_->current_camera()->target());
            SceneNode *n = GetSelection(mouse_input_controller_, target);
            if (n != nullptr) {
                scene_manager_->current_camera()->set_target(target);
            }
        }
    }

    // If Selection: Move Object
    bool res = false;

    if (selection_handlers_.size() > 0) {
        auto selection = selection_handlers();
        while (selection->HasNext()) {
            std::shared_ptr<SceneNode> node = selection->Next();
            res |= node->manipulator()->mousePressed(mouse_input_controller_, node);
        }
    }
    if (!res && (event->button() == Qt::LeftButton && event->modifiers() & Qt::ControlModifier)) {
        panning_selection_ = true;
        selection_start_point_ = mouse_input_controller_->get2DInput();
        panning_selection_handlers_.clear();
        selection_overlay_->set_visible(true);
        selection_overlay_->internal_node()->addValue(new ork::Value2f("scale", Point2f(0.f, 0.f)));
        res = true;
    }

    // If Not: Move Camera
    if (!res)
    {
        camera_manipulator_->mousePressed(mouse_input_controller_);
    }

    // Notify
    scene_manager_->NotifyUpdate();
}

void SceneManipulator::mouseMoveEvent(QMouseEvent* event) {
    //If pen is enabled : launch Pen event instead
    if (is_pen_event_ && penUpdate(event)) { return; }

    // Register Event
    mouse_input_controller_->registerEvent(event);
    mouse_input_controller_->allInput2DTo3D(scene_manager_->current_camera().get(), scene_manager_->current_camera()->target(), true);

    ExpressiveEngine::GetSingleton()->set_mouse_screen_position(mouse_input_controller_->get2DInput()); // update mouse position, accessible from the entire application.

    if (panning_selection_) { // takes care of panning selection first.
        // cancel previous hovering.
        for (SceneManager::NodeId id : panning_selection_handlers_) {
            scene_manager_->get_node(id)->set_hovered(false);
        }
        panning_selection_handlers_.clear();

        // hover currently selected nodes.
        Point2 begin = selection_start_point_;
        Point2 end = mouse_input_controller_->get2DInput();
        AABBox2D b(begin, end, true);
        GetSelectionInArea(b, panning_selection_handlers_);
        for (SceneManager::NodeId id : panning_selection_handlers_) {
            scene_manager_->get_node(id)->set_hovered(true, true);
        }

        Point2 scale = end - begin;
        scale[0] = scale[0] * 2.0 / scene_manager_->current_camera()->GetActualWidth();
        scale[1] = -scale[1] * 2.0 / scene_manager_->current_camera()->GetActualHeight();

//        Point2 pos = scene_manager_->current_camera()->GetNormalizedScreenCoordinate((end + begin) / 2.0);
        Point2 pos = scene_manager_->current_camera()->GetNormalizedScreenCoordinate(begin);

        selection_overlay_->internal_node()->addValue(new ork::Value2f("pos", pos.cast<float>()));
        selection_overlay_->internal_node()->addValue(new ork::Value2f("scale", scale.cast<float>()));

        scene_manager_->NotifyUpdate();
        return;
    }

    // hovering handling
    if (mouse_input_controller_->getPressed() == false ) {
        Point target = scene_manager_->current_camera()->target();
        SceneNode *hover = GetSelection(mouse_input_controller_, target, false);
        scene_manager_->SetHoveredNode(hover);

        last_hovered_pos_ = target;
    }

    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->mouseMoved(mouse_input_controller_)) {
            scene_manager_->NotifyUpdate();
            return;
        }
    }

    // If Selection: Move Object
    bool res = false;
    this->RefreshSelectionHandlers();
    auto selection = selection_handlers();

    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        res |= node->manipulator()->mouseMoved(mouse_input_controller_, node);
    }


//    CommandManager::GetSingleton()->run(new UpdateMeshCommand(scene_manager_.get(), selection_handlers_));

    // If Not: Move Camera
    if (!res) {
        camera_manipulator_->mouseMoved(mouse_input_controller_);
    }

    // Notify
    scene_manager_->NotifyUpdate();
}

void SceneManipulator::mouseReleaseEvent(QMouseEvent* event) {
    //If pen is enabled : launch Pen event instead
    if (is_pen_event_ && penEnd(event)){return;}

    // Register Event
    mouse_input_controller_->registerEvent(event);
    mouse_input_controller_->allInput2DTo3D(scene_manager_->current_camera().get(), scene_manager_->current_camera()->target(), true);

    if (panning_selection_) {

        std::set<SceneManager::NodeId> ids;
        bool has_unselected_nodes = false;
        for (SceneManager::NodeId id : panning_selection_handlers_) {
            std::shared_ptr<SceneNode> n = scene_manager_->get_node(id);
            has_unselected_nodes |= (n->selected() == false);
            n->set_hovered(false);
            ids.insert(n->manipulation_handler_id());
        }

        if (has_unselected_nodes) {
            scene_manager_->AddToSelection(panning_selection_handlers_);
        } else {
            scene_manager_->RemoveFromSelection(panning_selection_handlers_);
        }
        auto nodes = scene_manager_->selected_nodes();
        selection_handlers_.clear();
        while (nodes->HasNext()) {
            auto n = nodes->Next();
            // make sure that selection handlers don't create duplicated actions :
            // for example, if both the SceneNode and it's proxy are displayed and selected, mouse actions will occur twice on the proxy, which we don't want.
            // There are no such issues for items without a proxy.
            if (n->HasManipulatorProxy() == false ||
                    (ids.find(n->manipulator_proxy()->id()) == ids.end())) {
                selection_handlers_.insert(n->manipulation_handler_id());
            }
        }

        panning_selection_handlers_.clear();
        panning_selection_ = false;

        selection_overlay_->set_visible(false);

        scene_manager_->NotifyUpdate();
        return;
    }

    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->mouseReleased(mouse_input_controller_)) {
            scene_manager_->NotifyUpdate();
            return;
        }
    }
    // If Selection: Move Object
    bool res = false;
    auto selection = selection_handlers();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        res |= node->manipulator()->mouseReleased(mouse_input_controller_, node);
    }

    // If Not: Move Camera
    if (!res) {
        camera_manipulator_->mouseReleased(mouse_input_controller_);
    }

    // Notify
    scene_manager_->NotifyUpdate();
}

void SceneManipulator::mouseDoubleClickEvent(QMouseEvent* event) {
    mouse_input_controller_->registerEvent(event);
    mouse_input_controller_->allInput2DTo3D(scene_manager_->current_camera().get(), scene_manager_->current_camera()->target(), true);

    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->mouseDoubleClicked(mouse_input_controller_)) {
            scene_manager_->NotifyUpdate();
            return;
        }
    }
    if (event->button() == Qt::LeftButton) {
        Point target;
        SceneNode * n = GetSelection(mouse_input_controller_, target, false);
        //scene_manager_->current_camera()->set_target(target);

        if (n == nullptr) { // double click on void cancels any selection

            scene_manager_->RemoveAllFromSelection();
//            selection_handlers_.clear();
            //            camera_target_ = nullptr;
            //            camera_manager_->set_target(scene_manager_->GetSceneBarycenter());
            //            camera_target_pos_ = scene_manager_->GetSceneBarycenter();
        } else { // double click on an item = focus on that item
            panning_selection_handlers_.clear();
            panning_selection_ = false;
            scene_manager_->RemoveAllFromSelection();
            scene_manager_->AddToSelection(n->id());
//            selection_handlers_.insert(n->ancestor()->id());
            n->ancestor()->manipulator()->mouseDoubleClicked(mouse_input_controller_, scene_manager_->get_node(n->ancestor()->id()));
        }
    }

    // Notify
    scene_manager_->NotifyUpdate();
}


void SceneManipulator::wheelEvent(QWheelEvent *event)
{
    mouse_input_controller_->registerEvent(event);
    mouse_input_controller_->allInput2DTo3D(scene_manager_->current_camera().get(), scene_manager_->current_camera()->target(), true);
    //mouse_input_controller_->allInput2DTo3D(camera_manager_.get(), camera_manager_->target(), true);

    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->wheelEvent(mouse_input_controller_)) {
            scene_manager_->NotifyUpdate();
            return;
        }
    }

    // If Selection: Update Object
    bool res = false;
    auto selection = selection_handlers();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        res |= node->manipulator()->wheelEvent(mouse_input_controller_, node);
    }


    if (!res) { camera_manipulator_->wheelEvent(mouse_input_controller_); }

    // Notify
    scene_manager_->NotifyUpdate();
}

///////////////////////////////////////////////////////////////////////////////
// Keyboard Events

void SceneManipulator::keyPressEvent ( QKeyEvent * event ) {
    keyboard_input_controller_->registerEvent(event);
    bool res = false;
    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->keyPressed(keyboard_input_controller_)) {
            scene_manager_->NotifyUpdate();
            return;
        }
    }

    auto selection = selection_handlers();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        res |= node->manipulator()->keyPressed(keyboard_input_controller_, node);
    }

    if (!res) {
        switch(event->key()) {
        case Qt::Key_F1:
            ork::ShowLogTask::enabled = !ork::ShowLogTask::enabled;
            res = true;
            break;
        case Qt::Key_F12:
            SceneManager::screenshot();
            res = true;
            break;
        case Qt::Key_Space:
        case Qt::Key_1:
        case Qt::Key_2:
        case Qt::Key_3:
        case Qt::Key_4:
        case Qt::Key_5:
        case Qt::Key_6:
        case Qt::Key_8:
        case Qt::Key_9:
            camera_manipulator_->keyPressed(keyboard_input_controller_);
            res = true;
            break;
        case Qt::Key_W:
            ToggleWireframe();
            res = true;
            break;
        case Qt::Key_D:
            ToggleSkeletons();
            res = true;
            break;
        case Qt::Key_V:
            ToggleVisibility();
            res = true;
            break;
        case Qt::Key_H:
            ork::Logger::INFO_LOGGER->log("HELP", "[HELP]");
            ShowHelp();
            res = true;
            break;
        case Qt::Key_P:
            is_pen_event_ = !is_pen_event_;
//            if (is_pen_event_){
//                std::cout<< "Pen Enabled" << std::endl;
//            }else{
//                std::cout<< "Pen Disabled" << std::endl;
//            }
            res = true;
            break;
        case Qt::Key_Plus:
            if (is_pen_event_){
                if (pen_size_ < 10){
                    pen_size_ *= 2;
//                    std::cout << "pen size :" << pen_size_ << std::endl;
                }else{
                    pen_size_ = 0;
//                    std::cout << "pen size : [auto]" << std::endl;
                }
            }
            res = true;
            break;
        case Qt::Key_Minus:
            if (is_pen_event_){
                if (pen_size_ == 0){
                    pen_size_ = 0.8;
//                    std::cout << "pen size :" << pen_size_ << std::endl;
                }else{
                    pen_size_ /= 2;
//                    std::cout << "pen size :" << pen_size_ << std::endl;
                }
            }
            res = true;
            break;
        case Qt::Key_Z:
            if (event->modifiers() & Qt::ControlModifier) {
                if (event->modifiers() & Qt::ShiftModifier) {
                    scene_manager_->command_manager()->redo();
                } else {
                    scene_manager_->command_manager()->undo();
                }
            }
            res = true;
            break;
        case Qt::Key_Delete:
            DeleteSelection();
            res = true;
            break;

        default:
            break;
        }
    }
    if (!res) {
        camera_manipulator_->keyPressed(keyboard_input_controller_);
    }
    // Notify
    scene_manager_->NotifyUpdate();
    NotifyUpdate();
}

void SceneManipulator::keyReleaseEvent ( QKeyEvent * event) {
    keyboard_input_controller_->registerEvent(event);
    bool res = false;
    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->keyReleased(keyboard_input_controller_)) {
            scene_manager_->NotifyUpdate();
            return;
        }
    }

    auto selection = selection_handlers();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        res |= node->manipulator()->keyReleased(keyboard_input_controller_, node);
    }

    if (!res) {
        camera_manipulator_->keyReleased(keyboard_input_controller_);
    }
}



///////////////////////////////////////////////////////////////////////////////
//Touch Events

bool SceneManipulator::touchBegin ( QEvent *event)
{
    // Register Event
    multitouch_input_controller_->registerEvent(event);
    multitouch_input_controller_->allInput2DTo3D(scene_manager_->current_camera().get(), scene_manager_->current_camera()->target(), true);

    bool res = false;
    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->touchBegin(multitouch_input_controller_)) {
            scene_manager_->NotifyUpdate();
            return true;
        }
    }

    // Unselect Stuff
    scene_manager_->RemoveAllFromSelection();
    selection_handlers_.clear();

    // If SNR
    std::shared_ptr<core::Camera> camera_manager = scene_manager_->current_camera();
    int w = camera_manager->GetActualWidth ();
    int h = camera_manager->GetActualHeight();

    auto p = multitouch_input_controller_->get2DInput();
    if (!(p[0]<w/20 || p[0]>19*w/20 ||
          p[1]<h/20 || p[1]>19*h/20)) {

        // Select Object
        Point target = Point(scene_manager_->current_camera()->target());
        GetSelection(multitouch_input_controller_, target);
//        camera_manager->set_target(target);
    }


    // If Selection: Move Object
    auto selection = selection_handlers();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        res |= node->manipulator()->touchBegin(multitouch_input_controller_, node);
    }

    // If Not: Move Camera
    if (!res) { camera_manipulator_->touchBegin(multitouch_input_controller_); }

    // Notify
    scene_manager_->NotifyUpdate();
    return true;
}

bool SceneManipulator::touchUpdate(QEvent* event) {
    // Register Event
    multitouch_input_controller_->registerEvent(event);
    multitouch_input_controller_->allInput2DTo3D(scene_manager_->current_camera().get(), scene_manager_->current_camera()->target(), true);

    bool res = false;
    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->touchUpdate(multitouch_input_controller_))
            { scene_manager_->NotifyUpdate(); return true; }
    }

    // If Selection: Move Object
    auto selection = selection_handlers();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        res |= node->manipulator()->touchUpdate(multitouch_input_controller_, node);
    }

    // If Not: Move Camera
    if (!res) { camera_manipulator_->touchUpdate(multitouch_input_controller_); }

    // Notify
    scene_manager_->NotifyUpdate();
    NotifyUpdate();

    return true;
}

bool SceneManipulator::touchEnd ( QEvent *event) {
    multitouch_input_controller_->registerEvent(event);
    multitouch_input_controller_->allInput2DTo3D(scene_manager_->current_camera().get(), scene_manager_->current_camera()->target(), true);

    bool res = false;
    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->touchEnd(multitouch_input_controller_))
            { scene_manager_->NotifyUpdate(); return true; }
    }

    // If Selection: Move Object
    auto selection = selection_handlers();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        res |= node->manipulator()->touchEnd(multitouch_input_controller_, node);
    }



    // If Not: Move Camera
    if (!res) { camera_manipulator_->touchEnd(multitouch_input_controller_); }

    // Notify
    scene_manager_->NotifyUpdate();
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//Pen Events

bool SceneManipulator::penBegin ( QMouseEvent *event){
    //Register Event not done because it is actually a mouse event.
//    event->registerEventType();
    mouse_input_controller_->registerEvent(event);
    mouse_input_controller_->allInput2DTo3D(scene_manager_->current_camera().get(), scene_manager_->current_camera()->target(), true);

    pen_on_surface_ = true;

    if (event->button() == Qt::LeftButton) {
        Point target;
        SceneNode * n = GetSelection(mouse_input_controller_, target, false);
        //scene_manager_->current_camera()->set_target(target);

        if (n == nullptr) { // click on void cancels any selection
            scene_manager_->RemoveAllFromSelection();
//            selection_handlers_.clear();
        } else { // click on an item = focus on that item
            scene_manager_->RemoveAllFromSelection();
            scene_manager_->AddToSelection(n->id());
            selection_handlers_.erase(n->id());
            selection_handlers_.insert(n->ancestor()->id());
            n->ancestor()->manipulator()->penBegin(mouse_input_controller_, scene_manager_->get_node(n->ancestor()->id()), pen_size_);
         }
    }

    // Notify
    scene_manager_->NotifyUpdate();
        return true;
}

bool SceneManipulator::penUpdate ( QMouseEvent *event){
    //Register Event not done because it is actually a mouse event.
//    event->registerEventType();
    // Register Event
    mouse_input_controller_->registerEvent(event);
    mouse_input_controller_->allInput2DTo3D(scene_manager_->current_camera().get(), scene_manager_->current_camera()->target(), true);

    if (pen_on_surface_){
        // If Selection: Move Object
        bool res = false;
        auto selection = selection_handlers();
        while (selection->HasNext()) {
            std::shared_ptr<SceneNode> node = selection->Next();
            res |= node->manipulator()->penUpdate(mouse_input_controller_, node, pen_size_);
        }

    }
    // Notify
    scene_manager_->NotifyUpdate();
    NotifyUpdate();
    return true;
}

bool SceneManipulator::penEnd ( QMouseEvent *event){
    //Register Event not done because it is actually a mouse event.
//    event->registerEventType();
    pen_on_surface_ = false;

    mouse_input_controller_->registerEvent(event);
    mouse_input_controller_->allInput2DTo3D(scene_manager_->current_camera().get(), scene_manager_->current_camera()->target(), true);

    // If Selection: Move Object
    bool res = false;
    auto selection = selection_handlers();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        res |= node->manipulator()->mouseReleased(mouse_input_controller_, node);
        res |= node->manipulator()->penEnd(mouse_input_controller_, node);
    }

    // Notify
    scene_manager_->NotifyUpdate();
    return true;
}

void SceneManipulator::resizeEvent(const QSize& newSize, const QSize& /*oldSize */)
{
    uint width = newSize.width();
    uint height = newSize.height();
    mouse_input_controller_->setInputSize(width, height);
    keyboard_input_controller_->setInputSize(width, height);
    multitouch_input_controller_->setInputSize(width, height);
    bool res = false;
    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->resize(width, height)) {
            res = true;
        }
    }
    if (res) {
        scene_manager_->NotifyUpdate();
    }
}

bool SceneManipulator::event(QEvent *e)
{
    switch (e->type()) {
    case QEvent::HoverEnter:
        mouse_input_controller_->set_mouse_in_window(true);
        break;
    case QEvent::HoverLeave:
        mouse_input_controller_->set_mouse_in_window(false);
        break;
    default:
        break;
    }

    return false;
}

void SceneManipulator::redisplay()
{
    double curFrameTime = timer_.end();
    bool res = false;
    for (auto listener : listeners_) {
        if (dynamic_cast<SceneManipulatorListener*>(listener)->redisplay(curFrameTime, curFrameTime - lastFrameTime_)) {
            res = true;
        }
    }

    // If Selection: Move Object
    auto selection = selection_handlers();
    while (selection->HasNext()) {
        std::shared_ptr<SceneNode> node = selection->Next();
        res |= node->manipulator()->redisplay(curFrameTime, curFrameTime - lastFrameTime_);
    }

    res |= camera_manipulator_->redisplay(curFrameTime, curFrameTime - lastFrameTime_);

    if (res) {
        scene_manager_->NotifyUpdate();
    }
    lastFrameTime_ = curFrameTime;
}

///////////////////////////////////////////////////////////////////////////////
//
SceneManager::NodeIteratorPtr SceneManipulator::selection_handlers() const {
    return make_shared<SceneManager::NodeSetIterator>(scene_manager_.get(), selection_handlers_);
}

///////////////////////////////////////////////////////////////////////////////
// Selection

void SceneManipulator::GetSelectionInArea(const AABBox2D &box, std::set<SceneManager::NodeId> &selected_items)
{
    std::shared_ptr<core::Camera> camera_manager = scene_manager_->current_camera();
    scene_manager_->GetCameraBoxIntersection(camera_manager.get(), box, selected_items);
}

void SceneManipulator::GetSelectionInArea(const Point2 &center, Scalar radius, std::set<SceneManager::NodeId> &selected_items)
{
    std::shared_ptr<core::Camera> camera_manager = scene_manager_->current_camera();
    scene_manager_->GetCameraBrushIntersection(camera_manager.get(), center, radius, selected_items);
}

SceneNode *SceneManipulator::GetSelection(std::shared_ptr<MouseIC> event, Point& target, bool add_to_selection) {
    auto p = event->get2DInput();

    std::shared_ptr<core::Camera> camera_manager = scene_manager_->current_camera();
    SceneNode * n = scene_manager_->GetCameraRayIntersection(camera_manager.get(), p[0], p[1], target);

//    target = Point(0,0,0);

    double weight = 0;

    if (n != nullptr) { // something was selected
        if (add_to_selection) {
            bool addIt = true;
            if (scene_manager_->selected_node_count() > 0) { // if there is already a selection
                // if the ctrl-key modifier is pressed, it means we keep previous selection and just add new stuff.
                // unless the clicked item was already selected, in which case we remove it from selection.
                if (!(event->modifiers() & Qt::ControlModifier)) {
                    if (n->selected() == false) {
                        scene_manager_->RemoveAllFromSelection();
//                        selection_handlers_.clear();
                    }
                } else {
                    if (n->selected() && scene_manager_->selected_node_count() > 1) {
                        addIt = false;
                        scene_manager_->RemoveFromSelection(n->id());
//                        selection_handlers_.erase(n->manipulation_handler_id());
                    }
                }
            }

            // make sure that selection handlers don't create duplicated actions :
            // for example, if both the SceneNode and it's proxy are displayed and selected, mouse actions will occur twice on the proxy, which we don't want.
            // There are no such issues for items without a proxy.
            if (n->HasManipulatorProxy() && selection_handlers_.find(n->manipulator_proxy()->id()) != selection_handlers_.end()) {
                addIt = false;
            }
            if (addIt) {
//                selection_handlers_.insert(n->manipulation_handler_id());
                scene_manager_->AddToSelection(n->id());

                target = weight * target + n->bounding_box().ComputeCenter();
                weight += 1.0;
                target /= weight;
            }
        }
    }

    return n;
}

SceneNode *SceneManipulator::GetSelection(std::shared_ptr<MultiTouchIC> event, Point& target, bool add_to_selection) {
    return GetSelection(event, target, event->getThumbId(), add_to_selection);
}

SceneNode *SceneManipulator::GetSelection(std::shared_ptr<MultiTouchIC> event, Point& target, int finger_id, bool add_to_selection) {

    auto p = event->get2DInput(finger_id);

    std::shared_ptr<core::Camera> camera_manager = scene_manager_->current_camera();
    SceneNode * n = scene_manager_->GetCameraRayIntersection(camera_manager.get(), p[0], p[1], target, true);

    if (n != nullptr) { // something was selected
        if (add_to_selection) {
            bool addIt = true;
            if (scene_manager_->selected_node_count() > 0) { // if there is already a selection
                // Todo : handle multiple selection ? (used to be with ctrl+click)
                if (n->selected() == false) {
                    scene_manager_->RemoveAllFromSelection();
//                    selection_handlers_.clear();
                }
            }

            if (addIt) {
                scene_manager_->AddToSelection(n->id());
                selection_handlers_.erase(n->id());
                selection_handlers_.insert(n->ancestor()->id()); // TODO check if ancestor is still the correct thing to select here.
            }
        }
    } else {
        if (event->isDoubleTouch()){
            scene_manager_->RemoveAllFromSelection();
//            selection_handlers_.clear();
            Point center = camera_manager->GetPointInWorld() + (target-camera_manager->GetPointInWorld()).dot(camera_manager->GetRealDirection())*camera_manager->GetRealDirection();
            camera_manager->set_target(center);
        }
    }

    return n;

}

void SceneManipulator::DeleteSelection()
{
//    scene_manager_->command_manager()->run(new DeleteNodeCommand(scene_manager_, scene_manager_->selected_node_ids()));
    scene_manager_->RemoveSelectedNodes();
}

///////////////////////////////////////////////////////////////////////////////
//
void SceneManipulator::CheckManipulator(std::shared_ptr<SceneNode> node) {
    if (node->manipulator() == nullptr) {
        node->set_manipulator(default_manipulator_);
    }
}

void SceneManipulator::SceneNodeAdded(NodeId node) {
    CheckManipulator(scene_manager_->get_node(node));
}

void SceneManipulator::SceneNodeRemoved(NodeId node) {
    selection_handlers_.erase(node);
}

void SceneManipulator::SceneNodeSelected(NodeId node)
{
    selection_handlers_.insert(scene_manager_->get_node(node)->manipulation_handler_id());
}

void SceneManipulator::SceneNodeUnselected(NodeId node)
{
    selection_handlers_.erase(scene_manager_->get_node(node)->manipulation_handler_id());
}

void SceneManipulator::UpdateSceneNodeMesh(NodeId node)
{
    emit MeshNeedsUpdating(node);
}

void SceneManipulator::UpdateMesh(NodeId node)
{
    auto n = scene_manager_->get_node(node);

    auto mesh = n->tri_mesh();

    if (mesh != nullptr) {
        std::lock_guard<std::mutex> mlock(mesh->get_buffers_mutex());
        mesh->ForceUpdate();
        //        mesh->internal_mesh()->clearBuffers();
        n->set_tri_mesh(mesh);
    }
}

//void SceneManipulator::SelectionUpdated(SceneManager *manager)
//{
//    RefreshSelectionHandlers();
//}

void SceneManipulator::RefreshSelectionHandlers()
{
    selection_handlers_.clear();
    SceneManager::NodeIteratorPtr nodes = scene_manager_->selected_nodes();
    while (nodes->HasNext()) {
        auto n = nodes->Next();
        selection_handlers_.insert(n->manipulation_handler_id());
    }
}

} // namespace plugintools
} // namespace expressive
