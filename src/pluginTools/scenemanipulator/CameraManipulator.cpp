#include <pluginTools/scenemanipulator/CameraManipulator.h>

#include "core/scenegraph/Camera.h"
#include "core/scenegraph/SceneCommands.h"

// Dependencies: pluginTools
#include <pluginTools/inputcontroller/MouseInputController.h>
#include <pluginTools/inputcontroller/KeyboardInputController.h>
#include <pluginTools/inputcontroller/MultiTouchInputController.h>
#include <pluginTools/ground/CameraGround.h>
#include <pluginTools/GLExpressiveEngine.h>
#include <pluginTools/scenemanipulator/SceneManipulator.h>

#include <core/utils/ConfigLoader.h>
#include "core/utils/KeyBindingsList.h"

using namespace std;
using namespace expressive::core;

namespace expressive {
namespace plugintools {

//////////////////////////////////////////////////////////////////////
// Constructor / Destructor

CameraManipulator::CameraManipulator(Camera* camera_manager, Scalar speed) :
    camera_manager_(camera_manager),
    cam_rot_mode_(ECamRotationMode(Config::getIntParameter("default_mouse_rotate_mode"))),
//    cam_rot_mode_(E_TrackballRotationMode),
    key_direction_(E_NONE), moving_(false), move_speed_(speed),
    slow_mode_(false), world_translation_mode_(false),
    zoom_relative_to_target_distance_(false),
    ground_ {std::make_shared<CameraGround>()}, groundMode_ {EGroundMode::None}, ground_camera_height_ {1.65},
    active_script_(false)
{}

//////////////////////////////////////////////////////////////////////
// Input Controller

void CameraManipulator::set_camera_speed(Scalar speed)
{
    move_speed_ = speed;
}

void CameraManipulator::set_zooming_relative_to_target_distance(bool b)
{
    zoom_relative_to_target_distance_ = b;
}

bool CameraManipulator::redisplay(double /*t*/, double dt)
{
    // dt is in microseconds, speed is in units per second -> We need to scale the speed.
    Scalar speed =  move_speed_ * dt / 1000000.f;
    // force speed to be below a 10 fps mode
    speed = min(speed, move_speed_ * 0.166f);
    if (slow_mode_) {
        speed /= 10.f;
    }

    Vector3 translation = Vector3::Zero();

    if (key_direction_ & E_FORWARD)  { translation -= Vector3::UnitZ() * speed; }
    if (key_direction_ & E_BACKWARD) { translation += Vector3::UnitZ() * speed; }
    if (key_direction_ & E_LEFT)     { translation -= Vector3::UnitX() * speed; }
    if (key_direction_ & E_RIGHT)    { translation += Vector3::UnitX() * speed; }
    if (key_direction_ & E_UP)       { translation += Vector3::UnitY() * speed; }
    if (key_direction_ & E_DOWN)     { translation -= Vector3::UnitY() * speed; }

    camera_manager_->Translate(translation, world_translation_mode_ ? SceneNode::TS_WORLD : SceneNode::TS_LOCAL);

    groundUpdate();

    if (active_script_) {
        ScriptPlayer *s = ScriptPlayer::GetSingleton();
        s->play(1);
        if (s->current_frame() == s->num_frames()) {
            active_script_ = false;
        }
    }

    return false;
}

// Mouse

bool CameraManipulator::wheelEvent(shared_ptr<InputController> event, shared_ptr<SceneNode> n) {
    // Check MouseIC and SceneNode
    shared_ptr<MouseIC> e = static_pointer_cast<MouseIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    // Camera Motion
    //    camera_manager_->Translate(0.0, 0.0, -e->wheel_delta());

    Point target = camera_manager_->target();
    Point localTarget = camera_manager_->GetPointInLocal(target);
    Scalar mult = 1.0;
    if (localTarget[2] > 0.0) {// if camera went through target and is now behind it.
        mult = -1.0;
    }

//    camera_manager_->ZoomOnTarget(exp(e->wheel_delta() * 0.1));
    Scalar dist = zoom_relative_to_target_distance_ ? std::max((float)localTarget.norm() / 10.f, 0.5f) : 1.0;
    Scalar speed = dist * mult * e->wheel_delta() * -move_speed_ / 10.0f;
    if (slow_mode_) {
        speed /= 10.f;
    }

    camera_manager_->ZoomOnTarget(speed);

    return true;
}

bool CameraManipulator::mousePressed(shared_ptr<InputController> event, shared_ptr<SceneNode> n) {
    // Check MouseIC and SceneNode
    shared_ptr<MouseIC> e = static_pointer_cast<MouseIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    // Register 3D Points
    e->allInput2DTo3D(camera_manager_, camera_manager_->target(), true);
    // Begin Camera Motion
    moving_ = true;

    return true;
}

Vector computeRotationVectorFromMouseOffset(Camera* camera_manager, const Point2 &p)
{
    Vector dir = (p[0]*camera_manager->GetRealRight() - p[1]*camera_manager->GetRealUp()).cross(camera_manager->GetRealDirection());
    //                dir = (p[0]*camera_manager_->GetRealRight()).cross(camera_manager_->GetRealDirection());

    if(dir.norm() < 1e-3)
        return Vector(0.0, 0.0, 0.0);

    dir = dir.normalized();
    dir = -dir * sqrt(p[0]*p[0] + p[1]*p[1]);


    return dir;
}

bool CameraManipulator::mouseMoved(shared_ptr<InputController> event, shared_ptr<SceneNode> n) {

    //    return false;

    // Check MouseIC and SceneNode
    shared_ptr<MouseIC> e = static_pointer_cast<MouseIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    // Register 3D Points
    e->allInput2DTo3D(camera_manager_, camera_manager_->target(), true);

    // Camera Motion
    if (moving_) {
        switch(e->button()) {
        case Qt::RightButton: {     // camera rotation
            auto p = e->get2DDelta();
            // TODO : find real coordinates and determine real angles to avoid speed issues.
            if (cam_rot_mode_ == ECamRotationMode::E_AxisLockedRotationMode) {
                if (abs(p[0]) > abs(p[1])) {
                    p[0] *= 0.1;
                    p[1] = 0.0;
                } else {
                    p[0] = 0.0;
                    p[1] *= 0.1;
                }
            } else {
                p[0] *= 0.1;
                p[1] *= 0.1;
            }

            Vector dir = computeRotationVectorFromMouseOffset(camera_manager_, p);

            Point target = dynamic_cast<GLExpressiveEngine*>(ExpressiveEngine::GetSingleton())->scene_manipulator()->last_hovered_pos();//camera_manager_->camera_manager_->target();

            if (cam_rot_mode_ == ECamRotationMode::E_AxisLockedRotationMode) {

//                Point old_p = camera_manager_->GetPointInWorld();

                camera_manager_->Rotate(/*camera_manager_->GetRealUp()*/Vector::UnitY(), target, -p[0] * 0.1, SceneNode::TS_WORLD);

//                float angle = -p[1] * 0.1;
//                float angle_max = acos(camera_manager_->GetRealDirection().normalized().dot(Vector::UnitY())) - 0.01;
//                float angle_min = M_PI - (angle_max + 0.01) - 0.01;

//                cout << angle << " " << angle_max << " " << angle_min << endl;




                float d_angle = (float)(p[1] * 0.1);
                float old_angle = (float)acos(camera_manager_->GetRealDirection().normalized().dot(Vector::UnitY()));
                float new_angle = old_angle + d_angle;


//                cout << old_angle << " " << new_angle << " " << d_angle << endl;


                new_angle = min(new_angle,  float(M_PI) - 0.01f);
                new_angle = max(new_angle, -float(0.00) + 0.01f);

                d_angle = new_angle - old_angle;


                camera_manager_->Rotate(camera_manager_->GetRealRight(), target, -d_angle, SceneNode::TS_WORLD);

                camera_manager_->LookAt(camera_manager_->target(), SceneNode::TS_WORLD);

//                if(camera_manager_->GetRealUp().dot(Vector::UnitY()) < 0.01 * camera_manager_->GetRealUp().norm())
//                {
//                    printf("restoring old cam parameters !\n");
//                    camera_manager_->SetPos(old_p);
//                    camera_manager_->LookAt(camera_manager_->target(), SceneNode::TS_WORLD);
//                }

            } else {
                camera_manager_->RotateAround(camera_manager_->target(), dir * 5 * 0.01);
            }

            break;
        }
        case Qt::MiddleButton: {     // camera translation
            camera_manager_->Translate(-e->get3DDelta(), SceneNode::TS_WORLD);
            break;
        }
        case Qt::LeftButton: {
            Point p0 = camera_manager_->GetPointInLocal(e->getOld3DInput());
            Point mv = camera_manager_->GetPointInLocal(e->get3DInput()) - camera_manager_->GetPointInLocal(e->getOld3DInput());


            if (cam_rot_mode_ == ECamRotationMode::E_AxisLockedRotationMode)
                mv[abs(mv[0]) > abs(mv[1])?1:0] = 0.0;

            camera_manager_->Yaw  (-atan(mv[0] / p0[2]), SceneNode::TS_WORLD);
            camera_manager_->Pitch( atan(mv[1] / p0[2]));
            break;
        }
        default: break;
        }
    }

    return true;
}

bool CameraManipulator::mouseReleased(shared_ptr<InputController> event, shared_ptr<SceneNode> n) {
    // Check MouseIC and SceneNode
    shared_ptr<MouseIC> e = static_pointer_cast<MouseIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    // Stop Camera Motion
    moving_ = false;

    return true;
}

// Keyboard

bool CameraManipulator::keyPressed(shared_ptr<InputController> event, shared_ptr<SceneNode> n) {
    // Check KeyboardIC and SceneNode
    shared_ptr<KeyboardIC> e = static_pointer_cast<KeyboardIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    switch (e->key()) {
    case Qt::Key_Space:
    case Qt::Key_5:
//        camera_manager_->reset();
        moveCameraTo(camera_manager_->GetDefaultView(), camera_manager_->target());
        break;
    case Qt::Key_2:
//        camera_manager_->SetBottomView();
        moveCameraTo(camera_manager_->GetBottomView(), camera_manager_->target());
        break;
    case Qt::Key_3:
        moveCameraTo(camera_manager_->GetBackView(), camera_manager_->target());
//        camera_manager_->SetBackView();
        break;
    case Qt::Key_4:
        moveCameraTo(camera_manager_->GetLeftView(), camera_manager_->target());
//        camera_manager_->SetLeftView();
        break;
    case Qt::Key_6:
        moveCameraTo(camera_manager_->GetRightView(), camera_manager_->target());
//        camera_manager_->SetRightView();
        break;
    case Qt::Key_8:
        moveCameraTo(camera_manager_->GetTopView(), camera_manager_->target());
//        camera_manager_->SetTopView();
        break;
    case Qt::Key_9:
        moveCameraTo(camera_manager_->GetFrontView(), camera_manager_->target());
//        camera_manager_->SetFrontView();
        break;
    case Qt::Key_1:
        camera_manager_->RecordCurrentView();
        break;
    case Qt::Key_7:
        moveCameraTo(camera_manager_->GetRecordedView(), camera_manager_->target());
//        camera_manager_->SetRecordedView();
        break;
    case Qt::Key_Control:
        slow_mode_ = true;
        return false;
    case Qt::Key_Alt:
        world_translation_mode_ = true;
        return false;
    case Qt::Key_Up:
        //case Qt::Key_Z:
        key_direction_ = (EMovingDirection) (key_direction_ | E_FORWARD);
        return true;
    case Qt::Key_Down:
        //case Qt::Key_S:
        key_direction_ = (EMovingDirection) (key_direction_ | E_BACKWARD);
        return true;
    case Qt::Key_Left:
        //case Qt::Key_Q:
        key_direction_ = (EMovingDirection) (key_direction_ | E_LEFT);
        return true;
    case Qt::Key_Right:
        //case Qt::Key_D:
        key_direction_ = (EMovingDirection) (key_direction_ | E_RIGHT);
        return true;
    case Qt::Key_PageUp:
        key_direction_ = (EMovingDirection) (key_direction_ | E_UP);
        return true;
    case Qt::Key_PageDown:
        key_direction_ = (EMovingDirection) (key_direction_ | E_DOWN);
        return true;
    default:
        break;
    }

    return true;
}

bool CameraManipulator::keyReleased(std::shared_ptr<core::InputController> event, std::shared_ptr<core::SceneNode> n)
{
    shared_ptr<KeyboardIC> e = static_pointer_cast<KeyboardIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    switch(e->key()) {
    case Qt::Key_Control:
        slow_mode_ = false;
        return true;
    case Qt::Key_Alt:
        world_translation_mode_ = false;
        return true;
    case Qt::Key_Up:
    case Qt::Key_Z:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_FORWARD);
        return true;
    case Qt::Key_Down:
    case Qt::Key_S:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_BACKWARD);
        return true;
    case Qt::Key_Left:
    case Qt::Key_Q:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_LEFT);
        return true;
    case Qt::Key_Right:
    case Qt::Key_D:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_RIGHT);
        return true;
    case Qt::Key_PageUp:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_UP);
        return true;
    case Qt::Key_PageDown:
        key_direction_ = (EMovingDirection) (key_direction_ & !E_DOWN);
        return true;
    }

    return false;
}
// Touch

bool CameraManipulator::touchBegin (shared_ptr<InputController> event, shared_ptr<SceneNode> n) {
    // Check TouchIC and SceneNode
    shared_ptr<MultiTouchIC> e = static_pointer_cast<MultiTouchIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    // Register 3D Points
    e->allInput2DTo3D(camera_manager_, camera_manager_->target(), true);
    e->setThumbDist(500.);

    // Register Phase Analysis
    bool tr = false, rt = false, sc = false;
    e->phaseAnalysis(tr, rt, sc);

    // Begin Camera Motion
    moving_  = true;
    t_state_ = T_IDLE;

    return true;
}

bool CameraManipulator::touchUpdate(shared_ptr<InputController> event, shared_ptr<SceneNode> n) {
    // Check TouchIC and SceneNode
    shared_ptr<MultiTouchIC> e = static_pointer_cast<MultiTouchIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    // Register 3D Points
    e->allInput2DTo3D(camera_manager_, camera_manager_->target(), true);

    // Register 2Handed
    bool two_handed = e->isTwoHanded();

    // Register Phase Analysis
    bool tr1 = false, rt1 = false, sc1 = false;
    bool tr2 = false, rt2 = false, sc2 = false;

    if (two_handed) {
        e->getHand1()->phaseAnalysis(tr1, rt1, sc1);
        e->getHand2()->phaseAnalysis(tr2, rt2, sc2);
    } else {
        e->phaseAnalysis(tr1, rt1, sc1);
    }

    // Camera Motion
    if (moving_) {

        switch (t_state_) {

        case T_IDLE: {
            if (two_handed) {
                if      (tr2)        { t_state_ = T_2H_TRANSLATION; }
                else if (sc2)        { t_state_ = T_2H_SCALING;     }
                else if (tr1 && rt1) { t_state_ = T_TR_AND_ROT;     }
                else if (tr1)        { t_state_ = T_TRANSLATION;    }
                else if (rt1)        { t_state_ = T_ROTATION;       }
                else if (sc1)        { t_state_ = T_SCALING;        }
            } else {
                if      (tr1 && rt1) { t_state_ = T_TR_AND_ROT;  }
                else if (tr1)        { t_state_ = T_TRANSLATION; }
                else if (rt1)        { t_state_ = T_ROTATION;    }
                else if (sc1)        { t_state_ = T_SCALING;     }
            }
            break;
        }

        case T_TRANSLATION: {
            // State Changes:
            if (!tr1) { t_state_ = T_IDLE; return true; }
            if ( rt1) { t_state_ = T_TR_AND_ROT; }

            // Motion
            camera_manager_->Translate(-e->get3DDelta(e->getThumbId()), SceneNode::TS_WORLD);
            break;
        }

        case T_ROTATION: {
            // State Changes:
            if (!rt1) { t_state_ = T_IDLE; return true; }
            if ( tr1) { t_state_ = T_TR_AND_ROT; }

            // Motion

            ////            camera_manager_->RotateAround(camera_manager_->target(), computeRotationVectorFromMouseOffset(camera_manager_, e->get2DDelta()));
            //            camera_manager_->Roll(e->getRotationData()); // *180./M_PI;
            break;
        }

        case T_TR_AND_ROT: {
            // State Changes:
            if      (!tr1 && !rt1) { t_state_ = T_IDLE; return true; }
            else if ( tr1 && !rt1) { t_state_ = T_ROTATION; }
            else if (!tr1)         { t_state_ = T_TRANSLATION; }

            // Motion
            camera_manager_->Translate(-e->get3DDelta(e->getThumbId()), SceneNode::TS_WORLD);
            //camera_manager_->Roll(e->getRotationData()*57.2957795131); // *180./M_PI;
            break;
        }

        case T_SCALING: {
            // State Changes:
            if (!sc1) { t_state_ = T_IDLE; return true; }

            // Motion
            std::cout << "Scaling : " << e->getScaleData(true) << std::endl;
            camera_manager_->ZoomOnTarget((e->getScaleData(true) - 1.0)*move_speed_);
            break;
        }

        case T_2H_TRANSLATION: {
            // State Changes:
            if (!tr2) { t_state_ = T_IDLE; return true; }

            //            // Motion
            //            auto   p = e->getHand2()->get2DDelta(e->getHand2()->getThumbId());
            //            Scalar x = p[0];
            //            Scalar y = p[1];

            //            camera_manager_->Translate(-e->getHand1()->get3DDelta(e->getHand1()->getThumbId()), SceneNode::TS_WORLD);

            //            camera_manager_->Yaw  (x);
            //            camera_manager_->Pitch(y);


            auto p = e->getHand2()->get2DDelta(e->getHand2()->getThumbId());
            p *= 0.1;

            camera_manager_->Rotate(/*camera_manager_->GetRealUp()*/Vector::UnitY(), camera_manager_->target(), -p[0] * 0.1, SceneNode::TS_WORLD);
            camera_manager_->Rotate(camera_manager_->GetRealRight(), camera_manager_->target(), -p[1] * 0.1, SceneNode::TS_WORLD);

            camera_manager_->LookAt(camera_manager_->target(), SceneNode::TS_WORLD);

            break;
        }

        case T_2H_SCALING: {
            // State Changes:
            if (!sc2) { t_state_ = T_IDLE; return true; }

            // Motion
            camera_manager_->Translate   (-e->getHand1()->get3DDelta  (e->getHand1()->getThumbId()), SceneNode::TS_WORLD);
            camera_manager_->ZoomOnTarget(( e->getHand2()->getScaleData() - 1.0)*move_speed_);
            break;
        }
        default: assert(false); // Should Not Happen
        }
    }

    if (t_state_ != T_IDLE) {
        if (two_handed) {
            e->getHand1()->store_current_2D();
            e->getHand2()->store_current_2D();
        }else{
            e->store_current_2D();
        }
    }
    return true;
}

bool CameraManipulator::touchEnd(shared_ptr<InputController> event, shared_ptr<SceneNode> n) {
    // Check TouchIC and SceneNode
    shared_ptr<MultiTouchIC> e = static_pointer_cast<MultiTouchIC>(event);
    if (e == nullptr || n != nullptr) { assert(false); return false; }

    // Stop Camera Motion
    moving_ = false;

    return true;
}

KeyBindingsList CameraManipulator::GetKeyBindings() const
{
    KeyBindingsList kbl;

    kbl.AddKey("Camera", "Space", "Reset camera position");
    kbl.AddKey("Camera", "5", "Reset camera position");
    kbl.AddKey("Camera", "2", "Go to bottom view");
    kbl.AddKey("Camera", "3", "Go to back view");
    kbl.AddKey("Camera", "4", "Go to left view");
    kbl.AddKey("Camera", "6", "Go to right view");
    kbl.AddKey("Camera", "8", "Go to top view");
    kbl.AddKey("Camera", "9", "Go to front view");
    kbl.AddKey("Camera", "1", "Record camera position");
    kbl.AddKey("Camera", "7", "Go to recorded camera position");
    kbl.AddKey("Camera", "CTRL", "Enable slow motion camera mode");
    kbl.AddKey("Camera", "Alt", "Forces translation in world space instead of camera space");
    kbl.AddKey("Camera", "Up Arrow", "Move Forward");
    kbl.AddKey("Camera", "Down Arrow", "Move backward");
    kbl.AddKey("Camera", "Right Arrow", "Strafe right");
    kbl.AddKey("Camera", "Left Arrow", "Strafe left");
    kbl.AddKey("Camera", "Z", "Move Forward");
    kbl.AddKey("Camera", "S", "Move Backward");
    kbl.AddKey("Camera", "D", "Strafe right");
    kbl.AddKey("Camera", "Q", "Strafe left");
    kbl.AddKey("Camera", "Page Up", "Move up");
    kbl.AddKey("Camera", "Page Down", "Move down");

    return kbl;
}

void CameraManipulator::groundUpdate()
{
    if(groundMode_ == EGroundMode::None)
        return;

    Point pos = camera_manager_->GetPointInWorld();
    static int count = 0;
    static double min_t = 1000000.0;
    static double max_t = 0;
    static double sum_t = 0;



    std::chrono::time_point<std::chrono::high_resolution_clock> chrono_start, chrono_stop;
    chrono_start = std::chrono::high_resolution_clock::now();

    std::pair<bool, double> groundH = ground_->height(Vector2(pos[0], pos[2]));

    chrono_stop = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed_seconds = chrono_stop-chrono_start;
    double time = 1000.0 * elapsed_seconds.count();

    min_t = std::min(min_t, time);
    max_t = std::max(max_t, time);
    sum_t += time;

    if(++count ==100)
    {
        std::cerr << "Profile: " << min_t << " - "<<max_t << " (avg: " << sum_t / (float)count << ")" << std::endl;
        count = 0;
        min_t = 1000000.0;
        max_t = 0;
        sum_t = 0;

    }


    if(groundH.first)
    {
        if(groundMode_ == EGroundMode::Limit)
            pos[1] = std::max(pos[1], groundH.second + ground_camera_height_);
        else
            pos[1] = groundH.second + ground_camera_height_;
    }

    camera_manager_->SetPos(pos, SceneNode::TS_WORLD);
}

void CameraManipulator::moveCameraTo(const Point &p)
{
    return moveCameraTo(p, camera_manager_->target());
}

void CameraManipulator::moveCameraTo(const Point &p, const Point &target)
{
    ScriptPlayer *s = ScriptPlayer::GetSingleton();

    ScriptElement elt(std::make_shared<MoveNodeCommand>(camera_manager_->owner(), camera_manager_->id(), p, target, camera_manager_->GetPointInWorld(), camera_manager_->target()), 0, 30);
    s->clear();
    s->addScriptElement(elt);
    active_script_ = true;
}

} // namespace plugintools

} // namespace expressive
