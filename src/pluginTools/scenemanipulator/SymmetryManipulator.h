/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SYMMETRYMANIPULATOR_H
#define SYMMETRYMANIPULATOR_H

// Dependencies: core
#include "core/graph/GraphCommands.h"

// Dependencies: pluginTools
#include "pluginTools/PluginToolsRequired.h"

namespace expressive {

namespace core {
    class SceneManager;
    class SceneNode;
    class Camera;
}

namespace plugintools {

struct PLUGINTOOLS_API SymmetryStruct {
    // Getters
    virtual ~SymmetryStruct();
    virtual Point getSymmetryPoint (Point p) = 0;
    virtual Point getProjectedPoint(Point p) = 0;
};

struct PLUGINTOOLS_API SymmetryPlane : public SymmetryStruct {
    // Constructor / Destructor
    SymmetryPlane(Point center, Vector normal) : center_(center), normal_(normal) {}
    
    virtual ~SymmetryPlane() {}
    // Getters
    virtual Point getSymmetryPoint (Point p);
    virtual Point getProjectedPoint(Point p);

    // Data
    Point  center_;
    Vector normal_;
};

class PLUGINTOOLS_API SymmetryManipulator {
public:
    typedef std::map <core::WeightedVertexId, core::WeightedVertexId> Symmetries;
    typedef std::pair<core::WeightedVertexId, core::WeightedVertexId> Symmetry;

    // Constructor / Destructor
    SymmetryManipulator(core::SceneManager* scene_manager, core::Camera* camera_manager);
    virtual ~SymmetryManipulator();
    
    // SymmetryEvent
    virtual bool geometricalSymmetry(std::shared_ptr<core::SceneNode> n, std::set<core::WeightedVertexId>  ids, std::shared_ptr<core::MovePointCommand<core::WeightedPoint> > command);
    virtual bool geometricalSymmetry(std::shared_ptr<core::SceneNode> n, std::set<core::WeightedVertexId>& ids);

    // SymmetryAxis Data
    void addPlane(             SymmetryPlane* p);
    void setPlane(unsigned id, SymmetryPlane* p);
    void remPlane(unsigned id);

    inline SymmetryStruct* getPlane(unsigned id) { return planes_[id]; }

    // SymmetryVertex Data
    void addSymmetry(unsigned plane_id, core::WeightedVertexId v_id1, core::WeightedVertexId v_id2);
    void remSymmetry(unsigned plane_id, core::WeightedVertexId v_id1, core::WeightedVertexId v_id2);

    core::WeightedVertexId getSymmetry(unsigned plane_id, core::WeightedVertexId v_id);

protected:
    // Scene Data
    core::SceneManager*  scene_manager_;
    core::Camera* camera_manager_;

    // Planes of Symmetry:
    // Remark: first symmetric planes is the main one, so order should be from end to begin!
    std::vector<SymmetryStruct*> planes_;

    // Symmetry Vertex
    // Vector Id => Plane, Map => Id to Id
    std::vector<Symmetries> symmetries_;
};

} // namespace plugintools
} // namespace expressive

#endif // SYMMETRYMANIPULATOR_H
