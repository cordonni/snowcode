/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DEADLINESCENEMANIPULATORLISTENER_H
#define DEADLINESCENEMANIPULATORLISTENER_H

#include "pluginTools/scenemanipulator/SceneManipulatorListener.h"

namespace expressive
{

namespace plugintools
{


/**
 * The DeadlineSceneManipulatorListener class is used to store every piece of dirty code used for deadlines.
 * This is usable as a sandbox too. Please just remember that any piece of code here has been or will be used for deadlines.
 * Don't break stuff that isn't yours as this will probably be used in higly stressing times.
 * Also, favor copying code instead of replacing it : This will create more examples.
 */
class PLUGINTOOLS_API DeadlineSceneManipulatorListener : public SceneManipulatorListener
{
public:
    

    DeadlineSceneManipulatorListener(SceneManipulator* target = nullptr);

    virtual ~DeadlineSceneManipulatorListener();

    // Mouse Events
    virtual bool wheelEvent        (std::shared_ptr<core::InputController> /*event*/) { return false; }
    virtual bool mousePressed      (std::shared_ptr<core::InputController> /*event*/) { return false; }
    virtual bool mouseReleased     (std::shared_ptr<core::InputController> /*event*/) { return false; }
    virtual bool mouseMoved        (std::shared_ptr<core::InputController> /*event*/) { return false; }
    virtual bool mouseDoubleClicked(std::shared_ptr<core::InputController> /*event*/) { return false; }

    // Keyboard Events
    virtual bool keyPressed (std::shared_ptr<core::InputController> /*event*/);
    virtual bool keyReleased(std::shared_ptr<core::InputController> /*event*/) { return false; }

    // Touch Events
    virtual bool touchBegin (std::shared_ptr<core::InputController> /*event*/) { return false; }
    virtual bool touchUpdate(std::shared_ptr<core::InputController> /*event*/) { return false; }
    virtual bool touchEnd   (std::shared_ptr<core::InputController> /*event*/) { return false; }

    // Touch Events
    virtual bool penBegin (std::shared_ptr<core::InputController> /*event*/) { return false; }
    virtual bool penUpdate(std::shared_ptr<core::InputController> /*event*/) { return false; }
    virtual bool penEnd   (std::shared_ptr<core::InputController> /*event*/) { return false; }

    virtual void updated(SceneManipulator *) {}

    virtual core::KeyBindingsList GetKeyBindings() const;

    // Implicit surfaces : SIGG 2014
    void TransferSceneDeformationToModels();
    void RotateChangeAndRotate();
    void StartRotatingSelectedNodes(Scalar nbCircles = 1, unsigned int nbFrames = 48);
    void ChangeSelectionSpecialParameter(Scalar factor);
};

} // namespace plugintools

} // namespace expressive


#endif // DEADLINESCENEMANIPULATORLISTENER_H
