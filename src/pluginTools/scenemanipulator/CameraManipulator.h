/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef CAMERAMANIPULATOR_H
#define CAMERAMANIPULATOR_H

// Dependencies: core
#include <core/scenegraph/SceneNodeManipulator.h>

// Dependencies: plugintools
#include <pluginTools/PluginToolsRequired.h>

#include "core/VectorTraits.h"

namespace expressive {

namespace core
{
    class Camera;
}

namespace plugintools {

class CameraGround;

class PLUGINTOOLS_API CameraManipulator : public core::SceneNodeManipulator {
public:
    

    enum EMovingDirection {
        E_NONE = 0,
        E_FORWARD = 1,
        E_BACKWARD = 2,
        E_LEFT = 4,
        E_RIGHT = 8,
        E_UP = 16,
        E_DOWN = 32
    };

    // Definitions
    typedef core::SceneNodeManipulator Base;

    enum ECamRotationMode {
        E_BasicRotationMode,
        E_TrackballRotationMode,
        E_BlenderLike,              //ToDo
        E_AxisLockedRotationMode
    };

    // ground operations
    enum class EGroundMode {
        None,  // no ground interaction
        Limit, // camera is free for Y above ground
        Glue   // camera is forced to follow the ground
    };

    // Constructor/Destructor
    CameraManipulator(core::Camera*, Scalar speed = 10.f);
    /////////////////////////
    // Getters & setters   //
    /////////////////////////
    inline ECamRotationMode cam_rot_mode() const { return cam_rot_mode_; }
    inline void set_cam_rot_mode(ECamRotationMode mode) { cam_rot_mode_ = mode; }
    void set_camera_speed(Scalar speed); // < speed is indicated as units / second.
    void set_zooming_relative_to_target_distance(bool b = true);

    // ground
    std::shared_ptr<CameraGround> ground() const {return ground_;}
    void setGround(std::shared_ptr<CameraGround> g) {ground_ = g;}
    EGroundMode groundBehavior() const  { return groundMode_;}
    void setGroundBehavior(EGroundMode g) { groundMode_ = g;}
    double groundCameraHeight() const  { return ground_camera_height_;}
    void setGroundCameraHeight(double g) { ground_camera_height_ = g;}

    // Redisplay event
    virtual bool redisplay(double t, double dt);

    // Mouse Input Controller
    virtual bool wheelEvent   (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool mousePressed (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool mouseMoved   (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool mouseReleased(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);

    // Keyboard Input Controller
    virtual bool keyPressed(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool keyReleased(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);

    // Touch Events
    virtual bool touchBegin (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool touchUpdate(std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);
    virtual bool touchEnd   (std::shared_ptr<core::InputController>, std::shared_ptr<core::SceneNode> = nullptr);

    core::KeyBindingsList GetKeyBindings() const;

    void groundUpdate();

    void moveCameraTo(const Point &p);
    void moveCameraTo(const Point &p, const Point &target);

protected:
    // Camera Data
    core::Camera* camera_manager_;

    ECamRotationMode cam_rot_mode_;
    EMovingDirection key_direction_;

    // Transformation Data
    bool moving_; // < whether or not camera is being manipulated.
    Scalar move_speed_; // < speed factor for the camera. Used to scale the displacements.
    bool slow_mode_; // < if set to true, camera will move  times slower
    bool world_translation_mode_; // < if set to true, camera will move in world space instead of local space.
    bool zoom_relative_to_target_distance_; //< if set to true, zooming will slow down when reaching target.

    // Touch Data
    TouchState t_state_;

    // camera ground
    std::shared_ptr<CameraGround> ground_;
    EGroundMode groundMode_;
    double ground_camera_height_;

    // script playing
    bool active_script_;
};

} // namespace plugintools
} // namespace expressive

#endif // CAMERAMANIPULATOR_H
