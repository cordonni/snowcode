#include "pluginTools/blobtreemanagement/BlobtreeSelectionView.h"
#include "pluginTools/blobtreemanagement/BlobtreeModel.h"


namespace expressive {

BlobtreeSelectionView::BlobtreeSelectionView(QWidget* parent)
    : QTreeView(parent)
{}

BlobtreeSelectionView::~BlobtreeSelectionView()
{}

void BlobtreeSelectionView::mousePressEvent(QMouseEvent *event)
{
    //TODO : @todo
    //...->UnselectNode(...); //fonction deselectionnant les noeuds du  NodeSelectionWidget
    //  QModelIndex index = selectionModel()->currentIndex();
    //  selectionModel()->select(index,QItemSelectionModel::Deselect);

    QTreeView::mousePressEvent(event);
}

void BlobtreeSelectionView::UpdateNodeViewState()
{
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());

    QModelIndex start_index = model->index(0,0,QModelIndex());
    int current_i = 0;
    QModelIndex current_index = start_index;

    while(current_index != QModelIndex())
    {
        UpdateViewStateStateChildren(current_index);

        if(bt_view_widget_->MustBeExpanded(model->GetBlobtreeNode(current_index)))
            setExpanded(current_index, true);

        ++current_i;
        current_index = start_index.sibling(current_i,0);
    }
}

void BlobtreeSelectionView::set_blobtree_view_widget(BlobtreeViewWidget *bt_view_widget)
{
    bt_view_widget_ = bt_view_widget;
}

void BlobtreeSelectionView::UpdateViewStateStateChildren(QModelIndex & index)
{
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());

    QModelIndex parent_index = index;
    int current_i = 0;
    QModelIndex current_index = parent_index.child(current_i,0);

    while(current_index != QModelIndex())
    {
        UpdateViewStateStateChildren(current_index);

        if(bt_view_widget_->MustBeExpanded(model->GetBlobtreeNode(current_index)))
            setExpanded(current_index, true);

        ++current_i;
        current_index = parent_index.child(current_i,0);
    }
}

} // namespace expressive
