/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
  \file: BlobtreeView.h

  Language: C++

  License: Convol Licence

  \author: Cedric Zanni
  E-Mail: cedric.zanni@inrialpes.fr

  Description:  This class (widget) enable a graphical selection of node
                in the blobtree, and the retrieval of their pointer
                It is used to select parameter for BlobtreeRoot manipulation functions
                with the knowledge of the main parameter node of these functions
                (bt_view_widget_->current_node_).

                It MUST be used in conjunction of a BlobtreeViewWidget.
*/

#ifndef NODE_SELECTION_WIDGET_H
#define NODE_SELECTION_WIDGET_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include "ui_NodeSelectionWidget.h"

// Qt dependencies
#include <QTabWidget>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

// std dependencies
#include<vector>

#include <convol/blobtreenode/BlobtreeNode.h>
#include <convol/blobtreenode/BlobtreeRoot.h>

class BlobtreeViewWidget;

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO : @todo : TODO LIST
// known bug : dans le cas de la selection unique il est possible de selectionner un noeud dans
// chaque arbre, c'est alors le noeud de l'arbre principale qui est prioritaire (et il est impossible
// une fois un noeud selectionner dans un arbre de se retrouver sans noeud selectionner !!! )
///////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace expressive {

class NodeSelectionWidget : public QTabWidget, private Ui::NodeSelectionWidget
{
    Q_OBJECT

public:
    NodeSelectionWidget(QWidget *parent  = 0)
        : QTabWidget(parent)
    {
        setupUi(this);
    }
    ~NodeSelectionWidget(){}
    
    void SetCompulsoryAttributs( BlobtreeViewWidget *bt_view_widget, bool unique_selection );

    void Finished();

    /*!
     * Function for retrieval of BlobtreeNode pointer in case of a unique selection
     */
    std::shared_ptr<convol::BlobtreeNode> GetSelectedNode();
    /*!
     * Function for retrieval of BlobtreeNode pointer in case of a multiple selection
     */
    std::vector<std::shared_ptr<convol::BlobtreeNode> > GetMultiSelectedNode();

private:
    BlobtreeViewWidget *bt_view_widget_;

    bool unique_selection_;
};

} //end of namespace expressive

#endif // NODE_SELECTION_WIDGET_H
