#include <pluginTools/blobtreemanagement/AddChildrenWidget.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

// Qt Dependencies
#include <QtGui>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

// std dependencies
#include <vector>

#include <convol/blobtreenode/BlobtreeNode.h>
#include <convol/blobtreenode/BlobtreeRoot.h>

#include <pluginTools/blobtreemanagement/BlobtreeModel.h>
#include <pluginTools/blobtreemanagement/BlobtreeViewWidget.h>

namespace expressive {

AddChildrenWidget::AddChildrenWidget(BlobtreeViewWidget *bt_view_widget)
    : QWidget(0), bt_view_widget_(bt_view_widget)
{
    setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    node_selection_widget_->SetCompulsoryAttributs( bt_view_widget_, false );

    connect(validate_action, SIGNAL(clicked()), this, SLOT(PerformAction()));
    connect(cancel, SIGNAL(clicked()), this, SLOT(Cancel()));
}

void AddChildrenWidget::closeEvent(QCloseEvent * event)
{
    node_selection_widget_->Finished();

    bt_view_widget_->ResetCurrentNode();
    bt_view_widget_->BlobtreeModified();

    event->accept();
}

void AddChildrenWidget::Cancel()
{
    this->close();
}

void AddChildrenWidget::PerformAction()
{
    assert(bt_view_widget_->current_node() != nullptr);

    // Choice of the nodes to be added to the children of curent_node
    std::vector<std::shared_ptr<convol::BlobtreeNode> > new_children = node_selection_widget_->GetMultiSelectedNode();

    // Effective Action
    (bt_view_widget_->blobtree_root())->AddChildrenToNAryNode(bt_view_widget_->current_node(), new_children);

    this->close();
}

} // end namespace expressive
