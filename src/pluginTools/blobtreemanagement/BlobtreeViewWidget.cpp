#include <pluginTools/blobtreemanagement/BlobtreeViewWidget.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
// Qt Dependencies
#include <QtGui>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {

BlobtreeViewWidget::BlobtreeViewWidget(std::shared_ptr<convol::ConvolObject> convol_object,
                                       QWidget* parent)
    :   QWidget(parent),
        convol_object_(convol_object)
{
    setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    blobtree_root()->AddListener(this);

    // Initialization of the tree models
    model_root_     = new BlobtreeModel(this, blobtree_root());
    model_basket_   = new BlobtreeModel(this, blobtree_root()->basket());

    // Initialization of the tree views
    tree_view_maintree_->setModel(model_root_);
    tree_view_maintree_->set_blobtree_view_widget(this);

    tree_view_baskettree_->setModel(model_basket_);
    tree_view_baskettree_->set_blobtree_view_widget(this);

    // ...
    current_node_ = nullptr;
    selection_widget_opened_ = false;

    nb_call_ = 0;

    connect(empty_basket_button_, SIGNAL(clicked()), this, SLOT(EmptyBasket()));
}

BlobtreeViewWidget::~BlobtreeViewWidget() {
    // Delete of the tree model
    if(model_root_ != nullptr) {
        delete model_root_;
        model_root_ = nullptr;
    }

    if(model_basket_ != nullptr) {
        delete model_basket_;
        model_basket_ = nullptr;
    }

    blobtree_root()->RemoveListener(this);
}

/*
void BlobtreeViewWidget::UpdateObject() {
    delete model_root_;
    delete model_basket_;

	BlobtreeRoot& blobtree_root_ = objects_pool_->current_object().blobtree_root();
    blobtree_root_.AddListener(this);

    // Initialization of the tree models
    model_root_     = new BlobtreeModel(this, &blobtree_root_);
    model_basket_   = new BlobtreeModel(this, &blobtree_root_.nonconst_basket());

    // Initialization of the tree views     // TODO : @todo : this 4 function should be passed in setupUI if this is possible
    tree_view_maintree_->setModel(model_root_);
    tree_view_maintree_->set_blobtree_view_panel(this);

    tree_view_baskettree_->setModel(model_basket_);
    tree_view_baskettree_->set_blobtree_view_panel(this);

    // ...
    current_node_ = NULL;
    selection_widget_opened_ = false;

    nb_call_ = 0;
	id_object_ = objects_pool_->current_object_id();
}
*/

bool BlobtreeViewWidget::IsSelectable(std::shared_ptr<convol::BlobtreeNode> bt_node) const
{
    if(!selection_widget_opened_) { return true; }
    else {
        convol::BlobtreeNode const* node = current_node_.get();
        while(node != nullptr) {
            if (node == bt_node.get()) return false;
            node = node->parent();
        }
        node = bt_node.get();
        while (node != nullptr) {
            if (node == current_node_.get()) return false;
            node = node->parent();
        }
        return true;
    }
}

void BlobtreeViewWidget::SaveNodeViewState()
{
    tree_view_maintree_->SaveNodeViewState();
    tree_view_baskettree_->SaveNodeViewState();
}

bool BlobtreeViewWidget::MustBeExpanded(std::shared_ptr<convol::BlobtreeNode> bt_node) {
    // Use the data structure saved in node_view_state_ to know if the node
    // was in an expanded state before the reset of the model (and associated views)
    return (node_view_state_.find(bt_node) != node_view_state_.end());
}

void BlobtreeViewWidget::UpdateCurrentNode(BlobtreeModel * model, QModelIndex & index)
{
    //TODO:@todo : the first parameter is probably useless since a QModelIndex
    // has a parmeter model or at least I think it has it
    current_node_ = model->GetBlobtreeNode(index);
}

void BlobtreeViewWidget::ResetCurrentNode()
{
    current_node_ = nullptr;
}

void BlobtreeViewWidget::EmptyBasket()
{
    model_basket_->ModelAboutToBeReseted();
    blobtree_root()->ClearBasket();
    model_basket_->ModelReseted();
}

} //end namespace expressive
