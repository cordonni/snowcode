#include <pluginTools/blobtreemanagement/NodeOperatorInsertionWidget.h>

#include <convol/blobtreenode/BlobtreeNode.h>
#include <convol/blobtreenode/BlobtreeRoot.h>
#include <convol/blobtreenode/NodeOperatorT.h>

#include <pluginTools/blobtreemanagement/BlobtreeModel.h>
#include <pluginTools/blobtreemanagement/BlobtreeViewWidget.h>
    // CustomWidgets
        // BlobtreeNodeWidgets
        #include <pluginTools/blobtreemanagement/widgets/ChooseNodeOperatorWidget.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

// Qt Dependencies
#include <QtGui>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {

NodeOperatorInsertionWidget::NodeOperatorInsertionWidget(BlobtreeViewWidget *bt_view_widget)
    : QWidget(0), bt_view_widget_(bt_view_widget), choose_node_operator_widget_(NULL)
{
    setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    choose_node_operator_widget_ = new ChooseNodeOperatorWidget(this);

    int index = 0;
    main_group_box_vertical_layout->insertWidget(index, choose_node_operator_widget_);

    ComboBoxNodeCurrentIndexChanged();

    connect(validate_action, SIGNAL(clicked()), this, SLOT(PerformAction()));
    connect(cancel, SIGNAL(clicked()), this, SLOT(Cancel()));

    connect(choose_node_operator_widget_->combo_box_node, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxNodeCurrentIndexChanged()));
}
NodeOperatorInsertionWidget::~NodeOperatorInsertionWidget()
{
    delete choose_node_operator_widget_;
}


void NodeOperatorInsertionWidget::ComboBoxNodeCurrentIndexChanged()
{
    //TODO:@todo : effectively building the node should not be required
    std::shared_ptr<convol::BlobtreeNode> node = choose_node_operator_widget_->BuildNodeOperator();
    if(node) {
        switch (node->arity())
        {
            case -1:
                {
                    frame_new_node_position->hide();
                    node_selection_widget_->SetCompulsoryAttributs(bt_view_widget_, false);
                }
                break;
            case 2:
                {
                    frame_new_node_position->show();
                    node_selection_widget_->SetCompulsoryAttributs( bt_view_widget_, true );
                }
                break;
            default:
                assert(false);
                break;
        }

    } else {
        node_selection_widget_->SetCompulsoryAttributs(bt_view_widget_, false);
    }
}
    
void NodeOperatorInsertionWidget::closeEvent(QCloseEvent * event)
{
    node_selection_widget_->Finished();

    bt_view_widget_->ResetCurrentNode();
    bt_view_widget_->BlobtreeModified();

    event->accept();
}

    
void NodeOperatorInsertionWidget::Cancel()
{
    this->close();
}

void NodeOperatorInsertionWidget::PerformAction()
{
    assert(bt_view_widget_->current_node() != nullptr);

    // Choice of the new node to be created/inserted
    std::shared_ptr<convol::BlobtreeNode> new_node = choose_node_operator_widget_->BuildNodeOperator();

    if(new_node) {
        switch (new_node->arity())
        {
            case -1:
                {
                    std::vector<std::shared_ptr<convol::BlobtreeNode> > other_children = node_selection_widget_->GetMultiSelectedNode();
                    bt_view_widget_->blobtree_root()->InsertNAryNode(bt_view_widget_->current_node(), new_node, other_children); //, curent_node_new_position->value());
                }
            break;
            case 2:
                {
                    std::shared_ptr<convol::BlobtreeNode> second_child = node_selection_widget_->GetSelectedNode();
                    bt_view_widget_->blobtree_root()->InsertBinaryNode(bt_view_widget_->current_node(), new_node, second_child, curent_node_new_position->value());
                }
            break;
            default:
                assert(false);
                break;
        }
    }

    this->close();
}
    
}  // end of namespace expressive
