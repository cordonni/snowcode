/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
  \file: BlobtreeView.h

  Language: C++

  License: Convol Licence

  \author: Cedric Zanni
  E-Mail: cedric.zanni@inrialpes.fr

  Description:  This class, derived from QTreeView, enable a graphical view of a Blobtree.
                It allow to perform blobtree manipulation via the opening of
                pop-up widget.

                It MUST be used in conjunction of a BlobtreeViewPanel.
*/

/// TODO List ///////////////////////////////////////////
/// TODO:@todo :  Implementation of Drag&Drop actions
/////////////////////////////////////////////////////////

#ifndef BLOBTREE_VIEW_H
#define BLOBTREE_VIEW_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

// Qt dependencies
#include <QTreeView>
#include <QMenu>
#include <QContextMenuEvent>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <convol/blobtreenode/BlobtreeNode.h>
#include <convol/blobtreenode/BlobtreeRoot.h>


namespace expressive {

class BlobtreeViewWidget;


class BlobtreeView : public QTreeView
{
    Q_OBJECT

public:
    BlobtreeView(QWidget* parent = 0);
    ~BlobtreeView();

    //////////////////////////////////////
    // Graphical Manipulation Functions //
    //////////////////////////////////////

    void contextMenuEvent(QContextMenuEvent *event);

    ///////////////////////////////////////
    // Functions for View time-coherence //
    ///////////////////////////////////////

    /*!
     *  Save state (expanded or not) of node in a data structure of bt_view_widget_
     */
    void SaveNodeViewState();
    /*!
     *  Use saved data structure (of bt_view_widget_) to set the node in there
     *  previous state (expanded or not) before reset of the model and view
     */
    void UpdateNodeViewState();

    ///////////////
    // Modifiers //
    ///////////////

    void set_blobtree_view_widget(BlobtreeViewWidget *bt_view_widget);

    ///////////////////////////////////
    // Slots : Blobtree Manipulation //
    ///////////////////////////////////

public slots:
    /*!
     *  The majority of this functions use pop-up widget to perform
     *  blobtree modification
     */
    void AddChildren();
    void ChangeNodeType();
    void ChangeNodeOperatorType();
    void ChangeBlobtree();
    void InsertNodeOperator();
    void MoveToBasket();
    void MoveToMain();
    void RenameNode();

    ////////////////////////////////////
    // Recursive auxiliary functions  //
    ////////////////////////////////////

private:
    void SaveNodeViewStateChildren(QModelIndex & index);
    void UpdateViewStateStateChildren(QModelIndex & index);

public:
    BlobtreeViewWidget *bt_view_widget_;
};

} //end namespace expressive

#endif // BLOBTREE_VIEW_H
