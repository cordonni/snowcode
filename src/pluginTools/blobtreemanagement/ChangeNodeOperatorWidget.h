/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ChangeNodeOperatorWidget.h

   Language: C++

   License: Convol Licence

   \author: Cédric Zanni
   E-Mail: cedric.zanni@inria.fr

   Project: Convol-ConvolLab

   Description: Header file for ChangeNodeOperatorWidget
                This is the widget for modifying a NodeOperator in the Blobtree.

                WARNING : if a N-ary node opertor is transformed into a binary
                          then all the node after the second will be moved to basket

   Platform Dependencies: None
*/

#ifndef CHANGE_NODE_OPERATOR_WIDGET_H
#define CHANGE_NODE_OPERATOR_WIDGET_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include "ui_ChangeNodeOperatorWidget.h"

// Qt dependencies
#include <QWidget>
#include <QCloseEvent>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {

class BlobtreeViewWidget;    //#include<Convol-ConvolLabLib/include/Panels/BlobtreeViewWidget.h>
class ChooseNodeOperatorWidget; // #include<Convol-ConvolLabLib/include/CustomWidgets/BlobtreeNodeWidgets/ChooseNodeOperatorWidget.h>


class ChangeNodeOperatorWidget : public QWidget, private Ui::ChangeNodeOperatorWidget
{
    Q_OBJECT

public:

    ChangeNodeOperatorWidget(BlobtreeViewWidget *bt_view_widget);
    ~ChangeNodeOperatorWidget();

    void closeEvent(QCloseEvent * event);

private slots:
    void PerformAction();
    void Cancel();

private:
    BlobtreeViewWidget *bt_view_widget_;

    ChooseNodeOperatorWidget* choose_node_operator_widget_;
};

} // end of namespace expressive

#endif // CHANGE_NODE_OPERATOR_WIDGET_H
