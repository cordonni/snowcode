#include <pluginTools/blobtreemanagement/BlobtreeView.h>

#include <convol/blobtreenode/NodeOperatorT.h>

#include <pluginTools/blobtreemanagement/BlobtreeModel.h>
#include <pluginTools/blobtreemanagement/BlobtreeViewWidget.h>
    #include <pluginTools/blobtreemanagement/AddChildrenWidget.h>
    #include <pluginTools/blobtreemanagement/ChangeNodeOperatorWidget.h>
    #include <pluginTools/blobtreemanagement/NodeOperatorInsertionWidget.h>

/*
    #include<Convol-ConvolLabLib/include/BlobtreeManagement/ChangeNodeWidget.h>
    #include<Convol-ConvolLabLib/include/BlobtreeManagement/ChangeBlobtreeWidget.h>
    #include<Convol-ConvolLabLib/include/BlobtreeManagement/NodeParameterWidget.h>
*/
//TODO : @todo : rajouter Icon dans le menu déroulant: const QIcon & icon

namespace expressive {

BlobtreeView::BlobtreeView(QWidget* parent) : QTreeView(parent){}
BlobtreeView::~BlobtreeView(){}

void BlobtreeView::contextMenuEvent(QContextMenuEvent *event){

    // The list of actions depend whether the curent node is in the main tree or the basket
    // It also depend on the arity of the selected node

    QMenu menu(this);

    // Retrieve the selected node
    QModelIndex index = selectionModel()->currentIndex();
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());
    std::shared_ptr<convol::BlobtreeNode> bt_node = model->GetBlobtreeNode(index);

    if(bt_node == nullptr) return;

    if(bt_node->arity()==-1)
    {
        //This action is only enable for N-Ary node
        menu.addAction(QString("Add Children"),this,SLOT(AddChildren()));
        menu.addSeparator();
    }

    if(std::dynamic_pointer_cast<convol::NodeOperator>(bt_node) != nullptr)
    {
        menu.addAction(QString("Change Node Operator type"),this,SLOT(ChangeNodeOperatorType()));
    }
    menu.addAction(QString("Change Blobtree"),this,SLOT(ChangeBlobtree()));

    menu.addSeparator();

    menu.addAction(QString("Insert Node Operator"),this,SLOT(InsertNodeOperator()));

    menu.addSeparator();

    if(model->root_node() == bt_view_widget_->blobtree_root())
    {
        //The node is in the main tree
        menu.addAction(QString("Move to Basket Tree"),this,SLOT(MoveToBasket()));
    }
    else
    {
        //The node is in the basket tree
        menu.addAction(QString("Move to Main Tree"),this,SLOT(MoveToMain()));
    }

    menu.addSeparator();

    menu.addAction(QString("Rename"),this,SLOT(RenameNode()));

    menu.exec(event->globalPos());

}


void BlobtreeView::SaveNodeViewState()
{
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());

    QModelIndex start_index = model->index(0,0,QModelIndex());
    int current_i = 0;
    QModelIndex current_index = start_index;

    while(current_index != QModelIndex())
    {
        SaveNodeViewStateChildren(current_index);

        if(isExpanded(current_index))
            bt_view_widget_->node_view_state().insert(model->GetBlobtreeNode(current_index));

        ++current_i;
        current_index = start_index.sibling(current_i,0);
    }
}

void BlobtreeView::SaveNodeViewStateChildren(QModelIndex & index)
{
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());

    QModelIndex parent_index = index;
    int current_i = 0;
    QModelIndex current_index = parent_index.child(current_i,0);

    while(current_index != QModelIndex())
    {
        SaveNodeViewStateChildren(current_index);

        if(isExpanded(current_index))
            bt_view_widget_->node_view_state().insert(model->GetBlobtreeNode(current_index));

        ++current_i;
        current_index = parent_index.child(current_i,0);
    }
}

void BlobtreeView::UpdateNodeViewState()
{
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());

    QModelIndex start_index = model->index(0,0,QModelIndex());
    int current_i = 0;
    QModelIndex current_index = start_index;

    while(current_index != QModelIndex())
    {
        UpdateViewStateStateChildren(current_index);

        if(bt_view_widget_->MustBeExpanded(model->GetBlobtreeNode(current_index)))
            setExpanded(current_index, true);

        ++current_i;
        current_index = start_index.sibling(current_i,0);
    }
}

void BlobtreeView::UpdateViewStateStateChildren(QModelIndex & index)
{
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());

    QModelIndex parent_index = index;
    int current_i = 0;
    QModelIndex current_index = parent_index.child(current_i,0);

    while(current_index != QModelIndex())
    {
        UpdateViewStateStateChildren(current_index);

        if(bt_view_widget_->MustBeExpanded(model->GetBlobtreeNode(current_index)))
            setExpanded(current_index, true);

        ++current_i;
        current_index = parent_index.child(current_i,0);
    }
}

void BlobtreeView::set_blobtree_view_widget(BlobtreeViewWidget *bt_view_widget)
{
    bt_view_widget_ = bt_view_widget;
}

void BlobtreeView::AddChildren()
{
    // Recovery of the selected node ("bt_node_panel_->current_node_")
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());
    QModelIndex index = selectionModel()->currentIndex();
    bt_view_widget_->UpdateCurrentNode(model, index);

    bt_view_widget_->BlobtreeAboutToBeModified();    //TODO:@todo : il ce peut qu'il faille le passer après la création du widget

    // Pop-up for parameter selection

    AddChildrenWidget *add_children_widget = new AddChildrenWidget(bt_view_widget_);
    add_children_widget->show();
}

void BlobtreeView::ChangeNodeType()
{
/*
    // Recovery of the selected node ("bt_node_panel_->current_node_")
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());
    QModelIndex index = selectionModel()->currentIndex();
    bt_view_widget_->UpdateCurrentNode(model, index);

    bt_view_widget_->BlobtreeAboutToBeModified();

    // Pop-up for parameter selection

    ChangeNodeWidget *change_node_widget = new ChangeNodeWidget(bt_view_widget_);
    change_node_widget->show();
*/
}

void BlobtreeView::ChangeNodeOperatorType()
{
    // Recovery of the selected node ("bt_node_panel_->current_node_")
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());
    QModelIndex index = selectionModel()->currentIndex();
    bt_view_widget_->UpdateCurrentNode(model, index);

    bt_view_widget_->BlobtreeAboutToBeModified();

    // Pop-up for parameter selection
    ChangeNodeOperatorWidget* change_node_operator_widget = new ChangeNodeOperatorWidget(bt_view_widget_);
    change_node_operator_widget->show();
}

void BlobtreeView::ChangeBlobtree()
{
    /*
    // Recovery of the selected node ("bt_node_panel_->current_node_")
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());
    QModelIndex index = selectionModel()->currentIndex();
    bt_view_widget_->UpdateCurrentNode(model, index);

    bt_view_widget_->BlobtreeAboutToBeModified();

    // Pop-up for parameter selection
    ChangeBlobtreeWidget *change_blobtree_widget = new ChangeBlobtreeWidget(bt_view_widget_);
    change_blobtree_widget->show();
    */
}

void BlobtreeView::InsertNodeOperator()
{
    // Recovery of the selected node ("bt_node_panel_->current_node_")
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());
    QModelIndex index = selectionModel()->currentIndex();
    bt_view_widget_->UpdateCurrentNode(model, index);

    bt_view_widget_->BlobtreeAboutToBeModified();

    // Pop-up for parameter selection
    NodeOperatorInsertionWidget* insert_node_operator_widget = new NodeOperatorInsertionWidget(bt_view_widget_);
    insert_node_operator_widget->show();
}



void BlobtreeView::MoveToBasket()
{
    // Retrieve of the selected node
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());
    QModelIndex index = selectionModel()->currentIndex();
    bt_view_widget_->UpdateCurrentNode(model, index);

    // Perform action and update the view
    bt_view_widget_->BlobtreeAboutToBeModified();
    bt_view_widget_->blobtree_root()->MoveToBasket( bt_view_widget_->current_node() );
    bt_view_widget_->BlobtreeModified();

    bt_view_widget_->ResetCurrentNode();
}
void BlobtreeView::MoveToMain()
{
    // Retrieve of the selected node
    BlobtreeModel *model = static_cast<BlobtreeModel*> (this->model());
    QModelIndex index = selectionModel()->currentIndex();
    bt_view_widget_->UpdateCurrentNode(model, index);

    // Perform action and update the view
    bt_view_widget_->BlobtreeAboutToBeModified();
    bt_view_widget_->blobtree_root()->MoveToRoot( bt_view_widget_->current_node() );
    bt_view_widget_->BlobtreeModified();

    bt_view_widget_->ResetCurrentNode();
}

void BlobtreeView::RenameNode()
{
    /*
    edit(selectionModel()->currentIndex());
    */
}

} //end namespace expressive
