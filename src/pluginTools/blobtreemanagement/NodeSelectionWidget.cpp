#include <pluginTools/blobtreemanagement/NodeSelectionWidget.h>

#include <pluginTools/blobtreemanagement/BlobtreeModel.h>
#include <pluginTools/blobtreemanagement/BlobtreeViewWidget.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include <QtGui>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {

void NodeSelectionWidget::SetCompulsoryAttributs( BlobtreeViewWidget *bt_view_widget, bool unique_selection )
{
    bt_view_widget_ = bt_view_widget;
    bt_view_widget_->set_selection_widget_opened(true);

    unique_selection_ = unique_selection;

    tree_selection_view_maintree_->setModel( bt_view_widget_->model_root() );
    tree_selection_view_baskettree_->setModel( bt_view_widget_->model_basket() );

    //TODO : @todo : ???
    // Update the views according to the saved information
    tree_selection_view_maintree_->set_blobtree_view_widget(bt_view_widget_);
    tree_selection_view_maintree_->UpdateNodeViewState();
    tree_selection_view_baskettree_->set_blobtree_view_widget(bt_view_widget_);
    tree_selection_view_baskettree_->UpdateNodeViewState();


    if(unique_selection_)
    {
        tree_selection_view_maintree_->setSelectionMode(QAbstractItemView::SingleSelection);
        tree_selection_view_baskettree_->setSelectionMode(QAbstractItemView::SingleSelection);
    }
    else
    {
        tree_selection_view_maintree_->setSelectionMode(QAbstractItemView::MultiSelection);
        tree_selection_view_baskettree_->setSelectionMode(QAbstractItemView::MultiSelection);
    }
}


void NodeSelectionWidget::Finished()
{
    bt_view_widget_->set_selection_widget_opened(false);

    tree_selection_view_maintree_->reset();
    tree_selection_view_baskettree_->reset();
}


std::shared_ptr<convol::BlobtreeNode> NodeSelectionWidget::GetSelectedNode()
{
    //TODO : @todo : cette assert n'est pas une bonne facon de gérer les choses
    assert( unique_selection_ );

    BlobtreeModel * model;
    QModelIndex index;

    if(tree_selection_view_maintree_->selectionModel()->hasSelection())
    {
        index = tree_selection_view_maintree_->selectionModel()->currentIndex();
        model = static_cast<BlobtreeModel*> (tree_selection_view_maintree_->model());
    }
    else if(tree_selection_view_baskettree_->selectionModel()->hasSelection())
    {
        index = tree_selection_view_baskettree_->selectionModel()->currentIndex();
        model = static_cast<BlobtreeModel*> (tree_selection_view_baskettree_->model());
    }
    else
    {
        return NULL;
    }

    std::shared_ptr<convol::BlobtreeNode> bt_node = model->GetBlobtreeNode(index);

    return bt_node;
}


std::vector<std::shared_ptr<convol::BlobtreeNode> > NodeSelectionWidget::GetMultiSelectedNode()
{
    //TODO : @todo : cette assert n'est pas une bonne facon de gérer les choses
    assert( !unique_selection_ );

    std::vector<std::shared_ptr<convol::BlobtreeNode> > vector_node;

    QModelIndexList list_index;
    BlobtreeModel *model;

    // Retrieval of the selected node in the main tree
    model = static_cast<BlobtreeModel*> (tree_selection_view_maintree_->model());
    list_index = tree_selection_view_maintree_->selectionModel()->selectedIndexes();
    for(QModelIndexList::iterator iter = list_index.begin(); iter != list_index.end(); ++iter)
        vector_node.push_back( model->GetBlobtreeNode( *iter ) );

    // Retrieval of the selected node in the basket tree
    model = static_cast<BlobtreeModel*> (tree_selection_view_baskettree_->model());
    list_index = tree_selection_view_baskettree_->selectionModel()->selectedIndexes();
    for(QModelIndexList::iterator iter = list_index.begin(); iter != list_index.end(); ++iter)
        vector_node.push_back( model->GetBlobtreeNode( *iter ) );

    return vector_node;
}

}  //end of namespace expressive

