#include <pluginTools/blobtreemanagement/ChangeNodeOperatorWidget.h>

#include <convol/functors/operators/BlendSum.h>
//#include<Convol/include/functors/Operators/BlendOperators/BlendBartheT.h>
//#include<Convol/include/functors/Operators/BlendOperators/BlendCleanUnionT.h>
//#include<Convol/include/functors/Operators/BlendOperators/BlendGradientT.h>
//#include<Convol/include/functors/Operators/BlendOperators/BlendDiffT.h>

#include <convol/blobtreenode/BlobtreeNode.h>
#include <convol/blobtreenode/BlobtreeRoot.h>
#include <convol/blobtreenode/NodeOperatorT.h>
            // NodeOperators
//            #include<Convol/include/ScalarFields/BlobtreeNode/NodeOperators/NodeOperator2BlendBBCWLocalT.h>
//            #include<Convol/include/ScalarFields/BlobtreeNode/NodeOperators/NodeOperatorNWitnessedBlendT.h>

#include <pluginTools/blobtreemanagement/BlobtreeModel.h>
#include <pluginTools/blobtreemanagement/BlobtreeViewWidget.h>
#include <pluginTools/blobtreemanagement/widgets/ChooseNodeOperatorWidget.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
// Qt Dependencies
#include <QtGui>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {

ChangeNodeOperatorWidget::ChangeNodeOperatorWidget(BlobtreeViewWidget *bt_view_widget)
    : QWidget(0), bt_view_widget_(bt_view_widget), choose_node_operator_widget_(nullptr)
{
    setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    assert(bt_view_widget_->current_node() != nullptr);
    choose_node_operator_widget_ = new ChooseNodeOperatorWidget(this, std::dynamic_pointer_cast<convol::NodeOperator>(bt_view_widget_->current_node()));

    int index = 0;
    main_group_box_vertical_layout->insertWidget(index, choose_node_operator_widget_);

    connect(validate_action, SIGNAL(clicked()), this, SLOT(PerformAction()));
    connect(cancel, SIGNAL(clicked()), this, SLOT(Cancel()));
}
ChangeNodeOperatorWidget::~ChangeNodeOperatorWidget()
{
    delete choose_node_operator_widget_;
}


void ChangeNodeOperatorWidget::closeEvent(QCloseEvent * event)
{
    bt_view_widget_->ResetCurrentNode();
    bt_view_widget_->BlobtreeModified();

    event->accept();
}


void ChangeNodeOperatorWidget::Cancel()
{
    this->close();
}

void ChangeNodeOperatorWidget::PerformAction()
{
    assert(bt_view_widget_->current_node() != nullptr);

    // Choice of the new node to be created/inserted
    std::shared_ptr<convol::BlobtreeNode> new_node = choose_node_operator_widget_->BuildNodeOperator();

    if(new_node==nullptr) {
        this->close();
        return;
    }

    if(bt_view_widget_->current_node()->arity() != new_node->arity()) {
        //TODO:@todo : changing the arity of the node would potentialy require more change in the tree
        this->close();
        return;
    }

    bt_view_widget_->blobtree_root()->SwitchNode(bt_view_widget_->current_node(),new_node);

    this->close();
}

} // end of namespace expressive
