/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
  \file: BlobtreeView.h

  Language: C++

  License: Convol Licence

  \author: Cedric Zanni
  E-Mail: cedric.zanni@inrialpes.fr

  Description:  This class, derived from QAbstractItemModel, act as an interface between
                Qt View and the Blobtree data.

                It MUST be used in conjunction of a BlobtreeViewPanel.
*/

#ifndef BLOBTREE_MODEL_H
#define BLOBTREE_MODEL_H

// std dependencies
#include <list>

#include <convol/blobtreenode/BlobtreeNode.h>
#include <convol/blobtreenode/BlobtreeRoot.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

// Qt dependencies
#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {

class BlobtreeViewWidget;

class BlobtreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    BlobtreeModel(BlobtreeViewWidget *bt_view_widget, std::shared_ptr<convol::BlobtreeNode> root_node);
    ~BlobtreeModel(){}

    /////////////////////////////////////////////////////////////////////////////
    // Followning function provide a public acces to protected member function //
    /////////////////////////////////////////////////////////////////////////////

    void ModelAboutToBeReseted()
    {
        beginResetModel();
    }
    void ModelReseted()
    {
        endResetModel();
    }

    /////////////////////////////////////////////////////////
    // Redefinition of QAbstractItemModel virtual function //
    /////////////////////////////////////////////////////////

    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;

    ///////////////
    //           //
    ///////////////

    /*!
     *  ???
     */
    void GetInformation(const QModelIndex &index);

    /*!
     *  Return a pointer on the BlobtreeNode associated to the QModelIndex index.
     */
    std::shared_ptr<convol::BlobtreeNode> GetBlobtreeNode(const QModelIndex &index) const;

    /*!
     *  Return the position of bt_node among the children of its parent.
     */
    int RowIndexOfNode(const convol::BlobtreeNode* bt_node) const;

    ///////////////
    // Accessors //
    ///////////////

    std::shared_ptr<convol::BlobtreeNode> root_node() { return root_node_; }

    ///////////////
    // Attributs //
    ///////////////

private:
    BlobtreeViewWidget * bt_view_widget_;

    // Root of the blobtree to display (in practice : a BlobtreeRoot or a BlobtreeBasket)
    std::shared_ptr<convol::BlobtreeNode> root_node_;
};

} // end namespace expressive

#endif // BLOBTREE_MODEL_H
