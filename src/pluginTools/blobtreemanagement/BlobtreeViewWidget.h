/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
  \file: BlobtreeViewWidget.h

  Language: C++

  License: expressive Licence

  \author: Cedric Zanni
  E-Mail: cedric.zanni@inria.fr

  Description:  This class (widget) enable a graphical manipulation of
                a BlobtreeRoot via "derived" QTreeView-s (one for the main tree
                and one for the basket) and pop-up widgets.
                It listen to the associated BlobtreeRoot in order to update correctly its views.
*/

#ifndef BLOBTREE_VIEW_WIDGET_H
#define BLOBTREE_VIEW_WIDGET_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include "ui_BlobtreeViewWidget.h"

#include <QWidget>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <convol/ConvolObject.h>
#include <convol/blobtreenode/BlobtreeNode.h>

#include <pluginTools/blobtreemanagement/BlobtreeModel.h>
#include <pluginTools/blobtreemanagement/BlobtreeView.h>

#include <convol/blobtreenode/listeners/BlobtreeRootListener.h>

#include <set>


///////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO:@todo : TODO LIST
//  * il faudrait modifier la vue de selection (et aussi les modèle et cette classe si) afin de permettre
//      d'être complêtement sur que les parametre que l'on récupère sont correcte
//      Par exemple : on peut selectionner comme second paramêtre un père et son fils alors que ... suppression récursive
//
//  * KNOWN BUG : on peut avoir simultanément un selection dans chacun des arbres => pas cool ????
//
//  * The function IsSelectable could be partly implemented in BlobtreeRoot (avoid const_cast), it can be
//      done in 2 functions : IsDescendantOf and IsAncestorOf
///////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace expressive {

class BlobtreeViewWidget
        :   public QWidget, private Ui::BlobtreeViewPanel, public convol::BlobtreeRootListener
{
    Q_OBJECT

public:

    BlobtreeViewWidget(std::shared_ptr<convol::ConvolObject> convol_object,  QWidget *parent  = 0);
    ~BlobtreeViewWidget();

    ////////////////////////////////////
    // BlobtreeRootListener reactions //
    ////////////////////////////////////

    /*!
     * The two following functions keep the BTree View and the associated model in a coherent state
     * For the sake of simplicity, this is done in a very "rough" way :
     * The visibility (opened or not) of each node in the BlobtreeView-s is saved,
     * Then the model is reseted (=> avoid all segfault due to none updated QModelIndex)
     * The views are then re-opened according to the saved information.
     */
    virtual void BlobtreeAboutToBeModified()
    {
         if(nb_call_ == 0) {
            // Save the current state of the BlobtreeView-s (are node expanded or not?)
            SaveNodeViewState();

            // Inform the models that they will soon be reseted
            model_root_->ModelAboutToBeReseted();
            model_basket_->ModelAboutToBeReseted();
        }
        nb_call_++;
    }

    virtual void BlobtreeModified()
    {
        nb_call_--;
        if(nb_call_ == 0) {
            // Inform the models (and associated views) that the reset action must be performed
            model_root_->ModelReseted();
            model_basket_->ModelReseted();

            // Update the views according to the saved information
            tree_view_maintree_->UpdateNodeViewState();
            tree_view_baskettree_->UpdateNodeViewState();

            // Reset saving node opening state data structure
            node_view_state_.clear();
        }

        if(convol_object_->auto_updating_polygonizer() != nullptr)
            convol_object_->auto_updating_polygonizer()->UpdateSurface();
//        emit BlobtreeHasBeenModified();
    }


    ////////////////////////////////////
    /*!
     *  This function test if a node can be selected.
     *  Either there is no selection_widget opened, then a node is always "selectable".
     *  Or a selection_widget is opened (a modification of the Blobtree is currently being prepared).
     *  In this second case the node is "selectable" if, and only if, it has no chance to cause problem
     *  if it is choosen as second parameter of the modification function (test if the node contains
     *  the "current_node_" in its "line of descent" or the contrary).
     */
    bool IsSelectable(std::shared_ptr<convol::BlobtreeNode> bt_node) const;

    /*!
     * This function saved all the node of the Blobtree that are currently
     * expanded in either tree_view_maintree_ or tree_view_baskettree_.
     */
    void SaveNodeViewState();

    /*!
     * This function test whether a node in a view must be expanded or not
     * according to the information saved in node_opening_state_.
     */
    bool MustBeExpanded(std::shared_ptr<convol::BlobtreeNode> bt_node);

    ////////////////////////////////////
    /*!
     *  This function should be called before a selection of node
     *  is performed via a blobtree manipulation widget.
     *  It update the value of the current node (which is the main parameter of the
     *  blobtree manipulation function).
     */
    void UpdateCurrentNode(BlobtreeModel * model, QModelIndex & index);

    /*!
     *  This function should be called when a manipulation widget
     *  is closing. It reset the current_node_ to NULL.
     */
    void ResetCurrentNode();

    ///////////////
    // Accessors //
    ///////////////

    std::shared_ptr<convol::BlobtreeRoot> blobtree_root(){ return convol_object_->blobtree_root(); }

    BlobtreeModel* model_root(){ return model_root_; }
    BlobtreeModel* model_basket(){ return model_basket_; }

    bool selection_widget_opened(){ return selection_widget_opened_; }
    std::shared_ptr<convol::BlobtreeNode> current_node(){ return current_node_; }

    std::set<std::shared_ptr<convol::BlobtreeNode> >& node_view_state(){ return node_view_state_; }

    ///////////////
    // Modifiers //
    ///////////////

    void set_selection_widget_opened(bool selection_widget_opened)
    {
        selection_widget_opened_ = selection_widget_opened;
    }

    ////////////
    // Slots  //
    ////////////

private slots:
    void EmptyBasket();

    /////////////
    // Signals //
    /////////////

signals:
    void BlobtreeHasBeenModified();

    ///////////////
    // Attributs //
    ///////////////

private:
    // This is the implicit surface that is represented and manipulated by this widget
    std::shared_ptr<convol::ConvolObject> convol_object_;

    // Model associated to blobtree_root_
    BlobtreeModel *model_root_;     // one for the main tree
    BlobtreeModel *model_basket_;   // and another one for the basket tree

    // This attribut is set to false when no selection (via a NodeSelectionWidget) is underway
    bool selection_widget_opened_;
    // This attribut is set to NULL when no selection is underway
    // Otherwise it is set to the node selected before "launching" new modification widget
    std::shared_ptr<convol::BlobtreeNode> current_node_;

    // This data structure enable an easy update of the both tree_view_maintree_ and tree_view_baskettree_
    std::set<std::shared_ptr<convol::BlobtreeNode> > node_view_state_;

    // This integer is used to avoid "interweaving" of call to Listener
    int nb_call_;
    unsigned id_object_;
};

} //end namespace expressive

#endif // BLOBTREE_VIEW_WIDGET_H
