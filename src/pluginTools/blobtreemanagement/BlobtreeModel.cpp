#include <pluginTools/blobtreemanagement/BlobtreeModel.h>

#include <convol/blobtreenode/BlobtreeNode.h>
#include <convol/blobtreenode/BlobtreeRoot.h>

#include <pluginTools/blobtreemanagement/BlobtreeViewWidget.h>

// Qt dependencies

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include <QColor>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

/////////////////////////////////////////////////////////
// Redefinition of QAbstractItemModel virtual function //
/////////////////////////////////////////////////////////

namespace expressive {

BlobtreeModel::BlobtreeModel(BlobtreeViewWidget *bt_view_widget, std::shared_ptr<convol::BlobtreeNode> root_node)
    :   QAbstractItemModel(bt_view_widget), bt_view_widget_(bt_view_widget), root_node_(root_node){}

QVariant BlobtreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
            return QVariant();

    std::shared_ptr<convol::BlobtreeNode> bt_node = GetBlobtreeNode(index);

    switch(role)
    {
        case Qt::ForegroundRole :
            // Put the name of the current_node in dark red
            if(bt_node != bt_view_widget_->current_node())
                return QVariant();
            else
                return QVariant( QColor( Qt::darkRed ) );
            break;

        case Qt::BackgroundColorRole :
            // Put none selectable node in dark grey
            if(bt_view_widget_->IsSelectable(bt_node))
                return QVariant();
            else
                return QVariant(QColor(Qt::darkGray));
            break;

        case Qt::DisplayRole :
        case Qt::EditRole :
            /*if(index.column()==0)
            {*/
                // If first column of the tree view : display the node information
                return QVariant(
                           /* QString("%1%2%3").arg(  QString(bt_node->Name().c_str())            ,
                                                    QString(bt_node->FunctorName().c_str())     ,
                                                    QString(bt_node->ParameterName().c_str())   )*/
                            QString(bt_node->Name().c_str()) //UserFriendlyName
                               );
            /*}
            else
            {
                // If other column : display the "label" convenience string data associated to the node
                return QVariant(QString(bt_node->label.c_str()));
            }*/
            break;

        default: {}
//            return QVariant();
    }
    return QVariant();
}

QVariant BlobtreeModel::headerData(int /*section*/, Qt::Orientation /*orientation*/,
                    int /*role*/) const
{
    return QVariant("Blobtree");
}

//Returns the index of the item in the model specified by the given row, column and parent index.
QModelIndex BlobtreeModel::index(int row, int column,
                  const QModelIndex &parent) const
{
    if(!hasIndex(row, column, parent))
         return QModelIndex();

    std::shared_ptr<convol::BlobtreeNode> parent_node;
    if(!parent.isValid())
        parent_node = root_node_;
    else
        parent_node = GetBlobtreeNode(parent);
    
    if(row >= 0 && row < (int) parent_node->nb_children())
    {
        convol::BlobtreeNode* child_node = parent_node->child(row).get();

        if(child_node)
             return createIndex(row, column, (void*) (child_node));
        else
             return QModelIndex();
    }
    else
    {
         return QModelIndex();
    }
}

QModelIndex BlobtreeModel::parent(const QModelIndex &index) const
{
    if(!index.isValid())
         return QModelIndex();

    std::shared_ptr<convol::BlobtreeNode> child_node = GetBlobtreeNode(index);
    const convol::BlobtreeNode *parent_node = child_node->parent();

    if( parent_node == nullptr )
    {
        std::cout << "BlobtreeModel : parent(index) :le parent voulu est null !?! should not happen" << std::endl;
        assert(false);
        return QModelIndex();
    }

    if(parent_node == root_node_.get() )
        return QModelIndex();

    return createIndex(RowIndexOfNode(parent_node), 0, (void*) (parent_node));
}

int BlobtreeModel::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
        return 0;

    std::shared_ptr<convol::BlobtreeNode> parent_node;
    if (!parent.isValid())
         parent_node = root_node_;
    else
         parent_node = GetBlobtreeNode(parent);

     return parent_node->nb_children();
}

int BlobtreeModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 1; //for now we use a basic display
}

Qt::ItemFlags BlobtreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
    {
          return Qt::NoItemFlags;
    }
    else
    {
        /*if(bt_view_widget_->selection_widget_opened())
        {
            // Default case : no selection for Blobtree modification are underway
            if(index.column() == 0)
            {
                return  Qt::ItemIsEnabled | Qt::ItemIsSelectable;
            }
            else
            {
                // Enable the modification of the label
                return  Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
            }
        }
        else
        {
            // Selection for Blobtree modification are underway
            BlobtreeNode *bt_node = GetBlobtreeNode(index);

            if( bt_view_widget_->IsSelectable(bt_node) )
                return  Qt::ItemIsEnabled | Qt::ItemIsSelectable;
            else
                return  Qt::ItemIsEnabled;
        }*/
        Qt::ItemFlags flag = Qt::ItemIsEnabled;
        if (!index.isValid() || bt_view_widget_->IsSelectable(GetBlobtreeNode(index)))
            flag = flag | Qt::ItemIsSelectable;

        if (bt_view_widget_->selection_widget_opened() && index.column() != 0)
            flag = flag | Qt::ItemIsEditable;

        if (!bt_view_widget_->selection_widget_opened())
            flag = flag | Qt::ItemIsEditable;

        return flag;
    }
}


bool BlobtreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
     UNUSED(value);

    if (role == Qt::EditRole)
    {
        if (!index.isValid())
              return 0;

        //if(index.column() == 0)
        //    return 0;

       QString new_label = value.toString().simplified();
       std::shared_ptr<convol::BlobtreeNode> node = GetBlobtreeNode(index);
       node->set_label(new_label.toStdString());
    }
    return true;
}



///////////////
//           //
///////////////

void BlobtreeModel::GetInformation(const QModelIndex &index)
{
    //TODO:@todo : to be implemented
    if (!index.isValid()) {
    } else {
        //std::shared_ptr<convol::BlobtreeNode> bt_node = GetBlobtreeNode(index);
    }
}


std::shared_ptr<convol::BlobtreeNode> BlobtreeModel::GetBlobtreeNode(const QModelIndex &index) const
{
    if(index.isValid())
        return static_cast<convol::BlobtreeNode*>(index.internalPointer())->shared_from_this();
    else
        return nullptr;
}

int BlobtreeModel::RowIndexOfNode(const convol::BlobtreeNode* bt_node) const
{
    assert(bt_node != nullptr);

    const convol::BlobtreeNode *parent_node = bt_node->parent();
    if(parent_node == nullptr)
    {
        return 0; //TODO:@todo : I think it should not happen
    }
    else
    {
        int row_index = 0;
        for(auto& child : parent_node->children()) {
            if(child.get() == bt_node) break;

            ++row_index;
        }

        return row_index;
    }
}

} //end namespace expressive

