#include <pluginTools/blobtreemanagement/widgets/NodeProceduralDetailWidget.h>

#include <convol/factories/BlobtreeNodeFactory.h>

#include <pluginTools/blobtreemanagement/widgets/BlobtreeNodeWidgetFactory.h>

namespace expressive {

void NodeProceduralDetailWidget::DelegateConstructor()
{
    setupUi(this);

    functor_choice_widget_ = new FunctorChoiceWidget(
                convol::BlobtreeNodeFactory::GetInstance().GetBtNodeFactory(StaticName()),
                this, nullptr
                );
    verticalLayout_noise_grid_parameter->insertWidget(0,functor_choice_widget_);

    child_functor_choice_widget_ = new FunctorChoiceWidget(
                convol::BlobtreeNodeFactory::GetInstance().GetBtNodeFactory(StaticChildName()),
                this, nullptr
                );
    verticalLayout_child_freq_prop->insertWidget(3, child_functor_choice_widget_);

    display_noise_widget_ = new DisplayGaborSurfaceNoiseWidget(functor_choice_widget_, child_functor_choice_widget_, this);
    verticalLayout_noise_picture->insertWidget(0, display_noise_widget_);
}

TiXmlElement * NodeProceduralDetailWidget::GetXmlFunctor()
{
    return functor_choice_widget_->ToXml();
}

TiXmlElement * NodeProceduralDetailWidget::GetXmlAttributs()
{
    TiXmlElement *element = new TiXmlElement( StaticName() );

    element->SetDoubleAttribute("iso_value",dbl_spin_box_iso_value_->value());
    element->SetDoubleAttribute("iso_detail_inf",dbl_spin_box_iso_detail_inf_->value());
    element->SetDoubleAttribute("iso_detail_sup",dbl_spin_box_iso_detail_sup_->value());

    return element;
}

void NodeProceduralDetailWidget::UpdateWidgetFromNode(std::shared_ptr<convol::NodeOperator> node)
{
    auto casted_node = std::dynamic_pointer_cast< convol::AbstractNodeProceduralDetail > (node);

    dbl_spin_box_iso_value_->setValue(casted_node->iso_value());
    dbl_spin_box_iso_detail_inf_->setValue(casted_node->iso_detail_inf());
    dbl_spin_box_iso_detail_sup_->setValue(casted_node->iso_detail_sup());

    functor_choice_widget_->UpdateWidgetFromKernel(casted_node->noise_functor());

    node_ = node;
    spinBox_child_index->setMaximum(node_->nb_children());
    spinBox_child_index->setValue(0);
    UpdateChildWidgetFromNode(0);
}

void NodeProceduralDetailWidget::UpdateChildWidgetFromNode(int child_index)
{
    auto casted_child_node = std::dynamic_pointer_cast< convol::AbstractNodeProceduralDetailParameter > (node_->child(child_index));

    child_functor_choice_widget_->UpdateWidgetFromKernel(casted_child_node->noise_parameter());
}

void NodeProceduralDetailWidget::TestNoiseInterpolationRequested()
{
    auto ptr = functor_choice_widget_->BuildFunctor();
    if(ptr!=nullptr)
    {
        TestControllableGaborSurfaceNoiseInterpolationWidget* widget = new TestControllableGaborSurfaceNoiseInterpolationWidget(nullptr);
        widget->UpdateWidgetFromNode(node_);
        widget->show();
    }
}



static BlobtreeNodeWidgetFactory::Type<NodeProceduralDetailWidget> NodeProceduralDetailWidgetType;

} // end of namespace expressive
