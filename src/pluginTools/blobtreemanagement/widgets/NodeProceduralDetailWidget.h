/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: NodeProceduralDetailWidget.h

   Language: C++

   License: Convol Licence

   \author: Cédric Zanni
   E-Mail: cedric.zanni@inrialpes.fr

   Project: Convol-ConvolLab

   Description: Header file for NodeProceduralDetailWidget.
                This is the widget that contains the UI about NodeProceduralDetail widget.

   Platform Dependencies: None
*/

#pragma once
#ifndef NODE_PROCEDURAL_DETAIL_WIDGET_H
#define NODE_PROCEDURAL_DETAIL_WIDGET_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include "ui_NodeProceduralDetailWidget.h"

#include <QWidget>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <convol/blobtreenode/NodeProceduralDetailT.h>
#include <pluginTools/blobtreemanagement/widgets/AbstractNodeOperatorWidget.h>
#include <pluginTools/blobtreemanagement/widgets/FunctorChoiceWidget.h>
#include <pluginTools/blobtreemanagement/widgets/noisewidgets/DisplayGaborSurfaceNoiseWidget.h>
#include <pluginTools/blobtreemanagement/widgets/noisewidgets/TestControllableGaborSurfaceNoiseWidgetInterpolation.h>

#include <memory>

namespace expressive {

class NodeProceduralDetailWidget : public AbstractNodeOperatorWidget, private Ui::NodeProceduralDetailWidget
{
    Q_OBJECT

public:
    NodeProceduralDetailWidget(QWidget* parent = 0)
        : AbstractNodeOperatorWidget(parent)
    {
        DelegateConstructor();

        connect(spinBox_child_index,SIGNAL(valueChanged(int)),this, SLOT(UpdateChildWidgetFromNode(int)));
        connect(pushButton_test_noise_interpolation,SIGNAL(clicked()),this, SLOT(TestNoiseInterpolationRequested()));
    }
    ~NodeProceduralDetailWidget()
    {
        if(functor_choice_widget_) delete functor_choice_widget_;
        if(child_functor_choice_widget_) delete child_functor_choice_widget_;
        if(display_noise_widget_) delete display_noise_widget_;
    }

    virtual std::string Name() { return StaticName(); }
    static std::string StaticName() {
        return convol::AbstractNodeProceduralDetail::StaticName();
    }
    static std::string StaticChildName() {
        return convol::AbstractNodeProceduralDetailParameter::StaticName();
    }

    virtual TiXmlElement * GetXmlFunctor();
    virtual TiXmlElement * GetXmlAttributs();

    virtual void UpdateWidgetFromNode(std::shared_ptr<convol::NodeOperator> node);

private slots:
    void UpdateChildWidgetFromNode(int child_index);

    void TestNoiseInterpolationRequested();


private:
    FunctorChoiceWidget* functor_choice_widget_;
    FunctorChoiceWidget* child_functor_choice_widget_;

    DisplayGaborSurfaceNoiseWidget* display_noise_widget_;

    void DelegateConstructor();

    std::shared_ptr<convol::NodeOperator> node_; //otherwise we can't have access to children when the index is updated...
}; // End class NodeProceduralDetailWidget declaration

} // end of namespace expressive

#endif // NODE_PROCEDURAL_DETAIL_WIDGET_H
