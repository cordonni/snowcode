/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: NodeProceduralDetailParameterWidget.h

   Language: C++

   License: Convol Licence

   \author: Cédric Zanni
   E-Mail: cedric.zanni@inrialpes.fr

   Project: Convol

   Description: Header file for NodeProceduralDetailParameterWidget.
                This is the widget that contains the UI about NodeProceduralDetailParameter widget.

   Platform Dependencies: None
*/

#pragma once
#ifndef NODE_PROCEDURAL_DETAIL_PARAMETER_WIDGET_H
#define NODE_PROCEDURAL_DETAIL_PARAMETER_WIDGET_H


#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include "ui_NodeProceduralDetailParameterWidget.h"

#include <QWidget>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <convol/blobtreenode/NodeProceduralDetailT.h>
#include <convol/blobtreenode/NodeProceduralDetailParameterT.h>

#include <pluginTools/blobtreemanagement/widgets/AbstractNodeOperatorWidget.h>

#include <pluginTools/blobtreemanagement/widgets/FunctorChoiceWidget.h>

#include <pluginTools/blobtreemanagement/widgets/noisewidgets/DisplayGaborSurfaceNoiseWidget.h>

//TODO:@todo : following will change
#include <pluginTools/blobtreemanagement/widgets/noisewidgets/PolynomialDeformationMapWidget.h>


namespace expressive {

class NodeProceduralDetailParameterWidget : public AbstractNodeOperatorWidget, private Ui::NodeProceduralDetailParameterWidget
{
    Q_OBJECT

public:
    NodeProceduralDetailParameterWidget(QWidget* parent = 0)
        : AbstractNodeOperatorWidget(parent)
    {
        DelegateConstructor();
    }
    ~NodeProceduralDetailParameterWidget()
    {
        if(functor_choice_widget_) delete functor_choice_widget_;
        if(parent_functor_choice_widget_) delete parent_functor_choice_widget_;
        if(display_noise_widget_) delete display_noise_widget_;
    }

    virtual std::string Name() { return StaticName(); }
    static std::string StaticName() {
        return convol::AbstractNodeProceduralDetailParameter::StaticName();
    }
    static std::string StaticParentName() {
        return convol::AbstractNodeProceduralDetail::StaticName();
    }

    virtual TiXmlElement * GetXmlFunctor();
    virtual TiXmlElement * GetXmlAttributs();

    virtual void UpdateWidgetFromNode(std::shared_ptr<convol::NodeOperator> node);

private:
    FunctorChoiceWidget* functor_choice_widget_;
    FunctorChoiceWidget*  parent_functor_choice_widget_;
    DisplayGaborSurfaceNoiseWidget* display_noise_widget_;

    PolynomialDeformationMapWidget* polynomial_deformation_map_widget_;

    void DelegateConstructor();

}; // End class NodeProceduralDetailParameterWidget declaration

} // end of namespace expressive

#endif // NODE_PROCEDURAL_DETAIL_PARAMETER_WIDGET_H
