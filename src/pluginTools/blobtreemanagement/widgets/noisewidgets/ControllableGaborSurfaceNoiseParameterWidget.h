/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ControllableGaborSurfaceNoiseParameterWidget.h

   Language: C++

   License: Convol Licence

   \author: Cédric Zanni, Maxime Quiblier
   E-Mail: cedric.zanni@inria.fr

   Project: ???

   Description: Header file for ControllableGaborSurfaceNoiseParameterWidget.
                This is the widget that contains the UI about ControllableGaborSurfaceNoiseParameter widget.

   Platform Dependencies: None
*/

#pragma once
#ifndef BAND_LIMITED_GABOR_SURFACE_NOISE_PARAMETER_WIDGET_H
#define BAND_LIMITED_GABOR_SURFACE_NOISE_PARAMETER_WIDGET_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include "ui_ControllableGaborSurfaceNoiseParameterWidget.h"

#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include "ork/core/Logger.h"
#include "convol/functors/noises/ControllableGaborSurfaceNoise.h"
 
#include "pluginTools/convolwidgets/FunctorWidget.h"

namespace expressive {

class ControllableGaborSurfaceNoiseParameterWidget : public AbstractFunctorWidget , private Ui::ControllableGaborSurfaceNoiseParameterWidget
{
    Q_OBJECT

public:
    ControllableGaborSurfaceNoiseParameterWidget(QWidget* parent = 0)
        : AbstractFunctorWidget(parent)
    {
        setupUi(this);
    }

    ~ControllableGaborSurfaceNoiseParameterWidget() {}

    virtual std::string Name()  const { return StaticName(); }
    static std::string StaticName() {
        return convol::ControllableGaborSurfaceNoiseParameters::StaticName();
    }

    TiXmlElement* ToXml() const
    {
        TiXmlElement *element = new TiXmlElement( Name() );

        element->SetDoubleAttribute("bandwith", dbl_spin_box_bandwith_wrt_Fs->value()*dbl_spin_box_F_scale->value());
        element->SetDoubleAttribute("F_min", dbl_spin_box_F_scale->value() * (1.0 - dbl_spin_box_F_half_width_wrt_Fs->value()));
        element->SetDoubleAttribute("F_max", dbl_spin_box_F_scale->value() * (1.0 + dbl_spin_box_F_half_width_wrt_Fs->value()));

        element->SetDoubleAttribute("w0_mean",dbl_spin_box_w0_mean->value());
        element->SetDoubleAttribute("w0_half_opening",dbl_spin_box_w0_half_opening->value());

        return element;
    }

    void UpdateWidgetFromNode(const core::Functor* functor)
    {
        const convol::ControllableGaborSurfaceNoiseParameters* casted_functor
                = dynamic_cast<const convol::ControllableGaborSurfaceNoiseParameters*> (functor);

        dbl_spin_box_F_scale->setValue(casted_functor->F_scale());
        dbl_spin_box_F_half_width_wrt_Fs->setValue(casted_functor->F_half_width_wrt_Fs());
        dbl_spin_box_bandwith_wrt_Fs->setValue(casted_functor->bandwith_wrt_Fs());
        dbl_spin_box_w0_mean->setValue(casted_functor->get_w0_mean());
        dbl_spin_box_w0_half_opening->setValue(casted_functor->w0_half_opening());
    }

    /**
     * @brief Functor return a ControllableGaborSurfaceNoise functor by copy using parameters in spin boxes
     * @return a ControllableGaborSurfaceNoise functor
     */
    ///TODO:@todo : we should just use the function with xml => should be sufficient with factories and avoid copy of code...
    convol::ControllableGaborSurfaceNoiseParameters Functor() const
    {
        Scalar F_scale = dbl_spin_box_F_scale->value();
        Scalar F_half_width_wrt_Fs = dbl_spin_box_F_half_width_wrt_Fs->value();
        Scalar bandwith_wrt_Fs = dbl_spin_box_bandwith_wrt_Fs->value();
        Scalar w0_mean = dbl_spin_box_w0_mean->value();
        Scalar w0_half_opening = dbl_spin_box_w0_half_opening->value();

        auto functor = convol::ControllableGaborSurfaceNoiseParameters(
                    F_scale, bandwith_wrt_Fs, F_half_width_wrt_Fs, Point2::UnitX(), w0_half_opening);
        functor.set_w0(w0_mean, w0_half_opening);

        return functor;
    }

}; // End class ControllableGaborSurfaceNoiseParameter Widget declaration

} //end of namespace expressive

#endif // BAND_LIMITED_GABOR_SURFACE_NOISE_PARAMETER_WIDGET_H
