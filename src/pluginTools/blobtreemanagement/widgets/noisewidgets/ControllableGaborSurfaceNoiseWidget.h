/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ControllableGaborSurfaceNoiseWidget.h

   Language: C++

   License: Convol Licence

   \author: Cédric Zanni, Maxime Quiblier
   E-Mail: cedric.zanni@inria.fr

   Project: Convol

   Description: Header file for ControllableGaborSurfaceNoiseWidget.
                This is the widget that contains the UI about ControllableGaborSurfaceNoise widget.

   Platform Dependencies: None
*/

#pragma once
#ifndef CONTROLLABLE_GABOR_SURFACE_NOISE_WIDGET_H
#define CONTROLLABLE_GABOR_SURFACE_NOISE_WIDGET_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include "ui_ControllableGaborSurfaceNoiseWidget.h"

#include <QImage>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <ork/core/Logger.h>
#include <convol/functors/noises/ControllableGaborSurfaceNoise.h>
 
#include <pluginTools/convolwidgets/FunctorWidget.h>
#include <pluginTools/blobtreemanagement/widgets/noisewidgets/ControllableGaborSurfaceNoiseParameterWidget.h>

#include <chrono>


namespace expressive {

class ControllableGaborSurfaceNoiseWidget : public AbstractFunctorWidget , private Ui::ControllableGaborSurfaceNoiseWidget
{
    Q_OBJECT

public:
    ControllableGaborSurfaceNoiseWidget(QWidget* parent = 0)
        : AbstractFunctorWidget(parent)
    {
        setupUi(this);
    }

    ~ControllableGaborSurfaceNoiseWidget() {}

    virtual std::string Name()  const { return StaticName(); }
    static std::string StaticName() {
        return convol::ControllableGaborSurfaceNoise::StaticName();
    }

    TiXmlElement* ToXml() const
    {
        TiXmlElement *element = new TiXmlElement( Name() );

        element->SetDoubleAttribute("impulse",dbl_spin_box_impulse->value());
        element->SetDoubleAttribute("error",dbl_spin_box_error->value());
        element->SetAttribute("seed",spin_box_seed->value());

        return element;
    }

    void UpdateWidgetFromNode(const core::Functor* functor)
    {
        const convol::ControllableGaborSurfaceNoise* casted_functor = dynamic_cast<const convol::ControllableGaborSurfaceNoise*> (functor);

        dbl_spin_box_impulse->setValue(casted_functor->impulse());
        dbl_spin_box_error->setValue(casted_functor->error());
        spin_box_seed->setValue(casted_functor->seed());
    }

}; // End class ControllableGaborSurfaceNoise Widget declaration

} //end of namespace expressive

#endif // BAND_LIMITED_GABOR_SURFACE_NOISE_WIDGET_H
