#include <pluginTools/blobtreemanagement/widgets/noisewidgets/NoiseGaborSurfaceWidget.h>

#include <pluginTools/convolwidgets/FunctorWidgetFactory.h>

namespace expressive {

static FunctorWidgetFactory::Type<NoiseGaborSurfaceWidget> NoiseGaborSurfaceWidgetType;

} //end of namespace expressive

