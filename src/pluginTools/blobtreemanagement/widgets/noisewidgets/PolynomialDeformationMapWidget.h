/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: .h

   Language: C++

   License: Convol Licence

   \author: Cédric Zanni, Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Project: Convol

   Description: Header file for PolynomialDeformationMapWidget.
                This is the widget that contains the UI about PolynomialDeformationMap widget.

   Platform Dependencies: None
*/

#pragma once
#ifndef POLYNOMIAL_DEFORMATION_MAP_WIDGET_H
#define POLYNOMIAL_DEFORMATION_MAP_WIDGET_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include "ui_PolynomialDeformationMapWidget.h"
#include <QWidget>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <convol/functors/noises/PolynomialDeformationMap.h>



namespace expressive {


class PolynomialDeformationMapWidget : public QWidget , private Ui::PolynomialDeformationMapWidget
{
    Q_OBJECT

public:
    PolynomialDeformationMapWidget(QWidget* parent = 0)
        : QWidget(parent)
    {
        DelegateConstructor();
    }
    PolynomialDeformationMapWidget(const convol::PolynomialDeformationMap& deformation_map, QWidget* parent = 0)
        : QWidget(parent)
    {
        DelegateConstructor();
        UpdateWidgetFromFunctor(deformation_map);
    }

    ~PolynomialDeformationMapWidget()
    {}

    TiXmlElement* ToXml()
    {
        TiXmlElement *element = new TiXmlElement( convol::PolynomialDeformationMap::StaticName() );

        element->SetDoubleAttribute("deform_0_0",dbl_spin_box_deform_0_0->value());
        element->SetDoubleAttribute("deriv_0_0",dbl_spin_box_deriv_0_0->value());

        element->SetDoubleAttribute("deform_0_25",dbl_spin_box_deform_0_25->value());
        element->SetDoubleAttribute("deriv_0_25",dbl_spin_box_deriv_0_25->value());

        element->SetDoubleAttribute("deform_0_5",dbl_spin_box_deform_0_5->value());
        element->SetDoubleAttribute("deriv_0_5",dbl_spin_box_deriv_0_5->value());

        element->SetDoubleAttribute("deform_0_75",dbl_spin_box_deform_0_75->value());
        element->SetDoubleAttribute("deriv_0_75",dbl_spin_box_deriv_0_75->value());

        element->SetDoubleAttribute("deform_1_0",dbl_spin_box_deform_1_0->value());
        element->SetDoubleAttribute("deriv_1_0",dbl_spin_box_deriv_1_0->value());

        return element;
    }

//    // Return a PolynomialDeformationMap by copy using parameters in spin boxes
//    convol::PolynomialDeformationMap Functor() const
//    {
//        Scalar deform_0_0 = dbl_spin_box_deform_0_0->value();
//        Scalar deform_0_25 = dbl_spin_box_deform_0_25->value();
//        Scalar deform_0_5 = dbl_spin_box_deform_0_5->value();
//        Scalar deform_0_75 = dbl_spin_box_deform_0_75->value();
//        Scalar deform_1_0 = dbl_spin_box_deform_1_0->value();
        
//        Scalar deriv_0_0 = dbl_spin_box_deriv_0_0->value();
//        Scalar deriv_0_25 = dbl_spin_box_deriv_0_25->value();
//        Scalar deriv_0_5 = dbl_spin_box_deriv_0_5->value();
//        Scalar deriv_0_75 = dbl_spin_box_deriv_0_75->value();
//        Scalar deriv_1_0 = dbl_spin_box_deriv_1_0->value();
        
//        return convol::PolynomialDeformationMap(deform_0_0, deriv_0_0,
//                                        deform_0_25, deriv_0_25,
//                                        deform_0_5, deriv_0_5,
//                                        deform_0_75, deriv_0_75,
//                                        deform_1_0, deriv_1_0);
//    }

    void UpdateWidgetFromFunctor(const convol::PolynomialDeformationMap& deformation_map)
    {
        dbl_spin_box_deform_0_0->setValue(deformation_map.deform_0_0());
        dbl_spin_box_deform_0_25->setValue(deformation_map.deform_0_25());
        dbl_spin_box_deform_0_5->setValue(deformation_map.deform_0_5());
        dbl_spin_box_deform_0_75->setValue(deformation_map.deform_0_75());
        dbl_spin_box_deform_1_0->setValue(deformation_map.deform_1_0());

        dbl_spin_box_deriv_0_0->setValue(deformation_map.deriv_0_0());
        dbl_spin_box_deriv_0_25->setValue(deformation_map.deriv_0_25());
        dbl_spin_box_deriv_0_5->setValue(deformation_map.deriv_0_5());
        dbl_spin_box_deriv_0_75->setValue(deformation_map.deriv_0_75());
        dbl_spin_box_deriv_1_0->setValue(deformation_map.deriv_1_0());
    }

private:

    void DelegateConstructor()
    {
        setupUi(this);
    }

}; // End class NoiseGaborSurface Widget declaration

} //end of namespace expressive

#endif // POLYNOMIAL_DEFORMATION_MAP_WIDGET_H
