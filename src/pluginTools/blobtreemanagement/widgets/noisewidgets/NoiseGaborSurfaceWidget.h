/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: NoiseGaborSurface.h

   Language: C++

   License: Convol Licence

   \author: Cédric Zanni, Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Project: Convol-QtOgreViewer

   Description: Header file for NoiseGaborSurfaceWidget.
                This is the widget that contains the UI about NoiseGaborSurface widget.

   Platform Dependencies: None
*/

#pragma once
#ifndef NOISE_GABOR_SURFACE_WIDGET_H
#define NOISE_GABOR_SURFACE_WIDGET_H

#include "ui_NoiseGaborSurfaceWidget.h"

#include <ork/core/Logger.h>
#include <convol/functors/noises/NoiseGaborSurface.h>
 
#include <pluginTools/convolwidgets/FunctorWidget.h>

#include <chrono>

#include <QImage>

namespace expressive {

class NoiseGaborSurfaceWidget : public AbstractFunctorWidget , private Ui::NoiseGaborSurfaceWidget
{
    Q_OBJECT

public:
    NoiseGaborSurfaceWidget(QWidget* parent = 0)
        : AbstractFunctorWidget(parent)
    {
        setupUi(this);

        n_ = 200;
        noise_picture_ = new QImage(n_,n_,QImage::Format_ARGB32);

        connect(button_display_noise,SIGNAL(clicked()),this, SLOT(DisplayNoiseRequested()));
    }

    ~NoiseGaborSurfaceWidget()
    {
        if(noise_picture_ != nullptr)
        {
            delete noise_picture_;
            noise_picture_ = nullptr;
        }
    }

    virtual std::string Name()  const { return StaticName(); }
    static std::string StaticName() {
        return convol::NoiseGaborSurface::StaticName();
    }

    virtual TiXmlElement* ToXml() const
    {
        TiXmlElement *element = new TiXmlElement( Name() );

        element->SetDoubleAttribute("lambda",dbl_spin_box_lambda->value());
        element->SetDoubleAttribute("r",dbl_spin_box_r->value());
        element->SetDoubleAttribute("a",dbl_spin_box_a->value());
        element->SetDoubleAttribute("F0",dbl_spin_box_F0->value());
        element->SetDoubleAttribute("w0",dbl_spin_box_w0->value());

        element->SetAttribute("seed",spin_box_seed->value());

        if(check_box_isotropic->isChecked())
            element->SetAttribute("isotropic",1);
        else
            element->SetAttribute("isotropic",0);

        return element;
    }

    virtual void UpdateWidgetFromNode(const core::Functor* functor)
    {
        const convol::NoiseGaborSurface* casted_functor = dynamic_cast<const convol::NoiseGaborSurface*> (functor);

        dbl_spin_box_lambda->setValue(casted_functor->lambda());
        dbl_spin_box_r->setValue(casted_functor->r());
        spin_box_seed->setValue(casted_functor->seed());
        dbl_spin_box_F0->setValue(casted_functor->F0());
        dbl_spin_box_w0->setValue(casted_functor->w0());
        dbl_spin_box_a->setValue(casted_functor->a());

///TODO:@todo : on ne peut pas passé de isotrope à anisotrope par interpolation pour le moment...
        check_box_isotropic->setChecked(casted_functor->isotropic());
    }

    // Return a NodeGaborSurface noise by copy using parameters in spin boxes
    //TODO:@todo : for now, still usefull for the DisplayNoise
    convol::NoiseGaborSurface Functor() const
    {
        Scalar lambda = dbl_spin_box_lambda->value();
        Scalar r  = dbl_spin_box_r->value();
        unsigned int seed = spin_box_seed->value();
        Scalar F0 = dbl_spin_box_F0->value();
        Scalar w0 = dbl_spin_box_w0->value();
        Scalar a = dbl_spin_box_a->value();
        bool isotropic = check_box_isotropic->isChecked();
        
        return convol::NoiseGaborSurface(lambda, r, seed, F0, w0, a,isotropic);
    }


protected slots:
    void DisplayNoiseRequested()
    {
        convol::NoiseGaborSurface local_noise = Functor();

        std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
        start_time = std::chrono::high_resolution_clock::now();

        float pos_x, pos_y, pos_z;
        pos_z = 0.0;
        for(int i=0;i<n_;i++)
        {
            for(int j=0;j<n_;j++)
            {
                pos_x = ((Scalar) i)/(Scalar(n_));
                pos_y = ((Scalar) j)/(Scalar(n_));
                float noise_pos = local_noise(Point(pos_x, pos_y, pos_z),
                                              Point::UnitZ(),
                                              Point::UnitX(),
                                              Point::UnitY());

                float colorPixel = 255.0 * noise_pos;
                noise_picture_->setPixel(i,j,qRgb(colorPixel,colorPixel,colorPixel));
            }
        }
        QPixmap noise_pixmap_ = QPixmap::fromImage(*noise_picture_);
//        noise_pixmap_ = noise_pixmap_.scaled(200,200);
        noise_picture_label->setPixmap(noise_pixmap_);

        end_time =  std::chrono::high_resolution_clock::now();
        ork::Logger::DEBUG_LOGGER->logf("NOISE_TEXTURE", "Computation of Noise texture : %f", std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000.0);
    }

private:

    int n_;
    QImage *noise_picture_;

}; // End class NoiseGaborSurface Widget declaration

} //end of namespace expressive

#endif // NOISE_GABOR_SURFACE_WIDGET_H
