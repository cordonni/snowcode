#include <pluginTools/blobtreemanagement/widgets/noisewidgets/ControllableGaborSurfaceNoiseWidget.h>

#include <pluginTools/convolwidgets/FunctorWidgetFactory.h>

namespace expressive {

static FunctorWidgetFactory::Type<ControllableGaborSurfaceNoiseWidget> ControllableGaborSurfaceNoiseWidgetType;

} //end of namespace expressive

