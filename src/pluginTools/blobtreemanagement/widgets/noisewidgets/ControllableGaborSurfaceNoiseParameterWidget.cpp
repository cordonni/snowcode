#include <pluginTools/blobtreemanagement/widgets/noisewidgets/ControllableGaborSurfaceNoiseParameterWidget.h>

#include <pluginTools/convolwidgets/FunctorWidgetFactory.h>

namespace expressive {

static FunctorWidgetFactory::Type<ControllableGaborSurfaceNoiseParameterWidget> ControllableGaborSurfaceNoiseParameterWidgetType;

} //end of namespace expressive

