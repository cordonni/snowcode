#include <pluginTools/blobtreemanagement/widgets/noisewidgets/TestControllableGaborSurfaceNoiseWidgetInterpolation.h>

#include <convol/blobtreenode/NodeProceduralDetailT.h>
#include <convol/blobtreenode/NodeProceduralDetailParameterT.h>
#include <convol/factories/BlobtreeNodeFactory.h>
#include <convol/functors/noises/ControllableGaborSurfaceNoise.h>

namespace expressive {

void TestControllableGaborSurfaceNoiseInterpolationWidget::DelegateConstructor()
{
            functor_choice_widget_ = new FunctorChoiceWidget(
                        convol::BlobtreeNodeFactory::GetInstance().GetBtNodeFactory(convol::AbstractNodeProceduralDetail::StaticName()),
                        this, nullptr );
            functor_param_choice_widget_1_ = new FunctorChoiceWidget(
                        convol::BlobtreeNodeFactory::GetInstance().GetBtNodeFactory(convol::AbstractNodeProceduralDetailParameter::StaticName()),
                        this, nullptr );
            functor_param_choice_widget_2_ = new FunctorChoiceWidget(
                        convol::BlobtreeNodeFactory::GetInstance().GetBtNodeFactory(convol::AbstractNodeProceduralDetailParameter::StaticName()),
                        this, nullptr );
            horizontalLayout_functor->insertWidget(0, functor_choice_widget_);
            horizontalLayout_functor->insertWidget(1, functor_param_choice_widget_1_);
            horizontalLayout_functor->insertWidget(2, functor_param_choice_widget_2_);

            n_ = 200;
            noise_picture_ = new QImage(4*n_,n_,QImage::Format_ARGB32);
}

void TestControllableGaborSurfaceNoiseInterpolationWidget::UpdateWidgetFromNode(std::shared_ptr<convol::BlobtreeNode> node)
{
    auto casted_node_1 = std::dynamic_pointer_cast< convol::AbstractNodeProceduralDetail > (node);
    functor_choice_widget_->UpdateWidgetFromKernel(casted_node_1->noise_functor());

    ///TODO:@todo : add a system to choose the child index as in NodeProceduralDetailWidget.h
    auto casted_child_node = std::dynamic_pointer_cast< convol::AbstractNodeProceduralDetailParameter > (node->child(0));
    functor_param_choice_widget_1_->UpdateWidgetFromKernel(casted_child_node->noise_parameter());
    functor_param_choice_widget_2_->UpdateWidgetFromKernel(casted_child_node->noise_parameter());
}

void TestControllableGaborSurfaceNoiseInterpolationWidget::DisplayNoiseRequested()
{
    auto tmp = functor_choice_widget_->BuildFunctor();
    convol::ControllableGaborSurfaceNoise noise_functor
            = *std::static_pointer_cast<convol::ControllableGaborSurfaceNoise>(tmp);

    tmp = functor_param_choice_widget_1_->BuildFunctor();
    convol::ControllableGaborSurfaceNoiseParameters noise_param1
            = *std::static_pointer_cast<convol::ControllableGaborSurfaceNoiseParameters>(tmp);

    tmp = functor_param_choice_widget_2_->BuildFunctor();
    convol::ControllableGaborSurfaceNoiseParameters noise_param2
            = *std::static_pointer_cast<convol::ControllableGaborSurfaceNoiseParameters>(tmp);

    std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
    start_time = std::chrono::high_resolution_clock::now();

    std::vector<Scalar> vec_field = {0.0, 0.0};
    std::vector<convol::ControllableGaborSurfaceNoiseParameters>
            vec_noise_param = {noise_param1, noise_param2};

    Scalar pos_x, pos_y, pos_z;
    pos_z = 0.0;
    #pragma omp parallel for
    for(int i=0;i<4*n_;i++) {
        for(int j=0;j<n_;j++) {
            pos_x = ((Scalar) i)/(Scalar(n_)); // between 0 and 4
            pos_y = ((Scalar) j)/(Scalar(n_)); // between 0 and 1

            Scalar factor = pos_x/4.0;
            vec_field[0] = 1.0-factor;
            vec_field[1] = factor;

            Scalar noise;
            Vector grad_noise;
            noise_functor.ComputeInterpolatedNoiseAndGrad(
                        Point(pos_x, pos_y, pos_z),
                        Point::UnitZ(), Point::UnitX(), Point::UnitY(),
                        vec_field, 1.0,
                        vec_noise_param,
                        [](const std::vector<convol::ControllableGaborSurfaceNoiseParameters>&  v, int i)
                        -> const convol::ControllableGaborSurfaceNoiseParameters& { return v[i]; },
                        noise, grad_noise
            );

            noise = (noise < 0.0) ? 0.0 : ((noise > 1.0) ? 1.0 : noise); // clipping of the noise

            int colorPixel = int(255.0 * noise);
            noise_picture_->setPixel(i,j,qRgb(colorPixel,colorPixel,colorPixel));
        }
    }
    QPixmap noise_pixmap_ = QPixmap::fromImage(*noise_picture_);
    noise_picture_label->setPixmap(noise_pixmap_);

    end_time =  std::chrono::high_resolution_clock::now();
    ork::Logger::DEBUG_LOGGER->logf("NOISE_TEXTURE", "Computation of Noise texture : %f", std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000.0);
}

} //end of namespace expressive
