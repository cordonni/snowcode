/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ControllableGaborSurfaceNoiseWidget.h

   Language: C++

   License: Convol Licence

   \author: Cédric Zanni, Maxime Quiblier
   E-Mail: cedric.zanni@inria.fr

   Project: Convol

   Description: Header file for ControllableGaborSurfaceNoiseWidget.
                This is the widget that contains the UI about ControllableGaborSurfaceNoise widget.

   Platform Dependencies: None
*/

#pragma once
#ifndef TEST_CONTROLLABLE_GABOR_SURFACE_NOISE_WIDGET_H
#define TEST_CONTROLLABLE_GABOR_SURFACE_NOISE_WIDGET_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include "ui_TestControllableGaborSurfaceNoiseWidgetInterpolation.h"

#include <QImage>

#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include <ork/core/Logger.h>
#include <convol/blobtreenode/BlobtreeNode.h>
#include <pluginTools/blobtreemanagement/widgets/FunctorChoiceWidget.h>

#include <chrono>
namespace expressive {

class TestControllableGaborSurfaceNoiseInterpolationWidget : public QWidget, private Ui::TestControllableGaborSurfaceNoiseInterpolationWidget
{
    Q_OBJECT

public:
    TestControllableGaborSurfaceNoiseInterpolationWidget(QWidget* parent = 0)
        : QWidget(parent)
    {
        setupUi(this);
        setAttribute(Qt::WA_DeleteOnClose);

        DelegateConstructor();

        connect(button_display_noise,SIGNAL(clicked()),this, SLOT(DisplayNoiseRequested()));
    }

    ~TestControllableGaborSurfaceNoiseInterpolationWidget()
    {
        if(noise_picture_)
        {
            delete noise_picture_;
            noise_picture_ = nullptr;
        }
        if(functor_choice_widget_) delete functor_choice_widget_;
        if(functor_param_choice_widget_1_) delete functor_param_choice_widget_1_;
        if(functor_param_choice_widget_2_) delete functor_param_choice_widget_2_;
    }

    void DelegateConstructor();

    void UpdateWidgetFromNode(std::shared_ptr<convol::BlobtreeNode> node);

protected slots:
    void DisplayNoiseRequested();

private:
    FunctorChoiceWidget* functor_choice_widget_;
    FunctorChoiceWidget* functor_param_choice_widget_1_;
    FunctorChoiceWidget* functor_param_choice_widget_2_;

    int n_;
    QImage *noise_picture_;

}; // End class ControllableGaborSurfaceNoise Widget declaration

} //end of namespace expressive

#endif // BAND_LIMITED_GABOR_SURFACE_NOISE_WIDGET_H
