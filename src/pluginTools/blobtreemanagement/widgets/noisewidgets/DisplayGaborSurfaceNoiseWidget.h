/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: DisplayGaborSurfaceNoiseWidget.h

   Language: C++

   License: Convol Licence

   \author: Cédric Zanni, Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Project: Convol

   Description: Header file for DisplayGaborSurfaceNoiseWidgetWidget.
                This is the widget that contains the UI about DisplayGaborSurfaceNoiseWidget widget.

   Platform Dependencies: None
*/

#pragma once
#ifndef DISPLAY_GABOR_SURFACE_NOISE_WIDGET_H
#define DISPLAY_GABOR_SURFACE_NOISE_WIDGET_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif

#include "ui_DisplayGaborSurfaceNoiseWidget.h"

#include <QImage>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include "ork/core/Logger.h"

#include "pluginTools/blobtreemanagement/widgets/FunctorChoiceWidget.h"
#include "convol/functors/noises/ControllableGaborSurfaceNoise.h"

#include <chrono>

namespace expressive {

class DisplayGaborSurfaceNoiseWidget : public QWidget, private Ui::DisplayGaborSurfaceNoiseWidget
{
    Q_OBJECT

public:
    DisplayGaborSurfaceNoiseWidget(FunctorChoiceWidget* gabor_noise_widget,
                                   FunctorChoiceWidget* gabor_noise_parameter_widget,
                                   QWidget* parent = 0)
        : QWidget(parent),
          gabor_noise_widget_(gabor_noise_widget),
          gabor_noise_parameter_widget_(gabor_noise_parameter_widget)
    {
        setupUi(this);

        n_ = 200;
        noise_picture_ = new QImage(n_,n_,QImage::Format_ARGB32);

        connect(button_display_noise,SIGNAL(clicked()),this, SLOT(DisplayNoiseRequested()));
    }

    ~DisplayGaborSurfaceNoiseWidget()
    {
        if(noise_picture_) {
            delete noise_picture_;
            noise_picture_ = nullptr;
        }
    }

protected slots:
    void DisplayNoiseRequested()
    {
        auto tmp = gabor_noise_widget_->BuildFunctor();
        convol::ControllableGaborSurfaceNoise noise_functor
                = *std::static_pointer_cast<convol::ControllableGaborSurfaceNoise>(tmp);
        tmp = gabor_noise_parameter_widget_->BuildFunctor();
        convol::ControllableGaborSurfaceNoiseParameters noise_parameter
                = *std::static_pointer_cast<convol::ControllableGaborSurfaceNoiseParameters>(tmp);

        std::chrono::time_point<std::chrono::high_resolution_clock> start_time, end_time;
        start_time = std::chrono::high_resolution_clock::now();

        Scalar pos_x, pos_y, pos_z;
        pos_z = 0.0;
Scalar mean = 0.0;
//        #pragma omp parallel for
        for(int i=0;i<n_;i++)
        {
            for(int j=0;j<n_;j++)
            {
                pos_x = ((Scalar) i)/(Scalar(n_));
                pos_y = ((Scalar) j)/(Scalar(n_));
                Scalar noise = noise_functor( Point(pos_x, pos_y, pos_z),
                                             Point::UnitZ(), Point::UnitX(), Point::UnitY(),
                                             noise_parameter);

//                noise = (noise < 0.0) ? 0.0 : ((noise > 1.0) ? 1.0 : noise); // clipping of the noise

//                if(noise>10.0) {
//                    noise_picture_->setPixel(i,j,qRgb(255.0,0.0,0.0));
//                }else{
                int colorPixel = int(255.0 * noise);
                noise_picture_->setPixel(i,j,qRgb(colorPixel,colorPixel,colorPixel));
//                }
                mean += noise;
//                if(noise > 0.95) noise_picture_->setPixel(i,j,qRgb(255.0,0.0,0.0));
//                if(noise < 0.05) noise_picture_->setPixel(i,j,qRgb(0.0,0.0,255.0));
//                if(0.47<noise && noise<0.53) noise_picture_->setPixel(i,j,qRgb(0.0,255.0,0.0));
            }
        }
        QPixmap noise_pixmap_ = QPixmap::fromImage(*noise_picture_);
        noise_picture_label->setPixmap(noise_pixmap_);

        end_time =  std::chrono::high_resolution_clock::now();
        ork::Logger::DEBUG_LOGGER->logf("NOISE_TEXTURE", "Computation of Noise texture : %f", std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count()/1000.0);

        mean /= ((Scalar)(n_*n_));
        ork::Logger::DEBUG_LOGGER->logf("NOISE_TEXTURE", "Mean value of Noise texture : %f", mean);
    }

private:
    FunctorChoiceWidget* gabor_noise_widget_;
    FunctorChoiceWidget* gabor_noise_parameter_widget_;

    int n_;
    QImage *noise_picture_;

}; // End class DisplayGaborSurfaceNoiseWidget Widget declaration

} //end of namespace expressive

#endif // DISPLAY_GABOR_SURFACE_NOISE_WIDGET_H
