#include <pluginTools/blobtreemanagement/widgets/ChooseNodeOperatorWidget.h>

#include <core/functor/FunctorFactory.h>
#include <convol/factories/BlobtreeNodeFactory.h>

#include <pluginTools/blobtreemanagement/widgets/BlobtreeNodeWidgetFactory.h>


#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QComboBox>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {

void ChooseNodeOperatorWidget::InitializeComboBox()
{
    BlobtreeNodeWidgetFactory::GetInstance().InitializeComboBox(combo_box_node);
    combo_box_node->addItem(QString("Not Implemented !"), QVariant(""));

    connect(combo_box_node,SIGNAL(activated(int)),this, SLOT(UpdateWidgetFromComboBox()));
}

void ChooseNodeOperatorWidget::UpdateWidgetFromComboBox()
{
    RemoveActiveWidget();

//    if(combo_box_node->currentData().toString().size() != 0) {
    QVariant currentData = combo_box_node->itemData(combo_box_node->currentIndex());
    if (currentData.toString().size() != 0) {
        active_node_operator_widget_ = BlobtreeNodeWidgetFactory::GetInstance().CreateNewBlobtreeNodeWidget(currentData.toString().toStdString());
        active_node_operator_widget_->setParent(this);
        main_vertical_layout->insertWidget(1, active_node_operator_widget_);
        active_node_operator_widget_->show();
    }
}

void ChooseNodeOperatorWidget::UpdateWidgetFromNode(std::shared_ptr<convol::NodeOperator> node)
{
    RemoveActiveWidget();

    if(node) {
        int index = combo_box_node->findData(QVariant(node->Name().c_str()));

        if(index == -1) index = combo_box_node->count()-1;

        combo_box_node->setCurrentIndex(index);

        if(index != combo_box_node->count()-1) {
            active_node_operator_widget_ = BlobtreeNodeWidgetFactory::GetInstance().CreateNewBlobtreeNodeWidget(node->Name());
            active_node_operator_widget_->UpdateWidgetFromNode(node);
            active_node_operator_widget_->setParent(this);
            main_vertical_layout->insertWidget(1, active_node_operator_widget_);
            active_node_operator_widget_->show();
        }
    } else {
        combo_box_node->setCurrentIndex(combo_box_node->count()-1);
    }
}

std::shared_ptr<convol::BlobtreeNode> ChooseNodeOperatorWidget::BuildNodeOperator() const
{
    if(active_node_operator_widget_) {
        // Create functor
        auto element_functor = active_node_operator_widget_->GetXmlFunctor();
        auto functor = core::FunctorFactory::GetInstance().CreateFunctorFromXml(element_functor);

        // Create the node
        auto node_operator = convol::BlobtreeNodeFactory::GetInstance().CreateNewBlobtreeNode(active_node_operator_widget_->Name(), *functor);

        // Initialize parameter from xml
        auto element_attribut = active_node_operator_widget_->GetXmlAttributs();
        node_operator->InitFromXml(element_attribut);

        return node_operator;
    } else {
        return nullptr;
    }
}

}
