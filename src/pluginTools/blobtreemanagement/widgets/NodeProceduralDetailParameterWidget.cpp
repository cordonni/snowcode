#include <pluginTools/blobtreemanagement/widgets/NodeProceduralDetailParameterWidget.h>

#include <convol/functors/noises/PolynomialDeformationMap.h>

#include <convol/factories/BlobtreeNodeFactory.h>

#include <pluginTools/blobtreemanagement/widgets/BlobtreeNodeWidgetFactory.h>

#include <memory>

namespace expressive {

void NodeProceduralDetailParameterWidget::DelegateConstructor()
{
    setupUi(this);

    functor_choice_widget_ = new FunctorChoiceWidget(
                convol::BlobtreeNodeFactory::GetInstance().GetBtNodeFactory(StaticName()),
                this, nullptr
                );
    verticalLayout_noise_freq_parameter->insertWidget(0, functor_choice_widget_);

    parent_functor_choice_widget_ = new FunctorChoiceWidget(
                convol::BlobtreeNodeFactory::GetInstance().GetBtNodeFactory(StaticParentName()),
                this, nullptr
                );
    verticalLayout_noise_grid_parameter->insertWidget(0, parent_functor_choice_widget_);

    display_noise_widget_ = new DisplayGaborSurfaceNoiseWidget(parent_functor_choice_widget_, functor_choice_widget_, this);
    verticalLayout_noise_picture->insertWidget(0, display_noise_widget_);

    polynomial_deformation_map_widget_ = new PolynomialDeformationMapWidget(this);
    functor_param_vertical_layout->insertWidget(0, polynomial_deformation_map_widget_);
}


TiXmlElement * NodeProceduralDetailParameterWidget::GetXmlFunctor()
{
    return functor_choice_widget_->ToXml();
}

TiXmlElement * NodeProceduralDetailParameterWidget::GetXmlAttributs()
{
    TiXmlElement *element = new TiXmlElement( StaticName() );

    element->SetDoubleAttribute("tilt_angle",dbl_spin_box_tilt_angle->value());
    element->SetDoubleAttribute("rotate_angle",dbl_spin_box_rotate_angle->value());

//    TiXmlElement *element_functor_noise = new TiXmlElement( convol::AbstractNodeProceduralDetailParameter::XmlFunctorField() );
//    element->LinkEndChild(element_functor_noise);
//    TiXmlElement * element_child_noise = noise_functor_.ToXml();
//    element_functor_noise->LinkEndChild(element_child_noise);

    TiXmlElement *element_functor_deformation_map = new TiXmlElement( "TFunctorDeformationMap" );
    element->LinkEndChild(element_functor_deformation_map);
//    TiXmlElement * element_child_deform = deformation_map_functor_.ToXml();
    TiXmlElement * element_child_deform = polynomial_deformation_map_widget_->ToXml();
    element_functor_deformation_map->LinkEndChild(element_child_deform);

    return element;
}

void NodeProceduralDetailParameterWidget::UpdateWidgetFromNode(std::shared_ptr<convol::NodeOperator> node)
{
    auto casted_node = std::dynamic_pointer_cast< convol::AbstractNodeProceduralDetailParameter > (node);

   dbl_spin_box_tilt_angle->setValue(casted_node->tilt_angle());
    dbl_spin_box_rotate_angle->setValue(casted_node->rotate_angle());

    functor_choice_widget_->UpdateWidgetFromKernel(casted_node->noise_parameter());
    polynomial_deformation_map_widget_->UpdateWidgetFromFunctor(casted_node->deformation_map_functor());

    const auto* casted_parent_node = dynamic_cast<const convol::AbstractNodeProceduralDetail*> (node->parent());
    if(casted_parent_node)
        parent_functor_choice_widget_->UpdateWidgetFromKernel(casted_parent_node->noise_functor());
}

static BlobtreeNodeWidgetFactory::Type<NodeProceduralDetailParameterWidget> NodeProceduralDetailParameterWidgetType;

} // end of namespace expressive

