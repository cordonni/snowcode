#include <pluginTools/blobtreemanagement/widgets/NodeOperatorNArityWidget.h>

#include <convol/factories/BlobtreeNodeFactory.h>

#include <pluginTools/blobtreemanagement/widgets/BlobtreeNodeWidgetFactory.h>

namespace expressive {

void NodeOperatorNArityWidget::DelegateConstructor()
{
    setupUi(this);

    int index = 0;

    functor_choice_widget_ = new FunctorChoiceWidget(
                convol::BlobtreeNodeFactory::GetInstance().GetBtNodeFactory(StaticName()),
                this, nullptr
                );

    main_vertical_layout->insertWidget(index, functor_choice_widget_);

//    connect(combo_box_functor,SIGNAL(currentIndexChanged(int)),this, SLOT(FunctorChoice()));
}

TiXmlElement * NodeOperatorNArityWidget::GetXmlFunctor()
{
    return functor_choice_widget_->ToXml();
}

TiXmlElement * NodeOperatorNArityWidget::GetXmlAttributs()
{
    TiXmlElement *element = new TiXmlElement( StaticName() );
    return element;
}

void NodeOperatorNArityWidget::UpdateWidgetFromNode(std::shared_ptr<convol::NodeOperator> node)
{
    auto casted_node = std::dynamic_pointer_cast< convol::AbstractNodeOperatorNArity > (node);

    functor_choice_widget_->UpdateWidgetFromKernel(casted_node->blend_operator());
}

static BlobtreeNodeWidgetFactory::Type<NodeOperatorNArityWidget> NodeOperatorNArityWidgetType;

} // end namespace expressive

