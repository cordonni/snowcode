/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: BlobtreeNodeWidgetFactory.h

   Language: C++

   License: Convol license

   \author: Cedric Zanni
   E-Mail: cedric.zanni@inria.fr

   Description: Header file for a BlobtreeNodeWidget factory (created object should
                derive from AbstractNodeOperatorWidget).
                This factory can create the required object from a name
                (the one returned by StaticName of a given BlobtreeNode).

   Platform Dependencies: None
*/

#pragma once
#ifndef EXPRESSIVE_BLOBTREE_NODE_WIDGET_FACTORY_T_H_
#define EXPRESSIVE_BLOBTREE_NODE_WIDGET_FACTORY_T_H_

// core dependencies
#include <core/CoreRequired.h>
    // utils
    #include <ork/core/Logger.h>

#include <pluginTools/PluginToolsRequired.h>
#include <pluginTools/blobtreemanagement/widgets/AbstractNodeOperatorWidget.h>

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QComboBox>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

namespace expressive {

/*! \brief ...
 */
class PLUGINTOOLS_API BlobtreeNodeWidgetFactory
{
public:
    typedef AbstractNodeOperatorWidget* (*BuilderFunction) ();
    typedef typename std::map<std::string, BuilderFunction> MapperType;
    
    static BlobtreeNodeWidgetFactory& GetInstance();

    AbstractNodeOperatorWidget* CreateNewBlobtreeNodeWidget(const std::string &node_name);

    void InitializeComboBox(QComboBox* combo_box);

    template<typename TBlobtreeNodeWidget>
    void RegisterBlobtreeNodeWidget(BuilderFunction f);

    template <class TBlobtreeNodeWidget>
    class Type
    {
    public:
        static AbstractNodeOperatorWidget* ctor ()
        {
            return new TBlobtreeNodeWidget();
        }
        
        Type()
        {
            BlobtreeNodeWidgetFactory::GetInstance().RegisterBlobtreeNodeWidget<TBlobtreeNodeWidget>(ctor);
        }
    };

private:
    MapperType types;
};
    


template <typename TBlobtreeNodeWidget>
void BlobtreeNodeWidgetFactory::RegisterBlobtreeNodeWidget(BuilderFunction f)
{
    types[TBlobtreeNodeWidget::StaticName()] = f;
}

} // Close namespace expressive

#endif // EXPRESSIVE_BLOBTREE_NODE_WIDGET_FACTORY_T_H_
