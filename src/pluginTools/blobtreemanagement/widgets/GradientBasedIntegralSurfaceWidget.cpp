#include <pluginTools/blobtreemanagement/widgets/GradientBasedIntegralSurfaceWidget.h>

#include <convol/factories/BlobtreeNodeFactory.h>

#include <pluginTools/blobtreemanagement/widgets/BlobtreeNodeWidgetFactory.h>

namespace expressive {

void GradientBasedIntegralSurfaceWidget::DelegateConstructor()
{
    setupUi(this);

    int index = 1;

    functor_choice_widget_ = new FunctorChoiceWidget(
                convol::BlobtreeNodeFactory::GetInstance().GetBtNodeFactory(StaticName()),
                this, nullptr
                );

    vertical_layout_functor->insertWidget(index, functor_choice_widget_);

//    connect(dbl_spin_box_clipping_length, SIGNAL(valueChanged(double)), this, SLOT(UpdateClippedLength()));
}

TiXmlElement * GradientBasedIntegralSurfaceWidget::GetXmlFunctor()
{
    return functor_choice_widget_->ToXml();
}

TiXmlElement * GradientBasedIntegralSurfaceWidget::GetXmlAttributs()
{
    TiXmlElement *element = new TiXmlElement( StaticName() );

    element->SetDoubleAttribute("special_parameter", dbl_spin_box_special_parameter->value());

    if(checkBox_analytical_projection->isChecked())
        element->SetAttribute("analytical_projection", 1);
    else
        element->SetAttribute("analytical_projection", 0);

    return element;
}

void GradientBasedIntegralSurfaceWidget::UpdateWidgetFromNode(std::shared_ptr<convol::NodeOperator> node)
{
    auto casted_node = std::dynamic_pointer_cast< convol::AbstractGradientBasedIntegralSurface > (node);

    dbl_spin_box_special_parameter->setValue(casted_node->special_parameter());
    checkBox_analytical_projection->setChecked(casted_node->analytical_projection());

    auto casted_node_2 = std::dynamic_pointer_cast< convol::AbstractIntegralSurfaceOperator > (node);

    functor_choice_widget_->UpdateWidgetFromKernel(casted_node_2->kernel_functor());
}

static BlobtreeNodeWidgetFactory::Type<GradientBasedIntegralSurfaceWidget> GradientBasedIntegralSurfaceWidgetType;

} // end namespace expressive

