#include <pluginTools/blobtreemanagement/widgets/FunctorChoiceWidget.h>

#include <core/functor/FunctorFactory.h>

namespace expressive {

TiXmlElement* FunctorChoiceWidget::ToXml() const
{
    if(active_functor_widget_) {
        return active_functor_widget_->ToXml();
    } else {
        return nullptr;
    }
}

std::shared_ptr<core::Functor> FunctorChoiceWidget::BuildFunctor() const
{
    if(active_functor_widget_) {
        auto element_functor = active_functor_widget_->ToXml();
        return core::FunctorFactory::GetInstance().CreateFunctorFromXml(element_functor);
    } else {
        return nullptr;
    }
}

void FunctorChoiceWidget::UpdateWidgetFromKernel(const core::Functor& functor)
{
    RemoveActiveWidget();

    int index = comboBox_functor->findData(QVariant(functor.Name().c_str()));

    if(index == -1) index = comboBox_functor->count()-1;

    comboBox_functor->setCurrentIndex(index);

    if(index != comboBox_functor->count()-1) {
        active_functor_widget_ = FunctorWidgetFactory::GetInstance().CreateNewFunctorWidget(functor.Name());
        active_functor_widget_->UpdateWidgetFromNode(&functor);
        active_functor_widget_->setParent(this);
        layout_functor->insertWidget(1, active_functor_widget_); //TODO:@todo : 1 or 0 ???
        active_functor_widget_->show();
    }
}

void FunctorChoiceWidget::UpdateWidgetFromKernel(std::shared_ptr<core::Functor> functor)
{
    if(functor) {
        UpdateWidgetFromKernel(*functor);
    } else {
        RemoveActiveWidget();
        comboBox_functor->setCurrentIndex(comboBox_functor->count()-1);
    }
}

void FunctorChoiceWidget::UpdateWidgetFromComboBox()
{
    RemoveActiveWidget();

//    if(comboBox_functor->currentData().toString().size() != 0) {
    QVariant currentData = comboBox_functor->itemData(comboBox_functor->currentIndex());
    if (currentData.toString().size() != 0) {
        active_functor_widget_ = FunctorWidgetFactory::GetInstance().CreateNewFunctorWidget(currentData.toString().toStdString());
        active_functor_widget_->setParent(this);
        layout_functor->insertWidget(1, active_functor_widget_); //TODO:@todo : 1 or 0 ???
        active_functor_widget_->show();
    }
}


} // end of namespace expressive


/*
FunctorChoiceWidget::FunctorChoiceWidget(std::vector< FunctorTypeTree >& kernel_enabled, QWidget* parent)
    : QWidget(parent),
      current_kernel_(NULL),
      current_kernel_widget_(NULL),
      current_family_combo_box_(NULL)
{
    setupUi(this);

    // Add all the different Kernel widgets
    int index = 0;
    QStandardItem* item_tmp = NULL;
    ////////////////////////
    // Cauchy Family      //
    ////////////////////////
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_kernel_family->model())->item(comboBox_kernel_family->findText(QString("Cauchy Family")));
    comboBox_kernel_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("Cauchy Family"), item_tmp));
    //
    cauchy_widget_ker_ = new CauchyWidget(this);
    layout_kernel->insertWidget(index, cauchy_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("Cauchy")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("Cauchy"), item_tmp));
    ++index;
    //
    standard_cauchy3_widget_ker_ = new StandardCauchy3Widget(this);
    layout_kernel->insertWidget(index, standard_cauchy3_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("StandardCauchy3")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("StandardCauchy3"), item_tmp));
    ++index;
    standard_cauchy4_widget_ker_ = new StandardCauchy4Widget(this);
    layout_kernel->insertWidget(index, standard_cauchy4_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("StandardCauchy4")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("StandardCauchy4"), item_tmp));
    ++index;
    standard_cauchy5_widget_ker_ = new StandardCauchy5Widget(this);
    layout_kernel->insertWidget(index, standard_cauchy5_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("StandardCauchy5")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("StandardCauchy5"), item_tmp));
    ++index;
    standard_cauchy6_widget_ker_ = new StandardCauchy6Widget(this);
    layout_kernel->insertWidget(index, standard_cauchy6_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("StandardCauchy6")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("StandardCauchy6"), item_tmp));
    ++index;
    hornus_cauchy3_widget_ker_ = new HornusCauchy3Widget(this);
    layout_kernel->insertWidget(index, hornus_cauchy3_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("HornusCauchy3")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HornusCauchy3"), item_tmp));
    ++index;
    hornus_cauchy4_widget_ker_ = new HornusCauchy4Widget(this);
    layout_kernel->insertWidget(index, hornus_cauchy4_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("HornusCauchy4")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HornusCauchy4"), item_tmp));
    ++index;
    hornus_cauchy5_widget_ker_ = new HornusCauchy5Widget(this);
    layout_kernel->insertWidget(index, hornus_cauchy5_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("HornusCauchy5")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HornusCauchy5"), item_tmp));
    ++index;
    hornus_cauchy6_widget_ker_ = new HornusCauchy6Widget(this);
    layout_kernel->insertWidget(index, hornus_cauchy6_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("HornusCauchy6")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HornusCauchy6"), item_tmp));
    ++index;
    homothetic_cauchy3_widget_ker_ = new HomotheticCauchy3Widget(this);
    layout_kernel->insertWidget(index, homothetic_cauchy3_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("HomotheticCauchy3")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticCauchy3"), item_tmp));
    ++index;
    homothetic_cauchy4_widget_ker_ = new HomotheticCauchy4Widget(this);
    layout_kernel->insertWidget(index, homothetic_cauchy4_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("HomotheticCauchy4")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticCauchy4"), item_tmp));
    ++index;
    homothetic_cauchy5_widget_ker_ = new HomotheticCauchy5Widget(this);
    layout_kernel->insertWidget(index, homothetic_cauchy5_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("HomotheticCauchy5")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticCauchy5"), item_tmp));
    ++index;
    homothetic_cauchy6_widget_ker_ = new HomotheticCauchy6Widget(this);
    layout_kernel->insertWidget(index, homothetic_cauchy6_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_cauchy_family->model())->item(comboBox_cauchy_family->findText(QString("HomotheticCauchy6")));
    comboBox_cauchy_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticCauchy6"), item_tmp));
//    ++index;
    ////////////////////////
    // Inverse Family     //
    ////////////////////////
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_kernel_family->model())->item(comboBox_kernel_family->findText(QString("Inverse Family")));
    comboBox_kernel_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("Inverse Family"), item_tmp));
    //
    inverse_widget_ker_ = new InverseWidget(this);
    layout_kernel->insertWidget(index, inverse_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("Inverse")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("Inverse"), item_tmp));
    ++index;
    //
    standard_inverse3_widget_ker_ = new StandardInverse3Widget(this);
    layout_kernel->insertWidget(index, standard_inverse3_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("StandardInverse3")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("StandardInverse3"), item_tmp));
    ++index;
    standard_inverse4_widget_ker_ = new StandardInverse4Widget(this);
    layout_kernel->insertWidget(index, standard_inverse4_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("StandardInverse4")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("StandardInverse4"), item_tmp));
    ++index;
    standard_inverse5_widget_ker_ = new StandardInverse5Widget(this);
    layout_kernel->insertWidget(index, standard_inverse5_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("StandardInverse5")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("StandardInverse5"), item_tmp));
    ++index;
    standard_inverse6_widget_ker_ = new StandardInverse6Widget(this);
    layout_kernel->insertWidget(index, standard_inverse6_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("StandardInverse6")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("StandardInverse6"), item_tmp));
    ++index;
    //
    hornus_inverse3_widget_ker_ = new HornusInverse3Widget(this);
    layout_kernel->insertWidget(index, hornus_inverse3_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("HornusInverse3")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HornusInverse3"), item_tmp));
    ++index;
    hornus_inverse4_widget_ker_ = new HornusInverse4Widget(this);
    layout_kernel->insertWidget(index, hornus_inverse4_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("HornusInverse4")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HornusInverse4"), item_tmp));
    ++index;
    hornus_inverse5_widget_ker_ = new HornusInverse5Widget(this);
    layout_kernel->insertWidget(index, hornus_inverse5_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("HornusInverse5")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HornusInverse5"), item_tmp));
    ++index;
    hornus_inverse6_widget_ker_ = new HornusInverse6Widget(this);
    layout_kernel->insertWidget(index, hornus_inverse6_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("HornusInverse6")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HornusInverse6"), item_tmp));
    ++index;
    //
    homothetic_inverse3_widget_ker_ = new HomotheticInverse3Widget(this);
    layout_kernel->insertWidget(index, homothetic_inverse3_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("HomotheticInverse3")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticInverse3"), item_tmp));
    ++index;
    homothetic_inverse4_widget_ker_ = new HomotheticInverse4Widget(this);
    layout_kernel->insertWidget(index, homothetic_inverse4_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("HomotheticInverse4")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticInverse4"), item_tmp));
    ++index;
    homothetic_inverse5_widget_ker_ = new HomotheticInverse5Widget(this);
    layout_kernel->insertWidget(index, homothetic_inverse5_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("HomotheticInverse5")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticInverse5"), item_tmp));
    ++index;
    homothetic_inverse6_widget_ker_ = new HomotheticInverse6Widget(this);
    layout_kernel->insertWidget(index, homothetic_inverse6_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_inverse_family->model())->item(comboBox_inverse_family->findText(QString("HomotheticInverse6")));
    comboBox_inverse_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticInverse6"), item_tmp));
    ++index;
    ////////////////////////////////////
    // Compact Polynomial Family      //
    ////////////////////////////////////
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_kernel_family->model())->item(comboBox_kernel_family->findText(QString("Compact Polynomial Family")));
    comboBox_kernel_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("Compact Polynomial Family"), item_tmp));
    //
    standard_compact_polynomial4_widget_ker_ = new StandardCompactPolynomial4Widget(this);
    layout_kernel->insertWidget(index, standard_compact_polynomial4_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_compact_poly_family->model())->item(comboBox_compact_poly_family->findText(QString("StandardCompactPolynomial4")));
    comboBox_compact_poly_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("StandardCompactPolynomial4"), item_tmp));
    ++index;
    standard_compact_polynomial6_widget_ker_ = new StandardCompactPolynomial6Widget(this);
    layout_kernel->insertWidget(index, standard_compact_polynomial6_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_compact_poly_family->model())->item(comboBox_compact_poly_family->findText(QString("StandardCompactPolynomial6")));
    comboBox_compact_poly_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("StandardCompactPolynomial6"), item_tmp));
    ++index;
    standard_compact_polynomial8_widget_ker_ = new StandardCompactPolynomial8Widget(this);
    layout_kernel->insertWidget(index, standard_compact_polynomial8_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_compact_poly_family->model())->item(comboBox_compact_poly_family->findText(QString("StandardCompactPolynomial8")));
    comboBox_compact_poly_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("StandardCompactPolynomial8"), item_tmp));
    ++index;
    //
    hornus_compact_polynomial4_widget_ker_ = new HornusCompactPolynomial4Widget(this);
    layout_kernel->insertWidget(index, hornus_compact_polynomial4_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_compact_poly_family->model())->item(comboBox_compact_poly_family->findText(QString("HornusCompactPolynomial4")));
    comboBox_compact_poly_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HornusCompactPolynomial4"), item_tmp));
    ++index;
    hornus_compact_polynomial6_widget_ker_ = new HornusCompactPolynomial6Widget(this);
    layout_kernel->insertWidget(index, hornus_compact_polynomial6_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_compact_poly_family->model())->item(comboBox_compact_poly_family->findText(QString("HornusCompactPolynomial6")));
    comboBox_compact_poly_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HornusCompactPolynomial6"), item_tmp));
    ++index;
    hornus_compact_polynomial8_widget_ker_ = new HornusCompactPolynomial8Widget(this);
    layout_kernel->insertWidget(index, hornus_compact_polynomial8_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_compact_poly_family->model())->item(comboBox_compact_poly_family->findText(QString("HornusCompactPolynomial8")));
    comboBox_compact_poly_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HornusCompactPolynomial8"), item_tmp));
    ++index;
    //
    homothetic_compact_polynomial4_widget_ker_ = new HomotheticCompactPolynomial4Widget(this);
    layout_kernel->insertWidget(index, homothetic_compact_polynomial4_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_compact_poly_family->model())->item(comboBox_compact_poly_family->findText(QString("HomotheticCompactPolynomial4")));
    comboBox_compact_poly_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticCompactPolynomial4"), item_tmp));
    ++index;
    homothetic_compact_polynomial6_widget_ker_ = new HomotheticCompactPolynomial6Widget(this);
    layout_kernel->insertWidget(index, homothetic_compact_polynomial6_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_compact_poly_family->model())->item(comboBox_compact_poly_family->findText(QString("HomotheticCompactPolynomial6")));
    comboBox_compact_poly_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticCompactPolynomial6"), item_tmp));
    ++index;
    homothetic_compact_polynomial8_widget_ker_ = new HomotheticCompactPolynomial8Widget(this);
    layout_kernel->insertWidget(index, homothetic_compact_polynomial8_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_compact_poly_family->model())->item(comboBox_compact_poly_family->findText(QString("HomotheticCompactPolynomial8")));
    comboBox_compact_poly_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticCompactPolynomial8"), item_tmp));
    ++index;
    ////////////////////////
    // Other Family       //
    ////////////////////////
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_kernel_family->model())->item(comboBox_kernel_family->findText(QString("Other Kernel Family")));
    comboBox_kernel_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("Other Kernel Family"), item_tmp));
    //
    gaussian_widget_ker_ = new GaussianWidget(this);
    layout_kernel->insertWidget(index, gaussian_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_other_family->model())->item(comboBox_other_family->findText(QString("Gaussian")));
    comboBox_other_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("Gaussian"), item_tmp));
    ++index;
    metaballs_widget_ker_ = new MetaballsWidget(this);
    layout_kernel->insertWidget(index, metaballs_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_other_family->model())->item(comboBox_other_family->findText(QString("Metaballs")));
    comboBox_other_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("Metaballs"), item_tmp));
    ++index;
    quartic_polynomial_widget_ker_ = new GaussianWidget(this);   //TODO : @todo
    layout_kernel->insertWidget(index, quartic_polynomial_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_other_family->model())->item(comboBox_other_family->findText(QString("QuarticPolynomial")));
    comboBox_other_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("QuarticPolynomial"), item_tmp));
    ++index;
    ////////////////////////
    // Distance Family    //
    ////////////////////////
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_kernel_family->model())->item(comboBox_kernel_family->findText(QString("Distance Family")));
    comboBox_kernel_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("Distance Family"), item_tmp));
    //
    profil_distance_widget_ker_ = new ProfilDistanceWidget(this);
    layout_kernel->insertWidget(index, profil_distance_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_distance_family->model())->item(comboBox_distance_family->findText(QString("ProfilDistance")));
    comboBox_distance_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("ProfilDistance"), item_tmp));
    ++index;
    homothetic_distance_widget_ker_ = new HomotheticDistanceWidget(this);
    layout_kernel->insertWidget(index, homothetic_distance_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_distance_family->model())->item(comboBox_distance_family->findText(QString("HomotheticDistance")));
    comboBox_distance_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("HomotheticDistance"), item_tmp));
    ++index;
    damien_widget_ker_ = new DamienWidget(this);
    layout_kernel->insertWidget(index, damien_widget_ker_);
    item_tmp = qobject_cast<QStandardItemModel *>(comboBox_distance_family->model())->item(comboBox_distance_family->findText(QString("Damien")));
    comboBox_distance_family_item_map_.insert(std::pair<std::string,QStandardItem*>(std::string("Damien"), item_tmp));

    KernelMapNode node;
    FunctorTypeTree type;
    ////////////////////////
    // Cauchy Family      //
    ////////////////////////
    type.type = std::string("Cauchy");
    node.kernel_widget = cauchy_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("StandardCauchy3");
    node.kernel_widget = standard_cauchy3_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("StandardCauchy4");
    node.kernel_widget = standard_cauchy4_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("StandardCauchy5");
    node.kernel_widget = standard_cauchy5_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("StandardCauchy6");
    node.kernel_widget = standard_cauchy6_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HornusCauchy3");
    node.kernel_widget = hornus_cauchy3_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HornusCauchy4");
    node.kernel_widget = hornus_cauchy4_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HornusCauchy5");
    node.kernel_widget = hornus_cauchy5_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HornusCauchy6");
    node.kernel_widget = hornus_cauchy6_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HomotheticCauchy3");
    node.kernel_widget = homothetic_cauchy3_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HomotheticCauchy4");
    node.kernel_widget = homothetic_cauchy4_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HomotheticCauchy5");
    node.kernel_widget = homothetic_cauchy5_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HomotheticCauchy6");
    node.kernel_widget = homothetic_cauchy6_widget_ker_;
    kernel_map_.insert(type, node);

    ////////////////////////
    // Inverse Family     //
    ////////////////////////
    type.type = std::string("Inverse");
    node.kernel_widget = inverse_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("StandardInverse3");
    node.kernel_widget = standard_inverse3_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("StandardInverse4");
    node.kernel_widget = standard_inverse4_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("StandardInverse5");
    node.kernel_widget = standard_inverse5_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("StandardInverse6");
    node.kernel_widget = standard_inverse6_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HornusInverse3");
    node.kernel_widget = hornus_inverse3_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HornusInverse4");
    node.kernel_widget = hornus_inverse4_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HornusInverse5");
    node.kernel_widget = hornus_inverse5_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HornusInverse6");
    node.kernel_widget = hornus_inverse6_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HomotheticInverse3");
    node.kernel_widget = homothetic_inverse3_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HomotheticInverse4");
    node.kernel_widget = homothetic_inverse4_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HomotheticInverse5");
    node.kernel_widget = homothetic_inverse5_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HomotheticInverse6");
    node.kernel_widget = homothetic_inverse6_widget_ker_;
    kernel_map_.insert(type, node);
    ////////////////////////////////////
    // Compact Polynomial Family      //
    ////////////////////////////////////
        type.type = std::string("StandardCompactPolynomial4");
        node.kernel_widget = standard_compact_polynomial4_widget_ker_;
        kernel_map_.insert(type, node);
        type.type = std::string("StandardCompactPolynomial6");
        node.kernel_widget = standard_compact_polynomial6_widget_ker_;
        kernel_map_.insert(type, node);
        type.type = std::string("StandardCompactPolynomial8");
        node.kernel_widget = standard_compact_polynomial8_widget_ker_;
        kernel_map_.insert(type, node);
        type.type = std::string("HornusCompactPolynomial4");
        node.kernel_widget = hornus_compact_polynomial4_widget_ker_;
        kernel_map_.insert(type, node);
        type.type = std::string("HornusCompactPolynomial6");
        node.kernel_widget = hornus_compact_polynomial6_widget_ker_;
        kernel_map_.insert(type, node);
        type.type = std::string("HornusCompactPolynomial8");
        node.kernel_widget = hornus_compact_polynomial8_widget_ker_;
        kernel_map_.insert(type, node);
        type.type = std::string("HomotheticCompactPolynomial4");
        node.kernel_widget = homothetic_compact_polynomial4_widget_ker_;
        kernel_map_.insert(type, node);
        type.type = std::string("HomotheticCompactPolynomial6");
        node.kernel_widget = homothetic_compact_polynomial6_widget_ker_;
        kernel_map_.insert(type, node);
        type.type = std::string("HomotheticCompactPolynomial8");
        node.kernel_widget = homothetic_compact_polynomial8_widget_ker_;
        kernel_map_.insert(type, node);
    ////////////////////////
    // Other Family       //
    ////////////////////////
    type.type = std::string("Gaussian");
    node.kernel_widget = gaussian_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("Metaballs");
    node.kernel_widget = metaballs_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("QuarticPolynomial");
    node.kernel_widget = quartic_polynomial_widget_ker_;
    kernel_map_.insert(type, node);
    ////////////////////////
    // Distance Family    //
    ////////////////////////
    type.type = std::string("ProfilDistance");
    node.kernel_widget = profil_distance_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("HomotheticDistance");
    node.kernel_widget = homothetic_distance_widget_ker_;
    kernel_map_.insert(type, node);
    type.type = std::string("Damien");
    node.kernel_widget = damien_widget_ker_;
    kernel_map_.insert(type, node);


    // Enable only widgets in the given list
    UpdateKernelChoice(kernel_enabled);

    comboBox_cauchy_family->hide();
    comboBox_inverse_family->hide();
    comboBox_compact_poly_family->hide();
    comboBox_distance_family->hide();
    comboBox_other_family->hide();

    cauchy_widget_ker_->hide();
        //
        standard_cauchy3_widget_ker_->hide();
        standard_cauchy4_widget_ker_->hide();
        standard_cauchy5_widget_ker_->hide();
        standard_cauchy6_widget_ker_->hide();
        hornus_cauchy3_widget_ker_->hide();
        hornus_cauchy4_widget_ker_->hide();
        hornus_cauchy5_widget_ker_->hide();
        hornus_cauchy6_widget_ker_->hide();
        homothetic_cauchy3_widget_ker_->hide();
        homothetic_cauchy4_widget_ker_->hide();
        homothetic_cauchy5_widget_ker_->hide();
        homothetic_cauchy6_widget_ker_->hide();
        //
        standard_inverse3_widget_ker_->hide();
        standard_inverse4_widget_ker_->hide();
        standard_inverse5_widget_ker_->hide();
        standard_inverse6_widget_ker_->hide();
        hornus_inverse3_widget_ker_->hide();
        hornus_inverse4_widget_ker_->hide();
        hornus_inverse5_widget_ker_->hide();
        hornus_inverse6_widget_ker_->hide();
        homothetic_inverse3_widget_ker_->hide();
        homothetic_inverse4_widget_ker_->hide();
        homothetic_inverse5_widget_ker_->hide();
        homothetic_inverse6_widget_ker_->hide();
        //
        standard_compact_polynomial4_widget_ker_->hide();
        standard_compact_polynomial6_widget_ker_->hide();
        standard_compact_polynomial8_widget_ker_->hide();
        hornus_compact_polynomial4_widget_ker_->hide();
        hornus_compact_polynomial6_widget_ker_->hide();
        hornus_compact_polynomial8_widget_ker_->hide();
        homothetic_compact_polynomial4_widget_ker_->hide();
        homothetic_compact_polynomial6_widget_ker_->hide();
        homothetic_compact_polynomial8_widget_ker_->hide();

    inverse_widget_ker_->hide();
    gaussian_widget_ker_->hide();
    metaballs_widget_ker_->hide();
    quartic_polynomial_widget_ker_->hide();
    profil_distance_widget_ker_->hide();
    homothetic_distance_widget_ker_->hide();
    damien_widget_ker_->hide();

    connect(comboBox_kernel_family, SIGNAL(currentIndexChanged(int)), this, SLOT(KernelFamilyChoice())); // probablement pas ce qu'il faut faire : ajouter une autre fonction pour choisir le noyau courant

    KernelFamilyChoice(); // will call KernelChoice ...
}

void FunctorChoiceWidget::UpdateKernelChoice(std::vector<FunctorTypeTree>& kernel_enabled)
{
    // disable all items in all combo box
    for(std::map<std::string,QStandardItem*>::iterator it = comboBox_kernel_family_item_map_.begin(); it != comboBox_kernel_family_item_map_.end(); ++it)
    {
        it->second->setEnabled(false);
    }
    for(std::map<std::string,QStandardItem*>::iterator it = comboBox_cauchy_family_item_map_.begin(); it != comboBox_cauchy_family_item_map_.end(); ++it)
    {
        it->second->setEnabled(false);
    }
    for(std::map<std::string,QStandardItem*>::iterator it = comboBox_inverse_family_item_map_.begin(); it != comboBox_inverse_family_item_map_.end(); ++it)
    {
        it->second->setEnabled(false);
    }
    for(std::map<std::string,QStandardItem*>::iterator it = comboBox_compact_poly_family_item_map_.begin(); it != comboBox_compact_poly_family_item_map_.end(); ++it)
    {
        it->second->setEnabled(false);
    }
    for(std::map<std::string,QStandardItem*>::iterator it = comboBox_distance_family_item_map_.begin(); it != comboBox_distance_family_item_map_.end(); ++it)
    {
        it->second->setEnabled(false);
    }
    for(std::map<std::string,QStandardItem*>::iterator it = comboBox_other_family_item_map_.begin(); it != comboBox_other_family_item_map_.end(); ++it)
    {
        it->second->setEnabled(false);
    }

    // Enable only NumericalWidgets for all kernels concerned
    cauchy_widget_ker_->EnableNumericalOnly();
//    cauchy3_widget_ker_->EnableNumericalOnly();
//    cauchy4_widget_ker_->EnableNumericalOnly();
//    cauchy5_widget_ker_->EnableNumericalOnly();
//    cauchy6_widget_ker_->EnableNumericalOnly();
    inverse_widget_ker_->EnableNumericalOnly();
//    inverse3_widget_ker_->EnableNumericalOnly();
//    inverse4_widget_ker_->EnableNumericalOnly();
//    inverse5_widget_ker_->EnableNumericalOnly();
//    inverse6_widget_ker_->EnableNumericalOnly();
//    compact_polynomial4_widget_ker_->EnableNumericalOnly();
//    compact_polynomial6_widget_ker_->EnableNumericalOnly();
//    compact_polynomial8_widget_ker_->EnableNumericalOnly();
    gaussian_widget_ker_->EnableNumericalOnly();
    metaballs_widget_ker_->EnableNumericalOnly();
    quartic_polynomial_widget_ker_->EnableNumericalOnly();


    // Enable index in the combo box that are in kernel_enabled, also call EnableAnalytical on all widgets 
    // corresponding to analytical types given.
    bool enable_cauchy_family = false, enable_inverse_family = false, enable_compact_poly_family = false,
         enable_distance_family = false, enable_other_family = false;
    for(std::vector<FunctorTypeTree>::iterator it = kernel_enabled.begin(); it != kernel_enabled.end(); ++it)
    {
        bool is_numerical = it->type ==std::string("NumericalConvol");
        const std::string& atom_type = is_numerical ? it->subtypes[0].type : it->type;

        // seach in each kernel family map
        std::map<std::string,QStandardItem*>::iterator it_item = comboBox_cauchy_family_item_map_.find(atom_type);
        if(it_item != comboBox_cauchy_family_item_map_.end())
        {   // check if kernel is in Cauchy family
            if(!enable_cauchy_family)
                comboBox_cauchy_family->setCurrentIndex(comboBox_cauchy_family->findText(QString(atom_type.c_str())));
            enable_cauchy_family = true;
            it_item->second->setEnabled(true);
        }
        it_item = comboBox_inverse_family_item_map_.find(atom_type);
        if(it_item != comboBox_inverse_family_item_map_.end())
        {   // check if kernel is in Inverse family
            if(!enable_inverse_family)
                comboBox_inverse_family->setCurrentIndex(comboBox_inverse_family->findText(QString(atom_type.c_str())));
            enable_inverse_family = true;
            it_item->second->setEnabled(true);
        }
        it_item = comboBox_compact_poly_family_item_map_.find(atom_type);
        if(it_item != comboBox_compact_poly_family_item_map_.end())
        {   // check if kernel is in Compact Polynomial family
            if(!enable_compact_poly_family)
                comboBox_compact_poly_family->setCurrentIndex(comboBox_compact_poly_family->findText(QString(atom_type.c_str())));
            enable_compact_poly_family = true;
            it_item->second->setEnabled(true);
        }
        it_item = comboBox_distance_family_item_map_.find(atom_type);
        if(it_item != comboBox_distance_family_item_map_.end())
        { // check if kernel is in Distance family
            if(!enable_distance_family)
                comboBox_distance_family->setCurrentIndex(comboBox_distance_family->findText(QString(atom_type.c_str())));
            enable_distance_family = true;
            it_item->second->setEnabled(true);
        }
        it_item = comboBox_other_family_item_map_.find(atom_type);
        if(it_item != comboBox_other_family_item_map_.end())
        {   // check if kernel is in "Other" family
            if(!enable_other_family)
                comboBox_other_family->setCurrentIndex(comboBox_other_family->findText(QString(atom_type.c_str())));
            enable_other_family = true;
            it_item->second->setEnabled(true);
        }

        if(!is_numerical){
            KernelMapNode node;
            bool found = kernel_map_.find(*it, node);
            assert(found); // if triggered, the kernel given is not in the map. Note that numerical leads to the same node as non-numerical.
                           // Your problem may come from that...
            node.kernel_widget->EnableAnalytical();
        }
    }

    // Update which family should be accessible
    std::map<std::string,QStandardItem*>::iterator it;
    bool b = true;
    it = comboBox_kernel_family_item_map_.find(std::string("Cauchy Family"));
    if (enable_cauchy_family)
    {
        it->second->setEnabled(true);
        comboBox_kernel_family->setCurrentIndex(0); b = false;
    }
    it = comboBox_kernel_family_item_map_.find(std::string("Inverse Family"));
    if (enable_inverse_family)
    {
        it->second->setEnabled(true);
        if(b)
        {
            comboBox_kernel_family->setCurrentIndex(1); b = false;
        }
    }
    it = comboBox_kernel_family_item_map_.find(std::string("Compact Polynomial Family"));
    if (enable_compact_poly_family)
    {
        it->second->setEnabled(true);
        if(b)
        {
            comboBox_kernel_family->setCurrentIndex(2); b = false;
        }
    }
    it = comboBox_kernel_family_item_map_.find(std::string("Distance Family"));
    if (enable_distance_family)
    {
        it->second->setEnabled(true);
        if(b)
        {
            comboBox_kernel_family->setCurrentIndex(3); b = false;
        }
    }
    it = comboBox_kernel_family_item_map_.find(std::string("Other Kernel Family"));
    if (enable_other_family)
    {
        it->second->setEnabled(true);
        if(b)
        {
            comboBox_kernel_family->setCurrentIndex(4); b = false;
        }
    }
}

const FunctorTypeTree& FunctorChoiceWidget::KernelType()
{
    KernelMapNode node;
    FunctorTypeTree type;
    type.type = current_family_combo_box_->currentText().toStdString();
    bool found = kernel_map_.find(type, node);
    assert(found); // The combo box text should lead to some existing node in the map...
    return node.kernel_widget->KernelType();
}

FunctorChoiceWidget::Functor const* FunctorChoiceWidget::CreateKernel()
{
    if(current_kernel_ != NULL)
    {
        delete current_kernel_;
        current_kernel_ = NULL;
    }

    current_kernel_ = current_kernel_widget_->CreateFunctorKernel();

    return current_kernel_;
}


// Affiche une combo box correspondant a une famille de noyau
void FunctorChoiceWidget::KernelFamilyChoice()
{
    if(current_family_combo_box_ != NULL)
    {
        current_family_combo_box_->hide();
        disconnect(current_family_combo_box_, SIGNAL(currentIndexChanged(int)), this, SLOT(KernelChoice()));
    }

    switch(comboBox_kernel_family->currentIndex())
    {
        case 0 : // Cauchy family
            current_family_combo_box_ = comboBox_cauchy_family;
            break;
        case 1 : // Inverse family
            current_family_combo_box_ = comboBox_inverse_family;
            break;
        case 2 : // Compact Polynomial family
            current_family_combo_box_ = comboBox_compact_poly_family;
            break;
        case 3 : // Distance family
            current_family_combo_box_ = comboBox_distance_family;
            break;
        case 4 : // Other kernel family
            current_family_combo_box_ = comboBox_other_family;
            break;
        default:
            assert(false);
    }

    connect(current_family_combo_box_, SIGNAL(currentIndexChanged(int)), this, SLOT(KernelChoice()));
    current_family_combo_box_->show();

    KernelChoice();
}

// Affiche un widget kernel en fonction du choix fait dans la combo box
void FunctorChoiceWidget::KernelChoice()
{
    if(current_kernel_widget_ != NULL)
    {
        current_kernel_widget_->hide();
        disconnect(current_kernel_widget_, SIGNAL(parametersChanged()), this, SLOT(EmitKernelParametersChanged()));
    }
    
    // now show only the one corresponding to the combo box
    KernelMapNode node;
    FunctorTypeTree type;
    QString debug_str = current_family_combo_box_->itemText(current_family_combo_box_->currentIndex());
    type.type = debug_str.toStdString();
    bool found = kernel_map_.find(type, node);
    assert(found); // if triggered the kernel you want has not been found in the map... 
    node.kernel_widget->show();
    current_kernel_widget_ = node.kernel_widget;
    
    connect(current_kernel_widget_, SIGNAL(parametersChanged()), this, SLOT(EmitKernelParametersChanged()));

    CreateKernel();

    emit kernelChanged();
}

void FunctorChoiceWidget::EmitKernelParametersChanged()
{
    CreateKernel();
    emit kernelParametersChanged();
}

void FunctorChoiceWidget::SetComboBoxKernelType(const FunctorTypeTree& type)
{
    std::string type_we_care_about = type.type;
    if(type_we_care_about == std::string("NumericalConvol") || type_we_care_about == std::string("SemiNumericalHomotheticConvol"))
    {
        type_we_care_about = type.subtypes[0].type;
    }

    int idx = comboBox_cauchy_family->findText(QString(type_we_care_about.c_str()));
    if(idx >= 0)
    {
        comboBox_cauchy_family->setCurrentIndex(idx);
        comboBox_kernel_family->setCurrentIndex(0); // should "call" KernelFamilyChoice
    }
    else
    {
        idx = comboBox_inverse_family->findText(QString(type_we_care_about.c_str()));
        if(idx >= 0)
        {
            comboBox_inverse_family->setCurrentIndex(idx);
            comboBox_kernel_family->setCurrentIndex(1);  // should "call" KernelFamilyChoice
        }
        else
        {
            idx = comboBox_compact_poly_family->findText(QString(type_we_care_about.c_str()));
            if(idx >= 0)
            {
                comboBox_compact_poly_family->setCurrentIndex(idx);
                comboBox_kernel_family->setCurrentIndex(2);  // should "call" KernelFamilyChoice
            }
            else
            {
                idx = comboBox_distance_family->findText(QString(type_we_care_about.c_str()));
                if(idx >= 0)
                {
                    comboBox_distance_family->setCurrentIndex(idx);
                    comboBox_kernel_family->setCurrentIndex(3);  // should "call" KernelFamilyChoice
                }
                else
                {
                    idx = comboBox_other_family->findText(QString(type_we_care_about.c_str()));
                    if(idx >= 0)
                    {
                        comboBox_other_family->setCurrentIndex(idx);
                        comboBox_kernel_family->setCurrentIndex(4);  // should "call" KernelFamilyChoice
                    }
                }
            }
        }
    }
    assert(idx >= 0);

    KernelMapNode node;
    kernel_map_.find(FunctorTypeTree(type_we_care_about), node);
    node.kernel_widget->UpdateType(type);
}

void FunctorChoiceWidget::UpdateWidgetFromKernel(const Functor& kernel_functor)
{
    SetComboBoxKernelType(kernel_functor.Type());

    bool is_numerical = (kernel_functor.Type().type == std::string("NumericalConvol"));
    bool is_semi_numerical = (kernel_functor.Type().type == std::string("SemiNumericalHomotheticConvol"));

    KernelMapNode node;
    bool found;
    if(is_numerical || is_semi_numerical)
    {
        found = kernel_map_.find(kernel_functor.Type().subtypes[0], node);
    }
    else
    {
        found = kernel_map_.find(kernel_functor.Type(), node);
    }
    assert(found);
    node.kernel_widget->UpdateFromKernel(kernel_functor);
}
*/
