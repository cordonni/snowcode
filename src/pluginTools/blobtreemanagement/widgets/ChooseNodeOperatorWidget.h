/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: ChooseNodeOperatorWidget.h

   Language: C++

   License: Convol Licence

   \author: Maxime Quiblier
   E-Mail: maxime.quiblier@inrialpes.fr

   Project: Convol-ConvolLab

   Description: Header file for ChooseNodeOperatorWidget
                This is the widget that contains the UI about NodeOperator choice.

   Platform Dependencies: None
*/

#pragma once
#ifndef CHOOSE_NODE_OPERATOR_WIDGET_H
#define CHOOSE_NODE_OPERATOR_WIDGET_H

////////////////////////////////////////////////////////////////////
/// TODO:@todo : for now this widget only work for NodeOperator that have exactly one functor ...
/// TODO:@todo : should use qml
/// TODO:@todo : should find a way to initialize the ComboBox from a subset of kernel
////////////////////////////////////////////////////////////////////

#if defined(_MSC_VER)
#pragma warning(push, 0)
#pragma warning(disable: 4242)
#endif //_WIN32


#include <convol/blobtreenode/NodeOperatorT.h>

#include <pluginTools/blobtreemanagement/widgets/AbstractNodeOperatorWidget.h>


#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include <QWidget>
#include "ui_ChooseNodeOperatorWidget.h"
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#if defined(_MSC_VER)
#pragma warning(pop)
#endif //_WIN32

namespace expressive {

class ChooseNodeOperatorWidget : public QWidget, public Ui::ChooseNodeOperatorWidget
{
    Q_OBJECT

public:
    ChooseNodeOperatorWidget(QWidget* parent = 0, std::shared_ptr<convol::NodeOperator> node = nullptr)
        : QWidget(parent), active_node_operator_widget_(nullptr)
    {
        setupUi(this);

        InitializeComboBox();
        UpdateWidgetFromNode(node);
    }
    ~ChooseNodeOperatorWidget() { RemoveActiveWidget(); }

    std::shared_ptr<convol::BlobtreeNode> BuildNodeOperator() const;

    void InitializeComboBox();

    void UpdateWidgetFromNode(std::shared_ptr<convol::NodeOperator> node);

public slots:
    void UpdateWidgetFromComboBox();

private:
    void RemoveActiveWidget() {
        if(active_node_operator_widget_!=nullptr) {
            active_node_operator_widget_->hide();
            delete active_node_operator_widget_;
            active_node_operator_widget_ = nullptr;
        }
    }

    AbstractNodeOperatorWidget* active_node_operator_widget_;
};

///* //TODO:@todo : portage en cours
//    NodeOperator2Widget* node_operator_2_widget_;
//    NodeOperatorNArityWidget* node_operator_n_arity_widget_;
//    NodeOperator2BlendBBCWLocalWidget* node_blend_bbcw_local_widget_;
//    NodeOperatorNWitnessedBlendWidget* node_witnessed_blend_widget_;
//    GradientBasedIntegralSurfaceWidget* gradient_based_integral_surface_widget_;

} // end of namespace expressive

#endif // CHOOSE_NODE_OPERATOR_WIDGET_H
