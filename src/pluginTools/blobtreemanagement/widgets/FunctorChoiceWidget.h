/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: FunctorChoiceWidget.h

   Language: C++

   License: Convol Licence

   \author: Cédric Zanni, Maxime Quiblier, Galel Koraa
   E-Mail: maxime.quiblier@inrialpes.fr

   Project: Convol-ConvolLab

   Description: Header file for FunctorChoiceWidget.
                This is the widget that contains a simple ui to choose a kernel.

                TODO : a completer

   Platform Dependencies: None
*/

#pragma once
#ifndef KERNEL_CHOICE_WIDGET_H
#define KERNEL_CHOICE_WIDGET_H

#ifdef _MSC_VER
#pragma warning( push, 0 )
#endif
#include "ui_FunctorChoiceWidget.h"
#include <QWidget>
#ifdef _MSC_VER
#pragma warning( pop )
#endif


#include <core/functor/Functor.h>
#include <core/functor/FunctorFactory.h>

#include <pluginTools/convolwidgets/FunctorWidget.h>
#include <pluginTools/convolwidgets/FunctorWidgetFactory.h>


namespace expressive {

class FunctorChoiceWidget : public QWidget, public Ui::FunctorChoiceWidget
{
public:
    Q_OBJECT

public:

    /*!
     * \brief FunctorChoiceWidget
     * \param functor_enabled data structures containing the description of all the functor that should be constructible
     * \param parent
     */
    template <typename TFunctorTypeMap>
    FunctorChoiceWidget(const TFunctorTypeMap& functor_enabled, QWidget* parent = 0, std::shared_ptr<core::Functor> functor = nullptr);
    ~FunctorChoiceWidget() { RemoveActiveWidget(); }

    TiXmlElement* ToXml() const;

    std::shared_ptr<core::Functor> BuildFunctor() const;

    template <typename TFunctorTypeMap>
    void InitializeComboBox(const TFunctorTypeMap& functor_enabled);

    void UpdateWidgetFromKernel(const core::Functor& functor);
    void UpdateWidgetFromKernel(std::shared_ptr<core::Functor> functor);

public slots:
    void UpdateWidgetFromComboBox();

private:
    void RemoveActiveWidget() {
        if(active_functor_widget_!=nullptr) {
            active_functor_widget_->hide();
            delete active_functor_widget_;
            active_functor_widget_ = nullptr;
        }
    }

    AbstractFunctorWidget* active_functor_widget_;
};


template <typename TFunctorTypeMap>
FunctorChoiceWidget::FunctorChoiceWidget(const TFunctorTypeMap& functor_enabled, QWidget* parent, std::shared_ptr<core::Functor> functor)
    : QWidget(parent), active_functor_widget_(nullptr)
{
    setupUi(this);

    InitializeComboBox(functor_enabled);
    UpdateWidgetFromKernel(functor);
}

template <typename TFunctorTypeMap>
void FunctorChoiceWidget::InitializeComboBox(const TFunctorTypeMap& functor_enabled)
{
    //TODO:@todo : the following will become a little bit more complicated as soon as
    // some functor will share the same widget (for instance it could be the case for
    // all the kernel of a family) : we should check if the widget for the family is already
    // added and if yes update it accordingly

    for(const auto& pair : functor_enabled.mapper()) {
        // check if it register in the KernelWidgetFactory
        if(FunctorWidgetFactory::GetInstance().find(pair.first)) {
            comboBox_functor->addItem(pair.first.c_str(), QVariant(pair.first.c_str()));
        }
    }

    comboBox_functor->addItem(QString("Not Implemented !"), QVariant(""));

    connect(comboBox_functor,SIGNAL(activated(int)),this, SLOT(UpdateWidgetFromComboBox()));
}

} // end of namespace expressive


/*
    // Return only the type of the currently chosen kernel
    const FunctorTypeTree& KernelType();

    // Return the current kernel (Careful the returned Kernel will change if the selection changed).
    const Functor& Kernel() const { return *current_kernel_; }

signals:
    void kernelChanged(); // emitted if the kernel type changed
    void kernelParametersChanged(); // emitted if the kernel type is the same but the parameters changed

private slots:
    void KernelFamilyChoice();
    void KernelChoice();
    void EmitKernelParametersChanged(); // used to emit kernelParametersChanged after having updated all what should be

private:

    Functor* current_kernel_;

    KernelWidget* current_kernel_widget_;
    QComboBox* current_family_combo_box_;

    // Keep the item accessible in order to enable/disable it, one map for the kernel family and one for each family
    std::map<std::string,QStandardItem*> comboBox_kernel_family_item_map_;
    //
    std::map<std::string,QStandardItem*> comboBox_cauchy_family_item_map_;
    std::map<std::string,QStandardItem*> comboBox_inverse_family_item_map_;
    std::map<std::string,QStandardItem*> comboBox_compact_poly_family_item_map_;
    std::map<std::string,QStandardItem*> comboBox_distance_family_item_map_;
    std::map<std::string,QStandardItem*> comboBox_other_family_item_map_;

    //////////////////////////////////////////////////////////////////////////////////////////////
    // Map for emulating a switch statement in :
    //      UpdateWidgetFromKernel
    //////////////////////////////////////////////////////////////////////////////////////////////
    // The following structure will be useful when refactoring the other switch statement in this object.
    struct KernelMapNode {
        KernelMapNode()
            : kernel_widget(NULL)
        {}
        KernelWidget* kernel_widget;
    };
    //-------------------------------------------------------------------------
    Convol::FunctorTypeMapT<KernelMapNode> kernel_map_;
*/

#endif // KERNEL_CHOICE_WIDGET_H
