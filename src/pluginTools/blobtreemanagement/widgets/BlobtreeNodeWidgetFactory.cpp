#include <pluginTools/blobtreemanagement/widgets/BlobtreeNodeWidgetFactory.h>

using namespace expressive::core;

namespace expressive {

BlobtreeNodeWidgetFactory& BlobtreeNodeWidgetFactory::GetInstance()
{
    static BlobtreeNodeWidgetFactory INSTANCE = BlobtreeNodeWidgetFactory();
    return INSTANCE;
}

auto BlobtreeNodeWidgetFactory::CreateNewBlobtreeNodeWidget(const std::string &node_name)
-> AbstractNodeOperatorWidget*
{
    auto it = types.find(node_name);
    if(it != types.end())
    {
        return it->second();
    }
    else
    {
        ork::Logger::ERROR_LOGGER->log("PLUGINTOOLS", "BlobtreeNodeWidgetFactory : primitive not registered" );
        throw std::exception();
    }
//    return nullptr; // unreachable
}


void BlobtreeNodeWidgetFactory::InitializeComboBox(QComboBox* combo_box)
{
    for(auto& pair : types) {
        combo_box->addItem(pair.first.c_str(), QVariant(pair.first.c_str()));
        //TODO:@todo : the first pair.first could be replaced by a more user friendly name
    }
}


} // Close namespace expressive
