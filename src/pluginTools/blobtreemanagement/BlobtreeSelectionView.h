/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef BLOBTREE_SELECTION_VIEW_H
#define BLOBTREE_SELECTION_VIEW_H

////////////////////////////////////////////////////////////////////////////
///TODO:@todo : ce fichier est pour le moment completement moche ...
/// il y a ici une partie du code de BlobtreeView qui a été recopié de manière provisoir ...
/// cette classe est peut être a supprimer et à remplacer partout par BlobtreeView si il n'y a pas de nouvelle
/// modification qui y sont faite (il faudrat alors un booléen pour autorisé ou non certaine action en fonction)
////////////////////////////////////////////////////////////////////////////

#include <convol/blobtreenode/BlobtreeNode.h>
#include <convol/blobtreenode/BlobtreeRoot.h>

#include <pluginTools/blobtreemanagement/BlobtreeViewWidget.h>

// Qt dependencies
#include <QTreeView>
#include <QMenu>
#include <QContextMenuEvent>


namespace expressive {

//class BlobtreeModel;

class BlobtreeSelectionView : public QTreeView
{
    Q_OBJECT

public:

    BlobtreeSelectionView(QWidget* parent = 0);
    ~BlobtreeSelectionView();

    void mousePressEvent(QMouseEvent *event);

    ///////////////////////////////////////
    // Functions for View time-coherence //
    ///////////////////////////////////////

    /*!
     *  Use saved data structure (of bt_view_panel_) to set the node in there
     *  previous state (expanded or not) before reset of the model and view
     */
    void UpdateNodeViewState();

    ///////////////
    // Modifiers //
    ///////////////

    void set_blobtree_view_widget(BlobtreeViewWidget *bt_view_widget);

    ////////////////////////////////////
    // Recursive auxiliary functions  //
    ////////////////////////////////////

private:
    void UpdateViewStateStateChildren(QModelIndex & index);

    ///////////////
    // Attributs //
    ///////////////

public:
    BlobtreeViewWidget *bt_view_widget_;
};

} // end of namespace expressive

#endif // BLOBTREE_SELECTION_VIEW_H
